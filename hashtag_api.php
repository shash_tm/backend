<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/HashTag.class.php";

try{
	$data = $_POST;
	$func=new functionClass();
	$login_mobile = $func->isMobileLogin();
	$user = functionClass::getUserDetailsFromSession ();	
	if(isset($user['user_id']))
	{
		$hashtagApi = new HashTag($user['user_id']);	
		if(isset($data['action']) && $data['action']=='search') {
			$hashtagApi->searchHashTag($data['text']);
		}
	}
}
catch (Exception $e) {
	trigger_error("PHP Web:".$e->getMessage(), E_USER_WARNING);
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);	
}


?>