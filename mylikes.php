<?php
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/header.php";



class MyLikes {

	private $user_id;
	private $conn;
	private $user_utils_dbo;
	private $user;
	private $user_utils;

	function __construct($user) {
		$this->user=$user;
		$this->user_id = $user->getUserID();
		$this->user_utils_dbo = new userUtilsDBO();
		$this->user_utils=new UserUtils();
	}

	public function getLikesData(){
		global $baseurl;

		$data = null;
		$likedData = $this->user_utils_dbo->getMyLikesQueue($this->user_id, $limit = 20);
		/*	echo '<pre>';
		 var_dump($likedData);*/
		$likedIds = null;
		foreach ($likedData as $val){
			$likedIds[] = $val['user2'];
			$like_data[$val['user2']]['timestamp']  = $val['timestamp'];
		}

		//	var_dump($likedIds);die();
		if($likedIds != null)
		$tileData = $this->user_utils_dbo->getTileDate($likedIds);
		else $tileData = null;

		$newTileDate = null;
		foreach ($tileData as $v){
			$newTileDate[$v['user_id']] = $v;
		}
		$my_gender=$this->user->fetchGender();
		$other_gender=($my_gender=="F")?"M":"F";
		$result=array();

		foreach($likedIds as $v){
			$val = $newTileDate[$v];
			if($val['status'] != "authentic") continue;
			$data=array();
			$user_id=$val['user_id'];
			$data['user_id']=$user_id;
			$data['profile_data']['timestamp']= date('Y-m-d', strtotime($like_data[$user_id]['timestamp']));
			$data['profile_data']['fname']=str_repeat("x",strlen($val['fname']));//$val['fname'];
			$data['profile_data']['age']=$val['age'];
			$data['profile_data']['city']=($val['stay_city_display']==null)?$val['state']:$val['stay_city_display'];
			$data['profile_data']['profile_pic'] = $this->user_utils->getProfilePic($val['thumbnail'], $other_gender);
			$data['profile_data']['profile_id']=$user_id;
			$data['profile_data']['profile_link']=$this->user_utils->generateProfileLink($this->user_id,$user_id). '&from_like=true';
			$result[]=$data;
		}

		return $result;

	}

}


try {
	//get user details from session should return if user is valid or not

	$device_type = Utils::getDeviceType();
	if($device_type == "mobile") $flag = true; else $flag=false;
	functionClass::redirect("myLikes", $flag);
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$user=new UserData($user_id);

	$myLikes = new MyLikes($user);
	$data = $myLikes->getLikesData();
	//$data=$MutualLikes->getMutualLikes($_REQUEST['page_id']);

	$device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
	if ($device_type == 'mobile') {
		$response=array();
		$response['responseCode']=200;
		$response['data']=$data;

		echo json_encode($response);
		exit;
	}
		
	$header = new header($user_id);
	$header_values = $header->getHeaderValues();
	$smarty->assign("header", $header_values);
	$smarty->assign("mutual_matches",$data);
	$smarty->assign("my_id", $user_id);

	$smarty->display("templates/dashboard/mylikes.tpl");

	//$status = $user['status'];


}
catch(Exception $e){
	//echo $e->getMessage();
	trigger_error("PHP WEB:".$e->getTraceAsString(), E_USER_WARNING);
}

?>








