<?php 

require_once (dirname ( __FILE__ ) . '/UserInterestHobbies.php');
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once (dirname ( __FILE__ ) . '/UserSuggestions.php');
require_once (dirname ( __FILE__ ) . '/FacebookPageSearch.php');
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../fb.php";
require_once dirname ( __FILE__ ) . "/FacebookLikesUtils.php";


try {
	$user = functionClass::getUserDetailsFromSession();
	$user_id = $user ['user_id'];
	
	if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'get_likes') {
		$uih = new UserInterestHobbies();
		$data = $uih->get_user_interest_hobbies(array($user_id));
		$data = isset($data[$user_id]) ? $data[$user_id] : array();
		$response = array("responseCode" => 200, "likes" => FacebookLikesUtils::process_response($data));
		echo json_encode($response);
	}
	
	if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'suggestions') {
		$categories = isset($_REQUEST['categories']) ? explode(',', $_REQUEST['categories']) : array();
		$us = new UserSuggestions();
		$suggestions = array();
		foreach ($categories as $category) {
			$suggestions = array_merge($suggestions, FacebookLikesUtils::process_response($us->get_suggestions(
						$user_id, $category)));
		}
		$response = array("responseCode" => 200, "suggestions" => $suggestions);
		echo json_encode($response);
	}
	
	if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'save_likes') {
		$data = isset($_REQUEST['data']) ? $_REQUEST['data'] : "{}";
		$data = json_decode($data, true);
		$added_pages = isset($data['add']) ? $data['add'] : array();
		$removed_pages = isset($data['remove']) ? $data['remove'] : array();
		
		$uih = new UserInterestHobbies();
		$uih->save_pages($user_id, $added_pages, $removed_pages);
		$response = array("responseCode" => 200);
		echo json_encode($response);
	}

	if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'search_page') {
		$search_text = isset($_REQUEST['search_text']) ? $_REQUEST['search_text'] : "";
		$category = isset($_REQUEST['category']) ? $_REQUEST['category'] : "";
		$response = array("responseCode" => 200);
		$result = array();
		if (strlen($search_text) >= 3)
			$result = FacebookLikesUtils::process_response(FacebookPageSearch::search_pages($category, $search_text));
		$response["pages"] = $result;
		echo json_encode($response);
	}

} catch (Exception $e) {
	trigger_error("PHP Web:" . $e->getTraceAsString(), E_USER_WARNING);
}

?>
