<?php 

require_once (dirname ( __FILE__ ) . '/../include/Utils.php');

class FacebookLikesUtils {
	
	// Facebook Likes related responses as array to maintain the order at the client
	public static function process_response($response) {
	        $result = array();
        	foreach ($response as $cat => $cat_data) {
                	if (isset(Utils::$page_categories_db_fields_map[$cat]) || isset(Utils::$page_categories_db_fields_reverse_map[$cat])) {
                        	$cat_result = array();
	                        foreach ($cat_data as $page_id => $data) {
        	                        $cat_result[] = array_merge(array("id" => $page_id), $data);
                	        }
                        	$result[$cat] = $cat_result;
	                } else {
        	                $result[$cat] = $cat_data;
                	}
        	}
        	return $result;
	}

}


