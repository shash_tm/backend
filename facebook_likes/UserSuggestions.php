<?php 

require_once dirname(__FILE__) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../DBO/facebook_likes/ZoneMappingDBO.php";
require_once dirname(__FILE__) . '/../DBO/facebook_likes/FacebookPageDBO.php';
require_once dirname(__FILE__) . '/../DBO/facebook_likes/TopFacebookPageDBO.php';
require_once dirname(__FILE__) . '/UserInterestHobbies.php';
require_once dirname(__FILE__) . "/../include/Utils.php";

class UserSuggestions {
	
	public function __construct() {
		
	}
	
	/* return 50 suggesions for category input excluding pages liked by user */
	public function get_suggestions($user_id, $category) {
		
		// get all pages ids liked by user
		$uih = new UserInterestHobbies();
		$user_liked_page_ids = $uih->get_all_user_liked_page_ids($user_id);
		$user_liked_pages_map = array();
		foreach ($user_liked_page_ids as $page_id)
			$user_liked_pages_map[$page_id] = true;
		
		// get user's zone from state->zone mapping
		$uu_dbo = new userUtilsDBO();
		$my_data = $uu_dbo->getUserData(array($user_id), true);
		$state = $my_data['stay_state'];
		$zone = ZoneMappingDBO::get_zone($state);
		
		$all_pages = array();
		$category_suggestions = array();
		$categories = array($category);
		// when food requested show travel suggestions as well
		foreach (Utils::$merge_categories_map as $from_cat => $to_cat) {
			$target_cat = Utils::$page_categories_db_fields_reverse_map[$to_cat];
			if ($target_cat == $category)
				$categories[] = Utils::$page_categories_db_fields_reverse_map[$from_cat];
		}
		
		// SATISH TODO: cache top pages ?
		$suggestions = TopFacebookPageDBO::get_random_suggestions($zone, $categories);
		$category_suggestions[$category] = $suggestions;
		$all_pages = array_merge($all_pages, $category_suggestions[$category]);
		
		$db_pages = FacebookPageDBO::get_facebook_pages($all_pages);
		$page_names_map = array();
		foreach ($db_pages as $db_page) {
			$page_names_map[$db_page['page_id']] = $db_page['name'];
		}
		
		// exlude already liked pages and limit 50 suggestions in the result
		$result = array();
		foreach ($category_suggestions as $cat => $suggestions) {
			$result[$cat] = array();
			foreach ($suggestions as $page_id) {
				if (count($result[$cat]) < Utils::$suggestions_count && strlen($page_names_map[$page_id]) > 1 && 
						!isset($user_liked_pages_map[$page_id]))
					$result[$cat][$page_id]['name'] = $page_names_map[$page_id];
			}
		}
		return $result;
	}
}

?>
