<?php

require_once (dirname ( __FILE__ ) . '/../DBO/facebook_likes/FacebookPageDBO.php');
require_once (dirname ( __FILE__ ) . '/../include/Utils.php');
require_once (dirname ( __FILE__ ) . '/../include/config.php');
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";

/**
 * Here's the overview of search using Redis 

1. We're using sorted set data structure provided by redis for search
    ----> All the pages are are populated with same score so they'll be ordered by lexographic order
    ----> We're using zrangebylex to search pages with prefix matches
2. Popularity of facebook pages
    ----> Using interest hobbies data of users from user_search table, number of likes for pages are determined
3. We split the name of a page with common delimiters and populate redis along with popularity so that the page is searchable 
with any word in the title of the page.
    ----> The format is : suffix name##actual name##page_id##popularity
4. When user searches for some page, we get 1000 pages from redis using zrangebylex and sort these pages by poppularity and 
remove duplicates and send back 10 pages to client

 *  
 * Search Redis configs are read from system configs table.
 * All operations on Search Redis instance are in try/catch block. App should work fine if there's issue with search redis.
 * */

class FacebookPageSearch {

	public static function search_pages($category, $search_text) {
		$result = FacebookPageSearch::search_pages_from_redis($category, $search_text);
		if (Utils::$fallback_search_from_db_enabled && count($result[$category]) == 0)
			$result = FacebookPageSearch::search_pages_from_db($category, $search_text);
		if (Utils::$ugc_enabled && count($result[$category]) == 0) {
			$ugc_id = substr("ugc:" . strtolower($search_text), 0, Utils::$ugc_id_max_length);
			$ugc_result = array($ugc_id => array("name" => $search_text));
			$result[$category] = isset($result[$category]) ? array_merge($ugc_result, $result[$category]) : $ugc_result;
		}
		return $result;
	}

	public static function search_pages_from_db($category, $search_text) {
		$result = array();
		$db_pages = FacebookPageDBO::search_facebook_pages($category, $search_text);
		foreach ($db_pages as $db_page) {
			$page_id = $db_page['page_id'];
			$cat = $db_page['tm_category'];
			$result[$cat][$page_id]['name'] = $db_page['name'];
		}
		return $result;
	}

	
	/* Adding page to redis used for search so that its easily searchable starting with any word in the title */
	public static function check_and_add_pages_to_redis($pages) {
		try {
			$redis = FacebookPageSearch::get_redis_search_instance();
			$redis_map = array();
			
			foreach ($pages as $cat => $cat_data) {
				$category_key = strtolower($cat) . Utils::$redis_suffix_for_fb_pages;
				foreach ($cat_data as $page_id => $value) {
					$title = strtolower($value);
					$title = trim(preg_replace('!\s+!', ' ', str_replace(array ("\r", "\t", "\n"), " ", $title)));
					$words = explode(" ", $title);
					for ($i = 0; $i < count($words); $i ++) {
						$key = "";
						for ($j = $i; $j < count($words); $j ++) {
							if (strlen($key) > 0) $key .= " ";
							$key .= $words[$j];
						}
						$key .= Utils::$redis_search_delimiter . $value . Utils::$redis_search_delimiter . $page_id 
							. Utils::$redis_search_delimiter . "0";
					
						$redis_map[$category_key][] = Utils::$redis_score_for_fb_page;
						$redis_map[$category_key][] = $key;
					}
				}
			}
		
			foreach ($redis_map as $cat_key => $cat_data) {
				call_user_func_array(array($redis, 'ZADD'), array_merge(array($cat_key), $cat_data));
			}
		
			$redis->close();
		} catch (Exception $e) {
			trigger_error("PHP Web:" . $e->getMessage(), E_USER_WARNING);
		}
	}


	// redis for search
	public static function get_redis_search_instance() {
		try {
			$uf = new UserFlagsDBO();
			$flags = $uf->read("search_redis_configs");
			$redis = new Redis();
			$redis->connect($flags['search_redis_ip'], $flags['search_redis_port'], 5);
			return $redis;
		} catch (Exception $e) {
			trigger_error("PHP redis connect:" . $e->getMessage(), E_USER_WARNING);
		}
	}
	
	// food and travel are merged and stored as food in redis
	public static function search_pages_from_redis($category, $search_text) {
		$result = array($category => array());
		try {
			$redis = FacebookPageSearch::get_redis_search_instance();	
			$redis_key = $category . "_pages";
			$search_text = strtolower($search_text);
			$start_str = "[" . $search_text;
			$end_str = "[". $search_text . "\xff";
			$start_time =  microtime(true);
			// get 1000 matching pages from redis
			$items = $redis->zrangebylex($redis_key, $start_str, $end_str, 0, Utils::$pages_to_consider_from_redis_search);
			$popularity_map = array();
			$all_matches = array();
			foreach ($items as $item) {
				$words = explode(Utils::$redis_search_delimiter, $item);
				if (count($words) == 4) {
					$page_id = $words[2];
					$name = $words[1];
					$all_matches[$page_id] = $name;
					$popularity_map[$page_id] = intval($words[3]);
				}
			}

			// sort based on popularity
			arsort($popularity_map);
			$result_map = array();
			$name_page_ids_map = array();
			$result_count = 0;
			
			// consider top 10 pages from pages sorted by popularity with no duplicate names
			foreach ($popularity_map as $page_id => $popularity) {
				if ($result_count >= Utils::$pages_to_return_in_search)
					break;
				if (!isset($name_page_ids_map[$all_matches[$page_id]])) {
					$name_page_ids_map[$all_matches[$page_id]] = true;
					$result_map[$page_id] = $popularity;
					$result_count ++;
				}
			}
			
			$search_results = array();
			foreach ($result_map as $page_id => $popularity) {
				$search_results[$page_id]['name'] = $all_matches[$page_id];
			}
			$result[$category] = $search_results;
			$redis->close(); // SATISH TODO: is this required ?
		} catch (Exception $e) {
			trigger_error("PHP redis connect:" . $e->getMessage(), E_USER_WARNING);
		}
		return $result;
	}
}

?>


