<?php 

require_once (dirname ( __FILE__ ) . '/../DBO/facebook_likes/UserInterestHobbiesDBO.php');
require_once (dirname ( __FILE__ ) . '/../DBO/facebook_likes/FacebookPageDBO.php');
require_once (dirname ( __FILE__ ) . '/../include/Utils.php');

class UserInterestHobbies {
	
	public function __construct() {
		
	}
	

	public function get_user_interest_hobbies($users) {
		$uih = new UserInterestHobbiesDBO();
		$data = $uih->get_user_interest_hobbies($users);
		$result = array();
		foreach ($data as $user_id => $fav) {
			$result[$user_id] = $this->convert_to_assoc_array($fav);
		}
		return $result;
	}
	
	/* returns ids of all pages liked by user */
	public function get_all_user_liked_page_ids($user_id) {
		$user_liked_pages = $this->get_user_interest_hobbies(array($user_id));
		$user_liked_pages = $user_liked_pages[$user_id];
		$user_liked_page_ids = array();
		foreach (array_values(Utils::$page_categories_db_fields_map) as $cat) {
			$user_liked_page_ids = array_merge($user_liked_page_ids, isset($user_liked_pages[$cat]) ? 
					array_keys($user_liked_pages[$cat]) : array());
		}
		return $user_liked_page_ids;
	}
	
	
	/* adds common=true field for user's favs and match user's favs */
	public function process_common_favorites($my_fav, $match_fav) {
		foreach (array_values(Utils::$page_categories_db_fields_map) as $cat) {
			$my_page_ids = isset($my_fav[$cat]) ? array_keys($my_fav[$cat]) : array();
			$match_page_ids = isset($match_fav[$cat]) ? array_keys($match_fav[$cat]) : array();
			$common_page_ids = array_intersect($my_page_ids, $match_page_ids);
			foreach ($common_page_ids as $page_id) {
				$match_fav[$cat][$page_id]['common'] = true;
			}
		}
		
		$result = array();
		
		// show commons first
		foreach ($match_fav as $cat => $cat_pages) {
			foreach ($cat_pages as $page_id => $page_info) {
				if (isset($match_fav[$cat][$page_id]['common']))
					$result[$cat][$page_id] = $match_fav[$cat][$page_id];
			}
		}
		
		foreach ($match_fav as $cat => $cat_pages) {
			foreach ($cat_pages as $page_id => $page_info) {
				if (!isset($result[$cat][$page_id]))
					$result[$cat][$page_id] = $match_fav[$cat][$page_id];
			}
		}
		
		return $result;
	}

	
	private function convert_to_assoc_array($input_data) {
		$data = array();
		
		if (count($input_data) > 0) {
			foreach ($input_data as $cat => $fav) {
				if (isset(Utils::$merge_categories_map[$cat]))
					$cat = Utils::$merge_categories_map[$cat];
				$data[$cat] = array_merge(isset($data[$cat]) ? $data[$cat] : array(), (array)json_decode($fav, true));
			}
		}	
		
		$result = array();
		
		foreach ($data as $category => $cat_data) {
			if (in_array($category, array_values(Utils::$page_categories_db_fields_map))) {
				foreach ($cat_data as $key => $value)
					$result[$category][$key]['name'] = $value;
			}
			else
				$result[$category] = $data[$category];
		}
		
		return $result;
	}
	
	
	/* save pages to interest hobbies table for the user */
	public function save_pages($user_id, $added_pages, $removed_pages) {
		$pages = array_merge(array_keys($added_pages), array_keys($removed_pages));
		$db_pages = FacebookPageDBO::get_facebook_pages($pages);
		$page_names_map = array();
		$page_categories_map = array();
		$all_pages = array_merge($added_pages, $removed_pages);
		
		// use name and category if they are in request
		foreach ($all_pages as $page_id => $page_info) {
			if (!isset($page_names_map[$page_id]) && isset($page_info['name']))
				$page_names_map[$page_id] = $page_info['name'];
			if (!isset($page_categories_map[$page_id]) && isset($page_info['cat'])) {
				$cat = Utils::$page_categories_db_fields_map[$page_info['cat']];
				$page_categories_map[$page_id] = $cat;
			}
		}

		// find the name and category of page from db
		foreach ($db_pages as $db_page) {
			$page_id = $db_page['page_id'];
			if (!isset($page_names_map[$page_id]) && isset($db_page['name']))
				$page_names_map[$page_id] = $db_page['name'];
				if (!isset($page_categories_map[$page_id]) && isset($db_page['tm_category'])) {
					$cat = Utils::$page_categories_db_fields_map[$db_page['tm_category']];
					$page_categories_map[$page_id] = $cat;
				}
		}

		$uih = new UserInterestHobbiesDBO();
		$result = $uih->get_user_interest_hobbies(array($user_id));
		$result = $result[$user_id]; // SATISH TODO: add checks related to object not present everywhere
		$fav_data = array();
		foreach ($result as $cat => $fav) {
			$fav_data[$cat] = (array)json_decode($fav, true);
		}
		
		// add newly added pages at the beginnig, so that they'll appear in reverse chrnological order on the app
		$updated_favorites = array();
		foreach (array_keys($added_pages) as $page) {
			$page_ids = array();
			if (isset($page_categories_map[$page]) && $updated_favorites[$page_categories_map[$page]])
				$page_ids = array_keys($updated_favorites[$page_categories_map[$page]]);
				if (!in_array($page, $page_ids))
					$updated_favorites[$page_categories_map[$page]][$page] = $page_names_map[$page];
		}
		
		// pages which were already with the user
		foreach (array_values(Utils::$page_categories_db_fields_map) as $cat) {
			foreach ($fav_data[$cat] as $page => $name) {
				$page_ids = array();
				if (isset($updated_favorites[$cat])) $page_ids = array_keys($updated_favorites[$cat]);
				if (!in_array($page, $page_ids)) $updated_favorites[$cat][$page] = $name;
			}
		}
		
		// removing the pages
		foreach (array_keys($removed_pages) as $page) {
			// handle food, travel merge (todo right way)
			$t_category = null;
			if (isset(Utils::$merge_categories_reverse_map[$page_categories_map[$page]]))
				$t_category = Utils::$merge_categories_reverse_map[$page_categories_map[$page]];
			if ($t_category == null && isset(Utils::$merge_categories_map[$page_categories_map[$page]]))
				$t_category = Utils::$merge_categories_map[$page_categories_map[$page]];
			$categories = array($page_categories_map[$page]);
			if ($t_category != null)
				$categories[] = $t_category;
			foreach ($categories as $category) {
				if (in_array($page, array_keys($updated_favorites[$category])))
					unset($updated_favorites[$category][$page]);
			}
		}
	
		if (count($result) > 0)
			$uih->update_user_interest_hobbies($user_id, $updated_favorites);
		else
			$uih->insert_user_interest_hobbies($user_id, $updated_favorites);
	}
	
}

?>
