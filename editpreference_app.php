<?php 
require_once (dirname ( __FILE__ ) . '/include/config.php');
require_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . '/UserData.php');
require_once (dirname ( __FILE__ ) . '/UserUtils.php');
require_once (dirname ( __FILE__ ) . "/include/header.php");
require_once (dirname ( __FILE__ ) . "/DBO/user_flagsDBO.php");

class editPreference{
	
	private $login_mobile;
	private $conn;
	private $func;
	private $smarty;
	private $user_id;
	private $uu;
	private $redis;
	private $india = 113;
	private $united_states = 254;
	public function __construct(){
		global $conn,$smarty,$redis;
		$this->func = new functionClass();
		$this->login_mobile = $this->func->isMobileLogin();
		$session=functionClass::getUserDetailsFromSession();
		$this->user_id=$session['user_id'];
		$this->conn = $conn;
		$this->smarty = $smarty;
		$this->uu = new UserUtils();
		$this->redis = $redis;
	}
	
	function getData(){
		global $cdnurl,$registerJsonUrl,$registerSingaporeJsonUrl,$registerIndonesiaJsonUrl,$registerUSJsonUrl;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		
		$countriesSG = 113;
		if(!$this->login_mobile){
			//$religions_spouse=array("0"=>array("id"=>"Hindu","value"=>"Hindu"),"1"=>array("id"=>"Muslim","value"=>"Muslim"),"2"=>array("id"=>"Sikh","value"=>"Sikh"),"3"=>array("id"=>"Christian","value"=>"Christian"),"4"=>array("id"=>"Buddhist","value"=>"Buddhist"),"5"=>array("id"=>"Jain","value"=>"Jain"));
			$income_spouse=array("0"=>array("id"=>"0-10","value"=>"<10 lakhs"),"1"=>array("id"=>"10-20","value"=>"10 lakhs to 20 lakhs"),"2"=>array("id"=>"20-50","value"=>"20 lakhs to 50 lakhs"),"3"=>array("id"=>"50-51","value"=>">50 lakhs"));
			
			$header = new header($this->user_id);
			$header_values = $header->getHeaderValues();
			
			$rs = $this->conn->Execute("select id,degree_name as value from highest_degree order by id");
			$highest_degree = $rs->GetRows();
			
			//$rs = $this->conn->Execute ( "select * from geo_country where country_id=113" );
			$allcountries[0] = array("country_id"=>"113","name"=>"India");//$rs->GetRows();
			
			$rs = $this->conn->Execute ( "select * from geo_state where country_id=113" );
			$allstates = $rs->GetRows();
			
			$this->smarty->assign ( 'countries', $allcountries);
			$this->smarty->assign ( 'states', json_encode($allstates));
			$this->smarty->assign ( 'income', $income_spouse);
			$this->smarty->assign ( 'highest_degree', $highest_degree);
			$this->smarty->assign('header',$header_values);
		}else{
			$rs = $this->conn->Execute ( "select stay_country from user_data where user_id='" . $this->user_id . "'" );
			if ($rs->_numOfRows > 0) {
				$resultP = $rs->FetchRow();
				/*if(isset($resultP['stay_country']) && $resultP['stay_country']=='219'){
					$countriesSG = 219;
					$response['basics_data'] = array("url"=>$registerSingaporeJsonUrl);
				}else if(isset($resultP['stay_country']) && $resultP['stay_country']=='114') {
					$countriesSG = 114;
					$response['basics_data'] = array("url"=>$registerIndonesiaJsonUrl);
				}else*/ if(isset($resultP['stay_country']) && $resultP['stay_country']=='254') {
					$countriesSG = 254;
					$response['basics_data'] = array("url"=>$registerUSJsonUrl);
				}
				else 
					$response['basics_data'] = array("url"=>$registerJsonUrl);
			}
			$response['responseCode'] = 200;
			//$response['basics_data'] = array("url"=>$registerJsonUrl);//$cdnurl."/register_data.json?ver=10.3");
		}
	
		$rs = $this->conn->Execute ( "select user_id,start_age,end_age,marital_status,religionCaste,location,start_height,end_height,smoking_status,drinking_status,food_status,income,education from user_preference_data where user_id='" . $this->user_id . "'" );
		if ($rs->_numOfRows > 0) {
			$demography = $rs->FetchRow();
			$countryWise = explode ( ";", $demography['location'] );
			$countries = $states = $cities = '';
			foreach ( $countryWise as $countrywise ) {
				$stateWise = explode ( ":", $countrywise );
				if ($countries == '' && $stateWise [0] != '')
					$countries = $stateWise [0];
				else if ($stateWise [0] != '')
					$countries .= "," . $stateWise [0];
				$country = $stateWise [0];
				unset ( $stateWise [0] );
				foreach ( $stateWise as $statewise ) {
					$citiWise = explode ( "-", $statewise );
					if ($states == '' && $citiWise [0] != '')
						$states = $country . "-" . $citiWise [0];
					else if ($citiWise [0] != '')
						$states .= "," . $country . "-" . $citiWise [0];
					$state = $citiWise [0];
					unset ( $citiWise [0] );
					foreach ( $citiWise as $citywise )
						if ($cities == '' && $citywise != '')
							$cities = $country . "-" . $state . "-" . $citywise;
						else if ($citywise != '')
							$cities .= "," . $country . "-" . $state . "-" . $citywise;
				}
			}
			$religionWise = explode ( ";", $demography['religionCaste'] );
			$religions = '';
			
			foreach ( $religionWise as $religionwise ) {
				$casteWise = explode ( ":", $religionwise );
				if ($religions == '' && $casteWise [0] != '')
					$religions = $casteWise [0];
				else if ($casteWise [0] != '')
					$religions .= "," . $casteWise [0];
				unset ( $casteWise [0] );
			}
			unset ( $demography['religionCaste'] );
			unset ( $demography['location'] );
			
			$demo[0]['user_id'] = $demography['user_id'];
			$demo[0]['start_age'] = isset($demography['start_age'])?$demography['start_age']:"18";
			$demo[0]['end_age'] = isset($demography['end_age'])?$demography['end_age']:"70";
			$demo[0]['marital_status'] = $demography['marital_status'];
			$demo[0]['religions'] = $religions; 
			$demo[0]['countries'] = $countriesSG;//$countries;          //fix for android app ver 67 Backend-243
			$demo[0]['states'] = $states;
			$demo[0]['cities'] = $cities;
			$demo[0]['marital_status_new'] = $demography['marital_status'];
			if(isset($demography['marital_status'])) {
				$demo[0]['marital_status'] = $this->uu->getMaritalStatusForEditPage($demography['marital_status']);
				$demo[0]['marital_status_new'] = $this->uu->getPreferenceMaritalStatus($demography['marital_status']);
			}
			
			$trait[0]['start_height'] = $demography['start_height'];
			$trait[0]['end_height'] = $demography['end_height'];
			$trait[0]['smoking_status'] = $demography['smoking_status'];
			$trait[0]['drinking_status'] = $demography['drinking_status'];
			$trait[0]['food_status'] = $demography['food_status'];
			
			$work[0]['income'] = json_decode($demography['income'],true);
			$work[0]['education'] = $demography['education'];
			
			if($this->login_mobile){
				$response['demo'] = $demo[0];
				$response['trait'] = $trait[0];
				$response['work'] = $work[0];
				$selected_countries_arr = explode(',',$countries);
				if($this->redis->sIsMember('active_regions',$resultP['stay_country']) == true)
				{
					$regions = $this->redis->sGetMembers('active_regions');
					$region_list = implode(',',$regions);
					$region_list .= ',' . $this->india ;
					$sql = "Select country_id, name from geo_country WHERE  country_id in ($region_list)";
					$country_res = $this->conn->Execute($sql);
					$available_countries = array();
					while($row_country = $country_res->FetchRow())
					{
						$available_countries[] = $row_country;
					}
					$response['active_countries'] = $available_countries;
					$response['selected_countries'] = $selected_countries_arr;
				}
				else
				{
					$response['show_us_profiles'] = in_array($this->united_states ,$selected_countries_arr);
				}

				$user_flags = new UserFlagsDBO();
				$international_flags = $user_flags->read('international_flags');
				$response['us_profiles_enabled'] = ($international_flags['us_profiles_enabled'] == 'true')? true : false;



			}else{
				$this->smarty->assign('demo', json_encode($demo[0]));
				$this->smarty->assign('trait', json_encode($trait[0]));
				$this->smarty->assign('work', json_encode($work[0]));
			}
		}
	
		if($this->login_mobile){
			
			$resource=$response;
			unset($resource['responseCode']);
			$resource=json_encode($resource);
			$hashCheck=functionClass::checkHash($resource,$_REQUEST['hash']);
				
			//var_dump($hashCheck);
				
			if($hashCheck['status']==false)
				$response['hash']=$hashCheck['hash'];
				else
				$response=array("responseCode"=>304);
			
		    $response["tstamp"]=time();
			print_r(json_encode($response));
			die;
		}else{
			$this->smarty->display ( dirname ( __FILE__ ) . '/templates/editpreference.tpl' );
		}
	}
}

try{
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	
	functionClass::redirect ( 'editpartner',$login_mobile );
	
	$myProfile = new editPreference ();
	$data = $_REQUEST;
	
	$myProfile->getData();
	
}catch(Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}

?>