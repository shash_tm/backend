<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/mailerAdminClass.php";

try
{
    if($_SESSION['super_admin'] == 'yes' || ( isset($_SESSION['privileges']) && in_array('mailer', $_SESSION['privileges']) ))
    {
        $mailerClass = new MailerAdmin();
        // return a template for main dashboard
        if(isset($_REQUEST['campaign_id']) && $_REQUEST['campaign_id'] != null)
        {
            $campaign_data = $mailerClass->getCampaign($_REQUEST['campaign_id']) ;
            $status = $campaign_data['status'] ;
            if($campaign_data['html'] == null && $status =='test' && $campaign_data['query'] == null)
            {
                // TODO :: Direct to the page to show preview or HTML
                $smarty->assign ("baseurl" , $admin_baseurl) ;
                $smarty->assign ("imageurl" , $imageurl) ;
                $smarty->assign ("campaign" , $campaign_data) ;
                $smarty->display ( "../templates/mailers/mailerPreview.tpl" );
            }
            else if( $status == 'test')
            {
                // TODO:: Show the Final sending option and expected user count
                $smarty->assign ("baseurl" , $admin_baseurl) ;
                $smarty->assign ("imageurl" , $imageurl) ;
                $smarty->assign ("campaign" , $campaign_data) ;
                $smarty->display ( "../templates/mailers/mailerSend.tpl" );
            }else if( $status == 'queried')
            {
                // TODO:: Show the Final sending option and expected user count
                $smarty->assign ("baseurl" , $admin_baseurl) ;
                $smarty->assign ("imageurl" , $imageurl) ;
                $smarty->assign ("campaign" , $campaign_data) ;
                $smarty->display ( "../templates/mailers/mailerSend.tpl" );
            }
            else if ($status =='sending' || $status =='sent' || $status =='failed' || $status =='queued' )
            {
                $smarty->assign ("baseurl" , $admin_baseurl) ;
                $smarty->assign ("imageurl" , $imageurl) ;
                $smarty->assign ("campaign" , $campaign_data) ;
                $smarty->display ( "../templates/mailers/mailerStatus.tpl" );
            }
            else
            {
                $smarty->assign ("baseurl" , $admin_baseurl) ;
                $smarty->assign ("imageurl" , $imageurl) ;
                $smarty->assign ("campaign" , $campaign_data) ;
                $smarty->display ( "../templates/mailers/createCampaign.tpl" );
            }

        }
        else
        {
            $smarty->assign ("baseurl" , $admin_baseurl) ;
            $smarty->assign ("imageurl" , $imageurl) ;
            $smarty->assign ("campaign" , $campaign_data) ;
            $smarty->display ( "../templates/mailers/createCampaign.tpl" );
        }

    }
    else
    {
        //TODO:: Do something for showing unauthorised
        $smarty->display ( "../templates/mailers/mailerDashboard.tpl" );
    }
}
catch (Exception $e)
{
    echo $e->getMessage();
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>