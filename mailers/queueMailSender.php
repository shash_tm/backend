<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/mailerUtils.php";
require_once dirname ( __FILE__ ) . "/../DBO/mailerDBO.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../email/MailFunctions.php";


class  MailSender
{
    private $template_array = array() ;
    private $subject_array = array() ;
    private $mailObject ;

    function __construct()
    {
        $this->mailObject =  new MailFunctions();
    }

    private function getTemplate($campaign_id)
    {
        if(isset($this->template_array[$campaign_id]))
        {
            return $this->template_array[$campaign_id] ;
        }
        else
        {
            $template_arr = MailerDBO::getCampaignDBO($campaign_id);
            $template = base64_decode($template_arr['html']) ;
            $this->template_array[$campaign_id] = $template ;
            return $template ;
        }
    }

    private function getSubject($campaign_id)
    {
        if(isset($this->subject_array[$campaign_id]))
        {
            return $this->subject_array[$campaign_id] ;
        }
        else
        {
            $data = MailerDBO::getCampaignDBO($campaign_id) ;
            $subject = $data['mail_subject'] ;
            $this->subject_array[$campaign_id] = $subject ;
            return $subject ;
        }
    }



    public function sendMailFromQueue($mailData)
    {
       // var_dump($mailData);
        $name = $mailData['name'] ;
        $user_id = $mailData['user_id'] ;
        $email_id = $mailData['email_id'] ;
        $campaign_id = $mailData['campaign_id'] ;

        $mail_subject = $this->getSubject($campaign_id) ;
        $mail_subject = MailerUtils::addNameToSubject($mail_subject,$name) ;

        $template = $this->getTemplate($campaign_id) ;
        $template = MailerUtils::addNameToContent($template,$name) ;
        $template = MailerUtils::addTrackingLinksToTemplate($template,$campaign_id,$user_id,$email_id) ;

        // now that everything is ready, just send the damn mail and make an entry in the DB
        $this->mailObject->sendMail(array($email_id),$template,"",$mail_subject,$user_id);
        MailerDBO::increaseSentCounter($campaign_id);

    }
}