<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../DBO/mailerDBO.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../email/MailFunctions.php";

/*
 * To manage/perform all the actions pertaining to the mailer dashboard
 * Author : Sumit
 *🖖 Live Long and Prosper.
 */

class MailerAdmin
{
    private $mailObject ;
    public $campaign_id ;
    private $conn_reporting ;
    private $emailFrom = 'admintm@trulymadly.com';
    private $redis ;
    function __construct($campaign_id = null)
    {
        global $conn_reporting, $redis ;
        $this->mailObject =  new MailFunctions();
        $this->campaign_id = $campaign_id ;
        $this->conn_reporting = $conn_reporting;
        $this->redis = $redis ;
    }


    public function getCampaign($campaign_id)
    {
        $campaign = MailerDBO::getCampaignDBO($campaign_id) ;
        $campaign['html'] = base64_decode($campaign['html']);
        return $campaign ;
    }

    public function saveNewCampaign($campaign_name)
    {
        $campaign_id = MailerDBO::saveNewCampaignDBO($campaign_name);
        return $campaign_id ;
    }

    public function updateCampaign($campaign_id, $status, $html, $conditions,$subject)
    {
        if($html != null)
        {
            MailerDBO::updateCampaign($campaign_id, $status, array('html'=> base64_encode($html), 'mail_subject'=>$subject ));
        }

        if(is_array($conditions))
        {
            $query = $this->generateQueryFromConditions($conditions['gender'],$conditions['max_age'], $conditions['min_age'], $conditions['cities']) ;
            MailerDBO::updateCampaign($campaign_id, $status, array('query'=>$query));
            $count = $this->getCountForThisQuery($query) ;
            MailerDBO::updateCampaign($campaign_id,$status,array('row_count'=>$count)) ;
        }
    }
    
    public function getCountFordisplay($conditions)
    {
    	$query = $this->generateQueryFromConditions($conditions['gender'],$conditions['max_age'], $conditions['min_age'], $conditions['cities']);
    	$count = $this->getCountForThisQuery($query);
    	return $count ;
    }

    public function updateCampaignStatus($campaign_id, $status)
    {
        MailerDBO::updateCampaign($campaign_id,$status,array());
    }
    
    public function generateImageOnlyHTML($image_url)
    {
    	$html = '<!doctype html>'.PHP_EOL.
				'<html>'.PHP_EOL.
				'<body style="margin:0; padding:0;">'.PHP_EOL.
				'<img src="'. $image_url .'">'.PHP_EOL.
				'</body>'.PHP_EOL.
				'</html>';
    	return base64_encode($html);
    }

    public function sendCampaignToEmails($campaign_id, $emailIds)
    {
        $email_arr = explode(',',$emailIds);
        foreach($email_arr as $email)
        {
            $email_arr = array( "user_id" =>  0,
                "name" => null,
                "email_id" => $email,
                "campaign_id" => $campaign_id);
            $data = json_encode($email_arr);
            // Use rpop at the receiving end since we are using lpush here
            $this->redis->lpush(Utils::$redis_keys['mailing_queue'], $data);
        }
        $this->updateCampaignStatus($campaign_id,'sent');
    }

    private function getCountForThisQuery($query)
    {
        $sql = "Select count(*) as cu " . $query ;
        $resObj = $this->conn_reporting->Execute($sql) ;
        $count_arr= $resObj->FetchRow();
        return $count_arr['cu'] ;
    }


    public function getCampaignList($status = null)
    {
        $campaignList = MailerDBO::getCampaignsListDBO($status) ;
        return $campaignList ;
    }

    public function sendTestMail($email_id, $html_content, $mail_subject)
    {
        $response = array();
        if($this->isTrulyMadlyEmail($email_id))
        {
            try
            {
                // sendEmail has been used for testing only
                //TODO- Switch to ses mail before going live
               //$mid =$this->sendMailAndLog($email_id,$html_content,$mail_subject);
               Utils::sendEmail($email_id, $this->emailFrom,$mail_subject,$html_content,true);
                $mid ='1212';
            }
            catch (Exception $e)
            {
                trigger_error("PHP Mailer sending from dashboard failed ".$e->getMessage(),E_USER_WARNING);
            }
            if($mid)
            {
                $response['status'] = "success" ;
            }
            else
            {
                $response['status'] = "failure" ;
                $response['error'] = "Mail Could Not be Sent" ;
            }
        }
        else
        {
            $response['status'] = "failure" ;
            $response['error'] = "That wasn't a TrulyMadly Email ID , Was it ?";
        }
        return $response;
    }


    public function sendMailAndLog($email_id, $html_content, $subject)
    {
        $mid= $this->mailObject->sendMail(array($email_id) ,$html_content,$html_content,$subject,null,null, array(),true);
        return $mid ;
    }

    private function isTrulyMadlyEmail($string)
    {
       $end = "@trulymadly.com";
       return (strpos($string, $end, strlen($string) - strlen($end)) !== false);
      //  return true; // return true for now TODO change it before going live
    }

    private function generateQueryFromConditions($gender,$max_age, $min_age, $cities)
    {
        $sql = "FROM user u join user_data ud on u.user_id = ud.user_id WHERE ";

        if($gender == 'F' || $gender == 'M')
        {
            $sql .= "u.gender = '$gender' AND " ;
        }
        if(is_array($cities) and count($cities) > 0)
        {
            $city_list = implode(",",$cities) ;
            $sql .= " ud.stay_city in ($city_list) AND " ;
        }

        if(is_numeric($min_age) == false || $min_age < 18 || $min_age > 100)
         $min_age = 18 ;
        if(is_numeric($max_age) == false || $max_age < 18 || $max_age > 100)
            $max_age = 100 ;

        $sql .= "EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >= $min_age AND ".
                "EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=$max_age AND ".
                "u.status ='authentic' AND email_id is not null and email_status is null";
        return $sql ;
    }
}


?>