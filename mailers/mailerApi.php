<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/mailerAdminClass.php";

try
{
    $response = array();
    if($_SESSION['super_admin'] == 'yes' || ( isset($_SESSION['privileges']) && in_array('mailer', $_SESSION['privileges']) ))
    {
        // check for requests
        if( isset($_REQUEST['action']) && $_REQUEST['action'] !=null )
        {
            $mailerAdmin = new MailerAdmin();
            if($_REQUEST['action'] == 'save_new_campaign' && isset($_REQUEST['campaign_name']) && $_REQUEST['campaign_name'] !=null )
            {
                // Now save this  campaign";
                $campaign_id = $mailerAdmin->saveNewCampaign($_REQUEST['campaign_name']);

                $response['responseCode'] = 200 ;
                $response['status'] = "success" ;
                $response['campaign_id'] = $campaign_id ;
            }
            else if($_REQUEST['action'] == 'send_test_mail' && isset($_REQUEST['email_id']) && $_REQUEST['email_id'] !=null )
            {
                // Now send the test mail";
                $result = $mailerAdmin->sendTestMail($_REQUEST['email_id'], $_REQUEST['mail_content'], $_REQUEST['mail_subject']);
                if($result['status'] == "success")
                {
                    $response['responseCode'] = 200 ;
                    $response['status'] = "success" ;
                }
                else
                {
                    $response['responseCode'] = 200 ;
                    $response['status'] = "failure" ;
                    $response['error'] = $result['error'];
                }

            }
            else if($_REQUEST['action'] == 'save_mail_content')
            {
                $conditions = null;
                $status ='test';
                $mailerAdmin->updateCampaign($_REQUEST['campaign_id'], $status, $_REQUEST['mail_content'],$conditions,$_REQUEST['mail_subject']);
                $response['responseCode'] = 200 ;
                $response['status'] = 'success';
                $response['campaign_id'] = $_REQUEST['campaign_id'] ;
            }
            else if($_REQUEST['action'] == 'save_segment')
            {
                // TODO :: Update the campaign , save the html , generate the query based on condition/ save the query , save the count
                $conditions = array('gender' => $_REQUEST['gender'],
                                    'max_age'=> $_REQUEST['max_age'],
                                    'min_age'=> $_REQUEST['min_age'],
                                    'cities'=>$_REQUEST['cities']);
                $status ='queued';
                $mailerAdmin->updateCampaign($_REQUEST['campaign_id'], $status, null,$conditions,null);
                $response['responseCode'] = 200 ;
                $response['status'] = 'success';
                $response['campaign_id'] = $_REQUEST['campaign_id'] ;
            }
            else if($_REQUEST['action'] == 'count_users')
            {
            	// TODO :: Update the campaign , save the html , generate the query based on condition/ save the query , save the count
            	$conditions = array('gender' => $_REQUEST['gender'],
            			'max_age'=> $_REQUEST['max_age'],
            			'min_age'=> $_REQUEST['min_age'],
            			'cities'=>$_REQUEST['cities']);
            
            	$count = $mailerAdmin->getCountFordisplay($conditions);
            	$response['responseCode'] = 200 ;
            	$response['status'] = 'success';
            	$response['count'] = $count ;
            }
            else if($_REQUEST['action'] == 'send_campaign')
            {
                // TODO :: Queue the campaign so that It can be sent at the earliest
                $status ='queued';
                $mailerAdmin->updateCampaignStatus($_REQUEST['campaign_id'], $status);
                $response['responseCode'] = 200 ;
                $response['status'] = 'success';
                $response['campaign_id'] = $_REQUEST['campaign_id'] ;
            }
            else if($_REQUEST['action'] == 'send_to_emails')
            {
                // TODO :: Queue the campaign so that It can be sent at the earliest
                $mailerAdmin->sendCampaignToEmails($_REQUEST['campaign_id'], $_REQUEST['email_ids']);
                $response['responseCode'] = 200 ;
                $response['status'] = 'success';
                $response['campaign_id'] = $_REQUEST['campaign_id'] ;
            }
            else if($_REQUEST['action'] == 'generate_html' && isset($_REQUEST['image_url']) && $_REQUEST['image_url'] != null)
            {
            	// TODO :: Queue the campaign so that It can be sent at the earliest
            	$html = $mailerAdmin->generateImageOnlyHTML($_REQUEST['image_url']);
            	$response['responseCode'] = 200 ;
            	$response['status'] = 'success';
            	$response['html'] = $html ; 
            }
        }
        else
        {
            $response['responseCode'] = 200 ;
            $response['status'] = "Go Home" ;

        }
    }
    else
    {
        //TODO:: Return error response for un-authorised  user
        $response['responseCode'] = 200 ;
        $response['status'] = "Sorry ! , You don't have enough permissions for this action" ;
    }
    print_r(json_encode($response));
}
catch (Exception $e)
{
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>