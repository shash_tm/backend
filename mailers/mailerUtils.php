<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../DBO/mailerDBO.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../email/MailFunctions.php";

/*
 * Utitilies for the mailers
 * Author : Sumit
 *🖖 Live Long and Prosper.
 */


class MailerUtils
{
    public static $name_string = "|NAME|";
    public function addNameToSubject($mail_subject, $name)
    {
        return str_replace(self::$name_string,$name,$mail_subject) ;
    }

    public function addNameToContent($mail_content, $name)
    {
        return  str_replace(self::$name_string, $name, $mail_content) ;
    }

    public function addTrackingLinksToTemplate($template, $campaign_id, $user_id,$email_id)
    {
        $analytics_links = self::generateCampaignMailerLinks($campaign_id,$email_id,$user_id);
        $trackingElem = '<img src="'.$analytics_links['analytics_open_rate_system_link'] . '" width="1" height="1" style="float:left;" />';
        return  str_replace('</body>',$trackingElem . '  </body>' ,$template);
    }
    public static function generateCampaignMailerLinks($campaign_id, $emailId, $userId){

        global $config, $baseurl;
        $analyticsLinks = array();
        $currentTime = time();
       // $tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'
        $campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$campaign_id";
        //	$analyticsLinks["analytics_open_rate_link"] = "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$userId&t=event&ec=email&ea=open&el=$userId&cs=system_emailer_open&cm=email&cn=$campaignName";
        //$analyticsLinks["analytics_open_rate_link"] = "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$userId&t=event&ec=email&ea=open&cs=system_emailer_open&cm=email&cn=$campaignName";
        $analyticsLinks["analytics_open_rate_system_link"] = $baseurl."/trk.php" .$campaign."&utm_content=open_rate&uid=$userId&step=openRate&t=$currentTime";
        $unsubscribe = new Unsubscription($emailId);
        $unsubLink = $unsubscribe->generateLink();
        $analyticsLinks["analytics_unsubscribe_link"]= $unsubLink."&".substr($campaign, 1,strlen($campaign))."&utm_content=unsubscribe&uid=$userId";

        return $analyticsLinks;
    }


}


