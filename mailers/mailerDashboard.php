<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/mailerAdminClass.php";



try
{
    if($_SESSION['super_admin'] == 'yes' || ( isset($_SESSION['privileges']) && in_array('mailer', $_SESSION['privileges']) ))
    {
        // return a template for main dashboard
        $mailerAdmin = new MailerAdmin();
        $campaigns = $mailerAdmin->getCampaignList();
        $smarty->assign ("baseurl" , $admin_baseurl) ;
        $smarty->assign ("imageurl" , $imageurl) ;
        $smarty->assign ("campaigns" , $campaigns) ;
        $smarty->display ( "../templates/mailers/mailerDashboard.tpl" );
    }
    else
    {
        //TODO:: Return an empty template
        $smarty->display ( "../templates/mailers/mailerDashboard.tpl" );
    }
}
catch (Exception $e)
{
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>