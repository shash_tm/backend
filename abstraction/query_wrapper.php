<?php 
require_once dirname ( __FILE__ ) . "/query.php";
//require_once dirname(__FILE__).'/../include/config.php';
//require_once dirname(__FILE__).'/../include/config_admin.php';
require_once dirname(__FILE__).'/../include/Utils.php';
require_once dirname(__FILE__).'/../include/function.php';
require_once dirname(__FILE__).'/../Redis_data_cache.php';
//require_once dirname(__FILE__).'/../scripts/redis/read_data.php';

/*
 * @uthor:arpan
 * Valar Morghulis
 * Query abstraction class with tablename for redis cache invalidation
 */

class Query_Wrapper extends Query{

	public static function INSERT($query, $param_array, $tablename= null, $db_type = "master", $ifAffectedRows = false,$user_id=NULL){
		$return_value= parent::INSERT($query, $param_array,$tablename,  $db_type, $ifAffectedRows);
		self::invalidate_redis($tablename,$user_id);
		return $return_value;
	}

	public static function UPDATE($query, $param_array, $tablename = null, $db_type = "master", $user_id=NULL){
		$return_value=parent::UPDATE($query, $param_array,$tablename, $db_type);
		self::invalidate_redis($tablename,$user_id);
		return $return_value;
	}

	public static function DELETE($query, $param_array,$tablename= null, $db_type = "master"){
		$return_value= parent::DELETE($query, $param_array,$tablename, $db_type);
		return $return_value;
	}


	public static function BulkINSERT($query, $param_array,$tablename = null, $db_type = "master"){
	   $return_value= parent::BulkINSERT($query, $param_array,$tablename, $db_type);
	   return $return_value;
	}

	public static function SELECT($query, $param_array,$db_type = "slave", $fetchRowEnabled = false){
		$return_value= parent::SELECT($query, $param_array, $db_type, $fetchRowEnabled );
		return $return_value;
	}

	public static function invalidate_redis($tablename,$userId){
		if($userId==NULL)
		{
		 $session=functionClass::getUserDetailsFromSession();
		 if(!$session['user_id'])
		 return;
	     else
		 $user_id=$session['user_id'];
		}
		else 
			$user_id=$userId;
		
		//ini_set("error_log", "/var/log/httpd/error_log");
		if(!isset($user_id))
		{
			$msg="No user_id present for the table : $tablename in query_wrapper"; 
			trigger_error ( "PHP Fatal error : ($msg) ", E_USER_WARNING );
		}
		
		$key=Utils::$redis_keys['user_data'].$user_id;
		$index=null;
		switch($tablename)
		{
			case 'user_lastlogin':
				{
					$index='lastlogin';
					break;
				}
			case 'user':
					{
						$index='basic';
						break;
					}
			case 'user_data':
					{
						$index='data';
						break;
					}
			case 'geo_state':
					{
						$index='data';
						break;
					}			
		    case 'geo_city':
					{
						$index='data';
						break;
					}		

		    case 'industries_new':
						{
							$index='data';
							break;
						}
		    case 'highest_degree':
						{
							$index='data';
							break;
						}
		    case 'tm_celeb':
						{
							$index='data';
							break;
						}
		    case 'user_trust_score':
						{
							$index='trust_score';
							break;
						}
		    case 'interest_hobbies':
						{
							$index='interest_hobbies';
							break;
						}
			case 'user_photo':
						{
							$index='photos';
							break;
						}					
			default:
				$index=null;
		}
		
		if($index==null)
			return;
		
		$redis_read=new RedisCache($user_id);
		$data=$redis_read->getval($index);
		if($data)
		{
			$data['validation_flag']=0;
		    $redis_read->setval($index, $data);
		    return;
		}
		else return;
			
	}

	public static function getParamCount($param_array)
	{
		$param_count = count($param_array) ;
		$in_param =  trim(str_repeat('?, ', $param_count), ', ');
		return $in_param ;

	} 
}

?>