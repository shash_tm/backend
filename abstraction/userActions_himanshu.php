<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/redis.php";
require_once dirname ( __FILE__ ) . "/query.php";
require_once dirname ( __FILE__ ) . "/../DBO/userActionsDBO.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";




class UserActionConstant {
	const like = 0;
	const hide = 2;
	const reject = 4;
	const credit_consume = 6;
	/*const like_user1 = 0;
	 const like_user2 = 1;
	 const hide_user1 = 3;
	 const hide_user2 = 4;
	 const reject_user1 = 5;
	 const reject_user2 = 6;
	 const credit_consume_user1 = 6;
	 const credit_consume_user2 = 7;*/
}

class UserActionConstantValues {
	/*const like ="like";
	 const hide = "hide";
	 const reject = "reject";
	 const credit_consume = "credit_consume";*/
	const like_user1 = "like_user1";
	const like_user2 = "like_user2";
	const hide_user1 = "hide_user1";
	const hide_user2 = "hide_user2";
	const reject_user1 = "reject_user1";
	const reject_user2 = "reject_user2";
	const credit_consume_user1 = "credit_consume_user1";
	const credit_consume_user2 = "credit_consume_user2";
}

/*
 * Describes the possible user actions
 * like/hide/reject/consumes_credit/can-cummunicate
 * @Himanshu
 */

class UserActions{

	private $redis;
	private $uaKey;
	private $uaDBO;
	private $baseurl;

	function __construct() {
		global $baseurl;
		$this->uaKey = Utils::$redis_keys['userAction'];
		$this->matchKey = Utils::$redis_keys['matches'];
		$this->redis = new redisClass();
		$this->uaDBO = new userActionsDBO();
		$this->baseurl = $baseurl;
	}

	private function getuaKey($user1, $user2){
		$uaKey = $user1>$user2?($this->uaKey ."$user2:$user1"):($this->uaKey ."$user1:$user2");
		return $uaKey;
	}

	private function setAction($user1, $user2, $bit) {

		$useruaKey = $this->getuaKey($user1, $user2);
		$setBits = $this->redis->getValue($useruaKey);
		$val = ($user1<$user2)?(1 << $bit):(1<< ($bit+1));
		$updatedBits = ($setBits)?($setBits | $val):$val;
		$this->redis->setKey($useruaKey, $updatedBits);

	}

	public  function generateQuery($user_action,$uid,$mid){
		$timestamp= time();
		$hash=UserActions::getHash($user_action,$timestamp,$uid,$mid);
		$query="uid=$uid&mid=$mid&sh=$hash&ts=$timestamp&ua=$user_action";
		return $query;
	}

	private  function getHash($user_action, $timestamp, $user1, $user2) {
		return md5 ( "$user_action|$timestamp|$user1|$user2 |$salt" );
	}

	public  function checkHash(){
		$uid=$_REQUEST['uid'];
		$mid=$_REQUEST['mid'];
		$hash_old=$_REQUEST['sh'];
		$timestamp=$_REQUEST['ts'];
		$user_action=$_REQUEST['ua'];
		$hash_new=UserActions::getHash($user_action,$timestamp,$uid,$mid);
		return $hash_new==$hash_old;
	}


	private function getBitToActionMapping($action) {
		$actions = array ();

		if ($action & (1 << UserActionConstant::like))
		$actions [] = UserActionConstantValues::like_user1;
		if ($action & (1 << (UserActionConstant::like +1)))
		$actions [] = UserActionConstantValues::like_user2;
		if ($action & (1 << UserActionConstant::hide))
		$actions [] = UserActionConstantValues::hide_user1;
		if ($action & (1 << (UserActionConstant::hide + 1)))
		$actions [] = UserActionConstantValues::hide_user2;
		if ($action & (1 << UserActionConstant::reject))
		$actions [] = UserActionConstantValues::reject_user1;
		if ($action & (1 << (UserActionConstant::reject + 1)))
		$actions [] = UserActionConstantValues::reject_user2;
		if ($action & (1 << UserActionConstant::credit_consume))
		$actions [] = UserActionConstantValues::credit_consume_user1;
		if ($action & (1 << (UserActionConstant::credit_consume +1 )))
		$actions [] = UserActionConstantValues::credit_consume_user2;

		return $actions;
	}

	public function getActionsDone($user1, $user2) {
		$useruaKey = $this->getuaKey($user1, $user2);
		$bits = $this->redis->getValue($useruaKey);
		$actions = $this->getBitToActionMapping($bits);
		return $actions;
	}

	/**
	 * performs the action possible
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 * @param unknown_type $action
	 */

	public function performAction($user1, $user2, $action){

		echo $action;
		echo UserActionConstant::like;
		if($action == UserActionConstant::like)
		$this->setAction($user1, $user2, UserActionConstant::like);

		if($action == UserActionConstant::hide)
		$this->setAction($user1, $user2, UserActionConstant::hide);

		if($action == UserActionConstant::reject)
		$this->setAction($user1, $user2, UserActionConstant::reject);

		if($action == UserActionConstant::credit_consume)
		$this->setAction($user1, $user2, UserActionConstant::credit_consume);

		if($action == UserActionConstant::like || $action == UserActionConstant::hide || $action == UserActionConstant::reject){
			$this->remove_from_recommendation($user1, $user2);
			$table = null;
			switch ($action){
					
				case UserActionConstant::like:
					$table = "user_like";
					break;
						
				case UserActionConstant::hide:
					$table = "user_hide";
					break;

				case UserActionConstant::reject:
					$table = "user_reject";
					break;
			}
			$this->uaDBO->addToQueue($user1, $user2, $table);
		}
	}

		/**
		 * sets mutual like if not set
		 * and returns the current status of mutual like
		 * @param unknown_type $user1
		 * @param unknown_type $user2
		 */
		public function getSetMutualLike($user1, $user2){
			$actions = $this->getActionsDone($user1, $user2);
			if(isset($actions) && in_array(UserActionConstantValues::like_user1, $actions) && in_array(UserActionConstantValues::like_user2, $actions)){
				$this->uaDBO->setMutualLike($user1, $user2);
				//$this->setMutualLike($user1, $user2);
				return true;
			}
			else return false;
		}

		/**
		 * remove user2 from user1 recommendation queue
		 * @param unknown $user1
		 * @param unknown $user2
		 */
		private function remove_from_recommendation($user1,$user2){
			$mkey = Utils::getMatchKey($user1);
			$this->redis->removeFromList($mkey, $user2);
			//$uaKey	= "u:m:$user1";
			//TODO: check the redis set/list to user
			//$this->redis->LREM($uaKey, $user2 );
		}

		/**
		 *
		 * returns the description if two users can communicate
		 * along with info if one can see name etc
		 * ALL the AB Testing plan needs to be directed through this function only
		 * No need to edit any of the basic calls
		 * TODO: AB testing
		 */
		public function canCommunicate($user1, $user2){

			$actions = $this->getActionsDone($user1, $user2);

			if(in_array(UserActionConstantValues::like_user1, $actions)
			&& in_array(UserActionConstantValues::like_user2, $actions)
			&& !in_array(UserActionConstantValues::reject_user1, $actions)
			&& !in_array(UserActionConstantValues::reject_user2, $actions)
			&& ((in_array(UserActionConstantValues::credit_consume_user1, $actions))
			|| (in_array(UserActionConstantValues::credit_consume_user2, $actions)))){
				return true;
			}
			return false;
		}

		/**
		 * will return the permitted actions for a user on other user (like/hide/msg)
		 * @param $user1
		 * @param $user2
		 */
		public function getPermittedActions($user1, $user2, $user1_status){
			$actions = $this->getActionsDone($user1, $user2);
			$permittedActions = array("canMsg" => false, "canLike" => false, "canHide" => false);

			//if user is not authentic then no need to give any link
			if($user1_status != 'authentic') return $permittedActions;

			$actionFlag = ($user1< $user2)?1:2;
			if($actionFlag == 1){
				$likeAction = UserActionConstantValues::like_user1;
				$hideAction = UserActionConstantValues::hide_user1;
			}
			else{
				$likeAction = UserActionConstantValues::like_user2;
				$hideAction = UserActionConstantValues::hide_user2;
			}

			//actions are permitted only when none of them had rejected each other earlier
			if(!in_array(UserActionConstantValues::reject_user1, $actions)&& !in_array(UserActionConstantValues::reject_user2, $actions)){

				//check if already liked or hide
				if(!in_array($likeAction, $actions) && !in_array($hideAction, $actions) ){
					$permittedActions['canLike'] = true;
					$permittedActions['canHide'] = true;
				}
				//a separate function call since it needs to be directed by AB testing engine
				$permittedActions['canMsg'] = $this->canCommunicate($user1, $user2);

			}
			return $permittedActions;

		}

		/**
		 * generate links by checking if the action is even permitted or not
		 * @param unknown_type $user1
		 * @param unknown_type $user2
		 */
		public function generateLinksForPermittedActions($user1, $user2, $user1_status){

			$uu = new UserUtils();
			$permittedActions = $this->getPermittedActions($user1, $user2, $user1_status);
			$links = array("msg_url" => null, "like_url" => null , "hide_url" => null);
				
			if($permittedActions['canMsg'])
			$links['msg_url'] = $uu->generateMessageLink($user1, $user2);

			if($permittedActions['canLike'])
			$links['like_url'] = $uu->generateLikeLink($user1, $user2);
				
			if($permittedActions['canHide'])
			$links['hide_url'] = $uu->generateHideLink($user1, $user2);
				
			$links['profile_url'] = $uu->generateProfileLink($user1, $user2);

			return $links;
		}
		/**
		 * remove user2 from user1 recommendation queue
		 * @param unknown $user1
		 * @param unknown $user2
		 */
		/*private function incrementCounter($action,$user_id){
		 $uaKey= "u:q:$user_id:$action";
		 $this->redis->INCR($uaKey);
		 }*/

		/*	private function incrementNotification($user_id){
		 $uaKey= "u:n:$user_id";
		 $this->redis->INCR($uaKey);
		 }*/


	}

