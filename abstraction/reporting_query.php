<?php 
require_once dirname ( __FILE__ ) . "/../include/config_reporting_dashboard.php";
require_once dirname ( __FILE__ ) . "/../DBO/dashboardDBO.php";

/*
 * Base for all reporting queries, generated from dashBoard
 * Author :: Sumit
 * 🖖 Live Long and Prosper.
 */
class ReportingQuery {
	private $conn_reporting_dashboard ;
	private $fields ;
	private $error_msg ;
	private $number_of_rows ;
	private $sql_execute ;
	private $sql ;
	private $param_array ;
	private $time_taken ;
	private $query_start_time ;
	private $query_end_time ;
	private $logging_enabled ;
	
	function __construct($sql,$param_array= NULL, $logging = TRUE)
	{
		global $conn_reporting_dashboard;
		$this->logging_enabled = $logging ;
		$this->conn_reporting_dashboard = $conn_reporting_dashboard ;
		$this->conn_reporting_dashboard->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->fields ;
		$this->error_msg ;
		$this->number_of_rows ;
		$this->sql_execute ;
		$this->time_taken ;
		$this->sql = $sql ;
		$this->param_array = $param_array ;
		$this->query_start_time = microtime(true) ;
		$this->query_end_time ;
		$this->ExecuteSelect() ;
		
	}
	
	private function ExecuteSelect() 
	{	
		$sql_prepare = $this->conn_reporting_dashboard->Prepare($this->sql); 
		try {
			$this->sql_execute = $this->conn_reporting_dashboard->Execute($sql_prepare,$this->param_array); 
		} catch (Exception $e) {
			if($this->sql_execute == FALSE)
			{	
			$this->error_msg = $this->conn_reporting_dashboard->ErrorMsg();
			$this->logQuery();
			return ;
			}
		}
		
			$temp_fields = $this->sql_execute->fields;  
			$i = 0 ;
			foreach ($temp_fields as $key => $val)
			{
				if( is_integer($key) == FALSE)
				{
					$this->fields[$i] = $key;
					$i++ ;
				}
				
			}
			// $this->fields = $this->sql_execute->fields; 
			
			 $this->number_of_rows = $this->sql_execute->RowCount();
		 	 $this->logQuery();

	}
	
	private function logQuery()
	{
		$this->query_end_time = microtime(true) ;
		$data['query'] = $this->sql ;
		$data['admin_id'] = $_SESSION['admin_id'] ;
		$data['ran_from'] = "panel" ;
		$data['time_taken'] = $this->query_end_time - $this->query_start_time ;
		$data['rows_count'] = $this->number_of_rows ;
		$data['error'] = $this->error_msg ;
		if($this->logging_enabled == TRUE)
			dashboardDBO::logQueryDBO ($data) ;
	}
	
	
	public function getRowsWithLimit($limit=1000)
	{
		if($this->sql_execute == FALSE)
		{
			return ;
		} 
		else
		{
			$result = $this->sql_execute->GetRows();
		}
		return $result ;
	}
	
	public function getFields()
	{
		return $this->fields;
	}
	
	public function getErrorMsg() 
	{
		return $this->error_msg;
	}
	public function getRowCount()
	{
		return $this->number_of_rows;
	}
}


?>