<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../google_analytics/google-api-php-client/src/Google/autoload.php";

/**
 * Class BigQuery :: This class will help us in solving most of the current problems in the world like Terrorism, hunger
 * and we can finally establish world peace.
 * LOL JK , This will act as wrapper to all requests to BIG Query.
 * Author :: Sumit
 * 🖖 Live Long and Prosper. */

class BigQuery
{

    /*
     * Exposed method for selecting the data from Bigquery
     * returns array of arrays
     * $query is the Bigquery select query
     */
    public static function BQSELECT($query)
    {
        global $googleProjectID ;
        $bigquery = self::getBQConnection();
        $request = new Google_Service_Bigquery_QueryRequest();

        $request->setQuery($query);
        $response = $bigquery->jobs->query($googleProjectID, $request);
        $rows = $response->getRows();
        $data = array();
        // var_dump($rows);
        foreach ($rows as $row)
        {
            $tuple = array();
            foreach ($row['f'] as $field)
            {
                $tuple[] = $field['v'] ;
            }
            $data[]= $tuple ;
        }
        return $data ;
    }

    /*
     * returns the bigquery object for selecting the data from BIg Query
     */
    private static function getBQConnection()
    {
        global  $BQServiceAccountEmail, $BQKeyFileLocation;
        $client = new Google_Client();
        $client->setApplicationName("BigQuery");
        $bigquery = new Google_Service_Bigquery($client);

        // Read the generated client_secrets.p12 key.
        $key = file_get_contents($BQKeyFileLocation);
        $cred = new Google_Auth_AssertionCredentials(
            $BQServiceAccountEmail,
            array(Google_Service_Bigquery::BIGQUERY),
            $key
        );
        $client->setAssertionCredentials($cred);
        if($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($cred);
        }
        return $bigquery ;
    }
}

?>