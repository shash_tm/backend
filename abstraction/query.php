<?php

require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";


/**
 * Base of SQL Queries
 * @Himanshu
 * NOTE: DO NOT EDIT till the time YOU, yes "YOU" are sure enough
 */


class Query{

	public static $__slave = "slave" ;
	public static $__master = "master";


	/**
	 * initiates the connection (master/slave/default)
	 * @param unknown_type $db_type
	 */
	private static function getConnection($db_type){
		global $conn_master, $conn_slave, $conn;
		if($db_type == Query::$__slave) $connection = $conn_slave;
		else if($db_type == Query::$__master) $connection = $conn_master;
		else $connection = $conn;
		$connection->SetFetchMode ( ADODB_FETCH_ASSOC );
		return $connection;
	}

	/**
	 * Insert a row using prepared statement
	 * @param $query
	 * @param $param_array
	 * @param $db
	 * @param ifAffectedRows to be true in case insert query contains on duplicate update
	 */
	public static function INSERT($query, $param_array, $tablename = null, $db_type = "master", $ifAffectedRows = false,$user_id=NULL){
		$connection = Query::getConnection($db_type);
		$sql = $connection->Prepare($query);
		$result = $connection->Execute($sql, $param_array);
		if($ifAffectedRows == true) return $connection->Affected_Rows();
		else return $connection->Insert_ID();;
	}

	/**
	 * Update Query using prepared statement
	 * @param unknown_type $query
	 * @param unknown_type $param_array
	 * @param unknown_type $db_type
	 */
	public static function UPDATE($query, $param_array,$tablename= null, $db_type = "master",$user_id=NULL){
		$connection = Query::getConnection($db_type);
		$sql = $connection->Prepare($query);
		$result = $connection->Execute($sql, $param_array);
		return $result;
	}


	/**
	 * Delete Query using prepared statement
	 * @param unknown_type $query
	 * @param unknown_type $param_array
	 * @param unknown_type $db_type
	 * NOTE: Do not use this query, double check before deleting a row in the system
	 */
	public static function DELETE($query, $param_array,$tablename= null, $db_type = "master"){
		$connection = Query::getConnection($db_type);
		$sql = $connection->Prepare($query);
		$result = $connection->Execute($sql, $param_array);
		return $result;
	}


	/**
	 * Mass Insert using prepared statement
	 * @param $query
	 * @param $param_array
	 * @param $db
	 */
	public static function BulkINSERT($query, $param_array,$tablename = null, $db_type = "master"){
		$connection = Query::getConnection($db_type);
		$connection->bulkBind = true;
		$sql = $connection->Prepare($query);
		$result = $connection->Execute($sql, $param_array);
		return $result;
	}
	
	/**
	 * returns the result set of Select query using prepared statement
	 * @param $query
	 * @param $param_array
	 * @param $db_type
	 */
	public static function SELECT($query, $param_array, $db_type = "slave", $fetchRowEnabled = false){
		$connection = Query::getConnection($db_type);
	//	echo $query;
		$sql = $connection->Prepare($query);
		$result = $connection->Execute($sql, $param_array);
		$num_of_rows = $result->RowCount();
		if($num_of_rows == 1 && $fetchRowEnabled) $rows = $result->FetchRow();
		else
		$rows = $result->GetRows();

		return $rows;
	}

}

?>
