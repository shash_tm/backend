<?php

require_once dirname ( __FILE__ ) . "/../include/config.php";

/**
 * Base Class contains all the actions that can be operated on Redis
 * @author himanshu
 */

class redisClass{

	private $redis_conn;

	function __construct(){
		global $redis;
		$this->redis_conn = $redis;
	}

	/**
	 * string functions
	 */
	public function setKey($key, $value){
		$this->redis_conn->SET($key, $value);
	}

	public function getValue($key){
		return $this->redis_conn->GET($key);
	}


	/**
	 * List Functions
	 */	
	public function leftPushList($key, $value){
		$this->redis_conn->LPUSH($key, $value);
	}

	public function rightPushList($key, $value){
		$this->redis_conn->RPUSH($key, $value);
	}

	public function removeFromList($key, $value){
		$this->redis_conn->LREM($key, $value);
	}
	
	public function getList($key, $start = 0 , $end = -1){
		return $this->redis_conn->LRANGE($key, $start, $end); 
	}
	
	
	
}

?>