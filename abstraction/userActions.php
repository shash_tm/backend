<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
//require_once dirname ( __FILE__ ) . "/redis.php";
require_once dirname ( __FILE__ ) . "/query.php";
require_once dirname ( __FILE__ ) . "/../DBO/userActionsDBO.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../mobile_utilities/pushNotification.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../DBO/messagesDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/eventsDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../utilities/counter.php";
require_once dirname ( __FILE__ ) . "/../Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . "/../msg/MessageConstants.php";
require_once dirname ( __FILE__ ) . "/../msg/MessageFullConversation.class.php";
require_once dirname ( __FILE__ ) . "/../logging/EventTrackingClass.php";






/*
 * Describes the possible user actions
 * like/hide/reject/consumes_credit/can-cummunicate
 * @Himanshu
 */

class UserActionConstant {
	const like = 0;
	const hide = 1;
	const reject = 2;
	const maybe = 3;
}

class UserActionConstantValues {
	const like = "like";
	const hide = "hide";
	const reject = "reject";
	const maybe = "maybe";
}

class UserActions{

	private $redis;
	private $uaDBO;
	private $baseurl;
	private $matchKey;
	private $redisEvenDB;
	private $redisOddDB;
	private $redisActivityDB;
	
	function __construct() {
		global $baseurl, $redis, $redis_db_odd, $redis_db_even, $redis_db_userActivity;
		$this->matchKey = Utils::$redis_keys['matches'];
		$this->matchKeyWithScores = Utils::$redis_keys['matches_with_scores'];
		$this->redis = $redis;
		$this->uaDBO = new userActionsDBO();
		$this->baseurl = $baseurl;
		$this->redisEvenDB = $redis_db_even;
		$this->redisOddDB = $redis_db_odd; 
		$this->redisActivityDB = $redis_db_userActivity;
	}


	public  function generateQuery($user_action,$uid,$mid, $flag=0, $gender,$availableFlag,$repeatProfile){
		$timestamp= time();
		$hash=UserActions::getHash($user_action,$timestamp,$uid,$mid, $flag, $gender, $availableFlag);
		$repeat_string=isset($repeatProfile)?"&rl=1":null;
		$query= isset($availableFlag)?"uid=$uid&mid=$mid&sh=$hash&ts=$timestamp&ua=$user_action&fl=$flag&g=$gender&a=$availableFlag":"uid=$uid&mid=$mid&sh=$hash&ts=$timestamp&ua=$user_action&fl=$flag&g=$gender$repeat_string";
		return $query;
	}

	private  function getHash($user_action, $timestamp, $user1, $user2, $flag =0,$gender, $availableFlag) {
		return isset($availableFlag)?md5 ( "$user_action|$timestamp|$user1|$user2 |$salt|$flag|$gender|$availableFlag" ):md5 ( "$user_action|$timestamp|$user1|$user2 |$salt|$flag|$gender" );
	}

	public  function checkHash(){
		$uid=$_REQUEST['uid'];
		$mid=$_REQUEST['mid'];
		$hash_old=$_REQUEST['sh'];
		$timestamp=$_REQUEST['ts'];
		$user_action=$_REQUEST['ua'];
		$gender=$_REQUEST['g'];
		$availability = $_REQUEST['a'];
		
		$flag = 0;
		if(isset($_REQUEST['fl']) && $_REQUEST['fl'] == 1) $flag = 1;
		$hash_new=UserActions::getHash($user_action,$timestamp,$uid,$mid, $flag, $gender, $availability);
		return $hash_new==$hash_old;
	}



	/**
	 * performs the action possible
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 * @param unknown_type $action
	 */



	private function setMatchCountKey($user_id){
		$countKey = Utils::$redis_keys["match_action_count"]. $user_id;
		$this->redis->INCR($countKey);
		$midnight = strtotime ( '+330 minutes',strtotime("tomorrow 00:00:00") ) ;
		$time = ($midnight - time())/60;
		UserUtils::setExpiry($countKey, $time);
	}

	
	public function performAction($user1, $user2, $action, $likeFlag = 0, $user1_gender, $availabilityFlag = 0, $sparkFlag = false, $extra_params = array() ){
		global $imageurl, $dummy_male_image, $dummy_female_image;
		$actionSet = null;
		$table = null;
		$uu = new UserUtils();
                $selectObject = new Select($user1);
                $selectData = $selectObject->getSelectMembers(array($user1, $user2));
                $obj=new UserFlagsDBO();
                $flags = $obj->read("select_config");

		$actionCountKey = Utils::$redis_keys['match_action_count']. $user1;
                $user_data = userUtilsDBO::getUserBasicInfo(array($user1));
                $myCountry= $user_data['stay_country'] ;
                
		if($action == UserActionConstantValues::like ){
			$table = "user_like";
			$mutualLikeFlag = $this->getSetMutualLike($user1, $user2);
			
			if(!$mutualLikeFlag && $selectData[$user2] === true && !isset($selectData[$user1]) && $flags['allow_like'] !== "true" && $myCountry == 113){
				return array("error"=> "Cannot like a Select Member","responseCode"=>"403");
			}

			if($mutualLikeFlag && !$sparkFlag){
				//set the entry in messages queue as well if it's a mutual like
				$msgDBO = new MessagesDBO();
				$msgDBO->setEntryInMessageQueueOnMutualLike($user1, $user2);
				$pushnotify = new pushNotification();
				$uu = new UserUtils();
				
				$counter=new Counters();
				$counters=$counter->addMutualLikeCounter($user1,$user2);
				//$push_data1 = array("mutual_like_count" => $count_user1, "current_like" => $user2);
				$push_data2 = array("mutual_like_count" => $counters['user2_mutual_like_count'], "current_like" => $user1);
				
				$user_data = $uu->getNamenPic($user1);

				$ticker_text = "Mutual like from ".$user_data['name'];

				$pic_url = $imageurl.$user_data['thumbnail'];

				$match_profile_link = $uu->generateProfileLink($user2,$user1);
				$data = "Hey! ".$user_data['name']." also liked you.";
				$pushnotify->notify($user2, array("content_text"=>$data,"pic_url"=>$pic_url,"ticker_text"=>$ticker_text,"title_text"=>$user_data['name'],"match_profile_url"=>$match_profile_link,"push_type"=>"MATCH"),$user1);

				$actionSet= array("mutualLike" => $counters['mutualLike'], "conversation_count"=>$counters['conversation_count']);
			} 
			else if ( ProfileCampaign::isUserMutualMatchableAdCampaign($user2) == true  && !$sparkFlag)
			{
				//echo "profile campaing option";
				// add a reverse like from user2 to user1 
				$this->uaDBO->addToQueue($user2, $user1, $table);
				$this->uaDBO->setMutualLike($user1, $user2);
				
				
				// add to mutual_likes  
				$msgDBO = new MessagesDBO();
				$msgDBO->setEntryInMessageQueueOnMutualLike($user1, $user2);
				
				$counter=new Counters();
				$counters=$counter->addMutualLikeCounter($user1,$user2);
				$actionSet= array("mutualLike" => $counters['mutualLike'], "conversation_count"=>$counters['conversation_count']);
				
				$MessageFullConversation = new MessageFullConversation ( $user2, $user1 );
				
				$first_message_arr = ProfileCampaign::getAdCampaignMessage($user2);
				if($first_message_arr['coupon_code_message'] != null)
				{
					//echo "custom Message true";
					// check if coupon codes are available if yes then use then assign a coupon code here .or send the alternate message
					//$city= ProfileCampaign::getUserCity($user1);
					//var_dump($city);
					$coupon_code_arr = ProfileCampaign::getCouponCode($user2, null) ;
					if( count($coupon_code_arr)>0)
					{
					//	echo "coupons available";
						$coupon_code = $coupon_code_arr['coupon_code'] ;
						$coupon_code_message = $first_message_arr['coupon_code_message'] ;
						$first_message = str_replace('COUPON_CODE', $coupon_code, $coupon_code_message) ;
						// set this coupon code as used 
						ProfileCampaign::setCouponStatus($coupon_code,'used',$user1) ;
					} 
					else 
					{
						$first_message = $first_message_arr['custom_message'] ;
					//	echo "coupon not available";
					} 
				} else
				{
					$first_message = $first_message_arr['custom_message'] ;
				}

				if($first_message_arr['is_event'] == 'yes')
				{
					$event_details= json_decode($first_message_arr['event_details'], true) ;
					try
					{
						EventsDBO::updateUserEvent($user1, $event_details['event_id'],'follow' );
					}
					catch(exception $e)
					{
						trigger_error("PHP user follow event error ".$e->getMessage(), E_USER_WARNING);
					}

				}

			    $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
				$msg_id =  $MessageFullConversation->storeMostRecentChat ( $first_message, $first_message_arr['msg_type'], date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
				// send the push notification
				$pushnotify = new pushNotification();
				$uu = new UserUtils();
				$user_data = $uu->getNamenPic($user2);
				$ticker_text = "Message from ".$user_data['name'];
				$tstamp=date('Y-m-d H:i:s');
				if(isset($user_data['thumbnail']))
					$pic_url = $imageurl.$user_data['thumbnail'];
					else{
						if($user_data['gender']==M)
							$pic_url = $dummy_male_image;
							else
							$pic_url = $dummy_female_image;
					    }
					$message_url = $uu->generateMessageLink($user1 ,$user2);
					//var_dump($match_profile_link);exit;
					$adminFlag = 0;
					$push_type = "MESSAGE";
					$push_data = (strlen ( $first_message ) > 30) ? substr ( $first_message, 0, 29 ) . '...' : $first_message;
					$push_arr = array (
							"is_admin_set" => $adminFlag,
							"content_text" => $push_data,
							"pic_url" => $pic_url,
							"ticker_text" => $ticker_text,
							"title_text" => $user_data ['name'],
							"message_url" => $message_url,
							"match_id" => $user2,
							"msg_id"=> $msg_id,
							"tstamp"=>$tstamp,
							"msg_type" => $msg_type,
							"push_type" => $push_type
					);
					$pushnotify->notify ( $user1, $push_arr, $user2 );
			}
		} 
 		
                // For cases where a non-Select member hides a Select member - we want to ignore the hide.
		if($action == UserActionConstantValues::hide ) {
                    $islikedBefore = $this->uaDBO->isActionSet($user2, $user1, "user_like");
                    if(!(!$islikedBefore && $selectData[$user2] === true && !isset($selectData[$user1]) && $myCountry == 113)) {
                        $table = "user_hide";
                    } else {
                        $table = "user_hide_ignored";
                    }
                }
		if($action == UserActionConstantValues::reject )
                    $table = "user_reject";
		if($action == UserActionConstantValues::maybe )
                    $table = "user_maybe";

		if(isset($table)){
                    //dont insert value in repeat_like is set in the hide table
                    if(!(isset($extra_params['repeat_like'])&&$table=="user_hide"))
                        $addedToQueue = $this->uaDBO->addToQueue($user1, $user2, $table);
		}                
                #Updating the Session table since the user has taken an action now
                if($action == UserActionConstantValues::hide || $action == UserActionConstantValues::like) {
                    $this->uaDBO->updateSessionTable($user1);
                }

		$minuteOfHour = Utils::getCurrentRedisIntervalInSpanOfFiveMinutes($this->redis);
		if($minuteOfHour == 0)$db =$this->redisOddDB; else $db =$this->redisEvenDB;

		//for logging female likes per day in redis
		if($user1_gender=="F" && $action == UserActionConstantValues::like)
		{
                    $redisKey=Utils::$redis_keys['female_like_day'];
                    $redisKey=$redisKey.date("Y-m-d");
                    $this->redis->hIncrBy($redisKey, $user1, 1);
		}
		
		if($user1_gender == "M")
		{
			if(!$mutualLikeFlag && $action == UserActionConstantValues::like)
			{
				if($availabilityFlag != '0')
				{
					$this->redis->SELECT($db);
						
					$freshProfileActionCount = Utils::$redis_keys['fresh_like_count']. $user2;
					$femaleFreeKey = Utils::$redis_keys['like_count'] . $user2;
						
					if ($availabilityFlag == "B" || !isset($availabilityFlag))
					$this->redis->INCR($femaleFreeKey);
						
					else if ($availabilityFlag == "F")
					$this->redis->INCR($freshProfileActionCount);
						
					$this->redis->SELECT(0);
				}
				//increment the counter for male only when it's not a mutual like but a like
				$this->redis->INCR($actionCountKey);
			}
		}
		else if($availabilityFlag != '0' || !isset($availabilityFlag))
		{
			$this->redis->SELECT($db);
			$freshProfileActionCount = Utils::$redis_keys['fresh_like_count']. $user1;
			$femaleFreeKey = Utils::$redis_keys['like_count'] . $user1;

			if ($availabilityFlag == "B" || !isset($availabilityFlag))
			$this->redis->DECR($femaleFreeKey);

			else if ($availabilityFlag == "F")
			$this->redis->DECR($freshProfileActionCount);

			$this->redis->SELECT(0);
		}
		
		if($action != UserActionConstantValues::maybe){
			$this->uaDBO->setInactiveForMayBeQueue($user1, $user2);
		}

		if($action == UserActionConstantValues::like || $action==UserActionConstantValues::hide||$action == UserActionConstantValues::maybe){
                    /*Increment select counters when select user has taken action on select profiles*/
                    if(isset($selectData[$user1]) && isset($selectData[$user2]) && $selectData[$user1] == true && $selectData[$user2] == true) {
                        $selectCountKey = Utils::$redis_keys['select_matches_for_the_day'] . "$user1"; //key for select matches
                        $this->redis->INCR($selectCountKey);
                    }
                    
			if($likeFlag == 1){
				$key = (isset($availabilityFlag))?$user2."l:".$availabilityFlag: $user2."l";
				$this->remove_from_recommendation($user1,$key );
			}else{				
				$key = (isset($availabilityFlag))?$user2.":".$availabilityFlag: $user2;
				$this->remove_from_recommendation($user1, $key);
			}
			
			/*
			 * change the counters only when an action is taken
			 * written to avoid multiple calls although blocked on web 
			 */
			if($addedToQueue== true){
				//increment the count for female only here as for male it'll increment only on likes
				//increment activity count for match id only if a female takes action on a male
				if($user1_gender == "F")
				{
					$this->incrementActivityCount($user2);
					$this->redis->INCR($actionCountKey);
				}
			}

		}


		/*if($mutualLikeFlag){
			return $actionSet = "mutualLike";
			}*/

		if (isset($extra_params['mf_shown']) && $extra_params['mf_shown']) {
			$event_info = array("action" => $action, "match_id" => $user2);
			$this->log_mutual_friends_shown_action($user1, $event_info);
		}
		
		
		if(isset($extra_params['repeat_like'])&&$extra_params['repeat_like']=='1'){

			$action_taken=($action==UserActionConstantValues::like)?"like":"hide";
			$this->uaDBO->insertRepeatLikes($user1,$user2, $action_taken);
		}

		return $actionSet;

	}

	private function log_mutual_friends_shown_action($user_id, $event_info) {
		$event_tracker = new EventTrackingClass();
		$data_logging = array();
		$data_logging [] = array("user_id" => $user_id, "actvity" => "user_action" , "event_type" => "user_action_mf",
				"event_status" => "success", "event_info" => $event_info, "time_taken" => 0);
		$event_tracker->logActions($data_logging);
	}


	/**
	 * if any action is taken then increase the activity count for the matchid
	 * @param unknown_type $user_id
	 */
	private function incrementActivityCount($user_id){ 
		$activityKey = Utils::$redis_keys['activity'].$user_id;
		$this->redis->SELECT ($this->redisActivityDB);
		$value = $this->redis->INCR($activityKey);
		if($value == 1){
			//set expiry till midnight of GMT
			$timeTillMn= Utils::timeTillMidnight(1);/* - (60*330);
			if($timeTillMn<0)$timeForKeyExpiry = Utils::timeTillMidnight(2)- (60*330);
			else $timeForKeyExpiry = Utils::timeTillMidnight() - (60*330);*/
			
			$this->redis->EXPIRE($activityKey, $timeTillMn);
			
		}
	}
	public function getActionsDone($user_id , $user_array){
		$actionIds = $this->uaDBO->getActionsDone($user_id, $user_array);
		//$remainingIds = array_diff($user_array, $actionIds);
		return $actionIds;
	}
	
	public function isActionDone($user1, $user2, $action){
		
		$table = null;
		$result = false;
		if($action == "like") $table = "user_like";
		if($action == "hide") $table = "user_hide";
		if($action == "maybe") $table = "user_maybe";
		
		if($table != null) $result = $this->uaDBO->isActionDone($user1, $user2, $table);
		return $result;
	}

	/**
	 * sets mutual like if not set
	 * and returns the current status of mutual like
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	public function getSetMutualLike($user1, $user2){
		$isliked = $this->uaDBO->isActionSet($user2, $user1, "user_like");
		//var_dump($isliked);
		if($isliked == true){
			$this->uaDBO->setMutualLike($user1, $user2);
			return true;
		}
		return false;
	}
	

	/**
	 * remove user2 from user1 recommendation queue
	 * @param unknown $user1
	 * @param unknown $user2
	 */
	private function remove_from_recommendation($user1,$user2){
		$key = $this->matchKey . $user1;
		//$this->redis->SREM($key, $user2 );
		$this->redis->LREM($key, $user2 );
		$this->check_and_remove_from_second_list($user1, $user2);
	}

	private function check_and_remove_from_second_list($user1, $user2) {
		$uu = new UserUtils();
		$key = $this->matchKeyWithScores . $user1;
		if ($uu->ifKeyExists($key)) {
			$matches_with_scores = $this->redis->LRANGE($key, 0, -1);
			foreach ($matches_with_scores as $data) {
				$pos = strpos($data, $user2);
				if ($pos !== false && $pos == 0) {
					$this->redis->LREM($key, $data);
					break;
				}
			}
		}
	}

	private function incrementActionCount($user1, $user2){
		$key = utils::$redis_keys['match_action_count'] .$user1;
		$this->redis->INCR($key);
	}
	/**
	 *
	 * returns the description if two users can communicate
	 * along with info if one can see name etc
	 * ALL the AB Testing plan needs to be directed through this function only
	 * No need to edit any of the basic calls
	 * TODO: AB testing
	 */
	public function canCommunicate($user1, $user2){
		$ismutualLike = $this->uaDBO->isMutualLikeSet($user1, $user2);
		//var_dump($ismutualLike);
		/*if($ismutualLike){
		$rejectFlag = $this->uaDBO->isActionSet($user1, $user2, "user_reject");
		if(!$rejectFlag) return true;

		}
		return false;
		*/	return $ismutualLike;
	}

	/**
	 * will return the permitted actions for a user on other user (like/hide/msg)
	 * @param $user1
	 * @param $user2
	 */
	public function getPermittedActions($user1, $user2, $user1_status){
		//$actions = $this->getActionsDone($user1, $user2);
		$permittedActions = array("canMsg" => false, "canLike" => false, "canHide" => false);

		//if user is not authentic then no need to give any link
		if($user1_status == 'authentic') {

			$permittedActions['canLike'] = true;
			$permittedActions['canHide'] = true;
			//a separate function call since it needs to be directed by AB testing engine
			$permittedActions['canMsg'] = $this->canCommunicate($user1, $user2);
		}

		return $permittedActions;

	}

	/**
	 * generate links by checking if the action is even permitted or not
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	public function generateLinksForPermittedActions($user1, $user2, $user1_status){

		$uu = new UserUtils();
		$permittedActions = $this->getPermittedActions($user1, $user2, $user1_status);
		$links = array("msg_url" => null, "like_url" => null , "hide_url" => null);

		if($permittedActions['canMsg'])
		$links['msg_url'] = $uu->generateMessageLink($user1, $user2);

		if($permittedActions['canLike'])
		$links['like_url'] = $uu->generateLikeLink($user1, $user2);

		if($permittedActions['canHide'])
		$links['hide_url'] = $uu->generateHideLink($user1, $user2);

		$links['profile_url'] = $uu->generateProfileLink($user1, $user2);

		return $links;
	}



}

