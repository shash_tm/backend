<?php
require_once dirname( __FILE__ ) . "/include/config.php" ;
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";


/**
 * Getting vouchers on last tile of matches for invite friends
 */

class Voucher{

	private $userId;
	private $conn;
	private $uu;
	private $login_mobile;
	private $func;

	function __construct($user_id, $login_mobile) {
		global $conn;

		$this->userId = $user_id;
		$this->conn = $conn;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->func = new functionClass();
		$this->login_mobile = $login_mobile;
	}

	function getVoucher($campaignName){
		global $image_url;
		$MIN_INVITE_COUNT = 10;
		$TOTAL_VOUCHER_AMOUNT = 50;
		$response = array();

		$voucher_info = $this->isVoucherExists($campaignName);
		if(!$voucher_info){
			/*$response['vouchers_count'] = 0;
			$response['minimum_invite_count'] = $MIN_INVITE_COUNT;
			*/$response['vouchers'] = null;
		}else{
			$response['minimum_invite_count'] = $MIN_INVITE_COUNT;
			$response['text'] = "Invite ".$MIN_INVITE_COUNT." or more friends and grab a Rs ".$TOTAL_VOUCHER_AMOUNT." Voucher";
			$i=0;
			foreach($voucher_info as $row => $col){
				$voucher_data[$i]["name"] = $col["sub_category"];
				$voucher_data[$i]["price"] = $TOTAL_VOUCHER_AMOUNT;
				$voucher_data[$i]["expiry"] = $col["expiry"];
				$date_expiry= date ( 'Y-m-d',strtotime($col["expiry"]) );

				if($col['sub_category'] == 'invite_friends_ccd')
				{
					$displayName= "Cafe Coffee Day";
					$imageUrl=  "http://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1416291657639.8_940750387497246208.png";
					$terms= array("Vouchers valid upto ".$date_expiry, "Redeemable at any CCD outlet");
					$friendlyText = "Cafe coffee day voucher worth Rs ".$col["price"];
				}
				else if($col['sub_category'] == 'invite_friends_bookMyShow')
				{
					$displayName= "Book My Show";
					$imageUrl=  "http://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1416291631522_070673003327101472.png";
					$terms= array("Vouchers valid upto ".$date_expiry, "Redeemable at bookmyshow.com");
					$friendlyText = "BookMyShow.com voucher worth Rs ".$col["price"];
				}
				$voucher_data[$i]["display_name"]= $displayName;
				$voucher_data[$i]["image_url"]=  $imageUrl;
				$voucher_data[$i]["terms_conditions"]= $terms;
				$voucher_data[$i]["friendly_text"]= $friendlyText;
				$i++;
			}
			$response['vouchers_count'] = $i;
			$response['vouchers'] = $voucher_data;

		}
		return $response;
	}

	function getVoucherCode($campaignName,$voucher_name){
		$response = array();
		$data = $this->isVoucherExists($campaignName,$voucher_name);
		if(!$data){
			$response['voucher_code'] = null;
		}else{
			$i=0;
			foreach($data as $row => $col){
				$voucher_data[$i]["code"] = $col["voucher_code"];
				$voucher_data[$i]["expiry"] = $col["expiry"];
				$voucher_data[$i]["code_price"] = $col["price"];
				$id[$i] = $col['s_no'];

				$this->conn->Execute($this->conn->prepare("update vouchers SET user_id=?,tstamp=now() where s_no = ?")
				,array($this->userId,$col['s_no']));

				$i++;
			}
			$response['voucher_code'] = $voucher_data;

		}

		return $response;
	}

	function isVoucherExists($campaignName,$voucher_name=null){

		$this->uu = new UserData($this->userId);
		$gender = $this->uu->fetchGender();
		$status = $this->uu->fetchStatus();

		if($status == 'authentic'){

			$rs = $this->conn->Execute($this->conn->prepare("SELECT user_id from vouchers where user_id=? and campaign regexp ?"),array($this->userId,$campaignName));
			$data = $rs->FetchRow();
			if($rs->RowCount() > 0) {
				return false;
			}else{
				$sql = "SELECT s_no , price, voucher_code,expiry, vInfo.sub_category FROM vouchers v LEFT JOIN voucher_info vInfo
					ON v.campaign=vInfo.sub_category WHERE  sub_status='active' AND 
				   vInfo.voucher_name= ? AND user_id IS NULL";
				if(isset($voucher_name)){
					if($voucher_name=="invite_friends_bookMyShow"){
						$sql .= " and sub_category = 'invite_friends_bookMyShow' limit 1";
					}elseif ($voucher_name=="invite_friends_ccd"){
						$sql .= " and sub_category = 'invite_friends_ccd' limit 1";
					}
				}else{
					$sql .= " group by sub_category limit 2";
				}

				$rs = $this->conn->Execute($this->conn->prepare($sql),array($campaignName));
				if($rs->RowCount() > 0){
					$data = $rs->GetRows();
					return $data;
				}else return false;
			}
		}
		return false;
	}
}


try{

	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user['user_id'];

	if(isset($user_id)){
		$output = array();
		$func = new functionClass();
		$headers = Utils::getAllSystemHeaderFields();
		$login_mobile =  $headers['login_mobile'];
		
		if($login_mobile){
			$voucher = new Voucher($user_id, $login_mobile);
			if(isset($_REQUEST['voucher_name']) && isset($_REQUEST['campaign'])){
				$output = $voucher->getVoucherCode($_REQUEST['campaign'],$_REQUEST['voucher_name']);
			}
			else if($_REQUEST['campaign']=='invite_friends'){
				$output = $voucher->getVoucher($_REQUEST['campaign']);
			}
		}
		$output["responseCode"] = 200;
	}
	else{
		$output ['responseCode'] = 401;
	}

	print_r(json_encode($output));

}catch(Exception $e){
	trigger_error("PHP WEB:". $e->getTraceAsString(), E_USER_WARNING);
}


?>