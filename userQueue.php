<?php


include_once(dirname(__FILE__).'/include/config.php');
//include_once(dirname(__FILE__).'/include/constants.php');

/*
 *@author: Himanshu
 * defines a USER's actions on others
 * create all the user queues namely (new, fav, like, hide, reject, comm, liked me)
 * push and pull the data into the queues
 * make copies wherever and whenever required
 * perform deletions when required
 * track all the paths traced by user
 */

class userQueue{

	private $redis;
	private $conn;

	function __construct() {
		global $redis,$conn;
		/*$red = new Redis();
		$red->connect($redis['IP'], $redis['PORT']);*/
		$this->redis = $redis;
		$this->conn=$conn;
	}

	
	//increment the counter 
	public function incUnreadCount($key){
		return $this->redis->INCR($key);
	}
	
	public function incUnreadByCount($key, $count){
		return $this->redis->INCRBY($key, $count);
	}
	
	public function decUnreadCount($key){
		return $this->redis-> DECR($key);
	}
	
	public function decUnreadByCount($key, $count){
		return $this->redis->DECRBY($key, $count);
	}
	
	public function getUnreadCount($key){
		return $this->redis->GET($key);
	}
	
	public function getCountandSetZero($key, $value){
		/*var_dump($key);
		echo $value;*/
		return $this->redis->GETSET($key, $value);
	}
	
	public function getCount($key){
		return $this->redis->GET($key);
	}
	
	public function deleteKey($key){
		$this->redis->DEL($key);
	}
	
	//stores the key value in hash set
	public function storeInHashSet($userId1, $userId2, $key, $val){
		if($userId1>$userId2)
		$this->redis->HSET($userId1. '_' . $userId2, $key, $val);
		else
		$this->redis->HSET($userId2. '_'. $userId1, $key, $val);

	}

	//storet the key value in hash set
	public function getFromHashSet($userId1, $userId2, $key){
		if($userId1>$userId2)
		return $this->redis->HGET($userId1. '_' . $userId2, $key);
		else
		return $this->redis->HGET($userId2 . '_' . $userId1, $key);
	}

	public function storeCurrentState($userId1, $userId2, $state){
		$q1 = $userId1 . '_state_' . $userId2;
		var_dump($state);
		$this->redis->SADD($q1, $state);
	}

	public function getCurrentState($userId1, $userId2){
		return $this->redis->SMEMBERS($userId1 . '_state_' . $userId2);
	}

	//remove ranked id from a rank queue
	public function removeRankedId($userId, $qName, $matchId){
		$key = $qName . '_' . $userId;
		$this->redis->ZREM($key, $matchId);
	}

	//store rank of users in redis
	public function storeRanksInQueue($setName, $keys, $values){

		$key = $setName;
		$args = array();
		$length = count($values);
		$args[] = $key;
		for ($i = 0; $i < $length; $i++) {
			$args[] = $values[$i];
			$args[] = $keys[$i];
		}

		call_user_func_array(array($this->redis,'ZADD'),$args);
	}

	//deletes a key from redis
	public function deleteQueue($qName, $userId){
		$q = $qName . '_' . $userId;
		//echo '***'. $q . "******";
		$this->redis->DEL($q);
	}

	public function ifQueueExists($qName, $userId){
		$q = $qName . '_' . $userId;
		return $this->redis->EXISTS($q);
	}

	public function storeRanksInQ($userId, $qName, $Ids, $scores){
		//var_dump($Ids);
		//var_dump($scores);
		//echo 'inside redis' ;
		//echo
		$q = $qName . '_' . $userId;
		//echo $q . PHP_EOL;
		//$this->redis->SADD($qName. '_' . $myId, $toAdd);
		//echo $this->redis->ZADD('rank_10', 100, 'one');

		$this->redis->ZADD($qName. '_' . $userId  , $scores, $Ids);
	}

	//get count of ranked ids
	public function getCountInQueue($userId, $qName){
		$q = $qName . '_' . $userId;
		return $this->redis->ZCOUNT($q, '-inf', '+inf');
	}

	//get ids on the basis of ranks in a range by index
	public function getRankedIdsInIndexRange($userId, $qName, $start , $end){
		//echo '***********Inside ranked fun()**';
		$q = $qName. '_' . $userId ;
		/* echo $q . '****';
		 echo $start . '******';
		 echo $end ;*/
		//var_dump(	$this->redis->ZREVRANGE('rank_100', 4,6));
		return $this->redis->ZREVRANGE($q, $start, $end);
	}

	/**
	 * TODO: we can store the list in 1 set
	 * @param unknown $user_id
	 */
	public function getInteractedUserList($user_id){
		$qArray = array (
 			'like',
 			'favorite',
 			'rejectedMe',
 			'hide',
 			'reject',
 			'communicate',
 			'likedMe'
 			);
 			$oldIdsList = null;
 			foreach ( $qArray as $qVar )
 			$oldIdsList .= implode ( $this->getDatafromQ ( $user_id, $qVar ), ',' );
 			return $oldIdsList;

	}
	
	public function getMatchesCount($key){
		return $this->redis->LLEN($key);
	}

	public function getUserInteractions($user_id){

// 		$qArray = array (
// 				'like',
// 				'favorite',
// 				'rejectedMe',
// 				'hide',
// 				'reject',
// 				'communicate',
// 				'likedMe'
// 		);
		
		//user_likes_queues","user_favorites_queues","user_hide_queues","user_rejectedMe_queues","user_reject_queues","user_communicate_queues
		$sql=$this->conn->Prepare("select distinct user2 from (select user2 from user_likes_queues where user1=? union 
select user2 from user_favorites_queues where user1=? union
select user2 from user_rejectedMe_queues where user1=? union
select user2 from user_hide_queues where user1=? union
select user2 from user_reject_queues where user1=? union
select user2 from user_communicate_queues where user1=?)as t");
		
		$user_to_neglect=$this->conn->Execute($sql,array($user_id,$user_id,$user_id,$user_id,$user_id,$user_id));
		
		$users=array();
		
		foreach($user_to_neglect as $val){
			$users[]=$val['user2'];
		}	
		
		$key = "user:match:$user_id";
		$redislist = $this->fetchMatch($key, 0 , -1);

		$excludedArr = array_merge($users, $redislist);
		return $excludedArr;
	}
	

	//return user ids of a particular set
	public function getDatafromQ($userId, $qName){
		return  $this->redis->SMEMBERS($qName . '_'. $userId);
	}


//add to match set 
public function setMatches ( $key, $user_array ){
	//echo 'setting match results ' . $key ;
	//var_dump($user_array);
	//TODO: change it to a single insert
	foreach ($user_array as $k => $val)
	$this->redis->RPUSH($key, $k);
}

//get from match set
public function fetchMatch($key, $start, $end){
	//echo 'fetching match results';
	return  $this->redis->LRANGE($key, $start, $end);
}

public function ifKeyExists($key){
	return $this->redis->EXISTS($key);
}

public function removeFromMatchQueue($key, $val, $occurence = 0){
	//echo PHP_EOL. $key .PHP_EOL ;
	//echo $val . PHP_EOL;
	return  $this->redis->LREM($key, $val, $occurence );
}
	//return the user ids of  a particular set for match score
	public function setMatchScoreOfUser($userId1, $userId2, $matchScore){
		$this->redis->HSET('matchQ_'. $userId1, $userId2, $matchScore);
	}

	//by default it returns all the ids in the match Queue
	public function getMatchScoreOfUserIds($myId, $userIdList){
		//var_dump($userIdList);
		//var_dump($myId);
		$key_generation=array();
		foreach ($userIdList as $val){
			$key=($val>$myId)?"$myId:$val":"$val:$myId";
			$key_generation[$val]="MatchQ:$key";
		}
		return array($key_generation,$this->redis->mGet($key_generation));
	}

	public function setMatchScoreOfUserIds($user_id1,$keyVal){
		$final_array=array();
		foreach ($keyVal as $key=>$val){
			$tostore=($key>$user_id1)?"$user_id1:$key":"$key:$user_id1";
			$final_array["MatchQ:$tostore"]=$val;
		}
		$this->redis->mset($final_array);
	}

	//returns if a member exists in a queue
	public function ifExistInQ($myId, $searchId, $qName){
		return $this->redis->SISMEMBER($qname . '_' . $myId, $newUserId);
	}

	//remove an element from a queue
	public function removeFromQueue($myId, $toRemId, $qName){
		echo 'here';
		$this->redis->SREM($qName .'_'. $myId, $toRemId);
	}

	//move from one queue to other
	public function moveToQueue($myId, $searchId, $fromQ, $toQ){
		if($this->usrQ->ifExistInQ($myId, $searchId, $fromQ))
		$this->redis->SMOVE($fromQ .'_'. $myId, $toQ, $searchId);
	}

	// a new user will be added to existing user's queue
	public function addToQueue($myId, $toAdd, $qName){
		//check if the user is not already existing
		/*if($this->redis->SISMEMBER($qName.'_'. $myId, $toAdd)){
		print_r("Member already exists in " . $qName . " queue");
		return;
		}
		else{
		*/ return $this->redis->SADD($qName. '_' . $myId, $toAdd);
		//echo 'Added to '. $qName .  'Queue';
		//TODO:add logging here
		// }
	}
}

/*
$usrQ =  new userQueue();
//global $usrQ;
//$user_id = $_GET['new_user_id'];
//$my_id = $_GET['user_id'];
$newUserId = 942806;*/

/*$usrQ->flushQueue($qName);
 *///$usrQ->flushPrefQueue($newUserId, 'rank', array());
/*for($i=100; $i<160; $i++){
 $usrQ->addToQueue($newUserId, $i, 'favorite');
 }*/
//$usrQ->setMatchScoreOfUser($newUserId, 10274, 10);
//$usrQ->getMatchScoreOfUserIds$newUserId, 10274;
//$usrQ->removeFromQueue($newUserId,67488 , 'rank');
//$usrQ->storeRanksInQ(100, 'rank' , '12, 23 ,25' , '5,5,6');
//var_dump($usrQ->getRankedIdsInIndexRange(942806, 'rank', 0 , 4));

//var_dump($usrQ->getDataFromQ(942806, 'like'));
//$myId = 1;
//addToMatchScoreQ(100,1000,25); 
//$redis = userQueue::createConnection();
//according to the button clicked on UI, pass the Queue Name to the function
//$usrQ->addToMatchScoreQ(100, 4000, 10);
/*$usrQ->addToQueue($newUserId, 119087, 'favorite');
 $usrQ->addToQueue($newUserId, 104498, 'like');

 $usrQ->addToQueue($newUserId, 109754, 'like');*/
//$usrQ->addToQueue($newUserId, 119087, 'favorite');

//$usrQ->storeMatchScore($newUserId, "67488", 50);
//$usrQ->storeMatchScore($newUserId, "64509", 50);
//$ids = array(67488, 64509);
//$val = $usrQ->getMatchScoreOfUserIds($newUserId, $ids);
//var_dump($ids);
//$temp = userQueue::getIds(1000, 'like');
//if(...)
//$usrQ->addToQueue($redis, $myId, $newUserId, $queueName);
//var_dump($usrQ->getMatchedUserIdsByMatchScoreRange(100));

?>

