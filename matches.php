<?php
$then=microtime(true);//
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/include/header.php";
require_once dirname ( __FILE__ ) . "/abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/DBO/matchesDBO.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/include/systemMessages.php";
require_once dirname ( __FILE__ ) . "/logging/emailerLogging.php";
require_once dirname ( __FILE__ ) . "/utilities/counter.php";
require_once dirname ( __FILE__ ) . "/AB_Profiler/Logger/AB_Logger.php";
require_once dirname ( __FILE__ ) . "/AB_Profiler/Config/AB_Config.php";
require_once dirname ( __FILE__ ) . "/Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . "/Recommender.php";
require_once dirname ( __FILE__ ) . "/DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/DBO/sparkDBO.php";
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";
require_once dirname(__FILE__)    . "/include/config.php";

foreach( glob(dirname( __FILE__ ) . "/SelectionStrategy/*.php") as $selection_strategy ) {
    require_once( $selection_strategy );
}
foreach( glob(dirname( __FILE__ ) . "/AB_Profiler/Selector/*.php") as $selector ) {
    require_once( $selector );
}

try {
	global $redis;
	$logg=new EventTrackingClass();

	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
        $systemMsg = null;
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	functionClass::redirect("matches", $login_mobile);
	$device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
	//if user id is valid and redirect

	#Reading the last user id from recommendations before select was bought.
	$select_match_id = $_REQUEST['select_match_id'];

	$select_profile_hash = $_REQUEST['select_profile_hash'];

	if(isset($select_match_id) && $select_match_id != null && is_numeric((int)$select_match_id)){
		if(!isset($select_profile_hash) || $select_profile_hash != SparkDBO::getMd5Hash($user_id, $select_match_id, "spark")){
			//do nothing. hash doesn't match
		}else
			$lastUserId = $select_match_id;
	}else{
		$lastUserId = $redis->get("m:one_before_select:".$user_id);
		if(isset($lastUserId) && is_numeric((int)$lastUserId)) {
			$redis->DEL("m:one_before_select:".$user_id);
		}
	}


	if(isset($user_id)){
		$ud = new UserData($user_id);
		$status = $ud->fetchStatus();
		$gender = $ud->fetchGender();
		if($status == "authentic"){
			$systemFlags = $ud->fetchSystemFlagsOfUser();
		}
		$reco_count = ($gender=="M")?Utils::$recommendation_count_male:Utils::$recommendation_count_female;
		$authenticity = $ud->fetchAuthenticity();
		$isAuthentic = $authenticity['TM_authentic'];

		if($user_id < 17103){
			$isHashTagSet = $ud->isShownHashTile();
			if($device_type == "html"){
				if($isHashTagSet == false){
					$oldUserDiv = $ud->fetchOldUserDiv();
					if($oldUserDiv != "1"){
						$smarty->assign("oldUser", 1);
						$ud->setOldUserMessageDiv();
					}
				}
			}
		}
		else{
			$isHashTagSet = true;
		}

		if($isAuthentic == true){
			$firstTileAuthenticFlag = $ud->fetchFirstTileShown();;
		}
                
                $userUtils = new userUtilsDBO();
                $user_data = $userUtils->getUserData(array($user_id), false);
                
                
                $age = -1;
                $state = "";
                $city = "";
                $country = "";
                if(sizeof($user_data) > 0) {
                    $age = $user_data[0]['age'];
                    $state = $user_data[0]['stay_state'];
                    $city = $user_data[0]['stay_city'];
                    $country = $user_data[0]['stay_country'];
                }
		$fmatch_user = new Recommender ($user_id , $gender, $age, $state, $city);
		
		$recommendations_data = $fmatch_user->fetchResults();
		$ids = $recommendations_data['recommendations'];
                $repeat_likes = $recommendations_data['repeat_likes'];
                $selectSprinkledProfiles = $recommendations_data['selectSprinkledProfiles'];
                $profilesAdCampaigns = $recommendations_data['profilesAdCampaigns'];
                
                $ids = ExtraneousProfileHandler::recommendationFeedInserter($ids,array("profilesAdCampaigns" => $profilesAdCampaigns,"repeat_likes" => $repeat_likes,"selectProfiles" => $selectSprinkledProfiles));
                
                $logdata = $recommendations_data['recommendations_information'];
		$sessionId = $recommendations_data['sessionId'];
		$validProfilePic = $recommendations_data['validProfilePic'];
		$noPhotos = $recommendations_data['noPhotos'];
		$likedInThePast = $recommendations_data['likedInThePast'];
                $hasMoreRecommendations = $recommendations_data['hasMoreRecommendations'];
                $isNewRecommendations = $recommendations_data['isNewRecommendations'];
                $isSelect = $recommendations_data['isSelect'];
		$mutual_friends_data = array();
		if (isset($recommendations_data['mutualFriendsData']))
			$mutual_friends_data = $recommendations_data['mutualFriendsData'];

		//to filter ids if multiple calls happened during generation of matches
		$ids = array_unique($ids);
		$arr = $fmatch_user->processIds($ids);
		$all_ids = $arr["all_ids"];
		$likedIds = $arr["like_ids"];
		$availableIds = $arr["available_ids"];
		$countActionTaken = $fmatch_user->getActionCount();
                
                $ids = $all_ids;
               // var_dump($ids);exit;
		if(count($ids)<1 && $countActionTaken == 0){
                    $systemMsg['last_tile_msg'] = systemMsgs::$matches['no-matches'];
			//adding the link even for no matches in case of authentic profiles
			if($status == "authentic")
			$systemMsg['last_tile_link'] = "invite_friends";
		}
		else{
			if($status == 'non-authentic'){
				$systemMsg['first_tile'] = systemMsgs::$matches['first-tile-non-authentic'];
				$systemMsg['first_tile_link'] = "trustbuilder";
				$systemMsg['first_tile_button'] = "Verify now";
				$systemMsg['like_hide_message'] = systemMsgs::$matches['non-authentic-like-hide-msg'];
			}
			elseif($status =='authentic' && !$isAuthentic){
				$systemMsg['first_tile'] = systemMsgs::$matches['first-tile-not-Profile-Pic'];
				$systemMsg['first_tile_link'] = "upload_photo";
				$systemMsg['first_tile_button'] = "Upload photo";
				$systemMsg["stopMovement"] = true;
			}
			elseif($isAuthentic && $firstTileAuthenticFlag == null){
			}

			if($status == "authentic"){
				if ($countActionTaken + count($ids) >= $reco_count){
 					$systemMsg['last_tile_msg'] = systemMsgs::$matches['last-tile-authentic-full-recommendation'];
				}else{
					$systemMsg['last_tile_msg'] = systemMsgs::$matches['last-tile-authentic-less-recommendation'];
				}
				$systemMsg['last_tile_link'] = "invite_friends";
			}
			else{
				if( ($countActionTaken + count($ids) == $reco_count)){
					$systemMsg['last_tile_msg'] = systemMsgs::$matches['last-tile-non-authentic-full-recommendation'];
				}
				else{
					$systemMsg['last_tile_msg'] = systemMsgs::$matches['last-tile-non-authentic-less-recommendation'];
				}
				$systemMsg['last_tile_link'] = "trustbuilder";
			}
		}

		$systemMsg['non_mutual_like_msg'] = systemMsgs::$profile['not_mutual_like'];

		if(!$isAuthentic){
			$systemMsg['like_hide_action_msg'] = systemMsgs::$matches['non-authentic'];
		}
		if ($device_type == 'mobile') {
			$authentication = functionClass::redirect ( 'matches', true );
			if ($authentication ['responseCode'] == 403){
				$output=array("responseCode"=>403);
				echo json_encode ( $output );
				die;
			}
			else{
				$output=array("responseCode"=>200);
				$uu = new UserUtils();
				$i=0;
				$extra_params = array();
				$extra_params['mutual_friends_data'] = $mutual_friends_data;
				$extra_params['repeat_likes'] = $repeat_likes;
                                $extra_params['selectSprinkledProfiles'] = $selectSprinkledProfiles;
                                $extra_params['isSelect'] = $isSelect;

				$mobile_header_values=getallheaders();

				#Putting the ID which was last viewed before the user bought Select here. We should maintain that.
				if(isset($lastUserId) && $lastUserId !== false && is_numeric((int)$lastUserId)){
					$ids = array_values(array_unique(array_merge(array($lastUserId), $ids)));
				}

				$attributes_mobile = $uu->getAttribute("matches_profile", $user_id, $ids, true, $isAuthentic,
						 false, $login_mobile,false,$likedIds, "matches", $availableIds,$mobile_header_values['app_version_code'],
						 $extra_params);
			//	var_dump($attributes_mobile);
				//exit;
				if($attributes_mobile == null) $attributes_mobile = array();
				$ids = array_keys($attributes_mobile);
				$my_data_key = array_search("my_data", $ids);
				unset($ids[$my_data_key]); 
				//to run for loop the variable shud be an array first get counters
				
				$Counters=new Counters();
				$unread_counters=$Counters->getConversationCounter ($user_id);
				//var_dump($attributes_mobile);
				//die;
				$uu->formatMobileData($attributes_mobile, $ids, $output, $user_id, false, true);
				
				
				$output["my_data"]['mutual_like_count']=$unread_counters['mutual_like_count'];
				$output["my_data"]['conversation_count']=$unread_counters['conversation_count'];
				$output["my_data"]['message_count']=$unread_counters['message_count'];

				$output["my_data"]['status'] = $status;
				$output["my_data"]['user_id'] = $user_id;
				$output["my_data"]["show_hash"] = !$isHashTagSet;
				$output['system_messages'] = $systemMsg;
				//check for socket debug
				$socket_debug_flag=($user_id%10)==1?true:false;
				$output["system_flags"]['socket_debug_flag']=$socket_debug_flag;
				$output["system_flags"]['chat_ip']=$config['chat']['failover_ip'];
				//adding socket enabled only when set in db
				
				$output["system_flags"]["is_socket_enabled"]= Recommender::socketEnabledCheck($mobile_header_values,$systemFlags);
                                $output["abString"] = $logdata;
                                $output["hasMoreRecommendations"] = $hasMoreRecommendations;
                                $output["isNewRecommendations"] = $isNewRecommendations;
                                $output["sessionId"] = $sessionId;
				$output["validProfilePic"] = $validProfilePic;
				$output["noPhotos"] = $noPhotos;
				$output["likedInThePast"] = $likedInThePast;
                                $output["sparkCountersLeft"] = SparkDBO::getUserActiveSparkCount($user_id);

				
				//	$output["system_flags"]["is_socket_enabled"] = ($user_id%2 ==1 || (isset($systemFlags["is_socket_enabled"]) && $systemFlags["is_socket_enabled"] == "yes" ) )? true : false;
//				$output["system_flags"]["is_socket_enabled"] = true;	
                
                //$uu->swap_profile_pic($output);
				$time_taken=microtime(true)-$then;
				$log_array=array("user_id"=>$user_id,"activity"=>"matches","event_type"=>"backend_time_track","event_status"=>"success","time_taken"=>$time_taken,"event_info"=>$isNewRecommendations);
				$logg->logActions(array($log_array));
				echo json_encode($output);

			}
		} else {
			functionClass::redirect ( 'matches' );
			$attributes = null;
			if(isset($_REQUEST['getAttributes'])){
				if(count($ids)>0)
				$attributes = $fmatch_user->getAttributes($arr, $isAuthentic, $likedIds, $status);
				echo json_encode($attributes);
				die;
			}
			else{
				$header = new header($user_id);
				$header_values = $header->getHeaderValues();
				$smarty->assign("firstMaybe", $firstMayBeFlag);
				$smarty->assign("isHashTagSet", $isHashTagSet);
				$smarty->assign("header", $header_values);
				$smarty->assign("systemMsgs", $systemMsg);
				$smarty->assign("isAuthentic", $isAuthentic);
				$smarty->assign("user_id", $user_id);
				$smarty->assign("matches",json_encode($ids));
				$smarty->display("templates/match/match.tpl");
			}
		}
	}
	else{
		if($device_type == "mobile"){
			functionClass::redirect("matches", true);
		}
		else header("Location:index.php");
	}
}
catch(Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}
?>
