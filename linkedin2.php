<?php

include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
include_once (dirname ( __FILE__ ) . '/include/tbconfig.php');
require_once (dirname ( __FILE__ ) . '/TrustBuilder.class.php');
require_once (dirname ( __FILE__ ) . '/photomodule/Photo.class.php');
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";

try{

$data=$_POST;
$session=functionClass::getUserDetailsFromSession();

if(isset($session['user_id'])) {
	if($data['numConnections'] < 10){
		$sql=$conn->Prepare("insert into log_linkedin (user_id,time_of_login,member_id,token,token_secret,linkedin_data,num_connections,status) values(?,now(),?,?,?,?,?,?)");
		$conn->Execute($sql,array($session['user_id'],$data['member_id'],'','',$data['firstName']." ".$data['lastName']."-".$data['numConnections']."-".$data['current_designation'],$data['numConnections'],"few connections"));
		print_r(json_encode(array("responseCode"=>403,"error"=>"Too few professional connections for us to validate your linkedin account")));
		die;
	}

	$rs=$conn->Execute("select user_id,member_id from user_linkedin where user_id='".$session['user_id']."' or member_id='".$data['member_id']."'");
	$result=$rs->GetRows();

	if($rs->_numOfRows==0) {
		if(!isset($data['connected_from']))
			$data['connected_from'] = '';
		$data['linkedin_data'] = json_encode($data['linkedin_data']);
		$conn->Execute("insert into user_linkedin (user_id,name,member_id,linkedin_token,linked_token_secret,last_login,num_connections,connected_from,linkedin_user_data) values(?,?,?,?,?,now(),?,?,?);",
		array($session['user_id'],$data['firstName'].' '.$data['lastName'],$data['member_id'],$response['oauth_token'],$response['oauth_token_secret'],$data['numConnections'],$data['connected_from'],$data['linkedin_data']));

		$sql=$conn->Prepare("insert into log_linkedin (user_id,time_of_login,member_id,token,token_secret,linkedin_data,num_connections,status) values(?,now(),?,?,?,?,?,?)");
		$conn->Execute($sql,array($session['user_id'],$data['member_id'],'','',$data['firstName']." ".$data['lastName']."-".$data['numConnections']."-".$data['current_designation'],$data['numConnections'],"Success"));


		$trustbuilder = new TrustBuilder($session['user_id']);
		//$trustbuilder->creditPoint($tbNumbers['credits']['linkedin'],'linkedin',$tbNumbers['validity']['linkenin']);
		$trustbuilder->updateLinkedinTrustScore($data['numConnections'],$data['current_designation']);
		$status = functionClass::getUserStatus($session['user_id']);
		if($status == 'non-authentic'){
			$trustbuilder->authenticateUser();	
		}

		// add photo if profile photo does not exists
		if(isset($data['profile_pic'])) {
			$photomodule = new Photo($session['user_id']);
			$pic_exits = $photomodule->profilePicExists($user_id);
			if(!$pic_exits) {
				$time1 = time();
				$photomodule->addFromComputer(false,$data['profile_pic'],true,'yes');
				$time2 = time();
				$diff = $time2-$time1;
				$Trk = new EventTrackingClass();
				$data_logging = array();
				$data_logging [] = array("user_id"=> $session['user_id'] , "activity" => "trustbuilder" , "event_type" => "linkedin_profile_pic", "event_status" => "success","time_taken"=>$diff );
				$Trk->logActions($data_logging);
			}
		}
		
		print_r(json_encode(array("responseCode"=>200)));
	}
	else if($session['user_id']!=$result[0]['user_id']||strcmp($result[0]['member_id'],$data['member_id'])){	
		$sql=$conn->Prepare("insert into log_linkedin (user_id,time_of_login,member_id,token,token_secret,linkedin_data,num_connections,status) values(?,now(),?,?,?,?,?,?)");
		$conn->Execute($sql,array($session['user_id'],$data['member_id'],'','',$data['firstName']." ".$data['lastName']."-".$data['numConnections']."-".$data['current_designation'],$data['numConnections'],"This linkedin account is in use by another trulymadly profile"));
		print_r(json_encode(array("responseCode"=>403,"error"=>"Err! Seems like this LinkedIn account is already in use.")));
	}
	else if($session['user_id']==$result[0]['user_id'] && !strcmp($result[0]['member_id'],$data['member_id'])){
		$sql=$conn->Prepare("insert into log_linkedin (user_id,time_of_login,member_id,token,token_secret,linkedin_data,num_connections,status) values(?,now(),?,?,?,?,?,?)");
		$conn->Execute($sql,array($session['user_id'],$data['member_id'],'','',$data['firstName']." ".$data['lastName']."-".$data['numConnections']."-".$data['current_designation'],$data['numConnections'],"re verify linkedin account"));
	
		/*
	 	* update linkedin data on re-verify
	 	*/
		$conn->Execute($conn->prepare("update user_linkedin set num_connections=?,connected_from=?,linkedin_user_data=? where user_id=?"),
			array($data['numConnections'],$data['connected_from'],json_encode($data['linkedin_data']),$session['user_id']));
	
		$trustbuilder = new TrustBuilder($session['user_id']);
		$trustbuilder->updateLinkedinTrustScore($data['numConnections'],$data['current_designation']);
		print_r(json_encode(array("responseCode"=>200)));
	}
	else{
		print_r(json_encode(array("responseCode"=>200)));
	}
}
else {
	print_r(json_encode(array("responseCode"=>401)));
}
}
catch (Exception $e){
	trigger_error($e->getMessage());
}


?>
