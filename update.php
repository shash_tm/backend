<?php 
require_once (dirname ( __FILE__ ) . '/include/config.php');
require_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . '/include/validate.php');
require_once (dirname ( __FILE__ ) . '/UserUtils.php');
require_once (dirname ( __FILE__ ) . '/HashTag.class.php');
require_once (dirname ( __FILE__ ) . '/include/Utils.php');
require_once (dirname ( __FILE__ ) . '/abstraction/query_wrapper.php');

try{
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	functionClass::redirect ( 'editprofile',$login_mobile );
	$data = $_POST;
	$user = functionClass::getUserDetailsFromSession();
	$user_id = $user['user_id'];
	$status = functionClass::getUserStatus($user_id);
	$uu = new UserUtils();
	
	unset($data['login_mobile']);
	
	switch ($data ['param']) {
		
		case "interest":
			unset($data['param']);
			
			$user_data = getHashTag($user_id);          // get the user's old hashtag stored
			$result = array_diff_assoc($user_data, $data);
		
			/*
			 * if there is some change in hash tag then update the hashtag and clear the recommendation cache
			*/
			if(count($result)>0){
				
				/*$query="insert into interest_hobbies(user_id,preferences,tstamp) values(?,?,NOW()) on DUPLICATE KEY UPDATE preferences=?,tstamp=NOW()";
				$param_array=array($user_id,$data['TM_interest'],$data['TM_interest']);
				$tablename="interest_hobbies";
				Query_Wrapper::INSERT($query, $param_array, $tablename);
				*/
				/*$conn->Execute($conn->prepare("insert into interest_hobbies(user_id,preferences,tstamp) values(?,?,NOW()) on DUPLICATE KEY UPDATE preferences=?,tstamp=NOW()"),array($user_id,$data['TM_interest'],$data['TM_interest']));*/
				$hashTag = new HashTag($user_id);
				$data['TM_interest'] = json_decode($data['TM_interest'],true);
				$user_data['TM_interest'] = json_decode($user_data['TM_interest'],true);
				
				$diff = array();
				$newArr = array();
				foreach ($data['TM_interest'] as $val) {
					$newArr[] = strip_tags($val);
					if(!in_array($val,$user_data['TM_interest'])) {
						$diff[] = $val;
					}
				}
				$hashTag->saveHashTag($diff);

				$newHashTag = json_encode($newArr);
				$query="insert into interest_hobbies(user_id,preferences,tstamp) values(?,?,NOW()) on DUPLICATE KEY UPDATE preferences=?,tstamp=NOW()";
                                $param_array=array($user_id,$newHashTag,$newHashTag);
                                $tablename="interest_hobbies";
                                Query_Wrapper::INSERT($query, $param_array, $tablename);

				$uu->clearRecommendationCache($user_id,$status);
			}
			break;
		case "age":
			$stay_city_display="";
			if($data['TM_HaveChildren']=='')
				unset($data['TM_HaveChildren']);
				
			if($data['TM_S3_StayState']==999999){
				$data['TM_S3_StayState'] = null;
			}else{
				$stayState=explode("-",$data['TM_S3_StayState']);
				if($stayState[1]){
					if($stayState[1]==999999){
						$stayState[1] = null;
					}
					$data['TM_S3_StayState']=$stayState[1];
				}
				else unset($data['TM_S3_StayState']);
			}
			
			if($data['TM_S3_StayState'] == '756') {
				$data['TM_S3_StayCountry'] = 219;
			}	
		    else if($data['TM_S3_StayState'] == '651') {
				$data['TM_S3_StayCountry'] = 114;
			}
			if($data['TM_S3_StayCity']==999999){
				$stay_city_display = null;
			}
			else{
				$stayCity=explode("-",$data['TM_S3_StayCity']);
				if($stayCity[2]){
					if($stayCity[2]==999999){
						$stayCity[2] = null;
					}
					$stay_city_display=$stayCity[2];
				}
				else unset($data['TM_S3_StayCity']);
			}
			$data['TM_S3_StayCity'] = $stay_city_display;
				
			if($status=="authentic"){
				
				$query="update user_data set marital_status=?,haveChildren=?,
						stay_country=?,stay_state=?,stay_city=? where user_id=?";
				$param_array=array($data['TM_Marital_status'],$data['TM_HaveChildren'],
								$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$user_id);
				$tablename="user_data";
				Query_Wrapper::update($query, $param_array, $tablename);
				
				/*$conn->Execute($conn->prepare("update user_data set marital_status=?,haveChildren=?,
						stay_country=?,stay_state=?,stay_city=? where user_id=?"),array($data['TM_Marital_status'],$data['TM_HaveChildren'],
								$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$user_id));*/
				
				if($data['TM_Marital_status'] == 'Divorced' || $data['TM_Marital_status'] == 'Widowed' || $data['TM_Marital_status'] == 'Separated' || $data['TM_Marital_status'] == 'Married Before') {
					$data['TM_Marital_status'] = 'Married Before';
				}else {
					$data['TM_Marital_status'] = 'Never Married';
				}
				
// 				$conn->Execute($conn->prepare("update user_search set marital_status=?,country=?,state=?,city=? where user_id=?"),array($data['TM_Marital_status'],
// 								$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$user_id));
				
				
			}else{
				
				
				
				$query="update user_data set marital_status=?,haveChildren=?,
					stay_country=?,stay_state=?,stay_city=? where user_id=?";
				$param_array=array($data['TM_Marital_status'],$data['TM_HaveChildren'],
							$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$user_id);
				$tablename="user_data";
				Query_Wrapper::update($query, $param_array, $tablename);
				
				/*$conn->Execute($conn->prepare("update user_data set marital_status=?,haveChildren=?,
					stay_country=?,stay_state=?,stay_city=? where user_id=?"),array($data['TM_Marital_status'],$data['TM_HaveChildren'],
							$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$user_id));*/
			}
			break;
		case "lifestyle":
			
			
			
			$query="update user_data set smoking_status=?,drinking_status=?,food_status=?
					 where user_id=?";
			$param_array=array($data['TM_S2_Smoking_status'],$data['TM_S2_Drinking_status'],
					 $data['TM_S2_Food_status'],$user_id);
			$tablename="user_data";
			Query_Wrapper::update($query, $param_array, $tablename);
			
			/*$conn->Execute($conn->prepare("update user_data set smoking_status=?,drinking_status=?,food_status=?
					 where user_id=?"),array($data['TM_S2_Smoking_status'],$data['TM_S2_Drinking_status'],
					 $data['TM_S2_Food_status'],$user_id));*/
			
// 			if($status == 'authentic'){
// 				$conn->Execute($conn->prepare("update user_search set smoking_status=?,drinking_status=?,food_status=?
// 					 where user_id=?"),array($data['TM_S2_Smoking_status'],$data['TM_S2_Drinking_status'],
// 					 $data['TM_S2_Food_status'],$user_id));
// 			}
			break;
		case "religion":
			$data['height']=12*$data['TM_S1_HeightFoot']+$data['TM_S1_HeightInch'];
			
			
			$query="update user_data set religion=?,height=?,income_start=?,income_end=?
					where user_id=?";
			$param_array=array($data['TM_S2_Religion'],$data['height'],$data['TM_Income_start'],
					$data['TM_Income_end'],$user_id);
			$tablename="user_data";
			Query_Wrapper::update($query, $param_array, $tablename);
			
			/*$conn->Execute($conn->prepare("update user_data set religion=?,height=?,income_start=?,income_end=?
					where user_id=?"),array($data['TM_S2_Religion'],$data['height'],$data['TM_Income_start'],
					$data['TM_Income_end'],$user_id));*/
			
// 			if($status == 'authentic'){
// 				$conn->Execute($conn->prepare("update user_search set height=? where user_id=?"),array($data['height'],$user_id));
// 			}
			break;
		case "work":
			
			if($data['TM_Designation_0']==''){
				$data['TM_Designation_0'] = null;
			}else{
				if($login_mobile){
					$data['TM_Designation_0'] = urldecode(strip_tags($data['TM_Designation_0']));
				}else{
					$data['TM_Designation_0'] = html_entity_decode(strip_tags($data['TM_Designation_0']));
				}
			}
			if($login_mobile){
				$data['TM_Company_name_0'] = urldecode($data['TM_Company_name_0']);
			}
			$data['TM_Company_name_0'] =  processData($data['TM_Company_name_0']);
			if($data['TM_Working_area']=='Not Working' || $data['TM_Working_area']=='no'){
				$data['TM_Working_area']='no';
			}else{
				$data['TM_Working_area']='yes';
			}
			
			if($data['TM_Income_start']=='' || $data['TM_Income_end']=='') {
				$data['TM_Income_start'] = null;
				$data['TM_Income_end'] = null;
			}
				
			if($data['TM_Industry_0']==''){
				$data['TM_Industry_0'] = null;
			}
			
			$query="update user_data set industry=?,designation=?,work_status=?,company_name=?
					where user_id=?";
			$param_array=array($data['TM_Industry_0'],$data['TM_Designation_0'],$data['TM_Working_area'],
					$data['TM_Company_name_0'],$user_id);
			$tablename="user_data";
			Query_Wrapper::update($query, $param_array, $tablename);
				
			
			/*$conn->Execute($conn->prepare("update user_data set industry=?,designation=?,work_status=?,company_name=?
					where user_id=?"),array($data['TM_Industry_0'],$data['TM_Designation_0'],$data['TM_Working_area'],
					$data['TM_Company_name_0'],$user_id));*/

			break;
		case "education":
			if($login_mobile){
				$data['TM_Institute_0'] = urldecode($data['TM_Institute_0']);
			}
			$data['TM_Institute_0'] =  processData($data['TM_Institute_0']);
			
			unset($data['param']);
			$user_data = getEducation($user_id);
			$result = array_diff_assoc($user_data, $data);
				
			/*
			 * if there is some change in education data then update the data and clear the recommendation cache
			*/
			if(count($result)>0){
				if($data['TM_Education_0']<=0 || $data['TM_Education_0']>21) {
					$data['TM_Education_0'] = Utils::highestDegree($data['TM_Education_0']);
				}
				
				
				$query="update user_data set highest_degree=?,institute_details=?
					where user_id=?";
				$param_array=array($data['TM_Education_0'],$data['TM_Institute_0'],$user_id);
				$tablename="user_data";
				Query_Wrapper::update($query, $param_array, $tablename);
					
				
				/*$conn->Execute($conn->prepare("update user_data set highest_degree=?,institute_details=?
					where user_id=?"),array($data['TM_Education_0'],$data['TM_Institute_0'],$user_id));*/
					
// 				if($status == 'authentic'){
// 					$sql = $conn->Prepare("select rank from highest_degree where id = ?");
// 					$res = $conn->Execute($sql, array($data['TM_Education_0']));
// 					$row = $res->FetchRow();
			
// 					$conn->Execute($conn->prepare("update user_search set highest_education=?,education_group=?
// 					where user_id=?"),array($data['TM_Education_0'],$row['rank'],$user_id));
// 				}
				$uu->clearRecommendationCache($user_id,$status);
			}
			break;
		case "worknew":
			if($data['TM_Designation_0']==''){
                $data['TM_Designation_0'] = null;
            }else{
            	if($login_mobile){
					$data['TM_Designation_0'] = urldecode(strip_tags($data['TM_Designation_0']));
                }else{
                    $data['TM_Designation_0'] = html_entity_decode(strip_tags($data['TM_Designation_0']));
                }
            }
            if($login_mobile){
                $data['TM_Company_name_0'] = urldecode($data['TM_Company_name_0']);
            }
            $data['TM_Company_name_0'] =  processData($data['TM_Company_name_0']);
                        
            if($data['TM_Working_area']=='Not Working' || $data['TM_Working_area']=='no'){
                    $data['TM_Working_area']='no';
            }else{
                    $data['TM_Working_area']='yes';
            }
			if($data['TM_Income_start']=='' || $data['TM_Income_end']=='') {
				$data['TM_Income_start'] = null;
				$data['TM_Income_end'] = null;
			}
			
			if($data['TM_Industry_0']==''){
				$data['TM_Industry_0'] = null;
			}
			
			unset($data['param']);
			$user_data = getWorkDetails($user_id);
			$result = array_diff_assoc($user_data, $data);
				
			/*
			 * if there is some change in work data then update the data and clear the recommendation cache
			*/
			if(count($result)>0) {
				

				$query="update user_data set industry=?,designation=?,work_status=?,company_name=?,
					income_start=?,income_end=? where user_id=?";
				$param_array=array($data['TM_Industry_0'],$data['TM_Designation_0'],
										$data['TM_Working_area'],$data['TM_Company_name_0'],$data['TM_Income_start'],$data['TM_Income_end'],$user_id);
				$tablename="user_data";
				Query_Wrapper::update($query, $param_array, $tablename);
				
				/*$conn->Execute($conn->prepare("update user_data set industry=?,designation=?,work_status=?,company_name=?,
					income_start=?,income_end=? where user_id=?"),array($data['TM_Industry_0'],$data['TM_Designation_0'],
										$data['TM_Working_area'],$data['TM_Company_name_0'],$data['TM_Income_start'],$data['TM_Income_end'],$user_id));*/
// 				if($status=='authentic') {
// 					$conn->Execute($conn->prepare("update user_search set income_start=?,income_end=? where user_id=?"),
// 							array($data['TM_Income_start'],$data['TM_Income_end'],$user_id));
// 				}
				$uu->clearRecommendationCache($user_id,$status);
			}
			
			break;
		case "basics":
			$stay_city_display="";
			if($data['TM_HaveChildren']=='')
				unset($data['TM_HaveChildren']);
			
			if($data['TM_S3_StayState']==999999){        // for other states only for android
				$data['TM_S3_StayState'] = null;
			}else{
				$stayState=explode("-",$data['TM_S3_StayState']);
				if($stayState[1]){
					if($stayState[1]==999999){
						$stayState[1] = null;
					}
					$data['TM_S3_StayState']=$stayState[1];
				}
				else unset($data['TM_S3_StayState']);
			}

			if($data['TM_S3_StayState'] == '756') {
                  $data['TM_S3_StayCountry'] = 219;
            }
            else if($data['TM_S3_StayState'] == '651') {
            	$data['TM_S3_StayCountry'] = 114;
            }
            
			if($data['TM_S3_StayCity']==999999){      // for other cities only for android
				$stay_city_display = null;
			}
			else{
				$stayCity=explode("-",$data['TM_S3_StayCity']);
				if($stayCity[2]){
					if($stayCity[2]==999999){
						$stayCity[2] = null;
					}
					$stay_city_display=$stayCity[2];
				}
				else unset($data['TM_S3_StayCity']);
			}
			$data['TM_S3_StayCity'] = $stay_city_display;
			
			$data['height']=12*$data['TM_S1_HeightFoot']+$data['TM_S1_HeightInch'];
			
			unset($data['param']);
			$user_data = getBasicData($user_id);
			$result = array_diff_assoc($user_data, $data);
				
			/*
			 * if there is some change in basics data then update the data and clear the recommendation cache
			*/
			if(count($result)>0) {
				if($status=="authentic"){
					if($data['TM_Marital_status'] == 'Divorced' || $data['TM_Marital_status'] == 'Widowed' || $data['TM_Marital_status'] == 'Separated' || $data['TM_Marital_status'] == 'Married Before') {
						$data['TM_Marital_status'] = 'Married Before';
					}else {
						$data['TM_Marital_status'] = 'Never Married';
					}
// 					$conn->Execute($conn->prepare("update user_search set marital_status=?,country=?,state=?,city=?,height=? where user_id=?"),array($data['TM_Marital_status'],
// 							$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$data['height'],$user_id));
			
				}
				
				$query="update user_data set marital_status=?,haveChildren=?,
						stay_country=?,stay_state=?,stay_city=?,height=? where user_id=?";
				$param_array=array($data['TM_Marital_status'],$data['TM_HaveChildren'],
											$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$data['height'],$user_id);
				$tablename="user_data";
				Query_Wrapper::update($query, $param_array, $tablename);
				
				/*$conn->Execute($conn->prepare("update user_data set marital_status=?,haveChildren=?,
						stay_country=?,stay_state=?,stay_city=?,height=? where user_id=?"),array($data['TM_Marital_status'],$data['TM_HaveChildren'],
											$data['TM_S3_StayCountry'],$data['TM_S3_StayState'],$data['TM_S3_StayCity'],$data['height'],$user_id));*/
			
				$uu->clearRecommendationCache($user_id,$status);
			}
			
			break;
	}
	
	setResponse();
	
}catch(Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}

function setResponse() {
	print_r(json_encode(array("responseCode"=>200)));
	die;
}

function getHashTag($user_id) {
	global $conn;
	$sql = $conn->Execute($conn->prepare("select preferences as TM_interest from interest_hobbies where user_id=?"),array($user_id));
	$data = $sql->FetchRow();
	return $data;
}

function getBasicData($user_id) {
	global $conn;
	$sql = $conn->Execute($conn->prepare("select marital_status as TM_Marital_status, haveChildren as TM_HaveChildren,
			stay_country as TM_S3_StayCountry, stay_state as TM_S3_StayState, stay_city as TM_S3_StayCity,
			height from user_data where user_id=?"),array($user_id));
	$data = $sql->FetchRow();
	return $data;
}

function getEducation($user_id) {
	global $conn;
	$sql = $conn->Execute($conn->prepare("select highest_degree as TM_Education_0, institute_details as TM_Institute_0
			from user_data where user_id=?"),array($user_id));
	$data = $sql->FetchRow();
	return $data;
}

function getWorkDetails($user_id) {
	global $conn;
	$sql = $conn->Execute($conn->prepare("select industry as TM_Industry_0, designation as TM_Designation_0,work_status as TM_Working_area,
			company_name as TM_Company_name_0, income_start as TM_Income_start, income_end as TM_Income_end from user_data where user_id=?"),
			array($user_id));
	$data = $sql->FetchRow();
	return $data;
}

function processData($data){
	$tmp = array();
	$ds = array();
	$data = json_decode($data,true);
	foreach($data as $id=>$value){
		if(!$login_mobile){
			$value = html_entity_decode($value);
		}
		if(isset($value) && strcasecmp($value, "null") != 0){
			$data2 = explode(" ",$value);
			foreach($data2 as $key=>$val){
				$tmp[$key] = ucfirst(trim(strip_tags($val))); 
			}
		}

		$ds[$id] = implode(" ",$tmp);
		unset($tmp);
	}
	$data = json_encode($ds);
	return $data;
}


?>
