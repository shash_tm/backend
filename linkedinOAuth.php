<?php

include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . '/TrustBuilder.class.php');
require_once dirname ( __FILE__ ) . "/include/Utils.php";

try{

	$userData=$_REQUEST;
	$auth_code = $userData['auth_code'];
	$session=functionClass::getUserDetailsFromSession();

	if(isset($auth_code)) {
		$resultData = getAccessTokenFromAuthCode($auth_code);
		if(isset($resultData['error'])) {
			print_r(json_encode(array("responseCode" => 403, "error" => $resultData['error_description'])));
		}
		else if(isset($resultData['access_token'])){
			$userData = getUserDataFromAccessToken($resultData['access_token']);
			if(isset($userData["id"])){
				verifyMinimumConnections($userData);
				$sql = "select user_id,member_id from user_linkedin where  member_id=?";
				$userMember = Query_Wrapper::SELECT($sql, array($userData['id']), Query::$__slave, true) ;
				if(count($userMember) == 0){
					createNewLinkedinEntry($userData, $resultData['access_token']);
				}
				else if($session['user_id']!=$result[0]['user_id']||strcmp($result[0]['member_id'],$userData['id'])){
					$sql= "insert into log_linkedin (user_id,time_of_login,member_id,token,token_secret,linkedin_data,num_connections,status) values(?,now(),?,?,?,?,?,?)";
					Query_Wrapper::INSERT($sql,array($session['user_id'],$userData['id'],'','',$userData['firstName']." ".$userData['lastName']."-".$userData['numConnections']."-".$userData['current_designation'],$userData['numConnections'],"This linkedin account is in use by another trulymadly profile"), "log_linkedin");
					print_r(json_encode(array("responseCode"=>403,"error"=>"Err! Seems like this LinkedIn account is already in use.")));
				}
			}
		}

	}
	else {
		print_r(json_encode(array("responseCode"=>401)));
	}
}
catch (Exception $e){
	trigger_error($e->getMessage());
}

function getAccessTokenFromAuthCode($auth_code){
	global $linkedin_api, $linkedin_secret;
	$url = "https://www.linkedin.com/uas/oauth2/accessToken";
	$redirectUri = urlencode("http://www.trulymadly.com");
	$paramString = "grant_type=authorization_code&code=$auth_code&redirect_uri=$redirectUri&client_id=$linkedin_api&client_secret=$linkedin_secret";
	$result = Utils::curlCall($url,$paramString,array("Content-Type"=> "application/x-www-form-urlencoded"));
	$resultData = json_decode($result,true);
	return $resultData;
}

function getUserDataFromAccessToken($access_token){
	$url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,industry,num-connections,email-address,positions,picture-urls::(original))?format=json";
	$headers = array("Authorization: Bearer ".$access_token,'Content-Type: application/json',"x-li-format=json");
	$result = Utils::curlCall($url, null, $headers, "GET");
	$userData = json_decode($result, true);
	return $userData;
}

function verifyMinimumConnections($userData){
	global $session;
	if($userData['numConnections'] < 10){
		$sql="insert into log_linkedin (user_id,time_of_login,member_id,token,token_secret,linkedin_data,num_connections,status) values(?,now(),?,?,?,?,?,?)";
		Query_Wrapper::INSERT($sql, array($session['user_id'],$userData['id'],'','',$userData['firstName']." ".$userData['lastName']."-".$userData['numConnections']."-".$userData['current_designation'],$userData['numConnections'],"few connections"), "log_linkedin") ;
		print_r(json_encode(array("responseCode"=>403,"error"=>"Too few professional connections for us to validate your linkedin account")));
		die;
	}
}

function createNewLinkedinEntry($userData, $oauth_token){
	global $session;
	
	$linkedin_data = json_encode($userData);
	$sql = "insert into user_linkedin (user_id,name,member_id,linkedin_token,linked_token_secret,last_login,num_connections,connected_from,linkedin_user_data) values(?,?,?,?,?,now(),?,?,?)";
	Query_Wrapper::INSERT($sql, array($session['user_id'],$userData['firstName'].' '.$userData['lastName'],$userData['id'],$oauth_token,null,$userData['numConnections'],'trustbuilder',$linkedin_data), "user_linkedin");

	$sql="insert into log_linkedin (user_id,time_of_login,member_id,token,token_secret,linkedin_data,num_connections,status) values(?,now(),?,?,?,?,?,?)";
	Query_Wrapper::INSERT($sql,array($session['user_id'],$userData['id'],'','',$userData['firstName']." ".$userData['lastName']."-".$userData['numConnections']."-".$userData['current_designation'],$userData['numConnections'],"Success"),"log_linkedin");


	$trustbuilder = new TrustBuilder($session['user_id']);
	//$trustbuilder->creditPoint($tbNumbers['credits']['linkedin'],'linkedin',$tbNumbers['validity']['linkenin']);
	$trustbuilder->updateLinkedinTrustScore($userData['numConnections'],$userData['current_designation']);
	$status = functionClass::getUserStatus($session['user_id']);
	if($status == 'non-authentic'){
		$trustbuilder->authenticateUser();
	}

	// add photo if profile photo does not exists
	if(isset($userData['profile_pic']) || isset($userData['pictureUrls']['values'][0])){
		$photomodule = new Photo($session['user_id']);
		$pic_exits = $photomodule->profilePicExists($session['user_id']);
		if(!$pic_exits) {
			$time1 = time();
			$photomodule->addFromComputer(false,$userData['pictureUrls']['values'][0],true,'yes');
			$time2 = time();
			$diff = $time2-$time1;
			$Trk = new EventTrackingClass();
			$userData_logging = array();
			$userData_logging [] = array("user_id"=> $session['user_id'] , "activity" => "trustbuilder" , "event_type" => "linkedin_profile_pic", "event_status" => "success","time_taken"=>$diff );
			$Trk->logActions($userData_logging);
		}
	}

	print_r(json_encode(array("responseCode"=>200, "connections"=>$userData['numConnections']." Connections", "profile_pic" => $userData['pictureUrls']['values'][0])));
}
?>
