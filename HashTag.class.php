<?php 
/*
 * @author Rajesh
*/

require_once dirname( __FILE__ ) . "/include/config.php" ;
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/utilities/wordlist-regex.php";
require_once dirname ( __FILE__ ) . "/DBO/messagesDBO.php";

class HashTag {
	
	static $hashArray = array('#Foodie','#Trendy','#AnimalLover','#TravelBuff','#PartyHopper',
			'#Homebody','#Artsy','#BookWorm','#HealthFreak','#Scholar','#SportsNut',
			'#Shopaholic','#Carefree','#Goofy','#ChatterBox','#GoGetter');
	
	private $conn,$user_id;
	
	function __construct($user_id=NULL) {
		global $conn;
		$this->conn = $conn;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->user_id = $user_id;
	}
	
// 	public function searchHashTag($text) {
// 		global $badwords;
// 		$pos = strpos($text, "#");
	
// 		$userText = array();
// 		$userText[] = strtolower($text);
// 		$isBadWord = array_intersect($userText,$badwords);
		
// 		if($isBadWord != null) {
// 			$this->storeBadMessage($text);
// 			print_r(json_encode(array("responseCode"=>403,"error"=>"Why would you call yourself that?")));
// 			die();
// 		}
// 		$sql = "select name from user_hashtags where name like "."'%$text%'";
		
// 		$hashArr = array();
// 		if($pos === false) {
// 			$text = "#".$text;
// 		}
// 		$hashArr[] = $text;
// 		$rs = $this->conn->Execute($sql);
// 		if ($rs->RowCount () > 0) {
// 			$data = $rs->GetRows ();
// 			foreach($data as $key=>$value){
// 				if(strcasecmp($text, $data[$key]['name']) != 0) {
// 					$hashArr[] = $data[$key]['name'];
// 				}else{
// 					$hashArr[0] = $data[$key]['name'];
// 				}
// // 				if($text != $data[$key]['name'])
// // 					$hashArr[] = $data[$key]['name'];
// 			}
// 		}
// 		print_r(json_encode(array("responseCode"=>200,"hashtag"=>$hashArr)));	
// 	}
	
	public function searchHashTag($text) {
		global $badwords_hashtag;
		$text = ltrim($text,"#");
		$text = strtolower($text);
		
		$text1 = ucfirst($text);
		//$pos = strpos($text, "#");
	
		$userText = array();
		$userText[] = $text;
		$isBadWord = array_intersect($userText,$badwords_hashtag);
	
		if($isBadWord != null) {
			$this->storeBadMessage($text);
			print_r(json_encode(array("responseCode"=>403,"error"=>"Why would you call yourself that?")));
			die();
		}
		global $redis;
		
		$hashArr = array();
// 		if($pos === false) {
// 			$text = "#".$text;
// 			$text1 = "#".$text1;
// 		}
		$hashArr[] = "#".$text1;
		$min = "[".$text1;  $max = "[".$text1."\xff";
		
		$items = $redis->zRangeByLex("user_hashtags", $min, $max);
		
		foreach ($items as $value) {
			if($value == $text1) {
				$hashArr[0] = "#".$value;
			}else {
				$hashArr[] = "#".$value;
			}
		}
			
		print_r(json_encode(array("responseCode"=>200,"hashtag"=>$hashArr))); die();
	}
	
	public function saveHashTag($hashtag=array()) {
		$hashArr = array();
		if(is_array($hashtag)) {
			for ($i = 0; $i < count($hashtag); $i++) {
				if(!in_array($hashtag[$i],self::$hashArray)){
					$hashArr[] = ltrim($hashtag[$i],'#');
				}
			}
		}
		if(count($hashArr)>0) {	
			global $redis;
			for ($i = 0; $i < count($hashArr); $i++) {
				//$text = strtolower($hashArr[$i]);
				//$redis->zAdd("hashtags", 1, strtolower($hashArr[$i]).':'.$hashArr[$i]);
				$redis->zAdd("user_hashtags", 1, ucfirst(strtolower(strip_tags($hashArr[$i]))) );
				//$redis->zAdd("hashtags", 1, $hashArr[$i]);
				
				$hashTag = ucfirst(strtolower(strip_tags($hashArr[$i])));
				$this->conn->Execute($this->conn->prepare("insert into user_hashtags(user_id,name,is_editable,count,tstamp) values(?,?,?,?,NOW())
					on DUPLICATE KEY UPDATE count=count+1"),array($this->user_id,"#".$hashTag,1,1));
			}
		}
	}
	
	private function storeBadMessage ($badMsg){
		$msgDBO = new MessagesDBO();
		$msgDBO->saveBadMessage($this->user_id, NULL, $badMsg,"hashtag");
	}
}

?>
