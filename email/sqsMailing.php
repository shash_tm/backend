<?php

require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
//require_once dirname ( __FILE__ ) . "/../include/aws.phar";
require_once dirname ( __FILE__ ).'/../include/aws/aws-autoloader.php';
require_once dirname ( __FILE__ ).'/../abstraction/query_wrapper.php';

use Aws\Sqs\SqsClient;


/**
 * Script to scheduled for updation of user table
 * on the email-ids in bounce/spam/complaint/out-of-office/suppression list
 * Deleting the messages from SQS after making an updation in the system
 * @author himanshu
 */

class sqsMailing{

	private $config;
	private $conn_master;
	private $sqs;
	//private $throttling_time;

	public function __construct(){
		global $config, $conn_master;
		$this->config=$config;
		$this->conn = $conn_master;

		$this->sqs=SqsClient::factory(array(
				'key'    => $config['amazon_s3']['access_key'],//AKIAJVOYWTGJVYUKYGFQ',
				'secret' => $config['amazon_s3']['secret_key'],//'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',
				'region' => $config['amazon_s3']['sqs_region']));
		//$this->throttling_time=(int)(1000000/$config['email']['throttling'])+1;
	}


	public function readSqsQueueAndDeleteMessage(){
		$configArr = array(
					'QueueUrl' => $this->config['amazon_s3']['sqs_queue_url'],
		//	"MaxNumberOfMessages" =>10,
		//'VisibilityTimeout' => 0,
		'WaitTimeSeconds' => 20,
		"ReceiveMessageWaitTimeSeconds" =>20
		/*'WaitTimeSeconds' => 1,
		 'VisibilityTimeout' => 2*/
		/* 'AttributeNames' => array('string', ... ),
		 'MaxNumberOfMessages' => integer,
		 'VisibilityTimeout' => integer,
		 'WaitTimeSeconds' => integer,		*/
		);
		$results = $this->sqs->receiveMessage($configArr);
		$email_addresses = array();
		$delete_arr = array();
		$i = 0;
		foreach ($results["Messages"] as $key=>$val){
			$delete_arr[$i]["Id"] = $val["MessageId"];
			$delete_arr[$i]["ReceiptHandle"]= $val["ReceiptHandle"];
			$i++;
		}

		$del_arr = array(
					'QueueUrl' => $this->config['amazon_s3']['sqs_queue_url'],
  				    'Entries' => $delete_arr
		);

		$bouncedList = null;
		$spamList = null;
		foreach ($results->getPath('Messages/*/Body') as $messageBody) {

			$arr =  json_decode($messageBody,true);
			$msgBody = json_decode($arr['Message'], true);
			if(isset($msgBody["bounce"])){
				$bouncedList .=  "'".$msgBody["bounce"]["bouncedRecipients"][0]["emailAddress"]. "'". ",";
			}
			if(isset($msgBody['complaint'])){
				$spamList .= "'". $msgBody['complaint']['complainedRecipients'][0]["emailAddress"]."'". ",";
			}
		}

		if($bouncedList != null){
			$bouncedList = rtrim($bouncedList, ',');
			echo $bouncedList;
			
			$query="UPDATE user set email_status = 'bounce' where email_id in (?)";
			$param_array=array($bouncedList);
			$tablename="user";
			
			/*$sql = "UPDATE user set email_status = 'bounce' where email_id in ($bouncedList)";*/
			
			echo $query;
			
			Query_Wrapper::UPDATE($query, $param_array, $tablename);
			/*$this->conn->Execute($sql);*/
		}

		if($spamList !=null){
			$spamList = rtrim($spamList, ',');
			echo $spamList;
			
			$query="UPDATE user set email_status = 'complaint' where email_id in (?)";
			$param_array=array($spamList);
			$tablename="user";
				
			/*$sql = "UPDATE user set email_status = 'complaint' where email_id in ($spamList)";*/
			echo $query;
			Query_Wrapper::UPDATE($query, $param_array, $tablename);
			/*$this->conn->Execute($sql);*/

		}
		if($bouncedList!=null || $spamList!=null){
		$res = $this->sqs->deleteMessageBatch($del_arr);
		var_dump($res);}
	}

}

try{
	//echo '<pre>';
	$sqs = new sqsMailing();
	$sqs->readSqsQueueAndDeleteMessage();
}
catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(), E_USER_NOTICE);
}


?>