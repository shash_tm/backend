<?php 

require_once dirname ( __FILE__ ) . "/../include/config.php";
//require_once dirname ( __FILE__ ) . "/../include/aws.phar";
require_once dirname ( __FILE__ ).'/../include/aws/aws-autoloader.php';

use Aws\Ses\SesClient;

class MailFunctions{
	
	private $config;
	private $ses;
	private $throttling_time;
	
	public function __construct(){
		global $config;
		$this->config=$config;
		$this->ses=SesClient::factory(array(
				'key'    => $config['amazon_s3']['access_key'],//AKIAJVOYWTGJVYUKYGFQ',
				'secret' => $config['amazon_s3']['secret_key'],//'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',
				'region' => $config['amazon_s3']['ses_region']));
		$this->throttling_time=(int)(1000000/$config['email']['throttling'])+1;
	}
	
	/**
	 * $toAddress must be an array of email address to which mail is to send.
	 */
	public function sendMail($toAddress,$htmlData,$textData,$subject,$userId, $from =null, $replyTo = array(),$is_test = false){
		if(count($toAddress)>0 && isset($toAddress[0]) && $this->config['email']['send_mail'] &&  (strpos($toAddress[0], "@trulymadly.com") ==false || $is_test== true)){
		$msg = array();

		if(isset($from)){
			$msg['Source'] = $from;
		}
		else{
			$msg['Source'] = $this->config['email']['source'];//"contact@dev.trulymadly.com";
		}
		
		
		if(isset($replyTo) && !empty($replyTo))
		{ 
			$msg['ReplyToAddresses'] = $replyTo;
		}
		//ToAddresses must be an array
		$msg['Destination']['ToAddresses'] = $toAddress;
		//$msg['Destination']['BccAddresses'] = array('mail-bcc@trulymadly.com');
		$msg['Message']['Subject']['Data'] = $subject;
		$msg['Message']['Subject']['Charset'] = "UTF-8";
		
		$msg['Message']['Body']['Text']['Data'] = $textData;
		$msg['Message']['Body']['Text']['Charset'] = "UTF-8";
		$msg['Message']['Body']['Html']['Data'] =$htmlData;
		$msg['Message']['Body']['Html']['Charset'] = "UTF-8";
		
		$result = $this->ses->sendEmail($msg);
		usleep($this->throttling_time);
		//save the MessageId which can be used to track the request
		$msg_id = $result->get('MessageId');
		return $msg_id;
		}
		/*} catch (Exception $e) {
			//An error happened and the email did not get sent
			trigger_error($e->getMessage());
			return false;
		}
		return true;*/
	}
}


?>
