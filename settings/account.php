<?php 
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";

class UserAccount 
{
	private  $user_id;
	
	function __construct($user_id)
	{
		$this->user_id = $user_id ;
		$this->UserUtils = new userUtilsDBO();
	}
	
	public function deleteAccount($request, $status, $rating) 
	{
	   $reason = $request ['reason'] ;
	    
	   //remove scripts from the field 
	   $sub_reason = htmlentities($request [ 'sub_reason' ]);
// 	   $data = $this->User->getUserDetails($this->user_id);
// 	   $status = $data ['status'] ;
	   
	   if ($status != 'blocked')
	   	$suspend = $this->UserUtils->suspend($this->user_id, $reason, $status,null , 'blocked');
	  
	   $this->UserUtils->markForDeletion($this->user_id, $reason, $sub_reason, $rating) ;
	     
	} 
	
}

?>