<?php
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/config.php";
//require_once dirname ( __FILE__ ) . "/include/tbconfig.php";
//require_once dirname ( __FILE__ ) . "/TrustBuilder.class.php";


/**
 * API wrapper over profile.php to show share profile UI
 * @Himanshu
 */
try{

	$user_id = $_REQUEST['id'];
	$tstamp = $_REQUEST['ts'];
	$checksum = $_REQUEST['cs'];
	$source = $_REQUEST['s'];
	$output = array();
	$uu = new UserUtils();

	if(isset($user_id)&&isset($checksum)&&isset($tstamp)){
		$postCheckSum = $uu->endorsementHash($user_id, $tstamp,$source);
		if($postCheckSum == $checksum){
			$attributes =  $uu->getAttribute('profile', $user_id, array($user_id), $from_match=false, $isAuthentic=false, $myProfile=false, $login_mobile=false, $mutualLikeFlag = false, $likedIds = null, $setFlag = null);
			$attributes[$user_id]['fname'] = str_repeat("x",strlen($attributes['my_data']['fname']));
			$smarty->assign("attributes",$attributes[$user_id]);
			$smarty->display(dirname ( __FILE__ ).'/templates/shareProfile.tpl');
		}
		else{
			//if someone tries to change checksum - relocate it to index page
			header("Location:index.php");
		}
	}
	else{
		header("Location:index.php");
	}
}catch (Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	//echo "PHP WEB: ". $e->getTraceAsString();
}