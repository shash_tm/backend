<?php
include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
include_once (dirname ( __FILE__ ) . '/fb.php');
require_once dirname(__FILE__)."/EventTracking.php";

$data=$_POST;
$func=new functionClass();
$login_via_mobile=$func->isMobileLogin();

function getInfo(){
	return;
}

if($data['login']) {
try {

$prescence=$func->checkEmail($data['email'],$data['password']);
}
catch(Exception $e) {
trigger_error("Could not check email id prescence in user table",E_USER_WARNING);
$error['canNot']=1;
}

if($prescence=='USER_EXISTS') {
	try {
 			$func->setSession('NOT_FB_USER',$data['email']);
 			$user_details=$func->getUserDetailsFromSession();
 			$user_id=$user_details['user_id'];
 			
 			$info = getInfo();
 			$eventTrack = new EventTracking($user_id);
 			$eventTrack->logAction("login through mobile APP using Email ID",0,null,null,null,$info);
 			
 			if($login_via_mobile==true){
 				$sql=$conn->Prepare("insert into user_mobile_gcm_id(user_id,registration_id,tstamp) values(?,?,now()) on duplicate key update
					user_id=?,tstamp=now()");
 				$conn->Execute($sql,array($user_id,$_REQUEST['registration_id'],$user_id));
 			}
 			$response['status']='OK';
	}
 	catch(Exception $e) {
 		trigger_error($e->getMessage(),E_USER_WARNING);
 	}
}
else if($prescence=='FB_USER') {
	$response['status']='Failed';
	$response['reason']='please login via facebook only';
}
else {
	$response['status']='Failed';
	$response['reason']='Incorrect User id or email';
}

}

else if($data['from_fb']) {
	$token=$data['token'];
	$object=new fb($token);
	$object->getBasicData();
	$prescence=$object->checkFbPrescence();
	if($prescence=='CONNECTED_VIA_EMAIL') {
		$response['status']='Failed';
		$response['reason']='please login via Email only';
	}
	else if($prescence=='NEW_USER') {
		$response['status']='Failed';
		$response['reason']='Your account does not exist with us';
	}
	else {
		$user_id=$prescence;
		$object->storeFacebook($user_id,"USER_EXISTS");
		$func->setSession('FB_USER',$object->data['id']);
		$info = getInfo();
		if($login_via_mobile==true){
			$sql=$conn->Prepare("insert into user_mobile_gcm_id(user_id,registration_id,tstamp) values(?,?,now()) on duplicate key update
					user_id=?,tstamp=now()");
			$conn->Execute($sql,array($user_id,$_REQUEST['registration_id'],$user_id));
		}
		$eventTrack = new EventTracking($user_id);
		$eventTrack->logAction("login through mobile APP using Facebook",0,null,null,null,$info);
		$response['status']='OK';
	}
}
print_r(json_encode($response));
?>