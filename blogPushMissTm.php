<?php
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ )  . "/include/config_admin.php";
//require_once dirname ( __FILE__ )  . "/../../DBO/messagesDBO.php";
require_once dirname ( __FILE__ ) . "/msg/MessageFullConversation.class.php";
require_once dirname ( __FILE__ ) . "/Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . "/mobile_utilities/pushNotification.php";


class NewCouponMessage
{
    private $conn_reporting;
    private $user_id ;
    private $messageFullObj ;
    private $message ;
    private $misstm ;
    private $gender ;
    private $notify;
    private $uu ;
    private $messageId ;
    //  private $type;
    function __construct()
    {
        global $conn_reporting, $miss_tm_id ;
        // $this->type = $type;
        $this->conn_reporting = $conn_reporting ;
        $this->user_id ;
        $this->messageFullObj ;
        $this->message ;
        $this->imageMessage;
        $this->misstm= $miss_tm_id;
        $this->gender ;
        $this->notify = new pushNotification();
        $this->uu = new UserUtils();
        $this->messageId;
    }

    private function sendMessage()
    {
        $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
        $this->messageId=  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
        //  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

    }
    private function sendImageMessage()
    {
        $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
        $this->messageId =  $this->messageFullObj->storeMostRecentChat ( $this->imageMessage, 'JSON', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
        //  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

    }

    private function getMessage()
    {
        /*if($this->type == 'mingle')
          $this->message="Guys! Here's an exciting way to meet other like-minded peeps. Unleash your inhibitions for a fun night of dancing, drinking, mingling, and good fun! Book your slot now";
        else*/
        $this->message="Hey guys! Who's game for game night? From Bumbling Beer Pong to Twisting Taboo, we've a massive collection of games that'll keep everyone intrigued. Book your slot now";

    }

    private function getImageMessage()
    {
        global $cdnurl, $baseurl ;
//            if($this->type == 'mingle')
//            {
        $this->imageMessage = array( "msg"=>"32 Pick up lines that you should never use.",
            "metadata" => array("link_landing_url" => "http://bit.ly/CreepyPickUpLines",
                "message_type" => "IMAGE_LINK",
                "link_image_url" =>$cdnurl."/images/notifications/zoey.png"),
            "message_type" => "IMAGE_LINK");
        /* }
         else if ($this->type == 'game')
         {
             $this->imageMessage = array( "msg"=>"Hey guys! Who's game for game night? From Bumbling Beer Pong to Twisting Taboo, we've a massive collection of games that'll keep everyone intrigued. Book your slot now",
                 "metadata" => array("link_landing_url" => "https://trulymad.ly/evnt.php?e=675_38816",
                     "message_type" => "IMAGE_LINK",
                     "link_image_url" =>$cdnurl."/images/notifications/game.jpg"),
                 "message_type" => "IMAGE_LINK");
         }
         else
         {
             echo "No type defined";
             die ;
         }*/
        $this->imageMessage= json_encode($this->imageMessage);
        echo $this->imageMessage;

        //Hello1
//            $this->imageMessage = array("msg"=> "You've received an image!",
//                                        "metadata" => array("message_type" => "IMAGE",
//                                                            "urls" => array("jpeg_size" => "18KB",
//                                                                "webp"=>"https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064.webp",
//                                                                "jpeg"=>"https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064.jpeg",
//                                                                "thumbnail_jpeg"=> "https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064_1391366929.jpg",
// 			                                                    "thumnnail_webp" => "https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064_1391366929.webp",
// 			                                                    "webp_size"=> "14KB"
//                                                                ))) ;
        //         $this->imageMessage= json_encode($this->imageMessage) ;
    }


    private function createMessageConvoObj()
    {
        $this->messageFullObj = new MessageFullConversation ( $this->misstm, $this->user_id );
    }
    private function sendPushNotification()
    {
        $message_url = $this->uu->generateMessageLink($this->user_id,$this->misstm);
        $push_arr =  array("is_admin_set"=> 0,
            "content_text"=>"32 Pick up lines that you should never use.",
            "ticker_text"=>"Message From Miss TM",
            "title_text"=>"TrulyMadly",
            "message_url"=>$message_url,
            "match_id"=>$this->user_id,
            "msg_type" => "TEXT",
            "msg_id"=> $this->messageId,
            "push_type"=>"MESSAGE",
            "event_status"=> "Miss_tm_Blog_push");

        $this->notify->notify($this->user_id, $push_arr, $this->misstm);
    }

    public function getUsersAndSend($target)
    {
        if($target == 'all')
        {
            $sql="select distinct(u.user_id), u.gender from user_data ud join user u  on u.user_id = ud.user_id
                    join messages_queue mq on mq.sender_id = u.user_id
                    join user_gcm_current ugc on ugc.user_id = u.user_id
                    where  mq.receiver_id = $this->misstm and mq.blocked_by is null and ugc.status ='login'
                    and u.gender ='M'";
        }else
        {
            $sql="select u.user_id, u.gender from user_data ud join user u  on u.user_id = ud.user_id  join messages_queue mq on mq.sender_id = u.user_id
                       join user_app_status uas on uas.user_id = u.user_id where  mq.receiver_id = $this->misstm and mq.blocked_by is null
                         and uas.android_app ='install' and u.user_id = $target ";
        }

        $stmt=$this->conn_reporting->Prepare($sql);
        $exe=$this->conn_reporting->Execute($stmt,array());
        $i=0;
        while($row=$exe->fetchRow())
        {
            $this->gender = $row['gender'] ;
            $this->user_id = $row['user_id'] ;
            $this->createMessageConvoObj();
            $this->getImageMessage();
            $this->sendImageMessage();
//                $this->getMessage();
//                $this->sendMessage();
            $this->sendPushNotification();
            $i++;
        }
        echo "Messages sent to $i people" ;
        //var_dump($this->message) ;

    }

}


try
{
    if (php_sapi_name() == 'cli')
    {
        $target = $argv[1];
//            $type = $argv[2];
        $message_obj = New NewCouponMessage() ;
        if(isset($target) && $target != null)
            $message_obj->getUsersAndSend($target);
        else
            echo " NO target";
    }

}
catch (Exception $e)
{
    echo $e->getMessage();
}

?>
