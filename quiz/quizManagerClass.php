<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../DBO/quizDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../quiz/questions.php";



/* Admin class to update, delete, view or add a new QUIZ in the DB
 * Author :: @Sumit
 * 🖖 Live Long and Prosper    */

class QuizManager
{
    private $imageurl ;
    private $cdnurl ;

    function __construct()
    {
        global $imageurl, $cdnurl;
        $this->imageurl = $imageurl;
        $this->cdnurl=$cdnurl."/images/quiz/";
    }

    /*returns the list of quizzes */
    public function getQuizList($status = null)
    {
        $quizzes = QuizDBO::getQuizListDBO($status);
//        foreach($quizzes as $key=>$val)
//        {
//            $quizzes[$key]['image'] = $this->cdnurl . $val['image'];
//            $quizzes[$key]['banner'] = $this->cdnurl . $val['banner'];
//        }
        return $quizzes;
    }

    public function getQuizDetails($quiz_id)
    {
        $quiz_info = QuizDBO::QuizExists($quiz_id);
        if(count($quiz_info) == 0)
        {
            return;
        }
        else
        {
            $quiz_info['image'] =  $quiz_info['image'];
            $quiz_info['banner'] = $quiz_info['banner'];
            $questions =  $this->processQuestionsAndAnswers($quiz_id);
            $quiz_info['questions'] = $questions;
            return $quiz_info;
        }
    }

    private function processQuestionsAndAnswers($quiz_id)
    {
        global $imageurl , $cdnurl ;
        $option_url = $cdnurl . "/images/quiz/options/" ;
        $questions = QuizDBO::showQuizQuestions ($quiz_id) ;
        $ques_array = array () ;


        foreach ($questions as $val)
        {
            $Option_arr = array () ;

            $ques_array[$val['question_id']]['question_id'] = $val['question_id'] ;
            $ques_array[$val['question_id']]['question_text'] = $val['question_text'] ;
            $ques_array[$val['question_id']]['rank'] = $val['rank'] ;

            if ($val['question_image'] != null)
            {
                $ques_array[$val['question_id']]['question_image'] = $val['question_image'] ;
            }


            $Option_arr['option_id'] = $val['option_id'];
            $Option_arr['option_text'] = $val['option_text'];

            if( $val['option_image'] != null)
            {
                $Option_arr['option_image'] = $val['option_image'] ;
                $ques_array[$val['question_id']]['option_type'] = QuizQuestions::$typeImage ;
            }
            else
            {
                $ques_array[$val['question_id']]['option_type'] = QuizQuestions::$typeText ;
            }
            $ques_array[$val['question_id']]['options'][] =  $Option_arr ;
        }

        $final_array = array();
        foreach ($ques_array as $val)
        {
            $final_array[] = $val;
        }
        return $final_array ;
    }

    public function saveQuizAttributes($data)
    {
        $quiz_id = null;
        if(isset($data['quiz_id']) && $data['quiz_id'] )
            $quiz_id = $data['quiz_id'];
		$state_id = null ;
		$city_id = null ;
		if($data['state_id'] != 0)
			$state_id = $data['state_id'];
		if($data['city_id'] != 0)
			$city_id = $data['city_id'];
		
        $insert_id =  QuizDBO::saveQuizAttributesDBO($quiz_id,
        											$data['description'], 
        											$data['title'], 
        											$data['display_name'], 
        											$data['image'], 
        											$data['banner'], 
        											$state_id,
        											$city_id);

        if($quiz_id == null)
            return $insert_id ;
        else
            return $quiz_id  ;
    }

    public function updateQuizQuestions($quiz_id, $questions)
    {
        foreach($questions as $key => $question)
        {
            $question_id = null ;
            $insert_id = null;
            if(isset($question['question_id']) && $question['question_id'] != null)
                $question_id = $question['question_id'] ;

            // save the question
            if($question_id != null ||  $question['status'] == 'active')
              $insert_id = QuizDBO::updateQuestionTextDBO($quiz_id, $question_id, $question['question_text'], $question['status'],  $question['rank']) ;

            if($question_id == null)
                $question_id = $insert_id;

            // save the options
            if($question_id != null ||  $question['status'] == 'active')
            QuizDBO::updateQuizOptionsDBO($question_id, $question['options']) ;
        }
    }


    public function updateQuizStatus($quiz_id, $status)
    {
        QuizDBO::updateQuizStatusDBO($quiz_id, $status);
        $currentQuiz = QuizDBO::QuizExists($quiz_id);

        if($status == 'active' || $currentQuiz['status'] ='active')
            $this->UpdateAffectedQuizVersion($currentQuiz['state_id'],$currentQuiz['city_id']);
           //QuizDBO::updateVersionInConfig('quiz');
    }

    private function UpdateAffectedQuizVersion($state_id, $city_id)
    {
        /*TODO :: Get current Max Version
                  Update all concerned versions
                  If global , update global version
                  Update max version
        */
        $userFlags = new UserFlagsDBO();
        $all_versions = $userFlags->read('QUIZ_VERSIONS');
        $current_version = $all_versions['MAX'] ;
        $version_to_set = $current_version+1 ;

        if($state_id == null && $city_id == null)
        {
            // Update global versions as well as all other version in the hash
           foreach($all_versions as $key=>$val)
           {
               $userFlags->write('HASH','QUIZ_VERSIONS',$version_to_set,$key);
           }
        }
        else if ($city_id == null )
        {
            foreach($all_versions as $key=>$val)
            {
                if(strpos($key,$state_id) !== FALSE && strpos($key,$state_id) == 0 )
                {
                    $userFlags->write('HASH','QUIZ_VERSIONS',$version_to_set,$key);
                }
            }
            $userFlags->write('HASH','QUIZ_VERSIONS',$version_to_set,$state_id);
            $userFlags->write('HASH','QUIZ_VERSIONS',$version_to_set,"MAX");
        }
        else
        {
            $current_key= $state_id."_".$city_id ;
            $userFlags->write('HASH','QUIZ_VERSIONS',$version_to_set,$current_key);
            $userFlags->write('HASH','QUIZ_VERSIONS',$version_to_set,"MAX");
        }
    }

}



?>