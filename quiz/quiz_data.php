 <?php 

require_once dirname ( __FILE__ ) . "/../include/config.php"; 
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/questions.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../DBO/quizDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../msg/socket.io-php-emitter/Emitter.php";
require_once dirname ( __FILE__ ) . "/../mobile_utilities/pushNotification.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/datesIcon.php";
require_once dirname ( __FILE__ ) . "/../DBO/curatedDatesDBO.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";




/* This class deals with all the actions pertaining to quizzes
 * Live Long and Prosper.
 * Author :: @Sumit */
class Quizzes
{
	private $user_id;
	private $current_version;
    private $imageurl;
    private $testIds;
    private $quizStatusForUser ; 
    private $quiz_salt;
    private $admin_id ; 
    private $app_version_code ; 
    private $source ;
    private $emitter ;
    private $redis ;
	private $version_for_test ;
	private $userData ;
    static $APP_VERSION_WITH_FLARE = array( 'androidApp' => 107,
    		                                    'iOSApp' => 139,
    		                               'windows_app' => 1305    ); 
  
    
	function __construct($user_id) 
	{  	
		global $imageurl, $cdnurl, $quiz_salt, $admin_id, $redis;
		$this->user_id = $user_id;
		$this->imageurl = $imageurl;
		$this->cdnurl=$cdnurl."/images/quiz/";
		$this->testIds = array(2015);
		$this->version_for_test = (string)intval(microtime(true)/60);
		$this->userData = userUtilsDBO::getUserData(array($this->user_id),true);
		$this->quizStatusForUser = (($redis->sIsMember('curated_deal_users',$this->user_id)) ? 'test' : 'active' );
		$this->current_version =  ($this->quizStatusForUser == 'active')? $this->getCurrentRelevantVersion() : $this->version_for_test;
		$this->quiz_salt = $quiz_salt ;
		$this->admin_id = $admin_id ;
		$this->functionClass =   new functionClass();
		$this->app_version_code = $this->functionClass->getHeader ();
		$this->source = $this->functionClass->getSourceFromHeader ();
		$this->redis = $redis ;
		$this->emitter = new SocketIO\Emitter($this->redis);
	}
    
    /*check Quiz version and returns the list of quizzes */
	public function checkQuizVersion ( $version )
	{	
		$is_new_version = false;
		if ($this->current_version != $version ) 
		{ 
			$quizzes = $this->getCurrentQuizzes($this->quizStatusForUser) ;	
			$is_new_version = true;
		} 
		else
		{	
			 if ($this->quizStatusForUser == 'test')
			 {
			    $quizzes = $this->getCurrentQuizzes($this->quizStatusForUser) ;
			    $is_new_version = true;
			 }
		}
		
		$output["responseCode"] = 200 ;
		$output["current_version"] = $this->current_version; 
		$output['is_new_version'] = $is_new_version;
		$output["quizzes"] = $quizzes ;
		return $output;
	}

	private function getCurrentRelevantVersion()
	{
		$userFlags = new UserFlagsDBO();
		$allVersions= $userFlags->read('QUIZ_VERSIONS');
		//echo "state :: ".$this->userData['stay_state'] . " city:: ".$this->userData['stay_city'];
		$current_key= $this->userData['stay_state'] ."_". $this->userData['stay_city'] ;
		if(isset($allVersions[$current_key]) && $allVersions[$current_key] != null)
			return $allVersions[$current_key] ;
		else if (isset($allVersions[$this->userData['stay_state']]) && $allVersions[$this->userData['stay_state']] != null)
			return $allVersions[$this->userData['stay_state']] ;
		else
			return $allVersions['Global'];
	}
	
	
	/*To get the status of a current quizzes, if  played by a user_id and match id */
	public function getPlayedQuiz ($match_id)
	{ 
	  $quiz_play_status = QuizDBO::userPlayStatusAll($this->user_id, $match_id );

	  $quiz_play_arr = array() ;
		foreach ($quiz_play_status as $val)
	    {   
	    	$quiz_play_arr[$val['quiz_id']]['quiz_id'] =  $val['quiz_id'] ;
	    	if ( $val['user_id'] == $this->user_id )
	    	{
	    		$quiz_play_arr[$val['quiz_id']]['played_by_user'] = true  ;
	    	}
	    	else 
	    	{
	    		$quiz_play_arr[$val['quiz_id']]['played_by_match'] = true  ;
	    	}
	    	
		}
		$pre_final_arr = array();
		$quiz_id_for_score = array();
		foreach ($quiz_play_arr as $val)
		{
			if(!isset($val['played_by_match']))
			$val['played_by_match'] = false ;
			
			if(!isset($val['played_by_user']))
			$val['played_by_user'] = false ;
			
			$val['flare'] = false ;
			
			if($val['played_by_user'] == true && $val['played_by_match'] == true)
			$quiz_id_for_score[]= $val['quiz_id'];
			
			$pre_final_arr[] = $val ;
		}

     if ($quiz_id_for_score != array())  
	  $score_arr = $this->getScoreForQuizzes($match_id, $quiz_id_for_score) ;
	 // print_r(json_encode($score_arr)) ; die ;
	  $final_arr = array();
	  foreach ($pre_final_arr as $value) 
	  {
	  	$temp_arr = array();
	  	$temp_arr = $value ;
	  	
	  	if( $score_arr[$value['quiz_id']]['score'] >= QuizQuestions::$MINIMUM_SCORE_FOR_FLARE) 
	  	{
	  		$temp_arr ['flare'] = true ;
	  	} else 
	  	{	
	  		$temp_arr ['flare'] = false ;
	  	}
	  	$final_arr[] = $temp_arr;
	  }
		
      $output["responseCode"] = 200;
      $output["current_version"] = $this->current_version;
      $output["played_quizzes"] = $final_arr;
      
	  
      return $output ;
		
	}
	
	/*returns quiz questions and options if quiz id is active and user has not taken the quiz already */
	public function getQuestions ( $quiz_id )
	{
           $quiz_attrib = QuizDBO::QuizExists($quiz_id);
           // $question_arr = $this->questionsAndOptions($quiz_id);
	  return $output;
	}
	
	  
	public function saveAnswers($quiz_id, $answers, $match_id)
	{
		$quiz_attrib = QuizDBO::QuizExists( $quiz_id); 
		//var_dump(count($quiz_attrib));
		if (count($quiz_attrib) > 0)
		{
			$output['responseCode'] = 200 ;
			$output['quiz_id'] = $quiz_id ;
			$output['name'] = $quiz_attrib['display_name'];
			$output['image'] =  $this->imageurl . 'quiz/'.$quiz_attrib['banner'] ;
			
			
			$play_status = QuizDBO::userQuizStatus ($quiz_id, array($this->user_id, $match_id));
		
			if ( $play_status[$this->user_id] == null )
			{  
			// save the answers
				$ans_array = array();
				foreach ($answers as $key => $val)
				{
					$ans_array[] = array( $this->user_id, $quiz_id, $key, $val);
				}
				
				QuizDBO::saveAllAnswers( $ans_array ) ;
				QuizDBO::saveUserQuizStatus( $this->user_id, $quiz_id) ;
			}

			$this->sendQuizRefreshEmit($match_id) ; 
			/*if ( $play_status[$match_id] )
			{
		    	$compAsnwers = QuizQuestions::compareAnswers($this->user_id, $match_id, $quiz_id);
		    	$response_type = "common_answers_and_nudge_list";
			}
			else
			{*/
				
			//}
		
				 
		$flare_version = $this->isFlareVersion() ;
		if ($flare_version == true) 
		{   
			//var_dump($play_status [$match_id]) ;
			if ($play_status [$match_id]) 
			{
					$response_type = "common_answers";
					$compareAnswer = QuizQuestions::compareAnswers ( $this->user_id, $match_id, $quiz_id );
					$output ['answers'] = $compareAnswer ['common_answers'];
					$output ['score'] = $compareAnswer ['score'];
					$output ['all_count'] = $compareAnswer ['total_count'] ;
					$output ['match_answer_count'] = $compareAnswer ['match_answer_count'] ;
					$output ['flare'] = $compareAnswer ['flare'] ;
					$output['image'] =  $this->imageurl . 'quiz/'.$quiz_attrib['image'] ;
			}
			
		} else 
		{
			$response_type = "nudge_list";
			$nudge_list = $this->getNudgeList($quiz_id , $match_id, true);
			$output['auto_nudge'] =  false ;
			$output['nudge_list']	= $nudge_list;
		}

		$output['response_type'] =  $response_type;

		}
		else 
		{
		// quiz doesn't exist
		$output['responseCode'] = 403 ;
		}
		return $output ;
	}
	
	public function getCommonAnswers($quiz_id, $match_id)
	{
	       $quiz_attrib = QuizDBO::QuizExists( $quiz_id); 
		    $compareAnswer = QuizQuestions::compareAnswers($this->user_id, $match_id, $quiz_id);

		    $output['responseCode'] = 200 ;
		    $output['current_version'] =  $this->current_version;
	        $output['response_type'] =  'common_answers';
            $output['quiz_id'] = $quiz_id ;
            $output['name'] = $quiz_attrib['display_name'];
            $output['image'] =  $this->imageurl . 'quiz/'.$quiz_attrib['banner'] ;
            $output['answers'] = $compareAnswer ['common_answers'];
            $output['score'] = $compareAnswer ['score_array']; 
		    
		return $output;
	}
	
	
	private function getCurrentQuizzes ($quiz_status)
	{

		$quiz_data = QuizDBO::allQuizzes($this->quizStatusForUser, $this->userData['stay_state'],$this->userData['stay_city'] );
		
		$quiz_array = array();
		foreach ($quiz_data as $value) {
			$value['image'] = $this->imageurl . 'quiz/'.$value['image'];
			$value['banner_url'] = $this->imageurl . 'quiz/'.$value['banner_url'];
			if($value['is_sponsored'] == 'yes')
			{
				$metadata = json_decode($value['sponsored_metadata'],true);
				foreach($metadata as $metakey => $metaval)
				{
					$value[$metakey] = $metaval;
				}
				if($metadata['show_banner_url'] =='yes')
					$value ['sponsored_image'] = $value['banner_url'] ;
			}
			unset($value['sponsored_metadata']);
			$quiz_array[] = $value;
		}
		return $quiz_array;
		
	}
	
	/*to get the nudge list for a user . 
	 * $fullList = false (if only one user is to be nudged)*/
	public function getNudgeList ($quiz_id , $match_id = null, $fullList)
	{
		$uu = new UserUtils();
		global  $dummy_male_image, $dummy_female_image;
	
		if ($fullList == true)
		{
			$userList= QuizDBO::getUserIdForNudgeList($this->user_id, $quiz_id, $match_id);
		}
		else 
		{
			$userList = QuizDBO::getOneUserToNudge ($this->user_id, $quiz_id, $match_id );
		}
	
		$matchIds = array();
	  
		foreach ($userList as $val) 
		{
			$matchIds [] = $val['match_id'] ;
		}
		 
		$nudgeListAttrib = userUtilsDBO::getTileDate($matchIds);
		$nudgeListKey =  array();
		foreach ($nudgeListAttrib as $value)
		{
			$nudgeListKey[$value['user_id']] = $value ;
		}
	   
		$allAttrib = array();
		
		foreach ($userList as $attrib)
		{
		 $temp_arr = array();
		 $temp_arr['match_id'] = $attrib['match_id'];
		 
		 if ($attrib['user_id'] == null)
		 {
		 	$temp_arr['is_played'] = false ;
		 	$message_type = 'NUDGE_SINGLE' ;
		 }
		 else 
		 {
		 	$temp_arr['is_played'] = true ;
		 	$message_type = 'NUDGE_DOUBLE' ; 	
		 }
	 
		 $temp_arr['name'] = ucfirst( $nudgeListKey[$attrib['match_id']]['fname'] );
		 $temp_arr['full_conv_link'] = $uu->generateMessageLink($this->user_id, $attrib['match_id']);
	
		 
		 if ( isset($nudgeListKey[$attrib['match_id']]['thumbnail']))
		 {
		    $temp_arr['profile_pic'] = $this->imageurl.$nudgeListKey[$attrib['match_id']]['thumbnail'];	
		 }
		 else if ( $nudgeListKey[$attrib['match_id']]['gender'] == 'M' ) 
		 {
		 	$temp_arr['profile_pic'] = $dummy_male_image ; 
		 }
		 else 
		 {
		 	$temp_arr['profile_pic'] = $dummy_female_image ; 
		 }
		 
		$temp_arr['checksum'] = $this->createCheckSum( $quiz_id , $attrib['match_id'], $message_type );
		// echo $nudgeListKey[$attrib['match_id']]['status'] ;
		 if ($nudgeListKey[$attrib['match_id']] && $nudgeListKey[$attrib['match_id']]['status'] == 'authentic' && $attrib['match_id'] != $this->admin_id )
		      $allAttrib[] = $temp_arr; 
		 else if ($nudgeListKey[$attrib['match_id']]  == $match_id  && $attrib['match_id'] != $this->admin_id)
		       $allAttrib[] = $temp_arr; 
		}
		 
		$return_array = array_slice($allAttrib, 0 , 20);
	     return $return_array;
	}
	
	private function createCheckSum($quiz_id, $match_id, $message_type) 
	{
		//quiz_id+my_id+match_id+message_type+salt 
	   return md5($quiz_id . $this->user_id . $match_id . $message_type . $this->quiz_salt)	;
	}
	
	public function decideQuizAction ($request)
	{
		$quiz_attrib = QuizDBO::QuizExists( $request ['quiz_id']);
	
     // echo "deciding action for quiz";
      if($request ['from_chat'] == "yes" || $request ['from_chat'] == 1 )
      	$from_chat_temp = true;
	  if( $quiz_attrib['status'] == $this->quizStatusForUser || $quiz_attrib['status'] == 'active' || ($quiz_attrib['status'] =='inactive'  && $from_chat_temp = true ))
	  {  
	  	$output['responseCode'] = 200 ;
        $output['current_version'] =  $this->current_version;
	  	$output ['quiz_id'] = $request ['quiz_id'] ;
        $output ['image'] = $this->imageurl . 'quiz/'.$quiz_attrib['image'] ;
        $output ['name'] = $quiz_attrib['display_name'];
        $output ['description'] = $quiz_attrib['description'];
        $output ['is_sponsored'] = $quiz_attrib['is_sponsored'];
		  if($quiz_attrib['is_sponsored'] == 'yes')
		  {
			  $metadata = json_decode($quiz_attrib['sponsored_metadata'],true);
			  foreach($metadata as $metakey => $metaval)
			  {
				  $output[$metakey] = $metaval;
			  }

			  if($metadata['show_banner_url'] =='yes')
				  $output ['sponsored_image'] = $this->imageurl . 'quiz/'.$quiz_attrib['banner'] ;
		  }

        $play_status = QuizDBO::userQuizStatus ($request ['quiz_id'], array($this->user_id, $request ['match_id']));
			
			if( $play_status[$this->user_id] == null )
			{
				//echo "user has not played the quiz , show the questions";
				$question_arr = QuizQuestions::questionsAndOptions ($request ['quiz_id']);   

            	$output['response_type'] = "questions";
            	$output['questions'] = $question_arr;
			}
			else
			{
				//echo "user played the quiz";
				//$match_play_status = QuizDBO::userQuizStatus ($request ['quiz_id'], array($request ['match_id']));
				 if ($play_status[$request ['match_id']] == null )
				 {
				 	//echo "match has not played the quiz" ; 
				 	
				 	$fullList =  ($request ['from_chat'] == "yes") ? false : true;  
					$nudgeList = $this->getNudgeList($request ['quiz_id'], $request ['match_id'], false );
				 	 
					
	     			$output['response_type'] =  "nudge_list";
	     			$output['auto_nudge'] =  true ;
	     			$output['from_chat'] =  $request ['from_chat'] ;
	     			$output['nudge_list'] = $nudgeList;
				 } 
				 else 
				 {
				 	// both users have played the quiz now compare the answers
				 	$compareAnswer = QuizQuestions::compareAnswers($this->user_id, $request ['match_id'], $request ['quiz_id']);

	        		$output['image'] =  $this->imageurl . 'quiz/'.$quiz_attrib['image'] ;
				 	$output['response_type'] =  'common_answers';
            		$output ['answers'] = $compareAnswer ['common_answers'];
					$output ['score'] = $compareAnswer ['score'];
					$output ['all_count'] = $compareAnswer ['total_count'] ;
					$output ['match_answer_count'] = $compareAnswer ['answer_match_count'] ;
					$output ['flare'] = $compareAnswer ['flare'] ; 
				 }
			}
	  } 
	  else 
	  {
	      // quiz doesn't exist for the user
	  	  	$output['responseCode'] = 403 ;
	  }
			
			return $output;
	}
	
	/**
	 * To check wether this version of app supports showing flare
	 */
	private function isFlareVersion() 
	{
		if ($this->app_version_code < Quizzes::$APP_VERSION_WITH_FLARE [$this->source])
			return false ;
		else 
		    return true ;
	}
	
	public function getPlayedQuizWithFlare ( $request ) 
	{
		global $redis ;
		$user_pair = $this->user_id . "_" . $request ['match_id'] ;
		// Adding hack for ios quiz status bug 
		if (  $this->source =='iOSApp' && $this->app_version_code < 150 && $redis->hGet('ios_quiz_status_sent', $user_pair ) < 2 )
		{
			$quiz_play_status = QuizDBO::userPlayStatusAll ($this->user_id, $request ['match_id']) ;
			$redis->hIncrBy('ios_quiz_status_sent', $user_pair, 1);
			$is_ios_hack = true ;
		} 
		else if (isset($request ['user_tstamp']) && isset($request ['match_tstamp']) )
		{
			$quiz_play_status = QuizDBO::userPlayStatusWithTime($this->user_id , $request ['match_id'], $request ['user_tstamp'] , $request ['match_tstamp']);
		}
		else 
		{
			$quiz_play_status = QuizDBO::userPlayStatusAll ($this->user_id, $request ['match_id']) ;	 
		}
			 
		//var_dump($quiz_play_status);
		//die ;
		$played_by_user = array() ;
		$played_by_match = array() ;
		$user_tstamp = '1970-01-01 00:00:00' ;
		$match_tstamp = '1970-01-01 00:00:00' ; 
     	foreach ($quiz_play_status as $val) 
     	{
     		if ($val['user_id'] == $this->user_id)
     		{	
     			//$played_by_user [] = $val ['quiz_id']  ;
     			if ($val ['timestamp'] > $request ['user_tstamp']   ||  $is_ios_hack == true)
     			   $played_by_user [] = (int)$val ['quiz_id']  ;
     			
     			if ($val ['timestamp'] > $user_tstamp) 
     				$user_tstamp = $val ['timestamp'] ;
     		}
     		else
     		{ 
     			//$played_by_match [] = $val ['quiz_id'] ;
     			if ( $val ['timestamp']  > $request ['match_tstamp']  ||  $is_ios_hack == true)
     		         $played_by_match [] = (int)$val ['quiz_id'] ;
     		   if ($val ['timestamp'] > $match_tstamp) 
     			 	$match_tstamp = $val ['timestamp'] ;
     		}	
     	}
     	//var_dump($played_by_user) ; die ;
     	$played_quizzes = $played_by_match + $played_by_user ;
     	$flare= array() ;
     	if ($played_quizzes != array()) 
     	$flare = $this->getFlareForQuizzes($request ['match_id'], $played_quizzes) ; 
     	
     	$datesIcon = new DealIcon($this->user_id) ;
     	$curatedIcon= array() ;
     	$curatedIcon['icon_shown'] = $datesIcon->is_icon_shown();
     	
     	if ($curatedIcon['icon_shown'] == true)
     	{ 
     		$curatedIcon['icon_clickable'] = $datesIcon->is_icon_clickabe($request ['match_id']);
     	}
     	else
     	{
     		$curatedIcon['icon_clickable'] = false;
     	}
     	$stickerVersion = QuizDBO::getVersionFromconfig('sticker'); 
     	
     	$output["responseCode"] = 200;
     	$output["current_version"] = $this->current_version;
     	$output["sticker_version"] = $stickerVersion;
     	$output["user_tstamp"] = $user_tstamp ;
     	$output["match_tstamp"] = $match_tstamp ;
     	$output["user_quiz"] = $played_by_user ;
     	$output["match_quiz"] = $played_by_match ;
     	$output["flare"] = $flare ;
     	$output["curated_deal_icon"] = $curatedIcon ;
     	
  
     	return $output ;
	}
	
	private function getScoreForQuizzes ($match_id, $quizzes)
	{
		$score = QuizDBO::fetchQuizScore ($this->user_id, $match_id, $quizzes) ;
		
		$quiz_id_without_score = array() ;
		
		foreach ($quizzes as $row)
		{
			if( ! isset($score[$row]))
				$quiz_id_without_score[] = $row ;
		}
		
		if($quiz_id_without_score != array())
		{
			$new_score = QuizQuestions::computeAndStoreScore($this->user_id, $match_id, $quiz_id_without_score) ;
		}

		foreach ($quiz_id_without_score as $val)
		{
			$score[$val['quiz_id']] = $new_score[$val ['quiz_id']] ;
		}
		//print_r(json_encode($score)) ;  ; die ;
		return $score ;
	}
	
	
	private function getFlareForQuizzes ($match_id, $quiz_array)
	{
		//echo "getting flare for quizzes" ;
		$quiz_play_status = QuizDBO::quizPlayStatus ($this->user_id, $match_id, $quiz_array) ;
		$quizzes = array() ;
		foreach ($quiz_play_status as $value) 
		{
			$quizzes[] = $value ['quiz_id'] ;
		}
		if ($quizzes == array())
			return ;
		$score = $this->getScoreForQuizzes($match_id, $quizzes) ;
	 
		$flare = array() ;
		foreach ($score as $key => $row )
		{
			if ($row ['score'] >= QuizQuestions::$MINIMUM_SCORE_FOR_FLARE)
				$flare [] = $key ;
 		}
 		
 		return $flare ; 
	}
	
	private function sendQuizRefreshEmit( $receiver_id)
	{
		$this->emitter->to($receiver_id)->emit('QUIZ_REFRESH' ,
				                          array("sender_id" => $this->user_id ));
		
		$notification = new pushNotification() ;
		$push_data['compatible'] = array('push_type' => 'QUIZ_REFRESH',
			         	   "sender_id" =>  $this->user_id ) ;  
		$notification->notify($receiver_id, $push_data, $this->user_id, 1, array("lower"=> 0, "higher"=> 106), array('androidApp'));  
	}

}

try {
	
	 
   $user = functionClass::getUserDetailsFromSession ();
   $user_id = $user['user_id'];

   if (isset($user_id)){
   	    
      $quizObject  = new Quizzes($user_id);
   	      if($_REQUEST['action'] == 'get_quiz')
   	      {
   	      	//Return all active quizzes and version no";  	
   	      
   	      	     $output = $quizObject->checkQuizVersion($_REQUEST['version']); 
   	      	      
   	      } 
   	      else if ($_REQUEST['action'] == 'get_played_quiz') 
   	      {	
   	      	     	// Get info on which quiz has been played by user and match
   	      	      if (isset($_REQUEST['match_id']))     
   	      	         $output = $quizObject->getPlayedQuiz ( $_REQUEST['match_id']);
   	      	      else 
   	      	         $output["responseCode"] = 403;

   	      } 
   	      else if ($_REQUEST['action'] == 'get_played_quiz_flare')
   	      {
   	      	// Get info on which quiz has been played by user and match
   	      	if (isset($_REQUEST['match_id'])){
   	      		$output = $quizObject->getPlayedQuizWithFlare ( $_REQUEST );
   	      		$newDatesCount['newDatesCount'] = "0";
   	      		if($output["curated_deal_icon"]["icon_clickable"] == true) {
	   	      		if(isset($_REQUEST['lastDealViewedTimeStamp'])){
	   	      			$lastDealTimeStamp = $_REQUEST['lastDealViewedTimeStamp'];
	   	      		} else{
	   	      			$lastDealTimeStamp = "2016-01-01 00:00:00"; //Default Timestamp
	   	      		}
	   	      		$newDatesCount = curatedDatesDBO::getNewDatesFlag($_REQUEST['user_id'],$lastDealTimeStamp);
   	      		}
   	      		$output=array_merge($output,$newDatesCount);
   	      	}else{
   	      			$output["responseCode"] = 403;
   	      	}
   	      
   	      }
   	      /*else if ($_REQUEST['action'] == 'get_questions') 
   	      {
   	      	    if (isset($_REQUEST['quiz_id']))
   	      	       $output = $quizObject->getQuestions($_REQUEST ['quiz_id']);
   	      	    else 
   	      	       $output["responseCode"] = 403;  	   
   	      } 
   	      else if ($_REQUEST['action'] == 'get_common_answers') 
   	      {
   	      	  //"return common answers for a quiz between user id and match id";
   	      	   if( isset($_REQUEST['quiz_id']) && isset($_REQUEST['match_id']))
   	      	   {
   	      	   	   $output = $quizObject->getCommonAnswers($_REQUEST['quiz_id'], $_REQUEST['match_id']);
   	      	   }
   	      	   else
   	      	   {
   	      	   	   $output["responseCode"] = 403;  	  
   	      	   }
   	      	   
   	      }*/ 
   	      else if ($_REQUEST['action'] == 'save_answer') 
   	      {
   	      	   //save the options given by users ";
   	      	   if (isset($_REQUEST['quiz_id']) && isset($_REQUEST['answers']))
   	      	   {
   	      	   		
   	      	   	 $answers = json_decode( $_REQUEST['answers'], true) ;
   	      	   	 $output = $quizObject->saveAnswers($_REQUEST['quiz_id'] , $answers , $_REQUEST['match_id']) ;
   	      	   }
   	      	    
   	      	   else 
   	      	   { 
   	      	   	 // invalid request
   	      	     $output["responseCode"] = 403;
   	      	   }
   	      } 
  		 else if ($_REQUEST['action'] == 'quiz_click') 
   	      {
   	      	   //save the options given by users ";
   	      	   if (isset($_REQUEST['quiz_id']) && isset($_REQUEST['match_id']) ) 
   	      	   {
   	      	   	 $output = $quizObject->decideQuizAction($_REQUEST ) ;
   	      	   }
   	      	    
   	      	   else 
   	      	   { 
   	      	   	 // invalid request :: either quiz_id or match_id was not given
   	      	     $output["responseCode"] = 403;
   	      	   
   	      	   }
   	      } else 
   	      {
   	      	 $output["responseCode"] = 403;
   	      	 $output["error"] = "Unknown Request";
   	      }
   	
   	 
   } else {
      $output["responseCode"] = 401;
   }
	
   	print_r(json_encode($output));
	
} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
}

?>
