<?php 
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../DBO/quizDBO.php";

class QuizQuestions 
{
	public static $typeImage =  "image";
	public static $typeText =  "text";
	public static $typeTextAndImage =  "textAndImage";
	public static $MINIMUM_SCORE_FOR_FLARE = 0.75 ;
									
	 
	public function questionsAndOptions ( $quiz_id)
	{
		global $imageurl , $cdnurl ;
		$option_url = $imageurl . "quiz/" ;
		$questions = QuizDBO::showQuizQuestions ($quiz_id) ;
		$ques_array = array () ;  
		
	
		foreach ($questions as $val) 
		{
		    $Option_arr = array () ;
		    
			$ques_array[$val['question_id']]['question_id'] = $val['question_id'] ;
			$ques_array[$val['question_id']]['question_text'] = $val['question_text'] ;

			if ($val['question_image'] != null)
			{
				$ques_array[$val['question_id']]['question_image'] = $option_url.$val['question_image'] ;
			}
			
			
			$Option_arr['option_id'] = $val['option_id'];
			$Option_arr['option_text'] = $val['option_text'];
			 
			if( $val['option_image'] != null)
			{
				$Option_arr['option_image'] = $option_url.$val['option_image'] ;
				$ques_array[$val['question_id']]['option_type'] = QuizQuestions::$typeImage ;
			}
			else 
			{
				$ques_array[$val['question_id']]['option_type'] = QuizQuestions::$typeText ;
			}
			$ques_array[$val['question_id']]['options'][] =  $Option_arr ;
		}
		
		$final_array = array();
		foreach ($ques_array as $val)
		{
			$final_array[] = $val;
		}
		return $final_array ;
	}
	
	public function compareAnswers ($user_id, $match_id, $quiz_id )
	{
		global $imageurl , $cdnurl ;
		$option_url = $imageurl . "quiz/" ;
		$answers = QuizDBO::getQuestionAndAnswers ($user_id, $match_id, $quiz_id);
		$ans_arr = array();
		
		foreach ($answers as $key => $val)
		{
		   unset($user_answer);
		   unset($match_answer);
		   
		   $ans_arr[$val['question_id']]['question_id'] = $val['question_id'];
		   $ans_arr[$val['question_id']]['question_text'] = $val['question_text'];
		   
		   if ($val['question_image'] != null)
		   $ans_arr[$val['question_id']]['question_image'] = $option_url.$val['question_image'];
		   
		   if ($val['user_id'] == $user_id)
		   {
		   	$user_answer['option_id'] = $val['answer'];
		    $user_answer['text'] = $val['answer_text'];	
		    
		   }
		   else 
		   {
		   	$match_answer['option_id'] = $val['answer'];
		    $match_answer['text'] = $val['answer_text']; 
		   }
		   
		   
		   if (isset ($val['answer_image'] ))
		   {
		   	   $ans_arr[$val['question_id']]['option_type'] = QuizQuestions::$typeImage ;
		   	   if ($val['user_id'] == $user_id)
		   	    $user_answer['image'] = $option_url.$val['answer_image'];
		   	   else 
		   	    $match_answer['image'] = $option_url.$val['answer_image'];    
		   }
		   else
		   {
		   	     $ans_arr[$val['question_id']]['option_type'] = QuizQuestions::$typeText ;
		   }
		   
		   if(isset($user_answer))
		   {
		   		$ans_arr[$val['question_id']]['user_answer'] = $user_answer ;
		   }
		   if (isset($match_answer))
		   {
		   	    $ans_arr[$val['question_id']]['match_answer'] = $match_answer ;
		   }
		}
		$final_arr= array();
		$answer_match_count = 0;
		foreach ($ans_arr as $val) 
		{
			$final_arr[] = $val;
			//var_dump($val); die ;
			if($val['user_answer']['option_id'] == $val['match_answer']['option_id'])
				$answer_match_count++ ;
		}
		$all_count = count($ans_arr) ;
		$score= $answer_match_count / $all_count ;
		if($score >= QuizQuestions::$MINIMUM_SCORE_FOR_FLARE)
			$flare = true ;
		else 
			$flare = false ;
		
		
	
		 
		 return array("common_answers" => $final_arr,
				         "total_count" => $all_count,
				  "answer_match_count" => $answer_match_count,
				               "score" => $score,
				               "flare" => $flare	);
		//return $final_arr; 
	}
	
	
	
	public function compareOptionsOnly ($user_id, $match_id, $quiz_id )
	{
		$answers = QuizDBO::getQuestionAndAnswersIds ($user_id, $match_id, $quiz_id);
		$ans_arr = array();
	
		foreach ($answers as $key => $val)
		{
			unset($user_answer);
			unset($match_answer);
			 
			$ans_arr[$val['question_id']]['question_id'] = $val['question_id'];
				 
				if ($val['user_id'] == $user_id)
				{
					$user_answer = $val['answer'];	
				}
				else
				{
					$match_answer = $val['answer'];
				}
				 
				if(isset($user_answer))
				{
					$ans_arr[$val['question_id']]['user_answer'] = $user_answer ;
				}
				if (isset($match_answer))
				{
					$ans_arr[$val['question_id']]['match_answer'] = $match_answer ;
				}
		}
		$final_arr= array();
	
		foreach ($ans_arr as $val)
		{
			$final_arr[] = $val;
		}
		return $final_arr;
	}
	
	
	

	/**
	 * Checks the quiz score between two users for a quiz from table user_quiz-score
	 * if quiz score is not present then calculate and add it to table user_quiz_score
	 */
	public function computeAndStoreScore ($user_id, $match_id, $quiz_array )
	{
		
		if($user_id < $match_id)
		{
			$user1 = $user_id ;
			$user2 = $match_id ;
		} else 
		{
			$user1 = $match_id ;
			$user2 = $user_id	;
		}
		
		$quiz_score = array();
		$quiz_score_new = array();
		foreach ($quiz_array as $quiz_id)
		{
			$answers = QuizQuestions::compareOptionsOnly($user1, $user2, $quiz_id) ;
			$total_count = count($answers);
			$answer_match_count = 0 ;
			foreach ($answers as $val)    
			{
				if ($val['user_answer'] == $val['match_answer']) 
				{
					//echo "answer matched ";
					$answer_match_count++;     
				}
			} 
			//echo $answer_match_count ;
			$score = $answer_match_count / $total_count ;
			//echo $user1 . " " . $user2 . " " . $quiz_id .  " " . $total_count . " " . $answer_match_count . " " . $score . "\n" ;
			$quiz_score[$quiz_id] = array($user1, $user2, $quiz_id, $total_count, $answer_match_count, $score) ;
			$quiz_score_new[$quiz_id] = array ( "quiz_id" => $quiz_id,
					                            "score" => $score ) ;
					

 		}
 		// var_dump($quiz_score) ;die ;
 		QuizDBO::storeMatchScore($quiz_score) ;
 		
 		return $quiz_score_new;
 		
	}
	
}

?>