<?php 

require_once dirname(__FILE__).'/../include/config.php';
require_once dirname ( __FILE__ ) . "/../logging/EventTrackingClass.php";


class SendMessage {
	
	private $key;
	private $sender_id;
	private $url;
	private $phn;
	private $msg;
	private $activity;
	private $event;
	
	function __construct() {
		global $infibuzz_url,$infibuzz_api_key;
		$this->url=$infibuzz_url;
		$this->key=$infibuzz_api_key;
		$this->sender_id='TRUMAD';
		$this->phn=NULL;
		$this->msg=NULL;
	}
	
	function sendSMS($phn,$msg,$activity=null,$event='sms') {
		$this->phn=$phn;
		$this->msg=urlencode($msg);
		$this->activity=$activity;
		$this->event=$event;
		$time1=time();
		//var_dump($this->phn);
		//var_dump($this->msg);
		
		$this->url=$this->url."api/v3/index.php?method=sms&api_key=$this->key&to=$this->phn&sender=$this->sender_id&message=$this->msg";
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$response = curl_exec($ch);
		curl_close($ch);
		
		$response=json_decode($response,true);
		//var_dump($response);
		if($response['status']=='OK')
		{   
		$return['responseCode']=200;
		$return['status']= 'success';
		$time2=time();
		$diff=$time2-$time1;
		$this->logAction($this->event, "success", $diff,$response['message']);
		}
		else
		{
			$return['responseCode']=403;
			$return['status']= 'fail';
			$return['call_me']=TRUE;
			$return['error'] = 'SMS sending failed!';
			$time2=time();
			$diff=$time2-$time1;
			$this->logAction($this->event, "failure", $diff,$response['message']);
			trigger_error( "PHP Fatal error SMS sending failed :".$response['message'], E_USER_WARNING);
				
		}
		return $return;
	}
	
	private function logAction($event,$status,$diff,$event_info='success') {
		$Trk = new EventTrackingClass();
		$data_logging = array();
		$data_logging [] = array("activity" => $this->activity , "event_type" => $event, "event_status" => $status, "event_info" => $event_info, "time_taken"=>$diff );
		//var_dump($data_logging);
		$Trk->logActions($data_logging);
	}
}


?>