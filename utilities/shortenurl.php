<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";



/**
 * API deals with all the functions to return only shorten urls, may it be for any purpose
 * current implementation includes conversion of link for conversation list only
 * @author himanshu
 */

class ShortenUrl{

	private $user_id;
	private $user_utils;

	function __construct($user_id){
		$this->user_id = $user_id;
		$this->user_utils = new UserUtils();
	}


	/**
	 * check if the call is authentic when made from conversation list page
	 * @param unknown_type $data
	 */
	private function _checkDataAuthenticityForConversationLink ($data)
	{
		$authentic_data = false;

		$sql = "SELECT count(*) as count from user where user_id = ? and gender = ? and status = ?";
		$rows = Query::SELECT($sql, array($this->user_id, 'F', 'authentic'), Query::$__slave, true);

		if ($rows ['count'] > 0 )
		{

			$mluser1 = ($data['match_id'] > $this->user_id)? $this->user_id:$data['match_id'];
			$mluser2 = ($data['match_id'] < $this->user_id)? $this->user_id:$data['match_id'];

			$mlSql = "SELECT count(*) as count from user_mutual_like where user1 = ? and user2 = ? and blocked_by is null ";
			$mlRows = Query::SELECT($mlSql,array($mluser1, $mluser2),  Query::$__slave, true);

			if ($mlRows ['count'] > 0 )
			{
				$authentic_data	= true;
			}

		}

		return $authentic_data;
	}


	/**
	 * generate shortened url for conversation list share feature
	 * @param unknown_type $request_data
	 */
	public function getShortenedUrlForConversationList ($request_data)
	{
		$output = array ();
		$isAuthenticCall = $this->_checkDataAuthenticityForConversationLink($request_data);
		if ($isAuthenticCall == true)
		{
			$output ['response'] = "OK";
			$match_id = $request_data ['match_id'];
			$link = $this->user_utils->generateShareProfileLinks(array($match_id), $request_data['source']);
			$google_response_encoded = Utils::getShortenedUrlFromGoogleAPI($link[$match_id], $this->user_id);
			$google_result = json_decode($google_response_encoded, true);
			if(!isset($google_result['error']))
			{
				$output ['link']= $google_result ['id'];
			}
			else
			{
				$output ['link']= $link [$this->user_id];
			}
		}
		else
		{
			$output ['response'] = "ERROR";
		}

		return $output;
	}
}

try
{
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$shortnerObj = new ShortenUrl ($user_id);
	$output = array();

	if (isset($user_id))
	{
		if ($_REQUEST['shorten_url_conversationlist'] == 1)
		{
			if (isset($_REQUEST['match_id']))
			{
				$result = $shortnerObj->getShortenedUrlForConversationList ($_REQUEST);
				if ($result ['response'] == "ERROR")
				{
					$output ['responseCode'] = 403;
					$output ['error'] = "UNAUTHORIZED_CALL";
				}
				else
				{
					$output ['response'] = array( "link" => $result ['link'],
					 							  "share_message" => "Hey! Lemme know what you think of this guy I met on TrulyMadly",
                                                  "alert_message" => "Ask your besties about what they think of him. Shh... This feature is only for girls!");
					$output ['responseCode'] = 200;
					   
				}
			}
			else
			{
				$output ['responseCode'] = 403;
				$output ['error'] = "UNAUTHORIZED_CALL";
			}
		}
		else
		{
			$output ['responseCode'] = 404;
			$output ['error'] = "NOT_FOUND";
		}
	}
	else
	{
		$output ['responseCode'] = 401;
		$output ['error'] = "BAD_REQUEST";
	}

	echo json_encode ($output);
}
catch (Exception $e)
{
	trigger_error("PHP WEB: ".$e->getMessage() . " : " . $e->getTraceAsString(), E_USER_WARNING);
}

?>
