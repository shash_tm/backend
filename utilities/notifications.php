<?php 

require_once dirname ( __FILE__ ) . "/counter.php";

/**
 * class to return every query regarding notification (may it be counter or checking sanity of content being displayed or any other confirmation from mobile side)
 * @author himanshu
 *
 */




try
{
	
	
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	if(isset($user_id)==false)return;
	$Counters=new Counters();
	$unread_counters=$Counters->getConversationCounter ($user_id);
	
	$response = array();
	$response ['responseCode'] = 200;
	$response['counters']=$unread_counters;
	
	
	echo json_encode ($response);
	
}
catch (Exception $e)
{
	echo json_encode(array("responseCode"=>403));
	trigger_error("PHP WEB: Error: " . $e->getMessage() . "  trace: " .$e->getTraceAsString(), E_USER_WARNING);
}
?>
