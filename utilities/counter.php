<?php 

require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";

/**
 * class to return every query regarding notification (may it be counter or checking sanity of content being displayed or any other confirmation from mobile side)
 * @author shashwat
 *
 */

class Counters
{
	
	private $redis;
	
	function __construct()
	{
		global $redis;
		$this->redis = $redis;
	}
	
	public function addMutualLikeCounter($user1,$user2){
		$mutualLikeKey = Utils::$redis_keys['mutual_like_notification']. $user1;
		$mutualLikeMatchKey = Utils::$redis_keys['mutual_like_notification']. $user2;
		//$mutualLikesListKey = Utils::$redis_keys['mutual_like_only_list']. $user1;
		//$mutualLikesMatchListKey = Utils::$redis_keys['mutual_like_only_list']. $user2;
		$messagesListKey = Utils::$redis_keys['messages']. $user1;
		
		$conversation_list_union_user1=Utils::$redis_keys['conversation_list_union']. $user1;
		$conversation_list_union_user2=Utils::$redis_keys['conversation_list_union']. $user2;
		//$messagesMatchListKey = Utils::$redis_keys['messages']. $user2;
		
		
		/**
		 * maintaining queues for supporting old and new version
		 * adding user id in message q
		 * adding user id in mutual like q
		 * inc counter for mutual like
		 */
		$this->redis->INCR($mutualLikeKey);
		$this->redis->INCR($mutualLikeMatchKey);
		//$this->redis->SADD($mutualLikesListKey, $user2);
		$this->redis->SADD($conversation_list_union_user1, $user2);
		$this->redis->SADD($conversation_list_union_user2, $user1);
		
		//$this->redis->SADD($mutualLikesMatchListKey, $user1);
		//$this->redis->SADD($messagesListKey, $user2);
		//$this->redis->SADD($messagesMatchListKey, $user1);
		$count_user1 = intval($this->redis->GET($mutualLikeKey));
		$count_user2 = intval($this->redis->GET($mutualLikeMatchKey));
		
		$unread_message_count = intval($this->redis->SCARD($messagesListKey));
		$conversation_count=intval($this->redis->SCARD($this->getConversationUnionKey($user1)));
		return array("user2_mutual_like_count"=>$count_user2,"mutualLike"=>$count_user1,"conversation_count"=>$conversation_count);
	}

	private function getMutualLikeKey($user_id){
		return 	Utils::$redis_keys['mutual_like_notification'].$user_id;
	}
	
	private function getConversationUnionKey($user_id){
		return 	Utils::$redis_keys['conversation_list_union'].$user_id;
	}
	
	private function getMessageUnreadKey($user_id){
		return 	Utils::$redis_keys['messages'].$user_id;
	}
	
	private function getMutualListSetKey($user_id){
		return 	Utils::$redis_keys['mutual_like_only_list'].$user_id;
	}
	
	public function clearMutualLikeCounter($user_id){
		$mutual_like_key=$this->getMutualLikeKey($user_id);
		$this->redis->DEL($mutual_like_key);
	}
	
	
	public function clearUnreadCount($user1,$user2){
		$flag1=$this->redis->SREM($this->getMessageUnreadKey($user1) , $user2);
		$flag2=$this->redis->SREM($this->getConversationUnionKey($user1) , $user2);
		return $flag1||$flag2;
		//$this->redis->SREM($this->getMutualListSetKey($user1) , $user2);
		//}	
	}
	
	public function markUnread($user1,$user2){
		$this->redis->SADD($this->getConversationUnionKey($user1) , $user2);
		$this->redis->SADD($this->getMessageUnreadKey($user1) , $user2);
		//$this->redis->SREM($this->getMutualListSetKey($user1) , $user2);
		//}
	}
	
	
	
	public function getConversationCounter ($user_id)
	{
		$mutualLikeKey = Utils::$redis_keys['mutual_like_notification']. $user_id;
		$messagesListKey = Utils::$redis_keys['messages']. $user_id;
		
		$mutual_like_counter = intval($this->redis->GET($mutualLikeKey));
		$unread_message_count = intval($this->redis->SCARD($messagesListKey));
//		$conversation_count=intval($this->redis->SCARD($this->getConversationUnionKey($user_id)));
		$unread_messaages = $this->redis->SMEMBERS($this->getConversationUnionKey($user_id));
		$conversation_count= count($unread_messaages);
		
		return array("mutual_like_count"=>$mutual_like_counter,
				    "conversation_count"=>$conversation_count,
				         "message_count"=>$unread_message_count,
        				"unread_messages"=>$unread_messaages		);
	}
}



?>
