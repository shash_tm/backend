<?php 
include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/validate.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
include_once (dirname ( __FILE__ ) . '/include/User.php');
include_once (dirname ( __FILE__ ) . '/fb.php');
include_once (dirname ( __FILE__ ) . '/TrustBuilder.class.php');
include_once (dirname ( __FILE__ ) . '/EventTracking.php');



class login{
	
	private $user;
	private $func;
	function __construct(){
		$this->user = new User();
		$this->func = new functionClass();
	}
	
	private function startSession(){
		session_start();
	}
	function registerViaEmail($data){
		
		$object = new Validation();
		$data['email'] = trim($data['email']);
		$data['fname'] = ucfirst(trim($data['fname']));
		$data['lname'] = ucfirst(trim($data['lname']));
		$error['email']=$object->validateEmail($data['email']);
		$error['password']=$object->validateText($data['password']);
		$error['fname']=$object->validateText($data['fname']);
		$error['lname']=$object->validateText($data['lname']);
		 
		if(!($data['gender']=='M' or $data['gender']=='F'))
			$error['gender']=true;
		if($error['email'] || $error['password'] || $error['fname'] ||  $error['lname']|| $error['gender'])
			$error['register']=true;
		if($error['register']){
			$response['responseCode'] = 403;
			$response['error'] = "Sorry Error in Registration data,try again";
		}else{
			$presence = $this->user->checkEmail($data['email']);
			switch($presence){
				case 'FB_USER':
					$response['responseCode'] = 403;
					$response['error'] = "Please login via facebook";
					break;
				case 'USER_EXISTS':
					$response['responseCode'] = 403;
					$response['error'] = "Email Id already exists";
					break;
				case 'USER_ABSENT':
					$this->startSession();
					$this->user->createNewUser($data);
					$this->func->setSession('NOT_FB_USER',$data['email']);
					$response['responseCode'] = 200;
					break;
			}
		}
		print_r(json_encode($response));die;
	}
	
	function registerViaFB($data){
		global $response;
		$token=$data['token'];
		try{
			$object=new fb($token);
			//$object->getData();
			$object->getBasicData();
			//$object->logFacebook(NULL);
			//$trustBuilder = new TrustBuilder(NULL);
		}
		catch(Exception $e) {
			trigger_error("Facebook:could not create facebook object:".$e->getMessage(),E_USER_WARNING);
			$response['responseCode'] = 403;
			$response['error'] = $e->getMessage();//"Sorry,You can not proceed with this action due to internal server problem";
			print_r(json_encode($response));die;
		}
		try {
			$prescence=$object->checkFbPrescence();
			if($prescence=='NEW_USER') {
				$object->getData();
				$trustBuilder = new TrustBuilder(NULL);
				$verify = $trustBuilder->authoriseFBAccount($object->data,true);
				if(!$verify['flag']){
					$object->logFacebook(NULL, $verify['message']);
					throw new Exception($verify['message']);
				}
				$user_id = $trustBuilder->CreateUserForFacebook($object->data);
				$this->startSession();
				$this->func->setSession('FB_USER',$object->data['id']);
				$object->storeFacebook($user_id,"NEW","login");
				$eventTrack = new EventTracking($user_id);
				$eventTrack->logAction("SignUp_facebook");
				$trustBuilder->setUserId($user_id);
				/**
				 * commenting the update trust score of facebook and credit assigning
				 * we'll be awarding the same once registeration is complete
				 * on the other hand we do log data from fb at this moment
				 * @Himanshu
				 */
				//$trustBuilder->creditPoint($tbNumbers['credits']['facebook'],'facebook',$tbNumbers['validity']['facebook']);
				//$trustBuilder->updateFacebookTrustScore($object->data['connections']);
				$response['responseCode'] = 200;
			}else if($prescence=='CONNECTED_VIA_EMAIL'){
					$response['responseCode'] = 403;
					$response['error'] = "Please login with email id";	
			}
			else {
				$user_id=$prescence;
				$object->storeFacebook($user_id,"USER_EXISTS");
				$this->startSession();
				$this->func->setSession('FB_USER',$object->data['id']);
				$eventTrack = new EventTracking($user_id);
				$eventTrack->logAction("login_facebook");
				$response['responseCode'] = 200;
			}
			print_r(json_encode($response));die;
		}catch(Exception $e) {
				$response['responseCode'] = 403;
				$response['error'] = $e->getMessage();//"Sorry,You can not proceed with this action due to internal server problem";
				print_r(json_encode($response));die;
		}	
	}
	
	function loginUser($data){
		$object = new Validation();
		$data['email'] = trim($data['email']);
		$error['email']=$object->validateText($data['email']);
		$error['password']=$object->validateText($data['password']);
		if(!$error['email']&&!$error['password']) {
			$presence = $this->user->checkEmail($data['email'],$data['password']);
			switch($presence){
				case 'USER_EXISTS':
					$this->startSession();
					$func->setSession('NOT_FB_USER',$data['email']);
					if(isset($_SESSION['user_id'])){
						$user_id = $_SESSION['user_id'];
						$eventTrack = new EventTracking($user_id);
						if($login_mobile){
							$eventTrack->logAction("login_APP_ID",0,null,null,null,null);
							/*	$sql=$conn->Prepare("insert into user_mobile_gcm_id(user_id,registration_id,tstamp) values(?,?,now()) on duplicate key update
							 user_id=?,tstamp=now()");
							$conn->Execute($sql,array($user_id,$_REQUEST['registration_id'],$user_id));
							*/
						}else
							$eventTrack->logAction("login");
						
						$user_details = $this->func->getUserDetailsFromSession();
					}
					$response['responseCode'] = 200;
					$response['data'] = $user_details['status'];
					break;
				default :
					$response['responseCode'] = 403;
					$response['error'] = "In correct Email Id or password";	
					break;
			}
		}else if($error['email']){
			$response['responseCode'] = 403;
			$response['error'] = "Email Id is not correct";
		}
		print_r(json_encode($response));die;
	}
	
	
	function getUser(){
		$session = $this->func->getUserDetailsFromSession();
		//var_dump($session);
		if(isset($session['user_id'])){
			$userId = $session['user_id'];
			//$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
			//$rs = $conn->Execute ( "select status,is_fb_connected from user where user_id=$userId" );
			$result = $this->user->getUserformenu($userId);//$rs->GetRows ();
			$response['responseCode'] = 200;
			$response['data'] = $result;
			print_r(json_encode($response));
			exit;
		}else{
			$response['responseCode'] = 401;
			print_r(json_encode($response));
			exit;
		}
	}
}



try{
	$func=new functionClass();
	if(!$_REQUEST['login_mobile'])
		functionClass::redirect('index');
	
	//$response = '';
	$login = new login();
	
	//$user_details = $this->user->getUserDetailsFromSession();
	
	$data = $_POST;
	
	
	if($_REQUEST['login_mobile'] && $_REQUEST['getuserdata']){
		$login->getUser();
	}
	
	if($data['registerViaEmail']){
		$login->registerViaEmail($data);
	}
	if($data['from_fb']){
		$login->registerViaFB($data);
	}
	if($data['login']){
		$login->loginUser($data);
	}
	
}catch(Exception $e){
	trigger_error($e->getMessage(),E_USER_WARNING);
}

if(!$_REQUEST['login_mobile'])
	$smarty->display("templates/index_new.tpl");
?>