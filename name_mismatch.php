<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute("select user_id,connection from user_facebook where user_id in (18917,19204,23142,24238,24291,24322,24673,24982,25627,27182,27699,27746,27939,29369,29372,29768,30131,30141,30565,30617,31097,31621,31908,32720,33656,33748,34263,34459,34849,34893,35587,35664,35716,36295,36622,37173,37246)");
	$data = $sql->GetRows();
	
	foreach ($data as $key=>$value){
		
		$conn->Execute($conn->prepare("update user_trust_score SET fb_connections=?,fb_credits=0 where user_id=? "),
				array($data[$key]['connection'],$data[$key]['user_id']));
	}
	
	$conn->Execute("update user_facebook SET mismatch=NULL where user_id in (18917,19204,23142,24238,24291,24322,24673,24982,25627,27182,27699,27746,27939,29369,29372,29768,30131,30141,30565,30617,31097,31621,31908,32720,33656,33748,34263,34459,34849,34893,35587,35664,35716,36295,36622,37173,37246)");
	
	$conn->Execute("update user SET status='authentic' where user_id in (18917,19204,23142,24238,24291,24322,24673,24982,25627,27182,27699,27746,27939,29369,29372,29768,30131,30141,30565,30617,31097,31621,31908,32720,33656,33748,34263,34459,34849,34893,35587,35664,35716,36295,36622,37173,37246)");
	
	$conn->Execute("update user_trust_score SET trust_score=trust_score+30 where user_id in (18917,19204,23142,24238,24291,24322,24673,24982,25627,27182,27699,27746,27939,29369,29372,29768,30131,30141,30565,30617,31097,31621,31908,32720,33656,33748,34263,34459,34849,34893,35587,35664,35716,36295,36622,37173,37246)");
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}
?>