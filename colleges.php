<?php
include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/allValues.php');
global $conn;
$data=$_GET;

if($data['institute']) {
try {
$rs=$conn->Execute($conn->prepare("select name from colleges where name regexp ? limit 20"),array('[[:<:]]'.$data['value']));
}
catch(Exception $e) {
echo $e->getMessage();
die;

}
print_r(json_encode($rs->GetRows()));

}

else if($data['specialisation']) {
try {
$rs=$conn->Execute($conn->prepare("select name from courses_main where education=? and name regexp ? limit 20"),array($data['education'],'[[:<:]]'.$data['value']));
}
catch(Exception $e) {
echo $e->getMessage();
die;

}
print_r(json_encode($rs->GetRows()));
}

else if($data['designation']) {
try {
$rs=$conn->Execute($conn->prepare("select name from designations where name regexp ? limit 20"),array('[[:<:]]'.$data['value']));
}
catch(Exception $e) {
echo $e->getMessage();
die;

}
print_r(json_encode($rs->GetRows()));



}
?>
