<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../EventTracking.php";

$user = functionClass::getUserDetailsFromSession ();
$event = new EventTracking($user['user_id']);

if(isset($_REQUEST['action']) && $_REQUEST['action'] == "iframe_load_time"){
	$info = "iframe Loading time Logging";
	$event->logAction("payments-iframe",($_REQUEST['time']/1000), null, null,  null, $info );
}

if(isset($_REQUEST['error_type']) && $_REQUEST['error_type']=="iframe_error"){

	$info = "Error: loading iframe";
	$event->logAction("payments-iframe",null, null, null,  null, $info );
}
?>