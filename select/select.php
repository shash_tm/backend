<?php
require_once dirname ( __FILE__ ) . "/Select.class.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../quiz/questions.php";

$user = functionClass::getUserDetailsFromSession ();
$user_id = $user['user_id'];
global $redis;
try {

    if (isset($user_id))
    {
        $select = new Select($user_id) ;
        $action = $_REQUEST["action"];
        $output = array();
        switch($action){
            case 'get_quiz':
                $quizObject = new QuizQuestions();
                $quiz_id = $select->getSelectQuizQuestionId();
                $output["questions"] = $quizObject->questionsAndOptions($quiz_id);
                $output["quiz_id"] = $quiz_id;
                $output["responseCode"] = 200;
                break;

            case 'submit_quiz':
                $answers = $_POST['answers'];
                $output = $select->saveAnswers($select->getSelectQuizQuestionId(),$answers);
                $output["responseCode"] = 200;
                break;

            case 'my_data':
                $output = $select->getMySelectData();
                $output["responseCode"] = 200;
                break;

            case 'delete_select':
                $output = $select->deleteSelectMemberShip();
                $output["responseCode"] = 200;
                break;

            case 'compare_quiz':
                $match_id=$_POST['match_id'];
                $output = $select->compareQuiz($match_id);
                $output["responseCode"] = 200;
                $output["match_id"] = $match_id;
                break;

            case 'start_free_trial':
                $output = $select->startFreeTrial();
                $output['my_select'] = $select->getMySelectData();
                break;

            default:
                $output["responseCode"] = 403;
                $output["error"] = "UnknownRequest";
        }

    } else
    {
        $output["responseCode"] = 401;
    }



    print_r(json_encode($output));

} catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>