<?php
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

/*
 * Non-select members were hiding promo-select profiles due to which 
 *  1. Bucket of select members were falling
 *  2. When these non-select converted to Select, we didn't have enough Select ptofiles to show to them.
 * The issue has been fixed now i.e. such a hide is no longer registered. The class is used to clean up this error from the past.
 * This will remove the hides from the user_pi_activity table
 */

class nonSelect2SelectHidesReduceFromPiActivityTable {
    public static function reduceHidesFromUserHideIgnored($genderString) {
        global $conn_reporting;
        $bugFixTimeStamp = " '2017-01-16 10:32:33' ";
        $query  = "select t.user2 as user_id, count(*) as hidesReceived from "
                . "(select * from user_hide_ignored where timestamp < ".$bugFixTimeStamp.") t "
                . "join user_subscription us2 on t.user2 = us2.user_id "
                . "left join user_subscription us1 on t.user1 = us1.user_id "
                . "join user_data ud on t.user1 = ud.user_id "
                . "join user u on t.user1 = u.user_id "
                . "where ud.stay_country = '113' and u.gender = ".$genderString." and t.timestamp > us2.tstamp and (t.timestamp < us1.tstamp or us1.tstamp is null) group by user_id ";
        print("Read Mode : Clean User Hide Ignored :".$query);
        $tuples = $conn_reporting->Execute($query);
        $i = 0;
        while ($tuple = $tuples->FetchRow()) {
            $i = $i + 1;
            $user_id = $tuple['user_id'];
            $hidesReceived = $tuple['hidesReceived'];
            nonSelect2SelectHidesReduceFromPiActivityTable::reduceHidesForUser($user_id,$hidesReceived);
            if($i % 100 == 0) {
                sleep(1);
            }
        }
    }
    
    public static function reduceHidesFromUserHide($genderString) {
        global $conn_reporting;
        # Identify the tuples (user1,user2) that we want to delete. The conditions are : Non-select (Indians) to Select hides in the syste,
        $selectStartDate = " '2016-11-16 01:15:24' ";
        $query  = "select t.user2 as user_id, count(*) as hidesReceived from "
                . "(select * from user_hide where timestamp >".$selectStartDate.") t "
                . "join user_subscription us2 on t.user2 = us2.user_id "
                . "left join user_subscription us1 on t.user1 = us1.user_id "
                . "join user_data ud on t.user1 = ud.user_id "
                . "join user u on t.user1 = u.user_id "
                . "where ud.stay_country = '113' and u.gender = ".$genderString." and t.timestamp > us2.tstamp and (t.timestamp < us1.tstamp or us1.tstamp is null) group by user_id ";
        print("Read Mode : Clean User Hide : ".$query);
        $tuples = $conn_reporting->Execute($query);
        $i = 0;
        while ($tuple = $tuples->FetchRow()) {
            $i = $i + 1;
            $user_id = $tuple['user_id'];
            $hidesReceived = $tuple['hidesReceived'];
            nonSelect2SelectHidesReduceFromPiActivityTable::reduceHidesForUser($user_id,$hidesReceived);
            if($i % 100 == 0) {
                sleep(1);
            }
        }
    }
    
    public static function reduceHidesForUser($user_id,$hidesReceived) {
        global $conn_master;
        $query = "update user_pi_activity set total_hides = total_hides - ? where user_id = ?";
        print("Query used for updation : ".$query);
        $sql_prep =$conn_master->Prepare($query);
        $sql_exec = $conn_master->Execute($sql_prep, array($hidesReceived,$user_id));
    }
}

try {
    echo "Starting";
    if ( php_sapi_name() == 'cli' ){
        nonSelect2SelectHidesReduceFromPiActivityTable::reduceHidesFromUserHide(" 'M' ");
        nonSelect2SelectHidesReduceFromPiActivityTable::reduceHidesFromUserHideIgnored(" 'M' ");
        nonSelect2SelectHidesReduceFromPiActivityTable::reduceHidesFromUserHide(" 'F' ");
        nonSelect2SelectHidesReduceFromPiActivityTable::reduceHidesFromUserHideIgnored(" 'F' ");
    }
} catch (Exception $e) {
    echo $e->getTraceAsString();
    trigger_error($e->getTraceAsString(), E_USER_WARNING);
    trigger_error($e->getMessage(), E_USER_WARNING);
}
?>