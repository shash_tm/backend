<?php

require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

/*
 * Non-select members were hiding promo-select profiles due to which 
 *  1. Bucket of select members were falling
 *  2. When these non-select converted to Select, we didn't have enough Select ptofiles to show to them.
 * The issue has been fixed now i.e. such a hide is no longer registered. The class is used to clean up this error from the past.
 */
class nonSelect2SelectHidesCleanUp {
    
    public static function cleanTable() {
        global $conn_reporting;
        # Identify the tuples (user1,user2) that we want to delete. The conditions are : Non-select (Indians) to Select hides in the syste,
        $selectStartDate = " '2016-11-16' ";
        $batchUpdateSize = 100;
        $query  = "select t.user1 as user1,t.user2 as user2,t.timestamp as ts from "
                . "(select * from user_hide where timestamp >".$selectStartDate.") t "
                . "join user_subscription us2 on t.user2 = us2.user_id "
                . "join user_subscription us1 on t.user1 = us1.user_id "
                . "join user_data ud on t.user1 = ud.user_id "
                . "join user u on t.user1 = u.user_id "
                . "where ud.stay_country = '113' and u.gender = 'M' and t.timestamp > us2.tstamp and (t.timestamp < us1.tstamp)";
        $tuples = $conn_reporting->Execute($query);
        $counter = 0;
        $batchStringForDelete = "";
        $batchStringForInsertion = "";
        $updateCounter = 0;
        while ($tuple = $tuples->FetchRow()) {
            $batchStringForDelete .= ($counter>0?",(":"(").$tuple['user1'].",".$tuple['user2'].")";
            $batchStringForInsertion .= ($counter>0?",(":"(").$tuple['user1'].",".$tuple['user2'].",'".$tuple['ts']."')";
            if($counter < $batchUpdateSize) {
                $counter += 1;
            } else {
                $updateCounter += 1;
                nonSelect2SelectHidesCleanUp::insertTuplesToIgnoredHidesTable($batchStringForInsertion);
                nonSelect2SelectHidesCleanUp::deleteTuplesFromHideTable($batchStringForDelete);
                print("Deleted the batch number:".$updateCounter);
                $counter = 0;
                $batchStringForDelete = "";
                $batchStringForInsertion = "";
            }
        }
        if(strlen($batchStringForDelete) > 0 ){
            nonSelect2SelectHidesCleanUp::insertTuplesToIgnoredHidesTable($batchStringForInsertion);
            nonSelect2SelectHidesCleanUp::deleteTuplesFromHideTable($batchStringForDelete);
            print("Deleted the last batch number:".$updateCounter);
        }
        
    }
    
    public static function deleteTuplesFromHideTable($batchStringForDelete) {
        global $conn_master;
        $query = "delete from user_hide where (user1,user2) in (".$batchStringForDelete.")";
        print("Query used for deletion : ".$query);
        $sql_prep =$conn_master->Prepare($query);
        $sql_exec = $conn_master->Execute($sql_prep, array());
    }
    
    public static function insertTuplesToIgnoredHidesTable($batchStringForInsertion) {
        global $conn_master;
        $query = "insert into user_hide_ignored (user1,user2,timestamp) values ".$batchStringForInsertion;
        print("Query used for insertion : ".$query);
        $sql_prep =$conn_master->Prepare($query);
        $sql_exec = $conn_master->Execute($sql_prep, array());
    }
}

try {
    echo "Starting";
    if ( php_sapi_name() == 'cli' ){
        nonSelect2SelectHidesCleanUp::cleanTable();
    }
} catch (Exception $e) {
    echo $e->getTraceAsString();
    trigger_error($e->getTraceAsString(), E_USER_WARNING);
    trigger_error($e->getMessage(), E_USER_WARNING);
}
?>