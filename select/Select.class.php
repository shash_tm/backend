<?php
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../DBO/selectDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../DBO/userActionsDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/quizDBO.php";
require_once dirname ( __FILE__ ) . "/../spark/Spark.class.php";



$checkSum = "";



class Select
{
	private $user_id;
	private $source;
	private $functionClass;
	private $app_version_code;
	private $uaDBO;

	function __construct($user_id)
	{
		$this->functionClass = new functionClass();
		$this->source = $this->functionClass->getSourceFromHeader();
		$this->user_id = $user_id;
		$this->app_version_code = $this->functionClass->getHeader();
		$this->uaDBO = new userActionsDBO();
	}


	public function logAction($event, $status, $event_info, $user_id = null)
	{
		//event_type can be payment_update, google_server_call, android_server_call
		//for payement update, status will be payment status, for google android server call, status will be call response status
		$Trk = new EventTrackingClass();
		$data_logging = array();
		$user_id = $user_id == null ? $this->user_id : $user_id;
		$data_logging [] = array("activity" => "spark_payment", "event_type" => $event, "event_status" => $status, "event_info" => $event_info, "time_taken" => 0, "user_id" => $user_id);
		$Trk->logActions($data_logging);
	}

	public function saveAnswers($quiz_id, $answers){
		$answers = json_decode($answers,true);
		$ans_array = array();
		foreach ($answers as $key => $val)
		{
			$ans_array[] = array( $this->user_id, $quiz_id, $key, $val);
		}

		quizDBO::saveAllAnswers( $ans_array ) ;
		selectDBO::markSelectQuizPlayed($this->user_id, true);
		$output['responseCode'] = 200 ;
		return $output;
	}

	public function getMySelectData(){
		$select_data = selectDBO::getCurrentUserSelectData($this->user_id);
		$metadata = $select_data['metadata'];
		if($select_data && sizeof($select_data) > 0){
			if(isset($metadata)) {
				$metadata = json_decode($metadata, true);
				if(isset($metadata['quiz_played']))
					$select_data['quiz_played'] = $metadata['quiz_played'];
				else
					$select_data['quiz_played'] = false;
			}else{
				$select_data['quiz_played'] = false;
			}

			$days_left = (int)$select_data['days_left'];
			if($days_left > 0 && ($select_data['status'] == 'active' || $select_data['status'] == 'trial')){
				$select_data['is_tm_select'] = true;
				$select_data['side_cta'] = "Renew Now";
			}else{
				$select_data['is_tm_select'] = false;
				$select_data['days_left'] = null;
				$select_data['side_cta'] = "Join Now";		//Rejoin case
				$select_data['profile_cta'] = "Join Now";
			//	$select_data['profile_cta_text'] = "Rs. 999 after first month";
			}
		}else{
			if($this->isUserEligibleForFreeTrial() === true) {
				$select_data['profile_cta_text'] = "Rs. 999 after first month";
				$select_data['side_cta'] = "Join Free";
				$select_data['profile_cta'] = "Join Free";
			}else{
				$select_data['side_cta'] = "Join Now";
				$select_data['profile_cta'] = "Join Now";
			}
			$select_data['is_tm_select'] = false;
			$select_data['days_left'] = null;


			$select_data['quiz_played'] = false;
		}
		$select_data['compatibility_text_def'] = "What an interesting match! Time to find out more about each other.";
		$select_data['compatibility_image_def'] = self::getCategoryImage("default");
		unset($select_data['metadata']);
		return $select_data;
	}

	public function compareQuiz($match_id){
		$quiz_id = selectDBO::getSelectQuizId();
		$select_data = quizDBO::getQuestionAndAnswers($this->user_id, $match_id, $quiz_id);
		$result = array();
		$questions = array();
		$total_questions=0;
		$matched_questions=0;
		foreach ($select_data as $data) {
			$object = isset($questions[$data['question_id']]) ? $questions[$data['question_id']] : array();
			$object['question_text'] = $data['question_text'];
			$object['question_image'] = $data['question_image'];
			if($data['user_id'] == $this->user_id) {
				$object['my_option_id'] = $data['answer'];
				$object['my_option_text'] = $data['answer_text'];
			}else{
				$object['match_option_id'] = $data['answer'];
				$object['match_option_text'] = $data['answer_text'];
			}
			$questions[$data['question_id']] = $object;
			if($object['my_option_id'] == $object['match_option_id'])
				$matched_questions++;
			$total_questions++;
		}
		$result['match_percent'] = ($matched_questions/($total_questions/2))*100;
		$result['questions'] = $questions;

		return $result;
	}

	public function startFreeTrial(){
		$output = array();
		$freeTrialState = $this->isUserEligibleForFreeTrial(true);
		if(is_array($freeTrialState) && $freeTrialState['free_trial'] === true) {
			$freeTrialBucket = $freeTrialState['bucket_id'];
			$packages = SparkDBO::getActivePackages($this->user_id, $this->source, $freeTrialBucket, 'select');
			$free_trial_days = $packages[0]['expiry_days'];
			$metadata = $packages[0]['metadata'];
			selectDBO::subscribeToSelect($this->user_id, $free_trial_days, 'trial');
			$sparkObj = new Spark($this->user_id);
			if(isset($metadata) && $metadata != null){
				$metadata = json_decode($metadata, true);
				if(isset($metadata['chat_assist']) && ($metadata['chat_assist'] == true || $metadata['chat_assist'] == 'true')){
					SparkDBO::addSparkCount($this->user_id, 0, 0, 1);
					$sparkObj->matchWithRE($metadata);
				}
			}

			$sparkObj->saveLastRecommendation($this->user_id);
			$uu = new UserUtils();
			$uu->clearRecommendationCache($this->user_id);
                        $uu->clearMatchCountData($this->user_id);
			$output["success"] = "Free Trial Started";
			$output["responseCode"] = 200;
		}else{
			$output["error"] = "User no eligible for free trial";
			$output["responseCode"] = 403;
		}
		return $output;
	}

	public function getSelectCommanalities($match_id_array){
		$output = array();
		$allUsersOutput = array();
		$result = selectDBO::getCategoriesForSelectMatches($this->user_id, $match_id_array);
		foreach($result as $match){
			if(!isset($output[$match['match_id']]))
				$output[$match['match_id']] = array("category" => $match['category'], "count" => $match['count']);
			else
				$output[$match['match_id']] = $output[$match['match_id']]['count'] < $match['count'] ?
					array("category" => $match['category'], "count" => $match['count']) : $output[$match['match_id']];
		}
		$selectUsers = selectDBO::getUserListSelectData($match_id_array);
		foreach($selectUsers as $key=>$value){
			$current_user = isset($selectUsers['user_id']) ? $selectUsers['user_id'] : $selectUsers[$key]['user_id']; 		//differs for matchs/profile
			$metadata = json_decode($selectUsers[$key]['metadata'], true);
			if($metadata['quiz_played'] != true)
				continue;
			$userResult = array();
			$userResult['is_tm_select'] = true;
			if(isset($output[$current_user])){
				$db_category = $output[$current_user]['category'];
				$actual_category_name = self::getActualCategoryName($db_category);
				$first_sentence = self::getFirstSentenceForCategoryName($actual_category_name);
				$second_sentence = self::getSecondSentenceForCategoryName($db_category);
				$userResult['common_image'] = self::getCategoryImage($db_category);
				$userResult['category'] = $actual_category_name;
				$userResult['common_string'] = $first_sentence;
				$userResult['quote'] = $second_sentence;
			}else{
				$userResult['common_image'] = self::getCategoryImage("default");
				$userResult['category'] = "default";
				$userResult['common_string'] = "What an interesting match! Time to find out more about each other.";
			//	$userResult['quote'] = $second_sentence;
			}
			$allUsersOutput[$current_user] = $userResult;
		}
	//	$result = array("is_tm_select"=>true,"common_string"=>"You both match on awesomeness",
	//		"common_image" => "http://findicons.com/files/icons/1049/2s_space_emotions_v2/256/love.png");
		return $allUsersOutput;
	}

	public function isUserEligibleForFreeTrial($getBucket=false){
		$select_data = selectDBO::getCurrentUserSelectData($this->user_id);
		if($select_data && sizeof($select_data) > 0){
			return false;
		}else{
			$sparkObj = new Spark($this->user_id);
			$isFreeTrialBucket = $sparkObj->getBucketForUSer('select', true);
			if(is_array($isFreeTrialBucket) && $isFreeTrialBucket['free_trial'] === true)
				return $getBucket === true ? $isFreeTrialBucket : true;
		}
		return false;
	}

	public function getSelectMembers($user_id_array){
		$selectUsers = selectDBO::getUserListSelectData($user_id_array);
		$output = array();
		foreach($selectUsers as $user){
			$output[$user['user_id']] = true;
		}
		return $output;
	}

	public function getCompatibilityStrings($dbString){
		//Strings in DB 'trust', 'intimacy', 'family', 'choice', 'identity'
		//Actuals: 'trust', 'intimacy', 'the importance of family', 'freedom of choice', 'personal identity'

	}

	private function getActualCategoryName($dbName){
		//Strings in DB 'trust', 'intimacy', 'family', 'choice', 'identity'
		//Actuals: 'trust', 'intimacy', 'the importance of family', 'freedom of choice', 'personal identity'
		$categories = array("trust" => "trust","intimacy" => "intimacy","family" => "the importance of family","choice" => "freedom of choice","identity" => "personal identity");
		return $categories[$dbName];
	}

	private function getFirstSentenceForCategoryName($category_name){
		if($this->source == 'androidApp')
			$category_name = "<b>".$category_name."</b>";

		$sentences = array("Your beliefs around CATEGORY_NAME match perfectly.",
			"Your thoughts around CATEGORY_NAME are similar.",
			"Your views about CATEGORY_NAME match just as well as you do!",
			"You both think very similarly about CATEGORY_NAME.",
			"Your compatibility is soaring on your thoughts around CATEGORY_NAME.");
		$rand_keys = array_rand($sentences);
		return str_replace("CATEGORY_NAME", $category_name, $sentences[$rand_keys]);
	}

	private function getSecondSentenceForCategoryName($category_name){
		$sentences = array(
			"trust" => array("Trust is built only with consistency.",
				"To be trusted is a greater compliment than being loved.",
				"The best way to find out if you can trust somebody is to trust them.",
				"To be trusted is a greater compliment than being loved.",
				"Trust is the fruit of a relationship in which you know you are loved.",
				"Loving someone is giving them the power to break your heart, but trusting them not to."
			),
			"intimacy" => array(
				"People get close by healing each other and reparing all that's broken.",
				"It is not time or opportunity but disposition alone that defines intimacy.",
				"Intimacy is the capacity to be completely weird with someone!",
				"There's nothing more intimate in life than simply being understood",
				"Real intimacy is of the soul, and the soul is reserved." ,
				"The opposite of Loneliness is not Togetherness , It's Intimacy",
				"Intimacy causes us to inherit and trade each other's stories."
			),
			"family" => array(
				"Everyone needs a house to live in, but a supportive family is what builds a home.",
				"A family is a place where minds come in contact with one another.",
				"Having somewhere to go is home. Having someone to love is family. Having both is a blessing.",
				"The love of a family is life’s greatest blessings.",
				"Other things may change us, but we start and end with the family.",
				"A house is built by hands, but a home is built by heart.",
				"A man travels the world over in search of what he needs, and returns home to find it.",
				"Many men can make a fortune but very few can build a family.",
				"The family is one of nature's masterpieces."
			),
			"choice" => array(
				"It is by choice and not chances that we change our circumstances",
				"You cannot hinder someone’s free will, that’s the first law of the Universe, no matter what the decision." ,
				"Never say yes to anything you can't say no to.",
				"May your choices reflect your hopes and not your fears.",
				"One is free to choose but not free from the consequences of choices.",
				"The choices we make determine our destiny.",
				"Sometimes, even the wrong choices can lead us to the right places!"
			),
			"identity" => array(
				"We are masters of our own thoughts but slaves of our emotions",
				"A strongly marked personality can influence descendants for generations." ,
				"The world is a tragedy to those who feel, but a comedy to those who think." ,
				"Style is a reflection of your attitude and your personality.",
				"Personality has power to uplift, power to depress, power to curse, and power to bless.",
				"Personality is an unbroken series of successful gestures.",
				"Positive expectations are the mark of the superior personality."
			)
		);

		$rand_keys = array_rand($sentences[$category_name],1);
		return $sentences[$category_name][$rand_keys];
	}

	public function getSelectQuizQuestionId(){
		return selectDBO::getSelectQuizId();
	}

	public function deleteSelectMemberShip(){
		selectDBO::deleteSelectMembership($this->user_id);
	}

	private function getCategoryImage($category_name){
		global $cdnurl;
		$dir = $cdnurl."/images/select_categories/";
		return $dir.$category_name.".gif" ;
	}
}

