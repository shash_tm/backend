<?php 
include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/validate.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
include_once (dirname ( __FILE__ ) . '/include/User.php');
include_once (dirname ( __FILE__ ) . '/fb.php');
include_once (dirname ( __FILE__ ) . '/TrustBuilder.class.php');
require_once dirname ( __FILE__ ) . "/logging/emailerLogging.php";
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/geoIPTM.php";
require_once (dirname ( __FILE__ ) . '/include/Utils.php');



class login{
	private $user;
	private $func;
	private $login_mobile;
	private $source;
	private $version;
	private $ip_country;
	function __construct(){
		$this->user = new User();
		$this->func = new functionClass();
		$this->login_mobile = $this->func->isMobileLogin();
		$this->source = $this->func->getSourceFromHeader();
		$this->version = $this->func->getHeader();
	}
	
	private function startSession(){
		session_start();
		//session_regenerate_id(true);
	}
	function registerViaEmail($data){
		
		$object = new Validation();
		$data['email'] = trim($data['email']);
		$data['fname'] = ucfirst(strip_tags(trim($data['fname'])));
		$data['lname'] = ucfirst(strip_tags(trim($data['lname'])));
		$error['email']=$object->validateEmail($data['email']);
		$error['password']=$object->validateText($data['password']);
		$error['fname']=$object->validateText($data['fname']);
		$error['lname']=$object->validateText($data['lname']);

		
		if(!($data['gender']=='M' or $data['gender']=='F'))
			$error['gender']=true;
		if($error['email'] || $error['password'] || $error['fname'] ||  $error['lname']|| $error['gender'])
			$error['register']=true;
		
		$data['birthday'] = $data['TM_S1_WhenBornYear'] . "-" . $data['TM_S1_WhenBornMonth'] . "-" . $data['TM_S1_WhenBornDay'];
		if($this->source == "androidApp" && $this->version >= 100 && !$error['register']) {
			$age = date_create($data['birthday'])->diff(date_create('today'))->y;
			if(!($age>=18 && $age<=70)){
				$error['register'] = true;
			}else {
				$pref_age = Utils::preferenceAge($age,$data['gender']);
				$data['start_age'] = $pref_age['start'];
				$data['end_age'] = $pref_age['end'];
			}
		}
		
		if($error['register']){
			$response['responseCode'] = 403;
			$response['error'] = "Sorry Error in Registration data,try again";
		}
		else{
			$presence = $this->user->checkEmail($data['email']);
			switch($presence){
				case 'FB_USER':
					$response['responseCode'] = 403;
					$response['error'] = "Please login via facebook";
					break;
				case 'USER_EXISTS':
					$response['responseCode'] = 403;
					$response['error'] = "Email Id already exists";
					break;
				case 'USER_ABSENT':
					$registered_from = $this->func->getSourceofReg();
					$this->startSession();
					$this->user->createNewUser($data,$registered_from,$this->version);
					$this->func->setSession('NOT_FB_USER',$data['email']);
					$user_details = functionClass::getUserDetailsFromSession();
					/** Sending data on login : @udbhav **/
					$user_id = $user_details['user_id'];
					$result = $this->user->getUserformenu($user_id);
					$response['data'] = $result;
					$response['basics_data'] = array("url"=>$this->getJSONUrl($result['country']));
					$response['responseCode'] = 200;
					break;
			}
		}
		print_r(json_encode($response));die;
	}
	
	function registerViaFB($data){
		global $response;
		$currTime = time();
		$token=$data['token'];
		try{
			$object=new fb($token);
			//$object->getData();
			$object->getBasicData();
			$object->logFacebook(NULL);
			//$trustBuilder = new TrustBuilder(NULL);
		}
		catch(Exception $e) {
			$currTime1 = time();
			$diff = $currTime1-$currTime;
			$this->logAction("fb_session_timeout",$diff);
			//trigger_error("Facebook:could not create facebook object:".$e->getMessage(),E_USER_WARNING);
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			$response['responseCode'] = 403;
			$response['error'] = "Your facebook session is timed out. Please login again.";//$e->getMessage();//"Sorry,You can not proceed with this action due to internal server problem";
			print_r(json_encode($response));die;
		}
		try {
			if($this->login_mobile){
				$prescence=$object->checkFbPrescenceForApp();
				$device_id = $data['device_id'];
			}else 
				$prescence=$object->checkFbPrescence();

			$json_url = $this->getJSONUrl();
			if($prescence['status']=='NEW_USER') {
				$object->getData();
				$test = $object->testfids($object->data['id']);
				
				$trustBuilder = new TrustBuilder(NULL);
				if(!$test){
					$verify = $trustBuilder->authoriseFBAccount($object->data,true);
					if(!$verify['flag']){
						$object->logFacebook(NULL, $verify['message']);
						throw new Exception($verify['message']);
					}
				}
					
				$registered_from = $this->func->getSourceofReg();
				$user_id = $trustBuilder->CreateUserForFacebook($object->data,$registered_from,$device_id);
				$this->startSession();
				if($test)
					$this->func->setSession('FB_USER_test',$user_id);
				else 
					$this->func->setSession('FB_USER',$object->data['id']);
				$object->ip_country = $this->ip_country;
				$object->storeFacebook($user_id,"NEW","login");
				$trustBuilder->setUserId($user_id);
				/**
				 * commenting the update trust score of facebook and credit assigning
				 * we'll be awarding the same once registeration is complete
				 * on the other hand we do log data from fb at this moment
				 * @Himanshu
				 */
				
				$status = functionClass::getUserStatus($user_id);
				$response['responseCode'] = 200;
				/** sending data on login : @udbhav **/
				$result = $this->user->getUserformenu($user_id);
				$result['isNewUser'] = true;      // for tracking purposes for app only done by rajesh
				if(!isset($result['country_code']) || $result['country_code'] == null)
					$result['country_code'] = $this->ip_country;
				$response['data'] = $result;
				$response['basics_data'] = array("url"=>$json_url);
			}else if($prescence['status']=='FACEBOOK_NOT_RETURNED_EMAIL'){
				$response['responseCode'] = 403;
				$response['error'] = "We can't pull your Email ID from facebook,So Please Register by Email";
			}else if($prescence['status']=='CONNECTED_VIA_EMAIL'){
				$user_id = $prescence['user_id'];
				$trustBuilder = new TrustBuilder($user_id);
				$res = $trustBuilder->verifyFacebook($token,true,'login_email');
				if($res['responseCode'] == 403) {
					throw new Exception($res['error']);
				}
				$this->startSession();
				$this->func->setSession('FB_USER',$object->data['id']);
				$status = functionClass::getUserStatus($user_id);
				if($status == 'non-authentic') {
					if($trustBuilder->authenticateUser()) {
						$status = 'authentic';
					}
				}
				$response['responseCode'] = 200;
				/** sending data on login : @udbhav **/
				$result = $this->user->getUserformenu($user_id);
				$result['isNewUser'] = false;      // for tracking purposes for app only done by rajesh
				$response['data'] = $result;
				$response['basics_data'] = array("url"=>$this->getJSONUrl($result['country']));;
				
// 					$response['responseCode'] = 403;
// 					$response['error'] = "Please login with email id";
				}else if($prescence['deletion_status']=="mark_deleted"){
					$response['responseCode'] = 403;
					$response['status'] = "deleted";
					$response['error'] = "Sorry, you can't login now. Give us 24 hours to remove your info. We'll treat you as a new user if you want to reactivate.";
			}else if($prescence['status']=="DEACTIVATED_PROFILE"){
					$this->startSession();
					$this->func->setSession('FB_USER',$object->data['id']);
					$response['responseCode'] = 403;
					$response['status'] = "suspended";
			}else if($prescence['status']=="BLOCKED_PROFILE"){
					$response['responseCode'] = 403;
					$response['status'] = "blocked";
					$response['error'] = "Your profile is blocked. Please contact Trulymadly Customer Care";
			}else {
				$user_id=$prescence['user_id'];
				$object->storeFacebook($user_id,"USER_EXISTS");
				$this->startSession();
				$this->func->setSession('FB_USER',$object->data['id']);
				//$eventTrack = new EventTracking($user_id);
				//$eventTrack->logAction("login_facebook");
				$status = functionClass::getUserStatus($user_id);
				$response['responseCode'] = 200;
				/** sending data on login : @udbhav **/
				$result = $this->user->getUserformenu($user_id);
				$result['isNewUser'] = false;      // for tracking purposes for app only done by rajesh
				$response['data'] = $result;
				$response['basics_data'] = array("url"=>$this->getJSONUrl($result['country']));
			}
			
			$currTime1 = time();
			$diff = $currTime1-$currTime;
			if(isset($result['isNewUser']) && $result['isNewUser']) {
				$this->logAction("fb_new_user",$diff,$user_id);
			}
			else {
				$this->logAction("fb_existing_user",$diff);
			}
			
			print_r(json_encode($response));die;
		}catch(Exception $e) {
			//trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			//trigger_error("PHP Web:".$e->getMessage(), E_USER_WARNING);
				$response['responseCode'] = 403;
				$response['error'] = $e->getMessage();//"Sorry,You can not proceed with this action due to internal server problem";
				print_r(json_encode($response));die;
		}	
	}
	
	function loginUser($data){ 
		global $inner_ips ;
		$object = new Validation();
		$data['email'] = trim($data['email']);
		$error['email']=$object->validateText($data['email']);
		$error['password']=$object->validateText($data['password']);
		if(!$error['email']&&!$error['password']) {
			if($this->login_mobile == true || in_array($_SERVER['REMOTE_ADDR'],$inner_ips))
				$presence = $this->user->checkEmailForApp($data['email'],$data['password']);
			else{
				if($data['email'] == 'shashwat@trulymadly.com') {
					$presence = $this->user->checkEmail($data['email'],$data['password']);
				}else {
					$response['responseCode'] = 403;
					$response['error'] = "Email Id is not correct";
					print_r(json_encode($response));die;
				}
				//$presence = $this->user->checkEmail($data['email'],$data['password']);
			}
			switch($presence){
				case 'USER_EXISTS':
					$this->startSession();
					$this->func->setSession('NOT_FB_USER',$data['email']);
					$user_details = functionClass::getUserDetailsFromSession();
					$user_id = $user_details['user_id'];
					$status = functionClass::getUserStatus($user_id);
					$response['responseCode'] = 200;
					/** sending data on login : @udbhav **/
					$result = $this->user->getUserformenu($user_id);
					$response['data'] = $result;
					$response['basics_data'] = array("url"=>$this->getJSONUrl($result['country']));
					break;
				case "USER_DELETED":
					$response['responseCode'] = 403;
					$response['status'] = "deleted";
					$response['error'] = "Sorry, you can't login now. Give us 24 hours to remove your info. We'll treat you as a new user if you want to reactivate. ";
					break ;  
				case "DEACTIVATED_PROFILE":
					$this->startSession();
					$this->func->setSession('NOT_FB_USER',$data['email']);
					$response['responseCode'] = 403;
					$response['status'] = "suspended";
					break;
				case "BLOCKED_PROFILE":
					$response['responseCode'] = 403;
					$response['status'] = "blocked";
					$response['error'] = "Your profile is blocked. Please contact Trulymadly Customer Care";
					break;
				case "LOGIN_BLOCKED":
					$response['responseCode'] = 403;
					$response['status'] = "blocked";
					$response['error'] = "You have made too many login attempts";
					break;
				default :
					$response['responseCode'] = 403;
					$response['error'] = "Incorrect email id or password";	
					break;
			}
		}else if($error['email']){
			$response['responseCode'] = 403;
			$response['error'] = "Email Id is not correct";
		}
		print_r(json_encode($response));die;
	}
	
	
	function getUser(){
		$session = functionClass::getUserDetailsFromSession();
	//	var_dump($session);
		if(isset($session['user_id'])){
			$userId = $session['user_id'];
			$result = $this->user->getUserformenu($userId);
			$response['responseCode'] = 200;
			$response['data'] = $result;
			$response['basics_data'] = array("url"=>$this->getJSONUrl($result['country']));
			print_r(json_encode($response));
			exit;
		}else{
			$response['responseCode'] = 401;
			print_r(json_encode($response));
			exit;
		}
	}
	
	function activateUser(){
		$session = functionClass::getUserDetailsFromSession();
		if(isset($session['user_id'])){
			$status = functionClass::getUserStatus($session['user_id']);
			if($status=='suspended'){
				$this->user->restoreUser($session['user_id']);
				$response['responseCode'] = 200;
				print_r(json_encode($response));
				die();
			}
		}
	}
	
	function getJSONUrl($location= null) {
		global $registerJsonUrl,$registerSingaporeJsonUrl,$registerIndonesiaJsonUrl,$registerUSJsonUrl;
		if(isset($location) && $location == 'India') {
			$this->ip_country = 113;
			return $registerJsonUrl;
		}else if(isset($location) && $location == 'United States')
		{
			$this->ip_country = 254;
			return $registerUSJsonUrl;
		}
		else{
			try {
				$country = geoIPTM::getCountryName();
				 if($country == 'India') {
					 $this->ip_country = 113;
					 return $registerJsonUrl;
				}else if($country == 'United States') {
					$this->ip_country = 254;
					return $registerUSJsonUrl;
				}
			} 
			catch (Exception $e) {
				trigger_error("PHP Web: Fatal error ".$e->getTraceAsString(), E_USER_WARNING);
				$this->ip_country = 113;
				return $registerJsonUrl;
			}
			
		}
		$this->ip_country = 113;
		return $registerJsonUrl;
	}
	/*
	 * to log the time taken by fb calls
	*
	*/
	
	private function logAction($event,$diff,$user_id=NULL) {
		$Trk = new EventTrackingClass();
		$data_logging = array();
		$data_logging [] = array("user_id" => $user_id , "activity" => "fb_process" , "event_type" => $event, "event_status" => "success", "time_taken"=>$diff );
		$Trk->logActions($data_logging);
	}
}



try{

	$func=new functionClass();
	$inner_ips = array('115.249.198.73','52.77.119.109');//
	if(!$_REQUEST['login_mobile'])
		functionClass::redirect('index');
	
	
	
	//$response = '';
	$login = new login();
	
	//$user_details = $this->user->getUserDetailsFromSession();
	
	$data = $_POST;
	//var_dump($data);exit;
	$data['ver'] = $func->getHeader();
	
	
	$device_id=Utils::getDeviceIdFromHeader();
	
	$tb_object=new TrustBuilder();
	$is_fid_blocked=$tb_object->checkDeviceId($device_id);
	
	if($is_fid_blocked['status']=='success')
	{
		$response['responseCode'] = 403;
		$response['status'] = "blocked";
		$response['error'] = "Your Device has been blocked because of your relationship status or inappropriate activity. Please email us at contact@trulymadly.com for further queries.";
		print_r(json_encode($response));
		exit;	
	}

	
	
	if($_REQUEST['login_mobile'] && $_REQUEST['getuserdata']){
		$login->getUser();
	}
	if($data['activate']){
		$login->activateUser();
	}
	if($data['registerViaEmail']){
		$login->registerViaEmail($data);
	}
	if($data['from_fb']){
		$login->registerViaFB($data);
	}
	if($data['login']){
		$login->loginUser($data);
	}
	if(isset($_REQUEST['show']) && $_REQUEST['show']=='deactivate'){
		$smarty->assign('show_deactivate','1');
	}
//	if(isset($_REQUEST['allowLogin']) && $_REQUEST['allowLogin']=='admin') {
//		$smarty->assign('show_login','1');
//	}


	if(in_array($_SERVER['REMOTE_ADDR'],$inner_ips))
		$smarty->assign('show_login','1');

	
}catch(Exception $e){
//	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	trigger_error("PHP Web:".$e->getMessage(), E_USER_WARNING);
}

if(!$_REQUEST['login_mobile'])
	$smarty->display("templates/index.tpl");
?>
