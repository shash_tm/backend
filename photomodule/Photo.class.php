<?php 

require_once dirname ( __FILE__ ) . "/../TMObject.php";
require_once dirname(__FILE__)."/../lib/wideimage/WideImage.php";
require_once dirname(__FILE__)."/../admin/logging.php";
require_once dirname(__FILE__)."/../include/mail_undermoderation.php";
require_once dirname(__FILE__)."/../abstraction/query_wrapper.php";
require_once dirname(__FILE__)."/../include/config.php";
require_once dirname(__FILE__)."/../include/Utils.php";
require_once dirname(__FILE__). "/../DBO/VideoDBO.php";

require_once dirname(__FILE__)."/../videos/VideoEncoder.class.php";

class Photo extends TMObject{
	private $userId;
	private  $filesize=8388608;//8MB
	private  $filesize_in_words="8MB";
	private $profile_pic_exists=false;
    private $admin_log;
    private $toadmin;
	private $VideoDBO;
	private $is_video_enabled;
    
	function __construct($user_id=NULL,$is_video_enabled=false) {
		parent::__construct ( $user_id );
		$this->userId = $user_id;
		$this->admin_log = new logging();
		$this->VideoDBO=new VideoDBO();
		$this->is_video_enabled=$is_video_enabled;
		//$this->toadmin = new UnderModeration();
	}

	public function getVideoParams($device_name)
	{
		$video_params=array();
		global $video_prefix,$video_s3_bucket,$video_identity_pool;
		$video_params['prefix']=$video_prefix;
		$video_params['bucket']=$video_s3_bucket;
		$key=$this->VideoDBO->GetHashParameters($this->userId);
		$encrypted= $this->encrypt($video_identity_pool,strrev($key['myhash']));
		$video_params['poolid']=$encrypted;
		$compression_disabled_array=array("Redmi Note 3","m3","ASUS_T00J","SM-C101","SM-J700F","SM-G360H","SM-G920I","SM-J710F","Mi 4i");
		if(in_array($device_name,$compression_disabled_array)){
			$video_params['video_compression_enabled']=false;
		}else {
			$video_params['video_compression_enabled']=true;
		}
		$video_params['video_upload_enabled']=true;//
		$video_params['rotate_video_on_app']=false;
		$video_params['keep_original_audio_during_transcode']=true;
		return $video_params;
	}


	public function encrypt($input, $key) {
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
		$input = $this->pkcs5_pad($input, $size);
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
		$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $key, $iv);
		$data = mcrypt_generic($td, $input);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		$data = base64_encode($data);
		return $data;
	}
	public function pkcs5_pad ($text, $blocksize) {
		$pad = $blocksize - (strlen($text) % $blocksize);
		return $text . str_repeat(chr($pad), $pad);
	}

	public function getHash()
	{
		return $this->VideoDBO->GetHashParameters($this->userId)['myhash'];
	}

	public function getUserVideos()
	{
		try {
			global $video_cdn,$video_prefix,$video_s3_output_prefix;
			$user_array=array();
			array_push($user_array,$this->userId);
			$videos = $this->VideoDBO->getActiveVideos($user_array, true);
			$video_coll = array();

			foreach ($videos as $video_item) {
				//VideoObj
				$video_obj = array();

				$video_obj['video_id'] = $video_item['video_id'];
				$video_obj['width'] = $video_item['width'];
				$video_obj['height'] = $video_item['height'];
				$video_obj['duration'] = $video_item['duration'];
				$video_obj['filename']=$video_item['filename'];
				$video_obj['resolutions'] = json_decode($video_item['available_resolutions'], true);
				$reso='';
				if(count($video_obj['resolutions'])>0)
				{
					$reso=$video_obj['resolutions'][0];
				}
				$encoder=new VideoEncoder($this->user_id);
				$signed_url=$encoder->getSignedURl($video_prefix.$video_item['filename']);
				$video_obj['UnEncodedURL'] = $signed_url;
				$file=explode('.',$video_item['filename'])[0];
				$ext=explode('.',$video_item['filename'])[1];

				$video_obj['EncodedURL']=$video_cdn.$video_s3_output_prefix.$file.'_'.$reso.'.'.$ext;
				$video_obj['caption'] = $video_item['caption'];

				$video_obj['status'] = $video_item['status'];
				$video_obj['encoding_status']=$video_item['job_status'];
				$video_obj['thumbnail'] = $video_cdn.$video_s3_output_prefix.$video_item['thumbnail'];
				if($video_item['job_status']=='pending'||$video_item['job_status']=='error')
				{
					$video_obj['thumbnail']=null;
					$video_obj['EncodedURL']=$signed_url;
					unset($video_obj['UnEncodedURL']);
				}

				if ($video_item['encoding_id'] == '-1') {
					$video_obj['resolutions'] = null;
				}
				if ($video_item['ismuted'] == 1) {
					$video_obj['mute'] = true;
				} else {
					$video_obj['mute'] = false;
				}
				array_push($video_coll, $video_obj);
			}
			return $video_coll;
		}
		catch(Exception $ex)
		{
			trigger_error($ex->getMessage(),E_WARNING);
		}
	}

	public function getUserPhotos(){
		global $imageurl;
		$rs=$this->conn->Execute("select photo_id, name, thumbnail, status, is_profile, can_profile, admin_approved, mapped_to from user_photo where user_id='".$this->userId."' 
				and status in ('active','rejected','under_moderation','temp_active') ORDER BY photo_id DESC");
		$results=$rs->GetRows();
		if($rs->_numOfRows>0) {
			$o=0;
			$temp_images = array() ;
			foreach($results as $result)  {
				if ( isset($result ['mapped_to']) && $result ['mapped_to'] != NULL)
					$temp_images [] = $result ['mapped_to'] ;
				if ( !in_array($result['photo_id'], $temp_images))
				{
					$encoded_data[$o]['photo_id']=$result['photo_id'];
					$encoded_data[$o]['name']=$imageurl.$result['name'];
					$encoded_data[$o]['thumbnail']=$imageurl.$result['thumbnail'];
					$encoded_data[$o]['is_profile']=$result['is_profile'];
					$encoded_data[$o]['status']=$result['status'];
					$encoded_data[$o]['can_profile']=$result['can_profile'];
					$encoded_data[$o]['admin_approved']="yes";//['admin_approved'];
					if($result['status']=='under_moderation'  ||  $result['status']=='temp_active'  ){
						$encoded_data[$o]['status']='active';
					}else if($result['status']=='rejected'){
						$this->updatePhotoStatus($result['photo_id'],'rejected_shown');
   						$encoded_data[$o]['status']='Rejected';
						$encoded_data[$o]['is_profile']='no';
					}
					$o++;
				}
				
			}
		}
		if(!$this->is_video_enabled)
		return $encoded_data;
		else
		{
			$return_data=array("photos"=>$encoded_data,"videos"=>$this->getUserVideos());
			return $return_data;
		}
	}

	public function  getUserFbPic(){
		$rs=$this->conn->Execute("select profile_url from user_facebook where user_id='".$this->userId."'");
		$result=$rs->GetRows();
		if($result[0]['profile_url']) {
			return $result[0]['profile_url'];
		}
		return false;
	}

	public function checkProfileExist($data){
		$profile_exist = 0;
		foreach ($data as $item){
			if($data['is_profile'] == 'yes')
				$profile_exist = 1;
		}
		return $profile_exist;
	}

	/*
	 * valdiate size and error and log in action_log
	 * return size of the image
	 */
	
	private function validateSize($file)
	{
		$file_name=$_FILES["file"]['tmp_name'];
		$fsize = intval($file["size"]);
	/*	echo "validating size"; 
		var_dump($fileSize);die;*/
		if($fsize==0||$fsize>$this->filesize){
			//var_dump("size issues");
			throw new Exception("File Size should be less than ".$this->filesize_in_words." .Current Size: $fsize");
	    }
		return $fsize;
	}
	
	private function validateType($file)
	{
		if(exif_imagetype($file['tmp_name'])===FALSE){
			//var_dump("type issues");
			throw new Exception("Not an Image File");
		}
	}
	
	private function validateUpload($file)
	{
		if ($file['error'] === UPLOAD_ERR_OK) {
			//return true;
		} else {
			//var_dump("upload issues");
			throw new Exception($file['error']);
		}
	}
	private  function validateImage($file){
		try {
			
			// validating upload_error , extension , size of file 
			$this->validateUpload($file);
			$this->validateType($file);
			$fsize=$this->validateSize($file);
		  
}catch (Exception $e) {
			trigger_error("PHP Fatal error Web:".$e->getMessage(), E_USER_WARNING);
				
			trigger_error("PHP Fatal error Web:".$e->getTraceAsString(), E_USER_WARNING);
		   	}
		return $fsize;
	}
	
	public function profilePicExists($user_id){
		if($this->profile_pic_exists==true)
			return true;
		
		$sql=$this->conn->Prepare("select count(*) as cnt from user_photo where user_id=? and is_profile='yes' and 
				(status='active' or status='under_moderation' )");
		$rs=$this->conn->Execute($sql,array($user_id));
		$result=$rs->GetRows();
		if($result[0]['cnt']>0) {
			$this->profile_pic_exists=true;
			return true;
		}
		return false;
		
	}
	
	public function getNewImageName(){
		global $image_basedir;
		$name=1000*microtime(true)."_".$this->random(18);
		$name_thumbNail=$name."_".$this->random(10).".jpg";
		$name_original=$name."_original.jpg";
		$name=$name.".jpg";
		$image_location=$image_basedir.$name;
		$image_original_location=$image_basedir.$name_original;
		$image_thumbNail_location=$image_basedir.$name_thumbNail;
		return array($name,$name_thumbNail,$name_original,$image_location,$image_thumbNail_location,$image_original_location);	
	}
	
	
	public function rotateImage($photo_id,$angle){
		global $baseurl, $imageurl;
		$image_id = $photo_id;
		
		try {
				$sql = $this->conn->Prepare ( "select user_id,photo_id,is_profile,from_fb,name from user_photo where user_id=? and photo_id=?" );
				$rs = $this->conn->Execute ( $sql, array (
						$this->userId,
						$image_id
				));
		
				$row = $rs->FetchRow ();
				//var_dump($row);exit;
				if ($row ['user_id'] == $this->userId) {
					$file_name = $row ['name'];
					$url = $imageurl.$file_name;
					$this->addFromComputer(false,$url,true,$row['is_profile'],null,$row['from_fb'],$angle);
					
					// change status of old image
									
					
					$query="update user_photo set status='changed',is_profile='no' where  user_id=? and photo_id=? ";
					$param_array=array (
							$this->userId,
							$image_id
					) ;
					$tablename='user_photo';
					$rs=Query_Wrapper::UPDATE($query, $param_array, $tablename);
					
					/*$sql = $this->conn->Prepare ( "update user_photo set status='changed',is_profile='no' where  user_id=? and photo_id=?  " );
					$rs = $this->conn->Execute ( $sql, array (
							$this->userId,
							$image_id
					) );*/
					if(isset($_SESSION['admin_id'])){
						$query["sql"] = "update user_photo set status='changed',is_profile='no' where  user_id=? and photo_id=?";
						$query["parameter"] = "'$this->userId'.','.'$image_id'";
						$query = json_encode($query);
						$step = "rotate photo";
							
						$this->admin_log->log($_SESSION['admin_id'],$this->userId,$query,$step);
					}
				}
			
			$encoded_data = $this->getUserPhotos ();
			print_r ( json_encode ( $encoded_data ) );
		} catch ( Exception $e ) {
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			die ();
		}
	}
	
	public function addFromComputer($withoutcrop=true,$fb_url=null,$dont_return=false,$save_as_profile=null,$fromCrop=null,$fromFb=null,$rotateAngle=null, $photo_id = NULL) {
		global $baseurl,$imageurl,$image_basedir,$redis;
// 		if (!isset($_FILES["file"]))
// 			return;
        $file_name=null;

		try {
			//throw new Exception("Intentional Exception\n");
			
			list($name,$name_thumbNail,$name_original,$image_location,$image_thumbNail_location,$image_original_location)=$this->getNewImageName();
			//list($name_blur,$name_thumbNail_blur,$image_location_blur,$image_thumbNail_location_blur)=$this->getNewImageName();
			$webp_image_location = Utils::getWebpName($image_location) ;
			$webp_image_thumbnail_location = Utils::getWebpName($image_thumbNail_location);
			$webp_name = Utils::getWebpName($name) ;
			$webp_name_thumbnail = Utils::getWebpName($name_thumbNail) ;

			
			$file_name=$_FILES["file"]['tmp_name'];


			if(isset($fb_url)){
				$this->downloadFile($fb_url,$image_location);
				$file_name=$image_location;
			}
			else{
			$img_size=$this->validateImage($_FILES["file"]);
			}

			$original_image = WideImage::load($file_name);
		    $natural_width=$original_image->getWidth();
		    // actual height of image i.e before compression 
		    $original_height=$original_image->getHeight();

		    // // actual width of image i.e before compression 
		    $original_width=$natural_width;

			//if image is of more than 640 pixels reduce the size
			$image=$original_image->resize(640,640,"inside","down");
			$new_width=$image->getWidth();
			$new_height=$image->getHeight();
			if($withoutcrop==true){
			//crop calculation
			$ratio=$natural_width/$_POST['width'];
			$x=$ratio*$_POST['x'];
			$y=$ratio*$_POST['y'];
			$width=$ratio*$_POST['w'];
			$height=$ratio*$_POST['h'];
			
			$new_ratio=$new_width/$natural_width;
			$x=intval($x*$new_ratio);
			$y=intval($y*$new_ratio);
			$width=intval($new_ratio*$width);
			$height=intval($new_ratio*$height);
			
			$image = $image->crop($x,$y,$width,$height);
			}

			/*
			 * rotate image with a given angle in degrees clockwise
			 * 
			 */
			if(isset($rotateAngle)){
				$image = $image->rotate($rotateAngle);
				//$original_image=$original_image->rotate($rotateAngle);
			}
		    //$original_image->saveToFIle($image_original_location,90);
			$image->saveToFile($image_location,90);
			$this->convertTowebP($image_location, $webp_image_location);

			//$name_blur,$name_thumbNail_blur,$image_location_blur,$image_thumbNail_location_blur
			
	/*		$blurred_image=$this->blurImage($image);
			$blurred_image->saveToFile($image_location_blur,90);    */
				
			$thumbnail_image = $image->resize(100,100,"fill");
			$thumbnail_image->saveToFile($image_thumbNail_location,90);
			$this->convertTowebP($image_thumbNail_location, $webp_image_thumbnail_location) ;
			
	/*		$blurred_thumbnail_image=$this->blurImage($thumbnail_image);
			$blurred_thumbnail_image->saveToFile($image_thumbNail_location_blur,90);  */
				
			
			$is_profile=$this->profilePicExists($this->userId)==true?"no":"yes";
			if(isset($save_as_profile) && $is_profile=='yes'){
				$is_profile=$save_as_profile;
			}
			if(isset($fb_url)){
				$from_fb='yes';
			}else
				$from_fb=$_POST['from_fb'];//"no";
			
			if(isset($fromCrop) || isset($rotateAngle)){
				$is_profile = $save_as_profile;
				$from_fb = $fromFb;
			}
			$mapped_to = null ;
			if (isset($photo_id) && $photo_id != null) 
			{
				// if this is an edited photo 
				$mapped_to = $photo_id ;
				$status_to_set = 'temp_active' ;
				if (isset($save_as_profile) && $save_as_profile != null )
				{
					// if the older photo was a profile photo then set this one as profile photo
					$is_profile = $save_as_profile ;
				}
					
			} 
			else 
			{
				$status_to_set = 'active' ;
			}
			 
			
			$query="insert into user_photo (
					user_id, name,original_name,original_height,original_width,
					thumbnail,
					time_of_saving,
					is_profile,
					width,
					height,
					is_moderated,
					from_fb,
					status,
					admin_approved,
					mapped_to,
					system_profile,
					webp_flag) values(?,?,?,?,?,?,Now(),?,?,?,?,?,?,?,?,?,?) ";
			$param_array=array($this->userId,$name,$name_original,$original_height,$original_width,$name_thumbNail,
							$is_profile,$width,$height,'no', $from_fb, $status_to_set, 'no', $mapped_to,'no','yes');
			$tablename='user_photo';
			 $insert_id = Query_Wrapper::INSERT($query, $param_array, $tablename);
				
			/*$this->conn->Execute($this->conn->prepare("insert into user_photo (
					user_id, name,original_name,original_height,original_width,
					thumbnail,
					time_of_saving,
					is_profile,
					width,
					height,
					is_moderated,
					from_fb,
					status,
					admin_approved,
					mapped_to
					) values(?,?,?,?,?,?,Now(),?,?,?,?,?,?,?,?)"),
					array($this->userId,$name,$name_original,$original_height,$original_width,$name_thumbNail,
							$is_profile,$width,$height,'no', $from_fb, $status_to_set, 'no', $mapped_to));*/
			
			/*
			
			$this->conn->Execute($this->conn->prepare("insert into user_photo (
					user_id,name,original_name,
					thumbnail,
					time_of_saving,
					is_profile,
					width,
					height,
					is_moderated,
					from_fb,
					status,admin_approved) values(?,?,?,?,Now(),?,?,?,?,?,'active','no')"),
					array($this->userId,$name,$name_original,$name_thumbNail,
							$is_profile,$width,$height,'no',$from_fb));
            */
			

			Utils::saveOnS3($image_location,"files/images/profiles/".$name);
			Utils::saveOnS3($image_thumbNail_location,"files/images/profiles/".$name_thumbNail);
			Utils::saveOnS3($webp_image_location,"files/images/profiles/".$webp_name);
			Utils::saveOnS3($webp_image_thumbnail_location,"files/images/profiles/".$webp_name_thumbnail);

			unlink($image_location);
			unlink($image_thumbNail_location);
			unlink($webp_image_location);
			unlink($webp_image_thumbnail_location);

			if(isset($_SESSION['admin_id'])){
				$query["sql"] = "insert into user_photo (user_id,name,original_image,original_height,original_width,thumbnail,time_of_saving,is_profile,width,height,is_moderated,from_fb,status,blur_image,blur_thumbnail,admin_approved) values(?,?,?,Now(),?,?,?,?,?,'under_moderation',?,?,'no')";
				$query["parameter"] = "'$this->userId'.','.'$name'.','.'$name_original'.','.'$original_width'.','.'$name_thumbNail'.','.'$is_profile'.','.'$width'.','.'$height'.',no'.','.'$from_fb'.','.'$name_blur'.','.'$name_thumbNail_blur'";
				$query = json_encode($query);
				$step = "Added a new photo from Computer";
					
				$this->admin_log->log($_SESSION['admin_id'],$this->userId,$query,$step);
			}
			if($dont_return==false)
			{
				$encoded_data = $this->getUserPhotos();
				return $encoded_data;
			} else
			{
				return $insert_id;
			}
		//	$this->toadmin->notifytoadmin($this->userId);
		}catch(Exception $e) {
			echo $e->getMessage();
			$arr=array("fb_url"=>$fb_url,
					"name"=>$name,
					"image_location"=>$image_location,
					"file_name"=>$file_name
			);
			$arr=json_encode($arr);
			$redis->hset(Utils::$redis_keys["photomodule_error"],$this->userId,$arr);

			if($fb_url!=null)
			{
				trigger_error("PHP Fatal error Web: $fb_url ".$e->getMessage(), E_USER_WARNING);

				trigger_error("PHP Fatal error Web: $fb_url ".$e->getTraceAsString(), E_USER_WARNING);

			}
			else
			{
				trigger_error("PHP Fatal error Web(Without URL): ".$e->getMessage(), E_USER_WARNING);

				trigger_error("PHP Fatal error Web:(Without URL) ".$e->getTraceAsString(), E_USER_WARNING);

			}
		    //var_dump($e->getMessage());
			//die;
		}
	}
	
	public function updatePhotoStatus($photo_id, $status_to_set) 
	{
		
		$query="UPDATE user_photo SET status = ? WHERE user_id = ? and photo_id = ?";
		$param_array=array($status_to_set, $this->userId,$photo_id);
		$tablename='user_photo';
		$sql_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
		
		/*$sql = "UPDATE user_photo SET status = ? WHERE user_id = ? and photo_id = ?" ;
		$sql_prep = $this->conn->Prepare ($sql) ;
		$sql_exec = $this->conn->Execute ($sql_prep, array($status_to_set,  $this->userId, $photo_id)) ;*/
	}
	/**
	 * to get the Data of one Photo id
	 */
	public function checkOnePhoto ($photo_id)
	{
		$sql = "SELECT * FROM user_photo WHERE photo_id = ? and user_id = ?" ;
		$sql_prep = $this->conn->Prepare ($sql) ;
		$sql_exec = $this->conn->Execute ($sql, array($photo_id, $this->userId)) ;
		$res = $sql_exec->FetchRow();
		return $res ;
	}


	public function downloadFile($url,$location){
		$ch = curl_init ();		
		curl_setopt ( $ch, CURLOPT_URL, $url );
		//curl_setopt ( $ch, CURLOPT_REFERER, "http://www.example.org/yay.htm" );
		//curl_setopt ( $ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0" );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_TIMEOUT, 10 );
		$output = curl_exec ( $ch );
		$fp = fopen($location, 'w');
		fwrite($fp, $output);
		curl_close($ch);
		fclose($fp);
	}
	
	public function saveImage(){
		global $baseurl, $imageurl;
		$image_id = $_POST ['id'];
		
		try {
			if ($image_id == "from_fb") {
				$url = $this->getUserFbPic ();
				$url = json_decode($url,true);
				if ($url == false) {
					return;
				}
				//$this->addFromComputer(true,$url,true);
				for($i=0;$i<count($url);$i++){
					$this->addFromComputer(true,$url[$i],true);
				}
			//	$name=1000*microtime(true)."_".$this->random(18);
			//	$tmp_file = "/tmp/$name.jpg";
		//		$this->downloadFile ( $url,$tmp_file );
		//		$this->saveImageInSystem ( $tmp_file, $_POST ['naturalWidth'], $_POST ['width'], $_POST ['x'], $_POST ['y'], $_POST ['w'], $_POST ['h'], "yes", "yes" );
			} else {
				$sql = $this->conn->Prepare ( "select user_id,photo_id,is_profile,from_fb,name from user_photo where user_id=? and photo_id=?" );
				$rs = $this->conn->Execute ( $sql, array (
						$this->userId,
						$image_id 
				) );
	
				$row = $rs->FetchRow ();
				//var_dump($row);exit;
				if ($row ['user_id'] == $this->userId) {
					$file_name = $row ['name'];
					$url = $imageurl.$file_name;
					//if($row['is_profile'])
					$this->addFromComputer(true,$url,true,$row['is_profile'],true,$row['from_fb']);
					//$tmp_file = "/tmp/" . $file_name;
					//Utils::getFromS3 ( "files/images/profiles/" . $file_name, $tmp_file );
					//$this->saveImageInSystem ( $tmp_file, $_POST ['naturalWidth'], $_POST ['width'], $_POST ['x'], $_POST ['y'], $_POST ['w'], $_POST ['h'], $row ['is_profile'], $row ['from_fb'] );
						
					// set status=change of old photo
					
					$query="update user_photo set status='changed',is_profile='no' where  user_id=? and photo_id=?";
					$param_array=array (
							$this->userId,
							$image_id 
					);
					$tablename='user_photo';
					$sql_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
					
					
					
					/*$sql = $this->conn->Prepare ( "update user_photo set status='changed',is_profile='no' where  user_id=? and photo_id=?  " );
					$rs = $this->conn->Execute ( $sql, array (
							$this->userId,
							$image_id 
					) );*/
						if(isset($_SESSION['admin_id'])){
							$query["sql"] = "update user_photo set status='changed',is_profile='no' where  user_id=? and photo_id=?";
							$query["parameter"] = "'$this->userId'.','.'$image_id'";
							$query = json_encode($query);
							$step = "save photo";
						     
							$this->admin_log->log($_SESSION['admin_id'],$this->userId,$query,$step);
						}
				}
			}
			$encoded_data = $this->getUserPhotos ();
			return $encoded_data;
			//print_r ( json_encode ( $encoded_data ) );
		} catch ( Exception $e ) {
			
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			
			die ();
		}
	}
	
	public function blurImage($image){
		$blur_image_location=dirname(__FILE__)."/imageblur.png";
		$blur_image = WideImage::load($blur_image_location);
		
		$width=$image->getWidth();
		$height=$image->getHeight();
		
		$blur_image=$blur_image->resize($width,$height,"fill");
				
		$output_image = $image->merge($blur_image, 0, 0, 100);
		return $output_image;
	}
	
	public function saveAsProfileImage(){
		$image_id=$_POST['id'];
		try{
			$sql=$this->conn->Prepare("select user_id, photo_id, name, mapped_to from user_photo where user_id=? and photo_id=?
					and is_profile='no'");
			$rs=$this->conn->Execute($sql,array($this->userId,$image_id));
			$row=$rs->FetchRow();
			if($row['user_id']==$this->userId){
				
				
				$query="update user_photo set is_profile='no' where is_profile='yes' and user_id=?";
				$param_array=array($this->userId);
				$tablename='user_photo';
				$rs=Query_Wrapper::UPDATE($query, $param_array, $tablename);
				
				/*
				$query="update user_photo set system_profile='no' where system_profile='yes' and user_id=?";
				$param_array=array($this->userId);
				$tablename='user_photo';
				$rs=Query_Wrapper::UPDATE($query, $param_array, $tablename);
				*/
				//$sql=$this->conn->Prepare("update user_photo set is_profile='no' where is_profile='yes' and user_id=? ");
				//$rs=$this->conn->Execute($sql,array($this->userId));
				
				$query="update user_photo set is_profile='yes',time_of_saving=now() where user_id=? and photo_id=? ";
				$param_array=array($this->userId,$image_id);
				$tablename='user_photo';
				$rs=Query_Wrapper::UPDATE($query, $param_array, $tablename);
				
				/*$sql=$this->conn->Prepare("update user_photo set is_profile='yes',time_of_saving=now() where user_id=? and photo_id=? ");
				$rs=$this->conn->Execute($sql,array($this->userId,$image_id));*/
				
				if ( isset($row ['mapped_to']) && $row ['mapped_to'] != NULL)
				{
					// this photo iwas mapped to another photo set that old photo as profile pic as well
					
					$query="update user_photo set is_profile='yes',time_of_saving=now() where user_id=? and photo_id=?  ";
					$param_array=array($this->userId, $row ['mapped_to']);
					$tablename='user_photo';
					$rs=Query_Wrapper::UPDATE($query, $param_array, $tablename);
					
					//$sql=$this->conn->Prepare("update user_photo set is_profile='yes',time_of_saving=now() where user_id=? and photo_id=? ");
					//$rs=$this->conn->Execute($sql,array($this->userId, $row ['mapped_to']));
					
				}
				
			}
			if(isset($_SESSION['admin_id'])){
				$query["sql"] = "update user_photo set is_profile='yes',time_of_saving=now() where user_id=? and photo_id=?";
				$query["parameter"] = "'$this->userId'.','.'$image_id'";
				$query = json_encode($query);
				$step = "change profile picture";
					
				$this->admin_log->log($_SESSION['admin_id'],$this->userId,$query,$step);
			}
			$encoded_data = $this->getUserPhotos();
			return $encoded_data;
			//print_r(json_encode($encoded_data));
		}catch (Exception $e){
	
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	
			die;
		}
	}

	
	public function deleteImage(){
		global $baseurl,$imageurl;
		$image_id=$_POST['id'];	
		try{
			$sql=$this->conn->Prepare("select user_id,photo_id,name,mapped_to,system_profile from user_photo where user_id=? and photo_id=? 
					and (is_profile='no' or status='rejected')");
			$rs=$this->conn->Execute($sql,array($this->userId,$image_id));
			$row=$rs->FetchRow();
			if($row['user_id']==$this->userId){			
				
				$query="update user_photo set status='deleted' where  user_id=? and photo_id=? ";
				$param_array=array($this->userId,$image_id);
				$tablename='user_photo';
				$rs=Query_Wrapper::UPDATE($query, $param_array, $tablename);
				
				
				
					
				
				//$sql=$this->conn->Prepare("update user_photo set status='deleted' where  user_id=? and photo_id=? ");	
				//$rs=$this->conn->Execute($sql,array($this->userId,$image_id));
				if(isset($_SESSION['admin_id'])){
					$query["sql"] = "update user_photo set status='deleted' where  user_id=? and photo_id=?";
					$query["parameter"] = "'$this->userId'.','.'$image_id'";
					$query = json_encode($query);
					$step = "Photo Deleted";
						
					$this->admin_log->log($_SESSION['admin_id'],$this->userId,$query,$step);
				}

				if ( isset($row ['mapped_to'])  && $row ['mapped_to'] != NULL )
				{
					// if this photo was mapped to another photo then change the status of old photo to changed
					$this->updatePhotoStatus($row ['mapped_to'], 'changed') ;
				}
			}
			$encoded_data = $this->getUserPhotos();
			return $encoded_data;
			//print_r(json_encode($encoded_data));
		}catch (Exception $e){
				
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
				
			die;
		}
	}
		
	public function random($digits) {
		return str_pad ( rand ( 0, pow ( 10, $digits ) - 1 ), $digits, '0', STR_PAD_LEFT );
	}

	public function convertTowebP($src,$dest)
	{
		$size=getimagesize($src);
		switch($size["mime"]){
			case "image/jpeg":
				$im = imagecreatefromjpeg($src); //jpeg file
				break;
			case "image/gif":
				$im = imagecreatefromgif($src); //gif file
				break;
			case "image/png":
				$im = imagecreatefrompng($src); //png file
				break;
			default:
				$im=false;
				break;
		}
		if($im==false)
			return false;
		else {
			$webp =imagewebp($im, $dest);
			imagedestroy($im);
			return $webp;
		}

	}


}
?>
