<?php 
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname(__FILE__)."/../lib/wideimage/WideImage.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";

class PhotoUtils 
{
	static $MaxFileSize =  8388608;
	public function saveFileFromUrl($url, $saveDir, $compression=80, $width=0, $height=0) 
	{
		
		list($name,$name_thumbNail,$name_original,$image_location,$image_thumbNail_location,$image_original_location)= self::getImageNames();
		self::downloadFileFromURL($url, $image_location);
       $file_name=$image_location;
		$original_image = WideImage::load($file_name);
	
		
		$natural_width=$original_image->getWidth();
		// actual height of image i.e before compression
		$original_height=$original_image->getHeight();
		// // actual width of image i.e before compression
		$original_width=$natural_width;
		
	//	$image=$original_image->resize(640,640,"inside","down");
		if($width == 0 && $height ==0)
			$image=$original_image->resize($original_width,$original_height,"inside","down");
		else
			$image=$original_image->resize($width,$height,"inside","down");
		
		$image->saveToFile($image_location,$compression);
		$save_location = $saveDir . "/" .$name;
		$status = Utils::saveOnS3($image_location,"files/images/profiles/".$save_location);
		return $save_location ; 
		
	}
	
	public function saveImageForMsgs($image_source, $compression = 90, $angle = 0)
	{
		global $imageurl ;
		$img_size= self::validateImage($image_source);
		if ($img_size == FALSE)
		{ 
			return  array("error" => "Invalid Image");
		}
		list($name,$name_thumbNail,$name_thumbNail_webp,$image_location,$image_thumbNail_location,$image_thumbNail_location_webp,$webp_image,$webp_location)= self::getImageNames();
		$image_temp_name = $image_source['tmp_name'];
		$original_image = WideImage::load($image_temp_name);
		$image_path = "files/images/profiles/";

		$natural_width=$original_image->getWidth();
		$original_height=$original_image->getHeight();
		$original_width=$natural_width;
		$image=$original_image->resize(500,500,"inside","down")->rotate($angle) ;
	//	$image = $original_image->copy();
		$image->saveToFile($image_location, 80) ;
       // $webp_image_resource = imagecreatefromjpeg($image_location);
        self::convertTowebP($image_location,$webp_location);
		$thumbnail_image = $image->resize(40,40, "inside");
		$thumbnail_image->saveToFile($image_thumbNail_location,90);
		self::convertTowebP($image_thumbNail_location,$image_thumbNail_location_webp);
 		$name = "msgs/" . $name ;
 		$name_thumbNail = "msgs/" . $name_thumbNail ;
		$webp_image = "msgs/" . $webp_image ;
		$name_thumbNail_webp = "msgs/" . $name_thumbNail_webp;
		Utils::saveOnS3($image_location,  $image_path . $name);
		Utils::saveOnS3($image_thumbNail_location, $image_path . $name_thumbNail);
		Utils::saveOnS3($webp_location, $image_path . $webp_image);
		Utils::saveOnS3($image_thumbNail_location_webp, $image_path . $name_thumbNail_webp);
		$data = array("jpeg" =>  $name,
			     "jpeg_size" => intval(filesize($image_location)/1024). "KB",
			          "webp" =>  $webp_image ,
			     "webp_size" => intval(filesize($webp_location)/1024). "KB",
			     "thumbnail_jpeg" =>  $name_thumbNail,
			      "thumnnail_webp"=>  $name_thumbNail_webp);
		unlink($image_location);
		unlink($image_thumbNail_location);
		unlink($webp_location);
		unlink($image_thumbNail_location_webp);
	    return $data ;
		
	}
	
	public function getImageNames(){
		global $image_basedir; 
		$name=1000*microtime(true)."_".self::randomInt(18);
		$name_thumbNail_format=$name."_".self::randomInt(10);
		$name_thumbNail = $name_thumbNail_format . ".jpg" ;
		$name_thumbNail_webp = $name_thumbNail_format . ".webp" ;
		$name_original=$name."_original.jpg";
		$webp_name = $name . ".webp";
		$name=$name.".jpg";
		$image_location=$image_basedir.$name;
		$image_thumbNail_location_webp=$image_basedir. $name_thumbNail_webp;
		$image_thumbNail_location=$image_basedir.$name_thumbNail;
		$webp_location = $image_basedir . $webp_name ;
		return array($name,$name_thumbNail,$name_thumbNail_webp,$image_location,$image_thumbNail_location,$image_thumbNail_location_webp,$webp_name, $webp_location);
	}
	
	public function downloadFileFromURL($url,$location){
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		//curl_setopt ( $ch, CURLOPT_REFERER, "http://www.example.org/yay.htm" );
		//curl_setopt ( $ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0" );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_TIMEOUT, 10 );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
		$output = curl_exec ( $ch );
		$fp = fopen($location, 'w');
		fwrite($fp, $output);
		curl_close($ch);
		fclose($fp);
	}
	public function randomInt($digits) {
		return str_pad ( rand ( 0, pow ( 10, $digits ) - 1 ), $digits, '0', STR_PAD_LEFT );
	}
	
	
	private function validateImageType($file)
	{
		if(exif_imagetype($file['tmp_name'])===FALSE){
			//var_dump("type issues");
			throw new Exception("Not an Image File");
		}
	}
	
	private function validateImageUpload($file)
	{
		if ($file['error'] === UPLOAD_ERR_OK) {
			//return true;
		} else {
			//var_dump("upload issues");
			throw new Exception($file['error']);
		}
	}
	public  function validateImage($file)
	{
		try {
			// validating upload_error , extension , size of file
			self::validateImageUpload($file);
			self::validateImageType($file);
			$fsize=self::validateIMageSize($file);
	
		}catch (Exception $e) {
			trigger_error("PHP Fatal error Web:".$e->getMessage(), E_USER_WARNING);
	
			trigger_error("PHP Fatal error Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
		return $fsize;
	}
	
	private function validateImageSize($file)
	{
		$file_name=$_FILES["file"]['tmp_name'];
		$fsize = intval($file["size"]);

		if($fsize==0||$fsize> self::$MaxFileSize){
			//var_dump("size issues");
			throw new Exception("File Size should be less than ".self::$MaxFileSize." .Current Size: $fsize");
		}
		return $fsize;
	}


	public static function convertTowebP($src,$dest)
	{
		$size=getimagesize($src);
		switch($size["mime"]){
			case "image/jpeg":
				$im = imagecreatefromjpeg($src); //jpeg file
				break;
			case "image/gif":
				$im = imagecreatefromgif($src); //gif file
				break;
			case "image/png":
				$im = imagecreatefrompng($src); //png file
				break;
			default:
				$im=false;
				break;
		}
		if($im==false)
			return false;
		else {
			$webp =imagewebp($im, $dest);
			imagedestroy($im);
			return $webp;
		}

	}
}






?>
