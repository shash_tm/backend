<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if (PHP_SAPI === 'cli')
	{

		$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$sql = $conn->Execute("select user_id,travel_favorites from interest_hobbies"); 
	
		$data = $sql->GetRows();
	
		//var_dump($data);
	
	
		foreach($data as $key=>$value){
		
			$travel_food = array();
			if(isset($data[$key]['travel_favorites']) && $data[$key]['travel_favorites']!='null') {
				$travel = json_decode($data[$key]['travel_favorites'],true);
				foreach($travel as $k=>$val){
					if(is_array($val)) {
						$travel_food[$k] = $val[0];
					}
					else {
						$travel_food[$k] = $val;
					}
				}
				
			}else {
				$travel_food = NULL;
			}
		
			if(count($travel_food)>0) {
				$travelData = json_encode($travel_food);
			}else {
				$travelData = NULL;
			}
			$conn->Execute($conn->prepare("update interest_hobbies SET travel_favorites=? where user_id=? "),
					array($travelData,$data[$key]['user_id']));
		
		}	
		echo count($data)." rows updated";
	}
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}

?>