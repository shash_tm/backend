<?php 
require_once (dirname ( __FILE__ ) . '/../include/config.php');
require_once (dirname ( __FILE__ ) . '/../HashTag.class.php');
require_once (dirname ( __FILE__ ) . '/../abstraction/query_wrapper.php');

class registerDBO{
	
	public static function insertUserBasicData($values,$source='', $version){
		global $conn;
		
		if(($source == 'iOSApp' && $version>121) || ($source == 'androidApp' && $version>=100) || ($source == 'windows_app' && $version>1304)) {
			if(isset($values ['TM_S3_StayState']) || isset($values ['TM_S3_StayCity'])) {
				
				$query="insert into user_data(user_id,stay_country,stay_state,stay_city,height,
				designation,work_status,highest_degree,tstamp) values(?,?,?,?,?,?,?,?,NOW()) on Duplicate KEY UPDATE stay_country=?,
					stay_state=?,stay_city=?,height=?,designation=?,work_status=?,highest_degree=?,tstamp=NOW()";
				$param_array=array($values['user_id'],$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
								$values['height'],$values['TM_Designation_0'],$values['TM_Working_area'],$values['TM_Education_0'],
								$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
								$values['height'],$values['TM_Designation_0'],$values['TM_Working_area'],$values['TM_Education_0']);
				$tablename='user_data';
				Query_Wrapper::INSERT($query, $param_array, $tablename);
				
				/*$conn->Execute($conn->prepare("insert into user_data(user_id,stay_country,stay_state,stay_city,height,
				designation,work_status,highest_degree,tstamp) values(?,?,?,?,?,?,?,?,NOW()) on Duplicate KEY UPDATE stay_country=?,
					stay_state=?,stay_city=?,height=?,designation=?,work_status=?,highest_degree=?,tstamp=NOW()"),
						array($values['user_id'],$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
								$values['height'],$values['TM_Designation_0'],$values['TM_Working_area'],$values['TM_Education_0'],
								$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
								$values['height'],$values['TM_Designation_0'],$values['TM_Working_area'],$values['TM_Education_0']));*/
			}
			else {
				
				
				$query="insert into user_data(user_id,stay_country,height,
				designation,work_status,highest_degree,tstamp) values(?,?,?,?,?,?,NOW()) on Duplicate KEY UPDATE stay_country=?,
					height=?,designation=?,work_status=?,highest_degree=?,tstamp=NOW()";
				$param_array=array($values['user_id'],$values ['TM_S3_StayCountry'],$values['height'],$values['TM_Designation_0'],$values['TM_Working_area'],
								$values['TM_Education_0'],$values ['TM_S3_StayCountry'],$values['height'],$values['TM_Designation_0'],
								$values['TM_Working_area'],$values['TM_Education_0']);
				$tablename='user_data';
				Query_Wrapper::INSERT($query, $param_array, $tablename);
				
				/*$conn->Execute($conn->prepare("insert into user_data(user_id,stay_country,height,
				designation,work_status,highest_degree,tstamp) values(?,?,?,?,?,?,NOW()) on Duplicate KEY UPDATE stay_country=?,
					height=?,designation=?,work_status=?,highest_degree=?,tstamp=NOW()"),
						array($values['user_id'],$values ['TM_S3_StayCountry'],$values['height'],$values['TM_Designation_0'],$values['TM_Working_area'],
								$values['TM_Education_0'],$values ['TM_S3_StayCountry'],$values['height'],$values['TM_Designation_0'],
								$values['TM_Working_area'],$values['TM_Education_0']));*/
			}
			
			$values['stay_country'] = $values['TM_S3_StayCountry'].';';
			$preLocArray = self::getDefaultLocationPrefereence($values['user_id']);

			
			$query="insert into user_preference_data(user_id,start_height,end_height,location,location_preference_for_matching,tstamp)
				values(?,?,?,?,?,NOW()) on DUPLICATE KEY UPDATE start_height=?,end_height=?,location=?,location_preference_for_matching=?,tstamp=NOW()";
			$param_array=array($values ['user_id'],$values['start_height'],$values['end_height'],$values['stay_country'],$values['TM_S3_StayCountry'],
							$values['start_height'],$values['end_height'],$preLocArray['location'],$preLocArray['location_preference_for_matching']);
			$tablename='user_preference_data';
			Query_Wrapper::INSERT($query, $param_array, $tablename);
			
			
			/*$conn->Execute($conn->prepare("insert into user_preference_data(user_id,start_height,end_height,location,location_preference_for_matching,tstamp)
				values(?,?,?,?,?,NOW()) on DUPLICATE KEY UPDATE start_height=?,end_height=?,location=?,location_preference_for_matching=?,tstamp=NOW()"),
					array($values ['user_id'],$values['start_height'],$values['end_height'],$values['stay_country'],$values['TM_S3_StayCountry'],
							$values['start_height'],$values['end_height'],$values['stay_country'],$values['TM_S3_StayCountry']));*/
		}
		else {
			
			$query="insert into user_data(user_id,DateOfBirth,marital_status,haveChildren,religion,mother_tongue,
				stay_country,stay_state,stay_city,height,smoking_status,drinking_status,food_status,income_start,income_end,industry,
				designation,work_status,company_name,highest_degree,institute_details,tstamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW()) on Duplicate KEY UPDATE DateOfBirth=?,
				marital_status=?,haveChildren=?,religion=?,mother_tongue=?,stay_country=?,stay_state=?,stay_city=?,height=?,smoking_status=?,drinking_status=?,food_status=?,income_start=?,income_end=?,industry=?,
				designation=?,work_status=?,company_name=?,highest_degree=?,institute_details=?,tstamp=NOW()";
			$param_array=array($values['user_id'],$values['dob'],$values['TM_Marital_status'],$values['TM_HaveChildren'],$values ['TM_S2_Religion'],
						$values ['TM_S3_MotherTongue'],$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
						$values['height'],$values['TM_S2_Smoking_status'],$values['TM_S2_Drinking_status'],$values['TM_S2_Food_status'],
						$values['TM_Income_start'],$values['TM_Income_end'],$values['TM_Industry_0'],$values['TM_Designation_0'],
						$values['TM_Working_area'],$values['TM_Company_name_0'],$values['TM_Education_0'],$values['TM_Institute_0'],$values['dob'],$values['TM_Marital_status'],$values['TM_HaveChildren'],$values ['TM_S2_Religion'],
						$values ['TM_S3_MotherTongue'],$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
						$values['height'],$values['TM_S2_Smoking_status'],$values['TM_S2_Drinking_status'],$values['TM_S2_Food_status'],
						$values['TM_Income_start'],$values['TM_Income_end'],$values['TM_Industry_0'],$values['TM_Designation_0'],
						$values['TM_Working_area'],$values['TM_Company_name_0'],$values['TM_Education_0'],$values['TM_Institute_0']);
			$tablename='user_data';
			Query_Wrapper::INSERT($query, $param_array, $tablename);
				
			
			
			/*$conn->Execute($conn->prepare("insert into user_data(user_id,DateOfBirth,marital_status,haveChildren,religion,mother_tongue,
				stay_country,stay_state,stay_city,height,smoking_status,drinking_status,food_status,income_start,income_end,industry,
				designation,work_status,company_name,highest_degree,institute_details,tstamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW()) on Duplicate KEY UPDATE DateOfBirth=?,
				marital_status=?,haveChildren=?,religion=?,mother_tongue=?,stay_country=?,stay_state=?,stay_city=?,height=?,smoking_status=?,drinking_status=?,food_status=?,income_start=?,income_end=?,industry=?,
				designation=?,work_status=?,company_name=?,highest_degree=?,institute_details=?,tstamp=NOW()"),
				array($values['user_id'],$values['dob'],$values['TM_Marital_status'],$values['TM_HaveChildren'],$values ['TM_S2_Religion'],
						$values ['TM_S3_MotherTongue'],$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
						$values['height'],$values['TM_S2_Smoking_status'],$values['TM_S2_Drinking_status'],$values['TM_S2_Food_status'],
						$values['TM_Income_start'],$values['TM_Income_end'],$values['TM_Industry_0'],$values['TM_Designation_0'],
						$values['TM_Working_area'],$values['TM_Company_name_0'],$values['TM_Education_0'],$values['TM_Institute_0'],$values['dob'],$values['TM_Marital_status'],$values['TM_HaveChildren'],$values ['TM_S2_Religion'],
						$values ['TM_S3_MotherTongue'],$values ['TM_S3_StayCountry'],$values ['TM_S3_StayState'],$values ['TM_S3_StayCity'],
						$values['height'],$values['TM_S2_Smoking_status'],$values['TM_S2_Drinking_status'],$values['TM_S2_Food_status'],
						$values['TM_Income_start'],$values['TM_Income_end'],$values['TM_Industry_0'],$values['TM_Designation_0'],
						$values['TM_Working_area'],$values['TM_Company_name_0'],$values['TM_Education_0'],$values['TM_Institute_0']));*/
			
			$values['stay_country'] = $values['TM_S3_StayCountry'].';';
			
			
			$query="insert into user_preference_data(user_id,start_age,end_age,start_height,end_height,location,location_preference_for_matching,tstamp)
				values(?,?,?,?,?,?,?,NOW()) on DUPLICATE KEY UPDATE start_age=?,end_age=?,start_height=?,end_height=?,location=?,location_preference_for_matching=?,tstamp=NOW()";
			$param_array=array($values ['user_id'],$values ['TM_S1_LookingAgeStart'],$values ['TM_S1_LookingAgeEnd'],$values['start_height'],$values['end_height'],$values['stay_country'],$values['TM_S3_StayCountry'],
							$values ['TM_S1_LookingAgeStart'],$values ['TM_S1_LookingAgeEnd'],$values['start_height'],$values['end_height'],$values['stay_country'],$values['TM_S3_StayCountry']);
			$tablename='user_preference_data';
			Query_Wrapper::INSERT($query, $param_array, $tablename);
				
			/*$conn->Execute($conn->prepare("insert into user_preference_data(user_id,start_age,end_age,start_height,end_height,location,location_preference_for_matching,tstamp)
				values(?,?,?,?,?,?,?,NOW()) on DUPLICATE KEY UPDATE start_age=?,end_age=?,start_height=?,end_height=?,location=?,location_preference_for_matching=?,tstamp=NOW()"),
					array($values ['user_id'],$values ['TM_S1_LookingAgeStart'],$values ['TM_S1_LookingAgeEnd'],$values['start_height'],$values['end_height'],$values['stay_country'],$values['TM_S3_StayCountry'],
							$values ['TM_S1_LookingAgeStart'],$values ['TM_S1_LookingAgeEnd'],$values['start_height'],$values['end_height'],$values['stay_country'],$values['TM_S3_StayCountry']));*/
		}

		if(isset($values['TM_interest'])) {
			$hashArr = json_decode($values['TM_interest'],true);
			$newHashTag = array();
			foreach ($hashArr as $val) {
                    		$newHashTag[] = strip_tags($val);
			}
			$interest_new = json_encode($newHashTag);
		}
		
		$query="insert into interest_hobbies(user_id,preferences,tstamp) values(?,?,NOW()) on DUPLICATE KEY UPDATE preferences=?,tstamp=NOW()";
		$param_array=array($values['user_id'],$interest_new,$interest_new);
		$tablename='interest_hobbies';
		Query_Wrapper::INSERT($query, $param_array, $tablename);
			
		
		/*$conn->Execute($conn->prepare("insert into interest_hobbies(user_id,preferences,tstamp) values(?,?,NOW()) on DUPLICATE KEY UPDATE preferences=?,tstamp=NOW()"),array($values['user_id'],$values['TM_interest'],$values['TM_interest']));*/
		
		
		if($source == 'iOSApp' || $source == 'windows_app' || ($source == 'androidApp' && $version>=100)){
			
			$query="update user set steps_completed='basics,photo' where user_id=?";
			$param_array=array($values['user_id']);
			$tablename='user';
			Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
			/*$conn->Execute("update user set steps_completed='basics,photo' where user_id=".$values['user_id']);*/
		}
		else
		{	
			$query="update user set steps_completed='basics' where user_id=?";
			$param_array=array($values['user_id']);
			$tablename='user';
			Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
			/*$conn->Execute("update user set steps_completed='basics' where user_id=".$values['user_id']);*/
		}
		
		$hashArr = json_decode($values['TM_interest'],true);
		$hashTag = new HashTag($values['user_id']);
		$hashTag->saveHashTag($hashArr);
	}

	private static function getDefaultLocationPrefereence($user_id)
	{
		global $redis ;
		$countries = $redis->sMembers('active_regions');
		$countries[] = 113;
		$location = implode(';',$countries) . ";" ;
		$location_for_matching = implode(',',$countries);
		return array('location' => $location,
					'location_preference_for_matching' => $location_for_matching);
	}
	
	public static function updateUserPhoto($user_id){
		global $conn;
		
		$query="update user set steps_completed='basics,photo' where user_id=?";
		$param_array=array($user_id);
		$tablename='user';
		Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
		/*$conn->Execute("update user set steps_completed='basics,photo' where user_id=".$user_id);*/
	}
}

?>
