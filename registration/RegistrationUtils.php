<?php 
class RegistrationUtils{	


	public function getTraitPreferences($preference,$value_array){
		if(isset($preference)){
			$preference=explode(",",$preference);
			$max_id=-1;
			for($i=0;$i<count($value_array);$i++){
				if(in_array($value_array[$i],$preference)){
					$max_id=$i>$max_id?$i:$max_id;
				}
			}
			$max_id--;
			if($max_id>=0){
				$preference[]=$value_array[$max_id];
			}
			$preference=array_unique($preference);
	
			$preference=implode(",",$preference);
		}
		
		return $preference;
	}
	
	public function getTraitPreferencesBody($preference,$value_array,$bodyTypesCombined){	
		
		if(isset($preference)){
			$preference=explode(",",$preference);
			//$max_id=-1;
			$combine_all=false;
			for($i=0;$i<count($preference);$i++){
				if(in_array($preference[$i],$bodyTypesCombined)){
					$combine_all=true;
					break;
				}
			}
			
			if($combine_all==true){
				$preference=array_unique(array_merge($preference,$bodyTypesCombined));
				$preference=implode(",",$preference);
			}
						
			else{
			//	var_dump($preference);
				return $this->getTraitPreferences(implode(",",$preference),$value_array);
				//var_dump(implode(",",$preference));
			}
			
		}
		return $preference;
	}

	public function getWorkAreaPreferences($preference,$working_areas_combined){
		if(isset($preference)){
			$preference=explode(",",$preference);
			//$max_id=-1;
			$combine_all=false;
			for($i=0;$i<count($preference);$i++){
				if(in_array($preference[$i],$working_areas_combined)){
					$combine_all=true;
					break;
				}
			}
			if($combine_all==true){
				$preference=array_unique(array_merge($preference,$working_areas_combined));
			}
			$preference=implode(",",$preference);
		}
		return $preference;
	}
	//$work_area_preference=$this->utils->getWorkAreaPreferences($values['TM_Working_area_spouse'],$working_areas_combined);
	
}

?>