<?php
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/header.php";
require_once dirname ( __FILE__ ) . "/utilities/counter.php";



class MutualLikes {

	private $user_id;
	private $numberOfResults = 10;
	private $conn;
	private $user_utils_dbo;
	private $user;
	private $user_utils;
	private $mutual_like_key;
	private $message_key;
	private $mutual_like_list_key;
	private $redis;
	
	function __construct($user) {
		global $conn, $redis;
		$this->user=$user;
		$this->user_id = $user->getUserID();
		$this->conn=$conn;
		$this->user_utils_dbo = new userUtilsDBO();
		$this->user_utils=new UserUtils();
		$this->mutual_like_key = Utils::$redis_keys['mutual_like_notification'] . $this->user_id;
		$this->message_key = Utils::$redis_keys['messages'] . $this->user_id;
		$this->mutual_like_list_key = Utils::$redis_keys['mutual_like_only_list'] . $this->user_id;
				
		$this->redis = $redis;
	}

	public function setSeen(){
		$counter=new Counters();
		$counter->clearMutualLikeCounter($this->user_id);
		//$this->redis->DEL($this->mutual_like_key);
		/*
		$mutual_like_ids = 	$this->redis->SMEMBERS($this->mutual_like_list_key);
		//$this->redis->SREM($this->message_key, $mutual_like_ids);
		$this->redis->DEL($this->mutual_like_list_key);
		
		$args[0] = $this->message_key;
		$i = 1;
		foreach ($mutual_like_ids as $value) {
			$args[$i] = $value;
			$i++;
		}
 		call_user_func_array(array($this->redis,'SREM'),$args);
 		*/
	}

	public function getMutualLikes($page_id){
		global $baseurl;
		/*
		$page_id=intval($page_id);
		$start_limit=$page_id*10;
		$end_limit=11;
		*/
		//TODO: To put the limit
		$sql=$this->conn->Prepare("select user1,user2,DATE(timestamp) as matched_on,timestamp as matched_timestamp,
				blocked_by,blocked_shown,suspended_shown from user_mutual_like where (user1=? or user2=?) and 
				(blocked_by is null or blocked_by!=?) 
				and blocked_shown is null order by timestamp desc 
				");
		
		//var_dump($this->user_id);
		$rs=$this->conn->Execute($sql,array($this->user_id,$this->user_id,$this->user_id));
	//	echo "<pre>";
		//$data=$rs->GetRows();	
		//var_dump($
		$user_array=array();
		$mutual_data=array();
		foreach ($rs->GetRows() as $val){
			$other_user_id=($val['user1']==$this->user_id)?$val['user2']:$val['user1'];
			$mutual_data[$other_user_id]['matched_on']=$val['matched_on'];
			$mutual_data[$other_user_id]['matched_timestamp']=strtotime($val['matched_timestamp']);
			$mutual_data[$other_user_id]['is_blocked']=isset($val['blocked_by'])?true:false;
			$mutual_data[$other_user_id]['suspended_shown']=isset($val['suspended_shown'])?true:false;
			$user_array[]=($val['user1']==$this->user_id)?$val['user2']:$val['user1'];
		}

		$user_data=$this->user_utils_dbo->getTileDate($user_array);
		$my_gender=$this->user->fetchGender();
		$other_gender=($my_gender=="F")?"M":"F";
		$result=array();
		/*
		echo "<pre>";
		var_dump($user_data);
		exit;
		*/
		//to run for loop the variable shud be an array first
		if (is_array($user_data)){
			foreach($user_data as $val){
				$data=array();
					
				if($val['status'] == 'blocked') continue;
				
				$user_id=$val['user_id'];
				$data['user_id']=$user_id;
				$data['matched_timestamp']=$mutual_data[$user_id]['matched_timestamp'];;
				$data['profile_data']['fname']=$val['fname'];
				$data['profile_data']['age']=$val['age'];
				$data['profile_data']['city']=($val['stay_city_display']==null)?$val['state']:$val['stay_city_display'];
				$data['profile_data']['matched_on']=$mutual_data[$user_id]['matched_on'];
				if(isset($val['thumbnail'])){
					$data['profile_data']['profile_pic']=$this->user_utils->getImageFullPathFromDB($val['thumbnail']);
				}
				else
				{
					$data['profile_data']['profile_pic']=$this->user_utils->getDummyImage($other_gender);
				}
				$data['profile_data']['profile_id']=$user_id;
				if($mutual_data[$user_id]['is_blocked']==true){
					$data['profile_data']['is_blocked']='yes';
					$data['profile_data']['block_shown_url']=$baseurl."/blocked_shown.php?uid=".$user_id."&bs=1";
				}
				//check
		/*		else if(($val['status']=="suspended"||$val['status']=="blocked")){
					if($mutual_data[$user_id]['suspended_shown']!=true){
							
						$data['profile_data']['is_blocked']='yes';
						$data['profile_data']['ss']='1';
						$data['profile_data']['block_shown_url']=$baseurl."/blocked_shown.php?uid=".$user_id."&ss=1";
					}
					//else don't show this profile
					else continue;
				}*/
				else{
					$data['profile_data']['profile_link']=$this->user_utils->generateProfileLink($this->user_id,$user_id)."&from_mutual_like=true";
					$data['profile_data']['message_link']=$this->user_utils->generateMessageLink($this->user_id,$user_id);

					//$data['profile_data']['auth_key']=$this->user_utils->getCheckSumForProfileVisit($this->user_id,$user_id);
					//show user the profile details so that user can show ids
				}
				$result[]=$data;
			}
		}
		//usort($result,array("MutualLikes","sortMutualLikes('matched_timestamp')"));
		
		usort($result,array("MutualLikes","sortLikes"));
	//	echo "<pre>";
	//	var_dump($result);
		
	
		
		return $result;
	 	//var_dump($user_data);
		
	}
	
	static function sortLikes($a,$b){
		return $b["matched_timestamp"] - $a["matched_timestamp"];
	}
	
	static function sortMutualLikes($key) {
		return function ($a, $b) use($key) {
			if ($a == $b) {
				return 0;
			}
			return ($a < $b) ? - 1 : 1;
		};
	}
	
	
	
}


try {
	//get user details from session should return if user is valid or not

	$device_type = Utils::getDeviceType();
	if($device_type == "mobile") $flag = true; else $flag=false;
	functionClass::redirect("likes", $flag);
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$user=new UserData($user_id);
	//here we shold check if we have to proceed ahead
	//we should check here is it is valid
	
	$MutualLikes=new MutualLikes($user);
	$MutualLikes->setSeen();
	$data=$MutualLikes->getMutualLikes($_REQUEST['page_id']);
	$device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
	if ($device_type == 'mobile') {
		$response=array();
		$response['responseCode']=200;
		$response['data']=$data;
		
		echo json_encode($response);
		exit;
	}
	//call from desktop
	//echo "<pre>";

	//var_dump($data);
		
	$header = new header($user_id);
	$header_values = $header->getHeaderValues();
	$smarty->assign("header", $header_values);
	$smarty->assign("mutual_matches",$data);
	$smarty->assign("my_id", $user_id);
	
	$smarty->display("templates/dashboard/mutual_like.tpl");
	
	//$status = $user['status'];

	
}
catch(Exception $e){
	//echo $e->getMessage();
	trigger_error("php_script_error:".$e->getTraceAsString(), E_USER_WARNING);
}

?>








