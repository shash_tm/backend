<?php

require_once  (dirname ( __FILE__ ) . '/../TMObject.php');
require_once  (dirname ( __FILE__ ) . '/../include/header.php');
require_once  (dirname ( __FILE__ ) . '/../abstraction/query_wrapper.php');

class Interesthobbies extends TMObject{

	private $userId;


	public function __construct($user_id=NULL) {
		parent::__construct ( $user_id );
		$this->userId = $user_id;
	}


	public function getDBData(){
		$toSelect = array();
		$rs=$this->conn->Execute("select * from interest_hobbies where user_id='".$this->userId."'");
		
		if($rs->_numOfRows>0) {
			$alreadySelected=$rs->FetchRow();
			
			$toSelect['music']['favorites']=json_decode(str_replace("\\\"","",$alreadySelected['music_favorites']));
			$toSelect['movies']['favorites']=json_decode(str_replace("\\\"","",$alreadySelected['movies_favorites']));
			$toSelect['travel']['favorites']=json_decode(str_replace("\\\"","",$alreadySelected['travel_favorites']));
			$toSelect['others']['favorites']=json_decode(str_replace("\\\"","",$alreadySelected['other_favorites']));
			$toSelect['books']['favorites']=json_decode(str_replace("\\\"","",$alreadySelected['books_favorites']));

		} else {
			$toSelect=json_decode('{}');
		}
		return $toSelect;
	}

	public function checkFBConnected(){
		$rs=$this->conn->Execute("select fid from user_facebook where user_id='".$this->userId."'");
		if($rs->_numOfRows>0) {
			return true;
		}else{
			return false;
		}
	}


	//public function initializeIH($smarty,$interests){
	public function initializeIH($smarty,$from_mobile=false,$rd='',$stepCompleted,$reg=false){
		$header = new header($this->userId);
		$header_values = $header->getHeaderValues();
		//$smarty->assign('allInterests',json_encode($interests));
		$fb_likes=$this->conn->Execute("select name,category_name from fb_likes")->GetRows();

		foreach($fb_likes as $fb_like){
			$allLikes[strtolower($fb_like['name'])]=$fb_like['category_name'];
		}
			
		

		$toSelect = $this->getDBData();

		if($from_mobile){
			$response['responseCode'] = 200;
			$response['page'] = 'hobby';
			$response['favourites'] = $toSelect;
			
			$resource=$response;
			unset($resource['responseCode']);
			$resource=json_encode($resource);
			$hashCheck=functionClass::checkHash($resource,$_REQUEST['hash']);
				
			//var_dump($hashCheck);
				
			if($hashCheck['status']==false)
				$response['hash']=$hashCheck['hash'];
				else
				$response=array("responseCode"=>304);
		    $response["tstamp"]=time();
			print_r(json_encode($response));die;
		}else{
			if($this->checkFBConnected()) {
				$smarty->assign('fb_loaded',true);
			}
			$smarty->assign('uid',$this->userId);
			$smarty->assign('header',$header_values);
			$smarty->assign ( "rdUrl", $rd );
			$smarty->assign ( "stepCompleted", $stepCompleted );
			if($reg)
				$smarty->assign("fromRegister",'1');
			$smarty->assign('fb_likes',json_encode($allLikes));
			$smarty->assign('toSelect',json_encode($toSelect ,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
		}
		
	}

	public  function saveInterestAndHobbies(){
		try {
		$data=json_decode($_REQUEST['data'],true);
		//$preferences=$data['preferences'];
		$favourites_new = array();
		$favourites=$data['favourites'];
		$categories=array("music","food","sports","movie","travel","read","others");

		foreach($favourites as $category=>$data){
			foreach($data as $key=>$value){
				//$favourites[$category][htmlentities($key)]=htmlentities($value);
				$favourites_new[$category][strip_tags($key)]=strip_tags($value);
			}
		}

		$music_favorites=json_encode($favourites_new['music']);
		$books_favorites=json_encode($favourites_new['books']);
		$others_favorites=json_encode($favourites_new['others']);
		$movie_favorites=json_encode($favourites_new['movies']);
		$travel_favorites=json_encode($favourites_new['travel']);

		
		
		$query="insert into interest_hobbies (
				user_id,music_favorites,books_favorites,movies_favorites,other_favorites,travel_favorites)
				values(?,?,?,?,?,?) on duplicate key update music_favorites = ?,books_favorites =?,
				movies_favorites = ?,other_favorites =?,travel_favorites =?";
		$param_array=array($this->userId,$music_favorites,$books_favorites,
						$movie_favorites,$others_favorites,$travel_favorites,
						$music_favorites,$books_favorites,$movie_favorites,
						$others_favorites,$travel_favorites);
		$tablename='interest_hobbies';
		Query_Wrapper::INSERT($query, $param_array, $tablename);
		
		/*$this->conn->Execute($this->conn->prepare("insert into interest_hobbies (
				user_id,music_favorites,books_favorites,movies_favorites,other_favorites,travel_favorites)
				values(?,?,?,?,?,?) on duplicate key update music_favorites = ?,books_favorites =?,
				movies_favorites = ?,other_favorites =?,travel_favorites =?"),
				array($this->userId,$music_favorites,$books_favorites,
						$movie_favorites,$others_favorites,$travel_favorites,
						$music_favorites,$books_favorites,$movie_favorites,
						$others_favorites,$travel_favorites));*/
		
		$sql = $this->conn->Prepare("select steps_completed from user where user_id=?");
		$exe = $this->conn->Execute($sql,array($this->userId));
		$data = $exe->FetchRow();
		$steps = explode ( ",", $data['steps_completed']);
		if(!in_array('hobby',$steps)){
			$steps[] = 'hobby';
			$steps_completed = implode(',',$steps);	
			
			$query="update user set steps_completed=? where user_id=?";
			$param_array=array($steps_completed,$this->userId);
			$tablename='user';
			Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
			/*$this->conn->Execute($this->conn->prepare("update user set steps_completed=? where user_id=?"),array($steps_completed,$this->userId));*/
		}
		
		//$this->conn->Execute("update user set steps_completed='basics,photo,psycho1,psycho2,hobby' where user_id=".$this->userId);
		
		}catch(Exception $e){
			//console.log($e->getMessage());
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
		
	}

}
?>