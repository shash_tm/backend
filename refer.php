<?php
require_once dirname(__FILE__) . "/include/config.php";
try
{
    $smarty->assign ("baseurl" , $baseurl) ;
   // var_dump(getallheaders());
    $headers = getallheaders();
    $deep_link = $_REQUEST['deep_link'];
    if(isset($deep_link) && ($deep_link == true || $deep_link == 'true')){
        $deep_link = true;
    }
    if(!isset($headers['app_version_code']) && !$deep_link){
        $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
        if($headers['source'] == 'androidApp' || stripos($ua,'android') !== false)
            header('Location: '.'https://play.google.com/store/apps/details?id=com.trulymadly.android.app');
        else
            header('Location: '.'https://itunes.apple.com/in/app/trulymadly/id964395424?mt=8');
    }
    $smarty->display ( "templates/refer.tpl" );

} catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_WARNING);
}