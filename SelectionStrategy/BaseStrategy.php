<?php

/**
 * The class BaseStrategy is used as an abstraction for various strategies that
 * shall be used to run an AB Test.
 */
interface BaseStrategy {
    
    /* 
     * The function selectImpl implements the strategy that should be employed
     * for selection in an AB test.
     * @param - $config - an array containing key value pairs where 
     *                    key = names/identifier/classname of an AB
     *                    value = % coverage for the AB.
     */
    public function selectImpl($configPercentFilters);
}

?>