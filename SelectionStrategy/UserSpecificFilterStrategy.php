<?php
require_once dirname ( __FILE__ ) . "/BaseStrategy.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";

/**
 * UserSpecificStrategy is an implementation for selection of an AB.
 * The strategy shall assign an AB to a specified % of users i.e. each user 
 * shall be assigned an AB and any analysis shall happen across users. The AB group assigned
 * will depend on last 2 digits of the user_id.
 */
class UserSpecificFilterStrategy implements BaseStrategy {
    private $selected_strategy = null;
    private $selected_index = 0;
 
	#\TODO  
    public function selectImpl($configPercentFilters, $userId = -1, $gender = "Female", $age = -1, 
    $location = "", $city = "",  $sessionId = "") {
    	// Need a deterministic algorithm that returns the same recommender for a user
    	// using the config object.
    	// The function will require all the user_ids for selection.
    	//1. Select a recommender system
    	$configPercentFiltersRemainder = array_filter($configPercentFilters, function($x) use ($gender,$age,$location, $city, $sessionId, $userId) {
    		error_reporting(E_ERROR);
    		$filter = json_decode($x[1], true);
    		$header = Utils::getAllSystemHeaderFields();
    		
    		// select this Recommendation Engine only mutual_friends = true and batchCount = 2 (Please see config_ab.ini)
    		$mutual_friends = isset($header['mutual_friends']) ? $header['mutual_friends'] : False;
    		$batchCount = isset($header['batchCount']) ? $header['batchCount'] : -1;
    		
    		if ($filter == NULL |
    				(  ($filter['Gender'] == NULL   | in_array($gender, $filter['Gender']))
    				& ($filter['Age'] == NULL      | in_array($age, $filter['Age']))
    				& ($filter['Location'] == NULL | in_array($location, $filter['Location']))
    				& ($filter['City'] == NULL | in_array($city, $filter['City']))
    				& ($filter['Session'] == NULL | in_array($sessionId, $filter['Session']))
    				& ($filter['mutual_friends'] == NULL | ($mutual_friends && $filter['mutual_friends']))
    				& ($filter['batchCount'] == NULL | ($filter['batchCount'] == $batchCount))
    				)) { 
						if (isset($filter['test_users']) && in_array($userId, $filter['test_users'])) {
							$this->selected_strategy = $x; // If user id in test users, set this
						}
    					return $x;
    				}
			if ($this->selected_strategy == null)
				$this->selected_index ++;
	    	});
    	
 
    	// test ids for Recommendation Engine only considers filters other than % of AB group
    	if ($this->selected_strategy != null && $this->selected_index < count($configPercentFilters)) {
    		$orig_remainder = array_keys($configPercentFilters);
    		$selectedStrategy = $orig_remainder[$this->selected_index];
    		return $selectedStrategy;
    	}
    	
    	$numAvailStrategies = sizeof($configPercentFiltersRemainder);
    	$selectedIndex = -1;
    	if ($configPercentFiltersRemainder != NULL) {
    		$arrayPercentValues = array_map(function($x) {
    			return $x[0];
    		}, $configPercentFiltersRemainder);
    		// Calculate the cumsum
    		$cumsum = array();
    		for ($i = 1; $i < $numAvailStrategies; $i++) {
    			$cumsum[] = array_sum(array_slice($arrayPercentValues, 0, $i));
    		}
    		for ($j = 0; $j < $numAvailStrategies - 1; $j++) {
    			if ($userId % 100 < $cumsum[$j]) {
    				$selectedIndex = $j;
    				break;
    			}
    		}
    	}
    	if ($selectedIndex >= 0) {
    		$orig_keys = array_keys($configPercentFiltersRemainder);
    		$selectedStrategy = $orig_keys[$selectedIndex];
    	} else {
    		$orig_remainder = array_keys($configPercentFilters);
    		$selectedStrategy = $orig_remainder[sizeof($configPercentFilters) - 1];
    	}
    	return $selectedStrategy;
    }
}

?>

