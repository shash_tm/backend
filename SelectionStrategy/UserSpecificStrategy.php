<?php
require_once dirname ( __FILE__ ) . "/BaseStrategy.php";

/**
 * UserSpecificStrategy is an implementation for selection of an AB.
 * The strategy shall assign an AB to a specified % of users i.e. each user 
 * shall be assigned an AB and any analysis shall happen across users.
 */
class UserSpecificStrategy implements BaseStrategy {
    
    #\TODO  
    public function selectImpl($configPercentFilters, $user_id = -1) {
        // Need a deterministic algorithm that returns the same recommender for a user
        // using the config object.
        // The function will require all the user_ids for selection.
        return "";
    }
}

?>