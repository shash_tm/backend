<?php

require_once dirname ( __FILE__ ) . "/../include/Utils.php";

class BaseFilterStrategy{

	/*
	 * TODO: Move the basic values with the user like gender user_id app version and app version code in User Object and use that everywhere
	 */
	private $user_id;
	private $strategy_array;
	private $gender;
	private $app_type;//android ios
	private $app_version;
	
	function __construct($user_id,$strategy_array,$gender){
		$this->user_id=$user_id;	
		$this->gender=$gender;
		$this->strategy_array=$strategy_array;
		$this->setAppTypes();
	}	
	
	/**
	 * Right now it will simple check if (user_id%10)<percentage/10
	 */
	protected function userComeUnderPercentage($percentage){
		//var_dump($this->user_id);
		$user_id_remainder=($this->user_id)%10;
		$percentage_to_check=ceil ($percentage/10  );
		$return_value=$user_id_remainder<$percentage_to_check;
		//var_dump($user_id_remainder);
		//var_dump($percentage_to_check);
		//var_dump($return_value);
		return $return_value;
	}
	
	/*
	 * Logic:
	 * 
	 * if there is a match on the key we will check if we can run .If not we will return false
	 * if there is no matche we will return true.
	 * for not matching on default.Add default with percentage
	 * and default should be at the end
	 */
	private function checkGender($gender_array){
		//$output=true;
		//check for default
		
		//var_dump($this->gender);
		//var_dump($gender_array);
		$default=true;
		foreach($gender_array as $key=>$value){
			if($key==$this->gender){
				return $this->userComeUnderPercentage($value['percentage']);
			}
			if($key=='default'){
				$default=$this->userComeUnderPercentage($value['percentage']);
			}
		}
		return $default;
	}
	
	/*
	 * -1 means infinity here
	 */
	private function checkApp($app_array){
		$default=true;
		//var_dump($app_array);
		//var_dump($this->app_version);
		//var_dump($this->app_type);
		foreach($app_array as $key=>$value){
			if($key==$this->app_type){
				//check for version type
				if(($value['min']=="-1"||$value['min']<=$this->app_version)&&($value['max']=="-1"||$value['max']>=$this->app_version)){
				return $this->userComeUnderPercentage($value['percentage']);
				}
				else{
					return false;
				}
			}
			if($key=='default'){
				$default=$this->userComeUnderPercentage($value['percentage']);
			}
		}
		return $default;
		
	}
	
	private function setAppTypes(){
		$header = Utils::getAllSystemHeaderFields();
		$this->app_type=($header['source']=='androidApp')?"android":"ios";
		$this->app_version=$header['app_version_code'];
	}
	

	public function GetConfig(){
		return $this->strategy_array;
	}
	
	/*
	 * We run on percentage based on the user_id
	 */
    protected function canRun() {
        return $this->checkGender($this->strategy_array['Gender'])&&$this->checkApp($this->strategy_array['app_version']);
        
    }

}

?>
