<?php
require_once dirname ( __FILE__ ) . "/BaseStrategy.php";

/**
 * SessionSpecificStrategy is an implementation for selection of an AB.
 * The strategy uses random assignment based on the % exposure passed to the
 * function i.e. an user shall b eexposed to multiple ABs and any analytics 
 * shall happen across ABs for each user.
 */
class SessionSpecificNoFilterStrategy implements BaseStrategy {
    
    /*
     * The implementation of the random assignment strategy based on % exposure
     * set up in the config.
     * @param - config - an array containing key value pairs where 
     *                    key = names/identifier/classname of an AB
     *                    value = % coverage for the AB.
     */
    public function selectImpl($configPercent) {
        //1. Select a recommender system
        $numAvailStrategies = sizeof($configPercent);
        $arrayPercentValues = array_map(function($x){return $x[0];},$configPercent);
        
        $randomNumber = rand(1, 100);
        // Calculate the cumsum
        $cumsum = array();
        for( $i=1; $i<$numAvailStrategies; $i++ ) {
            $cumsum[] = array_sum(array_slice($arrayPercentValues,0,$i));
        }
        $selectedIndex = -1;
        for( $j=0; $j<$numAvailStrategies-1; $j++ ) {
            if($randomNumber < $cumsum[$j]) {
                $selectedIndex = $j;
                break;
            }
        }
        
        $keys = array_keys($configPercent);
        if($selectedIndex >= 0) {
            $selectedStrategy = $keys[$selectedIndex];
        }
        else {
            $selectedStrategy = $keys[$numAvailStrategies-1];
        }
        return $selectedStrategy;
    }
}

?>