<?php

require_once dirname(__FILE__) . "/BaseStrategy.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
/**
 * SessionSpecificStrategy is an implementation for selection of an AB.
 * The strategy uses random assignment based on the % exposure passed to the
 * function i.e. an user shall b eexposed to multiple ABs and any analytics 
 * shall happen across ABs for each user.
 */
class SessionSpecificFilterStrategy implements BaseStrategy {
    /*
     * The implementation of the random assignment strategy based on % exposure
     * set up in the config using the configured filters as a first level filter.
     * @param - config - an array containing key value pairs where 
     *                    key = names/identifier/classname of an AB
     *                    value = % coverage for the AB and corresponding filters.
     */

    public function selectImpl($configPercentFilters, $userId = -1, $gender = "Female", $age = -1, $location = "", $city = "",  $sessionId = "") {
        //1. Select a recommender system
       // echo "<pre>";
        //var_dump($configPercentFilters);
       // var_dump("$userId|$gender|$age|$location|$city|$sessionId");
        $configPercentFiltersRemainder = array_filter($configPercentFilters,function($x) use ($gender,$age,$location, $city, $sessionId) {
            error_reporting(E_ERROR);
            
            $filter = json_decode($x[1], true);
          //  var_dump($filter);
            $header = Utils::getAllSystemHeaderFields();
            $mutual_friends = isset($header['mutual_friends']) ? $header['mutual_friends'] : false;
            $batchCount = isset($header['batchCount']) ? $header['batchCount'] : -1;
            if ($filter == NULL | 
                    (  ($filter['Gender'] == NULL   | in_array($gender, $filter['Gender']))
                     & ($filter['Age'] == NULL      | in_array($age, $filter['Age']))
                     & ($filter['Location'] == NULL | in_array($location, $filter['Location']))
                    & ($filter['City'] == NULL | in_array($city, $filter['City']))
                    & ($filter['Session'] == NULL | in_array($sessionId, $filter['Session']))
                    & ($filter['mutual_friends'] == NULL | ($mutual_friends && $filter['mutual_friends']))
                    & ($filter['batchCount'] == NULL | ($filter['batchCount'] == $batchCount))
                    )) {
                
                return $x;
            }
        });

        $numAvailStrategies = sizeof($configPercentFiltersRemainder);
        $selectedIndex = -1;
        if ($configPercentFiltersRemainder != NULL) {
            $arrayPercentValues = array_map(function($x) {
                return $x[0];
            }, $configPercentFiltersRemainder);
          
            $randomNumber = rand(1, 100);
            // Calculate the cumsum
            $cumsum = array();
            
            for ($i = 1; $i < $numAvailStrategies; $i++) {
                $cumsum[] = array_sum(array_slice($arrayPercentValues, 0, $i));
            }

            for ($j = 0; $j < $numAvailStrategies - 1; $j++) {
                if ($randomNumber < $cumsum[$j]) {
                    $selectedIndex = $j;
                    break;
                }
            }
        }
        

        if ($selectedIndex >= 0) {
            $orig_keys = array_keys($configPercentFiltersRemainder);
            $selectedStrategy = $orig_keys[$selectedIndex];
        } else {
            $orig_remainder = array_keys($configPercentFilters);
            $selectedStrategy = $orig_remainder[sizeof($configPercentFilters) - 1];
        }
       
        return $selectedStrategy;
    }

}

?>
