<?php
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../DBO/sparkDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../utilities/send_sms.php";
require_once dirname ( __FILE__ ) . "/../utilities/wordlist-regex.php";
require_once dirname ( __FILE__ ) . "/../abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../DBO/userActionsDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/selectDBO.php";


header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

// following files need to be included
require_once( dirname ( __FILE__ ) ."/Paytm_App_Checksum_Kit_PHP/lib/config_paytm.php");
require_once( dirname ( __FILE__ ) ."/Paytm_App_Checksum_Kit_PHP/lib/encdec_paytm.php");

$checkSum = "";



class Spark {
	private $user_id;
	private $source;
	private $functionClass;
	private $app_version_code;
	private $uaDBO;
	private $gender;

	function __construct($user_id)
	{
		$this->functionClass =   new functionClass();
		$this->source = $this->functionClass->getSourceFromHeader ();
		$this->user_id = $user_id;
		$this->app_version_code = $this->functionClass->getHeader();
		$this->uaDBO=new userActionsDBO();
	}

	
	public function getActiveSparkPackages($package_type)
	{
		$bucket_id = $this->getBucketForUSer($package_type);
		$packages = SparkDBO::getActivePackages($this->user_id, $this->source, $bucket_id, $package_type);
		if($package_type == 'spark')
			$old_app_packages = $this->getTitleAndDescriptionArray();
		foreach($packages as $key => $value){
			if($packages[$key]['currency'] == null || $packages[$key]['currency'] == 'rupee' )
				$packages[$key]['currency'] = '₹';
			else if ($packages[$key]['currency'] == 'dollar')
				$packages[$key]['currency'] = '$';
			if(isset($packages[$key]['metadata']) && $packages[$key]['metadata'] != null) {
				$packages[$key]['metadata'] = json_decode($packages[$key]['metadata'], true);
			}
			if($package_type == 'spark')
			{
				$packages[$key]['title'] = $old_app_packages[$key]['title'] == "" ? null: $old_app_packages[$key]['title'];
				if($bucket_id != 9)
					$packages[$key]['description'] = $old_app_packages[$key]['description'] == "" ? null : str_replace("SPARK_COUNT", $packages[$key]['spark_count'], $old_app_packages[$key]['description']);

			}
		}
		return $packages;
	}

	private function getTitleAndDescriptionArray(){
		$old_app_packages = null;
		if (($this->source == 'iOSApp' && $this->app_version_code <= 453) ||
			($this->source == 'androidApp' && $this->app_version_code <= 504)) {
			$old_app_packages = array(
				array("title" => "Trying it out?", "description" => "Get a set of SPARK_COUNT Sparks"),
				array("title" => "Set ablaze!", "description" => "Get a set of SPARK_COUNT Sparks"),
				array("title" => "Burn it up!", "description" => "Get a set of SPARK_COUNT Sparks")
			);
		}else if($this->source == 'androidApp' && $this->app_version_code <= 559 && $this->app_version_code >= 550){
			$old_app_packages = array(
				array("title" => "Starter", "description" => "Coming soon!"),
				array("title" => "Plus", "description" => "Chat sessions with TM Relationship Expert"),
				array("title" => "Premium", "description" => "Chat sessions with TM Relationship Expert")
			);
		}else if(($this->source == 'iOSApp' && $this->app_version_code > 453) ||
			($this->source == 'androidApp' && $this->app_version_code > 559)){
			$old_app_packages = array(
				array("title" => "", "description" => ""),
				array("title" => "", "description" => "Consult with TM relationship expert"),
				array("title" => "", "description" => "Consult with TM relationship expert")
			);
		}
		return $old_app_packages;
	}

	public function assignPackage($package_id, $payment_id)
	{
		$package_details =SparkDBO::getPackageDetails($package_id);
		if(count($package_details)> 0)
		{
			// TODO :: payment id needs to be verified and validated.
			//SparkDBO::initiatePayment($package_details, $this->user_id, $payment_id);

			return array('success' => "Purchase Successful") ;
		}
		else
		{
			//TODO ::  Invalid package Id , return an error or something
			return array('error' => "Invalid Package Id") ;
		}
	}

	public function sparkPaymentForGoogle($google_sku, $hash, $google_token, $payment_id, $order_id, $pg){
		$package_details =SparkDBO::getPackageDetailsBySku($google_sku, $this->source);
		$activity = "spark_payment";
		if($package_details['type'] == 'select')
			$activity = "select_payment";
		if(!isset($google_token) && isset($package_details) && $package_details != null) {
			if($package_details['status'] == 'inactive'){
				return array('responseCode' => 403, 'error'=> 'Invalid spark package');
			}
			return $this->initiateSparkPayment($package_details, $pg);
		}
		else{
			//following is only development scenario. no sparks will be added.
			if($google_sku == 'android.test.purchased' && $google_token == 'inapp:com.trulymadly.android.app.dev:android.test.purchased'){
				$output = array('responseCode' => 200 ,'success' => "Purchase Success");
				$output['new_sparks_added'] = 0;
				$output['consumptionState'] =  true;
				$output['chat_assist'] = true;
				$output['sparks'] = SparkDBO::getUserActiveSparkCount($this->user_id) ;
				$selectObject = new Select($this->user_id);
				$output['my_select'] = $selectObject->getMySelectData();
				return $output;
			}
			$payment_status_google = $this->getPaymentDetailsFromGoogle($google_token, $google_sku, $activity);
			if(isset($payment_status_google['error'])){
				return array('responseCode' => 403, 'error'=> $payment_status_google['message']);
			}
			//Google specific logic below
			if(isset($payment_status_google['developerPayload'])){
				$user_spark_txn = SparkDBO::getSparkPaymentByPaymentId($payment_status_google['developerPayload']);

				if(!isset($user_spark_txn) || $user_spark_txn == null)
					return array('responseCode' => 403, 'error'=> 'No transaction found');

				if($user_spark_txn['package_id'] != $package_details['package_id']){
					return array('responseCode' => 403, 'error'=> 'No transaction found');
				}

				if($this->user_id != $user_spark_txn['user_id']){
					return array('responseCode' => 403, 'error'=> 'Given transaction was done by a different TM account.');
				}

			//	var_dump($user_spark_txn);
				$output = array();

				if($payment_status_google['consumptionState'] == 1 && ($user_spark_txn['status'] == 'purchased' || $user_spark_txn['status'] == 'initiated')){		//sparks consumed on app. Add to user account
					$this->updatePaymentStatus($payment_status_google['developerPayload'], 'consumed',$google_token, 'google', $order_id, null, $activity);
					$output = $this->getPaymentSuccessData($package_details, $this->user_id, "google", $payment_status_google['consumptionState'] == 1 ? true : false);
				}else if($payment_status_google['purchaseState'] == 0 && $payment_status_google['consumptionState'] == 0){		//purchased, yet to be consumed
					$this->updatePaymentStatus($payment_status_google['developerPayload'], 'purchased', $google_token,'google', $order_id, null, $activity);
					$output['consumptionState'] = $payment_status_google['consumptionState'] == 1 ? true : false;
				}else if($payment_status_google['purchaseState'] == 1){						//payment cancelled
					$this->updatePaymentStatus($payment_status_google['developerPayload'], 'canceled', $google_token, 'google', $order_id, null, $activity);
					$output['consumptionState'] = $payment_status_google['consumptionState'] == 1 ? true : false;
				}
				$output = $this->getRepeatCallSuccessResponse($package_details, $output);
				return $output;
			}
		}
	}

	public function sparkPaymentForIOS($apple_sku, $hash, $receipt_data, $payment_id, $transaction_id){
		$package_details = SparkDBO::getPackageDetailsBySku($apple_sku, $this->source);
		$activity = "spark_payment";
		if($package_details['type'] == 'select')
			$activity = "select_payment";

		if(!isset($receipt_data) ) {
			if($package_details['status'] == 'inactive'){
				return array('responseCode' => 403, 'error'=> 'Invalid spark package');
			}
			return $this->initiateSparkPayment($package_details, 'apple');
		} else{

			$payload = array("receipt-data"=>$receipt_data);
			$apple_receipt = $this->getPaymentDataFromApple($payload, $activity);
			$existingTxn = SparkDBO::getSparkPaymentByAppleTxnId($transaction_id,'consumed',$payment_id);
			$duplicateTxn = false;
			if(isset($existingTxn) && $existingTxn != null && count($existingTxn) > 0){
				$duplicateTxn = true;
			}
			if($apple_receipt['status'] == 0){
				if(!isset($transaction_id)){
					return array('responseCode' => 403,'error' => "Transaction ID is required");
				}
				$txns = $apple_receipt['receipt']['in_app'];
				$txn = null;
				foreach($txns as $tranx){
					if($tranx['transaction_id'] == $transaction_id){
						$txn = $tranx;
						break;
					}
				}


				if($txn == null && isset($apple_receipt['receipt']['transaction_id']))
					$txn = $apple_receipt['receipt'];
				//var_dump($txn);die;

				$user_spark_txn = SparkDBO::getSparkPaymentByPaymentId($payment_id);
				if($this->user_id != $user_spark_txn['user_id']){
					return array('responseCode' => 403, 'error'=> 'Given transaction was done by a different TM account.');
				}

				$package_details = SparkDBO::getPackageDetailsBySku($txn['product_id'],$this->source);
				$output  =array();
				if($user_spark_txn['sku'] !=  $txn['product_id']){
					//user did some chedkhani. Flag or return error??
					return array('responseCode' => 403,'error' => "Transaction has been flagged and will be verified manually."/*, "apple_response" => $apple_receipt, "txn" => $txn, 'server_txn'=>$user_spark_txn, "apple_sku"=>$user_spark_txn['apple_sku'], 'txn_sku'=>$txn['product_id']*/) ;
				}else {
					if($duplicateTxn){
						$this->updatePaymentStatus($payment_id, 'apple_restore', null, 'apple', $txn['transaction_id'], null, $activity);
						$output =  array('responseCode' => 200, 'success' => "Purchase Success",
							"sparks" => SparkDBO::getUserActiveSparkCount($this->user_id), 'new_sparks_added' =>  $package_details['spark_count'],
							"chat_assist" => $this->packageHasChatAssist($package_details));
						$output = $this->getRepeatCallSuccessResponse($package_details, $output);
						return $output;

					}
					else if($user_spark_txn != null && $txn != null && $user_spark_txn['status'] != 'consumed' && !$duplicateTxn) {
						$this->updatePaymentStatus($payment_id, 'consumed', null, 'apple', $txn['transaction_id'], null, $activity);
						return $this->getPaymentSuccessData($package_details, $this->user_id, "apple", true);
					}
					$output = array('responseCode' => 200, 'success' => "Purchase Success", 'new_sparks_added' =>  $package_details['spark_count']);
					$output = $this->getRepeatCallSuccessResponse($package_details, $output);
					return $output;
				}

			}else{
				return array('responseCode' => 403,'error' => "Some error occured, please try again") ;
			}
		}
	}

	public function sparkPaymentForPaytm($google_sku, $hash, $payment_id, $transaction_id, $status, $verify, $cron){
		$package_details = SparkDBO::getPackageDetailsBySku($google_sku, 'androidApp');
		$consumptionState = false;
		if(!isset($transaction_id) ) {
			if($package_details['status'] == 'inactive'){
				return array('responseCode' => 403, 'error'=> 'Invalid spark package');
			}
			return $this->initiateSparkPayment($package_details, 'paytm');
		} else{
			$user_spark_txn = SparkDBO::getSparkPaymentByPaymentId($payment_id);

			if(!isset($user_spark_txn) || $user_spark_txn == null)
				return array('responseCode' => 403, 'error'=> 'No transaction found');
			if(!$verify) {
				if ($user_spark_txn['package_id'] != $package_details['package_id']) {
					return array('responseCode' => 403, 'error' => 'Wrong package txn');
				}

				if ($this->user_id != $user_spark_txn['user_id']) {
					return array('responseCode' => 403, 'error' => 'Given transaction was done by a different TM account.');
				}
			}

			if($package_details['type'] == 'select')
				$activity = "select_payment";
			//	var_dump($user_spark_txn);

			if($user_spark_txn['status'] == 'purchased' && $user_spark_txn['gateway_txn_id'] != null
				&& $user_spark_txn['gateway_txn_id'] == $transaction_id){		//sparks consumed on app. Add to user account
				$this->updatePaymentStatus($payment_id, 'consumed',null, 'paytm', $transaction_id, null, $activity);
				$output = $this->getPaymentSuccessData($package_details, $this->user_id, "paytm", true);
			}else if($user_spark_txn['status'] == 'initiated' &&  $user_spark_txn['gateway_txn_id'] == null && $status == 'TXN_SUCCESS'){		//purchased, yet to be consumed
				if($cron){
					$this->updatePaymentStatus($payment_id, 'consumed',null, 'paytm', $transaction_id, $user_spark_txn['user_id'], $activity);
					$output = $this->getPaymentSuccessData($package_details, $this->user_id, "paytm", true);
				}else {
					$this->updatePaymentStatus($payment_id, 'purchased', null, 'paytm', $transaction_id, $user_spark_txn['user_id'], $activity);
					$output['sparks'] = SparkDBO::getUserActiveSparkCount($user_spark_txn['user_id']) ;
				}
			}else if($status == 'TXN_FAILURE' && $user_spark_txn['status'] != "canceled"){						//payment cancelled
				if($user_spark_txn['status'] == "consumed")
					$output['remove_sparks_manually']=true;
			//	$this->updatePaymentStatus($payment_id, 'canceled', null, 'paytm', $transaction_id, $user_spark_txn['user_id'], $activity);
				$output['sparks'] = SparkDBO::getUserActiveSparkCount($user_spark_txn['user_id']) ;
			}else if($user_spark_txn['status'] == 'consumed'){
				$output['consumptionState'] = true;
				$output['sparks'] = SparkDBO::getUserActiveSparkCount($user_spark_txn['user_id']) ;
			}
			$output = $this->getRepeatCallSuccessResponse($package_details, $output);
			return $output;
		}
	}

	public function cancelPayment($payment_ids){
		$payment_id_array = explode(",", $payment_ids);
		foreach($payment_id_array as $key => $value){
			$paymentRow = SparkDBO::getSparkPaymentByPaymentId($value);
			if($paymentRow['status'] != 'consumed' && $paymentRow['status'] != 'purchased' && $paymentRow['user_id'] == $this->user_id){
				$this->updatePaymentStatus($value, 'canceled', null,'google',null, null);
			}
		}
	}

	private function updatePaymentStatus($payment_id, $status, $token=null, $source="google", $order_id=null, $user_id=null, $activity="spark_payment"){
		$logInfo = array("source"=> $source, "order_id"=> $order_id, "payment_id"=>$payment_id);
		$this->logAction('update_status', $status, json_encode($logInfo), $user_id,$activity);
		SparkDBO::updateUserSparkTxnStatus($payment_id, $status, $token, $source, $order_id);
	}

	private function initiateSparkPayment($package_details, $pg){
		$payment_id = $this->getUniquePaymentId();

		$bucket_id = $this->getBucketForUSer($package_details['type']);
		$validPackage = SparkDBO::checkIfPackageExistsInBucker($bucket_id, $package_details['package_id']);
		$activity = "spark_payment";

		if($validPackage === false){
			$output = array('responseCode' => 403 ,'error' => "Trying to buy invalid package.") ;
			$logInfo = $output;
			$this->logAction('update_status', 'initiated', json_encode($logInfo),null,$activity);
			return $output;
		}

		if($package_details['type'] == 'select')
			$activity = "select_payment";
		$payload_hash = SparkDBO::getMd5Hash($this->user_id, $package_details['sku'], "payment", $payment_id);	//Hash not required for Android.

		$transaction_id = SparkDBO::initiatePayment($package_details, $this->user_id, $payment_id, $pg);

		if(isset($transaction_id) && $transaction_id != null){
			$output = array('responseCode' => 200 ,'success' => "Purchase Initiated", "developer_payload" => $payment_id, "secure_hash" => $payload_hash) ;
		}else{
			$output = array('responseCode' => 403 ,'error' => "Some error occured, please try again") ;
		}
		$logInfo = $output;
		$logInfo['source'] = $pg;
		$this->logAction('update_status', 'initiated', json_encode($logInfo),null,$activity);
		return $output;
	}

	//checks the payment table and returns a unique payment id (non auto increment)
	private function getUniquePaymentId(){
		$random_id = 'TM2016_';
		$random_id .= rand(pow(10, 5 - 1), pow(10, 5) - 1);
		$random_id .= '_';
		$random_id .= microtime(true);
		return $random_id;
	}

	public function sendSpark($user1, $user2, $unique_id, $message, $spark_hash,$is_repeat_profile){
		$uaDBO = new userActionsDBO();
                $uuDBO = new userUtilsDBO();
                $obj=new UserFlagsDBO();
		$isliked = $uaDBO->isActionSet($user2, $user1, "user_like");		//checking if user2 has already liked user1 who is sparking now.
		$selectObject = new Select($this->user_id);
		$selectData = $selectObject->getSelectMembers(array($user1, $user2));
		$flags = $obj->read("select_config");
                $myData = $uuDBO->getUserData(array($this->user_id), true);
		$myCountry = $myData['stay_country'];
		if(!$isliked && $selectData[$user2] === true && !isset($selectData[$user1]) && $flags['allow_spark'] !== "true" && $myCountry == 113){
			return array("error"=> "Cannot spark a SelectMember","responseCode"=>"403");
		}
		/*if((!$redis->sIsMember('spark_test_users',$user1) || !$redis->sIsMember('spark_test_users',$user2)) &&
			$obj->read("spark_flags")["send_test_spark_only"] == 'true'){
			return array("error"=> "Test Users only","responseCode"=>"403");
		}*/

		if(!isset($spark_hash) || $spark_hash != SparkDBO::getMd5Hash($user1, $user2, "spark")){
			return array("error"=> "Invalid Spark hash","responseCode"=>"403");
		}
		$sparkExists = SparkDBO::checkForDuplicateSparkForUsers($user1,$user2);		// A user can send only 1 spark to another user

		$userSparkCounter = SparkDBO::getUserActiveSparkCount($user1);			// A user should have at least 1 active spark

		if($sparkExists){
			$result =  array("success"=> "Spark created","responseCode"=>"200");
			$result['sparks_left'] = $userSparkCounter;
			return $result;
		}

		if($userSparkCounter == null || !isset($userSparkCounter) || $userSparkCounter <= 0)
			return array("error"=> "No active sparks","responseCode"=>"403", "error_code"=>1);


		global $badwords;
		$pendingSparks = $userSparkCounter - 1;
		if(!$sparkExists) {
			$obj=new UserFlagsDBO();
			$spark_duration = $obj->read("spark_flags")["spark_duration"];
			if(isset($spark_duration))
				$spark_duration = (int)$spark_duration;
			else
				$spark_duration = 43200;	//default 12 hour spark validation
			if(!isset($message) || trim($message) == ""){
				$message = "You've been Sparked!";
				$result = SparkDBO::createSpark($user1, $user2, $message, null, $spark_duration, $pendingSparks);
			}else {
				$msg = new MessageFullConversation($user1, $user2);
				$output = $msg->saveMessage($message, $badwords, "SPARK", null, $unique_id, null, null);
				$output = json_decode($output, true);
				$result = SparkDBO::createSpark($user1, $user2, $message, $output['msg_id'], $spark_duration, $pendingSparks);
				$result['message'] = $output;
			}
			$result['sparks_left'] = $pendingSparks;
			$this->remove_from_recommendation($user1,$user2);
			$this->remove_from_recommendation($user2,$user1);
			if($is_repeat_profile=="true"){
				$action_taken="spark";
				$this->uaDBO->insertRepeatLikes($user1,$user2, $action_taken);
			}
		}else{
			$result =  array("success"=> "Spark created","responseCode"=>"200");
			$result['sparks_left'] = $userSparkCounter;
		}
		return $result;
	}

	public function getSparksReceivedByUser($user_id){
                $limit = 5;
		$sparks = SparkDBO::getSparksForUser($user_id,-1);
                $count = sizeof($sparks);
                $sparks = array_slice($sparks, 0,$limit);
		if(isset($sparks) && $sparks != null && count($sparks) > 0 ) {
			$sparks = $this->processSparkList($sparks);
			return array("sparks" => $sparks,"count" => $count);
		}

		return array("sparks" => array(),"count" => 0);
	}

	public function getTotalSparksReceived($user_id){
		return SparkDBO::getPendingReceivedSparks($user_id);
	}

	public function processSparkList($sparks){
		global $imageurl;
		$userUtils = new UserUtils();
		$user_ids = array();
		foreach($sparks as $key => $value){
			$sparks[$key]['hash'] = SparkDBO::getMd5Hash($sparks[$key]['sender_id'], $sparks[$key]['user2'], "spark");		//add hash
			$sparks[$key]['profile_link'] = $userUtils->generateProfileLink($sparks[$key]['user2'],$sparks[$key]['sender_id'])."&from_spark=true";
			$sparks[$key]['full_conv_link'] = $userUtils->generateMessageLink($sparks[$key]['user2'],$sparks[$key]['sender_id']);
			if(!isset($sparks[$key]['designation']) || $sparks[$key]['designation'] == null || trim($sparks[$key]['designation']) == ""){
				$sparks[$key]['designation'] = $sparks[$key]['highest_degree'];
			}
			$user_ids[] = $sparks[$key]['sender_id'];
	//		$sparks[$key]['images'] = $userUtils->getActiveImages(array($sparks[$key]['sender_id']));

			if($sparks[$key]['status'] == 'seen'){
				$sparks[$key]['expiry_time'] = $diff = strtotime($sparks[$key]['expired_date']) - strtotime($sparks[$key]['current_mysql_time']);
			}
			unset($sparks[$key]['user2']);
			unset($sparks[$key]['expired_time']);
			unset($sparks[$key]['current_mysql_time']);
		}
		$images_array = $userUtils->getActiveImages($user_ids);
		$selectObject = new Select($this->user_id);
		$selectUsers = $selectObject->getSelectMembers($user_ids);
		foreach($sparks as $key => $value){
			$other_images = $images_array[$sparks[$key]['sender_id']]['other_pics'];
			$sparks[$key]['profile_pic'] = /*$imageurl.*/ $images_array[$sparks[$key]['sender_id']]['profile_pic'];
			if($other_images != null && count($other_images) > 0)
				$sparks[$key]['images'] = $other_images;
			if(isset($selectUsers[$sparks[$key]['sender_id']])){
				$sparks[$key]['is_select'] = true;
			}else{
				$sparks[$key]['is_select'] = false;
			}
		}

		return $sparks;
	}

	public function updateSpark($user1, $user2, $hash, $action){
		$hashVerification = SparkDBO::getMd5Hash($user1, $user2, "spark");
		if($hash != $hashVerification){
			return array("error"=>"UnAuthorized Action", "responseCode"=>200 );
		}else{
			SparkDBO::updateSpark($user1, $user2, $action);
		}
		return array("success"=>"Spark updated", "responseCode"=>200 );
	}

	//Ran only first time. NOT TO BE CALLED ANYMORE. HAVE COMMENTED OUT THE FUNCTION CALL to prevent accidental calling
	public function getAccessTokenFromGoogle($code){
		global $google_client_id,$google_client_secret, $google_redirect_uri;
		$fields = array(
			"grant_type" => "authorization_code",
			"code" => $code,
			"client_id" => "$google_client_id",
			"client_secret" => "$google_client_secret",
			"redirect_uri" => "$google_redirect_uri"
		);
		$result = null;
		/*
		 * $this->authResponseFromGoogle($fields); -- Preventing accidental calling, and returning null instead.
		 * auth code is generated if Shashwat (or anybody with admin play store rights visits following link in browser:
		 * https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/androidpublisher&response_type=code&access_type=offline&redirect_uri=http://dev.trulymadly.com/tarun/spark/googleauth.php&client_id=829694726682-s5jv1q2tjsutk5ovvuh1o9vmoetk03cl.apps.googleusercontent.com
		*/
		return $result;
	}

	public function refreshGoogleAccessToken(){
		global $google_client_id,$google_client_secret;
		global $google_refresh_token;
		$fields = array(
			"grant_type" => "refresh_token",
			"client_id" => $google_client_id,	//shall go into config file
			"client_secret" => $google_client_secret,												//shall go into config file
			"refresh_token" => $google_refresh_token
		);

		$result = $this->authResponseFromGoogle($fields);
		$obj=new UserFlagsDBO();
		if(isset($result['access_token']))
			$obj->write("STRING","google_spark_access_token",array($result['access_token']), null);

		return $result['access_token'];
	}

	public function generateCheckSum(){
		//Here checksum string will return by getChecksumFromArray() function.
		$checkSum = getChecksumFromArray($_POST,PAYTM_MERCHANT_KEY);
		//print_r($_POST);

		return array("CHECKSUMHASH" => $checkSum,"ORDER_ID" => $_POST["ORDER_ID"], "payt_STATUS" => "1");

		//Sample response return to SDK

		//  {"CHECKSUMHASH":"GhAJV057opOCD3KJuVWesQ9pUxMtyUGLPAiIRtkEQXBeSws2hYvxaj7jRn33rTYGRLx2TosFkgReyCslu4OUj\/A85AvNC6E4wUP+CZnrBGM=","ORDER_ID":"asgasfgasfsdfhl7","payt_STATUS":"1"}

	}

	private function authResponseFromGoogle($fields){
		global $google_auth_url;
		$url = $google_auth_url;
		$ch = curl_init();

		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$result = curl_exec($ch);
		return json_decode($result, true) ;
	}


	public function getSparkPackagesFromGoogle(){
		global $google_package_name;
		$url = "https://www.googleapis.com/androidpublisher/v2/applications/$google_package_name/inappproducts?access_token=";
		return $this->getDataFromGoogle($url,null,"spark_payment");
	}

	public function getPaymentDetailsFromGoogle($token, $productId, $activity){
		global $google_package_name;
		$url = "https://www.googleapis.com/androidpublisher/v2/applications/$google_package_name/purchases/products/".$productId."/tokens/".$token."?access_token=";
		return $this->getDataFromGoogle($url,null,$activity);
	}

	private function getDataFromGoogle($url, $new_access_token = null, $activity="spark_payment"){
		global $redis;
		if($new_access_token == null)
			$access_token = $redis->get("google_spark_access_token");
		else
			$access_token = $new_access_token;
		//in case redis is screwed up or something bad happened. Extra precaution
		if(!isset($access_token) || $access_token == null)
			$access_token = $this->refreshGoogleAccessToken();	// refresh token shall go into google config
		$origUrl = $url;
		$url .= $access_token;
		//get package details from google.
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$result = curl_exec($ch);
		$output = json_decode($result, true);
		//if new token is true, than, out put should never contain true. Only added new token condition to prevent infinite loop in case google screws up.
		if(isset($output['error'])) {
			$this->logAction('google_payment_call_error',"success", $result,null,$activity);
			if ($output['error']['code'] == 401 && ($new_access_token == null || !isset($new_access_token))) {
				$access_token = $this->refreshGoogleAccessToken();    //pass refresh token
				return $this->getDataFromGoogle($origUrl, $access_token, $activity);
			}else if($output['error']['code'] == 400){	// invalid value
				return array("error"=> true, "message"=>"Invalid value");
			}else{
				return array("error"=> true, "message"=>$output['error']['message']);
			}
		}
		$log_info = $output;
		$log_info['url'] = $url;
		$this->logAction('google_payment_call',"success", json_encode($log_info),null,$activity);

		return $output;
	}

	private function getPaymentDataFromApple($receiptData, $activity="spark_payment"){
		global $apple_pay_url;
		$data_string = json_encode($receiptData);
		$ch = curl_init($apple_pay_url);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string))
		);
		$output   = curl_exec($ch);
		$this->logAction('apple_payment_call', "success", $output, null, $activity);
		return json_decode($output, true);
	}

	public static function getUserSparkTxnHistory($user_id){
		return  SparkDBO::getUserSparkTxnHistory($user_id);
	}

	private function remove_from_recommendation($user1,$user2){
		global $redis;
		$key	= "u:m:$user1";
		$arr = $redis->LRANGE($key, 0, -1);
		foreach($arr as $value){
			$redis_user = explode("l",explode(":", $value)[0])[0];
			if($redis_user == $user2) {
				$redis->LREM($key, $value);
				break;
			}
		}
		
		$key = Utils::$redis_keys['matches_with_scores'] . $user1;
		if ($redis->EXISTS($key)) {
			$arr = $redis->LRANGE($key, 0, -1);
			foreach($arr as $value){
				$redis_user = explode("l",explode(":", $value)[0])[0];
				if($redis_user == $user2) {
					$redis->LREM($key, $value);
					break;
				}
			}
		}
	}

	public function logAction($event,$status, $event_info, $user_id = null, $activity="spark_payment") {
		//event_type can be payment_update, google_server_call, android_server_call
		//for payement update, status will be payment status, for google android server call, status will be call response status
		$Trk = new EventTrackingClass();
		$data_logging = array();
		$user_id = $user_id == null ? $this->user_id : $user_id;
		$data_logging [] = array( "activity" => $activity , "event_type" => $event, "event_status" => $status, "event_info" => $event_info, "time_taken"=>0, "user_id" => $user_id);
		$Trk->logActions($data_logging);
	}

	public function getSparkPaymentByPaymentId($payment_id){
		return SparkDBO::getSparkPaymentByPaymentId($payment_id);
	}

	public function addSparkCount($user_id, $spark_count, $expiry_days, $metadata){
		if(isset($metadata) && $metadata != null)
		{
			$metadata = json_decode($metadata, true);
			if(isset($metadata['chat_assist']) && ($metadata['chat_assist'] == true || $metadata['chat_assist'] == 'true'))
			{
				SparkDBO::addSparkCount($user_id, $spark_count, $expiry_days, 1);
				//check if match exists
				$this->matchWithRE($metadata);
			}
			else
			{
				SparkDBO::addSparkCount($user_id, $spark_count, $expiry_days, 0);
			}
		}
		else
		{
			SparkDBO::addSparkCount($user_id, $spark_count, $expiry_days, 0);
		}
	}


	public function subscribeToSelect($expiry_days, $metadata)
	{
		selectDBO::subscribeToSelect($this->user_id, $expiry_days, 'active');
		if(isset($metadata) && $metadata != null){
			$metadata = json_decode($metadata, true);
			if(isset($metadata['chat_assist']) && ($metadata['chat_assist'] == true || $metadata['chat_assist'] == 'true')){
				SparkDBO::addSparkCount($this->user_id, 0, 0, 1);
				$this->matchWithRE($metadata);
			}
		}
	}

	public function matchWithRE($metadata)
	{
		//check if match exists
		$obj=new UserFlagsDBO();
		$flags=$obj->read("spark_flags");
		$chat_assist_users = $flags["chat_assist_user"];
		$chat_assist_array = explode(",",$chat_assist_users);
		if(isset($metadata['chat_assist']) && $metadata['chat_assist'] == true){
			global $badwords;
			$chat_assist_status = SparkDBO::chatAssistMatched($this->user_id, $chat_assist_users);
			if(!$chat_assist_status){
				$ud = new UserData($this->user_id);
				$gender = $ud->fetchGender();
				if($gender == 'M'){
					$txt_msg = "Hey there! I know that relationships aren't always easy.".
						" You may not get a 'like' back, could be struggling to find the right opening line or be unsure about your DP.".
						" Now there's no need to worry, I'm here to clear all your doubts and help you master your dating life like a BOSS!".
						PHP_EOL.PHP_EOL.
						"So ask away, I'm all ears.";
				}else{
					$txt_msg = "Hey girls! Relationships are hard work but I'm here to make it easier for you! ".
						" Talk to me about the cute guy who just liked you back, which DP to upload, the right date night outfit or even just gossip a little :) ".
						" Just reply here to get started.".
						PHP_EOL.PHP_EOL.
						"P.S. I promise to guard your secrets as my own.";
				}
				$userAction = new UserActions();
				$chat_assist_user = $this->user_id % 2 == 0 ? $chat_assist_array[0] : $chat_assist_array[1];
				$userAction->performAction($this->user_id, $chat_assist_user,'like',0,null,0,true);
				$userAction->performAction($chat_assist_user, $this->user_id,'like',0,null,0,true);

				$msg = new MessageFullConversation($chat_assist_user, $this->user_id);
				$output = $msg->saveMessage($txt_msg, $badwords, "TEXT", null, microtime(true)."", null, null);
			}
		}
	}

	public function getBucketForUSer($package_type, $checkFreeTrial = false){
		$uuDBO = new userUtilsDBO();
		$myData = $uuDBO->getUserData(array($this->user_id), true);
	//	$ud = new UserData($this->user_id);
		$age              = $myData['age'];
		$myState            = $myData['stay_state'];
		$myCity             = $myData['stay_city'];
		$myCountry = $myData['stay_country'];
		$ud = new UserData($this->user_id);
		$gender = $ud->fetchGender();
		$this->gender = $gender;
		$user_age_on_tm = $ud->fetchNumberOfDaysSpentByUserOnTheApp();
		$previous_purchase = sparkDBO::hasPreviousTransaction($this->user_id, $package_type);
		$bucketList = SparkDBO::getActivePackageBuckets($this->source,$package_type);
		$bucket_id = null;
		$freeTrial = 0;
		$seen_select_paid = 0;
		if($package_type == 'select')
			$seen_select_paid = SelectDBO::hasViewedPaidSelectPackages($this->user_id);

		foreach($bucketList as $bucket){
			$filt = $bucket['filter'];
			$filter = json_decode($filt, true);
			if ($filter != NULL) {
				if ($filter['in_country'] != NULL && in_array($myCountry, $filter['in_country'])) {
					$bucket_id = $bucket['bucket_id'];
					break;
				}
				if ($filter['Age'] != NULL && in_array($age, $filter['Age'])) {
					$bucket_id = $bucket['bucket_id'];
					break;
				}
				if($filter['previous_purchase'] !== null && $filter['previous_purchase'] == $previous_purchase && count($filter) == 1){
					$bucket_id = $bucket['bucket_id'];
					break;
				}
				if(
					(($filter['previous_purchase'] !== null && $filter['previous_purchase'] === $previous_purchase) &&
					($filter['gender'] !== null && strtolower($filter['gender']) === strtolower($gender)) &&
					(
						(
							((!isset($filter['not_in_city'])) || (isset($filter['not_in_city']) && !in_array($myCity, $filter['not_in_city']))) &&
							((!isset($filter['not_in_state'])) || (isset($filter['not_in_state']) && !in_array($myState, $filter['not_in_state']))) &&
							((!isset($filter['age'])) || (isset($filter['age']) && in_array($age, $filter['age']))) &&
							((!isset($filter['seen_select_paid'])) || (isset($filter['seen_select_paid']) && $filter['seen_select_paid'] == $seen_select_paid))
						)
						||
						(
							(isset($filter['maximum_tm_age'])) && (isset($filter['maximum_tm_age']) && $user_age_on_tm <= $filter['maximum_tm_age'])
						)
					))
					||
					(in_array($this->user_id, array(1064262,856525,822505,668045,4927)) && $filter['previous_purchase'] == $previous_purchase)	//One time free trial for users due to iOS error.
				){
					$bucket_id = $bucket['bucket_id'];
					$freeTrial++;
					if($checkFreeTrial){
						return array("free_trial"=>true, "bucket_id"=>$bucket_id);
					}

					break;
				}
				if(
					($filter['gender'] !== null && strtolower($filter['gender']) === strtolower($gender)) &&
					((!isset($filter['age'])) || (isset($filter['age']) && in_array($age, $filter['age']))) &&
					!isset($filter['previous_purchase'])
				){
					$bucket_id = $bucket['bucket_id'];
					break;
				}
				if(
					(
						(isset($filter['in_city']) && in_array($myCity, $filter['in_city'])) &&
						((!isset($filter['ab_value']) || (isset($filter['ab_value']) && $this->user_id % $filter['ab_value'] == 0)))
					)
				){
					$bucket_id = $bucket['bucket_id'];
					break;
				}
			}
		}
		if($bucket_id == null){
			$bucket_id = $this->getDefaultBucket($package_type);
		}
		if( $this->user_id == 21826)		// Test user only. Mainly for getting iOS package approval and other package testing.
			$bucket_id = 10;

		if( $this->user_id == 157942)		// Test user only. Mainly for getting iOS package approval and other package testing.
			$bucket_id = 8;

		if($package_type == 'select' && $freeTrial == 0 && $checkFreeTrial === false){
			SelectDBO::registerUserSeenSelectPackages($this->user_id);
		}
		return $bucket_id;
	}

	private function packageHasChatAssist($package_details){
		if(isset($package_details['metadata'])){
			$metadata = json_decode($package_details['metadata'], true);
			if(isset($metadata['chat_assist']) && ($metadata['chat_assist'] == true || $metadata['chat_assist'] == 'true'))
				return true;
		}
		return false;
	}

	private function getDefaultBucket( $package_type){
		if ($this->source == 'androidApp') {
			$bucket_id = $package_type == "spark" ? 6 : 12;
		}else{
			$bucket_id=$package_type == "spark" ? 0 : 13;
		}
		return $bucket_id;
	}

	private function getPaymentSuccessData($package_details, $user_id, $pg, $consumption_state=null){
		$output = array();
		if($package_details['type'] == 'spark') {
			$this->addSparkCount($user_id, $package_details['spark_count'], $package_details['expiry_days'], $package_details['metadata']);
			$output['new_sparks_added'] = $package_details['spark_count'];
		}
		else if($package_details['type'] == 'select'){
			$this->subscribeToSelect($package_details['expiry_days'], $package_details['metadata']);
			$this->saveLastRecommendation($user_id);
			$uu = new UserUtils();
			$uu->clearRecommendationCache($user_id);
                        $uu->clearMatchCountData($user_id);
		}
		$output['consumptionState'] = $consumption_state;
		$output = $this->getRepeatCallSuccessResponse($package_details, $output);
		return $output;
	}

	private function getRepeatCallSuccessResponse($package_details, $output){
		if(!isset($output))
			$output = array();
		$output['responseCode'] = 200;
		$output['success'] = "Purchase Success";
		if($package_details['type'] == 'select') {
			$selectObject = new Select($this->user_id);
			$output['my_select'] = $selectObject->getMySelectData();
		}else if($package_details['type'] == 'spark'){
			$output['chat_assist'] = $this->packageHasChatAssist($package_details);
			$output['sparks'] = SparkDBO::getUserActiveSparkCount($this->user_id) ;
		}
		return $output;
	}


	public function saveLastRecommendation($user){
		global $redis;
		$key	= "u:m:$user";
		$arr = $redis->LRANGE($key, 0, -1);
		foreach($arr as $value){
			$redis_user = explode("l",explode(":", $value)[0])[0];
			if((int)$redis_user > 0){
				$new_key = "m:one_before_select:$user";
				$redis->set($new_key, $redis_user);
				break;
			}
		}
	}


	public function getSparkVideoURL()
	{
		global $cdnurl;
		if($this->gender == 'M')
		{
			$url = $cdnurl .'/video_content/spark_boy.mp4';
		}
		else
		{
			$url = $cdnurl .'/video_content/spark_girl.mp4';
		}
		return $url;
	}
}
