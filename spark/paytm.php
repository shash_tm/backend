<?php
require_once dirname ( __FILE__ ) . "/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";

$user = functionClass::getUserDetailsFromSession ();
$user_id = $user['user_id'];
/// $user_id = $_REQUEST['user_id'];
// $user_id = 2136 ; //492801;
global $redis;
try {
    $action = $_REQUEST['action'];
    if (isset($user_id)  || $action == 'generate_checksum')
    {
        $spark = new Spark($user_id) ;
        $output = array();
        switch($action){  //because while calling generate checksum, no userid is set in session
            case "generate_checksum":
                $initiatePayment = $spark->getSparkPaymentByPaymentId($_POST['ORDER_ID']);
                if($_REQUEST['TXN_AMOUNT'] != $initiatePayment['price']){
                    $output["responseCode"] = 401;
                    $output['message'] = "Amount has been tempered with";
                    break;
                }
                if($_REQUEST['TXNTYPE'] == 'REFUND' || $_REQUEST['TXNTYPE'] == 'CANCEL' || $_REQUEST['TXNTYPE'] == 'REFUND CANCEL'){
                    $output["responseCode"] = 401;
                    $output['message'] = "I did read the PayTM docs too my friend :)";
                    break;
                }
                $output = $spark->generateCheckSum();
                break;

            default:
                $output['responseCode'] = 404;
                $output['message'] = "Action Not Found !";
        }
    } else
    {
        $output["responseCode"] = 401;
    }



    print_r(json_encode($output));

} catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>
