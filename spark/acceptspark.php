<?php
require_once dirname ( __FILE__ ) . "/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";

$user = functionClass::getUserDetailsFromSession ();
$user_id = $user['user_id'];
$spark = new Spark();
try {

    if($_GET['action'] == 'update_spark' && isset($_GET['spark_action']) && $_GET['spark_action'] == 'accepted'
        && isset($_GET['hash']) && isset($_GET['sender_id']) && isset($_GET['receiver_id'])){
        $output = $spark->updateSpark($_GET['sender_id'], $_GET['receiver_id'], $_GET['hash'], $_GET['spark_action']);
    }
    else
    {
        $output["responseCode"] = 403;
        $output["error"] = "UnknownRequest";
    }
    print_r(json_encode($output));

} catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>