<?php
require_once dirname ( __FILE__ ) . "/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../CarouselNotifications/Commonalities.php";

$user = functionClass::getUserDetailsFromSession ();
$user_id = $user['user_id'];
/// $user_id = $_REQUEST['user_id'];
//$user_id = 2136 ; //492801;
global $redis;
try {

    if (isset($user_id))
    {
        $spark = new Spark($user_id) ;
        
        if ( $_POST['action'] == 'get_packages' )
        {
            $output["responseCode"] = 200;
            $type = isset($_POST['type']) ? $_POST['type'] : 'spark';
            $output['spark_packages']  = $spark->getActiveSparkPackages($type);
            $output['spark_video']  = $spark->getSparkVideoURL();
            $spark_flags = $redis->hGetAll('spark_flags');
            if(isset($spark_flags['match_guarantee']))
                $output['match_guarantee'] = $spark_flags['match_guarantee'];
        }
        else if ($_POST['action'] == 'buy_package')
        {
            if(isset($_POST['google_sku'])) {
                $pg = $_POST['pg'] == 'paytm' ? 'paytm' : 'google';
                if($pg == 'google')
                    $output = $spark->sparkPaymentForGoogle($_POST['google_sku'], $_POST['secure_hash'], $_POST['google_token'], $_POST['developer_payload'], $_POST['order_id'], $pg);
                else if($pg == 'paytm'){
                    global $md5_salt;
                    $output = $spark->sparkPaymentForPaytm($_POST['google_sku'], $_POST['secure_hash'],  $_POST['developer_payload'], $_POST['transaction_id'], $pg, false, false);
                    $output['customer_id'] = "TM_U_".$user_id;
                }
            }
            else if(isset($_POST['apple_sku']) || isset($_POST['receipt-data'])){
                $output = $spark->sparkPaymentForIOS($_POST['apple_sku'], $_POST['secure_hash'], $_POST['receipt-data'], $_POST['developer_payload'], $_POST['transaction_id']);
            }
        }
        else if($_POST['action'] == 'send_spark'  && isset($_POST['receiver_id']) && $_POST['receiver_id'] != null      //Send a spark to match
            && isset($_POST['unique_id']) && $_POST['unique_id'] != null
        ){
            $output = $spark->sendSpark($user_id, $_POST['receiver_id'],$_POST['unique_id'], strip_tags($_POST['message']),$_POST['spark_hash'],$_POST['has_liked_before']);
        }
        else if($_POST['action'] == 'get_sparks'){
            $output["responseCode"] = 200;
            $result = $spark->getSparksReceivedByUser($user_id);
            $output["sparks"] = $result["sparks"];
            $output["total_sparks"] = $result["count"];
        }
        else if($_POST['action'] == 'get_commonalities' && isset($_POST['match_id'])){
            $output["responseCode"] = 200;
            $match_id = $_POST['match_id'];
            $datespot_id = null;
            if(isset($_POST['datespot_id'])) $datespot_id = $_POST['datespot_id'];
            
            /* The function finds out the common stuff between the 2 users and returns back in the form of an array of displayable strings */
            /* Hack for iOS - html string not supported at the moment.*/
            $functionClass = new functionClass();
            $source = $functionClass->getSourceFromHeader();
            $isHtmlTagsEnabled = ($source == "iOSApp") ? false : true;
            $content_texts = Commonalities::getCommonalitiesString($user_id, array($match_id), $isHtmlTagsEnabled, $datespot_id);
            if(isset($content_texts[$match_id])) {
                $output['commonalities_string'] = $content_texts[$match_id];
            }
            else {
                $output['commonalities_string'] =  array("Got a quirky story about yourself, we're sure she'd love to hear it!");
            }
        }
        else if($_POST['action'] == 'update_spark' && isset($_POST['spark_action']) && $_POST['spark_action'] != null
            && isset($_POST['sender_id']) && $_POST['sender_id'] != null
            && isset($_POST['hash']) && $_POST['hash'] != null ){
            $output = $spark->updateSpark($_POST['sender_id'], $user_id, $_POST['hash'], $_POST['spark_action']);
        }
        else if ( $_POST['action'] == 'google_auth' ) {     ///* Ran single time to get access and refresh tokens against Shaswat's auth code.
            $output = $spark->getAccessTokenFromGoogle($_POST['auth_code']);
        }
        else if ( $_POST['action'] == 'get_google_packages' ){
            $output = $spark->getSparkPackagesFromGoogle();
        }
        else if ( $_POST['action'] == 'user_spark_package_history' ){       //history of google sku purchase for user (for rollback)
            $output = $spark->getUserSparkTxnHistory($user_id);
            $output["responseCode"] = 200;
        }else if( $_POST['action'] == 'cancel_payment'){
            $spark->cancelPayment($_POST['payment_ids']);
            $output["responseCode"] = 200;
            $output['message'] = "Orders Canceled";
        }
        else
        {
            $output["responseCode"] = 403;
            $output["error"] = "UnknownRequest";

        }
    } else
    {
        $output["responseCode"] = 401;
    }



    print_r(json_encode($output));

} catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>