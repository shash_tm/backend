<?php
require_once dirname(__FILE__) . "/include/config.php";
require_once dirname(__FILE__) . "/curatedDeals/eventCategoryClass.php";
try
{
	$event_id = isset($_REQUEST['e'])?$_REQUEST['e'] : 0;
	$eventObj = new EventCategory(0) ;
	$datespot_details = $eventObj->getDateSpotById($event_id);
	$smarty->assign("event_id",$event_id);
	$smarty->assign ("event" , $datespot_details) ;
	$smarty->display ( "templates/event/redirect_socials.tpl" );

} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
} 