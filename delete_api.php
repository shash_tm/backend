<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/settings/account.php";

/* API for delete user, this will block the user and add user id to the deletion_log*/
try 
{
   $user = functionClass::getUserDetailsFromSession ();
   $user_id = $user['user_id'];
   
 //  $status = $user['status'] ;
   if (isset($user_id))
   {
   	$accountObj =  new UserAccount($user_id);
   	$rating = (int)$_REQUEST['app_rate'] ;
   
   	if ( $_REQUEST['action'] == 'delete_account' && isset( $_REQUEST['reason'] )  )
   	 {
//    	 	if ( $rating >0 && $rating <= 5 && is_int($rating) == true )
//    	 	{
			$status = functionClass::getUserStatus($user_id);
   	 		$accountObj->deleteAccount($_REQUEST, $status, $rating);
   	 		$output["responseCode"] = 200;	
//    	 	} else 
//    	 	{
//    	 		$output ["responseCode"] = 403 ;
//    	   		$output ["error"] = "InvalidRating" ;
//    	 	}
   	 
   	 }
   	else 
   	{
   	   $output ["responseCode"] = 403 ;
   	   $output ["error"] = "unknownRequest" ;
   	}
   }
   else 
   {
   	 $output["responseCode"] = 401;
   }
   print_r(json_encode($output));   
} 
catch (Exception $e) 
{
trigger_error($e->getMessage(), E_USER_WARNING);
}
?>