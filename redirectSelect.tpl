<!doctype html>
<html>
<!--
@author
udbhav with MAT
-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=1.0" />

    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="trulymadly">
    <meta property="og:title" content="{$event.name}" />
    <meta property="og:description" content="Check it out!" />
    <meta property="og:image" itemprop="image" content="{$event.image}">
    <meta property="og:image:secure_url" itemprop="image" content="{$event.image}">


    <title>{$event.name}</title>
    <title></title>


    <script type="text/javascript">
        var isMobile = {
            Android : function() {
                return (/Android/i.test(navigator.userAgent)) && !this.Windows();
            },
            BlackBerry : function() {
                return /BlackBerry/i.test(navigator.userAgent) && !this.Windows();
            },
            iOS : function() {
                return /iPhone|iPad|iPod/i.test(navigator.userAgent) && !this.Windows();
            },
            Windows : function() {
                return /IEMobile/i.test(navigator.userAgent);
            },
            any : function() {
                return (isMobile.Android() || isMobile.BlackBerry()
                || isMobile.iOS() || isMobile.Windows());
            }
        };

        var urlsAndroid = ["trulymadly://launch?action=buy_select","https://play.google.com/store/apps/details?id=com.trulymadly.android.app"];
        var urlsIOS = ['trulymadly://',"https://itunes.apple.com/in/app/trulymadly/id964395424?mt=8"];
        var redirectFn;
        var lastFired = new Date().getTime();

        if (navigator.userAgent.match(/Android/)) {
            if (navigator.userAgent.match(/Chrome/)) {
                redirectFn = redirectWebkit;
            } else {
                redirectFn = redirectDefault;
            }
        } else if(isMobile.iOS()) {
            if (navigator.userAgent.match(/WebKit/)) {
                redirectFn = redirectWebkit;
            } else {
                redirectFn = redirectDefault;
            }
        } else if (navigator.userAgent.match(/Silk/)) {
            redirectFn = redirectDefault;
        } else {
            redirectFn = redirectWeb;
        }

        function invoke(isAndroid, isIos) {
            var redirectUrls = [].concat(isAndroid? urlsAndroid : (isIos? urlsIOS : urlsAndroid));
            redirect(redirectFn, redirectUrls);
        }

        // redirect using specified function. If only one URL, we failed invoke and markets, force move to web
        function redirect(fn, urls) {
            var url;

            while (urls.length > 1) {
                url = urls.shift();

                if (url) {
                    return fn(url, urls);
                }
            }

            if (urls.length === 1) {
                window.location = urls.shift();
            }
        }

        // Webkit browsers (iOS/Chrome)
        function redirectWebkit(url, urls) {
            setTimeout(function() {
                if (!document.webkitHidden) {
                    now = new Date().getTime();
                    if(now - lastFired < 5000) {   //if it's been less than 5 seconds
                        redirect(redirectWebkit, urls);
                    }
                }
            }, 2000);

            window.location = url;
        }

        // Non-webkit browser redirection
        function redirectDefault(url, urls) {
            var iframe = document.createElement("iframe");

            iframe.style.border = "none";
            iframe.style.width = "1px";
            iframe.style.height = "1px";

            iframe.onload = function() {
                redirect(redirectDefault, urls);
            };
            iframe.src = url;

            // Body element doesn't exist until after page load. Need to defer.
            window.onload = function() {
                document.body.appendChild(iframe);
            }
        }

        function redirectWeb(url, urls) {
            window.location = urls[urls.length - 1];
        }
        //http://stackoverflow.com/questions/523266/how-can-i-get-a-specific-parameter-from-location-search
        var parseQueryString = function() {
            var str = window.location.search;
            var objURL = {};
            str.replace(
                    new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
                    function( $0, $1, $2, $3 ){
                        objURL[ $1 ] = $3;
                    }
            );
            return objURL;
        };


        if(isMobile.Android()) {
            invoke(true, false);
        } else if (isMobile.iOS()) {
            invoke(false, true);
        } else {
            window.location.href = "http://onelink.to/trulymadly";
        }

    </script>
</head>
<body>
<div id="siteInfo">
    trulymadly
</div>
</body>
</html>
