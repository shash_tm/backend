<?php
require_once dirname(__FILE__) . "/include/config.php";

try
{
    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'buy_sparks')
        $smarty->display ( "redirectSparks.tpl" );
    else if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'buy_select')
        $smarty->display ( "redirectSelect.tpl" );
    else
        $smarty->display ( "redirectMain.tpl" );

} catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_WARNING);
}