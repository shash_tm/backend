<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute("select distinct user_id from user_gcm_current where app_version_code='76' and status='login' limit 2000");
	
	if($sql->RowCount()>0){
		while ($row = $sql->FetchRow()){
			$user_id = $row['user_id'];
			$conn->Execute($conn->prepare("insert into user_flags (user_id,hide_myself,is_socket_enabled) values(?,?,?) on DUPLICATE KEY UPDATE hide_myself=?,is_socket_enabled=?"),
					array($user_id,NULL,'yes',NULL,'yes'));
		}
	}
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}


?>