<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../DBO/adminDBO.php";

/* Class to handle admin login, logout and permissions
 * @Sumit
 * 🖖 Live Long and Prosper. !!!
 */

class adminLogin 
{
	private $admin_id;
	private $location ;
	private $smarty ;
	private $error_login ;
	private $baseurl ;
	private $loginUrl ;
	private $admin_baseurl;
	private $adminPanelUrl ;
	private $to_be_redirected ;
	
	function __construct($location = "/dealAdmin/dealList.php")
	{
		global $smarty, $admin_baseurl ;
		$this->location= $location ;
		$this->loginUrl = "/dealAdmin/adminLogin.php" ;
		$this->smarty = $smarty ;
		$this->error_login ;
		$this->baseurl = $admin_baseurl ;
		$this->admin_id ;	
		$this->adminPanelUrl = "/admin/adminpanel.php" ;
		$this->to_be_redirected = true;
	}

	public function loginStuff()
	{
		if ($_POST ['signup'])
		{
			$this->setAdminSession();
		} else
		{
			$this->checkAdminSession();
		}
	}
	
	public function loginNew()
	{
		$username = $_POST ['email_id'];
		$password = $_POST ['password'];
		 $result = $this->checkAndSetSession($username, $password);
		 return $result ;
	}
	
	public function checkAdminSession() 
	{
		$this->admin_id = $_SESSION ['admin_id'];
		if(isset($this->admin_id) && $this->admin_id != null)
		{
			if(!isset($_COOKIE['admin_id']))
				setcookie("admin_id",$adminId,time()+3600*21*30,'/');
		}
		if($this->to_be_redirected == true)
			$this->redirectControl() ;
	}
	
	public function setAdminSession() 
	{
		$username = $_POST ['user'];
		$password = $_POST ['pass'];
		$result = $this->checkAndSetSession($username, $password);
		if($this->to_be_redirected == true)
			$this->redirectControl ();
	}
	private function checkAndSetSession($user_name, $password)
	{
		$rs = AdminDBO::verifyUsernameAndPassword ( $user_name, $password );
		if (count($rs) > 0)
		{
			$this->setInfoToSession($rs) ;
			return true ;
		}
		else
		{
			$this->error_login = true;
			return false;
		}
	}
	
	private function setInfoToSession($rs)
	{
		session_start ();
		setcookie ( "admin_id", $rs ['id'], time () + 3600 * 3, '/' );
		$_SESSION ['admin_id'] = $rs ['id'];
		$_SESSION ['super_admin'] = $rs ['super_admin'];
		$_SESSION ['admin_name'] = $rs ['user_name'];
		if ($rs ['privileges'])
			$_SESSION ['privileges'] = explode ( ',', $rs ['privileges'] );	
		else 
			$_SESSION ['privileges'] = array();
	}
	
	private function redirectControl() 
	{
		if (isset($_SESSION['admin_id']) )
		{
			if ($this->location == "dealAdmin/AdminLogin.php")
			{
				// display the default logged in page
				$redirectUrl = $this->baseurl . $this->adminPanelUrl ;
				header('Location: '.$redirectUrl); 
			}
			else 
			{
				return ;
			}
				 
		}
		else 
		{
			if ($this->location == "dealAdmin/AdminLogin.php")
			{
				$this->smarty->assign ("error_login", $this->error_login) ;
				$this->smarty->display ( "../templates/admin/adminlogin.tpl" );
			}
			else 
			{
				$redirectUrl = $this->baseurl . $this->loginUrl ;
				header('Location: '.$redirectUrl); 
			}
			die ;
		}
	}
	
	public function destroySession()
	{
		// to do this will destroy the session
		if(isset($_SESSION['admin_id'])){
		// 	$eventTrack = new EventTracking($_SESSION['user_id']);
		// 	$eventTrack->logAction("logout");

			session_unset();
			$params = session_get_cookie_params();
			session_destroy();
		}


		// unset cookies
		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				setcookie($name, '', time()-1000);
				setcookie($name, '', time()-1000, '/');
			}
		}
		if($this->to_be_redirected == TRUE)
			$this->redirectControl();
		
	}
	
	
	public function goToDefault() 
	{
		$redirectUrl = $this->baseurl . $this->adminPanelUrl ;
		header('Location: '.$redirectUrl);
	}
	public function setRedirectStatus($status)
	{
		$this->to_be_redirected = $status ;
	}
	
	public function googleLogin($data)
	{
		$gid = $data['gid'] ;
		$email_id = $data['email_id'] ;
		
		if( $this->isTrulyMadlyEmail($email_id) ==  FALSE)
		{
			return array("login" => FALSE,
					"tm_email_error" => true);
		}
			
			
		$adminRow = AdminDBO::getRowFromGid($gid) ;
		if(count($adminRow) == 0 )
		{
			// this email does not exist  . insert into admin table this email id with zero permission 
			AdminDBO::createNewAdmin($email_id, $gid);
			$newAdmin = AdminDBO::getRowFromGid($gid) ;
			$this->setInfoToSession($newAdmin);
			$new_user = true ;
			$login_status = true ;
		}
		else
		{
			// this email exists set the admin session
			if($adminRow['user_name'] == $email_id )
			{
				$this->setInfoToSession($adminRow);
				$new_user = false ;
				$login_status = true;
			}
			else 
			{
				$new_user = false ;
				$login_status = false;
			}
			
		}
		return array("login" => $login_status,
				     "new_user" =>$new_user
		);
		
	}
	private function isTrulyMadlyEmail($string)
	{
		$end = "@trulymadly.com";
		return (strpos($string, $end, strlen($string) - strlen($end)) !== false);
	}
	
	
}

?>