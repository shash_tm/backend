<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/dealManagerClass.php";
try 
{	
	if($_SESSION['super_admin'] == 'yes' || in_array('deal_view', $_SESSION['privileges'] ) )
	{
		if (  isset($_REQUEST['datespot_id']) && $_REQUEST['datespot_id'] != null)
		{
			$manager = new DealsManager() ;
			$deal = $manager->showDatespotDetails($_REQUEST['datespot_id']) ;
			if ($_REQUEST['action'] =='copy')
			{
				$deal['datespot_id'] = null ;
				$deal['hash_id'] = null ;
				$deal['status'] = null ;
			}
		}
	
		if((in_array('deal_edit',$_SESSION['privileges']) == false &&  $_SESSION['super_admin'] != 'yes') || $deal['status'] == 'active')
		{
			$smarty->assign ("read_only" , "true") ;
		}
			
		$smarty->assign ("deal" , $deal) ;
		$smarty->assign ("baseurl" , $admin_baseurl) ;
		$smarty->display ( "../templates/deals/eventForm.tpl" );
	}else{
		$smarty->display ( "../templates/deals/eventList.tpl" );
	}

} catch (Exception $e) {
echo $e->getMessage() ;  
} 