<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/dealManagerClass.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/eventCategoryClass.php";
try
{	
	$response_data = array();
	
	if(($_SESSION['super_admin'] == 'yes' || in_array('deal_edit', $_SESSION['privileges'] )) )
	{
		if(isset($_POST['datespot_id']) && $_POST['datespot_id'] != null && $_POST['action'] != 'add_new_DS')
		{
			$data= $_POST ;
			$datespot_id = $_POST['datespot_id'] ;
			$manager = new DealsManager() ;
			foreach ($data as $key => $val)
			{
				if( $key != 'datespot_id' && $key != 'locations_json'&& $key != 'hash_id')
				{
					$manager->editDatespot($datespot_id, $key, $val) ;
				}
			}
			CuratedDatesDBO::addDatespotLocations($data['locations_json'], $data['datespot_id'],$data['hash_id']) ;
		}
		else if ($_POST['action'] == 'add_new_DS')
		{
			$data= $_POST ;
			$manager = new DealsManager() ;
			$newDatespotId = $manager->addCompleteDatespot($data);  
			$response_data['new_ds'] = 'true';
			$response_data['datespot_id'] = $newDatespotId;
		}
		else if ($_POST['action'] =='deactivate' && isset($_POST['datespotHashId']) && $_POST['datespotHashId'] != null)
		{
			$manager = new DealsManager() ;
			$manager->changeStatus($_POST['datespotHashId'], 'inactive');
		}
		else if ($_POST['action'] =='activate' && isset($_POST['datespotHashId']) && $_POST['datespotHashId'] != null)
		{
			$manager = new DealsManager() ;
			$manager->changeStatus($_POST['datespotHashId'], 'active');
		}
		else if ($_POST['action'] =='update_rank' && isset($_POST['datespotHashId']) && $_POST['datespotHashId'] != null)
		{
			$rank = $_POST['rank'];
			$manager = new DealsManager() ;
			$manager->changeRank($_POST['datespotHashId'], $rank);
		}
		else if (($_POST['action'] =='delete' || $_POST['action'] =='undo_delete') && isset($_POST['datespotHashId']) && $_POST['datespotHashId'] != null)
		{
			$manager = new DealsManager() ;
			$manager->deleteDatespotAction($_POST['datespotHashId'], $_POST['action']);
		}
		else if ($_POST['action'] =='add_category')
		{
			$manager = new DealsManager() ;
			$response_data = $manager->addNewCategory($_POST['category_name'], $_POST['state_id'], $_POST['city_id'], $_POST['image_url'], $_POST['background_image_url']);
		}
		else if ($_GET['action'] =='get_category')
		{
			$manager = new DealsManager() ;
			$response_data['categories'] = $manager::getAllCategories($_GET['city_id'], $_GET['state_id']);
		}
		else
		{
			$response_data['error'] = "unKnownRequest" ;
		}
		
		$response_data['status'] = 'success' ;
	} else 
	{
		$response_data['status'] = 'failure' ;
	}
	print_r(json_encode($response_data));
} catch (Exception $e) {
echo $e->getMessage() ;  
} 