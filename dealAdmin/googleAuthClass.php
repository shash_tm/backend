<?php 
require_once '../google_analytics/google-api-php-client/src/Google/autoload.php';
 
class googleAuth
{
	protected $client ;
	private $googleProfileData ;
	private $googleClientId ;
	private $googleClientSecret ;
	function __construct(Google_Client $googleClient = NULL) 
	{
		global $googleClientId , $googleClientSecret ;
		$this->googleClientId = $googleClientId;
		$this->googleClientSecret = $googleClientSecret ;
		$this->client = $googleClient;
		$this->googleProfileData;
		if($this->client)
		{
			$this->client->setClientId($this->googleClientId);
			$this->client->setClientSecret($this->googleClientSecret);
			$this->client->SetRedirectUri('postmessage');
			$this->client->SetScopes('email');
		}
		
	} 
	
	
	public function verifyAuthCode($authCode)
	{
		$this->client->authenticate($authCode);	
		$this->getPayLoadFromGoogle();
		return $this->googleProfileData;
	}
	
	private function getPayLoadFromGoogle() 
	{
		$this->googleProfileData = $this->client->verifyIdToken()->getAttributes();
	}
	
	
}




?>