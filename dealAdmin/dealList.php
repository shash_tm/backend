<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/dealManagerClass.php";
try 
{	
//	$dealAdminObj  = new adminLogin("dealAdmin/dealView.php") ;
//	$dealAdminObj->checkAdminSession() ;
	if($_SESSION['super_admin'] == 'yes' || in_array('deal_view', $_SESSION['privileges'] ) )
	{
		$manager = new DealsManager() ;
		$deals = $manager->showDatespotsList(null, 0, 'datespot') ;
		//var_dump($deals);

		$smarty->assign ("baseurl" , $admin_baseurl) ;
		if(in_array('deal_edit',$_SESSION['privileges']) == false &&  $_SESSION['super_admin'] != 'yes')
		{
			$smarty->assign ("read_only" , "true") ;
		}else{
			$smarty->assign ("read_only" , "false") ;
		} 
		$smarty->assign ("deals" , $deals) ;
	}
	$smarty->display ( "../templates/deals/dealList.tpl" );

} catch (Exception $e) {
echo $e->getMessage() ;  
} 