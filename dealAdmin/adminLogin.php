<?php 
require_once dirname ( __FILE__ ) . "/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/googleAuthClass.php";


$dealAdminObj  = new adminLogin("dealAdmin/AdminLogin.php") ;
 
$data = array() ; 
$data['responseCode'] = 200;
if($_REQUEST['action'] == 'logout')
{
	$dealAdminObj->setRedirectStatus(FALSE);
	$dealAdminObj->destroySession();
	$data['status'] = "success" ;
	header('Location: '.$_SERVER['HTTP_REFERER']);
}
else if(isset($_POST['authCode']) && $_POST['authCode'] != NULL )
{
	$googleClient = new Google_Client;
	$auth = new googleAuth($googleClient);
	$googleData= $auth->verifyAuthCode($_POST['authCode']);  
	$data = array('gid' => $googleData['payload']['sub'] ,
			     'email_id' => $googleData['payload']['email'] );
	$result = $dealAdminObj->googleLogin($data);
	print_r(json_encode($result));

}
else if ($_POST['action'] == 'login_new' && isset($_POST['email_id']) && $_POST['email_id'] != null )
{
	$dealAdminObj->setRedirectStatus(FALSE) ;
	if (isset($_POST['password']) && $_POST['password'] != null) 
	{
		$login = $dealAdminObj->loginNew(); 
	    if($login == true)
	    {
	    	$data['status'] = "success" ;
	    }
	    else
	    {
	    	$data['status'] = "failure" ;
	    }
	}  
	else if(isset($_POST['password']) && $_POST['password'] != null)
	{ 
		$result = $dealAdminObj->googleLogin($_POST);
		
		if($result['login_status'] == true)
		{
			$data['status'] = "success";
		}
		else 
		{
			$data['status'] = "failure" ;
		}
		$data['new_user'] = $result['new_user'] ;
	}
print_r(json_encode($data));
}
else if($_REQUEST['action'] == 'get_google_id')
{
	$data = array("status" => "success",
			      "google_id" => $googleClientId );
	print_r(json_encode($data));
}
else if($_POST ['signup'] )
{
	$dealAdminObj->loginStuff();
}
else 
{
	$smarty->display ( "../templates/ReportingDashboard/queryListNew.tpl" );
}

?> 