<?php 
require_once dirname ( __FILE__ ) . "/adminPermissionsClass.php";
$data = array();
if($_SESSION['super_admin'] == 'yes')
{
	if($_POST['action'] == 'grant_privileges'  && isset($_POST['admin_id']) && isset($_POST['privileges']) )
	{
		$admin_id = $_POST['admin_id'] ;
		$permissions_arr = $_POST['privileges'] ;
		adminPermissions::grantPermissions($admin_id, $permission_arr) ;
		$data['responseCode'] = 200;
		$data['status'] = 'success' ;
	}
	else 
	{
		$data['responseCode'] = 403 ;
		$data['status'] = 'failure' ;
	}
}
else 
{
	$data['responseCode'] = 401 ;
	$data['status'] = 'unauthorised' ;
}


print_r(json_encode($data)) ;

?>