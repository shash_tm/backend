<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname(__FILE__)."/../lib/wideimage/WideImage.php";
require_once dirname ( __FILE__ ) . "/../photomodule/photoUtils.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/dealManagerClass.php";


$manager = new DealsManager();


try 
{	
	if(isset($_FILES['fileName']['tmp_name'])){
		$fileContents = file_get_contents($_FILES['fileName']['tmp_name']);
		$error="";
		$fileContents = json_decode($fileContents, true);
		if($fileContents == null || !isset($fileContents))
			$error="Invalid Json Detected";
		foreach ($fileContents['deals'] as $deal){
			foreach ($deal as $key => $value){
				$tempImageArray = array();
				if($key == 'images_json' || $key == 'menu_image_json'){
					foreach ($deal[$key] as $value){
						$tempImageArray[] = PhotoUtils::saveFileFromUrl($value , 'datespot');		
					}
					$deal[$key] = json_encode($tempImageArray);
				}else if($key == 'list_view_image'){
					$deal[$key] = PhotoUtils::saveFileFromUrl($deal[$key], 'datespot');  
				}
			}
			$deal['hashtag_json'] = json_encode($deal['hashtag_json']);
			$deal['terms_and_conditions'] = json_encode($deal['terms_and_conditions']);
			$manager->addCompleteDatespot($deal);
		}
	}else{
		echo "Some error YO";
	}
	$redirectUrl = admin_baseurl.'/dealAdmin/dealList.php';
	if($error != null)
		$redirectUrl .= '?error='.$error;
	header('Location: '.$redirectUrl);
	//$smarty->display ( "../templates/deals/dealList.tpl" );	
} catch (Exception $e) {
echo $e->getMessage() ;  
} 