<?php
require_once dirname ( __FILE__ ) . "/../curatedDeals/dealManagerClass.php";
try 
{
	$manager = new DealsManager();
	if(isset($_POST['zoneName'])){
		echo json_encode($manager->addNewZone(trim($_POST['zoneName']),$_POST['stateId'],$_POST['cityId'])) ;
	}
	else {
		echo json_encode($manager->getZonesData());
	}

} catch (Exception $e) {
echo $e->getMessage() ;  
} 