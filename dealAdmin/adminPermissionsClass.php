<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../DBO/adminDBO.php";

/*
 * class to manage all actions which revokes or grants the privileges to admin 
 * only for a  super admin 
 * Author :: Sumit
 * 🖖 Live Long and Prosper. 
 */

class adminPermissions 
{
	public function grantPermissions($admin_id, $permission_arr) 
	{
		$permissions = implode(",", $permission_arr) ;
		AdminDBO::setPrivileges($admin_id,$permissions);
	}
}

?>