<?php 
/*
 * @author Rajesh
 */
require_once dirname( __FILE__ ) . "/include/config.php" ;
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname( __FILE__ ) . "/include/User.php" ;
require_once dirname ( __FILE__ ) . "/include/tbconfig.php";
require_once dirname ( __FILE__ ) . "/include/tbmessages.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/otp.php";
require_once dirname ( __FILE__ ) . '/abstraction/query_wrapper.php';
require_once dirname(__FILE__).'/logging/EventTrackingClass.php';

class TrustBuilder{
	private $userId;
	private $trustScore = 0;
	private $fbConnected = false;
	private $conn;
	private $userUtils;
	private $userUtilsDBO;
	private $device_id;
	function __construct($user_id=NULL) {
		global $conn;
		$this->userId = $user_id;
		$this->conn = $conn;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->userUtils = new UserUtils();
		$this->userUtilsDBO = new userUtilsDBO();
		$this->device_id=null;
		$this->event_logging=new EventTrackingClass();

	}
//get the device id from user_gcm_current corresponding to user_id or the request headers if present
	private function getDeviceId()
	{
		$this->device_id=Utils::getDeviceIdFromHeader();
		
		// if device id is not present in request headers the fetch it from user_gcm_current 
		if( (!isset($this->device_id)) && isset($this->userId))
		{
		$rs = $this->conn->Execute($this->conn->prepare("Select device_id from user_gcm_current where user_id=? order by tstamp desc limit 1"),array($this->userId));
		if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
			    if(isset($row['device_id']))
			    	$this->device_id=$row['device_id'];
		}	
		else
		{
			$rs2 = $this->conn->Execute($this->conn->prepare("Select device_id from user where user_id=?"),array($this->userId));
			if ($rs2->RowCount () > 0) {
				$row = $rs2->FetchRow ();
				if(isset($row['device_id']))
					$this->device_id=$row['device_id'];
			}
			
		}
		}
	}
//checks if the device id is blocked or not
	public function checkDeviceId($device_id)
	{
		$rs = $this->conn->Execute($this->conn->prepare("Select * from block_fids where device_id=?"),array($device_id));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow ();
			$response['status']='success';
			$response['reason']=$row['reason'];
		}
		else 
			$response['status']='fail';
		return $response;
	}
	
	 /*function to assign credits to a user
	 *$points - number of points to be assigned
	 *$type - assigned through which function i.e. facebook, linkedin etc...
	 */
	public function creditPoint($points,$type,$expiryDays = -1){
		try{
			$rs = $this->conn->Execute($this->conn->prepare("Select * from user_current_credits where user_id=?"),array($this->userId));
			if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
				$current_expiry = $row['expiry_date'];
				$plan_expiry = date("Y-m-d", strtotime("+ ".$expiryDays." days"));
				$date_diff = strtotime($plan_expiry) - strtotime($current_expiry);
				if($date_diff>0){
					$expiry_date = $plan_expiry;
				}else{
					$expiry_date = $current_expiry;
				}
				$query="update user_current_credits SET current_credits=?,total_assigned_credits=?,expiry_date=? where user_id=? ";
				$param_array=array($row['current_credits']+$points,$row['total_assigned_credits']+$points,$expiry_date,$this->userId);
				$tablename='user_current_credits';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
				
				
				/*$this->conn->Execute($this->conn->prepare("update user_current_credits SET current_credits=?,total_assigned_credits=?,expiry_date=? where user_id=? "),
						array($row['current_credits']+$points,$row['total_assigned_credits']+$points,$expiry_date,$this->userId));*/
			}else{
				$expiry_date = date("Y-m-d", strtotime("+ ".$expiryDays." days"));
				
				$query="insert into user_current_credits (user_id,current_credits,total_assigned_credits,expiry_date) values(?,?,?,?)";
				$param_array=array($this->userId,$points,$points,$expiry_date);
				$tablename='user_current_credits';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename);
				
				/*$this->conn->Execute($this->conn->prepare("insert into user_current_credits (user_id,current_credits,total_assigned_credits,expiry_date) values(?,?,?,?)"),
						array($this->userId,$points,$points,$expiry_date));*/
			}
			
			$query="insert into user_credit_logs (user_id,credit_assgined,credit_type,tstamp) values(?,?,?,now())";
			$param_array=array($this->userId,$points,$type);
			$tablename='user_credit_logs';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename);
			
			/*$this->conn->Execute($this->conn->prepare("insert into user_credit_logs (user_id,credit_assgined,credit_type,tstamp) values(?,?,?,now())"),
					array($this->userId,$points,$type));*/
		}catch(Exception $e) {
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
				//	echo $e->getMessage();
			// 			die;
		}
	}



	/**
	 *function to authenticate a user based on certain condition
	 */
	public function authenticateUser(){
			$rs = $this->conn->Execute($this->conn->prepare("SELECT trust_score from user_trust_score where user_id=?"),array($this->userId));
			if($rs->RowCount() > 0){
				$row = $rs->FetchRow();
				
// 				$ud = new UserData($this->userId);
// 				$gender = $ud->fetchGender();
// 				$status = $ud->fetchStatus();
// 				$authenticityConstant = ($gender == "M")?TrustAuthenticConstants::TrustAuthenticConstantMale:TrustAuthenticConstants::TrustAuthenticConstantFemale;
// 				/*
// 				 * ABTesting change the authenticity constant for female non-authentic users
// 				 */
				
// 				if(TrustAuthenticConstants::ABTestVerification && $gender == 'F' && $status == 'non-authentic') {
// 					$authenticityConstant = $this->getAuthenticityConstForABTest();
// 				}

				$authenticityConstant = $this->getThreshold();
				
				if($row['trust_score']>=$authenticityConstant){
					$query="update user SET status=? where user_id=?";
					$param_array=array("authentic",$this->userId);
					$tablename='user';
					$rs=Query_Wrapper::UPDATE($query, $param_array,$tablename,"master",$this->userId);
					/*$this->conn->Execute($this->conn->prepare("update user SET status=? where user_id=?"),
					array("authentic",$this->userId));
					*/	
					/**
					 * if call is from mobile and with app version code more than 100, i.e. iOS launch start
					 * then clear the match set since we don't have expiry over the initial set
					 * the other work around is set expiry for the existing match set
					 * @Himanshu
					 */
					//$this->userUtils->clearMatchesForNonAuthToAuthForiOS($this->userId);
					$this->userUtils->clearRecommendationCache($this->userId, null, true);
					return true;
				}else{
					$query="update user SET status=? where user_id=?";
					$param_array=array("non-authentic",$this->userId);
					$tablename='user';
					$rs=Query_Wrapper::UPDATE($query, $param_array,$tablename,"master",$this->userId);
					/*$this->conn->Execute($this->conn->prepare("update user SET status=? where user_id=?"),
							array("non-authentic",$this->userId));*/
				}
			}else{
				$query="update user SET status=? where user_id=?";
				$param_array=array("non-authentic",$this->userId);
				$tablename='user';
				$rs=Query::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("update user SET status=? where user_id=?"),
						array("non-authentic",$this->userId));*/
					
			}
		return false;
	}

	/**
	 * update the user trustscore after facebook verification
	 */
	public function updateFacebookTrustScore($connections){
		global $tbNumbers;
		if(!isset($connections)) {
			$connections = -1;
		}
			$rs = $this->conn->Execute($this->conn->prepare("Select * from user_trust_score where user_id=?"),array($this->userId));
			if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
				if($row['fb_connections']!='' || $row['fb_connections']!=NULL) {
					$updated_trustScore = $row['trust_score'];
				}
				else {
					$updated_trustScore = $row['trust_score']+$tbNumbers['score']['facebook'];
				}
				if($updated_trustScore>100) {
					$updated_trustScore = 100;
				}
				

				$query="update user_trust_score SET fb_connections=?,fb_credits = ?,trust_score=? where user_id=?";
				$param_array=array($connections,$tbNumbers['credits']['facebook'],$updated_trustScore,$this->userId);
				$tablename='user_trust_score';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				/*
				$this->conn->Execute($this->conn->prepare("update user_trust_score SET fb_connections=?,fb_credits = ?,trust_score=? where user_id=? "),
						array($connections,$tbNumbers['credits']['facebook'],$updated_trustScore,$this->userId));*/
				//$this->authenticateUser();
			}else{
				$query="insert into user_trust_score (user_id,fb_connections,fb_credits,trust_score) values(?,?,?,?)";
				$param_array=array($this->userId,$connections,$tbNumbers['credits']['facebook'],$tbNumbers['score']['facebook']);
				$tablename='user_trust_score';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("insert into user_trust_score (user_id,fb_connections,fb_credits,trust_score) values(?,?,?,?)"),
						array($this->userId,$connections,$tbNumbers['credits']['facebook'],$tbNumbers['score']['facebook']));*/
			}
	}

	public function updateLinkedinTrustScore($connections,$position){
		global $tbNumbers;
		if($position=="")
			$position = null;
		$rs = $this->conn->Execute($this->conn->prepare("Select * from user_trust_score where user_id=?"),array($this->userId));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow ();
			if($row['linkedin_connections']=='' || $row['linkedin_connections']==NULL){
				$updated_trustScore = $row['trust_score']+$tbNumbers['score']['linkedin'];
				if($updated_trustScore>100) {
					$updated_trustScore = 100;
				} 
				$query="update user_trust_score SET linkedin_connections=?,linked_credits=?,trust_score=?,linkedin_designation=? where user_id=?";
				$param_array=array($connections,$tbNumbers['credits']['linkedin'],$updated_trustScore,$position,$this->userId);
				$tablename='user_trust_score';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
					
				
			 /*	$this->conn->Execute($this->conn->prepare("update user_trust_score SET linkedin_connections=?,linked_credits=?,trust_score=?,linkedin_designation=? where user_id=? "),
						array($connections,$tbNumbers['credits']['linkedin'],$updated_trustScore,$position,$this->userId));*/
			}else{
				$query="update user_trust_score SET linkedin_connections=?,linkedin_designation=? where user_id=?";
				$param_array=array($connections,$position,$this->userId);
				$tablename='user_trust_score';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				/*$this->conn->Execute($this->conn->prepare("update user_trust_score SET linkedin_connections=?,linkedin_designation=? where user_id=? "),
						array($connections,$position,$this->userId));*/
			}
		}else{
			$query="insert into user_trust_score (user_id,linkedin_connections,trust_score,linked_credits,linkedin_designation) values(?,?,?,?,?)";
			$param_array=array($this->userId,$connections,$tbNumbers['score']['linkedin'],$tbNumbers['credits']['linkedin'],$position);
			$tablename='user_trust_score';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
			
			/*$this->conn->Execute($this->conn->prepare("insert into user_trust_score (user_id,linkedin_connections,trust_score,linked_credits,linkedin_designation) values(?,?,?,?,?)"),
					array($this->userId,$connections,$tbNumbers['score']['linkedin'],$tbNumbers['credits']['linkedin'],$position));*/
		}
	}

	public function updateMobileTrustScore($number){
		global $tbNumbers;
		$number = substr_replace($number,"XXXXX",5,5);
		$rs = $this->conn->Execute($this->conn->prepare("Select * from user_trust_score where user_id=?"),array($this->userId));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow ();
			if($row['mobile_number']!='' || $row['mobile_number']!=NULL)
				exit;
			$updated_trustScore = $row['trust_score']+$tbNumbers['score']['mobile'];
			if($updated_trustScore>100) {
				$updated_trustScore = 100;
			}
			
			$query="update user_trust_score SET mobile_number=?,mobile_credits=?,trust_score=? where user_id=?";
			$param_array=array($number,$tbNumbers['credits']['mobile'],$updated_trustScore,$this->userId);
			$tablename='user_trust_score';
			$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
			/*	
			$this->conn->Execute($this->conn->prepare("update user_trust_score SET mobile_number=?,mobile_credits=?,trust_score=? where user_id=? "),
					array($number,$tbNumbers['credits']['mobile'],$updated_trustScore,$this->userId));*/
		}else{
			$query="insert into user_trust_score (user_id,mobile_number,trust_score,mobile_credits) values(?,?,?,?)";
			$param_array=array($this->userId,$number,$tbNumbers['score']['mobile'],$tbNumbers['credits']['mobile']);
			$tablename='user_trust_score';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
			
			/*$this->conn->Execute($this->conn->prepare("insert into user_trust_score (user_id,mobile_number,trust_score,mobile_credits) values(?,?,?,?)"),
					array($this->userId,$number,$tbNumbers['score']['mobile'],$tbNumbers['credits']['mobile']));*/
		}
	}

	public function updateIdTrustScore($id_type){
			global $tbNumbers;
			$rs = $this->conn->Execute($this->conn->prepare("Select * from user_trust_score where user_id=?"),array($this->userId));
			if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
				//var_dump($row["id_proof"]);
				if ($row["id_proof"] != 'active')
				{
					$updated_trustScore = $row['trust_score']+$tbNumbers['score']['photoid'];
					if($updated_trustScore>100) {
						$updated_trustScore = 100;
					}
					
					$query="update user_trust_score SET id_proof='active',trust_score=?,id_credits = ?,id_type=? where user_id=?";
					$param_array=array($updated_trustScore,
								$tbNumbers['credits']['photoid'],
								$id_type,
								$this->userId);
					$tablename='user_trust_score';
					$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
						
				/*$this->conn->Execute($this->conn->prepare("update user_trust_score SET id_proof='active',trust_score=?,id_credits = ?,id_type=? where user_id=? "),
						array($updated_trustScore,
								$tbNumbers['credits']['photoid'],
								$id_type,
								$this->userId));*/
			}
			}else{
				
				$query="insert into user_trust_score (user_id,id_proof,trust_score,id_credits, id_type) values(?,'active',?,?,?)";
				$param_array=array(  $this->userId,
								$tbNumbers['score']['photoid'],
								$tbNumbers['credits']['photoid'],
								$id_type);
				$tablename='user_trust_score';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
					
				
				/*$this->conn->Execute($this->conn->prepare("insert into user_trust_score (user_id,id_proof,trust_score,id_credits, id_type) values(?,'active',?,?,?)"),
						array(  $this->userId,
								$tbNumbers['score']['photoid'],
								$tbNumbers['credits']['photoid'],
								$id_type));*/
			}
			
	}
	public function updateEmploymentTrustScore() {
		global $tbNumbers;
			$res = $this->conn->Execute($this->conn->prepare("SELECT company_name, show_name from user_employment_proof where user_id=?"),array($this->userId));
			$rws = $res->FetchRow();
			$company=$rws['company_name'];
			if ($rws['show_name']==='no')
			{
				$new_company= $this->hidecompanyname ($company);
			}
			else {
				$new_company=$company;
			}
			//echo $this->userId;
			$rs = $this->conn->Execute ( $this->conn->prepare ( "Select * from user_trust_score where user_id=?" ), array ($this->userId));
				
				
			if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
				//var_dump ($row);
				if($row['employment']!=null) die;
				
				$updated_trustScore = $row['trust_score']+$tbNumbers['score']['employment'];
				if($updated_trustScore>100) {
					$updated_trustScore = 100;
				}
				
				$query="update user_trust_score SET employment=?,trust_score=?,employment_credits=?,company_name=?, employment_verified_on = now() where user_id=? ";
				$param_array=array (
						'active',
						$updated_trustScore,
						$tbNumbers['credits']['employment'],
						$new_company,
						$this->userId
				) ;
				$tablename='user_trust_score';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				/*
				$this->conn->Execute ( $this->conn->prepare ( "update user_trust_score SET employment=?,trust_score=?,employment_credits=?,company_name=?, employment_verified_on = now() where user_id=? " ), 
				array (
						'active',
						$updated_trustScore,
						$tbNumbers['credits']['employment'],
						$new_company,
						$this->userId
				) );*/
			} else {
				
				$query="insert into user_trust_score (user_id, employment, trust_score, employment_credits, company_name, employment_verified_on) values(?,?,?,?,?,now()) ";
				$param_array=array (
						$this->userId,
						'active',
						$tbNumbers['score']['employment'],
						$tbNumbers['credits']['employment'],
						$new_company)  ;
				$tablename='user_trust_score';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
				/*$this->conn->Execute ( $this->conn->prepare ( "insert into user_trust_score (user_id, employment, trust_score, employment_credits, company_name, employment_verified_on) values(?,?,?,?,?,now())" ), array (
						$this->userId,
						'active',
						$tbNumbers['score']['employment'],
						$tbNumbers['credits']['employment'],
						$new_company

				) );*/
			}
	}
	public function hidecompanyname($str){
		$len = strlen($str);
		return substr($str, 0,1). str_repeat('X',$len - 2) . substr($str, $len - 1 ,1);
	}

	/**
	 * return the trust score and other details like credits assigned from each verification of the user.
	 */
	public function getTrustScore(){
			$rs = $this->conn->Execute($this->conn->prepare("Select * from user_trust_score where user_id=?"),array($this->userId));
			if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
				$this->trustScore = isset($row['trust_score'])?$row['trust_score']:0;
				$this->trustScore = ($this->trustScore>100)?100:$this->trustScore;
				return $row;
			}else{
				$return['trust_score'] = 0;
				return $return;
			}
	}



	/**
	 *function to do the facebook verification
	 */
	public function verifyFacebook($token,$is_registeration_complete=true,$connectedFrom = 'trustbuilder'){
		$return = NULL;
		global $tbNumbers;
		
		try {
			$fb = new fb($token);
			$fb->getData();
			
			if(!$fb->testfids($fb->data['id'])){
				$verify = $this->authoriseFBAccount($fb->data);
				if(!$verify['flag']){
					$fb->logFacebook($this->userId, $verify['message']);
					throw new Exception($verify['message']);
				}
			}
			$fb->storeFacebook($this->userId,'NEW',$connectedFrom);
			$diff = $this->checkFbTMDiff();
			//var_dump($diff);exit;
			if($is_registeration_complete) {
				$query="update user SET fid=?,is_fb_connected='Y' where user_id=?";
				$param_array=array($fb->data['id'],$this->userId);
				$tablename='user';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
					
				/*$this->conn->Execute($this->conn->prepare("update user SET fid=?,is_fb_connected='Y' where user_id=?"),
						array($fb->data['id'],$this->userId));*/

				if(isset($diff) && count($diff)==0){
					//$this->creditPoint($tbNumbers['credits']['facebook'],'facebook',$tbNumbers['validity']['facebook']);
					$this->updateFacebookTrustScore($fb->data['connections']);
				}else{
					if($diff['name'] && $diff['age']){
						$this->mismatchReason($this->userId, "Name and Age mismatch");
						$fb->logFacebook($this->userId, "Name and Age mismatch : ".json_encode($diff));
					}else if($diff['name']){
						$this->mismatchReason($this->userId, "Name mismatch");
						$fb->logFacebook($this->userId, "Name mismatch : ".json_encode($diff));
					}else if($diff['age']){
						$this->mismatchReason($this->userId, "Age mismatch");
						$fb->logFacebook($this->userId, "Age mismatch : ".json_encode($diff));
					}else if($diff['gender']){
						$this->mismatchReason($this->userId, "Gender mismatch");
						$fb->logFacebook($this->userId, "Gender mismatch : ".json_encode($diff));
						//$this->blockUser($this->userId);
					}
				}
			}

			$companies = $fb->processCompanies();
			$colleges = $fb->processColleges();
			
			$this->updateCompaniesnColleges($companies,$colleges);
			
			if(isset($diff) && count($diff)>0){
				if($connectedFrom == 'trustbuilder'){
// 					$return['status'] = 'mismatch';
// 					if($diff['name'] && $diff['age']){
// 						$return['error'] = "Name and Age mismatch";
// 					}else if($diff['name']){
// 						$return['error'] = "Name mismatch";
// 					}else if($diff['age']){
// 						$return['error'] = "Age mismatch";
// 					}else if($diff['gender'])
// 						$return['error'] = "Gender mismatch";
					
// 					$return['responseCode'] = 403;
// 					$return['diff'] = $diff;
					$return = $this->checkFBDiffType($diff);
				}else{
					$return['responseCode'] = 200;
					$return['status']='success';
					$return['connections'] = (isset($fb->data['connections']))?$fb->data['connections']:"Some";
				}
			}
			else{
				$return['responseCode'] = 200;
				$return['status']='success';
				$return['connections'] = (isset($fb->data['connections']))?$fb->data['connections']:"Some";
			}
		}
		catch(Exception $e) {
			$return['responseCode'] = 403;
			$return['status']='fail';
			$return['error'] = $e->getMessage();
		}
		return $return;
	}

	private function updateCompaniesnColleges($companies,$colleges){
		$user_data = array();
		if(isset($companies) || isset($colleges)) {
			$userN = new User();
			$user_data = $userN->getUserCollegenCompany($this->userId);
			$company = (isset($user_data['company_name'])) ? json_decode($user_data['company_name'],true) : NULL;
			$college = (isset($user_data['institute_details'])) ? json_decode($user_data['institute_details'],true) : NULL;
			if(!isset($company)) {
				$company = $companies;
			}elseif(count($company)==0) {
				$company = $companies;
			}else {
				$company = json_encode($company);
			}
			if(!isset($college)) {
				$college = $colleges;
			}elseif(count($college)==0) {
				$college = $colleges;
			}else {
				$college = json_encode($college);
			}
			
			$query="update user_data SET company_name=?, institute_details=? where user_id=?";
			$param_array=array($company,$college,$this->userId);
			$tablename='user_data';
			$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
			
			/*$this->conn->Execute($this->conn->prepare("update user_data SET company_name=?, institute_details=? where user_id=?"),
					array($company,$college,$this->userId));*/
			
		}
	}
	
	public function checkFBDiffType($diff) {
		$return['status'] = 'mismatch';
		if($diff['name'] && $diff['age']){
			$return['error'] = "Name and Age mismatch";			
		}else if($diff['name']){
			$return['error'] = "Name mismatch";
		}else if($diff['age']){
			$return['error'] = "Age mismatch";
		}else if($diff['gender'])
			$return['error'] = "Gender mismatch";
		
		$return['responseCode'] = 403;
		$return['diff'] = $diff;
		
		return $return;
	}

	public function mismatchReason($user_id,$reason){
		$query="update user_facebook SET mismatch=? where user_id=?";
		$param_array=array($reason,$user_id);
		$tablename='user_facebook';
		$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
			
		/*$this->conn->Execute($this->conn->prepare("update user_facebook SET mismatch=? where user_id=?"),
				array($reason,$user_id));*/
	}

	/**
	 * verifying the new data imported from fb
	 * @param $token
	 * @Himanshu
	 */
	public function imposeReimportedData($token){
		global $tbNumbers;
		$fb = new fb($token);
		$fb->getBasicData();
		//trigger_error("PHP Web:".$this->userId." get Basic Data", E_USER_WARNING);
		$uu = new User();
		if(!$this->checkfidforSync($fb->data['id'])){
			$return['responseCode'] = 403;
			$return['status']='fail';
			$return['error'] = 'Please use the same facebook account used to create the profile';
			return $return;
		}
		$reimportData = $fb->getReimportData();
		
		//trigger_error("PHP Web:".$this->userId." get Reimport Data from facebook ".json_encode($fb->data), E_USER_WARNING);
		//var_dump($reimportData);exit;
		$fb->storeReimportedData($this->userId);
		
		//trigger_error("PHP Web:".$this->userId." store reimport Data in user_facebook", E_USER_WARNING);
		
		$this->overrideTMFromFb($reimportData);
		$this->mismatchReason($this->userId,null);
		
		//trigger_error("PHP Web:".$this->userId." store null in mismatch in user_facebook", E_USER_WARNING);
		
		$connections = $uu->getFbConnectionCount($this->userId);
		//$this->creditPoint($tbNumbers['credits']['facebook'],'facebook',$tbNumbers['validity']['facebook']);
		$this->updateFacebookTrustScore($connections);
		
		//trigger_error("PHP Web:".$this->userId." update user trust score with connections ".$connections, E_USER_WARNING);
		$return['responseCode'] = 200;
		$return['status']='success';
		$return['connections'] =  (isset($connections))?$connections:"Some";

		return $return;
	}
	/*
	 * 
	 * function to check fid for sync by rajesh
	 */
	public function checkfidforSync($fid){
		
		$sql = $this->conn->Prepare("SELECT fid from user_facebook where user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		if($rs->RowCount () > 0){
			$row = $rs->FetchRow();
			if($row['fid']==$fid)
				return true;
			else 
				return false;
		}else 
			return false;
		
	}
	/**
	 *function to check FB account to be authorised is valid or not.
	 */
	public function authoriseFBAccount($data,$from_login=false){
		$return['flag'] = true;
		$return['message'] = "";
		global $tbMessages;
		
		$isBlock = $this->isFidBlock($data['id']);
		if($isBlock['status']) {
			if($isBlock['reason'] == 'married') {
				$return['message'] = $tbMessages['facebook']['married'];
				$return['flag'] = false;
				return $return;
			} 
		}
		
		if(isset($data['connections']) && (($data['gender']=='male' && $data['connections']<50)||($data['gender']=='female' && $data['connections']<20)))
		{
		$return['message'] = "Oho! You don't have enough connections for us to validate your Facebook account.";//"Too few friend connections for us to validate your facebook account";
		$return['flag'] = false;
		return $return;
		}
		

		if($data['relationship_status']=='Married'){
			//$return['message'] = "Looks like you have already found someone special! TrulyMadly is only for Singles...";
			$return['message'] = $tbMessages['facebook']['married'];
			$return['flag'] = false;
			$this->blockUser($data['id'],"married");
			return $return;
		} 

		// check age of user
		if(!isset($data['birthday'])) {
			$return['message'] = "Tweak your privacy settings on Facebook to help us retrieve your birth date! ";
			$return['flag'] = false;
			return $return;
		}
		
		if (isset($data['birthday'])) {
			$age = date_create($data['birthday'])->diff(date_create('today'))->y;
			if($age<18) {
				$return['message'] = "Our minimum age for sign up is 18 yrs. You are ".$age." on Facebook.";
				$return['flag'] = false;
				return $return;
				//$this->blockUser($this->userId);
			}
			if($age>70) {
				$return['message'] = "Our maximum age for sign up is 70 yrs. You are ".$age." on Facebook.";
				$return['flag'] = false;
				return $return;
				//$this->blockUser($this->userId);
			}
		}
		
		if(!isset($data['gender'])){
			$return['message'] = "We can't pull your Gender from Facebook.";
			$return['flag'] = false;
			return $return;
		}
		
		if(!isset($data['first_name']) || !isset($data['last_name'])){
			$return['message'] = "We can't pull your Name from Facebook.";
			$return['flag'] = false;
			return $return;
		}

		if(!$from_login){
			$sql = $this->conn->Prepare("select gender from user where user_id = ?");
			$res = $this->conn->Execute($sql, array($this->userId));
			$row = $res->FetchRow();
			if($data['gender']=='male')
				$data['gender'] = 'M';
			else if($data['gender']=='female')
				$data['gender'] = 'F';
			if($data['gender'] != $row['gender']){
				$return['message'] = "Er, apologies, but there seems to be a mismatch in the gender as selected by you.";
				$return['flag'] = false;
			}
		}
		/**
		 * adding logging @Himanshu
		 */
		/*if($return['flag'] == false){
			$this->conn->Execute($this->conn->prepare("insert into log_facebook (user_id,fid,username,name,first_name,last_name,link,location,gender,email,token, error)
					values (?,?,?,?,?,?,?,?,?,?,?,?)"),
					array($user_id,$data['id'],$data['username'],$data['name'],$data['first_name'],$data['last_name'],
							$data['link'],$data['location'],$data['gender'],$data['email'],$this->token, $return['message']));

		}*/
		return $return;
	}

	/*
	 * block fid whose relationship status is married
	*/
	
	public function blockUser($fid,$reason) {
		$this->getDeviceId();
		if(isset($this->device_id))
		{
			
			$query="insert ignore into block_fids (fid, user_id, device_id, reason,tstamp) values(?,?,?,?,now())";
			$param_array=array (
					$fid,
					$this->userId,
					$this->device_id,
					$reason
			) ;
			$tablename='block_fids';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename);
			/*
			$this->conn->Execute ( $this->conn->prepare ( "insert ignore into block_fids (fid, user_id, device_id, reason,tstamp) values(?,?,?,?,now())" ), array (
					$fid,
					$this->userId,
					$this->device_id,
					$reason
			) );*/
		}
		else 	
		{
			$query="insert ignore into block_fids (fid, user_id, reason,tstamp) values(?,?,?,now())";
			$param_array=array (
				$fid,
				$this->userId,
				$reason
		      );
			$tablename='block_fids';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename);
				
			/*
			$this->conn->Execute ( $this->conn->prepare ( "insert ignore into block_fids (fid, user_id, reason,tstamp) values(?,?,?,now())"), array (
				$fid,
				$this->userId,
				$reason
		) );*/
		}
		
		
		if(isset($this->userId))
		{
			$last_changed_status=NULL;
		    $rs = $this->conn->Execute($this->conn->prepare("SELECT status from user where user_id=?"),array($this->userId));
		    if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow();
			$last_changed_status=$row['status'];
		    }
			
		    
		    $query="UPDATE user SET status = 'blocked' , last_changed_status =? where user_id=?";
		    $param_array=array (
					$last_changed_status,
					$this->userId
			);
		    $tablename='user';
		    $res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
		    	
			/*$this->conn->Execute ( $this->conn->prepare ( "UPDATE user SET status = 'blocked' , last_changed_status =? where user_id=?"), array (
					$last_changed_status,
					$this->userId
			) );*/	
		}
		
	}
	
	/*
	 * return true if fid exist in block_fids table
	*
	*/
	private function isFidBlock($fid) {
		$return['status'] = false;
		$return['reason'] = "";
		$rs = $this->conn->Execute($this->conn->prepare("SELECT fid,reason from block_fids where fid=?"),array($fid));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow();
			$return['status'] = true;
			$return['reason'] = (isset($row['reason'])) ? $row['reason'] : "";
		}
		return $return;
	}
	
	/**
	 *function to upload documents
	 *$data is the post request recieved
	 *$name is the name to be assigned to the document
	 *$data["type"] 1-Id upload 2- Address upload 3 - Employment upload
	 */
	public function documentUpload($data,$name){
		global $image_basedir;
		$error = 0;
		
		// array of extensions of valid document
		$invalidExtensions=array (
			"application/exe",
			"application/app",
			"application/dmg",
			"application/zip",
			"application/apk",	
			"application/osx",
			"application/out",
			"application/run",
			"application/prg"	
		);
		
		try{
			if(!($_FILES["file"]["error"] === UPLOAD_ERR_OK)) {
				$error=1;
			}
		 	else {
		 		if(exif_imagetype($_FILES["file"]['tmp_name'])===FALSE){     // check for image file
		 			$error = 1;
		 			if(in_array($_FILES["file"]["type"],$invalidExtensions)) {      // check for non-image file
		 				$error=1;
		 			}else {
		 				$error = 0;
		 			}
				}else {
					if(in_array($_FILES["file"]["type"],$invalidExtensions)) {      // check for non-image file
						$error=1;
					}
				}
		 	}
		 	
		 	//checking size less than 8mb
			if($_FILES["file"]["size"] < 8388608 ){
				
				$extension = urldecode(end(explode(".", $_FILES["file"]["name"])));
				if ($_FILES["file"]["error"] > 0) {
					$error = 1;
				}
				else {
					move_uploaded_file($_FILES["file"]["tmp_name"], $image_basedir . $name . '.' . $extension);
					
					Utils::saveOnS3($image_basedir . $name . '.' . $extension,"files/images/profiles/".$name.'.'.$extension);
					
					if($data['type']==1){
						$rs = $this->conn->Execute($this->conn->prepare("Select * from user_id_proof where user_id=?"),array($this->userId));
						if ($rs->RowCount () > 0) {
							$row = $rs->FetchRow ();
							
							$query="update user_id_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? ";
							$param_array=array($data['proofType'],$name.".".$extension,$row['number_of_trials']+1,$this->userId);
							$tablename='user_id_proof';
							$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
							 
							/*
							$this->conn->Execute($this->conn->prepare("update user_id_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
									array($data['proofType'],$name.".".$extension,$row['number_of_trials']+1,$this->userId));*/

							$query="insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?) ";
							$param_array=array($this->userId,$data['proofType'],$name.".".$extension,$row['number_of_trials']+1,'under_moderation');
							$tablename='user_id_proof_log';
							$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
								
							/*$this->conn->Execute($this->conn->prepare("insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?)"),
									array($this->userId,$data['proofType'],$name.".".$extension,$row['number_of_trials']+1,'under_moderation'));*/

						}else{
							$query="insert into user_id_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?) ";
							$param_array=array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation');
							$tablename='user_id_proof';
							$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
								
							/*$this->conn->Execute($this->conn->prepare("insert into user_id_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?)"),
									array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation'));*/

							$query="insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?)";
							$param_array=array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation');
							$tablename='user_id_proof_log';
							$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
								
							/*$this->conn->Execute($this->conn->prepare("insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?)"),
									array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation'));*/
						}
					}
// 					elseif ($data['type']==2){
// 						$rs = $this->conn->Execute($this->conn->prepare("Select * from user_address_proof where user_id=?"),array($this->userId));
// 						if ($rs->RowCount () > 0) {
// 							$row = $rs->FetchRow ();
// 							$this->conn->Execute($this->conn->prepare("update user_address_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
// 									array($data['proofType'],$name.".".$extension,$row['number_of_trials']+1,$this->userId));
// 						}else{
// 							$this->conn->Execute($this->conn->prepare("insert into user_address_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,landmark) values(Now(),?,?,?,?,?,'','','','','')"),
// 									array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation'));
// 						}
// 					}elseif($data['type']==3){
// 						//var_dump($data);exit;
// 						$data['company_name'] = trim($data['company_name']);
// 						$rs = $this->conn->Execute($this->conn->prepare("Select * from user_employment_proof where user_id=?"),array($this->userId));
// 						if ($rs->RowCount () > 0) {
// 							$row = $rs->FetchRow ();
// 							$this->conn->Execute($this->conn->prepare("update user_employment_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=?,company_name=?,show_name=?,emp_id=?,work_status=? where user_id=? "),
// 									array($data['proofType'],$name.".".$extension,$row['number_of_trials']+1,$data['company_name'],$data['show_company'],$data['emp_id'],$data['work_status'],$this->userId));

// 							$this->conn->Execute($this->conn->prepare("insert into user_employment_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,company_name,show_name,emp_id,work_status) values(Now(),?,?,?,?,?,?,?,?,?)"),
// 									array($this->userId,$data['proofType'],$name.".".$extension,$row['number_of_trials']+1,'under_moderation',$data['company_name'],$data['show_company'],$data['emp_id'],$data['work_status']));

// 						}else{
// 							/*$this->conn->Execute($this->conn->prepare("insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,name,type,tenure) values(Now(),?,?,?,?,?,'','','','','','','')"),
// 							 array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation'));*/
								
// 							$this->conn->Execute($this->conn->prepare("insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,company_name,show_name,emp_id,work_status) values(Now(),?,?,?,?,?,?,?,?,?)"),
// 									array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation',$data['company_name'],$data['show_company'],$data['emp_id'],$data['work_status']));

// 							$this->conn->Execute($this->conn->prepare("insert into user_employment_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,company_name,show_name,emp_id,work_status) values(Now(),?,?,?,?,?,?,?,?,?)"),
// 									array($this->userId,$data['proofType'],$name.".".$extension,'1','under_moderation',$data['company_name'],$data['show_company'],$data['emp_id'],$data['work_status']));
// 						}
// 					}
				}
			}else{
				$error = 2;
			}
		}catch(Exception $e) {
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			$done = 100;
		}
		$return = '';
		if($error==0){
			$return['responseCode'] = 200;
			$return['status']='success';
			$return['error'] = $tbMessages['id']['success'];
		}elseif($error==2){
			$return['responseCode'] = 403;
			$return['status']='fail';
			$return['error']='File Size can not be greater than 8 MB.';
		}else{
			$return['responseCode'] = 403;
			$return['status']='fail';
			$return['error']='Error in file could not be uploaded.';
		}
		print_r(json_encode($return));
		die;
	}

	// register a new user registering via facebook
	public function CreateUserForFacebook($data,$registered_from,$device_id) {
		//global $conn;

		//$func=new functionClass();
		//$company_name=$func->getCorporateDetails();

		if($data['gender']=='male')
			$data['gender']='M';
		else if($data['gender']=='female')
			$data['gender']='F';
		//$register_code = $func->random ( 10 );
		try {
			$query="insert into user (user_id,fname,lname,gender,email_id,password,fid,is_fb_connected,status,registered_from,device_id, registered_at)
							values('',?,?,?,?,'',?,'Y','incomplete',?,?, Now())";
			$param_array=array (
							$data ['first_name'],
							$data ['last_name'],
							$data ['gender'],
							$data ['email'],
							$data ['id'],$registered_from,$device_id
					);
			$tablename='user';
			$user_id=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
			/*$query="insert into user (user_id,fname,lname,gender,email_id,password,fid,is_fb_connected,status,registered_from,device_id, registered_at)
							values('',?,?,?,?,'',?,'Y','incomplete',?,?, Now())" ;
			$param_array=array (
							$data ['first_name'],
							$data ['last_name'],
							$data ['gender'],
							$data ['email'],
							$data ['id'],$registered_from,$device_id
					);
			$user_id=Query::INSERT($query, $param_array);*/
			
			/*$this->conn->Execute (
					$this->conn->prepare (
							"insert into user (user_id,fname,lname,gender,email_id,password,fid,is_fb_connected,status,
							registered_from,device_id, registered_at)
							values('',?,?,?,?,'',?,'Y','incomplete',?,?, Now())" ),
					array (
							$data ['first_name'],
							$data ['last_name'],
							$data ['gender'],
							$data ['email'],
							$data ['id'],$registered_from,$device_id
					) );*/
			//get $conn->last_insert_id();
			//$user_id = $this->conn->Insert_ID();
			
// 			$campaignDetails = $func->getCampaignDetails();
// 			if(isset($campaignDetails['utm_campaign']) || isset($campaignDetails['utm_medium']) || isset($campaignDetails['utm_source'])){
// 				$sql = $this->conn->Prepare("INSERT INTO campaign values(?,?,?,?, now())");
// 				$this->conn->Execute($sql, array($user_id, $campaignDetails['utm_source'],  $campaignDetails['utm_medium'],$campaignDetails['utm_campaign'] ))	;
// 			}
		} catch ( Exception $e ) {
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			//echo $e->getMessage ();
			//die ();
		}
		/*try {
			$rs=$this->conn->Execute($this->conn->prepare("select user_id from user where fid=?"),$data['id']);
			$result=$rs->GetRows();

		}
		catch(Exception $e) {
			echo $e->getMessage();
			die;
		}*/
		//$this->userId = $result[0]['user_id'];
		$this->userId = $user_id;
		return $this->userId;

	}

	public function saveAddressForm($data){
		try{
			
			$query="insert into user_address_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,landmark) values(Now(),?,'','',?,?,?,?,?,?,?) on duplicate key update upload_time=now(),status='under_moderation', address=?, city=?, state=?, pincode=?, landmark=?,number_of_trials = number_of_trials + 1 ;";
			$param_array=array($this->userId,'1','under_moderation',$data['address'],$data['city'],$data['state'],$data['pincode'],$data['landmark'],$data['address'],$data['city'],$data['state'],$data['pincode'],$data['landmark']);
			$tablename='user_address_proof';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
			/*$this->conn->Execute($this->conn->prepare("insert into user_address_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,landmark) values(Now(),?,'','',?,?,?,?,?,?,?) on duplicate key update upload_time=now(),status='under_moderation', address=?, city=?, state=?, pincode=?, landmark=?,number_of_trials = number_of_trials + 1 ;"),
					array($this->userId,'1','under_moderation',$data['address'],$data['city'],$data['state'],$data['pincode'],$data['landmark'],$data['address'],$data['city'],$data['state'],$data['pincode'],$data['landmark']));*/
		}catch(Exception $e){
			trigger_error($e->getMessage());
			return false;
		}
		return true;
	}

	public function saveEmploymentForm($data){
		try{
			$query="insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,tenure,name,type) values(Now(),?,'','',?,?,?,?,?,?,?,?,?) on duplicate key update upload_time=now(),status='under_moderation', address=?, city=?, state=?, pincode=?, tenure=?,name=?,type=?,number_of_trials = number_of_trials + 1 ;";
			$param_array=array($this->userId,'1','under_moderation',$data['address'],$data['city'],$data['state'],$data['pincode'],$data['tenure'],$data['name'],$data['code_type'],$data['address'],$data['city'],$data['state'],$data['pincode'],$data['tenure'],$data['name'],$data['code_type']);
			$tablename='user_employment_proof';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
			/*
			$this->conn->Execute($this->conn->prepare("insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,tenure,name,type) values(Now(),?,'','',?,?,?,?,?,?,?,?,?) on duplicate key update upload_time=now(),status='under_moderation', address=?, city=?, state=?, pincode=?, tenure=?,name=?,type=?,number_of_trials = number_of_trials + 1 ;"),
					array($this->userId,'1','under_moderation',$data['address'],$data['city'],$data['state'],$data['pincode'],$data['tenure'],$data['name'],$data['code_type'],$data['address'],$data['city'],$data['state'],$data['pincode'],$data['tenure'],$data['name'],$data['code_type']));*/
		}catch(Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			return false;
		}
		return true;
	}

	/**
	 *initiate phone verification for loggedin user
	 */
	public function phoneSubmit($phoneNumber){
		global $config;
		$return = NULL;
		try{
			$rs = $this->conn->Execute($this->conn->prepare("select user_id from user_phone_number where user_number=?  and status='active'"),$phoneNumber);
			if ($rs->RowCount () > 0) {
				$data = $rs->FetchRow ();
				if($data['user_id']!=$this->userId){
					$return['responseCode'] = 403;
					$return['status']= 'fail';
					$return['error'] = 'This phone number is in use by another TrulyMadly profile.';
					return $return;
				}
			}
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $config["phone"]["url"].$phoneNumber);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, false);
			$response = curl_exec($ch);
			curl_close($ch);
				
			$xml = new SimpleXMLElement($response);
				
			if(isset($xml->error))
				$error = $xml->error;
			else $error = "";
				
			if(isset($xml->callid))
				$callid = $xml->callid;
			else $callid = "";

			$query="insert into user_phone_log (user_id,call_time,user_number,status,call_id,request_send,error) VALUES (?,now(),?,?,?,?,?)";
			$param_array=array($this->userId,$phoneNumber,$xml->status,$callid,"http://int.kapps.in/webapi/trulymadly/api/mobileverification?callernumber=".$phoneNumber,$error);
			$tablename='user_phone_log';
			$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
			/*$this->conn->Execute($this->conn->Prepare("insert into user_phone_log (user_id,call_time,user_number,status,call_id,request_send,error) VALUES (?,now(),?,?,?,?,?)"),
					array($this->userId,$phoneNumber,$xml->status,$callid,"http://int.kapps.in/webapi/trulymadly/api/mobileverification?callernumber=".$phoneNumber,$error));*/
				
			if($xml->status != 'Failed'){
				$query="insert into user_phone_number (user_id,upload_time,user_number,call_id) VALUES (?,now(),?,?) on duplicate key update upload_time=now(),user_number = ?,call_id=?,number_of_trials = number_of_trials + 1,status = 'under_moderation'";
				$param_array=array($this->userId,$phoneNumber,$callid,$phoneNumber,$callid);
				$tablename='user_phone_number';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
					
				/*$this->conn->Execute($this->conn->prepare("insert into user_phone_number (user_id,upload_time,user_number,call_id) VALUES (?,now(),?,?) on duplicate key update upload_time=now(),user_number = ?,call_id=?,number_of_trials = number_of_trials + 1,status = 'under_moderation'"),
						array($this->userId,$phoneNumber,$callid,$phoneNumber,$callid));*/
				$return['responseCode'] = 200;
				$return['status'] = 'success';
			}else{
				$return['responseCode'] = 403;
				$return['status']= 'fail';
				$return['error'] = 'Your phone number could not be verified. Please try again.';
				return $return;
			}
		}catch (Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			return false;
		}
		return $return;
	}

	/**
	 *check facebook status and get connections of loggedin user.
	 */
	public function  checkFacebookStatus(){
		$fbAuthenication['status'] = 'no';
		$sql = $this->conn->Prepare("SELECT fid,connection,mismatch from user_facebook where user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		if ($rs->RowCount () > 0) {
			$data = $rs->FetchRow ();
			if(isset($data['fid']) && $data['mismatch']==null){
				$fbAuthenication['status'] = 'yes';
				$this->fbConnected = true;
				$fbAuthenication['connection'] = $data['connection'];
			}else{
				$fbAuthenication['mismatch'] = $data['mismatch'];
			}
		}
		//var_dump($fbAuthenication);die;
		return $fbAuthenication;
	}


	public function isAnyprofilePic(){
		$rs = $this->conn->Execute(" SELECT count(*) as profile_pic from user_photo where status = 'active' AND is_profile='yes' AND user_id = ?", array($this->userId));
		$data = $rs->FetchRow();
		//$uudbo= new userUtilsDBO();
		//$data = $uudbo->getProfilePic(array($this->userId));
		//if($data==NULL)
		return $data['profile_pic'];
	}

	/**
	 * return if fb verified
	 * @Himanshu
	 */
	public function isFbVerified(){
		$data['isVerify'] = false;
		$sql = $this->conn->Prepare("SELECT  fb_connections as connections from user_trust_score where user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		$row = $rs->FetchRow();
		if (isset($row['connections'])) {
			if($row['connections'] == -1) {
				$data['connection'] = "Some";
			}else {
				$data['connection'] = $row['connections'];
			}
			$data['isVerify'] = true;
			return $data;
		}
		return $data;
	}

	/**
	 * returns the difference between TM/fb name/age
	 * @Himanshu
	 */
	public function checkFbTMDiff(){
		$sql = $this->conn->Prepare("select u.fname as TMname, FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as TMage, ud.DateOfBirth as TM_dob, uf.fname as fbName, uf.lname as fbLName, FLOOR(DATEDIFF(now(),date_format(str_to_date(birthday, '%m/%d/%Y'), '%Y%m%d'))/365.25) as fbAge, date_format(str_to_date(birthday, '%m/%d/%Y'), '%Y-%m-%d') as fb_dob,u.gender as TMgender,uf.gender as fbGender
				from user_facebook uf left join user u on u.user_id = uf.user_id
				left join  user_data ud on uf.user_id = ud.user_id
				where uf.user_id = ?");
		$res = $this->conn->Execute($sql, array($this->userId));
		$row = $res->FetchRow();
		if($row['fbGender']=='male')
			$row['fbGender'] = 'M';
		else if($row['fbGender']=='female')
			$row['fbGender'] = 'F';

		$diff = array();
		$pos = stripos($row['fbName'], trim($row['TMname']," "));
		if ($pos === false) {
			$pos1 = stripos($row['fbLName'], trim($row['TMname']," "));
			if($pos1 === false){
				$diff['TMname'] = $row['TMname'];
				$diff['fbName'] = $row['fbName']." ".$row['fbLName'];
				$diff['name'] = true;
			}
		}
		if(isset($row['fbAge'])) {
			if(abs($row['TMage']-$row['fbAge'])>1){
				$diff['TMage'] = $row['TMage'];
				$diff['fbAge'] = $row['fbAge'];
				$diff['TM_dob'] = $row['TM_dob'];
				$diff['fb_dob'] = $row['fb_dob'];
				$diff['age'] = true;
			}
		}
		
		if($row['TMgender']!=$row['fbGender']){
			$diff['TMgender'] = $row['TMgender'];
			$diff['fbGender'] = $row['fbGender'];
			$diff['gender'] = true;
		}
			
		return $diff;
	}

	/**
	 * override the facebook name and age on to TM system
	 * @Himanshu
	 */
	public function overrideTMFromFb($data){
		$diff = $this->checkFbTMDiff();
		if($diff['name']){
			if($data['first_name']!='' || $data['last_name']!=''){
				
				
				$query="UPDATE user set fname =? , lname =? where user_id = ?";
				$param_array=array($data['first_name'],$data['last_name'], $this->userId);
				$tablename='user';
				$ex=Query_Wrapper::UPDATE($query, $param_array,$tablename,"master",$this->userId);
				//$sql = $this->conn->Prepare("UPDATE user set fname =? , lname =? where user_id = ?");
				//$ex = $this->conn->Execute($sql, array($data['first_name'],$data['last_name'], $this->userId));
			}
		}
		if($diff['age']){
			if($data['birthday']){
				$birthday=explode("/",$data['birthday']);
				if(count($birthday)==3){
					$birthday =  $birthday[2]."-".$birthday[0]."-".$birthday[1];
					
					$query="UPDATE user_data set DateOfBirth=? where user_id = ?";
					$param_array=array($birthday, $this->userId);
					$tablename='user_data';
					$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
					
					/*($sql = $this->conn->Prepare("UPDATE user_data set DateOfBirth=? where user_id = ?");
					$res = $this->conn->Execute($sql, array($birthday, $this->userId));*/
					
					$ud = new UserData($this->userId);
					$gender = $ud->fetchGender();
					$age = date("Y")-$birthday;
					$prefAge = Utils::preferenceAge($age,$gender);
					
					$query="UPDATE user_preference_data set start_age=?, end_age=? where user_id = ?";
					$param_array=array($prefAge['start'], $prefAge['end'], $this->userId);
					$tablename='user_preference_data';
					$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
					/*	
					$sql = $this->conn->Prepare("UPDATE user_preference_data set start_age=?, end_age=? where user_id = ?");
					$res = $this->conn->Execute($sql, array($prefAge['start'], $prefAge['end'], $this->userId));*/
				}
			}
		}
		//trigger_error("PHP Web:".$this->userId." update user data with following data ".json_encode($diff), E_USER_WARNING);
		$this->logOverrideAction($diff, 'reset From Fb');
		
		//trigger_error("PHP Web:".$this->userId." log into the user_fb_tm_mismatch_log ".json_encode($diff), E_USER_WARNING);
	}

	public function logOverrideAction($diff, $reason){
		$query="INSERT INTO user_fb_tm_mismatch_log values (?,?,?,?,?,?,now())";
		$param_array=array($this->userId, $diff['TMname'], $diff['fbName'],$diff['TM_dob'], $diff['fb_dob'],$reason);
		$tablename='user_fb_tm_mismatch_log';
		$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
			
		/*$sql2 = $this->conn->Prepare("INSERT INTO user_fb_tm_mismatch_log values (?,?,?,?,?,?,now())");
		$ex2 = $this->conn->Execute($sql2, array($this->userId, $diff['TMname'], $diff['fbName'],$diff['TM_dob'], $diff['fb_dob'],$reason));*/
	}
	/**
	 *check
	 */
	public function  checkLinkedInStatus(){
		$linkedInConnect['status'] = 'no';
		//$sql = $this->conn->Prepare("SELECT member_id,num_connections from user_linkedin where user_id=?");
		$sql = $this->conn->Prepare("SELECT linkedin_connections,linkedin_designation from user_trust_score where user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		if ($rs->RowCount () > 0) {
			$data = $rs->FetchRow ();
			if(isset($data['linkedin_connections'])){
				$linkedInConnect['status'] = 'yes';
				$linkedInConnect['connection'] = $data['linkedin_connections'];
				$linkedInConnect['position'] = $data['linkedin_designation'];
			}
		}
		return $linkedInConnect;
	}

	public function checkPhoneStatus(){
		global $tbMessages;
		$sql = $this->conn->Prepare("SELECT status,user_number,number_of_trials from user_phone_number where user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		if ($rs->RowCount () > 0) {
			$data = $rs->FetchRow ();
			if(isset($data['status'])){
				$data['responseCode'] = 200;
				if($data['status']=='rejected' && $data['number_of_trials']<3){
					$data['responseCode'] = 403;
					$data['error']= $tbMessages['phone']['rejected'];
				}else if($data['status']=='rejected' && $data['number_of_trials']>=3){
					$data['responseCode'] = 403;
					$data['error']= $tbMessages['phone']['rejectedgt3'];
				}else if($data['status']=='notpick'){
					$data['responseCode'] = 403;
					$data['error']= $tbMessages['phone']['notpick'];
				}else if($data['status'] == 'under_moderation'){
					$data['responseCode'] = 200;
				}
				return $data;
			}
		}
		return false;
	}


	public function checkIdProofStatus(){
		//$sql = $this->conn->Prepare("SELECT status,proof_type,reject_reason from user_id_proof where user_id=?");
		$sql = $this->conn->Prepare("SELECT id.status,proof_type,reject_reason,CONCAT(id.fname,' ',id.lname) as IDname,CONCAT(u.fname,' ',u.lname) as TMname,FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as TMage,FLOOR(DATEDIFF(now(),id.dateofbirth)/365.25) as IDage
				from user_id_proof id LEFT JOIN user u on id.user_id=u.user_id
				LEFT JOIN user_data ud on id.user_id=ud.user_id where id.user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		if ($rs->RowCount () > 0) {
			$data = $rs->FetchRow ();
			if(isset($data['status'])){
				return $data;
			}
		}
		return false;
	}

	public function checkAddressProofStatus(){
		$sql = $this->conn->Prepare("SELECT status,proof_type from user_address_proof where user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		if ($rs->RowCount () > 0) {
			$data = $rs->FetchRow ();
			if(isset($data['status'])){
				return $data;
			}
		}
		return false;
	}

	public function checkEmploymentProofStatus(){
		$sql = $this->conn->Prepare("SELECT status,proof_type,reject_reason,company_name,show_name from user_employment_proof where user_id=?");
		$rs = $this->conn->Execute ( $sql, array (
				$this->userId
		) );
		if ($rs->RowCount () > 0) {
			$data = $rs->FetchRow ();
			if(isset($data['status'])){
				return $data;
			}
		}
		return false;
	}

	public function setUserId($userId) {
		$this->userId = $userId;
	}

	public function getSystemAlert($credits,$plan = 'credit'){
		$systemAlert = SystemMessages::getSystemAlertMessages();
		if($this->trustScore < 25){
			return $systemAlert[0];
		}elseif($credits>0 && $this->trustScore < 50){
			return $systemAlert[3];
		}elseif($credits == 0 && $this->fbConnected && $plan != 'unlimited'){
			return $systemAlert[2];
		}elseif($credits == 0 && $plan != 'unlimited'){
			return $systemAlert[1];
		}else{
			return false;
		}
	}

	public function getUserRegisterationDate(){
		$sql = $this->conn->Prepare("SELECT registered_at from user where user_id = ?");
		$ex = $this->conn->Execute($sql, array($this->userId));
		$rss = $ex->FetchRow();
		return $rss['registered_at'];

	}
	
	public function syncNamewithID($data){
		$rs = $this->conn->Execute($this->conn->prepare("Select * from user_id_proof where user_id=?"),array($this->userId));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow ();
			if($data['update']=='age_mismatch'){
				
				$query="update user_data SET DateOfBirth=? where user_id=?";
				$param_array=array($row['dateofbirth'],$this->userId);
				$tablename='user_data';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("update user_data SET DateOfBirth=? where user_id=? "),
						array($row['dateofbirth'],$this->userId));*/
			}else if($data['update']=='name_mismatch'){
				
				$query="update user SET fname=?,lname=? where user_id=?";
				$param_array=array($row['fname'],$row['lname'],$this->userId);
				$tablename='user';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
					
				
				/*$this->conn->Execute($this->conn->prepare("update user SET fname=?,lname=? where user_id=? "),
						array($row['fname'],$row['lname'],$this->userId));*/
			}else if($data['update']=='name_age_mismatch'){
				$query="update user_data SET DateOfBirth=? where user_id=?";
				$param_array=array($row['dateofbirth'],$this->userId);
				$tablename='user_data';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("update user_data SET DateOfBirth=? where user_id=? "),
						array($row['dateofbirth'],$this->userId));*/
				$query="update user SET fname=?,lname=? where user_id=?";
				$param_array=array($row['fname'],$row['lname'],$this->userId);
				$tablename='user';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("update user SET fname=?,lname=? where user_id=? "),
						array($row['fname'],$row['lname'],$this->userId));*/
			}
				
			if($data['update']=='age_mismatch' || $data['update']=='name_mismatch' || $data['update']=='name_age_mismatch'){
				
				$query="update user_id_proof SET status='active',reject_reason='' where user_id=?";
				$param_array=array($this->userId);
				$tablename='user_id_proof';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("update user_id_proof SET status='active',reject_reason='' where user_id=? "),
						array($this->userId));*/
				
				$query="insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,reject_reason,password,name_age_mismatch_allow) values(Now(),?,?,?,?,?,?,?,?)";
				$param_array=array($this->userId,$row['proof_type'],$row['img_location'],$row['number_of_trials'],'active',$row['reject_reason'],'','user done the changes');
				$tablename='user_id_proof_log';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,reject_reason,password,name_age_mismatch_allow) values(Now(),?,?,?,?,?,?,?,?)"),
						array($this->userId,$row['proof_type'],$row['img_location'],$row['number_of_trials'],'active',$row['reject_reason'],'','user done the changes'));*/
		
				$this->updateIdTrustScore($row['proof_type']);
				$this->authenticateUser();
				/*
				 if($trustBuilder->authenticateUser()) {
				functionClass::setSessionStatus('authentic',true);
				$session['status']='authentic';
				}else {
				functionClass::setSessionStatus('non-authentic',true);
				$session['status']='non-authentic';
				}*/
			}
			$return['responseCode'] = 200;
			$return['status']='success';
		}else{
			$return['responseCode'] = 403;
			$return['status'] = 'fail';
		}
		return $return;
	}
	
	public function sendPassword($data){
		if($data['type']==1){
			$rs = $this->conn->Execute($this->conn->prepare("Select * from user_id_proof where user_id=?"),array($this->userId));
			if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
				
				$query="insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,reject_reason,password,name_age_mismatch_allow) values(Now(),?,?,?,?,?,?,?,?)";
				$param_array=array($this->userId,$row['proof_type'],$row['img_location'],$row['number_of_trials'],'under_moderation',$row['reject_reason'],$data['password'],'');
				$tablename='user_id_proof_log';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,reject_reason,password,name_age_mismatch_allow) values(Now(),?,?,?,?,?,?,?,?)"),
						array($this->userId,$row['proof_type'],$row['img_location'],$row['number_of_trials'],'under_moderation',$row['reject_reason'],$data['password'],''));*/
		
				$query="update user_id_proof SET status='under_moderation',reject_reason='' where user_id=? ";
				$param_array=array($this->userId);
				$tablename='user_id_proof';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				
				
				/*$this->conn->Execute($this->conn->prepare("update user_id_proof SET status='under_moderation',reject_reason='' where user_id=? "),
						array($this->userId));*/

				$return['responseCode'] = 200;
				$return['status']='success';
			}else{
				$return['responseCode'] = 403;
				$return['status'] = 'fail';
			}
		}elseif($data['type']==3){
			$rs = $this->conn->Execute($this->conn->prepare("Select * from user_employment_proof where user_id=?"),array($this->userId));
			if ($rs->RowCount () > 0) {
				$row = $rs->FetchRow ();
				
				$query="insert into user_employment_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,reject_reason,password,company_name,show_name,emp_id,work_status) values(Now(),?,?,?,?,?,?,?,?,?,?,?)";
				$param_array=array($this->userId,$row['proof_type'],$row['img_location'],$row['number_of_trials'],'under_moderation',$row['reject_reason'],$data['password'],$row['company_name'],$row['show_name'],$row['emp_id'],$row['work_status']);
				$tablename='user_employment_proof_log';
				$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("insert into user_employment_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,reject_reason,password,company_name,show_name,emp_id,work_status) values(Now(),?,?,?,?,?,?,?,?,?,?,?)"),
						array($this->userId,$row['proof_type'],$row['img_location'],$row['number_of_trials'],'under_moderation',$row['reject_reason'],$data['password'],$row['company_name'],$row['show_name'],$row['emp_id'],$row['work_status']));*/
		
				$query="update user_employment_proof SET status='under_moderation',reject_reason='' where user_id=?";
				$param_array=array($this->userId);
				$tablename='user_employment_proof';
				$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$this->userId);
				
				/*$this->conn->Execute($this->conn->prepare("update user_employment_proof SET status='under_moderation',reject_reason='' where user_id=? "),
						array($this->userId));*/
		
				$return['responseCode'] = 200;
				$return['status']='success';
			}else{
				$return['responseCode'] = 403;
				$return['status'] = 'fail';
			}
		}
		return $return;
	}
	

	/**
	 * update trust score for endorsement
	 * @Himanshu
	 */
	public function updateEndorsementScore(){
		$this->userUtilsDBO->updateTrustScoreForEndorsements($this->userId, EndorsementConstants::TrustScoreForEachEndorsement, EndorsementConstants::MinimumEndorsementCount, EndorsementConstants::NetScore); 
	}
	
	public function getEndorsementData(){
		return $this->userUtilsDBO->getEndorsementData(array($this->userId));
	}
	
	/****
	 * rajesh for photo skip part
	 */
	public function allowSkipPhoto() {
// 		$ud = new UserData($this->userId);
// 		$gender = $ud->fetchGender();
// 		$status = $ud->fetchStatus();
// 		$authenticityConstant = ($gender == "M")?TrustAuthenticConstants::TrustAuthenticConstantMale:TrustAuthenticConstants::TrustAuthenticConstantFemale;
		$authenticityConstant = $this->getThreshold();
		
		$rs = $this->conn->Execute($this->conn->prepare("SELECT trust_score from user_trust_score where user_id=?"),array($this->userId));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow();
			if($row['trust_score']>=$authenticityConstant) {
				return false;
			}else {
				$rs = $this->conn->Execute($this->conn->prepare("SELECT status from user_id_proof where user_id=?"),array($this->userId));
				if($rs->RowCount() > 0) {
					return false;
				}else {
					return true;
				}
			}
		}else {
			$rs = $this->conn->Execute($this->conn->prepare("SELECT status from user_id_proof where user_id=?"),array($this->userId));
			if($rs->RowCount() > 0) {
				return false;
			}else {
				return true;
			}
		}
	}
	/*
	 * for A/B testing done by rajesh
	 */
	public function getThreshold() {
		$ud = new UserData($this->userId);
		$gender = $ud->fetchGender();
		$status = $ud->fetchStatus();
		/*
		 * ABTesting change the authenticity constant for female non-authentic users
		*/
		if(TrustAuthenticConstants::ABTestVerification && $gender == 'F' ) {
				$authenticityConstant = $this->getAuthenticityConstForABTest($status);
		}else {
			$authenticityConstant = ($gender == "M")?TrustAuthenticConstants::TrustAuthenticConstantMale:TrustAuthenticConstants::TrustAuthenticConstantFemale;
		}
		return $authenticityConstant;
	}
	
	public function getAuthenticityConstForABTest($status) {
		$rs = $this->conn->Execute($this->conn->prepare("SELECT * from ABTest where user_id=?"),array($this->userId));
		if($rs->RowCount() > 0) {
			$row = $rs->FetchRow ();
			if(isset($row['TB_VERIFY_THRESHOLD'])) {
				return intval($row['TB_VERIFY_THRESHOLD']);
			}else {
				return $this->setAuthenticityConstForABTest();
			}
		}else {
			if($status == 'authentic') {
				$rs = $this->conn->Execute($this->conn->prepare("SELECT trust_score from user_trust_score where user_id=?"),array($this->userId));
				if ($rs->RowCount () > 0) {
					$row = $rs->FetchRow();
					if($row['trust_score']>=30) {
						$authenticityConstant = TrustAuthenticConstants::TrustAuthenticConstantFemale;
					}else {
						$authenticityConstant = 10;
					}
				}
				return $authenticityConstant;
			}
			return $this->setAuthenticityConstForABTest();
		}
	}
	
	public function setAuthenticityConstForABTest() {
		$authenticity = 30;
		$number = rand();			
		if($number%2 == 0) {
			$authenticity = 10;
		}	
		$query="insert into ABTest() values(?,?,now())";
		$param_array=array ($this->userId,$authenticity);
		$tablename='ABTest()';
		$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$this->userId);
		
		/*$this->conn->Execute ($this->conn->prepare( "insert into ABTest() values(?,?,now())" ), array ($this->userId,$authenticity) );*/
		return $authenticity;
	}
	
	/*
	 * for A/B testing done by rajesh
	*/
    
	public function generateOTP($phn) {
	$return = NULL;
	try {
		$otp=new OTP($this->userId);
		$return=$otp->phone_submit($phn);
	} 
	catch (Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			//return false;
		}
	return $return;	
	}
	public function verifyOTP($phn,$otp) {
		$return = NULL;
		try {
			$otp_obj=new OTP($this->userId);
			$return=$otp_obj->verify_otp($phn, $otp);
		}
		catch (Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			//return false;
		}
		return $return;
	}

	public function digitsVerify($server_url,$Oauth_string,$phone_number){
		global $config;
		$return=NULL;
		try{
			$rs = $this->conn->Execute($this->conn->prepare("select user_id from user_phone_number where user_number=?  and status='active'"),$phone_number);
			if ($rs->RowCount () > 0) {
				$data = $rs->FetchRow ();

				if($data['user_id']!=$this->userId){
					$return['responseCode'] = 403;
					$return['status']= 'fail';
					$return['error_code']=0;
					$return['error'] = 'This phone number is in use by another TrulyMadly profile.';

					return $return;
				}
				else{
					$return['responseCode'] = 403;
					$return['status']= 'fail';
					$return['error_code']=1;
					$return['error'] = 'This phone number is already verified by you.';
					return $return;
				}
			}

			//check some validation if the url we are calling is of digts , if not then return some other response
			if(parse_url($server_url, PHP_URL_HOST)!=NULL){
				$host=parse_url($server_url, PHP_URL_HOST);
				if($host!='api.digits.com'){
					$return['responseCode'] = 403;
					$return['status']= 'fail';
					$return['error_code']=2;
					$return['error'] = 'Your phone number could not be verified. Please try again.';
					return $return;
				}
			}

			$oauth_key=$config['phone']['digit_key'];
			$oauth_array=$this->parseOAuthString($Oauth_string);
			//var_dump($oauth_array['oauth_consumer_key']);die;
			//var_dump($Oauth_string);die;
			if($oauth_array!==false&&is_array($oauth_array)){
				if($oauth_array['oauth_consumer_key']!='"'.$oauth_key.'"'){
					$return['responseCode'] = 403;
					$return['status']= 'fail';
					$return['error_code']=3;
					$return['error'] = 'Your phone number could not be verified. Please try again.';
					return $return;
				}
			}

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server_url);
			$authorization_header='Authorization:'.$Oauth_string;


			//echo 'helllo';
			//echo $authorization_header;die;
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				"Authorization:".$Oauth_string
			));
			//echo($Oauth_string);die;
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			$log_array=array("user_id"=>$this->userId,"time_taken"=>0,"activity"=>"trustbuilder","event_type"=>"phone_verify_digits","event_info"=>$response,"event_status"=>'success');
			$this->event_logging->logActions(array($log_array));
			if($response!=null){
				$arr=json_decode($response,true);
				//var_dump($arr);die;
				if($arr!=NULL){
					//
					if(isset($arr['errors'])){
						//Log the error and return to the user error

						$return['responseCode'] = 403;
						$return['status']= 'fail';
						$return['error_code']=4;
						$return['error'] = 'Your phone number could not be verified. Please try again.';
						return $return;
					}
					//echo 'hello';
					//echo $oauth_array['oauth_token'];
					//echo($arr['access_token']['token']);
					//echo $arr['oauth_token']['token'];die;
					if(isset($arr['phone_number'])&&$arr['phone_number']==$phone_number){
						if($oauth_array['oauth_token']=='"'.$arr['access_token']['token'].'"'){
							//This is the success after all the checks performed
							$rs2 = $this->conn->Execute($this->conn->prepare("insert into user_phone_number(user_id, upload_time, user_number, status) values(?,now(),?,?)
					on duplicate key update upload_time=now(),user_number=?,status=?"),array($this->userId ,$phone_number,'active',$phone_number,'active'));

							$trustbuilder = new TrustBuilder($this->userId);
							$trustbuilder->updateMobileTrustScore($phone_number);
							$trustbuilder->authenticateUser();
							$return['responseCode'] = 200;
							$return['status']= 'active';
							$return['user_number']= substr_replace($phone_number,"xxxxxxx",3,strlen($phone_number)-5);
							return $return;

						}else {
							$return['responseCode'] = 403;
							$return['status']= 'fail';
							$return['error_code']=5;
							$return['error'] = 'Your phone number could not be verified. Please try again.';
							return $return;
						}

					}else{
						$return['responseCode'] = 403;
						$return['status']= 'fail';
						$return['error_code']=6;
						$return['error'] = 'Your phone number could not be verified. Please try again.';
						return $return;
					}

				}
			}

			curl_close($ch);




		}catch (Exception $ex){

			trigger_error($ex->getMessage(),E_USER_WARNING);
			//trigger_error("PHP Web:".$ex->getTraceAsString(), E_USER_WARNING);
			return false;
		}

	}

	public function parseOAuthString($str){
		//USed for Digits in IOS
		//Returns an array containing the fields of the Comma seperated values of the string sent by client
		$fields=explode(',',$str);
		$res_array=array();
		if(count($fields)>0){
			foreach($fields as $field){
				$key=explode('=',$field)[0];
				$value=explode('=',$field)[1];
				$res_array[$key]=$value;
			}
			return $res_array;
		}


		return false;
	}

}


?>
