<?php 
require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/include/header.php";


class PyschoRegistration extends TMObject {

	public function __construct(){
		parent::__construct ();
	}
	
	public function unRegisterCookie() {
		setcookie("psycho1_pageInd","",time()-3600);
		unset($_COOKIE["psycho1_pageInd"]);
		setcookie("psycho2_pageInd","",time()-3600);
		unset($_COOKIE["psycho2_pageInd"]);
		setcookie("physcoIntro","",time()-3600);
		unset($_COOKIE["physcoIntro"]);
		foreach ( $_COOKIE ["pa"] as $id => $value ) {
			foreach($value as $k=>$val){
				setcookie ( "pa%5B$id%5D%5B$k%5D", "", time () - 3600 );
				unset ( $_COOKIE ["pa%5B$id%5D%5B$k%5D"] );
			}
		}
	}
	
	public function getRegistrationPage($step,$isMobileLogin=false,$rd,$stepsCompleted,$user_id,$reg=false) {
		switch($step){
			case "psycho1":
				$question_type="values";
				break;
			case "psycho2":
				$question_type="interpersonal";
				break;
		}
		$header = new header($user_id);
		$header_values = $header->getHeaderValues();
		
		$sql = $this->conn->Prepare("select gender from user where user_id=?");
		$rs = $this->conn->Execute($sql,array($user_id));
		$rs = $rs->FetchRow();
		$gender = $rs['gender'];
		
		//$sql = $this->conn->Prepare("select * from psycho_question where question_type=?");
		$sql = $this->conn->Prepare("select * from psycho_question_new1 where status = 'active' and question_type=? and gender=? order by id");
		$rs = $this->conn->Execute($sql,array($question_type,$gender));
		$rs = $rs->getRows();
		//var_dump($rs);exit;
		$div_ids = array();
		$final_output = "";
		
		if(!$isMobileLogin){
			
			foreach($rs as $value){
				$id = $value['id'];
				$div_id = $value['id'];
				$question = $value['question'];
				$question_type = $value['question_type'];
				$answers = json_decode($value['answer'],true);
				$div_ids[] = $div_id;
					
				$this->smarty->assign ( 'question_id', $id );
				$this->smarty->assign ( 'psycho_answers', $answers );
				$this->smarty->assign ( 'psycho_question', $question );
				$this->smarty->assign ( 'div_id', $div_id );
					
				if ($question_type == "values") {
					$final_output .= $this->smarty->fetch( "templates/psycho/psycho_values.tpl" );
				} else if ($question_type == "interpersonal") {
					$final_output .= $this->smarty->fetch ( "templates/psycho/psycho_interpersonal.tpl" );
				}
			}
			$this->smarty->assign('uid',$user_id);
			$this->smarty->assign('header',$header_values);
			$this->smarty->assign ( "output", $final_output );
			$this->smarty->assign ( "psycho_step", $step );
			$this->smarty->assign ( "rdUrl", $rd );
			$this->smarty->assign('stepCompleted',$stepsCompleted);
			$this->smarty->assign("isRegistration",'1');
			if($reg)
				$this->smarty->assign("fromRegister",'1');
			$this->smarty->display( "templates/psycho/psycho.tpl" );
		}else if($isMobileLogin){
			$response['responseCode'] = 200;
			$response['page'] = $step;
			$response['psycho_question'] = $rs;//json_encode($rs);
			//print_r(json_encode(array("psycho_step"=>$step)));
			print_r(json_encode($response));
			die;
		}
		
	}
	
	
	public function saveRegistrationPage($user_id,$values,$step) {
		
		$sql = $this->conn->Prepare("select gender from user where user_id=?");
                $rs = $this->conn->Execute($sql,array($user_id));
                $rs = $rs->FetchRow();
                $gender = $rs['gender'];

		switch($step){
			case "psycho1":
				$step="values";
				break;
			case "psycho2":
				$step="interpersonal";
				break;
		}
		
		$val_A = 0;$val_B=0;$val_C=0;$val_D=0;$val_E=0;$val_F=0;
		
		//$data = $_COOKIE;
		//$values = $data['pa'];
		
		if($step == 'values'){
			foreach($values as $key=>$data){
				foreach($data as $k=>$val){
					if($k=="A"){
						$val_A += $val;
					}
					else if($k=="B"){
						$val_B += $val;
					}
					else if($k=="C"){
						$val_C += $val;
					}
					else if($k=="D"){
						$val_D += $val;
					}
					else if($k=="E"){
						$val_E += $val;
					}
					else if($k=="F"){
						$val_F += $val;
					}
				}
			}
		}
		
		//$sql = $this->conn->Prepare ( "select id,answer from psycho_question where question_type=?" );
                $sql = $this->conn->Prepare("select id,answer from psycho_question_new1 where status = 'active' and question_type=? and gender=?");
		$rs = $this->conn->Execute ( $sql, array ($step,$gender) );
		$rs = $rs->getRows ();
		//var_dump($rs[1]['answer']);exit;
		$answer = array();
		
		if($step == 'values'){
			
			foreach ( $rs as $value ) {
				$value['answer'] = json_decode ( $value ['answer'], true );
				for($i=1;$i<=2;$i++){
					$ans[$value['answer'][$i]['bucket']] = $values[$value['id']][$value['answer'][$i]['bucket']];
				}
				$answer[$value['id']] = json_encode($ans);
				unset($ans);
			}
			//var_dump($answer);exit;
			
			$this->conn->Execute ( $this->conn->prepare ( "insert into user_psycho_answer(user_id,values_answer,sum_value_A,sum_value_B,sum_value_C,sum_value_D,sum_value_E,sum_value_F,time_of_filling)
				values(?,?,?,?,?,?,?,?,Now()) on DUPLICATE KEY UPDATE values_answer=?,sum_value_A=?,sum_value_B=?,sum_value_C=?,sum_value_D=?,sum_value_E=?,sum_value_F=?,time_of_filling=NOW()" ), array (
						$user_id,
						json_encode($answer),
						$val_A,$val_B,$val_C,$val_D,$val_E,$val_F,json_encode($answer),
						$val_A,$val_B,$val_C,$val_D,$val_E,$val_F
				) );
			
			$sql = $this->conn->prepare("select steps_completed from user where user_id=?");
			$exe = $this->conn->Execute($sql,array($user_id));
			$data = $exe->FetchRow();
			$steps = explode ( ",", $data['steps_completed']);
			
			if(!in_array('psycho1',$steps)){
				$steps[] = 'psycho1';
				$steps_completed = implode(',',$steps);
				
				$this->conn->Execute($this->conn->prepare("update user set steps_completed=? where user_id=?"),array($steps_completed,$user_id));
			}
		}else if($step == 'interpersonal'){
			
			$sumall=0;
			
			foreach ( $rs as $value ) {
				$sum = 0;
				$value['answer'] = json_decode ( $value ['answer'], true );
				for($i=1;$i<=3;$i++){
					$ans[$value['answer'][$i]['bucket']] = $values[$value['id']][$value['answer'][$i]['bucket']];
					$sum += pow($value['answer'][$i]['score']-$values[$value['id']][$value['answer'][$i]['bucket']],2);
				}
				//$sum = round($sum/3);
				$sum = $sum/3;
				$sumall += $sum;
				$answer[$value['id']] = json_encode($ans);
				unset($ans);
			}
			
			$this->conn->Execute ( $this->conn->prepare ( "insert into user_psycho_answer(user_id,interpersonal_answer,interpersonal_score,time_of_filling)
					values(?,?,?,Now()) on DUPLICATE KEY UPDATE interpersonal_answer=?,interpersonal_score=?,time_of_filling=NOW()" ), array (
							$user_id,
							json_encode($answer),
							$sumall,json_encode($answer),
							$sumall
					) );
			
			
			$sql = $this->conn->prepare("select steps_completed from user where user_id=?");
			$exe = $this->conn->Execute($sql,array($user_id));
			$data = $exe->FetchRow();
			$steps = explode ( ",", $data['steps_completed']);
			
			if(!in_array('psycho2',$steps)){
				$steps[] = 'psycho2';
				$steps_completed = implode(',',$steps);
			
				$this->conn->Execute($this->conn->prepare("update user set steps_completed=? where user_id=?"),array($steps_completed,$user_id));
			}
		}
			
	}
}
?>
