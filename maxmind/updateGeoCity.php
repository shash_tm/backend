<?php

try {
    require_once dirname(__FILE__) . "/../include/config.php";
    require_once dirname(__FILE__) . "/../include/Utils.php";

    $command = 'bash ' . dirname(__FILE__) . '/updateGeoCity ' . $maxMindDir;
    $command = addslashes($command);
    $out = shell_exec($command);

    $file_attrib = array();
    if (file_exists($maxMindDB)) {
        $file_attrib['Last_modified_time'] =  date('Y-m-d H:i:s', filemtime($maxMindDB));
        $file_attrib['size'] = round(filesize($maxMindDB)/1048576) . " MB";
        $file_attrib['path'] = $maxMindDB;
        $subject = "Max Mind DB file updated ";
    } else {
        $file_attrib['error'] = "File not found";
        $subject ="Max Mind DB file not found ";
    }

    $subject .= gethostname();
    Utils::sendEmail('backend@trulymadly.com', 'admintm@trulymadly.com', $subject, json_encode($file_attrib));
}
catch (Exception $e)
{
    trigger_error($e->getMessage(),E_USER_WARNING);
    Utils::sendEmail('backend@trulymadly.com', 'sumit@trulymadly.com', 'Max Mind DB update failed', $e->getMessage());

}
?>