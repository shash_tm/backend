<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/include/tbconfig.php";


global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute("select user_id,connection from user_facebook where birthday is NULL");
	$data = $sql->GetRows();
	
	foreach ($data as $key=>$value){
		updateFacebookTrustScore($data[$key]['user_id'],$data[$key]['connection']);
	}
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}


function updateFacebookTrustScore($userId,$connections){
	global $tbNumbers,$conn;
	if(!isset($connections)) {
		$connections = -1;
	}
	$rs = $conn->Execute($conn->prepare("Select * from user_trust_score where user_id=?"),array($userId));
	if ($rs->RowCount () > 0) {
		$row = $rs->FetchRow ();
		if($row['fb_connections']!='' || $row['fb_connections']!=NULL)
			return;//exit;
		$updated_trustScore = $row['trust_score']+$tbNumbers['score']['facebook'];
		if($updated_trustScore>100) {
			$updated_trustScore = 100;
		}

		$conn->Execute($conn->prepare("update user_trust_score SET fb_connections=?,fb_credits = ?,trust_score=? where user_id=? "),
				array($connections,$tbNumbers['credits']['facebook'],$updated_trustScore,$userId));
	}else{
		$conn->Execute($conn->prepare("insert into user_trust_score (user_id,fb_connections,fb_credits,trust_score) values(?,?,?,?)"),
				array($userId,$connections,$tbNumbers['credits']['facebook'],$tbNumbers['score']['facebook']));
	}
		
	var_dump($userId);
	var_dump($connections);
}


?>