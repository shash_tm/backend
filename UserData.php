<?php

require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";

class UserData{

	private $user_id;
	private $user_table_data=null;
	private $user_utils_dbo;

	public function __construct($user_id) {
		$this->user_id = $user_id;
		//$this->conn=$conn;
		$this->user_utils_dbo = new userUtilsDBO();
	}

	private function checkUserTableData(){
		if(isset($this->user_table_data)==false){
			//make call to the db to fetch the data
			$this->user_table_data=$this->user_utils_dbo->getUserBasicInfo(array($this->user_id) );
			//var_dump($this->user_table_data);
		}
	}
	
	public function fetchSystemFlagsOfUser()
	{
		return $this->user_utils_dbo->getUserFlags(array($this->user_id), true);
	}

	public function getUserID(){
		return $this->user_id;
	}
    
	public function CheckNoPicForFemale()
	{
		//Will fetch from user_search if the user has profile pic or not
		
		return $this->user_utils_dbo->CheckIfFemaleWithNoPics($this->user_id);
		
	}
	public function fetchGender(){
		$this->checkUserTableData();
		return $this->user_table_data['gender'];
	}

	public function fetchName(){
		$this->checkUserTableData();
		return htmlentities($this->user_table_data['fname']);
	}
	
	public function fetchEmailId(){
		$this->checkUserTableData();
		return $this->user_table_data['email_id'];
	}

	public function fetchEmailStatus(){
		$this->checkUserTableData();
		return $this->user_table_data['email_status'];
	}


	public function fetchStatus(){
		$this->checkUserTableData();
		return $this->user_table_data['status'];
	}
	
	public function fetchFirstTileShown(){
		$this->checkUserTableData();
		return $this->user_table_data['is_first_tile_shown'];
	}
	
	public function setOldUserMessageDiv(){
		$this->user_utils_dbo->setOldUserMessageDiv($this->user_id);
	}
	

	public function setFirstMayBe(){
		$this->user_utils_dbo->setMaybeFirstTile($this->user_id);
	}
	
	public function getFirstMayBe(){
		$this->checkUserTableData();
		return $this->user_table_data['is_first_maybe_shown'];
	}

	public function setFirstTileShown(){
		$this->user_utils_dbo->setFirstTileShownForAuthenticUser($this->user_id);
	}
	

	public function fetchRegisterationDate(){
		$this->checkUserTableData();
		return $this->user_table_data['registered_at'];
	}

	public function fetchNumberOfDaysSpentByUserOnTheApp(){
		$this->checkUserTableData();
		return $this->user_table_data['daysSpentOnTheApp'];
	}
	
	public function fetchOldUserDiv(){
		$this->checkUserTableData();
		return $this->user_table_data['old_user_div_shown'];
	}
	public function fetchAuthenticity(){
		$this->checkUserTableData();
		$images = $this->user_utils_dbo->getProfilePic(array($this->user_id), false);
		$status = $this->fetchStatus();
		$authentication_arr['status'] = $status;
		$authentication_arr['profile_set'] = false;
		$authentication_arr['TM_authentic'] = false;
		if(!empty($images)){
			$authentication_arr['profile_set'] = true;
			if($status == 'authentic')
			$authentication_arr['TM_authentic'] = true;
		}
		return $authentication_arr;
	}
	
	public function isShownHashTile(){
		$preferences = $this->user_utils_dbo->getPreferenceInterests(array($this->user_id));
		//var_dump($preferences);
		if(isset($preferences['preferences'])){
			return true;
		}else
			return false;
	}


}



?>