<?php
require_once dirname(__FILE__).'/include/config.php';
require_once dirname ( __FILE__ ) . "/UserActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";

class Credits{

	private $conn;
	private $user_id;
	private $profile_id;
	private $basic_credits_req =10;

	function __construct($user_id, $profile_id){
		global $conn, $creditsToOpenProfile, $baseurl;
		//$this->basic_credits_req = $creditsToOpenProfile;
		$this->conn = $conn;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->profile_id = $profile_id;
		$this->user_id = $user_id;
	}

	public function consume($UserActions){
		$sql1 = $this->conn->Prepare("SELECT * from user_current_credits where user_id = ?");
		$res = $this->conn->Execute($sql1, array($this->user_id));
		$creditsInfo = $res->FetchRow();
		//var_dump($creditsInfo);die;

		if($creditsInfo['current_credits'] >= $this->basic_credits_req){

			$credits_left = $creditsInfo['current_credits'] - $this->basic_credits_req;

			$sql2 = $this->conn->Prepare("UPDATE user_current_credits  set current_credits = ? where user_id = ?");
			$this->conn->Execute($sql2, array($credits_left, $this->user_id));
			$sql3 = $this->conn->Prepare("INSERT INTO user_credit_consumption values(?,?,?,now())");
			$this->conn->Execute($sql3, array($this->user_id, $this->profile_id, $this->basic_credits_req));

			//$UserActions-> performAction($this->user_id,$this->profile_id, 'can_communicate');
			$UserActions-> performAction($this->user_id,$this->profile_id, 'credit_used');
			return true;

		}

		return false;
	}
}

try{
	$user = functionClass::getUserDetailsFromSession();
	$user_id = $user['user_id'];
	$post_user_id = $_REQUEST['uid'];


	$status = $user['status'];
	$profile_id = $_REQUEST['mid'];
	//$credits = $_REQUEST['credits'];
	$mesh = $_REQUEST['mesh'];
	//echo $user_id; die;
	/*	$checkSumStr = $user_id .'|' . $profile_id . '|' . $creditsToOpenProfile;;
	$postCheckSum = Utils::calculateCheckSum($checkSumStr);*/

	$UserActions=new UserActions();
	//TODO: check if already credit consumed
	$val=$UserActions->checkHash();
	$device_type = ($_REQUEST['login_mobile'] == true)? 'mobile' :'html';
	$output = array();

	if(($val == true) && $post_user_id == $user_id){

		if($status != 'blocked'){

			$UserActions=new UserActions();
			$actionsDone = $UserActions->getUserActionsDone($user_id, array($profile_id));
			$actions = $actionsDone[$profile_id];
			if(!in_array(UserActionConstantValues::credit_used, $actions)){

				$cc = new Credits($user_id, $profile_id);
				$canConsume = $cc->consume($UserActions);
				$msg_checksum_str = $user_id . '|' . $profile_id;
				$msg_checksum = Utils::calculateCheckSum($msg_checksum_str);

				if($canConsume == true){
					$sql_user = $conn->Prepare("SELECT fname, lname from user where user_id = ?");
					 $ex = $conn->Execute($sql_user, $profile_id);
					 $name = $ex->FetchRow();

					$output['consume_credit'] = array( "msg_url" => "$baseurl/msg/message_full_conv.php?match_id=$profile_id&mesh=$msg_checksum",
					"first_name" => $name['fname'],
					// "last_name" => $name['lname'],
					"ajax_mutual_conn_link" => "$baseurl/ajax_mutualConn.php?uid=$profile_id",
								 "status" => 200);
				}
				else{
					$output['consume_credit'] = array("error" => "Insufficient credits",
								"status" => 404);				
				}
			}
		}

		//	if($device_type=='mobile')
		//var_dump($output); die;
		echo json_encode($output); die;
		/*	if($device_type=='html'){
		 echo json_encode($output);
		 $smarty->assign("credit_reponse", $output);
		 }*/}
}
catch (Exception $e){
	trigger_error ( $e->getMessage (), E_USER_WARNING );
	}
?>