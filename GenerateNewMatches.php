<?php

require_once dirname ( __FILE__ ) . "/matches/matchScore.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/fb.php";
require_once dirname ( __FILE__ ) . "/UserActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";


/**
 * Generate new matches API
 * @author himanshu
 */

class generateNewMatches extends TMObject{


	private $userId;
	private $numberOfResults = 4;
	private $recommendation_number = 2;
	private $key;
	private $cronRecommendationLimit = 100;

	function __construct($user_id) {
		parent::__construct ( $user_id );
		$this->userId = $user_id;
		$this->key = "u:m:$user_id";

	}

	/**
	 * will fetch data from user_queue_no_match
	 */
	public function fetchResults($page_id, $caller = null) {
		
		//if( ($this->redis->exists ( $this->key ) == 0)|| $_SESSION['first_login']==true) {
		$uu = new UserUtils();
		$is_edited_set = $uu->getEditProfileInCache($this->userId);

		$sessDetails = functionClass::getUserDetailsFromSession();
		//var_dump($is_edited_set);
		//exit;
		if(($is_edited_set == true) || ($sessDetails['first_login']==true)) {
			$this->generateMatchesUsingMemoryTable($this->userId);

			if($is_edited_set == true){
				if($this->redis->LLEN($this->key)>0)
				$uu->setEditProfileMatchesFound($this->userId);
			}
			if($sessDetails['first_login']==true)
			unset($_SESSION['first_login']);
			//$this->generateMatches ( $this->userId,true );
		}

		//var_dump($is_edited_set);
		


		if($caller == 'dashboard')
		return $this->redis->LRANGE($this->key, 0, 3);

		else
		return $this->getMatches ( $this->userId, $page_id );
	}

	public function generateNewReccomendations($debug = false, $from_cron = false){
		//TODO: check if ids are correct
		//$ids = $this->generateMatches ( $this->userId);
		$ids=$this->generateMatchesUsingMemoryTable($this->userId, $debug, $from_cron);
		//	var_dump($ids); die;
		//find mutual connections here itself
		//$this->setMutualConnections($user_id, $ids);

	}


	public function getMatches($user_id, $page_id) {
		//$key = "user:match:$user_id";
		// echo $key;
		$page_id=$page_id-1;
		$start_range=$this->numberOfResults*$page_id;
		$end_range=$start_range+$this->numberOfResults-1;
		$data =  $this->redis->LRANGE($this->key, $start_range, $end_range);//$this->userQ->fetchMatch ( $this->key, $start_range, $end_range );
		return $data;
	}

	private function getRecommendationSet($user_id, $from_cron = false) {
		/*if($from_cron == true){
			
			//$sql1 = "SELECT user2 from user_recommendation where user1=$this->userId";
			$sql1 = "SELECT user2 from user_recommendation where user1=$this->userId 
					and user2 not in (select user2 from user_like where user1 = $this->userId
					union select user2 from user_reject where user1 = $this->userId 
					union select user2 from user_rejectMe where user1=$this->userId)";
			$rs = $this->conn->Execute($sql1);

			$count = $rs->RowCount();
			echo $count;
			if($count>=$this->cronRecommendationLimit) return 'exceeded_limit';

			$data = $rs->GetRows();
		}

		else{*/
			$sql = $this->conn->Prepare ( "select user2 from user_recommendation where user1=?  union
										select user2 from user_reject where user1=? union 
										select user2 from user_rejectMe where user1=?
										union select user1 from user_like where user2=?" );
			$rs = $this->conn->Execute ( $sql, array (
			$user_id,
			$user_id,
			$user_id,
			$user_id
			) );
			$output = array ();
			$data = $rs->GetRows ();
	//	}

		foreach ( $data as $val ) {
			$output [] = $val ['user2'];
		}
		$output = array_unique ( $output );
		//var_dump($user_id);
		//var_dump($output);exit;
		return $output;
	}

	public function add_apostrophe(&$item1, $key) {
		$item1 = "'$item1'";
	}
	private function addApostrophe($parameter) {
		$type = explode ( ",", $parameter );
		$val = array_walk ( $type, array (
		$this,
				'add_apostrophe' 
				) );
				return implode ( ",", $type );
	}

	private function checkissetorEmpty($value){
		if(isset($value)==false||strlen($value)==0)
			return false;
		return true;
	}
	
	public function generateMatchesUsingMemoryTable($user_id, $debug = false, $from_cron = false,$return_query=false){

		//var_dump($debug);exit;

		$user_id_to_fliter = $this->getRecommendationSet ( $this->userId , $from_cron);
		if($from_cron == true && $user_id_to_fliter == 'exceeded_limit') return null;
		//echo "cdiybcl";exit;
		$user_id_to_fliter = implode ( ",", $user_id_to_fliter );

		$t1 = microtime ( true );
		
		 // echo "<pre>"; echo 'filter'; var_dump($user_id_to_fliter);
		 
		$preferences = array ();
		$demo_array = array ();
		$demo_mine_array=array();
		$trait_array = array ();
		$trait_mine_array=array();
		$family_array = array ();
		$work_array = array ();
		
		$family_mine_array = array ();
		$work_mine_array = array ();
		
		$sql = $this->conn->Execute ( "SELECT start_age, end_age, languages_preference, location,religionCaste, marital_status from user_preference_demography where user_id = " . $this->userId );
			if ($sql->RowCount () > 0) {
			$demo_array = $sql->FetchRow ();
		}
		$sql=$this->conn->Execute("select DateOfBirth,religion,mother_tongue,stay_city,stay_state,marital_status,stay_country from user_demography where user_id = " . $this->userId );
		if ($sql->RowCount () > 0) {
			$demo_mine_array = $sql->FetchRow ();
		}
		
		// get the preference list from different table
		//		$sql = $this->conn->Execute ( "SELECT start_height, end_height, skin_tone, body_type,disability,smoking_status, drinking_status, food_status from user_preference_trait where user_id = " . $this->userId );
		$sql = $this->conn->Execute ( "SELECT start_height, end_height,body_type_search as body_type, smoking_status_search as smoking_status,drinking_status_search as drinking_status, food_status_search as food_status from user_preference_trait where user_id = " . $this->userId );
		if ($sql->RowCount () > 0) {
			$trait_array = $sql->FetchRow ();
		}

		$sql=$this->conn->Execute("select height,body_type,smoking_status,drinking_status,food_status from user_trait where user_id = " . $this->userId );
		if ($sql->RowCount () > 0) {
			$trait_mine_array = $sql->FetchRow ();
		}
		
		//$sql = $this->conn->Execute ( "SELECT marital_status, meet_spouse_havingChildren, family_type,family_status,manglik_status from user_preference_family where user_id = " . $this->userId );
		$sql = $this->conn->Execute ( "SELECT family_status from user_preference_family where user_id = " . $this->userId );
		if ($sql->RowCount () > 0) {
			$family_array = $sql->FetchRow (); // , $preferences );
		}
		/*
		$sql=$this->conn->Execute("select family_status from user_family where user_id = " . $this->userId );
		if ($sql->RowCount () > 0) {
			$family_mine_array = $sql->FetchRow ();
		}
		*/		
		
		// "SELECT working_area,industry,income_range,education from user_preference_work where user_id = " . $this->userId
		$sql = $this->conn->Execute ( "SELECT working_area_search as working_area,industry, education,  income_start, income_end from user_preference_work where user_id = " . $this->userId );
		if ($sql->RowCount () > 0) {
			$work_array = $sql->FetchRow ();
		}		
		
		$sql=$this->conn->Execute("select working_area,industry,education,income_start,income_end from user_work where user_id = " . $this->userId );
		if ($sql->RowCount () > 0) {
			$work_mine_array = $sql->FetchRow ();
		}
		
		$gender = $this->conn->Execute ( "select gender from user where user_id=" . $this->userId );
		$myGender = $gender->FetchRow ();
		if ($myGender ['gender'] == 'M')
		$oppositeGender = 'F';
		else
		$oppositeGender = 'M';
			
		$t2 = microtime ( true );

		if($debug == true){
			echo '		Time in preference queries ' . ($t2 - $t1);
		}
		//	echo 'preference Query Time: ' . ($t2-$t1);
		// $preferences = array_merge ( $demo_array );
		// exit ();
		// tables: user user_preference_work user_preference_family user_preference_demography user_preference_trait
		// tables: user user_work user_family user_demography user_trait
	

		$query_crude = "select user_id,UNIX_TIMESTAMP(last_login) as last_login, trust_score
		from user_search
		where gender='$oppositeGender'";

		// var_dump($demo_array);
		// var_dump(count ( $demo_array )>0 );
		// demo sql
		if (count ( $demo_array ) > 0) {
			$start_age_t = $demo_array ['start_age'];
			$end_age_t = $demo_array ['end_age'] + 1;

			/*$start_age = date ( "Y-m-d", strtotime ( "-$end_age_t years" ) );
			 $end_age = date ( "Y-m-d", strtotime ( "-$start_age_t years" ) );*/
			//	$query_crude .= " and ud.DateOfBirth>='$start_age' and ud.DateOfBirth<='$end_age'";

			$query_crude .= " and age>='$start_age_t' and age<='$end_age_t'";


			if ($this->checkissetorEmpty( $demo_array ['marital_status'] )) {
				$query_crude .= " and marital_status in (" . $this->addApostrophe ( $demo_array ['marital_status'] ) . ")";
			}

			if ($this->checkissetorEmpty( $demo_array ['languages_preference'] )) {
				$languages_preference = explode ( ",", $demo_array ['languages_preference'] );
				$val = array_walk ( $languages_preference, array (
				$this,
						'add_apostrophe' 
						) );
						$languages_preference = implode ( ",", $languages_preference );
						$query_crude .= " and mother_tongue in ($languages_preference)";
			}
			if ($this->checkissetorEmpty ( $demo_array ['location'] )) {
				// 113:5268-42504;1:827:829:5296;
				$location_string = explode ( ";", $demo_array ['location'] );
				$final_array = array ();
				foreach ( $location_string as $val ) {
					if (strlen ( $val ) == 0)
					continue;
					$state_segment = explode ( ":", $val );
					$country = $state_segment [0];
					$final_array [$country] = array ();
					for($j = 1; $j < count ( $state_segment ); $j ++) {
						$city_segment = explode ( "-", $state_segment [$j] );
						$state = $city_segment [0];
						$final_array [$country] [$state] = array ();
						for($k = 1; $k < count ( $city_segment ); $k ++) {
							$final_array [$country] [$state] [$city_segment [$k]] = "";
						}
					}
				}
				$query = "";
				$c_counter = 0;
				foreach ( $final_array as $ccode => $state_data ) {
					if ($c_counter == 0)
					$query .= " (country='$ccode' ";
					else
					$query .= " or (country='$ccode' ";
					if (count ( $state_data ) > 0) {
						$query .= " and (";
					}
					$s_counter = 0;
					foreach ( $state_data as $state => $city_data ) {
						if ($s_counter == 0)
						$query .= " (state='$state' ";
						else
						$query .= " or (state='$state' ";

						if (count ( $city_data ) > 0) {
							$cities = implode ( ",", array_keys ( $city_data ) );
							$query .= " and city in ($cities) ";
						}
						$s_counter ++;
						$query .= ")";
					}
					if (count ( $state_data ) > 0) {
						$query .= " )";
					}
					$c_counter ++;
					$query .= " ) ";
				}

				$query_crude .= " and (" . $query . " )";
				// stay_country | stay_state | stay_city
			}
			if ($this->checkissetorEmpty ( $demo_array ['religionCaste'] )) {

				// religion | caste
				$religion_string = explode ( ";", $demo_array ['religionCaste'] );
				// var_dump($reliogion_string);
				$final_array = array ();
				foreach ( $religion_string as $val ) {
					if (strlen ( $val ) == 0)
					continue;
					$caste = explode ( ":", $val );
					$religion = $caste [0];
					$final_array [$religion] = array ();
					for($j = 1; $j < count ( $caste ); $j ++) {
						$final_array [$religion] [$caste [$j]] = "";
					}
				}
				// var_dump($final_array);
				if (count ( $final_array ) > 0) {
					$query = "";
					$r_counter = 0;
					foreach ( $final_array as $relgion => $caste_data ) {
						if ($r_counter == 0)
						$query .= " (religion='$relgion' ";
						else
						$query .= " or (religion='$relgion' ";
						if (count ( $caste_data ) > 0) {
							$castes = implode ( ",", array_keys ( $caste_data ) );
							$query .= " and caste in ($castes) ";
						}

						$r_counter ++;
						$query .= " ) ";
					}
					$query_crude .= " and (" . $query . " )";
					// var_dump ( $query );
				}
			}
		}

		if (count ( $trait_array ) > 0) {
			$start_height = $trait_array ['start_height'];
			$end_height = $trait_array ['end_height'];
			$query_crude .= " and height>='$start_height' and height<='$end_height'";
			if ($this->checkissetorEmpty ( $trait_array ['body_type'] )) {
				$query_crude .= " and body_type in (" . $this->addApostrophe ( $trait_array ['body_type'] ) . ")";
			}
			if ($this->checkissetorEmpty ( $trait_array ['smoking_status'] )) {
				$query_crude .= " and smoking_status in (" . $this->addApostrophe ( $trait_array ['smoking_status'] ) . ")";
			}
			if ($this->checkissetorEmpty ( $trait_array ['drinking_status'] )) {
				$query_crude .= " and drinking_status in (" . $this->addApostrophe ( $trait_array ['drinking_status'] ) . ")";
			}
			if ($this->checkissetorEmpty ( $trait_array ['food_status'] )) {
				$query_crude .= " and food_status in (" . $this->addApostrophe ( $trait_array ['food_status'] ) . ")";
			}
		}
/*
		if (count ( $family_array ) > 0) {
			
			if ($this->checkissetorEmpty ( $family_array ['family_type'] )) {
				$query_crude .= " and family_type in (" . $this->addApostrophe ( $family_array ['family_type'] ) . ")";
			}
			if ($this->checkissetorEmpty ( $family_array ['family_status'] )) {
				$query_crude .= " and family_status in (" . $this->addApostrophe ( $family_array ['family_status'] ) . ")";
			}
			
		}
*/
		if (count ( $work_array ) > 0) {
			// for income group
			// $start_height = $trait_array ['start_height'];
			// $end_height = $trait_array ['end_height'];
			// $query_crude .= " and ut.height>='$start_height' and ut.height<='$end_height'";
			if ($this->checkissetorEmpty ( $work_array ['working_area'] )) {
				$query_crude .= " and working_area in (" . $this->addApostrophe ( $work_array ['working_area'] ) . ")";
			}
			if ($this->checkissetorEmpty ( $work_array ['industry'] )) {
				$query_crude .= " and industry in (" . $this->addApostrophe ( $work_array ['industry'] ) . ")";
			}
			if ($this->checkissetorEmpty ( $work_array ['education'] )) {
				$query_crude .= " and highest_education in (" . $this->addApostrophe ( $work_array ['education'] ) . ")";
			}

			/*
			if ($this->checkissetorEmpty ( $work_array ['income_start'] )) {
				$query_crude .= " and income_start >= (" .  ( $work_array ['income_start'] ) . ")";
			}
			if ($this->checkissetorEmpty ( $work_array ['income_end'] ) && !empty( $work_array ['income_end'] )) {
				$query_crude .= " and income_end <= (" .  ( $work_array ['income_end'] ) . ")";
			}
			*/
			$income_start=$this->checkissetorEmpty ( $work_array ['income_start'] )?$work_array ['income_start']:-100;
			$income_end=$this->checkissetorEmpty ($work_array ['income_end'] )?$work_array ['income_end']:200;
			$query_crude .= " and (GREATEST(IFNULL(income_start,-100),$income_start)<=
			LEAST(IFNULL(income_end ,200),$income_end)) ";
				
		}
		

		if(count($demo_mine_array)>0){
				
			$my_age=date_diff(date_create($demo_mine_array['DateOfBirth']), date_create('today'))->y;
			$religion=$demo_mine_array['religion'];
			$mother_tongue=$demo_mine_array['mother_tongue'];
			$stay_city=$demo_mine_array['stay_city'];
			$stay_state=$demo_mine_array['stay_state'];
			$stay_country=$demo_mine_array['stay_country'];
			$marital_status=$demo_mine_array['marital_status'];
			if($this->checkissetorEmpty($my_age)){
				$query_crude .= " and pref_start_age<='$my_age' and (pref_end_age+1)>='$my_age'";
			}
			if($this->checkissetorEmpty($religion)){
				$query_crude .= " and (pref_religionCaste regexp '[[:<:]]".$religion."[[:>:]]' or pref_religionCaste is NULL)";
			}
			if($this->checkissetorEmpty($mother_tongue)){
				$query_crude .= " and (pref_mother_tounge regexp '[[:<:]]".$mother_tongue."[[:>:]]' or pref_mother_tounge is NULL) ";
			}
			if($this->checkissetorEmpty($marital_status)){
				$query_crude .= " and (pref_marital_status regexp '[[:<:]]".$marital_status."[[:>:]]' or pref_marital_status is NULL) ";
			}
			$city_query=$stay_country."_".$stay_state."_".$stay_city;
			$state_query=$stay_country."_".$stay_state;
			$country_query=$stay_country;
				
			//make state query
			$query_crude.= " and (pref_location is null or pref_location
					 regexp '[[:<:]]".$city_query."[[:>:]]' or pref_location
					 regexp '[[:<:]]".$state_query."[[:>:]]' or pref_location
					 regexp '[[:<:]]".$country_query."[[:>:]]')";
		}
		
		if (count ( $trait_mine_array ) > 0) {
			$height = $trait_mine_array ['height'];
			$body_type=$trait_mine_array ['body_type'];
			$smoking_status = $trait_mine_array ['smoking_status'];
			$drinking_status = $trait_mine_array ['drinking_status'];
			$food_status = $trait_mine_array ['food_status'];

			if($this->checkissetorEmpty($height)){
				$query_crude .= " and pref_start_height<='$height' and pref_end_height>='$height'";
			}
			if($this->checkissetorEmpty($body_type)){
				$query_crude .= " and (pref_body_type regexp '[[:<:]]".$body_type."[[:>:]]' or pref_body_type is NULL)";
			}
			if($this->checkissetorEmpty($smoking_status)){
				$query_crude .= " and (pref_smoking_status regexp '[[:<:]]".$smoking_status."[[:>:]]' or pref_smoking_status is NULL) ";
			}
			if($this->checkissetorEmpty($drinking_status)){
				$query_crude .= " and (pref_drinking_status regexp '[[:<:]]".$drinking_status."[[:>:]]' or pref_drinking_status is NULL) ";
			}
			if($this->checkissetorEmpty($food_status)){
				$query_crude .= " and (pref_food_status regexp '[[:<:]]".$food_status."[[:>:]]' or pref_food_status is NULL) ";
			}
		}
		
		if (count ( $family_mine_array ) > 0) {
			$family_status = $family_mine_array ['family_status'];
			if($this->checkissetorEmpty($body_type)){
				$query_crude .= " and (pref_family_status regexp '[[:<:]]".$family_status."[[:>:]]' or pref_family_status is NULL)";
			}
		}
		
		if (count ( $work_mine_array ) > 0) {
			$income_start = $work_mine_array ['income_start'];
			$income_end = $work_mine_array ['income_end'];
			$working_area=$work_mine_array ['working_area'];
			$industry = $work_mine_array ['industry'];
			$education = $work_mine_array ['education'];
		
			if($this->checkissetorEmpty($income_start)&&$this->checkissetorEmpty($income_end)){
				$query_crude .= " and (GREATEST(IFNULL(pref_income_start,-100),$income_start)<= 
					 LEAST(IFNULL(pref_income_end ,200),$income_end)) ";
			}
			/*
			if($this->checkissetorEmpty($income_end)){
				$query_crude .= " and (pref_income_end>='$income_end' or  pref_income_end is NULL)";
			}*/
			if($this->checkissetorEmpty($working_area)){
				$query_crude .= " and (pref_working_area regexp '[[:<:]]".$working_area."[[:>:]]' or pref_working_area is NULL)";
			}
			if($this->checkissetorEmpty($industry)){
				$query_crude .= " and (pref_industry regexp '[[:<:]]".$industry."[[:>:]]' or pref_industry is NULL) ";
			}//special case of industry.if industry is null..
			else{
				$query_crude .= " and  (pref_industry is NULL) ";
			}
			
			if($this->checkissetorEmpty($education)){
				$query_crude .= " and (pref_education regexp '[[:<:]]".$education."[[:>:]]' or pref_education is NULL) ";
			}
		}

		if (strlen ( $user_id_to_fliter ) > 0)
		$query_crude .= ' and user_id not in (' . $user_id_to_fliter . ')   ';
		/*
		 * if ($revToggle == 1 and pref_start_age<$pref_end_age) $query .= ' and pref_start_age > age and pref_end_age < age ';
		 */
		$query_crude .= " order by last_login desc limit 1000";

		//query var_dump
	//	var_dump($query_crude);
	//exit;
	if($return_query==true){
		return $query_crude;
	}
		try {
			trigger_error($query_crude,E_USER_WARNING);
			$rs = $this->conn->Execute ( $query_crude );

			//	echo ' total:' . ($t3-$t1);

			//exit;
			$result = $rs->GetRows();
			$t3 = microtime ( true );

			if($debug){	echo '			 Crude Query Time: ' . ($t3-$t2);
			echo PHP_EOL;
			var_dump($query_crude);
			}

			//var_dump($query_crude);
		} catch ( Exception $e ) {
			//echo 'error: ' . $e;
			trigger_error ( $e->getMessage (), E_USER_WARNING );
		}


		//var_dump($query_crude);
		//var_dump($result);die;
		$idList = array ();
		$reArrangedArr = array ();
		$trustArr = array ();
		foreach ( $result as $row ) {
			$idList [] = $row ['user_id'];
		}

		// get the other way match also, only after that calculate rank etc

		// get the match score
		$ms = new matchScore ();
		// var_dump($group_ids);942845

		// $mid = 942845;

		if($myGender ['gender']=='F'){
			foreach ($idList as $key=>$val){
				$matchScores[$val] = $ms->calculateMatchScore ( $this->userId, $val,"female" );
			}
		}
		else {
			foreach ($idList as $key=>$val){
				$matchScores[$val] = $ms->calculateMatchScore ( $val, $this->userId,"male" );
			}
		}

		$t4 = microtime ( true );

		if($debug){
			echo ' psycho time:' . ($t4 - $t3);
		}

		$reccoLogArray = array();
		foreach ($result as $r){
			$reccoLogArray[$r['user_id']] = $r;
		}

		// calculate ranks of all the values
		$rankedSet = $this->calculateRank ( $result, $matchScores );
		
		//remove and log all the ids with negative match score
		$this->removeAndLogNegativeMatchScore($rankedSet, $matchScores, $reccoLogArray, $from_cron);
		
		asort ( $rankedSet );
		//var_dump($rankedSet); die; 
		//need to store only top $numberOfResults values;
		$recommendation_set=array_slice ( $rankedSet, 0, $this->recommendation_number, true );

		$idList=array_keys($recommendation_set);
		//exit;
		//store results
		if($from_cron == true){
			$table = Utils::getSlaveRecoTableName();
		}
		
		$this->storeResults($idList, $matchScores, $reccoLogArray, $rankedSet, $table);

		if(!$from_cron)
		$this->addMatches( $idList,$first_time);

		if(count($idList)>0){
			$uu = new UserUtils();
			$uu->setMutualConnections($this->userId, $idList, $from_cron);}
			// get ranks correspondence in array to store in redis - 2 different arrays
			$t6 = microtime ( true );
			return $rankedSet;
			// return array_slice ( $rankedSet, 0, $number_of_matches, true );

	}
	private function storeResults($id_arrays, $matchScores, $reccoLogArray,$rankedSet , $tableName =false){
		$table = "user_recommendation";
		
		if($tableName == true){
			/*$file = '/tmp/redis_mass_insert.txt';
			$key_length = strlen($this->key);*/
			$table = $tableName;
		}
		
		foreach($id_arrays as $val){
			//$last_login = Utils::GMTTOISTfromUnixTimestamp($reccoLogArray[$val]['last_login']);
			$mysqlTimestamp = date("Y-m-d H:i:s", $reccoLogArray[$val]['last_login']);
			//echo $rankedSet[$val]; die;
			//	var_dump($mysqlTimestamp); die;

			/*if($from_cron){
				$match_id_length = strlen((string)$val);
				$str = "*3\\r\\n$5\\r\\nlpush\\r\\n$$key_length\\r\\n$this->key\\r\\n$match_id_length\\r\\n$val\\r\\n";
				file_put_contents($file, $str, FILE_APPEND | LOCK_EX);
			}*/

			$prprdSt =$this->conn->Prepare("insert into $table values(?,?, now(),?, ?, ?, ?)");
			$this->conn->Execute($prprdSt,array($this->userId,$val, $matchScores[$val], $reccoLogArray[$val]['trust_score'],$mysqlTimestamp, $rankedSet[$val]));
		}

	}

	private function calculateRank($result, $matchedUsersArray) {

		$last_logins = array();
		$trust_scores = array();
		$match_scores = array();

		foreach ($result as $row){
			$last_logins[$row['user_id']] = (int)$row['last_login'];
			$trust_scores[$row['user_id']] = (int)$row['trust_score'];
		}


		arsort($last_logins);
		arsort($trust_scores);
		arsort($matchedUsersArray);

		/*		var_dump($last_logins);
		 var_dump($trust_scores);
		 var_dump($matchedUsersArray);*/

		$rank = array();
		$i=1;
		foreach ($last_logins as $key=>$val){
			$rank[$key] = $i;
			$i++;
		}

		$i=1;
		foreach ($trust_scores as $key=>$val){
			$rank[$key] += $i;
			$i++;
		}

		$i=1;
		foreach ($matchedUsersArray as $key=>$val){
			$rank[$key] += $i;
			$i++;
		}
		/*$weightMatrix = array(
		 'trust_score' => 25,
		 'match_score' => 25,
		 'last_login' => 25,
		 'impression_count' => 25);

		 foreach ( $result as $row ) {
			$rank [$row ['user_id']] = ($weightMatrix ['last_login'] * $row ['last_login'] + $weightMatrix ['trust_score'] * ($row ['trust_score'] != null ? (( int ) $row ['trust_score']) : 0)) + ($weightMatrix ['match_score'] * (isset ( $matchedUsersArray [$row ['user_id']] ) ? $matchedUsersArray [$row ['user_id']] : 0));
			}
			*/
		return $rank;
	}


	public function addMatches($user_array,$first_time) {
		$args[] = $this->key;
		for ($i = 0; $i < count($user_array); $i++) {
			$args[] = $user_array[$i];
		}
		call_user_func_array(array($this->redis,'RPUSH'),$args);
	}
	
	/**
	 * To remove all the ids with negative matchscore from current result set and
	 * log all those negative match score ids if we run the recommendation engine from cron
	 * @param unknown_type $allMatches
	 * @param unknown_type $matchScores
	 * @param unknown_type $reccoLogArray
	 * @param unknown_type $from_cron
	 */
	private function removeAndLogNegativeMatchScore(&$allMatches, $matchScores,$reccoLogArray, $from_cron = false){
		$negativeScoreArr = array();
		$negativeScoreIds = array();

		foreach ($matchScores as $key => $val){
			if($val < 0 ){
				$negativeScoreIds[$key]= $key;
				$negativeScoreArr[$key] = $allMatches[$key];
				unset($allMatches[$key]);
			}
		}

		if($from_cron == true){
			$table = Utils::$negativeRecoTableName;
			$this->storeResults($negativeScoreIds, $matchScores, $reccoLogArray, $negativeScoreArr, $table);
		}
	}
	
}
?>
