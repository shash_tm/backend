<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/fb.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/TrustBuilder.class.php";
require_once dirname ( __FILE__ ) . "/include/tbconfig.php";
require_once dirname ( __FILE__ ) . "/include/tbmessages.php";
require_once dirname ( __FILE__ ) . "/include/header.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/abstraction/query_wrapper.php";

$data = $_POST;
$func=new functionClass();
$login_mobile = $func->isMobileLogin();
$user = functionClass::getUserDetailsFromSession ();
//$header_arr=$func->getHeader();
if(isset($user['user_id'])){
	$status= functionClass::getUserStatus($user['user_id']);
}

if(isset($_REQUEST['caller']) && $_REQUEST['caller']=='admin' && isset($_SESSION['admin_id'])){
	$user['user_id'] = $_REQUEST['user_id'];
	$_SESSION['user_id_admin'] = $_REQUEST['user_id'];
}
if(!isset($user['user_id']) && isset($_SESSION['user_id_admin'])){
	$user['user_id'] = $_SESSION['user_id_admin'];
}

$trustBuilder = new TrustBuilder($user['user_id']);

if(isset($data['checkFacebook'])&&$data['checkFacebook']=='check'&&isset($data['token'])){
	$output = "";
	try{
		$facebook = new fb ( $data['token'] );
		$fb_data = $facebook->getBasicData();
		$facebook->logFacebook($user['user_id']);
		$presence = $facebook->checkPrescence($user['user_id']);
		$fb_likes = "";
		if(($presence['status']=='NEW')||isset($data['reimport'])){
			if(!isset($data['connected_from']))
			$data['connected_from'] = 'trustbuilder';

			if(isset($data['reimport']) && $data['reimport'] == 'reimport'){
				$output = $trustBuilder->imposeReimportedData($data['token']);
			}else{
				$output = $trustBuilder->verifyFacebook($data['token'],true,$data['connected_from']);
			}
				/*if(($user[status]=='non-authentic')&& !isset($output['error'])){
					if($trustBuilder->authenticateUser()){
						functionClass::setSessionStatus('authentic');
					}
				}*/
				if(!isset($output['error']) && $status=='non-authentic'){
					$trustBuilder->authenticateUser();
				}
		}
		elseif($presence['status']=='SUCCESS' && $data['connected_from'] == 'trustbuilder') {
			$diff = $trustBuilder->checkFbTMDiff();
			if(isset($diff) && count($diff)>0) {
				$output = $trustBuilder->checkFBDiffType($diff);
			}else {
				$output['responseCode'] = 200;
				$output['status']='success';
				$output['connections'] = $presence['connection'];
			}
		}
		elseif($presence['status']=='SUCCESS'){
			$output['responseCode'] = 200;
			$output['status']='success';
			$output['connections'] = $presence['connection'];
		}elseif($presence['status']=='ANOTHER'){
			$facebook->logFacebook($user['user_id'],"Another user exits");
			$output['responseCode'] = 403;
			$output['status']='fail';
			$output['error']='This Facebook account is in use by another Trulymadly profile.';//'Please use the same facebook account used to create the profile';
		}
		else{
			$facebook->logFacebook($user['user_id'],"Another user exits");
			$output['responseCode'] = 403;
			$output['status']='fail';
			$output['error']='This Facebook account is in use by another Trulymadly profile.';
		}

		if(isset($data['action'])&&$data['action']=='interest'&&($presence['status']=='NEW'||$presence=='SUCCESS')){
			$output['fb_likes']=$func->getFbLikes();
		}
	}
	catch (Exception $e){
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		$output['status']='refresh';
		$output['responseCode'] = 403;
		$output['error'] = "Your Facebook session is timed out. Please login again.";
	}
	
	print_r(json_encode($output));
	die;
}elseif(isset($data['fileSubmit'])&&isset($data['type'])&&isset($_FILES['file'])){
	$name=1000*microtime(true)."_".$func->random(18);
	$trustBuilder->documentUpload($data,$name);
}elseif(isset($data['photoUpload']) && $data['photoUpload']='yes' && isset($data['src']) && isset($data['type'])){
	try{
		$error = 0;
		$name=1000*microtime(true)."_".$func->random(18);
		$datas=explode("/",$data['src']);
		if(!($datas[0]=='http:' || $datas[0]=='https:')) {
			list($type, $data['src']) = explode(';', $data['src']);
			list(, $data['src']) = explode(',', $data['src']);
			$type_array=explode(":",$type);
			$fileType=$type_array[1];
			$fileTypeArray=explode("/",$fileType);
			$image_type=$fileTypeArray[1];
			$data['src'] = base64_decode($data['src']);

			if(!file_exists($baseurl."/files/images/profiles/".$name.".".$image_type))  {
				file_put_contents($baseurl."/files/images/profiles/".$name.".".$image_type,$data['src']);
				
				if($data['type']==1){
					$rs = $conn->Execute($conn->prepare("Select * from user_id_proof where user_id=?"),array($user['user_id']));
					if ($rs->RowCount () > 0) {
						$row = $rs->FetchRow ();
						$query="update user_id_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? ";
						$param_array=array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']);
						$tablename='user_id_proof';
						Query_Wrapper::UPDATE($query, $param_array, $tablename);
						/*$conn->Execute($conn->prepare("update user_id_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
								array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']));*/
					}else{
						$query="insert into user_id_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?)";
						$param_array=array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation');
						$tablename='user_id_proof';
						Query_Wrapper::INSERT($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("insert into user_id_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?)"),
								array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation'));*/
					}
				}elseif ($data['type']==2){
					$rs = $conn->Execute($conn->prepare("Select * from user_address_proof where user_id=?"),array($user['user_id']));
					if ($rs->RowCount () > 0) {
						$row = $rs->FetchRow ();
						
						$query="update user_address_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? ";
						$param_array=array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']);
						$tablename='user_address_proof';
						Query_Wrapper::UPDATE($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("update user_address_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
								array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']));*/
					}else{
						$query="insert into user_address_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,landmark) values(Now(),?,?,?,?,?,'','','','','')";
						$param_array=array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation');
						$tablename='user_address_proof';
						Query_Wrapper::INSERT($query, $param_array, $tablename);
						
						
						/*$conn->Execute($conn->prepare("insert into user_address_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,landmark) values(Now(),?,?,?,?,?,'','','','','')"),
								array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation'));*/
					}
				}elseif($data['type']==3){
					$rs = $conn->Execute($conn->prepare("Select * from user_employment_proof where user_id=?"),array($user['user_id']));
					if ($rs->RowCount () > 0) {
						$row = $rs->FetchRow ();
						
						$query="update user_employment_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? ";
						$param_array=array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']);
						$tablename='user_employment_proof';
						Query_Wrapper::UPDATE($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("update user_employment_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
								array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']));*/
					}else{
						$query="insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,name,type,tenure) values(Now(),?,?,?,?,?,'','','','','','','')";
						$param_array=array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation');
						$tablename='user_employment_proof';
						Query_Wrapper::INSERT($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,name,type,tenure) values(Now(),?,?,?,?,?,'','','','','','','')"),
								array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation'));*/
					}
				}
				
			}else{
				$error = 1;
			}
		}else{
			$error = 1;
		}
	}catch(Exception $e) {
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		$error = 1;
	}
	$return = '';
	if($error==0){
		$return['status']='success';
	}else{
		$return['status']='fail';
		$return['error']='Some Error Occured. Please try again.';
	}
	print_r(json_encode($return));
	die;
}elseif(isset($data['unlimited'])){
	$query="update user SET plan=? where user_id=?";
	$param_array=array('unlimited',$user['user_id']);
	$tablename='user';
	Query_Wrapper::UPDATE($query, $param_array, $tablename);
		
	/*$conn->Execute($conn->prepare("update user SET plan=? where user_id=?"),
			array('unlimited',$user['user_id']));*/
	$return['status']='success';
	print_r(json_encode($return));
	die;
}elseif(isset($data['action'])&&$data["action"]=='addressForm'){
	if($trustBuilder->saveAddressForm($data)){
		$return['status']='success';
	}else{
		$return['status']='fail';
	}
	print_r(json_encode($return));
	die;
}elseif (isset($data['action'])&&$data["action"]=='employmentForm'){
if($trustBuilder->saveEmploymentForm($data)){
		$return['responseCode'] = 200;
		$return['status']='success';
	}else{
		$return['responseCode'] = 403;
		$return['status']='fail';
	}
	print_r(json_encode($return));
	die;
}elseif(isset($data['action'])&&$data['action']=='numberVerify'){
	$status = $trustBuilder->phoneSubmit($data['number']);
	if($status){
		print_r(json_encode($status));
	}
	else print_r(json_encode(array("responseCode"=>403,"status"=>"fail","error"=>"Something happened! Please try calling.")));
	die;
}else if(isset($data['action'])&&$data['action']=='verifyDigits'){

	//var_dump($_REQUEST['action']);die;
	//echo $data['authToken'];die;
	$status = $trustBuilder->digitsVerify($data['authUrl'],$data['authToken'],$data['number']);
	//var_dump($data['authUrl']);
	if($status){
		print_r(json_encode($status));
	}
	else print_r(json_encode(array("responseCode"=>403,"status"=>"fail","error"=>"Your phone number could not be verified. Please try again.","error_code"=>7)));
	die;//
}
elseif(isset($data['action'])&&$data['action']=='checkNumber'){
	$status = $trustBuilder->checkPhoneStatus();
	if($status)
		print_r(json_encode($status));
	else print_r(json_encode(array("responseCode"=>403,"status"=>"fail","error"=>"Something happened! Please try calling.")));
	die;
}elseif(isset($data['action'])&&$data['action']=='numberVerifyOTP'){
	$status = $trustBuilder->generateOTP($data['number']);
	if($status){
		print_r(json_encode($status));
	}
	else print_r(json_encode(array("responseCode"=>403,"status"=>"fail","error"=>"Some error occured.Please retry.")));
	die;
}elseif(isset($data['action'])&&$data['action']=='checkOTP'){
	$status = $trustBuilder->verifyOTP($data['number'], $data['otp']);
	if($status){
		print_r(json_encode($status));
	}
	else print_r(json_encode(array("responseCode"=>403,"status"=>"fail","error"=>"Some error occured.Please retry.")));
	die;
}elseif(isset($data['action'])&&$data['action']=='sendpassword'){
	$return = $trustBuilder->sendPassword($data);
	print_r(json_encode($return));
	die;	
}elseif(isset($data['action'])&&$data['action']=='syncNamewithID'){
	if($data['type']==1){
		$return = $trustBuilder->syncNamewithID($data);
		print_r(json_encode($return));
		die;
	}
}

//$login_mobile = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
if(!(isset($_SESSION['user_id_admin']))){
	/*if ($login_mobile) {
		$authentication = functionClass::redirect ( 'trustbuilder', true );
		if ($authentication ['error'] != null)
			echo json_encode ( $authentication );
	} else {
		functionClass::redirect ( 'trustbuilder' );
	}*/
	functionClass::redirect ( 'trustbuilder',$login_mobile );
}

try {
	
	if(isset($_REQUEST['from_registration']) && $_REQUEST['from_registration'] == true){
		$smarty->assign("isRegistration",'1');
	}
	
	$user_id = $user ['user_id'];
	$header = new header($user_id);
	$header_values = $header->getHeaderValues();
	$gender = $header_values['gender'];
	$isEndorsementVerified = false;
	$idStatus = $trustBuilder->checkIdProofStatus();
	$phoneStatus = $trustBuilder->checkPhoneStatus();
	$employmentStatus = $trustBuilder->checkEmploymentProofStatus();
	//$facebookStatus = $trustBuilder->checkFacebookStatus();
	$isFbVerified = $trustBuilder->isFbVerified();
	$isAnyprofilePic = $trustBuilder->isAnyprofilePic();
	$authenticityConstant = $trustBuilder->getThreshold();     // for A/B testing

	
	
	/*if(	$facebookStatus['status'] == 'no'){
		$diff = $trustBuilder->checkFbTMDiff();
	}*/
	if(!$isFbVerified['isVerify']){
		$facebookStatus = $trustBuilder->checkFacebookStatus();
		if(isset($facebookStatus['mismatch'])){
			$diff = $trustBuilder->checkFbTMDiff();
		}
	}
	
	$linkedStatus = $trustBuilder->checkLinkedInStatus();
	$trustPoints = $trustBuilder->getTrustScore();
	
	/*
	 * endorsement block
	 */
	$endorsementData = array();
	$endorsementData['data'] =array();
	if($trustPoints['endorsement_count']>0){
		$endorses = $trustBuilder->getEndorsementData();
		$endorsementData['data'] = $endorses[$user_id];
	if(count($endorsementData['data'])==1){
		$endorsementData["endorsement_message"] = $tbMessages['endorsement_ifsingle_message'];
	}
	}
	$userUtils = new UserUtils();
	$endorsementData['link']=$userUtils->generateEndorsementLink($user_id, time(), $gender);
	if(!isset($endorsementData["endorsement_message"]))
	$endorsementData["endorsement_message"] = ($gender == "M")?$tbMessages['endorsement_male']:$tbMessages['endorsement_female'];
	$endorsementData["endorsement_success_message"] = $tbMessages['endorsement_success'];
	$endorsementData["endorsement_send"] = $tbMessages['endorsement_send'];
	$endorsementData["endorsement_profile_pic_added_success"] = $tbMessages['endorsement_profile_pic_added_success'];
	$endorsementData["endorsement_threshold_count"] = EndorsementConstants::MinimumEndorsementCount;
	if($trustPoints['endorsement_count'] >= EndorsementConstants::MinimumEndorsementCount) $endorsementData['isVerified'] = true;
	else $endorsementData['isVerified'] = false;

	
 	
} catch ( Exception $e ) {
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}

	try{
		if(!$login_mobile){
		$smarty->assign("isFbVerified", $isFbVerified['isVerify']);
		$smarty->assign("fbConnected",$facebookStatus['status']);
		$smarty->assign("fbConnection",$isFbVerified['connection']);
		$smarty->assign("diff", $diff);
		$smarty->assign("fbTmDiff", $facebookStatus['mismatch']);
		$smarty->assign("isAnyprofilePic",$isAnyprofilePic);
		
		if($phoneStatus){
			$smarty->assign("phoneVerified",$phoneStatus['status']);
			//$smarty->assign("phoneNumber",$phoneStatus['user_number']);
			$smarty->assign("phoneNumber",substr_replace($phoneStatus['user_number'],"xxxxxxx",3,9));
			$smarty->assign("phoneTrials",$phoneStatus['number_of_trials']);
		}else{
			$smarty->assign("phoneVerified",'N');
		}
		
		if($idStatus){
			$smarty->assign("idVerified",$idStatus['status']);
			$smarty->assign("idproofType",$idStatus['proof_type']);
			$smarty->assign("idReject",$idStatus['reject_reason']);
			$smarty->assign("id_diff",$idStatus);
		}else{
			$smarty->assign("idVerified",'N');
		}

		if($addressStatus){
			$smarty->assign("addressVerified",$addressStatus['status']);
			$smarty->assign("addressproofType",$addressStatus['proof_type']);
		}else{
			$smarty->assign("addressVerified",'N');
		}
		
		if($employmentStatus){
			if($employmentStatus['show_name']=='no'){
				$employmentStatus['company_name'] = substr($employmentStatus['company_name'], 0,1). str_repeat('x',strlen($employmentStatus['company_name']) - 2) . substr($employmentStatus['company_name'], strlen($employmentStatus['company_name']) - 1 ,1);
				$smarty->assign("companyName",$employmentStatus['company_name']);
			}else 
				$smarty->assign("companyName",$employmentStatus['company_name']);
			$smarty->assign("empVerified",$employmentStatus['status']);
			$smarty->assign("empproofType",$employmentStatus['proof_type']);
			$smarty->assign("empReject",$employmentStatus['reject_reason']);
			$smarty->assign("showName",$employmentStatus['show_name']);
		}else{
			$smarty->assign("empVerified",'N');
		}

		$smarty->assign("linkedinVerified",$linkedStatus['status']);
		$smarty->assign("linkedinConnection",$linkedStatus['connection']);
		$smarty->assign("linkedinDesignation",$linkedStatus['position']);
		$smarty->assign("trustscore",$trustPoints['trust_score']);
		$smarty->assign("trustpoints",$trustPoints);
		$smarty->assign("trustpie",$trustPoints['trust_score']>100 ? 100 : $trustPoints['trust_score']);
		$smarty->assign('header',$header_values);
		$smarty->assign('trustbuilder', 'trustbuilder');
		$smarty->assign('tbNumbers', $tbNumbers);
		$smarty->assign('tbMessages', $tbMessages);
		$smarty->assign('trustPage', 'trustPage');
		$smarty->display ("templates/trustbuilder.tpl" ) ;
		}
		/*if($login_mobile){
			$smarty->assign('from_mobile', true);
			$output = $smarty->fetch("templates/trustbuilder.tpl");
			$response['data'] = $output;
			print_r($output);die;
		}else
			$smarty->display ("templates/trustbuilder.tpl" ) ;*/
		else{
			$response['responseCode']=200;
			/*
			 * for facebook
			 */
			$fb['isVerified'] = $isFbVerified['isVerify'];
			$fb['fbConnection'] = $isFbVerified['connection'];
			$fb['TMFBdiff'] = $diff;
			$fb['fb_mismatch'] = $facebookStatus['mismatch'];
			$fb['fb_messages'] = $tbMessages['facebook'];			
			/*
			 * for linkedin
			 */
			$linkedin['isVerified'] = ($linkedStatus['status'])=='yes'?true:false;
			if($linkedin['isVerified']) {
				$linkedin['linkdesignation'] = (isset($linkedStatus['position']))?$linkedStatus['position']:$linkedStatus['connection']." Connections";
				$linkedin['linkconnection'] = $linkedStatus['connection'];
			}else {
				$linkedin['linkdesignation'] = NULL;
				$linkedin['linkconnection'] = NULL;
			}
			/*
			 * for ID Proof
			 */
			if($idStatus){
				if($idStatus['status']=='active'){
					$id['isVerified'] = true;
					$id['isProofType'] = $idStatus['proof_type'];
				}else{
					$id['isVerified'] = false;
					$id['status'] = $idStatus['status'];
					$id['isProofType'] = $idStatus['proof_type'];
					$id['isRejectReason'] = $idStatus['reject_reason'];
					$id['id_diff_data'] = $idStatus;
					$id['id_messages'] = $tbMessages['id'];
				}
			}else{
				$id['isVerified'] = false;
			}
			/*
			 * for phone number
			 */
			$phone['isVerified'] = ($phoneStatus['status']=='active'?true:false);
			$phone['phone_number'] = substr_replace($phoneStatus['user_number'],"XXXXX",5,5);
			$phone['number_of_trials'] = $phoneStatus['number_of_trials'];
			$phone['rejected_message'] = $tbMessages['phone']['rejectedgt3'];
			/*
			 * for employment proof 
			 */
			
			
			if($employmentStatus){
				if($employmentStatus['show_name']=='no'){
					$employmentStatus['company_name'] = substr($employmentStatus['company_name'], 0,1). str_repeat('x',strlen($employmentStatus['company_name']) - 2) . substr($employmentStatus['company_name'], strlen($employmentStatus['company_name']) - 1 ,1);
				}
				if($employmentStatus['status']=='active'){
					$employment['isVerified'] = true;
					$employment['companyName'] = $employmentStatus['company_name'];
				}else{
					$employment['isVerified'] = false;
					$employment['status'] = $employmentStatus['status'];
					$employment['companyName'] = $employmentStatus['company_name'];
					$employment['proofType'] = $employmentStatus['proof_type'];
					$employment['RejectReason'] = $employmentStatus['reject_reason'];
					$employment['emp_msg'] = $tbMessages['employment'];
				}
			}else{
				$employment['isVerified'] = false;
			}
			
			/*$ud = new UserData($user_id);
			$gender = $ud->fetchGender();*/
			//$authenticityConstant = ($gender == "M")?TrustAuthenticConstants::TrustAuthenticConstantMale:TrustAuthenticConstants::TrustAuthenticConstantFemale;
			$maleAuthenticityConstant = TrustAuthenticConstants::TrustAuthenticConstantMale;
			$trust_score = array("fb"=>$tbNumbers['score']['facebook'],"photo_id"=>$tbNumbers['score']['photoid'],"linkedin"=>$tbNumbers['score']['linkedin'],"employment"=>$tbNumbers['score']['employment'],"phone"=>$tbNumbers['score']['mobile'],"threshold"=>$authenticityConstant, "endorsements"=>$tbNumbers['score']['endorsement'] );
			
			$non_authentic_message = "Build at least ".$authenticityConstant."% to view matches.";//($gender == "M")?"Get ".$authenticityConstant."% score to become verified":"Get atleast ".$authenticityConstant."% score to become verified";
			$diffuseline1text1 = "Need ";//($gender == "M")?"Need ":"Build your ";
			$diffuseline1text2 = $authenticityConstant."% Score";//($gender == "M")?$authenticityConstant."% Score":"Trust Score";
			$diffuseline2text1 = "to become ";//($gender == "M")?"to become ":"to get ";
			$diffuseline2text2 = "Verified";
			$femaleSubText = "Shhh... Guys need ".$maleAuthenticityConstant."%!"; 
			$trustMessage = array();
			
			$trustMessage['zero'] = $non_authentic_message;
			$trustMessage['hundred'] = $tbMessages['trustMessage']['hundred'];
			$trustMessage['zeroToHundred'] = $tbMessages['trustMessage']['authentic'];
			
			$messages = array("non-authentic"=>$non_authentic_message ,"authentic"=>$tbMessages['trustMessage']['authentic'],"diffuseline1text1"=>$diffuseline1text1,"diffuseline1text2"=>$diffuseline1text2,"diffuseline2text1"=>$diffuseline2text1,"diffuseline2text2"=>$diffuseline2text2,"femaleSubText"=>$femaleSubText,"trustScoreMessage"=>$trustMessage);
			
			
			$status = functionClass::getUserStatus($user['user_id']);
			/*
			 * for trustScore
			 */
			$response['data'] = array("facebook"=>$fb,"linkedin"=>$linkedin,"idProof"=>$id,"phone"=>$phone,"employment"=>$employment,"trustScore"=>$trustPoints['trust_score'],"isAnyprofilePic"=>$isAnyprofilePic,"trustPoints"=>$trust_score,"status"=>$status,"messages"=>$messages, "endorsementData"=>$endorsementData);
			
			$resource=$response;
			unset($resource['responseCode']);
			$resource=json_encode($resource);
			$hashCheck=functionClass::checkHash($resource,$_REQUEST['hash']);
				
			//var_dump($hashCheck);
				
			if($hashCheck['status']==false)
				$response['hash']=$hashCheck['hash'];
				else
				$response=array("responseCode"=>304);
			$response["tstamp"]=time();
			print_r(json_encode($response));die();
		}
	}catch(Exception $e){
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	}
?>
