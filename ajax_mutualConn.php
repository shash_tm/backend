<?php
require_once dirname(__FILE__).'/include/config.php';
require_once dirname ( __FILE__ ) . "/UserActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";


$user = functionClass::getUserDetailsFromSession();

if(isset($user['user_id'])&&isset($_REQUEST['uid'])){


	$user_id = $user['user_id'];
	$post_user_id = $_REQUEST['uid'];
	$device_type = ($_REQUEST['login_mobile'] == true)? 'mobile' :'html';

	$UserActions=new UserActions();
	$actionsDone = $UserActions->getUserActionsDone($user_id, array($post_user_id));
	$actions = $actionsDone[$post_user_id];
	if(in_array(UserActionConstantValues::credit_used, $actions)){
		$sql_mConns = $conn->Prepare("SELECT mutual_connections from user_facebook_mutual_connections where user1=? and user2=?");
		$res_mconn = $conn->Execute($sql_mConns, ($post_user_id> $user_id)? array($post_user_id, $user_id): array($user_id, $post_user_id));
		$mutual_connections = $res_mconn->FetchRow();
		$mutualConnDetails = json_decode($mutual_connections['mutual_connections'], true);

		$mobile_arr = array();
		$smarty_var = '<ul class="mutualcon" id="mutfbcon">';
		foreach ($mutualConnDetails as $key=>$val){
			if($device_type == 'html'){
				$smarty_var .= "<li><img src='" .$val[picture][data][url]. "'><p>".$val[first_name]."</p></li>";
			}
			else{
				$mobile_arr[]['image'] = $val[picture][data][url];
				$mobile_arr[]['first_name'] = $val[first_name];
			}
		}
		$smarty_var .= '</ul><p class="rtviewall" id="vall"><a href="javascript:void(0)" onClick="fbmutcon()">View All</a></p>';
		if($device_type == 'html'){
			echo $smarty_var;
		}
		else echo json_encode($mobile_arr);
		//$smarty->assign('smarty_mutual', $smarty_var);
	}
}
?>