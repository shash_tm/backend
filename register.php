<?php
require_once (dirname ( __FILE__ ) . '/include/config.php');
require_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . '/include/Utils.php');
require_once  dirname ( __FILE__ ) . "/email/MailFunctions.php";
//require_once dirname ( __FILE__ ) . "/logging/systemLogger.php";
include_once (dirname ( __FILE__ ) . '/include/tbconfig.php');
require_once (dirname ( __FILE__ ) . '/registration/registerDBO.php');
require_once (dirname ( __FILE__ ) . '/include/User.php');
require_once (dirname ( __FILE__ ) . '/TrustBuilder.class.php');
require_once (dirname ( __FILE__ ) . "/include/header.php");
//require_once dirname ( __FILE__ ) . "/logging/emailerLogging.php";
require_once dirname ( __FILE__ ) . "/geoIPTM.php";



class Registration {
	
	private $conn;
	private $session;
	private $user_id;
	private $first_step;
	private $user;
	private $gender;
	
	public function __construct(){
		global $conn;
		$this->conn=$conn;
		$this->session=functionClass::getUserDetailsFromSession();
		$this->user_id=$this->session['user_id'];
		$this->user=new User();
		$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	}
		
	public function takeMeToRegistration() {
		global $baseurl_https,$baseurl;
		$data = $this->user->getUserDetails($this->user_id);
		$stepsCompleted = $data ['steps_completed'];
		$steps = explode ( ",", $stepsCompleted );
		
		$psycho=false;
		$myStep = ""; 
		
		if(!in_array('basics',$steps))
		{
			$this->first_step=true;
                $myStep='basics';
		}
		 else if(in_array('basics',$steps)&&!in_array('photo',$steps))
		 $myStep='photo';
	
		 else if(in_array('photo',$steps)){
		 	$myStep = 'complete';
		 }
		 
		 
		if(in_array($myStep,array("basics","photo","psycho1","psycho2","psycho3","hobby"))){
			$this->goSomeWhere ( $myStep, $data,$psycho );
		}elseif($myStep!='complete') header("Location:".$baseurl."/logout.php");
	}
	
	private function isLoggedInFromFB($data){
			return ($data['is_fb_connected']=="Y")?true:false;
	}
	
	public function goSomeWhere($page, $data,$psycho=false) {
		global $smarty,$login_mobile,$response,$cdnurl,$registerJsonUrl,$registerSingaporeJsonUrl,$registerUSJsonUrl;
		try {

			if($login_mobile){

				$userData = $this->user->getUserDataForPrefillTheForm($this->user_id);
				if(isset($userData['location']) && $userData['location']['country']['name']=='India') {
					$response['basics_data'] = array("url"=>$registerJsonUrl);
				}else {
					$country = geoIPTM::getCountryName();
					if ($country == 'United States')
					{
						$response['basics_data'] = array("url"=>$registerUSJsonUrl);
					}
					else
					{
						$response['basics_data'] = array("url"=>$registerJsonUrl);
					}
				}
				
				$response['data'] = $userData;
				$response['page'] = $page;
				$response['first_step'] = $this->first_step;
				$response['user_id'] = $this->user_id;
				$response['current_time'] = date('Y-m-d H:i:s');
				$response['responseCode'] = 200;
			}else{
				$header = new header($this->user_id);
				$header_values = $header->getHeaderValues();
				$smarty->assign ( 'data', $data );
				$smarty->assign("first_step",$this->first_step);
				$smarty->assign('uid', $this->session['user_id']);
				$smarty->assign('isRegistration','1');
				$smarty->assign('inRegister','1');
				$smarty->assign('header',$header_values);
			}

			if($page=="basics"&&$this->isLoggedInFromFB($data)==true && !$login_mobile){
				$fbdata = $this->user->getFBBasicData($this->user_id);
				$date_of_birth = $this->user->getFBDateofBirth($fbdata['birthday'],false);
				if(isset($date_of_birth)){
						$smarty->assign ("fb_dob",$date_of_birth);
				}
				$institute = $this->user->getFBInstitute($fbdata['education']);
				if(isset($institute)){
						$smarty->assign ("institute_details",$institute);
				}
				$relationship = $this->user->getFBRelationShipStatus($fbdata['relationship']);
				if(isset($relationship)){
						$relationship = $this->user->getRelationShipStatus($relationship);
						$smarty->assign ("fb_relationship",$relationship);
				}
				$work_details = $this->user->getFBWork($fbdata['work_details']);
				if(isset($work_details)){
						$smarty->assign ("work_details",$work_details);
				}
				$location = $this->user->getFBLocation($fbdata['location']);
			
				if(isset($location)){
						$smarty->assign ("location",json_encode($location));
				}
			}
			if($page=="photo"){
				$photodata = $this->user->getUserPhotosForPrefill($this->user_id);
				$smarty->assign("photos",$photodata);
			}
			if($login_mobile){
				print_r(json_encode($response));
				die;
			}else
				/*if($page=='basics'){
					$smarty->display (dirname ( __FILE__ ) . "/templates/registration/basics_rajesh.tpl" );
				}else*/		
					$smarty->display (dirname ( __FILE__ ) . "/templates/registration/" . $page . ".tpl" );
				//}
		}
		catch(Exception $e){
			//echo $e->getMessage();
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			die;
		}
	}
	
public function saveData($page,$source, $version){
	try{
		global $conn,$baseurl,$login_mobile,$tbNumbers;
		$values = $_COOKIE;
		$values ['user_id'] = $this->user_id;
		foreach($_COOKIE as $id=>$value){
			if("TM"==substr($id,0,2)) {
				setcookie($id,"",time()-3600);
				unset($_COOKIE[$id]);
			}
		}
		setcookie("pageInd","",time()-3600);
		unset($_COOKIE["pageInd"]);
		if ($page=='basics') {
			$year = $values['TM_S1_WhenBornMonth'].'/'.$values['TM_S1_WhenBornDay'].'/'.$values['TM_S1_WhenBornYear'];
			$values ['dob'] = $values ['TM_S1_WhenBornYear'] . "-" . $values ['TM_S1_WhenBornMonth'] . "-" . $values ['TM_S1_WhenBornDay'];

			$stay_city_display="";
			if($values['TM_HaveChildren']=='')
				unset($values['TM_HaveChildren']);
			
			if(isset($values['TM_S3_StayState'])) {
				if($values ['TM_S3_StayState']==999999){
					$values ['TM_S3_StayState'] = null;
				}else{
					$stayState=explode("-",$values ['TM_S3_StayState']);
					if($stayState[1]){
						if($stayState[1]==999999){
							$stayState[1] = null;
						}
						$values ['TM_S3_StayState']=$stayState[1];
					}
					else unset($values ['TM_S3_StayState']);
				}
			}
			
			if($values ['TM_S3_StayState'] == '756') {
				$values ['TM_S3_StayCountry'] = 219;       // for singapore
			}else if($values ['TM_S3_StayState'] == '651') {
				$values ['TM_S3_StayCountry'] = 114;       // for indonesia
			}
			
			//$stayCity=explode("-",$values ['TM_S3_StayCity']);
			if(isset($values['TM_S3_StayCity'])) {
				if($values ['TM_S3_StayCity']==999999){
					$stay_city_display = null;
				}
				else{
					$stayCity=explode("-",$values ['TM_S3_StayCity']);
					if($stayCity[2]){
						if($stayCity[2]==999999){
							$stayCity[2] = null;
						}
						$stay_city_display=$stayCity[2];
					}
					else unset($values ['TM_S3_StayCity']);
				}
				$values ['TM_S3_StayCity'] = $stay_city_display;
			}
			
			if($values['TM_S3_MotherTongue']==''){
				$values['TM_S3_MotherTongue']=null;
			}
				
			$data = $this->user->getUserDetails($this->user_id);
			$this->gender = $data['gender'];
			
			/*
			 * for preference age
			 */
			if( !(($source == "iOSApp" && $version > 121) || ($source == "androidApp" && $version >= 100) ||($source == 'windows_app' && $version >1304 ) ) ) {
				$age = date_create($values['dob'])->diff(date_create('today'))->y;
				//$year = date("Y")-$values ['TM_S1_WhenBornYear'];
				$prefAge = Utils::preferenceAge($age,$this->gender);
				$values ['TM_S1_LookingAgeStart'] = $prefAge['start'];
				$values ['TM_S1_LookingAgeEnd'] = $prefAge['end'];
			}
			
		/*
		 * for preference height
		 */
			$values['height']=12*$values['TM_S1_HeightFoot']+$values['TM_S1_HeightInch'];
			if($values['height']<48) {
				$values['height'] = 48;
			}
			if($values['height']>95) {
				$values['height'] = 95;
			}
			
			$height=$values['height'];
			if($this->gender=='M') {
				$startHeight=$height-10;
				$endHeight=$height;
			}
			else if($this->gender=='F') {
				$startHeight=$height;
				$endHeight=$height+10;
			}
			if($startHeight<4*12)
			{
				$startHeight=4*12;
			}
			if($endHeight>8*12)
			{
				$endHeight=8*12;
			}
			$values['start_height']=$startHeight;
			$values['end_height']=$endHeight;
			

		if($values['TM_Working_area']=='Not Working' || $values['TM_Working_area']=='no'){
			$values['TM_Working_area']='no';
		}else{
			$values['TM_Working_area']='yes';
		}	
		
		if($values['TM_interest']==''){
			$values['TM_interest'] = null;
		}
		
		if($values['TM_Designation_0']==''){
			$values['TM_Designation_0'] = null;
		}
		else {
			$values['TM_Designation_0'] = strip_tags($values['TM_Designation_0']);
		}
		
		if($values['TM_Education_0']<=0 || $values['TM_Education_0']>21) {
			$values['TM_Education_0'] = Utils::highestDegree($values['TM_Education_0']);
		}
		
		if($values['TM_Industry_0']==''){
			$values['TM_Industry_0'] = null;
		}
		
		if($values['TM_Income_start']=='' || $values['TM_Income_end']=='') {
			$values['TM_Income_start'] = null;
			$values['TM_Income_end'] = null;
		}
		/*$values['TM_Company_name_0'];*/
		$values['TM_Company_name_0'] = $this->processData($values['TM_Company_name_0']);
		$values['TM_Institute_0'] = $this->processData($values['TM_Institute_0']);
		
		if($source == 'iOSApp' || $source == 'windows_app' || $source == 'androidApp'){
			registerDBO::insertUserBasicData($values,$source,intval($version));
		}
		else
			registerDBO::insertUserBasicData($values);
		
		$trustBuilder = new TrustBuilder($this->user_id);
		$fbstatus = $trustBuilder->checkFacebookStatus();
		if($fbstatus['status'] == 'yes'){
			$diffTMFb = $trustBuilder->checkFbTMDiff();
			if(($fbstatus['status'] == 'yes') && isset($diffTMFb) && empty($diffTMFb)){
				$trustBuilder->creditPoint($tbNumbers['credits']['facebook'],'facebook',$tbNumbers['validity']['facebook']);
				$trustBuilder->updateFacebookTrustScore($this->user->getFbConnectionCount($this->user_id));
			}else if(isset($diffTMFb) && !empty($diffTMFb)){
				if($diffTMFb['name'] && $diffTMFb['age']){
					$trustBuilder->mismatchReason($this->user_id, "Name and Age mismatch");
				}else if($diffTMFb['name']){
					$trustBuilder->mismatchReason($this->user_id, "Name mismatch");
				}else if($diffTMFb['age']){
					$trustBuilder->mismatchReason($this->user_id, "Age mismatch");
				}else if($diffTMFb['gender']){
					$trustBuilder->mismatchReason($this->user_id, "Gender mismatch");
				}
			}
		}
		
			if($source == 'iOSApp' || $source == 'windows_app' || ($source == 'androidApp' && $version >= 100) ) {
				$trustBuilder->authenticateUser();
			}
		}
		if($page=='photo') {
			registerDBO::updateUserPhoto($values['user_id']);
			$trust=new TrustBuilder($this->session['user_id']);
			$trust->authenticateUser();
		}
	
		if($login_mobile){
			print_r(json_encode(array("responseCode"=>200)));die;
		}
	}catch (Exception $e){
		//echo $e->getMessage();
		trigger_error("PHP Web:".$this->user_id." ".$e->getTraceAsString(), E_USER_WARNING);
	}
	
}

private function processData($data){
	$tmp = array();
	$ds = array();
	$data = json_decode($data,true);
	if(isset($data)) {
		foreach($data as $id=>$value){
			$data2 = explode(" ",$value);
			foreach($data2 as $key=>$val){
				$tmp[$key] = ucfirst(trim(strip_tags($val)));
			}
			$ds[$id] = implode(" ",$tmp);
			unset($tmp);
		}
	}
	$data = json_encode($ds);
	return $data;
}

// private function welcomeMail(){
// 	global $conn, $smarty;
// 	try{
// 		$data = $this->user->getUserDetails($this->user_id);
// 		if(isset($data['company'])){
// 			$trust->creditPoint(1100,"corporate");
// 		}
	
// 		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=complete-registration";
// 		$redirectionFile = "/matches.php";
// 		$utm_content="view_complete-registration";
// 		$campaignName = "complete-registration";
		
// 		//passing email id to generate unsubscribe link
// 		$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $data['email_id'], $this->user_id);
// 		$smarty->assign("analyticsLinks", $analyticsLinks);
		
// 		$sysLog = new SystemLogger();
// 		$log['email_id'] = $data['email_id'];
// 	/*	if($login_mobile){
// 			print_r(json_encode($response['status']="success"));die;
// 		}*/
// 		$log['subject'] = "complete-registration";
// 		$log['user_id'] = $this->user_id;
// 		$toAddress = array($data['email_id']);
// 		$smarty->assign('name', $data['fname']);
// 		$mailObject = new MailFunctions();
		
// 		$mailObject->sendMail($toAddress,$smarty->fetch('templates/utilities/newmailers/emailWelcome.tpl'),"","Hurray! Your registration is complete.", $this->user_id);
// 		$sysLog->logSystemMail($log);
// 	}
// 	catch(Exception $e){
// 		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
// 	}
// }

}


$func = new functionClass();
$response = null;
$login_mobile = $func->isMobileLogin();
$source = $func->getSourceFromHeader();
$version = $func->getHeader();

if(isset($_REQUEST ['save']) && $login_mobile) {
	functionClass::redirect ('register_save',$login_mobile);
}else {
	functionClass::redirect ('register',$login_mobile);
}
$register = new Registration ();

if ($_REQUEST ['save']) {
	$toSave = $_REQUEST ['save'];
	if (in_array ( $toSave, array (
			"basics",
			"photo"
	) )) {
		try {
			$register->saveData( $toSave,$source, $version );
		} catch ( Exception $e ) {
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
			header ( "Location:" . $baseurl . "/register.php" );
			die ();
		}
	} else {
		die ( 'Invalid Page entered' );
	}
	die ();
}
$register->takeMeToRegistration ();

?>
