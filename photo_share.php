<?php 
include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
include_once (dirname ( __FILE__ ) . '/photomodule/photoUtils.php');
include_once (dirname ( __FILE__ ) . '/DBO/photoShareDBO.php');

try 
{
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user['user_id'];
	if (isset($user_id))
	{
		if($_POST['action'] == 'upload_image' && isset($_FILES['file']) && isset($_POST['match_id']))
		{
			$data = array('responseCode' =>  200 );
			$image = PhotoUtils::saveImageForMsgs($_FILES['file'], 90, $_POST['rotate'] );
			
			
			if($image['error'] == false)
			{
				photoShareDBO::saveSharedPhoto($user_id, $_POST['match_id'], $image['jpeg'] , $image['thumbnail_jpeg'] ) ;
				$image['jpeg'] = $imageurl . $image['jpeg'];
				$image['webp'] = $imageurl . $image['webp'];
				$image['thumbnail_jpeg'] = $imageurl . $image['thumbnail_jpeg'];
				$image['thumnnail_webp'] = $imageurl . $image['thumnnail_webp'];
				$data['urls'] = $image ;
				//$data['urls']['jpg_image'] =  $imageurl . $image['image'] ;
				//$data['urls']['webp_image'] = $imageurl . $image['webp'] ;
				//$data['urls']['thumbnail'] = $imageurl . $image['thumbnail'] ;
			}
			else 
			{
				$data['error'] = $image['error'];
			}
			
		}
		else 
		{
			$data = array('responseCode' =>  403 ,
					      'error' => "unknownRequest");
		}
	}
	else 
	{
		$data = array('responseCode' =>  401 );
	}
	
	print_r(json_encode($data));
} catch (Exception $e) {
	trigger_error("PHP photo upload:: ".$e->getMessage(),E_USER_WARNING) ;
	echo $e->getMessage();
}




?>