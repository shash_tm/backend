<?php 

include_once dirname(__FILE__)."/include/config.php";
include_once dirname(__FILE__)."/include/function.php";
//include_once (dirname ( __FILE__ ) . '/EventTracking.php');

//session_start();

$functionClass = new functionClass();

if(isset($_SESSION['user_id'])){
// 	$eventTrack = new EventTracking($_SESSION['user_id']);
// 	$eventTrack->logAction("logout");
	
	session_unset();
	$params = session_get_cookie_params();
	session_destroy();
}


// unset cookies
if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-1000);
        setcookie($name, '', time()-1000, '/');
    }
}

$login_mobile = $functionClass->isMobileLogin();
if($login_mobile) {
	$response["status"] = '200';
	$response["responseCode"] = 200;
	print_r(json_encode($response));
}
else {
	header("Location:".$baseurl."/index.php");
}
// $device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
// if($device_type=='html'){
// 	header("Location:".$baseurl."/index.php");
// }else{
// 	/* global $conn;
// 	if(isset($_REQUEST['registration_id']) && $_REQUEST['registration_id']!=''){
// 		$sql = $conn->Prepare("DELETE from user_mobile_gcm_id where registration_id=?");
// 		$ex = $conn->Execute($sql, $_REQUEST['registration_id']);
// 	} */ 
// 		$response["status"] = '200';
// 		$response["responseCode"] = 200;
// 		print_r(json_encode($response));
// }
?>
