<?php

require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/include/function.php";




/*
 * all the functions related to google maps api
 * @Himanshu
 */
class googleMaps{

	private $longitude;
	private $latitude;
	private $conn;

	function __construct(){
		global $conn;
		$this->conn = $conn;
	}

	/**
	 * get the information from tables and feed that to geocode api
	 * get the details to be fed to google maps api for location
	 */
	public function getMapDetails($userId, $type){
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );



			
		if($type == 'birth'){
			$query = "SELECT  gct.name as city, gstate.name as state , gcountry.name as country, gct.latitude, gct.longitude, gct.id
						   FROM user_demography ud 
						   LEFT JOIN geo_state gstate on ud.birth_state = gstate.state_id
						   LEFT JOIN geo_country gcountry on ud.birth_country = gcountry.country_id 
						   LEFT JOIN geo_city gct on ud.birth_city_display = gct.id 
						   where 
							  ud.user_id = " .$userId ;
		}
		if($type=='stay'){
			$query = "SELECT gci.name as city, gco.name as country, gs.name as state, gci.longitude, gci.latitude, gci.id
						   FROM user_demography ud 
						   LEFT JOIN geo_country gco on ud.stay_country = gco.country_id
						   LEFT JOIN geo_state gs on ud.stay_state = gs.state_id
   						   LEFT JOIN geo_city gci on ud.stay_city_display = gci.id 
						   where   ud.user_id = " .$userId ;
		}
		/*	$query = "select gcn.name as country,gs.name as state, gcy.name as city, udd.pincode as pincode
		 from user_demography udd
		 left join geo_country gcn on udd.stay_country = gcn.country_id
		 left join geo_state gs on udd.stay_state = gs.state_id
		 left join geo_city gcy on udd.stay_city = gcy.city_id
		 where
		 udd.user_id = " .$userId ;*/
			
			

		//var_dump($query);
		$execQuery  = $this->conn->Execute($query);
		$location = $execQuery->FetchRow();
		//var_dump($location); die;
			
		$city = $location['city'];
		$state = $location['state'];

		/*if((($type == 'stay')&&($location['stay_latitude']==null)&&($location['stay_longitude']==null))
		||(($type == 'birth')&&($location['birth_latitude']==null)&&($location['birth_longitude']==null))){*/
		if(($location['latitude']==null)&&($location['longitude']==null)){
		
			if($location['city'] == 'East Delhi' || $location['city'] == 'West Delhi') $location['city'] = 'North Delhi';
			if($location['state'] == 'Delhi / NCR') $location['state'] = 'Delhi';
			$urlForInfo1 = "maps.googleapis.com/maps/api/geocode/json?address=";
			$urlForInfo2 = (($location['country']!=null) ? $location['country']. "+":'')
			.(($location['state']!=null) ? $location['state']. "+":'')
			.(($location['city']!=null) ? $location['city']:'')
			.(($location['pincode']!=null) ? ("&postal_code=".$location['pincode']) : '');
			$urlForInfo3= "&sensor=true";

			$urlForInfo2 = rawurlencode($urlForInfo2);
			$urlForInfo = $urlForInfo1 . $urlForInfo2 . $urlForInfo3;
			//	echo $urlForInfo;die;
			//var_dump(rawurlencode($urlForInfo));
			//echo '<pre>';
			$response = Utils::curlCall($urlForInfo);
			//print_r($response);
			$geoResults = json_decode($response,true);
			/*	echo '<pre>';
			 var_dump($geoResults['results']); die;*/
			//TODO: write JSON parser as below values might differ
			//var_dump($geoResults['results'][1]['geometry']['location']);

			/*foreach ($geoResults as $key=>$val){
			 echo '<br><br>';
			 var_dump($key);
			 echo '<br>';
			 foreach ($val as $temp=>$m){
			 echo '<br><br>';

			 var_dump($temp);
			 var_dump($m);
			 }
			 }*/

			//var_dump($geoResults['results'][2]);
			//var_dump($geoResults['results']['location']);
			$flag = 0;
			$location['city'] = $city;
			$location['state'] = $state;
			foreach ($geoResults['results'] as $key =>$val){
				foreach (($val['address_components']) as $row=>$col){
					//var_dump(($col['long_name']));
					if(strcmp($col['long_name'],$city) == 0){
						//	echo $col['long_name'];
						//echo $city;
						$data['lat'] =  $val['geometry']['location']['lat'];
						$data['lng'] = $val['geometry']['location']['lng'];
						$flag = 1;
						break;
					}
					/*for($i=0; $i<=count($val['address_components']); $i++){
					 echo PHP_EOL;
					 echo $val['address_components'][$i]['long_name'];
					 echo $city;
					 if(strcasecmp($val['address_components'][$i]['long_name'],$city)){
						$data['lat'] = $geoResults['results'][$key]['geometry']['location']['lat'];
						$data['lng'] = $geoResults['results'][$key]['geometry']['location']['lng'];
						$flag = 1;
						break;
						}

						}
						if($flag == 1 ) break;*/
				}
				if($flag == 1 ) break;
					
			}

			if(!isset($data['lat'])){
				$data['lat'] = $geoResults['results'][0]['geometry']['location']['lat'];
				$data['lng'] = $geoResults['results'][0]['geometry']['location']['lng'];
			}
			/* else{
			 $data['lat'] = $geoResults['results'][0]['geometry']['location']['lat'];
			 $data['lng'] = $geoResults['results'][0]['geometry']['location']['lng'];
			 }*/
			
		/*	if ($type == 'birth'){
				$tlat = 'birth_latitude';
				$tlng = 'birth_longitude';
			}
			else {
				$tlat = 'stay_latitude';
				$tlng = 'stay_longitude';
			}*/
		/*	echo $type;
			echo $data['lat'];
			echo $data['lng'];*/
			try{
			//$sql = $this->conn->Prepare("UPDATE user_demography SET $tlat = ? , $tlng = ? WHERE user_id = ?");
				$sql = $this->conn->Prepare("UPDATE geo_city SET latitude = ? , longitude = ? where id =?");
				$this->conn->Execute($sql, array($data['lat'], $data['lng'],$location['id'] ));
			}
			catch (Exception $e)
			{
				echo $e->getMessage();
			}
		}

		else{
			/*if($type == 'stay'){
			$data['lat'] = $location['stay_latitude'];
			$data['lng'] = $location['stay_longitude'];
			}
			if($type == 'birth'){
				$data['lat'] = $location['birth_latitude'];
			$data['lng'] = $location['birth_longitude'];
			}*/
			$data['lat'] = $location['latitude'];
			$data['lng'] = $location['longitude'];
		}
		
/*		echo $type;
			echo $data['lat'];
			echo PHP_EOL;
			echo $data['lng'];die;*/
		$data['country'] = $location['country'];
		$data['state'] = $location['state'];
		$data['city'] = $location['city'];

		return $data;

		/*
		 $this->latitude = $geoResults['results']['location']['lat'];
		 $this->longitude = $geoResults['results']['location']['lng'];*/
	}

}


$gmap = new googleMaps();
//$userId = 6000153;

$userId = $_REQUEST['pid'];
//echo $userId;
$type = $_REQUEST['type'];
//$type = "stay";
//$userId = 6000164;
$data = $gmap->getMapDetails($userId, $type);
//var_dump($data);die;
global $smarty;
$smarty->assign ( "lat", $data['lat'] );
$smarty->assign ( "lng", $data['lng'] );
$smarty->assign ( "country", $data['country'] );
$smarty->assign ( "state", $data['state'] );
$smarty->display ("templates/utilities/google_maps.tpl" );

//$smarty->display ("templates/utilities/google_static_maps.html" );
?>