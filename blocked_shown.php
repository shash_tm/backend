<?php
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";


class BlockedShown {
	
	private $user_id;
	private $conn;
	
	public function __construct($user_id){
		global $conn;
		$this->user_id=$user_id;
		$this->conn=$conn;
	}
	
	public function markBlocked($uid){
		if($uid>$this->user_id){
			$user1=$this->user_id;
			$user2=$uid;
		}
		else{
			$user1=$uid;
			$user2=$this->user_id;
		}
		$sql=$this->conn->Prepare("update user_mutual_like set blocked_shown=true where user1=? and user2=?
				and blocked_by=?");
		
		//var_dump($this->user_id);
		$rs=$this->conn->Execute($sql,array($user1,$user2,$uid));	

		$sql2 = $this->conn->Prepare("UPDATE messages_queue SET is_blocked_shown = 1  where ((sender_id = ? AND receiver_id =?) OR (sender_id=? AND receiver_id=?)) ");
		$this->conn->Execute($sql2, array($user1, $user2, $user2, $user1));
	}
	
	public function markSuspendedShown($uid){
		if($uid>$this->user_id){
			$user1=$this->user_id;
			$user2=$uid;
		}
		else{
			$user1=$uid;
			$user2=$this->user_id;
		}
		$sql=$this->conn->Prepare("update user_mutual_like set suspended_shown=true where user1=? and user2=?");
	
		//var_dump($this->user_id);
		$rs=$this->conn->Execute($sql,array($user1,$user2));
	}
	
}


try {
	//get user details from session should return if user is valid or not
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	//$user=new UserData($user_id);
	//here we shold check if we have to proceed ahead
	//we should check here is it is valid
	$BlockedShown=new BlockedShown($user_id);
	$uid=intval($_REQUEST['uid']);
	if($uid==0)return;

	if($_REQUEST['bs']=="1"){
		$BlockedShown->markBlocked($uid);
	}

	if($_REQUEST['ss']=="1"){
		$BlockedShown->markSuspendedShown($uid);
	}
}
catch(Exception $e){
	//echo $e->getMessage();
	trigger_error("php_script_error:".$e->getTraceAsString(), E_USER_WARNING);
}
