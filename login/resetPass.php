<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . '/../include/validate.php';
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/User.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";




/**
 * 1.check the checksum and genrate a link
 * send email
 * 2. if link is clicked, if checksum is matched
 * extract parameters and update the db
 *
 * @Himanshu
 */

class ResetPassword{

	private $email;
	private $conn;
	private $user_id;
	private $smarty;
	private $baseurl;
	private $user;
	private $redis;
	private $userData;

	function __construct($email){
		global $conn, $baseurl, $smarty, $redis;
		$this->email = $email;
		$this->conn= $conn;
		$this->baseurl = $baseurl;
		$this->smarty = $smarty;
		$this->redis = $redis;
		$user = new User();
		$this->user = $user;
	}

	private function setEmailAttemptKey($ipAddress){
		$key="fp:$ipAddress";
		$val=$this->redis->incr($key);
		$this->redis->EXPIRE($key,1800);
	}

	private function getEmailAttemptCount($ipAdress){
		$key = "fp:$ipAdress";
		$val=$this->redis->get($key);
		return $val;
	}

	private function generateChecksum(){

		$row = $this->user->getUserDetailsOnEmailBasis($this->email);
		$this->userData = $row;
		$this->user_id = $row['user_id'];
		$checksum = md5($row['user_id'] . '|' . Utils::$salt . '|' . $row['registered_at']);
		return $checksum;
	}

	private function generateLink(){
		$checksum = $this->generateChecksum();
		$link = $this->baseurl . "/login/resetPass.php?status=reset&m=$checksum&email=$this->email";
		return $link;
	}

	public function sendEmail(){
		$ipAddress = $_SERVER['REMOTE_ADDR'] ;
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
		}
		$count = $this->getEmailAttemptCount($ipAddress);

		if($count < 3){
			$this->setEmailAttemptKey($ipAddress);

			/*to check if a user exists in the user table with this email */
			$isValid = $this->user->canSendMail($this->email, "bounce","suspended");
			
			if($isValid>0){ 
				$reset_link = $this->generateLink();
					
				$this->smarty->assign("name", ucfirst(htmlentities($this->userData['fname'])));
				$this->smarty->assign("reset_link", $reset_link);
				
				$mailObject = new MailFunctions();
				
				$mid= $mailObject->sendMail(array($this->email) ,$this->smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/emailReset.tpl'),"","Forgotten Password for TrulyMadly.com", $this->user_id);
			// Following is used on dev and T1
			//	$mid= Utils::sendEmail("tarun@trulymadly.com" , "admintm@trulymadly.com", "YO",$this->smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/emailReset.tpl'),true);

			//	if(isset($mid))

			}else {
				throw new Exception("Reset password instructions has been sent to your email address that is registered with TrulyMadly.");
			}
		}else{
			throw new Exception("Too many attempts.");
		}
	}

	private function verifyChecksum($checksum){
		$post = $this->generateChecksum();
		if($post == $checksum){
			return true;
		}
		return false;
	}
	public function linkClicked($checksum){
		$check = $this->verifyChecksum($checksum);
		if($check){
			$this->smarty->display(dirname ( __FILE__ ). '/../templates/utilities/resetPassword.tpl');
		}
	}

	public function updatePassword($pass, $checksum ){

		$check = $this->verifyChecksum($checksum);
		if($check){
			$row = $this->user->getUserDetailsOnEmailBasis($this->email,"unsubscribe");
			if(isset($row['user_id'])){
				
				$query = "INSERT ignore into user_password_log values(?,?, now()) ";
				$param_array=array($row['user_id'], $row['password']);
				$tablename='user_password_log';
				Query_Wrapper::INSERT($query, $param_array, $tablename);
				
				/*$sql3 = "INSERT ignore into user_password_log values(?,?, now()) ";
				$this->conn->Execute($sql3, array($row['user_id'], $row['password']));*/
				
				
				$query = "UPDATE user set password = ? where user_id = ?";
				$param_array= array(md5($pass), $row['user_id']);
				$tablename='user';
				Query_Wrapper::UPDATE($query, $param_array, $tablename);

				global $redis;
				$ipAddress = $_SERVER['REMOTE_ADDR'] ;
				if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
					$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
				}
				$redis_key = "login_log_".$ipAddress."_".$this->email;
				$redis->del($redis_key);
				/*$updateUserTableQuery = $this->conn->Prepare("UPDATE user set password = ? where user_id = ?");
				$this->conn->Execute($updateUserTableQuery, array(md5($pass), $row['user_id']));*/
				return "updated";
			}
		}
	}
}
	
	try{
		$email = $_REQUEST['email'];
		$resPass = new ResetPassword($email);
		if(isset($_REQUEST['status']) && $_REQUEST['status']=='reset' && !isset($_REQUEST['password'])){
			$resPass->linkClicked($_REQUEST['m']);
		}
		else if(isset($_REQUEST['status']) && $_REQUEST['status']=='reset' && isset($_REQUEST['password'])){
			$newPass = $_REQUEST['password'];
			$rePass = $_REQUEST['passwordAgain'];
			if(strcmp($rePass, $newPass) == 0){
				$object=new Validation();
				$validation =$object->validateText($newPass);
				if(!$validation)
				$stat = $resPass->updatePassword($newPass, $_REQUEST['m']);
				else{
					$stat =  "Password Error";
				}
				if($stat == "updated"){
					$fun = new functionClass();
					$fun->setSession(null, $email);
					//var_dump($_SESSION);
					header("Location:".$baseurl."/matches.php");
					//$fun->redirect ( 'dashboard' );
				}
			}
			else{
				$smarty->assign("stat", "Password Mismatch");
				$smarty->display(dirname ( __FILE__ ). '/../templates/utilities/resetPassword.tpl');
			}
		}
		else{ 
			$resPass->sendEmail();
			$result['status'] = "OK";
			$result['responseCode'] = 200;
			print_r(json_encode($result));
		}
	}
	catch(Exception $e){
		//echo $e->getMessage();
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		$result['status']="error";
		$result['responseCode'] = 403;
		$result['error'] = $e->getMessage();//"Not a valid Email address";
		print_r(json_encode($result));
	}

	?>
