<?php 
require_once (dirname ( __FILE__ ) . '/sdk/facebook.php');
require_once dirname ( __FILE__ ) . "/include/function.php";

class testFb {
	
	private $token = '', $facebook = '';
	private $limit = 5000; 
	private $afterId = "";
	private $friends = '';
	public $data = '';
	private $profile_new = ''; 
	
	function __construct($token) {
		global $config;
		$api = $config ['facebook'] ['api'];
		$secret = $config ['facebook'] ['secret'];
		$configFb = array ();
		$configFb ['appId'] = $api;
		$configFb ['secret'] = $secret;
		$this->facebook = new Facebook ( $configFb );
		$this->token = $token;
		$this->facebook->setAccessToken ( $this->token );
	}
	
	public function getData() {
		$profile = $this->facebook->api ('/me?fields=friends.fields(picture,first_name,last_name,id,email,gender).limit('.$this->limit.'),likes.limit('.$this->limit.'),albums.fields(id,name).limit('.$this->limit.'),email,first_name,last_name,gender,birthday,relationship_status,music.listens.limit(100),music.playlists.limit(100),books.quotes.limit(100),books.rates.limit(100),books.wants_to_read.limit(100),books.reads.limit(100),places.saves.limit(100),video.wants_to_watch.limit(100),video.rates.limit(100),video.watches.limit(100),education,work,location', 'GET' );
		
		if(isset($profile['error_code'])) {
			$this->limit = $this->limit-2000;
			$this->getFriendsData();
			$this->getOthersData();
		}else { 
			$this->profile_new = $profile;
		}
		
		$this->profile_new['connections']=$this->getConnection($this->profile_new);
		var_dump($this->profile_new);
	}
	
	public function getFriendsData($after = false) {
		if($after) {
			echo " next page ";
			
			$request = '/me/friends?fields=picture,first_name,last_name,id,email,gender&limit=';
			$request = $request.$this->limit;
			$request = $request.'&offset='.$this->limit.'&__after_id=';
			$request = $request.$this->afterId;
			
			$profile = $this->facebook->api ($request,'GET');
			
			$this->profile_new['friends']['data'] = array_merge($this->profile_new['friends']['data'],$profile['data']);
			
			if(isset($profile['paging']['next'])) {
				$after_id = explode('__after_id=',$profile['paging']['next']) ;
				$this->afterId = $after_id[1];
				self::getFriendsData(true); 
			}
		}else {
			echo "first page";
			
			$profile = $this->facebook->api ('/me?fields=friends.fields(picture,first_name,last_name,id,email,gender).limit('.$this->limit.')','GET');
			
			$this->profile_new = $profile;
			
			if(isset($profile['friends']['paging']['next'])) {
				$after_id = explode('__after_id=',$profile['friends']['paging']['next']) ;
				$this->afterId = $after_id[1];
				self::getFriendsData(true);
			}
			
		}
		
	}
	
	public function getOthersData() {
		$profile = $this->facebook->api ('/me?fields=likes.limit('.$this->limit.'),albums.fields(id,name).limit('.$this->limit.'),email,first_name,last_name,gender,birthday,relationship_status,music.listens.limit(100),music.playlists.limit(100),books.quotes.limit(100),books.rates.limit(100),books.wants_to_read.limit(100),books.reads.limit(100),places.saves.limit(100),video.wants_to_watch.limit(100),video.rates.limit(100),video.watches.limit(100),education,work,location', 'GET' );
		$this->profile_new = array_merge($profile,$this->profile_new);
	}
	
	public function getConnection($data){
		$friends = $data['friends'];
		$total = count($friends['data']);
		if($total<50) {
			$total = $this->getFBConnection();
		}
		return $total;
	}
	
	public function getFBConnection() {
		$profile = $this->facebook->api ('/v2.2/me?fields=friends', 'GET' );
		$friends = $profile['friends'];
		return $friends['summary']['total_count'];
	}
	
}

try {
	ini_set('memory_limit', '512M');
	set_time_limit(300);
	$fb = new testFb($_REQUEST['token']);
	$fb->getData();
	
}catch(Exception $e) {
	echo $e->getMessage();
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	die;
}
?>