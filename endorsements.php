<?php
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/include/tbconfig.php";
require_once dirname ( __FILE__ ) . "/TrustBuilder.class.php";
require_once dirname ( __FILE__ ) . "/scripts/endorsementMailer.php";
require_once dirname ( __FILE__ ) . "/mobile_utilities/pushNotification.php";



/*
 * in case logging is needed -
 *
 * require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";
 * $eventTrk = new EventTrackingClass();
 $eventTrk->logActions($data);
 */

/**
 * API wrapper over profile.php to show endorsement UI
 * @Himanshu
 */
try{

	$user_id = $_REQUEST['id'];
	$tstamp = $_REQUEST['ts'];
	$checksum = $_REQUEST['cs'];
	$gender = $_REQUEST['g'];
	$output = array();
	$uu = new UserUtils();

	if(isset($user_id)&&isset($checksum)&&isset($tstamp)){
		$postCheckSum = $uu->endorsementHash($user_id, $tstamp,$gender);
		if($postCheckSum == $checksum){
			if(isset($_REQUEST['endorse_response'])){
				$data = $_REQUEST['endorse_response'];
				//$data = json_decode($response, true);

				$data['user_id'] = $user_id;
				if($data['is_endorsed'] == 'yes'){
					//get the gender - if male then restrict
					$uuDBO = new userUtilsDBO();
				//	if($gender =="M" && $data['gender'] == "M")$data['is_endorsed'] = "no";
					$dataToVerify = array("email_id" => $data['email_id'], "fid" => $data['fid']);
					$dataAgainstWhichToBeVerified = array("user_id" => $user_id);
					//check if user is self endorsing
					if($uu->isSelfEndorsing($dataToVerify,$dataAgainstWhichToBeVerified) == false){
						$isAffectedRows = $uuDBO->setEndorsementsDetailsFromFb($data, true);
						$endorsementData = $uuDBO->getEndorsementData(array($user_id));
						$endorsementCount = count($endorsementData[$user_id]);
						//if the total count for endorsements is 2 then update the trust score
						//if($endorsementCount ==  EndorsementConstants::MinimumEndorsementCount){
						//update trust table if is_endorsed is  yes
						if($isAffectedRows == true  && $data['is_endorsed'] == "yes"){
							$trustbuilder = new TrustBuilder($user_id);
							$trustbuilder->updateEndorsementScore();
							//check if a user can be made authentic since for female we allow 10% trust score authentication as well
							$trustbuilder->authenticateUser();
								
							//send Mailer for endorsement received
							$emailerObj = new endorsementMailer($user_id);
							$emailerObj->sendMailer($endorsementCount, $data);

							$push = new pushNotification() ;
							$data= $data['fname'] . " has got your back. You just received a reference!";
							$push_array = array("content_text"=>$data,"ticker_text"=>"Message From TrulyMadly",
								"title_text"=>"TrulyMadly","push_type"=>"TRUST") ;


						    $push->notify($user_id, $push_array,"ENDORSEMENT");
						}
						//}
						$output['responseCode'] = 200;
						if($isAffectedRows == false)$output['has_already_endorsed'] = true;

						echo json_encode($output);
						
						
						
					}
					else{
						$output['responseCode'] = 200;
						$output['message'] = "You can't endorse yourself";
						echo json_encode($output);
					}
				}else{
					//log "no" response
				}

			}else if(isset($_REQUEST['nothanks']) && $_REQUEST['nothanks'] == 1) {
				$reason = $_REQUEST['reason'];
				/*var_dump($user_id); 
				var_dump($reason);die;*/
				$uu->logEndorsementNoThanksResponse($user_id, $reason);
			}
			else{
				global $dummy_male_image, $dummy_female_image;
				$endorsementFlag = 1;

				//show the profile page here
				$attributes =  $uu->getAttribute('profile', $user_id, array($user_id), $from_match=false, $isAuthentic=false, $myProfile=true, $login_mobile=false, $mutualLikeFlag = false, $likedIds = null, $setFlag = null);
				
				if(strpos( $attributes[$user_id]['profile_pic'], "dummy") !== false){
					$attributes[$user_id]['profile_pic'] = ($gender == "M")? $dummy_male_image: $dummy_female_image;
				}
				$smarty->assign("attributes",$attributes[$user_id]);
				$smarty->assign("gender", $gender);
				/*echo '<pre>';
				 var_dump($attributes[$user_id]); die;*/

			/*	if($gender == "F")
				$endorse_msg = "Hey! Recommend me on TrulyMadly. Help me build my trust score :)";//Your friend has picked you to endorse him on TrulyMadly. Click here to endorse him:";
				else
				$endorse_msg = "Hey! Recommend me on TrulyMadly. The girls will trust me more:-)";//Your friend has picked you to endorse her on TrulyMadly. Click here to endorse her:";
				*/	
				$endorse_msg = "Hey! can you certify my credentials on TrulyMadly please :)";
				$smarty->assign("endorse_msg", $endorse_msg);
				$smarty->display(dirname ( __FILE__ ).'/templates/endorsement.tpl');
			}
		}
	}
	else{/*
		$output['responseCode'] = 400;
		echo json_encode($output);*/
		header("Location:index.php");
	}
}catch (Exception $e){
	//	echo $e->getMessage();
	trigger_error( "PHP WEB: ". $e->getMessage(), E_USER_WARNING) ;
}