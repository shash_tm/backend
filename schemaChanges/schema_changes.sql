/* facebook likes schema changes */

CREATE TABLE `facebook_pages` (
  `page_id` varchar(255) NOT NULL,
  `name` varchar(1023) DEFAULT NULL,
  `genre` varchar(1023) DEFAULT NULL,
  `tm_category` varchar(127) DEFAULT NULL,
  `likes_count` int(11) DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/* SATISH TODO: is UNIQUE KEY `zone` (`zone`,`page_id`) required */
CREATE TABLE `facebook_top_pages` (
  `facebook_top_page_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zone` int(11) DEFAULT NULL,
  `tm_category` varchar(127) DEFAULT NULL,
  `page_id` varchar(255) DEFAULT NULL,
  `likes_count` int(11) DEFAULT '0',
  PRIMARY KEY (`facebook_top_page_id`),
  UNIQUE KEY `zone` (`zone`,`page_id`),
  KEY `zone_id` (`zone`),
  KEY `tm_category` (`tm_category`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `zones` (
  `zone_id` int(11) NOT NULL,
  `name` varchar(511) DEFAULT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/* SATISH TODO: default zone should it be 0 or 1 (delhi or overall)? */
alter table `geo_state` add column `zone_id` int(11) default 1;

/* NOTE: This is temporory table used to get top facebook pages */
CREATE TABLE `user_facebook_likes` (
  `user_facebook_like_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `page_id` varchar(255) DEFAULT NULL,
  `category` varchar(127) DEFAULT NULL,
  `page` varchar(511) DEFAULT NULL,
  `zone` int(11) DEFAULT '1',
  `gender` enum('M','F') DEFAULT NULL,
  PRIMARY KEY (`user_facebook_like_id`),
  UNIQUE KEY `up` (`user_id`,`page_id`),
  KEY `user_id` (`user_id`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1



