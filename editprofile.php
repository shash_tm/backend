<?php 
require_once (dirname ( __FILE__ ) . '/include/config.php');
require_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . "/include/header.php");

try{
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	$user = functionClass::getUserDetailsFromSession();
	functionClass::redirect ( 'editprofile',$login_mobile );
	$user_id = $user['user_id'];
	$status = functionClass::getUserStatus($user_id);
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute($conn->prepare("select * from user_data where user_id=?"),array($user_id));
	$sql1 = $conn->Execute($conn->prepare("select fname,lname,preferences as hashtags from user LEFT JOIN interest_hobbies on user.user_id=interest_hobbies.user_id where user.user_id=?"),array($user_id));
	$data = $sql->FetchRow();
	$data['company_name'] = json_decode($data['company_name'],true);
	
	$tmpCmp = array();
	$i=0;
	if(count($data['company_name'])>0) {
		foreach ($data['company_name'] as $id=>$value) {
			if(isset($value)) {
				$tmpCmp[$i] = $value;
				$i++;
			}
		}
	}
	
	$i=0;
	$data['company_name'] = $tmpCmp;
	$data['institute_details'] = json_decode($data['institute_details'],true);
	$tmpIns = array();
	if(count($data['institute_details'])>0) {
		foreach ($data['institute_details'] as $id=>$value) {
			if(isset($value)) {
				$tmpIns[$i] = $value;
				$i++;
			}
		}
	}
	$i=0;
	$data['institute_details'] = $tmpIns;
	$data['marital_status_new'] = $data['marital_status'];
	if($data['marital_status']=='Married Before') {
		$data['marital_status'] = 'Separated';
	}
	
	if(!$login_mobile){
		$cmp = array();
		$inst = array();
		$data['fname'] = htmlentities($data['fname']);
		$data['lname'] = htmlentities($data['lname']);
		$data['designation'] = htmlentities($data['designation']);
		$data['designation'] = rawurlencode($data['designation']);
		foreach($data['company_name'] as $id=>$value){
			$cmp[$id] = htmlentities($value);
			$cmp[$id] = rawurlencode($cmp[$id]);
		}$data['company_name'] = $cmp;
		foreach($data['institute_details'] as $id=>$value){
			$inst[$id] = htmlentities($value);
			$inst[$id] = rawurlencode($inst[$id]);
		}$data['institute_details'] = $inst;
	}
	
	$data['height_foot'] = intVal($data['height']/12);
	$data['height_inch'] = intVal($data['height']%12);
	$data1 = $sql1->FetchRow();
	$data1['hashtags'] = json_decode($data1['hashtags'],true);
	
	$data= array_merge($data,$data1);
	
	if($login_mobile){
		$response["responseCode"] = 200;
		$response['status'] = $status;
		$response["user_data"] = $data;
		if($data['stay_country'] == '254')
		{
			$response['basics_data'] = array("url"=>$registerUSJsonUrl);
		}
		else
			$response['basics_data'] = array("url"=>$registerJsonUrl);//$cdnurl."/register_data.json?ver=10.3");
		
			
			$resource=$response;
			unset($resource['responseCode']);
			unset($resource['status']);
			$resource=json_encode($resource);
			$hashCheck=functionClass::checkHash($resource,$_REQUEST['hash']);
				
			//var_dump($hashCheck);
				
			if($hashCheck['status']==false)
				$response['hash']=$hashCheck['hash'];
			else
				$response=array("responseCode"=>304);
						
			$response["tstamp"]=time();
			print_r(json_encode($response));
		die;
	}else{
		$header = new header($user_id);
		$header_values = $header->getHeaderValues();
		
		$smarty->assign('status',$status);
		$smarty->assign('header',$header_values);
		$smarty->assign ('edit', true );
		$smarty->assign ('user_data', json_encode($data,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) );
		$smarty->display (dirname ( __FILE__ ) . "/templates/editprofile.tpl" );
	}
	
}catch(Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}

?>
