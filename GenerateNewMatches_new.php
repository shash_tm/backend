<?php

require_once dirname ( __FILE__ ) . "/matches/matchScore.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/fb.php";
//require_once dirname ( __FILE__ ) . "/UserActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
//require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/abstraction/query.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/include/matchConstants.php";
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/UserData.php";



/**
 * Generate new matches API
 * @author himanshu
 */


class generateNewMatches {


	private $userId;
	private $recommendation_number ;
	private $key;
	private $cronRecommendationLimit = 100;
	private $timeForMatchesExpiry = 10800; // 3hrs
	private $timeForMatchCountExpiry ;
	private $matchCountKey;
	private $uuDBO;
	private $uu;
	private $redis;

	function __construct($user_id) {
		global $redis;
		//	parent::__construct ( $user_id );
		$this->userId = $user_id;
		
		$ud = new UserData($user_id);
		$gender = $ud->fetchGender();
		
		$this->recommendation_number = ($gender=="M")?Utils::$recommendation_count_male:Utils::$recommendation_count_female;
		//$this->redis = new redisClass();
		$this->key = Utils::$redis_keys['matches']. "$user_id";
		$this->matchCountKey = Utils::$redis_keys['match_action_count']. "$user_id";
		$this->timeForMatchCountExpiry = Utils::timeTillMidnight() - (60*330);
		//echo $this->timeForMatchCountExpiry/60 ;die;
		//	echo (($this->timeForMatchCountExpiry) - )/3600; die;
		$this->uu = new UserUtils();
		$this->uuDBO = new userUtilsDBO();
		$this->redis = $redis;
	}

	/**
	 * fetch data from recommendation query if a new user comes in
	 * else fetch result from redis
	 * @param $start_range
	 * @param $end_range
	 */
	public function fetchResults($start_range = 1, $end_range = -1) {
		/*echo "where ";
		 echo $this->key;
		 *///$matchCountKeyExists = $this->uu->ifKeyExists($this->matchCountKey);
		$matchKeyExists = $this->uu->ifKeyExists($this->key);
		//var_dump($matchKeyExists);die;
		if($matchKeyExists != 1){
			$actionCount = $this->redis->GET($this->matchCountKey);
			//var_dump ($this->recommendation_number);
			//var_dump($actionCount);
			$this->generateMatchesUsingMemoryTable($this->userId, $this->recommendation_number - $actionCount);
		}/*
		elseif($actionCount == null){
		$this->generateMatchesUsingMemoryTable($this->userId, $this->recommendation_number);
		}
		*/
		return $this->redis->LRANGE($this->key, $start_range, $end_range);
	}



	public function getMatches($user_id, $page_id) {
		//$key = "user:match:$user_id";
		// echo $key;
		$page_id=$page_id-1;
		$start_range=$this->numberOfResults*$page_id;
		$end_range=$start_range+$this->numberOfResults-1;
		$data =  $this->redis->LRANGE($this->key, $start_range, $end_range);//$this->userQ->fetchMatch ( $this->key, $start_range, $end_range );
		return $data;
	}

	private function getRecommendationSet($user_id, $from_cron = false) {
		/*if($from_cron == true){

		//$sql1 = "SELECT user2 from user_recommendation where user1=$this->userId";
		$sql1 = "SELECT user2 from user_recommendation where user1=$this->userId
		and user2 not in (select user2 from user_like where user1 = $this->userId
		union select user2 from user_reject where user1 = $this->userId
		union select user2 from user_rejectMe where user1=$this->userId)";
		$rs = $this->conn->Execute($sql1);

		$count = $rs->RowCount();
		echo $count;
		if($count>=$this->cronRecommendationLimit) return 'exceeded_limit';

		$data = $rs->GetRows();
		}

		else{*/
		/*		$sql =  "select user1  as user from user_hide where user2=?
		 union select user2  as user from user_hide where user1=?
		 */		$sql =  "select user1  as user from user_hide where user2=?
				 union select user2  as user from user_hide where user1=?
				 union select user2 as user from user_like where user1=?
				 union select user2 as user from user_maybe where user1 =? and is_active = 1" ;
		$output = array ();
		$data = Query::SELECT($sql, array($user_id, $user_id, $user_id, $user_id));
		//	}
		//var_dump($data);
		foreach ( $data as $val ) {
			$output [] = $val ['user'];
		}
		$output = array_unique ( $output );
		//var_dump($user_id);
		//var_dump($output);exit;
		return $output;
	}

	public function add_apostrophe(&$item1, $key) {
		$item1 = "'$item1'";
	}
	private function addApostrophe($parameter, $separator = ",") {
		$type = explode ( "$separator", $parameter );
		$type = array_filter($type);
		$val = array_walk ( $type, array (
		$this,
				'add_apostrophe' 
				) );
				return implode ( ",", $type );
	}

	private function checkissetorEmpty($value){
		if(isset($value)==false||strlen($value)==0)
		return false;
		return true;
	}

	/*	private function countOfProfilesToFetch(){
		return $this->redis->LRANGE($this->key, 0 , -1);
		}

		//Trim the list so that it contains two elements viz. -1 and the last seen profile
		private function removeLastLoginProfiles(){
		$this->redis->LTRIM($this->key, 0, 1);
		//		ltrim test 0 1
		}*/

	public function generateMatchesUsingMemoryTable($user_id, $countToFetch = 0){
		//var_dump($countToFetch);die;
		$user_id_to_fliter = $this->getRecommendationSet ( $this->userId , $from_cron);
		if($from_cron == true && $user_id_to_fliter == 'exceeded_limit') return null;
		$user_id_to_fliter = implode ( ",", $user_id_to_fliter );

		$prefData = $this->uuDBO->getUserPreferenceData(array($user_id), true);
		$myData = $this->uuDBO->getUserData(array($user_id), true);
		$myBasicInfo = $this->uuDBO->getUserBasicInfo(array($user_id), true);
		$myPsychoResults = $this->uuDBO->getPsychoScores(array($user_id), true);

		//var_dump($myBasicInfo);die;
		//var_dump($myData);die;
		$myGender = $myBasicInfo['gender'];
		$oppositeGender =  ($myGender == 'M')?'F':'M';
		/*
		 *
		 * select t.user_id, t.user1, case when t.deviation <=2 then 4 else
		 * (case when t.deviation <=5 then 3 else (case when t.deviation <=7 then 2 else 1 end) end) end + t.total_score as score
		 * from      (      select user_id, ul.user1, case when religion = 2 then 2 else (case when religion = 6 then 0 else 1 end)end
		 * 			+   case when marital_status in ('never married') then 2 else 0 end + case when city =2146 then 2 else(case when state = 11 then 1 else 0 end) end  + case when ((income_start >=5 and income_end <=10) OR (income_start=5)) then 2 else (case when income_start=2 then 1 else 0 end) end + case when ul.user1 is not null then 1 else 0 end as total_score , (abs(sumA-10) +abs(sumB-1)+abs(sumC-2)+abs(sumD-3)+abs(sumE-4)+abs(sumF-1))/6   as deviation from user_search_himanshu1  u left join user_like2 ul on u.user_id= ul.user1 and ul.user2 = 10 where gender='F' and age>='30' and age<='39'   and marital_status in ('Never Married') and   ( (country='113'  and ( (state='2168'  and city in (16743) ) or    (state='2172'  and city in (16007) ) or (state='2176' ) ) )  )     and ( (religion='1'  )  or (religion='3'  )  )     and height>='60' and height<='72' and     highest_education in ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16') and     (GREATEST(IFNULL(income_start,-100),-100)<=    LEAST(IFNULL(income_end ,200),200))      and pref_start_age<='20' and (pref_end_age+1)>='20'  and     (pref_marital_status regexp '[[:<:]]Never Married[[:>:]]' or pref_marital_status is NULL)      and (pref_location is null or pref_location       regexp '[[:<:]]113_2168_16743[[:>:]]' or pref_location             regexp '[[:<:]]113_2168[[:>:]]' or pref_location       regexp '[[:<:]]113[[:>:]]')             and pref_start_height<='70' and pref_end_height>='70'    and (pref_smoking_status regexp '[[:<:]]Never[[:>:]]' or pref_smoking_status is NULL)   and (pref_drinking_status regexp '[[:<:]]Never[[:>:]]' or pref_drinking_status is NULL)     and (pref_food_status regexp '[[:<:]]Non-Vegetarian[[:>:]]' or pref_food_status is NULL)      and (GREATEST(IFNULL(pref_income_start,-100),10)<=LEAST(IFNULL(pref_income_end ,200),20))     and (pref_education regexp '[[:<:]]21[[:>:]]' or pref_education is NULL)      and user_id not in (1129,4095,8895,9410,11898,12567,1,2,34,32,4242,42,42,42,42,3421,532532,52352,5,224,24,23,423,423,423,4,24,24,234,23,42325,25,252,352,532,523,5235,23,523523,52,5252,52,52,52552,52,25,25,25,2,5,2,87,97,979,69,56,53,85,835,8657567,65,75,75735,46,414,2,2353463,46,45645756,7,65765,7,56879,9,7,978,987,9789,4324,23,423,423,4,234,345346,35,46,54,65,7,65,786,867,8,67,867,876,9,789,87,9789,7,93492739,92,749237,49237,482648,84682364,816,48126481641,816481,481,6481,648346,82364823,482346,823648,23,6823823,8,38,3423)        limit 100000       )t order by score limit 100;
		 *
		 */
		/*var_dump($prefData['user_id']);
		 var_dump($prefData);*/

		$myReligionPref = $prefData['religionCaste'];

		if($this->checkissetorEmpty($myReligionPref)){
			$myReligionPrefString = $this->addApostrophe($myReligionPref, ";");
		}
		$myMaritalStatusPref = $prefData['marital_status'];
		$prefStartAge = $prefData['start_age'];
		$prefEndAge = $prefData['end_age'];
		/*$myPrefIncomeStart = $prefData['income_start'];
		 $myPrefIncomeEnd = $prefData['income_end'];
		 */

		$myPrefIncome = json_decode($prefData["income"], true);
		$myPrefIncome = implode(',', $myPrefIncome);
		$myPrefSmoking =  $prefData ['smoking_status'] ;
		$myPrefDrinking = $prefData ['drinking_status'];
		$myPrefFood = $prefData ['food_status'] ;
		$prefStartHeight = $prefData ['start_height'];
		$prefEndHeight = $prefData ['end_height'];
		$prefEducation = $prefData['education'];
		$prefLocation = $prefData ['location'];
			
		$myAge = $myData['age'];
		$myReligion = $myData['religion'];
		$myCity = $myData['stay_city'];
		$myState = $myData['stay_state'];
		$myCountry = $myData['stay_country'];
		$myMaritalStatus = $myData['marital_status'];
		$myIncomeStart = $myData['income_start'];
		$myIncomeEnd = $myData['income_end'];
		$myHeight = $myData ['height'];
		$myEducation = $myData['highest_degree'];
		$myEducationGroup = $myData['education_group'];
		$mySmoking =  $myData ['smoking_status'] ;
		$myDrinking = $myData ['drinking_status'];
		$myFood = $myData ['food_status'];
		$mySumA = $myPsychoResults['sum_value_A'];
		$mySumB = $myPsychoResults['sum_value_B'];
		$mySumC = $myPsychoResults['sum_value_C'];
		$mySumD = $myPsychoResults['sum_value_D'];
		$mySumE = $myPsychoResults['sum_value_E'];
		$mySumF = $myPsychoResults['sum_value_F'];
			
		$myCityPref = null;
		$myStatePref = null;
		$n=0;
		$m=0;
		if ($this->checkissetorEmpty ( $prefData ['location'] )) {
			// 113:5268-42504;1:827:829:5296;
			$location_string = explode ( ";", $prefData ['location'] );
			$final_array = array ();
			foreach ( $location_string as $val ) {
				if (strlen ( $val ) == 0)
				continue;
				$state_segment = explode ( ":", $val );
				$country = $state_segment [0];
				$final_array [$country] = array ();
				for($j = 1; $j < count ( $state_segment ); $j ++) {
					$city_segment = explode ( "-", $state_segment [$j] );
					$state = $city_segment [0];
					$myStatePref[$m] = $state;
					$m++;
					$final_array [$country] [$state] = array ();
					for($k = 1; $k < count ( $city_segment ); $k ++) {
						$final_array [$country] [$state] [$city_segment [$k]] = "";
						$myCityPref[$n]= $city_segment[$k];
						$n++;
					}
				}
			}
			$locationQuery = "";
			$c_counter = 0;
			foreach ( $final_array as $ccode => $state_data ) {
				if ($c_counter == 0)
				$locationQuery .= " (country='$ccode' ";
				else
				$locationQuery .= " or (country='$ccode' ";
				if (count ( $state_data ) > 0) {
					$locationQuery .= " and (";
				}
				$s_counter = 0;
				foreach ( $state_data as $state => $city_data ) {
					if ($s_counter == 0)
					$locationQuery .= " (state='$state' ";
					else
					$locationQuery .= " or (state='$state' ";

					if (count ( $city_data ) > 0) {
						$cities = implode ( ",", array_keys ( $city_data ) );
						$locationQuery .= " and city in ($cities) ";
					}
					$s_counter ++;
					$locationQuery .= ")";
				}
				if (count ( $state_data ) > 0) {
					$locationQuery .= " )";
				}
				$c_counter ++;
				$locationQuery .= " ) ";
			}

			$locationQuery_crude .= " and (" . $locationQuery . " )";
			// stay_country | stay_state | stay_city


		}
		else{
			$locationQuery = null;
		}

		//var_dump($locationQuery); die;
		$myCityPref = $this->checkissetorEmpty($myCityPref)?null:implode(',', $myCityPref);
		$myStatePref = $this->checkissetorEmpty($myStatePref)?null:implode(',', $myStatePref);
		/**
		 * TODO: GET THE HARD CODED DATA
		 * @var unknown_type
		 */

		$income_one_lower_arr = array("0:10" => "0:10","10:20"=>"0:10","20:50"=>"10:20","50:51"=>"20:50");
		$income_one_higher_arr = array("0:10" => "10:20","10:20"=>"20:50","20:50"=>"50:51","50:51"=>"50:51");

		$myIncomeRange = "$myIncomeStart:$myIncomeEnd";
		//echo $myIncomeRange;
		$incomeRangeOneLower = $income_one_lower_arr[$myIncomeRange];
		$split = explode(':', $incomeRangeOneLower);
		$incomeRangeOneLowerStart = $split[0];
		$incomeRangeOneLowerEnd = $split[1];
		//var_dump($split); die;
		$incomeRangeOneHigher = $income_one_higher_arr[$myIncomeRange];
		$split2 = explode(':', $incomeRangeOneHigher);
		$incomeRangeOneHigherStart = $split2[0];
		$incomeRangeOneHigherEnd = $split2[1];




		//constants for multiplication
		$myPsychoDummyConstant = 1000;
		$matchPsychoDummyConstant = 2000;

		$religionConstant = MatchEquationMutiplicationFactor::Religion;
		$maritalStatusConstant = MatchEquationMutiplicationFactor::MaritalStatus;
		$valuesConstant = MatchEquationMutiplicationFactor::Value;
		$commonLikeConstant = MatchEquationMutiplicationFactor::CommonLikes;
		$incomeConstant = ($myGender=='M')?MatchEquationMutiplicationFactor::IncomeMale : MatchEquationMutiplicationFactor::IncomeFemale;
		$educationConstant = ($myGender=='M')?MatchEquationMutiplicationFactor::EducationMale : MatchEquationMutiplicationFactor::EducationFemale;
		$cityConstant = ($myGender=='M')?MatchEquationMutiplicationFactor::CityMale : MatchEquationMutiplicationFactor::CityFemale;
		$likedConstant = ($myGender=='M')?MatchEquationMutiplicationFactor::LikedMeMale : MatchEquationMutiplicationFactor::LikedMeFemale;

		if($myReligion == "Muslim"){
			$religionCase = $this->checkissetorEmpty($myReligionPref)?0:"case when religion = 'Muslim' then 1*$religionConstant else 0 end";
		}else{
			//var_dump($this->checkissetorEmpty($myReligionPref));
			$religionCase = $this->checkissetorEmpty($myReligionPref)?0:" case when religion = '$myReligion' then 1*$religionConstant else (case when religion = 'Muslim' then 0 else 0.5*$religionConstant end)end ";
			/*	var_dump( $myReligionPref);
			 var_dump($religionCase); die;*/

		}
		$maritalStatusCase = $this->checkissetorEmpty($myMaritalStatusPref)?0:" case when marital_status = '$myMaritalStatus' then 1*$maritalStatusConstant else 0 end ";
		//var_dump( $this->checkissetorEmpty($myMaritalStatusPref)); die;
		//	$locationCase = $this->checkissetorEmpty($locationQuery)?"case when city = '$myCity' then 2 else(case when state = '$myState' then 1 else 0 end) end ":0;
		$locationCase = $this->checkissetorEmpty($prefLocation)?0:"case when city = '$myCity' then 1*$cityConstant else 0 end ";
		if($oppositeGender == 'M'){
			$incomeCase = $this->checkissetorEmpty($myPrefIncome)?0:"case when ((income_start >=$myIncomeStart and income_end <=$myIncomeEnd) OR (income_start=$myIncomeEnd)) then 1*$incomeConstant else (case when income_start=$incomeRangeOneHigherEnd then 0.5*$incomeConstant else 0 end) end";
			//	$educationCase = $this->checkissetorEmpty($myPrefIncomeStart)?0:"case when education_group >= $myEdGp 1 else 0 end ";
		}
		else{
			$incomeCase = $this->checkissetorEmpty($myPrefIncome)?0:"case when ((income_start >=$myIncomeStart and income_end <=$myIncomeEnd) OR (income_end=$myIncomeStart)) then 1*$incomeConstant else (case when income_end=$incomeRangeOneLowerStart then 0.5*$incomeConstant else 0 end) end";
			//	$educationCase = $this->checkissetorEmpty($myPrefIncomeStart)?0:"case when education_group <= $myEdGp 1 else 0 end ";
		}
		/*var_dump($incomeRangeOneHigherEnd);
		 var_dump($incomeCase); die;*/
		if($oppositeGender =="M"){
			$educationCase = $this->checkissetorEmpty($prefEducation)?0:" case when education_group IN ($myEducationGroup, $myEducationGroup -1) THEN 1*$educationConstant ELSE 0 END";
		}
		else{
			$educationCase = $this->checkissetorEmpty($prefEducation)?0:" case when education_group IN ($myEducationGroup, $myEducationGroup + 1) THEN 1*$educationConstant ELSE 0 END";
		}

		$psychoCase = ($this->checkissetorEmpty($mySumA)==true)?" case when sumA is not null THEN (abs(sumA-$mySumA) +abs(sumB-$mySumB)+abs(sumC-$mySumC)+abs(sumD-$mySumD)+abs(sumE-$mySumE)+abs(sumF-$mySumF))/6 ELSE $matchPsychoDummyConstant END ":$myPsychoDummyConstant;

		$isMyPsychoFilled = 1;
		if($this->checkissetorEmpty($mySumA) == false){
			$isMyPsychoFilled = 0;
		}
		
		if($myGender == "M"){
			$resultSetLimit = Utils::$recommendation_count_male + 1;
		}
		else{
			$resultSetLimit = Utils::$recommendation_count_female + 1;
		}
		/*var_dump($this->checkissetorEmpty($mySumA));
		 var_dump($isMyPsychoFilled);die;*/

		$query = "select t.user_id, t.trust_score, t.maybeScore, t.religionScore, t.maritalStatusScore, t.locationScore, t.incomeScore, t.educationScore, t.likeScore,
				case when t.matchSumA is not null then 1 else 0 end as is_match_psycho_filled, 
				case when t.deviation <=5 then 1*$valuesConstant else 0  end as  deviationScore,
				case when t.deviation <=5 then 1*$valuesConstant 
							else 0  end 
						+ t.total_score as score from     
					 (select u.user_id, uts.trust_score, $religionCase as religionScore, $maritalStatusCase as maritalStatusScore, $locationCase as locationScore,
					 $incomeCase as incomeScore, $educationCase as educationScore, case when ul.user1 is not null then 1*$likedConstant else 0 end as likeScore,
					 case when umb.user2 is not null then -3 else 0 end as maybeScore, 
					 $religionCase +  $maritalStatusCase  + $locationCase + $incomeCase + $educationCase + case when ul.user1 is not null then 1*$likedConstant else 0 end + case when umb.user2 is not null then -3 else 0 end as total_score ,
					 $psychoCase as deviation, sumA as matchSumA
					 	 	 FROM user_search  u 
					 	 	 LEFT JOIN user_like ul on u.user_id= ul.user1 and ul.user2 = $user_id  
					 	 	 LEFT JOIN user_maybe  umb on u.user_id = umb.user2 and umb.user1 = $user_id
					 	 	 LEFT JOIN user_trust_score uts on u.user_id = uts.user_id 
					 	 	 WHERE gender= '$oppositeGender' and age>='$prefStartAge' and age<='$prefEndAge'";

					 if($this->checkissetorEmpty($myMaritalStatusPref))
					 $query .=" and marital_status in (". $this->addApostrophe($myMaritalStatusPref) . ")";


					 if($this->checkissetorEmpty($locationQuery))
					 $query .=" and   ( $locationQuery ) ";


					 	
					 $query  .= " and height>='$prefStartHeight' and height<='$prefEndHeight' ";
					 	
					 if ($this->checkissetorEmpty ( $myPrefSmoking )) {
					 	$query .= " and smoking_status in (" . $this->addApostrophe ( $myPrefSmoking ) . ")";
					 }
					 if ($this->checkissetorEmpty ( $myPrefDrinking )) {
					 	$query .= " and drinking_status in (" . $this->addApostrophe ( $myPrefDrinking ) . ")";
					 }
					 if ($this->checkissetorEmpty ( $myPrefFood )) {
					 	$query .= " and food_status in (" . $this->addApostrophe ( $myPrefFood ) . ")";
					 }
					 	
					 //var_dump($myReligionPrefString);
					 if($this->checkissetorEmpty($myReligionPref)){
					 	$query .= " and religion in (" . $myReligionPrefString . ")";
					 }
					 /*	 var_dump($myReligionPrefString);
					  var_dump($query); die;
					  */
					 if ($this->checkissetorEmpty ( $prefEducation ))
					 $query .= " and highest_education in (" . $this->addApostrophe ( $prefEducation ) . ")";
					 	
					 	
					 //TODO: check income string match
					 /*if ($this->checkissetorEmpty ( $myPrefIncomeStart )&&$this->checkissetorEmpty($myPrefIncomeEnd))
					 $query .= "  and    $myPrefIncomeStart REGEXP income  and income_end <= $myPrefIncomeEnd  ";
					 */// $query .="  and (pref_marital_status regexp '[[:<:]]Never Married[[:>:]]' or pref_marital_status is NULL)      and (pref_location is null or pref_location       regexp '[[:<:]]113_2168_16743[[:>:]]' or pref_location             regexp '[[:<:]]113_2168[[:>:]]' or pref_location       regexp '[[:<:]]113[[:>:]]')             and pref_start_height<='70' and pref_end_height>='70'    and (pref_smoking_status regexp '[[:<:]]Never[[:>:]]' or pref_smoking_status is NULL)   and (pref_drinking_status regexp '[[:<:]]Never[[:>:]]' or pref_drinking_status is NULL)     and (pref_food_status regexp '[[:<:]]Non-Vegetarian[[:>:]]' or pref_food_status is NULL)      and (GREATEST(IFNULL(pref_income_start,-100),10)<=LEAST(IFNULL(pref_income_end ,200),20))     and (pref_education regexp '[[:<:]]21[[:>:]]' or pref_education is NULL)      and user_id not in (1129,4095,8895,9410,11898,12567,1,2,34,32,4242,42,42,42,42,3421,532532,52352,5,224,24,23,423,423,423,4,24,24,234,23,42325,25,252,352,532,523,5235,23,523523,52,5252,52,52,52552,52,25,25,25,2,5,2,87,97,979,69,56,53,85,835,8657567,65,75,75735,46,414,2,2353463,46,45645756,7,65765,7,56879,9,7,978,987,9789,4324,23,423,423,4,234,345346,35,46,54,65,7,65,786,867,8,67,867,876,9,789,87,9789,7,93492739,92,749237,49237,482648,84682364,816,48126481641,816481,481,6481,648346,82364823,482346,823648,23,6823823,8,38,3423)        limit 100000       )t order by score limit 100";

					 // var_dump($myPrefIncome); die;
					 if ($this->checkissetorEmpty ( $myPrefIncome ))
					 $query .= "  and   '$myPrefIncome' regexp concat(income_start,'-',income_end) ";


					 if($this->checkissetorEmpty($myAge)){
					 	$query .= " and pref_start_age<='$myAge' and pref_end_age>='$myAge'";
					 }
					 if($this->checkissetorEmpty($myReligion)){
					 	$query .= " and (pref_religion regexp '[[:<:]]".$myReligion."[[:>:]]' or pref_religion is NULL)";
					 }
					 	
					 if($this->checkissetorEmpty($myMaritalStatus)){
					 	$query .= " and (pref_marital_status regexp '[[:<:]]".$myMaritalStatus."[[:>:]]' or pref_marital_status is NULL) ";
					 }
					 $city_query=$myCountry."_".$myState."_".$myCity;
					 $state_query=$myCountry."_".$myState;
					 $country_query=$myCountry;

					 //make state query
					 $query.= " and (pref_location is null or pref_location
					 regexp '[[:<:]]".$city_query."[[:>:]]' or pref_location
					 regexp '[[:<:]]".$state_query."[[:>:]]' or pref_location
					 regexp '[[:<:]]".$country_query."[[:>:]]')";



					 if($this->checkissetorEmpty($myHeight)){
					 	$query .= " and pref_start_height<='$myHeight' and pref_end_height>='$myHeight'";
					 }
					 	
					 if($this->checkissetorEmpty($mySmoking)){
					 	if($mySmoking == "Never")
					 	$query .= " and (pref_smoking_status IN ('Never') or pref_smoking_status is NULL) ";
					 	else $query .= " and (pref_smoking_status is NULL)";
					 }
					 if($this->checkissetorEmpty($myDrinking)){
					 	if($myDrinking == "Never")
					 	$query .= " and (pref_drinking_status IN ('Never') or pref_drinking_status is NULL) ";
					 	else $query .= " and (pref_drinking_status is NULL)";

					 	//$query .= " and (pref_drinking_status regexp '[[:<:]]".$myDrinking."[[:>:]]' or pref_drinking_status is NULL) ";
					 }
					 if($this->checkissetorEmpty($myFood)){
					 	if($myFood == "Vegetarian")
					 	$query .= " and (pref_food_status IN ('Vegetarian') or pref_food_status is NULL) ";
					 	else $query .=" and ( pref_food_status is NULL )";
					 }


					 //TODO: check income - make it a string search
					 if($this->checkissetorEmpty($myIncomeStart)&&$this->checkissetorEmpty($myIncomeEnd)){
					 	//$query .= " and ((pref_income_start >=$myIncomeStart or  pref_income_start is NULL) and (pref_income_end <=$myIncomeEnd or  pref_income_end is NULL)) ";
					 	$query .= " and (pref_income regexp concat($myIncomeStart,'-',$myIncomeEnd) or  pref_income is NULL) ";
					 }
					 /*
					  if($this->checkissetorEmpty($income_end)){
					  $query .= " and (pref_income_end>='$income_end' or  pref_income_end is NULL)";
					  }*/
					 	
					 if($this->checkissetorEmpty($myEducation)){
					 	$query .= " and (pref_education regexp '[[:<:]]".$myEducation."[[:>:]]' or pref_education is NULL) ";
					 }


						if (strlen ( $user_id_to_fliter ) > 0)
						$query .= ' and u.user_id not in (' . $user_id_to_fliter . ')   ';

						$query .= " )t order by score desc, trust_score desc limit $resultSetLimit";

						//var_dump($query);  die;
						$qresult =null;
						try {
							//trigger_error($query_crude,E_USER_WARNING);
							$qresult = Query::SELECT($query, null);
							//var_dump($result);
						} catch ( Exception $e ) {
							//echo $e->getMessage();
							trigger_error ( $e->getMessage (), E_USER_WARNING );
						}



						//var_dump($qresult); die;
						$idList = null;
						$result = array ();
						$scoreData = array();
						$rankGroups = array();
						$final_resultSet = array();
						foreach ( $qresult as $row ) {
							$result[$row['user_id']] = $row['score'];
							$scoreData[$row['user_id']] = array('religion' => $row['religionScore'],
														 		'maritalStatus' =>$row['maritalStatusScore'],
																'location' => $row['locationScore'],
																'income' => $row['incomeScore'],
																'education' => $row['educationScore'],
																'like' => $row['likeScore'],
																'is_match_psycho_filled' => $row['is_match_psycho_filled'],
																'is_my_psycho_filled' => $isMyPsychoFilled,
																'deviation' => $row['deviationScore'],
																'trust_score' => $row['trust_score'],
																'maybe_score' => $row['maybeScore'],
																'total' => $row['score']);

							$idList[] = $row['user_id'];
						}

						//	if($idList == null) return null;

						/*						if(count($idList)>0){
							$uu = new UserUtils();
							$mutuals=$uu->setGetMutualConnections($this->userId, $idList, $from_cron);
							foreach ($mutuals as $key=>$val){
							$result[$key] .= $result[$key]*6;
							}

							}
							arsort($result);*/


						if(count($idList)>0){
							$uu = new UserUtils();
							$mutuals=$uu->setGetMutualConnections($this->userId, $idList);
						}
												 
						$recommendation_set=array_slice ( $result, 0, $countToFetch, true ); 

						$this->storeResults($recommendation_set, $query);
						$this->addMatches($recommendation_set);
						$this->logIndividualScore($scoreData);
						$this->setExpiryForRedisKeys($countToFetch);

	}


	private function setExpiryForRedisKeys($countToFetch){
		$uu = new UserUtils();
		$uu->setExpiry($this->key, $this->timeForMatchesExpiry);
		$uu->setKeyAndExpire($this->matchCountKey, $this->timeForMatchCountExpiry, $this->recommendation_number - $countToFetch);
	}

	private function logIndividualScore($data){
		//echo 'ge';
		$countIds = count($data);

		if($countIds > 0){
			$params_arr = array();
			$insertSql = "INSERT into user_recommendation_log values  (?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?)";

			foreach ($data as $key => $val){
				$params_arr[] = array($this->userId, $key, $val['religion'], $val['maritalStatus'], $val['location'], $val['income'],$val['education'],$val['like'], $val['deviation'], $val['total'], $val['is_match_psycho_filled'], $val['is_my_psycho_filled'], $val['trust_score'] , $val['maybe_score']  );
			}
			Query::BulkINSERT($insertSql, $params_arr);

		}


	}

	private function storeResults($resultSet, $query, $tableName =false){
		$table = "user_recommendation_new";

		if($tableName == true)
		$table = $tableName;

		$result = json_encode($resultSet);
		//var_dump($result);die;
		//		foreach($resultSet as $key=>$val){
		$prprdSt ="insert ignore into $table values(?,?,?, now())";
		Query::INSERT($prprdSt, array($this->userId, $query, $result));
		//	}

	}

	private function addMatches($user_array) {
		$i=2;
		$args[0] = $this->key;
		//if($existingCount == null){
		//$i++;
		$args[1] = -1;
		//}
		//if it's a new user, insert -1 as sentinel
		foreach ($user_array as $key=>$val){
			$args[$i] = $key;
			$i++;
		}
		call_user_func_array(array($this->redis,'RPUSH'),$args);
		//$this->redis->SET($this->matchCountKey, 0);
	}
}


//$g = new generateNewMatches(15801);
// $g->generateMatchesUsingMemoryTable(15801);


//$result = $gen->fetchResults();
//var_dump($result);
?>
