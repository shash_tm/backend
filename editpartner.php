
<?php

include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
include_once (dirname ( __FILE__ ) . '/include/validate.php');
include_once (dirname ( __FILE__ ) . '/include/dropdowns.php');
include_once (dirname ( __FILE__ ) . '/include/allValues.php');
include_once (dirname ( __FILE__ ) . '/TMObject.php');
class editPartner extends TMObject {
function __construct() {
parent::__construct();

}
}
try {
functionClass::redirect('editpartner');
$user_id=$_SESSION['user_id'];
$object = new dropdowns ();
$result=$conn->Execute("select * from user where user_id='".$user_id."'")->GetRows();
$data=$result[0];

$smarty->assign ('data', $data );
$allPages=array("demographic","work","family","trait");
foreach($allPages as $page) 
$object->getDropDowns ($page,$data);

$rs=$conn->Execute("select * from user_preference_demography where user_id='".$user_id."' limit 1");
if($rs->_numOfRows>0) {
$demography=$rs->GetRows();
$countryWise=explode(";",$demography[0]['location']);
$countries=$states=$cities='';
foreach($countryWise as $countrywise) {
$stateWise=explode(":",$countrywise);
if($countries==''&&$stateWise[0]!='')
$countries=$stateWise[0];
else if($stateWise[0]!='')
$countries .=",".$stateWise[0];
$country=$stateWise[0];
unset($stateWise[0]);
foreach($stateWise as $statewise) {
$citiWise=explode("-",$statewise);
if($states==''&&$citiWise[0]!='')
$states=$country."-".$citiWise[0];
else if($citiWise[0]!='') $states .=",".$country."-".$citiWise[0];
$state=$citiWise[0];
unset($citiWise[0]);
foreach($citiWise as $citywise)
if($cities==''&&$citywise!='')
$cities=$country."-".$state."-".$citywise;
else if($citywise!='') $cities .=",".$country."-".$state."-".$citywise;
}
}
$religionWise=explode(";",$demography[0]['religionCaste']);
$religions=$castes='';
foreach($religionWise as $religionwise) {
$casteWise=explode(":",$religionwise);
if($religions==''&&$casteWise[0]!='')
$religions=$casteWise[0];
else if($casteWise[0]!='') $religions .=",".$casteWise[0];
$religion=$casteWise[0];
unset($casteWise[0]);
foreach($casteWise as $castewise)
if($castes==''&&$castewise!='')
$castes=$religion."-".$castewise;
else if($castewise!='') $castes .=",".$religion."-".$castewise;
}
/*$languages_selected='';
$demography[0]['languages_preference']=explode(",",$demography[0]['languages_preference']);
foreach($demography[0]['languages_preference'] as $language) {
if($languages_selected=='')
$languages_selected=array_search($language,$tongues);
else $languages_selected .=",".array_search($language,$tongues);
}
$demography[0]['languages_preference']=$languages_selected;
*/

unset($demography[0]['religionCaste']);
unset($demography[0]['location']);
$demography[0]['religions']=$religions;
$demography[0]['castes']=$castes;
$demography[0]['countries']=$countries;
$demography[0]['states']=$states;
$demography[0]['cities']=$cities;

$smarty->assign('demo',json_encode($demography[0]));
}
$rs=$conn->Execute("select * from user_preference_trait where user_id='".$user_id."'  limit 1");
if($rs->_numOfRows>0) {
$trait=$rs->GetRows();
$smarty->assign('trait',json_encode($trait[0]));
}
$rs=$conn->Execute("select * from user_preference_family where user_id='".$user_id."'  limit 1");
if($rs->_numOfRows>0) {
$family=$rs->GetRows();
$smarty->assign('family',json_encode($family[0]));
}
$rs=$conn->Execute("select * from user_preference_work where user_id='".$user_id."'  limit 1 ");
if($rs->_numOfRows>0) {
$work=$rs->GetRows();
$smarty->assign('work',json_encode($work[0]));
///print_r($work[0]);die;
}
$myProfile = new editPartner();
$header= $myProfile->getHeaderValues($user_id);
$smarty->assign('header',$header);


$smarty->display(dirname ( __FILE__ ) . '/templates/partnerprefernces.tpl');
}
catch(Exception $e) {
trigger_error($e->getMessage(),E_USER_WARNING );

}
?>
