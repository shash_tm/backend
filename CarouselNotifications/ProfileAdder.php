<?php
require_once dirname(__FILE__) . "/../include/Utils.php";


class ProfileAdder {
    public function getFamousProfiles($user_id,$numberOfFamousProfilesToServe) {
        #Find males from bucket 5 who clear the 2 way matching and to whom the female hasn't liked/hidden before.
        $recommender = new RecommendationEngine_RetriveHighestBucketMatches($user_id);
        $recommender->setFreshCountToFetch($numberOfFamousProfilesToServe);
        $results = $recommender->getMatches($user_id);
        $final_set          = $results["final_set"];
        $total_score_set    = $results["total_score_set"];
        $result_fresh_set   = $results["result_fresh_set"];
        $freshProfileQuery  = $results["freshProfileQuery"];
        $availabile_set     = $results["availabile_set"];
        $ids = $recommender->addMatchesToSetOnly($final_set, array(), $availabile_set);
        
        $final_score_set = array();
        foreach ($final_set as $key) {
            if (key_exists($key, $total_score_set)) {
                $final_score_set[$key] = $total_score_set[$key];
            }
        }
        $logString = "{'RecommendationAB':{'RecommendationEngine_RetriveHighestBucketMatches':['',null]},'AestheticsAB':{'Aesthetics_None':['',null,null]}}";
        $recommender->doLogging($final_score_set, null, null, $result_fresh_set, $freshProfileQuery, $numberOfFamousProfilesToServe, $logString);
        return array($ids[0],$final_set);
    }
}