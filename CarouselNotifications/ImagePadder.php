<?php

require_once dirname(__FILE__) . "/../include/Utils.php";

class ImagePadder {

    public function getPaddedImage($pic_url,$paddingPercent) {
        $path = "files/images/profiles/";
        $new_pic = substr_replace($pic_url, "", -4) . "_Carousal.jpg";
        Utils::getFromS3($path . $new_pic, "/tmp/" . $new_pic);
        $new_image = imagecreatefromjpeg("/tmp/" . $new_pic);
        if (!$new_image) {
            Utils::getFromS3($path . $pic_url, "/tmp/" . $pic_url);
            $image_x = imagecreatefromjpeg("/tmp/" . $pic_url);
            list($width_x, $height_x) = getimagesize("/tmp/" . $pic_url);
			$padding = ($paddingPercent*$height_x)/100;
            $image = imagecreatetruecolor($width_x, $height_x);
			imagecopyresampled($image, $image_x, 0, $padding, 0, 0, $width_x, $height_x-$padding, $width_x, $height_x);
            imagejpeg($image, "/tmp/" . $new_pic);
            Utils::saveOnS3("/tmp/" . $new_pic, $path . $new_pic);
            //Clean up
            imagedestroy($image);
            imagedestroy($image_x);
            unlink("/tmp/".$new_pic);
        }
        return $new_pic;
    }
}
