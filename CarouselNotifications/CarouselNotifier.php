<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../Utils/ArrayUtils.php";
require_once dirname ( __FILE__ ) . "/ProfileAdder.php";
require_once dirname ( __FILE__ ) . "/InactiveUserIdentifier.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../Recommender.php";
require_once dirname ( __FILE__ ) . "/Commonalities.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/ImagePadder.php";
require_once dirname ( __FILE__ ) . "/MessageSelector.php";

/*
 * The class serves the purpose of firing out notifications to inactive females
 * in a Carousal fashion
 */

$experimentIds = array();
if ($argc > 1) {
   $experimentIds = $argv;
   array_shift($experimentIds);
}

class CarouselNotifier {

    private $matchNoticationMessages;

    function __construct()
    {        $this->matchNoticationMessages = array( 1 => "Find out what you're missing out on! Fire up the app now.",
                                                    2 => "Aren't you a teeny bit curious about who's out there? Fire up the app!",
                                                    3 => "Roses are red, violets are blue, find out who's waiting for you!");
    }
    public function notifyUsers($experimentIds) {
        global $redis, $imageurl, $admin_id;
        $userFlags = new UserFlagsDBO();
        
        $data=$userFlags->read("carousel_configurations");
        $inactiveDaysLow                = isset($data['inactiveDaysLow'])?$data['inactiveDaysLow']:15;
        $inactiveDaysHigh               = isset($data['inactiveDaysHigh'])?$data['inactiveDaysHigh']:25;
        $likesThreshold                 = isset($data['likesThreshold'])?$data['likesThreshold']:10;
        $numberOfDays4Notification      = isset($data['numberOfDays4Notification'])?$data['numberOfDays4Notification']:1;
        $numberOfFamousProfilesToServe  = isset($data['numberOfFamousProfilesToServe'])?$data['numberOfFamousProfilesToServe']:5;
	$paddingPercent			= isset($data['paddingPercent'])?$data['paddingPercent']:10;
        $title_text                     = MessageSelector::getTitleText($data);
        $sub_text                       = MessageSelector::getSubText($data);
        $ticker_text                    = MessageSelector::getTickerText($data);
        
        #1. Query to identify the inactive females with a large number of un-actioned likes received.
        $pushnotify = new pushNotification();
        $targetUsers = sizeof($experimentIds) > 0 ? $experimentIds :InactiveUserIdentifier::getInactiveUsers($inactiveDaysLow,$inactiveDaysHigh,$likesThreshold);
        
        #2. Ask recommendations for each of the above female.
        $userUtils = new userUtilsDBO();
        $uu = new UserUtils();    
        foreach($targetUsers as $user_id) {
            $ud = new UserData($user_id);
            $gender = $ud->fetchGender();
            $user_data = $userUtils->getUserData(array($user_id), false);
            $age = -1;
            $location = "";
            $city = "";
            if(sizeof($user_data) > 0) {
                    $age = $user_data[0]['age'];
                    $location = $user_data[0]['stay_state'];
                    $city = $user_data[0]['stay_city'];
            } 
            $key = Utils::$redis_keys['matches'] . "$user_id";
            $redis->DEL($key);
            $matcher = new Recommender($user_id , $gender, $age, $location, $city);
            $recommendations_data = $matcher->fetchResults();
            $ids = $recommendations_data['recommendations'];
            
            #a. Add a couple of profile in the front from the higher bucket.
            $famousProfilesArray = ProfileAdder::getFamousProfiles($user_id,$numberOfFamousProfilesToServe);
            $famousProfiles = $famousProfilesArray[0];
            $rawFamousProfileIds = $famousProfilesArray[1];
            $arr = array();
            $arr[0] = Utils::$redis_keys['matches'] . "$user_id";
            $arr = array_merge($arr,array_merge($famousProfiles, array("-2")));
            call_user_func_array(array($redis, 'LPUSH'), $arr);
            #b. Mark the redis expiry time for it as a day i.e. till the girl takes first action
            #IDEA: Insert a value of -2. Check in when accessing from redis !
            $time = 3600*24*$numberOfDays4Notification;
            $redis->EXPIRE($key, $time);
            
            # We'll have to create custom messages here.
            # Let's get the common_likes and common hashtags
            $content_texts = Commonalities::getCommonalitiesString($user_id, $rawFamousProfileIds, false);
            $push_arr = array();
            $push_arr["push_type"] = "MATCHES_IMAGE_MULTI";
            //Generic title text for backward compatibility
            $push_arr["title_text"] = $title_text;
            $i = 0;
            foreach($content_texts as $user => $content_text) {
                $i= $i + 1;
                #Prepare a notificaton for the female.					 
                $namePicData = $uu->getSystemProfilePicForUser($user);
                $imageObj = new ImagePadder();
                $pic_url = $imageObj->getPaddedImage($namePicData['name'],$paddingPercent);
                $pic_url = $imageurl.$pic_url;
                $push_arr[$i] = array ( 
                    "match_id"      => $user,
                    "content_text"  => $content_text[array_rand($content_text)], 
                    "sub_text"      => $sub_text,
                    "pic_url"       => $pic_url, 
                    "ticker_text"   => $ticker_text, 
                    "title_text"    => $title_text); 
            }
            if(sizeof($push_arr) > 0) {
                $pushnotify->notify($user_id, $push_arr, $admin_id,0,array(),array('androidApp'));
                $ios_push_data = $this->getIOSpushDataForUserCarousel($title_text);
                $pushnotify->notify($user_id, $ios_push_data, $admin_id,0,array(),array('iOSApp'));

                echo "sent to $user_id";}
        }   
    }

    private function getIOSpushDataForUserCarousel($title_text)
    {
        $msg_count = count($this->matchNoticationMessages);
        $index = rand(1, $msg_count);
        $content_text =  $this->matchNoticationMessages[$index];
        $ios_push_data = array('push_type' => "PROMOTE" ,
            'title_text' => $title_text,
            'event_status' => "MATCHES_IMAGE_MULTI",
            'content_text' => $content_text);
        return $ios_push_data;
    }
}

try{
    echo "Starting";
    $notifier = new CarouselNotifier();
    $notifier->notifyUsers($experimentIds);
} catch (Exception $e){
	echo $e->getTraceAsString();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	trigger_error($e->getMessage(), E_USER_WARNING); 
}
?>
