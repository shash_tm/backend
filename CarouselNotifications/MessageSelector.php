<?php
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";

class MessageSelector {
    public static function getTitleText($data) {
        $arr = explode("#",isset($data['title_text'])?$data['title_text']:"Men ‘O’ Clock");
        $day = date("D");
        if($day === 'Mon') {
            return "Match Mondays";
        }
        else if($day == 'Sat') {
            return "UnSingle Saturdays";
        }
        else if($day == 'Fri') {
            return "Flirty Friday";
        }
        else {
            //Choose randonly from array
            return $arr[array_rand($arr)];
        }
    }
    
    public static function getSubText($data) {
        $arr = explode("#",isset($data['sub_text'])?$data['sub_text']:"New matches ...");
        return $arr[array_rand($arr)];
    }
    
    public static function getTickerText($data) {
        $arr = explode("#",isset($data['ticker_text'])?$data['ticker_text']:"Check out the most popular matches for you!");
        return $arr[array_rand($arr)];
    }
}
