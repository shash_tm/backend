<?php

require_once dirname(__FILE__) . "/../Utils/ArrayUtils.php";
require_once dirname(__FILE__) . "/../include/config_admin.php";
require_once dirname(__FILE__) . "/../include/Utils.php";

/*
 * The class identifies the inactive females in the 
 */
class InactiveUserIdentifier {
    
    public static function getInactiveUsers($inactiveDaysLow,$inactiveDaysHigh,$likesThreshold) {
        //\\TODO - Android and version check add
        global $conn_reporting;
        $query = "select distinct(q.user_id) from "
                . "(select t.user_id as user_id,t.lastLogin as lastLogin from "
                    . "(select ull.user_id as user_id,TIMESTAMPDIFF(day,ull.last_login,curdate()) as lastLogin from "
                        . "user_lastlogin ull join user_search us on ull.user_id = us.user_id join user_gcm_current ugc on ull.user_id = ugc.user_id "
                        . "where us.gender = 'F' and us.install_status = 'install' and ugc.source = 'androidApp' and CAST(ugc.app_version_code AS UNSIGNED) >= 210 and ugc.status = 'login') t "
                    . "group by t.user_id) q "
                . "where q.lastLogin >= $inactiveDaysLow and q.lastLogin <= $inactiveDaysHigh";
        $ids = $conn_reporting->Execute($query)->GetRows();
        $result = array();
        foreach($ids as $id) {
            $result[] = $id['user_id'];
        }
        return $result;
    }
}