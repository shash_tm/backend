<?php
//\TODO - Decide if this should be CONFIG_ADMIN or CONFIG !?
// Resolved Purav's dilemma after bug on prod
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../Utils/ArrayUtils.php";
require_once dirname ( __FILE__ ) . "/ProfileAdder.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../Recommender.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/eventsDBO.php";
require_once dirname ( __FILE__ ) . "/../fb.php";
require_once dirname ( __FILE__ ) . "/../DBO/userFacebookDBO.php";


class Commonalities {

    /*
     * The array_uintersect uses a tree based structure to find out intersection. 
     * This does not work out with Levenshtein distances. 
     * Writing a custom o(n^2) function for it.
     */
    public static function _array_uintersect(array $arr1, array $arr2, $key_compare_func) {
        $arr_matched = [];
    	foreach ($arr1 as $k1 => $v1) {
            foreach ($arr2 as $k2 => $v2) {
                $diff = call_user_func($key_compare_func,$v1,$v2);        
                if ($diff === 0) {
                    $arr_matched[$k1] = $v1;
                }
            }
        }
        return $arr_matched;
    }
    
    /*
     * 1. Stripping the strings of all the unwanted characters
     * 2. Computing the Levenshtein distance between the two stripped strings.
     * 3. Check if the distance is withing the acceptable limit of length of string1 - 10%
     * @return - the return value is counter intuitive - 1 if they don't match and 0 when they do.
     * Has been designed to support the Commonalities::_array_uintersect calls
     */
    public static function levenshteinRawStringComparison($string1,$string2) {
        $rawString1 = strtolower(preg_replace("/[^a-zA-Z]/", "",$string1));
        $rawString2 = strtolower(preg_replace("/[^a-zA-Z]/", "",$string2));
        $distance   = levenshtein($rawString1,$rawString2);
        if(strlen($rawString1) <= 0) {
            return 1;
        }
        else {
            return (100*$distance/strlen($rawString1)) > 10 ? 1 : 0;
        }
    }
    
    /*
     * Finds out the commonalities between a pair of user ids.
     * The strings are added to result in the desired sequence.
     * \\TODO - Add facebook mutual friends to the list
     */
    public static function getCommonalitiesString($user_id, $match_ids, $isHtmlTagsEnabled = false, $datespot_id = null) {
        if(sizeof($match_ids) > 0) {
            $uu         = new UserUtils();
            $userFlags  = new UserFlagsDBO();
            $uuDBO      = new userUtilsDBO();
            $ud         = new UserData($user_id);

            $profile_data = array();
            $mergedIds = array_merge(array($user_id), $match_ids);
            
            //Find my gender
            $myGender = $ud->fetchGender();
            $heSheString        = ($myGender            == 'M' ?    "she" : "he");
            $heSheCapsString    = ($myGender            == 'M' ?    "She" : "He");
            $himHerString       = ($myGender            == 'M' ?    "her" : "him");
            $hisHerString       = ($myGender            == 'M' ?    "her" : "his");
            $prefixHtmlString   = ($isHtmlTagsEnabled   == true) ?  "<font color=\"#ffb700\">" : "";
            $suffixHtmlString   = ($isHtmlTagsEnabled   == true) ?  "</font>" : "";
            $colouredHtmlString = ($isHtmlTagsEnabled   == true) ?  "</font><font color=\"#46A3C5\">%s</font><font color=\"#ffb700\">" : "%s";
            
            //1. COMMON FACEBOOK FRIENDS

            //2. COMMON COMPANIES
            $myCompany      = array();
            $myInsti        = array();
            $myDegree       = null;
            $myIndustry     = null;
            //$myDesignation  = array();
            $row_data = $uuDBO->getUserData($mergedIds, false);
            foreach ($row_data as $key => $val) {
                if ($val['user_id'] == $user_id) { 
                    $myDegree       = $val['degree'];
                    $myIndustry     = $val['industry_name'];
                    //$myDesignation  = ucfirst($val['designation']);
                    $myCompany      = json_decode($val['company_name'], true);
                    $myInsti        = json_decode($val['institute_details'], true);
                    continue; 
                }
                $profile_data[$val['user_id']]['highest_degree']    = $val['degree'];
                $profile_data[$val['user_id']]['industry']          = $val['industry_name'];
                //$profile_data[$val['user_id']]['designation']       = ucfirst($val['designation']);
                $profile_data[$val['user_id']]['companies']         = json_decode($val['company_name'], true);
                $profile_data[$val['user_id']]['institute'] = json_decode($val['institute_details'], true);
            }

            // Find Interests
            $interests = $uu->getInterestsAndHobbies($mergedIds, false);
            $myMusicFavourites  = array();
            $myBooksFavourites  = array();
            $myMoviesFavourites = array();
            $mySportsFavourites = array();
            $myTravelFavourites = array();
            $myOtherFavourites  = array();
            $myHashTags         = array();
            foreach ($interests as $user => $val) {
                if ($user == $user_id) {
                    $myMusicFavourites  = $val["music_favorites"];
                    $myBooksFavourites  = $val["books_favorites"];
                    $myMoviesFavourites = $val["movies_favorites"];
                    $mySportsFavourites = $val["sports_favorites"];
                    $myTravelFavourites = $val["travel_favorites"];
                    $myOtherFavourites  = $val["other_favorites"];
                    $myHashTags         = $val["preferences"];
                    continue;
                }
                $profile_data[$user]['music_favorites']     = $val["music_favorites"];
                $profile_data[$user]['books_favorites']     = $val["books_favorites"];
                $profile_data[$user]['movies_favorites']    = $val["movies_favorites"];
                $profile_data[$user]['sports_favorites']    = $val["sports_favorites"];
                $profile_data[$user]['travel_favorites']    = $val["travel_favorites"];
                $profile_data[$user]['other_favorites']     = $val["other_favorites"];
                $profile_data[$user]['hash_tags']           = $val["preferences"];
            }
            
            $data=$userFlags->read("carousel_configurations");
            $result = array();
            $user_fb = new UserFacebookDBO();
            $access_token = $target_user_fb_id = null;
            $access_token = $user_fb->get_facebook_token($user_id);
            
            foreach($profile_data as $key => $data) {
                #Support for facebook mutual friends.
		$target_user_fb_id = $user_fb->get_facebook_id($key);
		if ($access_token != null && $target_user_fb_id != null) {
			$fb = new fb($access_token);
			$mutual_friends = $fb->get_mutual_friends($user_id, $key, $target_user_fb_id);
                        if(isset($mutual_friends['mutual_friends'])) {
                            $commonFbFriends = $mutual_friends['mutual_friends'];
                            $fbStrings = array();
                            if(sizeof($commonFbFriends) == 1) {
                                $fbStrings                  = array(    array($prefixHtmlString."How does %s know ".$colouredHtmlString."? Ask %s!".$suffixHtmlString,$heSheString,$commonFbFriends[array_rand($commonFbFriends)]['name'],$himHerString),
                                                                        array($prefixHtmlString."Look! You both know ".$colouredHtmlString."! Ask %s how.".$suffixHtmlString,$commonFbFriends[array_rand($commonFbFriends)]['name'],$himHerString));
                            }
                            else if (sizeof($commonFbFriends) == 2) {
                                $fbStrings                  = array(    array("You have a couple of common friends. Wonder how?"));
                            }
                            else if (sizeof($commonFbFriends) > 2) {
                                $fbStrings                  = array(    array($prefixHtmlString.$colouredHtmlString.",".$colouredHtmlString." and ".$colouredHtmlString." are friends with both of you!".$suffixHtmlString,$commonFbFriends[0]['name'],$commonFbFriends[1]['name'],$commonFbFriends[2]['name']),
                                                                        array("So many friends in common! Maybe your paths have crossed before?"),
                                                                        array($prefixHtmlString."How is %s friends with ".$colouredHtmlString.",".$colouredHtmlString." and ".$colouredHtmlString."? Ask away!".$suffixHtmlString,$heSheString,$commonFbFriends[0]['name'],$commonFbFriends[1]['name'],$commonFbFriends[2]['name']));
                            }
                            if(sizeof($fbStrings) > 0) {
                                $result[$key][] = call_user_func_array("sprintf",$fbStrings[array_rand($fbStrings)]);
                            }
                        }
                }
                
                
                #Support for events page commonalities string
                if(!is_null($datespot_id)) {
                    #Extract out the scene name
                    $dateSpotDetails = eventsDBO::getDatespotById($datespot_id);
                    $event_name = isset($dateSpotDetails['spark_display_name']) && $dateSpotDetails['spark_display_name'] != null
                    && trim($dateSpotDetails['spark_display_name']) != "" ? $dateSpotDetails['spark_display_name'] : $dateSpotDetails['name'];
                    if($event_name) {
                        $sceneStr = $event_name;
                        $eventsDependentStrings     = array(    array($prefixHtmlString."Say 'hi' and you may end up going to the ".$colouredHtmlString." together!".$suffixHtmlString,$sceneStr),
                                                                array($prefixHtmlString."If you need a plus one for ".$colouredHtmlString.", starting a conversation now would help!".$suffixHtmlString,$sceneStr),
                                                                array($prefixHtmlString."Ask %s the plan for ".$colouredHtmlString."! ".$suffixHtmlString,$himHerString,$sceneStr),
                                                                array($prefixHtmlString."Mingle and meet your kind of people at ".$colouredHtmlString."! Ask %s out!".$suffixHtmlString,$sceneStr,$himHerString),
                                                                array($prefixHtmlString."You both are attending ".$colouredHtmlString.". Don't let the cat get your tongue and send a message!".$suffixHtmlString,$sceneStr),
                                                                array($prefixHtmlString."Ask %s if %s'd want to go to ".$colouredHtmlString." together!".$suffixHtmlString,$himHerString,$heSheString,$sceneStr),
                                                                array($prefixHtmlString."A message could end up with a plus one for you at ".$colouredHtmlString."!".$suffixHtmlString,$sceneStr));
                        $result[$key][] = call_user_func_array("sprintf",$eventsDependentStrings[array_rand($eventsDependentStrings)]);
                    }
                    $eventsIndependentStrings       = array(    array("Why so speechless? Speak up and plan your Scene!"),
                                                                array("New scenes, new friends! The Scene is set"),
                                                                array("It's your move. It all starts here..."),
                                                                array("Two's never a crowd at this Scene. Ask %s out!",$himHerString),
                                                                array("Conversations and good company can change the Scene for you! Ask %s out!",$himHerString),
                                                                array("If you like the same Scene, you probably will like each other as well! Type that message."),
                                                                array("Turn your TM Scene into a date. First step: Type that message!"));
                    $result[$key][] = call_user_func_array("sprintf",$eventsIndependentStrings[array_rand($eventsIndependentStrings)]);
                }

                if($profile_data[$key]['highest_degree'] != null && $myDegree != null) {
                    if(!Commonalities::levenshteinRawStringComparison($myDegree, $profile_data[$key]['highest_degree'])) {
                        $common_degree_content_text = isset($data['common_degree_content_text']) ? $data['common_degree_content_text'] : 
                            array(array($prefixHtmlString."You both studied ".$colouredHtmlString.". Pretty neat topic to talk about!".$suffixHtmlString,$myDegree),
                                  array($prefixHtmlString."You both studied ".$colouredHtmlString.". Why not let %s know?".$suffixHtmlString,$myDegree,$himHerString));
                        $result[$key][] = call_user_func_array("sprintf",$common_degree_content_text[array_rand($common_degree_content_text)]);
                    }
                }

                if($profile_data[$key]['industry'] != null && $myIndustry != null) {
                    if(!Commonalities::levenshteinRawStringComparison($myIndustry, $profile_data[$key]['industry'])) {
                        $common_industry_content_text = isset($data['common_industry_content_text']) ? $data['common_industry_content_text'] : 
                            array(array($prefixHtmlString."Sounds like you both know the ups and downs of the ".$colouredHtmlString." industry!".$suffixHtmlString,$myIndustry),
                                  array($prefixHtmlString."Find out if you have someone in common from the ".$colouredHtmlString." industry.".$suffixHtmlString,$myIndustry));
                        $result[$key][] = call_user_func_array("sprintf",$common_industry_content_text[array_rand($common_industry_content_text)]);
                    }
                }

                if($profile_data[$key]['institute'] != null && $myInsti != null) {
                    $commonInstitutes = Commonalities::_array_uintersect($myInsti, $profile_data[$key]['institute'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonInstitutes)> 0 ) {
                        $common_institute_content_text  = isset($data['common_institute_content_text'])     ?$data['common_institute_content_text'] : 
                            array(array($prefixHtmlString."Did you know you both went to ".$colouredHtmlString." ?".$suffixHtmlString,$commonInstitutes[array_rand($commonInstitutes)]),
                                  array($prefixHtmlString."You both went to ".$colouredHtmlString." so ask if your paths crossed!".$suffixHtmlString,$commonInstitutes[array_rand($commonInstitutes)]),
                                  array($prefixHtmlString."Maybe you have met before since you both went to ".$colouredHtmlString.$suffixHtmlString,$commonInstitutes[array_rand($commonInstitutes)]));
                        $result[$key][] = call_user_func_array("sprintf",$common_institute_content_text[array_rand($common_institute_content_text)]);
                    }
                }

                if($profile_data[$key]['companies'] != null && $myCompany != null) {
                    $commonCompanies = Commonalities::_array_uintersect($myCompany, $profile_data[$key]['companies'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonCompanies)> 0 ) {
                        $common_company_content_text = isset($data['common_company_content_text']) ? $data['common_company_content_text'] : 
                            array(array($prefixHtmlString."You both have worked at ".$colouredHtmlString."! Share your stories.".$suffixHtmlString,$commonCompanies[array_rand($commonCompanies)]),
                                  array($prefixHtmlString."You both worked at ".$colouredHtmlString.". Ask %s if %s enjoyed the coffee there!".$suffixHtmlString,$commonCompanies[array_rand($commonCompanies)],$himHerString,$heSheString));
                        $result[$key][] = call_user_func_array("sprintf",$common_company_content_text[array_rand($common_company_content_text)]);
                    }
                }          

                if($profile_data[$key]['music_favorites'] != null && $myMusicFavourites != null) {
                    $commonMusicFavs = Commonalities::_array_uintersect($myMusicFavourites, $profile_data[$key]['music_favorites'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonMusicFavs)> 0 ) {
                        $common_music_content_text = isset($data['common_music_content_text']) ? $data['common_music_content_text'] : 
                            array(array($prefixHtmlString."How often does %s listen to ".$colouredHtmlString." ? Ask %s!".$suffixHtmlString,$heSheString,$commonMusicFavs[array_rand($commonMusicFavs)],$himHerString),
                                  array($prefixHtmlString."Ask %s which was the last song by ".$colouredHtmlString." that %s heard!".$suffixHtmlString,$himHerString,$commonMusicFavs[array_rand($commonMusicFavs)],$heSheString),
                                  array($prefixHtmlString."Ask %s what %s favourite song is by ".$colouredHtmlString."! Maybe it's the same as yours!".$suffixHtmlString,$himHerString,$hisHerString,$commonMusicFavs[array_rand($commonMusicFavs)]));
                        $result[$key][] = call_user_func_array("sprintf",$common_music_content_text[array_rand($common_music_content_text)]);
                    }
                }

                if($profile_data[$key]['movies_favorites'] != null && $myMoviesFavourites != null) {
                    $commonMoviesFavs = Commonalities::_array_uintersect($myMoviesFavourites, $profile_data[$key]['movies_favorites'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonMoviesFavs)> 0 ) {
                        $common_movies_content_text = isset($data['common_movies_content_text']) ? $data['common_movies_content_text'] :
                            array(array($prefixHtmlString."Ask %s if %s watches ".$colouredHtmlString." on repeat?".$suffixHtmlString,$himHerString,$heSheString,$commonMoviesFavs[array_rand($commonMoviesFavs)]),
                                  array($prefixHtmlString."Ask %s how many times %s has watched ".$colouredHtmlString."!".$suffixHtmlString,$himHerString,$heSheString,$commonMoviesFavs[array_rand($commonMoviesFavs)]),
                                  array($prefixHtmlString."Ask %s if ".$colouredHtmlString." is one of %s top-ten favourites!".$suffixHtmlString,$himHerString,$commonMoviesFavs[array_rand($commonMoviesFavs)],$hisHerString));
                        $result[$key][] = call_user_func_array("sprintf",$common_movies_content_text[array_rand($common_movies_content_text)]);
                    }
                }
                
                if($profile_data[$key]['hash_tags'] != null && $myHashTags != null) {
                    $commonHashTags = Commonalities::_array_uintersect($myHashTags, $profile_data[$key]['hash_tags'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonHashTags)> 0 ) {
                        $common_tags_content_text = isset($data['common_tags_content_text']) ? $data['common_tags_content_text'] : 
                            array(array($prefixHtmlString."".$colouredHtmlString." - Wow! That's something you can start chatting about!".$suffixHtmlString,$commonHashTags[array_rand($commonHashTags)]),
                                  array($prefixHtmlString."Look! So much to chat about - ".$colouredHtmlString.$suffixHtmlString,$commonHashTags[array_rand($commonHashTags)]),
                                  array($prefixHtmlString."".$colouredHtmlString." eh? Looks like %s is too!".$suffixHtmlString,$commonHashTags[array_rand($commonHashTags)],$heSheString),
                                  array($prefixHtmlString."".$colouredHtmlString." ? Really? %s is too!".$suffixHtmlString,$commonHashTags[array_rand($commonHashTags)],$heSheCapsString),
                                  array($prefixHtmlString."You both have ".$colouredHtmlString." on your profile! Get talking.".$suffixHtmlString,$commonHashTags[array_rand($commonHashTags)]));
                        $result[$key][] = call_user_func_array("sprintf",$common_tags_content_text[array_rand($common_tags_content_text)]);
                    }
                }

                if($profile_data[$key]['books_favorites'] != null && $myBooksFavourites != null) {
                    $commonBooksFavs = Commonalities::_array_uintersect($myBooksFavourites, $profile_data[$key]['books_favorites'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonBooksFavs)> 0 ) {
                        $common_books_content_text = isset($data['common_books_content_text']) ? $data['common_books_content_text'] : 
                            array(array($prefixHtmlString."Tell %s what you're reading now, since you both like ".$colouredHtmlString.".".$suffixHtmlString,$himHerString,$commonBooksFavs[array_rand($commonBooksFavs)]),
                                  array($prefixHtmlString."You both read ".$colouredHtmlString." so why not start with a great line?".$suffixHtmlString,$commonBooksFavs[array_rand($commonBooksFavs)]),
                                  array($prefixHtmlString."What does %s like about ".$colouredHtmlString." ? Ask %s!".$suffixHtmlString,$heSheString,$commonBooksFavs[array_rand($commonBooksFavs)],$himHerString));
                        $result[$key][] = call_user_func_array("sprintf",$common_books_content_text[array_rand($common_books_content_text)]);
                    }
                }

                if($profile_data[$key]['sports_favorites'] != null && $mySportsFavourites != null) {
                    $commonSportsFavs = Commonalities::_array_uintersect($mySportsFavourites, $profile_data[$key]['sports_favorites'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonSportsFavs)> 0 ) {
                        $common_sports_content_text = isset($data['common_sports_content_text']) ? $data['common_sports_content_text'] : 
                            array(array($prefixHtmlString."Playing or just watching? Ask %s why %s too likes ".$colouredHtmlString.".".$suffixHtmlString,$himHerString,$heSheString,$commonSportsFavs[array_rand($commonSportsFavs)]));
                        $result[$key][] = call_user_func_array("sprintf",$common_sports_content_text[array_rand($common_sports_content_text)]);
                    }
                }

                if($profile_data[$key]['travel_favorites'] != null && $myTravelFavourites != null) {
                    $commonTravelFavs = Commonalities::_array_uintersect($myTravelFavourites, $profile_data[$key]['travel_favorites'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonTravelFavs)> 0 ) {
                        $common_travel_content_text = isset($data['common_travel_content_text']) ? $data['common_travel_content_text'] : 
                            array(array($prefixHtmlString."Exchange exciting experiences around ".$colouredHtmlString.".".$suffixHtmlString,$commonTravelFavs[array_rand($commonTravelFavs)]),
                                  array($prefixHtmlString."Ask %s how %s experience was at ".$colouredHtmlString.".".$suffixHtmlString,$himHerString,$hisHerString,$commonTravelFavs[array_rand($commonTravelFavs)]));
                        $result[$key][] = call_user_func_array("sprintf",$common_travel_content_text[array_rand($common_travel_content_text)]);
                    }
                }

                if($profile_data[$key]['other_favorites'] != null && $myOtherFavourites != null) {
                    $commonOtherFavs = Commonalities::_array_uintersect($myOtherFavourites, $profile_data[$key]['other_favorites'],"Commonalities::levenshteinRawStringComparison");
                    if(sizeof($commonOtherFavs)> 0 ) {
                        $common_other_content_text = isset($data['common_other_content_text']) ? $data['common_other_content_text'] : 
                            array(array($prefixHtmlString."Looks like %s likes ".$colouredHtmlString." too!".$suffixHtmlString,$heSheString,$commonOtherFavs[array_rand($commonOtherFavs)]));
                        $result[$key][] = call_user_func_array("sprintf",$common_other_content_text[array_rand($common_other_content_text)]);
                    }
                }

                if(sizeof($result[$key]) <= 0) {
                    $default_content_text = isset($data['default_content_text']) ? $data['default_content_text'] : 
                        array(array($prefixHtmlString."Got a quirky story about yourself, we're sure %s'd love to hear it!".$suffixHtmlString,$heSheString),
                              array($prefixHtmlString."What made you stop on your tracks? Let %s know!".$suffixHtmlString,$himHerString),
                              array($prefixHtmlString."At a loss for words? How about you start by complimenting %s smile.".$suffixHtmlString,$hisHerString),
                              array($prefixHtmlString."Trending news or a TV show, try talking about a popular topic!".$suffixHtmlString,$himHerString),
                              array($prefixHtmlString."Charm %s with your wit!".$suffixHtmlString,$himHerString),
                              array($prefixHtmlString."Everyone loves someone with a good sense of humour, so show you have a funny bone!".$suffixHtmlString,$himHerString));
                    $result[$key][] = call_user_func_array("sprintf",$default_content_text[array_rand($default_content_text)]);
                }
            }
            return $result;
        }
        else {
            return array();
        }
    }
}
    
