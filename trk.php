<?php
//require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";

/**
 * to capture open rate of emailers
 * @author Himanshu
 */

/**
 * check for the content type json
 */

$data = array();
if(isset($_REQUEST['utm_campaign']) && $_REQUEST['step'] == "openRate"){
	//	if($_REQUEST['utm_content'] == "view_matches")
	$event_type = $_REQUEST['utm_campaign'];
	$data[] = array("user_id" => $_REQUEST['uid'], "activity" => "emailers" , "event_type" => $event_type);
}

if(array_key_exists('content_type',$_POST)&&$_POST['content_type']=='application/json'){
	$data=json_decode($_POST['data'],true);
}
else if(isset($_REQUEST['data'])){
	$data[] = $_REQUEST['data'];
	foreach ($data as $row=>$col){
		if(isset($col['event_info'])) {
			$data[$row]['event_info'] = json_decode($col['event_info'], true); 
		}else {
			$data[$row]['event_info'] = NULL;
		}
	}
	
}

foreach ($data as $value){
	//when install is referrer(incent) install
	if($value['activity']=='install' && $value['event_type']=='install_referrer'){
		require_once dirname ( __FILE__ ) . "/abstraction/query.php";
		
		$referrer_info = $value['event_info'];
		if(is_string($referrer_info))
		$referrer_info = json_decode($referrer_info,true);
		$referrer = urldecode($referrer_info['referrer']);
		
	    preg_match("/utm_source=([^&]+)/", $referrer, $utm_source);
	    
	    $referrer_info = json_encode($referrer_info);

	    $sql= "insert ignore into device_referrer   (device_id, utm_source, referrer, timestamp) values (?,?,?,now())";
	    Query::INSERT($sql, array($value['device_id'],$utm_source[1],$referrer_info));
	}
}

// when the activity = 'install'
if($data[0]['activity']=='install' && isset($data[0]['event_info'])){

	$event_info = $data[0]['event_info'];
	if(is_string($event_info))
	$event_info = json_decode($event_info,true);
	$referrer = urldecode($event_info['referrer']);
	preg_match("/utm_content=([^&]+)/", $referrer, $txnId);
	preg_match("/utm_source=([^&]+)/", $referrer, $utm_source);
	if($utm_source[1]=='tyroo'){
		//$url = "https://tda.tyroo.com/sdk/lead.php?cac=".$txnId[1]."&advOptInfo=".$data[0]['device_id'];
		$url = "http://tyroo.go2cloud.org/aff_lsr?offer_id=171&adv_sub=".$data[0]['device_id']."&transaction_id=".$txnId[1];
		$event_info['tyroo_url'] = $url;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$response = curl_exec($ch);
		curl_close($ch);
		
		$event_info['tyroo_url_response'] = $response;
		$data[0]['event_info'] = $event_info;
	}

}

$eventTrk = new EventTrackingClass();
$eventTrk->logActions($data);


$my_img = imagecreate( 1, 1 );
$background = imagecolorallocate( $my_img, 255, 255, 255 );

header( "Content-type: image/png" );
imagepng( $my_img );
imagedestroy( $my_img );

exit();

?>