<?php

require_once dirname(__FILE__) . "/../matches/matchScore.php";
require_once dirname(__FILE__) . "/../include/Utils.php";
require_once dirname(__FILE__) . "/../fb.php";
require_once dirname(__FILE__) . "/../include/function.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/../abstraction/query.php";
require_once dirname(__FILE__) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../include/matchConstants.php";
require_once dirname(__FILE__) . "/../include/config.php";
require_once dirname(__FILE__) . "/../UserData.php";
require_once dirname(__FILE__) . "/../logging/QueryLog.class.php";
require_once dirname(__FILE__) . "/BaseRecommendationEngine.php";
require_once dirname(__FILE__) . "/../Advertising/ProfileCampaign.php";

class RecommendationEngine extends BaseRecommendationEngine {

    function __construct($user_id, $selectedABParams = NULL) {
        parent::__construct($user_id);
    }

    /**
     * Get availability filter 
     */
    private function getAvailabilityFilterString($gender) {
        $availabilityFilter = '';

        if ($gender == "M") {
            if ($this->isFreshBucket == true)
                $availabilityFilter = " and case when age>=18 and age<=20 then (  
													case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (fresh_slots_filled  < " . (2.5 * AvailabilityFilters::age_18_20) . " or fresh_slots_filled is null)
													else (fresh_slots_filled  < " . AvailabilityFilters::age_18_20 . " or fresh_slots_filled is null)
													end
													)

												when age>20 and age<=22 then(
													case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (fresh_slots_filled  < " . (2.5 * AvailabilityFilters::age_21_22) . " or fresh_slots_filled is null)
													else (fresh_slots_filled  < " . AvailabilityFilters::age_21_22 . " or fresh_slots_filled is null)
													end
													)

												when age>=23 and age<=26 then (  
                                                                                                        case 
                                                                                                        when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (fresh_slots_filled  < " . (2.5 * AvailabilityFilters::age_23_26) . " or fresh_slots_filled is null)
                                                                                                        else (fresh_slots_filled  < " . AvailabilityFilters::age_23_26 . " or fresh_slots_filled is null)
                                                                                                        end)

												when bucket>0 then (
													case 
													when bucket =1 then ( fresh_slots_filled  < " . AvailabilityFilters::bucketOne . " or fresh_slots_filled is null) 
													when bucket = 2 then ( fresh_slots_filled  < " . AvailabilityFilters::bucketTwo . "  or fresh_slots_filled is null) 
													when bucket = 3 then ( fresh_slots_filled  < " . AvailabilityFilters::bucketThree . "  or fresh_slots_filled is null)
													when bucket = 4 then ( fresh_slots_filled  < " . AvailabilityFilters::bucketFour . "  or fresh_slots_filled is null)
													else  ( fresh_slots_filled  < " . AvailabilityFilters::bucketFive . "  or fresh_slots_filled is null) 
													end) 

												else (fresh_slots_filled < " . AvailabilityFilters::bucketZero . " or fresh_slots_filled is null)  end ";
            else
                $availabilityFilter = " and case when age>=18 and age<=20 then ( 
							     					case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (slots_filled < " . (2.5 * Utils::$availability_slots_limit_18_20) . " or slots_filled is null)
													else (slots_filled < " . Utils::$availability_slots_limit_18_20 . " or slots_filled is null)
													end
													)

							     		when age>20 and age<=22 then ( 
							     					case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (slots_filled < " . (2.5 * Utils::$availability_slots_limit_21_22) . " or slots_filled is null)
													else (slots_filled < " . Utils::$availability_slots_limit_21_22 . " or slots_filled is null)
													end
													)
							     		
							     		when age>=23 and age<=26 then ( case 
                                                                                                        when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (slots_filled < " . (2.5 * Utils::$availability_slots_limit_21_22) . " or slots_filled is null)
                                                                                                        else (slots_filled < " . Utils::$availability_slots_limit_23_26 . " or slots_filled is null)
                                                                                                        end)
							     		else ( slots_filled < " . Utils::$availability_slots_limit . " or slots_filled is null) end";
        }

        return $availabilityFilter;
    }

    /**
     * NOTE: whenever you add/remove any new data point from a query, update the sanity in test cases
     * finding how many matches to find as fresh
     * in male case - 
     *      fresh -  it'll be controlled - based on bucket and active chats (follow the below logic in case of edit as well)
     *      likes - push after every x hours
     * in female case -
     *      fresh & likes - send as per quota even after edit 
     */
    public function generateMatchesUsingMemoryTableClassSpecific($user_id, $actionTaken = 0, $logString = null, $sessionId = null, $containingUsers = null, $passedFreshLimit = 0,$passedLikeLimit = 0) {


        $myPIData = $this->uuDBO->getUserPiInfo(array($user_id), true);
        $myPi = $myPIData['actual_pi'];
        $myBucket = $myPIData['bucket'];
        $this->setIsFreshBucket($myPi, $myBucket);
        $this->setBatchSizeForFemale($myPIData);
        $isFreshCall = $this->isFreshCall($actionTaken);
        if ($this->gender == "M") {
            $fetchFreshCount = $this->getFreshProfilesCountForMales($isFreshCall, $myPIData, $actionTaken);
        }
        else {
            $fetchFreshCount = $this->getFreshProfilesCountForFeMales($myPIData);
        }
        $user_id_to_filter = $this->getRecommendationSet($this->userId);
        $spark_id_to_filter = BaseRecommendationEngine::getUsersAlreadyEngagedViaSpark($this->userId);
        
        $output_sparkedMe = $spark_id_to_filter['output_sparkedMe'];
        $output_sparkedByMe  = $spark_id_to_filter['output_sparkedByMe'];
        
        $iLikedIds = $user_id_to_filter['iLiked'];
        $likedMeFilters = $user_id_to_filter['likedMe'];
        $nonLikedMe = $user_id_to_filter['nonLikedMe'];
        $total_ids = array_unique(array_merge($likedMeFilters, $nonLikedMe, $iLikedIds,$output_sparkedMe,$output_sparkedByMe));
        $nonLikedMe_ids = array_unique(array_merge($nonLikedMe, $iLikedIds,$output_sparkedMe,$output_sparkedByMe));
        $user_id_to_filter_total = implode(",", array_values($total_ids));
        $user_id_to_filter_nonLikedMe = implode(',', array_values($nonLikedMe_ids));

        $prefData           = $this->uuDBO->getUserPreferenceData(array($user_id), true);
        $myData             = $this->uuDBO->getUserData(array($user_id), true);
        $myBasicInfo        = $this->uuDBO->getUserBasicInfo(array($user_id), true);
        $myGender           = $myBasicInfo['gender'];
        $oppositeGender     = ($myGender == 'M') ? 'F' : 'M';
        $myAge              = $myData['age'];
        $myState            = $myData['stay_state'];
        $myCity             = $myData['stay_city'];
        $myCountry          = $myData['stay_country'];
        $piCaseForFemales   = $this->getPIRanksAndFiltersForFemale($myBucket, "2",$myCountry);
        $locationQuery      = $this->getLocationQuery($prefData);

        if ($myGender == "M") {
            $freshProfileQuery  = $this->getFreshProfileQueryForMales($prefData, $myData, $myGender, $myBucket);
            $likeQuery          = $this->getLikeQueryForMales();
        } else {
            $query              = $this->getQueryForFemales($prefData, $myData, $myGender, $myBucket, $user_id);
            $freshProfileQuery  = $query["freshProfileQuery"];
            $likeQuery          = $query["likeQuery"];
        }
        

        $baseQuery = $this->generateBaseQuery($prefData, $myData, $oppositeGender, $locationQuery, $myBucket);
        $userIdToFilter_Query = $this->getUserIdToFilterQuery($user_id_to_filter_total);
        $availabilityStr = $this->getAvailabilityFilterString($this->gender, $myBucket, $myPi);
        $freshProfileQuery .= $baseQuery . $userIdToFilter_Query . $availabilityStr;
        $freshProfileQuery.=" and u.haspics=1 ";
        
        $tmSelectStringFreshFilter = ($this->isSelectMemberForThisSession || $myCountry != 113) ? "" : " and u.isSelect = 0 ";
        $freshProfileQuery.=$tmSelectStringFreshFilter;
        
        if ($this->gender == "M") {
        	
            $freshProfileQuery .= " and (u.hide_flag is null or u.hide_flag = 0) ";
        }
        else
        {
            if(isset($piCaseForFemales['piFilterCaseFreshQuery'])) {
                $filterCase=$piCaseForFemales['piFilterCaseFreshQuery'];
                $freshProfileQuery .= " $filterCase  ";
                }
        }

	$notInFilterQuery = $this->getNotInFilterQuery($user_id_to_filter_nonLikedMe);
	if ($containingUsers != null) {
		#$containingUsersString = implode(',',$containingUsers);
		#$likeQuery .= " and u.user_id in (" . $containingUsersString . ") ";
		#$freshProfileQuery .= " and u.user_id in (" . $containingUsersString . ") ";
	}
        if(!($containingUsers != null && !empty($containingUsers))) {
            $freshProfileQuery .= " order by totalScore desc, u.user_id%SECOND(now())  ";
        }
        else {
            # Sorting the sequence on the basis of remainder obtained by the remainder from the minimum possible user id
            $freshProfileQuery .= " order by totalScore desc, u.user_id%($this->userId%209)  "; 
        }
    
	$like_limit = null;    
	if ($myGender == "M") {
		#Male Query has no limit on females who have liked her
            if($containingUsers != null  && !empty($containingUsers) && $passedLikeLimit >= 0) {
                $likeQuery .= $notInFilterQuery . " limit ". $passedLikeLimit;
                $like_limit = $passedLikeLimit;
            }
            else {
                $likeQuery .= $notInFilterQuery;
            }
	} else {
		if($containingUsers != null && !empty($containingUsers)) {
                    if($passedLikeLimit >= 0) {
			#Override passed like limit when an external list is passed
			$likeQuery .= $baseQuery . $notInFilterQuery . $piCaseForFemales['piFilterCaseLikeQuery'] . "  order by totalScore desc, u.user_id%($this->userId%209)   limit " . $passedLikeLimit;
			$like_limit = $passedLikeLimit;
                    }
                    else {
                        $likeQuery .= $baseQuery . $notInFilterQuery . $piCaseForFemales['piFilterCaseLikeQuery'] . "  order by totalScore desc, u.user_id%($this->userId%209)";
                    }
		}else {
			$limit = max(Utils::$femaleMutualFriendsLikedMatchesLimit, Utils::$like_threshold_female);
			$like_limit = Utils::$like_threshold_female;
			$likeQuery .= $baseQuery . $notInFilterQuery . $piCaseForFemales['piFilterCaseLikeQuery'] . "  order by totalScore desc , u.user_id%SECOND(now())   limit " . $limit;
		}
        }
        $result_like = array();
        $result_fresh = array();
        $freshCompeteWithLikes = null;
        $countToFetchFresh = $fetchFreshCount;
        $store_second_key = true;
        try {
            if (!empty($likedMeFilters) && isset($likedMeFilters)) {
                $result_like = Query::SELECT($likeQuery, null);
            }
            $limit = 0;	
            if ($myGender == "M") {
                $countToFetchFresh = $fetchFreshCount;
		if($containingUsers != null && !empty($containingUsers)) {
                    if($passedFreshLimit >= 0) {
                    	$fetchFreshCount = $passedFreshLimit;
                    	$store_second_key = false;
			$freshProfileQuery .= "  limit $passedFreshLimit";
                    }
		}else {
			$likeQueryCount = count($result_like);
			$limit = $fetchFreshCount;
			if (Utils::$maleMutualFriendsTotalMatchesLimit - $likeQueryCount > 0)
				$limit = Utils::$maleMutualFriendsTotalMatchesLimit - $likeQueryCount;
			$limit = max($limit, $fetchFreshCount);
			if ($fetchFreshCount == 0) $limit = 0;
			$freshProfileQuery .= "  limit $limit";
		}
                if ($fetchFreshCount >= 0 || $limit > 0) {
                    $result_fresh = Query::SELECT($freshProfileQuery, null);
                }
            } else {
                $freshCompeteWithLikes = true;
                $likeQueryCount = count($result_like);
                if ($likeQueryCount < ($this->totalProfilesForFemale - $fetchFreshCount)) {
                    $fetchFreshCount = $this->totalProfilesForFemale - $likeQueryCount;
                    $freshCompeteWithLikes = false;
                }
                $countToFetchFresh = $fetchFreshCount;
		if($containingUsers != null && !empty($containingUsers)) {
                    if($passedFreshLimit >= 0) {
                    	$fetchFreshCount = $passedFreshLimit;
                    	$store_second_key = false;
			$freshProfileQuery .= " limit $passedFreshLimit";
                    }
		}else {
			$limit = $fetchFreshCount;
			if (Utils::$femaleMutualFriendsTotalMatchesLimit - $likeQueryCount > 0)
				$limit = Utils::$femaleMutualFriendsTotalMatchesLimit - $likeQueryCount;
			$limit = max($limit, $fetchFreshCount);
			if ($fetchFreshCount == 0) $limit = 0;
			$freshProfileQuery .= " limit $limit";
		}
                $result_fresh = Query::SELECT($freshProfileQuery, null);
            }
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
        }
        $idList = null;
        $likedIds = array();
        $result_fresh_set = array();
        $result_like_set = array();
        $scoreData_fresh_set = array();
        $scoreData_like_set = array();
        $location_sort_set = array();
        $availabile_set = array();
        $i = 0;
        $count_liked = 0;
        foreach ($result_like as $row) {
        	// This is done so that existing code modification is less
            if ($like_limit != null && $count_liked >= $like_limit)
                break;
            $likedIds[$row['user_id']] = $row['user_id'];
            $result_like_set[$row['user_id']] = $row['score'];
            $scoreData_like_set[$row['user_id']] = array(
                'likeFlag' => $row['likeflag'],
                'bucket' => $row['bucket'],
                'pi' => $row['pi'],
                'location' => $row['locationScore'],
                'piScore' => $row['piScore'],
                'activityScore' => $row['activityScore'],
                'educationScore' => $row['educationScore'],
                'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
                'availabilityScore' => $row['availabilityScore'],
                'total' => $row['score']);
              
            $location_sort_set[$row['user_id']] = $row['locationScore'];
            $idList[$i] = $row['user_id'];
            $availabile_set[$row['user_id']] = "0";
            if ($this->gender == "F" && $row['piScore'] > 0) {
                if ($row['bucket'] == 0 || $row['pi'] == -1) {
                    $availabile_set[$row['user_id']] = "F";
                } else {
                    $availabile_set[$row['user_id']] = "B";
                }
            }
            $i++;
            $count_liked ++;
        }

        $count_fresh = 0;
        foreach ($result_fresh as $row) {
            if ($count_fresh >= $fetchFreshCount)
                break;
            $result_fresh_set[$row['user_id']] = $row['score'];
            $scoreData_fresh_set[$row['user_id']] = array(
                'likeFlag' => $row['likeflag'],
                'bucket' => $row['bucket'],
                'pi' => $row['pi'],
                'location' => $row['locationScore'],
                'piScore' => $row['piScore'],
                'activityScore' => $row['activityScore'],
                'availabilityScore' => $row['availabilityScore'],
            	'educationScore' => $row['educationScore'],
                'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
                'total' => $row['score']);
            $location_sort_set[$row['user_id']] = $row['locationScore'];
            $idList[$i] = $row['user_id'];
            $availabile_set[$row['user_id']] = "0";

            if ($this->gender == "M" && in_array($myBucket, array(4, 5))) {
                $availabile_set[$row['user_id']] = "B";
            } else if ($this->gender == "M" && $row['piScore'] > 0) {
                if ($this->isFreshBucket == true)
                    $availabile_set[$row['user_id']] = "F";
                else
                    $availabile_set[$row['user_id']] = "B";
            }
            $i++;
            $count_fresh ++;
        }

        $remaining_fresh_set = array();
        $remaining_fresh_set_scores = array();
        $remaining_availabile_set = array();
        for ($index = $count_fresh; $index < count($result_fresh); $index++) {
        	$row = $result_fresh[$index];
        	$remaining_fresh_set[$row['user_id']] = $row['score'];
        	$remaining_fresh_set_scores[$row['user_id']] = array(
        			'likeFlag' => $row['likeflag'],
        			'bucket' => $row['bucket'],
        			'pi' => $row['pi'],
        			'location' => $row['locationScore'],
        			'piScore' => $row['piScore'],
        			'activityScore' => $row['activityScore'],
        			'availabilityScore' => $row['availabilityScore'],
        			'educationScore' => $row['educationScore'],
                                'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                                'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
        			'total' => $row['score']);
        	
        	$remaining_availabile_set[$row['user_id']] = "0";
        	
        	if ($this->gender == "M" && in_array($myBucket, array(4, 5))) {
        		$remaining_availabile_set[$row['user_id']] = "B";
        	} else if ($this->gender == "M" && $row['piScore'] > 0) {
        		if ($this->isFreshBucket == true)
        			$remaining_availabile_set[$row['user_id']] = "F";
        			else
        				$remaining_availabile_set[$row['user_id']] = "B";
        	}
        	 
        }
        
        $remaining_liked_set = array();
        $remaining_liked_set_scores = array();
        $remaining_liked_ids = array();
        for ($index = $count_liked; $index < count($result_like); $index ++) {
        	$row = $result_like[$index];
        	$remaining_liked_ids[$row['user_id']] = $row['user_id'];
        	$remaining_liked_set[$row['user_id']] = $row['score'];
        	$remaining_liked_set_scores[$row['user_id']] = array(
        			'likeFlag' => $row['likeflag'],
        			'bucket' => $row['bucket'],
        			'pi' => $row['pi'],
        			'location' => $row['locationScore'],
        			'piScore' => $row['piScore'],
        			'activityScore' => $row['activityScore'],
        			'availabilityScore' => $row['availabilityScore'],
        			'educationScore' => $row['educationScore'],
                                'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                                'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
        			'total' => $row['score']);
        
        	$remaining_availabile_set[$row['user_id']] = "0";
        	if ($this->gender == "F" && $row['piScore'] > 0) {
        		if ($row['bucket'] == 0 || $row['pi'] == -1) {
        			$remaining_availabile_set[$row['user_id']] = "F";
        		} else {
        			$remaining_availabile_set[$row['user_id']] = "B";
        		}
        	}
        }

        // TODO: if diff between availability here and there, then update table
        // While running this query , a diff might occur during redis flush and query then inconsistent data
        //TODO: if action is taken on first 30 which is mix of like and fresh then rest is mix of both again?
        //$total_set = $result_like_set + $result_fresh_set;
        $total_score_set = $scoreData_like_set + $scoreData_fresh_set;

        if(!($containingUsers != null && !empty($containingUsers))) {
            $final_set = $this->sortAndShuffleIds($result_like_set, $result_fresh_set, $freshCompeteWithLikes);
        }
        else {
            $final_set = $result_like_set + $result_fresh_set;
            $final_set = array_keys($final_set);
        }
        
        $res = $this->extraneousProfileHandler->handleExtraneousProfiles($final_set, $myGender, $myAge, $myState, $myCity, $sessionId, ($this->isSelectMember || $myCountry != 113) ? array() : array(), array());
        $final_set      = $res["final_set"];
        $repeat_likes   = $res["repeat_likes"];
        $selectSprinkledProfiles = $res["selectSprinkledProfiles"];

        $final_score_set = array();
        foreach ($final_set as $key) {
            if (key_exists($key, $total_score_set)) {
                $final_score_set[$key] = $total_score_set[$key];
            }
	}
	$aggregateInfoOfMatches = array();
	$aggregateInfoOfMatches['final_set'] = $final_set;
        $aggregateInfoOfMatches['repeat_likes'] = $repeat_likes;
        $aggregateInfoOfMatches['selectSprinkledProfiles'] = $selectSprinkledProfiles;
        
	$aggregateInfoOfMatches['liked_ids'] = $likedIds;
	$aggregateInfoOfMatches['myGender'] = $myGender;
	$aggregateInfoOfMatches['availabile_set'] = $availabile_set;
	$aggregateInfoOfMatches['actionTaken'] = $actionTaken;

	$aggregateInfoOfMatches['final_score_set'] = $final_score_set;
	$aggregateInfoOfMatches['result_like_set'] = $result_like_set;
	$aggregateInfoOfMatches['likeQuery'] = $likeQuery;
	$aggregateInfoOfMatches['result_fresh_set'] = $result_fresh_set;
	$aggregateInfoOfMatches['freshProfileQuery'] = $freshProfileQuery;
	$aggregateInfoOfMatches['countToFetchFresh'] = $countToFetchFresh;
	$aggregateInfoOfMatches['logString'] = $logString;
	
	$aggregateInfoOfMatches['final_set_second_key'] = array_merge($final_set, array_merge(array_keys($remaining_fresh_set), array_keys($remaining_liked_set)));
	$aggregateInfoOfMatches['liked_ids_second_key'] = $likedIds + $remaining_liked_ids;
	$aggregateInfoOfMatches['availabile_set_second_key'] = $availabile_set + $remaining_availabile_set;
	$aggregateInfoOfMatches['final_score_set_second_key'] = $final_score_set + $remaining_fresh_set_scores + $remaining_liked_set_scores;
	$aggregateInfoOfMatches['store_second_key'] = $store_second_key;

	return $aggregateInfoOfMatches;
    }

    public function generateMatchesUsingMemoryTable($user_id, $actionTaken = 0, $logString = null, $sessionId = null) {
	    $aggregateInfoOfMatches = $this->generateMatchesUsingMemoryTableClassSpecific($user_id,$actionTaken,$logString,$sessionId);

	    $final_set = $aggregateInfoOfMatches['final_set'];
	    $likedIds = $aggregateInfoOfMatches['liked_ids'];
	    $myGender = $aggregateInfoOfMatches['myGender'];
	    $availabile_set = $aggregateInfoOfMatches['availabile_set'];
	    $actionTaken = $aggregateInfoOfMatches['actionTaken'];
	    
	    $final_set_second_key = $aggregateInfoOfMatches['final_set_second_key'];
	    $availabile_set_second_key = $aggregateInfoOfMatches['availabile_set_second_key'];
	    $score_set_second_key = $aggregateInfoOfMatches['final_score_set_second_key'];
	    $liked_ids_second_key = $aggregateInfoOfMatches['liked_ids_second_key'];
        //add to redis
	    $this->addMatchesToSet($final_set, $likedIds, $myGender, $availabile_set);
	    if (Utils::$mutual_friends_enabled && isset($aggregateInfoOfMatches['store_second_key']) && $aggregateInfoOfMatches['store_second_key'])
	    	$this->checkAndAddMatchesToSetWithScores($final_set_second_key, $liked_ids_second_key, $myGender, $availabile_set_second_key, $score_set_second_key);
	    $this->setExpiryForRedisKeys($actionTaken);

	//Do Logging
	    $final_score_set = $aggregateInfoOfMatches['final_score_set'];
	    $result_like_set = $aggregateInfoOfMatches['result_like_set'];
	    $likeQuery = $aggregateInfoOfMatches['likeQuery'];
	    $result_fresh_set = $aggregateInfoOfMatches['result_fresh_set'];
	    $freshProfileQuery = $aggregateInfoOfMatches['freshProfileQuery'];
	    $countToFetchFresh = $aggregateInfoOfMatches['countToFetchFresh'];
	    $logString = $aggregateInfoOfMatches['logString'];
	    $this->doLogging($final_score_set, $result_like_set, $likeQuery, $result_fresh_set, $freshProfileQuery, $countToFetchFresh, $logString);

            return($aggregateInfoOfMatches);
    }

}

?>
