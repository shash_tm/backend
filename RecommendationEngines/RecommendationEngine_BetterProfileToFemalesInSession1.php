<?php

require_once dirname(__FILE__) . "/../matches/matchScore.php";
require_once dirname(__FILE__) . "/../include/Utils.php";
require_once dirname(__FILE__) . "/../fb.php";
require_once dirname(__FILE__) . "/../include/function.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/../abstraction/query.php";
require_once dirname(__FILE__) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../include/matchConstants.php";
require_once dirname(__FILE__) . "/../include/config.php";
require_once dirname(__FILE__) . "/../UserData.php";
require_once dirname(__FILE__) . "/../logging/QueryLog.class.php";
require_once dirname(__FILE__) . "/BaseRecommendationEngine.php";
require_once dirname(__FILE__) . "/RecommendationEngine.php";
require_once dirname(__FILE__) . "/../Advertising/ProfileCampaign.php";

class RecommendationEngine_BetterProfileToFemalesInSession1 extends RecommendationEngine {

    function __construct($user_id, $selectedABParams = NULL) {
        parent::__construct($user_id);
    }

    /**
     * Get the rank case for buckets/pi
     * @Vikram Says : We would like to provide males from best bucket (i.e. bucket number 5)
     * to the females of the given age group ( 18-21) in the session number 1 
     * or in case they have not taken any actions yet. Hence the $bucket 
     * parameter becomes useless/redundent and is there only to maintain 
     * the saintity of the function
     */
    protected function getPIRanksAndFiltersForFemale($bucket, $piScore) {

        $piRankCaseLikeQuery = " $piScore ";
        $piRankCaseFreshQuery = " case when u.bucket In (5) then $piScore else 0 end ";
        $piFilterCaseLikeQuery = " and u.bucket in (5) ";

        return array(
            "piRankCaseLikeQuery"   => $piRankCaseLikeQuery,
            "piFilterCaseLikeQuery" => $piFilterCaseLikeQuery,
            "piRankCaseFreshQuery"  => $piRankCaseFreshQuery);
    }
}

?>
