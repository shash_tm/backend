<?php

require_once dirname(__FILE__) . "/../matches/matchScore.php";
require_once dirname(__FILE__) . "/../include/Utils.php";
require_once dirname(__FILE__) . "/../fb.php";
require_once dirname(__FILE__) . "/../include/function.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/../abstraction/query.php";
require_once dirname(__FILE__) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../include/matchConstants.php";
require_once dirname(__FILE__) . "/../include/config.php";
require_once dirname(__FILE__) . "/../UserData.php";
require_once dirname(__FILE__) . "/../logging/QueryLog.class.php";
require_once dirname(__FILE__) . "/BaseRecommendationEngine.php";
require_once dirname(__FILE__) . "/../Advertising/ProfileCampaign.php";

class RecommendationEngine_RetriveHighestBucketMatches extends BaseRecommendationEngine {
    private $fetchFreshCount = -1;
    function __construct($user_id, $selectedABParams = NULL) {
        parent::__construct($user_id);
    }
    
    public function setFreshCountToFetch($count) {
        $this->fetchFreshCount = $count;
    }
    
    public function doLogging($final_score_set, $result_like_set, $likeQuery, $result_fresh_set, $freshProfileQuery, $countToFetchFresh, $logString) {
        $current_timestamp = date('Y-m-d H:i:s');
        $this->logIndividualScore($final_score_set, $logString,$current_timestamp);
        $this->storeResults($result_fresh_set, $freshProfileQuery, 0, $logString, $countToFetchFresh,$current_timestamp);
    }
    
    public function generateMatchesUsingMemoryTableClassSpecific($user_id, $actionTaken = 0, $logString = null, $sessionId = null, $containingUsers = null, $passedFreshLimit = 0,$passedLikeLimit = 0) {
	    #In this class containingUsers, passedFreshLimit and passedLikeLimit are being ignored
        $results            = $this->getMatches($user_id,$actionTaken, $logString);
        $final_set          = $results["final_set"];
        $availabile_set     = $results["availabile_set"];
        $total_score_set    = $results["total_score_set"];
        $result_fresh_set   = $results["result_fresh_set"];
        $freshProfileQuery  = $results["freshProfileQuery"];
        $myGender           = $results["myGender"];     
                

        $final_score_set = array();
        foreach ($final_set as $key) {
            if (key_exists($key, $total_score_set)) {
                $final_score_set[$key] = $total_score_set[$key];
            }
        }
	$aggregateInfoOfMatches = array();
	$aggregateInfoOfMatches['final_set'] = $final_set;
	$aggregateInfoOfMatches['liked_ids'] = array();
	$aggregateInfoOfMatches['myGender'] = $myGender;
	$aggregateInfoOfMatches['availabile_set'] = $availabile_set;
	$aggregateInfoOfMatches['actionTaken'] = $actionTaken;

	$aggregateInfoOfMatches['final_score_set'] = $final_score_set;
	$aggregateInfoOfMatches['result_like_set'] = array();
	$aggregateInfoOfMatches['likeQuery'] = "";
	$aggregateInfoOfMatches['result_fresh_set'] = $result_fresh_set;
	$aggregateInfoOfMatches['freshProfileQuery'] = $freshProfileQuery;
	$aggregateInfoOfMatches['countToFetchFresh'] = 5;
	$aggregateInfoOfMatches['logString'] = $logString;

	return $aggregateInfoOfMatches;
    }
    
    public function generateMatchesUsingMemoryTable($user_id, $actionTaken = 0, $logString = null, $sessionId = null) {
	    $aggregateInfoOfMatches = $this->generateMatchesUsingMemoryTableClassSpecific($user_id,$actionTaken,$logString,$sessionId);

	    $final_set = $aggregateInfoOfMatches['final_set'];
	    $likedIds = $aggregateInfoOfMatches['liked_ids'];
	    $myGender = $aggregateInfoOfMatches['myGender'];
	    $availabile_set = $aggregateInfoOfMatches['availabile_set'];
	    $actionTaken = $aggregateInfoOfMatches['actionTaken'];

        //add to redis
	    $this->addMatchesToSet($final_set, $likedIds, $myGender, $availabile_set);
	    $this->setExpiryForRedisKeys($actionTaken);

	//Do Logging
	    $final_score_set = $aggregateInfoOfMatches['final_score_set'];
	    $result_like_set = $aggregateInfoOfMatches['result_like_set'];
	    $likeQuery = $aggregateInfoOfMatches['likeQuery'];
	    $result_fresh_set = $aggregateInfoOfMatches['result_fresh_set'];
	    $freshProfileQuery = $aggregateInfoOfMatches['freshProfileQuery'];
	    $countToFetchFresh = $aggregateInfoOfMatches['countToFetchFresh'];
	    $logString = $aggregateInfoOfMatches['logString'];
	    $this->doLogging($final_score_set, $result_like_set, $likeQuery, $result_fresh_set, $freshProfileQuery, $countToFetchFresh, $logString);

    }
    /**
     * Get the rank case for buckets/pi
     */
    protected function getPIRankForMale($bucket, $piScore) {
        $piRankCase = "case when u.bucket = 5 then $piScore else 0 end ";
        return $piRankCase;
    }
    
    protected function getPIRanksAndFiltersForFemale($bucket, $piScore) {
        $piRankCaseFreshQuery = "case when u.bucket = 5 then $piScore else 0 end ";
        return $piRankCaseFreshQuery;
    }
    
    protected function getQueryForFemales($prefData, $myData, $myGender, $myBucket, $user_id) {
        $prefLocation = $prefData['location'];
        $myCity = $myData['stay_city'];
        $myState = $myData['stay_state'];
        $myCountry = $myData['stay_country'];

        $locationCase = $this->getLocationCase($prefLocation, $myCity, $myState, $myCountry, $myGender);
        $piRankCaseFreshQuery = $this->getPIRanksAndFiltersForFemale($myBucket, "2");
        #$activityCase = $this->getActivityCase();
        $activityCase = "case when upa.activity_bits_number = 0 then 0 else (case when upa.activity_bits_number%pow(2,3) > 0 then 1 else (case when upa.activity_bits_number%pow(2,7) > 0 then 0.75 else 0.5 end ) end ) end ";

        $freshProfileQuery = "select u.user_id,  0 as likeflag, $locationCase as locationScore, u.pi as pi, u.bucket as bucket, " . $piRankCaseFreshQuery . " as piScore, $activityCase as activityScore,
			$locationCase + " . $piRankCaseFreshQuery . " + $activityCase   as totalScore,
			$locationCase + " . $piRankCaseFreshQuery . " + $activityCase  as score
					 FROM user_search  u left join user_pi_activity upa on u.user_id = upa.user_id ";

        return $freshProfileQuery;
    }
    
    public function getMatches($user_id,$actionTaken = 0, $logString = null) {
        
        $myPIData = $this->uuDBO->getUserPiInfo(array($user_id), true);
        $myPi = $myPIData['actual_pi'];
        $myBucket = $myPIData['bucket'];
        $this->setIsFreshBucket($myPi, $myBucket);
        $this->setBatchSizeForFemale($myPIData);
        if($this->fetchFreshCount < 0) {
            if ($this->gender == "M") {
                $this->fetchFreshCount = $this->getFreshProfilesCountForMales(true, $myPIData, $actionTaken);
            } else {
                $this->fetchFreshCount = $this->getFreshProfilesCountForFeMales($myPIData);
            }
        }
        $user_id_to_filter = $this->getRecommendationSet($this->userId);
        $spark_id_to_filter = BaseRecommendationEngine::getUsersAlreadyEngagedViaSpark($this->userId);
        $output_sparkedMe = $spark_id_to_filter['output_sparkedMe'];
        $output_sparkedByMe  = $spark_id_to_filter['output_sparkedByMe'];
        
        $iLikedIds = $user_id_to_filter['iLiked'];
        $likedMeFilters = $user_id_to_filter['likedMe'];
        $nonLikedMe = $user_id_to_filter['nonLikedMe'];
        
        $total_ids = array_unique(array_merge($likedMeFilters, $nonLikedMe, $iLikedIds,$output_sparkedMe,$output_sparkedByMe));
        $user_id_to_filter_total = implode(",", array_values($total_ids));

        $prefData           = $this->uuDBO->getUserPreferenceData(array($user_id), true);
        $myData             = $this->uuDBO->getUserData(array($user_id), true);
        $myBasicInfo        = $this->uuDBO->getUserBasicInfo(array($user_id), true);
        $myGender           = $myBasicInfo['gender'];
        $oppositeGender     = ($myGender == 'M') ? 'F' : 'M';
        $locationQuery      = $this->getLocationQuery($prefData);
        $myCountry          = $myData['stay_country'];

        if ($myGender == "M") {
            $freshProfileQuery  = $this->getFreshProfileQueryForMales($prefData, $myData, $myGender, $myBucket);
        } else {
            $freshProfileQuery  = $this->getQueryForFemales($prefData, $myData, $myGender, $myBucket, $user_id);
        }

        $baseQuery = $this->generateBaseQuery($prefData, $myData, $oppositeGender, $locationQuery, $myBucket);
        $userIdToFilter_Query = $this->getUserIdToFilterQuery($user_id_to_filter_total);
        $freshProfileQuery .= $baseQuery . $userIdToFilter_Query;
        $freshProfileQuery.=" and u.haspics=1 ";
        
        $tmSelectStringFreshFilter = ($this->isSelectMemberForThisSession || $myCountry != 113) ? "" : " and u.isSelect = 0 ";
        $freshProfileQuery.=$tmSelectStringFreshFilter;
        
        if ($this->gender == "M") {
            $freshProfileQuery .= " and (u.hide_flag is null or u.hide_flag = 0) ";
        }

        $freshProfileQuery .= "   order by totalScore desc, u.user_id%SECOND(now())  ";

        $result_fresh = array();
        try {
            $freshProfileQuery .= "  limit $this->fetchFreshCount";
            $result_fresh = Query::SELECT($freshProfileQuery, null);
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
        }

        $idList = null;
        $result_fresh_set = array();
        $scoreData_fresh_set = array();
        $location_sort_set = array();
        $availabile_set = array();
        $i = 0;
        

        foreach ($result_fresh as $row) {
            $result_fresh_set[$row['user_id']] = $row['score'];
            $scoreData_fresh_set[$row['user_id']] = array(
                'likeFlag' => $row['likeflag'],
                'bucket' => $row['bucket'],
                'pi' => $row['pi'],
                'location' => $row['locationScore'],
                'piScore' => $row['piScore'],
                'activityScore' => $row['activityScore'],
                'availabilityScore' => $row['availabilityScore'],
                'total' => $row['score']);
            $location_sort_set[$row['user_id']] = $row['locationScore'];
            $idList[$i] = $row['user_id'];
            $availabile_set[$row['user_id']] = "0";

            if ($this->gender == "M" && in_array($myBucket, array(4, 5))) {
                $availabile_set[$row['user_id']] = "B";
            } else if ($this->gender == "M" && $row['piScore'] > 0) {
                if ($this->isFreshBucket == true)
                    $availabile_set[$row['user_id']] = "F";
                else
                    $availabile_set[$row['user_id']] = "B";
            }
            $i++;
        }


        // TODO: if diff between availability here and there, then update table
        // While running this query , a diff might occur during redis flush and query then inconsistent data
        //TODO: if action is taken on first 30 which is mix of like and fresh then rest is mix of both again?
        //$total_set =  $result_fresh_set;
        $total_score_set = $scoreData_fresh_set;

		$final_set = $this->sortAndShuffleIds(array(), $result_fresh_set, false);
        // Handle MISS TM id here
        $final_set = $this->extraneousProfileHandler->handleUnwantedProfiles($final_set);
        
        return array(
            "final_set"         => $final_set,
            "availabile_set"    => $availabile_set,
            "total_score_set"   => $total_score_set,
            "result_fresh_set"  => $result_fresh_set,
            "freshProfileQuery" => $freshProfileQuery,
            "myGender"          => $myGender);
    }
}
?>
