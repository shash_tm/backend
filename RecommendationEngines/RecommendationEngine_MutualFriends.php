<?php

require_once dirname(__FILE__) . "/../matches/matchScore.php";
require_once dirname(__FILE__) . "/../include/Utils.php";
require_once dirname(__FILE__) . "/../fb.php";
require_once dirname(__FILE__) . "/../include/function.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/../abstraction/query.php";
require_once dirname(__FILE__) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../include/matchConstants.php";
require_once dirname(__FILE__) . "/../include/config.php";
require_once dirname(__FILE__) . "/../UserData.php";
require_once dirname(__FILE__) . "/../logging/QueryLog.class.php";
require_once dirname(__FILE__) . "/BaseRecommendationEngine.php";
require_once dirname(__FILE__) . "/../Advertising/ProfileCampaign.php";
require_once dirname(__FILE__) . "/../mutual_friends/mutual_friends.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/Utils/RecommendationUtils.php";

/**
 * This Recommendation Engine will/should be used when batch_count = 2. This Engine won't get fresh matches making
 * db queries. It'll only process matches from redis. Here's the overview of the flow
 * 
1. Client makes matches call with batchCount=1, mutual_friends=true, server stores 2 matches lists in redis, 
if feature active(user ab test)
    ----> list1 -> Female -> 90 liked profiles + 10 fresh profiles (ideally) + Campaign profiles
                -> Male -> all liked + 10 fresh
    ----> list2 with scores -> Female -> 360 liked + 40 fresh (here top entries are same as list1)
                                      -> Male -> all liked + fresh (limit 100)
                                      -> list2 is stored if feature is active for user, same expiry time as list1
    ----> server sends back 30 profiles from list1

2. when user takes action on match, its removed from both lists

3. Client makes matches call with batchCount=2, mutual_friends=true.
   ----> If feature active(user ab test), list2 is picked
             1. get mutual friends count by making parallel batch calls, add mutual friends score, 0.25 for all matches with mutual friends
             2. all campaign profiles removed, and 2 lists are made
                 ----> liked list(360) -> sort based on total score (stable sort) + if first x profiles with mutual friends 
                       shuffle first 3x profiles
                 ----> fresh list(40) -> sort + shuffle
             3. get the counts of liked and fresh profiles remaining from list1 and a merged list is made
                 ----> merged list -> get top profiles from liked list and fresh list (from above counts) + add campaign profiles
                                   -> order of profiles depends on old logic +  if first x profiles with mutual friends 
                                      shuffle first 3x profiles
             4. delete both list1 and list2 and make merged list as list1
             5. server sends back all remaining profiles(list1)

 * 
 * */

class RecommendationEngine_MutualFriends extends BaseRecommendationEngine {
	private $params = array();
//	private $gender;

	function __construct($user_id, $selectedABParams) {
		$this->params = $selectedABParams;
		parent::__construct($user_id);
	}
	
	/**
	 * PLEASE READ THE COMMENTS AT THE BEGINNING OF THIS FILE.
	 * 
	 * picks list2, finds mutual friends counts, adds 0.25 score for matches with mf, sort the list based on total score, 
	 * deletes list2
	 * */
	public function fetchResults($logString = null, $sessionId = null) {
		// get result from redis
		$result = parent::fetchResults($logString, $sessionId);
		$result['isSelect'] = $this->isSelectMember;
		$mutual_friends_data = array();
		$header = Utils::getAllSystemHeaderFields();
		$mutual_friends_flag = isset($header['mutual_friends']) ? $header['mutual_friends'] : False;
		$matches_scores_key_exists = $this->uu->ifKeyExists($this->keyWithScores);
                $batchCount = isset($header['batchCount']) ? $header['batchCount'] : -1;
                $sessionId  = RecommendationUtils::getSetUserCurrentSession($this->userId,1);
		
		// case when key 2 present - reordering has to be done considering mf
		if (Utils::$mutual_friends_enabled && $mutual_friends_flag && $matches_scores_key_exists) {
			
			$matches_with_scores = $this->redis->LRANGE($this->keyWithScores, 0, -1);
			
			
			// $remaining_matches_count - count of matches in the final matches list
			$remaining_matches_count = $this->redis->LLEN($this->key);
			$remaining_matches_count = $remaining_matches_count > 0 ? $remaining_matches_count - 1 : 0;
			
			$matches_scores_map = array();
			if ($matches_with_scores != null && count($matches_with_scores) > 0)
				$matches_scores_map = $this->uu->getMatchesWithScores($matches_with_scores);
			
			$matches_redis_map = $this->uu->getMatchesDataMap($matches_with_scores);
			$matches_ids = array();
			$matches_ids = array_keys($matches_redis_map);
			
			// matches list1 from key1
			$matches_data_key1 = $this->redis->LRANGE($this->key, 0, -1);
			$key = array_search('-1', $matches_data_key1);
			// $remaining_liked_matches_count - count of liked matches in the final matches list
			$remaining_liked_matches_count = $this->uu->getLikedMatchesCount($matches_data_key1);
			$matches_data_key1 = $this->uu->getMatchesDataMapForFirstKey($matches_data_key1);
			
			/* Filter out sponsored profiles from list2. This is done because later on all the matches will sorted based on
			   total score and sponsored profiles will all go down the list. So they're removed now and 
			   inserted back again at the end */
			$sponsored_ids_position_map = $this->get_sponsored_ids_position_map($matches_data_key1, $matches_scores_map);
			$matches_scores_map = $this->filter_sponsored_profiles($matches_scores_map, array_values($sponsored_ids_position_map));
			
			// get mutual friends counts with matches by making api call
			$mf = new MutualFriends();
			$mutual_friends_data = $mf->get_mutual_friends_data($this->userId, $matches_ids);
			
			// reorder list2 considering mutual friends score
			$response = $this->reorder_matches_list($this->userId, $matches_scores_map, $mutual_friends_data, $matches_redis_map, 
					$remaining_matches_count, $remaining_liked_matches_count, $sponsored_ids_position_map, $sessionId);
			$reordered_list = $response['reordered_map'];
			$liked_matches  = $response['liked_matches'];
			$fresh_matches  = $response['fresh_matches'];
                        $repeat_likes   = $response['repeat_likes'];
                        $selectSprinkledProfiles = $response['selectSprinkledProfiles'];
                        $profilesAdCampaigns = $response['profilesAdCampaigns'];
                        
			$reordered_data_map = array();
			foreach ($reordered_list as $key => $value) {
				if (isset($matches_redis_map[$key]))
					$reordered_data_map[$key] = $matches_redis_map[$key];
			}
			
			$final_list = array_values($reordered_data_map);
			$result['recommendations'] = $final_list;
			$result['mutualFriendsData'] = $mutual_friends_data;			
			$result['repeat_likes']=$repeat_likes;
			$result['selectSprinkledProfiles']=$selectSprinkledProfiles;
                        $result['profilesAdCampaigns']=$profilesAdCampaigns;
			
			// delete both keys and make processed list as final key, do logging
			$this->redis->DEL($this->keyWithScores);
			
			if (count($final_list) > 0) {
				$redis_list = array();
				$final_score_set = array();
				
				foreach ($final_list as $data) {
					$key = $this->uu->getUserIdFromMatchData($data);
					$redis_list[] = $reordered_data_map[$key];
					$final_score_set[$key] = array('total' => $reordered_list[$key],
							'mutualFriendsScore' => (isset($mutual_friends_data[$key]) && $mutual_friends_data[$key]['fb_count'] > 0) 
												? Utils::$mutual_friends_weight : 0);
				}
				
				// delete both keys and make processed list as final key, set ttl for final key
				$ttl_first_key = $this->redis->TTL($this->key);
				if ($ttl_first_key == null) $ttl_first_key = 0;
				$ttl_first_key = $ttl_first_key > 1 ? $ttl_first_key - 2 : 0;
				$this->redis->DEL($this->key);
				
				$redis_list = array_merge(array($this->key, "-1"), $redis_list);
				call_user_func_array(array($this->redis, 'RPUSH'), $redis_list);
				$this->uu->setExpiry($this->key, $ttl_first_key);
				
				// logging
				$final_liked_set = array();
				foreach ($liked_matches as $m) $final_liked_set[$m] = $reordered_list[$m];
				$final_fresh_set = array();
				foreach ($fresh_matches as $m) $final_fresh_set[$m] = $reordered_list[$m];
				$this->doLogging($final_score_set, $final_liked_set, "", $final_fresh_set, "", count($fresh_matches), 
					$logString);
			}
		}
	
		/* By now reordering should've been done and 2nd key has been deleted already, 1st key itself the reordered one.
		 * This is to handle the case where matches batchcount=2 call comes mutliple times (shouldn't happen ideally) to send mf data
		 * Currently, as we're not storing the mutual friends counts in db, this code doesn't do anything.
		 */
		else if (Utils::$mutual_friends_enabled && $mutual_friends_flag && !$matches_scores_key_exists) {
			$final_list = $this->redis->LRANGE($this->key, 0, -1);
			$key = array_search('-1', $final_list);
			unset($final_list[$key]);
			$matches_data = $this->uu->getMatchesDataMapForFirstKey($final_list);
			$result['recommendations'] = $final_list;
			$result['repeat_likes'] = array(); //It's a safety net. Let's not drag the repeat profiles here as well
                        $result['selectSprinkledProfiles'] = array(); //It's a safety net. Let's not drag the repeat profiles here as well
                        $result['profilesAdCampaigns'] = array();
			$mf = new MutualFriends();
			$mutual_friends_data = $mf->get_previously_calculated_data($this->userId, array_keys($matches_data));
			$result['mutualFriendsData'] = $mutual_friends_data;
		}
		return $result;
	}
	
	
	
	
	private function get_sponsored_ids_position_map($matches_first_key, $matches_scores_map) {
		$position = 0;
		$result = array();
		foreach ($matches_first_key as $key => $value) {
			if (isset($matches_scores_map[$key]) && $matches_scores_map[$key] == "CP" && strpos($value, 'l') === FALSE ) {
				$result[$position] = $key;
			}
			$position ++;
		}
		return $result;
	}
	
	
	private function filter_sponsored_profiles($matches_scores_map, $sponsored_profiles) {
		$result = array();
		$sponsored_profiles_map = array();
		foreach ($sponsored_profiles as $sp) $sponsored_profiles_map[$sp] = True;
		foreach ($matches_scores_map as $key => $value) {
			if (!isset($sponsored_profiles_map[$key])) $result[$key] = $value;
		}
		return $result;
	}
	
	
	/* reorders list 2 considering mutual friends score */
	private function reorder_matches_list($user_id, $matches_scores_map, $mutual_friends_data, $matches_data_map,
			 $remaining_matches_count,
			$remaining_liked_matches_count, $sponsored_ids_position_map, $sessionId) {
		
		$matches_with_mutual_friends = array();
		foreach ($mutual_friends_data as $key => $value) {
			if ($value['fb_count'] > 0)
				$matches_with_mutual_friends[$key] = $value['fb_count'];
		}
		
		// adding 0.25 fb mf score
		foreach ($matches_scores_map as $key => $value) {
			$matches_scores_map[$key] += (isset($matches_with_mutual_friends[$key]) && $matches_with_mutual_friends[$key] > 0) ? Utils::$mutual_friends_weight : 0;
		}
		
		$temp_arr = array();
		$matches_scores_map = $this->custom_stable_sort($matches_scores_map);
		$matches_ids = array_keys($matches_scores_map);
		
		// Making 2 sorted lists - liked matches, fresh matches. This is done to maintain the likes:fresh ratio from key1
		$liked_matches = array();
		$fresh_matches = array();
		foreach ($matches_ids as $match_id) {
			$temp_arr[$match_id] = $matches_scores_map[$match_id];
			if (isset($matches_data_map[$match_id]) && strpos($matches_data_map[$match_id], 'l') !== FALSE)
				$liked_matches[] = $match_id;
			else
				$fresh_matches[] = $match_id;
		}

		/* limit the final matches - use the count of remaining liked and fresh counts in list1
		 * liked matches = remaining liked matches (count of remaining liked matches in list 1)
		 * fresh matches (count of remaining fresh matches)
		 * */ 
		$liked_matches = array_slice($liked_matches, 0, $remaining_liked_matches_count);
		
		$remaining_fresh_count = $remaining_matches_count - count($liked_matches) - count($sponsored_ids_position_map);
		
		if ($remaining_fresh_count >= 0)
			$fresh_matches = array_slice($fresh_matches, 0, $remaining_fresh_count);

		// x initial profiles with mf shuffle 3x from liked matches list
		$liked_profiles_to_shuffle = 0;
		foreach ($liked_matches as $m) {
			if (isset($matches_with_mutual_friends[$m]) && $matches_with_mutual_friends[$m] > 0)
				$liked_profiles_to_shuffle += Utils::$mutliplier_for_shuffling;
			else break;
		}
		$temp = array_slice($liked_matches, 0, $liked_profiles_to_shuffle);
		shuffle($temp);
		if (count($liked_matches) > $liked_profiles_to_shuffle)
			$liked_matches = array_merge($temp, array_slice($liked_matches, $liked_profiles_to_shuffle, 
					count($liked_matches) - $liked_profiles_to_shuffle));

		// x initial profiles with mf shuffle 3x from fresh matches list
		$fresh_profiles_to_shuffle = 0;
		foreach ($fresh_matches as $m) {
			if (isset($matches_with_mutual_friends[$m]) && $matches_with_mutual_friends[$m] > 0)
				$fresh_profiles_to_shuffle += Utils::$mutliplier_for_shuffling;
			else break;
		}
		$temp = array_slice($fresh_matches, 0, $fresh_profiles_to_shuffle);
		shuffle($temp);
		if (count($fresh_matches) > $fresh_profiles_to_shuffle)
			$fresh_matches = array_merge($temp, array_slice($fresh_matches, $fresh_profiles_to_shuffle,
					count($fresh_matches) - $fresh_profiles_to_shuffle));

		// constructing liked and fresh matches scores map
		$liked_matches_scores_map = array();
		foreach ($liked_matches as $m) {
			$liked_matches_scores_map[$m] = (isset($matches_scores_map[$m])) ? $matches_scores_map[$m] : 0;
		}
		$fresh_matches_scores_map = array();
		foreach ($fresh_matches as $m) {
			$fresh_matches_scores_map[$m] = (isset($matches_scores_map[$m])) ? $matches_scores_map[$m] : 0;
		}
		
		// using the same old fns for final order
		$freshCompeteWithLikes = null;
		if ($this->gender == "F") {
			$freshCompeteWithLikes = true;
			if (intval(count($liked_matches) / (count($fresh_matches) + count($liked_matches))) < Utils::$min_percent_to_shuffle)
				$freshCompeteWithLikes = false;
		}
		$final_set      = $this->sortAndShuffleIds($liked_matches_scores_map, $fresh_matches_scores_map, $freshCompeteWithLikes);
                $myData         = $this->uuDBO->getUserData(array($user_id), true);
		$res            = $this->extraneousProfileHandler->handleExtraneousProfiles($final_set, $this->gender, $this->age, $myData['stay_state'], $myData['stay_city'], $sessionId,
                                            $this->isSelectMember ? array('profileAdCampaign','repeat') : ($myData['stay_country'] != 113 ? array() : array('profileAdCampaign','repeat','select')),$this->params);
                $final_set      = $res["final_set"];
                $repeat_likes   = $res["repeat_likes"];
                $selectSprinkledProfiles = $res["selectSprinkledProfiles"];
                $profilesAdCampaigns = $res["profilesAdCampaigns"];
                
		// x initial profiles with mf shuffle 3x in the final matches list
		$profiles_to_shuffle = 0;
		foreach ($final_set as $m) {
			if (isset($matches_with_mutual_friends[$m]) && $matches_with_mutual_friends[$m] > 0)
				$profiles_to_shuffle += Utils::$mutliplier_for_shuffling;
				else break;
		}
		$temp = array_slice($final_set, 0, $profiles_to_shuffle);
		shuffle($temp);
		if (count($final_set) > $profiles_to_shuffle)
			$final_set = array_merge($temp, array_slice($final_set, $profiles_to_shuffle,
					count($final_set) - $profiles_to_shuffle));
		
		$reordered_map = array();
		foreach ($final_set as $m) {
			$reordered_map[$m] = (isset($matches_scores_map[$m])) ? $matches_scores_map[$m] : 0;
		}

		return array(   "reordered_map"  => $reordered_map, 
                                "liked_matches"  => $liked_matches, 
                                "fresh_matches"  => $fresh_matches,
                                "repeat_likes" => $repeat_likes,
                                "selectSprinkledProfiles" => $selectSprinkledProfiles,
                                "profilesAdCampaigns" => $profilesAdCampaigns);
	}
	
	public function generateMatchesUsingMemoryTableClassSpecific($user_id, $actionTaken = 0, $logString = null, $sessionId = null, 
			$containingUsers = null, $passedFreshLimit = 0,$passedLikeLimit = 0) {
		
	}
	
	public function generateMatchesUsingMemoryTable($user_id, $actionTaken = 0, $logString = null, $sessionId = null) {
		
	}
}

?>

