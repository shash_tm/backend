<?php

/**
 * Interface for the Recommendation Engine
 */
interface IRecommendationEngine {

    public function generateMatchesUsingMemoryTable($user_id, $actionTaken = 0, $logString = null, $sessionId = null);
    
    public function generateMatchesUsingMemoryTableClassSpecific($user_id, $actionTaken = 0, $logString = null, $sessionId = null, $containingUsers = null, $passedFreshLimit = 0,$passedLikeLimit = 0);

    public function fetchResults($logString = null);

    public function fetchMatches();
    
    public function doLogging($final_score_set, $result_like_set, $likeQuery, $result_fresh_set, $freshProfileQuery, $countToFetchFresh, $logString);
}

?>
