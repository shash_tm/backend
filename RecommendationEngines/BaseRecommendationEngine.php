<?php

require_once dirname(__FILE__) . "/../matches/matchScore.php";
require_once dirname(__FILE__) . "/../include/Utils.php";
require_once dirname(__FILE__) . "/../fb.php";
require_once dirname(__FILE__) . "/../include/function.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/../abstraction/query.php";
require_once dirname(__FILE__) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../include/matchConstants.php";
require_once dirname(__FILE__) . "/../include/config.php";
require_once dirname(__FILE__) . "/../UserData.php";
require_once dirname(__FILE__) . "/../logging/QueryLog.class.php";
require_once dirname(__FILE__) . "/IRecommendationEngine.php";
require_once dirname(__FILE__) . "/../Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../logging/EventTrackingClass.php";
require_once dirname(__FILE__) . "/../DBO/sparkDBO.php";
require_once dirname(__FILE__) . "/Utils/ExtraneousProfileHandler.php";
require_once dirname(__FILE__) . "/../select/Select.class.php";

define('SELECT_FEMALE_FRESH_SCORE',4);
define('SELECT_FEMALE_LIKE_SCORE',3);
define('SELECT_MALE_FRESH_SCORE',2);

define('SHOW_HIGHER_BUCKET_FEMALES_TO_NRI_MALES',0.66);
define('SHOW_SELECT_FEMALES_TO_NRI_MALES',0.33);
define('SHOW_HIGHER_BUCKET_MALES_TO_NRI_FEMALES',0.66);
define('SHOW_SELECT_MALES_TO_NRI_FEMALES',0.33);

define('SHOW_NRI_MALES_TO_HIGHER_BUCKET_FEMALES',1);
define('SHOW_NRI_MALES_TO_SELECT_FEMALES',0.5);
define('SHOW_NRI_FEMALES_TO_HIGHER_BUCKET_MALES',1);
define('SHOW_NRI_FEMALES_TO_SELECT_MALES',0.5);

define('MIN_SELECT_PACKAGE_DURATION',30);
define('MIN_SELECT_SCORE',0.25);

define('CITY_SCORES_NRI_MALE',8);
define('CITY_SCORES_NRI_FEMALE',8);

define('MAX_SELECT_PROFILES_TO_EXPOSE_TO_MALES',5);

/**
 * Description of BaseRecommendationEngine
 */
abstract class BaseRecommendationEngine implements IRecommendationEngine {

    protected $userId;
    protected $key;
    protected $keyWithScores;
    protected $timeForMatchesExpiry;
    protected $timeForMatchCountExpiry;
    protected $timeForSelectCountExpiry;
    protected $matchCountKey;
    protected $selectCountKey;
    protected $uu;
    protected $redis;
    protected $gender;
    protected $status;
    protected $ambiguousCitiesChandigarh;
    protected $ambiguousStatesChandigarh;
    protected $ambiguousCitiesNCR;
    protected $ambigousStates;
    protected $isFreshBucket;
    protected $totalProfilesForFemale;
    protected $queryLog;
    public $uuDBO;
    public $userFlags;
    public $canUserSendOutSparks;
    public $extraneousProfileHandler;
    public $selectObject;
    public $isSelectMember;
    public $selectMemberData;
    public $isSelectMemberForThisSession;

    function __construct($user_id) {
        global $redis;
        $this->userId = $user_id;
        $this->redis = $redis;
        $this->ambiguousCitiesNCR = array(47965, 47968, 47964, 47967, 47966);
        $this->ambigousStates = array(2168, 2176, 2193); //states : 2168 - NCR,  2176- Haryana, 2193 - UP
        $this->ambiguousCitiesChandigarh = array(16007, 42795);
        $this->ambiguousStatesChandigarh = array(2172, 2189);
        $ud = new UserData($user_id);
        $this->status = $ud->fetchStatus();
        $this->gender = $ud->fetchGender();
        $this->recommendation_number = ($this->gender == "M") ? Utils::$recommendation_count_male : Utils::$recommendation_count_female;
        $this->key = Utils::$redis_keys['matches'] . "$user_id"; //key for caching in redis
        $this->keyWithScores = Utils::$redis_keys['matches_with_scores'] . "$user_id"; //key for caching in redis with scores
        $this->matchCountKey = Utils::$redis_keys['match_action_count'] . "$user_id"; //key for refreshing matches
        $this->selectCountKey = Utils::$redis_keys['select_matches_for_the_day'] . "$user_id"; //key for select matches
        $this->repeatProfileKey=Utils::$redis_keys['matches_repeat_profile']."$user_id"; //key for repeat profiles
        $this->timeForMatchesExpiry = ($this->gender == "F") ? Utils::$femaleMatchesExpiryTime : Utils::$maleMatchesExpiryTime;    //setting the match refresh time
        $this->timeForMatchCountExpiry = $this->timeForMatchesExpiry;
        $this->canUserSendOutSparks = SparkDBO::canUserSendOutSparks($this->userId);
        $this->selectObject = new Select($user_id);
        $this->selectMemberData = $this->selectObject->getMySelectData();
        $this->isSelectMember = $this->selectMemberData['is_tm_select'];
        $selectProfilesActionedUp = $this->redis->GET($this->selectCountKey);
        $this->isSelectMemberForThisSession = ((isset($selectProfilesActionedUp) && $selectProfilesActionedUp >= MAX_SELECT_PROFILES_TO_EXPOSE_TO_MALES) && $this->gender == "M") ? false : $this->isSelectMember;

        //modifying the time for expiry in case there is time diff negative
	if ($this->gender == "M") {
            $timeTillMn = Utils::timeTillMidnight(1) - (60 * 330);
            if ($timeTillMn < 0) {
                $this->timeForMatchCountExpiry = Utils::timeTillMidnight(2) - (60 * 330);
            } else {
                $this->timeForMatchCountExpiry = Utils::timeTillMidnight() - (60 * 330);
            }
        }
        $this->timeForSelectCountExpiry = $this->timeForMatchCountExpiry;
        $this->uu = new UserUtils();
        $this->uuDBO = new userUtilsDBO();
        $this->queryLog = new QueryLog();
        $this->userFlags = new UserFlagsDBO();
        $this->extraneousProfileHandler = new ExtraneousProfileHandler($user_id,$this->gender);
    }

    /**
     * fetch data from recommendation query if a new user comes in
     * else fetch result from redis
     */
    public function fetchResults($logString = null, $sessionId = null) {
        $header = Utils::getAllSystemHeaderFields();
        $batchCount = isset($header['batchCount']) ? $header['batchCount'] : -1;
        $matchKeyExists = $this->uu->ifKeyExists($this->key);
        $start_index = 0;
        $data=$this->userFlags->read("matches_cache_optimisations");
        $count = isset($data['firstBatchCount'])?$data['firstBatchCount']:30;
        
        if($this->gender == "M") {
            $batchCount = -1;
        }
        
        if ($batchCount > 1 and $matchKeyExists) {
            // Get the next id from redis
            $start_index = -1;
        } else if ($batchCount < 0) {
            // Let the old logic flow 
            $count = -1;
        }

        //Don't use TTL b'cos TTL in seconds or -1 when key does not exist or does not have a timeout.
        //which actually might break flow for non-authentic users
        $aggregateInfoOfMatches = array();
        if (!$matchKeyExists) {
            $actionCount = $this->redis->GET($this->matchCountKey);
            if (!isset($actionCount)) {
                $actionCount = 0;
            }
	    $startTime = time();
            $aggregateInfoOfMatches = $this->generateMatchesUsingMemoryTable($this->userId, $actionCount, $logString, $sessionId);
            $endTime = time();
            $diff = $endTime-$startTime;
            $eventTracker = new EventTrackingClass();
            $data_logging = array();
            $data_logging [] = array(   "user_id"      => $this->userId,
                                        "activity"     => "matches" ,
                                        "event_type"   => "sql_call_time_tracker",
                                        "event_status" => "success",
                                        "time_taken"   => $diff   );
            $eventTracker->logActions($data_logging);
        }

        $result = $this->fetchMatches($start_index, $count);
        $result["repeat_likes"] = $aggregateInfoOfMatches['repeat_likes'];
        $result["selectSprinkledProfiles"] = $aggregateInfoOfMatches['selectSprinkledProfiles'];
        
        $result['isNewRecommendations'] = !$matchKeyExists;
        $result['isSelect'] = $this->isSelectMember;
        $mutual_friends = isset($header['mutual_friends']) ? $header['mutual_friends'] : false;
        if ($mutual_friends && !$matchKeyExists) {
        	$result['hasMoreRecommendations'] = true;
        }
        return $result;
    }


    public function fetchMatches($start_index = 0, $count = -1) {
        $hasMoreRecommendations = false;
        $arr = $this->redis->LRANGE($this->key, 0, -1);
        $cache_next_serve_key = Utils::$redis_keys['cache_next_serve_key'] . "$this->userId";

        //removal of -1 if present in the redis set
        $key = array_search('-1', $arr);
        unset($arr[$key]);
        //removal of -2 which signify a CAROUSEL Notification profile
        $key = array_search('-2', $arr);
        if ($key >= 0) {
            unset($arr[$key]);
            $this->redis->LREM($this->key, "-2");
            $this->redis->EXPIRE($this->key, ($this->gender == "F") ? Utils::$femaleMatchesExpiryTime : Utils::$maleMatchesExpiryTime);
            if ($this->uu->ifKeyExists($this->keyWithScores))
            	$this->redis->EXPIRE($this->keyWithScores, ($this->gender == "F") ? Utils::$femaleMatchesExpiryTime : Utils::$maleMatchesExpiryTime);
        }

        $result = array();
        foreach ($arr as $val) {
            $result[] = $val;
        }

        if ($count < 0 || sizeof($result) == 0) {
            // A fallback
            $this->redis->DEL($cache_next_serve_key); //Add/Update starting key as -1
            return array("recommendations" => $result, "hasMoreRecommendations" => $hasMoreRecommendations);
        }
        
        // SATISH TODO: test this below condn, get it reviewed
        $header = Utils::getAllSystemHeaderFields();
        $fetch_all_matches = isset($header['fetch_all_matches']) ? $header['fetch_all_matches'] : false;
        if ($fetch_all_matches) {
        	$start_index = 0;
        	$key = $this->redis->GET($cache_next_serve_key);
        	$count = NULL;
        	$this->redis->DEL($cache_next_serve_key);
        	$hasMoreRecommendations = false;
        }
        else if ($start_index < 0) {
            $start_index = 0;
            $key = $this->redis->GET($cache_next_serve_key);
            if (isset($key)) {
                $idx = array_search($key, $result);
                if ($idx) {
                    $start_index = $idx;
                }
            }
            $count = NULL;
            $this->redis->DEL($cache_next_serve_key);
            $hasMoreRecommendations = false;
        } else if ($start_index + $count < sizeof($result)) {
            $this->redis->SET($cache_next_serve_key, $result[$start_index + $count]);
            $hasMoreRecommendations = true;
        } else {
            $this->redis->DEL($cache_next_serve_key);
            $hasMoreRecommendations = false;
        }
        $final_result = array();
        $final_result['recommendations'] = array_slice($result, $start_index, $count); 
        $final_result['hasMoreRecommendations'] = $hasMoreRecommendations;
        return $final_result;
    }

    /**
     * Get batch size for female based on bucket
     */
    protected function setBatchSizeForFemale($piData) {
        $count = 0;

        if ($this->isFreshBucket == true) {
            $count = BatchSize::FemaleBucketZero;
        } else if ($piData['bucket'] == 1) {
            $count = BatchSize::FemaleBucketOne;
        } else if ($piData['bucket'] == 2) {
            $count = BatchSize::FemaleBucketTwo;
        } else if ($piData['bucket'] == 3) {
            $count = BatchSize::FemaleBucketThree;
        } else if ($piData['bucket'] == 4) {
            $count = BatchSize::FemaleBucketFour;
        } else if ($piData['bucket'] == 5) {
            $count = BatchSize::FemaleBucketFive;
        }
        $this->totalProfilesForFemale = $count;
    }

    /**
     * Return the count to be fetched for a user for fresh profiles
     */
    protected function getFreshProfilesCountForMales($isFreshCall, $piData, $alreadyActionedCount) {
        $count = 0;
        $likesLimitMultiplier = 1;
        if($this->canUserSendOutSparks)
            $likesLimitMultiplier = 2;
        
        if ($isFreshCall == true) {
            if ($piData['active_chats'] >= Utils::$activeChatsThreshold) {
                $count = FreshBatchCount::MaleExceedingChatThrehold;
            } else if ($this->isFreshBucket == true) {
                $count = FreshBatchCount::MaleBucketZero;
            } else if ($piData['bucket'] == 1) {
                $count = FreshBatchCount::MaleBucketOne;
            } else if ($piData['bucket'] == 2) {
                $count = FreshBatchCount::MaleBucketTwo;
            } else if ($piData['bucket'] == 3) {
                $count = FreshBatchCount::MaleBucketThree;
            } else if ($piData['bucket'] == 4) {
                $count = FreshBatchCount::MaleBucketFour;
            } else if ($piData['bucket'] == 5) {
                $count = FreshBatchCount::MaleBucketFive;
            }
        } else {
            if ($piData['active_chats'] >= Utils::$activeChatsThreshold) {
                if ($alreadyActionedCount < LikeCountThreshold::MaleExceedingChatThrehold) {
                    $count = RepeatBatchCount::MaleExceedingChatThrehold;
                } else {
                    $count = 0;
                }
            } else if ($piData['bucket'] == 0 || $piData['actual_pi'] == -1) {
                if ($alreadyActionedCount < ($likesLimitMultiplier*LikeCountThreshold::MaleBucketZero)) {
                    $count = RepeatBatchCount::MaleBucketZero;
                } else {
                    $count = 0;
                }
            } else if ($piData['bucket'] == 1) {
                if ($alreadyActionedCount < ($likesLimitMultiplier*LikeCountThreshold::MaleBucketOne)) {
                    $count = RepeatBatchCount::MaleBucketOne;
                } else {
                    $count = 0;
                }
            } else if ($piData['bucket'] == 2) {
                if ($alreadyActionedCount < ($likesLimitMultiplier*LikeCountThreshold::MaleBucketTwo)) {
                    $count = RepeatBatchCount::MaleBucketTwo;
                } else {
                    $count = 0;
                }
            } else if ($piData['bucket'] == 3) {
                if ($alreadyActionedCount < ($likesLimitMultiplier*LikeCountThreshold::MaleBucketThree)) {
                    $count = RepeatBatchCount::MaleBucketThree;
                } else {
                    $count = 0;
                }
            } else if ($piData['bucket'] == 4) {
                if ($alreadyActionedCount < ($likesLimitMultiplier*LikeCountThreshold::MaleBucketFour)) {
                    $count = RepeatBatchCount::MaleBucketFour;
                } else {
                    $count = 0;
                }
            } else if ($piData['bucket'] == 5) {
                if ($alreadyActionedCount < ($likesLimitMultiplier*LikeCountThreshold::MaleBucketFive)) {
                    $count = RepeatBatchCount::MaleBucketFive;
                } else {
                    $count = 0;
                }
            }
	}
        return $count;
    }

    /**
     * Return the count to be fetched for a user for fresh profiles
     */
    protected function getFreshProfilesCountForFeMales($piData) {
        $count = 0;
        if ($this->isFreshBucket == true) {
            $count = FreshBatchCount::FemaleBucketZero;
        } else if ($piData['bucket'] == 1) {
            $count = FreshBatchCount::FemaleBucketOne;
        } else if ($piData['bucket'] == 2) {
            $count = FreshBatchCount::FemaleBucketTwo;
        } else if ($piData['bucket'] == 3) {
            $count = FreshBatchCount::FemaleBucketThree;
        } else if ($piData['bucket'] == 4) {
            $count = FreshBatchCount::FemaleBucketFour;
        } else if ($piData['bucket'] == 5) {
            $count = FreshBatchCount::FemaleBucketFive;
        }
        return $count;
    }

    protected function getActivityCase() {
        return "case when ai < 0.25 then 0.25 else (case when ai <0.5 then 0.5 else (case when ai<0.75 then 0.75 else 1 end) end ) end ";
    }
    
    /*
     * The below function introduces dynamism in the computation of select Score.
     * For Spacing out the Select Profiles - 
     * Problem Statement : 
     *     1. Most of the Select Profiles come very early in the lifecycle of Select member i.e. immediately after the Select is purchased.
     *     2. In a given session, all the Select profiles come early in the recommendation list.
     * Proposed Solution :
     *     1. Assuming a 'd' day window for Select.
     *     2. Let 'x' be the number of days since the Select purchase (or renewal)
     *     3. The score assigned to the select profile would be 
     *         a. Pick up a point from the parabola equation (higher score initially, lower in the middle and then high again once we reach to the end of subscription. (Solves problem 1. above)
     *         b. Assign a random score between 0 and to that point on the curve. (Solves problem 2. above)
     *     4. In exact terms the formulae boils down to - 
     *                                     Score  = 2*rand(SEED)*({2*(x%MIN_SELECT_PACKAGE_DURATION)/MIN_SELECT_PACKAGE_DURATION}-1)2)
     *     5. The 'd' above has been replaced with a recursive cycle of parabolas when the entire duration exceeds MIN_SELECT_PACKAGE_DURATION
     */
    protected function getSelectScoreForMales() {
        # SQL rand function with a seed works surprisingly weird. Takes on values in cyclic format
        # 4k => 0.15,  4*k+1 => 0.40, 4*k+2 => 0.65, 4*k+3 => 0.90
        $seedForSelect = rand(0,11);
        $sql  = "select case when (t.tstamp is not null) then ".SELECT_MALE_FRESH_SCORE."*rand($seedForSelect)*pow((2*((DATEDIFF(now(),t.tstamp)%".MIN_SELECT_PACKAGE_DURATION.")/".MIN_SELECT_PACKAGE_DURATION.")-1),2) + ".MIN_SELECT_SCORE." else ".SELECT_MALE_FRESH_SCORE." end as selectScore from user_spark_transaction t join spark_packages sp on t.package_id = sp.package_id and sp.type = 'select' and t.user_id = ? and t.status in ('consumed','apple_restore')  order by t.tstamp desc limit 1 "; 
        $score = Query::SELECT($sql, array($this->userId),Query::$__slave,true);
        if(!isset($score['selectScore'])) {
            return SELECT_MALE_FRESH_SCORE;
        }
        return $score['selectScore'];
    }

    protected function generateBaseQuery($prefData, $myData, $oppositeGender, $locationQuery, $myBucket) {
        $prefStartAge = $prefData['start_age'];
        $prefEndAge = $prefData['end_age'];
        $prefStartHeight = $prefData ['start_height'];
        $prefEndHeight = $prefData ['end_height'];
        $myAge = $myData['age'];
        $myCity = $myData['stay_city'];
        $myState = $myData['stay_state'];
        $myCountry = $myData['stay_country'];
        $myHeight = $myData ['height'];

        $baseQuery = " WHERE gender= '$oppositeGender' and age>='$prefStartAge' and age<='$prefEndAge'";
        if ($this->checkissetorEmpty($locationQuery)) {
            $baseQuery .=" and   ( $locationQuery ) ";
        }
        $baseQuery .= " and height>='$prefStartHeight' and height<='$prefEndHeight' ";

        if ($this->checkissetorEmpty($myAge)) {
            $baseQuery .= " and pref_start_age<='$myAge' and pref_end_age>='$myAge'";
        }
        $city_query = $myCountry . "_" . $myState . "_" . $myCity;
        $state_query = $myCountry . "_" . $myState;
        $country_query = $myCountry;

        //make state query
        $baseQuery.= " and (pref_location is null or pref_location
					 regexp '[[:<:]]" . $city_query . "[[:>:]]' or pref_location
					 regexp '[[:<:]]" . $state_query . "[[:>:]]' or pref_location
					 regexp '[[:<:]]" . $country_query . "[[:>:]]'";

        if (in_array($myCity, $this->ambiguousCitiesNCR)) {
            $baseQuery .= " or pref_location like '%$myCity%' or pref_location regexp '[[:<:]]" . $myCountry . "_2168[[:>:]]' ";
        }
        if (in_array($myCity, $this->ambiguousCitiesChandigarh)) {
            foreach ($this->ambiguousStatesChandigarh as $state) {
                $baseQuery .= " or pref_location regexp '[[:<:]]" . $myCountry . "_" . $state . "[[:>:]]' ";
            }
            foreach ($this->ambiguousCitiesChandigarh as $city) {
                $baseQuery .= " or pref_location like '%$city%' ";
            }
        }
        $baseQuery .= " )";

        if ($this->checkissetorEmpty($myHeight)) {
            $baseQuery .= " and pref_start_height<='$myHeight' and pref_end_height>='$myHeight'";
        }
        
        #Handle for NRI Scenarios here:
        $baseQuery .= $this->getNRIFilterString($myCity,$myCountry,$myBucket,$this->isSelectMember);
        
        if (Utils::$education_rules_enabled) {
        	if ($this->gender == "F")
        		$baseQuery .= $this->getEducationSuppressionFilterQueryForFemale($myData);
                // Removing the Education suppression rules for male. They were limiting the number of profiles a male could see.
        	//if ($this->gender == "M")
        	//	$baseQuery .= $this->getEducationSuppressionFilterQueryForMale($myData);
        }
        return $baseQuery;
    }

    protected function getPiCase($myBucket,$myCountry) {
        $piCase = '';
        if ($this->gender == "M") {
            $piCase = $this->getPIRankForMale($myBucket, "2");
        } else {
            $piCase = $this->getPIRanksAndFiltersForFemale($myBucket, "2",$myCountry);
        }
        return $piCase;
    }

    /**
     * Get rank case in case of uninstalled user
     */
    protected function getUninstallRankCase() {
        return "case when install_status = 'uninstall' then -10000 else 0 end";
    }

    /**
     * Return if it's a fresh call or not
     */
    protected function isFreshCall($alreadyActionedCount) {
        return ($alreadyActionedCount > 0) ? false : true;
    }

    /**
     * Get the rank case for buckets/pi
     * The rules are :
     * Fresh Query			
        Non-Select      RANKING                         FILTERING
	F0              When M5 = 2, else 0             No filtering
	F1              When M0,M1,M2,M3 = 2, else 0	No filtering
	F2              When M0,M2,M3,M4 = 2, else 0	No filtering
	F3              When M0,M2,M3,M4,M5 = 2,else 0	No filtering
	F4              2 (no ranking)                  M1,M2,M3 filtered
	F5              2 (no ranking)                  M1,M2,M3 filtered		
        Like Query			
        Non-Select      RANKING                         FILTERING
	F0              2 (no ranking)                  M1,M2,M3,M4 filtered
	F1              2 (no ranking)                  No filtering
	F2              2 (no ranking)                  M1 filtered
	F3              2 (no ranking)                  M1,M2 filtered
	F4              2 (no ranking)                  M1,M2,M3 filtered
	F5              2 (no ranking)                  M1,M2,M3 filtered
        Fresh Query			
        Select          RANKING                         FILTERING
	F0              When M5 = 2, else 0             No filtering
	F1              When M0,M1,M2,M3 = 2, else 0	No filtering
	F2              When M0,M2,M3,M4 = 2, else 0	No filtering
	F3              When M0,M2,M3,M4,M5 = 2,else 0	No filtering
	F4              When M0,M4,M5 = 2, else 0       No filtering if male is Select else M1,M2,M3 filtered
	F5              When M0,M4,M5 = 2, else 0       No filtering if male is Select else M1,M2,M3 filtered		
        Like Query			
	Select          RANKING                         FILTERING
	F0              When M0,M5 = 2, else 0          No filtering if male is Select else M1,M2,M3,M4 filtered
	F1              2 (no ranking)                  No filtering
	F2              When M0,M2,M3,M4,M5 = 2, else 0	No filtering if male is Select else M1 filtered
	F3              When M0,M3,M4,M5 = 2, else 0	No filtering if male is Select else M1,M2 filtered
	F4              When M0,M4,M5 = 2,else 0        No filtering if male is Select else M1,M2,M3 filtered
	F5              When M0,M4,M5 = 2,else 0        No filtering if male is Select else M1,M2,M3 filtered
     */
    protected function getPIRanksAndFiltersForFemale($bucket, $piScore, $myCountry = 113) {
        $piRankCaseFreshQuery           = "";
        $piRankCaseFreshQueryForSelect  = " $piScore ";
        $piRankCaseLikeQuery            = " $piScore ";
        $piRankCaseLikeQueryForSelect   = "";
        
        $piFilterCaseFreshQuery         = "";
        $piFilterCaseFreshQueryForSelect= ""; 
        $piFilterCaseFreshQueryNonIndian= "";
        $piFilterCaseLikeQuery          = "";
        $piFilterCaseLikeQueryForSelect = "";
        $piFilterCaseLikeQueryNonIndian = "";

        if ($this->isFreshBucket == true) {
            $piRankCaseFreshQuery = " case when u.bucket In (5) then $piScore else 0 end ";
            $piRankCaseFreshQueryForSelect = $piRankCaseFreshQuery;
            $piRankCaseLikeQueryForSelect = " case when u.bucket In (0,5) then $piScore else 0 end ";

            $piFilterCaseFreshQueryNonIndian = " and (country!='113' or (country = '113' and (u.bucket in (4,5) or isSelect = 1))) ";
            $piFilterCaseLikeQuery = " and u.bucket in (0,5) ";
            $piFilterCaseLikeQueryForSelect = " and (u.country != 113 or u.isSelect = 1 or u.bucket in (0,5))";
            $piFilterCaseLikeQueryNonIndian = " and ((u.bucket in (0,5) and country!='113') or (country = '113' and (u.bucket in (4,5) or isSelect = 1)))";
        }
        if ($bucket == 1) {
            $piRankCaseFreshQuery = "case when u.bucket not in (4,5) then $piScore else 0 end ";
            $piRankCaseFreshQueryForSelect = $piRankCaseFreshQuery;
            $piRankCaseLikeQueryForSelect = " $piScore ";
            
            $piFilterCaseFreshQueryNonIndian = " and (country!='113' or (country = '113' and (u.bucket in (4,5) or isSelect = 1))) ";
            $piFilterCaseLikeQueryNonIndian = " and (country!='113' or (country = '113' and (u.bucket in (4,5) or isSelect = 1))) ";
        } else if ($bucket == 2) {
            $piRankCaseFreshQuery = "case when u.bucket not in (1,5) then $piScore else 0 end ";
            $piRankCaseFreshQueryForSelect = $piRankCaseFreshQuery;
            $piRankCaseLikeQueryForSelect = "case when u.bucket not in (1) then $piScore else 0 end ";
            
            $piFilterCaseFreshQueryNonIndian = " and (country!='113' or (country = '113' and (u.bucket in (4,5) or isSelect = 1))) ";
            $piFilterCaseLikeQuery = " and u.bucket != 1 ";
            $piFilterCaseLikeQueryForSelect = " and (u.country != 113 or u.isSelect = 1 or u.bucket != 1)";
            $piFilterCaseLikeQueryNonIndian = " and ((u.bucket != 1 and country!='113') or (country = '113' and (u.bucket in (4,5) or isSelect = 1))) ";
        } else if ($bucket == 3) {
            $piRankCaseFreshQuery = "case when u.bucket !=1 then $piScore else 0 end ";
            $piRankCaseFreshQueryForSelect = $piRankCaseFreshQuery;
            $piRankCaseLikeQueryForSelect = "case when u.bucket not in (1,2) then $piScore else 0 end ";
            
            $piFilterCaseFreshQueryNonIndian = " and (country!='113' or (country = '113' and (u.bucket in (4,5) or isSelect = 1))) ";
            $piFilterCaseLikeQuery = " and u.bucket not in (1,2) ";
            $piFilterCaseLikeQueryForSelect = " and (u.country != 113 or u.isSelect = 1 or u.bucket not in (1,2))";
            $piFilterCaseLikeQueryNonIndian = " and ((u.bucket not in (1,2) and country!='113') or (country = '113' and (u.bucket in (4,5) or isSelect = 1)))";
        } else if ($bucket == 4) {
            $piRankCaseFreshQuery = " $piScore ";
            $piRankCaseFreshQueryForSelect = "case when u.bucket in (0,4,5) then $piScore else 0 end ";
            $piRankCaseLikeQueryForSelect = "case when u.bucket in (0,4,5) then $piScore else 0 end ";
            
            $piFilterCaseFreshQuery = " and u.bucket not in (1,2,3) ";
            $piFilterCaseFreshQueryForSelect = " and (u.isSelect = 1 or u.bucket not in (1,2,3))";
            $piFilterCaseFreshQueryNonIndian = " and ((u.bucket not in (1,2,3) and country!='113') or ((country = '113' and (u.bucket in (4,5) or isSelect = 1))))";
            $piFilterCaseLikeQuery = " and (u.country != 113 or u.bucket not in (1,2,3)) ";
            $piFilterCaseLikeQueryForSelect = " and (u.country != 113 or u.isSelect = 1 or u.bucket not in (1,2,3))";
            $piFilterCaseLikeQueryNonIndian = " and ((u.bucket not in (1,2,3) and country!='113') or ((country = '113' and (u.bucket in (4,5) or isSelect = 1))))";
        } else if ($bucket == 5) {
            $piRankCaseFreshQuery = " $piScore ";
            $piRankCaseLikeQueryForSelect = "case when u.bucket in (0,4,5) then $piScore else 0 end ";
            $piRankCaseFreshQueryForSelect = $piRankCaseLikeQueryForSelect;

            $piFilterCaseFreshQuery = " and u.bucket not in (1,2,3) ";
            $piFilterCaseFreshQueryForSelect = " and (u.isSelect = 1 or u.bucket not in (1,2,3))";            
            $piFilterCaseFreshQueryNonIndian = " and ((u.bucket not in (1,2,3) and country!='113') or ((country = '113' and (u.bucket in (4,5) or isSelect = 1))))";
            $piFilterCaseLikeQuery = " and (u.country != 113 or u.bucket not in (1,2,3)) ";
            $piFilterCaseLikeQueryForSelect = " and (u.country != 113 or u.isSelect = 1 or u.bucket not in (1,2,3))";
            $piFilterCaseLikeQueryNonIndian = " and ((u.bucket not in (1,2,3) and country!='113') or ((country = '113' and (u.bucket in (4,5) or isSelect = 1))))";
        }

        if ($this->isSelectMember) {
            $piFilterCaseLikeQuery = $piFilterCaseLikeQueryForSelect;
            $piRankCaseLikeQuery = $piRankCaseLikeQueryForSelect;
            $piFilterCaseFreshQuery = $piFilterCaseFreshQueryForSelect;
            $piRankCaseFreshQuery = $piRankCaseFreshQueryForSelect;
        }
        if ($myCountry != 113) {
            $piFilterCaseLikeQuery = $piFilterCaseLikeQueryNonIndian;
            $piRankCaseLikeQuery = $piRankCaseLikeQuery;
            $piFilterCaseFreshQuery = $piFilterCaseFreshQueryNonIndian;
            $piRankCaseFreshQuery = $piRankCaseFreshQuery;
        }

        $return_array = array(
            "piRankCaseLikeQuery" => $piRankCaseLikeQuery,
            "piFilterCaseLikeQuery" => $piFilterCaseLikeQuery,
            "piRankCaseFreshQuery" => $piRankCaseFreshQuery
        );
        if ($piRankCaseFreshQuery != '') {
            $return_array["piFilterCaseFreshQuery"] = $piFilterCaseFreshQuery;
        }
        return $return_array;
    }

    /**
     * Get the rank case for buckets/pi
     */
    protected function getPIRankForMale($bucket, $piScore) {
        $piRankCase = '';

        if ($this->isFreshBucket == true) {
            $piRankCase = $piScore;
        } else if ($bucket == 1) {
            $piRankCase = "case when bucket = 1 then $piScore else 0 end ";
        } else if ($bucket == 2) {
            $piRankCase = "case when bucket >= 1 and bucket <=2 then $piScore else 0 end ";
        } else if ($bucket == 3) {
            $piRankCase = "case when bucket >= 1 and bucket <=3 then $piScore else 0 end ";
        } else if ($bucket == 4) {
            $piRankCase = "case when bucket not in (1,0) then $piScore else 0 end ";
        } else if ($bucket == 5) {
            $piRankCase = "case when bucket not in (1,2) then $piScore else 0 end ";
        }
        return $piRankCase;
    }

    protected function getRecommendationSet($user_id, $from_cron = false) {
        $sql = "select t.user, t.flag from (
                 select user1 as user, 0 as flag from user_hide where user2=?
                 union select user2 as user, 0 as flag from user_hide where user1=?
                 union select user2 as user, 2 as flag from user_like where user1=?
                 union select user1 as user, 1 as flag from user_like where user2=?)t join user_search u on t.user=u.user_id";

        $output = array();
        $output_likedMe = array();
        $output_iLiked = array();
        $data = Query::SELECT($sql, array($user_id, $user_id, $user_id, $user_id, $user_id));
        foreach ($data as $val) {
            if ($val['flag'] == 1)
                $output_likedMe[] = $val['user'];
            if ($val['flag'] == 2)
                $output_iLiked[] = $val['user'];
            if ($val['flag'] == 0)
                $output [] = $val ['user'];
        }
        $output = array_unique($output);
        return array("likedMe" => $output_likedMe, "nonLikedMe" => $output, "iLiked" => $output_iLiked);
    }

    protected function add_apostrophe(&$item1, $key) {
        $item1 = "'$item1'";
    }

    protected function addApostrophe($parameter, $separator = ",") {
        $type = explode("$separator", $parameter);
        $type = array_filter($type);
        $val = array_walk($type, array(
            $this,
            'add_apostrophe'
        ));
        return implode(",", $type);
    }

    protected function checkissetorEmpty($value) {
        if (isset($value) == false || (is_string($value) && strlen($value) == 0))
            return false;
        return true;
    }

    protected function getLocationCase($prefLocation, $myCity, $myState, $myCountry, $myGender) {
        //constants for multiplication
        $cityConstant = ($myGender == 'M') ? MatchEquationMutiplicationFactor::CityMale : MatchEquationMutiplicationFactor::CityFemale;
        if($myCountry != '113') {
            $cityConstant = ($myGender == 'M') ? CITY_SCORES_NRI_MALE : CITY_SCORES_NRI_FEMALE;
        }
        $locationCase = $this->getLocationRankCase($myCity, $myState, $myCountry, $cityConstant);
        return $locationCase;
    }

    /**
     * Get the location ranking case
     */
    protected function getLocationRankCase($city, $state, $country, $locationScore) {
        $stateQuery = '';
        // 47968 - Greater Noida, 47965 - noida, 47964 - gurgaon, 47967 - ghaziabad, 47966 - faridabad
        // states : 2168 - NCR,  2176- Haryana, 2193 - UP
        if (in_array($city, $this->ambiguousCitiesNCR) || $state == "2168") {
            $stateQuery .= " OR  (city in (" . implode(',', $this->ambiguousCitiesNCR) . " , 16743))";
        }
        //if punjab/chd is filled in state and city is not filled
        else if (in_array($state, $this->ambiguousStatesChandigarh)) {
            $stateQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesChandigarh) . "))";
        }
        return "case when city = '$city' $stateQuery  then " . $locationScore . "  else(case when state = '$state'  then " . $locationScore / 2 . " else (case when country = $country then case when country != '113' then ". $locationScore/2 ." else ". $locationScore/3 ." end else 0 end)end) end ";
    }

    protected function setIsFreshBucket($pi, $bucket) {
        $this->isFreshBucket = ($pi == -1 || $pi == null || $bucket == 0) ? true : false;
    }

    protected function setExpiryForRedisKeys($actionTaken) {
        if ($this->status == "authentic") {
            $this->uu->setExpiry($this->key, $this->timeForMatchesExpiry);
            $this->uu->setExpiry($this->keyWithScores, $this->timeForMatchesExpiry);
            $this->uu->setKeyAndExpire($this->matchCountKey, $this->timeForMatchCountExpiry, $actionTaken);
            $this->uu->setKeyAndExpire($this->selectCountKey, $this->timeForSelectCountExpiry, $actionTaken);
        }
    }

    protected function logIndividualScore($data, $logString = null, $current_timestamp = null) {
        $countIds = count($data);
        if ($countIds > 0) {
            $params_arr = array();
            foreach ($data as $key => $val) {
                $params_arr[] = array(  "user1" => $this->userId, 
                                        "user2" => $key, 
                                        "locationScore" => $val['location'],
                                        "piScore" => $val['piScore'], 
                                        "activityScore" => $val['activityScore'], 
                                        "availabilityScore" => $val['availabilityScore'],
                                        "educationScore" => $val['educationScore'], 
                                        "tmSelectStringRank" => isset($val['tmSelectStringRank'])?$val['tmSelectStringRank']:0,
                                        "nriStringRank" => isset($val['nriStringRank'])?$val['nriStringRank']:0,
                                        "mutualFriendsScore" => $val['mutualFriendsScore'],
                                        "tmSelectStringRank" => $val['tmSelectStringRank'],
                                        "nriStringRank" => $val['nriStringRank'],
                                        "totalScore" => $val['total'], 
                                        "likeFlag" => $val['likeFlag'], 
                                        "pi" => $val['pi'], 
                                        "bucket" => $val['bucket'], 
                                        "recommender" => $logString);
            }
            $this->queryLog->rankLogData($params_arr, $current_timestamp);
        }
    }

    protected function storeResults($resultSet, $query, $isLikeQuery = 0, $logString = null, $countToFetch = null, $current_timestamp = null) {
        if ($isLikeQuery == 0) {
            if ($countToFetch <= 0)
                $countToFetch = 0;
        }
        $result = json_encode($resultSet);
        $data_logging = array();
        $data_logging [] = array("user_id" => $this->userId, "query" => $query, "result" => $result, "isLikeQuery" => $isLikeQuery, "countToFetch" => $countToFetch, "countFetched" => count($resultSet), "recommender" => $logString);
        $this->queryLog->queryLogData($data_logging, $current_timestamp);
    }

    protected function addMatchesToSet($user_array, $likedIds, $myGender, $availabile_set) {
        //if gender is female then dont add -1 as first value so that key automatically deletes after all the actions
        $args[0] = $this->key;
        $args[1] = -1;
        $i = 2;
        foreach ($user_array as $key) {
            //Changed By Raghav, It was female in if condition for some unknown reason 
        	if (true) {
                if (isset($likedIds) && in_array($key, $likedIds)) {
                    $args[$i] = $key . 'l';
                } else {
                    $args[$i] = $key;
                }
            } else {
                $args[$i] = $key;
            }
            $args[$i] .= (isset($availabile_set[$key])) ? ":$availabile_set[$key]" : ":0";
            $i++;
        }
        if (isset($args[1]))
            call_user_func_array(array($this->redis, 'RPUSH'), $args);
    }

    protected function checkAndAddMatchesToSetWithScores($user_array, $likedIds, $myGender, $availabile_set, $scores) {
		$matches_scores_key_exists = $this->uu->ifKeyExists($this->keyWithScores);
		if ($matches_scores_key_exists)  $this->redis->DEL($this->keyWithScores);
		if ($this->uu->shouldStoreMatchesWithScores($this->userId, $myGender)) {

        	//if gender is female then dont add -1 as first value so that key automatically deletes after all the actions
	        $args[0] = $this->keyWithScores;
        	$args[1] = -1;
	        $i = 2;
        	foreach ($user_array as $key) {
	            if (true) {
        	        if (isset($likedIds) && in_array($key, $likedIds)) {
                	    $args[$i] = $key . 'l';
	                } else {
        	            $args[$i] = $key;
               	    }
           	    } else {
                	$args[$i] = $key;
            	}
                $args[$i] .= (isset($availabile_set[$key])) ? ":$availabile_set[$key]" : ":0";
                // total score stored for males to handle - limit uninstall matches
                if ($this->gender == "M")
                	$args[$i] .= (isset($scores[$key]) && isset($scores[$key]['totalScore'])) ? ":" . $scores[$key]['totalScore'] : ":CP";
                else
                	$args[$i] .= (isset($scores[$key]) && isset($scores[$key]['total'])) ? ":" . $scores[$key]['total'] : ":CP";
                $i++;
        	}
     	    if (isset($args[1]))
            	call_user_func_array(array($this->redis, 'RPUSH'), $args);
		}
    }

    public function addMatchesToSetOnly($user_array, $likedIds, $availabile_set) {
	$args = array();
        $i = 0;
        foreach ($user_array as $key) {
            if (isset($likedIds) && in_array($key, $likedIds)) {
                $args[$i] = $key . 'l';
            } else {
                $args[$i] = $key;
            }
            $args[$i] .= (isset($availabile_set[$key])) ? ":$availabile_set[$key]" : ":0";
            $i++;
        }
	return array($args,$likedIds);
	
    }
    protected function getLocationQuery($prefData) {
        $n = 0;
        $m = 0;

        if ($this->checkissetorEmpty($prefData ['location'])) {
            // 113:5268-42504;1:827:829:5296;
            $location_string = explode(";", $prefData ['location']);
            $final_array = array();
            foreach ($location_string as $val) {
                if (strlen($val) == 0) {
                    continue;
                }
                $state_segment = explode(":", $val);
                $country = $state_segment [0];
                $final_array [$country] = array();
                for ($j = 1; $j < count($state_segment); $j ++) {
                    $city_segment = explode("-", $state_segment [$j]);
                    $state = $city_segment [0];
                    $m++;
                    $final_array [$country] [$state] = array();
                    for ($k = 1; $k < count($city_segment); $k ++) {
                        $final_array [$country] [$state] [$city_segment [$k]] = "";
                        $n++;
                    }
                }
            }
            $locationQuery = "";
            $c_counter = 0;
            foreach ($final_array as $ccode => $state_data) {
                if ($c_counter == 0)
                    $locationQuery .= " (country='$ccode' ";
                else
                    $locationQuery .= " or (country='$ccode' ";
                if (count($state_data) > 0) {
                    $locationQuery .= " and (";
                }
                $s_counter = 0;
                foreach ($state_data as $state => $city_data) {
                    if ($s_counter == 0)
                        $locationQuery .= " (state='$state' ";
                    else
                        $locationQuery .= " or (state='$state' ";

                    if (count($city_data) > 0) {
                        $cities = implode(",", array_keys($city_data));
                        $locationQuery .= " and city in ($cities) ";
                    }
                    $s_counter ++;
                    $locationQuery .= ")";

                    //additional checks if the state is NCR then add more cities
                    //and if the city level check is there then add that particular city only
                    if (count($city_data) == 0) {
                        //if state is in ambigous then add cities separetely as well
                        if (in_array($state, $this->ambigousStates)) {
                            //	//47968 - Greater Noida, 47965 - noida, 47964 - gurgaon, 47967 - ghaziabad, 47966 - faridabad
                            //states : 2168 - NCR,  2176- Haryana, 2193 - UP
                            if ($state == 2168)
                                $locationQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesNCR) . "))";
                            elseif ($state == 2176)
                                $locationQuery .= " OR (city in (47964,47966 ))";
                            elseif ($state == 2193)
                                $locationQuery .= " OR (city in (47968, 47965, 47967))";
                        }elseif (in_array($state, $this->ambiguousStatesChandigarh)) {
                            //if punjab/chd is filled in state and city is not filled
                            $locationQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesChandigarh) . "))";
                        }
                    } else {
                        //if city is ambigous add city separately
                        $cityClash = array_intersect(array_keys($city_data), $this->ambiguousCitiesNCR);
                        if (!empty($cityClash)) {
                            $locationQuery .= " OR (city in (" . implode(',', $cityClash) . "))";
                        }
                        $cityClashForChd = array_intersect(array_keys($city_data), $this->ambiguousCitiesChandigarh);
                        if (!empty($cityClashForChd)) {
                            $locationQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesChandigarh) . "))";
                        }
                    }
                }
                if (count($state_data) > 0) {
                    $locationQuery .= " )";
                }
                $c_counter ++;
                $locationQuery .= " ) ";
            }
            // stay_country | stay_state | stay_city
        } else {
            $locationQuery = null;
        }

        return $locationQuery;
    }

    /*
     * Generate templates for fresh and like query for females
     */

    protected function getQueryForFemales($prefData, $myData, $myGender, $myBucket, $user_id) {
        $prefLocation = $prefData['location'];
        $myCity = $myData['stay_city'];
        $myState = $myData['stay_state'];
        $myCountry = $myData['stay_country'];
        $amISelect = $this->isSelectMember;

        $locationCase = $this->getLocationCase($prefLocation, $myCity, $myState, $myCountry, $myGender);
        $piCase = $this->getPIRanksAndFiltersForFemale($myBucket, "2",$myCountry);
        $educationCase = Utils::$education_rules_enabled ? $this->getEducationAmplificationCaseForFemale($myData) : "0";
        #$activityCase = $this->getActivityCase();
	$activityCase = "case when upa.activity_bits_number = 0 then 0 else (case when upa.activity_bits_number%pow(2,3) > 0 then 1 else (case when upa.activity_bits_number%pow(2,7) > 0 then 0.75 else 0.5 end ) end ) end ";
        
        $tmSelectStringFreshRank    = $this->isSelectMember ? " case when u.isSelect = 1 then ".SELECT_FEMALE_FRESH_SCORE." else 0 end " : " 0 ";
        $tmSelectStringLikeRank     = $this->isSelectMember ? " case when u.isSelect = 1 then ".SELECT_FEMALE_LIKE_SCORE." else 0 end " : " 0 ";
        $nriStringRank              = $this->getNRIRankingString($myCountry,$myBucket,$amISelect);

        $freshProfileQuery = "select u.user_id,  0 as likeflag, $locationCase as locationScore, u.pi as pi, u.bucket as bucket, " . $piCase['piRankCaseFreshQuery'] . " as piScore, $activityCase as activityScore,
			$educationCase as educationScore,
			$locationCase + " . $piCase['piRankCaseFreshQuery'] . " + $activityCase + $educationCase + $tmSelectStringFreshRank + $nriStringRank as totalScore,
			$locationCase + " . $piCase['piRankCaseFreshQuery'] . " + $activityCase + $educationCase + $tmSelectStringFreshRank + $nriStringRank as score
					 FROM user_search  u left join user_pi_activity upa on u.user_id = upa.user_id ";

        $likeQuery = "select u.user_id,  case when ul.user1 is not null then 1 else 0 end as likeflag, u.pi as pi, u.bucket as bucket, $locationCase as locationScore, " . $piCase['piRankCaseLikeQuery'] . " as piScore, $activityCase as activityScore, 
			$educationCase as educationScore,
			$locationCase + " . $piCase['piRankCaseLikeQuery'] . " + $activityCase + $educationCase + $tmSelectStringLikeRank + $nriStringRank + case when ul.user1 is not null then 10000 else 0 end as totalScore,
			$locationCase + " . $piCase['piRankCaseLikeQuery'] . " + $activityCase + $educationCase + $tmSelectStringLikeRank + $nriStringRank as score
					 FROM user_search  u  left join user_availability ua on u.user_id = ua.user_id left join user_pi_activity upa on u.user_id = upa.user_id 
					  JOIN user_like ul on u.user_id= ul.user1 and ul.user2 = $user_id ";
        return array("freshProfileQuery" => $freshProfileQuery, "likeQuery" => $likeQuery);
    }

    protected function getLikeQueryForMales() {
        $tmSelectStringFilter = $this->isSelectMember ? "" : ""; # and u.isSelect = 0 "; #Uncomment this when we do not want to show a select female like to non-select male
        return "SELECT ul.user1 as user_id from user_like ul join user_search u on u.user_id = ul.user1 $tmSelectStringFilter where ul.user2 = $this->userId";
    }

    protected function getUserIdToFilterQuery($user_id_to_filter_total) {
        if (strlen($user_id_to_filter_total) > 0) {
            return " and u.user_id not in (" . $user_id_to_filter_total . ")";
        } else {
            return '';
        }
    }

    protected function getNotInFilterQuery($user_id_to_filter_nonLikedMe) {
        if (strlen($user_id_to_filter_nonLikedMe) > 0) {
            return " and ul.user1 not in (" . $user_id_to_filter_nonLikedMe . ")";
        } else {
            return '';
        }
    }

    /*
     * The function generates the fresh query (all the feamles who have not
     * liked/hidden the user before) for males.
     */

    protected function getFreshProfileQueryForMales($prefData, $myData, $myGender, $myBucket) {
        $prefLocation = $prefData['location'];
        $myCity = $myData['stay_city'];
        $myState = $myData['stay_state'];
        $myCountry = $myData['stay_country'];
        $amISelect = $this->isSelectMember;

        $locationCase = $this->getLocationCase($prefLocation, $myCity, $myState, $myCountry, $myGender);
        $piCase = $this->getPIRankForMale($myBucket, "2");
        $uninstallCase = $this->getUninstallRankCase();
        $activityCase = $this->getActivityCase();
        $selectScore  = $amISelect ? $this->getSelectScoreForMales() : SELECT_MALE_FRESH_SCORE;
        $educationCase = Utils::$education_rules_enabled ? $this->getEducationAmplificationCaseForMale($myData) : "0"; 

        $tmSelectStringRank   = $amISelect ? " case when u.isSelect = 1 then ".$selectScore." else 0 end " : " 0 ";
        $nriStringRank        = $this->getNRIRankingString($myCountry, $myBucket,$amISelect);
        
        return "select u.user_id,  0 as likeflag, "
                    . "$locationCase as locationScore, pi, bucket, "
                    . "$piCase as piScore, "
                    . "$activityCase as activityScore,"
                    . "$educationCase as educationScore,"
                    . "$tmSelectStringRank as tmSelectStringRank,"
                    . "$nriStringRank as nriStringRank,"
                    . "$locationCase + $piCase + $activityCase + $educationCase + $uninstallCase + $tmSelectStringRank + $nriStringRank as totalScore,"
                    . "$locationCase + $piCase + $activityCase + $educationCase + $tmSelectStringRank + $nriStringRank as score
					 FROM user_search  u  left join user_availability ua on u.user_id = ua.user_id
					 					  left join user_lastlogin ul on u.user_id = ul.user_id ";
    }

    /*
     * Perform results and score logging.
     */

    public function doLogging($final_score_set, $result_like_set, $likeQuery, $result_fresh_set, $freshProfileQuery, $countToFetchFresh, $logString) {
        $current_timestamp = date('Y-m-d H:i:s');
        $this->logIndividualScore($final_score_set, $logString, $current_timestamp);
        $this->storeResults($result_like_set, $likeQuery, 1, $logString, null, $current_timestamp);
        $this->storeResults($result_fresh_set, $freshProfileQuery, 0, $logString, $countToFetchFresh, $current_timestamp);
    }

    /*
     * The function does the required sorting/shuffling on the generated 
     * set of recommendations.
     */

	protected function sortAndShuffleIds($like_set, $fresh_set, $freshCompeteWithLikes) {
        
        if(sizeof($like_set) == 0 | sizeof($fresh_set) <= 10 | $this->gender == "F") {
            $total_set = $like_set + $fresh_set;
            if ($freshCompeteWithLikes == true) {
            	$total_set = $this->custom_stable_sort($total_set);
            }
            $ids = array_keys($total_set);
            //do the shuffling only for male users 
            if ($this->gender == "M") {
                shuffle($ids);
            }
        } else {
            //For males and when likes > 0 and fresh count > 10
            $top_b = array_slice($fresh_set, 0, 10, TRUE);
            $end_b = array_slice($fresh_set, 10, sizeof($fresh_set)-10, TRUE);
            $a_top_b = $like_set + $top_b;
            $a_top_b_keys = array_keys($a_top_b);
            shuffle($a_top_b_keys);
            $end_b_keys = array_keys($end_b);
			shuffle($end_b_keys);
            $ids = array_merge($a_top_b_keys,$end_b_keys);
        }
        return $ids;
    }
    
    
    protected function custom_stable_sort($matches_with_scores) {
    	$matches_ids = array_keys($matches_with_scores);
    	for ($i = 1; $i < count($matches_ids); $i++) {
		$temp = $matches_ids[$i];
    		$element = $matches_with_scores[$matches_ids[$i]];
    		$j = $i;
    		while ($j > 0 && $matches_with_scores[$matches_ids[$j - 1]] < $element) {
    			$matches_ids[$j] = $matches_ids[$j - 1];
    			$j = $j - 1;
    		}
    		$matches_ids[$j] = $temp;
    	}
    	$result = array();
    	foreach ($matches_ids as $m) 
    		$result[$m] = $matches_with_scores[$m];
    	return $result;
    }

    protected function getEducationSuppressionFilterQueryForFemale($myData) {
    	$myDegree = isset($myData['highest_degree']) ? $myData['highest_degree'] : null;
    	$supDegrees = array();
    	if ($myDegree != null && $myDegree != "")
    		$supDegrees = $this->uuDBO->getEducationSuppressionDegreesForFemale($myDegree);
    	if (count($supDegrees) > 0)
	    	return " and u.highest_education not in (" . implode(',', $supDegrees) .  ") ";
    	return "";
    }
    
    protected function getEducationSuppressionFilterQueryForMale($myData) {
    	$myDegree = isset($myData['highest_degree']) ? $myData['highest_degree'] : null;
    	$supDegrees = array();
    	if ($myDegree != null && $myDegree != "")
    		$supDegrees = $this->uuDBO->getEducationSuppressionDegreesForMale($myDegree);
    	if (count($supDegrees) > 0)
    		return " and u.highest_education not in (" . implode(',', $supDegrees) .  ") ";
    	return "";
    }
    
    protected function getEducationAmplificationCaseForFemale($myData) {
    	$myDegree = isset($myData['highest_degree']) ? $myData['highest_degree'] : null;
    	$ampDegrees = array();
    	if ($myDegree != null && $myDegree != "")
    		$ampDegrees = $this->uuDBO->getEducationAmplificationDegreesForFemale($myDegree);
    	if (count($ampDegrees) > 0)
    		return "case when u.highest_education in (" . implode(',', $ampDegrees) .  ") then 0.25 else 0 end";
    	return "0";
    }
    
    protected function getEducationAmplificationCaseForMale($myData) {
    	$myDegree = isset($myData['highest_degree']) ? $myData['highest_degree'] : null;
    	$ampDegrees = array();
        $supDegrees = array();
    	if ($myDegree != null && $myDegree != "") {
    		$ampDegrees = $this->uuDBO->getEducationAmplificationDegreesForMale($myDegree);
                $supDegrees = $this->uuDBO->getEducationSuppressionDegreesForMale($myDegree);
        }
        $prefix = "";
        $suffix = "";
    	if (count($ampDegrees) > 0) {
    		$prefix  = " case when u.highest_education in (" . implode(',', $ampDegrees) .  ") then 0.25 else ";
                $suffix  = " end ";
        }
        if (count($supDegrees) > 0) {
    		$prefix .= " case when u.highest_education in (" . implode(',', $supDegrees) .  ") then -0.25 else ";
                $suffix .= " end ";
        }
        $result = $prefix."0".$suffix;
    	return $result;
    }
    
    public static function getUsersAlreadyEngagedViaSpark($user_id) {
        $sql = "select t.user, t.flag from (
                 select user1 as user,0 as flag from user_spark where user2 = ?
                 union 
                 select user2 as user,1 as flag from user_spark where user1 = ?)t 
                 join user_search u on t.user=u.user_id";

        $output_sparkedMe = array();
        $output_sparkedByMe  = array();
        $data = Query::SELECT($sql, array($user_id, $user_id));
        foreach ($data as $val) {
            if ($val['flag'] == 0) {
                $output_sparkedMe[] = $val['user'];
            }
            if ($val['flag'] == 1) {
                $output_sparkedByMe[] = $val['user'];
            }
        }
        return array("output_sparkedMe" => $output_sparkedMe, "output_sparkedByMe" => $output_sparkedByMe);
    }
    
    public function getNRIRankingString($myCountry, $myBucket, $amISelect) {
        $nriString = " 0 ";
        if($this->gender == 'M') {
            $nriString =  ($myCountry == '113') 
                            ? ( in_array($myBucket,array(4,5)) 
                                ? " case when country != 113 then ".SHOW_NRI_FEMALES_TO_HIGHER_BUCKET_MALES." else 0 end "
                                : " 0 "
                              )
                              . " + " .
                              ( $amISelect
                                ? " case when country != 113 then ".SHOW_NRI_FEMALES_TO_SELECT_MALES." else 0 end " 
                                : " 0 "
                              )
                            : " case when country = '113' and u.bucket in (4,5) then ".SHOW_HIGHER_BUCKET_FEMALES_TO_NRI_MALES." else 0 end + case when country = '113' and isSelect = 1 then ".SHOW_SELECT_FEMALES_TO_NRI_MALES." else 0 end ";
        } else {
            $nriString =  ($myCountry == '113') 
                            ? ( in_array($myBucket,array(4,5)) 
                                ? " case when country != 113 then ".SHOW_NRI_MALES_TO_HIGHER_BUCKET_FEMALES." else 0 end "
                                : " 0 "
                              )
                              . " + " .
                              ( $amISelect
                                ? " case when country != 113 then ".SHOW_NRI_MALES_TO_SELECT_FEMALES." else 0 end " 
                                : " 0 "
                              )
                            : " case when country = '113' and u.bucket in (4,5) then ".SHOW_HIGHER_BUCKET_MALES_TO_NRI_FEMALES." else 0 end + case when country = '113' and isSelect = 1 then ".SHOW_SELECT_MALES_TO_NRI_FEMALES." else 0 end ";
        }
        return $nriString;
    }
    
    public function getNRIFilterString($myCity,$myCountry, $myBucket, $amISelect) {
        $nriString = "";
        #| Bengaluru     || Pune          || Hyderabad     || Mumbai        || Chennai       |
        #| Delhi         || Kolkata       || Chandigarh    || Gurgaon       || Noida         |
        $metroCities = array(4058,4062,6453,6457,13147,16743,42561,42795,47964,47965,47968);
        if($myCountry != 113) {
            $nriString = " and (country = '$myCountry' or (country = '113' and u.city in (4058,4062,6453,6457,13147,16743,42561,42795,47964,47965,47968) and (u.bucket in (4,5) or isSelect = 1))) ";
        }
        else {
            if(!in_array($myCity,$metroCities) || (!in_array($myBucket,array(4,5)) && !$amISelect)) {
                $nriString = " and country = 113 ";
            }
        }
        return $nriString;
    }
}

?>
