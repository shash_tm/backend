<?php

require_once dirname(__FILE__) . "/../matches/matchScore.php";
require_once dirname(__FILE__) . "/../include/Utils.php";
require_once dirname(__FILE__) . "/../fb.php";
require_once dirname(__FILE__) . "/../include/function.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/../abstraction/query.php";
require_once dirname(__FILE__) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../include/matchConstants.php";
require_once dirname(__FILE__) . "/../include/config.php";
require_once dirname(__FILE__) . "/../UserData.php";
require_once dirname(__FILE__) . "/../logging/QueryLog.class.php";
require_once dirname(__FILE__) . "/BaseRecommendationEngine.php";
require_once dirname(__FILE__) . "/../Advertising/ProfileCampaign.php";

define('SELECT_MALE_FRESH_SCORE',2);

class RecommendationEngine_ZeroAcThrottle extends BaseRecommendationEngine {

    private $params = array();

    function __construct($user_id, $selectedABParams) {
        $this->params = $selectedABParams;
        parent::__construct($user_id);
    }

    private function getInActiveFilterString($gender) {
        $inActiveFilterString = '';
        if ($gender == "M") {
                $inActiveFilterString = " and ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=28 ";
        }
        return $inActiveFilterString;
    }
    
    private function getConfiguredLineOfEquilibrium() {
        if (key_exists('slopeForLoe', $this->params)) {
            return $this->params['slopeForLoe'];
        } else {
            return array(3, 1);
        }
    }
    
    private function getConfiguredDensityBinsForAvailabilityCounters() {
        if (key_exists('densityBinsAvailabilityCounters', $this->params)) {
            return $this->params['densityBinsAvailabilityCounters'];
        } else {
            return 10;
        }
    }
    
    private function getConfiguredTimeForDensityBinsForAvailabilityCounters() {
        if (key_exists('expiryTimeDensityBasedBinning', $this->params)) {
            return $this->params['expiryTimeDensityBasedBinning'];
        } else {
            return 3600;
        }
    }
    
    private function getConfiguredPercentageUninstalledFemalesForSparkMales() {
        if (key_exists('percentageUninstalledFemalesForSparkMales', $this->params)) {
            return $this->params['percentageUninstalledFemalesForSparkMales'];
        } else {
            return 0.5;
        }
    }

    /*
     *       ^   Above the line:
     *       |   FScore = -ve (high dot product should have high score)      =  -(0.5)(a.b)/(max(a.b))
     *       |   EScore = +ve (high dot product should have low score)       = 0.5-(0.5)(a.b)/(max(a.b))
     *       |   SparkFScore = -ve (high dot product should have high score) =  {(0.5)-(0.5)(a.b)/(max(a.b))}/(s.c)
     *       |   SparkEScore = +ve (high dot product should have low score)  = {1-(0.5)(a.b)/(max(a.b))}/(s.c)
     *       |   
     *       |                              *
     * Fresh |                        *
     * Count |                 *          Below the line :
     *  F(l) |          *                 Fscore = +ve (high dot product should have low score)       = 0.5-(0.5)(a.b)/(max(a.b))
     *       |  *                         EScore = -ve (high dot product should have high score)      =  -(0.5)(a.b)/(max(a.b))
     *       |                            SparkFscore = +ve (high dot product should have low score)  = {1-(0.5)(a.b)/(max(a.b))}/(s.c)
     *       |                            SparkEScore = -ve (high dot product should have high score) = {0.5-(0.5)(a.b)/(max(a.b))}/(s.c)
     *        ------------------------------->
     *               Existing Count E(l)
     * 
     * The entire quadrant is now divided into zones by lines of the type : x + y = k
     * where each k is decided such that equal number of females fall into each zone. (Density based division)
     * What we do is break the females by their sum of counters at seperate %ile values as configured by densityBinsAvailabilityCounters
     * Each zone is assigned a score - called fixedScore.
     * Entries within a zone are then sorted using the above specified formula (in graph) - variableScore.
     * Essentially, each zone is exclusive in the sense that the score generated (fixedScore + variableScore)
     * is separated by the lines x+y = k. There are no overlaps. We promote zones closer to the origin
     * and within a single zone we try to maintain a balance between the magnitude and angle (slope).
     * For a non-spark user the score is like (0.95 +- (-0.05,0.05))
     * For spark user the score is like       (0.9  +- (    0, 0.1))
     * * * * * * * * * * * * * * * * * * * * * * * */

    protected function getAvailabilityCase() {
        global $redis;
        
        #Get configurations
        $loe = $this->getConfiguredLineOfEquilibrium();
        $numberOfBins = $this->getConfiguredDensityBinsForAvailabilityCounters();
        $expiryTimeDensityBasedBinning = $this->getConfiguredTimeForDensityBinsForAvailabilityCounters();
        
        # Density based segregation of the counters. The results are saved in redis for some time.
        $key = Utils::$redis_keys['densityBasedAvailabilityCountersLimits'];
        $separationPoints = array();
        $limitCounterKeyExists = $redis->EXISTS($key);
        if(!$limitCounterKeyExists) {
            #Set maximum concat limit - check if this needs to be run on master \\TODO!
            $setMaximumConcatLimit = "set group_concat_max_len = 10485760;";
            Query::UPDATE($setMaximumConcatLimit, null);
        
            $separationPointQueryString = "SELECT ";
            for($binNumber=0 ; $binNumber<$numberOfBins-1;$binNumber++){
                $percentile = round(100*($binNumber+1)/$numberOfBins);
                $trailerString = ($binNumber == $numberOfBins-2) ? "/100) * COUNT(*) + 1), ',', -1) AS DECIMAL)" : "/100) * COUNT(*) + 1), ',', -1) AS DECIMAL),";
                $separationPointQueryString = 
                        $separationPointQueryString 
                        . " CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.counters ORDER BY t.counters SEPARATOR ','),',', ( "
                        . $percentile
                        . $trailerString;
            }
            $separationPointQueryString = 
                    $separationPointQueryString
                    . " FROM (select ifnull(ua.slots_filled,0)+ifnull(ua.fresh_slots_filled,0)  as counters from user_search us join user_availability ua on us.user_id = ua.user_id and us.gender = 'F') t;";        
            $separationPoints = Query::SELECT($separationPointQueryString, null, "slave", true);
            $separationPoints = array_values($separationPoints);
            #Add the key to redis
            $keyPoints = $separationPoints;
            array_unshift($keyPoints,$key);
			if(!$redis->EXISTS($key)) {
				#TODO - this should ideally be protected by a semaphore. IT still works because redis's operations are sequential.
	            call_user_func_array(array($redis, 'RPUSH'), $keyPoints);
    	        $redis->EXPIRE($key, $expiryTimeDensityBasedBinning);
			}
        }
        else {
            $separationPoints = $this->redis->LRANGE($key, 0, -1);
        }

        // fixedScores shall be dependent upon the density based segregation of the counters.
        $fixedScores =  " case ";
        $fixedScoresEndString = " end";
        for($binNumber=0 ; $binNumber<$numberOfBins-1;$binNumber++){
            $fixedScores = 
                    $fixedScores
                    . " when (ifnull(fresh_slots_filled,0) + ifnull(slots_filled,0)) < "
                    . $separationPoints[$binNumber]
                    . " then "
                    . ($this->canUserSendOutSparks ? (($numberOfBins - ($binNumber + 1)) / ($numberOfBins)) : ((2 * $numberOfBins - (2 * $binNumber + 1)) / (2 * $numberOfBins)));
        }
        $fixedScores = $fixedScores . ($this->canUserSendOutSparks ? (" else 0.0") : (" else " . (1 / (2 * $numberOfBins))));
        $fixedScores = $fixedScores . $fixedScoresEndString;
        
        $dotProduct = "((1+ifnull(slots_filled,0))*$loe[0]+(1+ifnull(fresh_slots_filled,0))*$loe[1])";
        $maxDotProducts = Query::SELECT("select max$dotProduct as max from user_availability;", null, "slave", true);
        $maxDotProduct = $maxDotProducts['max'];

        $variableScores = "";
        if($this->canUserSendOutSparks) {
            if ($this->isFreshBucket) {
                $variableScores = "case "
                                    . "when ((1+ifnull(fresh_slots_filled,0))/(1+ifnull(slots_filled,0)) > $loe[1]/$loe[0]) "
                                        . "then ( ( (1/(2 * $numberOfBins)) + ( ( -1*($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) ) / (1+ifnull(pending_sparks,0)) )"
                                    . "else "
                                        . "( ( (1/(2 * $numberOfBins)) + ( ( 1-($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) ) / (1+ifnull(pending_sparks,0)) ) end";
            } else {
                $variableScores = "case "
                                    . "when ((1+ifnull(fresh_slots_filled,0))/(1+ifnull(slots_filled,0)) > $loe[1]/$loe[0]) "
                                        . "then ( ( (1/(2 * $numberOfBins)) + ( ( 1-($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) ) / (1+ifnull(pending_sparks,0)) ) "
                                    . "else "
                                        . "( ( (1/(2 * $numberOfBins)) + ( ( -1*($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) ) / (1+ifnull(pending_sparks,0)) ) end";
            }
        }
        else {
            if ($this->isFreshBucket) {
                $variableScores = "case "
                                    . "when ((1+ifnull(fresh_slots_filled,0))/(1+ifnull(slots_filled,0)) > $loe[1]/$loe[0]) "
                                        . "then ( ( -1*($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) "
                                    . "else "
                                        . "( ( 1-($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) end";
            } else {
                $variableScores = "case "
                                    . "when ((1+ifnull(fresh_slots_filled,0))/(1+ifnull(slots_filled,0)) > $loe[1]/$loe[0]) "
                                        . "then ( ( 1-($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) "
                                    . "else "
                                        . "( ( -1*($dotProduct/$maxDotProduct) )/(2 * $numberOfBins) ) end";
            }
        }
        return $fixedScores . " + " . $variableScores;
    }

    protected function getFreshProfileQueryForMales($prefData, $myData, $myGender, $myBucket) {
        $prefLocation = $prefData['location'];
        $myCity = $myData['stay_city'];
        $myState = $myData['stay_state'];
        $myCountry = $myData['stay_country'];
        $amISelect = $this->isSelectMember;

        $locationCase = $this->getLocationCase($prefLocation, $myCity, $myState, $myCountry, $myGender);
        $piCase = $this->getPIRankForMale($myBucket, "2");
        $uninstallCase = $this->getUninstallRankCase();
        $activityCase = $this->getActivityCase();
        $selectScore  = $amISelect ? $this->getSelectScoreForMales() : SELECT_MALE_FRESH_SCORE;
        $availabilityCase = $this->getAvailabilityCase();
        $educationCase = Utils::$education_rules_enabled ? $this->getEducationAmplificationCaseForMale($myData) : "0";

        $tmSelectStringRank   = $amISelect ? " case when u.isSelect = 1 then ".$selectScore." else 0 end " : " 0 ";
        $nriStringRank        = $this->getNRIRankingString($myCountry, $myBucket,$amISelect);

        return "select u.user_id,  0 as likeflag, "
                . "$locationCase as locationScore, pi, bucket, "
                . "$piCase as piScore, "
                . "$activityCase as activityScore, "
                . "$availabilityCase as  availabilityCase,"
                . "$educationCase as educationScore,"
                . "$tmSelectStringRank as tmSelectStringRank,"
                . "$nriStringRank as nriStringRank,"
                . "$locationCase + $piCase  + $availabilityCase + $educationCase + $uninstallCase + $tmSelectStringRank + $nriStringRank as totalScore,"
                . "$locationCase + $piCase  + $availabilityCase + $educationCase + $tmSelectStringRank + $nriStringRank as score
					 FROM user_search  u  left join user_availability ua on u.user_id = ua.user_id 
                                             left join user_lastlogin ul on u.user_id = ul.user_id ";
    }

    /**
     * Return the count to be fetched for a user for fresh profiles
     */
    protected function getFreshProfilesCountForFeMales($piData) {
        $count = 0;
        if (key_exists('freshProfileCountForEachBucket', $this->params) &&
                sizeof($this->params['freshProfileCountForEachBucket']) == 6) {
            $freshProfileCounts = $this->params['freshProfileCountForEachBucket'];
            $count = $freshProfileCounts[$piData['bucket']];
        } else {
            $count = parent::getFreshProfilesCountForFeMales($piData);
        }
        return $count;
    }

    /**
     * Get batch size for female based on bucket
     */
    protected function setBatchSizeForFemale($piData) {
        $count = 0;
        if (key_exists('totalProfilesForFemale', $this->params) &&
                sizeof($this->params['totalProfilesForFemale']) == 6) {
            $freshProfileCounts = $this->params['totalProfilesForFemale'];
            $count = $freshProfileCounts[$piData['bucket']];
        } else {
            $count = parent::setBatchSizeForFemale($piData);
        }
        $this->totalProfilesForFemale = $count;
    }

    /**
     * NOTE: whenever you add/remove any new data point from a query, update the sanity in test cases
     * finding how many matches to find as fresh
     * in male case - 
     *      fresh -  it'll be controlled - based on bucket and active chats (follow the below logic in case of edit as well)
     *      likes - push after every x hours
     * in female case -
     *      fresh & likes - send as per quota even after edit 
     */
    public function generateMatchesUsingMemoryTableClassSpecific($user_id, $actionTaken = 0, $logString = null, $sessionId = null, $containingUsers = null, $passedFreshLimit = 0,$passedLikeLimit = 0) {

        $myPIData = $this->uuDBO->getUserPiInfo(array($user_id), true);
        $myPi = $myPIData['actual_pi'];
        $myBucket = $myPIData['bucket'];
        $this->setIsFreshBucket($myPi, $myBucket);
        $this->setBatchSizeForFemale($myPIData);
        $isFreshCall = $this->isFreshCall($actionTaken);
        if ($this->gender == "M") {
            $fetchFreshCount = $this->getFreshProfilesCountForMales($isFreshCall, $myPIData, $actionTaken);
        } else {
            $fetchFreshCount = $this->getFreshProfilesCountForFeMales($myPIData);
        }
        $user_id_to_filter = $this->getRecommendationSet($this->userId);
        $spark_id_to_filter = BaseRecommendationEngine::getUsersAlreadyEngagedViaSpark($this->userId);
        
        $output_sparkedMe = $spark_id_to_filter['output_sparkedMe'];
        $output_sparkedByMe  = $spark_id_to_filter['output_sparkedByMe'];
        
        $iLikedIds = $user_id_to_filter['iLiked'];
        $likedMeFilters = $user_id_to_filter['likedMe'];
        $nonLikedMe = $user_id_to_filter['nonLikedMe'];
        $total_ids = array_unique(array_merge($likedMeFilters, $nonLikedMe, $iLikedIds,$output_sparkedMe,$output_sparkedByMe));
        $nonLikedMe_ids = array_unique(array_merge($nonLikedMe, $iLikedIds,$output_sparkedMe,$output_sparkedByMe));
        $user_id_to_filter_total = implode(",", array_values($total_ids));
        $user_id_to_filter_nonLikedMe = implode(',', array_values($nonLikedMe_ids));

        $prefData = $this->uuDBO->getUserPreferenceData(array($user_id), true);
        $myData = $this->uuDBO->getUserData(array($user_id), true);
        $myBasicInfo = $this->uuDBO->getUserBasicInfo(array($user_id), true);
        $myGender = $myBasicInfo['gender'];
        $oppositeGender = ($myGender == 'M') ? 'F' : 'M';
        $myAge = $myData['age'];
        $myState = $myData['stay_state'];
        $myCity  = $myData['stay_city'];
        $myCountry = $myData['stay_country'];
        $piCaseForFemales = $this->getPIRanksAndFiltersForFemale($myBucket, "2",$myCountry);
        $locationQuery = $this->getLocationQuery($prefData);

        if ($myGender == "M") {
            $freshProfileQuery = $this->getFreshProfileQueryForMales($prefData, $myData, $myGender, $myBucket);
            $likeQuery = $this->getLikeQueryForMales();
        } else {
            $query = $this->getQueryForFemales($prefData, $myData, $myGender, $myBucket, $user_id);
            $freshProfileQuery = $query["freshProfileQuery"];
            $likeQuery = $query["likeQuery"];
        }

        $baseQuery = $this->generateBaseQuery($prefData, $myData, $oppositeGender, $locationQuery, $myBucket);
        $userIdToFilter_Query = $this->getUserIdToFilterQuery($user_id_to_filter_total);
        $inActiveFemaleFilter = $this->getInActiveFilterString($this->gender);
        $freshProfileQuery .= $baseQuery . $userIdToFilter_Query . $inActiveFemaleFilter;
        $freshProfileQuery.=" and u.haspics=1 ";
        
        $tmSelectStringFreshFilter = ($this->isSelectMemberForThisSession || $myCountry != 113) ? "" : " and u.isSelect = 0 ";
        $freshProfileQuery.=$tmSelectStringFreshFilter;
        
	if ($containingUsers != null && !empty($containingUsers)) {
		#$containingUsersString = implode(',',$containingUsers);
		#$likeQuery .= " and u.user_id in (" . $containingUsersString . ") ";
		#$freshProfileQuery .= " and u.user_id in (" . $containingUsersString . ") ";
	}

        if ($this->gender == "M") {
            $freshProfileQuery .= " and (u.hide_flag is null or u.hide_flag = 0) ";
        }

        if(!($containingUsers != null && !empty($containingUsers))) {
            $freshProfileQuery .= "   order by totalScore desc, u.user_id%SECOND(now())  ";
        }
        else {
            # Sorting the sequence on the basis of remainder obtained by the remainder from the minimum possible user id
            $freshProfileQuery .= "   order by totalScore desc, u.user_id%($this->userId%209)  "; 
        }
        
        $notInFilterQuery = $this->getNotInFilterQuery($user_id_to_filter_nonLikedMe);
        $like_limit = null;
        if ($myGender == "M") {
            if($containingUsers != null  && !empty($containingUsers) && $passedLikeLimit >= 0) {
                $likeQuery .= $notInFilterQuery . " limit ". $passedLikeLimit;
                $like_limit = $passedLikeLimit;
            }
            else {
                $likeQuery .= $notInFilterQuery;
            }
        } else {
            if (key_exists('like_threshold_female', $this->params)) {
                $like_threshold_female = $this->params['like_threshold_female'];
            } else {
                $like_threshold_female = Utils::$like_threshold_female;
	    }
	    if($containingUsers != null  && !empty($containingUsers)) {
                if($passedLikeLimit >= 0) {
		    $likeQuery .= $baseQuery . $notInFilterQuery . $piCaseForFemales['piFilterCaseLikeQuery'] . "  order by totalScore desc, u.user_id%($this->userId%209)   limit " . $passedLikeLimit;
                    $like_limit = $passedLikeLimit;
                } else {
                    $likeQuery .= $baseQuery . $notInFilterQuery . $piCaseForFemales['piFilterCaseLikeQuery'] . "  order by totalScore desc, u.user_id%($this->userId%209) ";
                }
	    }else {
                    $limit = max(Utils::$femaleMutualFriendsLikedMatchesLimit, $like_threshold_female);
                    $like_limit = $like_threshold_female;
		    $likeQuery .= $baseQuery . $notInFilterQuery . $piCaseForFemales['piFilterCaseLikeQuery'] . "  order by totalScore desc , u.user_id%SECOND(now())   limit " . $like_threshold_female;
	    }
        }

        $result_like = array();
        $result_fresh = array();
        $freshCompeteWithLikes = null;
        $countToFetchFresh = $fetchFreshCount;
        $store_second_key = true;
        try {
            if (!empty($likedMeFilters) && isset($likedMeFilters)) {
                $result_like = Query::SELECT($likeQuery, null);
            }
            $limit = 0;
            if ($myGender == "M") {
		    $countToFetchFresh = $fetchFreshCount;
		    if($containingUsers != null && !empty($containingUsers)) {
                        if($passedFreshLimit >= 0) {
                        	$fetchFreshCount = $passedFreshLimit;
                        	$store_second_key = false;
			    $freshProfileQuery .= "  limit $passedFreshLimit";
                        }
		    }
                    else {
			    $likeQueryCount = count($result_like);
		            $limit = $fetchFreshCount;
            		    if (Utils::$maleMutualFriendsTotalMatchesLimit - $likeQueryCount > 0)
	       	                $limit = Utils::$maleMutualFriendsTotalMatchesLimit - $likeQueryCount;
		            $limit = max($limit, $fetchFreshCount);
		            if ($fetchFreshCount == 0) $limit = 0;
			    $freshProfileQuery .= "  limit $limit";
		    }
                if ($fetchFreshCount > 0 || $limit > 0) {
                    $result_fresh = Query::SELECT($freshProfileQuery, null);
                }
            } else {
                $freshCompeteWithLikes = true;
                $likeQueryCount = count($result_like);
                if ($likeQueryCount < ($this->totalProfilesForFemale - $fetchFreshCount)) {
                    $fetchFreshCount = $this->totalProfilesForFemale - $likeQueryCount;
                    $freshCompeteWithLikes = false;
                }
                $countToFetchFresh = $fetchFreshCount;
		    if($containingUsers != null && !empty($containingUsers)) {
                        if($passedFreshLimit >= 0) {
                        	$fetchFreshCount = $passedFreshLimit;
                        	$store_second_key = false;
			    $freshProfileQuery .= " limit $passedFreshLimit";
                        }
		    }else {
			    $limit = $fetchFreshCount;
 		            if (Utils::$femaleMutualFriendsTotalMatchesLimit - $likeQueryCount > 0)
                		$limit = Utils::$femaleMutualFriendsTotalMatchesLimit - $likeQueryCount;
		            $limit = max($limit, $fetchFreshCount);
		            if ($fetchFreshCount == 0) $limit = 0;
			    $freshProfileQuery .= " limit $limit";
		    }
                $result_fresh = Query::SELECT($freshProfileQuery, null);
            }
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
        }
        
        $idList = null;
        $likedIds = array();
        $result_fresh_set = array();
        $result_like_set = array();
        $scoreData_fresh_set = array();
        $scoreData_like_set = array();
        $location_sort_set = array();
        $availabile_set = array();
        $i = 0;
        $count_liked = 0;
        foreach ($result_like as $row) {
            if ($like_limit != null && $count_liked >= $like_limit)
                break;
            $likedIds[$row['user_id']] = $row['user_id'];
            $result_like_set[$row['user_id']] = $row['score'];
            $scoreData_like_set[$row['user_id']] = array(
                'likeFlag' => $row['likeflag'],
                'bucket' => $row['bucket'],
                'pi' => $row['pi'],
                'location' => $row['locationScore'],
                'piScore' => $row['piScore'],
                'activityScore' => $row['activityScore'],
                'availabilityScore' => $row['availabilityScore'],
                'educationScore' => $row['educationScore'],
                'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
                'total' => $row['score'],
                'totalScore' => $row['totalScore']
            );

            $location_sort_set[$row['user_id']] = $row['locationScore'];
            $idList[$i] = $row['user_id'];
            $availabile_set[$row['user_id']] = "0";
            if ($this->gender == "F" && $row['piScore'] > 0) {
                if ($row['bucket'] == 0 || $row['pi'] == -1) {
                    $availabile_set[$row['user_id']] = "F";
                } else {
                    $availabile_set[$row['user_id']] = "B";
                }
            }
            $i++;
	        $count_liked ++;
        }
//        The code is conflicting with the facebook mutual friends dual code that we have introduced.Removing this for now.
//        #FB_MutualFriendsConflict
//        $count_uninstall_to_keep = $fetchFreshCount;
//        if($this->gender == "M" && $this->canUserSendOutSparks) {
//        	// Check uninstalled matches in fresh calls
//        	$percentageUninstalledFemalesForSparkMales = $this->getConfiguredPercentageUninstalledFemalesForSparkMales();
//        	$count_uninstall_to_keep = $fetchFreshCount * $percentageUninstalledFemalesForSparkMales;
//        }
        $count_fresh = 0;
        //$count_uninstall = 0;
        foreach ($result_fresh as $row) {
            //#FB_MutualFriendsConflict
            // Handle total uninstalled females shown to the males who can Spark
            if ($count_fresh >= $fetchFreshCount) // || $count_uninstall >= $count_uninstall_to_keep)
                break;
            $result_fresh_set[$row['user_id']] = $row['score'];
            $scoreData_fresh_set[$row['user_id']] = array(
                'likeFlag' => $row['likeflag'],
                'bucket' => $row['bucket'],
                'pi' => $row['pi'],
                'location' => $row['locationScore'],
                'piScore' => $row['piScore'],
                'activityScore' => $row['activityScore'],
                'availabilityScore' => $row['availabilityScore'],
                'educationScore' => $row['educationScore'],
                'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
                'total' => $row['score'],
                'totalScore' => $row['totalScore']
            );
            $location_sort_set[$row['user_id']] = $row['locationScore'];
            $idList[$i] = $row['user_id'];
            $availabile_set[$row['user_id']] = "0";

            if ($this->gender == "M" && in_array($myBucket, array(4, 5))) {
                $availabile_set[$row['user_id']] = "B";
            } else if ($this->gender == "M" && $row['piScore'] > 0) {
                if ($this->isFreshBucket == true)
                    $availabile_set[$row['user_id']] = "F";
                else
                    $availabile_set[$row['user_id']] = "B";
            }
            $i++;
            $count_fresh ++;
            //if ($data['totalScore'] < -1000)
            //	$count_uninstall ++;
        }


		$remaining_fresh_set = array();
        $remaining_fresh_set_scores = array();
        $remaining_availabile_set = array();
        for ($index = $count_fresh; $index < count($result_fresh); $index++) {
        	$row = $result_fresh[$index];
        	$remaining_fresh_set[$row['user_id']] = $row['score'];
        	$remaining_fresh_set_scores[$row['user_id']] = array(
        			'likeFlag' => $row['likeflag'],
        			'bucket' => $row['bucket'],
        			'pi' => $row['pi'],
        			'location' => $row['locationScore'],
        			'piScore' => $row['piScore'],
        			'activityScore' => $row['activityScore'],
        			'availabilityScore' => $row['availabilityScore'],
        			'educationScore' => $row['educationScore'],
                                'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                                'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
        			'total' => $row['score'],
        			'totalScore' => $row['totalScore']
        	);
        	
        	$remaining_availabile_set[$row['user_id']] = "0";
        	
        	if ($this->gender == "M" && in_array($myBucket, array(4, 5))) {
        		$remaining_availabile_set[$row['user_id']] = "B";
        	} else if ($this->gender == "M" && $row['piScore'] > 0) {
        		if ($this->isFreshBucket == true)
        			$remaining_availabile_set[$row['user_id']] = "F";
        		else
        			$remaining_availabile_set[$row['user_id']] = "B";
        	}
        	 
        }

        $remaining_liked_set = array();
        $remaining_liked_set_scores = array();
        $remaining_liked_ids = array();
        for ($index = $count_liked; $index < count($result_like); $index ++) {
            $row = $result_like[$index];
            $remaining_liked_ids[$row['user_id']] = $row['user_id'];
            $remaining_liked_set[$row['user_id']] = $row['score'];
            $remaining_liked_set_scores[$row['user_id']] = array(
                    'likeFlag' => $row['likeflag'],
                    'bucket' => $row['bucket'],
                    'pi' => $row['pi'],
                    'location' => $row['locationScore'],
                    'piScore' => $row['piScore'],
                    'activityScore' => $row['activityScore'],
                    'availabilityScore' => $row['availabilityScore'],
                    'educationScore' => $row['educationScore'],
                    'tmSelectStringRank' => isset($row['tmSelectStringRank'])?$row['tmSelectStringRank']:0,
                    'nriStringRank' => isset($row['nriStringRank'])?$row['nriStringRank']:0,
                    'total' => $row['score'],
                    'totalScore' => $row['totalScore']
            );

            $remaining_availabile_set[$row['user_id']] = "0";
            if ($this->gender == "F" && $row['piScore'] > 0) {
                if ($row['bucket'] == 0 || $row['pi'] == -1) {
                    $remaining_availabile_set[$row['user_id']] = "F";
                } else {
                    $remaining_availabile_set[$row['user_id']] = "B";
                }
            }
        }
        
        // TODO: if diff between availability here and there, then update table
        // While running this query , a diff might occur during redis flush and query then inconsistent data
        //TODO: if action is taken on first 30 which is mix of like and fresh then rest is mix of both again?
        #$total_set = $result_like_set + $result_fresh_set;
        $total_score_set = $scoreData_like_set + $scoreData_fresh_set;
        if(!($containingUsers != null && !empty($containingUsers))) {
            $final_set = $this->sortAndShuffleIds($result_like_set, $result_fresh_set, $freshCompeteWithLikes);
        }
        else {
            $final_set = $result_like_set + $result_fresh_set;
            $final_set = array_keys($final_set);
        }
        
        $res = $this->extraneousProfileHandler->handleExtraneousProfiles($final_set, $myGender, $myAge, $myState, $myCity, $sessionId, ($this->isSelectMember || $myCountry != 113) ? array() : array());
        $final_set      = $res["final_set"];
        $repeat_likes   = $res["repeat_likes"];
        $selectSprinkledProfiles = $res["selectSprinkledProfiles"];
        
        $final_score_set = array();
        foreach ($final_set as $key) {
            if (key_exists($key, $total_score_set)) {
                $final_score_set[$key] = $total_score_set[$key];
            }
        }
	$aggregateInfoOfMatches = array();
	$aggregateInfoOfMatches['final_set'] = $final_set;
        $aggregateInfoOfMatches['repeat_likes'] = $repeat_likes;
        $aggregateInfoOfMatches['selectSprinkledProfiles'] = $selectSprinkledProfiles;
        
	$aggregateInfoOfMatches['liked_ids'] = $likedIds;
	$aggregateInfoOfMatches['myGender'] = $myGender;
	$aggregateInfoOfMatches['availabile_set'] = $availabile_set;
	$aggregateInfoOfMatches['actionTaken'] = $actionTaken;

	$aggregateInfoOfMatches['final_score_set'] = $final_score_set;
	$aggregateInfoOfMatches['result_like_set'] = $result_like_set;
	$aggregateInfoOfMatches['likeQuery'] = $likeQuery;
	$aggregateInfoOfMatches['result_fresh_set'] = $result_fresh_set;
	$aggregateInfoOfMatches['freshProfileQuery'] = $freshProfileQuery;
	$aggregateInfoOfMatches['countToFetchFresh'] = $countToFetchFresh;
	$aggregateInfoOfMatches['logString'] = $logString;

	$aggregateInfoOfMatches['final_set_second_key'] = array_merge($final_set, array_merge(array_keys($remaining_fresh_set), array_keys($remaining_liked_set)));
	$aggregateInfoOfMatches['liked_ids_second_key'] = $likedIds + $remaining_liked_ids;
	$aggregateInfoOfMatches['availabile_set_second_key'] = $availabile_set + $remaining_availabile_set;
	$aggregateInfoOfMatches['final_score_set_second_key'] = $final_score_set + $remaining_fresh_set_scores + $remaining_liked_set_scores;
	$aggregateInfoOfMatches['store_second_key'] = $store_second_key;

	return $aggregateInfoOfMatches;
    }

    public function generateMatchesUsingMemoryTable($user_id, $actionTaken = 0, $logString = null, $sessionId = null) {
	    $aggregateInfoOfMatches = $this->generateMatchesUsingMemoryTableClassSpecific($user_id,$actionTaken,$logString,$sessionId);

	    $final_set = $aggregateInfoOfMatches['final_set'];
	    $likedIds = $aggregateInfoOfMatches['liked_ids'];
	    $myGender = $aggregateInfoOfMatches['myGender'];
	    $availabile_set = $aggregateInfoOfMatches['availabile_set'];
	    $actionTaken = $aggregateInfoOfMatches['actionTaken'];

	    $final_set_second_key = $aggregateInfoOfMatches['final_set_second_key'];
	    $availabile_set_second_key = $aggregateInfoOfMatches['availabile_set_second_key'];
	    $score_set_second_key = $aggregateInfoOfMatches['final_score_set_second_key'];
	    $liked_ids_second_key = $aggregateInfoOfMatches['liked_ids_second_key'];

        //add to redis
	    $this->addMatchesToSet($final_set, $likedIds, $myGender, $availabile_set, $aggregateInfoOfMatches['final_score_set']);
	    if (Utils::$mutual_friends_enabled && isset($aggregateInfoOfMatches['store_second_key']) && $aggregateInfoOfMatches['store_second_key'])
	    	$this->checkAndAddMatchesToSetWithScores($final_set_second_key, $liked_ids_second_key, $myGender, $availabile_set_second_key, $score_set_second_key);
	    $this->setExpiryForRedisKeys($actionTaken);

	//Do Logging
	    $final_score_set = $aggregateInfoOfMatches['final_score_set'];
	    $result_like_set = $aggregateInfoOfMatches['result_like_set'];
	    $likeQuery = $aggregateInfoOfMatches['likeQuery'];
	    $result_fresh_set = $aggregateInfoOfMatches['result_fresh_set'];
	    $freshProfileQuery = $aggregateInfoOfMatches['freshProfileQuery'];
	    $countToFetchFresh = $aggregateInfoOfMatches['countToFetchFresh'];
	    $logString = $aggregateInfoOfMatches['logString'];
	    $this->doLogging($final_score_set, $result_like_set, $likeQuery, $result_fresh_set, $freshProfileQuery, $countToFetchFresh, $logString);
            
            return($aggregateInfoOfMatches);
    }
}

?>
