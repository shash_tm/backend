<?php
require_once dirname(__FILE__) . "/../../DBO/userUtilsDBO.php";

class TwoWayMatching {
    public $uuDBO;
    
    function __construct() {
        $this->uuDBO = new userUtilsDBO();
    }
    
    /*
     * $filters is an array that consists of the following possibilities for activation :
     * array("male_age","female_age","male_height","female_height","male_bucket","female_bucket")
     */
    public function getTwoMatchingSQLString($user_id, $filters,$marker_string = "us.") {
        
        $prefData           = $this->uuDBO->getUserPreferenceData(array($user_id), true);
        $myData             = $this->uuDBO->getUserData(array($user_id), true);
        $myBasicInfo        = $this->uuDBO->getUserBasicInfo(array($user_id), true);
        $myPIData           = $this->uuDBO->getUserPiInfo(array($user_id), true);
        $myGender           = $myBasicInfo['gender'];
        $oppositeGender     = ($myGender == 'M') ? 'F' : 'M';

        $myAge              = $myData['age'];
        $prefStartAge       = $prefData['start_age'];
        $prefEndAge         = $prefData['end_age'];
        
        $myPi               = $myPIData['actual_pi'];
        $myBucket           = $myPIData['bucket'];
        $IsFreshBucket      = ($myPi == -1 || $myPi == null || $myBucket == 0) ? true : false;
        
        $myHeight           = $myData ['height'];
        $prefStartHeight    = $prefData ['start_height'];
        $prefEndHeight      = $prefData ['end_height'];
        $baseQuery          = $marker_string."gender = '$oppositeGender'";
        
        # Two way matching for age
        if((in_array("male_age",$filters) && $myGender == 'M') || (in_array("female_age",$filters) && $myGender == 'F')) {
            $baseQuery .= " and ".$marker_string."age>='$prefStartAge' and ".$marker_string."age<='$prefEndAge'";
            if (!(isset($myAge) == false || (is_string($myAge) && strlen($myAge) == 0))) {
                $baseQuery .= " and ".$marker_string."pref_start_age<='$myAge' and ".$marker_string."pref_end_age>='$myAge'";
            }
        }
        
        #TWO way matching for height
        if((in_array("male_height",$filters) && $myGender == 'M') || (in_array("female_height",$filters) && $myGender == 'F')) {
            $baseQuery .= " and ".$marker_string."height>='$prefStartHeight' and ".$marker_string."height<='$prefEndHeight' ";
            if (!(isset($myHeight) == false || (is_string($myHeight) && strlen($myHeight) == 0))) {
                $baseQuery .= " and ".$marker_string."pref_start_height<='$myHeight' and ".$marker_string."pref_end_height>='$myHeight'";
            }
        }
        
        #TWO way matching for buckets for males
        if((in_array("male_bucket",$filters) && $myGender == 'M')) {
            if ($myBucket == 1) {
             $baseQuery .= "and ".$marker_string."bucket = 1";
            }
            else if ($myBucket == 2) {
             $baseQuery .= "and ".$marker_string."bucket >= 1 and ".$marker_string."bucket <=2";
            }
            else if ($myBucket == 3) {
             $baseQuery .= "and ".$marker_string."bucket >= 1 and ".$marker_string."bucket <=3";
            }
            else if ($myBucket == 4) {
             $baseQuery .= "and ".$marker_string."bucket not in (1,0)";
            }
            else if ($myBucket == 5) {
             $baseQuery .= "and ".$marker_string."bucket not in (1,2)";
            } 
        }
        
        #TWO way matching for buckets for females
        if((in_array("female_bucket",$filters) && $myGender == 'F')) {
            if ($IsFreshBucket) {
                $baseQuery .= "and ".$marker_string."bucket in (0,5)";
            }
            else if($myBucket == 1) {
             $baseQuery .= "and ".$marker_string."bucket not in (4,5)";
            }
            else if ($myBucket == 2) {
             $baseQuery .= "and ".$marker_string."bucket != 1 ";
            }
            else if ($myBucket == 3) {
             $baseQuery .= "and ".$marker_string."bucket not in (1,2)";
            }
            else if ($myBucket == 4) {
             $baseQuery .= "and ".$marker_string."bucket not in (1,2,3)";
            }
            else if ($myBucket == 5) {
             $baseQuery .= "and ".$marker_string."bucket not in (1,2,3)";
            } 
        }
        return $baseQuery;
    }
}
