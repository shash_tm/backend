<?php
require_once dirname(__FILE__) . "/../../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../../Advertising/ProfileCampaign.php";
require_once dirname(__FILE__) . "/../../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";
require_once dirname(__FILE__) . "/../../SelectionStrategy/RepeatLikeStrategy.php";
require_once dirname(__FILE__) . "/TwoWayMatching.php";
require_once dirname(__FILE__) . "/../../include/config.php";
require_once dirname(__FILE__) . "/../../include/Utils.php";
require_once dirname(__FILE__) . "/../BaseRecommendationEngine.php";
require_once dirname(__FILE__) . "/../../user/UserFlags.class.php";


/*
 * The class represents a common functionality to add/subtract profiles/ad xampaigns over the existing recommendations.
 * The same will have to propogated all across to the matches page.
 */
define('MAX_SELECT_PROFILES',20);
define('MIN_SKIP_NUMBER',4);
define('ANDROID_APP_VERSION_CODE_SUPPORTING_SELECT',614);
define('IOS_APP_VERSION_CODE_SUPPORTING_SELECT',501);
define('SELECT_PROMO_PROFILES_TO_MALES',1);
define('SPARK_REPEAT_ALTERNATE_NUDGE_FREQUENCY',3);  // In sessions

class ExtraneousProfileHandler {
    public $user_id;
    public $gender;
    public $uuDBO;
    protected $redis;
    
    function __construct($user_id,$gender) {
        global $redis;
        $this->uuDBO = new userUtilsDBO();
        $this->user_id = $user_id;
        $this->gender = $gender;
        $this->redis = $redis;
    }
    /*
     * The function handles the following at the moment :
     * 1.Removes MISS TM ID from recommendations
     * 2.Sprinkles Profile Ad campaigns at appropriate locations
     * 3.Sprinkles TM Select Profiles to Non-Select Members
     * array_filter($a,function ($x){return(($x == 'select'));}
     * 
     */
    public function handleExtraneousProfiles($final_set, $myGender, $myAge, $myState, $myCity, $sessionId, $params = array(), $configParams = array()) {
        $final_set = $this->handleUnwantedProfiles($final_set);
        $profilesAdCampaigns    = array();
        $repeat_likes           = array();
        $selectProfiles         = array();
        if(in_array('profileAdCampaign',$params)) {
            $profilesAdCampaigns = $this->getProfileCampaigns($myGender, $myAge, $myState, $myCity);
        }
        if(in_array('repeat',$params) && (($sessionId-1)%SPARK_REPEAT_ALTERNATE_NUDGE_FREQUENCY != 0)) {
            $repeat_likes = $this->getRepeatProfiles($myGender, $myAge, $myState, $myCity, $configParams);
        }
        if(in_array('select',$params)) {
            $selectProfiles = $this->getSelectProfilesForNonSelectMember();
            $maxProfiles = ($myGender == 'F') ? floor(sizeof($final_set)/MIN_SKIP_NUMBER)-1 : SELECT_PROMO_PROFILES_TO_MALES;
            $selectProfiles = array_slice($selectProfiles,0,min($maxProfiles > 0 ? $maxProfiles : 0, MAX_SELECT_PROFILES));
        }
        
        #Make decisions here. What and where to put in the recommendation feed depending on the size !
        #$final_set = $this->recommendationFeedInserter($final_set,array("profilesAdCampaigns" => $profilesAdCampaigns,"repeat_likes" => $repeat_likes,"selectProfiles" => $selectProfiles));
        
        return array("final_set" => $final_set, "repeat_likes" => $repeat_likes, "selectSprinkledProfiles" => $selectProfiles, "profilesAdCampaigns" => $profilesAdCampaigns);
    }
    
    /*
     * The function is a hook to remove the MissTM from the list of recommendations.
     */
    public function handleUnwantedProfiles($final_set) {
        global $miss_tm_id;
        $result = $final_set;
        if (isset($miss_tm_id)) {
            #Dummy Spark Id to showcase guys how spark features pans out
            $dummySparkId = 1615723;
            $result = array_diff($final_set, array($miss_tm_id,$dummySparkId,$this->user_id));
            $result = array_diff($result, ProfileCampaign::getAllCampaignIds());
        }
        return $result;
    }

    /*
     * The function inserts the active profile campaigns are 'appropriate' positions 
     * in the generated set of recommendations.
     */
    public function handleProfileCampaigning($final_set, $myGender, $myAge, $myState, $myCity) {
        $result = $final_set;
        if (ProfileCampaign::isProfileCampaigningActive() && sizeof($final_set) > 0) {
            $result = ProfileCampaign::mergeRecommendationResultsWithActiveCampaigns($final_set, $this->user_id, $myGender, $myAge, $myState, $myCity);
        }
        return $result;
    }
    
     public function getProfileCampaigns($myGender, $myAge, $myState, $myCity) {
        $result = array();
        if (ProfileCampaign::isProfileCampaigningActive()) {
            $result = ProfileCampaign::getRelevantProfileAdCampaigns($this->user_id, $myGender, $myAge, $myState, $myCity);
        }
        return $result;
     }
    
    public function getRepeatProfiles($myGender, $myAge, $myState, $myCity, $configParams) {
        $previous_likes=array();
        if (array_key_exists ( 'repeat_like', $configParams )) {
            $repeat_profile_params=$configParams['repeat_like'];
            $RepeatLikeStrategy = new RepeatLikeStrategy ($this->user_id,$repeat_profile_params,$myGender);
            $can_run_repeat=$RepeatLikeStrategy->canRun ();
            if ($can_run_repeat) {
                $previous_likes = $this->handleRepeatProfiles($repeat_profile_params);
            }
	}
        return($previous_likes);
    }
    
    public function handleRepeatProfiles($params){

    	$sparked_users=  BaseRecommendationEngine::getUsersAlreadyEngagedViaSpark($this->user_id);
    	
    	$liked_start_date=(array_key_exists("liked_start_date",$params)&&intval($params['liked_start_date'])>0)?intval($params['liked_start_date']):14;
    	$liked_end_date=(array_key_exists("liked_end_date",$params)&&intval($params['liked_end_date'])>0)?intval($params['liked_end_date']):7;

    	$start_like_date=date('Y-m-d', strtotime("-$liked_start_date days")); //inclusive
    	$end_like_date=date('Y-m-d', strtotime("-$liked_end_date days"));	//exclusive
    	
    	$last_login_start_date_config=(array_key_exists("last_login_start_date",$params)&&intval($params['last_login_start_date'])>0)?intval($params['last_login_start_date']):7;
    	$last_login_end_date_config=(array_key_exists("last_login_end_date",$params)&&intval($params['last_login_end_date'])>0)?intval($params['last_login_end_date']):0;
    	 
    	$last_login_start_date=date('Y-m-d', strtotime("-$last_login_start_date_config days")); //inclusive
    	$last_login_end_date=date('Y-m-d', strtotime("-$last_login_end_date_config days")); //exclusive
    	   
    	$output_number=(array_key_exists("output_number",$params)&&intval($params['output_number'])>0)?intval($params['output_number']):5;
    	 
    	$output_number=floor($output_number*1.5);
    	
    	$sql = "select t.user, t.flag from (
                    select user1 as user, 0 as flag from user_hide where user2=?
                    union select user2 as user, 2 as flag from user_like where user1=? and timestamp>=? and timestamp<?
                    union select user1 as user, 1 as flag from user_like where user2=?
                    union select user2 as user, 0 as flag from user_repeat_profile where user1=?)t join user_search u on t.user=u.user_id
                    join user_lastlogin ull on ull.user_id=t.user and ull.last_login>=? and ull.last_login<? where u.isSelect = 0 and u.haspics = 1";
    	
    	
    	$output = array();
    	$output_likedMe = array();
    	$output_iLiked = array();
    	 
    	$data = Query::SELECT($sql, array($this->user_id, $this->user_id, $start_like_date, $end_like_date, 
    			$this->user_id,$this->user_id,$last_login_start_date,$last_login_end_date));
    	
    	$output_hidden=array();
    	$output_likedMe=array();
    	$output_iLiked=array();
    	foreach ($data as $val) {
    		if ($val['flag'] == 1)
    			$output_likedMe[] = $val['user'];
    		if ($val['flag'] == 2)
    			$output_iLiked[] = $val['user'];
    		if ($val['flag'] == 0)
    			$output_hidden[] = $val ['user'];
    	}
    	
    	if($sparked_users['output_sparkedMe']==null){
    		$sparked_users['output_sparkedMe']=array();
    	}
    	if($sparked_users['output_sparkedByMe']==null){
    		$sparked_users['output_sparkedByMe']=array();
    	}
    	
    	$profile_campaigns=ProfileCampaign::getAllCampaignIds();
    	if($profile_campaigns==null){
    		$profile_campaigns=array();
    	}
    	
    	$output=array_slice(array_diff($output_iLiked,$output_likedMe,$output_hidden,$sparked_users['output_sparkedMe'],
    			$sparked_users['output_sparkedByMe'],$profile_campaigns),0,$output_number);
    	$redis_output=$output;

        $repeatProfileKey = Utils::$redis_keys['matches_repeat_profile']."$this->user_id"; //key for repeat profiles
    	array_unshift($redis_output,$repeatProfileKey);
    	//add this to cache as well so that the final output can be fetched
    	call_user_func_array(array($this->redis, "sAdd"), $redis_output);
        $timeForMatchesExpiry = ($this->gender == "F") ? Utils::$femaleMatchesExpiryTime : Utils::$maleMatchesExpiryTime;    //setting the match refresh time
    	$this->redis->setTimeout($repeatProfileKey, $timeForMatchesExpiry);
    	 
    	return $output;
    }
    
    public function getSelectProfilesForNonSelectMember(){
        #Identify the profiles which should be shown to the non-select profiles using the TwoWayFilters
        
        # Making the app backward compatible here. We don't want select members to be pushed to non-select with old app.
        $headerParams = Utils::getAllSystemHeaderFields();
        $userFlags = new userFlags($this->user_id,false);
        if(($headerParams['source'] == 'androidApp' && $headerParams['app_version_code'] <= ANDROID_APP_VERSION_CODE_SUPPORTING_SELECT) || 
                ($headerParams['source'] == 'iOSApp' && $headerParams['app_version_code'] <= IOS_APP_VERSION_CODE_SUPPORTING_SELECT) ||
                !$userFlags->getSelectFlags(true)) {
            return array();
        }
        
        
        $filters = array("male_age","female_age","male_height","female_height","male_bucket","female_bucket");
        $matchingObj = new TwoWayMatching();
        $twoWayMatchSql = $matchingObj->getTwoMatchingSQLString($this->user_id, $filters,"uss.");
        $limitString = (MAX_SELECT_PROFILES > 0)?" limit ".MAX_SELECT_PROFILES:"";
        $selectBucketToExposeToNonSelect = ($this->gender == 'F') ? "4,5":"4,5";
        $sql = "select uss.user_id as user_id from user_search uss join user_subscription sub on uss.user_id=sub.user_id and (sub.status='active' or sub.status='trial') and sub.expiry_date>now() and sub.metadata is not null left join user_flags uf on uss.user_id = uf.user_id "
                . "where ".$twoWayMatchSql." and uss.user_id not in ("
                . "select user2 as user_id from user_like  where user1 = ? union "
                . "select user1 as user_id from user_like  where user2 = ? union "
                . "select user2 as user_id from user_hide  where user1 = ? union "
                . "select user1 as user_id from user_hide  where user2 = ? union "
                . "select user2 as user_id from user_hide_ignored  where user1 = ? union "
                . "select user2 as user_id from user_spark where user1 = ? union "
                . "select user1 as user_id from user_spark where user2 = ?) and uss.isSelect = 1 and uss.haspics = 1 and uss.bucket in (.$selectBucketToExposeToNonSelect.) and (uf.hide_myself is null or uf.hide_myself = 0) order by uss.user_id%SECOND(now()) ".$limitString;
        $u = $this->user_id;
        $rows = Query_Wrapper::SELECT($sql, array($u,$u,$u,$u,$u,$u,$u), Query::$__slave);
        $result = array();
        foreach($rows as $row) {
            $result[] = $row['user_id'];
        }
        return $result;
    }
    
    public static function recommendationFeedInserter($final_set,$insertions) {
        $profilesAdCampaigns    = $insertions["profilesAdCampaigns"];
        $repeat_likes           = $insertions["repeat_likes"];
        $selectProfiles         = $insertions["selectProfiles"];
        $addOns                 = array_merge($profilesAdCampaigns,$repeat_likes,$selectProfiles);
        shuffle($addOns);
        
        $result = $final_set;
        $recommendationCount = sizeof($result);
        $addOnsCount = sizeof($addOns);
        if($addOnsCount > 0) {
            $ratio = ceil($recommendationCount/$addOnsCount);
            $insertionGap  = ($ratio < MIN_SKIP_NUMBER)?(($recommendationCount < MIN_SKIP_NUMBER)?ceil($recommendationCount/2):$ratio):MIN_SKIP_NUMBER;
            if($ratio >= 0) {
                for($i=0; $i < $addOnsCount && $insertionGap*($i+1)+$i < sizeof($result); $i++) {
                    ArrayUtils::insertValueAtPos($result,$insertionGap*($i+1)+$i,$addOns[$i]);
                }
            }
        }
        return $result;
    }
}
