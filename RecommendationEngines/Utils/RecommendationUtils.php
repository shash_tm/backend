<?php
require_once dirname(__FILE__) . "/../../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";
require_once dirname(__FILE__) . "/../../include/config.php";
require_once dirname(__FILE__) . "/../../include/Utils.php";

class RecommendationUtils {
    public static function getSetUserCurrentSession($user_id,$batchCount) {
        $sessionId = "1"; # Get from the table
        // Sessions should be updated only for 1st serving of female cache or for backward compatibility (-1).
        if($batchCount <= 1) {
            #Session Identificaton
            $session_sql = "select current_session_id,action_taken from user_current_session_table where user_id = ?";
            $sessionInfo = Query::SELECT($session_sql, array($user_id), Query::$__slave, true);
                if(sizeof($sessionInfo) > 0) {
                    $sessionId = $sessionInfo['current_session_id'];
                    $actionTaken = $sessionInfo['action_taken'];
                    if($actionTaken) {
                        $actionTaken = 0;
                        ++$sessionId;
                    }
                    # Update the session with incremented information
                    $table = "user_current_session_table" ;
                    $session_insert_sql = "UPDATE user_current_session_table set current_session_id = ? ,action_taken = ?, tStamp = now()  where user_id = ?";
                    Query::UPDATE($session_insert_sql, array($sessionId ,$actionTaken, $user_id), $table,Query::$__master);
                }
                else {
                    // INSERT QUERY
                    $table = "user_current_session_table" ;
                    $session_insert_sql = "INSERT INTO user_current_session_table (user_id, current_session_id,action_taken,tStamp) values (?,?,?,now())";
                    Query::INSERT($session_insert_sql, array($user_id,1,0),$table, Query::$__master, false);
                }
        }
        return $sessionId;
    }
    
    public static function getUserCurrentSession($user_id,$batchCount=1) {
        $sessionId = "-1"; # Get from the table
        if($batchCount <= 1) {
            $session_sql = "select current_session_id from user_current_session_table where user_id = ?";
            $sessionInfo = Query::SELECT($session_sql, array($user_id), Query::$__slave, true);
                if(sizeof($sessionInfo) > 0) {
                    return $sessionInfo['current_session_id']; 
                }
        }
        return $sessionId;
    }
}

