<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
	$degree_arr = array("1"=>1,"2"=>18,"3"=>3,"4"=>4,"5"=>17,"6"=>6,"7"=>5,"8"=>15,"9"=>15,"10"=>12,"11"=>19,"12"=>38,"13"=>23,"14"=>24,"15"=>37,"16"=>26,"17"=>25,"18"=>35,"19"=>35,"20"=>32,"21"=>39,"22"=>21,"23"=>20,"24"=>40,"25"=>40,"26"=>42,"27"=>41);
	$income_map = array("0"=>"0-10","1"=>"10-20","2"=>"20-50","3"=>"50-51");
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute("select ud.user_id,ud.start_age as start_age,ud.end_age as end_age,ud.marital_status as marital_status,
				ud.religionCaste as religionCaste,ud.location as location,ut.start_height as start_height,
				ut.end_height as end_height,ut.smoking_status as smoking_status,
				ut.drinking_status as drinking_status,ut.food_status as food_status,
				ut.smoking_status_search as smoking_status_search,
				ut.drinking_status_search as drinking_status_search,ut.food_status_search as food_status_search,
				uw.income_start as income_start,uw.income_end as income_end,uw.education as education
				from user_preference_demography ud LEFT JOIN user_preference_trait ut on ud.user_id=ut.user_id
				LEFT JOIN user_preference_work uw on ud.user_id=uw.user_id");
	$data = $sql->GetRows();
	
	foreach($data as $key=>$value){
		//echo $data[$key]['religionCaste'];
		if($data[$key]['religionCaste']!=NULL){
			$religion = explode(';',$data[$key]['religionCaste']);
			for($i=0;$i<count($religion);$i++){
				if($religion[$i]==1){
					$religion[$i] = 'Hindu';
				}else if($religion[$i]==2){
					$religion[$i] = 'Muslim';
				}else if($religion[$i]==3){
					$religion[$i] = 'Sikh';
				}else if($religion[$i]==4){
					$religion[$i] = 'Jain';
				}else if($religion[$i]==5){
					$religion[$i] = 'Christian';
				}else if($religion[$i]==6){
					$religion[$i] = 'Other';
				}else if($religion[$i]==9){
					$religion[$i] = 'Buddhist';
				}
			}
			$data[$key]['religionCaste'] = implode(';',$religion);
		}
		if($data[$key]['smoking_status']!='Never'){
				$data[$key]['smoking_status'] = NULL;
		}
		
		if($data[$key]['drinking_status']!='Never'){
				$data[$key]['drinking_status'] = NULL;
		}
		
		if($data[$key]['food_status']!='Vegetarian'){
				$data[$key]['food_status'] = NULL;
		}
		
		if($data[$key]['education']!=NULL){
			$education = explode(',',$data[$key]['education']);
			for($i=0;$i<count($education);$i++){
				$education[$i] = $degree_arr[$education[$i]];
			}
			asort($education);
			$data[$key]['education'] = implode(',',$education);
		}
		
		if($data[$key]['income_start']!=NULL && $data[$key]['income_end']!=NULL){
			
			if($data[$key]['income_start']<10 && $data[$key]['income_end']<=10){
				$income = array("0"=>"0-10");
			}else if($data[$key]['income_start']<10 && $data[$key]['income_end']<=20){
				$income = array("0"=>"0-10","1"=>"10-20");
			}else if($data[$key]['income_start']<10 && $data[$key]['income_end']<=50){
				$income = array("0"=>"0-10","1"=>"10-20","2"=>"20-50");
			}else if($data[$key]['income_start']<10 && $data[$key]['income_end']>50){
				$income = array("0"=>"0-10","1"=>"10-20","2"=>"20-50","3"=>"50-51");
			}else if($data[$key]['income_start']==10 && $data[$key]['income_end']==10){
				$income = array("0"=>"0-10","1"=>"10-20");
			}else if($data[$key]['income_start']==20 && $data[$key]['income_end']==20){
				$income = array("0"=>"10-20","1"=>"20-50");
			}else if($data[$key]['income_start']==10 && $data[$key]['income_end']==20){
				$income = array("0"=>"10-20");
			}else if($data[$key]['income_start']==10 && $data[$key]['income_end']<=50){
				$income = array("0"=>"10-20","1"=>"20-50");
			}else if($data[$key]['income_start']==20 && $data[$key]['income_end']<=50){
				$income = array("0"=>"20-50");
			}else if($data[$key]['income_start']==10 && $data[$key]['income_end']>50){
				$income = array("0"=>"10-20","1"=>"20-50","2"=>"50-51");
			}else if($data[$key]['income_start']==20 && $data[$key]['income_end']>50){
				$income = array("0"=>"20-50","1"=>"50-51");
			}else if($data[$key]['income_start']>=50 && $data[$key]['income_end']>50){
				$income = array("0"=>"50-51");
			}
			$data[$key]['income'] = json_encode($income);
		}else if($data[$key]['income_start']!=NULL && $data[$key]['income_end']==NULL){
			if($data[$key]['income_start']<10){
				$income = array("0"=>"0-10","1"=>"10-20","2"=>"20-50","3"=>"50-51");
			}else if($data[$key]['income_start']==10){
				$income = array("0"=>"10-20","1"=>"20-50","2"=>"50-51");
			}else if($data[$key]['income_start']==20){
				$income = array("0"=>"20-50","1"=>"50-51");
			}else if($data[$key]['income_start']>=50){
				$income = array("0"=>"50-51");
			}
			$data[$key]['income'] = json_encode($income);
		}else
			$data[$key]['income'] = null;
		
		if($data[$key]['location']==""){
			$data[$key]['location'] = null;
		}
		
		$conn->Execute($conn->prepare("insert into user_preference_data(user_id,start_age,end_age,start_height,end_height,
				marital_status,religionCaste,location,smoking_status,drinking_status,food_status,smoking_status_search,drinking_status_search,
				food_status_search,income,income_start,income_end,education,tstamp)
				values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW()) on Duplicate KEY UPDATE start_age=?,end_age=?,start_height=?,end_height=?,
				marital_status=?,religionCaste=?,location=?,smoking_status=?,drinking_status=?,food_status=?,smoking_status_search=?,drinking_status_search=?,
				food_status_search=?,income=?,income_start=?,income_end=?,education=?,tstamp=NOW()"),
				array($data[$key]['user_id'],$data[$key]['start_age'],$data[$key]['end_age'],$data[$key]['start_height'],$data[$key]['end_height'],
						$data[$key]['marital_status'],$data[$key]['religionCaste'],$data[$key]['location'],$data[$key]['smoking_status'],
						$data[$key]['drinking_status'],$data[$key]['food_status'],$data[$key]['smoking_status'],
						$data[$key]['drinking_status'],$data[$key]['food_status'],$data[$key]['income'],
						$data[$key]['income_start'],$data[$key]['income_end'],$data[$key]['education'],
						$data[$key]['start_age'],$data[$key]['end_age'],$data[$key]['start_height'],$data[$key]['end_height'],
						$data[$key]['marital_status'],$data[$key]['religionCaste'],$data[$key]['location'],$data[$key]['smoking_status'],
						$data[$key]['drinking_status'],$data[$key]['food_status'],$data[$key]['smoking_status'],
						$data[$key]['drinking_status'],$data[$key]['food_status'],$data[$key]['income'],
						$data[$key]['income_start'],$data[$key]['income_end'],$data[$key]['education']));
	}
	
	echo "Done";
//	var_dump($data);exit;
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}

?>