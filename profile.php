<?php

require_once dirname(__FILE__).'/include/config.php';
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
include dirname ( __FILE__ ) . "/include/allValues.php";
require_once dirname(__FILE__)."/EventTracking.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/include/systemMessages.php";
require_once dirname ( __FILE__ ) . "/include/header.php";




/**
 * Shows the complete information of a profile
 * @author himanshu
 */

class Profile {

	private $user_id;
	private $profile_id;
	private $base_url;
	private $user_utils;

	function __construct($user_id, $profile_id){
		//parent::__construct();
		global  $baseurl;
		$this->profile_id = $profile_id;
		$this->user_id = $user_id;
		$this->base_url = $baseurl;
		$this->user_utils = new UserUtils();
	}

	public function getAttributes($profileId, $from_match = false, $isAuthentic = false, $device, $mutualLikeFlag = false, $likeId, $setFlag=null, $availableId){
		$extra_params = array('fetch_mutual_friends_data' => true);
		if($this->user_id == $profileId){
			$myProfile = true;
		}else {
			$extra_params['from_other_profile']=true;
		}
		$attributes =  $this->user_utils->getAttribute('profile', $this->user_id, array($profileId), $from_match, $isAuthentic, $myProfile, $device, $mutualLikeFlag,$likeId, $setFlag,$availableId, -1, $extra_params);
		return $attributes;
	}

	public function showBasicProfile(&$attributes){
		$first_name_count=strlen($attributes['name']['fname']);
		$attributes['name']['fname']  = ucfirst(substr($attributes['name']['fname'] , 0 ,1)) .  str_repeat("x",$first_name_count-1 );;
		$attributes['mutual_connections'] = null;
	}
	
}

try{

	$user = functionClass::getUserDetailsFromSession();
	$user_id = $user['user_id'];
	$systemMsg = null;
	$device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	
	if(isset($user_id)){

		$ua = new UserActions();
		
	$ud = new UserData($user_id);
	$uu = new UserUtils();
	$status = $ud->fetchStatus();
	$gender = $ud->fetchGender();
	$authenticity = $ud->fetchAuthenticity();
	//var_dump($authenticity);
	$isAuthentic = $authenticity['TM_authentic'];

	if(!$isAuthentic){
		$systemMsg['like_hide_action_msg'] = systemMsgs::$profile['non-authentic'];
	}
	//else{
	$systemMsg['not_mutual_like_msg'] = systemMsgs::$profile['not_mutual_like'];
	//}
	//var_dump($isAuthentic);


	$mid = explode('_', $_REQUEST['pid']);
	$profile_id = $mid[1];
	$event_id = isset($mid[2])?$mid[2].'_'.$mid[3]:null;

	/*if(isset($_REQUEST['uid'])){
		$eventTrack = new EventTracking($_REQUEST['uid']);
		$eventTrack->logAction("profile");
		}
		else if(isset($user_id)){
		$eventTrack = new EventTracking($user_id);
		$eventTrack->logAction("profile", null, null, null, $profile_id);
		}*/

	$from_match = $_REQUEST['from_match'];
	$from_like = $_REQUEST['from_like'];
	$from_maybe = $_REQUEST['from_maybe'];
	$from_mutual_like = $_REQUEST['from_mutual_like'];
	$from_msg = $_REQUEST['from_msg'];
	$fromSpark = $_REQUEST['from_spark'];
	$from_scene = $_REQUEST['from_scenes'];


	//var_dump($from_like); 
	
	/*if(!isset($_REQUEST['from_match'])){
		header("Location: $baseurl./match.php");
		}*/
	//var_dump($from_match);
	$matchFlag = null;
	$likeId = null;
	$setFlag = null;
	if($_REQUEST['me'] != true){
		if($from_match == "true"){
			
			$processedMatchSet = $uu->processMatchSet( $redis->LRANGE(Utils::$redis_keys['matches'].$user_id, 0 , -1));
			
			//elseif($redis->SISMEMBER(Utils::$redis_keys['matches'].$user_id, $profile_id."l") == 1){
			if(in_array( $profile_id, $processedMatchSet['like_ids'])){	
				$matchFlag = true; 
				$likeId = array($profile_id);
			}
			else if(in_array($profile_id, $processedMatchSet['all_ids']))
			//if($redis->SISMEMBER(Utils::$redis_keys['matches'].$user_id, $profile_id) == 1)
			$matchFlag = true;
			$flag = $ua->canCommunicate($user_id, $profile_id);
			if($flag == true){
				$matchFlag = false;
				unset($systemMsg['not_mutual_like']);
				header("Location:profile.php?pid=".$_REQUEST['pid']);
			}
		}
		
		if($matchFlag == true) $setFlag = "matches"; 
		
		if($from_like == true){
			$isLiked = $ua->isActionDone($user_id, $profile_id, "like");
			//var_dump($isLiked);//die;
			if($isLiked == false) header("Location: matches.php");
			else $setFlag ="like";
			//var_dump($setFlag); die;
		}
	
		if($from_maybe == true){
			$ismaybe = $ua->isActionDone($user_id, $profile_id, "maybe");
			if($ismaybe == false) header("Location: matches.php");
			else $setFlag ="maybe";
		}
	
		if($setFlag == null){
			$flag = $ua->canCommunicate($user_id, $profile_id);
			$reqArray =explode('_', $_REQUEST['pid']);
			$fromEvent = isset($reqArray[2]);
			if($flag == true){
				$matchFlag = false;
				$setFlag ="mutual_like";
				unset($systemMsg['not_mutual_like']);
			}else if($fromEvent){
				$matchFlag = false;
				$profileViewedFromEventFlag = true;
			}else if(SparkDBO::areUsersSparked($user_id, $profile_id, null)){
				$matchFlag = false;
				$profileViewedFromSparkFlag = true;
			}
			else{
				header("Location: matches.php");
			}
		}
	}
	//echo "here";

	functionClass::redirect ( 'profile' );

	if($_REQUEST['me'] == true) {
		$profile_id = $user_id;
		$deactivation_msgs = systemMsgs::$suspendMessages;
		$systemMsg["deactivation_msgs"] = $deactivation_msgs;
		$systemMsg["popularity_tip"] = systemMsgs::$profile['popularity_tip'];
	}

	/*echo $user_id;
	 echo $profile_id;*/
	//var_dump($profile_id);
	$checkSumStr = !isset($event_id) ? $user_id .'|' . $profile_id : $user_id .'|' . $profile_id .'|' .$event_id;
	$postCheckSum = Utils::calculateCheckSum($checkSumStr);
	/*echo $mid[0]. '	';
	 echo $postCheckSum;*/

	if(($mid[0] == $postCheckSum)||$_REQUEST['me'] == true){
		$profile = new Profile($user_id, $profile_id);
		$attributes = $profile->getAttributes($profile_id, $from_match, $isAuthentic, $login_mobile, $flag, $likeId, $setFlag, $processedMatchSet['available_ids']);
		if($attributes[$profile_id] == null){
			header("Location: matches.php");
		}
		/*	echo '<pre>';
		 var_dump($attributes); die;*/
		//$smarty->assign("myGender", $gender);

		if($device_type == 'mobile'){
			$output=array("responseCode"=>200);
			if(isset($from_scene) && ($from_scene == 'true' || $from_scene == true)){
				$output['sparkCountersLeft'] = (int)sparkDBO::getUserActiveSparkCount($user_id);
			}
			/*foreach ($attributes as $key ){
				if($key == "my_data")continue;
				$ids[] = $key;
				}
				echo '<pre>';
				var_dump($ids);*/
			$ids[0] = $profile_id;
			$uu->formatMobileData($attributes, $ids, $output, $user_id, true, $from_match);
			
			
			//var_dump($output);
			
			
			if(isset($output["data"][0]["profile_data"]["share_profile"]))
				unset($output["data"][0]["profile_data"]["share_profile"]);
			
			//var_dump($output["data"][0]["profile_data"]['share_profile']);	
				
			if(isset($systemMsg) && !empty($systemMsg)) 
			$output["system_messages"] = $systemMsg;
			//var_dump($output);
			//var_dump($_REQUEST['hash']);
			//for app caching, using md5 hash
			$resource=$output;			
			unset($resource['responseCode']);
			$resource=json_encode($resource);
			$hashCheck=functionClass::checkHash($resource,$_REQUEST['hash']);
			
			//var_dump($hashCheck);
			
			if($hashCheck['status']==false)
			    $output['hash']=$hashCheck['hash'];
			else
				$output=array("responseCode"=>304);
			 
			$output["tstamp"]=time();	
			echo json_encode($output);
		}
		else{

			$header = new header($user_id);
			$header_values = $header->getHeaderValues();
			$smarty->assign("match_id", $profile_id);
			$smarty->assign("header", $header_values);
			$smarty->assign("systemMsgs", $systemMsg);
			$smarty->assign("my_data", $attributes['my_data']);
			$smarty->assign("my_id", $user_id);
			//var_dump($attributes['my_data']);die;
			$smarty->assign("profile_data", $attributes[$profile_id]);
			$smarty->display("templates/profile/profile.tpl");
		}
	}
	else{

		header("Location: matches.php");
	}
	}
	else{
		if($device_type == "mobile")
		functionClass::redirect("profile", true);		
		else
		header("Location: matches.php");
	}
	
}
catch (Exception $e){
	//echo $e->getMessage();
	trigger_error("PHP WEB:".$e->getTraceAsString(), E_USER_WARNING);
}
?>
