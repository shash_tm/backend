<?php

require_once dirname ( __FILE__ ). "/../../include/config.php";
/**
 * [recommendations_ab]
 * engine[] = "RecommendationEngine_ZeroAcThrottle"
 * filter[] = '{"Gender":["F"]}'
 * percent[] = "100"
 * jsonParams[] = '{"slopeForLoe":[3,1], "freshProfileCountForEachBucket": [10,20,20,10,10,10], "like_threshold_female":90}'
 * engine[] = "RecommendationEngine"
 * filter[] = ""           ;Do not add a filter here. Must be necesarrily empty as a fallback mechanism.
 * percent[] = ""
 * jsonParams[] = ""
 * [aesthetics_ab]
 * aesthetic[] = "Aesthetics_Nudge"
 * ;filter[] = '{"Gender":["F","M"],"Age":[22,23,24,25,26,27],"Location":["1268","2171"]}'
 * filter[] = '{"Gender":["F"]}'
 * percent[] = "10"
 * jsonParams[] = '{"1":[10,40,70],"2":[10,40,70],"3":[20,50],"4":[20,50],"5":[20,50]}'
 * aesthetic[] = "Aesthetics_None"
 * filter[] = ""           ;Do not add a filter here. Must be necesarrily empty as a fallback mechanism.
 * percent[] = ""
 * jsonParams[] = ""
 */
class AB_Config {
    
    static public function getRecommendationConfigs() {
        global $config_ab;
        return $config_ab['recommendations_ab'];
    }
    
    static public function getAestheticsConfigs() {
        global $config_ab;
        return $config_ab['aesthetics_ab'];
    }
}
