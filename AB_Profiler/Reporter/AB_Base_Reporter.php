<?php
require_once dirname(__FILE__) . "/../../Utils/ArrayUtils.php";
require_once dirname(__FILE__) . "/../../include/config_admin.php";
require_once dirname(__FILE__) . "/../../include/Utils.php";
require_once dirname(__FILE__) . "/../../Utils/stats/WelchsT_Test.php";


abstract class AB_Base_Reporter {
    
    abstract public function computeUserBehaviourData($duration, $date);
    
    /*
     * The function computes standard summary for a sample set.
     * The standard summary is represented by : array(min,25%,50%,mean,75%,max,count) 
     */
    public function computeUserBehaviourSummary($ab_data) {
        $summaryReport = array();
        foreach ($ab_data as $key => $value) {
            $summaryReport[$key] = array_map('ArrayUtils::standardSummary', $value);
        }
        return $summaryReport;
    }

    /*
     * The function computes uses the Welch's Test to assign numerical probabalities
     * to the performance of each of the treatment group w.r.t the control group. 
     */
    public function computeTTestResults($ab_data) {

        //1.Identify the key
        $keys = array_keys($ab_data);
        $default_arr = preg_grep("*Aesthetics_None*", array_diff($keys, preg_grep("*RecommendationEngine_*", $keys)));
        $default_arr_keys = array_keys($default_arr);
        $default = $default_arr[$default_arr_keys[0]];
        $results = array();
        $ab_keys = array_diff($keys, $default_arr);

        foreach ($ab_keys as $ab_key) {
            $likes_O = $ab_data[$default][0];
            $likes_A = $ab_data[$ab_key][0];

            $hides_O = $ab_data[$default][1];
            $hides_A = $ab_data[$ab_key][1];

            $actions_O = $ab_data[$default][2];
            $actions_A = $ab_data[$ab_key][2];
            if (!empty($likes_O) && !empty($likes_A) &&
                    !empty($hides_O) && !empty($hides_A) &&
                    !empty($actions_O) && !empty($actions_A)) {
                $likePvals = WelchsT_Test::computeStatistics($likes_O, $likes_A);
                $hidePvals = WelchsT_Test::computeStatistics($hides_O, $hides_A);
                $actionPvals = WelchsT_Test::computeStatistics($actions_O, $actions_A);
                $results[$default][$ab_key] = array(round($likePvals["p_value"], 4, PHP_ROUND_HALF_UP),
                    round($hidePvals["p_value"], 4, PHP_ROUND_HALF_UP),
                    round($actionPvals["p_value"], 4, PHP_ROUND_HALF_UP));
            }
        }
        return $results;
    }

    public function sendEmail($duration, $date,$subject = NULL) {
        global $smarty;
        if ($date == "curdate()") {
            $subject = $subject . ": End Date :" . date('d-m-Y', strtotime("-1 days")) . ": Duration :" . $duration . " days";
        } else {
            $subject = $subject . ": End Date :" . $date . ": Duration :" . $duration . " days";
        }
        $to = "purav@trulymadly.com";
        Utils::sendEmail($to, "admintm@trulymadly.com", $subject, $smarty->Fetch(dirname(__FILE__) . '/AB_Reporter.tpl'), TRUE);
    }
}
