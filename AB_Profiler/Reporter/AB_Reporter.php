<?php

require_once dirname(__FILE__) . "/../../Utils/ArrayUtils.php";
require_once dirname(__FILE__) . "/../../include/config_admin.php";
require_once dirname(__FILE__) . "/../../include/Utils.php";
require_once dirname(__FILE__) . "/../../Utils/stats/WelchsT_Test.php";
require_once dirname(__FILE__) . "/AB_Base_Reporter.php";

$duration = 7;
$date = "curdate()";
if ($argc > 1) {
    $duration = $argv[1];
    if ($argc > 2) {
        $date = $argv[2];
    }
}

/**
 * AB_Reporter - Generate the reports to track AB recommendations
 *
 */
class AB_Reporter extends AB_Base_Reporter {
    /*
     * The function calculates the performance of both control and treatment groups.
     * The performance at the moment being measure by:
     *      Like % distribution
     *      Hide % distribution
     *      Total Action % distribution
     */

    public function computeUserBehaviourData($duration, $date) {
        global $conn_reporting;
        //1. Generate the AB report
        $report = array();
        $conn_reporting->Execute("SET SESSION group_concat_max_len = 1000000");
        $query = "SELECT user_id,
                        GROUP_CONCAT(logString SEPARATOR '###') AS logStrings,
                        GROUP_CONCAT(result SEPARATOR '###') AS results,
                        GROUP_CONCAT(tstamp SEPARATOR '###') AS tStamps,
                        GROUP_CONCAT(isLikeQuery) AS isLikeQueries
                FROM user_recommendation_query_log
                WHERE countFetched > 0 and logString is not null and tStamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and tStamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE)
                GROUP BY user_id
                HAVING (COUNT(DISTINCT (logString)) > 1)";

        $rows = $conn_reporting->Execute($query);
        $version_query = "SELECT user_id,COUNT(*) as count from user_gcm_current where ((app_version_code >= 152 and source = 'androidApp') or (app_version_code >= 153 and source = 'iOSApp')) group by user_id";
        $version_count_tab = $conn_reporting->Execute($version_query)->GetRows();
        $version_count = array();
        foreach ($version_count_tab as $current) {
            $version_count[$current['user_id']] = $current['count'];
        }


        while ($row = $rows->FetchRow()) {
            #foreach ($rows as $row) {
            $user_id = json_decode($row['user_id'], true);

            if (key_exists($user_id, $version_count)) {
                $tStamps = array();
                foreach (preg_split('*###*', $row['tStamps']) as $res) {
                    array_push($tStamps, date("Y-m-d H:i:s", strtotime($res)));
                }

                $results = array();
                foreach (preg_split('*###*', $row['results']) as $res) {
                    array_push($results, $res);
                }

                $isLikeQueries = preg_split('*,*', $row['isLikeQueries']);
                $num = sizeof($isLikeQueries);

                $logData = array();
                foreach (preg_split('*###*', $row['logStrings']) as $res) {
                    array_push($logData, json_decode($res, true));
                }

                $a_b = array();
                foreach ($logData as $log) {
                    $a_b = array_merge_recursive($a_b, $log);
                }

                if ((sizeof($a_b['RecommendationAB']) > 1 && $a_b['RecommendationAB']['RecommendationEngine'] != NULL)
                        or ( sizeof($a_b['AestheticsAB']) > 1 && $a_b['AestheticsAB']['Aesthetics_None'] != NULL)) {
                    $previousTimeStamp = NULL;

                    // Will have to find out the max. point where nudge was shown. Since we'll have to use that point as a filter for control group as well to ensure we do not introduce any bias.
                    $maxFirstNudgePoint = 0;
                    for ($i = $num - 1; $i >= 0; $i--) {
                        $log = $logData[$i];
                        if (array_key_exists("AestheticsAB", $log)) {
                            if (array_key_exists("Aesthetics_Nudge", $log["AestheticsAB"])) {
                                $sessionId = $log["AestheticsAB"]["Aesthetics_Nudge"][2];
                                $nudgeParams = $log["AestheticsAB"]["Aesthetics_Nudge"][1];
                                $nudgePoints = $nudgeParams[$sessionId];
                                if ($nudgePoints != NULL) {
                                    if ($nudgePoints[0] > $maxFirstNudgePoint) {
                                        $maxFirstNudgePoint = $nudgePoints[0];
                                    }
                                }
                            }
                        }
                    }

                    if ($maxFirstNudgePoint) {
                        for ($i = $num - 1; $i >= 0; $i--) {
                            $log = $logData[$i];
                            $logString = json_encode($log);
                            $result = json_decode($results[$i], true);
                            $newTimeStamp = $tStamps[$i];
                            $totalRecommendations = sizeof($result);
                            $totalLikes = 0;
                            $totalHides = 0;
                            $user2s = "(" . implode(",", array_keys($result)) . ")";
                            // Check if like hide table contains an entry for (user_id,user2)
                            if ($previousTimeStamp != NULL) {
                                $q1 = "SELECT COUNT(*) as count from user_like where user1 = $user_id and user2 in $user2s and timestamp < \"$previousTimeStamp\" and timestamp > \"$newTimeStamp\"";
                                $q2 = "SELECT COUNT(*) as count from user_hide where user1 = $user_id and user2 in $user2s and timestamp < \"$previousTimeStamp\" and timestamp > \"$newTimeStamp\"";
                            } else {
                                $q1 = "SELECT COUNT(*) as count from user_like where user1 = $user_id and user2 in $user2s and timestamp > \"$newTimeStamp\"";
                                $q2 = "SELECT COUNT(*) as count from user_hide where user1 = $user_id and user2 in $user2s and timestamp > \"$newTimeStamp\"";
                            }
                            $likeValue = $conn_reporting->Execute($q1)->GetRows();
                            if ($likeValue[0]['count']) {
                                $totalLikes += $likeValue[0]['count'];
                            } else {
                                $hideValue = $conn_reporting->Execute($q2)->GetRows();
                                if ($hideValue[0]['count']) {
                                    $totalHides += $hideValue[0]['count'];
                                }
                            }

                            $nudgeShown = false;
                            $nudgeConfigured = false;
                            if (array_key_exists("AestheticsAB", $log)) {
                                if (array_key_exists("Aesthetics_None", $log["AestheticsAB"])) {
                                    unset($log["AestheticsAB"]["Aesthetics_None"][2]);
                                } else if (array_key_exists("Aesthetics_Nudge", $log["AestheticsAB"])) {
                                    $nudgeConfigured = true;
                                    $sessionId = $log["AestheticsAB"]["Aesthetics_Nudge"][2];
                                    $nudgeParams = $log["AestheticsAB"]["Aesthetics_Nudge"][1];
                                    $nudgePoints = $nudgeParams[$sessionId];
                                    unset($log["AestheticsAB"]["Aesthetics_Nudge"][2]);
                                    if ($nudgePoints != NULL) {
                                        if ($nudgePoints[0] <= $totalLikes + $totalHides) {
                                            $nudgeShown = true;
                                        }
                                    }
                                }
                                $logString = json_encode($log);
                            }

                            if (!($nudgeConfigured xor $nudgeShown) and ( ($totalLikes + $totalHides) > $maxFirstNudgePoint)) {
                                if (!array_key_exists($logString, $report)) {
                                    $report[$logString] = array(array(), array(), array());
                                }
                                $report[$logString] = array_map('ArrayUtils::concat', $report[$logString], array(round($totalLikes * 100 / $totalRecommendations, 1, PHP_ROUND_HALF_UP), round($totalHides * 100 / $totalRecommendations, 1, PHP_ROUND_HALF_UP), round(($totalLikes + $totalHides) * 100 / $totalRecommendations, 1, PHP_ROUND_HALF_UP)));
                            }
                            $previousTimeStamp = $newTimeStamp;
                        }
                    }
                }
            }
        }
        return $report;
    }

}

try {
    global $smarty;
    $reports = new AB_Reporter();
    $ab_data = $reports->computeUserBehaviourData($duration, $date);
    $ab_summary = $reports->computeUserBehaviourSummary($ab_data);
    $ab_test_results = $reports->computeTTestResults($ab_data);
    $smarty->assign("ab_summary", $ab_summary);
    $smarty->assign("ab_test_results", $ab_test_results);
    $reports->sendEmail($duration, $date, "AB Testing :");
} catch (Exception $e) {
    $msg = "AB Reporting Failed";
    echo $e->getMessage();
    trigger_error("PHP WEB: Script failed ($msg) " . $e->getTraceAsString(), E_USER_WARNING);
}
