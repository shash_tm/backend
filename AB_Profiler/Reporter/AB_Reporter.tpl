<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>AB Statistics</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <br>
    <b>AB Stats</b><br>
    <br><b>Description:</b><br>
    <br><b>AB Identifier (Control/Treatment) :</b>
    <br>Description of the control/treatment group. The Control is the existing system with RecommendationAB = RecommendationEngine and Aesthetics = Aesthetics_None. Any different combination is a Treatment.
    <br><b>Count :</b> 
    <br>Number of times the group has been exposed to the users.
    <br><b>Like %ages :</b>
    <br>Distribution of like behaviour as a percentage of total recommendations.
    <br><b>Hide %ages :</b>
    <br>Distribution of hide behaviour as a percentage of total recommendations.
    <br><b>Total Action %ages :</b>
    <br>Distribution of total action (likes + hides) behaviour as a percentage of total recommendations.<br><br>
    
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td>AB Identifier</td>
            <td>Count</td>
            <td>*</td>
            <td>*</td>
            <td>Likes % Min</td>
            <td>Likes % 25%</td>
            <td>Likes % 50%</td>
            <td>Likes % Mean</td>
            <td>Likes % 75%</td>
            <td>Likes % Max</td>
            <td>*</td>
            <td>*</td>
            <td>Hides % Min</td>
            <td>Hides % 25%</td>
            <td>Hides % 50%</td>
            <td>Hides % Mean</td>
            <td>Hides % 75%</td>
            <td>Hides % Max</td>
            <td>*</td>
            <td>*</td>
            <td>Tot Actions % Min</td>
            <td>Tot Actions % 25%</td>
            <td>Tot Actions % 50%</td>
            <td>Tot Actions % Mean</td>
            <td>Tot Actions % 75%</td>
            <td>Tot Actions % Max</td>
        </tr>

        {foreach  from=$ab_summary key=k item=v}
            <tr>
                <td>{$k}</td>
                <td>{$v[0][6]}</td>
                <td>*</td>
                <td>*</td>
                <td>{$v[0][0]}</td>
                <td>{$v[0][1]}</td>
                <td>{$v[0][2]}</td>
                <td>{$v[0][3]}</td>
                <td>{$v[0][4]}</td>
                <td>{$v[0][5]}</td>
                <td>*</td>
                <td>*</td>
                <td>{$v[1][0]}</td>
                <td>{$v[1][1]}</td>
                <td>{$v[1][2]}</td>
                <td>{$v[1][3]}</td>
                <td>{$v[1][4]}</td>
                <td>{$v[1][5]}</td>
                <td>*</td>
                <td>*</td>
                <td>{$v[2][0]}</td>
                <td>{$v[2][1]}</td>
                <td>{$v[2][2]}</td>
                <td>{$v[2][3]}</td>
                <td>{$v[2][4]}</td>
                <td>{$v[2][5]}</td>  
            </tr>
        {/foreach}
    </table>
    
    <br><br><b>AB Comparisons with p-values</b><br>
    <table align="left" border="1" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td>Control</td>
            <td>Treatment</td>
            <td>*</td>
            <td>Likes p-value</td>
            <td>*</td>
            <td>*</td>
            <td>Hides p-value</td>
            <td>*</td>
            <td>*</td>
            <td>Tot Actions p-value</td>
        </tr>

        {foreach  from=$ab_test_results key=control item=abs}
            {foreach  from=$abs key=treatment item=pvalues}
            <tr>
                <td>{$control}</td>
                <td>{$treatment}</td>
                <td>*</td>
                <td>{$pvalues[0]}</td>
                <td>*</td>
                <td>*</td>
                <td>{$pvalues[1]}</td>
                <td>*</td>
                <td>*</td>
                <td>{$pvalues[2]}</td>
            </tr>
            {/foreach}
        {/foreach}
    </table>
</body>
</html>