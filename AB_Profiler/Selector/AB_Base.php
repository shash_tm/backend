<?php

/**
 * AB_Base class is used to defined the functionality that any feature that needs
 * AB should possess.
 */
abstract class AB_Base {
    
    public      $logger;
    public      $ab;
    protected   $strategy;
    protected   $selectedAB = "";
    protected   $selectedABFilters = array();
    protected   $selectedABPercent = 100;
    protected   $selectedABParams = array();
    protected   $configPercentFilters = array();
    protected   $configParams = array();
    
    function __construct(BaseStrategy $strategy, $configPercentFilters, $configParams, AB_Logger $logger, AB_Base $ab = NULL) {
        $this->logger = $logger;
        $this->ab = $ab;
        $this->strategy = $strategy;
        $this->configPercentFilters = $configPercentFilters;
        $this->configParams = $configParams;
    }
    
    public function getSelectedABPercent() {
        return $this->selectedABPercent;
    }
    
    public function getSelectedAB() {
        return $this->selectedAB;
    }
    
    public function getSelectedABParams() {
        return $this->selectedABParams;
    }
   
    protected function selectAB($user_id,$gender,$age,$location,$city = null, $sessionId = null) {
        if($this->selectedAB == "") {
            $this->reSelectAB($user_id,$gender,$age,$location,$city,$sessionId);
        }
        return $this->selectedAB;
    }
    
    abstract public function reSelectAB($user_id,$gender,$age,$location,$city = null, $sessionId = null);
    
    abstract public function logSelection();
    
    abstract public function getLogData($sessionId);
    
    abstract public function getSessionSpecificLogData($sessionId);
    
    abstract public function getLogString($sessionId);
    
    abstract public function getSessionSpecificLogString($sessionId);
}