<?php
require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/AB_Base.php";
require_once dirname ( __FILE__ ) . "/../../include/systemMessages.php";

/**
 * AB_Aesthetics class is use to test the aesthetic side of the UI.
 * Generally no implementation details are required here and only markers
 * need to be sent to the app and logged for AB.
 */
class AB_Aesthetics extends AB_Base {
    
    function __construct(BaseStrategy $recommendationStrategy, $configPercentFilters, $configParams, AB_Logger $logger, AB_Base $ab = NULL) {
        parent::__construct($recommendationStrategy, $configPercentFilters, $configParams, $logger, $ab);
    }
    
    public function reSelectAB($user_id,$gender,$age,$location, $city = null, $sessionId = null) {
        $this->selectedAB = $this->strategy->selectImpl($this->configPercentFilters,$user_id,$gender,$age,$location, $city, $sessionId);
        $this->selectedABPercent = $this->configPercentFilters[$this->selectedAB][0];
        $this->selectedABFilters = json_decode($this->configPercentFilters[$this->selectedAB][1],true);
        $this->selectedABParams = json_decode($this->configParams[$this->selectedAB],true);
        
        if($this->selectedAB == "") {
            $this->selectedAB = "Aesthetics_None";
            $this->selectedABPercent = 100;
            $this->selectedABFilters = array();
            $this->selectedABParams = array();
        }
        
        if($this->ab != NULL) {
            $this->ab->reSelectAB($user_id,$gender,$age,$location,$city,$sessionId);
        }
        return $this->selectedAB;
    }
    
    public function getLogData($sessionId) {
        if($this->ab != NULL) {
            return array("AestheticsAB" => array($this->selectedAB => array($this->selectedABPercent,$this->selectedABParams,$sessionId))) + $this->ab->getLogData($sessionId);
        }
        else {
            return array("AestheticsAB" => array($this->selectedAB => array($this->selectedABPercent,$this->selectedABParams,$sessionId)));
        }
    }
    
    public function getSessionSpecificLogData($sessionId) {
        $nudgePoints = $this->selectedABParams[$sessionId];
        $nudgeMessages = systemMsgs::$nudgeMessages[$sessionId];
        $filteredNudgePoints = array_values(
                                    array_intersect(
                                            $nudgePoints,
                                            array_keys($nudgeMessages)));
        $filteredNudgeMessages = array_values(
                                    array_map(
                                        function($nudgePoint) use ($nudgeMessages){
                                            return $nudgeMessages[$nudgePoint];
                                        },
                                        $filteredNudgePoints));
                            
        if($this->ab != NULL) {
            return array("AestheticsAB" => array($this->selectedAB => array($filteredNudgePoints, $filteredNudgeMessages))) + $this->ab->getSessionSpecificLogData($sessionId);
        }
        else {
            return array("AestheticsAB" => array($this->selectedAB => array($filteredNudgePoints, $filteredNudgeMessages)));
        }
    }
    
    public function getLogString($sessionId) {
        return json_encode($this->getLogData($sessionId));
    }
    
    public function getSessionSpecificLogString($sessionId) {
        return json_encode($this->getSessionSpecificLogData($sessionId));
    }
    
    public function logSelection($user_id = -1) {
        $this->logger->logAestheticsDetails($user_id,$this->selectedAB);
    }
}
