<?php
require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/AB_Base.php";

foreach( glob(dirname( __FILE__ ) . "/../../RecommendationEngines/*.php") as $recommendation_path ) {
    require_once( $recommendation_path );
}

/**
 * AB_Recommender class is use to select the Recommender to be used to serve the
 * user in the current request.
 */
class AB_Recommender extends AB_Base {
	
	
    function __construct(BaseStrategy $recommendationStrategy, $configPercentFilters, $configParams, AB_Logger $logger, AB_Aesthetics $aestheticsSelector = NULL) {
        parent::__construct($recommendationStrategy, $configPercentFilters, $configParams, $logger, $aestheticsSelector);
    }
    
    public function reSelectAB($user_id,$gender,$age,$location, $city = null, $sessionId = null) {
        $this->selectedAB = $this->strategy->selectImpl($this->configPercentFilters,$user_id,$gender,$age,$location,$city, $sessionId);
      //  echo "<pre>";
       // var_dump($this->selectedAB);
        
        $this->selectedABPercent = $this->configPercentFilters[$this->selectedAB][0];
        $this->selectedABFilters = json_decode($this->configPercentFilters[$this->selectedAB][1],true);
        $this->selectedABParams = json_decode($this->configParams[$this->selectedAB],true);
        
       // var_dump($this->selectedAB);
        //var_dump($this->selectedABParams);exit;
        
        if($this->selectedAB == "") {
            $this->selectedAB = "RecommendationEngine";
            $this->selectedABPercent = 100;
            $this->selectedABFilters = array();
            $this->selectedABParams = array();
        }
        
        
        if($this->ab != NULL) {
            $this->ab->reSelectAB($user_id,$gender,$age,$location,$city, $sessionId);
        }
        
        //var_dump( $this->selectedAB);
        return $this->selectedAB;
    }
    
    public function getLogData($sessionId) {
        if($this->ab != NULL) {
            return array("RecommendationAB" => array($this->selectedAB => array($this->selectedABPercent,$this->selectedABParams))) + $this->ab->getLogData($sessionId);
        }
        else {
            return array("RecommendationAB" => array($this->selectedAB => array($this->selectedABPercent,$this->selectedABParams)));
        }
    }
    
    public function getSessionSpecificLogData($sessionId) {
        if($this->ab != NULL) {
            return array("RecommendationAB" => array($this->selectedAB => array($this->selectedABPercent,$this->selectedABParams))) + $this->ab->getSessionSpecificLogData($sessionId);
        }
        else {
            return array("RecommendationAB" => array($this->selectedAB => array($this->selectedABPercent,$this->selectedABParams)));
        }
    }
    
    public function getLogString($sessionId) {
        return json_encode($this->getLogData($sessionId));
    }
    
    public function getSessionSpecificLogString($sessionId) {
        return json_encode($this->getSessionSpecificLogData($sessionId));
    }

    public function selectRecommendationEngine($recommendationEngine, $selectedABParams, $user_id) {
        $regex = '/.*(_[0-9]+)/';
        
        //preg_match ( $regex, $recommendationEngine, $matches );
        //var_dump($matches);
       
        if ( preg_match ( $regex, $recommendationEngine, $matches ) ) {
            $recommendationEngine = substr_replace($recommendationEngine,
                                                    '',
                                                    strrpos($recommendationEngine, $matches[1]),
                                                    strlen( $matches[1]));
        }
      
	$recommendation = new $recommendationEngine($user_id,$selectedABParams);
	return $recommendation;
    }
            
    public function fetchResultsFromRecommender($recommendationEngine, $selectedABParams, $user_id, $sessionId) {
    	//var_dump($recommendationEngine);exit;
        $recommendation = $this->selectRecommendationEngine($recommendationEngine, $selectedABParams, $user_id);
        $arr =  $recommendation->fetchResults($this->getLogString($sessionId), $sessionId);
        return $arr;
    }
    
    public function fetchResultsForUser($user_id,$gender,$age,$location, $city, $sessionId) {
        //1. Select Recommender
        $selectedRecommender = $this->selectAB($user_id, $gender, $age, $location, $city, $sessionId);
        
        //2. Use AB_Logger to log the selected recommender -> ("UserId","timeStamp","RecommenderSelected")
        #$this->logger->logRecommenderDetails($user_id,$selectedRecommender);
        
        //3. Get matches from the selected recommendation system
        return $this->fetchResultsFromRecommender($selectedRecommender,$this->selectedABParams,$user_id,$sessionId);
    }

    public function fetchResultsForUserFromContainingUsers($user_id,$gender,$age,$location, $city, $sessionId,$containingUsers = null, $passedFreshLimit = 0, $passedLikeLimit = 0) {
        //1. Select Recommender
        $selectedRecommender = $this->selectAB($user_id, $gender, $age, $location, $city, $sessionId);
        #Get the selected recommendation engine class
	$recommender = $this->selectRecommendationEngine($selectedRecommender,$this->selectedABParams,$user_id);
        $logString = $this->getLogString($sessionId);
	$resultantSet =  $recommender->generateMatchesUsingMemoryTableClassSpecific($user_id, 0, $logString, $sessionId, $containingUsers, $passedFreshLimit,$passedLikeLimit);
        #$recommender->doLogging($resultantSet['final_score_set'], $resultantSet['result_like_set'], $resultantSet['likeQuery'], $resultantSet['result_fresh_set'], $resultantSet['freshProfileQuery'], $resultantSet['countToFetchFresh'], $resultantSet['logString']); 
	return $recommender->addMatchesToSetOnly($resultantSet['final_set'], $resultantSet['liked_ids'], $resultantSet['availabile_set']);
    }

    public function logSelection($user_id = -1) {
        $this->logger->logRecommenderDetails($user_id,$this->selectedAB);
    }
}
