<?php
require_once dirname ( __FILE__ ) . "/../../DBO/AB_DBO.php";

/**
 * AB_Logger is a connector to send logs to the DBObject
 */
class AB_Logger {
    private $dbo;
    
    function __construct() {
        $this->dbo = new AB_DBO();
    }

    //1. Simply add rows to a table that logs the selected RecommenderSystem.
    public function logRecommenderDetails($user_id,$selectedRecommender) {
        $this->dbo->logRecommenderDetails($user_id,$selectedRecommender);
    }
    
    public function logAestheticsDetails($user_id,$selectedAesthetics) {
        $this->dbo->logAestheticsDetails($user_id,$selectedRecommender);
    }
}
