<?php 
/*
 * @uthor:Arpan
 * Valhar Morghulis
 */
require_once dirname(__FILE__).'/include/config.php'; 
require_once dirname(__FILE__).'/include/Utils.php';
require_once dirname ( __FILE__ ) . "/include/tbconfig.php";
require_once dirname ( __FILE__ ) . "/TrustBuilder.class.php";
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";
class OTP
{
	private $url;
	private $user_id;
	private $conn;
	private $otp;
	private $phn;
	private $key;
	private $sender_id;
	private $msg;
	private $trials;
	private $otp_tries;
	function __construct($user_id)
	{
		 global $conn,$infibuzz_url,$infibuzz_api_key;
		 $this->url=$infibuzz_url;
		 $this->user_id=$user_id;
		 $this->conn=$conn;
		 $this->phn=NULL;
		 $this->otp=NULL;
		 $this->key=$infibuzz_api_key;
		 $this->sender_id='TRUMAD';
		 $this->trials=0;
		 $this->otp_tries=0;
	}
	
	function phone_submit($phn) {
		$this->phn=$phn;
	    $time1=time();
	   //checks if the user is already verified by phone no. 
		$rs = $this->conn->Execute($this->conn->prepare("select * from user_phone_number where user_id=? and status='active'"),$this->user_id);
		if ($rs->RowCount () > 0)
		{
			$data = $rs->FetchRow ();
			if($data['user_id']!=$this->userId){
				$number=$data['user_number'];
				$number = substr_replace($number,"XXXXX",5,5);
				$return['responseCode'] = 403;
				$return['status']= 'fail';
				$return['call_me']= FALSE;
				$return['error'] = "We've already verified you with this phone no. $number";
				return $return;                                      
			}
		}
		
	  //checks if the phone no. is already assigned to any other user
	    $rs = $this->conn->Execute($this->conn->prepare("select user_id from user_phone_number where user_number=? and status='active'"),$phn);
		if ($rs->RowCount () > 0)
		{
			
			$data = $rs->FetchRow ();
			if($data['user_id']!=$this->userId){
				$return['responseCode'] = 403;
				$return['status']= 'fail';
				$return['call_me']= FALSE;
				$return['error'] = 'Hey! This number is in use by another TrulyMadly user.';
				return $return;                                      }
		}
		
		
		//checks if user has already tried too many times for otp
		$res = $this->conn->Execute($this->conn->prepare("select max(number_of_trials) as 'trials' from user_otp where user_id=? group by user_id"),$this->user_id);
		if($res->RowCount() > 0)
		{
			$data=$res->FetchRow();
			$this->trials=$data['trials'];
		}
		
		if ($this->trials >= Utils::$phoneNumberVerifyThreshold)
		{	
			$return['responseCode'] = 403;
			$return['status']= 'fail';
			$return['call_me']= TRUE;
			$return['error'] = "Oops! You've exhausted your verification codes. Please use verification by call.";
			return $return;
		}
			
		$rs2 = $this->conn->Execute($this->conn->prepare("select otp from user_otp where user_id=? and phone_number=?"),array($this->user_id,$phn));
	
		if ($rs2->RowCount()>0) {
			$data = $rs2->FetchRow();
			$this->otp=$data['otp'];
		}
		else 
		{
			
			$this->otp=rand(1001,9999);
			$rs3 = $this->conn->Execute($this->conn->prepare("insert into user_otp (user_id,phone_number,otp,timestamp) values(?,?,?,now())"),array($this->user_id,$phn,$this->otp));
			
		}
		
		$this->msg="Hi! Your TrulyMadly One Time Password is $this->otp. Safe way to unsingle  :)";
		$this->msg=urlencode($this->msg);
		$this->url=$this->url."api/v3/index.php?method=sms&api_key=$this->key&to=$this->phn&sender=$this->sender_id&message=$this->msg";

		//echo "\n$url";
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$response = curl_exec($ch);
		curl_close($ch);
	
		$response=json_decode($response,true);
		if($response['status']=='OK')
		{   $this->trials+=1;
			$rs5 = $this->conn->Execute($this->conn->prepare("update user_otp set number_of_trials=? where user_id=?"),array($this->trials,$this->user_id));
			$return['responseCode']=200;
			$return['status']= 'success';
			$time2=time();
			$diff=$time2-$time1;
			$this->logAction("otp_generated", "success", $diff,$response['message']);
		}
		else 
		{
			$return['responseCode']=403;
			$return['status']= 'fail';
			$return['call_me']=TRUE;
			$return['error'] = 'Verification code sending failed! Please use verification by call.';
			$time2=time();
			$diff=$time2-$time1;
			$this->logAction("otp_generated", "failure", $diff,$response['message']);
		}
		return $return;
	}
	
	function verify_otp($phn, $otp) {
		global $tbNumbers;
	    $time1=time();
		$rs = $this->conn->Execute($this->conn->prepare("select otp, otp_tries from user_otp where user_id=? and phone_number=?"),array($this->user_id,$phn));

		if ($rs->RowCount()>0)
		{
			$data=$rs->FetchRow();
			$this->otp=$data['otp'];
			$this->otp_tries=$data['otp_tries'];
			if ($this->otp_tries > Utils::$otpTriesThreshold) {
				$return['responseCode']=403;
				$return['status']= 'fail';
				$return['call_me']= TRUE;
				$return['error'] = 'Oops! Too many attempts were made for this verification code. Please use verification by call.';
				$time2=time();
				$diff=$time2-$time1;
				$this->logAction("otp_verification", "failure", $diff, $return['error'] );
				return $return;
			}
			else 
			{
			$this->otp_tries+=1;
			$rs4 = $this->conn->Execute($this->conn->prepare("update user_otp set otp_tries=? where user_id=? and phone_number=?"),array($this->otp_tries,$this->user_id,$phn));
			}	
		}
		else 
		{
			$return['responseCode']=403;
			$return['status']= 'fail';
			$return['call_me']= TRUE;
			$return['error'] = 'No OTP generated for this phone number';
			$time2=time();
			$diff=$time2-$time1;
			$this->logAction("otp_verification", "failure", $diff, $return['error'] );
			return $return;
		}

		//var_dump($this->otp);
		//var_dump($otp);
		if($otp==$this->otp)
		{   
			//var_dump($this->otp);
			$rs2 = $this->conn->Execute($this->conn->prepare("insert into user_phone_number(user_id, upload_time, user_number, status) values(?,now(),?,?) 
					on duplicate key update upload_time=now(),user_number=?,status=?"),array($this->user_id,$phn,'active',$phn,'active'));
			$rs3 = $this->conn->Execute($this->conn->prepare("update user_otp set is_verified='yes' where user_id=? and phone_number=?"),array($this->user_id,$phn));
				
			$trustbuilder = new TrustBuilder($this->user_id);
			$trustbuilder->updateMobileTrustScore($phn);
			$trustbuilder->authenticateUser();
			 
			$return['responseCode']=200;
			$return['status']= 'success';
			$return['is_otp_matched'] = TRUE;
			$time2=time();
			$diff=$time2-$time1;
			$this->logAction("otp_verification", "success", $diff,'success');
			return $return;	
		}
		else 
		{
			$return['responseCode']=200;
			$return['status']= 'success';
			$return['is_otp_matched'] = FALSE;
		  /*$return['$this->otp'] = $this->otp;
			$return['otp'] = $otp;
			$return['$phn'] = $phn;
			$return['$this->user_id'] = $this->user_id;*/
			$time2=time();
			$diff=$time2-$time1;
			$this->logAction("otp_verification", "failure", $diff,$return['error'] );
			return $return;
		}
	}
	
	private function logAction($event,$status,$diff,$event_info='success') {
		$Trk = new EventTrackingClass();
		$data_logging = array();
		$data_logging [] = array( "user_id"=>$this->user_id,"activity" => "phone_otp" , "event_type" => $event, "event_status" => $status, "event_info" => $event_info, "time_taken"=>$diff );
		$Trk->logActions($data_logging);
	}
	
	
}

/*
 try {
	$obj=new OTP('492697');
    //var_dump($obj);
	//echo "AA";
	//$result=$obj->phone_submit(9873791971);
	//var_dump($result);
	//echo $result['status'];
	//echo "AB";
	$result2=$obj->verify_otp('9873954808','5840');
	var_dump($result2);
	
} catch (Exception $e) {
	echo "Exception Occured :  $e->getMessage() \n $e->getTraceAsString()";
}
*/
?>