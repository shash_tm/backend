<?php
require_once dirname(__FILE__) . "/../../DBO/curatedDatesDBO.php";
require_once dirname(__FILE__) . "/../../abstraction/query_wrapper.php";
require_once dirname(__FILE__) . "/../RecommendationSystem/UtilsDate.php";
require_once dirname(__FILE__) . "/../RecommendationSystem/BaseDateliciousRecommendation.php";

/**
 * The class computes the count of datespots for each datespot based on deals sent
 * and accepted by users of different ages as well as the total deals exchanged & 
 * accepted on a daily basis
 */
class updatePopularityDatespots {
    
    public function updateTable() {
    	$lastDayDealsQuery = "select user1,user2,datespot_hash_id,created_tstamp,
    			datediff(date(now()),created_tstamp) as created,
    			status from user_deals
    			where created_tstamp between Date( DATE_SUB(now(),INTERVAL 1 DAY)) and date(now())
    			or last_updated_tstamp between Date( DATE_SUB(now(),INTERVAL 1 DAY)) and date(now())";
    	//     	DATE_SUB(Date(now()), INTERVAL '1 5:30' DAY_MINUTE
    	$lastDayDeals= Query_Wrapper::SELECT($lastDayDealsQuery, array(), Query::$__slave);
    	foreach($lastDayDeals as $key => $value){
    		//$diff = abs(strtotime(time()) - strtotime($lastDayDeals[$key]['created_tstamp']));
    		// Check if the deal was also created
    		if($lastDayDeals[$key]['created']==1){
    			$this->updateByUserGenderAge($lastDayDeals[$key]['user1'],$lastDayDeals[$key]['datespot_hash_id']);
    		}
    		// Check if the deal was accepted 
    	    if($lastDayDeals[$key]['status']=='accepted'){ 
    			$this->updateByUserGenderAge($lastDayDeals[$key]['user2'],$lastDayDeals[$key]['datespot_hash_id']);
    		}
    	}
    }
    	
    private function updateByUserGenderAge ($user1,$hash_id){
    	$genderArray = BaseDateliciousRecommendation::getGender($user1);
    	$gender = $genderArray['gender'];
    	$gender = strtolower($gender);
    	$age = BaseDateliciousRecommendation::getAge($user1);
    	$colName2 = $gender."Total";
    	if ($age>30){
    		$colName1 = $gender."30Plus";
    	} else{
    		$colName1 = $gender.$age;
    	}
    	$table = "datespot_popularity" ;
    	$updateSql = "insert into $table ( hash_id,".$colName1.",".$colName2.") 
    				  values (? ,1, 1) 
    			 	  on duplicate key 
    				  update " .$colName1."=".$colName1."+1, ".$colName2."=".$colName2."+1;" ;
    	Query_Wrapper::INSERT($updateSql, array($hash_id), $table) ;
    }
}

try{
	echo "Start";
    $updatePopularity = new updatePopularityDatespots();
    $updatePopularity->updateTable();
} catch (Exception $e){
	echo $e->getTraceAsString();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	trigger_error($e->getMessage(), E_USER_WARNING); 
}
?>
