<?php
require_once dirname(__FILE__) . "/../../DBO/curatedDatesDBO.php";
require_once dirname(__FILE__) . "/../../abstraction/query_wrapper.php";
require_once dirname(__FILE__) . "/../RecommendationSystem/UtilsDate.php";
require_once dirname(__FILE__) . "/../RecommendationSystem/BaseDateliciousRecommendation.php";

/**
 * The class populates the count of datespots for each datespot based on deals sent
 * and accepted by users of different ages as well as the total deals exchanged & 
 * accepted.
 * 
 * This script is to be run only once to initially fill the popularity table
 */
class createPopularityTable {
    
    public function updateTable() {
    	$lastDayDealsQuery = "select ud.user1,ud.user2,ud.datespot_hash_id,ud.created_tstamp,ud.last_updated_tstamp,
    						  ud.status
							  from user_deals ud
    						  where created_tstamp < date(now());";
    	$lastDayDeals= Query_Wrapper::SELECT($lastDayDealsQuery, array(), Query::$__slave);
    	foreach($lastDayDeals as $key => $value){
    		$this->updateByUserGenderAge($lastDayDeals[$key]['user1'],
    									 $lastDayDeals[$key]['datespot_hash_id']);
    		// For accepted status, update for user2 also.
    		if($lastDayDeals[$key]['status']=='accepted'){ 
    			$this->updateByUserGenderAge($lastDayDeals[$key]['user2'],
	    									 $lastDayDeals[$key]['datespot_hash_id']);
    		}
       	}
    }
    	
    private function updateByUserGenderAge ($user1,$hash_id){
    	$genderArray = BaseDateliciousRecommendation::getGender($user1);
    	$gender = $genderArray['gender'];
    	$gender = strtolower($gender);
    	$age = BaseDateliciousRecommendation::getAge($user1);
    	$colName2 = $gender."Total";
    	if ($age>30){
    		$colName1 = $gender."30Plus";
    	} else{
    		$colName1 = $gender.$age;
    	}
//     	$locationFields = $city_id.",".$zone_id.",'".$geo_location."',".$state_id;
//     	$locationFields = implode(",",array($city_id,$zone_id,$geo_location,$state_id));
       	$table = "datespot_popularity" ;
    	$updateSql = "insert into $table ( hash_id,".$colName1.",".$colName2.") 
    				  values (? ,1, 1) 
    			 	  on duplicate key 
    				  update " .$colName1."=".$colName1."+1, ".$colName2."=".$colName2."+1;" ;
    	Query_Wrapper::INSERT($updateSql, array($hash_id), $table) ;
    }
}

try{
	echo "Starting";
    $updatePopularity = new createPopularityTable();
    $updatePopularity->updateTable();
} catch (Exception $e){
	echo $e->getTraceAsString();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	trigger_error($e->getMessage(), E_USER_WARNING); 
}
?>
