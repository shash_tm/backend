<?php
require_once dirname ( __FILE__ ) . "/../DBO/curatedDatesDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
/**
 * To decide wether deal the icon is to be shown and wether is it clickable.
 * By- Sumit
 */
class DealIcon {
	private $user_id;
	private $user_location;
	private $last_deal_location ;
	private $curatedStatus ;
	
	function __construct($user_id) 
	{
		$this->user_id = $user_id;
		$this->curatedStatus = CuratedDatesDBO::getCuratedStatus();
		$this->user_location = array() ;
		$this->last_deal_location = array() ; 
	}
	
	// If the  deal icon is to be shown 
	public function is_icon_shown() 
	{   global $redis;
	    
		if(isset($this->user_id)&&$redis->sIsMember('curated_deal_users',$this->user_id))
		{
			return true;
		}
		// if the curated is not active then set  
		if ($this->curatedStatus != 'active')
		{

			return false ;
		}
		else
		{
			 $flag_shown = CuratedDatesDBO::currentLocation($this->user_id) ;
			 if ( count($flag_shown) > 0)
			 {
			 	return true;
			 }
			 else 
			 {
			 	$this->user_location = CuratedDatesDBO::getUserLocation($this->user_id) ;
			 	$deal_count = CuratedDatesDBO::countDealsForLocation($this->user_location['city_id'], $this->user_location['state_id']) ;
			 	
			 	if($deal_count > 0 )
			 	{
			 		CuratedDatesDBO::setCurrentLocation($this->user_id, $this->user_location['city_id'],$this->user_location['state_id'] ) ;
			 		return true ;
			 	}
			 	else 
			 	{
			 		return false ;
			 	}
			 }
		}
			
	}
	

	// If the city of the match has deals then the icon will be clickable 
	public function is_icon_clickabe($match_id)
	{
		// check if user pair has already been shown
		$clickable_shown = CuratedDatesDBO::clickableShown($this->user_id, $match_id);
		
		if( $clickable_shown > 0)
		{
			// clickable is already shown to this user pair . return true 
			return true ;
		}
		else 
		{
			$match_location = CuratedDatesDBO::getUserLocation($match_id) ;
			$match_deal_count = CuratedDatesDBO::countDealsForLocation($match_location['city_id'], $match_location['state_id']); 
			
			if ($match_deal_count > 0)
			{
				// this match's city has active deals and set the value in table and return true 
				CuratedDatesDBO::setUserPairClickableShown($this->user_id, $match_id);
				return true ;
			}
			else 
			{
				// this match's city does not have any active deals 
				return false; 
			}
		}
	}
}

?>