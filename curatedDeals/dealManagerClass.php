<?php 
require_once dirname ( __FILE__ ) . "/../DBO/curatedDatesDBO.php"; 
require_once dirname ( __FILE__ ) . "/../DBO/eventsDBO.php";
require_once dirname ( __FILE__ ) . "/../photomodule/photoUtils.php";

/*
 * TO manage the deals i.e. add new deals and edit the data for previous deals 
 * @Sumit
 */

class DealsManager 
{
	private $datespot_id ;
	
	function __construct()
	{
	   $this->datespot_id ;	
	}
	
	// to add an new sample datespot supposed to be edited later
	public function addSampleDatespot($friendly_name = "Sample Friendly Name", $name = "Sample Name", $location = "Some Area, New Delhi", $amount= "1000") 
	{
		CuratedDatesDBO::addSampleDS($friendly_name, $name, $location, $amount) ;	 
	}
	
	// to edit a priviously created datespot
	public function editDatespot($datespot_id, $field, $value) 
	{
		$tempImageArray = array();
		if($field == 'images_json' || $field == 'menu_images_json')
		{
			$value = json_decode($value);
			foreach ($value as  $img)
			{
			   try {
			   	if($field == 'images_json'){
			   		if(strpos($img,'profiles/datespot') == false)
			   			$tempImageArray[] = PhotoUtils::saveFileFromUrl($img , "datespot", 80, 1000, 1000);
			   		else
			   			$tempImageArray[] = PhotoUtils::saveFileFromUrl($img , "datespot", 100);			//not resizing image if it's already on our s3
			   	}
			   	else{
			   		$tempImageArray[] = PhotoUtils::saveFileFromUrl($img , "datespot", 100);				//not resizing menu images
			   	}
			   } catch (Exception $e) {
			   	trigger_error($e->getMessage(), E_USER_WARNING);
			   	continue ;
			   }
			}
			$value= json_encode($tempImageArray) ;
		}
		if($field == "list_view_image")
		{
			try
			{
				if(strpos($value,'profiles/datespot') == false)
					$value = PhotoUtils::saveFileFromUrl($value , "datespot", 80, 1000, 1000);
				else 
					$value = PhotoUtils::saveFileFromUrl($value , "datespot", 100);
			} catch (Exception $e) 
			{
				trigger_error($e->getMessage(), E_USER_WARNING);
				return ;
			}
			
		}
		if($field == 'category_id') {
			CuratedDatesDBO::editDatespotCategoryMapping($datespot_id, $value);
			return 0;
		}

		CuratedDatesDBO::editDS($datespot_id, $field, $value) ;
	}
	
	//  
	public function addCompleteDatespot ($deal) 
	{
		$tempImageArray = array();
		$images = json_decode($deal['images_json']) ;
		$menu_images = json_decode($deal['menu_images_json']) ;
		try {
			if(strpos($deal['list_view_image'],'profiles/datespot') == false)
				$deal['list_view_image'] =  PhotoUtils::saveFileFromUrl($deal['list_view_image'] , "datespot", 80, 1000, 1000);
			else 
				$deal['list_view_image'] =  PhotoUtils::saveFileFromUrl($deal['list_view_image']);
		} catch (Exception $e) {
			trigger_error($e->getMessage(), E_USER_WARNING);
		}
		foreach ($images as  $img)
			{
				try {
					if(strpos($img,'profiles/datespot') == false)
						$tempImageArray[] = PhotoUtils::saveFileFromUrl($img , "datespot", 80, 1000, 1000);		//not resizing image if it's already on our s3
					else
						$tempImageArray[] = PhotoUtils::saveFileFromUrl($img , "datespot", 100);						//not resizing menu image
				} catch (Exception $e) {
					trigger_error($e->getMessage(), E_USER_WARNING);
					continue ;
				}
			}
		$deal['images_json'] = json_encode($tempImageArray) ;
		$tempImageArray = array();
		
		foreach ($menu_images as  $img)
		{
			try {
				$tempImageArray[] = PhotoUtils::saveFileFromUrl($img , "datespot", 100);
			} catch (Exception $e) {
				trigger_error($e->getMessage(), E_USER_WARNING);
				continue ;
			}
		}
		$deal['menu_images_json'] = json_encode($tempImageArray) ;  
		return CuratedDatesDBO::addCompleteDS($deal) ;
	}
	
	public function showDatespotsList($status= NULL, $page = 0, $type='datespot')
	{
	    $lower = 100 * $page ;
	    $upper = 100 ; 
	    $list = CuratedDatesDBO::getDatespotList($status, $lower, $upper,$type) ;
	    return $list ;
	}
	
	public function showDatespotDetails($datespot_id)
	{
		global $imageurl;
		$datespot = CuratedDatesDBO::getDatespotDetails( $datespot_id ) ;
		$datespot['images_json'] = $this->attachImagePrefix(json_decode($datespot['images_json']));
		$datespot['menu_images_json'] = $this->attachImagePrefix(json_decode($datespot['menu_images_json'])); 
		$datespot['list_view_image'] =  $imageurl.$datespot['list_view_image'];
		$datespot['terms_and_conditions'] = json_decode($datespot['terms_and_conditions']);
		$datespot['hashtag_list'] = json_decode($datespot['hashtag_json'], true);
		$datespot['locations'] = CuratedDatesDBO::getDatespotLocations($datespot['hash_id']);
		$category_id = EventsDBO::getEventCategory($datespot_id);
		$datespot['category_id'] = $category_id['category_id'];
		//Do not question the logic if it works. 				------ St. Tarun

		for($i=0;$i < count($datespot['locations']);$i++){
			if($i == 0) {
				$datespot['city_id'] = $datespot['locations'][0]['city_id'];
				$datespot['phone_number'] = $datespot['locations'][0]['phone_number'];
				$datespot['location'] = $datespot['locations'][0]['location'];
				$datespot['address'] = $datespot['locations'][0]['address'];
				$datespot['state_id'] = $datespot['locations'][0]['state_id'];
				$datespot['zone_id'] = $datespot['locations'][0]['zone_id'];
				$datespot['geo_location'] = $datespot['locations'][0]['geo_location'];
			}
			if($i < count($datespot['locations'])) {
				$datespot['locations'][$i] = $datespot['locations'][$i + 1];
			}
		}
		unset($datespot['locations'][$i-1]);

		return $datespot ; 
	}

	public function getDatespotCategory($datespot_id){
		return EventsDBO::getEventCategory($datespot_id);
	}
	
	public function attachImagePrefix($img_array) 
	{
		global $imageurl;
		$return_arr = array() ;
		foreach ($img_array as $value) 
		{
			$return_arr [] = $imageurl . $value ;
		}
		
		return $return_arr ;
	}
	
	public function changeStatus($datespot_id, $status) 
	{
		CuratedDatesDBO::changeDatespotStatus($datespot_id, $status);
	}

	public function changeRank($datespot_id, $rank)
	{
		CuratedDatesDBO::changeDatespotRank($datespot_id, $rank);
	}

	public function deleteDatespotAction($datespot_id, $action)
	{
		if($action == 'delete')
			CuratedDatesDBO::deleteDatespotAction($datespot_id, 1);
		else
			CuratedDatesDBO::deleteDatespotAction($datespot_id, 0);
	}

	public function getZonesData(){
		return CuratedDatesDBO::getZonesData();
	}

	public function addNewZone($zoneName, $stateId, $cityId){
		$zoneId = CuratedDatesDBO::addNewZone($zoneName, $stateId, $cityId);
		if($zoneId == false){
			$result = array("status" => "error","msg" => "Zone with given name already exists.");
		}else {
			$zoneArray = array("zone_id" => $zoneId, "city_id" => $cityId, "state_id" => $stateId, "country_id" => 113, "name" => $zoneName);
			$result = array("status" => "success","zone_data" => $zoneArray);
		}
		return $result;
	}


	public function addNewCategory($categoryName, $stateId, $cityId, $imageUrl,$backgroundImage){
		$result = null;
		if(!isset($categoryName) || !isset($stateId) || !isset($cityId) || !isset($imageUrl) || !isset($backgroundImage))
			return array("status" => "error","msg" => "Insuffecient params");

		$backgroundImage = PhotoUtils::saveFileFromUrl($backgroundImage , "datespot", 80, 1000, 1000);
		// = PhotoUtils::saveFileFromUrl($imageUrl , "datespot", 80, 1000, 1000);

		$categoryId = eventsDBO::addNewCategory($categoryName, $stateId, $cityId,$imageUrl,$backgroundImage);
		if($categoryId == false){
			$result = array("status" => "error","msg" => "Category with given name already exists in the city/state.");
		}else {
			$categoryArray = array("category_id" => $categoryId, "city_id" => $cityId, "state_id" => $stateId, "name" => $categoryName);
			$result = array("status" => "success","category_data" => $categoryArray);
		}
		return $result;
	}

	public function getAllCategories($city_id,$state_id){
		return eventsDBO::getActiveEventCategoriesByCity($city_id, $state_id, false);
	}
}

?>