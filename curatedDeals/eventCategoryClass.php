<?php
require_once dirname ( __FILE__ ). "/../include/config.php";
require_once dirname(__FILE__) . "/../DBO/eventsDBO.php";
require_once dirname(__FILE__) . "/../DBO/curatedDatesDBO.php";
require_once dirname(__FILE__) . "/../DBO/sparkDBO.php";
require_once dirname(__FILE__) . "/../DBO/selectDBO.php";
require_once dirname(__FILE__) . "/../UserUtils.php";
require_once dirname(__FILE__) . "/../UserData.php";
require_once dirname(__FILE__) . "/../Recommender.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname(__FILE__) . "/../Advertising/IgnoredProfileUsersForReporting.php";

class eventcategory
{
    private $user_id;
    private $location ;
    private $categories ;
    private $userUtils;
    private $fmatch_user;
    private $ncr_city_array;
    private $functionClass;
    private $source;
    private $app_version_code;

    function __construct($user_id)
    {
        $this->user_id = $user_id ;
        $this->age = -1;
        $this->location ;
        $this->categories ;
        $this->userUtils = new UserUtils();
        $this->fmatch_user = NULL;
        $this->ncr_city_array = array(16743,47965,47964,47968,47967,47966);
        $this->functionClass =   new functionClass();
        $this->app_version_code = $this->functionClass->getHeader();
        $this->source = $this->functionClass->getSourceFromHeader();
    }

    public function showCategories ()
    {
        $this->getUserLocation();
        $this->getUserAge();
        $this->getCategoriesForLocation();
        return $this->categories;

    }

    private function getUserAge()
    {
	$userUtils = new userUtilsDBO();
        $user_data = $userUtils->getUserData(array($this->user_id), false);
	if(sizeof($user_data) > 0) {
		$this->age = $user_data[0]['age'];
	}
    }


    private function getUserLocation()
    {
        $this->location = curatedDatesDBO::getUserLocation($this->user_id);
    }
    private function getCategoriesForLocation()
    {
        if(in_array ($this->location['city_id'], $this->ncr_city_array)){
            $this->location['state_id']=2168;
        }
        $this->categories = eventsDBO::getActiveEventCategoriesByCity($this->location['city_id'],$this->location['state_id'], true);
    }

    private function getAdCampaigns()
    {
	$ignoredAdCampaignUserIds = IgnoredProfileUsersForReporting::getUserIds();
	return $ignoredAdCampaignUserIds;
    }

    #API to get mutual matches ignoring the AdCampaigns
    private function getMutualMatches($event_id, $ignoredAdCampaignUserIds,$userList,$mutualMatchLimit)
    {
        $mutual_matches = eventsDBO::getMutualMatchesForEvent($this->user_id, $event_id, $mutualMatchLimit);
	$mutualMatchArray = array();
	foreach($mutual_matches as $key => $value)
	{
            $mutualMatchArray[$mutual_matches[$key]['match_id']] = $mutual_matches[$key]['match_id'];
	}
	#remove the ad campaigns from mutual matches and return
	$diff_array = array_intersect(array_diff($mutualMatchArray,$ignoredAdCampaignUserIds),$userList);
	return $diff_array;
    }
    
    #API to get users liked by the user in the past and grouping them into no action taken and hidden
    private function getUsersLikedInThePast($ignoredAdCampaignUserIds,$userList, $limit)
    {
	if(sizeof($userList) <= 0) {
		$notActedArray = array();
		$wastedArray = array();
		return array('not_acted' => $notActedArray, 'wasted' => $wastedArray);
	}
	    $userUtils = new userUtilsDBO();;
            #Get users who have not yet acted on the like
	    $notActedLikesQueue =  array();
	    $tempA = $userUtils->getMyLikesNotActedUponQueue($this->user_id,$limit);
	    foreach($tempA as $key =>$value) {
		    $notActedLikesQueue[$tempA[$key]['user2']] = $tempA[$key]['user2'];
	    }
	    #Get users liked in the past but hidden by the other user
	    $wastedLikesQueue = array();
	    #$tempA = $userUtils->getMyWastedLikesQueue($this->user_id, $limit);
	    #foreach($tempA as $key =>$value) {
            #    array_push($wastedLikesQueue,$tempA[$key]['user2']);
	    #}
	return array('not_acted' => array_intersect(array_diff($notActedLikesQueue,$ignoredAdCampaignUserIds),$userList), 
                    'wasted' => array_intersect(array_diff($wastedLikesQueue,$ignoredAdCampaignUserIds),$userList));
    }

    private function getUsersSparkedInThePast($userList, $limit)
    {
        if(sizeof($userList) <= 0) {
            $sparked = array();
            return array_intersect(array('sparked' => $sparked), $userList);
        }

        return  array('sparked' => array_intersect(SparkDBO::getUsersSparkedByMe($this->user_id, $limit), $userList));
    }

    #API to get users who liked the user in the past and grouping them into no action taken and hidden
    private function getUsersWhoLikedMeInThePast($ignoredAdCampaignUserIds,$userList,$limit)
    {
	if(sizeof($userList) <= 0) {
		$notActedArray = array();
		$wastedArray = array();
		return array('not_acted' => $notActedArray, 'wasted' => $wastedArray);
	}
	    $userUtils = new userUtilsDBO();
            #Get users who have not yet acted on the like
	    $notActedLikesQueue = array();
	    #$tempA = $userUtils->getOthersLikesNotActedUponByMeQueue($this->user_id,$limit);
	    #foreach($tempA as $key =>$value) {
	    #	    array_push($notActedLikesQueue,$tempA[$key]['user1']);
	    #}
	    #Get users liked in the past but hidden by the other user
	    $wastedLikesQueue = array();
	    $tempA = $userUtils->getOthersWastedLikesOnMeQueue($this->user_id, $limit);
	    foreach($tempA as $key =>$value) {
		    $wastedLikesQueue[$tempA[$key]['user1']] = $tempA[$key]['user1'];
	    }
	return array('not_acted'    => array_intersect(array_diff($notActedLikesQueue,$ignoredAdCampaignUserIds),$userList), 
                     'wasted'       => array_intersect(array_diff($wastedLikesQueue,$ignoredAdCampaignUserIds),$userList));
    }

    #API to get recommended users
    private function getRecommendedUsers($gender)
    {
	$this->getUserAge();
	$this->getUserLocation();
	$this->fmatch_user = new Recommender ($this->user_id , $gender, $this->age, $this->location['state_id'],$this->location['city_id']);
	$recommendations_data = $this->fmatch_user->fetchResults();
	return $recommendations_data;
    }

    private function getRecommendedUsersOnlyFromProvidedList($gender,$userList,$passedLikeLimit,$passedFreshLimit)
    {
	$this->getUserAge();
	$this->getUserLocation();
	$this->fmatch_user = new Recommender ($this->user_id , $gender, $this->age, $this->location['state_id'],$this->location['city_id']);
	$recommendations_data = $this->fmatch_user->fetchResultsWithinUserList($userList,$passedLikeLimit,$passedFreshLimit);
	return $recommendations_data;
    }

    #API to get same gender users
    private function getSameGenderUsers($gender,$userList,$limit)
    {
	if($userList == "()") {
		return array();
	}
	    $userUtils = new userUtilsDBO();
	    $sameGenderUsers = $userUtils->getSameGenderUsers($gender,$userList,$limit);
	    return $sameGenderUsers;

    }

    public function getEventsByCategory($category_id,$zone_ids=null, $followStatus, $user_id,$is_ab = false){
        global $redis;
        $this->getUserLocation();
        if($redis->sIsMember('curated_deal_users',$user_id))
            $isTest=true;
        else
            $isTest = false;
        $eventList = eventsDBO::getEventsList($category_id,$zone_ids,$followStatus, $user_id,$isTest, $is_ab);
        $eventList = $this->processEventList($eventList, $user_id);
        return $eventList;
    }

    public function addGeoDistance($event_list, $lat, $lon){
        foreach($event_list as $key =>$event){
            $eventLoc = explode(",",$event['geo_location']);
            if($eventLoc != null && sizeof($eventLoc) == 2)
                $distance = $this->haversineGreatCircleDistance($lat,$lon,$eventLoc[0],$eventLoc[1]);
            $event_list[$key]["geo_location_distance"] = number_format((float)$distance, 1, '.', '');
            $event_list[$key]["distance_unit"] = "km";
        }
        usort($event_list, array(new EventCategory(), "cmp"));
        return $event_list;
    }

    public static function cmp($a, $b)
    {
        if($a['geo_location_distance'] < $b['geo_location_distance'])
            return 0;
        else
            return 1;

    }

    function getDateLocations($datespot_id, $zone_array)
    {
        $dateSpotList  = curatedDatesDBO::getDatespotLocations($datespot_id,$zone_array);
        foreach($dateSpotList as $key => $value){
            if(trim($dateSpotList[$key]['phone_number']) == "")
                $dateSpotList[$key]['phone_number'] = null;
        }
        return $dateSpotList;
    }

    function getDateSpotById($datespot_id)
    {
        global $imageurl;
        global $baseurl;
        $newImage = false;
        if(($this->source =='iOSApp' && $this->app_version_code >= 435) ||
        ($this->source =='androidApp' && $this->app_version_code >= 440))   {
            $newImage = true;
        }
        $dateSpotList  = eventsDBO::getDatespotById($datespot_id);
        if($dateSpotList['status'] == 'inactive')
            $dateSpotList['event_expired'] = true;
        else
            $dateSpotList['event_expired'] = false;
        if($dateSpotList['fb_page_url'] == null)
            unset($dateSpotList['fb_page_url']);

        $dateSpotList['ticket_price'] = $dateSpotList['ticket_price'] == null ? 0 : $dateSpotList['ticket_price'];
        $dateSpotList['is_trulymadly'] = $dateSpotList['is_trulymadly'] == 0 ? false : true;
        $dateSpotList['image'] = $imageurl. $dateSpotList['image'];
        $dateSpotList['images'] = $this->attachImageURL(json_decode($dateSpotList['images']));
        $dateSpotList['terms_and_conditions'] = json_decode($dateSpotList['terms_and_conditions']);
        if(strpos(strtolower($dateSpotList['event_ticket_text']), 'ticket') !== false ||
            strpos(strtolower($dateSpotList['event_ticket_text']), 'buy') !== false ||
            strpos(strtolower($dateSpotList['event_ticket_text']), 'book') !== false ){
            $dateSpotList['event_ticket_image'] = $newImage ? $baseurl."/images/events/ticket_new.png" : $baseurl."/images/events/ticket.png";
        }else{
            $dateSpotList['event_ticket_image'] = $newImage ? $baseurl."/images/events/more_info_new.png" : $baseurl."/images/events/more_info.png";
        }
        return $dateSpotList;
    }

    function processEventList($eventList, $user_id){
        global $imageurl;
        $eventMutualMatches =  $this->getMutualMatchesForAllEvents($user_id);
        foreach($eventList as $key => $value) {
            $mutualMatchCount = count($eventMutualMatches[$eventList[$key]["datespot_id"]]);
            $eventFollowerCount = eventsDBO::getUserCountForEvent($eventList[$key]["datespot_id"]);
            if($mutualMatchCount >= 3){
                $eventFollowerCount -= 3;
            }else{
                $eventFollowerCount -= $mutualMatchCount;
            }
            if($eventFollowerCount > 0 && $mutualMatchCount > 0){
                $eventFollowerCount = '+'.$eventFollowerCount;
            }

            if($eventList[$key]['event_age'] != null && $eventList[$key]['event_age'] <= 2){
                $eventList[$key]["new_event"] = true;
            }else{
                $eventList[$key]["new_event"] = false;
            }

            if(isset($eventList[$key]['is_trulymadly']) && $eventList[$key]['is_trulymadly'] == 1)
                $eventList[$key]['is_trulymadly'] = true;
            else
                $eventList[$key]['is_trulymadly'] = false;

           /* if(isset($last_seen) && $last_seen != null) {
                $diff = strtotime($last_seen) - strtotime($eventList[$key]['activate_date']);
                if ($diff < 0)
                    $eventList[$key]["new_event"] = true;
                else
                    $eventList[$key]["new_event"] = false;
            }*/
            $eventList[$key]["image"] = $imageurl . $eventList[$key]["image"];
            $eventList[$key]["mutual_matches"] = $eventMutualMatches[$eventList[$key]["datespot_id"]];
            $eventList[$key]["user_count"] = $eventFollowerCount;
        }
        return $eventList;
    }
        
    public function getUsersFollowingEvent($event_id, $currentUser){
        global $imageurl;
	global $config_socials_recommendation_list;
	$attendee_list = $config_socials_recommendation_list['socials_attendee_list'];
        $userData = new UserData($currentUser);
        $gender = $userData->fetchGender();
        $oppositeGender = ($gender == 'M') ? 'F' : 'M';
        #Now set the limits accordingly
	$recommendationCountForFemalesLike  = (int)$attendee_list['recommendationCountForFemalesLike'][0];
	$recommendationCountForMales        = (int)$attendee_list['recommendationCountForMalesFresh'][0];
	$recommendationCountForFemales      = (int)$attendee_list['recommendationCountForFemalesFresh'][0];
        $recommendationCountForMalesLike    = (int)$attendee_list['recommendationCountForMalesLike'][0];

        $mutualMatchesCountForMales         = (int)$attendee_list['mutualMatchesCountForMales'][0];
        $limitLikedMeInThePastForMales      = (int)$attendee_list['limitLikedMeInThePastForMales'][0];
        $limitILikedInThePastForMales       = (int)$attendee_list['limitILikedInThePastForMales'][0];
        $limitISparkedInThePastForMales       = (int)$attendee_list['limitISparkedInThePastForMales'][0];

        $mutualMatchesCountForFemales       = (int)$attendee_list['mutualMatchesCountForFemales'][0];
        $limitLikedMeInThePastForFemales    = (int)$attendee_list['limitLikedMeInThePastForFemales'][0];
        $limitILikedInThePastForFemales     = (int)$attendee_list['limitILikedInThePastForFemales'][0];
        $limitISparkedInThePastForFemales     = (int)$attendee_list['limitISparkedInThePastForFemales'][0];

        $recommendationCountForFemalesLikeSQL   = (int)$attendee_list['recommendationCountForFemalesLikeSQL'][0];
	$recommendationCountForMalesSQL         = (int)$attendee_list['recommendationCountForMalesFreshSQL'][0];
	$recommendationCountForFemalesSQL       = (int)$attendee_list['recommendationCountForFemalesFreshSQL'][0];
        $recommendationCountForMalesLikeSQL     = (int)$attendee_list['recommendationCountForMalesLikeSQL'][0];

        $mutualMatchesCountForMalesSQL          = (int)$attendee_list['mutualMatchesCountForMalesSQL'][0];
        $limitLikedMeInThePastForMalesSQL       = (int)$attendee_list['limitLikedMeInThePastForMalesSQL'][0];
        $limitILikedInThePastForMalesSQL        = (int)$attendee_list['limitILikedInThePastForMalesSQL'][0];
        $limitISparkedInThePastForMalesSQL        = (int)$attendee_list['limitISparkedInThePastForMalesSQL'][0];

        $mutualMatchesCountForFemalesSQL        = (int)$attendee_list['mutualMatchesCountForFemalesSQL'][0];
        $limitLikedMeInThePastForFemalesSQL     = (int)$attendee_list['limitLikedMeInThePastForFemalesSQL'][0];
        $limitILikedInThePastForFemalesSQL      = (int)$attendee_list['limitILikedInThePastForFemalesSQL'][0];
        $limitISparkedInThePastForFemalesSQL      = (int)$attendee_list['limitISparkedInThePastForFemalesSQL'][0];

        
        #Now get all the users to be shown in the list
        $userList = eventsDBO::getUserIdsFollowingEvent($event_id, $currentUser,$oppositeGender);
        $userIdsArray = array();
        #Now extract the ids and make it in the (,,) format
        foreach($userList as $key => $value)
        {
            array_push($userIdsArray,$userList[$key]['user_id']);
        }
        #first fetch the Ad campaigns
        $ignoredAdCampaignUserIds = $this->getAdCampaigns();
        #Fetch mutual matches
        $mutualMatchLimitSQL = ($gender == 'M') ? $mutualMatchesCountForMalesSQL : $mutualMatchesCountForFemalesSQL;
        $mutualMatchLimit = ($gender == 'M') ? $mutualMatchesCountForMales : $mutualMatchesCountForFemales;
        $mutual_matches = $this->getMutualMatches($event_id, $ignoredAdCampaignUserIds,$userIdsArray,$mutualMatchLimitSQL);
        if($mutualMatchLimit >= 0) {
            $mutual_matches = array_slice($mutual_matches,0,$mutualMatchLimit);
            $mutual_matches = array_combine($mutual_matches, $mutual_matches);
        }
        
        #Fetch same gender users
        #As per current discussions, we do not wish to pursue this, hence commenting it out
        #$same_gender_users = $this->getSameGenderUsers($gender,$userListString,-1);
	#Fetch users who liked me in the past
	$limitLikedMeInThePastSQL = ($gender == 'M') ? $limitLikedMeInThePastForMalesSQL : $limitLikedMeInThePastForFemalesSQL;
        $limitLikedMeInThePast = ($gender == 'M') ? $limitLikedMeInThePastForMales : $limitLikedMeInThePastForFemales;
	#Purav needs to revisit this, for female users  the wasted likes queue will be huge? Should we even call it for females?
	$likedMeInThePast = array();
        $likedMeInThePast = $this->getUsersWhoLikedMeInThePast($ignoredAdCampaignUserIds,array(),$limitLikedMeInThePastSQL);
        if($limitLikedMeInThePast >= 0) {
            $likedMeInThePast['not_acted'] = array_slice($likedMeInThePast['not_acted'],0,$limitLikedMeInThePast);
            $likedMeInThePast['not_acted'] = array_combine($likedMeInThePast['not_acted'],$likedMeInThePast['not_acted']);
            $likedMeInThePast['wastedArray'] = array_slice($likedMeInThePast['wastedArray'],0,$limitLikedMeInThePast);
            $likedMeInThePast['wastedArray'] = array_combine($likedMeInThePast['wastedArray'],$likedMeInThePast['wastedArray']);
        }

        #Fetch users liked in the past
        #Has 2 sub-arrays, wasted and not_acted
        $limitILikedInThePastSQL = ($gender == 'M') ? $limitILikedInThePastForMalesSQL : $limitILikedInThePastForFemalesSQL;
        $limitILikedInThePast = ($gender == 'M') ? $limitILikedInThePastForMales : $limitILikedInThePastForFemales;

        $limitISparkedInThePastSQL = ($gender == 'M') ? $limitISparkedInThePastForMalesSQL : $limitISparkedInThePastForFemalesSQL;
        $limitISparkedInThePast = ($gender == 'M') ? $limitISparkedInThePastForMales : $limitISparkedInThePastForFemales;

        $likedInThePast = $this->getUsersLikedInThePast($ignoredAdCampaignUserIds,$userIdsArray, $limitILikedInThePastSQL);

        $sparkedInThePast = $this->getUsersSparkedInThePast($userIdsArray, $limitISparkedInThePastSQL);

        $sparkedInThePast = $sparkedInThePast['sparked'];
        if($limitILikedInThePast >= 0) {
            $likedInThePast['not_acted'] = array_slice($likedInThePast['not_acted'],0,$limitILikedInThePast);
            $likedInThePast['not_acted'] = array_combine($likedInThePast['not_acted'],$likedInThePast['not_acted']);
            $likedInThePast['wastedArray'] = array_slice($likedInThePast['wastedArray'],0,$limitILikedInThePast);
            $likedInThePast['wastedArray'] = array_combine($likedInThePast['wastedArray'],$likedInThePast['wastedArray']);
        }

        //following is to remove liked user, whom i've sparked after having liked earlier.
        $common = array_intersect($likedInThePast['not_acted'],$sparkedInThePast);
        $likedInThePast['not_acted'] = array_diff($likedInThePast['not_acted'], $common);

        if($limitISparkedInThePast != -1 && $limitISparkedInThePast > 0){
            $sparkedInThePast = array_slice($sparkedInThePast, 0, $limitISparkedInThePast);
            $sparkedInThePast = array_combine($sparkedInThePast,$sparkedInThePast);
        }
        
	#Filter the users you are already acting on
        $remainingUsers = array_diff(array_diff($userIdsArray,$mutual_matches),$likedInThePast);
	$freshLimitSQL  = $recommendationCountForMalesSQL;
	$likeLimitSQL   = $recommendationCountForMalesLikeSQL;
        $freshLimit     = $recommendationCountForMales;
	$likeLimit      = $recommendationCountForMalesLike;

	if($gender == 'F') {
		$freshLimitSQL  = $recommendationCountForFemalesSQL;
		$likeLimitSQL   = $recommendationCountForFemalesLikeSQL;
                $freshLimit     = $recommendationCountForFemales;
		$likeLimit      = $recommendationCountForFemalesLike;
	}
        
        #Fetch the recommendation logic for the user only from subset of users
        $dummy = array(1); //\\TODO - remove the hack
        $recommended_users_overall = $this->getRecommendedUsersOnlyFromProvidedList($gender,$dummy,$likeLimitSQL,$freshLimitSQL);
        $ids_in_reco_set = array_unique($recommended_users_overall['recommendations']);
	$arr_processed = $this->fmatch_user->processIds($ids_in_reco_set);
        $all_ids = $arr_processed["all_ids"];
        $all_ids = array_intersect($all_ids, $remainingUsers);
	$likedIds = $arr_processed["like_ids"];
        $likedIds = array_intersect($likedIds, $remainingUsers);
        
        $freshIds = array_diff($all_ids, $likedIds);
	$availableIds = $arr_processed["available_ids"];
	$fresh_users = array();
	if(isset($freshIds)) {
            if($freshLimit >= 0) {
                $freshIds = array_slice($freshIds,0,$freshLimit);
            }
            $fresh_users = array_combine($freshIds, $freshIds);
	}
        
        $liked_users = array();
        if(isset($likedIds)) {
            if($likeLimit >= 0) {
                $likedIds = array_slice($likedIds,0,$likeLimit);
            }
            $liked_users = array_combine($likedIds, $likedIds);
	}

        #Now you have all the arrays, use it to fill the filter the list one by one
        #First set the necessary flags
	#Now here since original position in array is required, so in decorate instead of location in array, use the location in old array.
        $alreadyAddedUsers = array();
        $accumulatedIds = array_unique(array_merge($mutual_matches,$liked_users,$fresh_users,$likedInThePast['not_acted'],$sparkedInThePast));
        $finalUserList = array();
        if(sizeof($accumulatedIds) > 0) {
            $accumulatedIdsWithNameAndThumbnail = eventsDBO::getNameAndThumbNailForUsers($accumulatedIds,$event_id, $currentUser,$oppositeGender);
            #Now extract the ids and make it in the (,,) format
            $accumulatedArray = array();
            foreach($accumulatedIdsWithNameAndThumbnail as $key => $value)
            {
                $accumulatedArray[$value['user_id']]['thumbnail'] = $value['thumbnail'];
                $accumulatedArray[$value['user_id']]['fname'] = $value['fname'];
            }
        
            foreach($accumulatedIds as $key){
                if(isset($accumulatedArray[$key])) {
                    $finalUserList[$key]['user_id'] = $key;
                    $finalUserList[$key]['thumbnail'] = $imageurl.$accumulatedArray[$key]['thumbnail'];
                    $finalUserList[$key]['like_url'] = $this->userUtils->generateLikeLink($currentUser, $key,'0',$gender,'0');
                    $finalUserList[$key]['hide_url'] = $this->userUtils->generateHideLink($currentUser, $key,'0',$gender,'0');
                    $finalUserList[$key]['profile_url'] = $this->userUtils->generateProfileLinkForEvent($currentUser, $key,$event_id);
                    $finalUserList[$key]['message_url'] = $this->userUtils->generateMessageLink($currentUser, $key);
                    $fname =  $accumulatedArray[$key]['fname'];
                    $finalUserList[$key]['fname'] = "XXXXX";
                    $keyToSearch = (int)$key;
                    #Check if the user_id exists in the mutual_matches list don't impose any restrictions on count
                    if(isset($mutual_matches[$key]) and !isset($alreadyAddedUsers[$key])){
                        $finalUserList[$key]['fname'] = $fname;
                        $finalUserList[$key]['isMutualMatch'] = true;
                        $finalUserList[$key]['hasLiked'] = true;
                        $finalUserList[$key]['sameGender'] = false;
                        $finalUserList[$key]['likedByMe'] = true;
                        $alreadyAddedUsers[$key] = $key;
                    }elseif(isset($sparkedInThePast[$key]) and !isset($alreadyAddedUsers[$key])){
                        $finalUserList[$key]['isMutualMatch'] = false;
                        $finalUserList[$key]['hasLiked'] = false;
                        $finalUserList[$key]['sameGender'] = false;
                        $finalUserList[$key]['likedByMe'] = true;
                        $finalUserList[$key]['sparkedByMe'] = true;
                        $alreadyAddedUsers[$key] = $key;
                    }
                    #Check if the user was liked in the past but user did not act
                    elseif(isset($liked_users[$key]) and !isset($alreadyAddedUsers[$key])){
                        $finalUserList[$key]['isMutualMatch'] = false;
                        $finalUserList[$key]['hasLiked'] = true;
                        $finalUserList[$key]['sameGender'] = false;
                        $finalUserList[$key]['likedByMe'] = false;
                        #This user should be present in the recommendation set
                        #
                        $likeFlag = '1';
                        $availFlag = '0';
                        if(isset($availableIds) && isset($availableIds[$keyToSearch])) {
                                $availFlag = $availableIds[$keyToSearch];
                        }
                        #Update the Links
                        $finalUserList[$key]['like_url'] = $this->userUtils->generateLikeLink($currentUser, $key,$likeFlag,$gender,$availFlag);
                        $finalUserList[$key]['hide_url'] = $this->userUtils->generateHideLink($currentUser, $key,$likeFlag,$gender,$availFlag);
                        $alreadyAddedUsers[$key] = $key;
                    }
                    #Check if coming from recommendations ...this is only the fresh users
                    elseif(isset($fresh_users[$key]) and !isset($alreadyAddedUsers[$key])){
                        $finalUserList[$key]['isMutualMatch'] = false;
                        $finalUserList[$key]['hasLiked'] = false;
                        $finalUserList[$key]['sameGender'] = false;
                        $finalUserList[$key]['likedByMe'] = false;
                        $likeFlag = '0';
                        $availFlag = '0';
                        #Update the like/hide link
                        if(isset($liked_users[$key])) {
                                $likeFlag = '1';
                                $finalUserList[$key]['hasLiked'] = true;
                        }
                        if(isset($availableIds) && isset($availableIds[$keyToSearch])) {
                                $availFlag = $availableIds[$keyToSearch];
                        }
                        $finalUserList[$key]['like_url'] = $this->userUtils->generateLikeLink($currentUser, $key,$likeFlag,$gender,$availFlag);
                        $finalUserList[$key]['hide_url'] = $this->userUtils->generateHideLink($currentUser, $key,$likeFlag,$gender,$availFlag);
                        $finalUserList[$key]['spark_hash'] = SparkDBO::getMd5Hash($currentUser, $finalUserList[$key], "spark", null);
                        $alreadyAddedUsers[$key] = $key;
                    }
                    #Check if liked in the past but not acted upon
                    elseif(isset($likedInThePast['not_acted'][$key]) and !isset($alreadyAddedUsers[$key])){
                        $finalUserList[$key]['isMutualMatch'] = false;
                        $finalUserList[$key]['hasLiked'] = false;
                        $finalUserList[$key]['sameGender'] = false;
                        $finalUserList[$key]['likedByMe'] = true;
                        $alreadyAddedUsers[$key] = $key;
                    }
                    #As per current discussions, we do not wish to pursue this, hence commenting it out
                    else{
                    }
                }
            }
        }
       return array_values($finalUserList);
    }

    public function attachImageURL($img_array)
    {
        global $imageurl;
        $return_arr = array() ;
        foreach ($img_array as $value)
        {
            $return_arr [] = $imageurl . $value ;
        }

        return $return_arr ;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [km] (same as earthRadius)
     * All credits to: http://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
     */
    function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public function getZonesArray(){
        return curatedDatesDBO::getZonesForCity($this->location['city_id'], "event");
    }

    public function getCityName(){
        return curatedDatesDBO::getCityOrStateName($this->location['city_id']);
    }

    public function getUserEvent($user_id, $datespot_id){
        return eventsDBO::getEventFollowStatus($user_id, $datespot_id);
    }

    public function updateUserEvent($user_id, $datespot_id, $status){
        return eventsDBO::updateUserEvent($user_id, $datespot_id, $status);
    }

    public function getMutualMatchesForEvent($user_id, $event_id){
        global $imageurl;
        $mutual_matches = eventsDBO::getMutualMatchesForEvent($user_id,$event_id);
        $output = array();
        $output["count"] = sizeof($mutual_matches);
        $output["users"] = array();
        $count = 0;
        foreach($mutual_matches as $user_id){
            $thumb = $this->userUtils->getNamenPic($user_id['match_id']);
            $temp = array();
            $temp["user_id"] = $user_id['match_id'];
            $temp["image_url"] = $imageurl . $thumb['thumbnail'];
            array_push($output["users"], $temp);
            if($count == 2)
                break;

            $count++;
        }
        return $output;
    }

    public function getMutualMatchesForAllEvents($user_id){
        global $imageurl;
        $mutual_matches = eventsDBO::getMutualMatchesForAllEventsWithLimit($user_id,-1);
        $output = array();
        foreach($mutual_matches as $user_event){
            if(!isset($output[$user_event['datespot_hash_id']])){
                $output[$user_event['datespot_hash_id']] = array();
            }
            if(isset($user_event['thumbnail']) &&  $user_event['thumbnail'] != null) {
                $temp = array();
                $temp["user_id"] = $user_event['match_id'];
                $temp["image_url"] = $imageurl . $user_event['thumbnail']; // $this->userUtils->getNamenPic($user_event['match_id'])['thumbnail'];
                array_push($output[$user_event['datespot_hash_id']], $temp);
            }
        }
        return $output;
    }
}

?>
