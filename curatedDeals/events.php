<?php
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/eventCategoryClass.php";
require_once dirname ( __FILE__ ) . "/Datespot.class.php";

try
{
    $user = functionClass::getUserDetailsFromSession ();
    $user_id = $user['user_id'];
	global $redis;
    if(isset($user_id))
    {
		$eventObj = new EventCategory($user_id) ;
		if($_POST['action'] == "show_categories")
        {
            $events = $eventObj->showCategories() ;
			if($events == null)
				$events = array();
            $data = array("responseCode" => 200,
                          "categories" => $events) ;
        }
		else if($_POST['action'] == "show_events" && isset($_POST['category_id']))
		{
			$last_scene_scene = $_POST['last_seen_scene'];
			$zone_ids = null;
			if(isset($_POST['zone_ids'])){
				if(is_array($_POST['zone_ids'])){
					$zone_ids = implode(",",$_POST['zone_ids']);
				}else {
					$zone_array = explode(',', $_POST['zone_ids']);
					$zone_ids = "";
					foreach ($zone_array as $zone_id) {
						if (is_numeric($zone_id)) {
							$zone_ids .= trim($zone_id) . ",";
						}
					}
					$zone_ids = rtrim($zone_ids, ",");
				}
			}
			$followStatus = $_POST['follow_status'];
			$category_id = $_POST['category_id'];
			$scenes_ab = $_POST['scenes_ab'];
			if (strpos($category_id, ',') !== false) {
				$scenes_ab = true;
			}
			if(isset($scenes_ab) && $scenes_ab == true){
				$events = $eventObj->getEventsByCategory($category_id, $zone_ids, $followStatus, $user_id, true);
			}else {
				$events = $eventObj->getEventsByCategory($category_id, $zone_ids, $followStatus, $user_id, false);
			}
			if(isset($_POST['lat']) && isset($_POST['lon'])){
				$events = $eventObj->addGeoDistance($events,$_POST['lat'],$_POST['lon']);
			}
			$zones  = $eventObj->getZonesArray();
			$city_name = $eventObj->getCityName();
			$data = array("responseCode" => 200,
				"events" => $events, "zones" => $zones, "city_name" => $city_name) ;
		}
		else if($_POST['action'] == "event_details" && isset($_POST['datespot_id'])){

			//   $curated_obj = new CuratedDates($user_id);

			if($redis->exists("event_".$_POST['datespot_id'])){
				$datespot_details = json_decode($redis->get("event_".$_POST['datespot_id']), true);
			}else {
				$datespot_details = $eventObj->getDateSpotById($_POST['datespot_id']);
				$datespot_details['locations'] = $eventObj->getDateLocations($datespot_details['datespot_id'], null);
				$datespot_details['multiple_locations_text'] = 'Multiple Locations';
				$datespot_details['share_url'] = $short_url . "/evnt.php?e=" . $_POST['datespot_id'];
				$datespot_details['like_minded_text'] = "LIKE_COUNT others are interested.";
				$datespot_details['share_text'] = $datespot_details['name'] . " at " . $datespot_details['locations'][0]['location'] . "! \nThis looks like fun! Check this and other TM Scenes out on TrulyMadly- India's best dating app ";
				$redis->set("event_".$_POST['datespot_id'], json_encode($datespot_details), 21600);
			}


			$select_status = SelectDBO::getUserListSelectData(array($user_id));
			if(count($select_status) >0)
				$datespot_details['is_select'] = true ;
			else
				$datespot_details['is_select'] = false;
			
			$datespot_details['is_select_only'] = $datespot_details['is_trulymadly'];

			//To whom so ever it may concern.
			//below logic exists in .class file too. Since details are cached, replicating here as well.
			// Please remove below logic once app versions 435 and 440 for iOS and android have a hard updation.
			$newImage = false;
			$fnClass = new functionClass();
			$version= $fnClass->getHeader();
			$source = $fnClass->getSourceFromHeader();
			if(($source =='iOSApp' && $version >= 435) ||
				($source =='androidApp' && $version >= 440))   {
				$newImage = true;
			}
			if(strpos(strtolower($datespot_details['event_ticket_text']), 'ticket') !== false ||
				strpos(strtolower($datespot_details['event_ticket_text']), 'buy') !== false ||
				strpos(strtolower($datespot_details['event_ticket_text']), 'book') !== false ){
				$datespot_details['event_ticket_image'] = $newImage ? $baseurl."/images/events/ticket_new.png" : $baseurl."/images/events/ticket.png";
			}else{
				$datespot_details['event_ticket_image'] = $newImage ? $baseurl."/images/events/more_info_new.png" : $baseurl."/images/events/more_info.png";
			}

			$followStatus = $eventObj->getUserEvent($user_id, $_POST['datespot_id']);
			if ($followStatus != null && !empty($followStatus)) {
				if ($followStatus[0]['status'] == "follow")
					$datespot_details['follow_status'] = true;
				else
					$datespot_details['follow_status'] = false;
			} else {
				$datespot_details['follow_status'] = false;
			}

			if($datespot_details['is_trulymadly'] == true )
			{
				if($version >= 650)
				{
					if($datespot_details['is_select'] == true )
						$datespot_details['event_ticket_text'] = "Get Passes" ;
					else
						$datespot_details['event_ticket_text'] = "Join Select" ;
				}
				else
				{
					if($datespot_details['is_select'] == false)
					{
						$datespot_details['event_ticket_url'] = 'http://onelink.to/trulymadly';
						$datespot_details['event_ticket_text'] = "Update to Access" ;
					}
				}

			}

			if(!isset($_POST['attendee_list']) || $_POST['attendee_list'] == null || $_POST['attendee_list'] == true)
				$datespot_details['following_users'] = $eventObj->getUsersFollowingEvent($_POST['datespot_id'], $user_id);

			$data = array("responseCode" => 200, "event_details" => $datespot_details) ;
		}else if($_POST['action'] == "follow_event" && isset($_POST['datespot_id'])){
			$eventObj->updateUserEvent($user_id,$_POST['datespot_id'],"follow");
			$data = array("responseCode" => 200, "status" => "followed successfull") ;
		}else if($_POST['action'] == "unfollow_event" && isset($_POST['datespot_id'])){
			$eventObj->updateUserEvent($user_id,$_POST['datespot_id'],"unfollow");
			$data = array("responseCode" => 200, "status" => "unfollow successfull") ;
		}else if($_POST['action'] == "event_mutual_match" && isset($_POST['datespot_id'])){
			$userList = $eventObj->getMutualMatchesForEvent($user_id,$_POST['datespot_id']);
			$data = array("responseCode" => 200, "user_list" => $userList == null ?array() : $userList) ;
		}else if($_POST['action'] == "get_recommendations" && isset($_POST['datespot_id'])){
			$data['following_users'] = $eventObj->getUsersFollowingEvent($_POST['datespot_id'], $user_id);
		}
    }
    else
    {
        $data = array("responseCode" => 401);
    }

    print_r(json_encode($data));
}
catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_WARNING);
}

?>