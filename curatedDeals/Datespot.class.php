<?php
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../DBO/curatedDatesDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/selectDBO.php";
require_once dirname ( __FILE__ ) . "/../utilities/send_sms.php"; 
require_once dirname ( __FILE__ ) . "/curatedDatesClass.php";


class Datespot {

	private $datespot_id;
	private $datespot_data ;
	private $status;
	private $friendly_name;
	private $name;
	private $location;
	private $city_id;
	private $state_id;
	private $amount_type;
	private $amount_value;
	private $images;			//array
	private $menu_images;		//array
	private $offer;
	private $hashtag;			//array
	private $recommendation;
	private $phone_no;
	private $address;
	private $sms_msg ;
	private $sendSmsObj ;
	private $dealData ;
	private $preferredZones ;
	public $dsDetails;

	function __construct($id){
		$datespotDb = $this->getDateSpotById($id);
		$this->dsDetails = $datespotDb;
		$this->datespot_data = array();
		$this->datespot_id = $datespotDb['datespot_id'];
		$this->status = $datespotDb['status'];
		$this->friendly_name = $datespotDb['friendly_name'];
		$this->name = $datespotDb['name'];
//		$this->location = $datespotDb['location'];
		$this->city_id = $datespotDb['city_id'];
		$this->state_id = $datespotDb['state_id'];
		$this->amount_type = $datespotDb['amount_type'];
		$this->amount_value = $datespotDb['amount_value'];
		$this->images = json_decode($datespotDb['images']);			//array
		$this->menu_images = json_decode($datespotDb['menu_images']);		//array
		$this->offer = $datespotDb['offer'];
		$this->hashtag = json_decode($datespotDb['hashtag']);			//array
		$this->recommendation = $datespotDb['recommendation'];
//		$this->phone_no = $datespotDb['phone_no'];
//		$this->address = $datespotDb['address'];
		$this->sms_msg ;
		$this->sendSmsObj ;
		$this->dealData ;
		$this->preferredZones ;
		
	}
	
	function getDateSpotsByLocation($city_id, $status="active")
	{
		$dateSpotList  = CuratedDatesDBO::getDatespotByCity($city_id, $status);
		return $dateSpotList;
	}
	
	public function getDateSpotByDealId ($deal_id)
	{
		$deal = CuratedDatesDBO::getCuratedDateByAppDealId($deal_id);
		$datespot_id = $deal['datespot_id'];
		$this->datespot_data = $this->getDateSpotById($datespot_id);
		return array ('data' => $this->datespot_data,
				      'deal' => $deal 	     ) ;
		 
	}
	
	function getDateSpotById($datespot_id)
	{
		$dateSpotList  = CuratedDatesDBO::getDatespotById($datespot_id);
		$dateSpotList=$this->convertDateDataForApp($dateSpotList);
		return $dateSpotList;
	}

	function getDateLocations($datespot_id, $zone_array)
	{
		$dateSpotList  = CuratedDatesDBO::getDatespotLocations($datespot_id,$zone_array);
		return $dateSpotList;
	}

	private function convertDateDataForApp($dateSpotList){
		global $imageurl;
		$dateSpotList['communication_image'] = $imageurl. $dateSpotList['communication_image'];
		$dateSpotList['images'] = $this->attachImageURL(json_decode($dateSpotList['images']));
		$dateSpotList['menu_images'] = $this->attachImageURL(json_decode($dateSpotList['menu_images']));
		$dateSpotList['hashtags'] = json_decode($dateSpotList['hashtag']);
		$dateSpotList['terms_and_conditions'] = json_decode($dateSpotList['terms_and_conditions']);
		return $dateSpotList;
	}
	public function attachImageURL($img_array)
	{
		global $imageurl;
		$return_arr = array() ;
		foreach ($img_array as $value)
		{
			$return_arr [] = $imageurl . $value ;
		}
	
		return $return_arr ;
	}
	
	
	public function checkAndSendSMS($deal_id)
	{
		$this->dealData = CuratedDatesDBO::getSmsSentStatus($deal_id) ;
		$this->datespot_id = $this->dealData['datespot_hash_id'] ;
		if ($this->dealData['sms_user1'] != 'sent' || $this->dealData['sms_user2'] != 'sent')
		{
			if($this->datespot_data == array())
			{
				$this->datespot_data = CuratedDatesDBO::getDatespotById($this->datespot_id);
			}
			$this->sms_msg = $this->composeMessageForDatespot(); 
			$this->sendSmsObj = new SendMessage() ;
			if($this->dealData['sms_user1'] != 'sent')
				$this->sendDealSMS($this->dealData['user1']) ;
			if($this->dealData['sms_user2'] != 'sent')
				$this->sendDealSMS($this->dealData['user2']) ;
		}
	}
	
	private function sendDealSMS($user_id )
	{
		try 		{
			$phone_arr= CuratedDatesDBO::getUserNumberFromdeals($user_id) ;
 			if ($phone_arr['phone_number'] != null)
			{
 				$response = $this->sendSmsObj->sendSMS($phone_arr['phone_number'], $this->sms_msg) ;
				if($response['responseCode'] == 200)
 				{
 					if($user_id == $this->dealData['user1'])
						$user_col = "sms_user1" ;
					else 
						$user_col = "sms_user2" ;
				
					CuratedDatesDBO::setSmsSentStatus($this->dealData['deal_id'],$user_col, "sent");
					
 				}
     		}
			
			
		} catch (Exception $e) {
				trigger_error($e->getMessage(), E_USER_WARNING);
		}
	}
	
	private function composeMessageForDatespot()
	{
		$sms_text = "Enjoy your TrulyMadly date at " . $this->datespot_data['name'] ." (check app for participating locations) \nVoucher code : " .
			$this->dealData['coupon_code'] . "." ;
		if($this->datespot_data['datespot_id'] == '119_13915'){
			$sms_text = "All set to get Datelicious? Please book your table at Cafe Lota here: http://bit.ly/1mLgICr Enjoy your date!";
		}
		if($this->datespot_data['datespot_id'] == '120_21005'){
			$sms_text = "All set to get Datelicious? Please book your table at SodaBottleOpenerWala here: http://bit.ly/1mLgICr Enjoy your date!";
		}
		if($this->datespot_data['datespot_id'] == '117_57629'){
			$sms_text = "All set to get Datelicious? Please book your table at The Sassy Spoon here: http://bit.ly/1mLgNG2 Enjoy your date!";
		}
		if($this->datespot_data['datespot_id'] == '118_42149'){
			$sms_text = "All set to get Datelicious? Please book your table at Cafe Zoe here: http://bit.ly/1mLgNG2 Enjoy your date!";
		}
		if($this->datespot_data['datespot_id'] == '121_58297'){
			$sms_text = "All set to get Datelicious? Please book your table at Via Milano here: http://bit.ly/1orS0Iu Enjoy your date!";
		}
		if($this->datespot_data['datespot_id'] == '122_86937'){
			$sms_text = "All set to get Datelicious? Please book your table at Indian Kitchen here: http://bit.ly/1orS0Iu Enjoy your date!";
		}


		if($this->datespot_data['sms_text'] != null && trim($this->datespot_data['sms_text']) != ""){
			$sms_text = $this->datespot_data['sms_text'];
			$sms_text = str_replace("COUPON_CODE", $this->dealData['coupon_code'], $sms_text);
			$sms_text = str_replace("NAME", $this->datespot_data['name'], $sms_text);
			$sms_text = str_replace("PHONE", $this->datespot_data['phone_number'], $sms_text);
		}
		return $sms_text;
		//#vouchercode call $phone to make reservation";
		
	}   
	
}
