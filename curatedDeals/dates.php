<?php
require_once dirname ( __FILE__ ) . "/curatedDatesClass.php";
require_once dirname ( __FILE__ ) . "/Datespot.class.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/RecommendationSystem/UtilsDate.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";


$user = functionClass::getUserDetailsFromSession ();
$user_id = $user['user_id'];

try {
	
if (isset($user_id))
{
	$deals = new CuratedDates($user_id) ;
	if ( $_REQUEST['action'] == 'get_deals' && isset($_REQUEST['match_id']) && $_REQUEST['match_id'] != null )
	{	
		global $redis;
		$test=NULL;
		$city_id = UtilsDate::getCurrentCityUserData($user_id);
		$datespot_key = Utils::$redis_keys['datespot'] ."_". $user_id ."_". $_REQUEST['match_id'];
		$datespot_key_zones = Utils::$redis_keys['datespot_zones'] ."_". $user_id ."_". $_REQUEST['match_id'];
		$datespot_key_city = Utils::$redis_keys['datespot_city'] ."_". $user_id ."_". $_REQUEST['match_id'] ."_".$city_id;
		// Store the Datespot list in redis for 1 hour	
		if($redis->sIsMember('curated_deal_users',$user_id))
			$test='test';
		if($redis->EXISTS($datespot_key) & $redis->EXISTS($datespot_key_city)){
			$dealsJSON = $redis->GET($datespot_key);
			$output['dates'] = json_decode($dealsJSON,true);
			$dealsZonesJSON = $redis->GET($datespot_key_zones);
			$output['zones'] = json_decode($dealsZonesJSON,true);
			$dealsCityJSON = $redis->GET($datespot_key_city);
			$output['city_name'] = json_decode($dealsCityJSON,true);
		}else{
         	$output['dates']  = $deals->listDealsForUser($test,$_REQUEST['match_id']);
         	$output['dates']  = UtilsDate::datespotsActionLog($output['dates'],$user_id,$_REQUEST['match_id']);         	
         	$output['zones']  = $deals->getZonesArray();
         	$output['city_name']  = $deals->getCityName();
			// Cache data in redis         	
         	$datespot_cache_data = json_encode($output['dates']);
         	$datespot_cache_zones_data = json_encode($output['zones']);
         	$datespot_cache_city_data = json_encode($output['city_name']);
         	// For redis, used http://stackoverflow.com/questions/22232443/in-redis-how-would-you-update-a-key-and-reset-the-original-ttl
         	$redis->SETEX($datespot_key,UtilsDate::$redisTTL,$datespot_cache_data);
         	$redis->SETEX($datespot_key_zones,UtilsDate::$redisTTL,$datespot_cache_zones_data);
         	$redis->SETEX($datespot_key_city,UtilsDate::$redisTTL,$datespot_cache_city_data);
		}
			$output["responseCode"] = 200;
	}		
		
	 
	else if ($_REQUEST['action'] == 'save_preference')
	{
		$city_id = UtilsDate::getCurrentCityUserData($user_id);
		$datespot_key = Utils::$redis_keys['datespot'] ."_". $user_id ."_". $_REQUEST['match_id'];
		$datespot_key_zones = Utils::$redis_keys['datespot_zones'] ."_". $user_id ."_". $_REQUEST['match_id'];
		$datespot_key_city = Utils::$redis_keys['datespot_city'] ."_". $user_id ."_". $_REQUEST['match_id'] ."_".$city_id;
		//If exist in Redis, else fresh call
		if($redis->EXISTS($datespot_key) & $redis->EXISTS($datespot_key_city)){		
			$dealsJSON = $redis->GET($datespot_key);
			$dealsArray = json_decode($dealsJSON,true);
			$dealsZonesJSON = $redis->GET($datespot_key_zones);
			$zonesArray = json_decode($dealsZonesJSON,true);
			$dealsCityJSON = $redis->GET($datespot_key_city);
			$output['city_name'] = json_decode($dealsCityJSON,true);
 		}else{
			$dealsArray  = $deals->listDealsForUser($test,$_REQUEST['match_id']);
			$zonesArray  = $deals->getZonesArray();
			$output['city_name']  = $deals->getCityName();
		}
		// Get the deals stored in redis
		// Filter by zones or by coordinates
		if(isset($_REQUEST['zones']) && $_REQUEST['zones'] != null){
			$zone_arr =  $_REQUEST['zones'];
			$deals->savePreferredZones($zone_arr);
			if(count($zone_arr) == 1 && $zone_arr[0] == -1) {
				$output['dates'] = $dealsArray;
			}else {
				$output['dates']  = UtilsDate::filterDatespotByZone($user_id,$_REQUEST['match_id'],$dealsArray,$zone_arr);
			
			// Output if the zone is selected
				foreach ($zonesArray as $key=>$value){
					if(in_array($zonesArray[$key]['zone_id'],$_REQUEST['zones'])){
						$zonesArray[$key]['selected']=true;
					}
				}
			}	
		} else if(isset($_REQUEST['lat']) && isset($_REQUEST['lon']) && $_REQUEST['lat'] != null && $_REQUEST['lon'] != null){
			$deals->saveNearbyCoordinates($_REQUEST['lat'],$_REQUEST['lon']);
			$output['dates']  = UtilsDate::filterDatespotByDistance($user_id,$_REQUEST['match_id'],$dealsArray,$_REQUEST['lat'],$_REQUEST['lon']);
		} else {
			$output['dates'] = $dealsArray;
		}
		$output["zones"] = $zonesArray;
		$output["responseCode"] = 200;
	} 
	else if ($_REQUEST['action'] == 'save_number' && isset($_REQUEST['phone_number']) and $_REQUEST['phone_number'] != null )
 	{

 		$phone_number = $_REQUEST['phone_number'] ;
 		if (is_numeric($phone_number) && strlen($phone_number) == 10)
 		{
 			$deals->savePhoneNumberforDeals($phone_number) ;
 			if (isset($_REQUEST['deal_id']) && $_REQUEST['deal_id'] != null)
 			{
 				$dateSpotObj = new Datespot() ;
 				$dateSpotObj->checkAndSendSMS($_REQUEST['deal_id']) ;
 			}
 			$output["responseCode"] = 200;	
 		} 
 		else 
 		{
 			$output["responseCode"] = 403;
 			$output["error"] = "InValid Phone Number";
 		}
 	     

 	}
	else   
	{
		$output["responseCode"] = 403;
		$output["error"] = "UnknownRequest";
		
	}
}else 
{
	$output["responseCode"] = 401;
}



print_r(json_encode($output));

} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
}

?>
