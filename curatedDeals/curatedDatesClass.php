<?php 
require_once dirname ( __FILE__ ) . "/../DBO/curatedDatesDBO.php";
foreach( glob(dirname( __FILE__ ) . "/RecommendationSystem/*.php") as $selector ) {
    require_once( $selector );
}

/*
 * class for all the actions pertaining to curated deals 
 * Author :: Sumit
 * 🖖 Live Long and Prosper.
 */

class CuratedDates 
{
	private $user_id ;
	private $last_deal_location ;
	private $location ;
	private $dealList ;
	private $ncr_city_array;
	private $zone_array;
	private $city_name ;
	
	function __construct($user_id) 
	{  
		$this->user_id = $user_id ;
		$this->last_deal_location = array();
		$this->location = array() ;
		$this->dealList = array() ;
		$this->ncr_city_array = array(16743,47965,47964,47968,47967,47966);
		$this->zone_array = array();
		$this->city_name ;
	}
	
	public function listDealsForUser($test=NULL,$match_id=NULL) 
	
		{
				
		$this->getLocationOfUser() ;

		$this->getlastDealsLocation() ;

		$this->getDeals($test);

		
		if (count($this->dealList)>0)
		{   
			
			// deals are available in the present user location  
			if( $this->location['city_id'] != $this->last_deal_location['city_id'] || $this->location['state_id'] != $this->last_deal_location['state_id'])
			{  
				CuratedDatesDBO::setCurrentLocation($this->user_id, $this->location['city_id'], $this->location['state_id']);
				$this->last_deal_location['zones'] = null;
			}
		} 
		else
		{
			$this->location['city_id'] = $this->last_deal_location['city_id'] ;
			$this->location['state_id'] = $this->last_deal_location['state_id'] ;
			$this->getDeals($test) ;  
		}
		$this->setCityName();
		$this->setZonesArray(); 
		// Rank Recommendations based on the logic of popularity and historical preferences.
		if(isset($match_id) && count($this->dealList)>0){
			$recommender = new DateliciousRecommendation($this->user_id,$match_id,$this->dealList);
			$this->dealList = $recommender->generateRankedDealsForPair();
		}
		return $this->dealList ;
	}

	private function setZonesArray(){
		$this->zone_array = CuratedDatesDBO::getZonesForCity($this->location['city_id'], "deal");
		$selected_zone_array =  explode( ",",$this->last_deal_location['zones']);

		if($selected_zone_array != null && count($selected_zone_array) > 0) {
			foreach ($this->zone_array as $key => $zone) {
				if (in_array($zone['zone_id'],$selected_zone_array)){
					$this->zone_array[$key]['selected'] = true;
				}else
					$this->zone_array[$key]['selected'] = false;
			}
		}
	}
	private function setCityName()
	{
		$this->city_name = CuratedDatesDBO::getCityOrStateName($this->location['city_id']);
	}
	
	public function getCityName()
	{
		return $this->city_name ;
	}

	public function getZonesArray(){
		return $this->zone_array;
	}

	private function getLocationOfUser() 
	{
		$this->location = CuratedDatesDBO::getUserLocation($this->user_id);	
	}
	
	private function getlastDealsLocation()
	{
		$this->last_deal_location = CuratedDatesDBO::currentLocation($this->user_id);
	}
	
	public function getDeals($test=NULL) 
	{
		global $imageurl ;
		if(in_array ($this->location['city_id'], $this->ncr_city_array)){
			$this->location['state_id']=2168;
		}
// 		if( $this->location['city_id'] != $this->last_deal_location['city_id'] || $this->location['state_id'] != $this->last_deal_location['state_id']) {
			$this->dealList = CuratedDatesDBO::allActiveDealsForLocation($this->location['city_id'], $this->location['state_id'], null, $test);
// 		}else{
// 			$this->dealList = CuratedDatesDBO::allActiveDealsForLocation($this->location['city_id'], $this->location['state_id'], $this->last_deal_location['zones'], $test);
// 			$this->dealList = CuratedDatesDBO::allActiveDealsForLocation($this->location['city_id'], $this->location['state_id'], $this->last_deal_location['zones'], $test);
				
// 		}
		foreach ($this->dealList as $key=> $value)
		{
			$this->dealList [$key] ['hashtags'] = json_decode ( $this->dealList [$key] ['hashtags'] );
			$this->dealList [$key] ['image'] = $imageurl . $this->dealList [$key] ['image'];
		} 
	}
	
	public function isUserNumberPresent() 
	{
		$phone_array = array() ;
		$deal_phone_number = CuratedDatesDBO::getUserNumberFromdeals($this->user_id) ;
		
		if (strlen($deal_phone_number['phone_number']) == 10)
		{
			$phone_array['get_number'] = false ;
		}
		else
		{
			$tb_phone = CuratedDatesDBO::getUserNumberFromTB($this->user_id) ;
			$phone_array['get_number'] = true ;
			$phone_array['existing_no'] = $tb_phone['phone_number'] ;
		}
		return $phone_array ;
	}
	
	public function savePhoneNumberforDeals($user_number) 
	{
		 CuratedDatesDBO::saveUserNumberForDeals( $this->user_id, $user_number) ;
	}
	
	public function savePreferredZones($zones)
	{
		if(count($zones) == 1 && $zones[0] == -1) {
			return;
		}else {
			$temp = implode(",", $zones) ;
			$zoneArray = array('type'=>'zones','value'=>$temp);
			CuratedDatesDBO::updatePreferredZone($this->user_id, $zoneArray) ;
		}    
	}
	
	public function saveNearbyCoordinates($lat,$lon){
		$temp = implode(',',array($lat,$lon));
		$geo = array('type'=>'geo','value'=>$temp);
		CuratedDatesDBO::updatePreferredZone($this->user_id, $geo) ;
		
	}
	public function getPreferredZones() 
	{
		$this->getlastDealsLocation();
		if($this->last_deal_location['zones'] == null)
		{
			$zone_ids = NULL ;
		}
		else
		{
			$zone_ids = explode(',', $this->last_deal_location['zones']);
		}
		return $zone_ids ;
	}
}



?>
