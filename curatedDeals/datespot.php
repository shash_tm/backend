<?php
require_once dirname ( __FILE__ ) . "/Datespot.class.php";
require_once dirname ( __FILE__ ) . "/curatedDatesClass.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";


$user = functionClass::getUserDetailsFromSession ();
$user_id = $user['user_id'];
try {
	
if (isset($user_id))
{	
	if(isset($_REQUEST['deal_id']) && isset($_REQUEST['match_id']))
	{
		$datespot = new Datespot() ;
	//	$curated_obj = new CuratedDates($user_id) ;
		$zones_array = null;
		$data = $datespot->getDateSpotByDealId($_REQUEST['deal_id']);
		$deal = $data['deal'];
		$datespot_details = $data['data'] ;
		$datespot_details['locations'] = $datespot->getDateLocations($datespot_details['datespot_id'],$zones_array);
		$datespot_details['multiple_locations_text'] = 'Multiple Locations';
		$deal_data = array();
		$phone_array = array("get_number" => false ,
				  			 "existing_no" => null ) ;
	
		//var_dump($deal); 
		if ($deal['status'] == 'waiting')
		{
			if($user_id == $deal['user_id'] && $_REQUEST['match_id'] == $deal['match_id'])
			{
			    	$deal_data['deal_status'] = 'awaiting_response' ;
			    	$deal_data['deal_text'] = "Awaiting Response" ;
			} 
			else if ( $user_id == $deal['match_id'] && $_REQUEST['match_id'] == $deal['user_id'])
		    {	
			// send different response based on platform  i.e. Let's Go for IOS and Sounds Good for android
// 				$functionObj = new functionClass();
// 				$source= $functionObj->getSourceFromHeader();
				$deal_data['deal_status'] = 'lets_go' ; 
// 				if ($source == 'iOSApp')
// 					$deal_data['deal_text'] = "Let's Go!";
// 				else 	
				$deal_data['deal_text'] = "Get Voucher!";
			}
		} else if ($deal['status'] == 'accepted')
		{
			$curated_obj = new CuratedDates($user_id) ;
			$deal_data['deal_status'] = "voucher" ;
			$deal_data['deal_text'] = $deal['coupon_code']; 
			$datespot->checkAndSendSMS($_REQUEST['deal_id']) ;
			$phone_array = $curated_obj->isUserNumberPresent();
			
		}
		$output["responseCode"] = 200;
		$output["data"] = $datespot_details ;
		$output["deal_details"] = $deal_data ;
		$output['phone_details'] = $phone_array ;
		
	
	} else if (isset($_REQUEST['datespot_id']) && isset($_REQUEST['match_id']))
	{  
	    $datespot = new Datespot() ;
	 //   $curated_obj = new CuratedDates($user_id);
	    $zones_array = null ;

	    $datespot_details = $datespot->getDateSpotById($_REQUEST['datespot_id']);
		$datespot_details['locations'] = $datespot->getDateLocations($datespot_details['datespot_id'],$zones_array);
		$datespot_details['multiple_locations_text'] = 'Multiple Locations';

	    $deal_details = array();
	    
	    $match_data = new UserData($_REQUEST['match_id']);
	    $gender= $match_data->fetchGender(); 
	    
	    if($gender =='M')
	    {
	    	$deal_details['deal_status'] = "ask_him" ;
	    	//$deal_details['deal_text'] = "Ask Him" ;
	    }	
	    else 
	    {
	    	$deal_details['deal_status'] = "ask_her" ;
	    	//$deal_details['deal_text'] = "Ask Her" ;
	    }
		$deal_details['deal_text'] = "Let's Go!" ;
	    	
	    
	    $output["responseCode"] = 200;
	    $output["data"] = $datespot_details;
	    $output["deal_details"]= $deal_details ;
	}
	else{
		$output["responseCode"] = 403;
		$output["error"] = "UnknownRequest";		
	}
} else 
{
	$output["responseCode"] = 401;
}



print_r(json_encode($output));

} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
}

/*{
	"responseCode": 200,
	"data": {
		"datespot_id": "121325_trsjk",
		"friendly_name": "Date of the year",
		"name": "La meilleure cuisine dans le monde entier,",
		"location": "VASANT VIHAR, NEW DELHI",
		"amount_type": "price",
		"amount_value": "2000",
		"is_inclusive" : "(all price inclusive)",
		"is_per_couple": "Per Couple", 
		"images": [
			"https://s3-ap-southeast-1.amazonaws.com/trulymadlytestbucketnew/files/images/profiles/1449834649107.4_225232259836047872.jpg",
			"https://s3-ap-southeast-1.amazonaws.com/trulymadlytestbucketnew/files/images/profiles/1449834649107.4_225232259836047872.jpg",
			"https://s3-ap-southeast-1.amazonaws.com/trulymadlytestbucketnew/files/images/profiles/1449834649107.4_225232259836047872.jpg"
		],
		"hashtags": [
			"#casualDating",
			"#terrace",
			"#mediterranean",
			"#awesomeness"
		],
		"offer": "2 Glasses of Wine,  Parmigiana,  Florentine, Pizza , Coconut Puff with vanilla ice cream",
		"menu_images": [
			"https://s3-ap-southeast-1.amazonaws.com/trulymadlytestbucketnew/files/images/profiles/1448626819031.2_503314150031656000.jpg",
			"https://s3-ap-southeast-1.amazonaws.com/trulymadlytestbucketnew/files/images/profiles/1448626632741.7_741417599841952384.jpg"
		],
		"recommendation": "Miss TM Recommends the Florentine Pizza",
		"phone_no": "9650376719",
		"address": "1180 Chhatta Madan Gopal, Paranthe wali Gali, Chandni Chowk, Delhi 110006",
		"terms_and_conditions": [
			"This is just a dummy API",
			"This is only to show the schema of the Json",
			"Any Key if not present should be assumed null and not displayed in the UI",
			"Please give your suggestions"
		],
		"deal_status": "awaiting_response"
	}
	

}*/

?>
