<?php
require_once dirname(__FILE__) . "/../../logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/../../DBO/curatedDatesDBO.php";

class UtilsDate{
	public static $locationScoreFactor      = 30;
	public static $femaleLocationVal        = 2;
	public static $maleLocationVal          = 1;
	public static $popularityScoreFactor    = 1;
	public static $femalePopularityVal      = 2;
	public static $malePopularityVal        = 1;
	public static $nearbyRadius             = 10;
	public static $femaleAgeIntervalString  = '18,22,26,31'; 
	// Interval formed will be [x,y): for age 18, interval would contain 18,19,20,21 
	public static $maleAgeIntervalString    = '18,22,26,31';
	public static $globalPopularityConstant = 25;
	public static $lifeNewDatespot 			= 3; //Days till which datespot would be categorised as new (global value)
	public static $redisTTL					= '900'; //15 mins (15*60)
	/**
	 * Function sorts on a single column in descending order and deals with decimal also
	 */
    public function sortByKey($array) {
//  	For integer values - fast    	
// 		usort($array, function($a, $b) {
// 			return $b['totalScore'] - $a['totalScore'];
// 		});		
			usort($array, function($a, $b) {
				$result = 0;
				if ($a['totalScore'] > $b['totalScore']) {
					$result = -1;
				} else if ($a['totalScore'] < $b['totalScore']) {
					$result = 1;
				}
				return $result;
			});
	    return $array;
	}
	/**
	 * Function to change position of datespots on the basis of some constraints. 
	 * Example to shift the boosted datespot at 1 and new datespot at 2 index. (Array starting 0 index)
	 */
    public function changePosition($array,$column,$index){
    	$i=0;
    	$change=false;
    	foreach ($array as $key => $value) {
    		if($i>=$index && $array[$key][$column]>0){
    			 break;
    		}
    		$i=$i+1;
    	}
    	if($i>$index && $i<count($array)){
    		$temp = $array[$i];
    		//Remove first then add. Otherwise key values will change. 
    		array_splice($array,$i,1);
    		array_splice($array, $index, 0, array($temp));
    		$change=true;
    	}
    	return array($array,$change);
    }
    /**
     * Calculate the mean and variance of array
     */
    public function findVariance($aValues, $bSample = false){
    	if($aValues==NULL){
    		return 0;
    	}
    	$fMean = array_sum($aValues) / count($aValues);
    	$fVariance = 0.0;
    	foreach ($aValues as $i){
    		$fVariance += pow($i - $fMean, 2);
    	}
    	$fVariance /=count($aValues);
//     	$fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
//     	return array((float) $fVariance,(float) $fMean);
		return (float) $fVariance;
    }
    /**
     * Function to get Geo Distance
     *  Official Web site: http://www.geodatasource.com 
     */
    public function getGeoDistance($lat1, $lon1, $lat2, $lon2) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515 * 1.609344;
// 		$miles = $dist * 60 * 1.1515;
// 		$unit = strtoupper($unit);
// 	  	if ($unit == "K") {
// 	    	return ($miles * 1.609344);
// 	  	} else if ($unit == "N") {
// 	      	return ($miles * 0.8684);
// 	    } else {
// 	        return $miles;
// 	    }
		return $miles;
	}
	/**
	 * Function to filter datespots by distance
	 */
	public function filterDatespotByDistance($user_id,$match_id,$dealList,$latNearby,$lonNearby){
		$nearbyJSON = implode(',',array($latNearby,$lonNearby)); ;
		$data[] = array("user_id" =>$user_id,
				"activity" => "datespots" , "event_type" => "NearbyFilter",
				"event_status"=> $match_id,
				"source" => "backend",
				"event_info" => $nearbyJSON);
		UtilsDate::dumpInActionLog($data);
		foreach($dealList as $key => $value){
			if ($dealList[$key]['location_count']<2){
				$dealList[$key]['dist'] = UtilsDate::supporterFunctionForDistance ($dealList[$key]['geo_location'],
																				   $latNearby,$lonNearby);
			} else{
				$datespotLocation = curatedDatesDBO::getDatespotLocationData($dealList[$key]['datespot_id']);
				$minDist = 1000;
				$minGeoLocation = $dealList[$key]['geo_location'];
				$minLocation = $dealList[$key]['location'];
				$minZone = $dealList[$key]['zone_id'];
				foreach ($datespotLocation as $keyDS =>$valueDS){
					$eachDistance=UtilsDate::supporterFunctionForDistance ($datespotLocation[$keyDS]['geo_location'],
																		   $latNearby,$lonNearby);
					if($eachDistance<$minDist){
						$minDist = $eachDistance;
						$minGeoLocation = $datespotLocation[$keyDS]['geo_location'];
						$minLocation = $datespotLocation[$keyDS]['location'];
						$minZone = $datespotLocation[$keyDS]['zone_id'];
					}
				}
				$dealList[$key]['geo_location'] = $minGeoLocation;
				$dealList[$key]['location']=$minLocation;
				$dealList[$key]['zone_id']=$minZone;
				$dealList[$key]['dist']=$minDist;
			}
			$dealList[$key]['unit'] = 'km';
			if($dealList[$key]['dist']>UtilsDate::$nearbyRadius){
				unset($dealList[$key]);
			}
		}
		//Sort by distance now
		usort($dealList, function($a, $b) {
			$result = 0;
			if ($a['dist'] < $b['dist']) {
				$result = -1;
			} else if ($a['dist'] > $b['dist']) {
				$result = 1;
			}
			return $result;
		});
		$dealList=UtilsDate::datespotsActionLog($dealList,$user_id,$match_id,$flagVal=2);
		return array_values($dealList);
	}
	/**
	 *  To handle case of multiple locations 
	 */
	public function supporterFunctionForDistance ($geo_location,$latNearby,$lonNearby){
		if ($geo_location==NULL){return 1000;}
		$dealCoord = explode(',',$geo_location);
		$dist = UtilsDate::getGeoDistance($dealCoord[0],$dealCoord[1],$latNearby,$lonNearby);
		return number_format((float)$dist, 2, '.', '');
	}
	/**
	 * Function to filter datespots by zones. Deals with multiple locations as well.
	 */
	public function filterDatespotByZone($user_id,$match_id,$dealList,$zones){
		foreach($dealList as $key => $value){
			if(!in_array($dealList[$key]['zone_id'],$zones)){
				// Check if multiple locations exist for datespot
				if($dealList[$key]['location_count']<2){
					unset($dealList[$key]);
				}else{
					$datespotLocation = curatedDatesDBO::getDatespotLocationData($dealList[$key]['datespot_id']);
					$zonesDatespot = array_column($datespotLocation,'zone_id');
					$intersect = array_intersect($zonesDatespot,$zones);
					// No intersection.
					if(count($intersect)==0){
						unset($dealList[$key]);
					} else {
						$index = array_search($intersect[0],$zonesDatespot);
						$dealList[$key]['zone_id'] = $datespotLocation[$index]['zone_id'];
						$dealList[$key]['location'] = $datespotLocation[$index]['location'];
						$dealList[$key]['geo_location'] = $datespotLocation[$index]['geo_location'];
					}
				}
			}
		}
		//Log datespots and zones
		$zonesJSON = implode(",", $zones) ;
		$data[] = array("user_id" =>$user_id,
				"activity" => "datespots" , "event_type" => "ZonesFilter",
				"event_status"=> $match_id,
				"source" => "backend",
				"event_info" => $zonesJSON);
		UtilsDate::dumpInActionLog($data);
		$dealList=UtilsDate::datespotsActionLog($dealList,$user_id,$match_id,$flagVal=1);
		
		return array_values($dealList);
	}
	
	/**
	 * Function to prepare JSON for dumping in action log. Takes FlagValue as 0,1,2 
	 * to identify filtered or unfiltered datespots as the event type
	 */
	
	public function datespotsActionLog($dateList,$user_id,$match_id,$flagVal=0){
		$dumpData = array();
		foreach ($dateList as $key=>$value) {
//			Which practice to use?
// 			$dumpData[]= array($dateList[$key]['datespot_id'],
// 					$dateList[$key]['pricing'],
// 					$dateList[$key]['zone_id'],
// 					$dateList[$key]['geo_location'],
// 					$dateList[$key]['rank'],
// 					$dateList[$key]['isNew'],
// 					$dateList[$key]['locationScore'],
// 					$dateList[$key]['popularityScore'],
// 					$dateList[$key]['totalScore'],
// 					$dateList[$key]['isSpecial']
// 			);
			// Convert isNew flag to boolean from binary
			// Commented on request by Udhbav
// 			if($dateList[$key]['isNew']==0){
// 				$dateList[$key]['isNew']=false;
// 			} else if($dateList[$key]['isNew']==1){
// 				$dateList[$key]['isNew']=true;
// 			}
			$dumpData[$key]['datespot_id'] = $dateList[$key]['datespot_id'];
			$dumpData[$key]['pricing'] = $dateList[$key]['pricing'];
			$dumpData[$key]['zone_id'] = $dateList[$key]['zone_id'];
			$dumpData[$key]['geo_location'] = $dateList[$key]['geo_location'];
			$dumpData[$key]['rank'] = $dateList[$key]['rank'];
			$dumpData[$key]['isNew'] = $dateList[$key]['isNew'];
			$dumpData[$key]['locationScore'] = $dateList[$key]['locationScore'];
			$dumpData[$key]['popularityScore'] = $dateList[$key]['popularityScore'];
			$dumpData[$key]['totalScore'] = $dateList[$key]['totalScore'];
			$dumpData[$key]['special_code'] = $dateList[$key]['special_code'];
		}
		$dateListJSON = json_encode($dumpData);
		if($flagVal==1){
			$event_type = "FilteredDatespotsZone";
		}elseif($flagVal==2){
			$event_type = "FilteredDatespotsNearby";
		}
		else{
			$event_type = "UnfilteredDatespots";
		}
		$data[] = array("user_id" =>$user_id,
				"activity" => "datespots" , "event_type" => $event_type,
				"event_status"=> $match_id,
				"source" => "backend", 
				"event_info" => $dateListJSON);
		UtilsDate::dumpInActionLog($data);
		return $dateList;
	}

	/**
	 * Function calls the event tracking class and dumps the data
	 */
	public function dumpInActionLog($data){
		$eventObject = new EventTrackingClass();
 		$eventObject->logActions($data);
	}
	
	/**
	 * Public function used to get the current city of the user. (Used in dates.php for caching)
	 */
	public function getCurrentCityUserData($user_id){
		$location = curatedDatesDBO::getUserLocation($user_id);
		return $location['city_id'];
	}
	
}
?>
