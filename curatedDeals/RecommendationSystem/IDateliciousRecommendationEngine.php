<?php
/**
 * Interface for the Datelicious Recommendation Engine
 */

interface IDateliciousRecommendationEngine{
	public function generateRankedDealsForPair();
}

?>