<?php

require_once dirname(__FILE__) . "/BaseDateliciousRecommendation.php";
require_once dirname(__FILE__) . "/UtilsDate.php";
require_once dirname(__FILE__) . "/../../DBO/curatedDatesDBO.php";
require_once dirname(__FILE__) . "/../../abstraction/query_wrapper.php";

/**
 * Description of the base class
 */
class DateliciousRecommendation extends BaseDateliciousRecommendation {

    function __construct($user1, $user2, $dealList) {
        parent::__construct($user1, $user2, $dealList);
    }

    /**
     * Wrapper to rank the recommendations
     */
    public function generateRank($dateList) {
		$unsortedDateList = $dateList;
		foreach ($unsortedDateList as $key => $value) {
			// If datespot is of multiple locations, use zone and location with maximum score
			if($unsortedDateList [$key]['location_count']<2){	
	    		$unsortedDateList [$key]['locationScore'] = $this->generateLocationScore($unsortedDateList [$key] ['zone_id'], 
	    																				 $unsortedDateList [$key] ['geo_location']);
			}
			else{
				list($unsortedDateList [$key]['locationScore'],
					 $unsortedDateList [$key]['zone_id'],
					 $unsortedDateList [$key]['geo_location'],
					 $unsortedDateList [$key]['location'])	= $this->wrapperForLocationScore($unsortedDateList [$key] ['datespot_id'],
					 																		 $unsortedDateList [$key] ['zone_id'], 
	    																				 	 $unsortedDateList [$key] ['geo_location'],
					 																		 $unsortedDateList [$key] ['location']);
			}
	    	$unsortedDateList [$key]['popularityScore'] = $this->generatePopularityScore($unsortedDateList [$key] ['datespot_id'],$unsortedDateList [$key] ['city_id']);
			$unsortedDateList [$key] ['totalScore'] = UtilsDate::$locationScoreFactor*$unsortedDateList [$key]['locationScore'] + 
													  UtilsDate::$popularityScoreFactor*$unsortedDateList [$key]['popularityScore'];
			$unsortedDateList [$key] ['special_code'] = 0;
// 			if($unsortedDateList[$key]['isNew']==0){
// 				$unsortedDateList [$key] ['isNew'] = false;
// 			} else {
// 				$unsortedDateList [$key] ['isNew'] = true;
// 			}
					
		}
		return $unsortedDateList;
	
    }

}

?>

