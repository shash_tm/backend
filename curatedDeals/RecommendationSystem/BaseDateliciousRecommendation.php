<?php

require_once dirname(__FILE__) . "/IDateliciousRecommendationEngine.php";
require_once dirname(__FILE__) . "/UtilsDate.php";
require_once dirname(__FILE__) . "/../../DBO/curatedDatesDBO.php";
require_once dirname(__FILE__) . "/../../abstraction/query_wrapper.php";



/**
 * Description of the base class
 */
abstract class BaseDateliciousRecommendation implements IDateliciousRecommendationEngine {

    private $userMale;
    private $userFemale;
    private $dealList;
    private $genderFlag = array();
    private $countPrefMale;
    private $countZonesMale;
    private $countGeoMale;
    private $countPrefFemale;
    private $countZonesFemale;
    private $countGeoFemale;
    private $zonesMale = array();
    private $zonesFemale = array();
    private $geoMale = array();
    private $geoFemale = array();
    private $ageMale;
    private $ageFemale;
    private $popularityArray;
    

    function __construct($user1, $user2, $dealList) {
        //Assign userMale and userFemale by checking gender
        $this->genderFlag = $this->getGender($user1);
        if ($this->genderFlag['gender'] == 'F') {
            $this->userMale = $user2;
            $this->userFemale = $user1;
        } else {
            $this->userMale = $user1;
            $this->userFemale = $user2;
        }
        $this->dealList = $dealList;
    }

    /**
     * Wrapper to score the recommendations
     */
    abstract function generateRank($dateList);

    /**
     * Public Class to generate ranked Datelicious Recommendations for user pair
     */
    public function generateRankedDealsForPair() {
    	//Compute age intervals for both male and females which are used to get popularity of datespots.
    	$this->ageMale = $this->getAge($this->userMale);
    	$this->ageFemale = $this->getAge($this->userFemale);
    	//Columns that would be used for popularity of datespots.
    	$femaleSql = $this->getAgeSqlString($this->ageFemale,'f',UtilsDate::$femaleAgeIntervalString);
    	$maleSql = $this->getAgeSqlString($this->ageMale,'m',UtilsDate::$maleAgeIntervalString);    	 
    	//Get the popularity array for all the datespots.
    	$this->popularityArray = $this->getPopularity(array_column($this->dealList,'datespot_id'),$femaleSql,$maleSql);
    	//Fetch JSON object of historical location preferences of both male and female
    	$userFemalePref = $this->fetchLocationPreferences($this->userFemale);
    	$userMalePref = $this->fetchLocationPreferences($this->userMale);
    	// To parse JSON object and compute frequency of preferred zones.
       	list($this->zonesMale, 
       		 $this->geoMale,
       		 $this->countPrefMale,
       		 $this->countZonesMale,
       		 $this->countGeoMale) = $this->computePreferencesFreq($userMalePref['zones']);
    	list($this->zonesFemale,
    		 $this->geoFemale,
    		 $this->countPrefFemale,
    		 $this->countZonesFemale,
    		 $this->countGeoFemale) = $this->computePreferencesFreq($userFemalePref['zones']);
		// Main Ranking Function. 
    	$dateList = $this->generateRank($this->dealList);
		// Sort List on rank.    	
    	$dateList = $this->sortList($dateList);
		// First item as Miss TM recommended.
    	$dateList[0]['special_code'] = 1;
    	//Change position for New Datespots
    	list($dateList,$change) = UtilsDate::changePosition($dateList,'isNew',$index=1);
    	if($change==true){$dateList[1]['special_code'] = 2;}
    	//Change position for Boosted Datespots
    	list($dateList,$change) = UtilsDate::changePosition($dateList,'rank',$index=2);
    	if($change==true){$dateList[2]['special_code'] = 3;}
    	return $dateList;
    }
    
    /**
     * Function to get gender of the user
     */
    public function getGender($user_id) {
    	$genderQuery = "select u.user_id,u.gender from user u where u.user_id =?";
    	$genderUser = Query_Wrapper::SELECT($genderQuery, array($user_id), Query::$__slave, "true");
    	return $genderUser;
    }
    
    /**
     * For sorting
     */
    protected function sortList($dateList) {
    	$dateList1 = UtilsDate::sortByKey($dateList);
    	return $dateList1;
    }
    /**
     * To get JSON object of historical preference of the user
     */
    protected function fetchLocationPreferences($user_id) {
        $prefQuery = "select udis.user_id,udis.zones from user_deal_icon_shown udis where udis.user_id =?";
        $prefUser = Query_Wrapper::SELECT($prefQuery, array($user_id), Query::$__slave, "true");
        return $prefUser;
    }
    /**
     * Parse JSON object and compute frequency of each zone in the historical preferences of user
     */
    protected function computePreferencesFreq($preferences) {
    	$pref = json_decode($preferences,true);
		$countPref = count($pref);
		// To form separate arrays of zones and geo-coordinates of the preferences. 
		$zoneArray = array();
		$geoArray = array();
		$countZones=0;
		foreach($pref as $key=>$value){
			if($pref[$key]['type']=='zones'){
				$temp = explode(",",$pref[$key]['value']);
				$zoneArray = array_merge($zoneArray,$temp);
				$countZones+=1;
			}else if($pref[$key]['type']=='geo'){
				$geoArray = array_merge($geoArray,array($pref[$key]['value']));
			}
		}
		$zonesFreq = array_count_values($zoneArray);
		$countGeo = count($geoArray);		
		return array($zonesFreq, $geoArray,$countPref,$countZones,$countGeo);
    }
    /**
     * Function to handle multiple locations of datespots. Returns location with maximum score
     */
    protected function wrapperForLocationScore($hash_id,$currentZone,$currentGeoLocation,$currentLocation){
    	$datespotLocation = curatedDatesDBO::getDatespotLocationData($hash_id);
    	$maxScore = 0;
    	foreach ($datespotLocation as $key => $value) {
    		$eachLocationScore = $this->generateLocationScore($datespotLocation[$key]['zone_id'],
    														  $datespotLocation[$key]['geo_location']);
    		if($eachLocationScore>$maxScore){
    			$maxScore = $eachLocationScore;
    			$currentZone = $datespotLocation[$key]['zone_id'];
    			$currentGeoLocation = $datespotLocation[$key]['geo_location'];
    			$currentLocation = $datespotLocation[$key]['location'];
    		}
    	}
    	return array($maxScore,$currentZone,$currentGeoLocation,$currentLocation);
    }

    /**
     * Function to generate Location Score for each datespot.
     * Location Score of datespot = a*LocationScore for Female + b*Location Score for Male
     * a =2, b=1
     * Location Score for user = w*(zonesScore) + x*(geoScore)
     * zonesScore = (number of times user selects datespot's zone)/(no of times user set zone preferences)
     * geoScore = (number of times user datespot appeared in the previous Nearby)/(no of times user set nearby preferences)
     *
     * w=(no of times user set zone preferences)/(no.of times user set preferences)
     * x=(no of times user set Nearby preferences)/(no.of times user set preferences)
     *
     */
    protected function generateLocationScore($dateZone,$dateGeo) {
    	$zonesMaleVal = 0;
    	$zonesFemaleVal = 0;
    	$geoMaleVal = 0;
    	$geoFemaleVal = 0;
    	// Get location score for zones for male and female
    	if($this->countPrefMale>0){
	    	if(array_key_exists($dateZone,$this->zonesMale)){
	    		$zonesMaleVal = $this->zonesMale[$dateZone];
	    	}
	    	// For male get location score for Nearby preferences
	    	if ($this->countGeoMale>0) {
	    		$geoDateSpot = explode(",",$dateGeo);
	    		foreach ($this->geoMale as $key => $value) {
	    			$prefCord = explode(",",$this->geoMale[$key]);
	    			$dist = UtilsDate::getGeoDistance($geoDateSpot[0],$geoDateSpot[1],$prefCord[0],$prefCord[1]);
	    			if($dist < UtilsDate::$nearbyRadius){
	    				$geoMaleVal = $geoMaleVal+1;
	    			}
	    		}
	    	}
	    	// Weighted location score for zones and Nearby (Male)
	    	$zoneScoreMale = ($this->countZonesMale/$this->countPrefMale)*($zonesMaleVal/$this->countZonesMale);
	    	$coordScoreMale = ($this->countGeoMale/$this->countPrefMale)*($geoMaleVal/$this->countGeoMale);
	    	$locationScoreMale = $zoneScoreMale + $coordScoreMale;	    	
    	}
    	else{
    		$locationScoreMale=0;
    	}
    	if($this->countPrefFemale>0){
       		if (array_key_exists($dateZone,$this->zonesFemale)){
    			$zonesFemaleVal = $this->zonesFemale[$dateZone];
    		}
    		// For female get location score for Nearby preferences
    		if ($this->countGeoFemale>0) {
    			$geoDateSpot = explode(",",$dateGeo);
    			foreach ($this->geoFemale as $key => $value) {
    				$prefCord = explode(",",$this->geoFemale[$key]);
    				$dist = UtilsDate::getGeoDistance($geoDateSpot[0],$geoDateSpot[1],$prefCord[0],$prefCord[1]);
    				if($dist < UtilsDate::$nearbyRadius){
    					$geoFemaleVal = $geoFemaleVal+1;
    				}
    			}
    		}
	    	// Weighted location score for zones and Nearby (Female)
    		$zoneScoreFemale = ($this->countZonesFemale/$this->countPrefFemale)*($zonesFemaleVal/$this->countZonesFemale);
    		$coordScoreFemale = ($this->countGeoFemale/$this->countPrefFemale)*($geoFemaleVal/$this->countGeoFemale);
    		$locationScoreFemale = $zoneScoreFemale + $coordScoreFemale;
       	}
    	else{
    		$locationScoreFemale=0;
    	}    	
    	// Weighted location score for male and female
    	$locationScore = UtilsDate::$maleLocationVal*$locationScoreMale +
    					 UtilsDate::$femaleLocationVal*$locationScoreFemale;
    	return $locationScore;
    }
    
    /**
     * Function to get age of the user
     */
    public function getAge($user_id) {
    	$ageQuery = "select ud.user_id,floor(datediff(now(),ud.DateOfBirth)/365.25) as age from user_data ud where ud.user_id =?";
    	$ageUser = Query_Wrapper::SELECT($ageQuery, array($user_id), Query::$__slave, "true");
    	$ageVal = $ageUser['age'];
    	return $ageVal;
    }
    /**
     * Returns sql string to form intervals of age (Example:fAge1+fAge2)
     */
    public function getAgeSqlString($age,$gender,$ageIntervalString) {
    	//$ageIntervalString of the form '18,22,26,31'
    	//$age>30 , clubbed in 30Plus
    	$sqlAgeString = $gender;
    	if ($age>30){
    		$sqlAgeString .="30Plus";
    	} else{
    		$ageIntervalArray=explode(',',$ageIntervalString);
    		$i=0;
    		foreach($ageIntervalArray as $key => $value){
    			if ($age<$value){
    				break;
    			}
    			$i +=1;
    		}
    		// To get intervals of the form [x,y)
    		$ageArray=range($ageIntervalArray[$i-1],$ageIntervalArray[$i]-1);
    		// We need sum of all ages in that age group
    		$implodeString = '+'.$gender;
    		$a = implode($implodeString,$ageArray);
    		$sqlAgeString .= $a ;
    	}
    	return $sqlAgeString;
    }

    /**
     * Get popularity of the venue among age group of both male and female from DB
     */
    protected function getPopularity($datespots,$femaleSql,$maleSql){
    	//popularity of venue among male and female of age group
    	$array_count = Query_Wrapper::getParamCount($datespots);
    	$popularityQuery = "select hash_id," . $femaleSql . " as female, fTotal, ";
    	$popularityQuery .= $maleSql . " as male, mTotal ";
    	$popularityQuery .= "from datespot_popularity where hash_id in ($array_count)";
    	$popularity = Query_Wrapper::SELECT($popularityQuery, $datespots, Query::$__slave, "true");
    	$femaleAgeSum = array_sum(array_column($popularity,'female'));
    	$femaleGlobalSum = array_sum(array_column($popularity,'fTotal'));
    	$maleAgeSum = array_sum(array_column($popularity,'male'));
    	$maleGlobalSum = array_sum(array_column($popularity,'mTotal'));
    	foreach ($popularity as $key => $value){
    		$popularity[$key]['female'] = $popularity[$key]['female']/ $femaleAgeSum;
    		$popularity[$key]['fTotal'] = $popularity[$key]['fTotal']/ $femaleGlobalSum;
    		$popularity[$key]['male'] = $popularity[$key]['male']/ $maleAgeSum;
    		$popularity[$key]['mTotal'] = $popularity[$key]['mTotal']/ $maleGlobalSum;
    	}
    	return $popularity;
    }
    
    /**
     * Gets k-factor which acts as weightage between global popularity and age based popularity
     */
    protected function getPopularityWeights($col1,$col2){
    	$userAgeVariance = UtilsDate::findVariance(array_column($this->popularityArray,$col1));
    	$userGlobalVariance = UtilsDate::findVariance(array_column($this->popularityArray,$col2));
    	if($userGlobalVariance==0){
    		$k=UtilsDate::$globalPopularityConstant;
    	} else{
    		$k= $userAgeVariance/$userGlobalVariance;
    	}
    	return $k;
    }
    
    /**
     * Function to generate Popularity Score for each datespot.
     *
     * Popularity of datespot = a*Popularity among female of age interval x + b*Popularity among male of age interval y
     * a=2,b=1 : More preference to female
     *
     * Popularity for user = (P(venue in age group)+ P(global)*k ) / (k+1)
     * k = var(venue|age)/var(global)
     */
    protected function generatePopularityScore($datespot_id,$city_id) {
    	$key = array_search($datespot_id,array_column($this->popularityArray,'hash_id'));
    	if($key == NULL){
    		return 0;
    	}
    	 
    	//$k = UtilsDate::$globalPopularityConstant;
    	$kFemale = $this->getPopularityWeights('female','fTotal');
    	$kMale = $this->getPopularityWeights('male','mTotal');
    	//Get popularity value for female and male
    	$femalePopularity = ($this->popularityArray[$key]['fTotal']*$kFemale+
    						$this->popularityArray[$key]['female'])/($kFemale+1);
    	$malePopularity = ($this->popularityArray[$key]['mTotal']*$kMale+
    					  $this->popularityArray[$key]['male'])/($kMale+1);
    
    	return UtilsDate::$malePopularityVal*$malePopularity +
    		   UtilsDate::$femalePopularityVal*$femalePopularity;
    }
    
}

?>

