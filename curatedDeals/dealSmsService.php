<?php 
require_once dirname ( __FILE__ ) . "/Datespot.class.php";
require_once dirname ( __FILE__ ) . "/dealManagerClass.php";

$output= array();
if($_REQUEST['action'] == "send_deal_sms")
{
	if(isset($_REQUEST['deal_id']) && $_REQUEST['deal_id'] !=null)
	{
		$dateSpotObj = new Datespot() ;
		$dateSpotObj->checkAndSendSMS($_REQUEST['deal_id']) ;
		$output['responseCode'] = 200 ;
	}	
}else if($_REQUEST['action'] == "deactivate")
{
	if(isset($_REQUEST['deal_id']) && $_REQUEST['deal_id'] !=null)
	{
		$manager = new DealsManager() ;
		$manager->changeStatus($_REQUEST['deal_id'], 'inactive');
		$output['responseCode'] = 200 ;
	}
}
else
{
	$output['responseCode'] = 403 ;
	$output['error'] = "UnknownRequest" ;
}

print_r(json_encode($output)) ;
?>