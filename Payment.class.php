<?php 

require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/TrustBuilder.class.php";

class Payment extends TMObject{
	private $userId;


	function __construct($user_id=NULL) {
		parent::__construct ( $user_id );
		$this->userId = $user_id;
	}


	public function requesttrnxId($planId,$user){
		global $config,$baseurl;
		try{
			$array['udf1'] =  bin2hex(openssl_random_pseudo_bytes(64));
			$this->conn->Execute($this->conn->Prepare("INSERT INTO user_payment_log (user_id,plan_id,email,phone,secure_key) VALUES (?,?,?,?,?)"),array($this->userId,$planId,$user['email'],$user['phone'],$array['udf1']));
			$trnxId = $this->conn->insert_id();
				
			$rs = $this->conn->Execute($this->conn->Prepare("SELECT * from payment_plan where plan_id = ?"),array($planId));
			$row = $rs->FetchRow();
			$array['key'] = $config['payment']['merchant_key'];
			$array['amount'] = $row['cost'];
			$array['txnid'] = $trnxId;
			$array['productinfo']= $planId;
			$array['firstname']= $user['name'];
			$array['email']= $user['email'];
			$array['phone']= $user['phone'];
			$array['surl']= $baseurl.'/paymentresponse.php';
			$array['furl']= $baseurl.'/paymentresponse.php';
// 			$array['drop_category'] = 'COD';
			$array['hash'] = hash('sha512', $array['key'].'|'.$array['txnid'].'|'.$array['amount'].'|'.$array['productinfo'].'|'.$array['firstname'].'|'.$array['email'].'|'.$array['udf1'].'||||||||||'.$config['payment']['salt']);
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$config['payment']['curl_url']);
			curl_setopt($ch, CURLOPT_POST, 1);

			// in real life you should use something like:
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			http_build_query($array));

			// receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec ($ch);
			curl_close ($ch);
			
			$this->conn->Execute($this->conn->Prepare('insert into payu_tnxid_log values (?,?)'),array($trnxId,$server_output));

// 			$this->conn->Execute($this->conn->Prepare("update user_payment_log set payu_trxn_id = ?,secure_key = ? where trnx_id = ?"),array($server_output,$array['udf1'],$trnxId));

			return $server_output;
		}catch (Exception $e){
			trigger_error($e->getMessage(),E_USER_WARNING);
		}
	}

	/**
	 * if trn_id and secrue key matches -> just save
	 * any error-> trigger error with data
	 * @param unknown $data
	 * @return string|boolean
	 */
	public function successTransaction($data){
		try{
			$rs = $this->conn->Execute($this->conn->Prepare("SELECT * from user_payment_log where trnx_id = ? and secure_key = ?"),array($data['txnid'],$data['udf1']));
			if($rs->RowCount () == 1){
				$row = $rs->fetchRow();
					$rs2 = $this->conn->Execute($this->conn->Prepare("SELECT * from payment_plan where plan_id = ?"),array($data['productinfo']));
					$row2 = $rs2->fetchRow();
					$this->conn->Execute($this->conn->Prepare("insert into user_payment_callback (trnx_id,user_id,response,status,plan_id) values (?,?,?,?,?)"),
							array($data['txnid'],$this->userId,json_encode($data),'success',$data['productinfo']));
					$trustBuilder = new TrustBuilder($this->userId);
					$trustBuilder->creditPoint($row2['credits'],'credit_card',$row2['expiry_days']);
					$this->conn->Execute($this->conn->Prepare("update user set plan = ? where user_id = ?"),array($row2['plan_type'],$this->userId));
					//$this->conn->Execute($this->conn->Prepare("update user_payment_log set status = 'success', response_time = now() where user_id = ? and trnx_id = ?"),array($this->userId,$data['txnid']));
					return  "Success";
			}else{
				trigger_error("Payment Success error :".json_encode($data));
				return "Some error occured. We'll look into this.";
			}
		}catch(Exception $e){
			trigger_error("Payment success error:".json_encode($data));
			return "Request Already handled";
		}
		return false;
	}
	
	public function handleTransaction($data){
		try{
			$rs = $this->conn->Execute($this->conn->Prepare("SELECT user_id from user_payment_log where trnx_id = ? and secure_key = ?"),array($data['txnid'],$data['udf1']));
			if($rs->RowCount () == 1){
				$this->conn->Execute($this->conn->Prepare("insert into user_payment_callback (trnx_id,user_id,response,status,plan_id) values (?,?,?,?,?)"),
						array($data['txnid'],$this->userId,json_encode($data),$data['status'],$data['productinfo']));				
				if(strcasecmp($data["status"],'success')  == 0){
					$rs2 = $this->conn->Execute($this->conn->Prepare("SELECT * from payment_plan where plan_id = ?"),array($data['productinfo']));
					$row2 = $rs2->fetchRow();
					$trustBuilder = new TrustBuilder($this->userId);
					$trustBuilder->creditPoint($row2['credits'],'credit_card',$row2['expiry_days']);
					$this->conn->Execute($this->conn->Prepare("update user set plan = ? where user_id = ?"),array($row2['plan_type'],$this->userId));
				}
				if(strcasecmp($data["status"],'success')== 0){
					return  "Success";
				}elseif(strcasecmp($data["status"],'failure')== 0){
					return  "Failure";
				}else{
					return "Pending";
				}
			}else{
				trigger_error("Payment error :".json_encode($data));
				return "Some error occured. We'll look into this.";
			}
		}catch(Exception $e){
			trigger_error("Payment error:".json_encode($data));
			return "Request Already handled";
		}
		return false;
	}
	
	public function failTransaction($data){
		try{
			$rs = $this->conn->Execute($this->conn->Prepare("SELECT * from user_payment_log where trnx_id = ? and secure_key = ?"),array($data['txnid'],$data['udf1']));
			if($rs->RowCount () == 1){
				$row = $rs->fetchRow();
				$this->conn->Execute($this->conn->Prepare("insert into user_payment_callback (trnx_id,user_id,response,status,plan_id) values (?,?,?,?,?)"),
						array($data['txnid'],$this->userId,json_encode($data),'fail',$data['productinfo']));
				return  "Payment Transaction failed";
			}else{
				trigger_error("Payment Failure error:".json_encode($data));
				return "Some error occured. We'll look into this.";
			}
		}catch(Exception $e){
			trigger_error("Payment Failure error:".json_encode($data));
			return "Request Already handled";
		}
		return false;
	}
	
	public function pendingTransaction($data){
		try{
			$rs = $this->conn->Execute($this->conn->Prepare("SELECT * from user_payment_log where trnx_id = ? and secure_key = ?"),array($data['txnid'],$data['udf1']));
			if($rs->RowCount () == 1){
				$row = $rs->fetchRow();
				$this->conn->Execute($this->conn->Prepare("insert into user_payment_callback (trnx_id,user_id,response,status,plan_id) values (?,?,?,?,?)"),
						array($data['txnid'],$this->userId,json_encode($data),'pending',$data['productinfo']));
				return  "Payment Transaction pending";
			}else{
				trigger_error("Payment pending error:".json_encode($data));
				return "Some error occured. We'll look into this.";
			}
		}catch(Exception $e){
			trigger_error("Payment pending error:".json_encode($data));
			return "Request Already handled";
		}
		return false;
	}
	
	public function logError($data){
		try{
			$this->conn->Execute($this->conn->Prepare("insert into user_payment_callback (trnx_id,user_id,response,status,plan_id) values (?,?,?,?,?)"),
					array($data['txnid'],$this->userId,json_encode($data),'error',$data['productinfo']));
// 			$this->conn->Execute($this->conn->Prepare("insert into user_payment_callback (user_id,response) values (?,?)"),array($this->userId,json_encode($data)));
		}catch (Exception $e){
			trigger_error($e->getMessage());
		}
	}
	
/*	public function logError($data){
		try{
			$this->conn->Execute($this->conn->Prepare("insert into user_payment_errorlog (user_id,response) values (?,?)"),array($this->userId,json_encode($data)));
		}catch (Exception $e){
			trigger_error($e->getMessage());
		}
	} */
	
	public function getUserDetails(){
		$rs = $this->conn->Execute($this->conn->Prepare("SELECT fname,email_id,phone_number as phone from user where user_id = ?"),array($this->userId));
		$user = $rs->fetchRow();
		$rs = $this->conn->Execute($this->conn->Prepare("SELECT user_number from user_phone_number where user_id = ? and status = 'active'"),array($this->userId));
		if($rs->RowCount() == 1){
			$mobile = $rs->fetchRow();
			$user['phone'] = $mobile['user_number'];
		}
		return $user;		
	}
	
	public function logUserData($data,$trnx_id){
		try{
		     $this->conn->Execute($this->conn->Prepare("insert into user_data_log (user_id,phone,email,trxn_id) values (?,?,?,?)"),array($this->userId,$data['phone'],$data['email'],$trnx_id));
		}catch (Exception $e){
			trigger_error($e->getMessage());
		}
	}
	
	public function getUserloggedData(){
		$rs = $this->conn->Execute($this->conn->Prepare("select phone,email from user_data_log where user_id = ? order by time desc limit 0,1"),array($this->userId));
		if($rs->RowCount()==1){
			$row = $rs->fetchRow();
		}else{
			$row['phone'] = "";
			$row['email'] = "";
		}
		return $row;
	}
	
	public function getPaymentPlans($admin_plan = null ){
		if(isset($admin_plan))
		$sql = "Select * from payment_plan where status='active' order by plan_id desc";
		else 
		$sql = "Select * from payment_plan where status='active'  and description != 'admin plan' order by plan_id desc";
		$rs = $this->conn->Execute($sql);
		if($rs->RowCount()>0){
			return $rs->getRows();
		}else{
			return null;
		}
	}

}


?>