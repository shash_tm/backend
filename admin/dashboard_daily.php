<?php 
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../TMObject.php";

class dashboard extends TMObject{
	private $userIdList;
	private $no_of_active_users_m = array();
	private $new_user_m = array();
	private $new_user_f = array();
	private $total_user_m = array();
	private $authentic_user_m = array();
	private $recommendation_m = array();
	private $recommendation_f = array();
	private $like_sent_m = array();
	private $like_sent_f = array();
	private $like_accepted_m = array();
	private $like_accepted_f = array();
	private $credit_consumed_m = array();
	private $credit_consumed_f = array();
	private $message_accepted_m = array();
	private $message_accepted_f = array();
	private $message_sent_m = array();
	private $message_sent_f = array();
	private $active_recomm_m = array();
	private $active_recomm_f = array();
	private $dates;
	
	function __construct($user_id=null) {
		parent::__construct ( $user_id );
	}
	
	function getDates(){
		$sql = "select date(DATE_SUB(NOW(),INTERVAL 7 day)) as day7, date(DATE_SUB(NOW(),INTERVAL 6 day)) as day6, date(DATE_SUB(NOW(),INTERVAL 5 day)) as day5, date(DATE_SUB(NOW(),INTERVAL 4 day)) as day4, date(DATE_SUB(NOW(),INTERVAL 3 day)) as day3, date(DATE_SUB(NOW(),INTERVAL 2 day)) as day2, date(DATE_SUB(NOW(),INTERVAL 1 day)) as day1";
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$dates = $res->GetRows();
			$this->dates = $dates;
			//var_dump($this->dates);
		}
	}
	function getRecommendations($s){
		$recomm_m = array();
		$recomm_f = array();
		$k = 0; 
		/*$sql = " select date(timestamp) as date, gender, count(distinct(user1)) as no_of_users_got_recommendation from user_recommendation,user where user1=user_id and email_id not regexp 'trulymadly' and  date(timestamp)>=date(DATE_SUB(NOW(),INTERVAL 7 day)) group by date(timestamp),gender";
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0)
		{
				$nameRows = $res->GetRows();
				$this->recommendation_m = $nameRows;
				//var_dump($this->recommendation_m);
		}*/
		if($s=='m'){
			for($i=7;$i>=1;$i--){
				$sql = "select date(timestamp) as date, gender, count(distinct(user1)) as no_of_users_got_recommendation from user_recommendation,user where user1=user_id and email_id not regexp 'trulymadly' and  date(timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender='M'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						$recomm_m[$k] = $e['no_of_users_got_recommendation'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->recommendation_m = $recomm_m;
		}
		else{
			for($i=7;$i>=1;$i--){
				$sql = "select date(timestamp) as date, gender, count(distinct(user1)) as no_of_users_got_recommendation from user_recommendation,user where user1=user_id and email_id not regexp 'trulymadly' and  date(timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender='F'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						$recomm_f[$k] = $e['no_of_users_got_recommendation'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->recommendation_f = $recomm_f;
		}
		
	}
	
	function newUser($s){
		$newUser_m = array();
		$newUser_f = array();
		$k = 0;
		if($s=='m'){
			for($i=7;$i>=1;$i--){
				$sql = "select date(registered_at) as date,count(*) as new_user from user where date(registered_at)=date(DATE_SUB(NOW(),INTERVAL $i day)) and email_id NOT regexp 'trulymadly' and status<>'incomplete' and gender='M'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						$newUser_m[$k] = $e['new_user'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;		
				}
			}
			$this->new_user_m = $newUser_m;
		}
		else{
			for($i=7;$i>=1;$i--){
				$sql = "select date(registered_at) as date,count(*) as new_user from user where date(registered_at)=date(DATE_SUB(NOW(),INTERVAL $i day)) and email_id NOT regexp 'trulymadly' and status<>'incomplete' and gender='F'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						$newUser_f[$k] = $e['new_user'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->new_user_f = $newUser_f;
	   }
		/*$sql = "select date(registered_at) as date, gender, count(*) as new_user from user where date(registered_at)>=date(DATE_SUB(NOW(),INTERVAL 7 day)) and email_id NOT regexp 'trulymadly' and status<>'incomplete' group by date(registered_at),gender";
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$newUser = $res->GetRows();
			$this->new_user_m = $newUser;
			//var_dump($this->new_user_m);
		}*/
	}
	
	function totalUser(){
		$totalUser = array();
		$sql = "select gender,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 7 day)),1,0)) as total_user_1,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 6 day)),1,0)) as total_user_2,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 5 day)),1,0)) as total_user_3,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 4 day)),1,0)) as total_user_4,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 3 day)),1,0)) as total_user_5,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 2 day)),1,0)) as total_user_6,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 1 day)),1,0)) as total_user_7 from user where email_id NOT regexp 'trulymadly' and status<>'incomplete' group by gender";
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$totalUser = $res->GetRows();
			$this->total_user_m = $totalUser;
			//var_dump($this->total_user_m);
		}
	}
	
	function authenticUser(){
		$authenticUser = array();
		$sql = "select gender,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 7 day)),1,0)) as authentic_user_1,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 6 day)),1,0)) as authentic_user_2,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 5 day)),1,0)) as authentic_user_3,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 4 day)),1,0)) as authentic_user_4,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 3 day)),1,0)) as authentic_user_5,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 2 day)),1,0)) as authentic_user_6,sum(if(date(registered_at)<=date(DATE_SUB(NOW(),INTERVAL 1 day)),1,0)) as authentic_user_7 from user where status='authentic'and email_id NOT regexp 'trulymadly' group by gender";
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$authenticUser = $res->GetRows();
			$this->authentic_user_m = $authenticUser;
			//var_dump($this->authentic_user_m);
		}
	}
	
	function getActive(){
		$activeUser = array();
		$query = "select gender,sum(if(DATEDIFF(DATE_SUB(NOW(),INTERVAL 7 day),last_login)<=10,1,0)) as active_user_1,sum(if(DATEDIFF(DATE_SUB(NOW(),INTERVAL 6 day),last_login)<=10,1,0)) as active_user_2,sum(if(DATEDIFF(DATE_SUB(NOW(),INTERVAL 5 day),last_login)<=10,1,0)) as active_user_3,sum(if(DATEDIFF(DATE_SUB(NOW(),INTERVAL 4 day),last_login)<=10,1,0)) as active_user_4,sum(if(DATEDIFF(DATE_SUB(NOW(),INTERVAL 3 day),last_login)<=10,1,0)) as active_user_5,sum(if(DATEDIFF(DATE_SUB(NOW(),INTERVAL 2 day),last_login)<=10,1,0)) as active_user_6,sum(if(DATEDIFF(DATE_SUB(NOW(),INTERVAL 1 day),last_login)<=10,1,0)) as active_user_7 from user where email_id NOT regexp 'trulymadly' group by gender";
		$result = $this->conn->Execute($query);
		if($result->RowCount()>0){
			$activeUser = $result->GetRows();
			$this->no_of_active_users_m = $activeUser;
			//var_dump($this->no_of_active_users_m);
		}
	}
	function noRecommendation(){
		$attributes = array();
		/*$sql = "select date(registered_at) as date,sum(if(user1 is NULL,1,0)) as no_Matches from user LEFT JOIN user_recommendation on user_id=user1 where user_id in ($this->userIdList) group by date(registered_at)";
		echo $sql;
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0)
		{
			$nameRows = $res->GetRows();
			var_dump($nameRows);
			/*for($i=0;$i<count($nameRows);$i++){
				$attributes[] = array("date"=>$nameRows[$i]['date'],"matches"=>$nameRows[$i]['no_Matches'],"active_users"=>$this->no_of_active_users[$i]['active_users']);
			}*/
		//}
		//var_dump($attributes);
		
		
		
		return $attributes;
	}
	
	function likeSent($s){
		$like_sent_m = array();
		$like_sent_f = array();
		$k = 0;
		/*$sql = "select date(timestamp) as date, gender, count(*) as like_sent from user_like,user where user1=user_id and email_id NOT regexp 'trulymadly' and date(timestamp)>=date(DATE_SUB(NOW(),INTERVAL 7 day)) group by date(timestamp),gender";
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$nameRows = $res->GEtRows();
			$this->like_sent_m = $nameRows;
			//var_dump($this->like_sent_m);			
		}*/
		if($s=='m'){
			for($i=7;$i>=1;$i--){
				$sql = "select count(*) as like_sent from user_like,user where user1=user_id and email_id NOT regexp 'trulymadly' and date(timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender = 'M'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
						//	$credit_m[$k] = 0;
						//}else
							$like_sent_m[$k] = $e['like_sent'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->like_sent_m = $like_sent_m;
		}
		else{
			for($i=7;$i>=1;$i--){
				$sql = "select count(*) as like_sent from user_like,user where user1=user_id and email_id NOT regexp 'trulymadly' and date(timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender = 'F'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
							//$credit_f[$k] = 0;
						//}else
							$like_sent_f[$k] = $e['like_sent'];
					}
					$k++;
				}
			}
			$this->like_sent_f = $like_sent_f;
		}
		
	}
	
	function likeAccepted($s){
		$like_acc_m = array();
		$like_acc_f = array();
		$k = 0;
		/*
		$sql = "select date(t.timestamp), u.gender,count(*) as like_accepted from (select ul1.user1,date(ul1.timestamp) as timestamp from user_like ul1 join user_like ul2 on ul1.user1=ul2.user2 and ul1.user2=ul2.user1 where  ul1.timestamp>ul2.timestamp )t join user u on t.user1=u.user_id where u.email_id not regexp 'trulymadly.com'and date(t.timestamp)>=DATE_SUB(NOW(),INTERVAL 7 day) group by u.gender,date(t.timestamp)";
		$res =  $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$nameRows = $res->GetRows();
			$this->like_accepted_m = $nameRows;
			//var_dump($this->like_accepted_m);
		}	
		*/
		
		
		if($s=='m'){
			for($i=7;$i>=1;$i--){
				$sql = "select count(*) as like_accepted from (select ul1.user1,date(ul1.timestamp) as timestamp from user_like ul1 join user_like ul2 on ul1.user1=ul2.user2 and ul1.user2=ul2.user1 where  ul1.timestamp>ul2.timestamp )t join user u on t.user1=u.user_id where u.email_id not regexp 'trulymadly.com'and date(t.timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and u.gender='M'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
						//	$credit_m[$k] = 0;
						//}else
						$like_acc_m[$k] = $e['like_accepted'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->like_accepted_m = $like_acc_m;
		}
		else{
			for($i=7;$i>=1;$i--){
				$sql = "select count(*) as like_accepted from (select ul1.user1,date(ul1.timestamp) as timestamp from user_like ul1 join user_like ul2 on ul1.user1=ul2.user2 and ul1.user2=ul2.user1 where  ul1.timestamp>ul2.timestamp )t join user u on t.user1=u.user_id where u.email_id not regexp 'trulymadly.com'and date(t.timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and u.gender='F'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
						//$credit_f[$k] = 0;
						//}else
						$like_acc_f[$k] = $e['like_accepted'];
					}
					$k++;
				}
			}
			$this->like_accepted_f = $like_acc_f;
		}
	}
	
	function messageSent($s){
		/*$sql = "select date(tStamp) as date, gender,count(*) as message_sent from current_messages_new,user where sender_id=user_id and email_id NOT regexp 'trulymadly' and date(tStamp)>=date(DATE_SUB(NOW(),INTERVAL 7 day)) group by date(tStamp),gender";
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$nameRows = $res->GEtRows();
			$this->message_sent_m = $nameRows;
			//var_dump($this->message_sent_m);
		}*/
		$message_sent_m = array();
		$message_sent_f = array();
		$k = 0;
		
		if($s=='m'){
			for($i=7;$i>=1;$i--){
				$sql = "select date(tStamp) as date, gender,count(*) as message_sent from current_messages_new,user where sender_id=user_id and email_id NOT regexp 'trulymadly' and date(tStamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender='M'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
						//	$credit_m[$k] = 0;
						//}else
						$message_sent_m[$k] = $e['message_sent'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->message_sent_m = $message_sent_m;
		}
		else{
			for($i=7;$i>=1;$i--){
				$sql = "select date(tStamp) as date, gender,count(*) as message_sent from current_messages_new,user where sender_id=user_id and email_id NOT regexp 'trulymadly' and date(tStamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender='F'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
						//$credit_f[$k] = 0;
						//}else
						$message_sent_f[$k] = $e['message_sent'];
					}
					$k++;
				}
			}
			$this->message_sent_f = $message_sent_f;
		}
	}
	
	function messageAccepted($s){
		//if($s=='m')
		//	$sql = "select date(z.timestamp) as date,u.gender,count(*) from (select t.s2 as user1,t.r2,t.t2 as timestamp from(select a.sender_id as s1,a.receiver_id as r1,a.tStamp as t1,b.sender_id as s2,b.receiver_id as r2,b.tStamp as t2 from current_messages_new a join current_messages_new b on a.sender_id=b.receiver_id and b.sender_id=a.receiver_id where b.tStamp>a.tStamp)t group by t.s2,t.r2,t.t2) z join user u on z.user1=u.user_id and date(z.timestamp)>=date(DATE_SUB(NOW(),INTERVAL 7 day)) and u.email_id not regexp 'trulymadly' group by u.gender,date(z.timestamp)";
		//else 
			//$sql = "select date(tStamp) as date,count(*) as messages_accepted from (select sender_id,receiver_id ,max(tStamp) as tStamp from (select greatest(sender_id,receiver_id) as sender_id ,least(sender_id,receiver_id) as receiver_id ,tStamp from current_messages_new)as t group by sender_id,receiver_id having count(*)=2) as r,user where sender_id=user_id and email_id NOT regexp 'trulymadly' and gender='F' and date(tStamp)>=date(DATE_SUB(NOW(),INTERVAL 7 day)) group by date(tStamp)";
	/*		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0){
			$nameRows = $res->GEtRows();
			//if($s=='m'){
				$this->message_accepted_m = $nameRows;
				//echo "for males";
				//var_dump($this->message_accepted_m);
		}
		*/
		
		$message_acc_m = array();
		$message_acc_f = array();
		$k = 0; 
		
		if($s=='m'){
			for($i=7;$i>=1;$i--){
				$sql = "select count(*) as message_accepted from (select t.s2 as user1,t.r2,t.t2 as timestamp from(select a.sender_id as s1,a.receiver_id as r1,a.tStamp as t1,b.sender_id as s2,b.receiver_id as r2,b.tStamp as t2 from current_messages_new a join current_messages_new b on a.sender_id=b.receiver_id and b.sender_id=a.receiver_id where b.tStamp>a.tStamp)t group by t.s2,t.r2,t.t2) z join user u on z.user1=u.user_id and date(z.timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and u.email_id not regexp 'trulymadly' and u.gender='M'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
						//	$credit_m[$k] = 0;
						//}else
						$message_acc_m[$k] = $e['message_accepted'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->message_accepted_m = $message_acc_m;
		}
		else{
			for($i=7;$i>=1;$i--){
				$sql = "select count(*) as message_accepted from (select t.s2 as user1,t.r2,t.t2 as timestamp from(select a.sender_id as s1,a.receiver_id as r1,a.tStamp as t1,b.sender_id as s2,b.receiver_id as r2,b.tStamp as t2 from current_messages_new a join current_messages_new b on a.sender_id=b.receiver_id and b.sender_id=a.receiver_id where b.tStamp>a.tStamp)t group by t.s2,t.r2,t.t2) z join user u on z.user1=u.user_id and date(z.timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and u.email_id not regexp 'trulymadly' and u.gender='F'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						//if($e['credits_consumed'] == null){
						//	$credit_m[$k] = 0;
						//}else
						$message_acc_f[$k] = $e['message_accepted'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->message_accepted_f = $message_acc_f;
		}
		
	}
	
	function creditsConsumed($s){
		$credit_m  = array();
		$credit_f  = array();
		$k = 0;
		if($s=='m'){
			for($i=7;$i>=1;$i--){
				$sql = "select date(tstamp) as date,sum(credits_consumed) as credits_consumed from user_credit_consumption as a,user as b where a.user_id=b.user_id and b.email_id NOT regexp 'trulymadly' and date(tstamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender='M'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						if($e['credits_consumed'] == null){
							$credit_m[$k] = 0;
						}else
							$credit_m[$k] = $e['credits_consumed'];//$e['no_of_active_users_got_recommendation'];
					}
					$k++;
				}
			}
			$this->credit_consumed_m = $credit_m;
		}
		else{
			for($i=7;$i>=1;$i--){
				$sql = "select date(tstamp) as date, gender, sum(credits_consumed) as credits_consumed from user_credit_consumption as a,user as b where a.user_id=b.user_id and b.email_id NOT regexp 'trulymadly' and date(tstamp)=date(DATE_SUB(NOW(),INTERVAL $i day)) and gender='F'";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					//$nameRow = $result->GetRow();
					foreach ($res as $r=>$e){
						if($e['credits_consumed'] == null){
							$credit_f[$k] = 0;
						}else
							$credit_f[$k] = $e['credits_consumed'];
					}
					$k++;
				}
			}
			$this->credit_consumed_f = $credit_f;
		}
	}
	
function getActiveRecommendation($s){
		$userIds = array();
		$active_m = array();
		$active_f = array();
		$k = 0;
		if($s=='m'){
			for ($i = 7; $i >=1 ; $i--){
				$sql = "select user_id from user where email_id not regexp 'trulumadly' and gender='M' and DATEDIFF(DATE_SUB(NOW(),INTERVAL $i day),last_login)<=10";
				//echo $sql."</n>";
				$res = $this->conn->Execute($sql);
				if($res->RowCount()>0){
					$nameRows = $res->GetRows();
					//var_dump($nameRows);
					foreach ($res as $r=>$e){
						$userIds[] = $e['user_id'];
					}
					$this->userIdList = implode(',', $userIds);
					$query = "select count(distinct(user1)) as no_of_active_users_got_recommendation from user_recommendation where user1 in ($this->userIdList) and date(timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day))";
					//echo $query."</n>";
					$result = $this->conn->Execute($query);
					if($result->RowCount()>0){
						//$nameRow = $result->GetRow();
						foreach ($result as $r=>$e){
							$active_m[$k] = $e['no_of_active_users_got_recommendation'];//$e['no_of_active_users_got_recommendation'];
						}
						$k++;		
					}
					//echo $query;
				}
			}
			$this->active_recomm_m = $active_m;
			//echo "no of active male users who got recommendation";
		//	var_dump($this->active_recomm_m);
		}else{
				for ($i = 7; $i >=1 ; $i--){
					$sql = "select user_id from user where email_id not regexp 'trulumadly' and gender='F' and DATEDIFF(DATE_SUB(NOW(),INTERVAL $i day),last_login)<=10";
					//echo $sql."</n>";
					$res = $this->conn->Execute($sql);
					if($res->RowCount()>0){
						$nameRows = $res->GetRows();
						//var_dump($nameRows);
						foreach ($res as $r=>$e){
							$userIds[] = $e['user_id'];
						}
						$this->userIdList = implode(',', $userIds);
						$query = "select count(distinct(user1)) as no_of_active_users_got_recommendation from user_recommendation where user1 in ($this->userIdList) and date(timestamp)=date(DATE_SUB(NOW(),INTERVAL $i day))";
						//echo $query."</n>";
						$result = $this->conn->Execute($query);
						if($result->RowCount()>0){
							//$nameRow = $result->GetRow();
							foreach ($result as $r=>$e){
								$active_f[$k] = $e['no_of_active_users_got_recommendation'];//$e['no_of_active_users_got_recommendation'];
							}
							$k++;
						}
						//echo $query;
					}
				}
				$this->active_recomm_f = $active_f;
			//	echo "no of active female users who got recommendation";
				//var_dump($this->active_recomm_f);
		}
	}
	
	
	function getData(){
		//$content = "";
		$content = "USER STATISTICS".","." ";
		for($i=0;$i<count($this->dates);$i++){
			foreach ($this->dates[$i] as $key=>$value){
				$content .=",".$value;
			}
		}
		$content .= ","." ";
		for($i=0;$i<count($this->dates);$i++){
			foreach ($this->dates[$i] as $key=>$value){
				$content .=",".$value;
			}
		}
		$content .="\nNew User Added";
		$content .= ",M";
		for($i=0;$i<7;$i++){
				$content .= ",".$this->new_user_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
				$content .= ",".$this->new_user_f[$i];
		}
		$content .= "\nTotal Number Of Users";
		for($i=0;$i<count($this->total_user_m);$i++){
			foreach ($this->total_user_m[$i] as $key=>$value){
				$content .= ",".$value;
			}
		}
		$content .= "\nAuthentic Users";
		for($i=0;$i<count($this->authentic_user_m);$i++){
			foreach ($this->authentic_user_m[$i] as $key=>$value){
				$content .= ",".$value;
			}
		}
		$content .= "\nActive Users(logged in last 10 days)";
		for($i=0;$i<count($this->no_of_active_users_m);$i++){
			foreach ($this->no_of_active_users_m[$i] as $key=>$value){
				$content .= ",".$value;
			}
		}
		$content .= "\nNumber of Users who received Recommendations";
		$content .= ",M";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->recommendation_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->recommendation_f[$i];
		}
		$content .= "\nNumber of Active Users who received Recommendations";
		$content .= ",M";
		for($i=0;$i<7;$i++){
				$content .= ",".$this->active_recomm_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
				$content .= ",".$this->active_recomm_f[$i];
		}
		$content .= "\nNumber of Likes sent";
	$content .= ",M";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->like_sent_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->like_sent_f[$i];
		}
		$content .= "\nNumber of Likes Accepted";
		$content .= ",M";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->like_accepted_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->like_accepted_f[$i];
		}
		$content .= "\nNumber of Messages sent";
		$content .= ",M";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->message_sent_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->message_sent_f[$i];
		}
		$content .= "\nNumber of Messages Replied";
		$content .= ",M";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->message_accepted_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->message_accepted_f[$i];
		}
		$content .= "\nNumber of Credits consumed(1 profiles has 10 credit)";
		$content .= ",M";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->credit_consumed_m[$i];
		}$content .= ",F";
		for($i=0;$i<7;$i++){
			$content .= ",".$this->credit_consumed_f[$i];
		}
		
		//echo $content;
		try{
			$done =  file_put_contents("/tmp/dashboard_daily.csv",$content);
			return $done;
		}catch(Exception $e){
			echo $e->getMessage();
		}
		
	}
	
	function sendMailWithAttachment(){
		$to = "satendra@trulymadly.com".",";
		$to .= "shashwat@trulymadly.com" ;
		$subject = "Dashboard daily data";
		$message = "Hi PFA,";
		# Open a file
		$file = fopen( "/tmp/dashboard_daily.csv", "r" );
		if( $file == false )
		{
			echo "Error in opening file";
			exit();
		}
		# Read the file into a variable
		$size = filesize("/tmp/dashboard_daily.csv");
				$content = fread( $file, $size);
		
				# encode the data for safe transit
				# and insert \r\n after every 76 chars.
				$encoded_content = chunk_split( base64_encode($content));
		
				# Get a random 32 bit number using time() as seed.
				$num = md5( time() );
		
				# Define the main headers.
				$header = "From:rajesh.singh@trulymadly.com\r\n";
				$header .= "MIME-Version: 1.0\r\n";
				$header .= "Content-Type: multipart/mixed; ";
				$header .= "boundary=$num\r\n";
				$header .= "--$num\r\n";
		
				# Define the message section
				$header .= "Content-Type: text/plain\r\n";
				$header .= "Content-Transfer-Encoding:8bit\r\n\n";
				$header .= "$message\r\n";
				$header .= "--$num\r\n";
		
				# Define the attachment section
				$header .= "Content-Type:  multipart/mixed; ";
				$header .= "name=\"dashboard_daily.csv\"\r\n";
				$header .= "Content-Transfer-Encoding:base64\r\n";
				$header .= "Content-Disposition:attachment; ";
				$header .= "filename=\"dashboard_daily.csv\"\r\n\n";
				$header .= "$encoded_content\r\n";
				$header .= "--$num--";
		
				# Send email now
				$retval = mail ( $to, $subject, "", $header );
				if( $retval == true )
				{
				echo "Message sent successfully...";
				}
				else
				{
				echo "Message could not be sent...";
				}
	}
}

try{
	$dash = new dashboard();
	$dash->getDates();
	$dash->newUser('m');
	$dash->newUser('f');
	$dash->totalUser();
	$dash->authenticUser();
	$dash->getActive();
	//$dash->noRecommendation();
	$dash->getRecommendations('m');
	$dash->getRecommendations('f');
	$dash->likeSent('m');
	$dash->likeSent('f');
	$dash->likeAccepted('m');
	$dash->likeAccepted('f');
	$dash->messageSent('m');
	$dash->messageSent('f');
	$dash->messageAccepted('m');
	$dash->messageAccepted('f');
	$dash->creditsConsumed('m');
	$dash->creditsConsumed('f');
	$dash->getActiveRecommendation('m');
	$dash->getActiveRecommendation('f');
	
	$result = $dash->getData();
	if($result){
		$dash->sendMailWithAttachment();
	}
}catch(Exception $e){
	debug_backtrace();
}


?>