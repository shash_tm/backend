<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";

class DeviceIdActions
{
    function __construct()
    {
    }

    public function searchDeviceId($device_id)
    {
        $sql = "SELECT * FROM block_fids bf  WHERE bf.device_id = ?";
        $devices = Query_Wrapper::SELECT($sql, array($device_id));
        foreach($devices as $key => $val)
        {
            $user_array = array();
            // users registered from this device
            $sql_device = "SELECT user_id, u.fname,if(u.deletion_status is not null,'deleted',u.status) as user_status, fid FROM user u where device_id = ?";
            $device_users = Query_Wrapper::SELECT($sql_device, array($val['device_id'])) ;

            if(count($device_users) > 0)
                $user_array = $device_users ;

            if(isset($val['user_id']) && $val['user_id'] != null)
            {
                $sql_user = "SELECT user_id, u.fname,if(u.deletion_status is not null,'deleted',u.status) as user_status, fid FROM user u where user_id = ?" ;
                $user_data = Query_Wrapper::SELECT($sql_user, array($val['user_id']), Query::$__slave, true) ;
                $user_array[] = $user_data;
            }
            $devices[$key]['all_users'] = $user_array;
        }
        //print_r(json_encode($devices)); die ;
        return $devices[0];
    }

    public function unBlockDevice($device_id)
    {
        $sql = "DELETE FROM block_fids WHERE device_id = ?" ;
        Query_Wrapper::DELETE($sql, array($device_id));
    }

}