<?php 
require_once dirname ( __FILE__ ) . "/../include/config.php";


class logging{
		
	function __construct() {
	}
	
	public function log($adminId,$userId,$query,$step='',$details=''){
		global $conn;
		try{
			$conn->Execute($conn->prepare("insert delayed into admin_log (id,user_id,step,details,query) values(?,?,?,?,?)"),
					array($adminId,$userId,$step,$details,$query));
		}catch(Exception $e){
			trigger_error($e->getMessage());
		}
		
	}
}

?>