<?php
//require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../fb.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../TMObject.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../include/tbconfig.php";
//include dirname ( __FILE__ ) . "/include/allValues.php";
require_once dirname ( __FILE__ ) . "/../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../include/User.php";
require_once dirname ( __FILE__ ) . "/../TrustBuilder.class.php";
require_once dirname ( __FILE__ ) . "/logging.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../mobile_utilities/pushNotification.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
//require_once dirname ( __FILE__ ) . "/../DBO/adminDBO.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once (dirname ( __FILE__ ) . '/../photomodule/Photo.class.php');
require_once (dirname ( __FILE__ ) . '/DeviceIdActions.php');

class verifyImage extends TMObject {

	protected $conn_slave;
	protected $conn_master;
	protected $conn_reporting;
	private $_admin_dbo; 
	
	// limit for Super Admin to change User details such as Birthday,phoneNumber,name and gender !!!
	public static $admin_change_limit=50;   
	public static $admin_phone_limit=20;
	
	function __construct($user_id) {
		global $conn_slave,$conn_master,$conn_reporting;
		$this->conn_slave = $conn_slave;
		$this->conn_master = $conn_master;
		$this->conn_reporting=$conn_reporting;
		$this->conn_reporting->SetFetchMode(ADODB_FETCH_ASSOC);
//		$this->_admin_dbo = new AdminDBO();
		
		parent::__construct ( $user_id );
	}
    public function processUserUnderReview($user_id,$flag)
    {   global $redis; 
    	//var_dump($user_id) ;
    	//var_dump($flag);
    	if($flag==1)
    	{   $query="update user_under_review set admin_approved='yes' where user_id=?";
    	    $param_array=array($user_id);
    	    $tablename="user_under_review";
    	    Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$user_id);  
    	}
    	else 
    	{	
    		$query="update user_under_review set admin_approved='no' where user_id=?";
    	    $param_array=array($user_id);
     	    $tablename="user_under_review";
    	    Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$user_id);
    	    
    	    $query="delete from user_under_review where user_id=?";
    	    $param_array=array($user_id);
    	    $tablename="user_under_review";
    	    Query_Wrapper::DELETE($query, $param_array, $tablename);
    	    	
    	    
    	    $user_utils=new userUtilsDBO();
    	    $user_utils->suspend($user_id,"escort","authentic",$_SESSION['admin_id'],"blocked");
    	    $user_utils->block_device($user_id, "escort");
    	    
    	    $redisKey=Utils::$redis_keys['female_like_day'];
    	    $redisKey=$redisKey.date("Y-m-d");
    	    $redis->hDel($redisKey,$user_id);
    	    	
    	}
    	return true;
    	
    }
	public function sendEmail($type, $user_id, $photo_id = null){
        $sysLog = new SystemLogger();
		$user = new User();
		$row = $user->getUserDetails($user_id);
		$isValid = $user->canSendMail($row['email_id']);
		/*print_r($row);
		 echo $isValid*/;
		$this->smarty->assign("fname", ucfirst($row['fname']));
		$this->smarty->assign("name", ucfirst($row['fname'])); 
		$this->smarty->assign("lname", ucfirst($row['lname']));
		$mailObject = new MailFunctions();

		$template = null;
		if($isValid>0){
			switch($type){
			//	case "photoId":
					/*$sql = "SELECT img_location FROM user_id_proof where user_id = $user_id";
					 $res = $this->conn->Execute($sql);
					 $row = $res->FetchRow();
					 var_dump($row);
					 *///$this->smarty->assign("pic" ,$this->baseurl."/".$row['img_location']);
			/*		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photoId_rejected";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="view_admin_photoId_rejected";
					$campaignName = "admin_photoId_rejected";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/idRejection.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Important - We cannot verify your PhotoId, please contact us ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);

					break;*/
						
				case "photoId-approved":
					$this->smarty->assign("doc_type", "photo ID");
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photoId_approved";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="view_admin_photoId_approved";
					$campaignName = "admin_photoId_approved";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/PhotoIDVerified.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Hurray! Your PhotoID has been verified", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Verification Message from TrulyMadly.com ", $user_id);

					break;


				case "photoId-notClear":
					/*$sql = "SELECT img_location FROM user_id_proof where user_id = $user_id";
					 $res = $this->conn->Execute($sql);
					 $row = $res->FetchRow();
					 var_dump($row);*/
					//$this->smarty->assign("pic" ,$this->baseurl."/".$row['img_location']);
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photoId_notClear";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="view_admin_photoId_notClear";
					$campaignName = "admin_photoId_notClear";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/PhotoIdInvalid.tpl'; 
				    $mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"",ucfirst($row['fname']).", your Photo ID is illegible.", $user_id);
					//$mid= $mailObject->sendMail(array('thedeceptio@gmail.com') ,$this->smarty->fetch($template),"","PhotoID Verification: Document is Unclear/Invalid ", $user_id);

					break;

				case "name_age_mismatch";
					$sql="SELECT TIMESTAMPDIFF(YEAR,ud.DateOfBirth,CURDATE()) AS tm_age, CONCAT(ui.fname,' ',ui.lname) AS doc_name,
							TIMESTAMPDIFF(YEAR,ui.DateOfBirth,CURDATE()) AS doc_age
							FROM  user_id_proof ui JOIN user_data ud ON ud.user_id=ui.user_id
						  WHERE ud.user_id=$user_id";
						  $res = $this->conn_slave->Execute($sql);
					$rs=$res->FetchRow();
					$rs['doc_name']=ucfirst($rs['doc_name']);
					$this->smarty->assign("data", $rs);  
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photoId_name_age_mismatch";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="view_admin_photoId_name_age_mismatch";
					$campaignName = "admin_photoId_name_age_mismatch";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/PhotoIdMismatchNameAge.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Oops! There's a mismatch in your age and name.", $user_id);
				
					break;
					
				case "name_mismatch";
					$sql="SELECT CONCAT(fname,' ',lname) AS doc_name FROM  user_id_proof WHERE user_id=$user_id";
					$res = $this->conn_slave->Execute($sql);
					$rs=$res->FetchRow();
					$rs['doc_name']=ucfirst($rs['doc_name']);
					$this->smarty->assign("data", $rs);
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photoId_name_mismatch";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="view_admin_photoId_name_mismatch";
					$campaignName = "admin_photoId_name_mismatch"; 
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/PhotoIdMismatchName.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Oops! There's a mismatch in your name.", $user_id);
					
					break;
					
					
			 	case "age_mismatch";
					$sql="SELECT TIMESTAMPDIFF(YEAR,ud.DateOfBirth,CURDATE()) AS tm_age, TIMESTAMPDIFF(YEAR,ui.DateOfBirth,CURDATE()) AS doc_age
 						  FROM  user_id_proof ui JOIN user_data ud ON ud.user_id=ui.user_id WHERE ud.user_id=$user_id";
					$res = $this->conn_slave->Execute($sql);
					$rs=$res->FetchRow();
					$this->smarty->assign("data", $rs);
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photoId_age_mismatch";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="view_admin_photoId_age_mismatch";
					$campaignName = "admin_photoId_age_mismatch";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/PhotoIdMismatchAge.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Oops! There's a mismatch in your age.", $user_id);
					
					break;
					
					
				case "password_required";
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photoId_password_required";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="view_admin_photoId_password_required";
					$campaignName = "admin_photoId_password_required";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/PhotoIdPassword.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"",ucfirst($row['fname']).", your Photo ID is password protected", $user_id);
					//$mid= $mailObject->sendMail(array('thedeceptio@gmail.com') ,$this->smarty->fetch($template),"","PhotoID Verification: Document is Password Protected ", $user_id);
					break;
			//sumit - not needed anymore		
		/*		case "photo_required";
					
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=photo";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="photoId_photo_required";
					$campaignName = "photoId";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/photo_required.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","PhotoID Verification: Need a Profile Photo ", $user_id);
						
					break;*/
					
	/*			case "photo":
					$sql = "SELECT thumbnail, is_profile FROM user_photo where photo_id = $photo_id";
					$res = $this->conn_slave->Execute($sql);
					$row1 = $res->FetchRow();
					$this->smarty->assign("pic", $this->imageurl .  $row1['thumbnail']);
					$this->smarty->assign("is_profile",  $row1['is_profile']);

					//var_dump($row);
					$sql2 = "SELECT * FROM user_photo where user_id = $user_id";
					$res2 = $this->conn_slave->Execute($sql2);
					$row2 = $res2->GetRows();
					//	var_dump($row2);
					$allRejected = true;
					$anyApproved = NULL	;
					foreach ($row2 as $e =>$c){
						if($c['status']!='rejected') $allRejected = false;
						if($c['status'] == 'active') $anyApproved = true;
					}
					$this->smarty->assign("allRejected", $allRejected);
					$this->smarty->assign("anyApproved", $anyApproved);
					//echo $this->imageurl . "/". $row['thumbnail'];
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photo_rejected";
					$redirectionFile = "/photo.php";
					$utm_content="admin_photo_rejected";
					$campaignName = "admin_photo_rejected";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/photoRejection.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Photo Disapproved on Trulymadly ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);
					//echo $mid;
					break;
				case "photo_approved":

					$sql = "SELECT thumbnail FROM user_photo where photo_id = $photo_id";
					$res = $this->conn_slave->Execute($sql);
					$row1 = $res->FetchRow();
					$this->smarty->assign("pic", $this->imageurl .  $row1['thumbnail']);
					$this->smarty->assign("doc_type", "photo");
					$this->smarty->assign("can_profile", "no");
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_photo_approved";
					$redirectionFile = "/photo.php";
					$utm_content="admin_photo_approved";
					$campaignName = "admin_photo_approved";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/approvalEmailers/photoApproved.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Congrats! Your Photo has been approved ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Verification Message from TrulyMadly.com ", $user_id);
					break;*/

					/*				case "photo_approved_without_profile":
					 	
					$sql = "SELECT thumbnail FROM user_photo where photo_id = $photo_id";
					$res = $this->conn_slave->Execute($sql);
					$row1 = $res->FetchRow();
					$this->smarty->assign("pic", $this->imageurl .  $row1['thumbnail']);
					$this->smarty->assign("doc_type", "photo");
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=photo";
					$redirectionFile = "/photo.php";
					$utm_content="photo_approved_no_profile";
					$campaignName = "photo";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/approvalEmailers/photoApproved_noprofile.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Congrats! Your Photo has been approved ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Verification Message from TrulyMadly.com ", $user_id);
					break;*/
					


				case "all_photos_rejected":

					//$this->smarty->assign("name", ucfirst($row['fname']));
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_all_photos_rejected";
					$redirectionFile = "/photo.php";
					$utm_content="view_admin_all_photos_rejected";
					$campaignName = "admin_all_photos_rejected";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/DPReject.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Oops! Your profile picture cannot be approved.", $user_id);
					break;



				case "profile_photo_approved_others_rejected":

					//$this->smarty->assign("name", ucfirst($row['fname'])); 
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=admin_profile_photo_approved_others_rejected";
					$redirectionFile = "/photo.php";
					$utm_content="view_admin_profile_photo_approved_others_rejected";
					$campaignName = "admin_profile_photo_approved_others_rejected";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/PhotoRejectNotPP.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Oops! Your pictures cannot be approved.", $user_id);
					break;

					/*	case "address":
					 $campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=address";
					 $redirectionFile = "/trustbuilder.php";
					 $utm_content="address_reject";
					 $campaignName = "address";
					 $analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					 $this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/addressRejection.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Important - We cannot verify your Address, please contact us ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);

					break;

				case "address-notClear":
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=address";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="address_notClear";
					$campaignName = "address";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/addressNotclear.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Important - Incorrect document uploaded for Address verification ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);

					break;
					
					case "address_city_mismatch":
						$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=address";
						$redirectionFile = "/trustbuilder.php";
						$utm_content="address_city_mismatch";
						$campaignName = "address";
						$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
						$this->smarty->assign("analyticsLinks", $analyticsLinks);
						$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/address_city_mismatch.tpl';
						$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Address Verification: Current Location not Matching Address Proof ", $user_id);
						//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);
					
						break;
						
					case "address_name_mismatch":
						$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=address";
						$redirectionFile = "/trustbuilder.php";
						$utm_content="address_name_mismatch";
						$campaignName = "address";
						$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
						$this->smarty->assign("analyticsLinks", $analyticsLinks);
						$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/address_name_mismatch.tpl';
						$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Address Verification: Incorrect Name on Address Proof ", $user_id);
						//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);
								
						break;
						
					case "address_password_required":
						$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=address";
						$redirectionFile = "/trustbuilder.php";
						$utm_content="address_password_required";
						$campaignName = "address";
						$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
						$this->smarty->assign("analyticsLinks", $analyticsLinks);
						$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/address_password_required.tpl';
						$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),""," Address Verification: Document is Password Protected ", $user_id);
						//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);
						
						break;

				case "address-approved":
					$this->smarty->assign("doc_type", "address");
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=address";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="address_approved";
					$campaignName = "address";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/approvalEmailers/verificationApproved.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Congrats! Your Address has been verified ", $user_id);
				//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Verification Message from TrulyMadly.com ", $user_id);

					break;

				case "employment":
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=employment";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="employment_reject";
					$campaignName = "employment";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/employmentRejection.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Important - We cannot verify your Employment, please contact us ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);
					break;

				case "employment-notClear":
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=employment";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="employment_notClear";
					$campaignName = "employment";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/EmpIdInvalid.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Employment verification: Uho! There seems to be a problem with the document you submitted.", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);
					break;
                
					
				case "employment_password_required":
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=employment";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="employment_password_required";
					$campaignName = "employment";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/utilities/newmailers/EmpIdPassword.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Employment verification: Ohno! The document you submitted is password protected.", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);
					break;
					
				case "employment-approved":
					$this->smarty->assign("doc_type", "employment");
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=employment";
					$redirectionFile = "/trustbuilder.php";
					$utm_content="employment_approved";
					$campaignName = "employment";
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $row['email_id'], $user_id);
					$this->smarty->assign("analyticsLinks", $analyticsLinks);
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/approvalEmailers/verificationApproved.tpl';
					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Congrats! Your Employment has been verified ", $user_id);
					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Verification Message from TrulyMadly.com ", $user_id);

					break;*/
						
				case "suspend":
					$template = dirname ( __FILE__ ). '/../templates/admin/utilities/rejectionEmailers/suspend.tpl';

					//$mid= $mailObject->sendMail(array('him26.89@gmail.com') ,$this->smarty->fetch($template),"","Important Message from TrulyMadly.com ", $user_id);

					$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Suspension Notice from TrulyMadly ", $user_id);
					break;

			}
			
			    $data['email_id'] = $row['email_id'];
				$data['subject'] = $campaignName;
				$data['user_id'] =  $user_id;
				$sysLog->logSystemMail($data); 

			/*echo $row['email_id'];
				echo PHP_EOL;
				echo $template;
			 $this->smarty->display($template);*/ //die;
			//$mid= $mailObject->sendMail(array($row['email_id']) ,$this->smarty->fetch($template),"","Photo Disapproved Email", $user_id);

		}
 	}

 	public function sendnotification($type,$user_id){
 		global $imageurl,$admin_id;
//  		$sql="SELECT app_version_code FROM mobile_data WHERE user_id=$user_id";
//  		$res = $this->conn->Execute($sql);
//  		$row = $res->FetchRow();
//  		$ver=$row['app_version_code'];
//  if ($ver>=35) {
 	   $pushnotify = new pushNotification();
 		$uu = new UserUtils();
 		$user_data = $uu->getNamenPic($admin_id); 
 		if(isset($user_data['thumbnail'])) 
 		$pic_url = $imageurl.$user_data['thumbnail'];	
 		$ticker_text = "Message from TrulyMadly";
 	switch($type){
 		
 		case "id_active":
 			$title_text="PhotoID verification";
 			$data="Hurray! Your PhotoID has been verified";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 					"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 			break;
 		
 		case "name_age_mismatch":
 			$title_text="PhotoID verification";
 			$data="Oho! We have a name and age mismatch";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 					"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 			break;
 			
 	case "age_mismatch":
 			$title_text="PhotoID verification";
 			$data="Oho! We have a age mismatch";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 				"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 			break;
 			
 	case "name_mismatch":
 			$title_text="PhotoID verification";
 			$data="Oho! We have a name mismatch";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 				 "title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']); 
 			break;
 	
 	case "password_required":
 			$title_text="PhotoID verification";
 			$data="Your document is password protected. Please provide password";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 				"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 			//$pushnotify->iosNotify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"push_type"=>"TRUST"));
 			break;
 			
 	case "photoId-notClear": 
 			$title_text="PhotoID verification";
 			$data="Your document is either illegible or invalid.";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 					"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 			break;
 			
 	case "emp_active":
 			$title_text="Employment verification";
 			$data="Your employment verification is done!";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 						"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 				break;
 	
 	case "employment-notClear": 
 			$title_text="Employment verification";
 			$data="Uho! There seems to be a problem with the document you submitted.";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 					"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 			break;
 	
 	case "employment_password_required":
 			$title_text="Employment verification";
 			$data="Ohno! The document you submitted is password protected.";
 			$pushnotify->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 						"title_text"=>$title_text,"push_type"=>"TRUST"),$_SESSION['admin_id']);
 			break;
 			
 	case "profile_photo_rejected":
 			$title_text="TrulyMadly";
 			// check if Female 
 			$sql = "Select gender from user where user_id = ?" ;
 			$gender = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
 			if ($gender['gender'] == 'F')
 			{	
 				$past_like_sql = "select count(*) as cu from user_like where user1 =  ?";
 				$pastLikeCount = Query::SELECT($past_like_sql, array($user_id), Query::$__slave, true);
 				if($pastLikeCount['cu'] > 0 )
 				{
 					// female has likes before message
 					$data = " Oops! Your pic got rejected. Upload another one now!";
 					$push_array = array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 					"title_text"=>$title_text,"push_type"=>"PHOTO","event_status" => "profile_pic_rejected_liked_before","previousLikeStatus"=>true) ;
 				}
 				else
 				{
 					$data = " Oops! Your pic got rejected. Upload another one now!";
 					$push_array = array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 							"title_text"=>$title_text,"push_type"=>"PHOTO","event_status" => "profile_pic_rejected_no_previous_like","previousLikeStatus"=>false) ;
 				}
 				
 			}
 			else
 			{
 				// male message
 				$data=" Oops! Your pic got rejected. Upload another one now!";
 				$push_array = array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
 						"title_text"=>$title_text,"push_type"=>"PHOTO","event_status" => "profile_pic_rejected") ;
 			}
 			
 			$pushnotify->notify($user_id, $push_array,$_SESSION['admin_id']);
 			
 			break;
 			
 			
 		//}
 		}
 		

 		
 	
 	
 		
 	}
 	
    public function getIdProofs($user_id) {
		$data = NULL;
		  // $rs = $this->conn_slave->Execute ( "SELECT  ui.status as proof_status, Date(ui.current_action_time) as action_time, ui.number_of_trials, ui.img_location,ui.user_id,ui.proof_type,u.fname,u.lname from user_id_proof ui, user u where ui.user_id=u.user_id and u.user_id = $user_id  order by ui.upload_time desc" );
		 // $rs = $this->conn_slave->Execute ( "SELECT  ui.status as proof_status, ui.number_of_trials,ui.tstamp,  ui.img_location,ui.user_id,ui.proof_type,ui.password, ui.number_of_trials, ui.name_age_mismatch_allow, a.user_name as admin_name,u.fname,u.lname FROM user_id_proof_log ui LEFT JOIN admin a on a.id=ui.admin_id, user u  WHERE ui.user_id=u.user_id and u.user_id = $user_id order by tstamp desc" );
		 $sql = "SELECT  ui.status as proof_status, ui.number_of_trials,ui.tstamp,  ui.img_location,ui.user_id,ui.proof_type,ui.password, ui.number_of_trials, "
		 		. "ui.name_age_mismatch_allow, a.user_name as admin_name,u.fname,u.lname FROM user_id_proof_log ui LEFT JOIN admin a on a.id=ui.admin_id, user u  "
		 		. "WHERE ui.user_id=u.user_id and u.user_id = ? order by tstamp desc" ;
		  $data = Query_Wrapper::SELECT($sql, array($user_id));
	    	if (count($data) > 0) {
		//	$data = $rs->GetRows ();
			foreach ( $data as $key => $row ) {
				$fileType = end ( explode ( '.', $row ['img_location'] ) );
				if (in_array ( $fileType, array (
						'png',
						'jpg',
						'jpeg',
						'gif',
						'PNG',
						'JPEG',
						'JPG',
						'GIF'
						) )) {
							$data [$key] ['img_flag'] = 'yes';
						}
			}
		}
		return $data;
	}
	 
	public function getEmploymentProofs($user_id) {
		$data = NULL;
		//$rs = $this->conn_slave->Execute ( "SELECT ui.is_paid as is_paid, ui.verification_tracking_number as ref_no,ui.sent_for_verification,  date(ui.verification_sent_time) as verification_time,ui.img_location,ui.user_id,ui.number_of_trials,ui.status as proof_status,Date(ui.current_action_time) as action_time, ui.proof_type,u.fname,u.lname,ui.address,ui.city,ui.state,ui.pincode,upn.user_number,upn.status from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id, user u where  ui.user_id=u.user_id and u.user_id = $user_id order by ui.upload_time desc" );
// 		$rs = $this->conn_slave->Execute ( "SELECT ui.is_paid AS is_paid, ui.verification_tracking_number AS ref_no,ui.sent_for_verification,  DATE(ui.verification_sent_time) AS verification_time,ui.img_location,ui.user_id,ui.number_of_trials,ui.status AS proof_status,ui.password, ui.tstamp , ui.proof_type,ui.company_name,ui.emp_id, ui.work_status,ui.reject_reason,ui.show_name, ui.mismatch_allow,a.user_name as admin_name, u.fname,u.lname
// 				                                                                           FROM user_employment_proof_log ui
// 				                                                                           LEFT JOIN  user u on ui.user_id=u.user_id
// 				                                                                           LEFT JOIN admin a on a.id=ui.admin_id
// 				                                                                                     where u.user_id = $user_id order by tstamp desc" );
		$sql = "SELECT ui.is_paid AS is_paid, ui.verification_tracking_number AS ref_no,ui.sent_for_verification,  DATE(ui.verification_sent_time) "
				."AS verification_time ,ui.img_location,ui.user_id,ui.number_of_trials,ui.status AS proof_status,ui.password, ui.tstamp , "
				."ui.proof_type,ui.company_name,ui.emp_id, ui.work_status,ui.reject_reason,ui.show_name, ui.mismatch_allow,a.user_name as admin_name, " 
				."u.fname,u.lname FROM user_employment_proof_log ui  LEFT JOIN admin a on a.id=ui.admin_id where u.user_id = ? order by tstamp desc";
		$data = Query_Wrapper::SELECT($sql, array($user_id)) ;
		if (count($data) > 0) {
			//$data = $rs->GetRows ();
			foreach ( $data as $key => $row ) {
				$fileType = end ( explode ( '.', $row ['img_location'] ) );
				if (in_array ( $fileType, array (
						'png',
						'jpg',
						'jpeg',
						'gif',
						'PNG',
						'JPEG',
						'JPG',
						'GIF'
						) )) {
							$data [$key] ['img_flag'] = 'yes';
						}
			}
		}
		return $data;
		
	}
	
	public function getPhotosOfUser($user_id) {
		$data = NULL;
		//$rs = $this->conn->Execute ( "SELECT photo_id,name,u.fname,u.lname, u.user_id, u.gender, u.email_id,u.fid, u.status, up.is_profile, up.status as photo_status from user_photo up, user u where up.user_id=u.user_id and u.user_id = $user_id order by field(up.status, 'under_moderation',  'active', 'rejected','changed', 'deleted')" );
		$sql = "SELECT photo_id,name,u.fname,u.lname, u.user_id, u.gender, u.status, up.is_profile, up.status as photo_status ,	up.admin_approved as admin_approved, "
				."up.mapped_to from user_photo up, user u where up.user_id=u.user_id and u.user_id = ? and up.status not in ('changed','deleted') "
				."order by field(up.admin_approved, 'no',  'yes')" ;
		$data = Query_Wrapper::SELECT($sql, array($user_id)) ;
		if (count($data) > 0) {
			//$data = $rs->GetRows ();
			foreach ( $data as $key => $row ) {
				$fileType = end ( explode ( '.', $row ['img_location'] ) );
				if (in_array ( $fileType, array (
						'png',
						'jpg',
						'jpeg',
						'gif',
						'PNG',
						'JPEG',
						'JPG',
						'GIF'
						) )) {
							$data [$key] ['img_flag'] = 'yes';
						}
			}
		}
		return $data;
	}

 
	
	 
	public function searchByName($page_id=0,$fname, $lname){
	
		$this->smarty->assign('fname', $fname); 
		$this->smarty->assign('lname', $lname);
		$this->smarty->assign('search_by', 'name'); 

		$lower_range=($page_id-1)*30;
		$upper_range=30;
		$param_array = array();
		$userIds = array() ;
	
		if($fname && $lname )
			{
//			$sql="select user_id from user u where fname='$fname' and lname = '$lname' order by user_id desc limit $lower_range,$upper_range";
			$sql="select user_id from user u where fname= ? and lname = ? order by user_id desc limit ?,?";
			array_push($param_array, $fname,$lname, $lower_range, $upper_range) ;
			} 
		else if( $fname )
			{
			//$sql="select user_id from user u where fname='$fname' order by user_id desc limit $lower_range,$upper_range";
			$sql="select user_id from user u where fname= ? order by user_id desc limit ?,?";
			array_push($param_array, $fname, $lower_range, $upper_range) ;
			} 
		else if( $lname )
			{
// 			$sql="select user_id from user u where lname='$lname' order by user_id desc limit $lower_range,$upper_range";
			$sql="select user_id from user u where lname= ? order by user_id desc limit ?,?";

			array_push($param_array, $lname, $lower_range, $upper_range) ;
			} 
		else
		 	{
			return ;
			}
	
	//$rs = $this->conn_slave->Execute($this->conn_slave->Prepare($sql));
	$result = Query_Wrapper::SELECT($sql, $param_array) ;
	
	if(count($result)>0)
	{
			foreach ($result as $row)
			{
				$userIds[] = $row['user_id'];
			}
	} else {
		return ;
	}
	

	
	$attributes= $this->getuserdata($userIds,'Y');

	return $attributes;
	
 }

	public function getSearchData($page_id=0,$param, $value){
		//echo $param;
		//echo $value;
			//	global $industries_arr, $educations_arr,$educations_subarr;
		$this->smarty->assign('search_by', $param); 
		$this->smarty->assign('value', $value);
		$lower_range=($page_id-1)*30;
// 		echo $lower_range;
// 		echo $page_id;
		$upper_range=30;
		$user_id = null;
		switch($param){
			case "UserId":
				$userIds[] = (int)$value;
				$attributes= $this->getuserdata($userIds,'Y');
				break;
			case "name": 
				if ($value == null){
					return;
				}
				$sql = "SELECT user_id from user where fname like '%$value%' order by user_id desc limit $lower_range,$upper_range";
				$rs = $this->conn_slave->Execute($sql);
				if ($rs->RowCount () > 0) { 
					$res = $rs->GetRows();
					foreach ($rs as $r=>$e){
						$userIds[] = $e['user_id'];
					}
					$attributes= $this->getuserdata($userIds,'N');
				} else { 
					return;
				}
				break;
			case "email_id":
				$sql = "SELECT user_id from user where email_id like '$value'";
				$rs = $this->conn_slave->Execute($sql);
				if ($rs->RowCount () > 0) {
				$res = $rs->GetRows();
					foreach ($rs as $r=>$e){
						$userIds[] = $e['user_id'];
					}
					$attributes= $this->getuserdata($userIds,'N');
				}else{
					return;
				}
				
				break;
		}
		//$attributes= $this->getuserdata($userIds);
		return ($attributes);
		
			}
	
	public function getDataByDate($page_id=0, $sortBy = null, $default = null){
		//var_dump($religions_arr); die;
		//echo $sortBy;
		//echo $page_id;
		//global $industries_arr, $educations_arr,$educations_subarr;
 
		if($default == null){
			$date = " date_sub(CURDATE(), interval 1 day)";
		
		}
		else{
			$date = " date_sub(CURDATE(), interval $default day)";
			//echo $date;
			$this->smarty->assign("sortBy", "day");
			$this->smarty->assign("value", $default);
		}
		//echo $date; die;
		$lower_range=($page_id-1)*30;
			$upper_range=30;
			//echo $page_id;
		/*echo $lower_range;
		echo PHP_EOL;
		echo $upper_range;// die;
		*/$attributes = array();
		$userIds = array();
		$userIdList= null;
		//echo $sortBy;
		if ($sortBy == null)
		{
			return;
		}
		if($sortBy == 'day'){
	//	if($sortBy == null || $sortBy == 'day'){
		   //	echo 'here';
			//$sql_user = $this->conn_slave->Prepare("SELECT fname, date(registered_at) as registered_at, lname, email_id, last_login, gender, status, is_fb_connected, user_id from user where registered_at >= $date order by user_id desc limit $lower_range,$upper_range ");// $this->profile_id";
			$sql_user = $this->conn_slave->Prepare("SELECT user_id from user where registered_at >= $date order by user_id desc limit $lower_range,$upper_range ");
			//	echo "SELECT fname, date(registered_at) as registered_at, lname, email_id, last_login, gender, status, is_fb_connected, user_id from user where registered_at >= $date order by user_id desc limit $lower_range,$upper_range ";
			$res = $this->conn_slave->Execute($sql_user);
			$nameRows = $res->GetRows();
			$users = array();
			//var_dump($nameRows); die;
			foreach ($nameRows as $row => $col){
				//$user[$col['user_id']] = $col;
				$userIds[] = $col['user_id'];
			}
		}

		
		else{
			$rs = null;
			switch ($sortBy){
			
				case "photos-under-moderation-authm":
					$rs = $this->conn_slave->Execute ( "SELECT DISTINCT(up.user_id) FROM user_photo up JOIN user u ON u.user_id=up.user_id WHERE up.status='active' AND u.status='authentic' AND u.gender='M' AND admin_approved!='yes'  ORDER BY time_of_saving  desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "photos-under-moderation-authm");
					break;
				
				case "photos-under-moderation-authf":
					$rs = $this->conn_slave->Execute ( "SELECT DISTINCT(up.user_id) FROM user_photo up JOIN user u ON u.user_id=up.user_id WHERE up.status='active' AND u.status='authentic' AND u.gender='F' AND admin_approved!='yes'  ORDER BY time_of_saving  desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "photos-under-moderation-authf");
					break;
				
				case "photos-under-moderation":
					$rs = $this->conn_slave->Execute ( "SELECT distinct(user_id) from user_photo  where status='active' and admin_approved!='yes' order by time_of_saving desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "photos-under-moderation");
					break;

				case "photos-approved":
					$rs = $this->conn_slave->Execute(" SELECT distinct(user_id) FROM user_photo where STATUS = 'active' and admin_approved='yes' group by user_id order by time_of_saving  DESC limit $lower_range,$upper_range");
					$this->smarty->assign("sortBy", "photos-approved");
					break;
					
				case "photos-all-rejected":
					$rs = $this->conn_slave->Execute(" select u.user_id, sum(if(up.status='rejected',1,0)) as rejected, sum(if(up.status='active',1,0)) as active  from user u left join user_photo up on u.user_id =up.user_id where u.status = 'authentic'  and up.status not in ('deleted','changed')  group by u.user_id having active=0 and rejected>0 order by up.approval_time desc limit $lower_range,$upper_range");
					$this->smarty->assign("sortBy", "photos-all-rejected");
					break;
					
				case "photoIds-under-moderation":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_id_proof ui where ui.status='under_moderation' order by ui.upload_time  desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "photoIds-under-moderation");
					break;

				case "photoIds-rejected":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_id_proof ui where ui.status='rejected' order by ui.upload_time  desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "photoIds-rejected");
					break;

				case "photoIds-fail":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_id_proof ui where ui.status='fail' order by ui.upload_time  desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "photoIds-fail");
					break;

				case "photoIds-active":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_id_proof ui where ui.status='active' order by ui.upload_time  desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "photoIds-active");
					break;


				case "employment-under-moderation":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.status='under_moderation' order by ui.upload_time desc  limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "employment-under-moderation");
					break;

				case "employment-rejected":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.status='rejected' order by ui.upload_time desc  limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "employment-rejected");
					break;

				case "employment-fail":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.status='fail' order by ui.upload_time desc  limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "employment-fail");
					break;

				case "employment-active":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.status='active' order by ui.upload_time desc  limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "employment-active");
					break;

				case "employment-verification":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.sent_for_verification=1 and ui.status = 'under_moderation' order by ui.verification_sent_time  limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "employment-verification");
					break;
						
				case "employment-pending-verification":
					$rs = $this->conn_slave->Execute ( "SELECT ui.user_id from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.sent_for_verification=0 order by ui.verification_sent_time  limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "employment-pending-verification");
					break;
						
				case "facebook":
					$rs = $this->conn_slave->Execute ( "SELECT user_id from user_facebook order by user_id desc limit $lower_range,$upper_range" );
					$this->smarty->assign("sortBy", "facebook");
					break;

				case "linkedin":
					$rs = $this->conn_slave->Execute("SELECT user_id from user_linkedin order by user_id desc limit $lower_range,$upper_range");
					$this->smarty->assign("sortBy", "linkedin");
					break;

				/* case "name":
					$name = $_REQUEST['value'];
					$sql = "SELECT user_id from user where fname like '$name' order by user_id desc limit $lower_range,$upper_range";
					$rs = $this->conn_slave->Execute($sql);
					$this->smarty->assign("sortBy", "name");
					break; */

				case "female-profiles":
					$rs = $this->conn_slave->Execute("SELECT user_id from user where gender = 'F' order by user_id desc limit $lower_range,$upper_range");
					$this->smarty->assign("sortBy", "female-profiles");
					break;

				case "abused-profiles":
					$rs = $this->conn_slave->Execute("SELECT abuse_id as user_id from report_abuse order by abuse_time desc limit $lower_range,$upper_range");
					$this->smarty->assign("sortBy", "abused-profiles");
					break;
				case "females_users_under_review":
					$rs=$this->conn_slave->Execute("SELECT review.user_id from user_under_review review join user u on u.user_id=review.user_id  where admin_approved is null and u.status!='blocked' order by user_id desc limit $lower_range,$upper_range");
					$this->smarty->assign("sortBy","females_users_under_review");
					$var="true";
					$this->smarty->assign("under_review",$var);
					break;
			}

			
			//if($sortBy!="photos-all-rejected"){
				//var_dump($rs);
 				$res = $rs->GetRows();
				//var_dump($res);
				foreach ($res as $r=>$e){
					$userIds[] = $e['user_id'];
				}
			//}
		
		}
		$attributes= $this->getuserdata($userIds,'Y');
		return ($attributes);
	}
	/* returns the attributes when searched for verification(new admin panel)   */
	public function getModerationData ($page_id=0, $modType , $user_id){
		// returns 30 profiles in one go
		
		$lower_range=($page_id-1)*30;
		$upper_range=30; 

		if (isset($user_id)){
			// if searched by user_id
			$type= is_numeric($user_id);
			if($user_id == "" || $type == false)
			 return  null ;
			else 
			$userIdList = $user_id;
		}
		else {
  switch($modType)
  {
	  // if searched by moderation type
	  case "photos-male":
  	    $sql = "select distinct(u.user_id) from user u join user_photo up on u.user_id=up.user_id join user_data ud on u.user_id = ud.user_id where ud.stay_country != 254 and up.status='active' and admin_approved!='yes' and u.status='authentic' and u.gender='M' limit ?,?" ;
  	    break;

	  case "photos-male-us":
  	    $sql = "select distinct(u.user_id) from user u join user_photo up on u.user_id=up.user_id join user_data ud on u.user_id = ud.user_id where ud.stay_country= 254 and  up.status='active' and admin_approved!='yes' and u.status='authentic' and u.gender='M' limit ?,?" ;
  	    break;
		 
	  case "photos-female":
	    $sql = "select distinct(u.user_id) from user u join user_photo up on u.user_id=up.user_id join user_data ud on u.user_id = ud.user_id where ud.stay_country!= 254 and up.status='active' and admin_approved!='yes' and u.status='authentic' and u.gender='F' limit ?,?" ;
		break;

	  case "photos-female-us":
		  $sql = "select distinct(u.user_id) from user u join user_photo up on u.user_id=up.user_id join user_data ud on u.user_id = ud.user_id  where ud.stay_country= 254 and up.status='active' and admin_approved!='yes' and u.status='authentic' and u.gender='F' limit ?,?" ;
		  break;

	  case "photo-ids":
		  $sql = "SELECT ui.user_id from user_id_proof ui join user_data ud on ui.user_id = ud.user_id where ud.stay_country != 254 and ui.status='under_moderation' order by ui.upload_time  desc limit ?, ?" ;
		  break;

	  case "photo-ids-us":
		  $sql = "SELECT ui.user_id from user_id_proof ui join user_data ud on ui.user_id = ud.user_id  where ud.stay_country= 254 and ui.status='under_moderation' order by ui.upload_time  desc limit ?, ?" ;
		  break;

        // in default returns the photos of authentic users (male and female)
		default :
		return ;
		break;
                 }
                  $res = Query_Wrapper::SELECT($sql, array($lower_range,$upper_range));
                 foreach ($res as $r=>$e){
                 	                      $userIds[] = $e['user_id'];
                                          }
           if(!empty($userIds)){
  	                              $userIdList = implode(',', $userIds);
                                }
		}   
		   if(isset($userIdList)) {
       // get count ofpending photos for male and female separately
		  /* $sql_verification= "select u.gender,count(t.photo_id)  as counts from (select user_id, photo_id  from user_photo up where up.status in ('active','rejected') and admin_approved ='no' ) t join user u on t.user_id = u.user_id and  u.status ='authentic'  group by u.gender";
		   $res_verification = $this->conn_slave->Execute($sql_verification);
		   $res_veri_arr=   $res_verification->GetRows();
		   foreach ($res_veri_arr as $row => $col){
		   	$veri[$col['gender']] = $col;
		   }*/
		      
		// get count pending under moderation ids
		   /*$sql_id_veri= "SELECT COUNT(up.user_id) AS COUNT, up.status FROM user_id_proof  up LEFT JOIN user u ON up.user_id = u.user_id WHERE  u.status != 'suspended'AND  u.email_id NOT LIKE '%trulymadly%' AND up.status = 'under_moderation'";
		   $res_id_veri= $this->conn_slave->Execute($sql_id_veri);
		   $res_id_arr = $res_id_veri-> FetchRow();*/
		   
		//  get attributes of user ids in $userIdList
           $sql_user = "SELECT u.user_id,fname, lname, gender,fid, DATE(ADDTIME((registered_at),'05:30:00')) AS registered_at,
						u.status, ud.DateOfBirth AS DOB,EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) as age,
						 gc.display_name AS city, gs.name AS state, ud.designation, ud.company_name, ud.institute_details
                        FROM user u LEFT JOIN user_data ud ON u.user_id=ud.user_id
						LEFT JOIN geo_state gs ON gs.state_id=ud.stay_state 
						LEFT JOIN geo_city gc ON ud.stay_city=gc.city_id AND gc.state_id=ud.stay_state
 						WHERE u.user_id IN ($userIdList) ORDER BY FIELD( u.user_id, $userIdList)";
           $res = $this->conn_slave->Execute($sql_user);
           $nameRows = $res->GetRows();
           $user = array();
           foreach ($nameRows as $row => $col)
		   {
				$user[$col['user_id']] = $col;
			   if(is_array(json_decode($col['company_name'])))
					$user[$col['user_id']]['company_name'] = implode(',',json_decode($col['company_name'])) ;
			   if(is_array(json_decode($col['institute_details'])))
				$user[$col['user_id']]['institute_details'] = implode(',',json_decode($col['institute_details'])) ;
           	}

           	 // select photos url with status, admin approval status and can_profile status                                 
           $sql_photo = "SELECT up.user_id, up.photo_id, up.name, up.mapped_to, CASE WHEN admin_approved='yes' THEN (CASE WHEN STATUS='active' THEN 'approved' ELSE 'rejected' END) 
                         ELSE (CASE WHEN STATUS='deleted' THEN 'deleted' ELSE 'under_moderation' END) END AS status_new , up.is_profile, up.admin_approved,up.can_profile FROM user_photo up 
                         WHERE up.status NOT IN ('changed','deleted') AND user_id IN ($userIdList)";
           $res_photo = $this->conn_slave->Execute($sql_photo);
           $photo_arr= array();
           $i=0;
           $new_pair;
           foreach ($res_photo as $row =>$col){
                if ($new_pair== $col['user_id'].$col['status_new'] )
                $i++ ;
                else 
                $i=0;   
           	$photo_arr[$col['user_id']][$col['status_new']][$i] = $col;
           	  $new_pair =  $col['user_id'].$col['status_new'];  
           	         
           }

          /*  foreach ($photo_arr as $row=> $col){
           	echo $row;
           	foreach ($col as $r => $c){
           		echo "second".$r;
           		 foreach ($c as $r1 => $c1){
           		 //	var_dump($c1);
    		 	echo $c1['photo_id'];
         		 	echo "<br>";
           		 }
           	}
           	echo "<br>";
           } */
           
            $sql_fb= "SELECT uf.user_id, uf.profile_url FROM user_facebook uf WHERE user_id IN ($userIdList)";
            $res_fb = $this->conn_slave->Execute($sql_fb);
            $fb_pic=array();
            $picarr = array();
            foreach ($res_fb as $key => $val){
				$picarr=json_decode($val['profile_url'], true);
				if(is_array($picarr))
				{
					if(isset($picarr[0]['image']) && $picarr[0]['image'] != null )
					{
						$fb_pic[$val['user_id']]= $picarr[0]['image'];
					}
					else
					{
						$fb_pic[$val['user_id']]= $picarr[0];
					}
				}
            }
            $sql_id = "SELECT ui.* from user_id_proof ui where ui.user_id in($userIdList)";
            $res_id = $this->conn_slave->Execute ( $sql_id );
            $id_data = $res_id->GetRows();
            $id_arr = array();
            foreach ($id_data as $r => $c){
            	$id_arr[$c['user_id']] = $c;
            	$fileType = end ( explode ( '.', $c ['img_location'] ) );
            	if (in_array ( $fileType, array (
            			'png',
            			'jpg',
            			'jpeg',
            			'gif',
            			'PNG',
            			'JPEG',
            			'JPG',
            			'GIF'
            	) )) {
            		$id_arr[$c['user_id']] ['img_flag'] = 'yes';
            	}
            }
           	                                  $attributes = array(
           	                                  		"name" => $user,
           	                                  		"photo"=> $photo_arr,
           	                                  		"fb_photo"=> $fb_pic,
           	                                  		"id" =>$id_arr,
           	                                  		//"photo_veri"=>$veri,
           	                                  		// "id_veri"=>$res_id_arr
           	                                  );

           	      return $attributes;
		   }
		   return null;
           	      
	}
	
	public function getRemainingModerationData()
	{
		// get count of pending photos for male and female separately
		   $sql_verification= "select u.gender,count(t.photo_id)  as counts from (select user_id, photo_id  from user_photo up where up.status in ('active','rejected') and admin_approved ='no' ) t join user u on t.user_id = u.user_id and  u.status ='authentic'  group by u.gender";
		   $res_verification = $this->conn_slave->Execute($sql_verification);
		   $res_veri_arr=   $res_verification->GetRows();
		   foreach ($res_veri_arr as $row => $col){
		   //	var_dump($col);
		   	$veri[$col['gender']] = $col["counts"];
		   }
		   if(count($veri)==0)
		   {
		   	$veri['M']=0;
		   	$veri['F']=0;
		   }
		// get count pending under moderation ids
		   $sql_id_veri= "SELECT COUNT(up.user_id) AS COUNT, up.status FROM user_id_proof  up LEFT JOIN user u ON up.user_id = u.user_id WHERE  u.status != 'suspended'AND  u.email_id NOT LIKE '%trulymadly%' AND up.status = 'under_moderation'";
		   $res_id_veri= $this->conn_slave->Execute($sql_id_veri);
		   $res_id_veri = $res_id_veri-> FetchRow();
		   $res_id_arr=array();
		   if(count($res_id_veri)>0)
		   {
		   	$res_id_arr['count']=$res_id_veri['COUNT'];
		   }
		   else
		   {
		   	$res_id_arr['count']=0;
		   }
		   $result ['status'] = 'Success';
		   $result ['rem_photos']=$veri;
		   $result ['rem_ids']=$res_id_arr;
		return $result;
	
	}
	
	public function getuserdata ($userIds,$eng){
        // var_dump($userIds); 
		 if(!empty($userIds)){
		 	
			//$userIdList = implode(',', $userIds);
		 	$paramCount = Query_Wrapper::getParamCount($userIds);
			$sql_user = "SELECT fname, lname, ADDTIME((ull.last_login),'05:30:00') as last_l, gender, date(ADDTIME((registered_at),'05:30:00')) as registered_at, "
						."status, is_fb_connected,fid, u.user_id, email_id,registered_from,deletion_status, u.device_id from user u join user_lastlogin ull on u.user_id=ull.user_id "
						."where u.user_id in ($paramCount)  order by field( u.user_id, $paramCount) ";
			//echo $sql_user;
			//$res = $this->conn_slave->Execute($sql_user);
			$param_array= array_merge($userIds,$userIds) ;
			$nameRows = Query_Wrapper::SELECT($sql_user, $param_array) ;
			//$nameRows = $res->GetRows();
			$users = array();
			//var_dump($nameRows); die;
			foreach ($nameRows as $row => $col){
				$user[$col['user_id']] = $col;
				//$userIds[] = $col['user_id'];
			}
			
			//echo $userIdList; die; 
		/*	$sql_work = $this->conn_slave->Prepare("SELECT * FROM user_work where user_id in($userIdList)");

			$sql_demography = $this->conn_slave->Prepare("SELECT ud.user_id, ud.DateOfBirth as age, gci.name as stay_city_display,gct.name as birth_city_display,  gstate.name as birth_state , gcountry.name as birth_country,gco.name as country, gs.name as state
						   FROM user_demography ud 
						   LEFT JOIN geo_state gstate on ud.birth_state = gstate.state_id
						   LEFT JOIN geo_country gcountry on ud.birth_country = gcountry.country_id 
						   LEFT JOIN geo_country gco on ud.stay_country = gco.country_id
						   LEFT JOIN geo_state gs on ud.stay_state = gs.state_id
   						   LEFT JOIN geo_city gci on ud.stay_city_display = gci.id 
						   LEFT JOIN geo_city gct on ud.birth_city_display = gct.id 
						   LEFT JOIN castes c on ud.caste = c.caste_id
						   where ud.user_id in ($userIdList)"); */
			
// 			$sql_userdata= $this->conn_slave->Prepare("SELECT ud.user_id, ud.DateOfBirth AS age, gc.display_name as city, gs.name AS state, gco.name AS country, i.name AS industry, ud.designation AS designation, ud.company_name AS company
// 					FROM user_data ud
// 					LEFT JOIN geo_state gs ON gs.state_id=ud.stay_state
// 					LEFT JOIN geo_country gco ON gco.country_id=ud.stay_country
// 					LEFT JOIN geo_city gc ON gc.city_id=ud.stay_city AND gc.state_id=ud.stay_state
// 					LEFT JOIN industries_new i on i.id=ud.industry
// 					WHERE ud.user_id in ($userIdList)");
			$sql_userdata= $this->conn_slave->Prepare("SELECT ud.user_id, ud.DateOfBirth AS age, gc.display_name as city, gs.name AS state, gco.name AS country, i.name AS industry, ud.designation AS designation, ud.company_name AS company
					FROM user_data ud
					LEFT JOIN geo_state gs ON gs.state_id=ud.stay_state
					LEFT JOIN geo_country gco ON gco.country_id=ud.stay_country
					LEFT JOIN geo_city gc ON gc.city_id=ud.stay_city AND gc.state_id=ud.stay_state
					LEFT JOIN industries_new i on i.id=ud.industry
					WHERE ud.user_id in ($paramCount)");
			//var_dump($sql_userdata); die;
			$sql_fb = "SELECT user_id, profile_url, fname, lname, gender, birthday, connection, relationship, connected_from from user_facebook where user_id in ($paramCount) ";
			$sql_linkedin = "SELECT user_id, name, connected_from from user_linkedin where user_id in ($paramCount)";
			$sql_photo= "select count(photo_id) as count,  case when admin_approved='yes' then (case when status='active' then 'approved' else 'rejected' end) "
					    ."else (case when status='deleted' then 'deleted' else 'under_moderation' end) end as status_new, user_id  from user_photo  "
					    ."where user_id IN ($paramCount) and status not in ('changed','deleted') group by status_new,user_id";
			//$rs = $this->conn_slave->Execute ( "select count(photo_id) as count,  case when admin_approved='yes' then (case when status='active' then 'approved' else 'rejected' end) else (case when status='deleted' then 'deleted' else 'under_moderation' end) end as status_new, user_id  from user_photo  where user_id IN ($userIdList) and status not in ('changed','deleted') group by status_new,user_id" );
			$rows = Query_Wrapper::SELECT($sql_photo, $userIds) ;
			$photo_arr = array();

			foreach ($rows as $row =>$col){
				//$photo_arr[$col['user_id']]['status'] = $col['status'];
				$photo_arr[$col['user_id']][$col['status_new']] = $col['count'];
			}

			/* $rsm = $this->conn_slave->Execute ( "SELECT count(up.photo_id) as count, up.status, up.user_id from user_photo up where up.user_id in ($userIdList)  and up.admin_approved='no' and up.status='active' group by user_id order by user_id" );
			$rowsm = $rsm->GetRows();
			foreach ($rowsm as $row =>$col){
				//	$photo_arr[$col['user_id']]['status'] = $col['status'];
				$photo_arr[$col['user_id']]['under_moderation'] = $col['count'];
			} */
			//var_dump($photo_arr);
			//$res = $this->conn_slave->Execute ( "SELECT ui.* from user_id_proof ui where ui.user_id in ($userIdList)" );
			$sql_id = "SELECT ui.* from user_id_proof ui where ui.user_id in ($paramCount)" ;
			$rows = Query_Wrapper::SELECT($sql_id, $userIds);
			$photoIds = array();
			foreach ($rows as $r => $c){
				$photoIds[$c['user_id']]['status'] = $c['status'];
			}
	
			//	echo $userIdList;
			//echo $sql_linkedin; die;
		//	$res_work = $this->conn_slave->Execute($sql_work);
			//$res_demo = $this->conn_slave->Execute($sql_demography);
			//$res_data= $this->conn_slave->Execute($sql_userdata);
			$datau= Query_Wrapper::SELECT($sql_userdata, $userIds) ;
			//$res_fb = $this->conn_slave->Execute($sql_fb);
			$fb_arr = Query_Wrapper::SELECT($sql_fb, $userIds) ;
			//$res_linkedin = $this->conn_slave->Execute($sql_linkedin);
			$linkedin = Query_Wrapper::SELECT($sql_linkedin, $userIds) ;
         //  var_dump($res_data); die;
			//$work = $res_work->GetRows();
			//$demo = $res_demo->GetRows();
// 			$datau= $res_data->GetRows();
// 			$fb_arr = $res_fb->GetRows();
// 			$linkedin = $res_linkedin->GetRows();
	//	var_dump($datau[0]['company']);

			//var_dump($fb_arr); die; 
		$data_arr = array();
		foreach ($datau as $row => $col){
			$data_arr[$col['user_id']] = $col;
			$company= json_decode($col['company']);
			$comarrey = implode(', ', $company);
			$data_arr[$col['user_id']]['company']=$comarrey;
		}

		$fb_data = array();
		$fb_pic=array();
		$picarr = array();
			foreach ($fb_arr as $key => $val){
				$fb_data[$val['user_id']] = $val;
				$picarr=json_decode($val['profile_url'], true);
				if(is_array($picarr))
				{
					if(isset($picarr[0]['image']) && $picarr[0]['image'] != null )
					{
						$fb_pic[$val['user_id']]= $picarr[0]['image'];
					}
					else
					{
						$fb_pic[$val['user_id']]= $picarr[0];
					}
				}
			}
			$linkedin_data = array();
			foreach ($linkedin as $key => $col){
				$linkedin_data[$col['user_id']] = $col;
			}

			/* $rs = $this->conn_slave->Execute ( "SELECT ui.* from user_address_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.user_id in ($userIdList)" );
			$rows = $rs->GetRows();
			$address = array();
			//var_dump($rows);
			foreach ($rows as $row =>$col){
				$address[$col['user_id']]['status'] = $col['status'];
			} */

			//var_dump($userIdList);
// 			$rs = $this->conn_slave->Execute ( "SELECT user_id, status from user_employment_proof  where user_id in ($userIdList)" );
// 			//$rs = $this->conn_slave->Execute ( "SELECT ui.* from user_employment_proof ui LEFT JOIN user_phone_number upn ON ui.user_id=upn.user_id where ui.user_id in ($userIdList)" );
// 			$rows = $rs->GetRows();
// 			$employment = array();
// 			//var_dump($rows);
// 			foreach ($rows as $row =>$col){
// 				$employment[$col['user_id']]['status'] = $col['status'];
// 			}
 
				
			//phone numbers
			
			//$rs = $this->conn_slave->Execute ( "SELECT user_number, a.user_id,a.status  from user_phone_number a  where a.user_id in ($userIdList)" );
			$sql_phone = "SELECT  a.user_id,a.status  from user_phone_number a  where a.user_id in ($paramCount)" ;
			//$rows = $rs->GetRows();  
			$rows = Query_Wrapper::SELECT($sql_phone, $userIds) ;
			$phones = array(); 
			//var_dump($rows);
			foreach ($rows as $row =>$col){
				if($col['status']!=NULL){
					$phones[$col['user_id']]['status']= $col['status'];
				}else{
					$phones[$col['user_id']]['status'] = "No Phone Number";
				}
			} 
			
		/*if ($eng=='Y') {	
// 		
 		$sql_like_hide = "select r.user1 as user_id, recs, likes, hides from
						  (select user1, count(user2) as recs from user_recommendation_rank_log where user1 in ($paramCount)  group by user1)r
						  left join (select user1 , count(user2) as likes from user_like where user1 in ($paramCount) group by user1)l on r.user1=l.user1
						  left join (select user1 , count(user2) as hides from user_hide where user1 in ($paramCount) group by user1)h on r.user1=h.user1 ";
 		$param_array= array_merge($userIds,$userIds,$userIds) ;
 		$engDataRes = $this->conn_reporting->Execute($this->conn_reporting->Prepare($sql_like_hide),$param_array);
		// $engdata = Query_Wrapper::SELECT($sql_like_hide, $param_array) ;
		//				  $engdata=$erows->GetRows();
		$engage= array();
		  while($col = $engDataRes->FetchRow())
		  {
		  	$engage[$col['user_id']] = $col;
		  	$engage[$col['user_id']]['status'] = 'Y';
		  }
	    
// 	     $mrows=$this->conn_reporting->Execute ("SELECT DISTINCT(u.user_id) as user_id, COUNT(uml.user1) as mutual FROM user u JOIN user_mutual_like uml  ON u.user_id=uml.user1 OR u.user_id=uml.user2
//                                              WHERE u.user_id IN ($userIdList) GROUP BY u.user_id");
	     $sql_mutual = "SELECT DISTINCT(u.user_id) as user_id, COUNT(uml.user1) as mutual FROM user u JOIN user_mutual_like uml  ON u.user_id=uml.user1 OR "
	     		       ." u.user_id=uml.user2 WHERE u.user_id IN ($paramCount) GROUP BY u.user_id" ;
	     $mutualdata = Query_Wrapper::SELECT($sql_mutual, $userIds) ;
	   //  $mutualdata=$mrows->GetRows();
	     foreach ($mutualdata as $row =>$col){
	     	$mutual[$col['user_id']] = $col;
	     	$mutual[$col['user_id']]['status'] = 'Y';
	     }	
	}*/

	
	
	$sql_user = "SELECT user_id ,group_concat(concat(`source`,':',`app_version_code`) separator ' ') as version 
			from user_gcm_current where user_id in ($paramCount) and status in ('login','logout') group by user_id";
	//echo $sql_user;
	$nameRows = Query_Wrapper::SELECT($sql_user, $userIds) ;
	//$res = $this->conn_slave->Execute($sql_user);
	//$nameRows = $res->GetRows();
	$versions = array();
	foreach ($nameRows as $row => $col){
		$versions[$col['user_id']] = $col;
	
	}

			$attributes = array(
		 "name" => $user,
		// "work" => $work_arr,
		// "demo" => $demo_arr,
		"data"=> $data_arr,
		 "fb" =>$fb_data,
		"fpic" =>$fb_pic,
		"linkedin" => $linkedin_data,
		"photos_count" => $photo_arr,
		"photoIds" => $photoIds,
		"employment" => $employment,
		"address" => $address,
		"phones" => $phones,
		//"engage"=> $engage,
		"versions"=>$versions,	
		//"mutual"=> $mutual
		
			/*"other_qualification" => $other_education,*/
			);
			//var_dump($attributes[phones]);
			return $attributes;
		}

		else{
			return null;
		}
		/*echo '<pre>';
		 var_dump($attributes);die;*/
		/*
		 echo '<pre>';
		 var_dump($demo);die;*/
		/*	echo '<pre>';
		 var_dump($educationDetails);die;*/
		//if($mutual_connections!=null) $attributes = array_merge($attributes, $mutual_connections);
		//var_dump($attributes);//die;
		/*	echo '<pre>';
		var_dump($industryDetails); die;*/

	}
	
	public function getPhoneNumber($user_id)
	{
		try {
			$admin_log = new logging();
			$step="Phone Number";
			if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
					
				$changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);
					
				if($changes >=static::$admin_phone_limit ){
					$result ['status'] = 'Limit exceeded';
					return   $result ;
				}
				$sql_phone = "SELECT user_number, a.user_id,a.status  from user_phone_number a  where a.user_id =?" ;
				//$rows = $rs->GetRows();
				$rows = Query_Wrapper::SELECT($sql_phone, $user_id) ;
				$phones = array();
				//var_dump($rows);
				foreach ($rows as $row =>$col){
					if($col['user_number']!=NULL){
						$phones['phone'] = $col['user_number'];
						$phones['status']= $col['status'];
					}else{
						$phones['phone'] = NULL;
					}
				}
				$result['status']='Success';
				$result['phones']=$phones;
				$query["sql"] = "SELECT user_number, a.user_id,a.status  from user_phone_number a  where a.user_id =?";
				$query["parameter"] = "'$user_id'";
				$query = json_encode($query);
			
				$txt="Get_phone query";
			    
				$admin_log->log($_SESSION['admin_id'],$user_id,$query,$step,$txt);
				$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
			    
				return $result;
			
			}
			else
			{
				$result['status']="No access rights";
				return $result;
			}
		
	     } catch (Exception $e) {
		 $result['error']=$e->getMessage();
		 return $result;
	    }
	}


	public function getSparkStats($userId)
	{
		$data = array();
		$sql = "Select ust.price ,sp.type from user_spark_transaction ust join spark_packages sp on ust.package_id = sp.package_id WHERE ust.user_id =? and ust.status='consumed'
				order by ust.tstamp desc limit 1";
		$sql_prep = $this->conn_reporting->Prepare($sql);
		$sql_exec = $this->conn_reporting->Execute($sql_prep, array($userId));
		$data = $sql_exec->FetchRow();

		$sql = "Select count(*) as all_sent, sum(if(status='accepted',1,0)) as accepted from user_spark us WHERE us.user1 = ?" ;
		$sql_prep = $this->conn_reporting->Prepare($sql);
		$sql_exec = $this->conn_reporting->Execute($sql_prep, array($userId));
		$data_spark = $sql_exec->FetchRow();

		$sql_rem = "select user_id, spark_count from user_spark_counter where user_id = ?" ;
		$sql_prep = $this->conn_reporting->Prepare($sql_rem);
		$sql_exec = $this->conn_reporting->Execute($sql_prep, array($userId));
		$remaining = $sql_exec->FetchRow();

		$data['sparks_sent'] = $data_spark['all_sent'];
		$data['accepted'] = $data_spark['accepted'];
		$data['remaining'] = $remaining['spark_count'];
		$data['status'] = 'Success';
		//var_dump($data);
		return $data;
	}
	
	public function getEngagement($userId, $no_of_days=0)
	{
		if($no_of_days> 0 && is_numeric($no_of_days))
			$date_for_engagement = date ( 'Y-m-d H:i:s' ,strtotime ( '-' . $no_of_days .' days' ) );
		else
			$date_for_engagement = date ( 'Y-m-d H:i:s' ,strtotime ( '-1000 days' ) );
		
		$sql_recs = "Select sum(recs) as recs from (
						select user1, count(user2) as recs from user_recommendation_rank_log where user1 =?  and tstamp > '$date_for_engagement' group by user1
                     		 UNION
                     	select user1, count(user2) as recs from user_recommendation_rank_log_old where user1 =? and tstamp > '$date_for_engagement' group by user1)t";

		$param_array= array($userId,$userId);
		$recs_res = $this->conn_reporting->Execute($this->conn_reporting->Prepare($sql_recs),$param_array);
		$recs = $recs_res->FetchRow();

		$result = array();
		$result['user_id'] = $userId;
		$result['recs'] = $recs['recs'];


		$sql_like = "Select count(*) as likes from user_like where user1= ? and timestamp>'$date_for_engagement'";
		$like_res = $this->conn_reporting->Execute($this->conn_reporting->Prepare($sql_like),array($userId));
		$likes = $like_res->FetchRow();
		$result['likes'] = $likes['likes'];

		$sql_hide = "Select count(*) as hides from user_hide where user1= ? and timestamp>'$date_for_engagement'";
		$hide_res = $this->conn_reporting->Execute($this->conn_reporting->Prepare($sql_hide),array($userId));
		$hides = $hide_res->FetchRow();
		$result['hides'] = $hides['hides'];

		$sql_mutual = "Select count(*) as cu from user_mutual_like WHERE (user1= ? or user2 = ?) and timestamp > '$date_for_engagement'";
		$mutual_res = $this->conn_reporting->Execute($this->conn_reporting->Prepare($sql_mutual),array($userId, $userId));
		$mutual = $mutual_res->FetchRow();
		$result['mutual'] = $mutual['cu'];
		$result['status'] = 'Success';

		return $result;
		
	}
	
	public function getMutualLike($userIds)
	{
		$sql_mutual = "SELECT DISTINCT(u.user_id) as user_id, COUNT(uml.user1) as mutual FROM user u JOIN user_mutual_like uml  ON u.user_id=uml.user1 OR "
				." u.user_id=uml.user2 WHERE u.user_id IN ($paramCount) GROUP BY u.user_id" ;
				$mutualdata = Query_Wrapper::SELECT($sql_mutual, $userIds) ;
				foreach ($mutualdata as $row =>$col){
					$mutual[$col['user_id']] = $col;
					$mutual[$col['user_id']]['status'] = 'Y';
				}
		return $mutual;
	}
	
	public function changes_done_by_admin($id,$step){
		$date=date('Y-m-d 00:00:00');
		//$sql = "SELECT count(*) as total FROM admin_log where id = $id and tstamp>='$date' and step In ('$step')";
		$sql = "SELECT count(*) as total FROM admin_log where id = ? and tstamp>= ? and step In ('$step')";
		$row = Query_Wrapper::SELECT($sql, array($id, $date), Query_Wrapper::$__slave,true);
		$result = $row['total'] ; 
		return $result; 
	}
	
	public function admin_change_done_mail($id,$sub,$txt){
			//$sql = "SELECT user_name FROM admin where id = $id";
			$sql = "SELECT user_name FROM admin where id = ?";
			$row = Query_Wrapper::SELECT($sql, array($id));
			$txt = $txt." by ".$row['user_name'];
			$to="smita.prakash@trulymadly.com";
			//$to="anumod@trulymadly.com";
			$from="admin@trulymadly.com"; 
			
			Utils::sendEmail($to,$from,$sub,$txt);
			
	}   

	
	//List of Users Blocked by a specific user !!!
	public function getBlockList($user_id) {
		$data = NULL;
		$uu= new UserUtils();
		/*$rs=$this->conn_slave->Execute("SELECT u.fname,u.user_id,ms.blocked_by FROM messages_queue ms JOIN user u
				ON ms.receiver_id=u.user_id  WHERE (sender_id=$user_id ) AND ms.blocked_by IS NOT NULL
				UNION
				SELECT u.fname,u.user_id,ms.blocked_by FROM messages_queue ms JOIN user u
				ON ms.sender_id=u.user_id WHERE (receiver_id=$user_id  ) AND ms.blocked_by IS NOT NULL");
		*/		
// 		 $rs=$this->conn_slave->Execute("SELECT u.fname,u.user_id,ms.blocked_by,t1.reason, t1.block_time FROM messages_queue ms 
// 		                                 JOIN user u ON ms.receiver_id=u.user_id  
// 		                                 LEFT JOIN (select sender_id, abuse_id, reason, ADDTIME((abuse_time),'05:30:00') as block_time from report_abuse where sender_id=$user_id or abuse_id=$user_id)t1 
// 		                                 on (CASE when ms.blocked_by=$user_id then t1.abuse_id=ms.receiver_id else t1.sender_id=ms.receiver_id END) 
// 		                                 WHERE (ms.sender_id=$user_id) AND ms.blocked_by IS NOT NULL 
// 		                                 UNION 
// 		                                 SELECT u.fname,u.user_id,ms.blocked_by,t2.reason, t2.block_time FROM messages_queue ms 
// 		                                 JOIN user u ON ms.sender_id=u.user_id 
// 		                                 LEFT JOIN (select sender_id, abuse_id, reason, ADDTIME((abuse_time),'05:30:00') as block_time from report_abuse where sender_id=$user_id or abuse_id=$user_id)t2 
// 		                                 on (CASE when ms.blocked_by=$user_id then t2.abuse_id=ms.sender_id else t2.sender_id=ms.sender_id END) 
// 		                                 WHERE (ms.receiver_id=$user_id ) AND ms.blocked_by IS NOT NULL
// 		                                 ");
		 $sql_block = "SELECT u.fname,u.user_id,ms.blocked_by,t1.reason, t1.block_time FROM messages_queue ms
 						JOIN user u ON ms.receiver_id=u.user_id
						 LEFT JOIN (select sender_id, abuse_id, reason,ADDTIME((abuse_time),'05:30:00') as block_time from report_abuse where sender_id=? )t1
						 on  t1.abuse_id=ms.receiver_id
						 WHERE (ms.sender_id=?) and ms.blocked_by= ms.sender_id
						 UNION
						  SELECT u.fname,u.user_id,ms.blocked_by,t2.reason, t2.block_time FROM messages_queue ms
						 JOIN user u ON ms.receiver_id=u.user_id
						 LEFT JOIN (select sender_id, abuse_id, reason, ADDTIME((abuse_time),'05:30:00') as block_time from report_abuse where  abuse_id=?)t2
						 on  t2.sender_id =ms.receiver_id
						 WHERE ms.sender_id = ? AND ms.blocked_by = ms.receiver_id ";
		 $data = Query_Wrapper::SELECT($sql_block, array($user_id,$user_id,$user_id,$user_id));
	   
				$i=0;
				foreach ($data as $value) {
					$data[$i]['5']=$uu->generateMessageLink($user_id, $value['user_id']);
					$data[$i]['link_to_conversation']=$uu->generateMessageLink($user_id, $value['user_id']);
					$i++;
				}
				return $data;
	}
	
	// Function to unblock two specific users !!
	public function unblock($user_id_1,$user_id_2){ 
	
		$admin_log = new logging();
		$step = "Unblocked user";
		
		$changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);
	
		try{
			 
			if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
				if($changes >=static::$admin_change_limit ){
					$result ['status'] = 'Limit exceeded';
					return   $result ;
				}
				
				$query="update messages_queue set is_blocked_shown=NULL,blocked_by=NULL
					where  (sender_id=? and receiver_id=?) or
					 (receiver_id=? and sender_id=?)";
				$param_array=array($user_id_1,$user_id_2,$user_id_1,$user_id_2);
				$tablename='messages_queue';
				Query_Wrapper::UPDATE($query, $param_array, $tablename);
				
				
				/*$sql="update messages_queue set is_blocked_shown=NULL,blocked_by=NULL
					where  (sender_id=? and receiver_id=?) or
					 (receiver_id=? and sender_id=?)";
					
				$this->conn_master->Execute($this->conn_master->Prepare($sql),
						array($user_id_1,$user_id_2,$user_id_1,$user_id_2));*/  
				
				$query="update user_mutual_like set blocked_by=NULL,blocked_shown=NULL
					where ( (user1=? and user2=?)or(user1=? and user2=?) )";
				$param_array=array($user_id_1,$user_id_2,$user_id_2,$user_id_1);
				$tablename='user_mutual_like';
				Query_Wrapper::UPDATE($query, $param_array, $tablename);
				
				/*$sql="update user_mutual_like set blocked_by=NULL,blocked_shown=NULL
					where ( (user1=? and user2=?)or(user1=? and user2=?) )";
	
				$this->conn_master->Execute($this->conn_master->Prepare($sql),
						array($user_id_1,$user_id_2,$user_id_2,$user_id_1));*/
				
				
				$query["sql"] = "update user_mutual_like set blocked_by=NULL,blocked_shown=NULL
					where ( (user1=? and user2=?)or(user1=? and user2=?) )";
				$query["parameter"] = "'$user_id_1'.','.'$user_id_2'. ',' .'$user_id_2'.','.'$user_id_1'";
				$query = json_encode($query);
	
				$txt="User id ".$user_id_1." blocked user id ".$user_id_2." has been unset ";
	
				$admin_log->log($_SESSION['admin_id'],$user_id_1,$query,$step,$txt);
				$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
			}
			$result ['status'] = 'Success';
			$result ['no_changes_allowed'] = static::$admin_change_limit-$changes-1; // -1 for ongoing change
			return $result;
		}
		catch ( Exception $e ) {
			$result ['status'] = $e->getMessage ();
			return  $result ;
		}
	}
 
	
	
	// Function to deem reason inoffensive!!
	public function inoffensive($user_id_1,$user_id_2){
		$admin_log = new logging();
		$step = "Inoffensive report abuse";
	
		$changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);
	
		try{
			if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
					
				$query="update report_abuse set not_offensive=1
					    where  (sender_id=? and abuse_id=?)";
				$param_array=array($user_id_1,$user_id_2);
				$tablename='report_abuse';
				Query_Wrapper::UPDATE($query, $param_array, $tablename);
				
				/*$sql="update report_abuse set not_offensive=1
					where  (sender_id=? and abuse_id=?)";
					
				$this->conn_master->Execute($this->conn_master->Prepare($sql),
						array($user_id_1,$user_id_2));*/
	
				$query["sql"] = "update report_abuse set not_offensive=1
					             where  (sender_id=? and abuse_id=?)";
				$query["parameter"] = "'$user_id_1'.','.'$user_id_2'";
				$query = json_encode($query);
	
				$txt="User id ".$user_id_1." blocked user id ".$user_id_2." reason has been deemed not inoffensive ";
	
				$admin_log->log($_SESSION['admin_id'],$user_id_1,$query,$step,$txt);
				$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
			}
			$result ['status'] = 'Success';
			return $result;
		}
		catch ( Exception $e ) {
			$result ['status'] = $e->getMessage ();
			return  $result ;
		}
	}
	
	
    public function editNameOfUser ($data){
    	$admin_log = new logging();
    	$step = "Edited name";     
//    	$sql = "SELECT fname,lname from user where user_id=".$data['user_id'];
    	$sql = "SELECT fname,lname from user where user_id= ? ";
//     	$res = $this->conn->Execute($sql);
// 		$old_details = $res->FetchRow();
		$old_details = Query_Wrapper::SELECT($sql, array($data['user_id']),Query_Wrapper::$__slave,true);
    	$changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);  
    	try{
    		if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
    			if($changes >=static::$admin_change_limit ){
    				$result ['status'] = 'Limit exceeded';
    				return   $result ;
    			}
    			$query="UPDATE user SET fname=?,lname=? where user_id = ?";
    			$param_array=array($data['fname'],$data['lname'],$data['user_id']);
    			$tablename='user';
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			
    			/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user SET fname=?,lname=? where user_id = ?"),
    					array($data['fname'],$data['lname'],$data['user_id']));*/
    			
    		}
    	
    		  if(isset($_SESSION['admin_id'])){
    			$p1 = $data['action'];
    			$p2 = $data['user_id'];
    			$query["sql"] = "UPDATE user SET fname=?,lname=? where user_id=?";
    			$query["parameter"] = "'$p1'.','.'$p2'";
    			$query = json_encode($query);
    			 
    			$txt="Name of user id".$data['user_id']." is edited to ".$data['fname']." ".$data['lname'] ." from ".$old_details['fname']." ".$old_details['lname'];
    			
    			$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query,$step,$txt);
    			$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
    		}  
    		$result ['status'] = 'Success';
    		$result ['no_changes_allowed'] = static::$admin_change_limit-$changes-1;  // -1 for ongoing change
    		return   $result ;
    		
    	}
    	catch ( Exception $e ) {
    		$result ['status'] = $e->getMessage ();
    		return  $result ;
    	}	
    }
    
    public function editBdayOfUser ($data){
    	$admin_log = new logging();
    	$step = "Edited Birthday";
    	
    	$sql = "SELECT DateOfBirth from user_data where user_id= ?";
	//	$res = $this->conn->Execute($sql);
	//	$old_details = $res->FetchRow();
		$old_details = Query_Wrapper::SELECT($sql, array($data['user_id']),Query_Wrapper::$__slave, true);


		$changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);
    	try{
    		if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
    			
    			if($changes >=static::$admin_change_limit ){
    				$result ['status'] = 'Limit exceeded';
    				return   $result ;
    			}
    			
    			$query="UPDATE user_data SET DateOfBirth=? where user_id = ?";
    			$param_array=array($data['bdate'],$data['user_id']);
    			$tablename="user_data";
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			
    			/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user_data SET DateOfBirth=? where user_id = ?"),
    					array($data['bdate'],$data['user_id']));*/
    			
    		}
    	
    		  if(isset($_SESSION['admin_id'])){
    			$p1 = $data['action'];
    			$p2 = $data['user_id'];
    			$query["sql"] = "UPDATE user SET fname=?,lname=? where user_id=?";
    			$query["parameter"] = "'$p1'.','.'$p2'";
    			$query = json_encode($query);
    			$txt="Birthday of user id".$data['user_id']." is edited to ".$data['bdate']." from ".$old_details['DateOfBirth'];
    			$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query,$step,$txt);
    			$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
    		}  
    		$result ['status'] = 'Success';
    		$result ['no_changes_allowed'] = static::$admin_change_limit-$changes-1;// -1 for ongoing change
    		return   $result ;
    		 
    	}
    	catch ( Exception $e ) {
    		$result ['status'] = $e->getMessage ();
    		return  $result ;
    	} 	
    }
    
    
    public function editphoneNumberOfUser ($data){
    	$admin_log = new logging();
    	$step = "Edited Phone Number";
    	
    	$sql = "SELECT user_number from user_phone_number where user_id=?";
		$old_details = Query_Wrapper::SELECT($sql, array($data['user_id']),Query_Wrapper::$__slave, true);

	//	$res = $this->conn->Execute($sql);
	//	$old_details = $res->FetchRow();
    	
    	$changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);
    	try{
    		if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
    			
    			if($changes >=static::$admin_change_limit ){
    				$result ['status'] = 'Limit exceeded';
    				return   $result ;
    			}
    			
    			/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user SET phone_number=? where user_id = ?"),
    					array($data['phoneNumber'],$data['user_id']));
    			*/
    			$query="UPDATE user_phone_number SET user_number=? where user_id = ?";
    			$param_array=array($data['phoneNumber'],$data['user_id']);
    			$tablename='user_phone_number';
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			 
    			/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user_phone_number SET user_number=? where user_id = ?"),
    					array($data['phoneNumber'],$data['user_id']));*/
    			
    			$phno_for_trust_score=substr_replace($data['phoneNumber'],"XXXXX",5,5);
    			//var_dump($phno_for_trust_score); 
    			
    			$query="UPDATE user_trust_score SET mobile_number=? where user_id = ? AND mobile_number is not null";
    			$param_array=array($phno_for_trust_score,$data['user_id']);
    			$tablename='user_trust_score';
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			 
    			/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user_trust_score SET mobile_number=? where user_id = ? AND mobile_number is not null"),
    					array($phno_for_trust_score,$data['user_id']));*/
    			  
    			 
    		}
    	
    		  if(isset($_SESSION['admin_id'])){
    			$p1 = $data['action'];
    			$p2 = $data['user_id'];
    			$query["sql"] = "UPDATE user SET fname=?,lname=? where user_id=?";
    			$query["parameter"] = "'$p1'.','.'$p2'";
    			$query = json_encode($query);
    			$txt="Phone Number of user id".$data['user_id']." is edited to ".$data['phoneNumber']." from ".$old_details['user_number'];
    			$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query,$step,$txt);
    			$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
    		}  
    		$result ['status'] = 'Success';
    		$result ['no_changes_allowed'] = static::$admin_change_limit-$changes-1;// -1 for ongoing change
    		return   $result ;
    		
    	}
    	catch ( Exception $e ) {
    		$result ['status'] = $e->getMessage ();
    		return  $result ;
    	}	
    }
    
    
    public function editGenderOfUser ($data){
    	$admin_log = new logging();
    	$step = "Edited Gender";
    	
    	$sql = "SELECT gender from user where user_id=?";
		$old_details = Query_Wrapper::SELECT($sql, array($data['user_id']),Query_Wrapper::$__slave, true);
	//	$res = $this->conn->Execute($sql);
	//	$old_details = $res->FetchRow();
    	
    	$changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);
    	try{
    		if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
    			
    			if($changes >=static::$admin_change_limit ){
    				$result ['status'] = 'Limit exceeded';
    				return   $result ;
    			}
    			$query="UPDATE user SET gender=? where user_id = ?";
    			$param_array=array($data['gender'],$data['user_id']);
    			$tablename='user';
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			 
    			/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user SET gender=? where user_id = ?"),
    					array($data['gender'],$data['user_id']));*/
    			
    			$query="UPDATE user_search SET gender=? where user_id = ?";
    			$param_array=array($data['gender'],$data['user_id']);
    			$tablename='user_search';
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			 
    			/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user_search SET gender=? where user_id = ?"),
    					array($data['gender'],$data['user_id']));*/
    			
    		}
    	
    		  if(isset($_SESSION['admin_id'])){
    			$p1 = $data['action'];
    			$p2 = $data['user_id'];
    			$query["sql"] = "UPDATE user SET gender=? where user_id=?";
    			$query["parameter"] = "'$p1'.','.'$p2'";
    			$query = json_encode($query);
    			$txt="Gender of user id".$data['user_id']." is edited to ".$data['gender']." from ".$old_details['gender'];
    			$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query,$step,$txt);
    			$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
    		}  
    		$result ['status'] = 'Success';
    		$result ['no_changes_allowed'] = static::$admin_change_limit-$changes-1;// -1 for ongoing change
    		return   $result ;
    		
    	}
    	catch ( Exception $e ) {
    		$result ['status'] = $e->getMessage ();
    		return  $result ;
    	}	
    }
     
    
    public function employmentPaid($data){
    	$admin_log = new logging();
    	try{
    		//echo $data['user_id']; die;
    		$result1 = $this->getEmploymentProofs ($data['user_id']);
    		//var_dump($result1); die;
    		
    		$query="UPDATE user_".$data['type']."_proof SET is_paid = 'Y' where user_id = ?";
    		$param_array=array($data['user_id']);
    		$tablename="user_".$data['type']."_proof";
    		Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    		 
    		/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user_".$data['type']."_proof SET is_paid = 'Y' where user_id = ?"),
    				array($data['user_id']));*/
    	
    		$query="insert into user_employment_proof_log (user_id,verification_tracking_number, proof_type, img_location , company_name, emp_id, work_status, number_of_trials,sent_for_verification, verification_sent_time,admin_id, is_paid, tstamp)
    				values(?,?,?,?,?,?,?,?,?,?,?,'Y',now() )";
    		$param_array=array(  $data ['user_id'],
    						$result1[0]['ref_no'],
    						$result1[0]['proof_type'],
    						$result1[0]['img_location'],
    						$result1[0]['company_name'],
    						$result1[0]['emp_id'],
    						$result1[0]['work_status'],
    						$result1[0]['number_of_trials'],
    						$result1[0]['sent_for_verification'],
    						$result1[0]['verification_sent_time'],
    						$_SESSION['admin_id']
    				);
    		$tablename="user_employment_proof_log";
    		Query_Wrapper::INSERT($query, $param_array, $tablename,'master',$data['user_id']);
    		
    		/*$this->conn_master->execute ( $this->conn_master->prepare ( "insert into user_employment_proof_log (user_id,verification_tracking_number, proof_type, img_location , company_name, emp_id, work_status, number_of_trials,sent_for_verification, verification_sent_time,admin_id, is_paid, tstamp)
    				values(?,?,?,?,?,?,?,?,?,?,?,'Y',now() )" ),
    				array(  $data ['user_id'],
    						$result1[0]['ref_no'],
    						$result1[0]['proof_type'],
    						$result1[0]['img_location'],
    						$result1[0]['company_name'],
    						$result1[0]['emp_id'],
    						$result1[0]['work_status'],
    						$result1[0]['number_of_trials'],
    						$result1[0]['sent_for_verification'],
    						$result1[0]['verification_sent_time'],
    						$_SESSION['admin_id']
    				) );*/
    	
    	
    		if(isset($_SESSION['admin_id'])){
    			$p2 = $data['user_id'];
    			$query["sql"] = "UPDATE user_".$data['type']."_proof SET is_paid = 'Y' where user_id = ?";
    			$query["parameter"] = "'$p2'";
    			$query = json_encode($query);
    			$step = "authbridge_verification";
    	
    			$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query,$step);
    		}
    	
    		$result ['status'] = 'Success';
    		return  $result ;
    	}
    	catch ( Exception $e ) {
    		$result ['status'] = $e->getMessage ();
    		return  $result ;
    	}    	
    }

    public function authbridgeVerification($data){
    	$admin_log = new logging();
    	try{
    		
    		$result1 = $this->getEmploymentProofs ($data['user_id']);
    		//var_dump($result1); die;
    		
    		$query="UPDATE user_".$data['type']."_proof SET sent_for_verification = 1 , verification_sent_time = now() , verification_tracking_number = ? where user_id = ?";
    		$param_array=array($data['ref'],$data['user_id']);
    		$tablename="user_".$data['type']."_proof";
    		Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    		
    		/*$this->conn_master->Execute($this->conn_master->Prepare("UPDATE user_".$data['type']."_proof SET sent_for_verification = 1 , verification_sent_time = now() , verification_tracking_number = ? where user_id = ?"),
    				array($data['ref'],$data['user_id']));*/
    	
    		$query="insert into user_employment_proof_log (user_id,verification_tracking_number, proof_type, img_location, is_paid, company_name, emp_id, work_status, number_of_trials, admin_id, sent_for_verification, tstamp, verification_sent_time)
    				values(?,?,?,?,?,?,?,?,?,?,1, now(),now() )";
    		$param_array=array(  $data ['user_id'],
    						$data['ref'],
    						$result1[0]['proof_type'],
    						$result1[0]['img_location'],
    						$result1[0]['is_paid'],
    						$result1[0]['company_name'],
    						$result1[0]['emp_id'],
    						$result1[0]['work_status'],
    						$result1[0]['number_of_trials'],
    						$_SESSION['admin_id']
    				) ;
    		$tablename="user_employment_proof_log";
    		Query_Wrapper::INSERT($query, $param_array, $tablename,'master',$data['user_id']);
    		
    		/*$this->conn_master->execute ( $this->conn_master->prepare ( "insert into user_employment_proof_log (user_id,verification_tracking_number, proof_type, img_location, is_paid, company_name, emp_id, work_status, number_of_trials, admin_id, sent_for_verification, tstamp, verification_sent_time)
    				values(?,?,?,?,?,?,?,?,?,?,1, now(),now() )" ),
    				array(  $data ['user_id'],
    						$data['ref'],
    						$result1[0]['proof_type'],
    						$result1[0]['img_location'],
    						$result1[0]['is_paid'],
    						$result1[0]['company_name'],
    						$result1[0]['emp_id'],
    						$result1[0]['work_status'],
    						$result1[0]['number_of_trials'],
    						$_SESSION['admin_id']
    				) );*/
    	
    		if(isset($_SESSION['admin_id'])){
    			$p1 = $data['ref'];
    			$p2 = $data['user_id'];
    			$query["sql"] = "UPDATE user_".$data['type']."_proof SET sent_for_verification = 1 , verification_sent_time = now()  , verification_tracking_number = ? where user_id = ?";
    			$query["parameter"] = "'$p1'.','.'$p2'";
    			$query = json_encode($query);
    			$step = "authbridge_verification";
    	
    			$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query,$step); 
    		}
    	
    		$result ['status'] = 'Success';
    		return   $result ;
    	}
    	catch ( Exception $e ) {
    		$result ['status'] = $e->getMessage ();
    		return   $result ;
    	}
    }
    
    
    private function checkAge($dob)
    {
    	$flag=0;
    	$dob=explode("-", $data['dob']);
    	$today=date("Y-m-d");
    	$today=explode("-", $today);
    	
    	if($today[0]-$dob[0]<18)
    	{ $flag=1;
    	}
    	else if($today[0]-$dob[0]==18)
    	{
    		if($today[1]-$dob[1]<0)
    		{	$flag=1;}
    		else if($today[1]-$dob[1]==0)
    		{
    			if($today[2]-$dob[2]<0)
    			{$flag=1;}
    		}
    	
    	}
    	if($flag==1)
    	  return true;
    	else 
    	  return false; 
    }
    
    public function verifyId($data){
    	$admin_log = new logging();
    	try {
    	
    		$data ['fname']= trim($data ['fname']);
    		$data ['lname']= trim($data ['lname']);
    	
    		$data ['fname']= ucfirst($data ['fname']);
    		$data ['lname']= ucfirst($data ['lname']);
    		$result1 = $this->getIdProofs ($data['userId']);
    		if($data['status'] == 'rejected'){
    	
    		//checking age >=18	
    			if(isset($data['dob']))
    			{
    			    if ($this->checkAge($data['dob']))
    			    {	$result['error']='Age less than 18 years.';	
    			    	return $result;
    			    }  			   
    			}
    			
    			$query="UPDATE user_id_proof SET status=?,reject_reason=?,fname=?, lname=?,dateofbirth=?, current_action_time=now() where user_id=?";
    			$param_array= array (
    					$data ['status'],
    					$data ['reason'],
    					$data ['fname'],
    					$data ['lname'],
    					$data ['dob'],
    					$data ['userId']
    						
    			) ;
    			$tablename="user_id_proof";
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			
    				
    			/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user_id_proof SET status=?,reject_reason=?,fname=?, lname=?,dateofbirth=?, current_action_time=now() where user_id=?" ), array (
    					$data ['status'],
    					$data ['reason'],
    					$data ['fname'],
    					$data ['lname'],
    					$data ['dob'],
    					$data ['userId']
    						
    			) );*/
    			//var_dump($result1); die;
    			
    			$query="insert into  user_id_proof_log  (user_id, status, reject_reason,fname,lname, dateofbirth, proof_type, img_location, number_of_trials,admin_id, tstamp)
    					values (?,?,?,?,?,?,?,?,?,?,now() )";
    			$param_array= array( $data ['userId'],
    							$data ['status'],
    							$data ['reason'],
    							$data ['fname'],
    							$data ['lname'],
    							$data ['dob'],
    							$result1 [0]['proof_type'],
    							$result1 [0]['img_location'],
    							$result1 [0]['number_of_trials'],
    							$_SESSION['admin_id']
    	
    					);
    			$tablename="user_id_proof_log";
    			Query_Wrapper::INSERT($query, $param_array, $tablename,'master',$data['user_id']);
    			 
    			/*$this->conn_master->execute ( $this->conn_master->prepare ( "insert into  user_id_proof_log  (user_id, status, reject_reason,fname,lname, dateofbirth, proof_type, img_location, number_of_trials,admin_id, tstamp)
    					values (?,?,?,?,?,?,?,?,?,?,now() )" ),
    					array( $data ['userId'],
    							$data ['status'],
    							$data ['reason'],
    							$data ['fname'],
    							$data ['lname'],
    							$data ['dob'],
    							$result1 [0]['proof_type'],
    							$result1 [0]['img_location'],
    							$result1 [0]['number_of_trials'],
    							$_SESSION['admin_id']
    	
    					) );*/
    		}
    		else{
    			$query="UPDATE user_id_proof SET status=?,reject_reason=null, current_action_time=now() where user_id=?";
    			$param_array= array (
    					$data ['status'],
    					$data ['userId']
    			) ;
    			$tablename="user_id_proof";
    			Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    			
    			/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user_id_proof SET status=?,reject_reason=null, current_action_time=now() where user_id=?" ), array (
    					$data ['status'],
    					$data ['userId']
    			) );*/
    			$result1 = $this->getIdProofs ($data['userId']);
    			
    			$query="insert into  user_id_proof_log  (user_id, status, proof_type, img_location,number_of_trials,admin_id, tstamp)
    					values (?,?,?,?,?,?,now() )";
    			$param_array= array( $data ['userId'],
    							$data ['status'],
    							$result1 [0]['proof_type'],
    							$result1 [0]['img_location'],
    							$result1 [0]['number_of_trials'],
    							$_SESSION['admin_id']
    					);
    			$tablename="user_id_proof_log";
    			Query_Wrapper::INSERT($query, $param_array, $tablename);
    			 
    			/*$this->conn_master->execute ( $this->conn_master->prepare ( "insert into  user_id_proof_log  (user_id, status, proof_type, img_location,number_of_trials,admin_id, tstamp)
    					values (?,?,?,?,?,?,now() )" ),
    					array( $data ['userId'],
    							$data ['status'],
    							$result1 [0]['proof_type'],
    							$result1 [0]['img_location'],
    							$result1 [0]['number_of_trials'],
    							$_SESSION['admin_id']
    					) );*/
    		}
    	
    		if ($data ['status'] == 'active') {
    				
    			//$verifyImage->creditPoint ( $tbNumbers['credits']['photoid'], "id", $data ['userId'] );
    			//$verifyImage->updateIdTrustScore ( $data ['userId'] );
    			//$verifyImage->authenticateUser ( $data ['userId'] );
    			$tBuilder = new TrustBuilder($data['userId']);
    			$tBuilder->creditPoint($tbNumbers['credits']['photoid'], "id", $tbNumbers['validity']['photoid']);
    			$tBuilder->updateIdTrustScore($result1['0']['proof_type']);
    			$tBuilder->authenticateUser();
    			$this->sendnotification('id_active',$data['userId']);
    			$this->sendEmail('photoId-approved', $data['userId']);
    		}
    
    		if(isset($_SESSION['admin_id'])){
    			$p1 = $data['status'];
    			$p2 = $data['userId'];
    			
    			
    			$query["sql"] = "UPDATE user_id_proof SET status=?, current_action_time=now() where user_id=?";
    			$query["parameter"] = "'$p1'.','.'$p2'";
    			$query = json_encode($query);
    			$step = "ID Proof ".$data ['status'];
    	
    			$admin_log->log($_SESSION['admin_id'],$data['userId'],$query,$step);
    		}
    	
    		$result ['status'] = 'Success';
    		if($data['status'] == 'rejected') $stat = 'not clear';
    		else $stat = $data['status'];
    		$result['action'] = $stat;
    		
    		
    
//     		if($data['status'] == 'fail'){
//     			$this->sendEmail('photoId', $data['userId']);
//     		} 
    		if($data['status'] == 'rejected' && $data['reason']=='name_age_mismatch'){
    			$this->sendnotification('name_age_mismatch',$data['userId']);
    			$this->sendEmail('name_age_mismatch', $data['userId']);
    		}
    		if($data['status'] == 'rejected' && $data['reason']=='name_mismatch'){
    			$this->sendnotification('name_mismatch',$data['userId']);
    			$this->sendEmail('name_mismatch', $data['userId']);
    		}
    		if($data['status'] == 'rejected' && $data['reason']=='age_mismatch'){
    			$this->sendnotification('age_mismatch',$data['userId']);
    			$this->sendEmail('age_mismatch', $data['userId']);
    		}
    		if($data['status'] == 'rejected' && $data['reason']=='password_required'){
    		   $this->sendnotification('password_required',$data['userId']); 
    			$this->sendEmail('password_required', $data['userId']);
    		}
    		if($data['status'] == 'rejected' && $data['reason']=='not_clear'){
    			$this->sendnotification('photoId-notClear',$data['userId']);
    			$this->sendEmail('photoId-notClear', $data['userId']);
    		}
    		
//     		if ($data ['status'] == 'active') {
//     			$this->sendEmail('photoId-approved', $data['userId']);
//     		}
    		return   $result ;
    	} catch ( Exception $e ) {
    		$result ['error'] = $e->getMessage ();
    		return   $result ;
    	     }
    	  }
    	  
    public function verifyPhoto($data){  
    	$admin_log = new logging(); 
    	try
    	{  
    		// if this pic was mapped to another pic another pic then set the old  status as changed
    		if ( isset($data ['mapped_to']) && $data ['mapped_to'] != NULL )
    		{
    			$sql = "UPDATE user_photo SET status = 'changed' WHERE user_id = ? and photo_id = ?" ;
    			$param_array = array($data ['user_id'], $data ['mapped_to']) ;
    			$tablename = 'user_photo' ;
    			Query_Wrapper::UPDATE($sql, $param_array, $tablename);
    			// $this->conn_master->Execute( $this->conn_master->Prepare ($sql), array($data ['user_id'], $data ['mapped_to']));
    		}
    		
    		
    		if($data['status']=='rejected')
    		{    
    			
    			$query="UPDATE user_photo SET admin_approved='yes',mapped_to = null, is_profile='no',approval_time=now(), status=? where photo_id=?";
    			$param_array= array (
    					$data ['status'],
    					$data ['photo_id']
    			);
    			$tablename="user_photo";
    			Query_Wrapper::UPDATE($query, $param_array, $tablename);
    			
    			
    			$sql = "insert into photo_reject_reason(user_id,photo_id,reject_reason) values(?,?,?)" ;
    			$param_array = array($data ['user_id'], $data ['photo_id'],$data['reason']) ;
    			$tablename = 'photo_reject_reason' ;
    			Query_Wrapper::INSERT($sql, $param_array, $tablename);
    			/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user_photo SET admin_approved='yes', is_profile='no',approval_time=now(), status=? where photo_id=?" ), array (
    					$data ['status'],
    					$data ['photo_id']
    			) ); */
    		//if($data['is_profile']=='yes'){
    		
    			
    			
    			
    			 $ps = $this->conn_master->execute($this->conn_master->prepare("SELECT photo_id FROM user_photo where status ='active' and admin_approved='yes' and can_profile is null and user_id=?"),array($data ['user_id']));
    			
    			 if($ps->RowCount() > 0){
    				$row = $ps->FetchRow();
    				
    				//if this rejected pic was profile pic then set other admin approved pic as profile pic
    				if($data['is_profile']=='yes'){
    					$query="update user_photo set is_profile='yes' where photo_id=?";
    					$param_array= array($row['photo_id']);
    					$tablename="user_photo";
    					Query_Wrapper::UPDATE($query, $param_array, $tablename);
    					 
    				/*$this->conn_master->execute($this->conn_master->prepare("update user_photo set is_profile='yes' where photo_id=?"),array($row['photo_id']));*/
    				}
    				
    				
    			} 
    				$pSend= $this->conn_master->execute($this->conn_master->prepare("select count(*) as count, sum(if(is_profile='No',1,0)) as no_profile FROM user_photo  where status ='active'  and can_profile is null and user_id=?"),array($data ['user_id']));
    				$pCounts = $pSend->FetchRow();
    				//$var_dump($pCounts);  
    				if($pCounts['count'] == 0) 
    				{
    			        $this->sendnotification('profile_photo_rejected',$data ['user_id']);
    					$this->sendEmail('all_photos_rejected', $data ['user_id']);
    				} /* else if ($pCounts['no_profile'] == 0){
    					//$this->sendnotification('profile_photo_rejected',$data ['user_id']);
    					$this->sendEmail('profile_photo_approved_others_rejected', $data ['user_id']);
    				} */
    		
    		 // }
    		}
    		else if($data['status']=='active_no_profile'){
    			// set photo status as active , which can not be set a profile pic
    			$query="UPDATE user_photo SET is_profile='no',mapped_to = null,can_profile='no',is_profile='no',status='active',admin_approved='yes',approval_time=now() where photo_id=?";
    			$param_array= array($data ['photo_id']);
    			$tablename="user_photo";
    			
    			Query_Wrapper::UPDATE($query, $param_array, $tablename);
    			
    			/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user_photo SET is_profile='no', can_profile='no',is_profile='no',status='active',admin_approved='yes',approval_time=now() where photo_id=?" ), array(
    					$data ['photo_id']
    			) );*/
    			
    			       
    			      	$ps = $this->conn_master->execute($this->conn_master->prepare("SELECT photo_id FROM user_photo where status ='active' and admin_approved='yes' and can_profile is null and user_id=?"),array($data ['user_id']));
    			      	if($ps->RowCount() > 0){
    			      		$row = $ps->FetchRow();
    			      		if($data['is_profile']=='yes'){
    			      			$query="update user_photo set is_profile='yes' where photo_id=?";
    			      			$param_array= array($row['photo_id']);
    			      			$tablename="user_photo";
    			      			Query_Wrapper::UPDATE($query, $param_array, $tablename);
    			      			 
    			      		/*$this->conn_master->execute($this->conn_master->prepare("update user_photo set is_profile='yes' where photo_id=?"),array($row['photo_id']));*/
    			      		}
    			      	} 
    			      	$pSend= $this->conn_master->execute($this->conn_master->prepare("select count(*) as count, sum(if(is_profile='No',1,0)) as no_profile FROM user_photo  where status ='active'  and can_profile is null and user_id=?"),array($data ['user_id']));
    				$pCounts = $pSend->FetchRow();
    				//$var_dump($pCounts); 
    				if($pCounts['count'] == 0) 
    				{
    					//set discovery flag to default value if there's any
    					//$this->_admin_dbo->setDiscoveryFlagOnAllPhotosRejection($data['user_id']);
    					$this->sendnotification('profile_photo_rejected',$data ['user_id']);
    					$this->sendEmail('all_photos_rejected', $data ['user_id']);
    					
    				} /* else if ($pCounts['no_profile'] == 0){
    					//$this->sendnotification('profile_photo_rejected',$data ['user_id']);
    					$this->sendEmail('profile_photo_approved_others_rejected', $data ['user_id']);
    				} */
    				
    		} else if($data['status']=='active'){
    			// set photo status as active , which can be set profile pic
    			$query="UPDATE user_photo SET admin_approved='yes',approval_time=now(),status=?,can_profile = null,mapped_to = null where photo_id=?";
    			$param_array= array (
    					$data ['status'],
    					$data ['photo_id']
    			);
    			$tablename="user_photo";
    			Query_Wrapper::UPDATE($query, $param_array, $tablename);
    			
    			/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user_photo SET admin_approved='yes',approval_time=now(),status=?,can_profile = null where photo_id=?" ), array (
    					$data ['status'],
    					$data ['photo_id']
    			) );*/
    			
    			// if there was no profile pic , then set this photo as profile pic
    			$ps = $this->conn_master->execute($this->conn_master->prepare("SELECT photo_id FROM user_photo where is_profile ='yes'and user_id=?"),array($data ['user_id']));
    			if($ps->RowCount() == 0){ 
    				$query="update user_photo set is_profile='yes' where photo_id=?";
    				$param_array= array($data ['photo_id']);
    				$tablename="user_photo";
    				Query_Wrapper::UPDATE($query, $param_array, $tablename);
    				 
    				/*$this->conn_master->execute($this->conn_master->prepare("update user_photo set is_profile='yes' where photo_id=?"),array($data ['photo_id']));*/
    				 }  
    			}
    		
    		 
    	  
    		if(isset($_SESSION['admin_id'])){
    			$p1 = $data['status'];
    			$p2 = $data['photo_id'];
    			$query_log["sql"] = "UPDATE user_photo SET status=? where photo_id=?";
				$query_log["parameter"] = "'$p1'.','.'$p2'";
				$query_log = json_encode($query_log);
    			$step = "Photo ".$data ['status'];
    	
    			$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query_log,$step);
    		}
    	
    		$result ['status'] = 'Success';
    		return $result;
    		/* //$result['action'] = $data['status'];
    		if($data['status'] == 'active'){
    			$this->sendEmail('photo_approved', $data['user_id'], $data['userId']);
    		}
    		else if($data['status'] == 'rejected'){
    			$this->sendEmail('photo', $data['user_id'], $data['photo_id']);
    		}else if($data['status'] == 'active_no_profile'){
    			$this->sendEmail('photo_approved_without_profile', $data['user_id'], $data['photo_id']);
    		}
    	 */
    	
    		//print_r ( json_encode ( $result ) );
    	} catch ( Exception $e ) {
    		trigger_error($e->getMessage());
    		$result ['error'] = $e->getMessage ();
    		return $result;
    		//print_r ( json_encode ( $result ) );
    	}
    }
    
    public function verifyEmployment($data){
    	  	$admin_log = new logging();
    	  	 try {
    	  	 
    	  	$result1 = $this->getEmploymentProofs ($data['userId']);
    	  	if($data['status']=='rejected'){
    	  		
    	  		$query="UPDATE user_employment_proof SET status=?,reject_reason=?, current_action_time=now() where user_id=?";
    	  		$param_array= array (
    	  			$data ['status'],
    	  			$data ['reason'],
    	  			$data ['userId']
    	  	) ;
    	  		$tablename="user_employment_proof";
    	  		Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    	  		
    	  	/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user_employment_proof SET status=?,reject_reason=?, current_action_time=now() where user_id=?" ), array (
    	  			$data ['status'],
    	  			$data ['reason'],
    	  			$data ['userId']
    	  	) );*/
    	  	
    	  	
    	  		$query="insert into user_employment_proof_log (status, reject_reason,user_id, proof_type, img_location, is_paid, company_name, emp_id, work_status, number_of_trials,verification_tracking_number, admin_id, tstamp)
    	  			values(?,?,?,?,?,?,?,?,?,?,?,?, now() )";
    	  		$param_array=array(       	$data ['status'],
    	  					$data ['reason'],
    	  					$data ['userId'],
    	  					$result1[0]['proof_type'],
    	  					$result1[0]['img_location'],
    	  					$result1[0]['is_paid'],
    	  					$result1[0]['company_name'],
    	  					$result1[0]['emp_id'],
    	  					$result1[0]['work_status'],
    	  					$result1[0]['number_of_trials'],
    	  					$result1[0]['ref_no'],
    	  					$_SESSION['admin_id']
    	  			) ;
    	  		$tablename="user_employment_proof_log";
    	  		Query_Wrapper::INSERT($query, $param_array, $tablename);
    	  			
    	  	/*$this->conn_master->execute ( $this->conn_master->prepare ( "insert into user_employment_proof_log (status, reject_reason,user_id, proof_type, img_location, is_paid, company_name, emp_id, work_status, number_of_trials,verification_tracking_number, admin_id, tstamp)
    	  			values(?,?,?,?,?,?,?,?,?,?,?,?, now() )" ),
    	  			array(       	$data ['status'],
    	  					$data ['reason'],
    	  					$data ['userId'],
    	  					$result1[0]['proof_type'],
    	  					$result1[0]['img_location'],
    	  					$result1[0]['is_paid'],
    	  					$result1[0]['company_name'],
    	  					$result1[0]['emp_id'],
    	  					$result1[0]['work_status'],
    	  					$result1[0]['number_of_trials'],
    	  					$result1[0]['ref_no'],
    	  					$_SESSION['admin_id']
    	  			) );*/
    	  		
    	  		
    	  	}
    	  	
    	  	else{
    	  		
    	  		$query="UPDATE user_employment_proof SET status=?, current_action_time=now(),reject_reason=null where user_id=?";
    	  		$param_array= array (
    	  			$data ['status'],
    	  			$data ['userId']
    	  	)  ;
    	  		$tablename="user_employment_proof";
    	  		Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    	  			
    	  	/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user_employment_proof SET status=?, current_action_time=now(),reject_reason=null where user_id=?" ), array (
    	  			$data ['status'],
    	  			$data ['userId']
    	  	) );*/
    	  		
    	  	$result1 = $this->getEmploymentProofs ($data['userId']);
    	  	
    	  	$query="insert into user_employment_proof_log (status,user_id, proof_type, img_location, is_paid, company_name,emp_id, work_status,number_of_trials,verification_tracking_number,admin_id, tstamp)
    	  			values(?,?,?,?,?,?,?,?,?,?,?, now() )";
    	  	$param_array= array(       	$data ['status'],
    	  					$data ['userId'],
    	  					$result1[0]['proof_type'],
    	  					$result1[0]['img_location'],
    	  					$result1[0]['is_paid'],
    	  					$result1[0]['company_name'],
    	  					$result1[0]['emp_id'],
    	  					$result1[0]['work_status'],
    	  					$result1[0]['number_of_trials'],
    	  					$result1[0]['ref_no'],
    	  					$_SESSION['admin_id']
    	  			) ;
    	  	$tablename="user_employment_proof_log";
    	  	Query_Wrapper::INSERT($query, $param_array, $tablename,'master',$data['user_id']);
    	  		
    	  	/*$this->conn_master->execute ( $this->conn_master->prepare ( "insert into user_employment_proof_log (status,user_id, proof_type, img_location, is_paid, company_name,emp_id, work_status,number_of_trials,verification_tracking_number,admin_id, tstamp)
    	  			values(?,?,?,?,?,?,?,?,?,?,?, now() )" ),
    	  			array(       	$data ['status'],
    	  					$data ['userId'],
    	  					$result1[0]['proof_type'],
    	  					$result1[0]['img_location'],
    	  					$result1[0]['is_paid'],
    	  					$result1[0]['company_name'],
    	  					$result1[0]['emp_id'],
    	  					$result1[0]['work_status'],
    	  					$result1[0]['number_of_trials'],
    	  					$result1[0]['ref_no'],
    	  					$_SESSION['admin_id']
    	  			) );*/
    	  	
    	  	
    	  	}
    	  	
    	  	if ($data ['status'] == 'active') {
    	  	//$verifyImage->creditPoint ( $tbNumbers['credits']['employment'], "employment", $data ['userId'] );
    	  	//$verifyImage->updateEmploymentTrustScore ( $data ['userId'] );
    	  	$tBuilder = new TrustBuilder($data['userId']);
    	  	$tBuilder->updateEmploymentTrustScore();
    	  	$tBuilder->authenticateUser ();
    	  	$tBuilder->creditPoint($tbNumbers['credits']['employment'], "employment", $tbNumbers['validity']['employment']);
    	  	$this->sendnotification('emp_active',$data['userId']);
    	  	//$verifyImage->sendEmail('employment-approved', $data['userId']);
    	  	}
    	  	
    	  	if(isset($_SESSION['admin_id'])){
    	  	$p1 = $data['status'];
    	  	$p2 = $data['userId'];
    	  	
    	  	
    	  	$query["sql"] = "UPDATE user_employment_proof SET status=?, current_action_time=now() where user_id=?";
    	  	$query["parameter"] = "'$p1'.','.'$p2'";
    	  	$query = json_encode($query);
    	  	$step = "Employement ".$data ['status'];
    	  	
    	  	$admin_log->log($_SESSION['admin_id'],$data['userId'],$query,$step);
    	  	}
    	  	
    	  	$result ['status'] = 'Success';
    	  	if($data['status'] == 'rejected') $stat = 'not clear';
    	  	else $stat = $data['status'];
    	  	$result['action'] = $stat;
    	    	
    	  //	print_r ( json_encode ( $result ) );
    	  	
//     	  	if ($data ['status'] == 'active') {
//     	  	$this->sendEmail('employment-approved', $data['userId']);
//     	  	}
//     	  	if($data['status'] == 'fail'){
//     	  	$this->sendEmail('employment', $data['userId']); 
//     	  	}
    	  	
    	  	if($data['status'] == 'rejected' && $data['reason']=='not_clear'){
    	  	$this->sendnotification('employment-notClear',$data['userId']);
    	  	$this->sendEmail('employment-notClear', $data['userId']);
    	  	}
    	  	if($data['status'] == 'rejected' && $data['reason']=='password_required'){	
    	  	$this->sendnotification('employment_password_required', $data['userId']);
    	  	$this->sendEmail('employment_password_required', $data['userId']);
    	  	}
    	  	return $result;
    	  	} catch ( Exception $e ) {
    	  	$result ['error'] = $e->getMessage ();
    	  	return $result;
    	  	}
    	  	
    	  	
    	  	
    	  }
    
    public function blockUser($data){
    	  	$admin_log = new logging();
    	  	$step = "blocked";
    	    $changes=$this->changes_done_by_admin($_SESSION['admin_id'],$step);
 	
    	  	try {
    	  		
    	  		if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
    	  		
    	  		if($changes >=static::$admin_change_limit ){
    	  			$result ['status'] = 'Limit exceeded';
    	  			return   $result ;
    	  		}
    	  		
    	  		$user_id = $data['userId'];
				$sql = "SELECT status from user where user_id = ?";
				$row = Query_Wrapper::SELECT($sql, array($user_id),Query_Wrapper::$__slave, true);
    	  	//	$row= $ex->FetchRow();
    	  		//echo 'here'; die;
    	  	
    	  		$result1 = $this->getEmploymentProofs ($data['userId']);
    	  		
    	  		$query="UPDATE user SET status=? where user_id=?";
    	  		$param_array= array (
    	  				$data ['status'],
    	  				$data ['userId']
    	  		)  ;
    	  		$tablename="user";
    	  		Query_Wrapper::UPDATE($query, $param_array, $tablename,'master',$data['user_id']);
    	  		 
    	  		/*$this->conn_master->execute ( $this->conn_master->prepare ( "UPDATE user SET status=? where user_id=?" ), array (
    	  				$data ['status'],
    	  				$data ['userId']
    	  		) );*/
    	  		$query="Insert Into user_suspension_log value(?,?,?,?,now(),?)";
    	  		$param_array= array (
    	  				$data ['userId'],
    	  				$data ['reason'],
    	  				$row['status'],
    	  				$data['status'],  
    	  				$_SESSION ['admin_id']
    	  		) ;
    	  		$tablename="user_suspension_log";
    	  		Query_Wrapper::INSERT($query, $param_array, $tablename,'master',false,$data['user_id']);
    	  			
    	  		
    	  		/*$this->conn_master->execute ( $this->conn_master->prepare ( "Insert Into user_suspension_log value(?,?,?,?,now(),?)" ), array (
    	  				$data ['userId'],
    	  				$data ['reason'],
    	  				$row['status'],
    	  				$data['status'],  
    	  				$_SESSION ['admin_id']
    	  		) );*/
    	  	
    	  		 
    	  			$p1 = $data['status'];
    	  			$p2 = $data['userId'];
    	  			$query["sql"] = "UPDATE user SET status=? where user_id=?";
    	  			$query["parameter"] = "'$p1'.','.'$p2'";
    	  			$query = json_encode($query);
    	  			
    	  			$txt="User id".$data['user_id']." is blocked ";
    	  			
    	  			$admin_log->log($_SESSION['admin_id'],$data['userId'],$query,$step,$txt);
    	  			$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
    	  		
    	  	
    	  		$result ['status'] = 'Success';
    	  		$result ['no_changes_allowed'] = static::$admin_change_limit-$changes-1;  // -1 for ongoing change
    	  		return $result;
    	  		//$this->sendEmail('suspend', $data['userId']);
    	  	  }	
    	  		
    	  	} catch ( Exception $e ) {
    	  		$result ['status'] = $e->getMessage ();
    	  		return $result;
    	  	}	   
      }
      
  public function searchBlockedFacebookEmailId($email_id)
  {
  	$resultSet=array();
  	$sql="select fid from block_fids where fid in (select distinct fid from log_facebook where email=?)";
  	$rs = $this->conn_slave->Execute($this->conn_slave->Prepare($sql),array($email_id));
  	$rs=$rs->GetRows();
	foreach ($rs as $key=>$value)	
	{
		$resultSet[]=$value['fid'];
	}
	//var_dump($resultSet);
	$result=array("email_id"=>$email_id,
			      "fids"=>$resultSet
			  );
	return $result;
  }

  public function unblockFid($fid)
  {
  	try{
  	$sql="delete from block_fids where fid =?";
  	$param_array=array($fid);
  	$tablename="block_fids";
  	Query_Wrapper::DELETE($sql, $param_array,$tablename);
  	$admin_log = new logging();
  	$step = "Unblocked fid";
  	
  	$query["sql"] = " delete from block_fids where fid =?";
  	$query["parameter"] = $fid;
  	//var_dump($query);
  	$query = json_encode($query);
  	//var_dump($query);
  	$txt="Fid '".$fid."' has been unblocked";
  	$admin_log->log($_SESSION['admin_id'],-1,$query,$step,$txt);
  	//var_dump($_SESSION['admin_id']);
  	$this->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
  	
  	$result ['status'] = 'Success'; 
	return $result;
   } catch ( Exception $e ) {
  	$result ['status'] = $e->getMessage ();
  	return $result;
  	}		
  	
  }
  
   
}


/* $educations_arr = $educations;
$educations_subarr = $educationsSub;
$religions_arr = $religions;
$interests_arr = $interests;
$industries_arr = $industries; */
try {
	$data = $_POST;
	if ($data ['signup']) {
  
		$username = $data ['user'];
		$password = $data ['pass'];
		$sql = $conn_slave->Prepare ( "select id,user_name,super_admin,privileges from admin where user_name=? and password=md5(?)" );
		$rs = $conn_slave->Execute ( $sql, array (
		$username,
		$password
		) );
		if($rs&&$rs->rowCount()>0){
			$arr = $rs->FetchRow ();
			session_start();
			//var_dump($arr);exit;
			setcookie("admin_id",$arr['id'],time()+3600*3,'/');
			$_SESSION ['admin_id']=$arr['id'];
			$_SESSION ['super_admin']=$arr['super_admin'];
			if($arr['privileges'])
			$_SESSION ['privileges']= explode ( ',', $arr['privileges'] ); 
		}
		else{  
			$error_login=true;
		}

	}
	// $func=new functionClass();
	// $user = functionClass::getUserDetailsFromSession ();
	
	
	$adminId= $_SESSION ['admin_id'];

	if (isset ( $adminId ) == false) {
		if ($error_login == true) {
			$smarty->assign ( "error_login", $error_login );
		}
		$smarty->display ( "../templates/admin/adminlogin.tpl" );
		die ();
		// show login page
	}else{
		if(!isset($_COOKIE['admin_id']))
			setcookie("admin_id",$adminId,time()+3600*21*30,'/');
	}
} catch ( Exception $e ) {
	trigger_error($e->getMessage (),E_USER_WARNING);
	var_dump ( $e->getMessage () );
}

$verifyImage = new verifyImage ( $adminId );
$admin_log = new logging();
$user_utils = new userUtilsDBO();
//var_dump($attributes); die;

//var_dump($data['action']);
if (isset ( $data ['action'] ) && $data ['action'] == 'show_engagement')
{
	try{
		//echo "aa";
		$result=$verifyImage->getEngagement($data['user_id'], $data['days_count']);
		//var_dump($result);

	}catch(Exception $e){
		$result['status'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );
	die ();
}

if (isset ( $data ['action'] ) && $data ['action'] == 'show_sparks')
{
	try{
		$result=$verifyImage->getSparkStats($data['user_id']);
		print_r ( json_encode ( $result ) );
		die ();

	}catch(Exception $e){
		$result['status'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );
	die ();
}

if (isset ( $data ['action'] ) && $data ['action'] == 'getRemainingModerationData')
{
	try{
		//echo "aa";
		$result=$verifyImage->getRemainingModerationData();
		//var_dump($result);

	}catch(Exception $e){
		$result['error'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );
	die ();
}

if (isset ( $data ['action'] ) && $data ['action'] == 'getPhoneNumber')
{
	try{
		$result=$verifyImage->getPhoneNumber($data['user_id']);

	}catch(Exception $e){
		$result['error'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );
	die ();
}

if (isset ( $data ['action'] ) && $data ['action'] == 'block') {
 	$step="block"; 
 	$changes=$verifyImage->changes_done_by_admin($_SESSION['admin_id'],$step);
 	
 	if($changes >= verifyImage::$admin_change_limit ){
 		$result ['status'] = 'Limit exceeded';
 		}	 
 	else{
		$user_id=$data['userId']; 

		$sql = "SELECT status from user where user_id = ?";
		$row = Query_Wrapper::SELECT($sql, array($user_id),Query_Wrapper::$__slave, true);
		if($row['status'] == 'blocked' || $row['status'] == 'suspended'){
			$result ['status'] = "Already ".$row['status'];
		} 
		
		else{
			$user_utils->suspend($user_id,$data['reason'],$row['status'],$_SESSION['admin_id'],$data['status']);
			$user_utils->block_device($user_id, $data['reason']);
			if($data['block_all_devices'] == 'true')
			{
				$user_utils->blockAllUsersFromDevice($user_id);
			}
			$result ['status'] = "Success";
			$result ['no_changes_allowed'] = $verifyImage::$admin_change_limit-$changes-1;  // -1 for ongoing change
	
			$sql = "UPDATE user SET status = ? , last_changed_status =? where user_id=?";
			$txt="User id".$user_id." has been blocked ";
		 
			$admin_log->log($_SESSION['admin_id'],$user_id,$sql,$step,$txt);
			$verifyImage->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
 		}
 	}
	print_r ( json_encode ( $result ) );
	die;
	
 }

if(isset($data ['action']) && $data ['action'] == 'restore'){
	try{
		 
		$step = "restore previous state";
		$changes=$verifyImage->changes_done_by_admin($_SESSION['admin_id'],$step);
		
		if($changes >= verifyImage::$admin_change_limit ){
			$result ['status'] = 'Limit exceeded';
		}
		else{
			$user = new User();
			$user_id = $data['user_id'];
			    
			if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
				$result_func=$user->restoreUser($user_id);
				if($result_func==true)
				{
					$result ['status'] = "Success";
					$result ['no_changes_allowed'] = $verifyImage::$admin_change_limit-$changes-1;  // -1 for ongoing change
				}
				else 
				{
					$result['status']="Nolaststatus";
				}
			}
		  }	
		/*$ex = $conn_slave->Execute("SELECT previous_status from user_suspension_log where user_id = $user_id order by timestamp desc limit 1");
		$row= $ex->FetchRow();
		if(isset($_SESSION['super_admin']) && $_SESSION['super_admin']=='yes'){
			$conn_master->execute ( $conn_master->prepare ( "UPDATE user SET status=? where user_id=?" ), array (
				$row ['previous_status'],
				$data ['user_id']
			) );*/ 
			if(isset($_SESSION['admin_id']) && $result['status']=='Success'){
				
				$p1 = $data['action'];
				$p2 = $data['user_id'];
				$query["sql"] = "UPDATE user SET status=? where user_id=?";
				$query["parameter"] = "'$p1'.','.'$p2'";
				$query = json_encode($query); 	   
				$txt="User id".$data['user_id']." has been restored ";
				$admin_log->log($_SESSION['admin_id'],$data['user_id'],$query,$step,$txt);
				$verifyImage->admin_change_done_mail($_SESSION['admin_id'],$step,$txt);
			} 
			//$result ['status'] = 'Success';
		//}
	}catch(Exception $e){
		$result['status'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );  
	die ();
}



if(isset($data ['action']) && $data ['action'] == 'block_femaleUnderReview'){
	try{
		$rs=$verifyImage->processUserUnderReview($data['user_id'],0);
		if($rs=='true')
		$result ['status'] = 'Success';
		
	}catch(Exception $e){
		$result['status'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );
	die ();
}



if(isset($data ['action']) && $data ['action'] == 'clear_femaleUnderReview'){
	try{
		$rs=$verifyImage->processUserUnderReview($data['user_id'],1);
		if($rs=='true')
		$result ['status'] = 'Success';
		
	}catch(Exception $e){
		$result['status'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );
	die ();
}

if(isset($data ['action']) && $data ['action'] == 'unblockFid'){
	try{
		$result=$verifyImage->unblockFid($data['fid']);

	}catch(Exception $e){
		$result['status'] = $e->getMessage();
	}
	print_r ( json_encode ( $result ) );
	die ();
}
if(isset($data ['action']) && $data ['action'] == 'unblock_device'){
	$result = array();
	try{
		if(isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != null)
		{
			$deviceActions = new DeviceIdActions();
			$deviceActions->unBlockDevice($_REQUEST['device_id']) ;
		}
		$result['status'] = 'success';

	}catch(Exception $e){
		$result['error'] = "Something is wrong" .$e->getMessage() ;
		trigger_error($e->getMessage(),E_USER_WARNING);
	}
	print_r ( json_encode ( $result ) );
	die ();
}


if(isset($data['action']) && $data['action']=='editName'){
        $result=$verifyImage->editNameOfUser($data);
        print_r ( json_encode ( $result ) );
        die ();            
      }

if(isset($data['action']) && $data['action']=='editDOB'){
        $result=$verifyImage->editBdayOfUser($data);
        print_r ( json_encode ( $result ) );
        die ();            
		}

if(isset($data['action']) && $data['action']=='editGender'){
        $result=$verifyImage->editGenderOfUser($data);
        print_r ( json_encode ( $result ) );
        die ();            
		}

if(isset($data['action']) && $data['action']=='editphoneNumber'){ 
        $result=$verifyImage->editphoneNumberOfUser($data);
        print_r ( json_encode ( $result ) );
        die ();            
		}

 if (isset ( $data ['action'] ) && $data ['action'] == 'authbridge_verification') {
 		$result=$verifyImage->authbridgeVerification($data);
 		print_r ( json_encode ( $result ) );
 		die ();
 	            }     
 if(isset ( $data ['action'] ) && $data ['action'] == 'paid'){
 	$result=$verifyImage->employmentPaid($data);
 	print_r ( json_encode ( $result ) );
 	die ();
               }  
if (isset ( $data ['action'] ) && $data ['action'] == 'verify_id') {
	$result=$verifyImage->verifyId ($data);
	print_r ( json_encode ( $result ) );
    die ();
} /* elseif (isset ( $data ['action'] ) && $data ['action'] == 'verify_address') {
	$result=$verifyImage->verifyAddress ($data);
	print_r ( json_encode ( $result ) );
    die ();
} */
 elseif (isset ( $data ['action'] ) && $data ['action'] == 'verify_employment') {
 	$result=$verifyImage->verifyEmployment ($data);
 	print_r ( json_encode ( $result ) );
 	die ();
	 }
 elseif (isset ( $data ['action'] ) && $data ['action'] == 'verify_photo') {
 	$result=$verifyImage->verifyPhoto ($data);
 	print_r ( json_encode ( $result ) );
 	die ();
	}
	
  elseif (isset ( $data ['action'] ) && $data ['action'] == 'unblock') {
		$result=$verifyImage-> unblock($data['user_id1'],$data['user_id2']);
		print_r ( json_encode ( $result ) );
		die ();
	}	
	
  elseif (isset ( $data ['action'] ) && $data ['action'] == 'inoffensive') {
		$result=$verifyImage->inoffensive($data['user_id1'],$data['user_id2']);
		print_r ( json_encode ( $result ) );
		die ();
	} 	
	
 elseif(isset($data['action']) && $data['action']=='rotate'){
 	$photo = new Photo($data['user_id']);
    if(isset($data['user_id'])&&$data['action']=='rotate') {
	//var_dump($data);	
	$photo->rotateImage($data['id'],$data['angle']);
    }
 	die();
	}	


try {
	//var_dump($_REQUEST);
	if(!isset($_GET['type'])){
		$page_id = 1;
		if (isset ( $_GET ['page_id'] ))
		$page_id = $_GET ['page_id'];
		if (isset ( $_GET ['modType'] ))
			$modType = $_GET ['modType'];
		if(isset($_REQUEST['search_by'])){
			if($_REQUEST['search_by']=='name')
			$attributes = $verifyImage->searchByName($page_id, $_REQUEST['fname'],$_REQUEST['lname']);
			elseif ($_REQUEST['search_by']=='email_id_blocked')
			$attributes = $verifyImage->searchBlockedFacebookEmailId ($_REQUEST['value']);
			else 
			$attributes = $verifyImage->getSearchData($page_id, trim($_REQUEST['search_by']), $_REQUEST['value']);
		}
		else{ 
		    if($_REQUEST['sortBy'] == 'day')
			    $attributes = $verifyImage->getDataByDate($page_id, $_REQUEST['sortBy'], $_REQUEST['value']);
			else if($_REQUEST['sortBy'] == 'moderation')
				// used when searched for moderation only .
				$attributes = $verifyImage->getModerationData($page_id, $_REQUEST['modType'],$_REQUEST['user_id']);
			else
			    $attributes = $verifyImage->getDataByDate($page_id, $_REQUEST['sortBy']);
		}
		
		$smarty->assign("attributes", $attributes);
		$smarty->assign("page_num", $page_id);
		$smarty->assign("modType", $_REQUEST['modType']);
		 if($_REQUEST['sortBy'] == 'moderation') 
		$smarty->display("../templates/admin/adminPanel_moderation.tpl");
		 else if($_REQUEST['search_by']=='email_id_blocked')
		 $smarty->display("../templates/admin/adminpanel_blockedFacebookUsers.tpl");
		 else 
		 	$smarty->display("../templates/admin/adminPanel.tpl");
		
	}
	elseif (isset ( $_GET ['type'] ) && $_GET ['type'] == '2') {
		echo "Photo Verification<br>";
		//$result = $verifyImage->getPhotos ();
		$user_id = $_GET['user_id'];
		$result = $verifyImage->getPhotosOfUser($user_id) ;
		$smarty->assign ( "photos", $result );
		$smarty->assign ("imageurl",$imageurl);
		$smarty->assign ("user_id",$user_id);
		$smarty->display ( "../templates/admin/adminPanel_photo.tpl" );
	} elseif (isset ( $_GET ['type'] ) && $_GET ['type'] == '3') {
		echo "Address Verification<br>";
		$user_id = $_GET['user_id'];
		$result = $verifyImage->getAddressProofs ($user_id);
		$smarty->assign ( "proofs", $result );
		$smarty->display ( "../templates/admin/adminPanel_address.tpl" );
	} elseif (isset ( $_GET ['type'] ) && $_GET ['type'] == '4') {
		echo "Employment Verification<br>";
		$user_id = $_GET['user_id'];
		$result = $verifyImage->getEmploymentProofs ($user_id);
		$smarty->assign ( "proofs", $result );
		$smarty->display ( "../templates/admin/adminPanel_employment.tpl" );
	} else if (isset ( $_GET ['type'] ) && $_GET ['type'] == '1') {
		echo "Id Verification<br>";
		$user_id = $_GET['user_id'];
		$result = $verifyImage->getIdProofs($user_id);
		//var_dump($result); 
		$smarty->assign ( "proofs", $result );
		$smarty->display ( "../templates/admin/adminPanel_id.tpl" );
	}else if (isset ( $_GET ['type'] ) && $_GET ['type'] == '5') {
		$user_id = $_GET['user_id'];
		$result=$verifyImage->getBlockList($user_id);
		$smarty->assign("user_id",$user_id);   
		$smarty->assign ( "blocklist", $result );   
		$smarty->display ( "../templates/admin/adminpanel_blocklist.tpl" );		
	}else if (isset ( $_GET ['type'] ) && $_GET ['type'] == '6' && $_SESSION['super_admin'] == 'yes' ) {
		if(isset($_REQUEST['device_id']) && $_REQUEST['device_id'] !=null)
		{
			$deviceActions = new DeviceIdActions();
			$device = $deviceActions->searchDeviceId($_REQUEST['device_id']);
			$smarty->assign ("device" , $device );
		//	var_dump($deviceList);
		}
		$smarty->display ( "../templates/admin/blockedDevices.tpl" );
	}

} catch ( Exception $e ) {
	trigger_error($e->getMessage());
	//print_r ( $e->getMessage () ); 
}
?>
 
