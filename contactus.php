<?php

require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/abstraction/query.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";

/**
 * all the app related data activities like fetching data or updating the data
 * @author himanshu
 *
 */
class ContactUs{

	private $user_id;

	function __construct($userId){
		$this->user_id = $userId;
	}
	
	public function contactus($msg){
		$sql = "insert into mobile_app_feedback(user_id,tstamp,message) values(?,now(),?)";
		Query::INSERT($sql, array($this->user_id,$msg));
		
		$to = "rahul@trulymadly.com,shashwat@trulymadly.com";
		$subject = "App feedback from User Id- $this->user_id";
		$message = "User id- $this->user_id <br> Timestamp- ". Utils::GMTTOISTfromUnixTimestamp(time()). " <br> Message- $msg";
		$from = "shashwat@trulymadly.com";
		Utils::sendEmail($to, $from, $subject, $message,true);
		
		
	}
}

try {
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	if(isset($user_id)){
		$appUtil = new ContactUs($user_id);

		$msg=$_REQUEST['msg'];
		if(isset($msg) && strlen(trim($msg))>0){
			$appUtil->contactus($msg);
		}
	}
  $output['responseCode'] = 200;
                        echo json_encode($output); die;

}
catch (Exception $e){
	trigger_error("PHP WEB: ". $e->getTraceAsString(), E_USER_WARNING);
}

?>
