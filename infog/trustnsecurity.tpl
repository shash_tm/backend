<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="Description" content="We verify and authenticate all profiles so that we weed the creepy out.">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="Author" content="TrulyMadly.com"/>
<meta name="Copyright" content="2014 TrulyMadly.com"/>
<meta name="rating" content="General"/>
<meta name="language" content="English"/>
<title>Trust & Security</title>
<link href="{$cdnurl}/css/staticstyle.css?v=4.1" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
</script>
</head>

<body>

<div class="hdrcontainer" align="center">
<div class="hdrtopstc">

<!-- Top Header -->
<div class="pageheader" {if $header.login_mobile eq true} style="display:none;" {/if}>
        
<!-- Dropdown 1 -->
<div class="click-nav">
<ul class="no-js">
<li><a class="icon-reorder ddicon clicker"></a>
<ul>
<li><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
<li><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
<li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
<li><a>Trust & Security</a></li>
<li><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
<li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
</ul>
</li>
</ul>
</div>
<!-- Dropdown 1 end-->

<a href="{$baseurl}/index.php"><img src="{$cdnurl}/images/common/trulymadly_logo.png" width="172" height="35" class="sitelogo"></a>

<div class="followus hdrsoclink">
    	<p class="graytxt">Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>

</div>
<!-- Top Header end -->

</div>
</div>

<!-- Header responsive -->
<div class="respheader" {if $header.login_mobile eq true} style="display:none;" {/if}>
	<!-- Dropdown 2 -->
			<div class="click-nav2" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
			<ul class="no-js2">
				<li>
					<a class="clicker2 icon-reorder"></a>
					<ul style="display:none;">
                    <!--<li><a href="{$baseurl}/infog/aboutus.html" target="_blank">About Us</a></li>
					<li><a href="{$baseurl}/infog/ScientificMatching.html" target="_blank">Matching Process</a></li>
					<li><a href="{$baseurl}/infog/TrustSecurity.html" target="_blank">Trust & Security</a></li>
					<li><a href="#" target="_blank">Compatibility Model</a></li>
                    <li><a href="#" target="_blank">Media Coverage</a></li> -->
                    <li><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
                    <li><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
					<li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a>Trust & Security</a></li>
					<li><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
					</ul>
				</li>
			</ul>
			</div>
            <!-- Dropdown 2 end-->
            <div class="followus hdrsoclink">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>
</div>
<!-- Header responsive end -->

<div {if $header.login_mobile neq true} class="stchdr tbpagebg" {else} class="stchdr"{/if}>
{if $header.login_mobile neq true} 
<h1>Trust & Security</h1>
{/if}
<div class="stctxttop fr">
{if $header.login_mobile neq true}
<div class="hartshape1"></div>
{/if}
<p>
We verify and authenticate all profiles so that we weed the creepy out. <br><br>
We won't introduce you to any random people, especially since we know how shady dating sites can be. That's why we ask so many questions.<br><br>
We look for ID proofs, social profiles, phone number and employment records. Ours is more like a friend's living room. When you're introduced to someone, you should be sure that they are the real deal.<br><br>
But like a good friend, we don't share any of this with anyone else. Nor do we publish anything personal on any social networking sites. Do we hear a sigh of relief? 
</p>
<img src="{$cdnurl}/images/bghearts1.gif" width="300" height="300" class="hrtbg1"></div>
</div>
<div class="stcsec1" align="center">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg2">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg3">
<div class="stcsec1txt">
<h2>Here's how it works</h2>
<div class="stcsec1left">
<strong>Facebook</strong>
<p>We validate your name, gender, age, relationship status and photo from your profile. But we never publish anything on the profile or share your profile link with anyone. Pukka promise!</p>

<strong>LinkedIn</strong>
<p>We validate your designation, industry and company from your profile. We never publish anything on the profile or share your profile link with anyone.</p>

<strong>Photo ID</strong>
<p>Only government approved forms of photo IDs like, passport, driver's licence, PAN card, voter's ID and Aadhar card will be accepted. No selfies please! Your PhotoID is kept strictly confidential and only viewed by our internal team. </p>

<strong>Phone Number</strong>
<p>An automatic call back to the number you provide to us is how we verify it. But don't worry, your number will stay with us and not broadcasted on your profile.</p>

<strong>Employment</strong>
<p>We review your salary slips, offer letter, company document to validate your professional credentials. All the documents are kept strictly confidential and not shared with anyone. We understand that you might need privacy in these matters and we respect that. Also, we don't share your company name on your profile, unless you'd like to.</p>
</div>
<div class="stcsec1right">
<img src="{$cdnurl}/images/ts1.png">
<img src="{$cdnurl}/images/ts2.gif">
<img src="{$cdnurl}/images/ts3.gif">
<img src="{$cdnurl}/images/ts4.gif">
<img src="{$cdnurl}/images/ts5.gif">
<img src="{$cdnurl}/images/ts6.gif">
</div>
</div>
</div>
<div class="stcsec2">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg4">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg5">
<div class="stcsec2txt">
<h2>Why you should get a <br>good Trust Score</h2>
<img src="{$cdnurl}/images/blurpro.jpg" class="blurproimg">
<ul>
<li>For starters, only verified members can initiate communication with their matches.</li>
<li>Only verified members are visible to their matches. It's a little tease, we know, but like they
say, all's fair in love and war. </li>
<li>In the bargain, the higher your Trust Score, the more we'll recommend you to our members.
It's kinda like getting a VIP status and getting the early bird chance to start interacting with
their members.</li>
</ul>

</div>
</div>

<!-- Footer -->
<div align="center" class="ftrbg">
<div class="pagefooter" align="center">

	<div class="footerlink">
		<ul>
        	<li class="padt8"><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
            <li class="padt8"><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
            <li class="padt8"><a>Trust & Security</a></li>
			<li class="padt8"><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
			<li class="padt8"><!--© 2014, trulymadly.com,  --><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>	
            <li class="padt8"><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>		
			<li class="padt8"><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
            <li><div class="followus">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div></li>
		</ul>
		<p class="copyr">© 2015, trulymadly.com</p>
	</div>
</div>
</div>
<!-- Footer End-->


<script src="{$cdnurl}/js/common/jquery.min.js"></script>
<script>
$(function() {
	// Clickable Dropdown
	$('.click-nav > ul').toggleClass('no-js js');
	$('.click-nav .js ul').hide();
	$('.click-nav .js').click(function(e) {
		$('.click-nav .js ul').slideToggle(200);
		$('.clicker').toggleClass('active');
		e.stopPropagation();
	});
	$(document).click(function() {
		if ($('.click-nav .js ul').is(':visible')) {
			$('.click-nav .js ul', this).slideUp();
			$('.clicker').removeClass('active');
		}
	});
});

		$(function() {
			// Clickable Dropdown
			$('.click-nav2 > ul').toggleClass('no-js2 js');
			$('.click-nav2 .js ul').hide();
			$('.click-nav2 .js').click(function(e) {
				$('.click-nav2 .js ul').slideToggle(200);
				$('.clicker2').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav2 .js ul').is(':visible')) {
					$('.click-nav2 .js ul', this).slideUp();
					$('.clicker2').removeClass('active');
				}
			});
		});
</script>

</body>
</html>
