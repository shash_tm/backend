<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="Description" content="We want to find someone who is truly compatible with you.">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="Author" content="TrulyMadly.com"/>
<meta name="Copyright" content="2014 TrulyMadly.com"/>
<meta name="rating" content="General"/>
<meta name="language" content="English"/>
<title>True Compatibility</title>
<link href="{$cdnurl}/css/staticstyle.css?v=4.1" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
</script>
</head>

<body>

<div class="hdrcontainer" align="center">
<div class="hdrtopstc">

<!-- Top Header -->
<div class="pageheader" {if $header.login_mobile eq true} style="display:none;" {/if}>
        
<!-- Dropdown 1 -->
<div class="click-nav">
<ul class="no-js">
<li><a class="icon-reorder ddicon clicker"></a>
<ul>

					<li><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
                    <li><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
					<li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
					<li><a>True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
                    <li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
</ul>
</li>
</ul>
</div>
<!-- Dropdown 1 end-->

<a href="{$baseurl}/index.php"><img src="{$cdnurl}/images/common/trulymadly_logo.png" width="172" height="35" class="sitelogo"></a>

<div class="followus hdrsoclink">
    	<p class="graytxt">Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>

</div>
<!-- Top Header end -->

</div>
</div>

<!-- Header responsive -->
<div class="respheader" {if $header.login_mobile eq true} style="display:none;" {/if}>
	<!-- Dropdown 2 -->
			<div class="click-nav2" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
			<ul class="no-js2">
				<li>
					<a class="clicker2 icon-reorder"></a>
					<ul style="display:none;">
                    <!--<li><a href="{$baseurl}/infog/aboutus.html" target="_blank">About Us</a></li>
					<li><a href="{$baseurl}/infog/ScientificMatching.html" target="_blank">Matching Process</a></li>
					<li><a href="{$baseurl}/infog/TrustSecurity.html" target="_blank">Trust & Security</a></li>
					<li><a href="#" target="_blank">Compatibility Model</a></li>
                    <li><a href="#" target="_blank">Media Coverage</a></li> -->
                    <li><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
                    <li><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
                    <li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
					<li><a>True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
					</ul>
				</li>
			</ul>
			</div>
            <!-- Dropdown 2 end-->
            <div class="followus hdrsoclink">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>
</div>
<!-- Header responsive end -->

<div {if $header.login_mobile neq true} class="stchdr tcpagebg" {else} class="stchdr"{/if}>
{if $header.login_mobile neq true} 
<h1>True Compatibility</h1>
{/if}
<div class="stctxttop fl">
{if $header.login_mobile neq true} 
<div class="hartshape2"></div>
{/if}
<p class="fr">
We work out the initial compatibility so that you don't have to.<br><br>
We want to find someone who is truly compatible with you.<br><br>
We don't rely on mumbo-jumbo like star signs or random, haphazard matching. Think of us like your best friend, the one who knows you inside out. One who makes sure that instead of you walking into a room of complete strangers, you walk into a room of familiar strangers - they'd already be handpicked by us, according to what we know of you.<br><br>
For that we ran an exhaustive research across couples, singles and top relationship counsellors to understand what makes people tick and stick in this day and age, what they are looking for and the issues that come between them and what to do about it. We bring in this science to work the initial chemistry between you and who you should be with. Think of all the conversation starters once you already know who you'd be talking to. Works, right?<br><br><br>
<span class="paratitletxt">Our Compatibility Assesment Model</span><br><br>
<a>Education</a><a>Values</a><a>Adaptability</a><a>Interests</a><a>Lifestyle</a>
</p>
<img src="{$cdnurl}/images/bghearts1.gif" width="300" height="300" class="hrtbg1"></div>
</div>
<div class="stcsec1" align="center">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg2">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg5">
<div class="stcsec1txt">
<h2>Our Advisory Team</h2>
<div class="advprosec" align="center">
<div class="advpro"><img src="{$cdnurl}/images/draruna.jpg"><h3>Dr. Aruna Broota</h3>
<ul>
<li><i class="icon-minus"></i>Renowned Clinical Psychologist</li>
<li><i class="icon-minus"></i>President of National Academy of Psychology (2004)</li>
<li><i class="icon-minus"></i>Editor of the Journal of Research & Applications in Clinical Psychology</li>
<li><i class="icon-minus"></i>Former Professor, Department of Psychology, University of Delhi</li>
</ul>
</div>
<div class="advpro"><img src="{$cdnurl}/images/drkd.jpg"><h3>Dr. KD Broota</h3>
<ul>
<li><i class="icon-minus"></i>Renowned Psychologist and Psychometrician</li>
<li><i class="icon-minus"></i>Former Head of Department of Psychology, University of Delhi</li>
<li><i class="icon-minus"></i>PhD in Experimental Psychology </li>
</ul>
</div>
<div class="advpro"><img src="{$cdnurl}/images/drmaitri.jpg"><h3>Dr. Maitri Chand, PhD.</h3>
<ul>
<li><i class="icon-minus"></i>Marriage and Family Therapist</li>
<li><i class="icon-minus"></i>Director at Healing Relationships, New Delhi</li>
<li><i class="icon-minus"></i>Doctor of Philosophy in Marriage /Family Therapy, The Florida State University</li>
</ul>
</div>
<div class="advpro"><img src="{$cdnurl}/images/drmonika.jpg"><h3>Dr. Monika Sharma</h3>
<ul>
<li><i class="icon-minus"></i>Doctorate in Psychology with specialization in Psychometric and Social Psychology, Indian Statistical Institute.</li>
<li><i class="icon-minus"></i>Assistant Professor of Psychology, Christ University, Bangalore</li>
</ul>
</div>
</div>
</div>
</div>


<!-- Footer -->
<div align="center" class="ftrbg">
<div class="pagefooter" align="center">

	<div class="footerlink">
		<ul>
        	<li class="padt8"><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
            <li class="padt8"><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
        	<li class="padt8"><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
			<li class="padt8"><a>True Compatibility</a></li>
			<li class="padt8"><!--© 2014, trulymadly.com,  --><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>	
            <li class="padt8"><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>		
			<li class="padt8"><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
            <li><div class="followus">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div></li>
		</ul>
		<p class="copyr">© 2015, trulymadly.com</p>
	</div>
</div>
</div>
<!-- Footer End-->


<script src="{$cdnurl}/js/common/jquery.min.js"></script>
<script>
$(function() {
	// Clickable Dropdown
	$('.click-nav > ul').toggleClass('no-js js');
	$('.click-nav .js ul').hide();
	$('.click-nav .js').click(function(e) {
		$('.click-nav .js ul').slideToggle(200);
		$('.clicker').toggleClass('active');
		e.stopPropagation();
	});
	$(document).click(function() {
		if ($('.click-nav .js ul').is(':visible')) {
			$('.click-nav .js ul', this).slideUp();
			$('.clicker').removeClass('active');
		}
	});
});

		$(function() {
			// Clickable Dropdown
			$('.click-nav2 > ul').toggleClass('no-js2 js');
			$('.click-nav2 .js ul').hide();
			$('.click-nav2 .js').click(function(e) {
				$('.click-nav2 .js ul').slideToggle(200);
				$('.clicker2').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav2 .js ul').is(':visible')) {
					$('.click-nav2 .js ul', this).slideUp();
					$('.clicker2').removeClass('active');
				}
			});
		});
</script>

</body>
</html>
