<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="Description" content="When you come to us, we concentrate on you and find prospective people who would fit your life.">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="Author" content="TrulyMadly.com"/>
<meta name="Copyright" content="2014 TrulyMadly.com"/>
<meta name="rating" content="General"/>
<meta name="language" content="English"/>
<title>About Us</title>
<link href="{$cdnurl}/css/staticstyle.css?v=4.1" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
</script>
</head>

<body>

<div class="hdrcontainer" align="center">
<div class="hdrtopstc">

<!-- Top Header -->
<div class="pageheader">
        
<!-- Dropdown 1 -->
<div class="click-nav">
<ul class="no-js">
<li><a class="icon-reorder ddicon clicker"></a>
<ul>
					<li><a>About Us</a></li>
                    <li><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
					<li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
					<li><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
</ul>
</li>
</ul>
</div>
<!-- Dropdown 1 end-->

<a href="{$baseurl}/index.php"><img src="{$cdnurl}/images/common/trulymadly_logo.png" width="172" height="35" class="sitelogo"></a>

<div class="followus hdrsoclink">
    	<p class="graytxt">Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>

</div>
<!-- Top Header end -->

</div>
</div>

<!-- Header responsive -->
<div class="respheader">
	<!-- Dropdown 2 -->
			<div class="click-nav2" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
			<ul class="no-js2">
				<li>
					<a class="clicker2 icon-reorder"></a>
					<ul style="display:none;">
                    <!--<li><a href="{$baseurl}/infog/aboutus.html" target="_blank">About Us</a></li>
					<li><a href="{$baseurl}/infog/ScientificMatching.html" target="_blank">Matching Process</a></li>
					<li><a href="{$baseurl}/infog/TrustSecurity.html" target="_blank">Trust & Security</a></li>
					<li><a href="#" target="_blank">Compatibility Model</a></li>
                    <li><a href="#" target="_blank">Media Coverage</a></li> -->
                    <li><a>About Us</a></li>
                    <li><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
					<li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
					<li><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
					</ul>
				</li>
			</ul>
			</div>
            <!-- Dropdown 2 end-->
            <div class="followus hdrsoclink">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>
</div>
<!-- Header responsive end -->

<div class="abtpagebg"></div>

<div class="stcsec2">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg4">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg5">
<div class="stcsec2txt">
<h2 class="txtaleft">About Us</h2>
<p>
Epic love stories need epic beginnings. Not random introductions. No neighbourhood aunty finding you a girl who can cook or a boy who earns well. Thankfully when you come to us, we concentrate on you and find prospective people who would fit your life. We think of ourselves as those over-enthusiast friends who always have someone great to introduce to you. And that is the power of inspiration drawn from a cheesy love song that got us started in the first place.
</p>

</div>
</div>

<div class="stcsec1" align="center">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg2">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg3">
<div class="stcsec1txt">
<h2>Our Team</h2>
<div class="advprosec" align="center">
<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/sachin.jpg) center center no-repeat;"></div>
<div class="back" align="center">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Mr. Perfect<br><span class="txtpink">Madly</span> Globetrotter</p></div>
</div>
</div>
</div>
<h3>Sachin Bhatia<br><span>Co-founder</span></h3>
<ul>
<li><i class="icon-minus"></i>Co-Founder/CMO of MakeMyTrip</li>
<li><i class="icon-minus"></i>Delhi University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/hitesh.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> The Monk<br><span class="txtpink">Madly</span> Gym Addict</p></div>
</div>
</div>
</div>
<h3>Hitesh Dhingra<br><span>Co-founder</span></h3>
<ul>
<li><i class="icon-minus"></i>Founder & Ex-CEO of Letsbuy.com</li>
<li><i class="icon-minus"></i>UBS, Delhi University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/rahul.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Tech Guru<br><span class="txtpink">Madly</span> Comic Buff</p></div>
</div>
</div>
</div>
<h3>Rahul Kumar<br><span>Co-founder</span></h3>
<ul>
<li><i class="icon-minus"></i>Expedia, Oracle, MakeMyTrip</li>
<li><i class="icon-minus"></i>UT Austin, Bangalore University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/sgupta.jpg?v=1) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Geeky <br><span class="txtpink">Madly</span> Bollywood Buff </p></div>
</div>
</div>
</div>
<h3>Shashwat Gupta<br><span>Director - Technology</span></h3>
<ul>
<li><i class="icon-minus"></i>Vdopia, Yahoo, Microsoft</li>
<li><i class="icon-minus"></i>IIT Roorkee</li>
</ul>
</div>

<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/nitish.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Artistic<br><span class="txtpink">Madly</span> Shutterbug </p></div>
</div>
</div>
</div>
<h3>Nitish Vaid<br><span>Art Director</span></h3>
<ul>
<li><i class="icon-minus"></i>Drizzlin</li>
<li><i class="icon-minus"></i>Sheridan College, Toronto</li>
</ul>
</div>

<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/chetan.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Artsy<br><span class="txtpink">Madly</span> Gadget Freak </p></div>
</div>
</div>
</div>
<h3>Chetan Bhardwaj<br><span>Manager - UI/UX</span></h3>
<ul>
<li><i class="icon-minus"></i>Times of India - SimplyMarry, Advent</li>
<li><i class="icon-minus"></i>MDU, Kurukshetra University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/himanshu.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Workhorse<br><span class="txtpink">Madly</span> Witty</p></div>
</div>
</div>
</div>
<h3>Himanshu Jain<br><span>Sr. Software Engineer</span></h3>
<ul>
<li><i class="icon-minus"></i>PayU, ST-Ericsson</li>
<li><i class="icon-minus"></i>Punjab Engineering College</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/ankit.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Geeky<br><span class="txtpink">Madly</span> Sporty </p></div>
</div>
</div>
</div>
<h3>Ankit Jain<br><span>Sr. Software Developer</span></h3>
<ul>
<li><i class="icon-minus"></i>Inmobi, Photon Infotech</li>
<li><i class="icon-minus"></i>UP Technical University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/udbhav.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Wanderer<br><span class="txtpink">Madly</span> Party Animal</p></div>
</div>
</div>
</div>
<h3>Udbhav Rai<br><span>Sr. Software Engineer</span></h3>
<ul>
<li><i class="icon-minus"></i>iXiGO</li>
<li><i class="icon-minus"></i>Purdue University, NIT Surat</li>
</ul>
</div>

<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/rajesh.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Codeoholic <br><span class="txtpink">Madly</span> Foodie</p></div>
</div>
</div>
</div>
<h3>Rajesh Singh<br><span>Software Developer</span></h3>
<ul>
<li><i class="icon-minus"></i>Vdopia</li>
<li><i class="icon-minus"></i>IP University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/sumit.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Spell Checker<br><span class="txtpink">Madly</span> Scribbler </p></div>
</div>
</div>
</div>
<h3>Sumit Kumar<br><span>Software Engineer</span></h3>
<ul>
<li><i class="icon-minus"></i>REMTECH Roorkee</li>
</ul>
</div>

<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/devesh.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Mastermind<br><span class="txtpink">Madly</span> Outdoorsy</p></div>
</div>
</div>
</div>
<h3>Devesh Batra<br><span>Software Engineer</span></h3>
<ul>
<li><i class="icon-minus"></i>Open Solutions</li>
<li><i class="icon-minus"></i>NSIT, Delhi Univerity</li>
</ul>
</div>

<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/poonam.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Whiz Kid<br><span class="txtpink">Madly</span> Craftsman </p></div>
</div>
</div>
</div>
<h3>Poonam<br><span>QA Engineer</span></h3>
<ul>
<li><i class="icon-minus"></i>Dronacharya College of Engineering</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/sonal.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Cyber Chick <br><span class="txtpink">Madly</span> Fashionista</p></div>
</div>
</div>
</div>
<h3>Sonal Aggarwal<br><span>QA Engineer</span></h3>
<ul>
<li><i class="icon-minus"></i>Dronacharya College of Engineering</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/madhuri.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Wordsmith <br><span class="txtpink">Madly</span> Lovey-dovey </p></div>
</div>
</div>
</div>
<h3>Madhuri Iyer<br><span>Content & Community Manager</span></h3>
<ul>
<li><i class="icon-minus"></i>Jabong.com</li>
<li><i class="icon-minus"></i>Gujarat National Law University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/smita.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Miss Jugaad<br><span class="txtpink">Madly</span> Libra </p></div>
</div>
</div>
</div>
<h3>Smita Prakash<br><span>Relationship Manager</span></h3>
<ul>
<li><i class="icon-minus"></i>MakeMyTrip, Aptara</li>
<li><i class="icon-minus"></i>Delhi University</li>
</ul>
</div>


<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/krishna.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Stylista <br><span class="txtpink">Madly</span> Kickass Boxer</p></div>
</div>
</div>
</div>
<h3>Christina John<br><span>Customers Support Executive</span></h3>
<ul>
<li><i class="icon-minus"></i>S.S.Engineering</li>
</ul>
</div>

<div class="advpro1">
<div class="flip-container" ontouchstart="this.classList.toggle('hover');" align="center">
<div class="flipper">
<div class="front" style="background: url({$cdnurl}/images/aditya.jpg) center center no-repeat;"></div>
<div class="back">
<div class="fliptxt"><p><span class="txtblue">Truly</span> Excel Guru<br><span class="txtpink">Madly</span> Kite Runner</p></div>
</div>
</div>
</div>
<h3>Aditya Baldawa<br><span>Customers Support Executive</span></h3>
<ul>
<li><i class="icon-minus"></i>Helios Lifestyle, Aristotle</li>
<li><i class="icon-minus"></i>Shakambhar College,  Jaipur University</li>
</ul>
</div>

</div>
</div>
</div>

<div class="stcsec2">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg4">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg5">
<div class="stcsec3txt">
<h2 class="txtaleft">Contact Us</h2>
<ul>
<li>
General Queries - <br>
<a href="mailto:contact@trulymadly.com">contact@trulymadly.com</a>
</li>
<li>Careers -<br>
<a href="mailto:careers@trulymadly.com">careers@trulymadly.com</a>
</li>
<li>Site &amp; Usability Related - <br>
<a href="mailto:rahul.kumar@trulymadly.com">rahul.kumar@trulymadly.com</a>
</li>
</ul>

</div>
</div>
<!-- Footer -->
<div align="center" class="ftrbg">
<div class="pagefooter" align="center">

	<div class="footerlink">
		<ul>
			<li class="padt8"><a>About Us</a></li>
            <li class="padt8"><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
            <li class="padt8"><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
			<li class="padt8"><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
			<li class="padt8"><!--© 2014, trulymadly.com,  --><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
            <li class="padt8"><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>			
			<li class="padt8"><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
            
            <li><div class="followus">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div></li>
		</ul>
		<p class="copyr">© 2015, trulymadly.com</p>
	</div>
</div>
</div>
<!-- Footer End-->


<script src="{$cdnurl}/js/common/jquery.min.js"></script>
<script>
$(function() {
	// Clickable Dropdown
	$('.click-nav > ul').toggleClass('no-js js');
	$('.click-nav .js ul').hide();
	$('.click-nav .js').click(function(e) {
		$('.click-nav .js ul').slideToggle(200);
		$('.clicker').toggleClass('active');
		e.stopPropagation();
	});
	$(document).click(function() {
		if ($('.click-nav .js ul').is(':visible')) {
			$('.click-nav .js ul', this).slideUp();
			$('.clicker').removeClass('active');
		}
	});
});

		$(function() {
			// Clickable Dropdown
			$('.click-nav2 > ul').toggleClass('no-js2 js');
			$('.click-nav2 .js ul').hide();
			$('.click-nav2 .js').click(function(e) {
				$('.click-nav2 .js ul').slideToggle(200);
				$('.clicker2').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav2 .js ul').is(':visible')) {
					$('.click-nav2 .js ul', this).slideUp();
					$('.clicker2').removeClass('active');
				}
			});
		});
</script>

</body>
</html>
