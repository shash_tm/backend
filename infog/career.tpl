<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="Description" content="Be innovative. Be creative. Go crazy. We give you the freedom to be yourself.">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="Author" content="TrulyMadly.com"/>
<meta name="Copyright" content="2014 TrulyMadly.com"/>
<meta name="rating" content="General"/>
<meta name="language" content="English"/>
<title>Work With Us</title>
<link href="{$cdnurl}/css/staticstyle.css?v=4.1" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
</script>
</head>

<body>

<div class="hdrcontainer" align="center">
<div class="hdrtopstc">

<!-- Top Header -->
<div class="pageheader">
        
<!-- Dropdown 1 -->
<div class="click-nav">
<ul class="no-js">
<li><a class="icon-reorder ddicon clicker"></a>
<ul>
					<li><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
                    <li><a>Work With Us</a></li>
					<li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
					<li><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
</ul>
</li>
</ul>
</div>
<!-- Dropdown 1 end-->

<a href="{$baseurl}/index.php"><img src="{$cdnurl}/images/common/trulymadly_logo.png" width="172" height="35" class="sitelogo"></a>

<div class="followus hdrsoclink">
    	<p class="graytxt">Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>

</div>
<!-- Top Header end -->

</div>
</div>

<!-- Header responsive -->
<div class="respheader">
	<!-- Dropdown 2 -->
			<div class="click-nav2" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
			<ul class="no-js2">
				<li>
					<a class="clicker2 icon-reorder"></a>
					<ul style="display:none;">
                    <!--<li><a href="{$baseurl}/infog/aboutus.html" target="_blank">About Us</a></li>
					<li><a href="{$baseurl}/infog/ScientificMatching.html" target="_blank">Matching Process</a></li>
					<li><a href="{$baseurl}/infog/TrustSecurity.html" target="_blank">Trust & Security</a></li>
					<li><a href="#" target="_blank">Compatibility Model</a></li>
                    <li><a href="#" target="_blank">Media Coverage</a></li> -->
                    <li><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
                    <li><a>Work With Us</a></li>
					<li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
					<li><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
					</ul>
				</li>
			</ul>
			</div>
            <!-- Dropdown 2 end-->
            <div class="followus hdrsoclink">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div>
</div>
<!-- Header responsive end -->

<div class="careerpagebg"></div>

<div class="stcsec2">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg4">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg5">
<div class="stcsec4txt">
<h2 class="txtaleft">Work with us</h2>
<ul>
<li><i class="icon-ok"></i> &nbsp;Love playing wingman among your friends?</li>
<li><i class="icon-ok"></i> &nbsp;Love to rewrite the rules of dating?</li>
<li><i class="icon-ok"></i> &nbsp;Love to work and work on what you love? </li>
</ul>

<p>
<span>Be innovative. Be creative. Go crazy.</span> We give you the freedom to be yourself. <br><br>
Here's your chance to work with some of the most whacky minds in a crazy, motivated team environment. <br><br>
Enough about us- we are curious about you. Email us at <a href="mailto:careers@trulymadly.com">careers@trulymadly.com</a>
</p>

</div>
</div>

<div class="stcsec1" align="center">
<img src="{$cdnurl}/images/bghearts1.gif" class="hrtbg2">
<div class="stcsec1txt">
<h2>Current Vacancies</h2>
<ul class="openings">
<li onClick="document.getElementById('career6').style.display='block';"><i class="icon-briefcase"></i> &nbsp;Head of Data Science</li>
<li onClick="document.getElementById('career5').style.display='block';"><i class="icon-briefcase"></i> &nbsp;Mobile Product Manager</li>
<!--<li onClick="document.getElementById('career1').style.display='block';"><i class="icon-briefcase"></i> &nbsp;Customer Engagement Manager</li>-->
<li onClick="document.getElementById('career2').style.display='block';"><i class="icon-briefcase"></i> &nbsp;Mobile App Developers</li>
<li onClick="document.getElementById('career3').style.display='block';"><i class="icon-briefcase"></i> &nbsp;Senior Backend Developer</li>
<li onClick="document.getElementById('career4').style.display='block';"><i class="icon-briefcase"></i> &nbsp;Machine Learning Expert</li>
</ul>
</div>
</div>

<!-- Footer -->
<div align="center" class="ftrbg">
<div class="pagefooter" align="center">

	<div class="footerlink">
		<ul>
			<li class="padt8"><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
            <li class="padt8"><a>Work With Us</a></li>
            <li class="padt8"><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
			<li class="padt8"><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
			<li class="padt8"><!--© 2014, trulymadly.com,  --><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>	
            <li class="padt8"><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>		
			<li class="padt8"><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
            <li><div class="followus">
    	<p>Follow us :</p>
    	<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    	<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    	<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    	<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
        <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    </div></li>
		</ul>
		<p class="copyr">© 2015, trulymadly.com</p>
	</div>
</div>
</div>
<!-- Footer End-->

<!--
<div id="career1" style="display:none;" >
	<div class="difusescreen" align="center" onClick="document.getElementById('career1').style.display='none';"></div>
	<div class="popupframe" >
	<a onClick="document.getElementById('career1').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<h4>Customer Engagement Manager</h4>
<ul>
<li>Female candidates preferred.</li>
<li>Responsible for customer engagement and interactions.</li>
<li>Masters in Communication/Marketing from a premier institute.</li>
<li>Adept with numbers/analytics and should be data driven. </li>
<li>Prior work experience recommended. (Freshers can apply) </li>
</ul>
</div>
</div>-->


<div id="career2" style="display:none;" >
	<div class="difusescreen" align="center" onClick="document.getElementById('career2').style.display='none';"></div>
	<div class="popupframe" >
	<a onClick="document.getElementById('career2').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<h4>Mobile App Developers</h4>
<ul>
<li>Minimum 2 years of experience in developing scalable Android or IOS apps</li>
<li>BS or MS in Computer science or ECE degree or related technical discipline from a premier engineering institute</li>
<li>Expert in data structures and algorithms</li>
</ul>
</div>
</div>


<div id="career3" style="display:none;" >
	<div class="difusescreen" align="center" onClick="document.getElementById('career3').style.display='none';"></div>
	<div class="popupframe" >
	<a onClick="document.getElementById('career3').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<h4>Senior Backend Developer</h4>
<ul>
<li>Minimum 4-5 years of experience in developing scalable back-end systems which can handle millions of visitors</li>
<li>Proficient in PhP/MySQL and caching solutions</li>
<li>CS degree from a premier university</li>
<li>Adept in data structures and algorithms</li>
<li>Recommended experience in MongoDB, Node.js, Hadoop, Hive etc. </li>
</ul>
</div>
</div>

<div id="career4" style="display:none;" >
	<div class="difusescreen" align="center" onClick="document.getElementById('career4').style.display='none';"></div>
	<div class="popupframe" >
	<a onClick="document.getElementById('career4').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<h4>Machine Learning Expert</h4>
<ul>
<li>MS or PhD in Data Mining/AI or Machine learning from a  premier university.</li>
<li>Minimum 4-5 years of experience in building adaptive recommendation systems</li>
</ul>
</div>
</div>


<div id="career5" style="display:none;" >
	<div class="difusescreen" align="center" onClick="document.getElementById('career5').style.display='none';"></div>
	<div class="popupframe" >
	<a onClick="document.getElementById('career5').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<h4>Mobile Product Manager</h4>
<p style="font-size:20px; padding:0 0 0 20px; color:#999;">Responsibilities:</p>
<ul>
<li>Execute on the mobile product roadmap</li>
<li>Define product specs and work closely with the tech team to roll them out</li>
<li>Measure and assess the efficacy of the feature</li>
</ul>
<p style="font-size:20px; padding:0 0 0 20px; color:#999;">Requirements:</p>
<ul>
<li>Avid user of mobile apps</li>
<li>Demonstrated success by launching a few great B2C mobile apps</li>
<li>Deep understanding of the mobile eco system with respect to notifications, analytics, design etc.</li>
<li>Technical degree(CS or ECE) from a premier institute </li>
<li>2 to 3 years of experience</li>
</ul>
</div>
</div>

<div id="career6" style="display:none;" >
	<div class="difusescreen" align="center" onClick="document.getElementById('career6').style.display='none';"></div>
	<div class="popupframe" >
	<a onClick="document.getElementById('career6').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<h4>Head of Data Science</h4>
<p style="font-size:20px; padding:0 0 0 20px; color:#999;">Responsibilities:</p>
<ul>
<li>Analyze user behaviour patterns from data to improve user experience</li>
<li>Build predictive models to optimize the recommendation logic</li>
<li>Set up a small team of Data Analysts to assist in data mining and reporting</li>
</ul>
<p style="font-size:20px; padding:0 0 0 20px; color:#999;">Requirements:</p>
<ul>
<li>Minimum 5 years of experience in predictive modelling with big data sets</li>
<li>Deep understanding and working knowledge of regression and machine learning techniques used for predictive analysis</li>
<li>Masters/Ph.D in Statistics/Maths or Computer Science (with specialisation in Data Mining) from a Tier-1 institute</li>
</ul>
</div>
</div>

<script src="{$cdnurl}/js/common/jquery.min.js"></script>
<script>
$(function() {
	// Clickable Dropdown
	$('.click-nav > ul').toggleClass('no-js js');
	$('.click-nav .js ul').hide();
	$('.click-nav .js').click(function(e) {
		$('.click-nav .js ul').slideToggle(200);
		$('.clicker').toggleClass('active');
		e.stopPropagation();
	});
	$(document).click(function() {
		if ($('.click-nav .js ul').is(':visible')) {
			$('.click-nav .js ul', this).slideUp();
			$('.clicker').removeClass('active');
		}
	});
});

		$(function() {
			// Clickable Dropdown
			$('.click-nav2 > ul').toggleClass('no-js2 js');
			$('.click-nav2 .js ul').hide();
			$('.click-nav2 .js').click(function(e) {
				$('.click-nav2 .js ul').slideToggle(200);
				$('.clicker2').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav2 .js ul').is(':visible')) {
					$('.click-nav2 .js ul', this).slideUp();
					$('.clicker2').removeClass('active');
				}
			});
		});
</script>

</body>
</html>
