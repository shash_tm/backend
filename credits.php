<?php

require_once dirname(__FILE__).'/include/config.php';
//require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
//require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/include/systemMessages.php";
require_once dirname ( __FILE__ ) . "/include/header.php";

class Credits{ 

	private $user_id;

	function __construct($user_id){
		$this->user_id = $user_id;
	}

	public function getCredits(){
		$uuDBO = new userUtilsDBO();
		$creditRow = $uuDBO->getCurrentCredits(array($this->user_id), true);
		return $creditRow['current_credits'];
	}
}

  
try{

	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$systemMsg = null;
	//if user id is valid and redirect
	if(isset($user_id)){
		$creditsObj = new Credits($user_id);
		$credits = $creditsObj->getCredits();
		$header = new header($user_id);
		$header_values = $header->getHeaderValues();

		$smarty->assign("current_credits", $credits);
		$smarty->assign("header", $header_values);
		$smarty->display("templates/match/credits.tpl");
	}
	else{
		header("Location:matches.php");
	}
}
catch(Exception $e){
	trigger_error("PHP WEB:" . $e->getTraceAsString(), E_USER_WARNING);
}

?>