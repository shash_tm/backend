<?php
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/userQueue.php";

class UserActionConstant {
	const like = 0;
	const likeMe = 1;
	const favourite = 2;
	const hide = 3;
	const reject = 4;
	const rejectMe = 5;
	const credit_used = 6;
	const can_communicate = 7;
}

class UserActionConstantValues {
	const like = "like";
	//const like_back = "like_back";
	const likeMe = "likeMe";
	const favourite = "favourite";
	const hide = "hide";
	const reject = "reject";
	const rejectMe = "rejectMe";
	const credit_used = "credit_used";
	const view="view";
	const rejected_or="rejected_or";
	const can_communicate = "can_communicate";

}

/*
 * Describes the possible user actions when a user clicks on a profile's like/fav/reject @Himanshu
 */
class UserActions extends TMObject{
	private $userId;
	//private $conn;
	private $usrQ;
	private $currentQ;
	private static $salt = "trulyrocks";

	function __construct() {
		parent::__construct (  );

		//global $conn;
		//$this->conn = $conn;
		//$this->userId = $user_id;
		$userQueue = new userQueue ();
		$this->usrQ = $userQueue;
	}

	private function setBit($user1, $user2, $bit) {

		// var_dump("$user1, $user2, $bit");
		// var_dump("came here");
		$val = 1 << $bit;
		$sql = $this->conn->Prepare ( "insert into user_action(user1,user2,actions) values(?,?,$val) on duplicate key update actions=actions|$val" );
		$this->conn->Execute ( $sql, array (
		$user1,
		$user2
		) );

	}

	public  function generateQuery($user_action,$uid,$mid){
		$timestamp= time();
		$hash=UserActions::getHash($user_action,$timestamp,$uid,$mid);
		$query="uid=$uid&mid=$mid&sh=$hash&ts=$timestamp&ua=$user_action";
		return $query;
	}

	private  function getHash($user_action, $timestamp, $user1, $user2) {
		return md5 ( "$user_action|$timestamp|$user1|$user2 |$salt" );
	}

	public  function checkHash(){
		$uid=$_REQUEST['uid'];
		$mid=$_REQUEST['mid'];
		$hash_old=$_REQUEST['sh'];
		$timestamp=$_REQUEST['ts'];
		$user_action=$_REQUEST['ua'];
		$hash_new=UserActions::getHash($user_action,$timestamp,$uid,$mid);
		return $hash_new==$hash_old;
	}


	private function getBitToActionMapping($action) {
		$actions = array ();
		$rejectedOr=0;
		if ($action & (1 << UserActionConstant::like))
		$actions [] = UserActionConstantValues::like;
		if ($action & (1 << UserActionConstant::likeMe))
		$actions [] = UserActionConstantValues::likeMe;
		if ($action & (1 << UserActionConstant::favourite))
		$actions [] = UserActionConstantValues::favourite;
		if ($action & (1 << UserActionConstant::hide))
		$actions [] = UserActionConstantValues::hide;
		if ($action & (1 << UserActionConstant::reject))
		{
			$actions [] = UserActionConstantValues::reject;
			$rejectedOr=1;
		}
		if ($action & (1 << UserActionConstant::rejectMe)){
			$actions [] = UserActionConstantValues::rejectMe;
			$rejectedOr=1;
		}
		if ($action & (1 << UserActionConstant::credit_used))
		$actions [] = UserActionConstantValues::credit_used;

		if($rejectedOr==1){
			$actions [] = UserActionConstantValues::rejected_or;
		}
		if ($action & (1 << UserActionConstant::can_communicate))
		$actions [] = UserActionConstantValues::can_communicate;
			
		return $actions;
	}

	public function getUserActionsDone($user1, $user_2_array) {
		if (count ( $user_2_array ) == 0)
		return null;
		$user_2 = implode ( ",", $user_2_array );

		$sql = "select user2,actions from user_action where user1=$user1 and user2 in ($user_2)";
		$rs = $this->conn->Execute ( $sql );
		$data = $rs->GetRows ();

		$output = array ();
		foreach ( $data as $val ) {
			$output [$val ['user2']] = $this->getBitToActionMapping ( $val ['actions'] );
		}
		return $output;
	}



	public function addToQueue($user1, $user2, $table) {
		$sql = $this->conn->Prepare ( "insert ignore into $table(user1,user2,timestamp) values(?,?,now())" );
		$this->conn->Execute ( $sql, array (
		$user1,
		$user2
		) );
	}

	/**
	 * user1 viewed user2
	 * we will put the value in the queue of user2
	 * will return true if we were able to add a new values
	 * else will return false
	 * @param unknown $user1
	 * @param unknown $user2
	 */
	private function setView($user1,$user2){
		//check in redis if value exists
		$key="vq:$user2";
		$output=$this->redis->sAdd($key , $user1);
		//var_dump($output);
		//member was not present there
		if($output==1){
			$date=date('Y-m-d');
			$list_key="vql:$user2";
			$val="$user1:$date";
			$this->redis->lPush($list_key, $val);
			return true;
		}
		return false;
	}

	public function removeFromQueue($user1, $user2, $table, $is_removed = null) {

		/*if(isset($is_removed)){
			$sql = $this->conn->Prepare("UPDATE $table set is_removed = ? WHERE user1 = ? AND user2 = ?");
			$this->conn->Execute($sql, array(1, $user1, $user2));
			}
			else{*/
		$sql = $this->conn->Prepare ( "delete from $table where user1=? and user2=? " );
		$this->conn->Execute ( $sql, array (
		$user1,
		$user2
		) );
		//}
	}

	public function getIdsFromQueue($table, $start) {
		$sql = $this->conn->Prepare ( "Select user2 from $table where user1 = ? limit ?,10" );
		$exec = $this->conn->Execute ( $sql, array (
		$this->userId,
		$start
		) );
		$res = $exec->GetRows ();

		$ids = array ();
		foreach ( $res as $key => $val )
		$ids [] = $val ['user2'];
		return $ids;
	}

	public function getCurrentQueue($user1, $user2) {
		// user_favorites_queues,user_hide_queues, user_likes_queues
		$sql = $this->conn->Prepare ( "Select * from user_hide_queues where user1=? and user2=? " );
		$exec = $this->conn->Execute ( $sql, array (
		$user1,
		$user2
		) );

		if ($exec->RowCount () > 0) {
			return 'hide';
		}

		$sql3 = $this->conn->Prepare ( "Select * from user_likes_queues where user1=? and user2=? " );
		$exec = $this->conn->Execute ( $sql3, array (
		$user1,
		$user2
		) );

		if ($exec->RowCount () > 0) {
			return 'like';
		}

		$sql2 = $this->conn->Prepare ( "Select * from user_favorites_queues where user1=? and user2=? " );
		$exec = $this->conn->Execute ( $sql2, array (
		$user1,
		$user2
		) );

		if ($exec->RowCount () > 0) {
			return 'favorite';
		}

		return 'no_action_taken';
	}

	/**
	 * user1 like user2
	 * set like queue of user2
	 * push two values one key and one date
	 * @param unknown $user_1
	 * @param unknown $user_2
	 */
	private function setLikeMe($user1,$user2){
		$key="lq:$user2";
		$time=time();
		$this->redis->zAdd($key, $time, $user1);
	}

	/**
	 * user1 liked back user2
	 */
	private function removeLikeMe($user1,$user2){
		//var_dump("cdunc dinu sdns");
		$key="lq:$user1";
		$this->redis->zRem($key, $user2);
	}

	public function performAction($user_id,$match_id, $action, $current_queue) {
		$key = "user:match:$user_id";

		$userAction=$this->getUserActionsDone($user_id, array($match_id));
		$userAction=$userAction[$match_id];

		// :TODO to be filled
		if ($action == UserActionConstantValues::view) {
			$new_view=$this->setView( $user_id,$match_id);
			//echo "here";
			if($new_view==true){
				$this->addToQueue ( $match_id, $user_id, "user_who_view_me_queues" );
			}
		}
		/*else if (($action == UserActionConstantValues::favourite)) {
			$this->incrementCounter(UserActionConstantValues::favourite,$user_id);
			$this->addToQueue ( $user_id, $match_id, "user_favorite" );
			$this->setBit ($user_id, $match_id, UserActionConstant::favourite );
			//:TODO: check useraction and remove accordingly
			if ($current_queue == 'hide') {
			$this->removeFromQueue ( $user_id, $match_id, "user_hide_queues" );
			}

			}*/
		/**
		 * cant call hide if you have liked a profile
		 */
		/*else if (($action == UserActionConstantValues::hide)) {
			if ($current_queue == UserActionConstantValues::favourite) {
			$this->removeFromQueue ( $user_id, $match_id, "user_favorites_queues" );
			} else if ($current_queue == 'like') {
			// $this->removeFromQueue ( $this->userId, $match_id, "user_likes_queues" );
			}

			$this->incrementCounter(UserActionConstantValues::hide,$user_id);
			$this->addToQueue ( $user_id, $match_id, "user_hide" );
			$this->setBit ($user_id, $match_id, UserActionConstant::hide );
			}*/
		else if (($action == UserActionConstantValues::reject)) {
			$this->addToQueue ( $user_id, $match_id, "user_reject" );
			$this->addToQueue ( $match_id, $user_id, "user_rejectMe" );
			$this->setBit ( $user_id, $match_id, UserActionConstant::reject );
			$this->setBit ( $match_id, $user_id, UserActionConstant::rejectMe );

			$this->remove_from_recommendation($user_id, $match_id);
			$this->remove_from_recommendation($match_id, $user_id);

			if(in_array(UserActionConstantValues::likeMe,$userAction)){
				$this->addToQueue ( $match_id, $user_id, "user_likeMe_rejectMe" );
				$this->incrementNotification($match_id);
			}
			/*if(in_array(UserActionConstantValues::like,$userAction)){
				//$this->removeFromQueue($user_id, $match_id, "user_like");
				$this->removeLikeMe($user_id,$match_id);
				$this->decrementCounter(UserActionConstantValues::like, $user_id);
				}

				if(in_array(UserActionConstantValues::likeMe,$userAction)){
				//$this->removeFromQueue($user_id, $match_id, "user_likeMe");
				//$this->removeFromQueue($match_id, $user_id,  "user_like");
				$this->removeLikeMe($match_id,$user_id);
				$this->decrementCounter(UserActionConstantValues::like, $match_id);
				}*/



		}
		else if ($action == UserActionConstantValues::like) {
		//	$this->incrementCounter(UserActionConstantValues::like,$user_id);
			$this->addToQueue ( $user_id, $match_id, "user_like" );
			$this->addToQueue ( $match_id,$user_id, "user_likeMe" );

			$this->addToQueue ( $match_id,$user_id, "user_likeMe_rejectMe" );
			
			$this->setBit ( $user_id, $match_id, UserActionConstant::like );
			$this->setBit ( $match_id, $user_id, UserActionConstant::likeMe );
			$this->incrementNotification($match_id);
			$this->remove_from_recommendation($user_id, $match_id);

		/*	if(in_array(UserActionConstantValues::likeMe,$userAction)){
				$this->removeLikeMe($user_id,$match_id);
				$this->setLikeMe($user_id,$match_id);
				//$this->incrementNotification($user_id);
			}
			else
			{
				$this->setLikeMe($user_id,$match_id);
			}*/

			/*if(in_array(UserActionConstantValues::hide,$userAction) && in_array(UserActionConstantValues::likeMe,$userAction)){
			 	
			$this->removeFromQueue($user_id, $match_id, UserActionConstant::hide, 1);
			$this->decrementCounter(UserActionConstantValues::hide, $user_id);

			}*/
			//set the redis value of likes to display in header

			/*	if ($current_queue == 'hide') {
				$this->removeFromQueue ( $user_id, $match_id, "user_hide_queues" );
				}*/
		}

		if ($action == UserActionConstantValues::can_communicate) {
			$this->addToQueue ( $user_id, $match_id, "user_communicate" );
			$this->addToQueue ( $match_id,$user_id, "user_communicate" );
			$this->setBit ( $user_id, $match_id, UserActionConstant::can_communicate );
			$this->setBit ( $match_id, $user_id, UserActionConstant::can_communicate );
		}

		if ($action == UserActionConstantValues::credit_used) {
			$this->addToQueue ( $user_id, $match_id, "user_credit" );
			$this->setBit ( $user_id, $match_id, UserActionConstant::credit_used );
			$this->setBit ( $user_id, $match_id, UserActionConstant::can_communicate );
			$this->setBit (  $match_id, $user_id, UserActionConstant::can_communicate );
		}

		/*if ($action != UserActionConstantValues::view) {
		 $this->remove_from_recommendation($user_id, $match_id);
			// $this->usrQ->removeFromMatchQueue ( $key, $match_id );
			}*/

		if(($action == UserActionConstantValues::like)||($action == UserActionConstantValues::reject)||($action == UserActionConstantValues::hide)||($action == UserActionConstantValues::favourite)){
			$this->remove_from_recommendation($user_id, $match_id);
		}
	}

	/**
	 * remove user2 from user1 recommendation queue
	 * @param unknown $user1
	 * @param unknown $user2
	 */
	private function remove_from_recommendation($user1,$user2){
		$key	= "u:m:$user1";
		$this->redis->LREM($key, $user2 );
	}

	/**
	 * remove user2 from user1 recommendation queue
	 * @param unknown $user1
	 * @param unknown $user2
	 */
	private function incrementCounter($action,$user_id){
		$key= "u:q:$user_id:$action";
		$this->redis->INCR($key);
	}

	private function incrementNotification($user_id){
		$key= "u:n:$user_id";
		$this->redis->INCR($key);
	}

	private function decrementCounter($action,$user_id){
		$key= "u:q:$user_id:$action";
		$this->redis->DECR($key);
	}

	public function getQueueCounters($user_id){
		$key="u:q:$user_id:";
		$fav_key=$key.UserActionConstantValues::favourite;
		$like_key=$key.UserActionConstantValues::like;
		$hide_key=$key.UserActionConstantValues::hide;

		$counters=$this->redis->mGet(array($fav_key, $like_key, $hide_key));
		$final_array=array();
		$final_array['favorite']=intval($counters[0]);
		$final_array['like']=intval($counters[1]);
		$final_array['hide']=intval($counters[2]);
		return $final_array;
	}
}


//$userAction = new UserActions($_REQUEST['uid']);


//$output=$userAction->getUserActionsDone($_REQUEST['uid'], array($_REQUEST['mid']));

//var_dump($output);
//exit;
