<?php
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ )  . "/include/config_admin.php";
//require_once dirname ( __FILE__ )  . "/../../DBO/messagesDBO.php";
require_once dirname ( __FILE__ ) . "/msg/MessageFullConversation.class.php";
require_once dirname ( __FILE__ ) . "/Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . "/mobile_utilities/pushNotification.php";


class NewTextMessage
{
        private $conn_reporting;
        private $user_id ;
        private $messageFullObj ;
        private $message ;
        private $imageMessage ;
        private $misstm ;
        private $gender ;
        private $notify;
        private $uu ;
        private $messageId ;
        function __construct()
        {
                global $conn_reporting, $miss_tm_id ;
                $this->conn_reporting = $conn_reporting ;
                $this->user_id ;
                $this->messageFullObj ;
                $this->message ;
                $this->imageMessage;
	            $this->misstm= $miss_tm_id;
                $this->gender ;
            $this->notify = new pushNotification();
            $this->uu = new UserUtils();
            $this->messageId;
        }

        private function sendMessage()
        {
                $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
                 $this->messageId =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
              //  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

        }


        private function getMessage()
        {
            $this->message = "Who doesn't like to be pampered with gifts? It's your turn to impress your date! Check out personalized gifts from Shopper's Stop. ".
                            "Shop now- http://bit.ly/TrulyMadly_ShoppersStop";
        }

        private function createMessageConvoObj()
        {
                $this->messageFullObj = new MessageFullConversation ( $this->misstm, $this->user_id );
        }
         private function sendPushNotification()
        {
            $message_url = $this->uu->generateMessageLink($this->user_id,$this->misstm);
            $push_arr =  array("is_admin_set"=> 0,
                                "content_text"=>"Who doesn't like to be pampered with gifts?",
                                "ticker_text"=>"Message From Miss TM",
                                "title_text"=>"TrulyMaldy",
                                "message_url"=>$message_url,
                                "match_id"=>$this->misstm,
                                "msg_type" => "TEXT",
                                "msg_id"=> $this->messageId,
                                "push_type"=>"MESSAGE");
            $this->notify->notify($this->user_id, $push_arr, $this->misstm);
    }

        public function getUsers($target)
        {
            if($target == 'all')
            {
                $sql="select u.user_id, u.gender from user_data ud join user u  on u.user_id = ud.user_id  join messages_queue mq on mq.sender_id = u.user_id
                       join user_app_status uas on uas.user_id = u.user_id
                        where  mq.receiver_id = $this->misstm and mq.blocked_by is null and uas.android_app ='install'";
            } else
            {
                $sql="select u.user_id, u.gender from user_data ud join user u  on u.user_id = ud.user_id  join messages_queue mq on mq.sender_id = u.user_id
                       join user_app_status uas on uas.user_id = u.user_id where  mq.receiver_id = $this->misstm and mq.blocked_by is null
                         and uas.android_app ='install' and u.user_id = $target ";
            }


                      $stmt=$this->conn_reporting->Prepare($sql);
                      $exe=$this->conn_reporting->Execute($stmt,array());
                      $i=0;
                      while($row=$exe->fetchRow())
                      {
			            $this->gender = $row['gender'] ;
                        $this->user_id = $row['user_id'] ;
                      //    echo "user_id :  " .$this->user_id .PHP_EOL;
                        $this->createMessageConvoObj() ;
                        $this->getMessage() ;
                        $this->sendMessage() ;
                         // $this->sendPushNotification();

                      	$i++;
                      }
			         echo "Messages sent to $i people" ;
            var_dump($this->message) ;

        }

}

 
try
{
        if (php_sapi_name() == 'cli')
        {
             $target = $argv[1];
             $message_obj = New NewTextMessage() ;
            if(isset($target) && $target != null)
             $message_obj->getUsers($target);
            else
                echo " NO target";
        }

}
catch (Exception $e)
{
        echo $e->getMessage();
}

?>
