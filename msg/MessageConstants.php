<?php 

class MessageConstants{
	
	const miss_tm_first_message="Hi! I'm Miss TM, your guide to make your experience here awesome. After all that's what best buddies are for right?";

	const miss_tm_first_message_male="You couldn't have joined at a better time. Check out SPARK, our newest feature that's got everyone talking! Take a look and try it today! https://youtu.be/InNM5JtTEGc";
	const miss_tm_first_message_female="You couldn't have joined at a better time. Check out SPARK, our newest feature that's got everyone talking! Take a look and try it today! https://youtu.be/S8XGJF_mlbc";

	const miss_tm_first_message_usa_male="Hey there! Welcome to TrulyMadly, THE place for single Indians in US looking for long term commitment. Meet like minded women, especially handpicked for you and begin your quest for love! #trulymadlydesi";
	const miss_tm_first_message_usa_female="Hey there! Welcome to TrulyMadly, THE place for single Indians in US looking for long term commitment. Meet like minded men, especially handpicked for you and begin your quest for love! #trulymadlydesi";

	public static $miss_tm_convo_tips_male = array( 1 => "Introducing Ice-Breaker Tips by Miss TM! \nICE BREAKER TIP #1 \nDon’t know how to break the ice ? Maybe looking at her ‘Favorites’ can inspire a topic! For instance..If she loves reading then ask her about her favourite authors.",
										   2 => "ICE BREAKER TIP #2 \nAt a loss for words? How about you start by complimenting her smile… ",
										   3 => "ICE BREAKER TIP #3 \n“Hi Dear” isn’t a great conversation starter but “hey what do you do besides looking so pretty” just might be! ;)",
											4 => "ICE BREAKER TIP #4 \nInstead of the usual “how are you?” ask what the best part of her day was…",
											5 => "ICE BREAKER TIP #5 \nColdplay or Game of Thrones… try talking about a popular topic",
											6 => "ICE BREAKER TIP #6 \nThe clue to a great opening line might lie in her profile- her school, her interests... Take another look!",
											7 => "ICE BREAKER TIP #7 \nWhy the silence? Let our quizzes do the talking and take it from there..",
											8 => "ICE BREAKER TIP #8 \nHow about a fun game of choices... Long Drives or Movie Dates? Cat person or Dog person? Go!",
											9 => "ICE BREAKER TIP #9 \nGirls love it when you ask them questions! Let's face it, they love talking about themselves!",
											10 => "ICE BREAKER TIP #10 \n \"How you doin?\" worked for Joey, but you can try harder! How about complimenting her pics?",
											11 => "ICE BREAKER TIP #11 \nTell her two truths, one lie about yourself and ask her to guess which is which. Her guesses might leave you surprised!",
											12 => "ICE BREAKER TIP #12 \nUse emojis, it makes the conversation seem less intimidating",
											13 => "ICE BREAKER TIP #13 \nIs she a foodie? Does she love travelling? Read her profile and find common ground to talk about.",
											14 => "ICE BREAKER TIP #14 \nWhat's in a name? A lot! Ask her what her name means or compliment it, it never fails! :)",
											15 => "ICE BREAKER TIP #15 \nGirls love guys with a sense of humour.. how about telling her a funny incident that happened with you?",
											16 => "ICE BREAKER TIP #16 \nPassionate about something? She'd love to hear about it!",
											17 => "ICE BREAKER TIP #17 \nSwap ''craziest thing you've ever done'' stories.",
											18 => "ICE BREAKER TIP #18 \nFears? Strengths? Ask her about things that make you get to know her better.",
											19 => "ICE BREAKER TIP #19 \n Spell checking and typing full sentences go a long way in impressing her!",
											20 => "ICE BREAKER TIP #20 \nStaying in or hitting the dance floor? Why don't you ask what she's doing on the weekend? \nThis was the last tip, we're sure you can chat like a pro by now ;)") ;
	public static $miss_tm_convo_tips_female = array( 1 => "Introducing Ice-Breaker Tips by Miss TM! \nICE BREAKER TIP #1 \nChat at a standstill? Maybe looking at his ‘Favorites’ can inspire a topic! For instance..If he loves reading then ask him about his favourite authors",
										   2 => "ICE BREAKER TIP #2 \nDon't be shy , the more you chat, the more you'll know him.",
										   3 => "ICE BREAKER TIP #3 \nIt never hurts to say hi first... go ahead.",
											4 => "ICE BREAKER TIP #4 \nInstead of the usual response to “how are you?” ask what the best part of his day was…",
											5 => "ICE BREAKER TIP #5 \nWhy the silence? Let our quizzes do the talking and take it from there...",
											6 => "ICE BREAKER TIP #6 \nDoes he love cats or dogs? Find out more about him...",
											7 => "ICE BREAKER TIP #7 \nHas he watched your favourite band live? Go ahead, ask him!",
											8 => "ICE BREAKER TIP #8 \nLet him know you find him attractive, he might just compliment you back ;)",
											9 => "ICE BREAKER TIP #9 \nEveryone loves talking about themselves. Exchange personal stories to know him better!",
											10 => "ICE BREAKER TIP #10 \nHow about some tamasha? Spice things up by playing a role-play scene from a famous movie!",
											11 => "ICE BREAKER TIP #11 \nHello! only worked for Adele. Instead, pose a question from his profile!",
											12 => "ICE BREAKER TIP #12 \nShow him you're not just a pretty face. Charm him with your wit and be candid!",
											13 => "ICE BREAKER TIP #13 \nDon't wait for 24 hours to respond! Get to know him as quickly as possible!",
											14 => "ICE BREAKER TIP #14 \nEveryone loves a good sense of humour, so show you have a funny bone!",
											15 => "ICE BREAKER TIP #15 \nShow people your personality through pics instead of describing it in words!",
											16 => "ICE BREAKER TIP #16 \nWhat's his hashtag story? Why don't you ask him ;) ?",
											17 => "ICE BREAKER TIP #17 \nStaying in bed or hitting the dance floor? Why don't you ask him his weekend plans?",
											18 => "ICE BREAKER TIP #18 \nSilence is never golden. Don't miss out on this chance,ask him things that matter to you.",
											19 => "ICE BREAKER TIP #19 \nColdplay or Guns & Roses ? Find out what's music to his ears",
											20 => "ICE BREAKER TIP #20 \nJust ''Hey!'' won't cut it, how about a warm compliment instead? \nThis was the last tip, we're sure you can chat like a pro by now ;)") ;
}

?>