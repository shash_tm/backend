<?php

  /***************************************
  * http://www.program-o.com
  * PROGRAM O
  * Version: 2.4.7
  * FILE: chatbot/conversation_start.php
  * AUTHOR: Elizabeth Perreau and Dave Morton
  * DATE: MAY 17TH 2014
  * DETAILS: this file is the landing page for all calls to access the bots
  ***************************************/
  require_once   dirname ( __FILE__ ) .'/../../../include/config.php';
  require_once dirname ( __FILE__ ) . "/../../../utilities/wordlist-regex.php";
 
  global $conn;
  
  sleep(1);
  global  $badwords_hashtag_misstm;
  
  $options = array (
  		'convo_id'    => FILTER_SANITIZE_STRING,
  		'bot_id'      => FILTER_SANITIZE_NUMBER_INT,
  		'say' => array(
  				'filter'      => FILTER_SANITIZE_STRING,
  				'flags'       => FILTER_FLAG_NO_ENCODE_QUOTES
  		),
  		'format'      => FILTER_SANITIZE_STRING,
  		'debug_mode'  => FILTER_SANITIZE_NUMBER_INT,
  		'debug_level' => FILTER_SANITIZE_NUMBER_INT,
  		'name'        => FILTER_SANITIZE_STRING
  );
  
  
  $form_vars_post = filter_input_array(INPUT_POST, $options);
  $form_vars_get = filter_input_array(INPUT_GET, $options);
  
  $form_vars = array_merge((array)$form_vars_get, (array)$form_vars_post);

  $say=$form_vars['say'];
  $convo_id=$form_vars['convo_id'];
  $say=stripslashes($say);
  $say=strtolower($say);  
  $responses=array();
  $responses[]="Hey! Be sure to check your internet connectivity";
  $responses[]="Hey";
  $responses[]="I'm here to guide you on the app in case you need help.";
  //$responses[]="Your feedback matters a lot to us! I'll surely communicate this to our team. Cheers!";
  $responses[]="Hey! Are you super sure about deleting your account? This will remove all your conversations and matches permanently. You'll then have to start afresh if you want to reactivate your account. If you still feel like you don't want to be here, please tap on delete account in Settings.";
  $responses[]="Hey! You can check out our Help Desk section. Hope it helps :)";
  $responses[]="The best things in life are worth the wait :) Please give it some time, you'll get match once the people you've liked have liked you back. You can then chat with them on our messenger. Have any more questions? Check out our Help Desk. Cheers!";
  //$responses[]="I'm here to guide you on the app in case you need help.";
  $responses[]="How sweet of you to ask! I'm doing great :)";
  $responses[]="Aww.. thanks for the compliment! *Blushing*";
  $responses[]="It's not polite to ask a girl her age !";
  $responses[]=" Great question! Get all the tips for a great profile pic here: http://blog.trulymadly.com/profile-picture/";
  $responses[]="You have successfully unsubscribed from conversation tips by Miss TM.";
 // $responses[]="You will start receiving more tips now"; 
 
  $regex_array=array($responses[0]=>array("/\bchat\W*(?:\w*\W*){1,6}?slow\b/","/\bchat\W*(?:\w*\W*){1,6}?sucks\b/","/\bmessenger\W*(?:\w*\W*){1,6}?slow\b/","/\bmessenger\W*(?:\w*\W*){1,6}?sucks\b/","/\bslow*\b/"),
  		
  		$responses[1]=>array("/\bhi*\b/","/\bhello*\b/","/\bhey*\b/","/\bola*\b/","/\bhey*\b/"),
        $responses[2]=>array("/\bwhats\s+up*\b/","/\bwhat's\s+up*\b/","/\bwassup*\b/","/\bwhat\s+do\s+you\s+do*\b/","/\bwhat\s+\W*(?:\w*\W*){1,6}you\s+doing*\b/","/\bwhy\s+are\s+you\s+here*\b/","/\bwhat\s+is\s+your\s+job*\b/"),
  		$responses[3]=>array("/\baccount\W*(?:\w*\W*){1,6}?delete\b/","/\bdelete\W*(?:\w*\W*){1,6}?account\b/"),
  		$responses[4]=>array("/\bunable\b/","/\bunable\W*(?:\w*\W*){1,6}?to\b/","/\bnot\W*(?:\w*\W*){1,6}?able\b/","/\bhow\W*(?:\w*\W*){1,6}?to\b/"),
  		$responses[5]=>array("/\bno*\W*(?:\w*\W*){1,6}?matches\b/","/\bno*\W*(?:\w*\W*){1,6}?likes\b/","/\bno*\W*(?:\w*\W*){1,6}?profiles\b/","/\bno*\W*(?:\w*\W*){1,6}?profiles\b/","/\bunable\W*(?:\w*\W*){1,6}?matches\b/","/\bunable\W*(?:\w*\W*){1,6}?profiles\b/","/\bunable\W*(?:\w*\W*){1,6}?likes\b/","/\bdidn'tW*(?:\w*\W*){1,6}?matches\b/","/\bdon'tW*(?:\w*\W*){1,6}?matches\b/"),
  	    $responses[6]=>array("/\bhow\W*(?:\w*\W*){1,6}?you\b/"),
  		$responses[7]=>array("/\bpretty*\b/","/\bbeautiful*\b/","/\bsexy*\b/"),  		
  		$responses[8]=>array("/\bhow\W*(?:\w*\W*){1,6}?old\b/","/\bwhat\W*(?:\w*\W*){1,6}?age\b/"),
  		$responses[9]=>array("/\bpic*\W*(?:\w*\W*){1,6}?declined\b/","/\bpic*\W*(?:\w*\W*){1,6}?rejected\b/","/\bpic*\W*(?:\w*\W*){1,6}?upload\b/","/\bphoto*\W*(?:\w*\W*){1,6}?declined\b/","/\bphoto*\W*(?:\w*\W*){1,6}?rejected\b/","/\bphoto*\W*(?:\w*\W*){1,6}?upload\b/","/\bupload*\W*(?:\w*\W*){1,6}?pic*\b/","/\bupload*\W*(?:\w*\W*){1,6}?photo\b/"),
  		$responses[10]=>array("/\bstoptip*\b/")
  	//	$responses[11]=>array("/\bstarttip*\b/")
  		   
  		 
  		  		
  );
  $count=0;
  $miss_response='';
  $starbucks=false;
  foreach ($regex_array as $exprs)
  {
  	$selected_resp=$responses[$count];
  	foreach($exprs as $item)
  	{
  		if(preg_match($item,$say))
  		{
  			//echo $selected_resp;
  			
  			$miss_response=stripslashes($selected_resp);
  			if($count==1&&$redis!=null)
  			{
  				$miss_response=$miss_response.' '.$redis->hget('misstm:client'.$form_vars['convo_id'],'name').' !';
  			}
  			else if($count==10)
  			{
  				$stoptip = true;
  			}
  			else if($count==11)
  			{
  				$starttip = true ;
  			}
  			break;
  		}
  	}
  	$count++;
  }
  
  
  $starbucks=false;
  //var_dump($badwords_hashtag_misstm);
  if(count($badwords_hashtag)>0)
  {   //runDebug(__FILE__, __FUNCTION__, __LINE__, 'JSON Array greater than 1', 2);
	  foreach ($badwords_hashtag as $badWords)
	  {
	  	//runDebug(__FILE__, __FUNCTION__, __LINE__, $badWords." printing words  ", 4);
	  
	  	if (strpos(strtolower($say), $badWords) !== FALSE) {
	  		 
	  		$miss_response="Please don't use such words.";
	  		//$convoArr['aiml']['parsed_template']="Please don't use such words";
	  		break;
	  	}
	  }
  }

  
  
  if($starttip == true || $stoptip == true)
  {
  	try 
  	{
  		if($starttip == true)
  			$status = 'active';
  		else
  			$status = 'inactive' ;
  		$sql = "INSERT INTO user_convo_tips (user_id, status) VALUES (?, ?) ON DUPLICATE KEY UPDATE status = ?";
  		
  		$sql_prep = $conn->Prepare($sql);
  		$param_array = array($convo_id,$status,$status);
  		$exec = $conn->Execute($sql_prep,$param_array );
  	}
  	 catch (Exception $e) 
  	 {
  	 	trigger_error("PHP error".$e->getMessage(),E_USER_WARNING);
  	}
  	
  }
  
  
  if($starbucks)
  {
  	
  	try {
  		
  		//$insert_stmt=$conn->prepare("INSERT ignore into trulymadly_prod.gbq_log(date,tablename) values(:date,:tablename)");
  		$sql="select count(*) as cu from trulymadly_prod.campaign_coupon_codes where campaign_id='1126423' and assigned_to=? ";
  		
  		$stmt=$conn->Prepare($sql);
  		
  		
  		$exe=$conn->Execute($stmt,array($convo_id));
  		$count_arr=$exe->fetchRow();
  		//var_dump($count_arr['cu']);
   		if($count_arr['cu']=="0")
  		{
  		   $sql="select * from trulymadly_prod.campaign_coupon_codes where campaign_id='1126423' and status='available' limit 1";
  		   $stmt=$conn->Prepare($sql);
  		   
  		   $exe=$conn->Execute($stmt,array());
  		   $row=$exe->fetchRow();
  		   //var_dump($row);
  		   if($row===false)
  		   {
  		   		$miss_response="Oops, you've missed out on the Buy One Get One free offer. But you can still win an invitation to our special Valentine's event or Starbucks Gift Cards worth Rs. 500! Just Like the Starbucks profile and we'll tell you how to win. T&C apply.";
  		   }
  		   else 
  		   {
  		   	
  		   	  $coupon_id=$row['coupon_code'];
  		   	  $sql="update trulymadly_prod.campaign_coupon_codes set status='used' ,assigned_to=? where coupon_code= ?";
  		   	  $stmt=$conn->Prepare($sql);
  		   	  $exe=$conn->Execute($stmt,array($convo_id,$coupon_id));
  		   	  $miss_response="Congratulations! Show this code $coupon_id at Starbucks when you buy one beverage to get your second beverage for free! Valid until 14th February 2016. T&C apply";
  		   	  
  		   }
  		   
  		   
  		   
  		   
  		}
  		else 
  		{
  			//You have already used the code,sorry
  			$miss_response='Oops, you already have a Buy One Get One free code! Please go to your nearest Starbucks and show the code to enjoy the offer!';
  		}
  		
  		
  		
  		
  	}
  	catch (Exception $ex)
  	{
  		echo $ex->getMessage();
  	}
  	
  }
  
  
  $output=array();
  $output['usersay']=$say;
  $output['botsay']=$miss_response;
  $output['type']='TEXT';
  $output['convo_id']=$convo_id;
  echo json_encode($output,1);
  /*
  $time_start = microtime(true);
  $script_start = $time_start;
  $last_timestamp = $time_start;
  $thisFile = __FILE__;
 
  //load shared files
  require_once(_LIB_PATH_ . 'PDO_functions.php');
  include_once (_LIB_PATH_ . "error_functions.php");
  include_once(_LIB_PATH_ . 'misc_functions.php');
  ini_set('default_charset', $charset);

  //leave this first debug call in as it wipes any existing file for this session
  runDebug(__FILE__, __FUNCTION__, __LINE__, "Conversation Starting. Program O version " . VERSION . PHP_EOL . 'PHP  version ' . phpversion() . PHP_EOL . "OS: $os version $osv", 0);
  //load all the chatbot functions
  include_once (_BOTCORE_PATH_ . "aiml" . $path_separator . "load_aimlfunctions.php");
  //load all the user functions
  include_once (_BOTCORE_PATH_ . "conversation" . $path_separator . "load_convofunctions.php");
  //load all the user functions
  include_once (_BOTCORE_PATH_ . "user" . $path_separator . "load_userfunctions.php");
  //load all the user addons
  //include_once (_ADDONS_PATH_ . "load_addons.php");
  runDebug(__FILE__, __FUNCTION__, __LINE__, "Loaded all Includes", 4);
  //------------------------------------------------------------------------
  // Error Handler
  //------------------------------------------------------------------------
  // set to the user defined error handler
#  set_error_handler("myErrorHandler");
  $say = '';
  //open db connection
  $dbConn = db_open();
  //initialise globals
  //$convoArr = array();
 
  
  
  
  $convoArr = intialise_convoArray($convoArr);
  $new_convo_id = false;
  $old_convo_id = false;
  $say = '';
  $display = "";

  $options = array (
    'convo_id'    => FILTER_SANITIZE_STRING,
    'bot_id'      => FILTER_SANITIZE_NUMBER_INT,
    'say' => array(
      'filter'      => FILTER_SANITIZE_STRING,
      'flags'       => FILTER_FLAG_NO_ENCODE_QUOTES
    ),
    'format'      => FILTER_SANITIZE_STRING,
    'debug_mode'  => FILTER_SANITIZE_NUMBER_INT,
    'debug_level' => FILTER_SANITIZE_NUMBER_INT,
    'name'        => FILTER_SANITIZE_STRING
  );


  $form_vars_post = filter_input_array(INPUT_POST, $options);
  $form_vars_get = filter_input_array(INPUT_GET, $options);

  $form_vars = array_merge((array)$form_vars_get, (array)$form_vars_post);
  $session_name = 'PGOv' . VERSION;
  session_name($session_name);
  session_start();
  //CUSTOM CODE BY RAGHAV TO HANDLE THE NAME OF THE USER
  $convo_id = session_id();
  
  if (!isset($form_vars['say']))
  {
    error_log('Empty input! form vars = ' . print_r($form_vars, true) . PHP_EOL, 3, _LOG_PATH_ . 'debug_formvars.txt');
    $form_vars['say'] = '';
  }
  $say = ($say !== '') ? $say : trim($form_vars['say']);
 
  
  $debug_level = (isset($_SESSION['programo']['conversation']['debug_level'])) ? $_SESSION['programo']['conversation']['debug_level'] : $debug_level;
  $debug_level = (isset($form_vars['debug_level'])) ? $form_vars['debug_level'] : $debug_level;
  $debug_mode = (isset($form_vars['debug_mode'])) ? $form_vars['debug_mode'] : $debug_mode;
  if (isset($form_vars['convo_id'])) session_id($form_vars['convo_id']);
 
  runDebug(__FILE__, __FUNCTION__, __LINE__,"Debug level: $debug_level" . PHP_EOL . "session ID = $convo_id", 0);
  //if the user has said something
  if (!empty($say))
  {
    // Chect to see if the user is clearing properties
    $lc_say = (IS_MB_ENABLED) ? mb_strtolower($say) : strtolower($say);
    if ($lc_say == 'clear properties')
    {
      runDebug(__FILE__, __FUNCTION__, __LINE__, "Clearing client properties and starting over.", 4);
      $convoArr = read_from_session();
      $_SESSION = array();
      $user_id = (isset($convoArr['conversation']['user_id'])) ? $convoArr['conversation']['user_id'] : -1;
      $sql = "delete from `$dbn`.`client_properties` where `user_id` = $user_id;";
      
      $sth = $dbConn->prepare($sql);
      $sth->execute();



      $numRows = $sth->rowCount();
      $convoArr['client_properties'] = null;
      $convoArr['conversation'] = array();
      $convoArr['conversation']['user_id'] = $user_id;

 	
      // Get old convo id, to use for later
      $old_convo_id = session_id();
      // Note: This will destroy the session, and not just the session data!
      // Finally, destroy the session.
      runDebug(__FILE__, __FUNCTION__, __LINE__, "Generating new session ID.", 4);
      session_regenerate_id(true);
      $new_convo_id = session_id();
      $params = session_get_cookie_params();
      setcookie($session_name, $new_convo_id, time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
      // Update the users table, and clear out any unused client properties as needed
      $sql = "update `$dbn`.`users` set `session_id` = '$new_convo_id' where `session_id` = '$old_convo_id';";
      runDebug(__FILE__, __FUNCTION__, __LINE__, "Update user - SQL:\n$sql", 3);
      
      $sth = $dbConn->prepare($sql);
      $sth->execute();

      $confirm = $sth->rowCount();
      // Get user id, so that we can clear the client properties
      $sql = "select `id` from `$dbn`.`users` where `session_id` = '$new_convo_id' limit 1;";
      $row = db_fetch($sql, null, __FILE__, __FUNCTION__, __LINE__);
      if ($row !== false)
      {
        $user_id = $row['id'];
        $convoArr['conversation']['user_id'] = $user_id;
        $convoArr['conversation']['convo_id'] = $new_convo_id;
        runDebug(__FILE__, __FUNCTION__, __LINE__, "User ID = $user_id.", 4);
        $sql = "delete from `$dbn`.`client_properties` where `user_id` = $user_id;";
        runDebug(__FILE__, __FUNCTION__, __LINE__, "Clear client properties from the DB - SQL:\n$sql", 4);
      }
      
      $say = "Hello";
    }
    //add any pre-processing addons

    $rawSay = $say;
    //$say = run_pre_input_addons($convoArr, $say);
    $say = normalize_text($say);
    
    $bot_id = (isset($form_vars['bot_id'])) ? $form_vars['bot_id'] : $bot_id;
    runDebug(__FILE__, __FUNCTION__, __LINE__, "Details:\nUser say: " . $say . "\nConvo id: " . $convo_id . "\nBot id: " . $bot_id . "\nFormat: " . $form_vars['format'], 2);
    //get the stored vars
   $convoArr = read_from_session();

     
      // CUSTOM CODE TO GET USERNAME FOR NEW USER
	 
         if(isset($form_vars['name'])){
                $convoArr['conversation']['user_name']= trim($form_vars['name']);
                }

    $convoArr = load_default_bot_values($convoArr);
    //now overwrite with the recieved data
    $convoArr = check_set_convo_id($convoArr);
    $convoArr = check_set_bot($convoArr);
    $convoArr = check_set_user($convoArr);
    if (!isset($convoArr['conversation']['user_id']) and isset($user_id)) $convoArr['conversation']['user_id'] = $user_id;
    $convoArr = check_set_format($convoArr);
    $convoArr = load_that($convoArr);
    //save_file(_LOG_PATH_ . 'ca.txt', print_r($convoArr,true));
    //$convoArr = buildNounList($convoArr);
    $convoArr['time_start'] = $time_start;
    $convoArr = load_bot_config($convoArr);
    //if totallines isn't set then this is new user
    runDebug(__FILE__, __FUNCTION__, __LINE__,"Debug level = $debug_level", 0);
    //$debug_level = isset($convoArr['conversation']['debug_level']) ? $convoArr['conversation']['debug_level'] : $debug_level;
    runDebug(__FILE__, __FUNCTION__, __LINE__,"Debug level = $debug_level", 0);
    if (!isset ($convoArr['conversation']['totallines']))
    {
    //load the chatbot configuration for a new user
      $convoArr = intialise_convoArray($convoArr);
      //add the bot_id dependant vars
      $convoArr = add_firstturn_conversation_vars($convoArr);
      $convoArr['conversation']['totallines'] = 0;
      $convoArr = get_user_id($convoArr);
    }
    $convoArr['aiml'] = array();
    //add the latest thing the user said
    $convoArr = add_new_conversation_vars($say, $convoArr);

    //parse the aiml
    $convoArr = make_conversation($convoArr);
    //$convoArr = run_mid_level_addons($convoArr);
    
    $convoArr = log_conversation($convoArr);
    //$convoArr = log_conversation_state($convoArr);
    //$convoArr = write_to_session($convoArr);
    //print_r($convoArr);
    $convoArr = get_conversation($convoArr);
    //$convoArr = run_post_response_useraddons($convoArr);
    //return the values to display
    $display = $convoArr['send_to_user'];
    $time_start = $convoArr['time_start'];
    unset($convoArr['nounList']);
    $final_convoArr = $convoArr;
  }
  else
  {
    runDebug(__FILE__, __FUNCTION__, __LINE__, "Conversation intialised waiting user", 2);
    $convoArr['send_to_user'] = '';
  }
  runDebug(__FILE__, __FUNCTION__, __LINE__, "Closing Database", 2);
  $dbConn = db_close();
  $time_end = microtime(true);
  $time = number_format(round(($time_end - $script_start) * 1000,7), 3);
  display_conversation($convoArr);
  //runDebug(__FILE__, __FUNCTION__, __LINE__,"Hello", 1);
  runDebug(__FILE__, __FUNCTION__, __LINE__, "Conversation Ending. Elapsed time: $time milliseconds.", 0);
  $convoArr = handleDebug($convoArr, $time); // Make sure this is the last line in the file, so that all debug entries are captured.
  //shell_exec('redis-cli --raw keys "*miss_tm*" | xargs redis-cli del'); 

 session_gc();*/
