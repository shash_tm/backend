<?php 
/*
 * @uthor : Arpan Jain
 * Valar Morghulis
 */



require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ )."/../mobile_utilities/pushNotification.php";
require_once dirname ( __FILE__ )."/../include/notificationMessages.php";



// Class including the functions to notify user with the appropriate messages depending upon the reason of the block and no. of times they have been blocked. 

class Block_Notification
{
	private $user_id;
	private $reason;
	private $block_number;
	private $msg;
	private $admin_id;
	private $conn;
	
	function __construct($user_id,$reason) {
		global $admin_id,$conn;
		$this->user_id=$user_id;
		$this->reason=$reason;
		$this->block_number=0;
		$this->msg=NULL;
		$this->admin_id=$admin_id;
		$this->conn=$conn;
	}
	
	private function setBlockNumber()
	{
		$rs = $this->conn->Execute($this->conn->prepare("select * from report_abuse where abuse_id=? and reason=?"),array($this->user_id,$this->reason));
		$this->block_number=$rs->RowCount ();
	}
	private function setMessage()
	{   global $block_message;
		$this->setBlockNumber();
		if ($this->reason=='Misleading profile information' || $this->reason=='Misleading profile information.') {
			switch ($this->block_number) {
				case 1:
				$this->msg=$block_message['Misleading profile information'][1];
				break;
				case 2:
				$this->msg=$block_message['Misleading profile information'][2];
				break;
				case 3:
				$this->msg=$block_message['Misleading profile information'][3];
				break;
				default:
				$this->msg=$block_message['Misleading profile information'][3];
				break;
			}
		}
		
	 else if ($this->reason=='Inappropriate or offensive content' || $this->reason=='Inappropriate or offensive content.') {
			switch ($this->block_number) {
				case 1:
				$this->msg=$block_message['Inappropriate or offensive content'][1];
				break;
				case 2:
				$this->msg=$block_message['Inappropriate or offensive content'][2];
				break;
				case 3:
				$this->msg=$block_message['Inappropriate or offensive content'][3];
				break;
				default:
				$this->msg=$block_message['Inappropriate or offensive content'][3];
				break;
			}
		}
	else 
		$this->msg=NULL;
	}
	
	function test()
	{	$this->setMessage();
	   echo "\n\nNo. of times blocked with this particular reson   ".$this->block_number." \nMessage  ".$this->msg;
	
	}
	
	function notifyUser() {
		global $imageurl;
		$this->setMessage();
		//$uu = new UserUtils();
		//$user_data = $uu->getNamenPic($this->admin_id);
		//$pic_url = $imageurl.$user_data['thumbnail'];
		if($this->reason=='Inappropriate or offensive content'||$this->reason=='Inappropriate or offensive content.')
			$push_type='PROMOTE';
		else 
			$push_type='PROMOTE';
		
		if($this->reason=='Inappropriate or offensive content'||$this->reason=='Inappropriate or offensive content.')
			$event_status='Block_Inappropriate'.$this->block_number;
		else
			$event_status='Block_Misleading'.$this->block_number;
		
		$pushnotify = new pushNotification();
		$push_arr = array (
				"content_text" => $this->msg,
				//"pic_url" => $pic_url,
				"ticker_text" => "Hey! Seems like someone has reported you.",
				"title_text" => "TrulyMadly",
				"push_type" => $push_type,	
				"event_status" => $event_status
		);
		$pushnotify->notify ( $this->user_id, $push_arr, $this->admin_id );
	}
}
/*
try {
	$obj= new Block_Notification(2254, 'Misleading profile information');
	$obj->notifyUser();
}
catch (Exception $e)
{
	echo "\n Exception Occurred   ".$e->getMessage()." Location:   ".$e->getTraceAsString();
}
*/

?>
