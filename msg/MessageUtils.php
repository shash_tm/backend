<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../userQueue.php";
//require_once dirname ( __FILE__ ) . "/../utilities/wordlist-regex.php";

class MessageUtils {
	private $redis;
	public function GMTTOIST($time) {
		return date ( 'M d,Y h:i A', strtotime ( '+330 minutes', strtotime ( $time ) ) );
	}
	
	public function GMTTOISTfromUnixTimestamp($time) {
		return date ( 'M d,Y h:i A', strtotime ( '+330 minutes',  $time  ) );
	}
	public function __construct() {
		global $redis;
				
		$this->redis = $redis;
/*		$this->redis = new Redis();
		$this->redis->connect($redis['IP'], $redis['PORT']);*/
		/*$red = new Redis ();
		$red->connect ( $redis ['IP'], $redis ['PORT'] );
		$this->redis = $red;*/
	}
	public static function messageUnReadCount($user_id) {
		$key = $this->messageUnReadCountKey ($user_id);
		return $this->usrQ->getCount ( $key );
	}
	public function messageUnReadCountKey($user_id) {
		return "messageUnReadCount:$user_id:";
	}
	public function incrMessageUnReadCount($user_id) {
		$key = $this->messageUnReadCountKey ( $user_id );
		$this->redis->incr ( $key );
	}
	public function decrementMessageUnReadCount($user_id, $count) {
		$key = $this->messageUnReadCountKey ( $user_id );
		$output = $this->redis->decrBy ( $key, $count );
		if ($output < 0) {
			$this->redis->set ( $key, 0 );
		}
	}
	
// 	public function sendReply($user_id, $rec_id, $data){
	
// 		if(UserUtils::canMessage($user_id, $rec_id)){
		
// 		$data = trim($data); 

// 		$output=array();
// 		if(!empty($data)){

// 		$data = htmlspecialchars($data);
// 		//$data = htmlentities($data);
		
// 		//TODO: break the strings into words and parse each word 
// 		$arr = explode(' ', $data);
		
// 		$output['msg']=$data;
// 		$isBad = array_intersect($arr, $badwords);
// 		if($isBad!=null){
// 			$msg->storeBadMessage( $data);
// 			$output['error']=true;
// 			$output['error_msg']="\"$data\" contains foul words.Cannot post this.";
// 			echo json_encode($output);
// 		}
// 		else{
// 		$msg->storeMostRecentChat($data, $match_id);
// 		echo json_encode($output);
// 		}
// 		exit;
// 		}
// 		}
// 	}

	/*public static function OneToOneMessageUnReadCount($user_id, $rec_id) {
		$key = $this->oneToOneMessageUnReadCountKey($user_id, $rec_id);
		return $this->usrQ->getCount ( $key );
	}
	//key: from (user_id) to (rec_id) unread count
	public function oneToOneMessageUnReadCountKey($user_id, $rec_id){
		return "OneToOnemessageUnReadCount:$user_id:$rec_id";
	}
	public function incrOneToOneMessageUnReadCount($user_id, $rec_id) {
		$key = $this->messageUnReadCountKey ( $user_id, $rec_id );
		$this->redis->incr ( $key );
	}*/
	
	// If key does not exist, a new key holding a hash is created
	// If field already exists in the hash, it is overwritten
	//ZRANGE msg_send_que:10 0 -1 WITHSCORES
	//KEYS "msg_*"
	//ZRANGE msg_rec_que:1 0 -1 WITHSCORES
	public function setCommunicationQueues($sender_id, $receiver_id, $msg, $time) {
		$receiver_queue_key = "msg_rec_que:$receiver_id";
		$sender_queue_key = "msg_send_que:$sender_id";
		
		$message_hash_key_receiver = $receiver_queue_key . '_msgs';
		$message_hash_key_sender = $sender_queue_key . "_msgs";
		
		//var_dump($time);
		$this->redis->ZADD ( $receiver_queue_key, (-1)*$time, $sender_id );
		$this->redis->ZADD ( $sender_queue_key, (-1)*$time, $receiver_id );
		$val=$this->redis->hMSet ( $message_hash_key_receiver, array (
				$sender_id . ":msg" => $msg,
				$sender_id . ":seen" => 0 
		) );
		//var_dump("hashset value:$val");
		$val=$this->redis->hMSet ( $message_hash_key_sender, array (
				$receiver_id . ":msg" => $msg,
				$receiver_id . ":seen" => 0 
		) );
	}
	

	/**
	 * Receiver is the one who is seeing the data.
	 * mark all messages send to this to check.
	 * and mark similar values in sender id
	 * @param unknown $receiver_id
	 * @param unknown $sender_id
	 */
	public function setUnReadToReadInRecentMessages($receiver_id,$sender_id){
		$receiver_queue_key = "msg_rec_que:$receiver_id";
		$sender_queue_key = "msg_send_que:$sender_id";
		
		$message_hash_key_receiver = $receiver_queue_key . '_msgs';
		$message_hash_key_sender = $sender_queue_key . "_msgs";
		
		$val=$this->redis->hSet ( $message_hash_key_receiver, $sender_id . ":seen", 1 );
		$val=$this->redis->hSet ( $message_hash_key_sender, $receiver_id . ":seen",1 );
	}
	
	
	/**
	 * if get_sender_list==true it means we are fetching the sender list
	 * else we are fetching the receiver list for the user
	 * 
	 * @param unknown $userId        	
	 * @param unknown $get_sender_list        	
	 * @param number $start        	
	 * @param unknown $end        	
	 * @return multitype:unknown
	 */
	public function getCommunicationQueues($userId, $get_sent_list, $start = 0, $end = -1) {
		if ($get_sent_list == true) {
			$queue_key = "msg_send_que:$userId";
		} else {
			$queue_key = "msg_rec_que:$userId";
		}
		
		//var_dump($queue_key);
		$args = $this->redis->ZRANGE ( $queue_key, $start, $end, true );
		
		//var_dump($args);
		
		$ids=array_keys($args);
		$hash_key= $queue_key . "_msgs";
		
		
		//$res = $this->redis->HGETALL ( $q1 );
		$fields=array();
		foreach($ids as $val){
			$fields[]=$val.":msg";
			$fields[]=$val.":seen";
		}
		//var_dump($fields);
		
		$data=$this->redis->hmGet($queue_key. '_msgs', $fields);
		
		$output = array ();
		foreach ( $args as $key => $val ) {
			$output["$key"]=array("tstamp"=>(-1)*$val,"msg"=>$data["$key".":msg"],"seen"=>$data["$key".":seen"]);
		}	
		// var_dump($arr);
		return $output;
	}
}

?>