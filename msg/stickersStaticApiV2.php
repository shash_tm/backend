<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../DBO/quizDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";

class Stickers
{
	private $user_id;
	private $stickers;
	private $city_id;
	private $state_id;

	function __construct($user_id)
	{
		$this->user_id = $user_id;
		$userData = userUtilsDBO::getUserData(array($this->user_id),true);
		$this->city_id = $userData['stay_city'];
		$this->state_id = $userData['stay_state'];
	}

	public function getAllActiveStickers()
	{
		$this->getGloablStickers();
		$this->getStickersOfState();
		$this->getStickersOfCity();
		return $this->stickers;
	}

	private function getGloablStickers()
	{
		$file = "stickersGlobalJson.json" ;
		$allStickers = json_decode(file_get_contents($file),true) ;
		$this->stickers = $allStickers['gallery'];
	}

	private function getStickersOfState()
	{
		$file = "stickersState.json" ;
		$allStickers = json_decode(file_get_contents($file),true) ;
		if(is_array($allStickers))
		{
			$state_stickers = $allStickers[$this->state_id];
			if(is_array($state_stickers) && count($state_stickers)>0)
				$this->stickers = array_merge($state_stickers,$this->stickers);
		}
	}

	private function getStickersOfCity()
	{
		$file = "stickersCity.json" ;
		$allStickers = json_decode(file_get_contents($file),true) ;
		if(is_array($allStickers))
		{
			$cityStickers = $allStickers[$this->city_id];
			if(is_array($cityStickers) && count($cityStickers)>0)
				$this->stickers = array_merge($cityStickers, $this->stickers);
		}
	}
}


try
{
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user['user_id'];
	if(isset($user_id))
	{
		$version = QuizDBO::getVersionFromconfig('sticker');
		$output = array("responseCode" => 200,
			"sticker_version"=> (int)$version);
		$stickers = new Stickers($user_id);
		$output["gallery"] = $stickers->getAllActiveStickers();
	}
	else
	{
		$output["responseCode"] = 401;
	}


	print_r(json_encode($output));
}
catch (Exception $e)
{
	trigger_error($e->getMessage(),E_USER_WARNING);
}


?>