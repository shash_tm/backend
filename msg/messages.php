<?php


$currTime = time();

require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/encoding.php";
require_once dirname(__FILe__)."/MessageConstants.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../DBO/messagesDBO.php";
require_once dirname ( __FILE__ ) . "/../include/header.php";
require_once dirname ( __FILE__ ) . "/../include/systemMessages.php";
require_once dirname ( __FILE__ ) . "/../utilities/counter.php";
require_once dirname ( __FILE__ ) . "/../logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/../DBO/userActionsDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/sparkDBO.php";
require_once dirname(__FILe__)."/MessageFullConversation.class.php";
require_once dirname(__FILE__)."/../user/UserFlags.class.php";
require_once dirname(__FILE__)."/../UserData.php";


 

/*
 * Reading last sent or received by this user of messages along with seen/unseen
 * @Himanshu
 */

class Messages{

	private $user_id;
	private $msgDBO;
	private $uuDBO;
	private $msgKey;
	private $redis;
	private $apk_version;
	private $source;
	//code to add miss tm logic
	private $add_miss_tm=false;
	private $miss_tm_id;
	private $emitter;
	private $login_mobile;
	public function setAPKVersion($apk_version){
		$this->apk_version=$apk_version;
	}
	
	public function setSource($source){
		$this->source=$source;
	}
	
	
	public function setMissTMCreation($request_value){
		if(isset($request_value)&&$request_value=="true")
		$this->add_miss_tm=true;
	}
	

	function __construct($user_id){
		global $redis,$miss_tm_id;
		$this->user_id = $user_id;
		$this->msgDBO = new MessagesDBO();
		$this->uuDBO = new userUtilsDBO();
		$this->msgKey = Utils::$redis_keys['messages'];
		$this->redis = $redis;
		$this->miss_tm_id=$miss_tm_id;
		$this->uaDBO = new userActionsDBO();
		$this->emitter=new SocketIO\Emitter($this->redis);
		$func = new functionClass();
		$login_mobile = $func->isMobileLogin();
		$this->login_mobile=$login_mobile;

	}

	public function setBlockShown($blockedId){
		$this->msgDBO->setBlockedShown($this->user_id, $blockedId);
	}

	private function getMessageKey($userId){
		return $this->msgKey.$userId;
	}
	
	private function checkAndCreateMissTM(){
		$val1=$this->uaDBO->setMutualLike($this->user_id, $this->miss_tm_id);
		$val2=$this->msgDBO->setEntryInMessageQueueOnMutualLike($this->user_id, $this->miss_tm_id);
		return $val2;
	}

	private function getMissTmFirstMessage()
	{
		$data = $this->uuDBO->getUserBasicInfo(array($this->user_id),true);
		if($data['stay_country'] == 254)
		{
			if($data['gender'] == 'F')
				return MessageConstants::miss_tm_first_message_usa_female ;
			else
				return MessageConstants::miss_tm_first_message_usa_male ;
		}
		else
		{
			if($data['gender'] == 'F')
				return MessageConstants::miss_tm_first_message_female;
			else
				return MessageConstants::miss_tm_first_message_male ;
		}
	}
	
	public function getChats($status, $device =null, $isMessageMatchMergedList = false, $tstamp = null, $clearChatDataRequest=false){
		global $imageurl,  $dummy_male_image, $dummy_female_image, $admin_id ,$dummy_female_no_pics;
		
		
		//$isMessageMatchMergedList = true;
		$arr = array("responseCode"=>200,"user_id" => $this->user_id);
		$new_arr = array("responseCode"=>200,"user_id" => $this->user_id);

		$allInteractionIds = null;
		$tileDate = array();
		$uu = new UserUtils();

		$msgQueues = array();

		//check and create miss tm
		if($this->add_miss_tm==true){
			
			$user_flag = new userFlags($this->user_id);
			$user_flags_data=$user_flag->getChatConfigurations();
			$shall_run_miss_tm=(array_key_exists('show_miss_tm',$user_flags_data)&&$user_flags_data['show_miss_tm']==true)?true:false;
				/*if($_REQUEST['debug']){	var_dump($user_flags_data);
					var_dump($shall_run_miss_tm);
				}*/
					if ($shall_run_miss_tm) {
				$miss_tm_created = $this->checkAndCreateMissTM ();
				// var_dump($miss_tm_created);
				if ($miss_tm_created) {
					$MessageFullConversation = new MessageFullConversation ( $this->miss_tm_id, $this->user_id );

					$miss_tm_msg = $this->getMissTmFirstMessage();

					$MessageFullConversation->storeMostRecentChat ( $miss_tm_msg, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), null, null );

					//setting name of the user as well
					$UserData=new UserData($this->user_id);
					$user_name=$UserData->fetchName();
					$this->redis->hset("misstm:client".$this->user_id,'name',$user_name);
					// var_dump("value of miss tm created:".$miss_tm_created);
					// call the emitter
				}
			}
		}
		
		$counter=new Counters();
		if($isMessageMatchMergedList){
			$counter->clearMutualLikeCounter($this->user_id);
		}
		
		
		/**
		 * separating the query for admin message list so that we can tweak it later as well as per our need
		 */
		if($this->user_id == $admin_id)
		{
			$msgQueues = $this->msgDBO->getAllMessageQueuesForAdmin($this->user_id);
		}
		else
		{
			$msgQueues = $this->msgDBO->getAllMessageQueues($this->user_id, $tstamp);
		}

		//var_dump($msgQueues);
		
		foreach ($msgQueues as $val){
			$allInteractionIds[] = ($val['sender_id'] == $this->user_id)?$val['receiver_id']:$val['sender_id'];
		}

		//var_dump($allInteractionIds); die;
		$basicInfo = $this->uuDBO->getTileDate($allInteractionIds);
		if($device == 'html')
		$sub_array = SparkDBO::getUserSubscriptionType($allInteractionIds);
		/*var_dump($imageurl);
		 echo '<pre>';
		 var_dump($basicInfo); die;*/
		if(isset($basicInfo)){
			foreach ($basicInfo as $val){
				$tileDate[$val['user_id']] = $val;
			}
		}

		$alreadyUsedIds = array();
		$blockingPair = null;
		$all_match_ids = array();
		//	var_dump($msgQueues); exit;
		foreach ($msgQueues as $val){

			//if admin is opening the message list then show only first 20
			if($this->user_id == $admin_id){
				if(count($arr['data']) > 60 ) break;
			}
			
			$key = ($val['sender_id'] == $this->user_id)?$val['receiver_id']:$val['sender_id'];
			$non_key = ($val['sender_id'] == $this->user_id)?$val['sender_id']:$val['receiver_id'] ;
			if(!isset($tileDate[$key]['user_id'])) continue;

			if(strtolower($val['msg_type']) == 'mutual_like' &&$val['sender_id']==$this->user_id){
			continue;
			}
			if($val['msg_type']=='NUDGE_SINGLE'||$val['msg_type']=='NUDGE_DOUBLE'){
				if($val['sender_id']==$this->user_id){
					if((($this->source=="iOSApp"&&$this->apk_version>=139)||($this->source=="androidApp"&&$this->apk_version>=107)||($this->source=="windows_app"))&&date('Y-m-d H:i:s')>'2015-11-02 05:22:16'){
					}
					else
					continue;
				}
			}
			
			if(in_array($key, $alreadyUsedIds)) continue;
			else if(!isset($val['is_blocked_shown']) || $val['is_blocked_shown'] == false) 
				$alreadyUsedIds[] = $key;

			//for clubbing messages for admin irrespective of the blocked flag
			if(isset($_SESSION['admin_id'])){
				$alreadyUsedIds[] = $key;
			}
			
			//if(!in_array($key, $alreadyUsedIds)) $alreadyUsedIds[] = $key;
			//else continue;

			//	if(!isset($tileDate[$key]['thumbnail'])) continue;
			//if($val['is_blocked_shown'] == 1) continue;
			if(!isset($_SESSION['admin_id'])){
				
				if( $tileDate[$key]['status'] == "blocked" && $tileDate[$key]['deletion_status'] == null ) {
					//	$flag = $this->redis->SREM($this->getMessageKey($non_key) , $key);
					continue;
				}
			}
	
			
			
			
			if(strtolower($val['msg_type']) == 'sticker'){
				if($val['sender_id'] == $this->user_id)$val['msg_content'] = "You've sent a sticker!";
				else $val['msg_content'] = "You've received a sticker!";
			}
			
			
			//check for admin - in case admin has already read ... dont show that msg  
			if($this->user_id == $admin_id && $val['sender_id'] == $admin_id ) continue;
			
			if($this->user_id == $admin_id && $val['last_seen']>=$val['tStamp']) continue; 
			
			/**
			 * if it's a dummy entry from mutual like then 
			 * old version - remove it
			 * new version - show it with a static content
			 */
			
			
			if ($val['msg_content'] == null && $val['blocked_by'] == null && $isMessageMatchMergedList == true && strtolower($val['msg_type']) == 'mutual_like')
			{
				//echo "came here";
					$val ['msg_content'] = systemMsgs::$conversations ['mutual_like_no_message'];
			}
			elseif ($val ['msg_content'] == null && $val ['blocked_by'] == null && strtolower ($val ['msg_type']) == 'mutual_like' && $isMessageMatchMergedList == false )
			{
				continue;
			}else if(strtolower ($val ['msg_type']) == 'spark'){
				continue;		//not to show spark message in conversation list.
			}

			if(!$this->login_mobile)
			{
				$val['msg_content']=strip_tags($val['msg_content']);
			}
			
			$arr['data'][$key] = array( "message_list" => array("fname" => $tileDate[$key]['fname'],
											"age" => $tileDate[$key]['age'],
											"profile_pic" => (isset($tileDate[$key]['thumbnail']))?$imageurl.$tileDate[$key]['thumbnail']:(($tileDate[$key]['gender']=="M")? $dummy_male_image:  $dummy_female_image), 
											//"message" => $val['msg_content'],
											"message" => $val['msg_content'],
											"receiver_id" => $val['receiver_id'],
											"sender_id"	=> $val['sender_id'],
											"is_miss_tm"=>($val['sender_id']==$this->miss_tm_id||$val['receiver_id']==$this->miss_tm_id)?true:false,
											"profile_link" => $uu->generateProfileLink($this->user_id, $key),
											"full_conv_link" => $uu->generateMessageLink($this->user_id, $key),
											"timestamp" => ($device == "mobile")?$val['tStamp']:Utils::GMTTOIST($val['tStamp']),
											"row_updated_tstamp" =>$val['row_updated_tstamp'],
											"seen" => ($val['last_seen']>=$val['tStamp'])?true:false,
											"is_blocked_shown" => (isset($_SESSION['admin_id']))?null:$val['is_blocked_shown'],
											"blocked_by" => (isset($_SESSION['admin_id']))?null:$val['blocked_by'],
											"msg_type"=>$val['msg_type'],
											//"is_spark"=> sparkDBO::areUsersSparked($val['receiver_id'], $val['sender_id'], 'accepted')
										//	"is_msg_content_set" => ($val['msg_content'] == null && $val['msg_type'] == 'text')?false:true
			//"seen_date" => ($val['last_seen']>=$val['tStamp'])?Utils::GMTTOIST($val['last_seen']):null
			));
			
		}
		$sparked_array = SparkDBO::areUsersSetsSparked($this->user_id,$allInteractionIds,'accepted');
		$selectObj = new Select($this->user_id);
		$selectUsers = $selectObj->getSelectMembers($allInteractionIds);
	//
		/*	echo '<pre>';
		 var_dump($arr);
		 die;*/
		//$deletedIds = $this->redis->SMEMBERS($this->delKey);
		
		//var_dump($arr['data']);exit;
		if(isset($arr['data'])){
			foreach ($arr['data'] as $key => $val){
			//	use \ForceUTF8\Encoding;
				$val['message_list']['message'] = Encoding::toUTF8($val['message_list']['message']);
			/*	if ($val['message_list']['message'] == null && $val['message_list']['blocked_by'] != null && $val['message_list']['is_msg_content_set'] == false)
				{
					$val['message_list']['message'] = "You both have liked each other";
				}*/
				//if(isset($deletedIds) && (in_array($val['message_list']['receiver_id'], $deletedIds)||in_array($val['message_list']['sender_id'], $deletedIds))) continue;
				if( $this->user_id != $admin_id && ($val["message_list"]['receiver_id'] == $admin_id || $val["message_list"]['sender_id'] == $admin_id)) continue;
				if (($val["message_list"]["is_blocked_shown"] == "1") || ((isset($val['message_list']['blocked_by']) && $val["message_list"]["blocked_by"] == $this->user_id ))){
					unset ($arr['data'][$key]);
					continue;
				}
				if($sparked_array[$key] == true)
					$val['message_list']['is_spark'] = true;
				else
					$val['message_list']['is_spark'] = false;

				$val['message_list']['is_select_match'] = false;
				if(isset($selectUsers[$key])){
					$val['message_list']['is_select_match'] = true;
				}
				if($device == 'html')
				{
					$val['message_list']['select_status']= $sub_array[$key]['select_status'];
					$val['message_list']['spark_count']= $sub_array[$key]['spark_count'];
				}
				$new_arr['data'][] = $val;
			}
		}
		//Double check, asked by ankit. If previous call to get clear chat fails 
		// after fresh install, another param will be passed 
		// while fetching the conversation list to handle the same.
		if($isMessageMatchMergedList && $clearChatDataRequest){
			$clearChatData = $this->getClearChatData();
			$output = array();
			if($clearChatData != NULL){
				$new_arr['cleared_data'] = $clearChatData;
			}else{
				$new_arr['cleared_data'] = array();
			}		
		}
		return $new_arr;
	}

	public function logAction($user_id,$event,$diff) {
		$Trk = new EventTrackingClass();
		$data_logging = array();
		$data_logging [] = array( "user_id"=>$user_id, "activity" => "messages" , "event_type" => $event, "event_status" => "success", "time_taken"=>$diff );
		$Trk->logActions($data_logging);
	}
	
	public function getClearChatData(){
		return $this->msgDBO->getClearChatHistoryForUser($this->user_id);
	}
	
}

try{
	
	//echo 'memory used before including the functionc class : '. memory_get_usage();
	$func = new functionClass();
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$login_mobile = $func->isMobileLogin();
	$device_type = Utils::getDeviceType();
	$appHeaderFields = Utils::getAllSystemHeaderFields();
//	var_dump($appHeaderFields);
	$isMessageMatchMergedList = $appHeaderFields['isMessageMatchMergedList'];
	
	$authentication = functionClass::redirect ( 'messages' , $login_mobile);
	
	//echo $user_id;
	if(isset($user_id)){
		

		if(isset($_REQUEST['block_shown']) && $_REQUEST['block_shown'] == "block_shown"){
			try{
				//echo 'memory used before instantiating message class : '. memory_get_usage();
				$msg = new Messages ( $user_id );
				//echo 'memory used after instantiating message class : '. memory_get_usage();
				$msg->setBlockShown($_REQUEST['block_shown_id']);
				
				$output = array('responseCode' =>200, 'response' =>"success");
			}
			
			catch (Exception $e){
				$output = array('responseCode' =>401, 'response' =>"failure");
			}
			
			$currTime1 = time();
			$diff = $currTime1-$currTime;
			$msg->logAction($user_id,"message_list",$diff);
			echo json_encode($output);
			exit();
		}

		if($authentication['error'] != null){
			$arr['responseCode'] = 403;
		}
		else{
			$clearChatDataRequest = $_REQUEST['get_clear_chat_data'];
			if(isset($clearChatDataRequest) && $clearChatDataRequest != null && $clearChatDataRequest == "1"){
				$msg = new Messages($user_id);
				$clearChatData = $msg->getClearChatData();
				$output = array();
				if($clearChatData != NULL){
					$output['responseCode'] = '200';
					$output['cleared_data'] = $clearChatData;
				}else{
					$clearChatData = array();
					$output['responseCode'] = '200';
					$output['cleared_data'] = array();
				}
				echo json_encode($output);
				exit();
			}else{
				$clearChatDataRequest = isset($_REQUEST['get_clear_chat_data_conv_list']) ? $_REQUEST['get_clear_chat_data_conv_list'] : false;
				$msg = new Messages ( $user_id );
				$msg->setSource($appHeaderFields['source']);
				$msg->setAPKVersion($appHeaderFields['app_version_code']);
				$msg->setMissTMCreation($_REQUEST['create_miss_tm']);
				$arr = $msg->getChats($status, $device_type, $isMessageMatchMergedList, $_REQUEST['tstamp'], $clearChatDataRequest);
				/*echo '<pre>';
				 var_dump($arr);*/
			}
		}
		
		$currTime1 = time();
		$diff = $currTime1-$currTime;
		$msg->logAction($user_id,"message_list",$diff);
		
		if ($device_type == "html") {
			$header = new header($user_id);
			$header_values = $header->getHeaderValues();
			//var_dump($header_values);
			$smarty->assign("header", $header_values);
			$smarty->assign("my_id", $user_id);
			$smarty->assign("arr", $arr);
			$smarty->display ( "../templates/match/messages.tpl" );
		}
		else{
			echo json_encode($arr);
		}
	}
	else{
		header("Location:../matches.php"); die;
	}
}
catch (Exception $e){
	trigger_error("PHP Web:" . $e->getMessage(), E_USER_WARNING);
}
?>








