<?php
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../utilities/wordlist-regex.php";
//require_once dirname ( __FILE__ ) . "/MessageUtils.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
//require_once dirname ( __FILE__ ) . "/../UserActions.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../DBO/messagesDBO.php";
require_once dirname ( __FILE__ ) . "/../spark/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../include/header.php";
require_once dirname ( __FILE__ )."/../mobile_utilities/pushNotification.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../logging/emailerLogging.php";
require_once dirname ( __FILE__ ) . "/../include/systemMessages.php";
require_once dirname(__FILE__)."/socket.io-php-emitter/Emitter.php";;
require_once dirname ( __FILE__ ) . "/../utilities/counter.php";
require_once dirname ( __FILE__ ) . "/block_notification.php";
require_once dirname(__FILe__)."/MessageFullConversation.class.php";








try{
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$systemMsg = null;
	$func = new functionClass();

	$login_mobile = $func->isMobileLogin();
	$authentication = functionClass::redirect ( 'messages_full_conv' , $login_mobile);

	$user_communicated_with=intval($_REQUEST['match_id']);

	$isAdminSet = false;
	//var_dump($admin_id); die;
	if($user_communicated_with == $admin_id) {
		$isAdminSet = true;
	}
	$device_type = Utils::getDeviceType();
	$app_code = null;
	$header_values=Utils::getAllSystemHeaderFields();
	if($device_type == "mobile") $app_code = Utils::getAppVersionFromHeader();
	//if user id is valid and redirect
	if(isset($user_id)){

		$ud = new UserData($user_id);
		$status = $ud->fetchStatus();
		$authenticity = $ud->fetchAuthenticity();
		$isAuthentic = $authenticity['TM_authentic'];

		$chksum = $_REQUEST['mesh'];

		$msg = new MessageFullConversation($user_id,$user_communicated_with);
		$checkSumStr = $user_id .'|' . $user_communicated_with;
		$postCheckSum = Utils::calculateCheckSum($checkSumStr);
		$block_flag = $msg->isblocked();
		//echo $postCheckSum;die;

		if($postCheckSum == $chksum){

			$case = $_POST['type'];

			if(isset($case)==false){
				$case="recent_messages";
			}
			$names = array();
			switch($case){

				case "block_user":
					try{
						$msg->block();
						$block_output['response_code'] = 200;
					}
					catch (Exception $e){
						$block_output['response_code'] = 403;
						trigger_error($e->getTraceAsString(), E_USER_WARNING);
					}
					echo json_encode($block_output);
					exit();
					break;

				/**
				 * for newer version - fusing both block and report abuse into one
				 */	
				case "block_with_reason":	
				case "report_abuse":
					$label_id = $_POST['label_id'];
					$reason = $_POST['reason'];
					$description = (isset($_POST['description']) &&strlen($_POST['description']>0))?$_POST['description']:null;
					try{
						//to break the string for older version of app in case of others
						if(substr(strtolower($reason),0,6) == "Other,"){
							$reason = "Other";
							$description = substr($reason,7);
						}
						$msg->report_abuse($label_id, $reason, $description);
						$abuse_output['response_code'] = 200;
					}
					catch(Exception $e){
						$abuse_output['response_code'] = 403;
						trigger_error($e->getTraceAsString(), E_USER_WARNING);
					}

					echo json_encode($abuse_output);

					exit();
					break;

				case "send_msg":
					$data = $_POST['msg'];
					$msg_type = $_REQUEST['message_type'];
					$quiz_id=$_REQUEST['quiz_id'];
					$unique_id=$_REQUEST['unique_id'];
					$update_spark = $_REQUEST['update_spark'];
					$hash = $_REQUEST['hash'];

					if(isset($update_spark) && isset($hash)){
						$spark = new Spark($user_id); // $user_communicated_with, $user_id
						$spark->updateSpark($user_communicated_with, $user_id, $hash, 'accepted');
					}
				/*  $app_deal_id=$_REQUEST['deal_id'];
					$deal_status=$_REQUEST['deal_status'];
					$datespot_id=$_REQUEST['date_spot_id'];
				*/
					$metadata=$_REQUEST['metadata'];
					
					if(isset($metadata) && $metadata != null){
						$msg_type="JSON";
						$metadata = json_decode($metadata, true);
					}

					if($msg_type==null)
						$msg_type="TEXT";
					//echo $msg_type;die;
					try{
						if($block_flag == false){
							if($msg_type=='STICKER'){
								$data = str_replace('.webp', '.png', $data);
							}
							$output=$msg->saveMessage($data,$badwords, $msg_type, $device_type,$unique_id,$quiz_id,$metadata,$update_spark);
							// to add response code to the response
							if($login_mobile == true){
								$output = json_decode($output, true);
								$output['responseCode'] = 200;
								if(isset($update_spark) && isset($hash)){
									$output['update_spark'] = true;
								}
								$output = json_encode($output);
							}
						}
						else{
							$output["data"]["match_id"]=$user_communicated_with;
							$output["data"]["is_blocked"]= $block_flag;
							$output['responseCode'] = 200;
							$output = json_encode($output);;
						}
					}
					catch (Exception $e){
						$output['responseCode'] = 403;
						trigger_error("PHP WEB: ". $e->getTraceAsString(), E_USER_WARNING);
					}
					echo $output;
					exit();
					break;

				case "recent_messages":
					$msg_id = ($_REQUEST['msg_id'] >0 ? $_REQUEST['msg_id'] : -1);
					$chat_array = $msg->getCompleteChat($device_type,$msg_id, $app_code,$header_values['source'],$header_values);
					$last_msg_id= $msg->last_msg_id;
					break;
					
				case "clear_chat":		
					$output = array();
					$lastMessageId = (isset($_REQUEST['last_msg_id']) && $_REQUEST['last_msg_id'] >0 ? $_REQUEST['last_msg_id'] : -2);
					try{
						$output['last_msg_id'] = $msg->clearChat($user_id, $user_communicated_with, $lastMessageId);
						$output['responseCode'] = 200;
					}
					catch (Exception $e){
						$output['responseCode'] = 403;
						trigger_error($e->getTraceAsString(), E_USER_WARNING);
					}
					print_r(json_encode($output));
					exit();
					break;

				case "doodle":
					$output = array();
					try{
						
						if($ud->CheckNoPicForFemale())
						{
							//This is for the females who doesn't have a profile pic , and if they call out for doodle, then it will be held specially
							$output['doodle_url'] = Utils::getDoodleForUsers(null,null,true);
							$output['expiry']=3600;
						}
						else 
						{
							$output['doodle_url'] = Utils::getDoodleForUsers();
							$output['expiry']=86400;
						}
						$output['responseCode'] = 200;
					}
					catch (Exception $e){
						$output['responseCode'] = 403;
						trigger_error($e->getTraceAsString(), E_USER_WARNING);
					}
					print_r(json_encode($output));
					exit();
					break;
					
				case "update_seenTs":
					if($user_id == $admin_id)
					{
						$msg->updateLastSeenForAdmin();
					}
			}

			$chat_array["data"]["is_blocked"] = $block_flag;
			$chat_array["block_reasons"] = systemMsgs::$blockReasons["reasons"];
			$chat_array["block_editable_flags"] = systemMsgs::$blockReasons["edit_flags"];
			
			if($device_type=="html"){
				$subs_data = SparkDBO::getUserSubscriptionType(array($user_communicated_with));
				$iAmAdmin = false;
				if($admin_id == $user_id) $iAmAdmin = true;
				$header = new header($user_id);
				$header_values = $header->getHeaderValues();
				$smarty->assign("header", $header_values);
				$smarty->assign('output', $chat_array['data']);
				$smarty->assign('subs', $subs_data[$user_communicated_with]);
				$smarty->assign("iAmAdmin", $iAmAdmin);
				$smarty->assign("block_reasons",$chat_array["block_reasons"] );
				$smarty->assign("my_id", $user_id);
				$smarty->assign("match_id", $user_communicated_with);
				$smarty->assign("isAdmin", $isAdminSet);
				$smarty->assign("is_blocked", $block_flag);
				$smarty->assign("last_msg_id", $last_msg_id);
				$smarty->display ( dirname ( __FILE__ ) ."/../templates/match/messagethread.tpl" );
			}

			else{
				echo json_encode($chat_array);
			}
		}
		else{
			header("Location:messages.php"); die;
		}
	}	else{
		header("Location:messages.php"); die;


	}
}




/*
 if($device_type == 'mobile'){
 $authentication = functionClass::redirect ( 'messages_full_conv' , true);
 if($authentication['error'] != null)
 return json_encode($authentication);
 }

 else{
 functionClass::redirect ( 'messages_full_conv');
 }
 */

catch ( Exception $e ) {
	//	echo $e->getMessage();
	trigger_error ( "PHP WEB:" . $e->getTraceAsString(), E_USER_WARNING );

}

?>








