<?php
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../utilities/wordlist-regex.php";
//require_once dirname ( __FILE__ ) . "/MessageUtils.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
//require_once dirname ( __FILE__ ) . "/../UserActions.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../DBO/messagesDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/curatedDatesDBO.php";
require_once dirname ( __FILE__ ) . "/../include/header.php";
require_once dirname ( __FILE__ )."/../mobile_utilities/pushNotification.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../logging/emailerLogging.php";
require_once dirname ( __FILE__ ) . "/../include/systemMessages.php";
require_once dirname(__FILE__)."/socket.io-php-emitter/Emitter.php";;
require_once dirname ( __FILE__ ) . "/../utilities/counter.php";
require_once dirname ( __FILE__ ) . "/block_notification.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/Datespot.class.php";





/*this file depicts how to store the chat data into the system
 * retrieve the latest chat content from redis
 * and throwing the same on UI
 * @Himanshu
 */

class MessageFullConversation {

	private $user_id;
	private $user_communicated_with;
	private $msgDBO;
	private $msgKey;
	private $redis;
	private $emitter;
	private $mutualLikeKey;
	private $counters;
	private $before_mq ;
	private $before_full_chat ;
	private $after_full_chat ;
	private $before_return  ;
	private $miss_tm_id;
	public $last_msg_id;
	private $login_mobile;
	private $is_web_poll;

	function __construct($user_id,$user_communicated_with){
		global $redis,$miss_tm_id;
		$this->user_id = $user_id;
		$this->user_communicated_with=$user_communicated_with;
		$this->msgDBO = new MessagesDBO();
		$this->msgKey = Utils::$redis_keys['messages'];
		$this->mutualLikeKey = Utils::$redis_keys['mutual_like_only_list'];
		$this->redis = $redis;
		//emmiter for backword compatibility and web chat
		$this->emitter=new SocketIO\Emitter($this->redis);
		$this->counters=new Counters();
		$this->before_mq = null;
		$this->before_full_chat = null;
		$this->after_full_chat  = null;
		$this->before_return = null;
		$this->miss_tm_id=$miss_tm_id;
		$this->last_msg_id = 0;
		$func = new functionClass();
		$login_mobile = $func->isMobileLogin();
		$is_web_poll=$func->isWebPoll();
		$this->login_mobile=$login_mobile;
		$this->is_web_poll=$is_web_poll;
	}


	private function getMessageKey($userId){
		return $this->msgKey.$userId;
	}

	private function getMutualLikeKey($userId)
	{
		return $this->mutualLikeKey.$userId;
	}
	//if someone sends a msg it'll be added to receiver and sender's set of most recent comm
	public function storeMostRecentChat($data, $msg_type,$tstamp,$unique_id,$quiz_id){
		global $admin_id;
	
		$adminflag = ($admin_id == $this->user_communicated_with || $admin_id == $this->user_id )?true:false;
		$msgId = $this->msgDBO->storeMessage($this->user_id, $this->user_communicated_with, $data, $msg_type,$tstamp,$unique_id,$quiz_id);
		//send emiiter to nodejs..
		
		$sender_id_to_send=$adminflag?"admin":$this->user_id;
		$metadata="";
		
		if($msg_type == 'JSON'){   
			$data_array = json_decode($data);
			$metadata = json_encode($data_array->metadata);
			$data = $data_array->msg;
			$msg_type = $data_array->message_type;
		}
		
		//emitting to redis so that it can be used by socket
		if($msg_type != "SPARK") {
			$this->emitter->to($this->user_communicated_with)->emit('chat_received',
				array("msg_id" => $msgId, 'time' => $tstamp,
					'sender_id' => $sender_id_to_send, "receiver_id" => $this->user_communicated_with,
					"msg" => $data, "message_type" => $msg_type, "unique_id" => $unique_id, "quiz_id" => $quiz_id, "metadata" => $metadata
				));
		}
		if($adminflag == false){
			$this->counters->markUnread($this->user_communicated_with,$this->user_id);
			//hack right now for nudge list
			if($msg_type=='NUDGE_SINGLE'||$msg_type=='NUDGE_DOUBLE'){
				$this->counters->clearUnreadCount($this->user_id,$this->user_communicated_with);
			}
			
		//$this->redis->SADD($this->getMessageKey($this->user_communicated_with) , $this->user_id);
		}
		return $msgId;
	}



	/**
	 * will fetch the complete msg listing and if login_mob is true then on the basis of msg_id
	 * @param $login_mobile
	 * @param $msg_id
	 */
	public function getCompleteChat($device,$msg_id = null, $app_code = null,$source,$header){
		global $admin_id;
		$uu = new UserUtils();
		$arr = null;
		
		$attributes = $this->getMsgAttributes();
		$this->before_mq = microtime(true);
		
		$lastSeenData = $this->msgDBO->getMessageQueue($this->user_id, $this->user_communicated_with);
		$last_seen_mid = $lastSeenData['last_seen_msg_id'];
		$last_seen_time = $lastSeenData['last_seen'];
		$this->before_full_chat = microtime(true);
		$chat = $this->msgDBO->getChat($this->user_id, $this->user_communicated_with, $msg_id);
        $this->after_full_chat = microtime(true);
		$j=0;
		$updatedLastSeenMsgId = null;
		//var_dump($header);
		foreach ($chat as $row){
			//if($device == 'mobile') $row['msg_content'] = str_replace("<br />", "", $row['msg_content']);
			if(($row['msg_id'] > $last_seen_mid)&&($row['sender_id']== $this->user_id))
			$arr['data']['message_list'][$j]['seen'] = 0;
			else $arr['data']['message_list'][$j]['seen'] = 1;

			if($row['msg_id'] > $this->last_msg_id )
				$this->last_msg_id = $row['msg_id'];

			if($arr['data']['message_list'][$j]['seen'] == 1) $updatedLastSeenMsgId = $row['msg_id'];
			$arr['data']['message_list'][$j]['msg_id'] = $row['msg_id'];
			
			//check for quiz
			if($this->is_web_poll)
			{
				$row['msg_content']=strip_tags($row['msg_content']);
			}
			//echo $row['msg_content'];die;
			$content = $row['msg_content'];
			if($row['msg_type']=='NUDGE_SINGLE'||$row['msg_type']=='NUDGE_DOUBLE'||$row['msg_type']=='QUIZ_MESSAGE'){
				$message_array=json_decode($row['msg_content'],true);
				$content=$message_array['msg'];
				$row['quiz_id']=$message_array['quiz_id'];
			}else if($row['msg_type']=='JSON'){
				$message_array=json_decode($row['msg_content'],true);
				$content=$message_array['msg'];
				$row['msg']=$message_array['msg'];
				$row['msg_type']=$message_array['metadata']['message_type'];
				$row['metadata']=$message_array['metadata'];
			}
			
			if($device=="mobile"){
				if($header['app_version_code']=="65"&&$header['app_version_name']=='1.0.1065'&&isset($header['source'])==false){
					if($row['msg_type'] != 'TEXT'&&$row['msg_type'] != 'STICKER'){
						$row['msg_type']="TEXT";
					}
				}
				
				if($header['source']=='iOSApp'&&$header['app_version_code']<='121'){
					
				if($row['msg_type'] != 'TEXT'&&$row['msg_type'] != 'STICKER'){
					
						$row['msg_type']="TEXT";
					}
				}
				
				if($app_code<Utils::$appVersionForStickers){
				 if($row['msg_type'] == 'STICKER'){
				 	$content="You've received a sticker. Update the app to view it.";
				 	$arr['data']['message_list'][$j]['sticker_url'] = $row['msg_content'];
				 }
				}
			} 
			$arr['data']['message_list'][$j]['msg'] = ($device=="mobile")?$content:(($row['msg_type'] == 'TEXT')?$content:('<img style="max-width:150px;max-height:150px;" src="'.$content.'">'));
			$arr['data']['message_list'][$j]['unique_id'] = $row['unique_id']?$row['unique_id']:$row['msg_id'];
			$arr['data']['message_list'][$j]['quiz_id'] =   $row['quiz_id'];
			$arr['data']['message_list'][$j]['metadata'] =   $row['metadata'];
				
			$arr['data']['message_list'][$j]['message_type'] =   $row['msg_type'];
			$arr['data']['message_list'][$j]['time'] =  ($device=='mobile')?$row['tStamp']:Utils::GMTTOIST($row['tStamp']);
			$arr['data']['message_list'][$j]['sender_id'] =  $row['sender_id'];
			$arr['data']['message_list'][$j]['receiver_id'] =  $row['receiver_id'];
			$arr['data']['message_list'][$j]['sender'] = ($row['sender_id']==$this->user_id)?"Me":"Match";
			$j++;
		}


		$arr['data']['receiver']= array("fname" => $attributes['name'],
										"profile_pic"=>$attributes['pic'],
									    "profile_id" => $this->user_communicated_with,
										"last_seen_receiver_tstamp" => $last_seen_time,
										"last_seen_msg_id" => $last_seen_mid,
									"is_miss_tm"=>($this->user_id==$this->miss_tm_id||$this->user_communicated_with==$this->miss_tm_id)?true:false,
										"profile_link" => $uu->generateProfileLink($this->user_id, $this->user_communicated_with),
		'is_admin_set' => ($this->user_communicated_with == $admin_id) ?1:0
		);


		$arr["responseCode"]=200;
		$arr['data']['user_id']=$this->user_id;
		$counter=new Counters();
		$flag=$counter->clearUnreadCount($this->user_id,$this->user_communicated_with);
		/*echo'<pre>';
		 var_dump($arr); die;*/
		/*if($admin_id == $this->user_id || $admin_id == $this->user_communicated_with) $flag = 1;
		 else
		 */
	/*	$flag = $this->redis->SREM($this->getMessageKey($this->user_id) , $this->user_communicated_with);
		 
		if(!isset($arr['data']['message_list']))
		{
			$redisRes = $this->redis->SREM($this->getMutualLikeKey($this->user_id) , $this->user_communicated_with);
		}*/
		//var_dump($flag);
		if($flag == 1){
		$tstamp=date('Y-m-d H:i:s');		
			$this->msgDBO->updateMessageQueueLastSeenMsgId($this->user_communicated_with, $this->user_id, $updatedLastSeenMsgId,$tstamp);
			//set the emitter here as well
			
			//trigger_error("emiiting chat_read_listener call for ");
			//emit read to the socket if present.for backword compatibility and web chat
			$this->emitter->to($this->user_communicated_with)->emit('chat_read_listener',
					array("tstamp"=> date('Y-m-d H:i:s'),
							'sender_id'=>$this->user_communicated_with,"receiver_id"=>$this->user_id,
							"last_seen_msg_id"=>$updatedLastSeenMsgId
					));
		}
        $this->before_return = microtime(true) ;
		return $arr;
	}

	public function block(){
		$this->msgDBO->block($this->user_id, $this->user_communicated_with);
		
		//clear counter in case a user sends message and blocks the other one 
		//$this->redis->SREM($this->getMessageKey($this->user_communicated_with) , $this->user_id);
	}
	
	public function getExectionTime()
	{
		return array ("before_mq" => $this->before_mq ,
				      "before_full_chat" => $this->before_full_chat ,
				      "after_full_chat" => $this->after_full_chat,
				       "before_return" => $this->before_return
		);
	}

	public function getMsgAttributes(){
		global $imageurl, $dummy_male_image,$dummy_female_image ;
		$attributes = array();
		$uudbo = new userUtilsDBO();
		$basicInfo = $uudbo->getUserBasicInfo(array($this->user_communicated_with), true);
		$pic = $uudbo->getProfilePic(array($this->user_communicated_with), true);

		if(!isset( $pic['thumbnail'])){
			$image = ($basicInfo['gender'] == "M")? $dummy_male_image: $dummy_female_image;
		}
		else{
			$image = $imageurl. $pic['thumbnail'];
		}

		//		var_dump($image); die;
		$attributes = array('name' => $basicInfo['fname'],
							'pic' => $image);

		return $attributes;
	}

	/**
	 * @param unknown_type $label for label id on front end
	 * @param unknown_type $text for reason selected by user
	 */
	public function report_abuse( $label, $text , $description=null){
		$this->msgDBO->reportAbuse($this->user_id, $this->user_communicated_with, $label, $text, $description);
		$this->msgDBO->block($this->user_id, $this->user_communicated_with);
		if($text=='Inappropriate or offensive content'||$text=='Inappropriate or offensive content.'||$text=='Misleading profile information'||$text=='Misleading profile information.')
		{
		$block_notification=new Block_Notification($this->user_communicated_with, $text);
		$block_notification->notifyUser();
		}
		$this->counters->clearUnreadCount($this->user_communicated_with,$this->user_id);
		$this->counters->clearUnreadCount($this->user_id,$this->user_communicated_with);
		//$this->_remove_user_from_redisSet($this->user_communicated_with, $this->user_id);
	}

	/**
	 * remove user1 from user2 message queue (for unread count)
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	private function _remove_user_from_redisSet ($user1, $user2 )
	{
		$redis_key = $this->getMessageKey($user1);
		$this->redis->SREM($redis_key, $user2);
	}
	
	public function isblocked(){
		$blocking_arr = $this->msgDBO->getBlockingStatus($this->user_id, $this->user_communicated_with);
		//var_dump($blocking_arr);
		$isblocked = false;
		/*foreach ($blocking_arr as $val){
			if($val['is_blocked'] == 1){
			$isblocked = true;
			}
			}*/

		if(isset($blocking_arr['blocked_by'])) $isblocked = true;
		//var_dump($isblocked);
		return $isblocked;
	}

	public function storeBadMessage ($badMsg){
		$this->msgDBO->saveBadMessage($this->user_id, $this->user_communicated_with, $badMsg);
	}

	private function checkForBadWords($data){
	global $badwords;	
	$data = htmlspecialchars($data);
		$arr = explode(' ', $data);
		$arr = array_map('strtolower', $arr);
		return array_intersect($arr, $badwords);
	}

	public function saveMessage($data, $badwords, $msg_type = 'TEXT', $device,$unique_id,$quiz_id,$metadata,$update_spark){
		global $imageurl, $admin_id,$dummy_male_image,$dummy_female_image;
		$data = trim($data);
		//echo $data;
		$output=array();
		//main timestamp which will be used across this chat messgae
		$tstamp=date('Y-m-d H:i:s');		
		$isBad = null;
		if(!empty($data)){
			$isBad=$this->checkForBadWords($data);
			
			if($isBad!=null){
				$this->storeBadMessage( $data);
				$output['error']=true;
				$output['error_msg']="Inappropriate content will lead to immediate removal of profile and notification to all your mutual matches of the indecent behaviour. Additionally, the same may also amount to breach of Indian laws and may be punishable thereunder.";//"Can't send inappropriate words. Reported profiles are removed instantly.";//\"$data\" contains foul words.Cannot post this.";
			}
			else{
				if($msg_type == 'TEXT'){
					$data = htmlspecialchars($data);
					$data = nl2br($data);
					$output['msg']=$data;
				}
				else if($msg_type=='JSON'){
					$message_array=array("metadata"=>$metadata,"msg"=>$data, "message_type"=>$metadata['message_type']);
					$data=json_encode($message_array);
				//	$msg_type = $message_array['message_type'];
				}
				
				
				
				$output['time'] = 	date ( 'Y-m-d H:i:s',  time()   );
				$output['sender_id'] = $this->user_id;
				$output['receiver_id'] = $this->user_communicated_with;
				$output['unique_id'] =$unique_id;
				$output['quiz_id'] =$quiz_id;
				if(isset($metadata)){
					$data = json_decode($data);
					$output['msg'] = $data->msg;
					$output['message_type'] = $data->message_type;
					$output['metadata'] = $metadata;
					$data = json_encode($data);
				}else{
					$output['msg']=$data;
					$output['message_type'] = $msg_type;
				}


				
				if($msg_type=="STICKER"){
					$output['message_type'] = $msg_type;
					$content = $data;
					$output['msg'] = ($device=="mobile")?$content:('<img width="200px" src="'.$content.'">');
				}
				
				
				
				if($metadata['message_type'] == "CD_ASK"){
					$datespot_id = $metadata['date_spot_id'];
					$app_deal_id = $this->user_id."_".$this->user_communicated_with."_". $datespot_id."_".rand(pow(10, 5-1), pow(10, 5)-1);  //last part is 5 digit random number					
					curatedDatesDBO::createNewDeal($this->user_id, $this->user_communicated_with, $datespot_id, $app_deal_id);
					$output['metadata'] = $metadata;
					$output['metadata']['deal_id']=$app_deal_id;
					$data=json_decode($data);
					$data->metadata->deal_id=$app_deal_id;
					$data=json_encode($data);
				}
				
				if($metadata['message_type'] == "CD_VOUCHER"){
					$coupon_code = "TM".substr(str_shuffle(str_repeat("ABCDEFGHIJKLMNOPQURST123456789", 5)), 0, 5);
					$app_deal_id = $metadata['deal_id'];
					$datespot_id = $metadata['date_spot_id'];
					$deal_status = $metadata['deal_status'];
					curatedDatesDBO::updateDeal($app_deal_id, $deal_status, $coupon_code, $datespot_id);
					if($deal_status == 'accepted'){
								$dateSpotObj = new Datespot() ;
								$dateSpotObj->checkAndSendSMS($app_deal_id) ;
					}
					$output['metadata'] = $metadata;
					$output['metadata']['coupon_code'] = $coupon_code;
					$data=json_decode($data);
					$data->metadata->coupon_code=$coupon_code;
					$data=json_encode($data);
				}
				
				$msgId = $this->storeMostRecentChat($data, $msg_type,$tstamp,$unique_id,$quiz_id);
				$output['msg_id'] = $msgId;
				// push notification
				$pushnotify = new pushNotification();
				$uu = new UserUtils();
				$user_data = $uu->getNamenPic($this->user_id);
				$ticker_text = "Message from ".$user_data['name'];
				if(isset($user_data['thumbnail']))
				$pic_url = $imageurl.$user_data['thumbnail'];
				else{
					if($user_data['gender']==M)
					$pic_url = $dummy_male_image;
					else
					$pic_url = $dummy_female_image;
				}
				$message_url = $uu->generateMessageLink($this->user_communicated_with,$this->user_id);
				//var_dump($match_profile_link);exit;
				$adminFlag = 0;
				if($this->user_id == $admin_id) $adminFlag = 1;
					


				$push_type = "MESSAGE";
				if($msg_type == "STICKER") {
					/**
					 * sending two types of notification in case multiple devices exist
					 */
					/*$sql = "select min(app_version_code) as app_code from user_gcm_current where user_id = ? and status = 'login' ";
					 $res = Query::SELECT($sql, array($this->user_communicated_with), Query::$__slave, tru);
					 $app_code = $res['app_code'];
					 */
					$push_arr['compatible'] =  array("is_admin_set"=>$adminFlag ,
														"content_text"=>ucfirst($user_data['name'])."  has sent you a sticker.",
														"pic_url"=>$pic_url,
														"ticker_text"=>$ticker_text,
														"title_text"=>$user_data['name'],
														"message_url"=>$message_url,
														"match_id"=>$this->user_id,
														"tstamp"=>$tstamp, 
														"msg_type" => $msg_type,
														"msg_id"=> $msgId,
														"push_type"=>"MESSAGE");
						
					$push_arr['incompatible'] =  array("is_admin_set"=>$adminFlag ,
														"content_text"=>ucfirst($user_data['name'])."  has sent you a sticker. Update the app to see it.",
														"pic_url"=>$pic_url,
														"ticker_text"=>$ticker_text,  
														"title_text"=>$user_data['name'],
														"message_url"=>$message_url,
														"match_id"=>$this->user_id,
														"msg_type" => $msg_type,
														"msg_id"=> $msgId,
							"tstamp"=>$tstamp,
														"push_type"=>"WEB",
														"web_url"=>"market://details?id=com.trulymadly.android.app");
						
					//var_dump($push_arr); die; 
				//	$pushnotify->notify($this->user_communicated_with,$push_arr ,$this->user_id,1 , array("lower"=>Utils::$appVersionForMarketLinks, "higher"=>Utils::$appVersionForStickers));
					if($app_code<Utils::$appVersionForStickers && $app_code>=Utils::$appVersionForMarketLinks){

					}
					else{
						$push_data = ucfirst($user_data['name'])."  has sent you a sticker.";
					}
				}
				else {
					/**
					 * normal push notiification
					 */
					if($msg_type == 'JSON'){
						 $dataMsg = json_decode($data, true);
						 $msg_type = $dataMsg['message_type'];
						 $data = $dataMsg['msg'];
					}
					$title_text = $user_data ['name'];
					if(isset($update_spark)){
						$title_text = "Things just got interesting";
						$data = $user_data ['name']." just accepted your Spark.";
					}
					$push_data = (strlen ( $data ) > 30) ? substr ( $data, 0, 29 ) . '...' : $data;
					$push_arr = array (
							"is_admin_set" => $adminFlag,
							"content_text" => $push_data,
							"pic_url" => $pic_url,
							"ticker_text" => $ticker_text,
							"title_text" => $title_text,
							"message_url" => $message_url,
							"match_id" => $this->user_id,
							"msg_id"=> $msgId,
							"tstamp"=>$tstamp,
							"msg_type" => $msg_type,
							"push_type" => $push_type 
					);
					if($msg_type != 'SPARK')
						$pushnotify->notify ( $this->user_communicated_with, $push_arr, $this->user_id );
				}
 

			}
			return json_encode($output);
		}
	}
	
	/**
	 * update the last seen only in case of upate
	 */
	public function updateLastSeenForAdmin(){
		$this->msgDBO->updateLastSeenWithRespectToTstamp($this->user_id, $this->user_communicated_with, 1);
	}
	
	/**
	 * clear chat by user_1 with user_2
	 */
	public function clearChat($user1, $user2, $lastMessageid){
		if($lastMessageId == -2)
			$lastMessageid = $this->msgDBO->getLastMessageIdForUser($user1,$user2);
		$this->msgDBO->clearChatMessageList($user1, $user2, $lastMessageid);
		return $lastMessageid;
	}
}
