<?php
include_once (dirname ( __FILE__ ) . '/../include/config_admin.php');
include_once (dirname ( __FILE__ ) . '/../photomodule/photoUtils.php');


try
{
    if($_SESSION['super_admin'] == 'yes' || in_array('quiz_edit', $_SESSION['privileges'] ) ||  in_array('mailer', $_SESSION['privileges'] ))
    {
       // var_dump($_REQUEST); var_dump($_FILES);
        if($_POST['action'] == 'upload_image' && isset($_FILES['file']))
        {
            $data = array('responseCode' =>  200 );
            $img_size= PhotoUtils::validateImage($_FILES['file']);
            if ($img_size == FALSE)
            {
                $data['error'] = "Invalid Image" ;
                print_r(json_encode($data));
                die ;
            }
            else
            {
                $data['image_size'] = $img_size ;
                $name = 1000*microtime(true)."_".PhotoUtils::randomInt(5) .'.jpg';
                $local_location = $image_basedir.$name;
                $image_temp_name = $_FILES['file']['tmp_name'];
                $original_image = WideImage::load($image_temp_name);
                $image_path = "files/images/profiles/";
                if(isset($_REQUEST['path']) && $_REQUEST['path'] !=null)
                    $image_path .= $_REQUEST['path'];
                $image = $original_image->copy();
                $image->saveToFile($local_location,100) ;
                Utils::saveOnS3($local_location,  $image_path . $name);
                $data['image_url'] = $name;
                $data['status'] = "success";
                unlink($local_location);

            }
        }
        else if ($_POST['action'] == 'upload_image')
        {
            $data = array('responseCode' =>  200 ,
                          'error' => "No Image selected");
        }
        else
        {
            $data = array('responseCode' =>  403 ,
                'error' => "unknownRequest");
        }
    }
    else
    {
        $data = array('responseCode' =>  401 );
    }

    print_r(json_encode($data));
} catch (Exception $e) {
    trigger_error("PHP photo upload:: ".$e->getMessage(),E_USER_WARNING) ;
}