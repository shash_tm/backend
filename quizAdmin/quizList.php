<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/../quiz/quizManagerClass.php";
try 
{
	if($_SESSION['super_admin'] == 'yes' || in_array('quiz_view', $_SESSION['privileges'] ) )
	{
		$quizManager = new QuizManager() ;
		$quizzes = $quizManager->getQuizList();

		$smarty->assign ("baseurl" , $admin_baseurl) ;
		if(in_array('quiz_edit',$_SESSION['privileges']) == false &&  $_SESSION['super_admin'] != 'yes')
		{
			$smarty->assign ("read_only" , "true") ;
		}else{
			$smarty->assign ("read_only" , "false") ;
		}


		if(in_array('quiz_status',$_SESSION['privileges'])  || $_SESSION['super_admin'] == 'yes')
		{
			$smarty->assign ("quiz_status" , "true") ;
		}
		$smarty->assign ("quizzes" , $quizzes) ;
	}
	$smarty->display ( "../templates/quizAdmin/quizList.tpl" );

} catch (Exception $e) {
trigger_error($e->getMessage());
} 