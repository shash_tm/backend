<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/../quiz/quizManagerClass.php";
try
{
    if($_SESSION['super_admin'] == 'yes' || in_array('quiz_edit', $_SESSION['privileges'] ) )
    {
        if(isset($HTTP_RAW_POST_DATA) && $HTTP_RAW_POST_DATA != null)
        {
            $data = json_decode($HTTP_RAW_POST_DATA, true);
            if(isset($data['display_name']) && $data['display_name'] != null )
            {
                $quiz_manager = new QuizManager();
                $insert_id = $quiz_manager->saveQuizAttributes($data);

                if (isset($insert_id) && $insert_id != null && isset($data['questions']) && is_array($data['questions']))
                {
                    $quiz_manager->updateQuizQuestions($insert_id, $data['questions']);
                }

                $response = array("status" => "success",
                            "responseCode" => 200,
                                 "quiz_id" => $insert_id);
            }
            else
            {
                $response = array("status" => "failure",
                            "responseCode" => 200,
                                   "error" => "Display name is required at least");
            }
        }

    }

    if($_SESSION['super_admin'] == 'yes' || in_array('quiz_status', $_SESSION['privileges'] ) )
    {
        if(isset($_REQUEST['action'])  && $_REQUEST['action'] != null && isset($_REQUEST['quiz_id']) && $_REQUEST['quiz_id'] != null)
        {
            $quiz_manager = new QuizManager();

            if($_REQUEST['action'] == 'delete')
            {
                $quiz_manager->updateQuizStatus($_REQUEST['quiz_id'] , 'deleted');
            }
            else if($_REQUEST['action'] == 'deactivate' )
            {
                $quiz_manager->updateQuizStatus($_REQUEST['quiz_id'] , 'inactive');
            }
            else if($_REQUEST['action'] == 'activate' )
            {
                $quiz_manager->updateQuizStatus($_REQUEST['quiz_id'] , 'active');
            }
            else if($_REQUEST['action'] == 'undo_delete' )
            {
                $quiz_manager->updateQuizStatus($_REQUEST['quiz_id'] , 'inactive');
            }
            $response = array("status" => "success");
        }

    }

  print_r(json_encode($response));
}
catch (Exception $e)
{
    trigger_error($e->getMessage(), E_USER_WARNING);
}