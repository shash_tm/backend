<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/../quiz/quizManagerClass.php";
try 
{	
	if($_SESSION['super_admin'] == 'yes' || in_array('quiz_view', $_SESSION['privileges'] )|| in_array('quiz_edit', $_SESSION['privileges'] ) )
	{
		if (  isset($_REQUEST['quiz_id']) && $_REQUEST['quiz_id'] != null)
		{
			$quizManager = new QuizManager();
            $quiz= $quizManager->getQuizDetails($_REQUEST['quiz_id']);
		}
	
		if((in_array('quiz_edit',$_SESSION['privileges']) == false &&  $_SESSION['super_admin'] != 'yes') || $quiz['status'] == 'active' || $quiz['status'] == 'deleted')
		{
			$smarty->assign ("read_only" , "true") ;
		}

		$smarty->assign ("quiz" , $quiz) ; 
		$smarty->assign ("baseurl" , $admin_baseurl) ;
		$smarty->assign ("cdnurl" , $cdnurl."/images/quiz/") ;
		$smarty->assign ("imageurl" , $imageurl) ;
		$smarty->display ( "../templates/quizAdmin/quizForm.tpl" );
	}else{
		$smarty->display ( "../templates/quizAdmin/quizList.tpl" );
	}

}
catch (Exception $e)
{
	trigger_error($e->getMessage(), E_USER_WARNING);
} 