<?php
require_once dirname ( __FILE__ ) . "/abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/scripts/mutualLikeMailer.php";


/**
 * updates tables based on user action
 * returns data in case of mutual like
 * @Himanshu
 */

try{
	if (isset ( $_REQUEST['ua'])) {
		$UserActions=new UserActions();
		$val=$UserActions->checkHash();

		if($val){
			$user_id=$_REQUEST['uid'];
			$match_id=$_REQUEST['mid'];
			$action=$_REQUEST['ua'];
			$flag = $_REQUEST['fl'];
			$user_gender = $_REQUEST['g'];
			$repeat_like=$_REQUEST['rl'];
			$availability = $_REQUEST['a'];
			$mf_shown = (isset($_REQUEST['mf_shown']) && $_REQUEST['mf_shown'] == "true") ? True : False;
			/*$ud = new UserData($match_id);
			 $match_id_gender = $ud->fetchGender();
			 */
			//$user_id=$_REQUEST['uid'];
			/*	if(isset($_REQUEST['src']) && $_REQUEST['src'] == "maybe") $maybeFlag = true;
			else $maybeFlag = false;
			*/
			$extra_params = array("mf_shown" => $mf_shown);
			$extra_params['repeat_like']=	$repeat_like;
			$returnAction = $UserActions->performAction($user_id,$match_id, $action, $flag, $user_gender, $availability, false, $extra_params);
			//			var_dump($returnAction);
			$response=array("response_code"=>200);

			if(isset($returnAction)){
				$response['return_action'] = $returnAction;

		          if($returnAction['mutualLike'] > 0 ){
					$uu = new UserUtils();
					$uuDBO = new userUtilsDBO();
					$ud = new UserData($match_id);
					$response['msg_url'] = $uu->generateMessageLink($user_id, $match_id);
					$response['name']=$ud->fetchName();
					echo json_encode($response);
						
				/*	 $mutualMailerObj = new mutualLikeMailer($user_id, $match_id);
					$mutualMailerObj->sendMailer(); 
						
						$mutual_connections = $uuDBO->getMutualConnections($user_id, array($match_id));

					foreach ($mutual_connections as $key => $val){
					$response["mutual_connections_count"] = $val['mutual_connections_count'];
					$response["mutual_connections"] = json_decode($val['mutual_connections'], true);
					}*/
				}
			}
			else
			echo json_encode($response);
		}
	}
}
catch(Exception $e){
	//	echo $e->getMessage();
	trigger_error("PHP WEB: ".$e->getTraceAsString(), E_USER_WARNING);
}


?>
