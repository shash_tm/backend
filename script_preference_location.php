<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
$sql = $conn->Execute("select user_id,location from user_preference_data where location is not NULL");
$data = $sql->GetRows();

$countryOnly = false;
foreach($data as $key=>$value){
	$demo_preference_array = array();
	if($data[$key]['location'] != NULL) {
		$data[$key]['location'] = substr($data[$key]['location'], 0, -1);
		//echo $data[$key]['user_id'];
		//var_dump($data[$key]['location']);
		//echo "<br>";
		$location = explode(":",$data[$key]['location']);
		$countryOnly = true;
		//var_dump($location);
		if(count($location)>1) {
			$countryOnly = false;
		}
		if($countryOnly) {
			$str = "113";
			$demo_preference_array[] = $str;
		}
		// for states
		for($i=1;$i<count($location);$i++) {
			$states = explode("-",$location[$i]);
			//var_dump($states);
			$stateOnly = true;
			if(count($states)>1) {
				$stateOnly = false;
			}
			if($stateOnly) {
				$str = "113_".$states[0];
				$demo_preference_array[] = $str;
			}
			for($j=1;$j<count($states);$j++) {
				$str = "113_".$states[0]."_".$states[$j];
				$demo_preference_array[] = $str;
			}
		}
		
		$demo_preference_array = implode(",",$demo_preference_array);
		//echo $demo_preference_array;
		//echo "<br>";
		$conn->Execute($conn->prepare("update user_preference_data SET location_preference_for_matching=? where user_id=? "),
				array($demo_preference_array,$data[$key]['user_id']));
	}
}
}
catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}


?>