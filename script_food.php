<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	$sql = $conn->Execute("select user_id,food_favorites,travel_favorites from interest_hobbies");
	
	$data = $sql->GetRows();
	
	//var_dump($data);
	
	
	foreach($data as $key=>$value){
		$food = json_decode($data[$key]['food_favorites'],true);
		$travel = json_decode($data[$key]['travel_favorites'],true);
		if(empty($food) && empty($travel)){
			$travel_food = null;
			//echo "Both are empty";
		}else if(!empty($food) && empty($travel)){
			$travel_food = $food;
			//echo "travel is empty but food is not";
		}else if(!empty($travel) && empty($food)){
			$travel_food = $travel;
			//echo "food is empty but travel is not";
		}else{
			$travel_food = array_merge_recursive( $travel, $food );
		}
		//json_encode($travel_food);
		
		$conn->Execute($conn->prepare("update interest_hobbies SET travel_favorites=? where user_id=? "),
				array(json_encode($travel_food),$data[$key]['user_id']));
		
	}	
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}

?>