<?php

require_once dirname ( __FILE__ ) . "/matches/matchScore.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/fb.php";
//require_once dirname ( __FILE__ ) . "/UserActions.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
//require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/abstraction/query.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/include/matchConstants.php";
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/logging/QueryLog.class.php";



/**
 * Generate new matches API
 * @author himanshu
 */

// why pi
class generateNewMatches {


	private $userId; 
	private $recommendation_number ;   //??
	private $key;
	private $cronRecommendationLimit = 100;
	private $timeForMatchesExpiry ; // 3hrs
	private $timeForMatchCountExpiry ;
	private $matchCountKey;
	private $uuDBO;
	private $uu;
	private $redis;
	private $gender;
	private $status;
	private $ambiguousCitiesChandigarh;
	private $ambiguousStatesChandigarh;
	private $ambiguousCitiesNCR;
	private $ambigousStates;
	private $isFreshBucket ; 
	private $freshProfilesForFemale; 
	private $totalProfilesForFemale;
	private $queryLog;

	function __construct($user_id) {
		global $redis;
		//	parent::__construct ( $user_id );
		$this->userId = $user_id;
		$this->ambiguousCitiesNCR = array(47965,47968, 47964, 47967,47966 ); //??
		$this->ambigousStates = array(2168, 2176, 2193);		//states : 2168 - NCR,  2176- Haryana, 2193 - UP
		$this->ambiguousCitiesChandigarh = array(16007, 42795);   //?
		$this->ambiguousStatesChandigarh = array(2172,2189);      //?
		

		$secondsInAnHour = 3600; 
		$ud = new UserData($user_id);
		$gender = $ud->fetchGender();        //this->gender=$ud->fetchGender();  ?????
		$this->status = $ud->fetchStatus();
		$this->gender = $gender;

		$this->recommendation_number = ($gender=="M")?Utils::$recommendation_count_male:Utils::$recommendation_count_female;  
		//$this->redis = new redisClass();
		$this->key = Utils::$redis_keys['matches']. "$user_id";           //key for caching in redis
		$this->matchCountKey = Utils::$redis_keys['match_action_count']. "$user_id";   //key for refreshing matches

		//match key to expire in 8 hours for male and 3 hours for female
//		$this->timeForMatchesExpiry = ($gender == "M")?($secondsInAnHour*8):($secondsInAnHour*3);

//changing time for females to 8 hrs
	//	$this->timeForMatchesExpiry = $secondsInAnHour*8;
		

		//match key to expire in 3 hours for male and 30 min for female
		$this->timeForMatchesExpiry = ($gender == "F")?Utils::$femaleMatchesExpiryTime:Utils::$maleMatchesExpiryTime;    //setting the match refresh time
		
 		
		$this->timeForMatchCountExpiry = $this->timeForMatchesExpiry;

		/**
		 * modifying the time for expiry in case there is time diff negative
		 */
		//$this->timeForMatchCountExpiry = $this->timeForMatchesExpiry ;
		 //echo Utils::timeTillMidnight(); die;
		if($this->gender == "M"){
			$timeTillMn= Utils::timeTillMidnight(1) - (60*330);
			//$timeTillMn = 14651 - (60*330);
			if($timeTillMn<0)$this->timeForMatchCountExpiry = Utils::timeTillMidnight(2)- (60*330);
			else $this->timeForMatchCountExpiry = Utils::timeTillMidnight() - (60*330);
		}
		//echo $this->timeForMatchCountExpiry/60 ;die;
		//	echo (($this->timeForMatchCountExpiry) - )/3600; die;
		$this->uu = new UserUtils();
		$this->uuDBO = new userUtilsDBO();
		$this->redis = $redis;
		
		$this->queryLog = new QueryLog();
	}

	/** 
	 * fetch data from recommendation query if a new user comes in
	 * else fetch result from redis
	 */  
	public function fetchResults( ) {

		/**
		 * Don't use TTL b'cos
		 * TTL in seconds or -1 when key does not exist or does not have a timeout.
		 * which actually might break flow for non-authentic users
		 */
		$matchKeyExists = $this->uu->ifKeyExists($this->key);
		if(!$matchKeyExists){
			/**
			 * $actionCount : the user has already taken action on
			 * $count: count to fetch now
			 */
			$actionCount = $this->redis->GET($this->matchCountKey);
			if(!isset($actionCount))  $actionCount = 0;
			//$countAfterEdit = null;
			//if any action is taken and call is after edit then generate the remaining count only
		/*	if($actionCount > 0 ){
				if($this->gender == "M"){
					$countAfterEdit = $this->recommendation_number - $actionCount;
					if($countAfterEdit <= 0) $countAfterEdit = 0;
					else if ($countAfterEdit != $this->recommendation_number) 
				}else{
					$countAfterEdit = Utils::$like_threshold_female - $actionCount;
				}
			}*/

			//adding extra validation
			//if($actionCount == null)$actionCount=0;
			//change this if expiry of matches change for females
			/*if($this->gender == "F"){
			$actionCount = 0;
			}
			$count = $this->recommendation_number - $actionCount;
			*/
			//if($count <= 0) return array();
			//$this->generateMatchesUsingMemoryTable($this->userId, $count, $actionCount, $app_version_code);
			//if($countAfterEdit == null || $countAfterEdit >0 )
			$this->generateMatchesUsingMemoryTable($this->userId, $actionCount);

		}
		return $this->fetchMatches();
	}

	public function fetchMatches(){
		//	$arr = $this->redis->SMEMBERS($this->key);
		/*
		if($this->gender == "M") $start_range = 1;
		else $start_range = 0;*/
		$start_range = 0;
		$arr = $this->redis->LRANGE($this->key, $start_range, -1);
		
		//removal of -1 if present in the redis set
		$key = array_search('-1', $arr);
		unset($arr[$key]);

		$result = array();
		foreach ($arr as $val){
			$result[] = $val;
		}
		return $result;
	}

	
	/**
	 * get batch size for female based on bucket
	 * @param unknown_type $piData
	 */
	private function _setBatchSizeForFemale ($piData)
	{
		$count = 0 ;

		if ($this->isFreshBucket == true )
		{
			$count = BatchSize::FemaleBucketZero;
		}
		else if ($piData['bucket'] == 1)
		{
			$count = BatchSize::FemaleBucketOne;
		}
		else if ($piData['bucket'] == 2)
		{
			$count = BatchSize::FemaleBucketTwo;
		}
		else if ($piData['bucket'] == 3)
		{
			$count = BatchSize::FemaleBucketThree;
		}
		else if ($piData['bucket'] == 4)
		{
			$count = BatchSize::FemaleBucketFour;
		}
		else if ($piData['bucket'] == 5)
		{
			$count = BatchSize::FemaleBucketFive;
		}
		//	$count = Utils::$recommendation_count_female;


		$this->totalProfilesForFemale = $count;
	}
	
	/**
	 * return the count to be fetched for a user for fresh profiles
	 * @param $gender
	 * @param $isFreshCall
	 * @param $piData
	 */

	private function _getFreshProfilesCount ( $gender, $isFreshCall, $piData, $alreadyActionedCount )
	{
		$count = 0;
		if ($gender == "F")
		{
			if ($this->isFreshBucket == true )
			{
				$count = FreshBatchCount::FemaleBucketZero;
			}
			else if ($piData['bucket'] == 1)
			{
				$count = FreshBatchCount::FemaleBucketOne;
			}
			else if ($piData['bucket'] == 2)
			{
				$count = FreshBatchCount::FemaleBucketTwo;
			}
			else if ($piData['bucket'] == 3)
			{
				$count = FreshBatchCount::FemaleBucketThree;
			}
			else if ($piData['bucket'] == 4)
			{
				$count = FreshBatchCount::FemaleBucketFour;
			}
			else if ($piData['bucket'] == 5)
			{
				$count = FreshBatchCount::FemaleBucketFive;
			}
			//	$count = Utils::$recommendation_count_female;
		}
		else
		{
			if ($isFreshCall == true)
			{

				if ( $piData['active_chats'] >= Utils::$activeChatsThreshold)
				{
					$count = FreshBatchCount::MaleExceedingChatThrehold;
				}
				else if ($this->isFreshBucket == true )
				{
					$count = FreshBatchCount::MaleBucketZero;
				}
				else if ($piData['bucket'] == 1)
				{
					$count = FreshBatchCount::MaleBucketOne;
				}
				else if ($piData['bucket'] == 2)
				{
					$count = FreshBatchCount::MaleBucketTwo;
				}
				else if ($piData['bucket'] == 3)
				{
					$count = FreshBatchCount::MaleBucketThree;
				}
				else if ($piData['bucket'] == 4)
				{
					$count = FreshBatchCount::MaleBucketFour;
				}
				else if ($piData['bucket'] == 5)
				{
					$count = FreshBatchCount::MaleBucketFive;
				}


				/*(if ($piData['bucket'] == 1 || $piData['bucket'] == 2 || $piData['active_chats'] >= Utils::$activeChatsThreshold)
				 {
					$count = Utils::$recommendationFreshLikesCountBadBucketOrMoreChats;
					}
					else
					{
					$count = Utils::$recommendationFreshLikesCountGoodBucketsLessChats;
					}*/
			}
			else
			{
				if ( $piData['active_chats'] >= Utils::$activeChatsThreshold)
				{
					if ($alreadyActionedCount < FreshBatchCount::MaleExceedingChatThrehold)
					{
						$count = RepeatBatchCount::MaleExceedingChatThrehold;
					}
					else
					{
						$count = 0 ;
					}
				}
				else if ($piData['bucket'] == 0 || $piData['actual_pi'] == -1 )
				{
						
					if ($alreadyActionedCount < LikeCountThreshold::MaleBucketZero)
					{
						$count = RepeatBatchCount::MaleBucketZero;
					}
					else
					{
						$count = 0 ;
					}
						
				}
				else if ($piData['bucket'] == 1)
				{
					if ($alreadyActionedCount < LikeCountThreshold::MaleBucketOne)
					{
						$count = RepeatBatchCount::MaleBucketOne;
					}
					else
					{
						$count = 0 ;
					}
				}
				else if ($piData['bucket'] == 2)
				{
					if ($alreadyActionedCount < LikeCountThreshold::MaleBucketTwo)
					{
						$count = RepeatBatchCount::MaleBucketTwo;
					}
					else
					{
						$count = 0 ;
					}
				}
				else if ($piData['bucket'] == 3)
				{
					if ($alreadyActionedCount < LikeCountThreshold::MaleBucketThree)
					{
						$count = RepeatBatchCount::MaleBucketThree;
					}
					else
					{
						$count = 0 ;
					}
				}
				else if ($piData['bucket'] == 4)
				{
					if ($alreadyActionedCount < LikeCountThreshold::MaleBucketFour)
					{
						$count = RepeatBatchCount::MaleBucketFour;
					}
					else
					{
						$count = 0 ;
					}
				}
				else if ($piData['bucket'] == 5)
				{
					if ($alreadyActionedCount < LikeCountThreshold::MaleBucketFive)
					{
						$count = RepeatBatchCount::MaleBucketFive;
					}
					else
					{
						$count = 0 ;
					}
				}

				/*	if ($piData['bucket'] == 1 || $piData['bucket'] == 2 || $piData['active_chats'] >= Utils::$activeChatsThreshold)
				 {
					if ($alreadyActionedCount < Utils::$recommendationFreshLikesCountBadBucketOrMoreChats)
					{
					$count = Utils::$recommendationRepeatCountBadBucketOrMoreChats;
					}
					else
					{
					$count = 0 ;
					}
					}
					else
					{
					if ($alreadyActionedCount < Utils::$recommendationFreshLikesCountGoodBucketsLessChats)
					{
					$count = Utils::$recommendationRepeatCountGoodBucketsLessChats;
					}
					else
					{
					$count = 0;
					}
					}*/
			}
		}

		return $count;
	}

	/**
	 * get rank case in case of uninstalled user
	 */
	private function _getUninstallRankCase ()
	{
		return  "case when install_status = 'uninstall' then -10000 else 0 end";
	}
	
	
	/**
	 * get type of call if fresh or repeat
	 * @param unknown_type $countToFetch
	 * @param unknown_type $gender
	 */
	private function _getCallType ($countToFetch, $gender)
	{
		$isFreshCall = false;
		if ($gender == "M")
		{
			if ($countToFetch == Utils::$recommendation_count_male)
			$isFreshCall = true;
			else $isFreshCall = false;
		}
		else
		{
			if ($countToFetch == Utils::$recommendation_count_female)
			$isFreshCall = true;
			else $isFreshCall = false;
		}
		
		return $isFreshCall;
	}
	
	
	/**
	 * return if it's a fresh call or not
	 * @param unknown_type $alreadyActionedCount
	 */
	private function _isFreshCall ($alreadyActionedCount)
	{
		return  ($alreadyActionedCount > 0) ? false : true;
	}
	
	

	/**
	 * get the rank case for buckets/pi
	 * @param unknown_type $bucket
	 * @param unknown_type $piScore
	 */
	private function _getPIRanksAndFiltersForFemale ( $bucket , $piScore)
	{

		$piRankCaseLikeQuery = " $piScore ";
		$piFilterCaseLikeQuery = '';

		$piRankCaseFreshQuery = '';
		$piFilterCaseFreshQuery = '';


		if($this->isFreshBucket == true)
		{
			$piRankCaseFreshQuery = " case when bucket In (4,5) then $piScore else 0 end ";
			$piFilterCaseLikeQuery = " and bucket in (4,5,0) ";
		}
		if ($bucket == 1)
		{
			//$piCase = "case when bucket >= 1 and bucket <=5 and  pi != -1 and pi is not null then $piScore else 0 end ";
			$piRankCaseFreshQuery = "case when bucket not in (4,5) then $piScore else 0 end ";
//			$piRankCaseFreshQuery = " $piScore ";
		}
		else if ($bucket == 2)
		{
			//$piCase = "case when bucket >= 2 and bucket <=5 then $piScore else 0 end ";

			$piRankCaseFreshQuery = "case when bucket not in (1,5) then $piScore else 0 end ";
 			//$piRankCaseFreshQuery = " $piScore ";
			$piFilterCaseLikeQuery = " and bucket != 1 ";

		}
		else if ($bucket == 3)
		{
			$piRankCaseFreshQuery = "case when bucket !=1 then $piScore else 0 end ";
			$piFilterCaseLikeQuery = " and bucket not in (1,2) ";

		}
		else if ($bucket == 4)
		{
			$piRankCaseFreshQuery = "case when bucket not in (1,2) then $piScore else 0 end ";
			$piFilterCaseLikeQuery = " and bucket not in (1,2,3) ";

		}
		else if ($bucket == 5)
		{
			$piRankCaseFreshQuery = "case when bucket in (4,5) then $piScore else 0 end ";
			$piFilterCaseLikeQuery = " and bucket not in (1,2,3) ";

		}
			
		return array(	"piRankCaseLikeQuery"	 => $piRankCaseLikeQuery ,
						"piFilterCaseLikeQuery"	 =>	$piFilterCaseLikeQuery, 
						"piRankCaseFreshQuery" 	 => $piRankCaseFreshQuery ,
						"piFilterCaseFreshQuery" => $piFilterCaseFreshQuery );
	}

	
	
		/**
	 * get the rank case for buckets/pi
	 * @param unknown_type $bucket
	 * @param unknown_type $piScore
	 */
	private function _getPIRankForMale ( $bucket , $piScore)
	{
		$piRankCase = '';
		
		if ($this->isFreshBucket == true)
		{
		//	$piRankCase = "case when bucket >= 1 and bucket <=5 then $piScore else 0 end ";
			$piRankCase = $piScore ;
		}
		else if ($bucket == 1)
		{
			$piRankCase = "case when bucket = 1 then $piScore else 0 end ";
		}
		else if ($bucket == 2)
		{
			$piRankCase = "case when bucket >= 1 and bucket <=2 then $piScore else 0 end ";
		}
		else if ($bucket == 3)
		{
			$piRankCase = "case when bucket >= 1 and bucket <=3 then $piScore else 0 end ";
		}
		else if ($bucket == 4)
		{
			$piRankCase = "case when bucket != 1 then $piScore else 0 end ";
		}
		else if ($bucket == 5)
		{
			$piRankCase = "case when bucket not in (1,2) then $piScore else 0 end ";
		}

		return $piRankCase;
	}
	
	
	  /**
      * get availability filter 
      */
     private function _getAvailabilityFilterString ( $gender )
     {
     	$availabilityFilter = '' ;
/*
     	if ($gender == "M")
     	{
     		if ($pi == -1 || $pi == null)
     		{
     			$availabilityFilter = " and fresh_slots_filled < " . AvailabilityFilters::bucketZero . " or fresh_slots_filled is null) ";
     		}
     		else if ($bucket == 1)
     		{
     			$availabilityFilter = " and ( slots_filled < " . AvailabilityFilters::bucketOne ." or slots_filled is null) ";
     		}
     		else if ($bucket == 2)
     		{
     			$availabilityFilter = " and ( slots_filled < " . AvailabilityFilters::bucketTwo ." or slots_filled is null) ";
     		}
     		else if ($bucket == 3)
     		{
     			$availabilityFilter = " and ( slots_filled < " . AvailabilityFilters::bucketThree ." or slots_filled is null) ";
     		}
     		else if ($bucket == 4)
     		{
     			$availabilityFilter = " and ( slots_filled < " . AvailabilityFilters::bucketFour ." or slots_filled is null) ";
     		}
     		else if ($bucket == 5)
     		{
     			$availabilityFilter = " and ( slots_filled < " . AvailabilityFilters::bucketFive ." or slots_filled is null) ";
     		}
     	}
     	*/
     	
     	if ($gender == "M")
     	{
     		if($this->isFreshBucket == true)
					$availabilityFilter = " and case when age>=18 and age<=20 then (  
													case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (fresh_slots_filled  < ". (2*AvailabilityFilters::age_18_20)." or fresh_slots_filled is null)
													else (fresh_slots_filled  < ". AvailabilityFilters::age_18_20." or fresh_slots_filled is null)
													end
													)

												when age>20 and age<=22 then(
													case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (fresh_slots_filled  < ". (2*AvailabilityFilters::age_21_22)." or fresh_slots_filled is null)
													else (fresh_slots_filled  < ". AvailabilityFilters::age_21_22." or fresh_slots_filled is null)
													end
													)

												when age>=23 and age<=26 then (  
                                                                                                        case 
                                                                                                        when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (fresh_slots_filled  < ". (2*AvailabilityFilters::age_23_26)." or fresh_slots_filled is null)
                                                                                                        else (fresh_slots_filled  < ". AvailabilityFilters::age_23_26." or fresh_slots_filled is null)
                                                                                                        end)

												when bucket>0 then (
													case 
													when bucket =1 then ( fresh_slots_filled  < ". AvailabilityFilters::bucketOne." or fresh_slots_filled is null) 
													when bucket = 2 then ( fresh_slots_filled  < ".AvailabilityFilters::bucketTwo."  or fresh_slots_filled is null) 
													when bucket = 3 then ( fresh_slots_filled  < ". AvailabilityFilters::bucketThree."  or fresh_slots_filled is null)
													when bucket = 4 then ( fresh_slots_filled  < ". AvailabilityFilters::bucketFour."  or fresh_slots_filled is null)
													else  ( fresh_slots_filled  < ". AvailabilityFilters::bucketFive."  or fresh_slots_filled is null) 
													end) 

												else (fresh_slots_filled < ". AvailabilityFilters::bucketZero." or fresh_slots_filled is null)  end ";



else  $availabilityFilter = " and case when age>=18 and age<=20 then ( 
							     					case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (slots_filled < " . (2*Utils::$availability_slots_limit_18_20)." or slots_filled is null)
													else (slots_filled < " . Utils::$availability_slots_limit_18_20." or slots_filled is null)
													end
													)

							     		when age>20 and age<=22 then ( 
							     					case 
													when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (slots_filled < " . (2*Utils::$availability_slots_limit_21_22)." or slots_filled is null)
													else (slots_filled < " . Utils::$availability_slots_limit_21_22." or slots_filled is null)
													end
													)
							     		
							     		when age>=23 and age<=26 then ( case 
                                                                                                        when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, curdate())<=7 then (slots_filled < " . (2*Utils::$availability_slots_limit_21_22)." or slots_filled is null)
                                                                                                        else (slots_filled < " . Utils::$availability_slots_limit_23_26." or slots_filled is null)
                                                                                                        end)
							     		else ( slots_filled < " .Utils::$availability_slots_limit ." or slots_filled is null) end";

     		
     	}

     	return  $availabilityFilter;
     }

	
	
	
	private function getRecommendationSet($user_id, $from_cron = false) {
		/*if($from_cron == true){

		//$sql1 = "SELECT user2 from user_recommendation where user1=$this->userId";
		$sql1 = "SELECT user2 from user_recommendation where user1=$this->userId
		and user2 not in (select user2 from user_like where user1 = $this->userId
		union select user2 from user_reject where user1 = $this->userId
		union select user2 from user_rejectMe where user1=$this->userId)";
		$rs = $this->conn->Execute($sql1);

		$count = $rs->RowCount();
		echo $count;
		if($count>=$this->cronRecommendationLimit) return 'exceeded_limit';

		$data = $rs->GetRows();
		}

		else{*/
		/*		$sql =  "select user1  as user from user_hide where user2=?
		 union select user2  as user from user_hide where user1=?
		 */		/*$sql =  "select user1 as user, 0 as flag from user_hide where user2=?
				 union select user2 as user, 0 as flag from user_hide where user1=?
				 union select user2 as user, 2 as flag from user_like where user1=?
				 union select user1 as user, 1 as flag from user_like where user2=?" ;
		         */
		        $sql="select t.user, t.flag from (
                 select user1 as user, 0 as flag from user_hide where user2=?
                 union select user2 as user, 0 as flag from user_hide where user1=?
                 union select user2 as user, 2 as flag from user_like where user1=?
                 union select user1 as user, 1 as flag from user_like where user2=?)t join user_search u on t.user=u.user_id";
		 
		$output = array ();
		$output_likedMe = array();
		$output_iLiked = array();
		$data = Query::SELECT($sql, array($user_id, $user_id, $user_id, $user_id, $user_id));
		//	}
		//var_dump($data);
		foreach ( $data as $val ) {
			if($val['flag'] == 1)$output_likedMe[] = $val['user'];
			if($val['flag'] == 2) $output_iLiked[] = $val['user'];
			if($val['flag'] == 0) $output [] = $val ['user'];
		}
		$output = array_unique ( $output );
		//var_dump($user_id);
		//var_dump($output);exit;
		return array("likedMe" =>$output_likedMe, "nonLikedMe" => $output, "iLiked" => $output_iLiked);
	}

	public function add_apostrophe(&$item1, $key) {
		$item1 = "'$item1'";
	}
	private function addApostrophe($parameter, $separator = ",") {
		$type = explode ( "$separator", $parameter );
		$type = array_filter($type);
		$val = array_walk ( $type, array (
		$this,
				'add_apostrophe' 
				) );
				return implode ( ",", $type );
	}

	private function checkissetorEmpty($value){
		if(isset($value)==false||(is_string($value) && strlen($value)==0))
		return false;
		return true;
	}

	/*	private function countOfProfilesToFetch(){
		return $this->redis->LRANGE($this->key, 0 , -1);
		}

		//Trim the list so that it contains two elements viz. -1 and the last seen profile
		private function removeLastLoginProfiles(){
		$this->redis->LTRIM($this->key, 0, 1);
		//		ltrim test 0 1
		}*/

/*	public function getPreferenceLocationStringForNCR($states, $cities){
		$additionalLocationString = "";
		//47968 - Greater Noida, 47965 - noida, 47964 - gurgaon, 47967 - ghaziabad, 47966 - faridabad
		//states : 2168 - NCR,  2176- Haryana, 2193 - UP
		//if city is in NCR
		$cityClash = array();
		if(isset($cities)){
			$cityClash = array_intersect($cities, $this->ambiguousCitiesNCR);
		}
		if(!empty($cityClash)){
			$additionalLocationString = " OR (city in (". implode(',', $cityClash) ."))";
		}else{
			
			if (in_array("2168", array_keys($states))){
			$additionalLocationString = " OR (city in (" . implode(',', $this->ambiguousCitiesNCR) . "))";
		}else(in_array("2176", array_keys($states))){
			$additionalLocationString = " OR (city in (" . implode(',', $this->ambiguousCitiesNCR) . "))";
		}}

		return $additionalLocationString;
		
			$stateStr = " state = '$myState' ";
			$ambiguousCitiesNCRHaryana = array(47964, 47966);
			$ambiguousCitiesNCRUP = array(47968,47965,47967);
			if(in_array($myCity, $ambiguousCitiesNCR)){
			if(in_array($myCity, $ambiguousCitiesNCRHaryana)){
			$stateStr = " state in (2176,2168)";
			}
			if(in_array($myCity, $ambiguousCitiesNCRUP)){
			$stateStr = " state in (2193,2168)";
			}
			}
			//$statesForambiguousCitiesNCR = array();
			if($prefLocation == null){
			//$myPrefLocation = '113';
			$location = " and (((city = '$myCity' and $stateStr) or $stateStr )) " ;
			}
			elseif( rtrim($prefLocation, ";") == "113"){
			$location = " and (((city = '$myCity' and $stateStr) or $stateStr )) " ;
			}
			

	}*/
	
		/**
	 * get the location ranking case 
	 * @param unknown_type $city
	 * @param unknown_type $state
	 * @param unknown_type $gender
	 */
	private function _getLocationRankCase ($city, $state, $locationScore)
	{
		$stateQuery = '';
		/*$cityQuery = '';
		if($city == $this->ambiguousCitiesChandigarh)
		$cityQuery = " OR (city in (" . implode(',', $this->ambiguousCitiesChandigarh) . "))";
		else if ($city == $this->ambiguousCitiesNCR)
		$cityQuery = " OR (city in (" . implode(',', $this->ambiguousCitiesNCR) . "))";
	*/		
		// 47968 - Greater Noida, 47965 - noida, 47964 - gurgaon, 47967 - ghaziabad, 47966 - faridabad
		// states : 2168 - NCR,  2176- Haryana, 2193 - UP
		if(in_array($city, $this->ambiguousCitiesNCR) || $state == "2168")
		{
			$stateQuery .= " OR  (city in (" . implode(',', $this->ambiguousCitiesNCR) . " , 16743))";
			/*
			if($state == 2168)
			$stateQuery .= " OR  (city in (" . implode(',', $this->ambiguousCitiesNCR) . "))";
			elseif($state == 2176)
			$stateQuery .= " OR (city in (47964,47966 ))";
			elseif($state == 2193)
			$stateQuery .= " OR (city in (47968, 47965, 47967))";*/

		}
		//if punjab/chd is filled in state and city is not filled
		else if(in_array($state, $this->ambiguousStatesChandigarh))
		{
			$stateQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesChandigarh) . "))";
		}
			
		
		return "case when city = '$city' $stateQuery  then ".  $locationScore."  else(case when state = '$state'  then ". $locationScore/2 . " else 0 end) end ";
	
	}
		
		
	
		
	private function _setIsFreshBucket ($pi, $bucket)
	{
		$this->isFreshBucket =  ($pi == -1 || $pi == null || $bucket == 0) ? true : false;
	}

	/**
	 * NOTE: whenever you add/remove any new data point from a query, update the sanity in test cases
	 * @param unknown_type $user_id
	 * @param unknown_type $actionTaken
	 * @param unknown_type $debug
	 */
	public function generateMatchesUsingMemoryTable($user_id , $actionTaken = 0, $debug = false){
		
	 
		/**
		 * finding how many matches to find as fresh
		 * in male case - 
		 *     fresh -  it'll be controlled - based on bucket and active chats (follow the below logic in case of edit as well)
		 *     likes - push after every x hours
		 * in female case -
		 * 	   fresh & likes - send as per quota even after edit 
		 */

				
		$myPIData = $this->uuDBO->getUserPiInfo(array($user_id), true);
		$myPi  = $myPIData['actual_pi'];
		$myBucket = $myPIData['bucket'];
		$this->_setIsFreshBucket($myPi, $myBucket);
		//$this->isFreshBucket =  ($myPi == -1 || $myPi == null || $myBucket == 0) ? true : false; 
		$this->_setBatchSizeForFemale($myPIData);
		
		$isFreshCall = $this->_isFreshCall($actionTaken);

		$fetchFreshCount = $this->_getFreshProfilesCount($this->gender, $isFreshCall, $myPIData, $actionTaken);
		
		/*$nonAuthenticIosCall = false;

		//go through this only when it's not a call from non-authentic IOS user
		if ($this->status == "non-authentic" && $actionTaken > 0)
		{
			$nonAuthenticIosCall = true;
			
			if($this->gender == "F")
			$fetchFreshCount =  Utils::$like_threshold_female - $actionTaken;
			else $fetchFreshCount = $this->recommendation_number - $actionTaken;
			
			if($fetchFreshCount <= 0 ) return ;
			
		}*/
		
		//public function generateMatchesUsingMemoryTable($user_id, $countToFetch = 0, $actionTaken = 0,  $app_version_code = null){
		//var_dump($countToFetch);die;
		$user_id_to_fliter = $this->getRecommendationSet ( $this->userId , $from_cron);
		//	var_dump($user_id_to_fliter);
		$iLikedIds = $user_id_to_fliter['iLiked'];
		$likedMeFilters = $user_id_to_fliter['likedMe'];
		$nonLikedMe = $user_id_to_fliter['nonLikedMe'];
		$total_ids = array_merge($likedMeFilters,  $nonLikedMe, $iLikedIds);
		$nonLikedMe_ids = array_merge($nonLikedMe , $iLikedIds);

		//To get the only unique ids to filter
		$total_ids = array_unique($total_ids);
		$nonLikedMe_ids = array_unique($nonLikedMe_ids);

		$user_id_to_fliter_total = implode ( ",", array_values($total_ids) );
		$user_id_to_fliter_nonLikedMe = implode(',', array_values($nonLikedMe_ids));

		/*var_dump($user_id_to_fliter_total);
		 var_dump($user_id_to_fliter_nonLikedMe);*/
		$prefData = $this->uuDBO->getUserPreferenceData(array($user_id), true);
		$myData = $this->uuDBO->getUserData(array($user_id), true);
		$myBasicInfo = $this->uuDBO->getUserBasicInfo(array($user_id), true);

		//$myData = "SELECT * FROM user_search where user_id = $user_id";
	
		$myActiveChats = isset($myPIData['active_chats'])?$myPIData['active_chats']:0;
		//$myLikes = isset($myPIData['total_likes'])?$myPIData['total_likes']:0;
		//$myHides = isset($myPIData['total_hides'])?$myPIData['total_hides']:0;
		//var_dump($myBasicInfo);die;
		//var_dump($myData);die;
		$myGender = $myBasicInfo['gender'];
		$oppositeGender =  ($myGender == 'M')?'F':'M';
		/*
		 *
		 * select t.user_id, t.user1, case when t.deviation <=2 then 4 else
		 * (case when t.deviation <=5 then 3 else (case when t.deviation <=7 then 2 else 1 end) end) end + t.total_score as score
		 * from      (      select user_id, ul.user1, case when religion = 2 then 2 else (case when religion = 6 then 0 else 1 end)end
		 * 			+   case when marital_status in ('never married') then 2 else 0 end + case when city =2146 then 2 else(case when state = 11 then 1 else 0 end) end  + case when ((income_start >=5 and income_end <=10) OR (income_start=5)) then 2 else (case when income_start=2 then 1 else 0 end) end + case when ul.user1 is not null then 1 else 0 end as total_score , (abs(sumA-10) +abs(sumB-1)+abs(sumC-2)+abs(sumD-3)+abs(sumE-4)+abs(sumF-1))/6   as deviation from user_search_himanshu1  u left join user_like2 ul on u.user_id= ul.user1 and ul.user2 = 10 where gender='F' and age>='30' and age<='39'   and marital_status in ('Never Married') and   ( (country='113'  and ( (state='2168'  and city in (16743) ) or    (state='2172'  and city in (16007) ) or (state='2176' ) ) )  )     and ( (religion='1'  )  or (religion='3'  )  )     and height>='60' and height<='72' and     highest_education in ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16') and     (GREATEST(IFNULL(income_start,-100),-100)<=    LEAST(IFNULL(income_end ,200),200))      and pref_start_age<='20' and (pref_end_age+1)>='20'  and     (pref_marital_status regexp '[[:<:]]Never Married[[:>:]]' or pref_marital_status is NULL)      and (pref_location is null or pref_location       regexp '[[:<:]]113_2168_16743[[:>:]]' or pref_location             regexp '[[:<:]]113_2168[[:>:]]' or pref_location       regexp '[[:<:]]113[[:>:]]')             and pref_start_height<='70' and pref_end_height>='70'    and (pref_smoking_status regexp '[[:<:]]Never[[:>:]]' or pref_smoking_status is NULL)   and (pref_drinking_status regexp '[[:<:]]Never[[:>:]]' or pref_drinking_status is NULL)     and (pref_food_status regexp '[[:<:]]Non-Vegetarian[[:>:]]' or pref_food_status is NULL)      and (GREATEST(IFNULL(pref_income_start,-100),10)<=LEAST(IFNULL(pref_income_end ,200),20))     and (pref_education regexp '[[:<:]]21[[:>:]]' or pref_education is NULL)      and user_id not in (1129,4095,8895,9410,11898,12567,1,2,34,32,4242,42,42,42,42,3421,532532,52352,5,224,24,23,423,423,423,4,24,24,234,23,42325,25,252,352,532,523,5235,23,523523,52,5252,52,52,52552,52,25,25,25,2,5,2,87,97,979,69,56,53,85,835,8657567,65,75,75735,46,414,2,2353463,46,45645756,7,65765,7,56879,9,7,978,987,9789,4324,23,423,423,4,234,345346,35,46,54,65,7,65,786,867,8,67,867,876,9,789,87,9789,7,93492739,92,749237,49237,482648,84682364,816,48126481641,816481,481,6481,648346,82364823,482346,823648,23,6823823,8,38,3423)        limit 100000       )t order by score limit 100;
		 *
		 */
		/*var_dump($prefData['user_id']);
		 var_dump($prefData);*/


		$myMaritalStatusPref = (isset($prefData['marital_status']))?$this->uu->getPreferenceMaritalStatus($prefData['marital_status']):$prefData['marital_status'];
		$prefStartAge = $prefData['start_age'];
		$prefEndAge = $prefData['end_age'];
		/*$myPrefIncomeStart = $prefData['income_start'];
		 $myPrefIncomeEnd = $prefData['income_end'];
		 */

		$myPrefIncome = json_decode($prefData["income"], true);

		if(isset($myPrefIncome))
		$myPrefIncome = implode(',', $myPrefIncome);
		$myPrefSmoking =  $prefData ['smoking_status'] ;
		$myPrefDrinking = $prefData ['drinking_status'];
		$myPrefFood = $prefData ['food_status'] ;
		$prefStartHeight = $prefData ['start_height'];
		$prefEndHeight = $prefData ['end_height'];
		$prefEducation = $prefData['education'];
		$prefLocation = $prefData ['location'];
			
		$myAge = $myData['age'];
		$myReligion = $myData['religion'];
		$myCity = $myData['stay_city'];
		$myState = $myData['stay_state'];
		$myCountry = $myData['stay_country'];
		$myMaritalStatus = $myData['marital_status'];
		$myIncomeStart = $myData['income_start'];
		$myIncomeEnd = $myData['income_end'];
		$myHeight = $myData ['height'];
		$myEducation = $myData['highest_degree'];
		$myEducationGroup = $myData['education_group'];
		$mySmoking =  $myData ['smoking_status'] ;
		$myDrinking = $myData ['drinking_status'];
		$myFood = $myData ['food_status'];

			
		$myCityPref = null;
		$myStatePref = null;
		$n=0;
		$m=0;
		if ($this->checkissetorEmpty ( $prefData ['location'] )) {
			// 113:5268-42504;1:827:829:5296;
			$location_string = explode ( ";", $prefData ['location'] );
			$final_array = array ();
			foreach ( $location_string as $val ) {
				if (strlen ( $val ) == 0)
				continue;
				$state_segment = explode ( ":", $val );
				$country = $state_segment [0];
				$final_array [$country] = array ();
				for($j = 1; $j < count ( $state_segment ); $j ++) {
					$city_segment = explode ( "-", $state_segment [$j] );
					$state = $city_segment [0];
					$myStatePref[$m] = $state;
					$m++;
					$final_array [$country] [$state] = array ();
					for($k = 1; $k < count ( $city_segment ); $k ++) {
						$final_array [$country] [$state] [$city_segment [$k]] = "";
						$myCityPref[$n]= $city_segment[$k];
						$n++;
					}
				}
			}
			$locationQuery = "";
			$c_counter = 0;
			foreach ( $final_array as $ccode => $state_data ) {
				if ($c_counter == 0)
				$locationQuery .= " (country='$ccode' ";
				else
				$locationQuery .= " or (country='$ccode' ";
				if (count ( $state_data ) > 0) {
					$locationQuery .= " and (";
				}
				$s_counter = 0;
				foreach ( $state_data as $state => $city_data ) {
					if ($s_counter == 0)
					$locationQuery .= " (state='$state' ";
					else
					$locationQuery .= " or (state='$state' ";

					if (count ( $city_data ) > 0) {
						$cities = implode ( ",", array_keys ( $city_data ) );
						$locationQuery .= " and city in ($cities) ";
					}
					$s_counter ++;
					$locationQuery .= ")";

					//additional checks if the state is NCR then add more cities
					//and if the city level check is there then add that particular city only

					if (count ( $city_data ) == 0) {
						//if state is in ambigous then add cities separetely as well
						if(in_array($state, $this->ambigousStates)){
							//	//47968 - Greater Noida, 47965 - noida, 47964 - gurgaon, 47967 - ghaziabad, 47966 - faridabad
							//states : 2168 - NCR,  2176- Haryana, 2193 - UP
							if($state == 2168)
							$locationQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesNCR) . "))";
							elseif($state == 2176)
							$locationQuery .= " OR (city in (47964,47966 ))";
							elseif($state == 2193)
							$locationQuery .= " OR (city in (47968, 47965, 47967))";

						}elseif(in_array($state, $this->ambiguousStatesChandigarh)){
							//if punjab/chd is filled in state and city is not filled
							$locationQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesChandigarh) . "))";
						}
					}else{
						//if city is ambigous add city separately
						$cityClash = array_intersect(array_keys($city_data), $this->ambiguousCitiesNCR);
						if(!empty($cityClash)){
							$locationQuery .=  " OR (city in (". implode(',', $cityClash) ."))";

						}
						$cityClashForChd = array_intersect(array_keys($city_data), $this->ambiguousCitiesChandigarh);
						if(!empty($cityClashForChd)){
							$locationQuery .= " OR (city in (" . implode(',', $this->ambiguousCitiesChandigarh) . "))";
						}
					}
				}
				if (count ( $state_data ) > 0) {
					$locationQuery .= " )";
				}
				$c_counter ++;
				$locationQuery .= " ) ";
			}

			$locationQuery_crude .= " and (" . $locationQuery . " )";
			// stay_country | stay_state | stay_city


		}
		else{
			$locationQuery = null;
		}

		//var_dump($locationQuery); die;
		//below variables not in use anymore so commented in case needed in future
		/*$myCityPref = $this->checkissetorEmpty($myCityPref)?null:implode(',', $myCityPref);
		$myStatePref = $this->checkissetorEmpty($myStatePref)?null:implode(',', $myStatePref);
		*/



		//constants for multiplication
		$cityConstant = ($myGender=='M')?MatchEquationMutiplicationFactor::CityMale : MatchEquationMutiplicationFactor::CityFemale;
	//	$locationCase = "case when city = '$myCity'  then 1*$cityConstant else(case when state = '$myState' then 0.5*$cityConstant else 0 end) end ";
//		$locationCase = "case when city = '$myCity' and state = '$myState' then 1*$cityConstant else(case when state = '$myState' then 0.5*$cityConstant else 0 end) end ";
		//user pref null  or city/state same pref and my data


		$locationCase = 0 ;
		
		//if pref is just india or not set then apply location rank
		if ( !isset ($prefLocation) || ( rtrim ($prefLocation, ';') == '113'))
		$locationCase = $this->_getLocationRankCase($myCity, $myState, $cityConstant);

		/**
		 * finding filter for state and city for male profiles
		 */
		$location = '';
	/*	if($myGender=='M'){
			//47968 - Greater Noida, 47965 - noida, 47964 - gurgaon, 47967 - ghaziabad, 47966 - faridabad
			//states : 2168 - NCR,  2176- Haryana, 2193 - UP
			$stateStr = " state = '$myState' ";
			$ambiguousCitiesNCR = array(47965,47968, 47964, 47967,47966 );
			$ambiguousCitiesNCRHaryana = array(47964, 47966);
			$ambiguousCitiesNCRUP = array(47968,47965,47967);
			if(in_array($myCity, $ambiguousCitiesNCR)){
				if(in_array($myCity, $ambiguousCitiesNCRHaryana)){
					$stateStr = " state in (2176,2168)";
				}
				if(in_array($myCity, $ambiguousCitiesNCRUP)){
					$stateStr = " state in (2193,2168)";
				}
			}
			//$statesForambiguousCitiesNCR = array();
			if($prefLocation == null){
				//$myPrefLocation = '113';
				$location = " and (((city = '$myCity' and $stateStr) or $stateStr )) " ;
			}
			elseif( rtrim($prefLocation, ";") == "113"){
				$location = " and (((city = '$myCity' and $stateStr) or $stateStr )) " ;
			}

		}*/
		//$locationCase = $this->checkissetorEmpty($prefLocation)?0:"case when city = '$myCity' then 1*$cityConstant else when state = end ";
		//var_dump($location); die;

		$piBucketsMale = array(".04"=>1, ".08" => 2,".14" => 3, ".24"=>4);

		/*echo $myPi;
		 echo $myGender;*/
		/**
		 * find the actual bucket range for ranking
		 */
		if($myGender == "F"){
			$resultSetLimit = Utils::$recommendation_count_female ;
			$availabilityCase = " 0 ";

			/*	if(($myPi == null)||($myLikes+$myHides)<Utils::$likeHideCountForPIGenerationFemale){
				$myPi = Utils::$defaultBucketConstant;
				}*/
			/*
			 $myBucketRange =  "0.14"; //bucket 3
			 $lowerBucketStart = "0.4";
			 $upperBucketEnd = "0.24";*/
			/*	if($myPi < 0.3){
				$lowerBucketStart = "0";
				$upperBucketEnd = "0.08";
				}
				if ($mypi < 0.45 & $myPi>=0.3){
				$lowerBucketStart = "0";
				$upperBucketEnd = "0.14";
				}
				if($myPi<0.6 & $myPi>=0.45){
				$lowerBucketStart = "0.04";
				$upperBucketEnd = "0.24";
				}
				if($myPi<0.75 & $myPi >=0.6){
				$lowerBucketStart = "0.08";
				$upperBucketEnd = "1";
				}
				if($myPi>=0.75){
				$lowerBucketStart = "0.14";
				$upperBucketEnd = "1";
				}*/

		}
		else{
			$resultSetLimit = Utils::$recommendation_count_male ;
			$piBucketsFemale = array(".3" => 1, ".45" => 2, ".6" => 3, ".75" => 4 );
			//$availabilityCase = " case when ua.slots_filled >= 0 then (50-ua.slots_filled)/50 else 0 end ";
			$availabilityCase = " 0 ";


			/*if(($myPi == null)||($myLikes+$myHides)<Utils::$likeHideCountForPIGenerationMale){
				$myPi =  Utils::$defaultBucketConstant;
				}

				if($myPi < 0.04){
				$lowerBucketStart = "0";
				$upperBucketEnd = "0.45";
				}
				if ($mypi <0.08 & $myPi>=0.04){
				$lowerBucketStart = "0";
				$upperBucketEnd = "0.6";
				}
				if($myPi<0.14 & $myPi>=0.08){
				$lowerBucketStart = "0.3";
				$upperBucketEnd = "0.75";
				}
				if($myPi<0.24 & $myPi >=0.14 ){
				$lowerBucketStart = "0.45";
				$upperBucketEnd = "1";
				}
				if($myPi>=0.24){
				$lowerBucketStart = "0.6";
				$upperBucketEnd = "1";
				}*/

		}



		/*var_dump($this->checkissetorEmpty($mySumA));
		 var_dump($isMyPsychoFilled);die;*/

		//		$defaultPI = ($myGender == "M")? PopularityIndex::defaultFemalePI : PopularityIndex::defaultMalePI;

		$defaultPI =  Utils::$defaultBucketConstant;
		
		$piCase =  '';
		
		if ($this->gender == "M")
		{
			$piCase = $this->_getPIRankForMale($myBucket, "2");
		}
		else 
		{
			$piCase = $this->_getPIRanksAndFiltersForFemale($myBucket, "2");
		}
		
				
		$uninstallCase = $this->_getUninstallRankCase();
		
				
		$activityCase = "case when ai < 0.25 then 0.25 else (case when ai <0.5 then 0.5 else (case when ai<0.75 then 0.75 else 1 end) end ) end ";
		$availabilityStr = $this->_getAvailabilityFilterString($this->gender, $myBucket, $myPi);



		if($myGender == "M"){
			$freshProfileQuery = "select u.user_id,  0 as likeflag, $locationCase as locationScore, pi, bucket, $piCase as piScore, $activityCase as activityScore,  
			$locationCase + $piCase + $activityCase  + $uninstallCase as totalScore,
			$locationCase + $piCase + $activityCase  as score
					 FROM user_search  u  left join user_availability ua on u.user_id = ua.user_id 
					 					  left join user_lastlogin ul on u.user_id = ul.user_id ";	
			
			$likeQuery = "SELECT ul.user1 as user_id from user_like ul join user_search u on u.user_id = ul.user1 where ul.user2 = $this->userId"; 
	
		//$likeQuery = "SELECT ul.user1 as user_id from user_like ul join user u on u.user_id = ul.user1 join user_photo up on up.user_id = u.user_id where  up.is_profile = 'yes' and up.admin_approved = 'yes' and ul.user2 = $this->userId and  u.user_id is not null and u.status not in ('suspended', 'blocked') and (u.email_id is null or  u.email_id not like '%trulymadly.com%') and u.fid not in (100008254001811,100005921989349,100005780296713,100004832236718,100008305415203) ";
		}
		else{

			$freshProfileQuery = "select u.user_id,  0 as likeflag, $locationCase as locationScore, pi, bucket, " . $piCase['piRankCaseFreshQuery']. " as piScore, $activityCase as activityScore,
			$locationCase + ". $piCase['piRankCaseFreshQuery'] ." + $activityCase   as totalScore,
			$locationCase + ". $piCase['piRankCaseFreshQuery'] ." + $activityCase  as score
					 FROM user_search  u ";	

			$likeQuery = "select u.user_id,  case when ul.user1 is not null then 1 else 0 end as likeflag, pi, bucket, $locationCase as locationScore, ". $piCase['piRankCaseLikeQuery'] . " as piScore, $activityCase as activityScore, 
			$locationCase + ". $piCase['piRankCaseLikeQuery'] . " + $activityCase  + case when ul.user1 is not null then 10000 else 0 end as totalScore,
			$locationCase + ". $piCase['piRankCaseLikeQuery'] . " + $activityCase  as score
					 FROM user_search  u  left join user_availability ua on u.user_id = ua.user_id 
					  JOIN user_like ul on u.user_id= ul.user1 and ul.user2 = $user_id ";  

		}
		$baseQuery = 	 "WHERE gender= '$oppositeGender' and age>='$prefStartAge' and age<='$prefEndAge'";
		
		//added as a filtering for males only to get the profiles of max upto same state only
		//$baseQuery .= $location ;
		/*if($this->checkissetorEmpty($myMaritalStatusPref))
		$baseQuery .=" and marital_status in (". $this->addApostrophe($myMaritalStatusPref) . ")";*/


		if($this->checkissetorEmpty($locationQuery))
		$baseQuery .=" and   ( $locationQuery ) ";
			
		$baseQuery  .= " and height>='$prefStartHeight' and height<='$prefEndHeight' ";
			
		/*if ($this->checkissetorEmpty ( $myPrefSmoking )) {
			$baseQuery .= " and smoking_status in (" . $this->addApostrophe ( $myPrefSmoking ) . ")";
		}
		if ($this->checkissetorEmpty ( $myPrefDrinking )) {
			$baseQuery .= " and drinking_status in (" . $this->addApostrophe ( $myPrefDrinking ) . ")";
		}
		if ($this->checkissetorEmpty ( $myPrefFood )) {
			$baseQuery .= " and food_status in (" . $this->addApostrophe ( $myPrefFood ) . ")";
		}*/
			
		//var_dump($myReligionPrefString);
	/*	if($this->checkissetorEmpty($myReligionPref)){
			$baseQuery .= " and religion in (" . $myReligionPrefString . ")";
		}*/
		/*	 var_dump($myReligionPrefString);
		 var_dump($baseQuery); die;
		 */
		/*
		if ($this->checkissetorEmpty ( $prefEducation ))
		$baseQuery .= " and highest_education in (" . $this->addApostrophe ( $prefEducation ) . ")";
		*/	
			
		//TODO: check income string match
		/*if ($this->checkissetorEmpty ( $myPrefIncomeStart )&&$this->checkissetorEmpty($myPrefIncomeEnd))
		$baseQuery .= "  and    $myPrefIncomeStart REGEXP income  and income_end <= $myPrefIncomeEnd  ";
		*/// $baseQuery .="  and (pref_marital_status regexp '[[:<:]]Never Married[[:>:]]' or pref_marital_status is NULL)      and (pref_location is null or pref_location       regexp '[[:<:]]113_2168_16743[[:>:]]' or pref_location             regexp '[[:<:]]113_2168[[:>:]]' or pref_location       regexp '[[:<:]]113[[:>:]]')             and pref_start_height<='70' and pref_end_height>='70'    and (pref_smoking_status regexp '[[:<:]]Never[[:>:]]' or pref_smoking_status is NULL)   and (pref_drinking_status regexp '[[:<:]]Never[[:>:]]' or pref_drinking_status is NULL)     and (pref_food_status regexp '[[:<:]]Non-Vegetarian[[:>:]]' or pref_food_status is NULL)      and (GREATEST(IFNULL(pref_income_start,-100),10)<=LEAST(IFNULL(pref_income_end ,200),20))     and (pref_education regexp '[[:<:]]21[[:>:]]' or pref_education is NULL)      and user_id not in (1129,4095,8895,9410,11898,12567,1,2,34,32,4242,42,42,42,42,3421,532532,52352,5,224,24,23,423,423,423,4,24,24,234,23,42325,25,252,352,532,523,5235,23,523523,52,5252,52,52,52552,52,25,25,25,2,5,2,87,97,979,69,56,53,85,835,8657567,65,75,75735,46,414,2,2353463,46,45645756,7,65765,7,56879,9,7,978,987,9789,4324,23,423,423,4,234,345346,35,46,54,65,7,65,786,867,8,67,867,876,9,789,87,9789,7,93492739,92,749237,49237,482648,84682364,816,48126481641,816481,481,6481,648346,82364823,482346,823648,23,6823823,8,38,3423)        limit 100000       )t order by score limit 100";

		// var_dump($myPrefIncome); die;
		/*if ($this->checkissetorEmpty ( $myPrefIncome ))
		$baseQuery .= "  and   '$myPrefIncome' regexp concat(income_start,'-',income_end) ";
*/

		if($this->checkissetorEmpty($myAge)){
			$baseQuery .= " and pref_start_age<='$myAge' and pref_end_age>='$myAge'";
		}
	/*	if($this->checkissetorEmpty($myReligion)){
			$baseQuery .= " and (pref_religion regexp '[[:<:]]".$myReligion."[[:>:]]' or pref_religion is NULL)";
		}*/
			
		/*if($this->checkissetorEmpty($myMaritalStatus)){
			$baseQuery .= " and (pref_marital_status regexp '[[:<:]]".$myMaritalStatus."[[:>:]]' or pref_marital_status is NULL) ";
		}*/
		$city_query=$myCountry."_".$myState."_".$myCity;
		$state_query=$myCountry."_".$myState;
		$country_query=$myCountry;

		//make state query
		$baseQuery.= " and (pref_location is null or pref_location
					 regexp '[[:<:]]".$city_query."[[:>:]]' or pref_location
					 regexp '[[:<:]]".$state_query."[[:>:]]' or pref_location
					 regexp '[[:<:]]".$country_query."[[:>:]]'";
					 
		if(in_array($myCity, $this->ambiguousCitiesNCR)){
			$baseQuery .= " or pref_location like '%$myCity%' or pref_location regexp '[[:<:]]".$myCountry."_2168[[:>:]]' ";
		}
		if(in_array($myCity, $this->ambiguousCitiesChandigarh)){
			foreach ($this->ambiguousStatesChandigarh as $state){
				$baseQuery .= " or pref_location regexp '[[:<:]]".$myCountry."_".$state."[[:>:]]' ";
				//$baseQuery .= " or pref_location regexp '[[:<:]]".$myCountry."_2172[[:>:]]' or pref_location regexp '[[:<:]]".$myCountry."_2189[[:>:]]' ";
			}
			foreach ($this->ambiguousCitiesChandigarh as $city){
				$baseQuery .= " or pref_location like '%$city%' ";
			}
		}
					 
			$baseQuery .=	" )";
		



		if($this->checkissetorEmpty($myHeight)){
			$baseQuery .= " and pref_start_height<='$myHeight' and pref_end_height>='$myHeight'";
		}
			
		/*if($this->checkissetorEmpty($mySmoking)){
			if($mySmoking == "Never")
			$baseQuery .= " and (pref_smoking_status IN ('Never') or pref_smoking_status is NULL) ";
			else $baseQuery .= " and (pref_smoking_status is NULL)";
		}
		if($this->checkissetorEmpty($myDrinking)){
			if($myDrinking == "Never")
			$baseQuery .= " and (pref_drinking_status IN ('Never') or pref_drinking_status is NULL) ";
			else $baseQuery .= " and (pref_drinking_status is NULL)";

			//$baseQuery .= " and (pref_drinking_status regexp '[[:<:]]".$myDrinking."[[:>:]]' or pref_drinking_status is NULL) ";
		}
		if($this->checkissetorEmpty($myFood)){
			if($myFood == "Vegetarian")
			$baseQuery .= " and (pref_food_status IN ('Vegetarian') or pref_food_status is NULL) ";
			else $baseQuery .=" and ( pref_food_status is NULL )";
		}*/


		//TODO: check income - make it a string search
		/*if($this->checkissetorEmpty($myIncomeStart)&&$this->checkissetorEmpty($myIncomeEnd)){
			//$baseQuery .= " and ((pref_income_start >=$myIncomeStart or  pref_income_start is NULL) and (pref_income_end <=$myIncomeEnd or  pref_income_end is NULL)) ";
			$baseQuery .= " and (pref_income regexp concat($myIncomeStart,'-',$myIncomeEnd) or  pref_income is NULL) ";
		}*/
		/*
		 if($this->checkissetorEmpty($income_end)){
		 $baseQuery .= " and (pref_income_end>='$income_end' or  pref_income_end is NULL)";
		 }*/
		/*	
		if($this->checkissetorEmpty($myEducation)){
			$baseQuery .= " and (pref_education regexp '[[:<:]]".$myEducation."[[:>:]]' or pref_education is NULL) ";
		}
        */

		/*if (strlen ( $user_id_to_fliter ) > 0)
		 $baseQuery .= ' and u.user_id not in (' . $user_id_to_fliter . ')   ' . $availabilityStr;
		 */

		
		
		/**
		 * ~~~~~~~~~~~~~~debugger case start here ~~~~~~~~~~~~~~~~~~
		 * debugger case for no matches only
		 */
		if($debug == true && $myGender == "M")
		{
			global $conn_reporting;

			$result1 = $result2 = $result3 = -1;
			$query1 = $freshProfileQuery . $baseQuery;
			$result1 = Query::SELECT($query1, null);

			if ( count($result1) > 0)
			{
				if(strlen($user_id_to_fliter_total) > 0)
				{
					$query2 = $query1. " and u.user_id not in (" . $user_id_to_fliter_total . ") " ;
					$result2 = Query::SELECT($query2, null);

					$query3 = $query2 . $availabilityStr;
					$result3 = Query::SELECT($query3, null);
				}
				else
				{
					$query3 = $query1 . $availabilityStr;
					$result3 = Query::SELECT($query3, null);
				}

			}
			
			$no_match_res[$user_id] = array(
							"pref_filter" => count($result1),
							"actioned_filter" => ($result2 != -1)?count($result2):null,
							"availability_filter" => ($result3 != -1)?count($result3):null,
							"already_actioned" => count($total_ids)
			);
			
		/*	$loggerQuery = $conn_reporting->Prepare(
												"INSERT INTO user_recommendation_log_report 
												(user_id,two_way_filter, two_way_actioned_filter, two_way_filter_availability, already_actioned, tstamp) 
												values (?,?,?,?,?,now())"
												);

			$conn_reporting->Execute($loggerQuery, array($user_id, $no_match_res[$user_id]['pref_filter'], $no_match_res[$user_id]['actioned_filter'], $no_match_res[$user_id]['availability_filter'], $no_match_res[$user_id]['already_actioned']) );
*/
			//var_dump($no_match_res);
		    return $no_match_res;
		}
		
		
		/**
		 * ~~~~~~~~~~~~~`debugger case end here ~~~~~~~~~~
		 */
		
		
		if(strlen($user_id_to_fliter_total) > 0){
			$freshProfileQuery .= $baseQuery . " and u.user_id not in (" . $user_id_to_fliter_total . ")" ;

			if($myGender == "M"){
			 $freshProfileQuery .= $availabilityStr;
			}

		}else $freshProfileQuery .= $baseQuery . $availabilityStr;


		if($this->gender == "M") $freshProfileQuery .= " and (u.hide_flag is null or u.hide_flag = 0) ";
		
		if($this->gender == "F")
		{
			$freshProfileQuery .= $piCase['piFilterCaseFreshQuery'];
		}
		//if($this->gender == "F" && ($myPi == null || $myPi == -1)) $freshProfileQuery .= " and bucket In (4,5) ";

		$freshProfileQuery .=  "   order by totalScore desc, u.user_id%SECOND(now())  ";


		$notInFilterQuery = '';

		if(strlen($user_id_to_fliter_nonLikedMe) >0 )
		$notInFilterQuery = " and ul.user1 not in (" . $user_id_to_fliter_nonLikedMe . ")";
			
		if($myGender == "M"){
			$likeQuery .= $notInFilterQuery;
		}else{
			$likeQuery .= $baseQuery . $notInFilterQuery  . $piCase['piFilterCaseLikeQuery']. "  order by totalScore desc , u.user_id%SECOND(now())   limit " . Utils::$like_threshold_female;
		}
		// echo $likeQuery; die;
		/*		if(strlen($user_id_to_fliter_nonLikedMe)){
		$likeQuery .= $baseQuery . " and u.user_id not in (" . $user_id_to_fliter_nonLikedMe . ")";
		}else $likeQuery .=$baseQuery;
		*/

		/*if($myGender == "F"){

		}
		else{
		$likeQuery .= "  order by totalScore desc ";
		}*/
//echo $baseQuery; die;

		$result_like =array();
		$result_fresh = array();
		$freshCompeteWithLikes = null;
		$countToFetchFresh = $fetchFreshCount;
		$countToFetchLikes = null;

		try {
			if(!empty($likedMeFilters) && isset($likedMeFilters))
			$result_like = Query::SELECT($likeQuery, null);

			if($myGender == "M"){
				/*$fetchFreshCount = $this->recommendation_number;
				//in case the query comes from edit profile/pref
				if(isset($countAfterEdit)){
					$fetchFreshCount = $countAfterEdit;
				}*/

				$countToFetchFresh = $fetchFreshCount;
				
		    	$freshProfileQuery .=  "  limit $fetchFreshCount";
				
				if($fetchFreshCount > 0)
				$result_fresh = Query::SELECT($freshProfileQuery, null);
				//run query only if count to fetch > 0
				/*if( $countToFetch>0){
				$result_fresh = Query::SELECT($freshProfileQuery, null);
				}*/
			}else{
				$freshCompeteWithLikes = true;
				//$fetchFreshCount = $this->recommendation_number;
				$likeQueryCount = count($result_like);

				//go through this only when it's not a call from non-authentic IOS user
			//	if($nonAuthenticIosCall == false)
				//{
				
				if($likeQueryCount < ($this->totalProfilesForFemale - $fetchFreshCount)){
					$fetchFreshCount = $this->totalProfilesForFemale - $likeQueryCount;
					$freshCompeteWithLikes = false;
				}
				//}

				$countToFetchFresh = $fetchFreshCount;
				//get 50 - likedprofiles as fresh profiles
				//$countToFetch = $this->recommendation_number + (40-$likeQueryCount);
				$freshProfileQuery .= " limit $fetchFreshCount";
				$result_fresh = Query::SELECT($freshProfileQuery, null);

			}

		} catch ( Exception $e ) {
			trigger_error ( $e->getMessage (), E_USER_WARNING );
		}



		$idList = null;
		$result = array ();
		$scoreData = array();
		$likedIds = array();


		$nonLikeCount = 0;
		$result_fresh_set =array();
		$result_like_set = array();
		$scoreData_fresh_set = array();
		$scoreData_like_set = array();
		$location_sort_set = array();
		$availabile_set = array();
		$i=0;
		foreach ( $result_like as $row ) {

			$likedIds[$row['user_id']] = $row['user_id'];
			$result_like_set[$row['user_id']] = $row['score'];
			$scoreData_like_set[$row['user_id']] = array(				'likeFlag' => $row['likeflag'],
																'bucket' => $row['bucket'],
																'pi' =>$row['pi'],
																'location' => $row['locationScore'],
																'piScore' => $row['piScore'],
																'activityScore' => $row['activityScore'],
																'availabilityScore' => $row['availabilityScore'],
																'total' => $row['score']);

			$location_sort_set[$row['user_id']] = $row['locationScore'];
			$idList[$i] = $row['user_id'];
							
			$availabile_set[$row['user_id']] = "0";
			
			if ($this->gender == "F" && $row['piScore']>0 )
			{
				if ($row['bucket'] == 0 || $row['pi'] == -1)
				$availabile_set[$row['user_id']] = "F";
				
				else 
				$availabile_set[$row['user_id']] = "B";
				
			}
			$i++;
		}

		
		foreach ( $result_fresh as $row ) {
			$result_fresh_set[$row['user_id']] = $row['score'];
			$scoreData_fresh_set[$row['user_id']] = array(				'likeFlag' => $row['likeflag'],
																'bucket' => $row['bucket'],
																'pi' =>$row['pi'],
																'location' => $row['locationScore'],
																'piScore' => $row['piScore'],
																'activityScore' => $row['activityScore'],
																'availabilityScore' => $row['availabilityScore'],
																'total' => $row['score']);
			$location_sort_set[$row['user_id']] = $row['locationScore'];

			$idList[$i] = $row['user_id'];
		


			$availabile_set[$row['user_id']] = "0";
			
			if ($this->gender == "M" && in_array($myBucket, array(4,5)))
			{
				$availabile_set[$row['user_id']] = "B";
			}
			else if ($this->gender == "M" && $row['piScore']>0 )
			{
				if ($this->isFreshBucket == true)
				$availabile_set[$row['user_id']] = "F";
				
				else 
				$availabile_set[$row['user_id']] = "B";
				
			}
			$i++;
		}


		//TODO: if diff between availability here and there, then update table
		/*
		* while running this query , a diff might occur during redis flush and query then inconsistent data
		*//*
		var_dump($result_fresh);
		var_dump($result_like);die;*/

		$likeIdCount = count($likedIds);
		// update this count in availability table
		/*if($myGender == "F"){
			//move to where
			$sql1 = "UPDATE user_availability set slots_filled = ?  where user_id = ? and (slots_filled - ?) > 0 ";
			Query::UPDATE($sql1, array($likeIdCount,  $this->userId, $likeIdCount));
		}*/

		//var_dump($scoreData);
		if(count($idList)>0){
			$uu = new UserUtils();
			$mutuals=$uu->setGetMutualConnections($this->userId, $idList);
		}

		//TODO: if action is taken on first 30 which is mix of like and fresh then rest is mix of both again?
		/*$recommendation_set=array_slice ( $result_fresh_set, 0, $countToFetch, true );
		$total_set = $result_like_set + $recommendation_set;*/
	/*	var_dump($result_like_set);
		var_dump($result_fresh_set);
	*/	$total_set = $result_like_set + $result_fresh_set;
		$total_score_set = $scoreData_like_set + $scoreData_fresh_set;
/*var_dump($total_set); 
var_dump($total_score_set); die;*/
		/**
		 * sort by total score the whole set - in case of females and if the like threshold is not met
		 * then shuffle the whole set for everyone
		 */
		$final_set = $total_set;
		if($freshCompeteWithLikes == true){
			arsort($total_set);
			$final_set = $total_set;
			//$final_set = array_slice ( $total_set, 0, Utils::$like_threshold_female, true );
		}


		//do the shuffling only for male users 
		if($this->gender == "M"){
			$shuffled_array = array();
			$ids = array_keys($final_set);
			shuffle($ids);

			foreach ($ids as $key)
			$shuffled_array[$key] = $final_set[$key];

			$final_set = $shuffled_array;
		}



		//slice array only if its a edit call and gender is female; for male - we have changed the fresh query itself
	/*	if(isset($countAfterEdit) && $this->gender == "F"){
			$final_set = array_slice ( $final_set, 0, $countAfterEdit, true );
			$countToFetchFresh = $countAfterEdit;
		}*/
		
		
		//if it's a female find total set irrespective of the call count
/*		if( $this->gender == "F"){
			$final_set = array_slice ( $final_set, 0, $fetchFreshCount, true );
			$countToFetchFresh = $countAfterEdit;
		}*/
		//add to redis
		$this->addMatchesToSet($final_set, $likedIds, $myGender, $availabile_set);
		$this->setExpiryForRedisKeys(  $actionTaken); 


	/*	var_dump($result_like_set);
		var_dump($result_fresh_set); die;
	*/	//add logging here

		$final_score_set = array();
		foreach ($final_set as $key=>$val){
			$final_score_set[$key] = $total_score_set[$key];
		} 
		$this->logIndividualScore($final_score_set);
		$this->storeResults($result_like_set, $likeQuery, 1);
		$this->storeResults($result_fresh_set, $freshProfileQuery, 0, $countToFetchFresh);




		/*
		 $this->storeResults($result_like_set, $likeQuery, 1);
		 $this->storeResults($recommendation_set, $freshProfileQuery, 0, $countToFetch);
		 */
		/*$total_set = array_keys($total_set);
		 //shuffle whole set before adding to redis
		 shuffle($total_set);
		 $location_sort_set_new = array();
		 foreach ($total_set as $val){
			$location_sort_set_new[$val] = $location_sort_set[$val];
			}
			//sorting on the basis of location score
			arsort($location_sort_set_new);
			$total_set = array_keys($location_sort_set_new);
			*///var_dump($total_set); die;
		/*	$this->addMatchesToSet($total_set, $likedIds, $myGender);
		$this->logIndividualScore($total_score_set);
		$this->setExpiryForRedisKeys($actionTaken, $app_version_code);
		*/
	}


	//private function setExpiryForRedisKeys($actionTaken, $app_version_code){
	private function setExpiryForRedisKeys(  $actionTaken){
		//var_dump($actionTaken);die;
		/*if($this->status == "non-authentic" && $app_version_code >= Utils::$appVersionForNonAuthenticActionEnable ){
			return ;
			//	$uu->setKey($this->matchCountKey, $actionTaken);
		}else{*/
		if($this->status == "authentic"){
			$this->uu->setExpiry($this->key, $this->timeForMatchesExpiry);
			$this->uu->setKeyAndExpire($this->matchCountKey, $this->timeForMatchCountExpiry, $actionTaken);
		}
	}

	private function logIndividualScore($data){
		//echo 'ge';
		$countIds = count($data);

		if($countIds > 0){
			$params_arr = array();
			//$insertSql = "INSERT ignore into user_recommendation_rank_log (user1,user2, locationScore,piScore,activityScore,availabilityScore,totalScore,likeFlag,tstamp, pi, bucket)	values  (?,?,?,?,?,?,?,?,now(),?,?)";

			foreach ($data as $key => $val){
				$params_arr[] = array("user1"=>$this->userId, "user2"=>$key, "locationScore"=>$val['location'], 
						"piScore"=>$val['piScore'], "activityScore"=>$val['activityScore'], "availabilityScore"=>$val['availabilityScore'],
						"totalScore"=>$val['total'], "likeFlag"=>$val['likeFlag'], "pi"=>$val['pi'], "bucket"=>$val['bucket']);
			}
			$this->queryLog->rankLogData($params_arr);
			//Query::BulkINSERT($insertSql, $params_arr);

		}


	}

	private function storeResults($resultSet, $query, $isLikeQuery = 0, $countToFetch = null){

		$table = "user_recommendation_query_log";

		if($isLikeQuery == 0) {
			if($countToFetch <=0) $countToFetch =0;
		}
		/*if($tableName == true)
		 $table = $tableName;*/

		$result = json_encode($resultSet);
		//var_dump($result);die;
		//		foreach($resultSet as $key=>$val){
		//$prprdSt ="insert ignore into $table values(?,?,?, now(), ?,?,?)";
		//echo $prprdSt; die;
		//Query::INSERT($prprdSt, array($this->userId, $query, $result, $isLikeQuery, $countToFetch, count($resultSet)));
		//	}

		$data_logging = array();
		$data_logging [] = array( "user_id" => $this->userId , "query" => $query, "result" => $result, "isLikeQuery" => $isLikeQuery, "countToFetch"=>$countToFetch, "countFetched"=>count($resultSet));
		$this->queryLog->queryLogData($data_logging);
		 
	}


	private function addMatchesToSet($user_array, $likedIds, $myGender, $availabile_set) {
		//if gender is female then dont add -1 as first value so that key automatically deletes after all the actions
		$args[0] = $this->key;

		//if($myGender == "M"){
		$args[1] = -1;
		$i=2;
		//		$i++;
		//}

		//shuffle($user_array);
		foreach ($user_array as $key => $val){
			if($myGender == "F"){
				if(isset($likedIds) && in_array($key, $likedIds))
				$args[$i] = $key .'l';
				else
				$args[$i] = $key;
			}
			else{
				$args[$i] = $key;
			}
							
			$args[$i] .= (isset($availabile_set[$key]))? ":$availabile_set[$key]": ":0"; 
			$i++;
			
		}
		//	 print_r($args); die;
		//call_user_func_array(array($this->redis,'SADD'),$args);
		if(isset($args[1]))
		call_user_func_array(array($this->redis,'RPUSH'),$args);
		//die;
		//$this->redis->SADD($this->key,  $user_array);
	}

	/*private function addMatches($user_array) {

	$i=2;
	$args[0] = $this->key;
	//if($existingCount == null){
	//$i++;
	$args[1] = -1;
	//}
	//if it's a new user, insert -1 as sentinel
	foreach ($user_array as $key=>$val){
	$args[$i] = $key;
	$i++;
	}
	call_user_func_array(array($this->redis,'RPUSH'),$args);
	//$this->redis->SET($this->matchCountKey, 0);
	}*/
}

/* 
$g = new generateNewMatches(6679);
 $g->generateMatchesUsingMemoryTable(6679, 0, true);*/
 

//$result = $gen->fetchResults();
//var_dump($result);
?>
