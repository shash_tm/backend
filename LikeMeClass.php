<?php
require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname(__FILE__)."/UserActions.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
include dirname(__FILE__).'/include/config.php';


/*
 * this file depicts how to store the chat data into the system retrieve the latest chat content from redis and throwing the same on UI @Himanshu
 */
class LikeMeClass extends TMObject{


	private $user_id;

	function __construct($user_id){
		parent::__construct($user_id);
		$this->user_id=$user_id;
	}


	public function deleteNotificationCounter(){
		$key= "u:n:$this->user_id";
		$this->redis->del($key);
	}
	//if someone sends a msg it'll be added to receiver and sender's set of most recent comm
	public function showLikes($page_id=0, $userStatus){
		//	$base_url = $GLOBALS["baseurl"];
		$output=array();
		$lower_range=$page_id*10;
		$upper_range=($page_id+1)*10+9;
		$sql=$this->conn->Prepare("select user2,timestamp from user_likeMe_rejectMe where user1=? order by timestamp desc");
		$rs=$this->conn->Execute($sql,array($this->user_id, $this->user_id));

		if($rs->RowCount()==0)
		return $output;

		$data=$rs->getRows();
		$output=$this->getData($data, $userStatus);

		//var_dump($output);
		return $output;
	}

	private function getData($data, $userStatus){

		$ids=array();
		foreach($data as $val){
			$ids[]=$val['user2'];
		}

		$UserActions=new UserActions();
		$UserUtils=new UserUtils();
		$user_actions=$UserActions->getUserActionsDone($this->user_id, $ids);
		//$attributes=$UserUtils->getAttributes("likeMe", $ids);
		$attributes=$UserUtils->getAttributes("likeMe", $ids, $this->user_id, $user_actions);

		//echo "<pre>";
		//var_dump($user_actions);
		//var_dump($attributes);
		$i=0;
		foreach($data as $val){
			$reject_link = '';
			$like_link = '';
			$user_id2=$val['user2'];
			$user_action=$user_actions[$user_id2];



			$status = null; 
			
			if(in_array(UserActionConstantValues::reject,$user_action)==true)
			$status = 'reject';
			else if(in_array(UserActionConstantValues::rejectMe,$user_action)==true)
			$status = 'rejectMe';
			else if(in_array(UserActionConstantValues::like,$user_action)==true){
				$sql = $this->conn->Prepare("SELECT * FROM user_like where ((user1 =? AND user2 =?) OR (user1=? AND user2=?)) order by timestamp");
				$res = $this->conn->Execute($sql, array($this->user_id, $user_id2, $user_id2, $this->user_id));
				$rows = $res->GetRows();
			//	var_dump($rows); 
			if($rows[0]['user1'] == $this->user_id){
			$status="likedBack";	
			}	
			else $status = "likedBoth";
			//echo $status;
			}
			
			else if($userStatus=='authentic') {
				$status=null;
				$query_reject=$UserActions->generateQuery(UserActionConstantValues::reject,$this->user_id, $user_id2);
				$reject_link="$this->baseurl/ua.php?$query_reject";
					
				//	echo $reject_link;
				$query_like=$UserActions->generateQuery(UserActionConstantValues::like,$this->user_id, $user_id2);
				$like_link="$this->baseurl/ua.php?$query_like";
					
			}


			$str = $this->user_id .'|' . $user_id2;
			$mid = Utils::calculateCheckSum($str);

			$output[$i]=$attributes[$user_id2];
			$output[$i]["timestamp"]=Utils::GMTTOIST($val['timestamp']);
			$output[$i]["status"]=$status;
			$output[$i]['link_reject'] = $reject_link;
			$output[$i]['link_like'] = $like_link;
			$output[$i]["profile_link"]=$this->baseurl."/profile.php?pid=$mid".'_'.$user_id2;
			if($status=="likedBoth" || $status=="likedBack"){
				$output[$i]["txtmsg"] = "Awesome! You've both liked each other.";
			}else if($status=="reject"){
				$output[$i]["txtmsg"] = "has been declined by you.";
			}else if($status=="rejectMe"){
				$output[$i]["txtmsg"] = "will like to pass on your interest.";
			}else{
				$output[$i]["txtmsg"] = "likes your profile.";
			}
			$i++;
		}

		//	var_dump($output);
		return $output;
	}

	public function showLikeQueue(){
		//var_dump($this->user_id);
		$key="lq:$this->user_id";
		//var_dump($key);
		$type=$this->redis->zSize($key);
		//var_dump($type);

		$data=$this->redis->zRevRangeByScore($key, '+inf', 0, array("withscores" => TRUE));
		if(count($data)==0)return;

		$data_format=array();
		foreach($data as $key=>$value){
			$data_format[]=array("user2"=>$key,"timestamp"=>date("Y-m-d H:i:s",$value));
		}

		//	var_dump($data_format);
		$output=$this->getData($data_format);
		//var_dump($output);
		return $output;
	}
}


?>