<?php
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ )  . "/include/config_admin.php";
//require_once dirname ( __FILE__ )  . "/../../DBO/messagesDBO.php";
require_once dirname ( __FILE__ ) . "/msg/MessageFullConversation.class.php";
require_once dirname ( __FILE__ ) . "/Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . "/mobile_utilities/pushNotification.php";
 

class NewCouponMessage
{
        private $conn_reporting;
        private $user_id ;
        private $messageFullObj ;
        private $message ;
        private $misstm ;
        private $gender ;
        private $notify;
        private $uu ;
        private $messageId ;
        private $city;
      //  private $type;
        function __construct()
        {
                global $conn_reporting, $miss_tm_id ;
           // $this->type = $type;
            $this->conn_reporting = $conn_reporting ;
            $this->user_id ;
            $this->messageFullObj ;
            $this->message ;
            $this->imageMessage;
            $this->misstm= $miss_tm_id;
            $this->gender ;
            $this->notify = new pushNotification();
            $this->uu = new UserUtils();
            $this->messageId;
            $this->city='';
        }

        private function sendMessage()
        {
                $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
            $this->messageId=  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
              //  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

        }
       private function sendImageMessage()
        {
                $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
                $this->messageId =  $this->messageFullObj->storeMostRecentChat ( $this->imageMessage, 'JSON', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
              //  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

        }

        private function getMessage()
        {
          /*if($this->type == 'mingle')
            $this->message="Guys! Here's an exciting way to meet other like-minded peeps. Unleash your inhibitions for a fun night of dancing, drinking, mingling, and good fun! Book your slot now";
          else*/
              //$this->message="Look at us on Fighting Fame! Let your tee do the talking. Use code TM20 and get 20% off. Shop the UnSingle look now: http://bit.ly/TrulyMadlyxFF";


        }

        public function getImageAndMessageByCity()
        {
            global $cdnurl;
            $image_url='';
            $message_text='';
            $event_name='';
            $landing_url='';
            $index=-1;
            $prefix='/images/notifications/';
            if($this->city=='Delhi') {
                $message_text="Hey guys! Get ready to discover people to go on a laughing spree with you 'cos Laughya is back for an exciting night of comedy and fun. Click to know more: https://trulymad.ly/evnt.php?e=891_18812";
                $landing_url='https://trulymad.ly/evnt.php?e=891_18812';
                $event_name='HT City Laughya with Vipul Goyal Live';
                $image_url="Misstm_Delhi_Event.jpg";
                $index=0;
            }
            else if($this->city=='Bengaluru') {
                $message_text="Namma Bengaluru! The world's biggest pop-culture event is back and promises to be a blast! Find your cosplay partner and get ready to meet like-minded peeps, your comic book heroes and enjoy some crazy games and activities.Book your slot here:  https://trulymad.ly/evnt.php?e=854_49773";
                $landing_url='https://trulymad.ly/evnt.php?e=854_49773';
                $index=1;
                $event_name='Alto Bengaluru Comic Con';
                $image_url="Misstm_Bengaluru_Event.jpg";
            }else if($this->city=='Mumbai') {
                $message_text="You can still book a pass and catch performances by Coldplay, JayZ, AR Rahman, Farhan Akhtar and many more. Go, go go!:  https://trulymad.ly/evnt.php?e=893_90933";
                $event_name='Global Citizen India';
                $image_url="Misstm_Mumbai_Event.jpg";
                $index=2;
                $landing_url='https://trulymad.ly/evnt.php?e=893_90933';
            }else if($this->city=='Hyderabad'){
                $message_text="Daniel Fernandes is in town to tickle your funny bone! Plus, you can discover like minded peeps interested in stand up comedy. Win win! Book your slots here:  https://trulymad.ly/evnt.php?e=886_26803";
                $event_name='Comedy at its best feat. Daniel Fernandes';
                $index=3;
                $image_url="Misstm_Pune_Event.jpg";
                $landing_url='https://trulymad.ly/evnt.php?e=886_26803';
            }else if($this->city=='Pune'){
                $message_text="Discover other like minded peeps who are also interested in stand up comedy and get ready to UNSingle! Book your slots here:  https://trulymad.ly/evnt.php?e=888_69020";
                $event_name='Comedy at its best feat. Aisi Taisi Democracy';
                $index=4;
                $image_url="Misstm_Pune_Event.jpg";
                $landing_url='https://trulymad.ly/evnt.php?e=888_69020';
            }
            $image_url=$cdnurl.$prefix.$image_url;
            $this->message=$message_text;
            if($image_url!=''&&$message_text!=''){
                return array("image_url"=>$image_url,"message_text"=>$message_text,"event_name"=>$event_name,"landing_url"=>$landing_url,"index"=>$index);
            }else{
                return null;
            }
        }

        private function getImageMessage()
        {
            global $cdnurl, $baseurl ;
            //-------------Changes by Raghav-------------------START---
            $info=$this->getImageAndMessageByCity();
            $index=$info['index'];
            $image_url=$info['image_url'];
            if($info!=null) {
                $this->imageMessage = array("msg"=>$info['message_text'],
                    "metadata" => array("link_landing_url" => $info['landing_url'],
                        "message_type" => "IMAGE_LINK",
                        "event_status"=> "Message_from_miss_By_Lavi_$index",
                        "link_image_url" =>$image_url),
                    "message_type" => "IMAGE_LINK");

            }else{
                return null;
            }
            $this->imageMessage= json_encode($this->imageMessage);
            return $this->imageMessage;
            //---------------Changes by raghav-----------------END-----
//            if($this->type == 'mingle')
//            {
         /*              $this->imageMessage = array( "msg"=>"We have a special surprise for you!",
                               "metadata" => array("link_landing_url" => "http://bit.ly/2ckTVuq",
                                                        "message_type" => "IMAGE_LINK",
                                                            "event_status"=> "Message_from_miss_By_Shirin",
                                                        "link_image_url" =>$cdnurl."/images/notifications/MissTM1.png"),
                                    "message_type" => "IMAGE_LINK");*/


            $this->imageMessage = array( "msg"=>"If a picture can say 1000 words, imagine what a video can do!
Introducing Video profiles on TrulyMadly. Record or upload any 5-15 seconds video on your profile and let the magic unfold. Update the app and edit your gallery NOW!",
                "metadata" => array("message_type" => "TEXT",
                    "event_status"=> "Message_from_miss_tm_for_video_profiles"),
                "message_type" => "TEXT");
           /* }
            else if ($this->type == 'game')
            {
                $this->imageMessage = array( "msg"=>"Hey guys! Who's game for game night? From Bumbling Beer Pong to Twisting Taboo, we've a massive collection of games that'll keep everyone intrigued. Book your slot now",
                    "metadata" => array("link_landing_url" => "https://trulymad.ly/evnt.php?e=675_38816",
                        "message_type" => "IMAGE_LINK",
                        "link_image_url" =>$cdnurl."/images/notifications/game.jpg"),
                    "message_type" => "IMAGE_LINK");
            }
            else
            {
                echo "No type defined";
                die ;
            }*/
            $this->imageMessage= json_encode($this->imageMessage);
          //  echo $this->imageMessage;

            //Hello1
//            $this->imageMessage = array("msg"=> "You've received an image!",
//                                        "metadata" => array("message_type" => "IMAGE",
//                                                            "urls" => array("jpeg_size" => "18KB",
//                                                                "webp"=>"https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064.webp",
//                                                                "jpeg"=>"https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064.jpeg",
//                                                                "thumbnail_jpeg"=> "https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064_1391366929.jpg",
// 			                                                    "thumnnail_webp" => "https://s3-ap-southeast-1.amazonaws.com/devtrulymadly/files/images/profiles/msgs/1464687562150_527619294822216064_1391366929.webp",
// 			                                                    "webp_size"=> "14KB"
//                                                                ))) ;
   //         $this->imageMessage= json_encode($this->imageMessage) ;
        }


        private function createMessageConvoObj()
        {
                $this->messageFullObj = new MessageFullConversation ( $this->misstm, $this->user_id );
        }
    private function sendPushNotification()
    {
        $message_url = $this->uu->generateMessageLink($this->user_id,$this->misstm);
        $info=$this->getImageAndMessageByCity();
        $index=$info['index'];//

        $push_arr =  array("is_admin_set"=> 0,
            "content_text"=>"Find your next date on TM Scenes",
                            "ticker_text"=>"Message From Miss TM",
                            "title_text"=>"TrulyMadly",
                            "message_url"=>$message_url,
                            "match_id"=>$this->user_id,
                             "msg_type" => "TEXT",
                             "msg_id"=> $this->messageId,
                             "push_type"=>"MESSAGE",
                           "event_status"=> "Message_from_miss_tm_lavi_$index");

        $this->notify->notify($this->user_id, $push_arr, $this->misstm);
    }

    private function updateConvoTips()
    {
        $sql = "update user_convo_tips set last_sent_tstamp =now() where user_id = ?";
        Query_Wrapper::UPDATE($sql,array($this->user_id));
    }

        public function getUsersAndSend($target)
        {
            if($target == 'all') {
                $sql="select u.user_id, u.fname, gc.display_name as city,gc.state_id as state from user u
                join user_app_status uas on uas.user_id=u.user_id
                join user_lastlogin ull on u.user_id = ull.user_id
                join user_data ud on u.user_id = ud.user_id
                join geo_city gc on gc.city_id = ud.stay_city and gc.state_id = ud.stay_state
                where u.status ='authentic' and uas.android_app= 'install' and gc.city_id in (16743,47965,47964,47968,47967,47966,4058,4062,6453,6457)";
            }else {
                $sql="select u.user_id, u.fname, gc.display_name as city,gc.state_id as state from user u
                join user_app_status uas on uas.user_id=u.user_id
                join user_lastlogin ull on u.user_id = ull.user_id
                join user_data ud on u.user_id = ud.user_id
                join geo_city gc on gc.city_id = ud.stay_city and gc.state_id = ud.stay_state
                where u.status ='authentic' and uas.android_app= 'install' and gc.city_id in (16743,47965,47964,47968,47967,47966,4058,4062,6453,6457) and u.user_id = $target ";
            }

            $stmt=$this->conn_reporting->Prepare($sql);
            $exe=$this->conn_reporting->Execute($stmt,array());
            $i=0;
            $chunkCounter = 0;
            while($row=$exe->fetchRow())
            {
                $this->gender = $row['gender'] ;
                $this->user_id = $row['user_id'] ;
                $this->createMessageConvoObj();
                if($row['city']=='Noida'||$row['city']=='Gurgaon'||$row['city']=='Faridabad'||$row['city']=='Ghaziabad'||$row['city']=='Greater Noida') {
                    $this->city='Delhi';
                }
                else{
                    $this->city=$row['city'];
                }

                $obj=$this->getImageMessage();
                if($obj!=null) {
                    $this->sendMessage();
                    $this->sendImageMessage();
                    $this->sendPushNotification();

                    //$this->updateConvoTips();
                    $i++;
                    $chunkCounter++;
                    if ($chunkCounter >= 1000) {
                        sleep(100);
                        echo "sleeping for 100 seconds";
                        $chunkCounter = 0;
                    }
                }
            }
            echo "Messages sent to $i people" ;
            //var_dump($this->message) ;

        }

}

 
try
{
        if (php_sapi_name() == 'cli')
        {
             $target = $argv[1];
//            $type = $argv[2];
             $message_obj = New NewCouponMessage() ;
            if(isset($target) && $target != null)
             $message_obj->getUsersAndSend($target);
            else
                echo " NO target";
        }

}
catch (Exception $e)
{
        echo $e->getMessage();
}

?>
