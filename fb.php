<?php
//include_once (dirname ( __FILE__ ) . '/include/config.php');
require_once dirname ( __FILE__ ) . "/userQueue.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once (dirname ( __FILE__ ) . '/interesthobbies/Interesthobbies.php');
require_once (dirname (__FILE__ ) . '/photomodule/Photo.class.php');
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php"; 
require_once (dirname ( __FILE__ ) . '/include/Utils.php');
require_once dirname ( __FILE__ ) . "/include/config.php";
//require_once dirname ( __FILE__ ) . "/photo_fb.php";
require_once dirname ( __FILE__ ) . "/abstraction/query_wrapper.php";
require_once (dirname ( __FILE__ ) . '/sdk/facebook-sdk-v5/autoload.php');
require_once (dirname ( __FILE__ ) . '/DBO/facebook_likes/FacebookPageDBO.php');

class fb {
	private $facebook = '', $id = '', $token = '';
	public $data = '';
	private $conn;
	private $usrQ;
	private $limit = 5000;
	private $profile_new = '';
	private $offset = 5000;
	private $redis;
	private $default_graph_version = "v2.6";
	private $app_id;
	private $app_secret;
	public $ip_country;
	
	// sets up facebook object for the current facebook logged in user
	function __construct($token) {
		global $conn,$redis;
		$this->usrQ = new userQueue ();
		$this->conn = $conn;
		$this->redis = $redis;
		$this->token = $token;
		global $config;
  		$this->app_id = $config ['facebook'] ['api'];
  		$this->app_secret = $config ['facebook'] ['secret'];
//	    $api = "1442995922596983";
//	    $secret = "6ec296dca3fc202f9339178d839bbf0f";
		$configFb = array ();
		$configFb ['app_id'] = $this->app_id;
		$configFb ['app_secret'] = $this->app_secret;
		$configFb ['default_graph_version'] = $this->default_graph_version;
		$fid_data=array(); 
		$this->facebook = new Facebook\Facebook($configFb);
		$this->facebook->setDefaultAccessToken($this->token);
	}

	//returns facebook login email id of user
	public function getEmail() {
		return $this->data ['email'];
	}

	public function getRelationshipStatus() {
		return $this->data['relationship_status'];

	}
	
	public function getFirstName(){
		return $this->data['first_name'];
	}
	public function getLastName(){
		return $this->data['last_name'];
	}
	public function getGender(){
		return $this->data['gender'];
	}
	public function getBirthday(){
		return $this->data['birthday'];
	}

	
	public function testfids($fid){
		$fids = array("100008254001811","100005921989349","100005780296713","100004832236718","100008305415203");
		if(in_array($fid,$fids)){
			return true;
		}else
			return false;
	}
	// Trulio verification of a new facebook account
	public function validateTrulio() {
		global $conn, $config;
		$trulio_key = $config ['trulio'] ['key'];
		$url="https://api.trulioo.com/v2/truDetect?apiKey=$trulio_key&provider=fb&token=$this->token";
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$verification = curl_exec($ch);
		$verification = json_decode ( $verification );
		
		$query="insert into log_trulio (fid,token,confidence,reliability,transactionId) values(?,?,?,?,?)";
		$param_array=array($this->id,$this->token,$verification->confidence,$verification->reliability,$verification->transactionId);
		$tablename="log_trulio";
		Query_Wrapper::INSERT($query, $param_array, $tablename);
		/*$conn->Execute($conn->prepare("insert into log_trulio (fid,token,confidence,reliability,transactionId) values(?,?,?,?,?)"),array($this->id,$this->token,$verification->confidence,$verification->reliability,$verification->transactionId));*/

		$rs = $conn->Execute ( $conn->prepare ( "select value from config where `key`=?" ), array (
				'confidence'
		));

		$rs = $rs->GetRows ();
		$confidenceThreshHold = $rs[0]['value'];
		if ($verification->confidence>=$confidenceThreshHold)
			return true;
		return false;
	}

	// retrieves facebook data
	public function getData() {
		$calculateConn = true;
		$currTime = time();
		$profile = $this->make_api_request('/me?fields=friends.fields(picture,first_name,last_name,id,email,gender).limit('.$this->limit.'),likes.fields(id,name,category,created_time).limit('.$this->limit.'),albums.fields(id,name).limit('.$this->limit.'),email,first_name,last_name,gender,birthday,relationship_status,music.listens.limit(100),music.playlists.limit(100),books.quotes.limit(100),books.rates.limit(100),books.wants_to_read.limit(100),books.reads.limit(100),places.saves.limit(100),video.wants_to_watch.limit(100),video.rates.limit(100),video.watches.limit(100),education,work,location');
		$currTime1 = time();
		$diff = $currTime1-$currTime;
		$this->logAction("fb_data",$diff);
		
		if(!isset($profile['friends'])) {
			$calculateConn = false;
			$this->logAction("fb_friends_list_not_return",0);
		}
		//$profile = $this->facebook->api ('/me?fields=friends.fields(picture,first_name,last_name,id,email).limit(5000),likes.limit(5000),albums.fields(id,name).limit(5000),email,gender,birthday,relationship_status,music.listens.limit(100),music.playlists.limit(100),books.quotes.limit(100),books.rates.limit(100),books.wants_to_read.limit(100),books.reads.limit(100),places.saves.limit(100),video.wants_to_watch.limit(100),video.rates.limit(100),video.watches.limit(100),education,work,location', 'GET' );
	//print_r($profile);die;
		if(isset($profile['error_code'])) {
			$this->logFacebook(NULL,$profile['error_msg']);
			$this->limit = $this->limit-2000;
			$this->offset = $this->limit;
			/*
			 * if facebook data is large then split up the calls
			 */
			$this->getFriendsData();
			$this->getOthersData();
			$this->profile_new['likes']=$this->getLikes($this->profile_new);
			$this->profile_new['connections']=$this->getConnection($this->profile_new);
			$this->data = $this->profile_new;
			$this->id = $this->data['id'];
		}
		else {
			$profile['likes']=$this->getLikes($profile);
			if($calculateConn) {
				$profile['connections']=$this->getConnection($profile);
			}else {
				$profile['connections'] = NULL;
			}
			$this->data = $profile; 
			$this->id = $this->data['id'];
		}
		
//		print_r($this->data);die;
	}
	
	
	/*
	 * 
	 * for fetchng the friends data only
	 * 
	 */
	private function getFriendsData($after = false) {
		if($after) {
			$request = '/me/friends?fields=picture,first_name,last_name,id,email,gender&limit=';
			$request = $request.$this->limit;
			$request = $request.'&offset='.$this->offset.'&__after_id=';
			$request = $request.$this->afterId;
			
			$profile = $this->make_api_request($request);
			
			$this->profile_new['friends']['data'] = array_merge($this->profile_new['friends']['data'],$profile['data']);
			
			if(isset($profile['paging']['next'])) {
			$after_id = explode('__after_id=',$profile['paging']['next']) ;
					$this->afterId = $after_id[1];
					$this->offset = $this->offset+$this->limit;
					self::getFriendsData(true);
			}
			}else {
				$profile = $this->make_api_request('/me?fields=friends.fields(picture,first_name,last_name,id,email,gender).limit('.$this->limit.')');
			
				$this->profile_new = $profile;
			
				if(isset($profile['friends']['paging']['next'])) {
					$after_id = explode('__after_id=',$profile['friends']['paging']['next']) ;
					$this->afterId = $after_id[1];
					self::getFriendsData(true);
				}			
			}
	}
	
	public function getOthersData() {
		$profile = $this->make_api_request('/me?fields=likes.limit('.$this->limit.'),albums.fields(id,name).limit('.$this->limit.'),email,first_name,last_name,gender,birthday,relationship_status,music.listens.limit(100),music.playlists.limit(100),books.quotes.limit(100),books.rates.limit(100),books.wants_to_read.limit(100),books.reads.limit(100),places.saves.limit(100),video.wants_to_watch.limit(100),video.rates.limit(100),video.watches.limit(100),education,work,location');
		$this->profile_new = array_merge($profile,$this->profile_new);
	}
	
/**
 * fetch reimport data only from fb
 * @Himanshu
 */
	public function getReimportData(){
		$new_data = $this->make_api_request('/me?fields=first_name,last_name,birthday');
		$this->data = $new_data;
		return $new_data;
	}
	
	public function getBasicData(){
		$currTime = time();
		
		$profile = $this->make_api_request('/me?fields=id,email');
 		
		if(!isset($profile['email'])) {
			$this->logAction("fb_email_not_return",0);
		}
		$currTime1 = time();
 		$diff = $currTime1-$currTime;
 		$this->logAction("fb_basic",$diff);
 		
 		//exit;
 		//$profile = $this->facebook->api ('/me?fields=id');
		$this->data = $profile;
		$this->id = $this->data['id'];
	}


	public function getConnection($data){
		$friends = $data['friends'];
		//$total = count($friends['data']);
		$total = $friends['summary']['total_count'];
// 		if($total<20) {
// 			$total = $this->getFBConnection();
// 		}
		
		if($total<20) {
			$this->logAction("fb_less_friends",$total);
		}

		return $total;
	}
	
	public function getFBConnection() {
		$profile = $this->make_api_request('/me?fields=friends');
		$friends = $profile['friends'];
		return $friends['summary']['total_count'];
	}

	// /store facebook data for a new user in facebook table
	public function storeFacebook($user_id,$action,$from='login') {
		global $conn;
		if($action=='NEW') {
			
			$currTime = time();
			
			$albums=$this->data['albums'];
			foreach($albums['data'] as $album) {
				if($album['name']=='Profile Pictures')  {
					$currTime = time();
					$picture['photo'] = $this->make_api_request("/" . $album['id']."/photos?fields=images,id,likes.limit(100)");
					//print_r($picture['photo']);
					$height=0;
					$width=0;
					$arr_with_likes = array();
					$source = NULL;
					for($i=0;$i<count($picture['photo']['data']);$i++){
						foreach($picture['photo']['data'][$i]['images'] as $image){
							if($image['height']>200  && $image['width']>200){
								$source = $image['source'];
								break;
							}

						}
						if(isset($source)) {
							$arr_with_likes[$picture['photo']['data'][$i]['id']] = array("image"=>$source,"likes"=>count($picture['photo']['data'][$i]['likes']['data']));
							$source = NULL;
						}

					}
					usort($arr_with_likes,array(__CLASS__, 'sortByKeyCallback'));

					/*      foreach($picture['photo']['data'][0]['images'] as $image) {
                                    if($image['height']>$height&&$image['width']>$width) {
                                            $height=$image['height'];
                                            $width=$image['width'];
                                            $source=$image['source'];
                                    }
                            }

                            if(!($width<300 || $height<300))
                                    $this->data['photo']=$source;
                            break;
                            */

					if($from=='login'){
						$count = 6;
						$like_count=count($arr_with_likes);
						if($count>$like_count){$count=$like_count;}
					}else{/* if($from == 'trustbuilder'){*/
						$count = 1;
					}


					for($i=0;$i<$count;$i++){
						$photo[$i] = $arr_with_likes[$i];
					}

					$this->data['photo']= $photo; 
				}
			}
			try {
				$photomodule = new Photo($user_id);
				$totalFriends= $this->data['connections'];
				$photo_fb=$this->data['photo'];
				$email = $this->data['email'];
				$allLikes = isset($this->data['likes'])?json_encode($this->data['likes']):NULL;
				$allFriends = isset($this->data['friends']['data'])?json_encode($this->data['friends']['data']):NULL;
				
				$education = isset($this->data['education'])?json_encode($this->data['education']):NULL;
				$work_details = isset($this->data['work'])?json_encode($this->data['work']):NULL;
				$location = isset($this->data['location'])?json_encode($this->data['location']):NULL;
				$fbPhoto = isset($photo_fb)?json_encode($photo_fb):NULL;
				
				if($from == 'login_email'){
					$connectedFrom = 'login';
				}else {
					$connectedFrom = $from;
				}

				$query="insert into user_facebook (time_of_registration,user_id,fid,fb_token,profile_url,likes,connection,email,fname,lname,gender,relationship,birthday,connected_from,education,work_details,location)
				values(Now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE fid=?,fb_token=?, profile_url=? ";
				$param_array=array($user_id,$this->id,$this->token,$fbPhoto,$allLikes,$totalFriends,$email,$this->data['first_name'],$this->data['last_name'],$this->data['gender'],$this->data['relationship_status'],$this->data['birthday'],$connectedFrom,$education,$work_details,$location,$this->id,$this->token,$fbPhoto);
				$tablename="user_facebook";
				Query_Wrapper::INSERT($query, $param_array, $tablename);
				
				/*$conn->Execute($conn->prepare("insert into user_facebook (time_of_registration,user_id,fid,fb_token,profile_url,likes,connection,email,fname,lname,gender,relationship,birthday,connected_from,education,work_details,location) values(Now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"),array($user_id,$this->id,$this->token,$fbPhoto,$allLikes,$totalFriends,$email,$this->data['first_name'],$this->data['last_name'],$this->data['gender'],$this->data['relationship_status'],$this->data['birthday'],$connectedFrom,$education,$work_details,$location));*/
				/*
				 * store facebook default pic into user_photo with 1 profile pic
				 * rajesh
				 */
				
				if($from == 'login') {
					$headers = Utils::getAllSystemHeaderFields();
					$versionCode = $headers['app_version_code'];
					$source = $headers['source'];
					if( (($source == 'iOSApp' || $source == 'ios_app') && $versionCode > 121 ) || ($source == 'androidApp' && $versionCode >= 100) || ($source == 'windows_app' && $versionCode >= 1305)) {
						$this->fillUserData($user_id);
					}
				}
				$pic_exits = $photomodule->profilePicExists($user_id);
				$pic_exits = (isset($pic_exits))?$pic_exits:false;

				/*$temp_photo_fb=new temp_fb_photo($user_id);*/
				
				/*if(($from=='login' || ($from=='trustbuilder' && !$pic_exits)) && isset($photo_fb)){
					$temp_photo_fb->store_fb_url($photo_fb);
					$temp_photo_fb->pub_queue();
				}*/
				try {
					$gearman_client=new GearmanClient();
					$res_photo=$gearman_client->addServer('127.0.0.1',4730);
					$payload=array();
					$gearman_str=array();
					$payload[0]=$user_id;
					if ($from=='login' || ($from=='trustbuilder' && !$pic_exists)) {
						for($i=0;$i<count($photo_fb);$i++)
						{
							$payload[1]=$photo_fb[$i]['image'];
							if($i==0)
							{
								$payload[2]=1;
							}
							else
								$payload[2]=0;
							$payload[3]= $photo_fb[$i]['likes'];
							$payload2=json_encode($payload);
							$gearman_client->addTaskBackground("download_photo",$payload2);
							$gearman_str[$i]=$payload[0].':'.$payload[1].':'.$payload[2].':'.$payload[3];
						}
						for($i=0;$i<count($photo_fb);$i++)
						{
							$this->redis->sAdd(Utils::$redis_keys['gearman_key'],$gearman_str[$i]);
						}
						$result_photo=$gearman_client->runTasks();
					}
					
				} catch (Exception $e) {
					$msg = "Client Connection for gearman script failed";
					trigger_error ( "PHP Fatal error WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
				}
				
				/*
				if($from=='login' || ($from=='trustbuilder' && !$pic_exits)){
					for($i=0;$i<count($photo_fb);$i++){
						if($i==0){
							$photomodule->addFromComputer(false,$photo_fb[$i],true,'yes');
						}else
							$photomodule->addFromComputer(false,$photo_fb[$i],true);
					}
				}*/
				
				$currTime1 = time();
				
				$diff = $currTime1-$currTime;
				$this->logAction("fb_photo",$diff); 
				
				//log fid to the name
			//	$this->saveFacebookIds();
				$this->logFacebook($user_id,json_encode($this->timeTaken));
				$rs = $conn->Execute("SELECT * from user_facebook_connections where user_id='".$user_id."'");
				if($rs->_numOfRows == 0){
					
					$query="INSERT INTO user_facebook_connections (user_id,connections) VALUES (?,?)";
					$param_array=array($user_id,$allFriends);
					$tablename="user_facebook_connections";
					Query_Wrapper::INSERT($query, $param_array, $tablename);
						
					/*$conn->Execute($conn->prepare("INSERT INTO user_facebook_connections (user_id,connections) VALUES (?,?)"),array($user_id,$allFriends));*/
				}else{
					$query="UPDATE user_facebook_connections SET connections = ? where user_id = ?";
					$param_array=array($allFriends,$user_id);
					$tablename="user_facebook_connections";
					Query_Wrapper::UPDATE($query, $param_array, $tablename);
						
					/*$conn->Execute($conn->prepare("UPDATE user_facebook_connections SET connections = ? where user_id = ?"),array($allFriends,$user_id));*/
				}
				
				$this->storeInterestHobbies($user_id);
			}
			catch(Exception $e) {
				trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
				die;

			}
		}
		else  if($action=='update') {
			$totalFriends= $this->data['connections'];
			$allLikes = isset($this->data['likes'])?json_encode($this->data['likes']):NULL;
			$allFriends = isset($this->data['friends']['data'])?json_encode($this->data['friends']['data']):NULL;
			$education = isset($this->data['education'])?json_encode($this->data['education']):NULL;
			$work_details = isset($this->data['work'])?json_encode($this->data['work']):NULL;
			$location = isset($this->data['location'])?json_encode($this->data['location']):NULL;
				
			$query="UPDATE user_facebook SET fb_token=?, likes= ?, connection=?, relationship=?, birthday=?,
					 education=?, work_details=?, location=?, last_login=NOW() where user_id = ?";
			$param_array=array($this->token,$allLikes,$totalFriends,$this->data['relationship_status'],$this->data['birthday'],
							$education,$work_details,$location,$user_id);
			$tablename="user_facebook";
			Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
			/*$conn->Execute($conn->prepare("UPDATE user_facebook SET fb_token=?, likes= ?, connection=?, relationship=?, birthday=?,
					 education=?, work_details=?, location=?, last_login=NOW() where user_id = ?"),
					array($this->token,$allLikes,$totalFriends,$this->data['relationship_status'],$this->data['birthday'],
							$education,$work_details,$location,$user_id));*/
				
			$this->storeInterestHobbies($user_id);
				
				
			if(isset($allFriends)) {

				$query="INSERT INTO user_facebook_connections (user_id,connections) VALUES (?,?) on Duplicate KEY UPDATE connections=?";
				$param_array=array($user_id,$allFriends,$allFriends);
				$tablename="user_facebook_connections";
				Query_Wrapper::INSERT($query, $param_array, $tablename);
					
				/*$conn->Execute($conn->prepare("INSERT INTO user_facebook_connections (user_id,connections) VALUES (?,?) on Duplicate KEY UPDATE connections=?"),
						array($user_id,$allFriends,$allFriends));*/
			}
			$this->logFacebook($user_id,"updated");
		}
		else
		{
			$query="update user_facebook set last_login=Now(),fb_token=? where user_id=?";
			$param_array=array($this->token,$user_id);
			$tablename="user_facebook";
			Query_Wrapper::update($query, $param_array, $tablename);
			
			/*$conn->Execute("update user_facebook set last_login=Now(),fb_token='".$this->token."' where user_id='".$user_id."'");*/
		}
		
	}

	function sortByKeyCallback($a, $b)
	{
		$return = $b['likes'] - $a['likes'];
		return $return;
	}
/**
 * storing data while reimporting from fb
 * @param $user_id
 * @Himanshu
 */
	public function storeReimportedData($user_id){
		try {
			$query="update user_facebook set fname=?,lname=?,birthday=? where user_id =?";
			$param_array=array($this->data['first_name'],$this->data['last_name'],$this->data['birthday'], $user_id);
			$tablename="user_facebook";
			Query_Wrapper::update($query, $param_array, $tablename);
				
			/*$sql = $this->conn->prepare("update user_facebook set fname=?,lname=?,birthday=? where user_id =?");
			$this->conn->Execute($sql,array($this->data['first_name'],$this->data['last_name'],$this->data['birthday'], $user_id));*/
			$this->logFacebook($user_id);
			
		}
		catch(Exception $e) {
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
	}
	
	public function storeInterestHobbies($user_id){
		global $conn;
		try{

		$interesthobbies = new Interesthobbies($user_id);
		$toSelect = (array)$interesthobbies->getDBData();
		
		if(count($toSelect)>0){
			$alreadyExist = true;
		}
		
		$array = array();
		$no_of_likes = 0;
		$func = new functionClass();
		$likes  = $func->getFbLikes();
		
		foreach($likes as $key => $value){
			
			if($key == 'Sports'){
				$array['Sports'] = array();
				if(is_array($value['favorites'])){
				foreach ($value['favorites'] as $val){
					$array['Sports']['fid:'.$val->id] = $val->name;
					$no_of_likes++;
				}}
				
				if($alreadyExist){
					if(is_object($toSelect['others']['favorites'])) {
						$toSelect['others']['favorites'] = get_object_vars($toSelect['others']['favorites']);
					}
					if(is_array($toSelect['others']['favorites'])){
						foreach($toSelect['others']['favorites'] as $key => $value){
							$array['others'][$key] = $value;
						}
					}
				}
			}
			
			if($key == 'Food'){
				$array['Food'] = array();
				if(is_array($value['favorites'])){
				foreach ($value['favorites'] as $val){
					$array['Food']['fid:'.$val->id] = $val->name;
				}}
			
// 				if($alreadyExist)
// 				if(isset($toSelect['food']['favorites'])){
// 				foreach($toSelect['food']['favorites'] as $key => $value){
// 					$array['Food'][$key] = $value;
// 				}}
			}
			
			if($key == 'Music'){
				$array['Music'] = array();
				if(is_array($value['favorites'] )){
				foreach ($value['favorites'] as $val){
					$array['Music']['fid:'.$val->id] = $val->name;
					$no_of_likes++;
				}}
					
				if($alreadyExist){
					if(is_object($toSelect['music']['favorites'])) {
						$toSelect['music']['favorites'] = get_object_vars($toSelect['music']['favorites']);
					}
					if(is_array($toSelect['music']['favorites'])){
						foreach($toSelect['music']['favorites'] as $key => $value){
							$array['Music'][$key] = $value;
						}
					}
				}
			}
			
			if($key == 'Movies'){
				$array['Movies'] = array();
				if(is_array($value['favorites'])){
				foreach ($value['favorites'] as $val){
					$array['Movies']['fid:'.$val->id] = $val->name;
					$no_of_likes++;
				}}
					
				if($alreadyExist){
					if(is_object($toSelect['movies']['favorites'])) {
						$toSelect['movies']['favorites'] = get_object_vars($toSelect['movies']['favorites']);
					}
					if(is_array($toSelect['movies']['favorites'])){
						foreach($toSelect['movies']['favorites'] as $key => $value){
							$array['Movies'][$key] = $value;
						}
					}
				}
			}
			
			if($key == 'Travel' || $key == 'Food'){
				$array['Travel'] = array();
				if(is_array($value['favorites'])){
				foreach ($value['favorites'] as $val){
					$array['Travel']['fid:'.$val->id] = $val->name;
					$no_of_likes++;
				}}
					
				if($alreadyExist){
					if(is_object($toSelect['travel']['favorites'])) {
						$toSelect['travel']['favorites'] = get_object_vars($toSelect['travel']['favorites']);
					}
					if(is_array($toSelect['travel']['favorites'])){
						foreach($toSelect['travel']['favorites'] as $key => $value){
							$array['Travel'][$key] = $value;
						}
					}
				}
			}
			
			if($key == 'Books'){
				$array['Books'] = array();
				if(is_array($value['favorites'])){
				foreach ($value['favorites'] as $val){
					$array['Books']['fid:'.$val->id] = $val->name;
					$no_of_likes++;
				}}
					
				if($alreadyExist){
					if(is_object($toSelect['books']['favorites'])) {
						$toSelect['books']['favorites'] = get_object_vars($toSelect['books']['favorites']);
					}
					if(is_array($toSelect['books']['favorites'])){
						foreach($toSelect['books']['favorites'] as $key => $value){
							$array['Books'][$key] = $value;
						}
					}
				}
			}
		}
		
		if($alreadyExist){
			
			$query="update  interest_hobbies set
			music_favorites = ? ,books_favorites =? ,food_favorites = ?,
			movies_favorites = ?,other_favorites =? ,travel_favorites =? where user_id = ?";
			$param_array=array(json_encode($array['Music']),json_encode($array['Books']),
							json_encode($array['Food']),json_encode($array['Movies']),
							json_encode($array['others']),json_encode($array['Travel']),$user_id);
			$tablename="interest_hobbies";
			Query_Wrapper::update($query, $param_array, $tablename);
				
			
			/*$conn->Execute($conn->prepare("update  interest_hobbies set
			music_favorites = ? ,books_favorites =? ,food_favorites = ?,
			movies_favorites = ?,other_favorites =? ,travel_favorites =? where user_id = ?"),
					array(json_encode($array['Music']),json_encode($array['Books']),
							json_encode($array['Food']),json_encode($array['Movies']),
							json_encode($array['others']),json_encode($array['Travel']),$user_id));*/
			
			$query="update user_facebook set no_of_likes=? where user_id=?";
			$param_array=array($no_of_likes,$user_id);
			$tablename="user_facebook";
			Query_Wrapper::update($query, $param_array, $tablename);
			// SATISH TODO: is this right place ?
			FacebookPageDBO::check_and_add_pages($array);
			/*$conn->Execute($conn->prepare("update user_facebook set no_of_likes=? where user_id=?"),
					array($no_of_likes,$user_id));*/
				
		}else{
			
			$query="update user_facebook set no_of_likes=? where user_id=?";
			$param_array=array($no_of_likes,$user_id);
			$tablename="user_facebook";
			Query_Wrapper::update($query, $param_array, $tablename);
				
			/*$conn->Execute($conn->prepare("update user_facebook set no_of_likes=? where user_id=?"),
					array($no_of_likes,$user_id));*/
			
			$query="insert into interest_hobbies (user_id,music_favorites,books_favorites,food_favorites,
			movies_favorites,other_favorites,travel_favorites) 
			values(?,?,?,?,?,?,?)";
			$param_array=array($user_id,json_encode($array['Music']),json_encode($array['Books'])
			,json_encode($array['Food']),json_encode($array['Movies']),
			json_encode($array['others']),json_encode($array['Travel']));
			$tablename="interest_hobbies";
			Query_Wrapper::INSERT($query, $param_array, $tablename);
			FacebookPageDBO::check_and_add_pages($array);
			/*$conn->Execute($conn->prepare("insert into interest_hobbies (user_id,music_favorites,books_favorites,food_favorites,
			movies_favorites,other_favorites,travel_favorites) 
			values(?,?,?,?,?,?,?)"),
	array($user_id,json_encode($array['Music']),json_encode($array['Books'])
			,json_encode($array['Food']),json_encode($array['Movies']),
			json_encode($array['others']),json_encode($array['Travel']))); */
		}
		}catch (Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
	}
	 
	
	public function logFacebook($user_id , $error = null){
		global $conn;
		try {
			$query="insert into log_facebook (user_id,fid,username,name,first_name,last_name,link,location,gender,email,token, error) values (?,?,?,?,?,?,?,?,?,?,?,?)";
			$param_array=array($user_id,$this->data['id'],$this->data['username'],$this->data['name'],$this->data['first_name'],$this->data['last_name'],$this->data['link'],json_encode($this->data['location']),$this->data['gender'],$this->data['email'],$this->token, $error);
			$tablename="log_facebook";
			Query_Wrapper::INSERT($query, $param_array, $tablename);
				
			/*$conn->Execute($conn->prepare("insert into log_facebook (user_id,fid,username,name,first_name,last_name,link,location,gender,email,token, error) values (?,?,?,?,?,?,?,?,?,?,?,?)"),array($user_id,$this->data['id'],$this->data['username'],$this->data['name'],$this->data['first_name'],$this->data['last_name'],$this->data['link'],json_encode($this->data['location']),$this->data['gender'],$this->data['email'],$this->token, $error));*/
		}
		catch(Exception $e) {	
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);	
		}
	}
	
	public function checkFbPrescence() {
		$output['status'] = 'NEW_USER';
		
		if($this->testfids($this->id)){
			return $output;
		}
		
		global $conn;
		$rs = $conn->Execute ( $conn->prepare ( "select * from user where fid=?" ), $this->id );
		
		if ($rs->_numOfRows == 0) {
			$id = $this->getEmail ();
			if ($id) {
				$rs = $conn->Execute ( $conn->prepare ( "select * from user where email_id=?" ), $id );
				if ($rs->_numOfRows > 0) {
					$result=$rs->GetRows();
					$output['status'] = 'CONNECTED_VIA_EMAIL';
					$output['user_id'] = $result[0]['user_id'];
					return $output;
				}
				return $output;
			}else{
				return $output;
				//return 'FACEBOOK_NOT_RETURNED_EMAIL';
			}
		} else {
			
			$result=$rs->GetRows();
			if($result[0]['status']=='suspended'){
				$output['status'] = 'DEACTIVATED_PROFILE';
				return $output;
			}
			else if($result[0]['status']=='blocked'){
				$output['status'] = 'BLOCKED_PROFILE';
				return $output;
			}
			else {
				$output['status'] = 'SUCCESS';
				$output['user_id'] = $result[0]['user_id'];
				return $output;
			}
			
			//      return 'CONNECTED_VIA_FACEBOOK';
		}
	}
	
	//for App only
	public function checkFbPrescenceForApp() {
		$output['status'] = 'NEW_USER';
		
		if($this->testfids($this->id)){
			return $output;
		}
		
		global $conn;
		$rs = $conn->Execute ( $conn->prepare ( "select * from user where fid=?" ), $this->id );
	
		if ($rs->_numOfRows == 0) {
			$id = $this->getEmail ();
			if ($id) {
				$rs = $conn->Execute ( $conn->prepare ( "select * from user where email_id=?" ), $id );
				if ($rs->_numOfRows > 0) {
					$result=$rs->GetRows();
					$output['status'] = 'CONNECTED_VIA_EMAIL';
					$output['deletion_status'] = $result[0]['deletion_status'] ;
					$output['user_id'] = $result[0]['user_id'];
					return $output;
				}
				return $output;
			}else{
				return $output;
// 				return 'FACEBOOK_NOT_RETURNED_EMAIL';
			}
		} else {
				
			$result=$rs->GetRows();
			$output['status'] = 'SUCCESS';
			$output['user_id'] = $result[0]['user_id'];
			$output['deletion_status'] = $result[0]['deletion_status'] ;
			return $output;
			//      return 'CONNECTED_VIA_FACEBOOK';
		}
	}


	// check whether a facebook user is new or already registered with us
	public function checkPrescence($userId) {
		global $conn;
		$output['status'] = 'FAIL';
		//var_dump("yuser_id passwed:$userId");
		if($this->testfids($this->id)){
			$rs = $conn->Execute ( $conn->prepare ( "select user_id,fid,connection from user_facebook where user_id=? and fid=?" ), array($userId,$this->id) );
		}
		else
			$rs = $conn->Execute ( $conn->prepare ( "select user_id,fid,connection from user_facebook where user_id=? OR fid=?" ), array($userId,$this->id) );
		if ($rs->_numOfRows == 0) {
			$output['status'] = 'NEW';
			return $output;
		}else{
			$row = $rs->FetchRow();
			if($row['user_id']==$userId&&$row['fid']==$this->id){
				$output['status'] = 'SUCCESS';
				$output['connection'] = $row['connection'];
				return $output;
				//return 'SUCCESS';
			}elseif ($row['fid']!=$this->id){
				$output['status'] = 'ANOTHER';
				return $output['status'];
			}else{
				$output['status'] = 'FAIL';
				return $output['status'];
			}
		}
		return $output['status'];
	}
	
	
	public  function saveFacebookIds() {
		$c=1;
		$sql_array=array();
		$sql_base="insert ignore into facebook_likes_ids(fid,name) values";
		$sql_main=$sql_base;		
		
		$val_fetched=false;
		foreach($this->fid_data as $fid=>$data){
			$val_fetched=true;
			if($c%1000==0){
				$sql_main=substr($sql_main,0,-1);
				Query_Wrapper::INSERT($sql_main, $sql_array, "facebook_likes_ids");
				/*$this->conn->Execute($this->conn->Prepare($sql_main),$sql_array);*/
				$sql_main=$sql_base;
				$sql_array=array();
			}
			$sql_main.="(?,?),";
			$sql_array[]=$fid;
			$sql_array[]=$data;
			$c++;
		}
	
		if($val_fetched==true){
			$sql_main=substr($sql_main,0,-1);
			Query_Wrapper::INSERT($sql_main, $sql_array, "facebook_likes_ids");
			/*$this->conn->Execute($this->conn->Prepare($sql_main),$sql_array);*/
		}
	}

	//fetching user likes from fb
	public  function getLikes($data) {
		$func=new functionClass();
		$likes=$data['likes'];	
		$music_listens=$data['music.listens'];
		$music_playlists=$data['music.playlists'];
		$books_quotes=$data['books.quotes'];
		$books_rates=$data['books.rates'];	
		$books_wants_to_read=$data['books.wants_to_read'];
		$books_reads=$data['books.reads'];
		$video_wants_to_watch=$data['video.wants_to_watch'];
		$video_watches=$data['video.watches'];
		$video_rates=$data['video.rates'];
		$places_saves=$data['places.saves'];
		
		/**
		 * if any of the below parameter is null then making an empty array to avoid warnings
		 * ideally an empty array should be returned by above function instead of null
		 * @Himanshu
		 */
				
		/*if($likes==null)$music_listens=array();
		if($music_listens==null)$music_listens=array();
		if($music_playlists==null)$music_playlists=array();
		if($books_quotes==null)$books_quotes = array();
		if($books_rates==null)$books_rates == array();	
		if($books_wants_to_read==null)$books_wants_to_read=array();
		if($books_reads==null)$books_reads = array();
		if($video_wants_to_watch==null)$video_wants_to_watch=array();
		if($video_watches==null)$video_watches=array();
		if($video_rates==null)$video_rates=array();
		if($places_saves==null)$places_saves=array();*/
		
		
		//group by category
		//print_r($likes['paging']['next']);die;
		
		$i =0 ;
		if(is_array($likes['data'])){
		foreach($likes['data'] as $mylike) {
			$newLikes[$mylike['category']]['count']++;
			$newLikes[$mylike['category']][$i]['name']=str_replace(",","-",$mylike['name']);
			$newLikes[$mylike['category']][$i]['id']=str_replace(",","-",$mylike['id']);
			$i++;
			$this->fid_data[$mylike['id']]=$mylike['name'];
			// array_unique($newLikes[$mylike['category']]);
		}}
		
		$i = 0;
		if(is_array($music_listens['data'])){
		foreach($music_listens['data'] as $mylike) {
                        $newLikes['music_listens']['count']++;
                        $newLikes['music_listens'][$i]['name']=str_replace(",","-",$mylike['data']['song']['title']);
                        $newLikes['music_listens'][$i]['id']=str_replace(",","-",$mylike['data']['song']['id']);
                        $i++;
                        $this->fid_data[$mylike['data']['song']['id']]=$mylike['data']['song']['title'];
                }
		}
		$i = 0;
		if(is_array($music_playlists['data'])){
		foreach($music_playlists['data'] as $mylike) {
                        $newLikes['music_playlists']['count']++;
                        $newLikes['music_playlists'][$i]['name']=str_replace(",","-",$mylike['data']['song']['title']);
                        $newLikes['music_playlists'][$i]['id']=str_replace(",","-",$mylike['data']['song']['id']);
                        $i++;
                        $this->fid_data[$mylike['data']['song']['id']]=$mylike['data']['song']['title'];
                }
		}
        $i = 0; 
        if(is_array($books_quotes['data'])){       
		foreach($books_quotes['data'] as $mylike) {
                        $newLikes['book_quotes']['count']++;
                        $newLikes['book_quotes'][$i]['name']=str_replace(",","-",$mylike['data']['book']['title']);
                        $newLikes['book_quotes'][$i]['id']=str_replace(",","-",$mylike['data']['book']['id']);
                        $i++;
                        $this->fid_data[$mylike['data']['book']['id']]=$mylike['data']['book']['title'];
                }
        }
         $i = 0;
         if(is_array($books_rates['data'])){
		foreach($books_rates['data'] as $mylike) {
                        $newLikes['book_rates']['count']++;
                        $newLikes['book_rates'][$i]['name']=str_replace(",","-",$mylike['data']['book']['title']);
                        $newLikes['book_rates'][$i]['id']=str_replace(",","-",$mylike['data']['book']['id']);
                        $i++;
                        $this->fid_data[$mylike['data']['book']['id']]=$mylike['data']['book']['title'];
                }}
		$i =0;
		if(is_array($books_wants_to_read['data'])){
		foreach($books_wants_to_read['data'] as $mylike) {
                        $newLikes['book_wants_to_read']['count']++;
                        $newLikes['book_wants_to_read'][$i]['name']=str_replace(",","-",$mylike['data']['book']['title']);
                        $newLikes['book_wants_to_read'][$i]['id']=str_replace(",","-",$mylike['data']['book']['id']);
                        $i++;
                        $this->fid_data[$mylike['data']['book']['id']]=$mylike['data']['book']['title'];
                }
		}
         $i=0;
         if(is_array($books_reads['data'])){
		foreach($books_reads['data'] as $mylike) {
                        $newLikes['books_reads']['count']++;
                        $newLikes['books_reads'][$i]['name']=str_replace(",","-",$mylike['data']['book']['title']);
                        $newLikes['books_reads'][$i]['id']=str_replace(",","-",$mylike['data']['book']['id']);
                        $i++;
                        $this->fid_data[$mylike['data']['book']['id']]=$mylike['data']['book']['title'];
                }
         }
                
             $i=0; 
             if(is_array($video_watches['data'])){  
		foreach($video_watches['data'] as $mylike) {
                        $newLikes['video_watches']['count']++;
			if(isset($mylike['data']['movie']['title'])){
                        $newLikes['video_watches'][$i]['name']=str_replace(",","-",$mylike['data']['movie']['title']);
                        $newLikes['video_watches'][$i]['id']=str_replace(",","-",$mylike['data']['movie']['id']);
                        $this->fid_data[$mylike['data']['movie']['id']]=$mylike['data']['movie']['title'];
			}
			else  {
				$newLikes['video_watches'][$i]['name']=str_replace(",","-",$mylike['data']['tv_show']['title']);
				$newLikes['video_watches'][$i]['id']=str_replace(",","-",$mylike['data']['tv_show']['id']);
				$this->fid_data[$mylike['data']['tv_show']['id']]=$mylike['data']['tv_show']['title'];
			}
			$i++;
         }
             }
         
        $i =0 ; 
        if(is_array($video_rates['data'])){
		foreach($video_rates['data'] as $mylike) {
                        $newLikes['video_rates']['count']++;
			if(isset($mylike['data']['movie']['title'])){
                        $newLikes['video_rates'][$i]['name']=str_replace(",","-",$mylike['data']['movie']['title']);
                        $newLikes['video_rates'][$i]['id']=str_replace(",","-",$mylike['data']['movie']['id']);
                        $this->fid_data[$mylike['data']['movie']['id']]=$mylike['data']['movie']['title'];
                        
			}
                        else { $newLikes['video_rates'][$i]['name']=str_replace(",","-",$mylike['data']['tv_show']['title']);
                        $newLikes['video_rates'][$i]['id']=str_replace(",","-",$mylike['data']['tv_show']['id']);
                        $this->fid_data[$mylike['data']['tv_show']['id']]=$mylike['data']['tv_show']['title'];
                        
                        
                        }
                        $i++;
                }
        }
                
         $i=0;  
         if(is_array($places_saves['data'] )){     
		foreach($places_saves['data'] as $mylike) {
                        $newLikes['places_saves']['count']++;
                        $newLikes['places_saves'][$i]['name']=str_replace(",","-",$mylike['data']['song']['title']);
                        $newLikes['places_saves'][$i]['id']=str_replace(",","-",$mylike['data']['song']['id']);
                        $i++;
                        $this->fid_data[$mylike['data']['song']['id']]=$mylike['data']['song']['title'];
                }
         }

		// 		while(1) {
		// 			foreach($likes['data'] as $mylike) {
		// 				$newLikes[$mylike['category']]['count']++;
		// 				$newLikes[$mylike['category']][]=$mylike['name'];
		// 			}
		// 			if(isset($likes['paging']['next'])) {
		// 				//echo $likes['paging']['cursors']['after'];
		// 				$likes=$this->facebook->api('/me/likes?limit=25&after='.$likes['paging']['cursors']['after'],'GET');
		// 				//print_r($likes);die;

		// 				continue;
		// 			}
		// 			break;

		// 		}
	//	print_r($newLikes);die;
		return $newLikes;
		/* 
		 foreach($newLikes as $category=>$myNewLikes) {
		echo "<br/><br/>".$category.":";
		foreach($myNewLikes as $a)
			echo $a.",	";
		}
		*/

}


public  function getFriends($data) {
// 	$friends=$data['friends'];

// 	foreach($likes['data'] as $mylike) {
// 		$newLikes[$mylike['category']]['count']++;
// 		$newLikes[$mylike['category']][]=$mylike['name'];
// 	}
// 	return $newLikes;

}

/*
 * to find if two friends are mutually connected
* @Himanshu
*/

public function mutualConnections($fid1, $fid2){

	$key = $fid1. '/mutualfriends/' . $fid2;
	//echo '<br>' . $key;
	echo '<pre>';
	$mFriends = $this->make_api_request( $key);
		//var_dump($profile); die;
		foreach($mFriends['data'] as  $key=>$val){
			$img_url = "https://graph.facebook.com/". $val["id"] . "/picture";
	$strToStore[$val['id']] = "Name:" . $val["name"] . ",fid:" . $val["id"] . ",thumbnail:" . $img_url;
		}
		
		//var_dump($strToStore); die;
	/*$img_url = "https://graph.facebook.com/". $profile[$key]["data"][0]["id"] . "/picture";
	$strToStore = "Name:" . $profile[$key]["data"][0]["name"] . ",user_id:" . $profile[$key]["data"][0]["id"] . ",thumbnail:" . $img_url;*/
	//	$this->usrQ->storeInHashSet($user_id, $key, 'mutual_connections', $strToStore);
	//return  $mutualConnData;
}


public function mutuallyConnected($user_id, $match_ids){
	echo 'inside  fb';// die;
	//echo $user_id;
	$ids = $user_id  . ',942811,'.  implode($match_ids, ',');
	//check if users are mututally connected
	$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	$query = "SELECT user_id, fid, is_fb_connected as fb_connect from user where user_id in (" . $ids . ') order by field( user_id, ' . $ids . ')';
	$res = $this->conn->Execute($query);
	$results = $res->GetRows();
	//var_dump($query);
	//var_dump($results);

	if(($results[0]['fb_connect'] == 'N')&&($results[0]['user_id'] == $user_id)){
		// no need to check mutual connections
		return null;
	}
	/*var_dump($results);
	 echo '<br>';
	*/$op = array_slice($results, 1);
	//var_dump($op);
	//	echo "<br>";
	//check if user is connected through fb
	$fids = array();
	foreach ($op as $key){
		//echo $key;
		if($key['fb_connect'] == 'Y'){

			$fids[$key['user_id']] = $key['fid'];
		}
	}
	//var_dump($fids);
	$mutualConnData = array();
	foreach ($fids as $key=>$val){
		//		echo 'get profile';
		//	var_dump($fids);
		//	echo $val;
		//get the data from redis first if not then make a call
		$profile[$key] = $this->usrQ->getFromHashSet($user_id, $key, 'mutual_connections');
		//	var_dump($profile[$key]);
		//var_dump($this->usrQ->getFromHashSet($user_id, $key, 'mutual_connections'));
		if($profile[$key]== false){
			//	echo 'inside key';
			//	echo $key;
			$profile[$key] = $this->make_api_request('/me/mutualfriends/'. $val);
			//	var_dump($profile[$key]);
			$img_url = "https://graph.facebook.com/". $profile[$key]["data"][0]["id"] . "/picture";
			$strToStore = "Name:" . $profile[$key]["data"][0]["name"] . ",user_id:" . $profile[$key]["data"][0]["id"] . ",thumbnail:" . $img_url;
			$this->usrQ->storeInHashSet($user_id, $key, 'mutual_connections', $strToStore);
		}
		//$mutualConnData = array_push($mutualConnData, $profile);
		//below statement returns the name of mutual connection
		//var_dump($profile[$key]["data"][0]["name"]);

		//	print_r($profile[$key]["data"]);
	}
	//var_dump($profile[942809]["data"][0]["name"]);
	return  $mutualConnData;

}

public function getFbLikes($userId) {
	global $conn;
	$fb_likes=array();
	foreach($fb_likes as $fb_like)
		$allLikes[strtolower($fb_like['name'])]=$fb_like['category_name'];
	$rs2=$conn->Execute("select likes from user_facebook where user_id='".$userId."'");
	if($rs2->_numOfRows>0) {
		$result=$rs2->GetRows();
		$likes=(array)json_decode($result[0]['likes']);
		foreach($likes as $category=>$like) {
			$like=(array)$like;
			if($like['count']>0) {
				unset($like['count']);
				if(isset($allLikes[strtolower($category)]))
				foreach($like as $lik)
					$toSelect[$allLikes[strtolower($category)]]['favorites'][]=$lik;
				else Query_Wrapper::INSERT("insert into log_fb_likes values('".$category."')", NULL, "log_fb_likes");
					/*$conn->Execute("insert into log_fb_likes values('".$category."')");*/ //snehil wanted this table log_fb_likes
			}
		}
	}
	return $toSelect;
}

/*
 * to log the time taken by fb calls
 * 
 */

private function logAction($event,$diff) {
	$Trk = new EventTrackingClass();
	$data_logging = array();
	$data_logging [] = array( "activity" => "fb_new" , "event_type" => $event, "event_status" => "success", "event_info" => array("token"=>$this->token), "time_taken"=>$diff );
	$Trk->logActions($data_logging);
}


private function fillUserData($user_id) {
	global $conn;
	
	$dob = $this->processDOB();
	$companies = $this->processCompanies();
	$colleges = $this->processColleges();
	$location = $this->processLocation();
	$fb_designation=$this->processDesignation();
	
	$query="insert into user_data(user_id,DateOfBirth,company_name,institute_details,
			stay_country,stay_state,stay_city,tstamp,fb_designation) values(?,?,?,?,?,?,?,NOW(),?)";
	$param_array=array($user_id,$dob,$companies,$colleges,$location['country'],$location['state'],$location['city'],$fb_designation);
	$tablename="user_data";
	Query_Wrapper::INSERT($query, $param_array, $tablename);
	
	/*$conn->Execute($conn->prepare("insert into user_data(user_id,DateOfBirth,company_name,institute_details,
			stay_country,stay_state,stay_city,tstamp) values(?,?,?,?,?,?,?,NOW())"),
			array($user_id,$dob,$companies,$colleges,$location['country'],$location['state'],$location['city']));*/
	
	$values = $this->preferenceAge($dob);

	$query="insert into user_preference_data(user_id, start_age, end_age, tstamp)
				values(?,?,?,NOW()) on DUPLICATE KEY UPDATE start_age=?,end_age=?,tstamp=NOW()";
	$param_array=array($user_id,$values['start'],$values['end'],$values['start'],$values['end']);
	$tablename="user_preference_data";
	Query_Wrapper::INSERT($query, $param_array, $tablename);
	
	/*$conn->Execute($conn->prepare("insert into user_preference_data(user_id, start_age, end_age, tstamp)
				values(?,?,?,NOW()) on DUPLICATE KEY UPDATE start_age=?,end_age=?,tstamp=NOW()"),
			array($user_id,$values['start'],$values['end'],$values['start'],$values['end']));*/

}


private function preferenceAge($dob) {
	/*
	 * for preference age
	*/
	$age = date("Y")-$dob;
	$prefAge = Utils::preferenceAge($age,$this->data['gender']);
	return $prefAge;
}

public function processDOB() {
	
	if(isset($this->data['birthday'])) {
		$birthday=explode("/",$this->data['birthday']);
		if(count($birthday)==3){
			return $birthday[2]."-".$birthday[0]."-".$birthday[1];
		}
	}
}

public function processCompanies() {
	if(isset($this->data['work'])) {
		$work_details = $this->data['work'];
		
		for($i=0;$i<count($work_details);$i++){
			foreach($work_details[$i] as $key => $value){
				if($key=="employer"){
					$insert=true;
					$unset_index=-1;
					if(count($prev)>=1)
					{
						$index=0;
					
						foreach ($prev as $company)
						{
							$similarity_percent=0;
							$str=strtolower($value['name']);
							$str1=strtolower($company);
							similar_text($str,$str1,$similarity_percent);
							if($similarity_percent>60)
							{
								if(strlen($str)>strlen($str1))
								{
									//Insert this into array and unset the $index position in the array
									$unset_index=$index;
					
										
								}else
								{
									//
									$insert=false;
								}
							}
							$index++;
						}
					}
					if($unset_index!=-1)
					{
						//The index has beeen changed, proceed to unset
						$prev=Utils::custom_unset($prev,$unset_index);
					}
					if($insert)
						$prev[] = $value['name'];
					
					
					
					
					
				}
			}
		}
		return json_encode($prev,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
	}
	return NULL;
	
}

public function processColleges() {
	if(isset($this->data['education'])) {
		$result = $this->data['education'];
		$inst_count=0;
		for($i=0;$i<count($result);$i++){
			foreach($result[$i] as $key => $value){
				if($key=="school"){
					$insert=true;
					$unset_index=-1;
					if(count($institute)>=1)
					{
						$index=0;
						
						foreach ($institute as $inst)
						{
							$similarity_percent=0;
							$str=strtolower($value['name']);
							$str1=strtolower($inst);
							similar_text($str,$str1,$similarity_percent);
							if($similarity_percent>60)
							{
								if(strlen($str)>strlen($str1))
								{
									//Insert this into array and unset the $index position in the array
									$unset_index=$index;
								
									
								}else 
								{
									//
									$insert=false;
								}
							}
							$index++;
						}
					}
					if($unset_index!=-1)
					{
						//The index has beeen changed, proceed to unset
						$institute=Utils::custom_unset($institute,$unset_index);
					}
					if($insert)
					$institute[] = $value['name'];
				}
			}
		}
		return json_encode($institute,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
	}
	return NULL;
}

public function processLocation() {
	global $conn;
	
	if(isset($this->data['location'])){
		$location = $this->data['location'];
		$res = $conn->Execute($conn->Prepare("select city_id from facebook_cities_mapping where city_fid=?"),array($location['id']));
		if($res && $res->rowCount()>0){
			$result = $res->FetchRow();
			$res1 = $conn->Execute($conn->Prepare("select distinct * from geo_city where city_id=? and country_id=?"),array($result['city_id'] , $this->ip_country));
			$city = $res1->FetchRow();
			return array("country"=>$this->ip_country,"state"=>$city['state_id'],"city"=>$city['city_id']);
		}
		return array("country"=>$this->ip_country);
	}
	return array("country"=>$this->ip_country);
	
}
public function processDesignation()
{
	if(isset($this->data['work'][0]["position"]))
	{
		$designation= $this->data['work'][0]["position"]["name"];
		return json_encode($designation,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
	}
	else
		return NULL;
}


public function get_mutual_friends($user_id, $target_user_id, $target_user_fb_id) {
	$response = null;
	$start_time = time();
	$status = "success";
	$event_info = array();
	$api_response = $this->make_api_request("/$target_user_fb_id?fields=context.fields(all_mutual_friends.fields(first_name, picture).limit(1000))");
	$time_taken = time() - $start_time;
	$decoded_body = array();
	if ($api_response != null)
		$decoded_body = $api_response;
	$result = array();
	$mf_data = array();
	$count = 0;
	if (isset($decoded_body["id"]) && isset($decoded_body["context"]) && isset($decoded_body["context"]["all_mutual_friends"])
		 	&& isset($decoded_body["context"]["all_mutual_friends"]["data"])) {
		$responses = $decoded_body["context"]["all_mutual_friends"]["data"];
		foreach ($responses as $response) {
			$mf_data[] = array("name" => isset($response['first_name']) ? $response['first_name'] : "", "url" => (isset($response['picture'])
				&& isset($response['picture']['data']) && isset($response['picture']['data']['url'])) ? 
					$response['picture']['data']['url'] : "");
		}
	}
	if (isset($decoded_body["id"]) && isset($decoded_body["context"]) && isset($decoded_body["context"]["all_mutual_friends"])
			&& isset($decoded_body["context"]["all_mutual_friends"]["summary"])
			&& isset($decoded_body["context"]["all_mutual_friends"]["summary"]["total_count"]))
		$count = $decoded_body["context"]["all_mutual_friends"]["summary"]["total_count"];	
	
	if ($api_response != null) {
		$event_info = array("target_user" => $target_user_id, "response" => $api_response);
		$this->log_mutual_friends_data($user_id, "success", $event_info, time() - $time_taken);
	}
	$result['count'] = $count;
	$result['mutual_friends'] = $mf_data;
	return $result;
}


private function log_mutual_friends_data($user_id, $status, $event_info, $time_taken) {
	$event_tracker = new EventTrackingClass();
	$data_logging = array();
	$data_logging [] = array("user_id" => $user_id, "actvity" => "fb_new" , "event_type" => "all_mf",
			"event_status" => $status, "event_info" => $event_info, "time_taken" => $time_taken);
	$event_tracker->logActions($data_logging);
}


private function log_facebook_api_exception($event_info, $time_taken) {
	// not the right way
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = "";
	if ($user != null && isset($user['user_id']))
		$user_id = $user['user_id'];
	$event_tracker = new EventTrackingClass();
	$data_logging = array();
	$data_logging [] = array("user_id" => $user_id, "actvity" => "fb_new" , "event_type" => "fb_api_call_exception",
			"event_status" => "exception", "event_info" => $event_info, "time_taken" => $time_taken);
	$event_tracker->logActions($data_logging);
}


private function make_api_request($url) {
	$start_time = time();
	$time_taken = 0;
	try {
		$response = $this->facebook->get($url);
		$time_taken = time() - $start_time;
	} catch(Exception $e) {
		$status = "fail";
		$event_info = array();
		$event_info['error_message'] = $e->getMessage();
		$this->log_facebook_api_exception($event_info, $time_taken);
	}
	if ($response != null)
		return $response->getDecodedBody();
}

public function is_token_valid($user_id) {
	$start_time = time();
	$time_taken = 0;
	$access_token_valid = true;
	try {
		$response = $this->facebook->get("/me?access_token=" . $this->token);
		$time_taken = time() - $start_time;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		$event_info = array();
		if ($e->getCode() == 190) {
			$access_token_valid = false;
			$event_info['access_token_valid'] = false;
		}
		$status = "fail";
		$event_info['error_message'] = $e->getMessage();
		$this->log_facebook_api_exception($event_info, $time_taken);
	} catch(Exception $e) {
		$status = "fail";
		$event_info = array();
		$event_info['error_message'] = $e->getMessage();
		$this->log_facebook_api_exception($event_info, $time_taken);
	}
	return $access_token_valid;
}

}
?>
