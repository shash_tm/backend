<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute("select user_id,mobile_number from user_trust_score");
	
	if($sql->RowCount()>0){
		while ($row = $sql->FetchRow()){
			$user_id = $row['user_id'];
			$number = $row['mobile_number'];
			if(isset($number)) {
				$number = substr_replace($number,"XXXXX",5,5);
			}
			$conn->Execute($conn->prepare("update user_trust_score SET mobile_number=? where user_id=? "),
					array($number,$user_id));
	
		}
	}
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}


?>