<?php

require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../fb.php";
require_once dirname ( __FILE__ ) . "/../DBO/userFacebookDBO.php";

try {
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$target_user_id = (isset($_REQUEST ['target_user_id'])) ? $_REQUEST ['target_user_id'] : null;
	$response = array();
	if (isset($user_id) && $target_user_id != null) {
		$user_fb = new UserFacebookDBO();
		$access_token = $target_user_fb_id = null;
		$access_token = $user_fb->get_facebook_token($user_id);
		$target_user_fb_id = $user_fb->get_facebook_id($target_user_id);
		if ($access_token != null && $target_user_fb_id != null) {
			$fb = new fb($access_token);
			$mutual_friends = $fb->get_mutual_friends($user_id, $target_user_id, $target_user_fb_id);
			$response["responseCode"] = 200;
			$response["target_user_id"] = $target_user_id;
			$response["data"] = isset($mutual_friends['mutual_friends']) ? $mutual_friends['mutual_friends'] : array();
		} else {
			$response["responseCode"] = 401;
		}
	}
	else{
		$response["responseCode"] = 401;
	}
	echo json_encode($response);
}
catch(Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}
?>
