<?php 
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/../DBO/userFacebookDBO.php";


class MutualFriends {
	static $mysql_chunk_size = 500;
	static $mysql_insert_chunk_size = 500;
	static $fb_api_call_chunk_size = 500;
	private $event_tracker = null;	

	public function __construct() {
		$this->event_tracker = new EventTrackingClass();
	}	
	
	
	/** This fn gets mutual friends count from the table */
	public function get_previously_calculated_data($user_id, $matches_user_ids) {
		$previously_calculated_users = array();
		foreach (array_chunk($matches_user_ids, MutualFriends::$mysql_chunk_size) as $slice) {
			$param_count = Query_Wrapper::getParamCount($slice);
			$sql = "select user2, fb_count, tm_count from user_mutual_friends where user1 = ? and user2 in ($param_count)";
			$rows = Query_Wrapper::SELECT($sql, array_merge(array($user_id), $slice));
			if (count($rows) > 0) {
				foreach ($rows as $key => $value) {
					$row = $rows[$key];
					if (!isset($previously_calculated_users[$row['user2']]))
						$previously_calculated_users[$row['user2']] = array();
					$previously_calculated_users[$row['user2']]['fb_count'] = intval($row['fb_count']);
					$previously_calculated_users[$row['user2']]['tm_count'] = intval($row['tm_count']);
				}
			}
		}
		return $previously_calculated_users;
	}
	
	
	private function save_mutual_friends_data($user_id, $mutual_friends_data) {
		$time_stamp = time();
		$insertSql = "insert into user_mutual_friends (user1, user2, fb_count, tm_count, time_stamp) values(?,?,?,?,?)";
		foreach (array_chunk($mutual_friends_data, MutualFriends::$mysql_insert_chunk_size, true) as $slice) {
			$arr = array();
			foreach ($slice as $key => $value) {
				$arr[] = array($user_id, $key, $value['fb_count'], $value['tm_count'], $time_stamp);
			}
			if (count($arr) > 0)
				Query::BulkINSERT($insertSql, $arr);
		}
	}

	
	/* This fn was for AB test. It's not used anymore. AB testing is handled at Recommendation Engine level */
	private function should_fetch_mutual_friends_data($user_id) {
		$uf = new UserFlagsDBO();
		$key = "mutual_friends_ab_test_user_ids";
		$data = $uf->read($key);
		if (isset($data[$key]) && in_array($user_id, explode(',', $data[$key])))
			return TRUE;
		$key = "mutual_friends_ab_test";
		$data = $uf->read($key);
		if (isset($data[$key])) {
			// value format: F:10#M:0
			$data = explode("#", $data[$key]);
			$ab_percent_female = explode(":", $data[0])[1]; 
			$ab_percent_male = explode(":", $data[1])[1];
			$ud = new UserData($user_id);
			$gender = $ud->fetchGender();
			if ($gender == "F" && $user_id % 100 < $ab_percent_female)
				return TRUE;
			if ($gender == "M" && $user_id % 100 < $ab_percent_male)
				return TRUE;
		}
		return FALSE;
	}
	
	
	/** Gets mutual friends counts and returns the result */
	public function get_mutual_friends_data($user_id, $matches_user_ids) {
		/*if (!$this->should_fetch_mutual_friends_data($user_id))
			return array();*/
		$start_time = microtime(true);
		$user_fb = new UserFacebookDBO();
		$fb_acess_token = $user_fb->get_facebook_token($user_id);
		if ($fb_acess_token == null || $fb_acess_token == "")
			return array();
		
		// get mf count for users stored in db
		$previously_calculated_data = $this->get_previously_calculated_data($user_id, $matches_user_ids);
		$new_users = array_diff($matches_user_ids, array_keys($previously_calculated_data));
		
		$new_users_fb_ids = $user_fb->get_facebook_ids($new_users);
		
		$fb_to_user_id_map = array();
		foreach ($new_users_fb_ids as $key => $value)
			$fb_to_user_id_map[$value] = $key;
		
		global $config;
		$app_secret = $config ['facebook'] ['secret'];	
		
		// make call to api and get the mf counts
		$mutual_friends_data = $this->get_mutual_friends_data_from_api($user_id, $fb_acess_token, array_values(
				$new_users_fb_ids), $fb_to_user_id_map, $app_secret);
		
		unset($new_users_fb_ids);
		unset($fb_to_user_id_map);
		
		// not storing mf counts in db
		//$this->save_mutual_friends_data($user_id, $mutual_friends_data);
		
		$mutual_friends_data = $mutual_friends_data + $previously_calculated_data;
		$event_info = array();
		$diff = microtime(true) - $start_time;
		$time_elapsed_secs = number_format((float)$diff, 2, '.', '');
		
		// logging
		$event_info['all_users'] = $matches_user_ids;
		$event_info['already_cal_users'] = array_keys($previously_calculated_data);
		$event_info['new_users'] = array_values($new_users);
		$event_info['users_with_success'] = array_keys($mutual_friends_data);
		$event_info['users_with_errors'] = array_values(array_diff($new_users, array_keys($mutual_friends_data)));
		$this->log_mutual_friends_action($user_id, "mutual_friends_data", "success", $event_info, $time_elapsed_secs);
		
		return $mutual_friends_data;
	}
	
	
	/**
	 * Gets mutual friends counts by making call to python script.
	 */
	private function get_mutual_friends_data_from_api($input_user_id, $fb_access_token, $matches_fb_ids, $fb_to_user_id_map,
			$app_secret) {
		
		$mutual_friends_data = array();
		
		foreach (array_chunk($matches_fb_ids, MutualFriends::$fb_api_call_chunk_size) as $slice) {
			$ids = join(',', $slice);
			$start_time = microtime(true);
			
			// make call to python script
			$command = 'python ' . dirname ( __FILE__ ) .'/get_fb_mutual_friends_data.py ' 
				. $input_user_id . " " . $fb_access_token . " " . $ids . " " . $app_secret;
			$response = shell_exec($command);
			
			$diff = microtime(true) - $start_time;
			$time_elapsed_secs = number_format((float)$diff, 2, '.', '');
			$event_info = array();
			$status = "fail";
			
			if ($response != null) {
				$json = json_decode($response, true);
				
				// some error after python script execution - access token issues etc
				if (isset($json['status']) && $json['status'] != 200) {
						$event_info['responseCode'] = $json['status'];
				}
				
				// script execution successful
				if (isset($json['status']) && $json['status'] == 200 && isset($json['data'])) {
					$status = "success";
					$event_info['responseCode'] = $json['status'];
					
					foreach ($json['data'] as $mf_data) {
						$user_id = $fb_to_user_id_map[$mf_data['fb_id']];
						if (!isset($mutual_friends_data[$user_id]))
							$mutual_friends_data[$user_id] = array();
						$mutual_friends_data[$user_id]['fb_count'] = $mf_data['fb_count'];
						$mutual_friends_data[$user_id]['tm_count'] = $mf_data['tm_count'];
					}
					
				}
			}
			
			// No Response from python script 
			else {
				$event_info['responseCode'] = 195; // No reponse
			}
			
			// logging
			$this->log_mutual_friends_action($input_user_id, "mutual_friends_script", $status, $event_info, $time_elapsed_secs);
		}
		
		return $mutual_friends_data;
	}

	
	private function log_mutual_friends_action($user_id, $event_type, $status, $event_info, $time_taken) {
		$data_logging = array();
		$data_logging [] = array("user_id" => $user_id, "actvity" => "fb_new" , "event_type" => $event_type, 
			"event_status" => $status, "event_info" => $event_info, "time_taken" => $time_taken);
		$this->event_tracker->logActions($data_logging);
	}
	
}

?>



