from facepy import *
from threading import Thread
import threading
import time
import sys

import config

# This fn takes 50 fb-ids as input and makes batch call to fb
# sets the status in case of error
def get_mutual_friends_data_for_batch(user_id, fb_ids, graph, results):
	requests = []
	global status;
	for fb_id in fb_ids:
		requests.append({'method':'GET', 'relative_url':fb_id + '/?fields=context.fields(mutual_friends)'})
	
	try:	
		batch = graph.batch(requests = requests)
		for response in batch:
			if isinstance(response, dict) and response.has_key('id'):
				count_fb, count_tm = 0, 0
				if has_mutual_friends_on_fb(response):
					count_fb = response['context']['mutual_friends']['summary']['total_count']
					if has_mutual_friends_on_tm(response):
						count_tm = len(response['context']['mutual_friends']['data'])
				results.append({"fb_id" : str(response['id']), "fb_count" : count_fb, "tm_count" : count_tm})
		return results
	except OAuthError:
		status = config.ACCESS_TOKEN_ERROR
	except HTTPError:
		status = config.HTTP_ERROR
	except SignedRequestError:
		status = config.SIGNED_REQUEST_ERROR
	except InternalFacebookError:
		status = config.INTERNAL_FACEBOOK_ERROR
	except Exception:
		status = config.OTHER_ERROR


def has_mutual_friends_on_fb(response):
	if response.has_key('context') and response['context'].has_key('mutual_friends') and \
			response['context']['mutual_friends'].has_key('summary') and \
			response['context']['mutual_friends']['summary'].has_key("total_count"):
		return True
	return False


def has_mutual_friends_on_tm(response):
	if has_mutual_friends_on_fb(response) and response['context']['mutual_friends'].has_key('data'):
		return True
	return False
		

def get_mutual_friends_data_in_thread(user_id, fb_ids, graph):
	results = []
	thread = Thread(target = get_mutual_friends_data_for_batch, args = (user_id, fb_ids, graph, results,))
	return (thread, results)


def get_mutual_friends_data(user_id, fb_ids, graph):
	results = []
	# Creating Threads - Each Thread takes 50 fb-ids to be processed
	threads = [get_mutual_friends_data_in_thread(user_id, fb_ids[i : i + config.BATCH_SIZE], graph) \
			for i in xrange(0, len(fb_ids), config.BATCH_SIZE)]
	if threads is not None and len(threads) > 0:
		for thread in threads:
			# Setting the thread as Daemon, so that script execution ends after the timeout
			thread[0].setDaemon(True)
			thread[0].start()
		for thread in threads:
			thread[0].join(config.THREAD_TIMEOUT)
			#if thread[0].isAlive():
			#	print "A thread alive after timeout";
			results += thread[1]
	return results


if len(sys.argv) != 5:
	print "Usage: python get_fb_mutual_friends_data.py user_id access_token comma_separated_list_of_fb_ids app_secret"
	sys.exit(1)
user_id, access_token, fb_ids, app_secret = int(sys.argv[1]), sys.argv[2], sys.argv[3].split(','), sys.argv[4]
graph = GraphAPI(access_token, appsecret = app_secret, version = config.GRAPH_API_VERSION)
status = 200
responses = get_mutual_friends_data("", fb_ids, graph)
if status == 200:
	responses = "{'status': " + str(status) + ", 'data':" + str(responses) + "}";
else:
	responses = "{'status': " + str(status) + "}";
responses = responses.replace("'", '"')
print responses
sys.exit()

