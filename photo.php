<?php
include_once (dirname ( __FILE__ ) . '/include/config.php');
include_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . "/include/header.php");
include_once (dirname ( __FILE__ ) . '/photomodule/Photo.class.php');
require_once (dirname ( __FILE__ ) . '/TrustBuilder.class.php');
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/abstraction/query_wrapper.php";

//for tracking
if(isset($_REQUEST['utm_campaign'])){
	//if($_REQUEST['utm_content'] == "view_matches")
	$event_type = $_REQUEST['utm_campaign'] ;
	$data[] = array("user_id" => $_REQUEST['uid'], "activity" => "emailers_click" , "event_type" => $event_type);
	$eventTrk = new EventTrackingClass();
	$eventTrk->logActions($data);
}

$func=new functionClass();
$session = functionClass::getUserDetailsFromSession ();
$user_id = $session['user_id'];
$login_mobile = $func->isMobileLogin();
$app_version = $func->getHeader();
$is_video_profile=false;
$video_hash=null;
$video_params=null;
if(!isset($user_id) && isset($_SESSION['user_id_admin'])){
	$user_id = $_SESSION['user_id_admin'];
}
if(isset($_REQUEST['caller']) && $_REQUEST['caller']=='admin' && isset($_SESSION['admin_id'])){
	//$photoModule = new Photo($_REQUEST['user_id']);
	$user_id = $_REQUEST['user_id'];
	$_SESSION['user_id_admin'] = $_REQUEST['user_id'];
	//$_SESSION['id'] = "admin";
}

//$photoModule = new Photo($session['user_id']);
$photoModule = new Photo($user_id);
if(isset($_REQUEST['video_profile_enabled'])&&$_REQUEST['video_profile_enabled']==true)
{
	//Hellos
	$is_video_profile=true;
	$video_hash=$photoModule->getHash();
	$device_name='';
	if(isset($_REQUEST['device_name'])) {
		$device_name=$_REQUEST['device_name'];
	}
	$video_params=$photoModule->getVideoParams($device_name);
}
//if(isset($session['user_id'])&&$_POST['action']=='saveFb') {
if(isset($user_id)&&$_POST['action']=='saveFb') {
	$datas=$_POST['data'];
	foreach($datas as $data) {
		$values=explode(" ",$data);
		if($values[1]<200||$values[2]<200)
			continue;
		//	public function addFromComputer($withoutcrop=true,$fb_url=null,$dont_return=false,$save_as_profile=null) {
		
		$photoModule->addFromComputer(false,$values[0],true);
		//$photoModule->saveImageFromFB($values[0],$values[1],$values[2]);
		//trigger_error("saved photo $values[0],$values[1],$values[2]",E_USER_WARNING);
	}
	$encoded_data = $photoModule->getUserPhotos();
	$video_data = $photoModule->getUserVideos();//

	if(!isset($encoded_data)){
		$encoded_data = array();
	}
	if($login_mobile){
		if($app_version<37)
			print_r(json_encode($encoded_data));
		else{
			print_r(json_encode(array("responseCode"=>200,"data"=>$encoded_data,'video_data'=> $video_data)));
		}
	}else{
		print_r(json_encode($encoded_data));
	}
	//print_r(json_encode($photoData));
	die;
}

//if(isset($session['user_id'])&&$_POST['action']=='add_from_computer') {
if(isset($user_id)&&$_POST['action']=='add_from_computer') {
	if($_POST['crop']=='no'){
		$encoded_data = $photoModule->addFromComputer(false);      //upload photo from trustbuilder page
	}else
		$encoded_data = $photoModule->addFromComputer();
       /* added by rajesh to add response code*/ 
	if(!isset($encoded_data)){
                $encoded_data = array();
        }
        if($login_mobile){
                if($app_version<56)
                        print_r(json_encode($encoded_data));
                else{
					$video_data = $photoModule->getUserVideos();
					print_r(json_encode(array("responseCode"=>200,"data"=>$encoded_data,"videos"=>$video_data)));
                }
        }else{
                print_r(json_encode($encoded_data));
        }
	die;
}
//if(isset($session['user_id'])&&$_REQUEST['action']=='display_pics') {
if(isset($user_id)&&$_REQUEST['action']=='display_pics') {
	$encoded_data = $photoModule->getUserPhotos();
	if(!isset($encoded_data)){
		$encoded_data = array();
	}
	if($login_mobile){
		if($app_version<35)
			print_r(json_encode($encoded_data));
		else{
			/*
			 * photo be allowed to skip or not based on trustbuilder
			 */
			
			if(count($encoded_data)>0) {
				$allowSkip = true;
			}else {
				$trust=new TrustBuilder($user_id);
				$allowSkip = $trust->allowSkipPhoto();
			}
			$video_data = $photoModule->getUserVideos();
			$output=array("responseCode"=>200,"data"=>$encoded_data,"allowSkip"=>$allowSkip,"videos"=>$video_data);
			$resource=$output;			
			unset($resource['responseCode']);
			$resource=json_encode($resource);
			$hashCheck=functionClass::checkHash($resource,$_REQUEST['hash']);
			if($hashCheck['status']==false)
			    $output['hash']=$hashCheck['hash'];
			else
				$output=array("responseCode"=>304);

			if($is_video_profile)
			{
				$output['video_hash']=$video_hash;
				$output['video_params']=$video_params;
			}

		    $output["tstamp"]=time();
			print_r(json_encode($output));
		}
	}else{
		print_r(json_encode($encoded_data));
	}
	die;
}

//if(isset($session['user_id'])&&$_POST['action']=='add_from_computer_without_file_reader_api') {
if(isset($user_id)&&$_POST['action']=='add_from_computer_without_file_reader_api') {
	//$photoModule->addFromComputerWithOutFileReaderAPI();
	//$photoModule->addFromComputer(false);
	
	$encoded_data = $photoModule->addFromComputer(false);
	$video_data = $photoModule->getUserVideos();
	if(!isset($encoded_data)){
		$encoded_data = array();
	}
	if($login_mobile){
		if($app_version<56)
			print_r(json_encode($encoded_data));
		else{
			print_r(json_encode(array("responseCode"=>200,"data"=>$encoded_data,"videos"=>$video_data)));
		}
	}else{
		print_r(json_encode($encoded_data));
	}
	die;
}
//var_dump($_POST);
if( isset($user_id) && $_POST['action'] == 'edit_photo' && isset($_POST['photo_id']) ) {
	
		//$photoModule->addFromComputerWithOutFileReaderAPI();
	//$photoModule->addFromComputer(false);
	$old_pic_data = $photoModule->checkOnePhoto($_POST['photo_id']) ;
	if (count($old_pic_data) > 0 )
	{
		//print_r(json_encode($old_pic_data));
		 $is_profile =  $old_pic_data ['is_profile'] ;
		 
		 if ($old_pic_data ['status'] == 'temp_active')
		 {
		 	$photo_id_mapped = $old_pic_data ['mapped_to'];
		 	$photo_id_changed = $_POST['photo_id'] ;	
		 	$photoModule->updatePhotoStatus($photo_id_changed, 'changed') ;

		 }
		 else 
		 { 
		 	$photo_id_mapped = $_POST['photo_id'] ;		
		 	$photo_id_changed = $_POST['photo_id'] ;
		 }
		 
		 if ( ($old_pic_data ['status'] == 'active'  && $old_pic_data ['admin_approved'] ==  'yes') || $old_pic_data ['status'] == 'temp_active' )
		 {
		 	$encoded_data = $photoModule->addFromComputer(false, null, false, $is_profile, null, null, null, $photo_id_mapped);
		 }
		 else
		 {
		 	$photoModule->updatePhotoStatus($photo_id_changed, 'changed') ; 
		 	$encoded_data  = $photoModule->addFromComputer(false, null, false, $is_profile, null, null, null);
		 }
		 
		 
	}

	if(!isset($encoded_data))
	{
		$encoded_data = array();
	}
	$video_data = $photoModule->getUserVideos();
	print_r(json_encode(array("responseCode"=>200,"data"=>$encoded_data,"videos"=>$video_data)));
	
	die;
} 

//if(isset($session['user_id'])&&$_POST['action']=='save') {
if(isset($user_id)&&$_POST['action']=='save') {
	$encoded_data = $photoModule->saveImage();
	$video_data = $photoModule->getUserVideos();
	if(!isset($encoded_data)){
		$encoded_data = array();
	}
	if($login_mobile){
		if($app_version<37)
			print_r(json_encode($encoded_data));
		else{
			print_r(json_encode(array("responseCode"=>200,"data"=>$encoded_data,"videos"=>$video_data)));
		}
	}else{
		print_r(json_encode($encoded_data));
	}
	die;
}

//if(isset($session['user_id'])&&$_POST['action']=='delete') {
if(isset($user_id)&&$_POST['action']=='delete') {
	$encoded_data = $photoModule->deleteImage();
	$video_data = $photoModule->getUserVideos();
	if(!isset($encoded_data)){
		$encoded_data = array();
	}
	if($login_mobile){
		if($app_version<37) 
			print_r(json_encode($encoded_data));
		else{
			print_r(json_encode(array("responseCode"=>200,"data"=>$encoded_data,"videos"=>$video_data)));
		}
	}else{
		print_r(json_encode($encoded_data));
	}
	die;
}

//if(isset($session['user_id'])&&$_POST['action']=='set_as_profile') {
if(isset($user_id)&&$_POST['action']=='set_as_profile') {
	$encoded_data = $photoModule->saveAsProfileImage();
	$video_data = $photoModule->getUserVideos();
	if(!isset($encoded_data)){
		$encoded_data = array();
	}
	if($login_mobile){
		if($app_version<37)
			print_r(json_encode($encoded_data));
		else{
			print_r(json_encode(array("responseCode"=>200,"data"=>$encoded_data,"videos"=>$video_data)));
		}
	}else{
		print_r(json_encode($encoded_data));
	}
	die;
}

//if(isset($session['user_id'])&&$_POST['action']=='check') {
if(isset($user_id)&&$_POST['action']=='check') {
	//$rs=$conn->Execute("select * from user_photo where user_id='".$session['user_id']."' and is_profile='yes'");
	$rs=$conn->Execute("select * from user_photo where user_id='".$user_id."' and is_profile='yes'");

	if($rs->_numOfRows>0)
		print_r(json_encode(array("status"=>'ok')));
	else print_r(json_encode(array("status"=>'not ok')));
	die;
}

//if(isset($session['user_id'])&&$_POST['action']=='get_new') {
if(isset($user_id)&&$_POST['action']=='get_new') {
	$photoData = $photoModule->getUserPhotos();
	print_r(json_encode($photoData));
	die;
}

//if(isset($session['user_id'])&&$_POST['action']=='safariSubmit'){
if(isset($user_id)&&$_POST['action']=='safariSubmit'){
	$name=1000*microtime(true)."_".$func->random(18);
	$name_thumbNail=$name."_".$func->random(10);
	try{
		if($_FILES["file"]["size"] < 5242880){
			$extension = end(explode(".", $_FILES["file"]["name"]));
			if ($_FILES["file"]["error"] > 0) {
				$error = 1;
			} else {
				move_uploaded_file($_FILES["file"]["tmp_name"], "files/images/profiles/" . $name . '.' . $extension);
				list($width, $height) = getimagesize("files/images/profiles/" . $name . '.' . $extension);
				$im=ImageCreateFromJPEG("files/images/profiles/".$name.".".$extension);
				$newimage=imagecreatetruecolor(100,100);
				imagecopyresampled($newimage,$im,0,0,0,0,100,100,$width,$height);
				ImageJpeg($newimage,"files/images/profiles/".$name_thumbNail.".jpg");
				//$conn->Execute($conn->prepare("insert into user_photo (user_id,name,thumbnail,time_of_saving,is_profile,width,height,is_moderated,from_fb,status) values(?,?,?,Now(),?,?,?,?,?,'under_moderation')"),array($session['user_id'],$name.".jpg",$name_thumbNail.".jpg",$is_profile,$width,$height,'no','no'));
				$query="insert into user_photo (user_id,name,thumbnail,time_of_saving,is_profile,width,height,is_moderated,from_fb,status) values(?,?,?,Now(),?,?,?,?,?,'under_moderation')";
				$param_array=array($user_id,$name.".jpg",$name_thumbNail.".jpg",$is_profile,$width,$height,'no','no');
				$tablename='user_photo';
				Query_Wrapper::INSERT($query, $param_array, $tablename);
				//$conn->Execute($conn->prepare("insert into user_photo (user_id,name,thumbnail,time_of_saving,is_profile,width,height,is_moderated,from_fb,status) values(?,?,?,Now(),?,?,?,?,?,'under_moderation')"),array($user_id,$name.".jpg",$name_thumbNail.".jpg",$is_profile,$width,$height,'no','no'));
				Utils::saveOnS3(dirname ( __FILE__ )."/files/images/profiles/".$name.".jpg","files/images/profiles/".$name.".jpg");
				Utils::saveOnS3(dirname ( __FILE__ )."/files/images/profiles/".$name_thumbNail.".jpg","files/images/profiles/".$name_thumbNail.".jpg");
			}
		}

		$photoData = $photoModule->getUserPhotos();
		print_r(json_encode($photoData));
	}catch (Exception $e){
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	}
	die;
}

if(isset($_SESSION['admin_id'])){}else{
functionClass::redirect ( 'photo',$login_mobile );
}
try{
	//$tmObject = new TMObject();
	//$header= $tmObject->getHeaderValues($session['user_id']);
	//$header= $tmObject->getHeaderValues($user_id);
	$header = new header($user_id);
	$header_values = $header->getHeaderValues();
	
	$photodata = $photoModule->getUserPhotos();
	
	if($photodata){
		$smarty->assign("photos",json_encode($photodata));
		$smarty->assign("profile_photo_exist",$photoModule->checkProfileExist($photodata));
	}elseif($data['is_fb_connected']=='Y'){
		$fbPic = $photoModule->getUserFbPic();
		if($fbPic){
			$smarty->assign('incomplete',true);
			$smarty->assign('fb_profile',json_encode($fbPic));
		}
	}
	$smarty->assign("header",$header_values);
	$smarty->assign("edit_photo",true);
	$smarty->assign("editProfile","editProfile");
	$smarty->assign("uid",$user_id);
	$smarty->display ("templates/registration/photo.tpl" ) ;
}catch (Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}
?>
