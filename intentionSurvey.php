<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/abstraction/query.php";
require_once dirname ( __FILE__ ) . "/include/function.php";


/*To Store the user response (answers , no. of times clicked) from intention survey shown to users 
 * @Sumit*/
class intentionSurvey 
{
	private $user_id;
	
	function __construct($user_id) 
	{
		$this->user_id = $user_id ;
	}
	
    public function saveAnswer ( $question_id, $answer)
    {
    	$sql = "insert into  user_intention_survey (user_id, question_id, answer) values (?, ?, ?) on duplicate key update answer = ? ";
    	Query::INSERT($sql, array($this->user_id, $question_id, $answer, $answer));
    }
    
    public function saveCancel ( $question_id )
    {
    	$sql = "insert into user_intention_survey (user_id, question_id, cancel_count ) values (?, ?, ?) on duplicate key update cancel_count = cancel_count + ? ";
    	Query::INSERT($sql, array($this->user_id, $question_id, 1, 1));
    }
    
    public function checkSurvey ( $question_id )
    {
    	$sql = "select count(*) as c_u from user_intention_survey where user_id = ? and question_id = ? and answer is not null ";
    	$data = Query::SELECT($sql, array($this->user_id, $question_id), Query::$__slave, true);
    
    	if ($data['c_u'] > 0)
    	return true ;
    	else 
    	return false;
    }
}

try 
{
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user['user_id'];
	
	if (isset($user_id))
	{
		$intObject = new intentionSurvey($user_id);

		if (is_numeric( $_REQUEST ['question_id']) == true )
		{
			$output["responseCode"] = 200;

			if ( $_REQUEST ['cancelled'] == true )
			{
				$intObject->saveCancel($_REQUEST ['question_id']);
			}
			else if ( $_REQUEST ['answer']) 
			{
				$intObject->saveAnswer($_REQUEST ['question_id'], $_REQUEST ['answer']);
			}
			else if ( $_REQUEST ['get_status'] == true )
			{
				$output ['play_status'] = $intObject->checkSurvey($_REQUEST ['question_id']);
			}
			else 
			{
				$output ['error'] = "Unknown Request" ;
			}
		}
		else
		{
			$output["responseCode"] = 403 ;
		}
	}
	else
	{
		$output["responseCode"] = 401;
	}
	
	print_r(json_encode($output));
}
catch (Exception $e) 
{
	trigger_error($e->getMessage(), E_USER_WARNING);
}
?>