<?php
require_once dirname (__FILE__) ."/../include/config_admin.php";
require_once dirname (__FILE__) ."/../include/Utils.php";
include dirname(__FILE__)."/moEngageServerCall.php";



/*
 * This sends the data to Moengage to update various segments to which the notifications are sent
 * Author :: Sumit
 * Live Long and Prosper
 */

class notificationSegments
{
    private $conn_reporting ;
    private $conn_master ;
    private $registered_days_limit ;
    private $array_chunk_size ;
    private $moengage ;
    private $new_table ;
    private $not_chatted_table ;

    function __construct()
    {
        global $conn_reporting, $conn_master ;
        $this->conn_reporting = $conn_reporting ;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->conn_master = $conn_master ;
        $this->registered_days_limit = 30;
        $this->array_chunk_size = 100 ;
        $this->moengage = new moEngageServerCall() ;
        $this->new_table = "user_not_chatted_moengage_new";
        $this->not_chatted_table = "user_not_chatted_moengage";
    }

    public function updateMoEngage()
    {
        $this->updateRecentRegisteredUsers();
        $this->updateChatStatus() ;
        //$this->updateProfilePics() ;
        //$this->updateUninstallAndDeleted();
    }

    private function splitIntoChunkAndSend($sqlObject)
    {
        global $baseurl ;
        $data_arr = array() ;
        $i = 0 ;
        $all_count = 0 ;
        $rowCounts = $sqlObject->RowCount();

        while($row = $sqlObject->FetchRow())
        {
            $attributes = array() ;
            if($row['user_status'])  $attributes['user_status'] = $row['user_status'] ;
            if($row['app_status']) $attributes['app_status'] = $row['app_status'] ;
            if($row['chat_status']) $attributes['chat_status'] = $row['chat_status'] ;
            if($row['user_deletion_status']) $attributes['user_deletion_status'] = $row['user_deletion_status'] ;
            if($row['days_registered']) $attributes['days_registered_count'] = (int)$row['days_registered'] ;
            if($row['created_time']) $attributes['created_time'] = (int)$row['created_time'] ;
            if($row['profile_pic_status']) $attributes['profile_pic_status'] = $row['profile_pic_status'] ;
            if($row['rejected_once']) $attributes['rejected_once'] = $row['rejected_once'] ;

            $user_data = array("type" => "customer",
                            "customer_id" => $row['user_id'],
                            "attributes" => $attributes ) ;
            $data_arr[] = $user_data ;

            $i++ ;
            $all_count++ ;

            if($i >= $this->array_chunk_size || $all_count == $rowCounts  )
            {
                // make the call to server with current chunk of data and reset the counter and array
                //  make the call here with authentication .

                $payload = array();
                $payload["type"] = "transition" ;
                $payload["elements"] = $data_arr ;
                $payload = json_encode($payload); echo "making call to moengage" . PHP_EOL ;
                $result = $this->moengage->makeCallToMoEnagege($payload) ;
                $result_arr = json_decode($result,true) ;
                if($result_arr["status"] != "success")
                {
                    $to = "sumit@trulymadly.com,shashwat@trulymadly.com";
                    $from = "sumit@trulymadly.com";
                    Utils::sendEmail($to, $from, "Moengage server call didn't return success. Please try again " . $baseurl , "Json Response ". $result ) ;
                    die ;
                }
                else
                {
                    echo $result;
                }

                // now set the array to empty and reset the counter
                $i = 0 ;
                $data_arr = array() ;
            }
        }
    }



    private function updateChatStatus()
    {
        $this->createNewNotChattedTable();
        $this->fillNewTable();
        //$this->syncDataToMoengage() ;
        $this->updateYesterday();
        $this->updateToday();
        $this->replaceOldTable() ;
    }

    private function createNewNotChattedTable()
    {
        $drop_table = "DROP TABLE IF EXISTS $this->new_table";
        $this->conn_master->Execute($drop_table);

        $create_table = "CREATE TABLE $this->new_table (  user_id int(11) NOT NULL,
                                                    tstamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                                    PRIMARY KEY (user_id) ) ";
        $this->conn_master->Execute($create_table);
    }

    private function fillNewTable()
    {
        $sql = "select user_id from  (select u.user_id, if(ca = cm,'not_chatted','chatted') as chat_status,  uas.android_app as app_status,
                if(deletion_status is null,'active','deleted') as user_deletion_status from ( select sender_id, count(receiver_id) as ca,
                sum(if(msg_type = 'MUTUAL_LIKE',1,0)) as cm FROM messages_queue mq where DATEDIFF(NOW(),mq.tstamp) < 7 group by sender_id )t
                join user u on u.user_id = t.sender_id
                join user_app_status uas on u.user_id = uas.user_id
                where u.status='authentic' and uas.android_app='install')t where chat_status = 'not_chatted'";
        $sql_prep = $this->conn_reporting->Prepare($sql);
        $sql_exec = $this->conn_reporting->Execute($sql_prep);
       // echo $sql_exec->RowCount(); die;
        $this->conn_master->bulkBind = true;
        $sql_insert = "INSERT INTO $this->new_table (user_id) values (?)" ;
        $sql_insert_prep = $this->conn_master->Prepare($sql_insert);

        while($data = $sql_exec->GetRows(1000))
        {
            $this->conn_master->Execute($sql_insert_prep,$data) ;
        }
    }

    private function syncDataToMoengage()
    {
        $sql = "select u.user_id , if(u1.user_id is null, 'not_chatted','chatted') as chat_status from
                (select user_id from user_not_chatted_moengage
                  union
                 select user_id from user_not_chatted_moengage_new)u
                 left join user_not_chatted_moengage u1 on u.user_id =u1.user_id
                 left join user_not_chatted_moengage_new u2 on u.user_id =u2.user_id
                 where u1.user_id is null or u2.user_id is null";
        $sql_prep = $this->conn_reporting->Prepare($sql) ;
        $sql_exec = $this->conn_reporting->Execute($sql_prep) ;
        $this->splitIntoChunkAndSend($sql_exec);
    }

    private function updateYesterday()
    {
        $sql = "select user_id, 'null' as chat_status from user_not_chatted_moengage" ;
        $sql_prep = $this->conn_reporting->Prepare($sql) ;
        $sql_exec = $this->conn_reporting->Execute($sql_prep) ;
        $this->splitIntoChunkAndSend($sql_exec);
    }

    private function updateToday()
    {
        $sql = "select user_id, 'not_chatted' as chat_status from user_not_chatted_moengage_new" ;
        $sql_prep = $this->conn_reporting->Prepare($sql) ;
        $sql_exec = $this->conn_reporting->Execute($sql_prep) ;
        $this->splitIntoChunkAndSend($sql_exec);
    }



    private function replaceOldTable()
    {
        $sql_delete = "DELETE FROM $this->not_chatted_table where user_id not in (SELECT user_id from $this->new_table )";
        $this->conn_master->Execute($sql_delete) ;

        $sql_insert_new = "INSERT IGNORE INTO $this->not_chatted_table SELECT * FROM $this->new_table ";
        $this->conn_master->Execute($sql_insert_new) ;
    }

    private function updateRecentRegisteredUsers()
    {
        $sql = "select u.user_id,  u.status as user_status , uas.android_app as app_status, if(deletion_status is null,'active','deleted') as user_deletion_status, "
            ." DATEDIFF(NOW(),u.registered_at) as days_registered from user u join user_app_status uas on u.user_id = uas.user_id "
            ."where  DATEDIFF(NOW(),u.registered_at) < $this->registered_days_limit";
        $sql_prep  = $this->conn_reporting->Prepare($sql) ;
        $sql_exec = $this->conn_reporting->Execute($sql_prep,array()) ;
        $this->splitIntoChunkAndSend($sql_exec) ;
    }

    private function updateProfilePics()
    {
        $sql = "select u.user_id,  u.status as user_status , uas.android_app as app_status, if(deletion_status is null,'active','deleted') as "
                ."user_deletion_status, DATEDIFF(NOW(),u.registered_at) as days_registered, if(up.user_id is null ,'no','yes') as profile_pic_status,"
                ."if(time_of_saving > approval, 'yes','no') as rejected_once from user u join user_app_status uas on u.user_id = uas.user_id "
                ."left join user_photo up on up.user_id = u.user_id and up.is_profile ='yes' and up.status ='active' "
                ."left join (select user_id, MIN(approval_time) AS approval from user_photo where status='rejected' and DATEDIFF(NOW(),approval_time) < 7 group by user_id )t "
                ."on u.user_id =t.user_id where  uas.android_app ='install'";
        $sql_prep = $this->conn_reporting->Prepare($sql) ;
        $sql_exec = $this->conn_reporting->Execute($sql_prep) ;
        $this->splitIntoChunkAndSend($sql_exec) ;
    }

    private function updateUninstallAndDeleted()
    {
        $sql = "select u.user_id ,uas.android_app as app_status, if(deletion_status is null,'active','deleted') as user_deletion_status "
                ."from user u join user_app_status uas on u.user_id = uas.user_id where DATEDIFF(NOW(),tstamp) < 2 or DATEDIFF(NOW(),last_row_updated) < 2 " ;
        $sql_prep = $this->conn_reporting->Prepare($sql) ;
        $sql_exec = $this->conn_reporting->Execute($sql_prep) ;
        $this->splitIntoChunkAndSend($sql_exec) ;
    }


}


try
{
    $segments = new notificationSegments();
    $segments->updateMoEngage();
}
catch(exception $e)
{
    trigger_error("Moengage server API error : ".$e->getMessage(),E_USER_WARNING) ;
    echo $e->getMessage();
    Utils::sendEmail('sumit@trulymadly.com,shashwat@trulymadly.com', 'sumit@trulyamdly.com', "Moengage server call didn't return success. Please try again " . $baseurl ,
        "Moengage server API error : ". $e->getMessage()) ;
}



?>