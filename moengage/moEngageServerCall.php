<?php
require_once dirname(__FILE__)."/../include/config_admin.php";

/*
 * To make the call to moEngage server for Data API
 * Author :: Sumit
 *  Live Long and Prosper
 */

class moEngageServerCall
{
    private $moengage_app_id ;
    private $moengage_secret_key ;
    private $api_type ;
    private $moengage_url ;

    function __construct($api_type ='transition' )
    {
        global $moengage_app_id, $moengage_secret_key ;
        $this->moengage_app_id  = $moengage_app_id;
        $this->moengage_secret_key = $moengage_secret_key;
        $this->api_type = $api_type ;
        $this->moengage_url = "https://api.moengage.com/v1/" . $this->api_type . "?app_id=" . $this->moengage_app_id ;
    }

    public function makeCallToMoEnagege($payload)
    {
        $process = curl_init($this->moengage_url);
        curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($process, CURLOPT_HEADER, 1);
        curl_setopt($process, CURLOPT_USERPWD, $this->moengage_app_id . ":" . $this->moengage_secret_key);
        curl_setopt($process, CURLOPT_TIMEOUT, 60);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($process);
        $body=substr($result,curl_getinfo($process,CURLINFO_HEADER_SIZE));
        curl_close($process);
        return $body ;
    }


}



?>