<?php

 
#ini_set('display_errors','on'); 
//error_reporting(0);
error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_DEPRECATED);

/*$visitor = $_SERVER['REMOTE_ADDR']; 
if (preg_match("/122.162.26.107/",$visitor) || preg_match("/122.176.138.46/",$visitor)) {
	//header('Location:  http://www.trulymadly.com');
} else {
	header('Location:  http://www.trulymadly.com/templates/comingsoon.html');
}
*/


if(isset($_COOKIE['PHPSESSID']))
session_start();

date_default_timezone_set('UTC');
//echo dirname(__FILE__).'/../lib/adodb5/adodb.inc.php'; 
//global $config,$conn,$smarty;

// $config = array(
// "database"=>array(
// "hostname"=>"localhost",
// "username"=>"robin",
// "password"=>"",
// "database"=>"trulymadly"
// )
// );

$config_ini_file='/var/www/html/config/config.ini';
$config=parse_ini_file($config_ini_file,true);



if(array_key_exists("env",$config) && $config['env']['type']=='dev'){
   $config_to_add = preg_match("!^\/(.*?)\/!",$_SERVER['REQUEST_URI'],$matches);
  if(count($matches)>=1)
   $config = parse_ini_file($config_ini_file.'.'.$matches[1],true);

	//print_r($config);
}


$config_ab=parse_ini_file(dirname(__FILE__).'/config_ab.ini',true);
$config_ignore_user_ids=parse_ini_file(dirname(__FILE__).'/config_ignore_ad_campaigns.ini',true);
$config_socials_recommendation_list=parse_ini_file(dirname(__FILE__).'/config_socials_recommendation_list.ini',true);


require_once dirname(__FILE__).'/../lib/adodb5/adodb.inc.php';
require_once dirname(__FILE__).'/../lib/adodb5/adodb-exceptions.inc.php';


$DBHOST = $config["database"]["hostname"];
$DBUSER =$config["database"]["username"];
$DBPASSWORD =$config["database"]["password"];
$DBNAME =$config["database"]["database"];



$DBTYPE = 'mysqlt';
$conn = ADONewConnection($DBTYPE);
$conn->PConnect($DBHOST,$DBUSER,$DBPASSWORD,$DBNAME);

$push_notification_temp_file = $config['redis_temporary_file_paths']['push_notification']; 
$mailchimp_temp_file =  "/tmp/mail_chimp.txt" ;

$freebase_key = $config['google_freebase']['key'];
//$google_api_url = $config['google_freebase']['api_url'];
$google_api_url = "https://www.googleapis.com/urlshortener/v1/url";
$windows_pushnotification_baseurl = "https://sin.notify.windows.com/?token=";
$windows_pushnotification_clientId = $config['windows']['push_notification_client_id'];
$windows_pushnotification_clientSecret = $config['windows']['push_notification_client_secret']; 
$windows_pushnotification_grant = "client_credentials";
$windows_pushnotification_scope = "notify.windows.com";
$windows_pushnotification_url   = "https://login.live.com/accesstoken.srf";
$conn_master = $conn;
$conn_slave = $conn;
$admin_id = $config["admin"]["admin_id"];
$miss_tm_id=$config["admin"]["miss_tm_id"];
$redis_db_odd = 2;
$redis_db_even = 3; 
$redis_db_userActivity = 4;

/*$redis_db_odd = $config['redis_databases']['db1'];
$redis_db_even = $config['redis_databases']['db2'];*/
/*$DBHOST_master = $config["database_master"]["hostname"];
$DBUSER_master =$config["database_master"]["username"];
$DBPASSWORD_master =$config["database_master"]["password"];
$DBNAME_master =$config["database_master"]["database"];

$DBTYPE_master = 'mysqlt';
$conn_master = &ADONewConnection($DBTYPE_master);
$conn_master->PConnect($DBHOST_master,$DBUSER_master,$DBPASSWORD_master,$DBNAME_master);

$DBHOST_slave = $config["database_slave"]["hostname"];
$DBUSER_slave =$config["database_slave"]["username"];
$DBPASSWORD_slave =$config["database_slave"]["password"];
$DBNAME_slave =$config["database_slave"]["database"];

$DBTYPE_slave = 'mysqlt';
$conn_slave = &ADONewConnection($DBTYPE_slave);
$conn_slave->PConnect($DBHOST_slave,$DBUSER_slave,$DBPASSWORD_slave,$DBNAME_slave);*/

//print_r($rs->GetRows());die;
//$cliLogDebug=0;
//set execption mode 
//$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
//$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

//$header=array();
//require_once dirname(__FILE__)."/SupportFunctions.php";
//SupportFunctions::config_parseHeader();
//echo dirname(__FILE__)."/../lib/smarty/libs/Smarty.class.php";die;
 
//$baseurl=$config["urls"]["baseurl"];//"http://dev.trulymadly.com/trulymadly";
$image_basedir=$config["urls"]["image_basedir"];//"http://dev.trulymadly.com/trulymadly";

$baseurl_https = $config["urls"]["baseurl_https"];//"https://dev.trulymadly.com/trulymadly";
$admin_baseurl = $config["urls"]["admin_baseurl"];

if((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')||(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO']=='https')){
	$baseurl=$config["urls"]["baseurl_https"];
	$cdnurl=$config["urls"]["cdnurl_https"];;//"https://dev.trulymadly.com/trulymadly";
	$imageurl = $config["urls"]["imageurl_https"];;//'https://dev.trulymadly.com/trulymadly/files/images/profiles/';
	$server_url=$baseurl_https;
	$short_url=$config["urls"]["short_url"];
}else{
	$baseurl=$config["urls"]["baseurl"];
	$cdnurl=$config["urls"]["cdnurl"];;//"http://dev.trulymadly.com/trulymadly";
	$imageurl = $config["urls"]["imageurl"];;;//'http://dev.trulymadly.com/trulymadly/files/images/profiles/';
	$server_url=$baseurl;
	$short_url=$config["urls"]["short_url"];
}

$emailReportingIds = $config['email_reporting']['ids'];
$techTeamIds = $config['techTeam']['ids'];
 
$dummy_male_image=$cdnurl."/images/um_dummy_male.png";
$dummy_male_image_new=$cdnurl."/images/um_dummy_male_new2.jpg";
$dummy_female_image=$cdnurl."/images/um_dummy_female.png";
$dummy_female_image_new=$cdnurl."/images/um_dummy_female_new3.jpg";

$dummy_female_no_pics=$cdnurl."/images/um_dummy_feamle_no_profile_pic.jpg";
$dummy_female_no_pics_old_version=$cdnurl."/images/um_dummy_female_no_profile_pic_old_version.jpg";
$dummy_unapproved_male_image= $cdnurl . "/images/dashboard/maledummyphoto.gif";
$dummy_unapproved_female_image = $cdnurl . "/images/dashboard/femaledummyphoto.gif";

$crash_report_url = $config["crash_report_mobile"]["crash_report_url"];

$push_notification_url = 'https://android.googleapis.com/gcm/send';
$push_notification_auth_key = $config["push_notification"]["push_notification_auth_key"];

$ios_notification_conn_url = $config["ios_notification"]["conn_url"];
$ios_notification_feedback_url = $config["ios_notification"]["feedback_url"];
$ios_notification_passphrase = $config["ios_notification"]["passphrase"];
$ios_notification_filepath = $config["ios_notification"]["filepath"];

$mailchimp_api_key = $config['mailchimp']['api_key'];
$mandrill_api_key = $config['mandrill']['api_key']; 

$infibuzz_url=$config['infibuzz']['url'];
$infibuzz_api_key=$config['infibuzz']['api_key'];
$registerJsonUrl = $cdnurl."/register_data.json?ver=15";
$registerNewJsonUrl = $cdnurl."/register_data_new.json?ver=3";
$registerSingaporeJsonUrl = $cdnurl."/register_data_sg.json?ver=1.3";
$registerIndonesiaJsonUrl = $cdnurl."/register_data_indonesia.json?ver=1.0";
$registerUSJsonUrl = $cdnurl."/register_data_US.json?ver=1.3";
$maxMindDB = $config["maxmind"]["dbpath"];
$maxMindDir = dirname($maxMindDB) ;
$linkedin_api = $config['linkedin']['api'];
$linkedin_secret = $config['linkedin']['secret'];

$md5_salt=$config['secrets']['salt'];

$video_cdn=$config['video_profile']['video_cdn'];
$video_s3_bucket=$config['video_profile']['s3_bucket'];
$aws_pipeline=$config['video_profile']['aws_pipeline'];
$vision_key=$config['video_profile']['vision_key'];
$video_prefix=$config['video_profile']['video_prefix'];
$video_identity_pool=$config['video_profile']['aws_identity_pool'];
$video_s3_output_prefix=$config['video_profile']['video_output_prefix'];

//
$googleClientId = $config['google_login']['client_id'] ;
$googleClientSecret = $config['google_login']['client_secret'] ;
$moengage_app_id = $config['moengage']['app_id'];
$moengage_secret_key = $config['moengage']['secret_key'];

$google_auth_url=$config['spark_payment']['google_auth_url'] ;
$google_refresh_token=$config['spark_payment']['google_refresh_token'] ;
$google_client_id=$config['spark_payment']['google_client_id'] ;
$google_client_secret=$config['spark_payment']['google_client_secret'];
$google_redirect_uri=$config['spark_payment']['google_auth_url'] ;
$google_package_name=$config['spark_payment']['google_package_name'];
$apple_pay_url=$config['spark_payment']['apple_pay_url'];
$paytmStatusUrl = $config['spark_payment']['paytm_status_url'];
$paytmSecretKey = $config['spark_payment']['paytm_secret_key'];
$paytmMID = $config['spark_payment']['paytm_mid'];


$googleProjectID = $config['bigquery_access']['project_id'];
$BQServiceAccountEmail = $config['bigquery_access']['email'];
$BQKeyFileLocation = $config['bigquery_access']['file_location'];
//
require_once dirname(__FILE__)."/../lib/smarty/libs/Smarty.class.php";
$smarty=new Smarty();
$smarty->setTemplateDir(dirname(__FILE__).'/../lib/smarty/smarty_dir/templates/');
$smarty->setCompileDir(dirname(__FILE__).'/../lib/smarty/smarty_dir/templates_c/');
$smarty->setConfigDir(dirname(__FILE__).'/../lib/smarty/smarty_dir/configs/');
$smarty->setCacheDir(dirname(__FILE__).'/../lib/smarty/smarty_dir/cache/');
$smarty->assign ("SERVER", $server_url);
$smarty->assign("fb_api",$config['facebook']['api']);
$smarty->assign("linkedin_api",$config['linkedin']['api']);
//$smarty->assign("facebook_scope",'email,user_photos,user_relationships,user_actions.music,user_actions.books,user_actions.video,user_friends,user_birthday,user_likes,user_videos,user_actions.video,user_location');

$smarty->assign("facebook_scope",'email,user_photos,user_relationships,user_actions.music,user_actions.books,user_actions.video,user_friends,user_birthday,user_likes,user_work_history,user_education_history,user_location');
$smarty->assign('cdnurl',$cdnurl);
$smarty->assign ("baseurl",$baseurl);
$smarty->assign("imageurl",$imageurl);
$smarty->assign ("baseurl_https",$baseurl_https);
$smarty->assign ("admin_baseurl",$admin_baseurl);
$smarty->assign ("google_analytics_code",$config['google_analytics']['google_analytics_code']);

$linkedin_minimum_connections=20;
$maxAddMore=4;
$smarty->assign ("linkedin_minimum_connections",$linkedin_minimum_connections);
/*$redis = array(
		'IP' =>$config['redis']['ip'], //'127.0.0.1',
		'PORT' => $config['redis']['port']  //6379
);
*/
$redis = new Redis();
$redis->connect($config['redis']['ip'], $config['redis']['port']);
//$smarty->testInstall();
////$smarty->debugging = true;
//$smarty->display("a.tpl");
//print_r($smarty);die;
//print_r($rs->GetRows());die;

?>
