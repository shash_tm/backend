<?php

class systemMsgs{

	public static $matches = array(
            "non-authentic" => "Psst! You need to be verified to use this feature. Do it now?",
            "non-authentic-like-hide-msg" => "Your matches won’t receive your ‘likes’ unless you are verified.",
            "first-tile-not-Profile-Pic" => "Oye! Upload a profile photo before we take this any further.",
            "first-tile-non-authentic" => "You do know that for you to start any interaction with your matches, you'll first have to verify yourself, right?",
            "first-tile-authentic-first-time"=>'You will need to "Like" or "Hide" a match to view the next match and just so you know the match won\'t get to know this unless both of you have liked each other!',
            "last-tile-non-authentic" => "Um! You need to ‘like’ or ‘hide’ the existing profiles shown to you. Only then can we refresh the set. Remember, you’ll only get a maximum of 20 profiles per day to review.",
            "last-tile-authentic" => "Check us back tomorrow for your next set of profiles!",
            //"last-tile-authentic-full-recommendation" => "Ahem! Come back later for your next set of matches.",
            "last-tile-authentic-full-recommendation" => "Ahem! Come back later for your next set of matches. Till then why don't you help spread the love?",
            //"last-tile-authentic-full-recommendation-female" => "Ahem! Come back later for your next set of matches.",
            "last-tile-authentic-full-recommendation-female" => "Ahem! Come back later for your next set of matches. Till then why don't you help spread the love?",
            "last-tile-authentic-less-recommendation" => "Oh no! We don't have more matches to show you as of now. Come back later, will ya? ",
            "last-tile-non-authentic-full-recommendation" => "Ahem! Come back tomorrow for your next set of matches. Till then how about you get yourself verified.",
            "last-tile-non-authentic-less-recommendation" => "Oho! We don't have more matches for you right now. How about you spend this time wisely and get yourself verified?",
            "no-matches" => "Oh no! We don't have any matches to show you as of now. Come back later, will ya? ");

        public static $profile = array(
            "popularity_tip" => "Popularity score reflects the number of likes you receive compared to other male users.",
            "non-authentic" => "Psst! You need to be verified to use this feature. Do it now?",
            "not_mutual_like" => "You can view the name and chat only when both of you have liked each other. Patience my dear, patience.");
								
        public static $suspendMessages = array(
            "heading" => "Please select a reason",
            "reasons" => array(
                "I found someone on TrulyMadly","I did not really find anyone interesting","I’m only interested in matrimony","Other"));
                /*"Not getting enough profiles","Quality of matches","Found someone on TrulyMadly","Not really looking for someone","Other"));*/

        public static $blockReasons = array(
            "reasons" =>array("Inappropriate or offensive content","Not interested anymore","Misleading profile information","Others"),
            "edit_flags" =>array(false,false,true,true));
								
        public static $conversations = array(
            "mutual_like_no_message" => "has liked you too. Say Hello!");
        
        public static $nudgeMessages = array(
            "1" => array(
                "10" => "Eeny, meeny, miny, moe! Welcome to the exciting world of boy browsing ;)",
                "40" => "Learning from your likes and nopes so keep going...",
                "70" => "Just a few more to go..."),
            "2" => array(
                "10" => "#Artsy #Foodie #GoGetter? We're learning your type so keep browsing",
                "40" => "Engineer, doctor, lawyer?... getting to know your preferences better",
                "70" => "Only a few profiles left to browse..."),
            "3" => array(
                "20" => "So many boys, so many types...get ready to 'like'!",
                "50" => "Only a few guys left ....they might be worth the wait!"),
            "4" => array(
                "20" => "Don't let great guys pass you by!",
                "50" => "The last set of guys are waiting to be introduced"),
            "5" => array(
                "20" => "So, is it going to be a tick or a cross next?",
                "50" => "Almost there ....keep browsing")
        );
}
?>