<?php 

$tbMessages['facebook']['name_mismatch'] = "Huh! How come you call yourself tmName here but fbName on Facebook?";
$tbMessages['facebook']['age_mismatch'] = "Huh! You say you are tmAge here, but Facebook claims you are fbAge.";
$tbMessages['facebook']['name_age_mismatch'] = "Huh! How come you are tmName and are tmAge years old here, but on Facebook you are fbName who is fbAge years old?";
$tbMessages['facebook']['married'] = "Aha! Looks like you’ve already found your soulmate. Our website is for singles only.";


$tbMessages['id']['name_mismatch'] = "Huh! How come you call yourself tmName here but fbName on Photo ID?";
$tbMessages['id']['age_mismatch'] = "Huh! You say you are tmAge here, but Photo ID claims you are fbAge.";
$tbMessages['id']['name_age_mismatch'] = "Huh! How come you are tmName and are tmAge years old here, but on Photo ID you are fbName who is fbAge years old?";
$tbMessages['id']['moderation'] = "Thank you for uploading your idType. We will review it and contact you within 48 hours.";
$tbMessages['id']['pwd_reqired'] = "Hello hello! Your document is password protected. Please provide document password.";
$tbMessages['id']['not_clear'] = "Tsk! Tsk! Document is unclear or an invalid proof.";
$tbMessages['id']['success'] = "Yippee! Thank a ton for uploading your idType. We will review it and contact you within 48 hours.";


$tbMessages['phone']['rejected'] = "Your phone number could not be verified. Please try again.";
$tbMessages['phone']['rejectedgt3'] = "Your phone number could not be verified. Please contact TrulyMadly customer care.";
$tbMessages['phone']['notpick'] = "Your phone number could not be verified as there was no response.";


$tbMessages['employment']['moderation'] = "Thank you for uploading your idType. We will review it and contact you within 48 hours.";
$tbMessages['employment']['pwd_req'] = "Hello hello! Your document is password protected. Please provide document password.";
$tbMessages['employment']['not_clear'] = "Tsk! Tsk! Document is unclear or an invalid proof.";
$tbMessages['employment']['success'] = "Yippee! Thank a ton for uploading your idType. We will review it and contact you within 48 hours.";

$tbMessages['endorsement_male']="Friends know you best! Get a friend or two to refer you on TrulyMadly.";
$tbMessages['endorsement_female']="Friends know you best! Get a friend or two to refer you on TrulyMadly.";
$tbMessages['endorsement_success']="Request more friends.";
$tbMessages['endorsement_ifsingle_message'] = "Request more friends.";
$tbMessages['endorsement_send'] = "If you've requested a reference, we'll be sure to update your score.";
$tbMessages['endorsement_profile_pic_added_success'] = "Great! Photo uploaded successfully.";


$tbMessages['trustMessage']['zero'] = "You need 30% score to be verified.";
$tbMessages['trustMessage']['hundred'] = "You're verified!";
$tbMessages['trustMessage']['authentic'] = "Build more for more responses.";

?>