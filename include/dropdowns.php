<?php
require_once (dirname ( __FILE__ ) . '/config.php');
require_once (dirname ( __FILE__ ) . '/function.php');
require_once (dirname ( __FILE__ ) . '/../photomodule/Photo.class.php');


class dropdowns {

public function getDropDowns($page,$data) {
include (dirname ( __FILE__ ) . '/allValues.php');
global $baseurl,$conn,$smarty,$imageurl;
$smarty->assign('allQ',$allQuestions);
$smarty->assign('allE',json_encode($allErrors));
$smarty->assign('registration_top_content',$registration_top_content);
$session=functionClass::getUserDetailsFromSession();
if($page=='demographic')  {
/*$rs=$conn->Execute("select * from tongues");
$tongues=$rs->GetRows();
*/
/*$rs=$conn->Execute("select * from religions");
$religionArray=$rs->GetRows();
foreach($religionArray as $religion)
$religions[$religion['id']]=$religion['name'];
*/
$rs=$conn->Execute("select * from castes");
$result=$rs->GetRows();
foreach($result as $value) 
$caste[$value['religion_id']][]=array("name"=>$value['name'],"id"=>$value['caste_id']);
//caste dropdown prepared
//country dropdowns
$rs=$conn->Execute("select * from geo_country");
$countries=$rs->GetRows();
//spouse Age dropdown

if($data['gender']=='M') {
$ageSpouse=range(18,70);
$ageOwn=range(21,70);
}
else if($data['gender']=='F') {
$ageSpouse=range(21,70);
$ageOwn=range(18,70);
}
if($data['gender']=='M')
{
$range=range(18,70);
$smarty->assign('yearRange',range(date("Y")-21,date("Y")-70));

}
else if($data['gender']=='F')
{
	$range=range(21,70);
	$smarty->assign('yearRange',range(date("Y")-18,date("Y")-70));
	
}
	
//$smarty->assign('yearRange',$yearRange);
$smarty->assign('ageOwn',$ageOwn);
$smarty->assign('maritalStatus',json_encode($maritalStatus));
$smarty->assign('ageSpouse',$ageSpouse);
$smarty->assign('religions',json_encode($religions));
$smarty->assign('castes',json_encode($caste));
$smarty->assign('countries',$countries);
//$smarty->assign('countries',$countries);
$smarty->assign('tongues',json_encode($tongues));
//return $demography;	
}
if($page=='trait') {
$smarty->assign('heightFoot',json_encode($heightFoot));
$smarty->assign('skin_tones',json_encode($skin_tones));
$smarty->assign('bodyTypes',json_encode($bodyTypes));
$smarty->assign('disability_types',json_encode($disabilityTypes));
$smarty->assign('smokingValues',json_encode($smokingValues));
$smarty->assign('drinkingValues',json_encode($drinkingValues));
$smarty->assign('foodTypes',json_encode($foodTypes));
}
if($page=='family') {
/*$rs=$conn->Execute("select * from industries");
$result=$rs->GetRows();
*/

//$smarty->assign('maritalStatus',json_encode($maritalStatus));
$smarty->assign('income_spouse',json_encode($income_spouse));
$smarty->assign('incomes',json_encode($incomes));
$smarty->assign('familyTypes',json_encode($familyTypes));
$smarty->assign('familyStatus',json_encode($familyStatus));
}
if($page=='work') {
global $maxAddMore;
/*
$rs=$conn->Execute("select * from industries");
$result=$rs->GetRows();
*/
$rs1=$conn->Execute("select * from linkedin_industry_mapping");
$rows=$rs1->GetRows();
foreach($rows as $row) {
$row['industry_name']=str_replace("/"," ",$row['industry_name']);
$row['industry_name']=str_replace("\""," ",$row['industry_name']);
$linkedin_industry[$row['industry_name']]=$row['industry_id'];
}
/*
$rs3=$conn->Execute("select * from income");
$income=$rs3->GetRows();
*/
/*$rs2=$conn->Execute("select * from education_levels");
$rows2=$rs2->GetRows();
*/
$rs4=$conn->Execute("select * from courses");
$rows3=$rs4->GetRows();

$smarty->assign('maxAddMore',$maxAddMore);

$smarty->assign('working_areas',json_encode($working_areas));
$smarty->assign('industries',json_encode($industries));
$smarty->assign('linkedin_mapping_industry',json_encode($linkedin_industry));
$smarty->assign('education_levels',json_encode($educations));
$smarty->assign('education_sublevels',json_encode($educationsSub));
$smarty->assign('courses',json_encode($rows3));
}
if($page=='photo') {
	
$photomodule = new Photo($session['user_id']);
$photodata = $photomodule->getUserPhotos();

if($photodata){
	$smarty->assign("photos",json_encode($photodata));
	//there will always be one profile pic
	//$smarty->assign("profile_photo_exist",$photomodule->checkProfileExist($photodata));
}elseif($data['is_fb_connected']=='Y'){
	$fbPic = $photomodule->getUserFbPic();
	if(strlen($fbPic)>0){
	$first_time_facebook[0]['photo_id']="from_fb";
	$first_time_facebook[0]['status']="from_fb";
	$first_time_facebook[0]['name']=$fbPic;
	$first_time_facebook[0]['thumbnail']=$fbPic;
	$smarty->assign("photos",json_encode($first_time_facebook));
	}
}
}

if($page=='hobby') {
	
	$interesthobbies = new Interesthobbies($session['user_id']);
	$interesthobbies->initializeIH($smarty,$interests);
	
}


}
}


?>
