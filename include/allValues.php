<?php
$religions=array("1"=>"Hindu","2"=>"Muslim","3"=>"Sikh","5"=>"Christian","9"=>"Buddhist","4"=>"Jain","6"=>"Other");
$religions_spouse=array("0"=>array("id"=>"Hindu","value"=>"Hindu"),"1"=>array("id"=>"Muslim","value"=>"Muslim"),"2"=>array("id"=>"Sikh","value"=>"Sikh"),"3"=>array("id"=>"Christian","value"=>"Christian"),"4"=>array("id"=>"Buddhist","value"=>"Buddhist"),"5"=>array("id"=>"Jain","value"=>"Jain"));//2"=>"Muslim","3"=>"Sikh","5"=>"Christian","9"=>"Buddhist","4"=>"Jain","6"=>"Other");
$tongues=array("25"=>"Hindi","53"=>"Punjabi","8"=>"Bengali","44"=>"Marathi","66"=>"Telugu","29"=>"Kannada","65"=>"Tamil","2"=>"Aka","3"=>"Arabic","4"=>"Arunachali","5"=>"Assamese","6"=>"Awadhi","7"=>"Baluchi","9"=>"Bhojpuri","10"=>"Bhutia","11"=>"Brahui","12"=>"Brij","13"=>"Burmese","14"=>"Chattisgarhi","15"=>"Chinese","16"=>"Coorgi","17"=>"Dogri","18"=>"English","19"=>"French","20"=>"Garhwali","21"=>"Garo","22"=>"Gujarati","23"=>"Haryanavi","24"=>"Pahari","26"=>"Hindko","27"=>"Kakbarak","28"=>"Kanauji","30"=>"Kashmiri","31"=>"Khandesi","32"=>"Khasi","33"=>"Konkani","34"=>"Koshali","35"=>"Kumaoni","36"=>"Kutchi","37"=>"Ladakhi","38"=>"Lepcha","39"=>"Magahi","40"=>"Maithili","41"=>"Malay","42"=>"Malayalam","43"=>"Manipuri","45"=>"Marwari","46"=>"Miji","47"=>"Mizo","48"=>"Monpa","49"=>"Nepali","50"=>"Oriya","51"=>"Pashto","52"=>"Persian","54"=>"Rajasthani","55"=>"Russian","56"=>"Sanskrit","57"=>"Santhali","58"=>"Seraiki","59"=>"Sindhi","60"=>"Sinhala","61"=>"Sourashtra","62"=>"Spanish","63"=>"Swedish","64"=>"Tagalog","67"=>"Tulu","68"=>"Urdu","69"=>"Other");
$heightFoot=range(4,8);
$skin_tones=array(
"Fair",
"Wheatish",
"Tan",
"Dark"
);
$bodyTypes=array(
0=>"Slim",
1=>"Athletic",
2=>"Normal",
3=>"Heavy"
);

$bodyTypesCombined=array(
		"Slim",
		"Athletic",
		"Normal");

$disabilityTypes=array(
"physically"=>"Physical Disability",
"mentally"=>"Mental Disability"
);
$smokingValues=array(
0=>"Never",
1=>"Socially",
2=>"Habitually"
);
$drinkingValues=array(
"Habitually",
"Socially",
"Never"		
);
$drinkingValuesSearch=array(
		2=>"Often",
		1=>"Socially",
		0=>"Never"
);
$foodTypes=array(
0=>"Vegetarian",
1=>"Eggetarian",
2=>"Non-Vegetarian"
);
$maritalStatus=array(
"Never Married",
"Divorced",
"Widowed",
);
$childrencount=range(1,10);
$familyTypes=array(
"With Parents",
"Joint Family",
"Independently"
);
$industries=array("1"=>"Admin / HR","2"=>"Advertising / Marketing / Sales","3"=>"Agriculture / Dairy / Fishing","4"=>"Airline / Aviation","5"=>"Animation / Design","6"=>"Architect / Interior Design / Real Estate","7"=>"Art / Media / Entertainment / Sports","8"=>"Beauty / Fashion / Cosmetics","9"=>"BPO / KPO / Customer Support","10"=>"Chemicals / Textiles / Glass etc..","11"=>"Computer / Software / IT / Internet","12"=>"Consulting","13"=>"Defence / Civil Services","14"=>"Education / Teaching","15"=>"Electronics / Semiconductors","16"=>"Engineering","17"=>"Accounting / Banking / Finance","18"=>"Fitness / Personal Care","19"=>"Government services","20"=>"Hotel / Hospitality / Travel / Tourism","21"=>"Import Export / Trading","22"=>"Law / Legal","23"=>"Manufacturing / RnD","24"=>"Medical / Healthcare","25"=>"Merchant Navy","26"=>"NGO / Social Service / Charitites / Environment","27"=>"Oil / Gas / Mining / Energy","28"=>"Others","29"=>"Pharmaceutical / Biotechnology","30"=>"Photography","31"=>"Printing / Packaging / Publishing","32"=>"Restaurant / Food services","33"=>"Retail / Wholesale / Distribution","34"=>"Supply Chain / Logistics","35"=>"Telecom/ Hardware / Networking","36"=>"Tobaco / Wine & Spirits","37"=>"Writing / Editing / Translation","38"=>"Events & Planning");
$familyStatus=array(
"Middle",
"Upper Middle",
"Affluent"
);
$noOfBrothers=range(0,9);
$noOfSisters=range(0,9);
$working_areas=array(
"Salaried",
"Business",
"Self Employed",
"Not Working"
);

$working_areas_combined=array(
		"Business",
		"Self Employed"
);
//$incomes=array("0-0"=>"Not Earning","0-2"=>"Less than Rs. 2 lakhs","2-5"=>"Rs. 2 lakhs to  Rs. 5 lakhs","5-10"=>"Rs. 5 lakhs to  Rs. 10 lakhs","10-20"=>"Rs. 10 lakhs to  Rs. 20 lakhs","20-50"=>"Rs. 20 lakhs to  Rs. 50 lakhs","50-100"=>"Rs. 50 lakhs to  Rs. 1 Crore","100-101"=>"More than 1 Crore");
$incomes=array("0-10"=>"<10 lakhs","10-20"=>"10-20 lakhs","20-50"=>"20-50 lakhs","50-51"=>">50 lakhs");
//$income_spouse=array("0"=>array("id"=>"10-20","value"=>">10 lakhs"),"1"=>array("id"=>"20-50","value"=>">20 lakhs"),"2"=>array("id"=>"50-51","value"=>">50 lakhs"));//"10-20"=>">10 lakhs","20-50"=>">20 lakhs","50-51"=>">50 lakhs");//0,2,5,10,20,50,100);

$income_spouse=array("0"=>array("id"=>"0-10","value"=>"<10 lakhs"),"1"=>array("id"=>"10-20","value"=>"10 lakhs to 20 lakhs"),"2"=>array("id"=>"20-50","value"=>"20 lakhs to 50 lakhs"),"3"=>array("id"=>"50-51","value"=>">50 lakhs"));
/*$educations=array("1"=>"PhD","2"=>"Masters in Technology / Engineering","3"=>"Masters in Architecture / Planning","4"=>"Masters in Arts",
		"5"=>"Masters in Science","6"=>"Masters in Commerce","7"=>"Master of Business Administration / Management",
		"8"=>"Masters in Medicine - General/ Surgeon / Alternative","9"=>"Masters in Dental","10"=>"Masters in Legal",
		"11"=>"Masters in Others","12"=>"Bachelors in Technology / Engineering","13"=>"Bachelors in Architecture / Planning",
		"14"=>"Bachelors in Arts","15"=>"Bachelors in Science","16"=>"Bachelors in Commerce",
		"17"=>"Bachelors of Business Administration / Management",
		"18"=>"Bachelors in Medicine - General / Surgeon / Alternative","19"=>"Bachelors in Dental",
		"20"=>"Bachelors in Legal","21"=>"Bachelors in Others","22"=>"Professional Course in Finance",
		"23"=>"Civil Services","24"=>"Post Graduate Diploma","25"=>"Diploma","26"=>"Other Courses","27"=>"Sr. Secondary");*/

$educations=array("0"=>array("id"=>"1","value"=>"Doctorate (Phd)"),"1"=>array("id"=>"20","value"=>"Civil Services"),"2"=>array("id"=>"2","value"=>"Masters in Advertising"),"3"=>array("id"=>"3","value"=>"Masters in Architecture"),"4"=>array("id"=>"4","value"=>"Masters in Arts"),"5"=>array("id"=>"5","value"=>"Masters in Business Admin"),"6"=>array("id"=>"6","value"=>"Masters in Commerce"),"7"=>array("id"=>"7","value"=>"Masters in Communication"),"8"=>array("id"=>"8","value"=>"Masters in Computer Application"),
				  "9"=>array("id"=>"9","value"=>"Masters in Education"),"10"=>array("id"=>"10","value"=>"Masters in Engineering"),"11"=>array("id"=>"11","value"=>"Masters in Journalism"),"12"=>array("id"=>"12","value"=>"Masters in Law"),"13"=>array("id"=>"13","value"=>"Masters in Management"),
				"14"=>array("id"=>"14","value"=>"Masters in Marketing"),"15"=>array("id"=>"15","value"=>"Masters in Medicine"),"16"=>array("id"=>"16","value"=>"Masters in Planning"),"17"=>array("id"=>"17","value"=>"Masters in Science"),"18"=>array("id"=>"18","value"=>"Masters in Technology"),"19"=>array("id"=>"19","value"=>"Masters in Other"),
				"20"=>array("id"=>"22","value"=>"Bachelors in Advertising"),"21"=>array("id"=>"23","value"=>"Bachelors in Architecture"),"22"=>array("id"=>"24","value"=>"Bachelors in Arts"),"23"=>array("id"=>"25","value"=>"Bachelors in Business Admin"),"24"=>array("id"=>"26","value"=>"Bachelors in Commerce"),
				"25"=>array("id"=>"27","value"=>"Bachelors in Communication"),"26"=>array("id"=>"28","value"=>"Bachelors in Computer Application"),"27"=>array("id"=>"29","value"=>"Bachelors in Education"),"28"=>array("id"=>"30","value"=>"Bachelors in Engineering"),"29"=>array("id"=>"31","value"=>"Bachelors in Journalism"),"30"=>array("id"=>"32","value"=>"Bachelors in Law"),
				"31"=>array("id"=>"33","value"=>"Bachelors in Management"),"32"=>array("id"=>"34","value"=>"Bachelors in Marketing"),"33"=>array("id"=>"35","value"=>"Bachelors in Medicine"),"34"=>array("id"=>"36","value"=>"Bachelors in Planning"),"35"=>array("id"=>"37","value"=>"Bachelors in Science"),"36"=>array("id"=>"38","value"=>"Bachelors in Technology"),"37"=>array("id"=>"39","value"=>"Bachelors in Other"),"38"=>array("id"=>"21","value"=>"Professional Course in Finance"),"39"=>array("id"=>"40","value"=>"Diploma"),"40"=>array("id"=>"41","value"=>"High School"),"41"=>array("id"=>"42","value"=>"Other Courses"));

$educationsSub['2'] = array("1"=>"Master of Computer Applications ( MCA )","2"=>"Master of Engineering ( ME )","3"=>"Master of Technology ( Mtech )","4"=>"Master of Science ( MS )","5"=>"Other Masters Degree in Technology / Engineering");
$educationsSub['3'] = array("1"=>"Master of Architecture ( MArch )","2"=>"Master of Planning ( MPlan )","3"=>"Other Masters Degree in Architecture / Planning");
$educationsSub['4'] = array("1"=>"Master of Arts ( MA )","2"=>"Master of Education ( MEd )","3"=>"Master of Fine Arts ( MFA )","4"=>"Master of Social Work ( MSW )","5"=>"Master of Humanities ( MHums )","6"=>"Master of Philosophy ( MPhil )","7"=>"Other Masters Degree in Arts");
$educationsSub['5'] = array("1"=>"Master of Science ( MSc )","2"=>"Other Masters Degree in Science");
$educationsSub['6'] = array("1"=>"Master of Communication ( MCom )","2"=>"Master of Journalism and Mass Communication ( Mjmc )","3"=>"Master of Economics ( MEcon )","4"=>"Other Masters Degree in Commerce");
$educationsSub['7'] = array("1"=>"Master of Business Administration ( MBA )","2"=>"Master of Hotel Management ( MHM )","3"=>"Other Masters Degree in Business Administration / Management");
$educationsSub['8'] = array("1"=>"Doctor of Medicine ( MD )","2"=>"Master of Pharmacy ( MPharm )","3"=>"Master of Physical Therapy ( MPT )","4"=>"Master of Veterinary Science ( MVSc )","5"=>"Master of Medical Sciences ( MCMSc )","6"=>"Master of Surgery ( ChM )","7"=>"Other Masters Degree in Medicine - General / Surgeon / Alternative");
$educationsSub['9'] = array("1"=>"Master of Dentistal Surgery ( MDS )","2"=>"Master of Dentistry ( MDent )","3"=>"Other Masters Degree in Dental");
$educationsSub['10'] = array("1"=>"Master of Laws ( LLM )","2"=>"Other Masters Degree in Legal");
$educationsSub['12'] = array("1"=>"Bachelor of Computer Applications ( BCA )","2"=>"Bachelor of Engineering ( BE )","3"=>"Bachelor of Technology ( BTech )","4"=>"Other Bachelors Degree in Technology / Engineering");
$educationsSub['13'] = array("1"=>"Bachelor of Architecture ( BAR/BArch )","2"=>"Bachelor of Planning ( BPlan )","3"=>"Other Bachelors Degree in Architecture / Planning");
$educationsSub['14'] = array("1"=>"Bachelor of Arts ( BA )","2"=>"Bachelor of Education ( BEd )","3"=>"Bachelor of Fine Arts ( BFA )","4"=>"Bachelor of Social Work ( BSW )","5"=>"Bachelor of Humanities ( BHums )","6"=>"Bachelor of Philosophy ( BPhil )","7"=>"Other Bachelors Degree in Arts");
$educationsSub['15'] = array("1"=>"Bachelor of Science ( BSc )","2"=>"Aviation Degree","3"=>"Other Bachelors Degree in Science");
$educationsSub['16'] = array("1"=>"Bachelor of Communication ( BCom )","2"=>"Bachelor of Journalism and Mass Communication ( Bjmc )","3"=>"Bachelor of Economics ( BEcon )","4"=>"Other Bachelors Degree in Commerce");
$educationsSub['17'] = array("1"=>"Bachelor of Business Admin. ( BBA )","2"=>"Bachelor of Hotel Management ( BHM )","3"=>"Other Bachelors Degree in Business Administration / Management");
$educationsSub['18'] = array("1"=>"Bachelor of Ayurvedic Medicine ( BAM )","2"=>"Bachelor of Homeopath Medical Science ( BHMS )","3"=>"Bachelor of Medicine ( BM / MBBS )","4"=>"Bachelor of Pharmacy ( BPharma )","5"=>"Bachelor of Physical Therapy ( BPT )","6"=>"Bachelor of Surgery ( BChir )","7"=>"Bachelor of Unani Medical & Surgery ( BUMS )","8"=>"Bachelor of Veterinary Science ( BVSc )","9"=>"Other Bachelors Degree in Medicine - General / Surgeon / Alternative");
$educationsSub['19'] = array("1"=>"Bachelor of Dental Suregry ( BDS )","2"=>"Other Bachelors Degree in Dental");
$educationsSub['20'] = array("1"=>"Bachelor of Law ( LLB )","2"=>"Other Bachelors Degree in Legal");
$educationsSub['22'] = array("1"=>"Chartered Accountant ( CA )","2"=>"Chartered Financial Analyst ( CFA )","3"=>"Company Secretary ( CS )","4"=>"�ICWA","5"=>"Other Professional Course in Finance");
$educationsSub['23'] = array("1"=>"Indian Administrative Services ( IAS )","2"=>"Indian Engineering Services ( IES )","3"=>"Indian Foreign Services ( IFS )","4"=>"Indian Revenue Services ( IRS )","5"=>"Indian Police Services ( IPS )","6"=>"Other Civil Services");


$interests=array(

"music"=>array("Blues","Bollywood","Classical","Electronica & Dance","Gazals","Hip Hop","Jazz","Metal","Pop","R&B","Regional","Rock"),
"books"=>array("Autobiographies","Business & Trade","Educational","Fiction","Literature","Mystery","Mythology","Non Fiction","Poetry","Romantic","Science Fiction","Self-Help"),
"food"=>array("American","Chinese","Continental","Mediterranean","Mexican","North Indian","Italian","Japanese","Pan Asian","South Indian","Street Food","Seafood"),
"movies"=>array("Action","Animation","Comedy","Documentary","Family Drama","Historical","Horror","Mystery","Reality","Romance","Science Fiction","Thriller"),
"sports"=>array("Badminton","Cricket","Cycling","Football","Golf","Hockey","Running","Squash","Swimming","Table Tennis","Tennis","Volleyball"),
"travel"=>array("Adventure","Beaches","Cruises","Culture & Heritage","Group Travel","Hills","Luxury","Nature","Pilgrimages","Road Trips","Spas","Wellness & Yoga"),
"others"=>array("Aerobics","Arts & Crafts","Cooking","Dancing","Gymming","Painting","Pet Care","Photography","Singing","Reading","Writing","Yoga")
);
$psycho1_answers=array("1"=>"very_accurate","2"=>"accurate","3"=>"neither_accurate_nor_inaccurate","4"=>"inaccurate","5"=>"very_inaccurate");
$psycho2_answers=array("1"=>"strongly_agree","2"=>"agree","3"=>"neutral","4"=>"disagree","5"=>"strongly_disagree","6"=>"never","7"=>"rarely","8"=>"sometimes","9"=>"mostly","10"=>"always");
$psycho3_answers=array("1"=>"not_at_all_important","2"=>"low_importance","6"=>"moderately_important","4"=>"slightly_important","5"=>"neutral","7"=>"very_important","8"=>"extremely_important");
$psycho4_answers=array("1"=>"In a joint family with in-laws(spouse’s married siblings, parents, uncle, aunts and cousin etc.)","2"=>"In a joint family with just his parents and unmarried siblings.","3"=>"With his parents but in the house that my spouse and I arrange.","4"=>"Alone with my spouse (live independently)","5"=>"yes","6"=>"no","7"=>"Live in a joint family (with other married siblings, parents, uncle, aunts and cousin etc.)","8"=>"Live in a joint family with just my parents and siblings (if any)","9"=>"Live  in the house my spouse and I arrange with my parents joining later","10"=>"Live independently (just two of us and kids in future)","11"=>"Working","12"=>"Not Working","13"=>"Doesn't Matter","14"=>"Work","15"=>"Not Work");
$psycho5_answers=array("1"=>"myself","2"=>"spouse","3"=>"Share","4"=>"yes","5"=>"no");


$allQuestions=array(
"age"=>"Birthday",
"marital"=>"Relationship Status",
"children"=>"with Children",
"age_spouse"=>"Age Range",
"marital_spouse"=>"Relationship Status",
"religion"=>"Religion",
"caste"=>"Caste",
"tongue"=>"Native Language",
"religion_spouse"=>"Religion",
"caste_spouse"=>"Caste",
"tongue_spouse"=>"Native Language",
"stay"=>"Where do you live?",
"location_spouse"=>"Where should your match live?",
"height"=>"Height",
"height_spouse"=>"Height Range",
"complexion"=>"Complexion",
"complexion_spouse"=>"Complexion should be",
"body_type"=>"Appearance",
"body_type_spouse"=>"Appearance",
"smoke"=>"Smoking",
"smoke_spouse"=>"Smoking",
"drink"=>"Drinking",
"drink_spouse"=>"Drinking",
"food"=>"Eating",
"food_spouse"=>"Eating",
"family_status"=>"Family Status",
"family_status_spouse"=>"Family Status",
"income"=>"Personal Annual Income",
"income_spouse"=>"Personal Annual Income Range",
"work"=>"Work Status",
"industry"=>"Industry",
"designation"=>"Designation ",
"company"=>"Company Name",
"work_spouse"=>"Work Status",
"industry_spouse"=>"Industry",
"education"=>"Education Level",
"specialization"=>"Specialization",
"institute"=>"Institute",
"education_spouse"=>"Education Level",
"interest"=>"Share details for at least 4 categories"		
);

$allErrors=array(
"age"=>"Please specify your birth date",
"marital"=>"Please specify your relationship status",
"children"=>"Please specify if you have children",
"marital_spouse"=>"Please specify the relationship status of your match",
"age_spouse"=>"Please specify the age range of your match",
"age_invalid"=>"Sorry, invalid age range",
"tongue"=>"Please specify your native language",
"religion"=>"Please specify your religion",
"religion_spouse"=>"Please specify the preferred religion of your match",
"stay_country"=>"Please specify country you are staying in...",
"born_country"=>"Please specify country in which you were born",
"born_state"=>"Please specify the state you are from",
"born_city"=>"Please specify the city you are from",
"stay_state"=>"Please specify the state you are currently residing in",
"stay_city"=>"Please specify the city you are currently residing in",
"height"=>"Please specify your height",
"height_spouse"=>"Please specify the height range of your match",
"height_invalid"=>"Invalid height range chosen",
"skin_tone"=>"Please specify your skin tone",
"skin_tone_spouse"=>"Please specify skin tone of your spouse",
"body"=>"Please specify your appearance",
"body_spouse"=>"Please specify body structure of your preffered spouse",
"smoke"=>"Please specify your smoking habit",
"smoke_spouse"=>"Please specify smoking habit of your preffered spouse",
"drink"=>"Please specify your drinking habit",
"drink_spouse"=>"Please specify drinking habit of your preffered spouse",
"food"=>"Please specify your food preference",
"food_spouse"=>"Please specify food habit of your preffered spouse",
"family_status"=>"Please specify your family status",
"family_status_spouse"=>"Please specify whether you are open to meet someone with the particular family status",
"income"=>"Please specify your household income",
"work"=>"Please specify your work status",
"industry"=>"Please specify your industry",
"designation"=>"Please specify your designation",
"work_spouse"=>"Please specify working sector of your preffered spouse",
"education"=>"Please specify your educational qualification",
"interest"=>"Please fill details for at least 4 categories",
"subjective1"=>"Please enter at least 20 characters",
"subjectivecontinue"=>"Please fill details for at least one question!"
		
);

$registration_top_content=array(
	"birth_place"=>"We're excited to get to know you. Let's start!",
		
);
?>
