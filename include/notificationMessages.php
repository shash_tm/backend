<?php 
$block_message['Misleading profile information'][1] = "You've been reported for filling in misleading info. Update your profile with the correct changes.";
$block_message['Misleading profile information'][2] = "Your matches aren't happy with you for filling in wrong info.";
$block_message['Misleading profile information'][3] = "Seems like you've put misleading info in your profile! Update with correct details or we may suspend your account.";

$block_message['Inappropriate or offensive content'][1] = "Watch what you're doing! Someone has reported your profile.";
$block_message['Inappropriate or offensive content'][2] = "Careful! You've been reported again.Your profile could get suspended";
$block_message['Inappropriate or offensive content'][3] = "Your profile is under review for being reported repeatedly";

?>