<?php
$infoMessage = array(
				"profile" => array(
							"non_authentic" => array(
												"msg" => "You need a Trust Score of 25% to use this feature.",
												"action" => "Build Trust Score"
												),
							"userSuspended" => "Your profile is temporarily in suspended state.",
							"matchSuspended" => "This profile is temporarily in suspended state.",
							"locked" => "You have to spend 10 credit points. Do you want to use credits from your wallet?"
							),

				"messages_full_conv" => array(
										"userSuspended" => "Your profile is temporarily in suspended state.",
										"matchSuspended" => "This profile is temporarily in suspended state.",
										"reject" => "Either of you didn't strike a chord with the other."
										),

				"matches" => array(
							"no_matches" => "Our relationship experts are working on finding matches for you. You can modify your partner preferences at any time.",
							"no_likes" => "You have not liked any profiles."
							),


				"likes" => array(
							"userSuspended" => "Your profile is temporarily in suspended state.",
							"no_likes" => "No Likes yet, but good things in life take time!"
							),
				"messages" => array(
							"no_message"=>"You have no messages.",
							"received_notify"=>"has sent you a message!"
						)
		

);
?>