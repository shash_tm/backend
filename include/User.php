<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../photomodule/Photo.class.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";


class User{

	//private $email;
	private $conn;
	private $cdnurl;
	//private $user_id;
	/*private $smarty;
	private $baseurl;*/

	function __construct(){
		global $conn, $baseurl, $smarty,$imageurl;
		$this->conn= $conn;
		$this->cdnurl = $imageurl;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		/*$this->baseurl = $baseurl;
		$this->smarty = $smarty;*/
		
	}

	public  function isValidUser($userArray = null){
		if(isset($userArray['email'])){
		$sql = $this->conn->Prepare("SELECT status FROM user where email_id = ?");
		$rs = $this->conn->Execute($sql, array($userArray['email']));
		}

		if(isset($userArray['user_id'])){
		$sql = $this->conn->Prepare("SELECT status FROM user where user_id = ?");
		$rs = $this->conn->Execute($sql, array($userArray['user_id']));
		}
		
		$row = $rs->FetchRow();
		if($row['status'] == 'authentic' || $row['status'] == 'non-authentic'){
			return 1;
		}
		return 0;
	}
	
	public function getUserDetails($user_id){
		$sql = $this->conn->Prepare("SELECT * from user where user_id = ?");
		$rs = $this->conn->Execute($sql, array($user_id));
		return  $rs->FetchRow();
	}
	
	public function getApprovedPics($user_id){
		$rs = $this->conn->Execute(" SELECT * from user_photo where status = 'active' AND user_id = ?", array($user_id));
		return $rs->getRows();
	}

	public function getUserDetailsOnEmailBasis($email_id,$emailStatus = null){
		if($emailStatus == null) {
			$sql = $this->conn->Prepare("SELECT * from user where email_id = ? AND email_status is null");
			$rs = $this->conn->Execute($sql, array($email_id));
		}
		else {
			$sql = $this->conn->Prepare("SELECT * from user where email_id = ? AND (email_status is null or email_status=?)");
			$rs = $this->conn->Execute($sql, array($email_id,$emailStatus));
		}
		return  $rs->FetchRow();
	}
	
	/**
	 * If any email status is to be permitted in order to send mail
	 * @param unknown_type $email
	 * @param unknown_type $eStatus= "unsubscribe" if mail is to be sent to unsubscribed users too, otherwise null
	 * @param unknown_type $eStatus= 'suspended' if mail is to be sent to suspended users also ,  otherwise null
	 */
	public function canSendMail($email, $eStatus = null, $sStatus = null){
		if($eStatus == null){ 
		$sql = $this->conn->Prepare("SELECT count(*) as count from user where email_id = ? AND email_status is null and status in ('authentic', 'non-authentic', 'incomplete')");
		$rs = $this->conn->Execute($sql, array($email));
		}
		else{  
			   if ($sStatus == null ){
			$sql = $this->conn->Prepare("SELECT count(*) as count from user where email_id = ? AND (email_status Is NULL OR email_status =?) and status in ('authentic', 'non-authentic', 'incomplete','suspended')");
			$rs = $this->conn->Execute($sql, array($email, $eStatus));
		       } else {
		    $sql = $this->conn->Prepare("SELECT count(*) as count from user where email_id = ? AND (email_status Is NULL or email_status != ?) and status in ('authentic', 'non-authentic', 'incomplete', ? )");
		    $rs = $this->conn->Execute($sql, array($email, $eStatus, $sStatus ));
		       }
		}
		
		$count =  $rs->FetchRow();
		return $count['count'];
	}
	
	public static function getRefinedName(&$fname){
		if(strlen($fname)> 12){
			$fname = substr($fname, 0, 11);
		}
	}
	
	public function restoreUser1($user_id,$reason=null){
		$ex = $this->conn->Execute("SELECT previous_status,current_status from user_suspension_log where user_id = $user_id order by timestamp desc limit 1");
		$row= $ex->FetchRow();
		
		$query="UPDATE user SET status=? where user_id=?";
		$param_array=array (
					$row ['previous_status'],
					$user_id
		);
		$tablename='user';
		$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$user_id);
		
		
		/*$this->conn->execute ( $this->conn->prepare ( "UPDATE user SET status=? where user_id=?" ), array (
					$row ['previous_status'],
					$user_id
		) );*/
		
		$query="Insert Into user_suspension_log value(?,?,?,?,now(),?)";
		$param_array=array (
				$user_id,
				$reason,
				$row['current_status'],
				$row['previous_status'],
				$_SESSION ['admin_id']
		);
		$tablename='user_suspension_log';
		$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$user_id);
		
		/*$this->conn->execute ( $this->conn->prepare ( "Insert Into user_suspension_log value(?,?,?,?,now(),?)" ), array (
				$user_id,
				$reason,
				$row['current_status'],
				$row['previous_status'],
				$_SESSION ['admin_id']
		) );*/
		return 'Success';
	}
	
	public function restoreUser($user_id){
		$ex = $this->conn->Execute($this->conn->prepare("SELECT status,last_changed_status from user where user_id = ?"),$user_id);
		$row= $ex->FetchRow();
			if($row ['last_changed_status'] == null )
		       return false;
			
		$ex2 = $this->conn->Execute($this->conn->prepare("SELECT device_id from block_fids where user_id = ?"),$user_id);
		$row2= $ex2->FetchRow();
		 if($row2['device_id'])
		 {
		 	$this->conn->Execute($this->conn->prepare("DELETE from block_fids where user_id=? and device_id=?"),array($user_id,$row2['device_id']));
		 }
			
		
		$query=	 "UPDATE user SET status=?,last_changed_status=? where user_id=?";
		$param_array=array (
				$row ['last_changed_status'],
				NULL,
				$user_id
		);
		$tablename='user';
		$res_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename,"master",$user_id);
			
		/*$this->conn->execute ( $this->conn->prepare ( "UPDATE user SET status=?,last_changed_status=? where user_id=?" ), array (
				$row ['last_changed_status'],
				NULL,
				$user_id
		) );*/
		
		$query=	 "Insert Into user_suspension_log value(?,?,?,?,now(),?)";
		$param_array=array (
				$user_id,
				NULL,
				$row['status'],
				$row['last_changed_status'],
				$_SESSION ['admin_id']
		) ;
		$tablename='user_suspension_log';
		$res_exec=Query_Wrapper::INSERT($query, $param_array, $tablename,"master",false,$user_id);
		
		/*$this->conn->execute ( $this->conn->prepare ( "Insert Into user_suspension_log value(?,?,?,?,now(),?)" ), array (
				$user_id,
				NULL,
				$row['status'],
				$row['last_changed_status'],
				$_SESSION ['admin_id']
		) );*/
		
		return true;
	}
	
	public function getUserDataForPrefillTheForm($user_id){
		$user_id = $user_id;
		$rs = $this->conn->Execute ( "select fname,lname,is_fb_connected,steps_completed from user where user_id=$user_id" );
		$result = $rs->GetRows ();
			if($result[0]['is_fb_connected']=='Y'){
				$fbdata = $this->getFBBasicData($user_id);
				
				if(isset($fbdata['birthday']))
					$result[0]['dob']=$this->getFBDateofBirth($fbdata['birthday']);
				else 
					$result[0]['dob'] = null;
				
				if(isset($fbdata['education']))
					$result[0]['institute_details'] = $this->getFBInstitute($fbdata['education']);
				else 
					$result[0]['institute_details'] = null;
			
				if(isset($fbdata['relationship'])) {
					$result[0]['marital_status'] = $this->getFBRelationShipStatus($fbdata['relationship']);
					$result[0]['marital_status_new'] = $this->getRelationShipStatus($result[0]['marital_status']);
				}
				else
					$result[0]['marital_status'] = null;
				
				if(isset($fbdata['work_details']))
					$result[0]['work_details'] = $this->getFBWork($fbdata['work_details']);
				else
					$result[0]['work_details'] = null;
				
				if(isset($fbdata['location']))
					$result[0]['location'] = $this->getFBLocation($fbdata['location']);
				else 
					$result[0]['location'] = null;
				
				$result[0]['photos'] = $this->getUserPhotosForPrefill($user_id);
			}else{
				$result[0]['dob'] = null;
				$result[0]['institute_details'] = null;
				$result[0]['marital_status'] = null;
				$result[0]['work_details'] = null;
				$result[0]['location'] = null;
				$result[0]['photos'] = $this->getUserPhotosForPrefill($user_id);
			}
			return $result [0];
		
		
	}
	
	public function getFBBasicData($user_id){
		$rs = $this->conn->Execute($this->conn->Prepare("select birthday,education,work_details,relationship,location from user_facebook where user_id=?"),array($user_id));
		if($rs&&$rs->rowCount()>0){
			$result = $rs->FetchRow();
			return $result;
		}
		return null;
	}
	
	public function getFBDateofBirth($result,$from_mobile=true){
	//	$rs = $this->conn->Execute($this->conn->Prepare("select birthday from user_facebook where user_id=?"),array($user_id));
	//	if($rs&&$rs->rowCount()>0){
		//	$result = $rs->FetchRow();
			$birthday=explode("/",$result);
			if(count($birthday)==3){
				if($from_mobile)
					return $birthday[2]."-".$birthday[0]."-".$birthday[1];
				else
					return array("fb_date"=>$birthday[1],"fb_month"=>$birthday[0],"fb_year"=>$birthday[2]);
			}
		//}
	}
	
	public function getFBInstitute($result){
	//	$rs = $this->conn->Execute($this->conn->Prepare("select education from user_facebook where user_id=?"),array($user_id));
		//if($rs&&$rs->rowCount()>0){
		//	$res = $rs->FetchRow();
			$result = json_decode($result,true);
			for($i=0;$i<count($result);$i++){
				foreach($result[$i] as $key => $value){
					if($key=="school"){
						$institute[] = $value['name'];//rawurlencode($value['name']);
					}
				}
			}
			return array("institute"=>json_encode($institute,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));//json_encode($institute);
	//	}
		//return null;
	}
	public function getFBRelationShipStatus($result){
		//$rs = $this->conn->Execute($this->conn->Prepare("select relationship from user_facebook where user_id=?"),array($user_id));
		//if($rs&&$rs->rowCount()>0){
		//	$result = $rs->FetchRow(); 
		if($result=="In a relationship" || $result=="Engaged" || $result=="In an open relationship" || $result=="It's complicated" || $result=="Single"){
			$relationship = "Never Married";
		}else
			$relationship = $result;
			
		return $relationship;
		//}
		//return null;
	}
	
	public function getRelationShipStatus($status) {
		if($status == 'Divorced' || $status == 'Separated' || $status == 'Widowed') {
			return 'Married Before';
		} else {
			return $status;
		}
	}
	public function getFBWork($result){
		//$rs = $this->conn->Execute($this->conn->Prepare("select work_details from user_facebook where user_id=?"),array($user_id));
		//if($rs&&$rs->rowCount()>0){
		//	$result = $rs->FetchRow();
			$work_details = json_decode($result,true);
			$j=0;
			for($i=0;$i<count($work_details);$i++){
				foreach($work_details[$i] as $key => $value){
					if($key=="employer"){
						$prev[$j] = $value['name'];//rawurlencode($value['name']);//$value['name'];
						$j++;
					}
				}
			}
			return array("designation" => $work_details[0]['position']['name'],"companies"=>json_encode($prev,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
	//	}
	//	return null;
	}
	
	public function getFBLocation($result){
	//	$rs = $this->conn->Execute($this->conn->Prepare("select location from user_facebook where user_id=?"),array($user_id));
	//	if($rs&&$rs->rowCount()>0){
	//		$result = $rs->FetchRow();
			$location = json_decode($result,true);
			$res = $this->conn->Execute($this->conn->Prepare("select city_id from facebook_cities_mapping where city_fid=?"),array($location['id']));
			if($res && $res->rowCount()>0){
				$result = $res->FetchRow();
				$res1 = $this->conn->Execute($this->conn->Prepare("select distinct * from geo_city where city_id=?"),array($result['city_id']));
				$city = $res1->FetchRow();
				$res2 = $this->conn->Execute($this->conn->Prepare("select * from geo_state where state_id=?"),array($city['state_id']));
				$state = $res2->FetchRow();
				$res3 = $this->conn->Execute($this->conn->Prepare("select * from geo_country where country_id=?"),array($state['country_id']));
				$country = $res3->FetchRow();
				return array("country"=>$country,"state"=>$state,"city"=>$city);
			}else
				return null;
	//	}
	//	return null;
	}
	
	public function getUserPhotosForPrefill($user_id){
		$photomodule = new Photo($user_id);
		$photodata = $photomodule->getUserPhotos();
		if($photodata){
			return json_encode($photodata);
		}/*elseif($FBConnected=='Y'){
			$fbPic = $this->getUserFbPic($user_id);
			$profilePic = json_decode($fbPic,true);
			if(strlen($fbPic)>0){
				for($i=0;$i<count($profilePic);$i++){
					$first_time_facebook[$i]['photo_id']="from".$i;
					$first_time_facebook[$i]['status']="from_fb";
					$first_time_facebook[$i]['name']=$profilePic[$i];
					$first_time_facebook[$i]['thumbnail']=$profilePic[$i];
				}
				return json_encode($first_time_facebook);
			}
		}*/
		return null;
	}
	
	public function  getUserFbPic($user_id){
		$rs=$this->conn->Execute("select profile_url from user_facebook where user_id='".$user_id."'");
		$result=$rs->GetRows();
		if($result[0]['profile_url']) {
			return $result[0]['profile_url'];
		}
		return false;
	}
	/*
	 * to check whether email id is already exits or not
	 */
	public function checkEmail($email,$password=''){
		try{
			$rs=$this->conn->Execute($this->conn->prepare("select * from user where email_id=?"),array($email));
			if($rs->_numOfRows>0){
				$result = $rs->GetRows();
				//if($result[0]['fid'] && $password=='')
					
				if($result[0]['fid'] && $result[0]['password']=='')
					return 'FB_USER';
			//	if($password!=''){
			    if($password!=''){
			    	if($result[0]['password']==md5($password)){
			    		if($result[0]['deletion_status']=='mark_deleted'){
			    			return 'MARK_DELETED_PROFILE';
			    		} 
			    		if($result[0]['status']=='suspended'){
			    			return 'DEACTIVATED_PROFILE';
			    		}
			    		if($result[0]['status']=='blocked'){
			    			return 'BLOCKED_PROFILE';
			    		}
			    		return 'USER_EXISTS';
			    	}else return 'USER_ABSENT';
			    }else return 'USER_EXISTS';
					//$rs=$this->conn->Execute($this->conn->prepare("select * from user where email_id=? and password= ?"),array($email,md5($password)));
					//if($rs->_numOfRows>0)
					//	return 'USER_EXISTS';
					//else return 'USER_ABSENT';
				//}
				//else return 'USER_EXISTS';
			}else return 'USER_ABSENT';
		}catch(Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
	}
	
	
	
	/*
	 * 
	 * for App only
	 */
	
	public function checkEmailForApp($email,$password=''){
		global $redis;
		$ipAddress = $_SERVER['REMOTE_ADDR'] ;
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
		}
		$redis_key = "login_log_".$ipAddress."_".$email;
		$login_log = $redis->get($redis_key);
		if(isset($login_log) && $login_log != null && $login_log >= 10){
			return 'LOGIN_BLOCKED';
		}
		try{
			$rs=$this->conn->Execute($this->conn->prepare("select * from user where email_id=?"),array($email));
			if($rs->_numOfRows>0){
				$result = $rs->GetRows();
				//if($result[0]['fid'] && $password=='')
				
				if($result[0]['fid'] && $result[0]['password']=='')
					return 'FB_USER';
				//	if($password!=''){
				if($password!=''){
					if($result[0]['password']==md5($password)){
					if (isset($result [0] ['deletion_status']) && $result [0] ['deletion_status'] == 'mark_deleted')
						{
							return 'USER_DELETED' ;
						} else
						{
							return 'USER_EXISTS';
						}
					}else {
						if(isset($login_log) && $login_log != null && $login_log < 10){
							$login_log += 1;
						}else{
							$login_log = 1;
						}
						$redis->setex($redis_key, 1800, $login_log);
						return 'USER_ABSENT';
					}
				}else return 'USER_EXISTS';
				//$rs=$this->conn->Execute($this->conn->prepare("select * from user where email_id=? and password= ?"),array($email,md5($password)));
				//if($rs->_numOfRows>0)
				//	return 'USER_EXISTS';
				//else return 'USER_ABSENT';
				//}
				//else return 'USER_EXISTS';
			}else return 'USER_ABSENT';
		}catch(Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
	}
	
	/*
	 * to add new user
	 */
	public function createNewUser($data,$registered_from, $version = 0){
		try{
			$query=	 "insert into user(user_id,fname,lname,gender,email_id,password,fid,is_fb_connected,status,
				registered_from,device_id, registered_at)
					values('',?,?,?,?,?,'','N','incomplete',?,?, Now())";
			$param_array=array($data['fname'],$data['lname'],$data['gender'],$data['email'],md5($data['password']),$registered_from,$data['device_id']) ;
			$tablename='user';
			$user_id=Query_Wrapper::INSERT($query, $param_array, $tablename);
			
			/*$this->conn->Execute($this->conn->prepare("insert into user(user_id,fname,lname,gender,email_id,password,fid,is_fb_connected,status,
				registered_from,device_id, registered_at)
					values('',?,?,?,?,?,'','N','incomplete',?,?, Now())"),
					array($data['fname'],$data['lname'],$data['gender'],$data['email'],md5($data['password']),$registered_from,$data['device_id']));
			$user_id = $this->conn->Insert_ID();*/
			
			if($registered_from == 'android_app' && $version>=100 && isset($user_id)) {
				
				$query=	 "insert into user_data(user_id,DateOfBirth,tstamp) values(?,?,NOW())";
				$param_array=array($user_id,$data['birthday']);
				$tablename='user_data';
				$user_id=Query_Wrapper::INSERT($query, $param_array, $tablename);
					
				/*$this->conn->Execute($this->conn->prepare("insert into user_data(user_id,DateOfBirth,tstamp) values(?,?,NOW())"),
				array($user_id,$data['birthday']));*/
				
				$query=	 "insert into user_preference_data(user_id,start_age,end_age,tstamp) 
						values(?,?,?,NOW())";
				$param_array=array($user_id,$data['start_age'],$data['end_age']);
				$tablename='user_preference_data';
				$user_id=Query_Wrapper::INSERT($query, $param_array, $tablename);
				
				/*$this->conn->Execute($this->conn->prepare("insert into user_preference_data(user_id,start_age,end_age,tstamp) 
						values(?,?,?,NOW())"),
						array($user_id,$data['start_age'],$data['end_age']));*/
			}
		}catch(Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
	}
	
	public function getUserDetailsFromSession(){
		if(isset($_SESSION ['user_id'])) {
			$session_arr = array(
					"gender"=>$_SESSION['gender'],
					"user_id" => $_SESSION ['user_id'],
					"status" => $_SESSION['status'],
					"payment_plan" => $_SESSION['plan'],
					"first_login" => $_SESSION['first_login']);
		}
		return $session_arr;
	}
	
	public function getFbConnectionCount($user_id){
		$sql = $this->conn->Prepare("select connection from user_facebook where user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->FetchRow();
		return  $res['connection'];
	}
	
	public function getUserformenu($user_id){
		$sql = $this->conn->Prepare("select u.user_id as user_id,u.gender, u.status, u.is_fb_connected,fname as name, FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age,gc.name as city,
									up.thumbnail as profile_url,current_credits,gco.name as country,sc.code as state_code, ud.stay_country as country_code, ud.fb_designation
									from user u LEFT JOIN user_data ud on u.user_id=ud.user_id 
									LEFT JOIN geo_city gc on ud.stay_city = gc.city_id
									LEFT JOIN geo_country gco on ud.stay_country = gco.country_id
									LEFT JOIN state_codes sc on sc.state_id = gc.state_id
									LEFT JOIN user_photo up on u.user_id=up.user_id and up.is_profile='yes' 
									LEFT JOIN user_current_credits c  on u.user_id=c.user_id 
									where u.user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->GetRows();
		if(isset($res[0]['profile_url']))
			$res[0]['profile_url'] = $this->cdnurl.$res[0]['profile_url'];
		if(isset($res[0]['country']) && $res[0]['country'] == 113) {
			$res[0]['country'] = 'India';
		} 
		if(isset($res[0]["fb_designation"]))
		{
			$res[0]["fb_designation"]=json_decode($res[0]["fb_designation"]);
		}
		if($res[0]['state_code'] != null)
			$res[0]['city'] .= ", " . $res[0]['state_code'];
		return $res[0];
	}
	
	public function getFbLikesNum($user_id){
		$sql = $this->conn->Prepare("select no_of_likes from user_facebook where user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->FetchRow();
		return  $res['no_of_likes'];
	}
	
	public function getUserCollegenCompany($user_id){
		$sql = $this->conn->Prepare("select company_name, institute_details from user_data where user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->FetchRow();
		return  $res;
	}
	
}

?>
