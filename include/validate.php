<?php
// for validating form parameters
class Validation {
	function __construct() {



}
//validate whether a text is not null and equal to $length and match with regular expression $resgx
	public function validateText($text, $regx = 0, $length = 0) {
		if (! $text || $text == '')
			return 1;
		else if ($length && strlen ( $text ) !== $length)
			return 2;
		else if ($regx)
			return ! preg_match ( $regx, $text );
		return 0;
	}

//verify format of an Email Id
	public function ValidateEmail($mail) {
		return ! filter_var ( $mail, FILTER_VALIDATE_EMAIL );
	}
}
?>
