<?php

require_once dirname ( __FILE__ ) . "/config.php";
try{

/*Resolving the issue with getalheaders call. This is a temporary hack and the issue needs to be researched and resolved later.*/
if (!function_exists('getallheaders')) {
	function getallheaders() {
		$headers = '';
		foreach ($_SERVER as $name => $value) { 
			if (substr($name, 0, 5) == 'HTTP_') { 
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
			}
		}
		return $headers; 
	} 
}

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on'){
        $baseurl=$config["urls"]["baseurl_https"];
        $cdnurl=$config["urls"]["cdnurl_https"];;//"https://dev.trulymadly.com/trulymadly";
        $imageurl = $config["urls"]["imageurl_https"];;//'https://dev.trulymadly.com/trulymadly/files/images/profiles/';
        $server_url=$baseurl_https;
}else{
        $baseurl=$config["urls"]["baseurl"];
        $cdnurl=$config["urls"]["cdnurl"];;//"http://dev.trulymadly.com/trulymadly";
        $imageurl = $config["urls"]["imageurl"];;;//'http://dev.trulymadly.com/trulymadly/files/images/profiles/';
        $server_url=$baseurl;
}
	
$filepathForUserSearch = $config['mysql_table_generation_paths']['user_search']; 
$filepathForUserBucketGeneration = $config['mysql_table_generation_paths']['user_pi_activity']; 

$DBHOST_master = $config["database_master"]["hostname"];
$DBUSER_master =$config["database_master"]["username"];
$DBPASSWORD_master =$config["database_master"]["password"];
$DBNAME_master =$config["database_master"]["database"];

$DBTYPE_master = 'mysqlt';
$conn_master = &ADONewConnection($DBTYPE_master);
$conn_master->PConnect($DBHOST_master,$DBUSER_master,$DBPASSWORD_master,$DBNAME_master);

$DBHOST_slave = $config["database_slave"]["hostname"];
$DBUSER_slave =$config["database_slave"]["username"];
$DBPASSWORD_slave =$config["database_slave"]["password"];
$DBNAME_slave =$config["database_slave"]["database"];

$DBTYPE_slave = 'mysqlt';
$conn_slave = &ADONewConnection($DBTYPE_slave);
$conn_slave->PConnect($DBHOST_slave,$DBUSER_slave,$DBPASSWORD_slave,$DBNAME_slave);


$DBHOST_reporting = $config["database_reporting"]["hostname"];
$DBUSER_reporting=$config["database_reporting"]["username"];
$DBPASSWORD_reporting =$config["database_reporting"]["password"];
$DBNAME_reporting =$config["database_reporting"]["database"];
 
$DBTYPE_reporting = 'mysqlt';
$conn_reporting = &ADONewConnection($DBTYPE_reporting);
$conn_reporting->PConnect($DBHOST_reporting,$DBUSER_reporting,$DBPASSWORD_reporting,$DBNAME_reporting);


$emailIdsForReporting = array("suspension" => "shashwat@trulymadly.com, rahul@trulymadly.com, smita.prakash@trulymadly.com,
                                               hitesh@trulymadly.com, sumit@trulymadly.com, saumya@trulymadly.com, christina@trulymadly.com" ,
		"mailer_reporting" => "shashwat@trulymadly.com, rahul@trulymadly.com, hitesh@trulymadly.com,
												   sumit@trulymadly.com, saumya.agrawal@trulymadly.com, purav@trulymadly.com" ,
		"tracker" => "sumit@trulymadly.com, hitesh@trulymadly.com, rahul@trulymadly.com, shashwat@trulymadly.com,
                      maria@trulymadly.com, lavi@trulymadly.com, ankit@trulymadly.com, purav@trulymadly.com" ,
		"tracker_daily" => "sachin@trulymadly.com,sumit@trulymadly.com, hitesh@trulymadly.com, rahul@trulymadly.com, shashwat@trulymadly.com,
						    lavi@trulymadly.com, maria@trulymadly.com, ankit@trulymadly.com, purav@trulymadly.com" ,
		"churn" => "rahul@trulymadly.com, hitesh@trulymadly.com, saumya@trulymadly.com, shubhanshi@trulymadly.com,sumit@trulymadly.com ",
		"recommendation" => "shubhanshi@trulymadly.com, shashwat@trulymadly.com, rahul@trulymadly.com, shubham@trulymadly.com, purav@trulymadly.com",
		"recommendation_test"=>"vineet@trulymadly.com",
		"notification_reporting" => "tabish@trulymadly.com,hitesh@trulymadly.com,rahul@trulymadly.com,sumit@trulymadly.com,
									ankit@trulymadly.com,maria@trulymadly.com, purav@trulymadly.com" ,
		"admin_reporting" => "shashwat@trulymadly.com, rahul@trulymadly.com, hitesh@trulymadly.com, smita.prakash@trulymadly.com,
                                                  sumit@trulymadly.com, christina@trulymadly.com" ,
		"gearman"=>"sumit@trulymadly.com, shashwat@trulymadly.com" ,
		"photo_extra" => "suman.howlader@foiwe.com , rajeev.shrivastava@foiwe.com, ashif.hussain@foiwe.com,
                           					smita.prakash@trulymadly.com, sumit@trulymadly.com" ,
		"badword_reporting" => "arpan@trulymadly.com, rahul@trulymadly.com, smita.prakash@trulymadly.com,
                                               hitesh@trulymadly.com, sumit@trulymadly.com,shashwat@trulymadly.com",
		"phone_number_exchanged"=> "smita.prakash@trulymadly.com,hitesh@trulymadly.com,sumit@trulymadly.com",
		"admin_panel_messages" => "smita.prakash@trulymadly.com,hitesh@trulymadly.com,
                                               arpan@trulymadly.com, christina@trulymadly.com",
		"date_report" => "sumit@trulymadly.com,rahul@trulymadly.com,hitesh@trulymadly.com,sachin@trulymadly.com,
									rohit@trulymadly.com,shashwat@trulymadly.com,shoury@trulymadly.com,maria@trulymadly.com,lavi@trulymadly.com,ronit@trulymadly.com",
		"photo_sharing" => "rahul@trulymadly.com, shashwat@trulymadly.com, sumit@trulymadly.com",
		"spark_payment" => "team@trulymadly.com",
		"spark_report" => "team@trulymadly.com",
		"select_reco" => "sumit@trulymadly.com, dhruv@trulymadly.com, purav@trulymadly.com, shashwat@trulymadly.com, rahul@trulymadly.com",
		"video_report"=>"raghav@trulymadly.com,shashwat@trulymadly.com,rahul@trulymadly.com,sumit@trulymadly.com,smita.prakash@trulymadly.com,udbhav@trulymadly.com,purav@trulymadly.com"
) ;

/*try{
	$redis->ping();
}catch (Exception $e){
	$from = "himanshu@trulymadly.com";
	$to = "shashwat@trulymadly.com, himanshu@trulymadly.com";
	$subject = "Redis server is down for " . $baseurl;
	$message = $subject . " Reconnect Redis. " . $e->getMessage();
	$headers = "From: $from \r\n";
	mail($to,$subject,$message,$headers); 
} */
}
catch (Exception $e){
	trigger_error($e->getMessage(),E_USER_WARNING);
	die;
}
?>
