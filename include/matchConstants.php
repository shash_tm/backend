<?php

class InterpersonalConstants {
	const HighAdaptability = "High";
	const MediumAdaptability = "Medium";
	const LowAdaptability = "Low";
}

class PopularityIndex{

	const minActionCount = 20;

	const maleBucket1 = 4;
	const maleBucket2 = 8;
	const maleBucket3 = 14;
	const maleBucket4 = 24;

	const femaleBucket1 = 30;
	const femaleBucket2 = 45;
	const femaleBucket3 = 60;
	const femaleBucket4 = 75;

	const defaultMalePI = 0.1;
	const defaultFemalePI = 0.65;

}

class MatchEquation{

	const likeConstant = 50;

	/*const class  Religion {
	 const highest  = 1 ;
	 const medium = 0.5;
	 const lowest = 0;
	 };*/

	//private  class hell{}


	//const Religion =1;
	/*array("highest" => 1 , "medium" => 0.5, "lowest" => 0);
	const Income = array("highest" => 1 , "medium" => 0.5, "lowest" => 0);
	const MaritalStatus = array("highest" => 1 , "lowest" => 0);
	const Education = array("highest" => 1 , "lowest" => 0);
	const City = array("highest" => 1 , "lowest" => 0);
	const Value = array("highest" => 1 , "lowest" => 0);
	const CommonLikes = array("highest" => 1 , "lowest" => 0);
	const LikedMe = array("highest" => 1 , "lowest" => 0);*/
}

class MatchEquationMutiplicationFactor{


	const MaritalStatus =  48;
	const Religion = 24 ;
	const Value = 2 ;
	const CommonLikes = 1 ;


	/*const MaritalStatusMale =   48 ;
	 const MaritalStatusFemale =  48;
	 const ReligionMale = 24 ;
	 const ReligionFemale = 24;
	 */const IncomeMale =  6 ;
	const IncomeFemale = 12;
	const EducationMale = 3 ;
	const EducationFemale = 6;
	const CityMale = 3 ;
	const CityFemale = 3;
	/*const ValueMale = 2 ;
	 const ValueFemale = 2;
	 const CommonLikesMale = 1 ;
	 const CommonLikesFemale = 1;*/
	const LikedMeMale = 24 ;
	const LikedMeFemale = 6;

}

// for fresh availability
class AvailabilityFilters
{
	
	/*const bucketOne = 20;
	const bucketTwo = 20;
	const bucketThree = 15;
	const bucketFour = 10;
	const bucketFive = 10;*/
	const age_18_20 = 300;
	const age_21_22 = 100;
	const age_23_26 = 100;
	const bucketZero = 80;
	const bucketOne = 80;
	const bucketTwo = 80;
	const bucketThree = 80;
	const bucketFour = 80;
	const bucketFive = 80; 
}

class LikeCountThreshold
{
	const MaleExceedingChatThrehold = 10;
	const MaleBucketZero = 10;
	const MaleBucketOne = 10;
	const MaleBucketTwo = 10;
	const MaleBucketThree = 20;
	const MaleBucketFour = 20;
	const MaleBucketFive = 20;
}


class FreshBatchCount
{

	/*const MaleExceedingChatThrehold = 7;
	const MaleBucketZero = 7;
	const MaleBucketOne = 5;
	const MaleBucketTwo = 7;
	const MaleBucketThree = 10;
	const MaleBucketFour = 15;
	const MaleBucketFive = 15;*/
	
	const MaleExceedingChatThrehold = 10;
	const MaleBucketZero = 10;
	const MaleBucketOne = 10;
	const MaleBucketTwo = 10;
	const MaleBucketThree = 20;
	const MaleBucketFour = 20;
	const MaleBucketFive = 20;

	const FemaleBucketZero = 10;
	const FemaleBucketOne = 20;
	const FemaleBucketTwo = 20;
	const FemaleBucketThree = 10;
	const FemaleBucketFour = 10;
	const FemaleBucketFive = 10;

}

class RepeatBatchCount
{
	const MaleExceedingChatThrehold = 5;
	const MaleBucketZero = 5;
	const MaleBucketOne = 5;
	const MaleBucketTwo = 5;
	const MaleBucketThree = 10;
	const MaleBucketFour = 10;
	const MaleBucketFive = 10;
	
	/*
	const MaleExceedingChatThrehold = 5;
	const MaleBucketZero = 5;
	const MaleBucketOne = 3;
	const MaleBucketTwo = 5;
	const MaleBucketThree = 7;
	const MaleBucketFour = 12;
	const MaleBucketFive = 12;*/
	/*
	 * public static $recommendationFreshLikesCountGoodBucketsLessChats = 10;
	 public static $recommendationRepeatCountGoodBucketsLessChats = 5;
	 public static $recommendationFreshLikesCountBadBucketOrMoreChats = 5;
	 public static $recommendationRepeatCountBadBucketOrMoreChats = 3;


	 */
}

class BatchSize
{
	/*const FemaleBucketZero = 70;
	const FemaleBucketOne = 80;
	const FemaleBucketTwo = 80;
	const FemaleBucketThree = 70;
	const FemaleBucketFour = 70;
	const FemaleBucketFive = 70;*/
	const FemaleBucketZero = 100;
	const FemaleBucketOne = 110;
	const FemaleBucketTwo = 110;
	const FemaleBucketThree =100;
	const FemaleBucketFour = 100;
	const FemaleBucketFive = 100;

}
/*class Values {
 const High = 4;
 const
 }*/
?>
