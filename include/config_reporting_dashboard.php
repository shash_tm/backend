<?php
require_once dirname ( __FILE__ ) . "/config.php";
try{
	
$DBHOST_reporting_dashboard = $config["database_reporting_dashboard"]["hostname"];
$DBUSER_reporting_dashboard=$config["database_reporting_dashboard"]["username"];
$DBPASSWORD_reporting_dashboard =$config["database_reporting_dashboard"]["password"];
$DBNAME_reporting_dashboard =$config["database_reporting_dashboard"]["database"];
 
$DBTYPE_reporting_dashboard = 'mysqlt';
$conn_reporting_dashboard = &ADONewConnection($DBTYPE_reporting_dashboard);
$conn_reporting_dashboard->PConnect($DBHOST_reporting_dashboard,$DBUSER_reporting_dashboard,$DBPASSWORD_reporting_dashboard,$DBNAME_reporting_dashboard); 

}
catch (Exception $e){
	trigger_error($e->getMessage(),E_USER_WARNING);
	die;
}
?>
