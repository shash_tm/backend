<?php 

class TrustAuthenticConstants{
	const TrustAuthenticConstantFemale = 10;
	const TrustAuthenticConstantMale = 30;
	const ABTestVerification = false;        // true to enable A/B testing
}

class EndorsementConstants{
	const MinimumEndorsementCount= 1;
	const TrustScoreForEachEndorsement= 5;
	const NetScore = 15;
}

$tbNumbers['score']['facebook'] = 30;//25;
$tbNumbers['credits']['facebook'] = 0;//20;
$tbNumbers['validity']['facebook'] = 60;

//$tbNumbers['score']['linkedin'] = 10;
$tbNumbers['score']['linkedin'] = 15;//20;
$tbNumbers['credits']['linkedin'] = 0;//20;
$tbNumbers['validity']['linkedin'] = 60;

$tbNumbers['score']['mobile'] = 10;
$tbNumbers['credits']['mobile'] = 0;//10;
$tbNumbers['validity']['mobile'] = 60;

$tbNumbers['score']['email'] = 10;
$tbNumbers['credits']['email'] = 0;//10;
$tbNumbers['validity']['email'] = 60;

$tbNumbers['score']['photoid'] = 30;//25;
$tbNumbers['credits']['photoid'] = 0;//20;
$tbNumbers['validity']['photoid'] = 60;

$tbNumbers['score']['address'] = 15;
$tbNumbers['credits']['address'] = 0;//30;
$tbNumbers['validity']['address'] = 60;

//$tbNumbers['score']['employment'] = 15;
$tbNumbers['score']['employment'] = 15;
$tbNumbers['credits']['employment'] = 0;//30;
$tbNumbers['validity']['employment'] = 60;


$tbNumbers['score']['endorsement'] = 15;
$tbNumbers['credits']['endorsement'] = 0;//30;
$tbNumbers['validity']['endorsement'] = 60;
//$smarty->assign("tbNumbers",$tbNumbers);


class SystemMessages{
	public static function getSystemAlertMessages(){
		//Trust Score lower than 25%
		$systemAlert[0] = 'To make your profile visibile to other TrulyMadly members, you need a Trust Score of 25%.';
		
		//No Credits
		$systemAlert[1] = 'Earn credits and send personalised messages  by building your Trust Score or purchasing a Credit Plan.';
		
		//Facebook Connected but No Credits
		$systemAlert[2] = 'Earn credits and see common Facebook friends by building your Trust Score or purchasing a Credit Plan.';
		
		//Have Credits
		$systemAlert[3] = 'Get more profile views by building your Trust Score.';
		
		return $systemAlert;
	}
	
	public static function getCreditsMessage(){
		$message['facebook'] = 'Facebook Verification';
		$message['linkedin'] = 'LinkedIn Verification';
		$message['employment'] = 'Employment Verification';
		$message['id'] = 'ID Verification';
		$message['mobile_verified'] = 'Phone Verification';
		$message['address'] = 'Address Verification';
		$message['credit_card'] = 'Purchased Credits';	
		$message['corporate'] = 'Corporate Credits';
		return $message;	
	}
}



?>
