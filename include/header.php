<?php

require_once dirname ( __FILE__ ) . "/config.php";
require_once dirname ( __FILE__ ) . "/Utils.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";




class header{

	private $user_id;
	private $redis;

	function __construct($user_id){
		global $redis;
		$this->user_id = $user_id;
		$this->redis = $redis;
	}

	public function getHeaderValues(){
		global  $dummy_male_image,$dummy_female_image;//, $admin_id;
		$uu = new UserUtils();
		$uuDBO = new userUtilsDBO();
		$ud = new UserData($this->user_id);
		$output['name'] = $ud->fetchName();
		$gender = $ud->fetchGender(); 
		$output['gender'] = $gender;
		$images = $uu->getActiveImages(array($this->user_id), $gender, $myProfile = true);
		$output['profile_pic'] = $images[$this->user_id]['profile_pic_thumbnail'];
		if($output['profile_pic'] == null){
			$output['profile_pic'] = ($gender == "M")?$dummy_male_image:$dummy_female_image; 
		}
							
		$imgs[$row['user_id']] = $row['gender'] =='M'? $this->dummy_male_image:$this->dummy_female_image;
		
		$creditRow = $uuDBO->getCurrentCredits(array($this->user_id), true);
		//$myLikesData = $uuDBO->getMyLikesQueue($this->user_id);
		$maybeData = $uuDBO->getMayBeQueueCount($this->user_id);
		if(!empty($maybeData) && isset($maybeData))$output['myMaybe'] = $maybeData['count'];
		
		
		$credtis = $creditRow['current_credits'];
		$output['credits'] = $credtis;
		
		$counters = $uu->getNotificationCounters($this->user_id);
		$output['mutual_like_count'] = $counters['mutual_like'];
		$output['message_count'] = $counters['message_count'];
	//	$output["contact_link"] = $uu->generateMessageLink($this->user_id, $admin_id);
		return $output;
	}
}


?>