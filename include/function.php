<?php
include_once (dirname ( __FILE__ ) . '/../TrustBuilder.class.php');
require_once (dirname ( __FILE__ ) . '/Utils.php');
require_once (dirname ( __FILE__ ) . '/../abstraction/query_wrapper.php');
//general purpose functions
class functionClass {
	
	
	//register a new user
	public function Register($data) {
		global $conn;
		 
		$register_code = $this->random ( 10 );
		$password = md5 ( $data ['password'] );
		try {
			
			$query="insert into user_registration (fname,lname,gender,email_id,password,register_code,register_date) values(?,?,?,?,?,?,Now())";
			$param_array=array (
			$data ['fname'],
			$data ['lname'],
			$data ['gender'],
			$data ['email'],
			$password,
			$register_code
			);
			$tablename="user_registration";
			Query_Wrapper::INSERT($query, $param_array, $tablename);
			
			/*$conn->Execute ( $conn->prepare ( "insert into user_registration (fname,lname,gender,email_id,password,register_code,register_date) values(?,?,?,?,?,?,Now())" ), array (
			$data ['fname'],
			$data ['lname'],
			$data ['gender'],
			$data ['email'],
			$password,
			$register_code
			) );*/
		} catch ( Exception $e ) {
		}
		return $register_code;
	}

	//send email to user

	public function goToMail($mail, $code) {
		global $smarty, $baseurl;
		$NAME = 'Trulymadly';
		$email = 'robin@trulymadly.com';
		$header = "From:" . $NAME . "<" . $email . ">\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= "Content-type: text/html\r\n";

		// echo $header;die;
		try {
			//  mail ( $mail, "Please Verify", "Please click the following link to verify your account: " . $_SERVER ['SERVER_NAME'] . "/trulymadly/verifyEmail.php?code=" . $code . "&mail=" . $mail, $header );
		} catch ( Exception $e ) {
			echo $e->getMessage ();
			die ();
		}
		$url="$baseurl/verifyEmail.php?code=" . $code . "&mail=" . $mail;
		$smarty->assign("verifyEmail","Please click the following link to verify your account: <a href='$url'>$url</a>");
	}



	//check whether email is already registered or not
	public function checkEmail($mail,$password='') {
		global $conn;
		try{
			$rs=$conn->Execute($conn->prepare("select * from user where email_id=?"),$mail);
		}
		catch(Exception $e) {
			trigger_error($e->getMessage(),E_USER_WARNING);
			die;
		}

		if($rs->_numOfRows>0) {
			$result=$rs->GetRows();
			if($password==''&&$result[0]['fid'])
			return 'FB_USER';
			if($password!='') {
				$rs=$conn->Execute($conn->prepare("select * from user where email_id=? and password= ?"),array($mail,md5($password)));
				if($rs->_numOfRows>0)
				return 'USER_EXISTS';
				else return 'USER_ABSENT';
			}
			return 'USER_EXISTS';

		}
		else return 'USER_ABSENT';
	}

	private function getCompany(){
		//var_dump($_SERVER);
		$company_name=$_REQUEST['cn'];
		$hash=$_REQUEST['hs'];
		$salt="tmrocksforcompanies";
		$code=md5($company_name.$salt);
		if($code==$hash){
			return $company_name;
		}
		else return null;
	}
	
	
	public function createNewUser($data) {
	global $conn;
	/*
	$name = $data['fname'];
	$nameArray = explode(" ",$name);
	
	if(count($nameArray)>1){
		$data['lname'] = end($nameArray);
		
		$nameArray[count($nameArray)-1] = "";
		
		$name = implode($nameArray,' ');
		$data['fname'] = trim($name);
	}else{
		$data['lname'] = "";
	}
	*/

	$company_name=$this->getCorporateDetails();	
	//var_dump($company_name);
	//exit;
	
	$query="insert into user(user_id,fname,lname,gender,email_id,password,fid,is_fb_connected,status,
			last_login,test_score,phone_number,company) 
			values('',?,?,?,?,?,'','N','incomplete',Now(),5,?,?)";
	$param_array=array($data['fname'],$data['lname'],$data['gender'],$data['email'],md5($data['password']),$data['mobn'],$company_name);
	$tablename="user";
	Query_Wrapper::INSERT($query, $param_array, $tablename);
		
	 /*$conn->Execute($conn->prepare("insert into user(user_id,fname,lname,gender,email_id,password,fid,is_fb_connected,status,
			last_login,test_score,phone_number,company) 
			values('',?,?,?,?,?,'','N','incomplete',Now(),5,?,?)"),
			array($data['fname'],$data['lname'],$data['gender'],$data['email'],md5($data['password']),$data['mobn'],$company_name));*/
			
			
			$user_id = $conn->Insert_ID();
	$campaignDetails = $this->getCampaignDetails();
		if(isset($campaignDetails['utm_campaign']) || isset($campaignDetails['utm_medium']) || isset($campaignDetails['utm_source'])){
			
			$query="INSERT INTO campaign values(?,?,?,?, now())";
			$param_array=array($user_id, $campaignDetails['utm_source'],  $campaignDetails['utm_medium'],$campaignDetails['utm_campaign'] );
			$tablename="campaign";
			Query_Wrapper::INSERT($query, $param_array, $tablename);
			
			/*$sql = $conn->Prepare("INSERT INTO campaign values(?,?,?,?, now())");
			$conn->Execute($sql, array($user_id, $campaignDetails['utm_source'],  $campaignDetails['utm_medium'],$campaignDetails['utm_campaign'] ))	;*/	
	}


	}
	public static function setSessionStatus($status,$first_time=false) {
		$_SESSION['status']=$status;
		if($first_time)
			$_SESSION['first_login']=true;
	}
	
	//set session for user
	public function setSession($source,$value) {
		global $conn;
		try {
			if ($source=='FB_USER')
			{
				$rs = $conn->Execute ( $conn->prepare ( "select status,user_id from user where fid=?" ), $value );
			}
			else if($source=='FB_USER_test'){
				$rs = $conn->Execute ( $conn->prepare ( "select status,user_id from user where user_id=?" ), $value );
			}
			else
			{
				$rs = $conn->Execute ($conn->prepare("select status,user_id from user where email_id=?"),$value);
			}
		} 
		catch ( Exception $e ) {
		}
		$dataArray = $rs->FetchRow ();
		$_SESSION ['user_id'] = $dataArray ['user_id'];
		$_SESSION['status']=$dataArray ['status'];
		$_SESSION['lastRefresh'] = time();
		
		
		$sql="insert into user_lastlogin(user_id,last_login) values(?,now()) on Duplicate key UPDATE last_login=now()";
		$param_array=array($dataArray ['user_id']);
		$rs=Query_Wrapper::INSERT($sql, $param_array,'user_lastlogin');
		//$rs = $conn->Execute($conn->prepare("insert into user_lastlogin(user_id,last_login) values(?,now()) on Duplicate key
		//			UPDATE last_login=now()"),array($dataArray ['user_id']));
		//$this->refreshSession($_SESSION ['user_id']);
	}
	
	public static  function refreshSession($value){
		if(	!isset($_SESSION ['admin_id'])){
			global $conn;
			$rs = $conn->Execute ($conn->prepare("select status,gender from user where user_id=?"),$value);
			$dataArray = $rs->FetchRow ();
			$_SESSION['status']=$dataArray ['status'];
			$_SESSION['lastRefresh'] = time();
			$_SESSION['gender']=$dataArray['gender'];
		
			$sql="Update user_lastlogin set last_login = now() where user_id = ?";
			$param_array=$value;
			$rs=Query_Wrapper::UPDATE($sql, $param_array,'user_lastlogin');
			//$rs = $conn->Execute($conn->prepare("Update user_lastlogin set last_login = now() where user_id = ?"),$value);
			
		}
	}
	
	
public function setNewCredits($user_id,$num_connections) {
	global $conn;
	try {
	
		$sql="insert into user_current_credits (user_id,current_credits,total_assigned_credits) values(?,25,25)";
		$param_array=array($user_id);
		$tablename="user_current_credits";
		$rs=Query_Wrapper::INSERT($sql, $param_array,$tablename);
		
	/*$conn->Execute($conn->prepare("insert into user_current_credits (user_id,current_credits,total_assigned_credits) values(?,25,25)"),
			array($user_id));*/
	
		$sql="insert into user_trust_score (user_id,fb_connections,trust_score) values(?,?,25)";
		$param_array=array($user_id,$num_connections);
		$tablename="user_trust_score";
		$rs=Query_Wrapper::INSERT($sql, $param_array,$tablename);
		
	/*$conn->Execute($conn->prepare("insert into user_trust_score (user_id,fb_connections,trust_score) values(?,?,25)"),
			array($user_id,$num_connections));*/
		
		$sql="insert into user_credit_logs (user_id,credit_assgined,credit_type,tstamp) values(?,25,'facebook',now())";
		$param_array=array($user_id);
		$tablename="user_credit_logs";
		$rs=Query_Wrapper::INSERT($sql, $param_array,$tablename);
		
	/*$conn->Execute($conn->prepare("insert into user_credit_logs (user_id,credit_assgined,credit_type,tstamp) values(?,25,'facebook',now())"),
			array($user_id));*/
	}
	catch(Exception $e) {
	echo $e->getMessage();
	die;	
	}
	
	
}
	public static function getUserDetailsFromSession(){
	//	$_SESSION['plan'] ='unlimited';
		if(isset($_SESSION ['user_id'])) {
		if(!isset($_SESSION['lastRefresh'])|| time()- $_SESSION['lastRefresh'] > 79200){
			functionClass::refreshSession($_SESSION['user_id']);
		}
		$session_arr = array(
						"gender"=>$_SESSION['gender'],
						"user_id" => $_SESSION ['user_id'],
						"status" => $_SESSION['status'],
						"payment_plan" => $_SESSION['plan'],
						"first_login" => $_SESSION['first_login']);
		}
		return $session_arr;  
	}

	public function setCorporateDetails(){
		if(isset($_REQUEST['cn'])){
			$_SESSION ['cn']=$_REQUEST['cn'];
			$_SESSION ['hs']=$_REQUEST['hs'];
		}
	}
	
	public function getCorporateDetails() {
		if (isset ( $_SESSION ['cn'] )) {
			$company_name = $_SESSION ['cn'];
			$hash = $_SESSION ['hs'];
			$salt = "tmrocksforcompanies";
			$code = md5 ( $company_name . $salt );
			if ($code == $hash) {
				return $company_name;
			}
		}
		return null;
	}
	
	public function setCampaignDetails(){
		//echo $_REQUEST['utm_campaign']; die;
		//var_dump($_REQUEST); die;
		if(isset($_REQUEST['utm_campaign']) || isset($_REQUEST['utm_medium']) || isset($_REQUEST['utm_source'])){
			$_SESSION['utm_campaign'] = $_REQUEST['utm_campaign'];
			$_SESSION['utm_medium'] = $_REQUEST['utm_medium'];
			$_SESSION['utm_source'] = $_REQUEST['utm_source'];
		}
		//var_dump($_SESSION); die;
		//echo $_SESSION['utm_source'];
	}
	
	public function getCampaignDetails(){
		//var_dump($_SESSION);//  $_SESSION['utm_source'];
		return array("utm_source" => $_SESSION['utm_source'], "utm_medium" => $_SESSION['utm_medium'], "utm_campaign" => $_SESSION['utm_campaign']  );
	}
	
	//redirect the user according to its status
	public static function redirect($action, $fromMobile = false,$end=false) {
		global $baseurl,$conn;
		$incomplete_array=array('register','admin','register_save');
		$active_array=array('register_save','dashboard','admin','matches','profile','messages','messages_full_conv','editpartner','editprofile','trustbuilder','payment','photo', 'likes','psycho', 'myLikes', 'maybe', 'deactivate');
		$inactive_array=array('index','admin');

		$session=self::getUserDetailsFromSession();
		
		if($end) {

			$trust=new TrustBuilder($session['user_id']);

			if($trust->authenticateUser()) {
				self::setSessionStatus('authentic',true);
				$session['status']='authentic';
			}
			else {
				self::setSessionStatus('non-authentic',true);
				$session['status']='non-authentic';
			}
		}
		
		$status='inactive';
		if(isset($session['user_id']))
			$status=self::getUserStatus($session['user_id']);//$session['status'];
		if($status == 'blocked' || $status == 'suspended'){
			if ($_SESSION ['admin_id']){
				if(in_array($action,$active_array)) return false;
				//else{ header("Location:".$baseurl."/dashboard.php"); exit;}
				else
					self::move("matches",$fromMobile); 
				
			}else {
				
			
			if(in_array($action,$inactive_array)) return false;
			else 
				self::move("logout",$fromMobile);
			 	//header("Location:".$baseurl."/logout.php");
			}
		}
		elseif($status=='inactive') {
			if(in_array($action,$inactive_array)) return false;
			else{
				//self::move("index",$fromMobile);
				if($fromMobile == true){
					print_r(json_encode(array("responseCode"=>401)));exit;
				}else
					header("Location:".$baseurl."/index.php"); exit;
			}
		}
		else if($status=='authentic'||$status=='non-authentic') {
			if(in_array($action,$active_array)) return false;
			//else{ header("Location:".$baseurl."/dashboard.php"); exit;}
			else{
				self::move("matches",$fromMobile);
				/*if($fromMobile == true){
					print_r(json_encode(array("responseCode"=>"401")));exit;
				}else
					header("Location:".$baseurl."/matches.php"); exit;*/
			}
		}
		else if($status=='incomplete') {
			if(in_array($action,$incomplete_array)) return false;
			else{
				self::move("register",$fromMobile);
				/*
				if($fromMobile == true){
					print_r(json_encode(array("responseCode"=>"401")));exit;
				}else 
					header("Location:".$baseurl."/register.php"); exit;*/
			}
		}
		/*else if($status=='suspended'){
			header("Location:".$baseurl."/logging/deactivate.php");
		}*/
	}

	public static function move($to,$source=false){
		global $baseurl;
		if($source){
			print_r(json_encode(array("responseCode"=>401)));exit;
		}else
			header("Location:".$baseurl."/".$to.".php"); exit;
	}
	//retunrs random str of length $digits
	public function random($digits) {
		return str_pad ( rand ( 0, pow ( 10, $digits ) - 1 ), $digits, '0', STR_PAD_LEFT );
	}	
	
	public function getFbLikes() {
	global $conn;
	$session = self::getUserDetailsFromSession();
	$fb_likes=$conn->Execute("select name,category_name from fb_likes")->GetRows();
	foreach($fb_likes as $fb_like)
	$allLikes[strtolower($fb_like['name'])]=$fb_like['category_name'];
	$rs2=$conn->Execute("select likes from user_facebook where user_id='".$session['user_id']."'");
	if($rs2->_numOfRows>0) {
		$result=$rs2->GetRows();
		$likes=(array)json_decode($result[0]['likes']);
		foreach($likes as $category=>$like) {
			$like=(array)$like; 
			if($like['count']>0) {
				unset($like['count']);
				if(isset($allLikes[strtolower($category)]))
					foreach($like as $lik)
						$toSelect[$allLikes[strtolower($category)]]['favorites'][]=$lik;
						//else $conn->Execute("insert into log_fb_likes values('".$category."')"); //snehil wanted this table log_fb_likes
			}
		}
	}
return $toSelect;
}

public static function getUserStatus($user_id){
	global $conn;
	$rs = $conn->Execute ($conn->prepare("select status from user where user_id=?"),$user_id);
	$data = $rs->FetchRow();  
	return $data['status']; 
}

public function isMobileLogin(){
	$headers = Utils::getAllSystemHeaderFields();
	return $headers['login_mobile'];
}

public function getSourceofReg(){
	
	$source = $this->getSourceFromHeader();
	if(isset($source) && $source == 'iOSApp'){
		$registered_from="ios_app";
		return $registered_from;
	}elseif (isset($source) && $source == 'windows_app') {
		$registered_from="windows_app";
		return $registered_from;
	}
	$from_app = $this->isMobileLogin();
	$from_mobile = $this->getAgent();
	$registered_from = null;
	if($from_app){
		$registered_from="android_app";
	}else if($from_mobile){
		$registered_from="mobile_web";
	}else{
		$registered_from="web";
	}
	return $registered_from;
	
}

public function getAgent(){
	$useragent=$_SERVER['HTTP_USER_AGENT'];
	return  (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)));
}

public function getHeader(){
	$headers = Utils::getAllSystemHeaderFields(); 
	if(!isset($headers['app_version_code'])){
		return 1;
	}
	return $headers['app_version_code'];
}

public function escapeString($str) {
$str=str_replace(",","-",$str);
//$str=str_replace("'","`",$str);
return $str;
}

// for getting source from headers
public function getSourceFromHeader(){
	$headers = Utils::getAllSystemHeaderFields();
	if(!isset($headers['source'])) {
		return null;
	}
	return $headers['source'];
}

public static function checkHash($resource,$oldHash=null)
{
	$newHash=md5($resource);
	
	if($oldHash==NULL)
	{
		
		$response['status']=false;
		$response['hash']=$newHash;
	}
	else if($oldHash==$newHash)
	{
		$response['status']=true;
	}
	else 
	{	
		$response['status']=false;
	    $response['hash']=$newHash;
	}
	//var_dump($response);
	return $response;
}
	public function isWebPoll(){
		$var=$_REQUEST['from_web_poll'];
		$var=filter_var($var,FILTER_VALIDATE_BOOLEAN);
		return $var;
	}

}

?>
