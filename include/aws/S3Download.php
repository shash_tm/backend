<?php 
// Include the AWS SDK using the Composer autoloader.
require 'aws-autoloader.php';

require_once dirname ( __FILE__ ).'/../config.php';
use Aws\S3\S3Client;

class S3Download {
	public static function downloadFile($bucket,$file) {
		global $config;
		
		$bucket = "trulymadlybackups/sqlSlaveDumps/".$bucket;
		$keyname = $file;
		
		// Instantiate the client.
		//$s3 = S3Client::factory($config['amazon_s3']['access_key'],$config['amazon_s3']['secret_key']);
		$s3 = S3Client::factory(array('key'=>$config['amazon_s3']['access_key'],'secret'=>$config['amazon_s3']['secret_key'],'region'=>'ap-southeast-1'));
	try{	
		echo "Download start";
		$result = $s3->getObject(array(
				'Bucket' => $bucket,
				'Key'    => $keyname,
				'SaveAs' => "/mysql/mysql_backup/$keyname"
		));
		
		// Contains an EntityBody that wraps a file resource of /tmp/data.txt
		echo $result['Body']->getUri() . "\n";
	}
	catch(Exception $e) {
		echo $e->getMessage();
	}	
     }
}
?>
