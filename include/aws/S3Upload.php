<?php 
// Include the AWS SDK using the Composer autoloader.
require 'aws-autoloader.php';

require_once dirname ( __FILE__ ).'/../config.php';
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\S3Client;
//use Aws\Common\Credentials\CredentialsInterface;

class S3Upload {
	
	public static function upload($file,$saveTo,$key) {
		global $config;
		
		$bucket = $saveTo;
		$keyname = $key;
		
		// Instantiate the client.
		$s3 = S3Client::factory(array('key'=>$config['amazon_s3']['access_key'],'secret'=>$config['amazon_s3']['secret_key'],'region'=>'ap-southeast-1'));
		//$credentials = new Aws\Credentials\Credentials($config['amazon_s3']['access_key'], $config['amazon_s3']['secret_key']);	
		//$s3 = new S3Client(array('region' => 'ap-southeast-1', 'credentials' => $credentials));

		//Prepare the upload parameters.
		$uploader = UploadBuilder::newInstance()
		->setClient($s3)
		->setSource($file)
		->setBucket($bucket)
		->setKey($keyname)
		->setMinPartSize(25 * 1024 * 1024)
        ->setOption('Metadata', array(
					'param1' => 'value1',
					'param2' => 'value2'
			))
		->setOption('ACL', 'public-read')
		->setConcurrency(3)
		->build();
		
		
		// Perform the upload. Abort the upload if something goes wrong.
		try {
			$uploader->upload();
			echo "Upload complete.\n";
		} catch (MultipartUploadException $e) {
			$uploader->abort();
			echo "Upload failed.\n";
			echo $e->getMessage() . "\n";
		}
		
	}
}

?>
