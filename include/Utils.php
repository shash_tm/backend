<?php

require_once dirname ( __FILE__ ).'/S3.php';
require_once dirname ( __FILE__ ) . "/../logging/UnsubscriptionClass.php";
require_once dirname ( __FILE__ ).'/config.php';


class Utils{

	public static $salt = "trulyrocks";
	public static $NAstring = "n/a";
	// public static $appVersionForNonAuthenticActionEnable = 100;
	public static $appVersionForStickers = 56;
	public static $appVersionForDefaultIncome = 65;
	public static $appVersionForMarketLinks = 42;
	public static $messageDeletionConstant = -1;
	
	public static $recoStagedReleaseConstant = 1;   //to assign code to every  user
	public static $recommendation_count = 10;
	public static $tileCountForNonAuthenticGirlsForSurvey = 10;
	//public static $availability_slots_limit = 160;
	public static $availability_slots_limit = 200;
	public static $availability_slots_limit_18_20 = 500;
	public static $availability_slots_limit_21_22 = 250;
	public static $availability_slots_limit_23_26 = 250;
	public static $recommendation_count_male = 10;
	public static $recommendation_count_female = 10;
  //public static $like_threshold_female = 30;
  //public static $like_threshold_female = 70;
	public static $like_threshold_female = 90;
	public static $likeHideCountForPIGenerationFemale = 10;
	public static $likeHideCountForPIGenerationMale = 20;
	public static $defaultBucketConstant = -1;
	public static $maleMatchesExpiryTime = 3600;//3600*3;
	public static $femaleMatchesExpiryTime = 10800; //3600*3  
	public static $matches_repeat_profile_expire = 86400; //3600*24
	
	public static $activeChatsThreshold = 3;
	public static $recommendationFreshLikesCountGoodBucketsLessChats = 20; 
	public static $recommendationRepeatCountGoodBucketsLessChats = 10; 
	public static $recommendationFreshLikesCountBadBucketOrMoreChats = 10;
	public static $recommendationRepeatCountBadBucketOrMoreChats = 5; 
	public static $phoneNumberVerifyThreshold=5;
	public static $otpTriesThreshold=4;
	public static $dailyFemaleLikeThreshold=80; //for monitoring high acting females
	public static $mutualFriendsEngine = "RecommendationEngine_MutualFriends";
	  
	
	public static $responseCode = "responseCode";
	public static $mayBeExpiryDays = 10;
	public static $techTeam = "himanshu@trulymadly.com, shashwat@trulymadly.com, arpan@trulymadly.com";
	public static $mobileTechTeam = "himanshu@trulymadly.com, shashwat@trulymadly.com, udbhav@trulymadly.com";
	public static $managementTeam = "himanshu@trulymadly.com, shashwat@trulymadly.com, rahul@trulymadly.com";
	public static $machineStatusTeam = "rajesh.singh@trulymadly.com, shashwat@trulymadly.com, sumit@trulymadly.com, vikash@trulymadly.com";
	public static $contentTeam = "madhuri@trulymadly.com, saumya.agrawal@trulymadly.com";
	public static $negativeRecoTableName = "user_recommendation_negative_match_scores";
	public static $redis_keys = array(
                                            "userAction" => "ua:", 
                                            "matches" => "u:m:",
                                            "matches_with_scores" => "u:m:s:",
                                            "match_action_count"=> "m:c:",
                                            "mutual_like_notification" => "ml:n:",
                                            "mutual_like_only_list" => "ml:l:",
                                            "conversation_list_union" => "conversation_list_union:",
                                            "messages" => "mCnt:",
                                            "deleteMsg" => "d:m:",
                                            "like_count" => "u:l:",
                                            "fresh_like_count" => "f:l:",
                                            "activity" => "u:ac:",
                                            "notification" => "u:n:",
                                            "push_notification" => "p:n",
                                            "mailing_queue" => "mail:q",
                                            "gearman_key" => "gearman",
                                            "user_data" => "u:d:",
                                            "female_like_day" =>"female:like:",
                                            "female_like_checkpoint" => "check:female:like:",
                                            "photomodule_error" => "p:m:e",
                                            "cache_next_serve_key" => "cnsk:",
                                            "datespot" => "cd:",
                                            "datespot_zones"=> "d_zones:",
                                            "datespot_city"=> "d_city:",
                                            "densityBasedAvailabilityCountersLimits" => "countersLimits",
                                            "last_profile_before_select" => "m:one_before_select:",
                                            "matches_repeat_profile"=>"m:repeat:",
                                            "select_matches_for_the_day" => "m:select:");
	
	
	// fb mutual friends related configs
	public static $mutual_friends_enabled = true; // global flag to enable/disable feature
	public static $femaleMutualFriendsLikedMatchesLimit = 360; // to maintain 9:1 ratio for second key
	public static $femaleMutualFriendsTotalMatchesLimit = 400; // total limit for second key - female
	public static $maleMutualFriendsTotalMatchesLimit = 80; // total limit for second key - male
	public static $mutual_friends_weight = 0.25; // discussion on this weight
	public static $min_percent_to_shuffle = 80; // for female case, if liked profiles % < 80 then (first liked, then fresh)
	public static $mutliplier_for_shuffling = 3; // if top x profiles are with mutual friends then shuffle top 3x profiles
	public static $token_expired_logout_window = 604800; // week - in seconds 7 * 24 * 60 * 60
	
	// education rules in matching enable/disable flag
	public static $education_rules_enabled = true;
	
	// fb likes
	public static $suggestions_count = 50;
	public static $page_categories_db_fields_map = array('music' => 'music_favorites', 'books' => 'books_favorites',
			'travel' => 'travel_favorites', 'movies' => 'movies_favorites', 'others' => 'other_favorites',
			'food' => 'food_favorites');
	public static $page_categories_db_fields_reverse_map = array('music_favorites' => 'music',  'books_favorites' => 'books',
			 'travel_favorites' => 'travel',  'movies_favorites' => 'movies', 'other_favorites' => 'others',
			 'food_favorites' => 'food');
	public static $merge_categories_map = array('travel_favorites' => 'food_favorites');
	public static $merge_categories_reverse_map = array('food_favorites' => 'travel_favorites');
	public static $ugc_enabled = true;
	public static $fb_image_base_url = "http://graph.facebook.com";
	public static $pages_to_consider_from_redis_search = 1000; // SATISH TODO: discuss these params
	public static $pages_to_return_in_search = 10;
	public static $redis_search_delimiter = "##";
	public static $ugc_id_max_length = 250;
	public static $redis_score_for_fb_page = 1;
	public static $redis_suffix_for_fb_pages = "_pages";
	public static $fallback_search_from_db_enabled = false;
	/*
	 public static $constants = array(
	 "slave" => "slave" ,
	 "master" => "master");*/


	public static function notifyTechTeam($mailer_type){
		$subject = "mailed todays " . $mailer_type;
		$from = gethostname();//"admin@trulymadly.com";
		Utils::sendEmail(Utils::$techTeam, $from, $subject, null);

	}

	/**
	 * return if the current time is an even five minute interval or odd
	 */
	public static function getCurrentRedisIntervalInSpanOfFiveMinutes($redis){
		$redisTime = $redis->TIME();
		$redisUnixTimeInMin = ($redisTime[0])/60;
		$minuteOfHour = floor($redisUnixTimeInMin/5)%2;
		return $minuteOfHour;
	}



	/**
	 * get all the system required fields
	 * call this to receive array of fields required from header
	 * instead of calling the same header call again and again
	 * @return array of required fields
	 * @Himanshu
	 */
	public static function getAllSystemHeaderFields(){
		$header_arr = getallheaders();
		$header_params['app_version_code'] = $header_arr['app_version_code'];
		$header_params['source'] = $header_arr['source'];
		$header_params['login_mobile'] = ($_REQUEST['login_mobile'] == "true" || $header_arr['login_mobile'] == "true" || $_REQUEST['login_mobile'] == true || $header_arr['login_mobile'] == true)?true:false;
		$header_params['isMessageMatchMergedList'] = ($_REQUEST['isMessageMatchMergedList'] == "true" || $header_arr['isMessageMatchMergedList'] == "true" || $_REQUEST['isMessageMatchMergedList'] == true || $header_arr['isMessageMatchMergedList'] == true)?true:false;
		$header_params['app_version_name']=$header_arr['app_version_name'];
		if(isset($header_arr['device_id']))
		$header_params['device_id']=$header_arr['device_id'];
		  
		if($_REQUEST['login_mobile'] == "true" || $header_arr['login_mobile'] == "true" || $_REQUEST['login_mobile'] == true || $header_arr['login_mobile'] == true){
			$header_params['device_type'] = "mobile";
		}else{
			$header_params['device_type'] = "web";
		}
                $header_params['batchCount'] = $_REQUEST['batchCount'];
        $header_params['mutual_friends'] = (isset($_REQUEST['mutual_friends']) && $_REQUEST['mutual_friends'] == "true") ? True : False;
        $header_params['fetch_all_matches'] = (isset($_REQUEST['fetch_all_matches']) && $_REQUEST['fetch_all_matches'] == "true") ? True : False;
		return $header_params;
	}

	public static function custom_unset(&$array=array(), $key=0) {
		if(isset($array[$key])){
	
			// remove item at index
			unset($array[$key]);
	
			// 'reindex' array
			$array = array_values($array);
	
			//alternatively
			//$array = array_merge($array);
	
		}
		return $array;
	}
	
	public static function getDeviceType($full_header = null){
		$device_type = 'html';
		$header_arr = getallheaders();
		$header_login_param = $header_arr['login_mobile'];
		if($_REQUEST['login_mobile'] == "true" || $header_login_param == "true"){
			$device_type = "mobile";
		}
		return $device_type;
	}

	public static function getMatchKey($id){
		return Utils::$redis_keys['matches'] . $id;
	}

	public static function getNotificationKey($id){
		$notificationKey = "u:n:";
		return $notificationKey . $id;
	}

	/**
	 * get the app version from header call
	 */
	public static function getAppVersionFromHeader(){
		$header_arr = getallheaders();
		$header_app_param = $header_arr['app_version_code'];
		return $header_app_param;
	}
	
	
	/**
	 * returns secs left to midnight from the day passed
	 */
	public  static function timeTillMidnight($day = 1 ){
		//$midnight = strtotime("tomorrow 00:00:00");
		$tomorrow = mktime(0, 0, 0, date('m'), date('d') + $day, date('Y'));
		$diff = $tomorrow - time();
		return $diff;
	}

	public static function sendEmail($to, $from, $subject, $message,$ishtml=false){
		$headers = "From: $from \r\n";
		if($ishtml=="true"){
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			mail($to,$subject,$message,$headers);
		}
		else{
			mail($to,$subject,$message,$headers);
		}
	}
	
    public static function sendAttachmentEmail($to, $from, $subject, $message,$attachment){
    	    $multipartSep = '-----'.md5(time()).'-----';
    	$headers = array(   "From: $from",
        					"Reply-To: $from",
        					"Content-Type: multipart/mixed; boundary=\"$multipartSep\""
                      );
    	$attached = chunk_split(base64_encode(stream_get_contents($attachment)));
          // $attached = chunk_split(base64_encode(file_get_contents($attachment)));              
           $body = "--$multipartSep\r\n"
        . "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\r\n"
        . "Content-Transfer-Encoding: 7bit\r\n"
        . "\r\n"
        . "$body\r\n"
        . "--$multipartSep\r\n"
        . "Content-Type: text/csv\r\n"
        . "Content-Transfer-Encoding: base64\r\n"
        . "Content-Disposition: attachment; filename=\"Website-Report-" . date("F-j-Y") . ".csv\"\r\n"
        . "\r\n"
        . "$attached\r\n"
        . "--$multipartSep--";   
        
    	$mail_sent=mail($to,$subject,$body,implode("\r\n", $headers));  
    	if($mail_sent)
    	echo "attachment mail sent";
		else 
		echo "mail not sent"; 
    }
    
	public static function getSlaveRecoTableName(){
		$today = date("ymd");
		$todayTable = 'user_recommendation_slave_'.$today;
		return $todayTable;
	}

	public static function getSlavefbConnTableName(){
		$today = date("ymd");
		$todayTable = 'user_facebook_mutual_connections_'.$today;
		return $todayTable;
	}

	public static function humanTime($time){
		$time = strtotime($time);
		//$time = strtotime ( '-330 minutes', strtotime ( $time ) );
		$time = time() - $time; // to get the time since that moment

		$tokens = array (
			604800 => 'w',
			86400 => 'd',
			3600 => 'hour',
			60 => 'min',
			1 => 'sec'
		);
		
		if($time<0) return "Today";
		
		foreach ($tokens as $unit => $text) {
			if ($time < $unit) continue;
			$numberOfUnits = floor($time / $unit);
		
			if($text == "hour" || $text == "min" || $text == "sec") return "Today";
			
			if(($text == "w" && $numberOfUnits >1 ))  return '1w+ ago';
			
			if($text == "w" && $numberOfUnits <2 ) return '1w ago';
			
			return $numberOfUnits.$text.' ago';
			
		}
	}

	public static function heightCentimeterToFeet($cms) {
		$inches = $cms/2.54;
		$feet = intval($inches/12);
		$inches = $inches%12;
		return sprintf('%d ft %d ins', $feet, $inches);
	}

	public static function heightInchToFeet($inch) {
		$feet = $inch/12;
		$inches = $inch%12;
		return sprintf('%d&rsquo; %d&quot;', $feet, $inches);
	}

	public static function GMTTOIST($time) {
		return date ( 'M d,Y h:i A', strtotime ( '+330 minutes', strtotime ( $time ) ) );
	}

	public static function strToDate($date) {
		return date ( 'M d,Y', strtotime ( $date )  );
	}

	public static function GMTTOISTfromUnixTimestamp($time) {
		return date ( 'M d,Y h:i A', strtotime ( '+330 minutes',  $time  ) );
	}

	public static function getDateGMTTOIST($time){
		return date ( 'd/m/Y', strtotime ( '+330 minutes', strtotime ( $time ) ) );
	}

	public static function getDateGMTTOISTfromUnixTimestamp($time){
		return date ( 'd-m-Y', strtotime ( '+330 minutes', $time  ) );
	}

	public static function calculateCheckSum($str){
		
		$md5hash= md5($str . '|'. $salt);
		return $md5hash;
	}


/**
 * @Himanshu
 * @param unknown_type $urls - urls to make curl call to
 * @param unknown_type $options - to set options like it'll be POST call or you want a response in return etc
 */
	public static function performMultipleCurlCalls($urls, $options = array()){
		$ch = array();
		$results = array();
		$mh = curl_multi_init();

		foreach($urls as $key => $val) {
			$ch[$key] = curl_init();
			if ($options[$key]) {
				curl_setopt_array($ch[$key], $options[$key]);
			}
			curl_multi_add_handle($mh, $ch[$key]);
		}

		$running = null;
		do {
			curl_multi_exec($mh, $running);
			//NOTE: turn on the below parameter if CPU usage is shooting up
			//curl_multi_select($mh); 
		}
		while ($running > 0); //running will contain how many curls yet to be executed 

		// Get content and remove handles.
		$i=0;
		foreach ($ch as $key => $val) {
			$curlInfo = curl_getinfo($ch[$key]);
			
			$results[$i]['code'] = $curlInfo['http_code'];
			if($curlInfo['http_code']!= 200){
				$results[$i]['curl'] = $curlInfo;
			}
			curl_multi_remove_handle($mh, $ch[$key]);
			curl_close($ch[$key]);
			$i++; 
		}
 
		curl_multi_close($mh);
		//var_dump($results);
		return $results;
	}

	public static function curlCall($url, $params=null, $headers=null, $method="POST"){

		// create a new cURL resource
		$ch = curl_init($url);

		//set URL and other appropriate options

		if($headers!= null)
		curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);

		if($method == "POST") {
			curl_setopt($ch, CURLOPT_POST, 1);
			if ($params != null)
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}


		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// grab URL and pass it to the browser..
		$result = curl_exec($ch);
		// close cURL resource, and free up system resources
		curl_close($ch);
		return  $result;
		//return $result;
	}


	public static function saveOnS3($savefrom,$saveto,$video_bucket=0){
		global $config;
		try{
			$s3 = new S3($config['amazon_s3']['access_key'],$config['amazon_s3']['secret_key']);
			$bucket_name='';
			if($video_bucket==1) {
				$bucket_name=$config['video_profile']['s3_bucket'];

			}else if($video_bucket==2) {
				//Upload logs to S3
				$env=$config['env']['type'];
				if($env=='dev'||$env=='t1') {
					$bucket_name = $config['amazon_s3']['logs_bucket'];
				}
				else{
					$bucket_name='trulymadlylogslive';
				}

			}
			else
			{
				$bucket_name=$config['amazon_s3']['bucket'];
			}

			if($s3->putObjectFile($savefrom,$bucket_name , $saveto, S3::ACL_PUBLIC_READ))
				return true;
		}catch(Exception $e){
			trigger_error ($e->getMessage());
		}
	}




	public static function putLogs($filename,$type)
{
	global $config;
	$bucket=$config['amazon_s3']['logs_bucket'];
	$myfile=basename($filename);
	$dir=dirname($filename);
	$extension=explode('.',$myfile)[1];
	$myfile=explode('.',$myfile)[0];
	$day=explode('_',$myfile)[2];
	$year=explode('-',$day)[0];
	$month=explode('-',$day)[1];
	$date=explode('-',$day)[2];
	$hour=explode('_',$myfile)[3];
	$minute=explode('_',$myfile)[4];
	$log_type="reporting_log/$type";
	$compressed_file_name=$dir.'/'.$myfile.'.gz';
	$fp = gzopen ($compressed_file_name, 'w9');
	gzwrite ($fp, file_get_contents($filename));
	gzclose($fp);
	$s3_path=$log_type.'/'.$year.'/'.$month.'/'.$date.'/'.$hour.'/'.$minute.'_'.gethostname().'.txt.'.'gz';
	if(Utils::saveOnS3($compressed_file_name,$s3_path,2)) {
		 unlink($filename);

		 unlink($compressed_file_name);

		return true;
	}
	else {
		trigger_error("Cant save logs on s3",E_USER_WARNING);
		return false;
	}
}
	
    public static function deleteFromS3($uri_array){
		global $config;
		try{
			$s3 = new S3($config['amazon_s3']['access_key'],$config['amazon_s3']['secret_key']);
			$delete = array () ;
			foreach ($uri_array as $uri)
			{
				if( isset($uri) && $uri != null )
				{
					$result = $s3->deleteObject($config['amazon_s3']['bucket'],"files/images/profiles/".$uri) ;
				}
	
			}
			return true;
		}catch(Exception $e){
			trigger_error ($e->getMessage());
		}
	}

	public static function deleteFromDisk($uri_array)
	{
	  global $image_basedir;
	  try{
	  	foreach ($uri_array as $uri)
	  	{
	  		if( isset($uri) && $uri != null )
	  		{
	  			$result = unlink($image_basedir.$uri) ;
	  		}
	  
	  	}
	  	return true;
	  }catch(Exception $e){
	  	trigger_error ($e->getMessage());
	  	//trigger_error ($e->getTraceAsString());
	  }	
		
	}
	
	
	
	public static function checkBadWords($data, $badwords){
		$data = trim($data);
		$output=array();
		$arr = explode(' ', $data);
		$isBad = array_intersect($arr, $badwords);
		if($isBad!=null){
			return false;
		}else{
			return true;
		}
	}

	public static function getFromS3($getFrom,$saveTo,$from_logs=0,$bucket_name=''){
		global $config;
		try{
			$s3 = new S3($config['amazon_s3']['access_key'],$config['amazon_s3']['secret_key']);
			$bucket='';
			if($from_logs==0) {
				$bucket=$config['amazon_s3']['bucket'];

			}else{
				$bucket=$bucket_name;
			}
			//
			//var_dump($s3->getObject($bucket, $getFrom, $saveTo));
			if($s3->getObject($bucket, $getFrom, $saveTo))
				return true;

		}catch(Exception $e){
			trigger_error ($e->getMessage());
		}

		return false;
	}

	/**
	 * Will generate all the links to be included in an emailer
	 * @param $campaign e.g. matches
	 * @param $content e.g. view_matches
	 * @param $redirectionFile e.g. matches.php
	 * @param $emailId
	 * @param $userId
	 */
	public static function generateSystemMailerLinks($campaignName, $content, $redirectionFile, $emailId, $userId){

		global $config, $baseurl;
		$analyticsLinks = array();
		$currentTime = time();

		$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'
		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$campaignName";

		$analyticsLinks["analytics_login_link"] = $baseurl.$redirectionFile.$campaign."&utm_content=$content&uid=$userId";
		$analyticsLinks["analytics_header_link"] = $baseurl."/index.php".$campaign."&utm_content=header&uid=$userId";
		$analyticsLinks["analytics_footer_link"] = $baseurl."/index.php".$campaign."&utm_content=footer&uid=$userId";
		//	$analyticsLinks["analytics_open_rate_link"] = "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$userId&t=event&ec=email&ea=open&el=$userId&cs=system_emailer_open&cm=email&cn=$campaignName";
		$analyticsLinks["analytics_open_rate_link"] = "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$userId&t=event&ec=email&ea=open&cs=system_emailer_open&cm=email&cn=$campaignName";
		$analyticsLinks["analytics_open_rate_system_link"] = $baseurl."/trk.php" .$campaign."&utm_content=open_rate&uid=$userId&step=openRate&t=$currentTime";

		$unsubscribe = new Unsubscription($emailId);
		$unsubLink = $unsubscribe->generateLink();
		$analyticsLinks["analytics_unsubscribe_link"]= $unsubLink."&".substr($campaign, 1,strlen($campaign))."&utm_content=unsubscribe&uid=$userId";
			
		$analyticsLinks["analytics_photo_link"] = $baseurl."/photo.php".$campaign."&utm_content=$content&uid=$userId";

		return $analyticsLinks;

	}

	/**
	 * get a shortened url using google api
	 * @param unknown_type $url - url to shorten
	 */
	public static function getShortenedUrlFromGoogleAPI($url, $user_id){
		global $freebase_key, $google_api_url;
		$ch = curl_init();
		$apiURL = "$google_api_url?key=$freebase_key";
		//$apiURL .= "&userIp=".$_SERVER['REMOTE_ADDR'] ;

//		trigger_error("PHP url shortner: $apiURL for user_id: $user_id", E_USER_WARNING);
		
		curl_setopt($ch,CURLOPT_URL,$apiURL);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode(array("longUrl"=>$url)));
		curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
	public static function preferenceAge($year,$gender){
		$start = 18;
		$end = 70;
		
		$age = array();
		$age['start'] = $start;
		$age['end'] = $end;
		
		if($gender=='M' || $gender == 'male'){
			if($year>=18 && $year<=24) {
				$start = 18;
				
				if($year>=18 && $year<=19) {
					$end = 19;
				}
				else if($year>=20 && $year<=24) {
					$end = $year;
				}
			}
			else if($year == 25) {
				$start = 19;
				$end = $year;
			}
			else if($year>=26 && $year<=70) {
				$start = (20>($year-8))?20:$year-8; 
				$end = $year;
			}
		}
		else if($gender=='F' || $gender == 'female') {
			if($year>=18 && $year<=19){
				$start = 18; $end = $year+6;
			}else if($year>=20 && $year<=70) {
				$start = $year;
				if($year+8>70){
					$end = 70;
				}else{
					$end = $year+8;
				}
			}
		}
		
		if(isset($start) && isset($end)){
			$age['start'] = $start;
			$age['end'] = $end;
		}else {
			$age['start'] = 18;
			$age['end'] = 70;
		}
		return $age;
	}
	
	public static function highestDegree($degree){
		$degree_arr = array("1"=>1,"2"=>10,"3"=>10,"4"=>2,"5"=>3,"6"=>4,"7"=>10,"8"=>5,"9"=>10,"10"=>6,"11"=>10,"12"=>7,"13"=>3,"14"=>10,
				"15"=>8,"16"=>10,"17"=>9,"18"=>6,"19"=>10,"20"=>21,"21"=>21,"22"=>19,"23"=>19,"24"=>11,"25"=>12,"26"=>13,"27"=>19,"28"=>14,"29"=>19,
				"30"=>15,"31"=>19,"32"=>16,"33"=>12,"34"=>19,"35"=>17,"36"=>19,"37"=>18,"38"=>15,"39"=>19,"40"=>21,"41"=>20,"42"=>21);
		
		if(isset($degree_arr[$degree])) {
			return $degree_arr[$degree];
		}
		return 21;
	}
	
	public static function getDeviceIdFromHeader() {
		
		$header_arr = getallheaders();
		if(isset($header_arr['device_id']))
			return $header_arr['device_id'];
		else 
			return NULL;
	}

	public static function getDoodleForUsers($user_id=null, $match_id=null,$nopics=false){
		global $cdnurl;
		$dir = $cdnurl."/images/doodle/";
		$version = array("1");
		$img = rand (1,21);
		if($img < 10)
			$img = "0".$img;
		if($nopics)
		{
			//If no pics is true then use a different version of doodle, for female who don't have a profile pic
			
			$img=rand(1,3);
			$version=array("0");
			if($img < 10)
				$img = "0".$img;
		}
			
		return $dir. self::getRandomArrayVal($version)."-".$img.".png";
	}

	
	
	private function getRandomArrayVal($input)
	{
		$rand_keys = array_rand($input, 1);
		return $input[$rand_keys];
	}

	public static function getWebpName($name)
	{
		$webp_name  = rtrim($name , ".jpg") . ".webp" ;
		return $webp_name ;
	}
}

?>
