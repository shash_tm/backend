<?php 

include_once dirname ( __FILE__ ) . "/config.php";

try{

	$DBHOST_log = $config["database_log"]["hostname"];
	$DBUSER_log = $config["database_log"]["username"];
	$DBPASSWORD_log = $config["database_log"]["password"];
	$DBNAME_log = $config["database_log"]["database"];

	$DBTYPE = 'mysqlt';
	$conn_log = &ADONewConnection($DBTYPE);
	$conn_log->PConnect($DBHOST_log,$DBUSER_log,$DBPASSWORD_log,$DBNAME_log);

}
catch (Exception $e){
	trigger_error($e->getMessage(),E_USER_WARNING);
	die;
}
?>

