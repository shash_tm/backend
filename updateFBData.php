<?php 
/*
 * @author Rajesh
*/
require_once dirname( __FILE__ ) . "/include/config.php" ;
require_once dirname ( __FILE__ ) . "/include/function.php";
include_once (dirname ( __FILE__ ) . '/fb.php');
include_once (dirname ( __FILE__ ) . '/TrustBuilder.class.php');
include_once (dirname ( __FILE__ ) . '/abstraction/query_wrapper.php');


class updateFBData {
	
	private $user_id;
	private $trustbuilder;
	
	function __construct($user_id=NULL) {
		$this->user_id = $user_id;
		$this->trustbuilder = new TrustBuilder($this->user_id);
	}
	
	function checkFB($token) {
		
		$fb = new fb($token);
		$fb->getBasicData();
		
		if(!$this->trustbuilder->checkfidforSync($fb->data['id'])){
			$response['responseCode'] = 403;
			$response['error'] = 'Please use the same facebook account used to create the profile';
			$this->printOutput($response);
		}else {
			$fb->getData();
			$fb->storeFacebook($this->user_id,"update");
			$dob = $fb->processDOB();
			// user can not update the location from FB
			//$loc = $fb->processLocation();
			$companies = $fb->processCompanies();
			$institutes = $fb->processColleges();
			$age = 0;
			
			if(isset($fb->data['birthday']))
				$age = date_create($fb->data['birthday'])->diff(date_create('today'))->y;
			
			global $conn;
			
			$update = false;
			$last = false;
			
			$sql = "update user_data set";
			if(isset($dob) && $age >= 18 && $age <= 70) {
				$sql = $sql." DateOfBirth = '$dob'";
				$update = true;
			}
			if(isset($loc)) {
				$country = $loc['country']; $state = $loc['state']; $city = $loc['city'];
				if($update) {
					$sql = $sql.",";
				}
				$sql = $sql." stay_country = $country, stay_state = $state, stay_city = $city";
				$update = true;
			}
			
			if(isset($companies)) {
				if($update) {
					$sql = $sql.",";
				}
				$sql = $sql." company_name = '$companies'";
				$update = true;
			}
			
			if(isset($institutes)) {
				if($update) {
					$sql = $sql.",";
				}
				$sql = $sql." institute_details = '$institutes'";
				$update = true;
			}
			
			if($update) {
				$sql = $sql." where user_id = $this->user_id";
				
				Query_Wrapper::UPDATE($sql,NULL, "user_data");
				
				//$conn->Execute($sql);
			}
			
			//var_dump($sql);exit;
			$this->trustbuilder->updateFacebookTrustScore($fb->data['connections']);
			
			$response['responseCode'] = 200;
			$this->printOutput($response);
		}
	}
	
	function printOutput($response) {
		print_r(json_encode($response));
		die;
	}

}

try {
	$data = $_REQUEST;
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user['user_id'];
	$token = '';
	$updateFB = NULL;
	if(isset($user_id)) {
		$updateFB = new updateFBData($user_id);
		if(isset($data['token'])) {
			$token = $data['token'];
			try {
				$updateFB->checkFB($token);
			}catch(Exception $e) {
				trigger_error("PHP Web:".$e->getMessage(), E_USER_WARNING);
				trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
				
				$response['responseCode'] = 403;
				$response['error'] = 'Your facebook session is timed out. Please login again.';
				$updateFB->printOutput($response);
			}
		}else {
			$response['responseCode'] = 403;
			$response['error'] = 'Invalid Request';
			$updateFB->printOutput($response);
		}
	}else {
		$updateFB = new updateFBData();
		$response['responseCode'] = 401;
		$updateFB->printOutput($response);
	}
} 
catch (Exception $e) {
	trigger_error("PHP Web:".$e->getMessage(), E_USER_WARNING);
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}
?>