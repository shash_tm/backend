<?php 
 require_once dirname ( __FILE__ ) . '/include/config.php';
try 
{
	    header('Cache-Control: max-age=172800, public');
	    header('Pragma: ');
	   
		$smarty->display (dirname ( __FILE__ ) . "/faq_files/index.html" );
}
catch (Exception $e) 
{
	trigger_error("FAQs:".$e->getTraceAsString(), E_USER_WARNING);
}
?>