<?php

require_once dirname(__FILE__).'/include/config.php';
require_once (dirname ( __FILE__ ) . '/include/function.php');

/**
 * File includes the call to google freebase api to fetch the data
 * relevant fields form a query string
 *
 */



/*function saveFreebaseIds($fid_data) {
 global $conn;
 $c=1;
 $sql_array=array();
 $sql_base="insert ignore into freebase_ids(fid,name) values";
 $sql_main=$sql_base;

 $val_fetched=false;
 foreach($fid_data as $fid=>$data){
 $val_fetched=true;
 if($c%1000==0){
 $sql_main=substr($sql_main,0,-1);
 $conn->Execute($conn->Prepare($sql_main),$sql_array);
 $sql_main=$sql_base;
 $sql_array=array();
 }
 $sql_main.="(?,?),";
 $sql_array[]=$fid;
 $sql_array[]=$data;
 $c++;
 }

 if($val_fetched==true){
 $sql_main=substr($sql_main,0,-1);
 $conn->Execute($conn->Prepare($sql_main),$sql_array);
 }
 }*/


//perform a curl call to fetch the freebase data
function performCurl($url){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$json_data = json_decode(curl_exec($ch), true);
// 	if(!curl_errno($ch))
// 	{
// 		$info = curl_getinfo($ch);
// 		echo 'Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'];
// 	}
	curl_close($ch);
	return $json_data;
}

try{
	global $freebase_key;
	$toSearch=strip_tags($_GET['value']);
	$category=$_GET['category'];
	switch($category) {
		case "music":
			$filter="(any+type:/music/+type:/media_common/+type:/people/+type:/award/+type:broadcast/+type:/event/+type:/opera/+type:/celebrities/+type:/interests/+type:/radio/)";
			break;
		case "books":
			$filter="(any+type:/media_common/+type:/book/+type:/people/+type:/education/+type:/periodicals/+type:/library/+type:/cosmic_books/+type:/language/+type:/celebrities/+type:/interests/)";
			break;
		case "food":
			//$filter="(any+type:/book/+type:/people/+type:/tv/+type:/food/+type:/travel/+type:/celebrities/+type:/interests/)";
			$filter="(any+type:/food/)";
			break;
		case "movies":
			$filter="(any+type:/media_common/+type:/people/+type:/tv/+type:/film/+type:/fictional_universe/+type:/visual_art/+type:/broadcast/+type:/theater/+type:/event/+type:/celebrities/+type:/interests/)";
			break;
		case "sports":
			$filter="(any+type:/people/+type:/sports/+type:/award/+type:/soccer/+type:/cvg/+type:/olympics/+type:/american_football/+type:/baseball/+type:/event/+type:/basketball/+type:/cricket/+type:/ice_hockey/+type:/games/+type:/boxing/+type:/tennis/+type:/celebrities/+type:/interests/+type:/martial_arts/+type:/skiing/+type:/bicycles/)";
			break;
		case "travel":
			$filter="(any+type:/people/+type:/location/+type:/geography/+type:/transportation/+type:/protected_sites/+type:/travel/+type:/boats/+type:/aviation/+type:/event/+type:/exhibitions/+type:/rail/+type:/zoos/+type:/amusement_parks/+type:/spaceflight/+type:/interests/+type:/conferences/+type:/food/)";
			break;
		default:
	}

	$j=0;
	//$toSearch=urlencode($toSearch);

	//$url="https://www.googleapis.com/freebase/v1/search?query=".$toSearch."&indent=true&filter=".$filter;
	//echo $url;
	//$response=(array)json_decode(file_get_contents($url));

	$service_url = 'https://www.googleapis.com/freebase/v1/search';
	$params = array(
   				 'query' => $toSearch,
  			     'key' => $freebase_key,
	);
	
	$url = $service_url . '?' . http_build_query($params) ."&indent=true&prefixed=true&filter=".$filter;
	$response = performCurl($url);


	$results = array();
	$j=0;
	/**
	 * to return the free text in case no search matches
	 */
	
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	
	if($login_mobile) {
		$results[$j]['text']=$toSearch;
		$results[$j]['id']="ugc:". $toSearch;
	}else {
		$results[$j]['text']=$toSearch;
		$results[$j]['id']="ugc:". $toSearch;
	}
	$results[$j]['notable_name']="";
	$j++;

	/**
	 * google api data parser
	 */
	if($response['status'] == "200 OK"){
		foreach($response['result'] as $result) {
			if($result['name'] != null){
				$results[$j]['text']=$result['name'];
				$results[$j]['id']="freebase_id:".$result['mid'];
				$results[$j]['notable_name']=($result['notable']['name']!=null)?$result['notable']['name']:"";
				//$responses[$j]['id']=$j;
				$j++;
			}
		}
	}
	
	if(isset($response['error']))
		trigger_error("PHP Web: Freebase API fail to respond ". json_encode($response['error']),E_USER_WARNING);
	
	//var_dump($results); die;
	print_r(json_encode($results));
	//var_dump($response); die;
	/*	$responses[$j]['text']=urldecode($toSearch);
	$responses[$j]['id']="ugc:".$responses[$j]['text'];
	$responses[$j]['notable_name']="";

	$j++;
	foreach($response['result'] as $result) {
	$fid_data[$result->mid]=$result->name;
	$responses[$j]['text']=$result->name;
	$responses[$j]['id']="freebase_id:".$result->mid;
	$responses[$j]['notable_name']=$result->notable->name;
	//$responses[$j]['id']=$j;
	$j++;
	}
	//saveFreebaseIds($fid_data);
	print_r(json_encode($responses));*/
	
	
	
	
}catch(Exception $e){
	trigger_error("PHP Web:" . $e->getTraceAsString(),E_USER_WARNING);
}
?>
