<?php 
require_once dirname ( __FILE__ ) . "/include/config_admin.php";

global $conn_reporting,$conn_master;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	$isUpdate = false;
	$options = getopt("u:");
	if(isset($options["u"]) && $options["u"]=='yes') {
		$isUpdate = true;
	}
	
	$degree_arr = array("1"=>1,"2"=>10,"3"=>10,"4"=>2,"5"=>3,"6"=>4,"7"=>10,"8"=>5,"9"=>10,"10"=>6,"11"=>10,"12"=>7,"13"=>3,"14"=>10,
			"15"=>8,"16"=>10,"17"=>9,"18"=>6,"19"=>10,"20"=>21,"21"=>21,"22"=>19,"23"=>19,"24"=>11,"25"=>12,"26"=>13,"27"=>19,"28"=>14,"29"=>19,
			"30"=>15,"31"=>19,"32"=>16,"33"=>12,"34"=>19,"35"=>17,"36"=>19,"37"=>18,"38"=>15,"39"=>19,"40"=>21,"41"=>20,"42"=>21);
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = "select user_id,highest_degree from user_data";
	
	$rs = $conn_reporting->Execute($sql);
	if ($rs){
		while ($arr = $rs->FetchRow()) {
			$old_degree = $arr["highest_degree"];
			echo "old degree = ".$old_degree."\n";
			$new_degree = $degree_arr[$old_degree];
			echo "new degree = ".$new_degree."\n";
			if($isUpdate) {
				$conn_master->Execute($conn_master->prepare("update user_data set highest_degree=? where user_id=?"),
						array($new_degree,$arr["user_id"]));
			 	echo "updated \n";
	 		}
		}
	}
		
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}

?>
