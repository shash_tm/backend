<?php 
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname(__FILE__)."/UserFlags.class.php";

try {
	$data = $_REQUEST;
	$user = functionClass::getUserDetailsFromSession();
	if(isset($user['user_id'])) {
			$user_flag = new userFlags($user['user_id'], $_GET['last_seen_scene']);
		$user_flag->getUserDataForProfileCompleteness();
	}else {
		echo json_encode(array("responseCode"=>401));
	}
} 
catch (Exception $e) {
	echo json_encode(array("responseCode"=>403,"error"=>"Something went wrong"));
	trigger_error("PHP WEB: Error: " . $e->getMessage() . "  trace: " .$e->getTraceAsString(), E_USER_WARNING);
}

?>
