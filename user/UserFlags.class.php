<?php 
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/datesIcon.php";
require_once dirname ( __FILE__ ) . "/../DBO/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/curatedDatesDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/sparkDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/selectDBO.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/eventCategoryClass.php";
require_once dirname ( __FILE__ ) . "/../select/Select.class.php";
require_once dirname ( __FILE__ ) . "/../RecommendationEngines/Utils/RecommendationUtils.php";

define('SELECT_NUDGE_FREQUENCY',7); // In sessions
define('SPARK_REPEAT_ALTERNATE_NUDGE_FREQUENCY',3);  // In sessions

class userFlags {
	
	private $uuDBO;
	private $user_id;
	private $gender = '';
	private $ud;
	private $uu;
	private $uf;
	private $last_seen_scene;
	private $user_location;
	private $ncr_city_array;
	private $functionClass;
	private $app_version_code;
	private $source;
	private $country;
        private $sessionId;

	static $trustMale = 50;
	static $trustFemale = 30;
	
	static $trustMaleMessage = "A high trust score goes a long way with the ladies!";
	static $trustFemaleMessage = "Matches find a high trust score very attractive, build it now!";
	
	static $profileIncompleteMaleMessage = "Complete your profile, you could be missing out on a lot of great matches!";
	static $profileIncompleteAndTrustMaleMessage = "A completed profile with a high trust score is the way to get better responses!";
	static $profileIncompleteFemaleMessage = "Complete your profile for a better boy browsing experience!";
	
	static $likeCount = 6;
	static $hideCount = 20;
	
	function __construct($user_id, $user_last_seen_scene = null) {
		$this->functionClass =   new functionClass();
		$this->user_id = $user_id;
		$this->last_seen_scene = $user_last_seen_scene;
		$this->uuDBO = new userUtilsDBO();
		$this->ud = new UserData($this->user_id);
		$this->uu = new UserUtils();
		$this->uf = new UserFlagsDBO();
		$this->user_location = curatedDatesDBO::getUserLocation($user_id);
		$this->ncr_city_array = array(16743,47965,47964,47968,47967,47966);
		$this->app_version_code = $this->functionClass->getHeader();
		$this->source = $this->functionClass->getSourceFromHeader();
                $userData = $this->uuDBO->getUserDataForProfileComplete($this->user_id);
		$this->country = $userData['stay_country'];
                $this->sessionId = RecommendationUtils::getUserCurrentSession($this->user_id);
	}
	
	function getUserDataForProfileCompleteness() {
		global $redis;
		$response = array();
		
		$messages = array();
		$this->gender = $this->ud->fetchGender();
		$response['responseCode'] = 200;
		
		$response['isProfileComplete'] = $this->checkIsProfileIncomplete($userData);
		$response['isTrustVerified'] = $this->checkIsProfileTrustScoreVerified($userData['trust_score']);
		
		$response['trustScore'] = isset($userData['trust_score'])?intval($userData['trust_score']):0;
		
		$messages['trustIncompleteMessage'] = ($this->gender == 'M')?self::$trustMaleMessage:self::$trustFemaleMessage;
		$messages['profileIncompleteMessage'] = ($this->gender == 'M')?self::$profileIncompleteMaleMessage:self::$profileIncompleteFemaleMessage;
		if($this->gender == 'M') {
			if(!$response['isProfileComplete'] && !$response['isTrustVerified']) {
				$response['isTrustVerified'] = true;
				$messages['profileIncompleteMessage'] = self::$profileIncompleteAndTrustMaleMessage;
			}
		}
		$response['messages'] = $messages;
		
		$counter = array();
		$counter['likeCount'] = ($this->gender == 'F')?self::$likeCount:-1;
		$counter['hideCount'] = ($this->gender == 'F')?self::$hideCount:-1;
		$response['counter'] = $counter;
		
		if($this->gender == 'M') {
			$activity_data = $this->uu->getActivityDashboard(array($this->user_id));
			$activityData = array();
			foreach ($activity_data as $key=>$val){
				$popularity = $val['bucket'];
				if($popularity>=1 && $popularity <4){
					$activityData['intro_text'] = "Check back later for more matches.";
					$activityData['popularity_text'] = "Update pictures to see your popularity soar!";
					$activityData['popularity'] = $popularity;
					$activityData['activity'] = $val['visibility'] + $this->uu->getTodaysActivity($key);
					$activityData['activity_text'] = "Views in last 15 days";
				}
			}
			if(count($activityData)>0) 
				$response['activityData'] = $activityData;
		}
		
		$response['chat_configurations']=$this->getChatConfigurations();
		$response['native_ad_flags']= $this->getNativeAdFlags();
		$response['meetups_enabled']= $this->getMeetupsEnabledFlag($this->user_id);
	//	$response['meetups_video']= $this->getMeetupsVideoFlag();
		$response['curated_deal_icon']= $this->getCuaratedDealIcon();
		//var_dump($this->getMatchesAdsFlagsStatus());

		$response['update_version_flags']= $this->getUpdateVersionFlags();
		$response['new_event_added']= $this->getNewEventsFlag();
		$response['scenes_ab']= $this->getScenesABFlag();
		$response['dateshop_flags']= $this->getDateshopFlags();

		$limit_likes=  $this->getLimitLikesFlags(); ;
		if(count($limit_likes) >0)
			$response['limit_likes']= $limit_likes;

		$photo_share_flags = $this->getPhotoShareFlag();
		$response['app_virality_flag'] = $this->getAppViralityFlag();
		$response['sparks_active'] = $this->getSparkActiveFlag();
		$response['why_not_sparks'] = $this->getWhyNotSparkInstead();
		$response['sparkCountersLeft'] = sparkDBO::getUserActiveSparkCount($this->user_id);
		$response['select_flags'] = $this->getSelectFlags(false);
		$response['select_active'] = $this->getSelectFlags(true);
		if($redis->sIsMember('curated_deal_users',$this->user_id)  ||  $this->getMatchesAdsFlagsStatus()== 'true')
			$response['matches_ads_flags']= $this->getMatchesAdsFlags();
		$email = $this->ud->fetchEmailId();
		if(isset($email) && $email != null)
			$response['user_email'] = $this->ud->fetchEmailId();
		if($redis->sIsMember('chat_configure_users',$this->user_id) == true)
			$photo_share_test = true;


		foreach($photo_share_flags as $key => $val)
		{
			if($photo_share_test == true || $val=='true')
				$response[$key] = true;
			else
				$response[$key] = false;
		}
		// Adding shouldInactivityTimeBeShown to response
        	$shouldInactivityTimeBeShown = $this->uf->read("shouldInactivityTimeBeShown");
        	$json = json_decode(sizeof($shouldInactivityTimeBeShown)>0?$shouldInactivityTimeBeShown[0]:'{"M":true,"F":true}',true);
        	$response["shouldInactivityTimeBeShown"] = isset($json[$this->gender])?$json[$this->gender]:true;
		$response['matches_cache_optimisations']= $this->getMatchesCacheOptimisationsFlags();
		$response['all_mutual_friends'] = $this->getAllMutualFriendsFlag();
		$response['payments'] = $this->getPaymentsFlags(); 
		$response['favourites'] = $this->getFavouritesFlags();
		print_r(json_encode($response));
	}
	
	private function checkIsProfileIncomplete($user_data) {
		$user_data['is_profile'] = isset($user_data['is_profile'])?$user_data['is_profile']:0;
		$user_data['total_approved_photo'] = isset($user_data['total_approved_photo'])?$user_data['total_approved_photo']:0;
		$user_data['music_favorites'] = isset($user_data['music_favorites'])?json_decode($user_data['music_favorites'],true):array();
		$user_data['movies_favorites'] = isset($user_data['movies_favorites'])?json_decode($user_data['movies_favorites'],true):array();
		$user_data['books_favorites'] = isset($user_data['books_favorites'])?json_decode($user_data['books_favorites'],true):array();
		$user_data['travel_favorites'] = isset($user_data['travel_favorites'])?json_decode($user_data['travel_favorites'],true):array();
		$user_data['other_favorites'] = isset($user_data['other_favorites'])?json_decode($user_data['other_favorites'],true):array();
		$user_data['institute_details'] = isset($user_data['institute_details'])?json_decode($user_data['institute_details'],true):array();
		$user_data['company_name'] = isset($user_data['company_name'])?json_decode($user_data['company_name'],true):array();

		$return = false;
		if($this->gender == 'M') {
			if(count($user_data['music_favorites'])>0 && count($user_data['movies_favorites'])>0 
				&& count($user_data['books_favorites'])>0 && count($user_data['travel_favorites'])>0
				&& count($user_data['other_favorites'])>0 && count($user_data['institute_details'])>0) {
					$return = true;
				if($user_data['age']>25){
					if(count($user_data['company_name'])>0 && isset($user_data['income_start']) && isset($user_data['income_end'])) {
						$return = true;
					}else {
						$return = false;
						return $return;
					}
				}
				if($user_data['total_approved_photo']>=1 && $user_data['is_profile']>0) {
					$return = true;
				}else {
					$return = false;
					return $return;
				}
			}
		}
		else {
			if((count($user_data['music_favorites'])>0 || count($user_data['movies_favorites'])>0
				|| count($user_data['books_favorites'])>0 || count($user_data['travel_favorites'])>0
				|| count($user_data['other_favorites'])>0) && count($user_data['institute_details'])>0) {
					$return = true;
				if($user_data['age']>25){
					if(count($user_data['company_name'])>0) {
						$return = true;
					}else {
						$return = false;
						return $return;
					}
				}
			}
		}
		return $return;
	}
	
	private function checkIsProfileTrustScoreVerified($user_trust_score) {
		if($this->gender == 'M') {
			if($user_trust_score >= self :: $trustMale) {
				return true;
			}
			return false;
		}else {
			if($user_trust_score >= self :: $trustFemale) {
				return true;
			}
			return false;
		}
	}
	
	public function getChatConfigurations(){
                global $redis;

                //$data=$redis->hGetAll("chat_configurations");
                $data=$this->uf->read("chat_configurations");
                //$data_type=$redis->hGetAll("chat_configurations_ab");
                $data_type=$this->uf->read("chat_configurations_ab");
                //$users = $redis->sMembers("chat_configure_users");
                $users=$this->uf->read("chat_configure_users");
                //$prefixes=$redis->hGetAll("chat_configurations_prefix");
                $prefixes=$this->uf->read("chat_configurations_prefix");
                $final_prefix="";
                $final_data=array();
                foreach($data as $key=>$value){
                        $ab_range=$data_type[$key];
                        if(in_array($this->user_id, $users) || ($this->user_id)%10<$ab_range){
                                $final_data[$key]=$value;
                                $final_prefix.=$prefixes[$key]."_";
                        }
                }
                if(count($final_data)>0){
                        $final_data['ab_prefix']=$final_prefix;//$data_type['ab_prefix'];
                }
                return $final_data;
        }
	
	public function getNativeAdFlags() 
	{
	  $flags=$this->uf->read("native_ads_flags");
	  return $flags;
	  /* global  $redis ;	
	   $flags = $redis->hGetAll('native_ads_flags');
	   return $flags;*/
	}
	
	public function getCuaratedDealIcon() 
	{
		$iconObj = new DealIcon($this->user_id) ;
		$curatedIcon= array() ;
		$curatedIcon['icon_shown'] = $iconObj->is_icon_shown();
		return $curatedIcon;
	}

	public function getMeetupsEnabledFlag($user_id)
	{
		global $redis;
		if($redis->sIsMember('curated_deal_users',$this->user_id))
			return true;

		$flags=$this->uf->read("meetups_flag");
		$global_flag =  $flags['meetups_flag'];
		if($global_flag == 'false' || !$global_flag)
			return $global_flag;
		else{
			return $this->uf->hasActiveEvent($this->user_location);
		}
	}

	private function getPhotoShareFlag()
	{
		$flags=$this->uf->read("photo_share_flags");
		return $flags;
	}


	public function getAppViralityFlag()
	{
		$flags = $this->uf->read("app_virality_flag");
		$local_flag = $flags['app_virality_flag'];
		
		# Query used :
        	# select user_id from user_search where gender = 'M' and bucket = 5 and (user_id%200 < 1) and user_id not in (1032546,1034621,1061247,1112486,1112539,1126423,1184126,1254014,1254109,1300764,1314974,1315045,1315070,1315099,1449741,1454935,1474173,1491815,1534578,1545360,1570736,1580688);
		$referralMales = array(17600,24800,41400,43000,53000,125000,178000,202200,245200,257800,290200,374000,383600,411000,432200,448200,449200,459000,466600,484200,500800,509000,549400,558000,603200,620200,662200,667200,676000,737600,750600,751200,776800,782400,806200,898800,912000,967600,976000,980000,981600,994600,1000600,1048400,1079600,1102600,1127600,1175000,1209200,1256200,1263400,1266800,1268200,1278200,1289000,1318800,1323600,1345200,1359600,1376000,1376600,1383200,1383600,1390200,1399600,1401800,1403600,1404400,1416600,1421000,1422000,1447000,1456800,1457400,1460800,1470600,1489400,1491000,1494600,1500600,1514200,1517000,1519600,1520200,1535800,1540000,1543600,1543800,1549000,1550600,1552400,1552600,1556400,1561800,1569600,1570400,1571600,1572000,1573000,1574200,1577400);

		if ($local_flag == 'true') {
			if($this->gender == 'M' && in_array($this->user_id,$referralMales)) {
                        	return true;
			}
			else if($this->gender == 'F') {
				return true;
			}
                }
		return false;
	}

	
	public function getMatchesCacheOptimisationsFlags() {
		$flags = $this->uf->read("matches_cache_optimisations");
		$time = null;
		if ($this->gender == 'M' && isset($flags['appCacheExpiryTimeMale'])) 
			$time = $flags['appCacheExpiryTimeMale'];
		if ($this->gender == 'F' && isset($flags['appCacheExpiryTimeFemale'])) 
			$time = $flags['appCacheExpiryTimeFemale'];
		unset($flags['appCacheExpiryTimeMale']);
		unset($flags['appCacheExpiryTimeFemale']);
		if ($time != null)
			$flags['appCacheExpiryTime'] = $time;

		if($this->source == "iOSApp")
			$flags['appCacheExpiryTime'] = 600000;

		return $flags;
	}
	

	private function getMeetupsVideoFlag()
	{
		$flags = $this->uf->read("meetups_video");
		return $flags;
	}
	private function getMatchesAdsFlags()
	{
		global $redis ;
		if($redis->sIsMember('matches_ads_test_users',$this->user_id))
			$flags = $this->uf->read("matches_ads_flags_test");
		else if($this->gender == 'M'){
			$flags = $this->source == 'androidApp' ? $this->uf->read("matches_ads_flags_male_android"):$this->uf->read("matches_ads_flags_male_ios");
		}
		else{
			$flags =  $this->source == 'androidApp' ? $this->uf->read("matches_ads_flags_female_android") : $this->uf->read("matches_ads_flags_female_ios");
		}

		// disable is_mediation_enabled for version_code less than 615 on android for Avin
		if($this->app_version_code < 615 && $this->source == 'androidApp')
			$flags['is_mediation_enabled'] = false ;
		return $flags;
	}
	private function getMatchesAdsFlagsStatus()
	{
		$select_member = SelectDBO::getCurrentUserSelectData($this->user_id);
		if($this->country != 113)
			$key =  "matches_ads_flags_status_".$this->country;
		else if($select_member['days_left'] >= 0 && ($select_member['status'] == 'active' ||  $select_member['status'] == 'trial'))
			$key =  "matches_ads_flags_status_select";
		else
			$key = "matches_ads_flags_status" ;

		$flags = $this->uf->read($key);
		return $flags['is_enabled'];
	}

	private function getUpdateVersionFlags()
	{
		if($this->gender == 'M')
			$flags = $this->uf->read("update_version_flags_male");
		else
			$flags = $this->uf->read("update_version_flags_female");
		return $flags;
	}
	
	private function getAllMutualFriendsFlag() {
		global $redis;	
		if($redis->EXISTS('all_mutual_friends_test_users') && $redis->sIsMember('all_mutual_friends_test_users', $this->user_id))
			return true;
		
		$flags = $this->uf->read("all_mutual_friends");
		$global_flag = $flags['all_mutual_friends'];
		if ($global_flag != null && $global_flag != "" && $global_flag == 'true') {
			return true;
		}
		return false;
	}

	private function getNewEventsFlag(){
		$city_id = $this->user_location['city_id'];
		if(in_array ($city_id, $this->ncr_city_array))
			$city_id=2168;

		$lastEventAddedFlags =  $this->uf->read("last_activated_event_date");
		$lastEventAdded = $lastEventAddedFlags[$city_id];
		if(!isset($this->last_seen_scene) || !isset($lastEventAdded) || $lastEventAdded == null)
			return false;
		$diff = strtotime($lastEventAdded) - strtotime($this->last_seen_scene);
		if($diff > 0)
			return true;
		else
			return false;
	}

	private function getScenesABFlag(){
	//	$scenes_ab = $this->user_id % 2 == 0 ? true : false;
		$scenes_ab = true;
		$tracking_key = "scenes_ab";
		$eventObj = new EventCategory($this->user_id);
		$categories = $eventObj->showCategories();
		$category_ab = array();
		foreach($categories as $category){
			$category_ab[] = $category['category_id'];
		}
		if(count($categories) == 0)
			$category_ab[0]=-1;
		return array(
			"scenes_ab" => $scenes_ab,
			"scenes_ab_category"=>implode(",",$category_ab),
			"scenes_ab_tracking_key"=>$tracking_key
		);
	}
	private function getDateshopFlags()
	{
		$flags = $this->uf->read("dateshop_flags");
		return $flags;
	}

	private function getLimitLikesFlags()
	{
		if($this->country == 254)
		{
			if($this->gender == 'M')
				$key = "limit_likes_us_male";
			else
				$key = "limit_likes_us_female";
		}
		else
		{
			$key = "limit_likes";
		}

		$flags = $this->uf->read($key);
		return $flags;
	}

	private function getSparkActiveFlag(){
		/*global $redis;
		if($redis->sIsMember('spark_test_users',$this->user_id))
			return true;*/

		$flags=$this->uf->read("spark_flags");
		if($flags["sparks_active"] == 'true')
			return true;
		else
			return false;
	}

	private function getPaymentsFlags()
	{
		$flags = $this->uf->read("payment_flags");

		if($this->app_version_code >= 570) {
			$flags['paytm'] = false;
		}

		return $flags;
	}

	private function getFavouritesFlags()
	{
		$flags = $this->uf->read("favourites");
		return $flags;
	}

	public function getSelectFlags($globalOnly)
	{
		$flags = $this->uf->read("select_config");
		if($globalOnly){
			return $this->getSelectGlobalFlag($flags, $this->source);
		}

		$selectObject = new Select($this->user_id);
		$my_select = $selectObject->getMySelectData();
		$output['my_select'] = $my_select;

		$output['select_nudge'] = array(
			"enabled" => $this->getBooleanTrue($flags['select_nudge_enabled']) && (($this->sessionId-1)%SELECT_NUDGE_FREQUENCY==1),
			"frequency" => (int)$flags['select_nudge_frequency_in_hours'],
			"content" => $flags['select_nudge_content']
		);
		$output['action_nudge'] = array(
			"allow_like" => $this->getBooleanTrue($flags['allow_like']),
			"allow_spark" => $this->getBooleanTrue($flags['allow_spark']),
			"allow_likeback" => $this->getBooleanTrue($flags['allow_likeback'])
		);
		return $output;
	}

	private function getBooleanTrue($string){
		if(!isset($string) || $string == "false" || $string == false)
			return false;
		else
			return true;
	}

	private function getWhyNotSparkInstead(){
		$flags = array();
		if($this->gender == 'M') {
			$flags = $this->uf->read("why_not_sparks_male");
			$contentTexts = $this->uf->read("why_not_sparks_text_male");
		}
		else {
			$flags = $this->uf->read("why_not_sparks_female");
			$contentTexts = $this->uf->read("why_not_sparks_text_female");
		}
		$rand_keys = array_rand($contentTexts);
                $flags['enabled'] = (($this->sessionId-1)%SPARK_REPEAT_ALTERNATE_NUDGE_FREQUENCY == 0) ? $flags['enabled'] : "false";
		$flags['content'] = $contentTexts[$rand_keys];
		return $flags;
	}

	private function getSelectEnabledCities($source){
		$city_key = $source == "androidApp"? "select_cities_android" : "select_cities_ios";
		$flags = $this->uf->read($city_key);
		return $flags;
	}

	private function getSelectEnabledCountries(){
		$country_key = "select_countries_android" ;
		$flags = $this->uf->read($country_key);
		return $flags;
	}

	private function getSelectGlobalFlag($flags, $source){
		$testUsers = $this->uf->read("select_test_users");
		$selectErrorUserIds = array(65775,113826,118327,133922,166838,351422,431264,480746,709814,854917,1364853,1377503,1445090,1537166,1725487,1726943,1740054,1756723,1760714,1769160,1771788,1780810,1782486,7399,21189,45762,55292,60673,85002,88642,181910,211735,230946,294813,327435,341801,358736,370005,456121,506403,526887,534298,544882,558909,584165,593048,602101,659294,690375,724405,748305,771756,801902,814505,823026,828336,881533,899112,911726,921319,976048,1029613,1032576,1039556,1096514,1106986,1133085,1177884,1234070,1246829,1284835,1289844,1296433,1311976,1331980,1343711,1421438,1421615,1444777,1444905,1474372,1487189,1525891,1526972,1534851,1542222,1549790,1554037,1605983,1614589,1631093,1633612,1636329,1643789,1647178,1648199,1660626,1663392,1673916,1685377,1686988,1694929,1699415,1714832,1715264,1722537,1725723,1726528,1728095,1730664,1735009,1738316,1739925,1740606,1744409,1744514,1746880,1747424,1754191,1757244,1757410,1759642,1760545,1762755,1766511,1766714,1767735,1767907,1770549,1770672,1771014,1771146,1772870,1773505,1774881,1774978,1775190,1777280,1777827,1777843,1778118,1778231,1778612,1778728,1779122,1779280,1779985,1780838,1781234,1781481,1781880,1782342,1782414,1782472,1782490,1782625,1783022,1783176,1783205,1783306);
		if(in_array($this->user_id, $testUsers))
			return true;
		$select_cities = $this->getSelectEnabledCities($source);
		//$select_countries = $this->getSelectEnabledCountries();
		$user_city_id = $this->user_location['city_id'];
		$gender = $this->gender;
		$global_key = $source == "androidApp"? "global_enabled_android" : "global_enabled_ios";
		$global = $this->getBooleanTrue($flags[$global_key]);
		$minAge = $gender == "F" ? (int)$flags['female_min_age'] : (int)$flags['male_min_age'];
		$userData = $this->uuDBO->getUserDataForProfileComplete($this->user_id);
		if($global === false)
			return false;
		else if ($this->country != 113) // If country is not India , Then always return false
			return false;
		else{
			if($userData['age'] >= $minAge && (count($select_cities) == 0 || in_array($user_city_id, $select_cities))  )
				return true;
			else if(in_array($this->user_id,$selectErrorUserIds)) {
				return true;
			}
			else
				return false;

		}
	}
}
