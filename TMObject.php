<?php 

require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/userQueue.php";
require_once dirname ( __FILE__ ) . "/include/tbconfig.php";
require_once dirname ( __FILE__ ) . "/include/User.php";
require_once dirname(__FILE__). "/abstraction/redis.php";




class TMObject{

	protected $conn;
	protected $userQ;
	protected $smarty;
	protected $redis;
	protected $imageurl;
	protected $dummy_male_image;
	protected $dummy_female_image;
	protected $dummy_unapproved_male_image;
	protected $dummy_unapproved_female_image;
	protected $baseurl;

	/**
	 * TODO: catch exceptions if any of them is not working
	 * @param unknown $user_id
	 */
	function __construct(){
		global $conn,$smarty,$redis,$baseurl, $dummy_male_image,$dummy_female_image, $imageurl, $baseurl,$dummy_unapproved_female_image, $dummy_unapproved_male_image ;//, $total_physical_shards;
		$this->conn = $conn;
		$this->smarty=$smarty;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->userQ = new userQueue();
		$this->redis = $redis;
		//$this->redis = new redisClass(); 
		/*$this->redis = new Redis();
		 $this->redis->connect($redis['IP'], $redis['PORT']);*/
		$this->imageurl=$imageurl;
		$this->baseurl=$baseurl;
		$this->dummy_male_image=$dummy_male_image;
		$this->dummy_female_image=$dummy_female_image;
		$this->dummy_unapproved_female_image = $dummy_unapproved_female_image;
		$this->dummy_unapproved_male_image = $dummy_unapproved_male_image;
		//$smarty->assign("baseurl",$config[])
	}

	public function getHeaderValues($user1){
		$output=array();
		/*$like_key="lq:$user1";
		//calculate likes
		$like_count=$this->redis->ZCARD($like_key);*/
		$key= "u:n:$user1";
		$like_count = $this->redis->GET($key);
		$header=array();
		if($like_count!=null)
			$header['like']=$like_count;

		$sql = $this->conn->Prepare("SELECT count(*) as count from user_recommendation where user1 = ?");
		$ex = $this->conn->Execute($sql, array($user1));
		$row = $ex->FetchRow();
			
/*		$match_key = "u:m:$user1";
		$match_count = $this->redis->LLEN($match_key);
*/		//if($match_count<0)$match_count=0;
		$header['matches'] = $row['count'];
		//var_dump($this->conn); die;
		/*$sql = $this->conn->Prepare("select sender_id, count(*) from current_messages where receiver_id = ? group by sender_id");
		$ex = $this->conn->Execute($sql, array($user1));
		$header['messages'] = $ex->RowCount();*/
		$msgKey = "mCnt:$user1";
		$header['messages'] = $this->redis->SCARD($msgKey);
		$sql_user_image = $this->conn->Prepare("SELECT name from user_photo where user_id = ? AND status ='active' AND is_profile ='yes'");
		$ex  = $this->conn->Execute($sql_user_image, array($user1));
		$user_image = $ex->FetchRow();
		$header['user_image'] = $user_image['name'];


	/*	$myprofile = new User();
		$approvedPics = $myprofile->getApprovedPics($user1);
		$isAnyApproved = (count($approvedPics) > 0)?1:0;*/
		
		
		$sql_user = $this->conn->Prepare("SELECT fname, lname, gender, status from user where user_id = ?");
		$ex  = $this->conn->Execute($sql_user, array($user1));
		$username = $ex->FetchRow();
		$header['name'] = $username['fname'] . ' '.  $username['lname'];
		$header['status'] = $username['status'];

		if($header['user_image'] == null){
			$header['user_image'] = ($username['gender']=='M')? $this->dummy_male_image: $this->dummy_female_image;
		}
		else
			$header['user_image'] = $this->imageurl . $header['user_image'];

		/*	if( $isAnyApproved == 0){
				$header['user_image'] = ($username['gender']=='M')? $this->dummy_unapproved_male_image: $this->dummy_unapproved_female_image;
			}*/
			$rs = $this->conn->Execute($this->conn->prepare("Select * from user_current_credits where user_id=?"),array($user1));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow ();
			$header['credits'] = $row['current_credits'];
		}else{
			$header['credits'] = 0;
		}


		$creditMessages = SystemMessages::getCreditsMessage();

		$rs = $this->conn->Execute($this->conn->prepare("Select * from user_credit_logs where user_id = ? ORDER BY tstamp desc"),array($user1));
		if($rs->RowCount() > 0){
			$rows = $rs->getRows();
			foreach ($rows as $key => $row){
				$header['creditlog'][$key]['message'] = $creditMessages[$row['credit_type']];
				if($row['credit_assgined'] == 0){
					$header['creditlog'][$key]['credit_assgined'] = 'Unlimited';
				}else{
					$header['creditlog'][$key]['credit_assgined'] = '+'.$row['credit_assgined'];
				}
				$header['creditlog'][$key]['time'] = Utils::GMTTOIST($row['tstamp']);
			}

		}

		$session = functionClass::getUserDetailsFromSession();
		$header['payment_plan'] = $session['payment_plan'];

		return $header;
		//$this->smarty->assign("header",$header);
	}


}


?>