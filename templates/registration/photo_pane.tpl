{if isset($edit_photo)==false}
<p class="caption">Say Cheese!</p>
<div id="progressbar"></div>
{else}
<p class="caption fs10">&nbsp;</p>
{/if}

<div id="diffframe" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="sndmpopupframe">
<a href="javascript:void(0);" onClick="document.getElementById('diffframe').style.display='none';"><img src="images/register/close.png" class="clsbtn"></a>

<strong>Hesitating to upload a photo? Here's why you should</strong>

<ul>
<li>Your photo is only shown to members who meet your preferences.</li>
<li>Your photo cannot be downloaded or saved by anyone.</li>
<li>Only members with photos can view photos of other members. </li>
</ul>
<a href="javascript:void(0);" onClick="document.getElementById('diffframe').style.display='none';" class="puppopact">Upload Picture</a> <a href="#" id="nothnx" class="gray fs14 fr mr10 mt30">Skip &raquo;</a></div></div>

<div class="clearfix tc">
    <div class="pupactionbtn">
          
<div align="center" class="mrg" id="showactionbtn">     
<label class="fileContainer">
    Add from Computer <i class="icon-desktop"></i>
 
    <form method="POST" id="add_from_computer_form"
		enctype="multipart/form-data" class="fmstyle">
    <input type="file" id="add_from_computer" name="file"/>
    </form>
</label>
<a onclick="proceed();" {if isset($smarty.session.user_id_admin)} style="display: none;"{/if}>Import from Facebook <i class="icon-facebook-sign"></i></a>
</div>

<div align="center" class="mrg" id="hideactionbtn" style="display:none;">
        <p>Add from Computer <i class="icon-desktop"></i></p>
        <p>Import from Facebook <i class="icon-facebook-sign"></i></p>
</div>        
        
        </div>
  
  
    <div class="clear"></div>
	<div class="browser">
    
		<div class="tmphotoalbum">
			<div class="tmpalbl" id="testDiv1"></div>
		</div>
		<div class="phfrmsec" align="center">
        <p class="phupnote">Note: Photo must be in .JPG, .GIF or .PNG format and not larger than 4 MB</p><div class="fr whocpic"><span class="curpoint"  onmouseover="openwhoc()" onmouseout="closewhoc()">Who will see my photos?</span>&nbsp;<span class="graycolr">|</span>&nbsp;<span class="curpoint"  onmouseover="openptips()" onmouseout="closeptips()">Photo Tips</span>
        	<div class="whocpicinfo" id="whosee" style="display:none;">
            <!--<i class="icon-remove fr graycolr" onclick="closewhoc()"></i> -->
			<ul>
			<li><strong>Not the whole world:</strong>
 your photos will not be publicly visible. </li>
			<li><strong>A select few:</strong>
 only the matches hand-picked by TrulyMadly for you.</li>
			</ul>
			</div>
            
            
            <div class="phototips" id="ptips" style="display:none;">
            <!--<i class="icon-remove fr graycolr" onclick="closewhoc()"></i> -->
			<ul>
			<li>&bull; No photos with sunglasses</li>
<li>&bull; No group photos</li>
<li>&bull; No blurred photos</li>
<li>&bull; No obscene photos</li>
<li>&bull; No animated sketches </li>
<li>&bull; No copyrighted photos</li>
			</ul>
			</div>
            
        </div>
      <!--  <div><span>COVER PHOTO</span></div> -->
       <!-- <div class="fr" style="height:45px;">&nbsp;</div> -->
      
		<div class="hide" id="camera_btn">
		<div id='take-snapshot' class="fl talc fs10 mr10"><a id='take-snapshot' class="photoactbtn"><img id='take-snapshot' src="images/register/pwebbtn.png" class="mrg brdr" title="Take Snap" /></a><p>Take Snap</p></div>
		<div id='cancel-snapshot' class="fl talc fs10 mr10"><a id='cancel-snapshot' class="photoactbtn"><img id='cancel-snapshot' src="images/register/pcanbtn.png" class="mrg brdr" title="Cancel" /></a><p>Cancel</p></div>
		</div>	
        
        
			<div class="pcropouter"> <div id="progress" class="progbarup">
<div class="bar" style="width: 0%;"></div>
<!--Photo Upload Actions Messaging Start    -->         
<div id="messages">       
<div id="photoupload" style="display:none;">
<div class="actmsgtxt">
<strong>Photo uploaded successfully!</strong>
<ul>
<li class="pupfeticon">Upload more photos to show your different sides!<br />
Members with multiple photos get <strong>10x more responses</strong>.</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

<div id="nophoto"  style="display:none;">
<div class="actmsgtxt" >
<strong>Lets put a face to the profile! Here's why</strong>
<ul>
<li><i class="icon-thumbs-up"></i> Members with Photos get more Likes and Messages</li>
<li><i class="icon-eye-open"></i> Only Members with Photos can view Photos of other Members </li>
<li><i class="icon-lock"></i> Your Photo is completely safe with us, no download or save allowed</li>
<li><i class="icon-user"></i> Only Members matching your preferences get to see the Photos, no one else!</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

<div id="photosetprofile" style="display:none;">
<div class="actmsgtxt" >
<strong>Profile photo set successfully!</strong>
<ul>
<li class="pupfeticon">Upload more photos to show your different sides!<br />
Members with multiple photos get <strong>10x more responses</strong>.</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

<div id="photoupdelete" style="display:none;">
<div class="actmsgtxt" >
<strong>Photo deleted successfully!</strong><ul>
<li class="pupfeticon">Upload more photos to show your different sides!<br />
Members with multiple photos get <strong>10x more responses</strong>.</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

<div id="photoupcancel" style="display:none;">
<div class="actmsgtxt" >
<strong>Action Cancelled</strong><ul>
<li class="pupfeticon">Upload more photos to show your different sides!<br />
Members with multiple photos get <strong>10x more responses</strong>.</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

<div id="photoupsave" style="display:none;">
<div class="actmsgtxt" >
<strong>Photo changes saved successfully!</strong><ul>
<li class="pupfeticon">Upload more photos to show your different sides!<br />
Members with multiple photos get <strong>10x more responses</strong>.</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

<div id="small_image" style="display:none;">
<div class="actmsgtxt" >
<strong>The photograph you uploaded is too small.</strong>
<ul>
<li class="pupfeticon">Upload more photos to show your different sides!<br />
Members with multiple photos get <strong>10x more responses</strong>.</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

<div id="fbphotoimport" style="display:none;">
<div class="actmsgtxt" >
<strong>Photos imported successfully from Facebook</strong>
<ul>
<li class="pupfeticon">Upload more photos to show your different sides!<br />
Members with multiple photos get <strong>10x more responses</strong>.</li>
</ul>
You can <strong>Add from Computer</strong> or <strong>Import from Facebook</strong> by using the above buttons.
</div>
</div>

</div>
<!--Photo Upload Actions Messaging End    -->    
</div>
<p id="photoStatus"></p><div id="imageDiv" class="coverphotobox clear" align="center">
		    <!--<img id="photo_pane" src="#"   style="max-width:750px;  max-height:400px;"/>-->
            

           
			</div></div>
            <div id="say-cheese-container" class="webcambg"></div>
            <div style="display:none; margin: 0 auto; text-align:center;" id="image_upload_loader" align="center">
												<div class="progbarup">
													<div class="upload_loader_percent"></div>
												</div>
												Uploading....
											</div>
                                            
            
            <!--Action Buttons -->
            
<div class="tmpcaction hide fl mt10" id="right_panel">
     
				<div class="tmbtnsec" align="center">
<div id='save_as_profile' class="fl talc mr10">
<a id='' class="hide photoactbtn"><img id='' src="images/register/pfavbtn.png" class="mrg brdr fl" title="Set as profile photo" align="absmiddle" width="25"/><p class="fl mt3">Set as Profile Photo</p></a></div>
<div id='dummy_primary' class="fl talc fs10 mr10 hide" style="color:#ddd;"><a class="photoactbtn" style="background: #ddd;"><img src="images/register/pfavbtn.png" class="mrg brdr fl" title="Set as profile photo"  align="absmiddle" width="25"/><p class="fl mt3">Set as profile photo</p></a></div>
<div id='save' class="fl talc fs10 mr10"><a id='' class='hide photoactbtn'><img id='' src="images/register/psavebtn.png" class="mrg brdr fl" title="Crop & Save" align="absmiddle" width="25"/><p class="fl mt3">Crop &amp; Save</p></a></div>
<div id='dummy_save' class="fl talc fs10 mr10 hide" style="color:#ddd;"><a class="photoactbtn" style="background: #ddd;"><img src="images/register/psavebtn.png" class="mrg brdr fl" title="Save Photo"  align="absmiddle" width="25"/><p class="fl mt3">Save Photo</p></a></div>

<div id='upload_image' class="fl talc fs10 mr10 hide"><a id='' class='hide photoactbtn'><i class="icon-upload-alt fl fs24"></i><p class="fl mt3">&nbsp; Upload Photo</p></a></div>
<div id='upload_image_fromfacebook' class="fl talc fs10 mr10 hide"><a id='' class='hide photoactbtn'><i class="icon-upload-alt fl fs24"></i><p class="fl mt3">&nbsp; Upload Photo</p></a></div>
<div id='crop' class="fl talc fs10 mr10 hide"><a id='crop' class="photoactbtn"><img id='crop' src="images/register/pcropbtn.png" class="mrg brdr fl" title="Crop Photo" align="absmiddle" width="25" /><p class="fl mt3">Crop Photo</p></a></div> 
<div id='chop' class="fl talc fs10 mr10"><a class="photoactbtn" id='chop'><img src="images/register/pdelbtn.png" name="chop" class="mrg brdr fl" id='chop' title="Delete Photo" align="absmiddle" width="25"/><p class="fl mt3">Delete Photo</p></a></div>
<div id='dummy_delete' class="fl talc fs10 mr10 hide" style="color:#ddd;"><a class="photoactbtn" style="background: #ddd;"><img src="images/register/pdelbtn.png" class="mrg brdr fl" title="Delete Photo" align="absmiddle" width="25"/><p class="fl mt3">Delete Photo</p></a></div>
<div id='new_img_cancel' class="fl talc fs10 mr10 hide"><a id='new_img_cancel' class="hide photoactbtn"><img id='new_img_cancel' src="images/register/pcanbtn.png" class="mrg brdr fl" title="Cancel" align="absmiddle" width="25"/><p class="fl mt3">Cancel</p></a></div>
				</div>
			</div>
            
            <!--Action Buttons End -->
            
                                            
		</div>
	</div>
	
</div>
<input type="hidden" id="x" name="x" />
<input type="hidden" id="y" name="y" />
<input type="hidden" id="w" name="w" />
<input type="hidden" id="h" name="h" />

<div class="clb"></div>

<div class="clearfix">
<!--	<div class="backleft">
		<a onclick="onBack(this)">Back</a>
	</div>
-->
{if isset($edit_photo)==false}
	<div class="continuert">
		<a id="continuebtn" onclick="onContinue(this)">Continue</a>
	</div>
    <div class="diffcontinue" style="display:none;">
		<p>Continue</p>
	</div>
{/if}
</div>
<div id="snapshots"></div>
<iframe height="0"
	width="0" id="fb_submit_iframe" name="fb_submit_iframe"
	style="border: none; height: 0px; width: 0px;"></iframe>

<iframe name="file_upload_frame" id="file_upload_frame" height="0"
			width="0" style="height: 0px; width: 0px; border: none;"> </iframe>
        
            
        
	