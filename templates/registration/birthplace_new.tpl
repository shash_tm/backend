<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Top progress section start -->
    <div class="regprogress">
	<h2>DOWN TO BASICS</h2>
	 <div class="regscalesec" align="center">
           <div class="regprogbar pbarstep1"></div>
   	 </div>
    <!--<div class="regscalesec">
    	<img src="http://dev.trulymadly.com/trulymadly/templates/new/images/regprogress.png">
    	<div class="regprogbar"></div>
    </div>-->
	</div> 
<!-- Top progress section end -->
<!-- Form section start -->
	<div class="regformsec">
	   	<div class="formreg">
        <p>Date of birth</p>
        <ul>
		<li class="dobform fl">
        <div class="regselect-style regselddbg1">
        <select name="dd" class="regselwidth1" id="whenBornDay">
            <option value="" disabled selected>Date</option>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
		    <option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option>
            <option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
		</select>
        </div>
        </li>
        <li class="dobform padl1 fl">
        <div class="regselect-style regselddbg1">
        <select name="mm" class="regselwidth1" id="whenBornMonth">
            <option value="" disabled selected>Month</option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
		</select>
        </div>
        </li>
        <li class="dobform fr">
        <div class="regselect-style regselddbg1">
        <select name="yy" class="regselwidth1" id="whenBornYear">
            <option value="" disabled selected>Year</option>
		</select>
        </div>
        </li>
        </ul>
        <p id='whenBorn' style="color:red" class="fs11"></p>
<!-- <p class="fs16">Country</p>
        <div class="regselect-style">
          <select name="Country" id="stayCountry">
            <option value="" disabled selected>Select your country</option>
            <option value="" disabled>Select country</option>
          </select>
        </div>-->
		<p id='stayCountryError' style="color:red;" class="fs11"></p>
        <p class="fs16">State</p>
        <div class="regselect-style">
          <select name="State" id="stayState">
            <!--<option value="">Select your state</option>-->
          </select>
        </div>
   		<p id='stayStateError' style="color:red;" class="fs11"></p>
        <p class="fs16">City</p>
        <div class="regselect-style">
          <img id="loader" class="regloader hide" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="18px">
          <select name="City" id="stayCity">
            <!--<option value="">Select your city</option>-->
          </select>
        </div>
		<p id='stayCityError' style="color:red;" class="fs11"></p>
        
		<p>Height</p>
        <ul>
		<li class="fl mrg0">
        <div class="regselect-style regselddbg2">
        <select name="feet" id="feet" class="regselwidth2">
            <option value="" disabled selected>Feet</option>
            <option value="4">04</option>
            <option value="5">05</option>
            <option value="6">06</option>
            <option value="7">07</option>
		</select>
        </div>
        </li>
        <li class="fr mrg0">
        <div class="regselect-style regselddbg2">
        <select name="inches" id="inch" class="regselwidth2">
            <option value="" disabled selected>Inches</option>
            <option value="0">00</option>
            <option value="1">01</option>
            <option value="2">02</option>
            <option value="3">03</option>
            <option value="4">04</option>
            <option value="5">05</option>
            <option value="6">06</option>
            <option value="7">07</option>
            <option value="8">08</option>
            <option value="9">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
		</select>
        </div>
        </li>
        </ul>
        <p id='heightError' style="color:red;" class="fs11 mt5"></p>
		<p>Relationship status</p>
       	<ul>
		<li class="fl"><input class="cutomradio" type="radio" name="marital_status" value="Never Married"><label>Never Married</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="marital_status" value="Married Before"><label>Married Before</label></li>
        <!-- <li class="fr"><input class="cutomradio" type="radio" name="marital_status" value="Separated"><label>Separated</label></li>-->
        </ul>
       <!-- <ul>
        <li class="fl"><input class="cutomradio" type="radio" name="marital_status" value="Divorced"><label>Divorced</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="marital_status" value="Widowed"><label>Widowed</label></li>
        </ul> -->
        <p id='marital_Error' style="color:red;" class="fs11 mt5"></p>
     <div id='haveChildrenDiv'>
        <p>With children</p>
       <ul>
		<li class="fl"><input class="cutomradio" type="radio" name="haveChildren" value="yes"><label>Yes</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="haveChildren" value="no"><label>No</label></li>
        </ul>
        <p id='haveChildren_Error' style="color:red;" class="fs11 mt5"></p>
     </div>
		
        </div>
        
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul> 
    </div>
<!-- Form section end -->
<div class="regfrmaction">
<!--<a href="#" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a> -->
<a id="activeBirth" onclick="onContinue(this)" style="display:none;" class="regbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
<a id="inactiveBirth" class="diffregbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>
</div>
<!--Centered Container End -->
</div>
