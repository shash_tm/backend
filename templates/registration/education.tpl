<style>
.chzn-results li { font-size:13px!important;}
</style>


{if !$edit}
<p class="caption">We don't need a CV...just the necessary details!</p>
{/if}

        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="education">Education</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>    
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
                    <p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
                        <p class="formlabel">Education</p>
                        <!--
                        {if !$edit}
                        <div class="designation mt5 eduqua">
                        	<p class="formlabelnew">{$allQ.education}</p>
                            <p><select class="span6 chzn-select" data-placeholder="Highest Education level..." tabindex="1" id="education_0">
                                    <option value=""></option>
                                </select>
                            </p>
<p id='education_0Error' style="color:red" class="fs11"></p>
			<p class="formlabelnew mt10">{$allQ.specialization}<span class="pink fs11"> (optional)</span></p>
                            <p><input  rows="1" style="width:280px" id="specialization_0"/>
                            </p>
<p id='specialization_0Error' style="color:red" class="fs11"></p>
                            <p class="formlabelnew mt10">{$allQ.institute}<span class="pink fs11"> (optional)</span></p>
                            <p><input  rows="1" style="width:280px" id="institute_0"/>	
                            </p>
<p id='institute_0Error' style="color:red" class="fs11"></p>
                        </div>
                        {/if}
                       -->  
                        <p id='moreeducation' class="addmoreed"><a href="javascript:void(0);">Add more</a></p>
                    </div>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle innerright">
                        <p class="formlabel">{$allQ.education_spouse}</p>
                        <span class="pink fs12">(Select Multiple)</span>
                        <div>
                        	<!--<p class="formlabelnew mt10">Education level</p> -->
                            <p><select data-placeholder="Select Education Level" class="chzn-select span6" multiple="multiple" tabindex="6" id='education_spouse'>
                                </select>
                            </p>
<p id='education_spouse_Error' style="color:red" class="fs11"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="backleft"><a onclick="onBack4(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue4(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
                <div class="continuert"><a id="education_save" onclick="onContinue4(1)">Save</a></div>
        </div>
{/if}

