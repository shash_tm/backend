{include file='templates/registration/header_registration.tpl'}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/psycho.css">
<script src="{$cdnurl}/js/register/core_psycho4_female.js"></script>

<div class="container">
   
   
   
   
   <!-- Questionaries Section Start    -->
        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">Questions for Psychoanalysis...</p>
<div id="progressbar"></div>



<!-- My work Start    -->
<div class="questsecframe mt40">

<!-- Questions section start -->


<!-- Question 45 - a  -->
<!--
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">After marriage I am open to living...</div><p class="qnote">Note: You can select multiple options</p>
<div class="ansquest clb mrg" align="center" style="margin-top:10px;">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li class="fl mrg"><label class="checkbox"><input type="checkbox" value="1" id="optionsRadios4" name="answer_45"><span class="listitems aftrmrtxt">In a joint family with in-laws (spouse’s married siblings, parents, uncle, aunts and cousin etc.)</span></label></li>
</ul>
<ul class="formlist mt20 clearfix">
<li class="fl mrg"><label class="checkbox"><input type="checkbox" value="2" id="optionsRadios5" name="answer_45"><span class="listitems aftrmrtxt">In a joint family with just his parents and unmarried siblings.</span></label></li>
</ul>
<ul class="formlist mt20 clearfix">
<li class="fl mrg"><label class="checkbox"><input type="checkbox" value="3" id="optionsRadios5" name="answer_45"><span class="listitems aftrmrtxt">With his parents but in the house that my spouse and I arrange.</span></label></li>
</ul>
<ul class="formlist mt20 clearfix">
<li class="fl mrg"><label class="checkbox"><input type="checkbox" value="4" id="optionsRadios5" name="answer_45"><span class="listitems aftrmrtxt">Alone with my spouse (live independently)</span></label></li>
</ul>

</form>
</div>
</div>
-->
<!-- Question 54 - a  -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />Career-wise, after marriage I will be ... </div>
<div class="ansquest clb mrg" align="center" style="margin-top:10px;">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="14" id="optionsRadios4" name="answer_54"><span class="listitems ">Working</span></label></li>
<li><label class="radio"><input type="radio" value="15" id="optionsRadios5" name="answer_54"><span class="listitems ">Not Working</span></label></li>

<li><label class="radio"><input type="radio" value="13" id="optionsRadios4" name="answer_54"><span class="listitems ">Flexible</span></label></li>

</ul>

</form>
</div>
</div>


<!-- Question 46 -->
<!--
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">Is there a possibility/or a condition that your parents/or sibling will stay with you after marriage?</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="5" id="optionsRadios4" name="answer_46"><span class="listitems ">YES</span></label></li>
<li><label class="radio"><input type="radio" value="6" id="optionsRadios5" name="answer_46"><span class="listitems ">NO</span></label></li>
 </ul>

</form>
</div>
</div>
-->


<!-- Question 47 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />To what extent will you follow your partner�s family traditions and rituals?</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="5" id="optionsRadios4" name="answer_47"><span class="listitems">All of them</span></label></li>
<li><label class="radio"><input type="radio" value="6" id="optionsRadios5" name="answer_47"><span class="listitems">Most of them</span></label></li>
<li><label class="radio"><input type="radio" value="7" id="optionsRadios4" name="answer_47"><span class="listitems">Only the important ones</span></label></li>
<li><label class="radio"><input type="radio" value="8" id="optionsRadios5" name="answer_47"><span class="listitems">None of them</span></label></li>
 </ul>
</form>
</div>
</div>
<div id="tmquestions" class="tmquestions hide">
{include file='templates/registration/reloader.tpl'}
</div>


<!-- Questions section End -->
<!--<button class="hide fl mrg oopsbtn" id="back"><span style="font-size:18px;">&laquo;</span>Back</button> -->

</div>


<!-- My Work End    -->
        
          <div class="clearfix mt30 hide" id='next'>   
        	<div class="continuert mr50"><a >Continue</a></div>
        </div>
       
             </div>   
   <!-- Questionaries Section end    -->
     
     
     
     
     
     </div>
     
     
     
  </body>
</html>
