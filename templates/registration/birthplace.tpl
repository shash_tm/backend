    {if !$edit} 
	<p class="caption">{$registration_top_content.birth_place}</p>
{/if}
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="age">Cake</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>   
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
<p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
			<p class="formlabel">{$allQ.age}</p>
                        <div class="selectnew">
   
                        <p class="selectsearch">
                    	<select class="span6 chzn-select-dob" data-placeholder="DD" tabindex="1" id='whenBornDay'>
                            <option value=""></option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
		<option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option>
<option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
                       </select>
                    </p>
                    <p class="selectsearch ml15">
                        <select class="span6 chzn-select-dob" data-placeholder="MM" tabindex="1" id='whenBornMonth'>
                            <option value=""></option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                       </select>
                    </p>
                    <p class="selectsearch ml15">
                        <select class="span6 chzn-select-dob" data-placeholder="YYYY" tabindex="1" id='whenBornYear'>
                            <option value=""></option>
{foreach from=$yearRange item=year}
                <option value="{$year}" >{$year}</option>
{/foreach}
                       </select>
                    </p>
	<p id='whenBorn' style="color:red" class="fs11 mt5"></p>
                        <!-- old code
                        <p class="selectsearch">
                                <select class="chzn-select span6" data-placeholder="Years" tabindex="1" id='age'>
                                    <option value=""></option>
        {foreach from=$ageOwn item=age}               
         <option value="{$age}" >{$age}</option>
	{/foreach}
</select>
<br/><p id='age_Error' style="color:red" class="fs11"></p>
-->

</div>
<p class="formlabel mt15">{$allQ.marital}</p>
                        <ul class="formlist mt5">
{foreach from=json_decode($maritalStatus) key=k item=foo}
                                <li><label class="radio">
                                    <input type="radio" name="marital_status" id="optionsRadios{$k+1}" value="{$foo}">
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>
{/foreach}

                        </ul>
<p id='marital_Error' style="color:red" class="fs11 mt5"></p>
<div id='haveChildrenDiv'>
<p class="formlabel mt10">{$allQ.children}</p>
                        <ul class="formlist mt5">
                                <li class="agree"><label class="radio">
                                    <input type="radio" name="haveChildren" id="optionsRadios1" value="Yes">
                                    <span class="listitems">Yes</span>
                                </label>
                            </li>
                            <li class="disagree"><label class="radio">
                                    <input type="radio" name="haveChildren" id="optionsRadios2" value="No">
                                    <span class="listitems">No</span>
                                </label>
                            </li>
                        </ul>
<p id='haveChildren_Error' style="color:red" class="fs11"></p>
</div>
                    </div>
                </div>
            </div>

            <div class="rt-her">
            	<div class="innerdiv youdivider">
		{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle">
                        <p class="formlabel">{$allQ.age_spouse}</p>
                        <div class="clearfix">
                            <p class="selectsearch">
                                <select class="span6 chzn-select-dob" data-placeholder="Min." tabindex="1" id="lookingAgeStart"> 
                             	<option value=""></option>
				{foreach from=$ageSpouse item=age}
                        	<option value="{$age}" >{$age}</option> 
				{/foreach}

                               </select>
                           </p>
                           <p class="selectsearch mlr5">to</p>
                           <p class="selectsearch">
                               <select class="span6 chzn-select-dob" data-placeholder="Max." tabindex="1" id="lookingAgeEnd" >
                               		<option value=""></option>
	{foreach from=$ageSpouse item=age}
                        <option value="{$age}" >{$age}</option>         
{/foreach}


                               </select>
                           </p>
<br/><p id='ageRange' style="color:red" class="fs11 mt5"></p>
	<p class="formlabel mt15">{$allQ.marital_spouse}</p>
	<span class="pink fs12 mt5">(Select Multiple)</span>
                        <ul class="formlist mt5">
{foreach from=json_decode($maritalStatus) key=k item=foo}
                                <li>
                                <label class="checkbox" for="checkbox{$k+1}">
                                    <input type="checkbox"  value="{$foo}" id="checkbox{$k+1}" name='marital_status_spouse'>
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>
{/foreach}

                            <li>
                                <label class="checkbox" for="checkbox_notMatter_marital">
                                    <input type="checkbox" value="null"  id="checkbox_notMatter_marital" name='marital_status_spouse_notMatter'>
                                    <span class="listitems">Doesn’t Matter</span>
                                </label>
                            </li>
                        </ul>
<p id='marital_spouse_Error' style="color:red" class="fs11 mt5"></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
        	<div class="continuert"><a onclick="onContinue(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
                <div class="continuert"><a id="birthplace_save"  onclick="onContinue(0)">Save</a></div>
        </div>
{/if}

