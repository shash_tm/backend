<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Top progress section start -->

    <div class="regprogress">
    {if $isRegistration eq 1 }
		<h2>SAY CHEESE!</h2>
	{else}
		<h2>My Photos</h2>
	{/if}
	<div class="regscalesec" align="center" {if $isRegistration eq 1 }{else}style="display:none;"{/if}>
           <div class="regprogbar pbarstep5"></div>
   	 </div>
    <!--<div class="regscalesec"><img src="http://dev.trulymadly.com/trulymadly/templates/new/images/regprogress.png"><div class="regprogbar"></div></div>-->
	</div>
<!-- Top progress section end -->


<!--Skip popup start -->
<div id="diffframe" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="photopopup">
<div class="photowraper">
	<a href="javascript:void(0);" onClick="document.getElementById('diffframe').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>

<p class="pwraptxt">Hesitating to upload a photo? Here's why you should</p>

<ul class="pwrapulli">
<li>Your photo is only shown to members who meet your preferences.</li>
<li>Your photo cannot be downloaded or saved by anyone.</li>
<!--<li>Only members with photos can view photos of other members. </li>-->
</ul>
<a href="javascript:void(0);" onClick="document.getElementById('diffframe').style.display='none';" class="phactionbtn fl">Upload Picture</a> <a href="#" id="nothnx1" class="skiptxt">Skip</a>
<div class="clb"></div>
</div></div></div>
<!--Skip popup end -->

<!-- Photo section start -->
	<div class="photosec">
		<div class="pframeleft" id="testDiv1">
			 <div class='profilepicsec' id='testdivprofile'><div class='safhake addmore'><i class='icon-plus'></i></div></div>
			 <div class='addotherpicsec' id='testdiv_0'><div class='addmore'><i class='icon-plus'></i><br>Add More</div></div>
			 <div class='addotherpicsec' id='testdiv_1'><div class='addmore'><i class='icon-plus'></i><br>Add More</div></div>
			 <div class='addotherpicsec' id='testdiv_2'><div class='addmore'><i class='icon-plus'></i><br>Add More</div></div>
			 <div class='addotherpicsec' id='testdiv_3'><div class='addmore'><i class='icon-plus'></i><br>Add More</div></div>
			 <div class='addotherpicsec' id='testdiv_4'><div class='addmore'><i class='icon-plus'></i><br>Add More</div></div>
        </div>
        <div class="pframeright">
        <h2>Photo Guidelines</h2>
        <p class="phguidline fl"><img src="{$cdnurl}/images/phguidline1.png" width="65" height="65"><br>No photos<br>with sunglasses</p>
        <p class="phguidline fr"><img src="{$cdnurl}/images/phguidline2.png" width="65" height="65"><br>No blurred<br>photos</p>
        <p class="phguidline fl"><img src="{$cdnurl}/images/phguidline3.png" width="65" height="65"><br>No obscene<br>photos</p>
        <p class="phguidline fr"><img src="{$cdnurl}/images/phguidline4.png" width="65" height="65"><br>No copyrighted<br>photos</p>
        <span class="pnotetxt"><strong>Note:</strong> Only JPG,  GIF or PNG format not larger than 8 MB</span>
        <h2>Who will see my photos ?</h2>
        <ul>
       <!-- <li><i class="icon-ok"></i>Not the whole world:<br><span>your photos will not be publicly visible.</span></li>
        <li><i class="icon-ok"></i>A select few:<br><span>only the hand-picked matches.</span></li>-->
        <li>Your photos remain private and are only visible to profiles recommended by us.</li>
        <li>More so, no one can download or forward them. Picture perfect, yeah?</li>
        </ul>
        </div>
        
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul>
    </div>
<!-- Photo section end -->
<div class="regfrmaction phofrmaction">
<!--<a onclick="onBack(this)" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a> -->
<a id="continue" onclick="onContinue(this)" class="regbcbtn fr" {if $isRegistration eq 1 && !empty($photos)}{else}style="display:none;"{/if}>Continue &nbsp;<i class="icon-angle-right"></i></a>
<a href="#" id="nothnx" class="skiptxt1" {if $isRegistration eq 1 && empty($photos)}{else}style="display:none;"{/if}>Skip for now&nbsp;<i class="icon-double-angle-right"  style="text-decoration:none;"></i></a>
</div>

</div>
<!--Centered Container End -->
</div>


<!-- Photo Upload Options -->
<div id="phuploadoption" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="photopopup">
	<div class="photowraper">
	<a href="javascript:void(0);" onClick="document.getElementById('phuploadoption').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
	
	<a class="upoptionbtn"><form method="POST" id="add_from_computer_form" enctype="multipart/form-data" class="photofmstyle"><input type="file" id="add_from_computer" name="file" value="Add from computer"/></form><i class="icon-laptop"></i>Add from computer</a><br><br>
	<a onclick="proceed();" class="upoptionbtn"><i class="icon-facebook-sign"></i>Import From Facebook</a>
    <div class="clb"></div>
	<!--<a href="#" class="upoptionbtn"><i><img src="http://dev.trulymadly.com/trulymadly/templates/new/images/webcam.png" width="18" height="22" border="0" align="absmiddle"></i>Take a selfie</a>-->
    </div>
</div>
</div>
<!-- Pop Up end -->

<!--facebook album popup start -->

<div id="fbuploadpopup" style="display:none;">
    <div class="difusescreen" align="center"></div>
	<div class="photopopup">
	<div class="photowraper"></div>
	</div>
</div>


<!-- Photo Crop Pop Up Start -->
<div id="phuploadpopup" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="photopopup">
	<div class="photowraper">
	<a id="cancelPhoto" href="javascript:void(0);" onClick="document.getElementById('phuploadpopup').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
    <!--Photo Crop Area Start -->
    <div class="fberrmsg" id="photoerr"></div>
    <div class="cropframe" id="imageDiv"></div>

	<!--Photo Crop Area End -->
	<div id="upload_image" class="phactionbtn"><i class="icon-save"></i>Save Photo</div>
	<div id='save' class="phactionbtn"><i class="icon-save"></i>Crop &amp; Save</div>
    <div id='chop' class="phactionbtn"><i class="icon-trash"></i>Delete Photo</div>
    <!--<a href="#" class="phactionbtn"><i class="icon-user"><span class="icon-star"></span></i>Set As Profile Pic</a>-->
    <div id='save_as_profile' class="phactionbtn"><i class="icon-user"><span class="icon-star"></span></i>Set As Profile Pic</div>
    <div class="clb"></div>
    </div>
</div>
</div>
<input type="hidden" id="x" name="x" />
<input type="hidden" id="y" name="y" />
<input type="hidden" id="w" name="w" />
<input type="hidden" id="h" name="h" />
<!-- Pop Up end -->