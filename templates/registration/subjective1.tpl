{if !$edit}
{include file='templates/registration/header_registration.tpl'}
{/if}
<link href="{$cdnurl}/js/lib/select2/select2-bootstrap.css" rel="stylesheet"/>
<link href="{$cdnurl}/js/lib/select2/select2.css" rel="stylesheet"/>
<script>
var minimumword = 20;
</script>

<script type="text/javascript" src="{$cdnurl}/js/register/core_subjective1.js?v=15"></script>

<div class="container">
   
   
   
   
   <!-- Questionaries Section Start    -->
{if !$edit}        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">This is it...and you're all done!<br />
<span class="fs14 pink"> (Please fill details for at least one question!)</span></p>
<!-- <h2 class="mt20"></h2> -->
<div id="progressbar"></div>
{/if}
<div class="clearfix mt10">

	<!--Hobbies Pop up-->
	<div align="center" class="subjctvq">
    <p><!--<i>Question1</i> :  -->Three things that I am grateful for are...  </p>
    <textarea name="subq1" cols="" rows="" id = "unique_text" placeholder="Type Text...">{if isset($subjective)}{$subjective.unique_text}{/if}</textarea>
    <span>Example - Chocolate chip ice-cream, my pet dog, my limitless credit card, my morning Yoga session...</span>
    <span id='unique_char'></span>
    <p id="unique_error" style="color:red" class="fs11"></p>
    <p class="mt20"><!--<i>Question2</i> :  -->My idea of a perfect date is...</p>
    <textarea name="subq2" cols="" rows="" id="weekend_text" placeholder="Type Text...">{if isset($subjective)}{$subjective.weekend_text}{/if}</textarea>
    <span>Example - watching a movie with a tub of popcorn, going to my favourite spa, partying till sunrise...</span>
    <span id='weekend_char'></span>
    <p id="weekend_error" style="color:red" class="fs11"></p>
    <p class="mt20"><!--<i>Question3</i> :  -->My friends would describe me as...</p>
    <textarea name="subq3" cols="" rows="" id="friend_text" placeholder="Type Text...">{if isset($subjective)}{$subjective.friend_text}{/if}</textarea>
    <span>Example - loyal, fun, crazy, thoughtful...</span>
    <span id='friend_char'></span>
    <p id="friend_error" style="color:red" class="fs11"></p>
    </div>
   <span style="margin:-30px 20px 10px 0; display:none" id="error_class" class="fr pink fs12">Please fill complete details for at least one question!</span>
    </div>
    
    <div class="clearfix">
    {if !$edit}
	<div class="continuert"><a href="javascript:void(0)" onclick='onContinue9(this)'>Save</a></div>
	{else}
	<div class="continuert"><a href="javascript:void(0)" id="subjective_save" onclick='onContinue5(this)'>Save</a></div>
	{/if}
	</div>
    
    

    </div>
    {if !$edit}
</div>
</body>
</html>
{/if}

