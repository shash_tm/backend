
    	<p class="caption">You’re 15% done…keep it rolling !!</p>
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="language">Language</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>    
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
<p class="youtext">YOU</p>
                    <div class="mt20 formstyle">
                        <p class="formlabel">What is your mother tongue ?</p>
                        <p>
                            <select class="span6 chzn-select" data-placeholder="Select Language..." tabindex="1" id='motherTongue'>
	<option value=""></option>
                        {foreach from=json_decode($tongues) key=k item=foo}
                        <option value="{$k}">{$foo}</option>
{/foreach}

                            </select>
                        </p>
<p id='motherTongueError' style="color:red;" class="fs11"></p>
                        <p class="formlabel mt15">Other languages you know... <span>(optional)</span></p>
                        <p>
                            <select data-placeholder="Select Language..." class="chzn-select span6" multiple="multiple" tabindex="6" id='otherLanguages'>
                                <option value=""></option>
{foreach from=json_decode($tongues) key=k item=foo}
                        <option value="{$k}">{$foo}</option>

{/foreach}

                            </select>
                        </p>
                    </div>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt20 formstyle">
                        <p class="formlabel">What could be the mother tongue of your match ? <span>(optional)</span></p>
                        <p>
                            <select data-placeholder="Select Language..." class="chzn-select span6" multiple="multiple" tabindex="6" id='spouseMotherTongue'>
                                <option value=""></option>
{foreach from=json_decode($tongues) key=k item=foo}
                        <option value="{$k}">{$foo}</option>
{/foreach}

                            </select>
                        </p>
<p id='spouseMotherTongueError' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix mt20">
	<div class="backleft"><a onclick="onBack(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue(this)">Continue</a></div>

        </div>
    
