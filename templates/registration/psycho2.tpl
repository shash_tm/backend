{include file='templates/registration/header_registration.tpl'}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/psycho.css">
<script src="{$cdnurl}/js/register/core_psycho2.js"></script>

<div class="container">
   
   
   
   
   <!-- Questionaries Section Start    -->
        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">Questions for Psychoanalysis...</p>
<div id="progressbar"></div>



<!-- My work Start    -->
<div class="questsecframe mt40">

<!-- Questions section start -->

<!-- Question 21 
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">To a great extent, my life is controlled by accidental happenings.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_21"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_21"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_21"><span class="listitems">Neutral</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_21"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_21"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


 Question 22 
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">Often, there is no chance of protecting my personal interest from bad bad luck happenings.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_22"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_22"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_22"><span class="listitems">Neutral</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_22"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_22"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


- Question 23
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">When I get what I want, it's usually because I'm lucky.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_23"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_23"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_23"><span class="listitems">Neutral</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_23"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_23"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


 Question 24 
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">People like me have very little chance of protecting our personal interests where they conflict with those of strong pressure groups.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_24"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_24"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_24"><span class="listitems">Neutral</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_24"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_24"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


 Question 25
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">My life is chiefly controlled by powerful others.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_25"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_25"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_25"><span class="listitems">Neutral</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_25"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_25"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>

<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">I feel like what happens in my life is mostly determined by powerful people.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_26"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_26"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_26"><span class="listitems">Neutral</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_26"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_26"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>

-->
<!-- Question 27 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">test During conflict I examine issue until I find a solution that really satisfies me and the other party.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="6" id="optionsRadios1" name="answer_27"><span class="listitems">Never</span></label></li>
<li><label class="radio"><input type="radio" value="7" id="optionsRadios2" name="answer_27"><span class="listitems">Rarely</span></label></li>
<li><label class="radio"><input type="radio" value="8" id="optionsRadios3" name="answer_27"><span class="listitems">Sometimes</span></label></li>
<li><label class="radio"><input type="radio" value="9" id="optionsRadios4" name="answer_27"><span class="listitems">Mostly</span></label></li>
<li><label class="radio"><input type="radio" value="10" id="optionsRadios5" name="answer_27"><span class="listitems">Always</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 28 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />During conflict I stand for my own and other's goals & interests.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="6" id="optionsRadios1" name="answer_28"><span class="listitems">Never</span></label></li>
<li><label class="radio"><input type="radio" value="7" id="optionsRadios2" name="answer_28"><span class="listitems">Rarely</span></label></li>
<li><label class="radio"><input type="radio" value="8" id="optionsRadios3" name="answer_28"><span class="listitems">Sometimes</span></label></li>
<li><label class="radio"><input type="radio" value="9" id="optionsRadios4" name="answer_28"><span class="listitems">Mostly</span></label></li>
<li><label class="radio"><input type="radio" value="10" id="optionsRadios5" name="answer_28"><span class="listitems">Always</span></label>
</li>
 </ul>
</form>
</div>
</div>



<!-- Question 29 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />During conflict I examine ideas from both sides to find a mutually optimal solution.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="6" id="optionsRadios1" name="answer_29"><span class="listitems">Never</span></label></li>
<li><label class="radio"><input type="radio" value="7" id="optionsRadios2" name="answer_29"><span class="listitems">Rarely</span></label></li>
<li><label class="radio"><input type="radio" value="8" id="optionsRadios3" name="answer_29"><span class="listitems">Sometimes</span></label></li>
<li><label class="radio"><input type="radio" value="9" id="optionsRadios4" name="answer_29"><span class="listitems">Mostly</span></label></li>
<li><label class="radio"><input type="radio" value="10" id="optionsRadios5" name="answer_29"><span class="listitems">Always</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 30 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">During conflict I work out a solution that serves my own as well as other's interests as good as possible.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="6" id="optionsRadios1" name="answer_30"><span class="listitems">Never</span></label></li>
<li><label class="radio"><input type="radio" value="7" id="optionsRadios2" name="answer_30"><span class="listitems">Rarely</span></label></li>
<li><label class="radio"><input type="radio" value="8" id="optionsRadios3" name="answer_30"><span class="listitems">Sometimes</span></label></li>
<li><label class="radio"><input type="radio" value="9" id="optionsRadios4" name="answer_30"><span class="listitems">Mostly</span></label></li>
<li><label class="radio"><input type="radio" value="10" id="optionsRadios5" name="answer_30"><span class="listitems">Always</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 31 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I feel that I am a person of worth, at least on an equal plane with others.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_31"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_31"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_31"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_31"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>



<!-- Question 32 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I feel that I have a number of good qualities.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_32"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_32"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_32"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_32"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 33 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I feel as if I have not achieved anything in my life.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_33"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_33"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_33"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_33"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 34 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I am able to do things as well as most other people.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_34"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_34"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_34"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_34"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 35 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I feel I do not have much to be proud of.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_35"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_35"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_35"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_35"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 36 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I take a positive attitude toward myself.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_36"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_36"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_36"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_36"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 37 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />On the whole, I am satisfied with myself.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_37"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_37"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_37"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_37"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 38 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I wish I could have more respect for myself.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_38"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_38"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_38"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_38"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 39 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I certainly feel useless at times.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_39"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_39"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_39"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_39"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 40 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />At times I think I am no good at all.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_40"><span class="listitems">Strongly Agree</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_40"><span class="listitems">Agree</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_40"><span class="listitems">Disagree</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_40"><span class="listitems strdispad">Strongly Disagree</span></label>
</li>
 </ul>
</form>
</div>
</div>
<div id="tmquestions" class="tmquestions hide">
{include file='templates/registration/reloader.tpl'}
</div>





<!-- Questions section End -->
<!--<button class="hide fl mrg oopsbtn" id="back"><span style="font-size:18px;">&laquo;</span>Back</button> -->
</div>
<!-- My Work End    -->
      
        

                
   <!-- Questionaries Section end    -->
            <div class="clearfix mt30">
	<div class="oopsleft hide" id="back">Back</div>
	</div>
     
     
    </div> 
     </div>
     
     
     
  </body>
</html>
