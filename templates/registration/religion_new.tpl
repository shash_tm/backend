<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Top progress section start -->
    <div class="regprogress">
	<h2>OTHER DETAILS</h2>
	<div class="regscalesec" align="center">
           <div class="regprogbar pbarstep4"></div>
   	 </div>
    <!--<div class="regscalesec"><img src="http://dev.trulymadly.com/trulymadly/templates/new/images/regprogress.png"><div class="regprogbar"></div></div>-->
	</div> 
<!-- Top progress section end -->
<!-- Form section start -->
	<div class="regformsec">
	   	<div class="formreg">
        <p>Height</p>
        <ul>
		<li class="fl mrg0">
        <div class="regselect-style regselddbg2">
        <select name="feet" id="feet" class="regselwidth2">
            <option value="" disabled selected>Feet</option>
            <option value="4">04</option>
            <option value="5">05</option>
            <option value="6">06</option>
            <option value="7">07</option>
		</select>
        </div>
        </li>
        <li class="fr mrg0">
        <div class="regselect-style regselddbg2">
        <select name="inches" id="inch" class="regselwidth2">
            <option value="" disabled selected>Inches</option>
            <option value="0">00</option>
            <option value="1">01</option>
            <option value="2">02</option>
            <option value="3">03</option>
            <option value="4">04</option>
            <option value="5">05</option>
            <option value="6">06</option>
            <option value="7">07</option>
            <option value="8">08</option>
            <option value="9">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
		</select>
        </div>
        </li>
        </ul>
        <p id='heightError' style="color:red;" class="fs11 mt5"></p>
      <!--  <p>Relationship Status</p>
       	<ul>
		<li class="dobform fl"><input class="cutomradio" type="radio" name="marital_status" value="Never Married"><label>Never Married</label></li>
        <li class="dobform padl1 fl"><input class="cutomradio" type="radio" name="marital_status" value="Divorced"><label>Divorced</label></li>
        <li class="dobform fr"><input class="cutomradio" type="radio" name="marital_status" value="Widowed"><label>Widowed</label></li>
        </ul>
        <p id='marital_Error' style="color:red;" class="fs11 mt5"></p>
     <div id='haveChildrenDiv'>
        <p>With Children</p>
       <ul>
		<li class="fl"><input class="cutomradio" type="radio" name="haveChildren" value="yes"><label>Yes</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="haveChildren" value="no"><label>No</label></li>
        </ul>
        <p id='haveChildren_Error' style="color:red;" class="fs11 mt5"></p>
     </div>
        -->
        <p>Religion</p>
        <div class="regselect-style">
        <select name="Religion" id="religion">
            <option value="" disabled selected>Select religion</option>
		</select>
        </div>
		<p id='religionError' style="color:red;" class="fs11 mt5"></p>
        <!--<p>Native Language</p>
        <select name="Nlanguage" id="motherTongue">
            <option value="" disabled selected>Select native language</option>
		</select>
		<p id='motherTongueError' style="color:red;" class="fs11 mt5"></p>
		-->
		
		<p>Income</p>
        <div class="regselect-style">
        <select name="Income" id="income">
            <option value="" disabled selected>Select income</option>
            <option value="0-10">&lt; 10 lakhs</option>
			<option value="10-20">10-20 lakhs</option>
			<option value="20-50">20-50 lakhs</option>
			<option value="50-51">&gt; 50 lakhs</option>
		</select>
        </div>
		<p id='income_Error' style="color:red" class="fs11"></p>
        
      
        </div>
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul>
    </div>
<!-- Form section end -->
<div class="regfrmaction">
<a onclick="onBack(this)" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
<a id="activeReligion" onclick="onContinue(this)" class="regbcbtn fr hide">Continue &nbsp;<i class="icon-angle-right"></i></a>
<a id="inactiveReligion" class="diffregbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>
</div>
<!--Centered Container End -->
</div>