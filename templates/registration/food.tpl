    	{if !$edit}<p class="caption">Tell us about your habits... </p>
{/if}

        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="food">Food</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>  
                {else}  <div class="youicon"><img src="images/register/women.png"></div>
                {/if}
                    <p class="youtext">YOU</p>
                    <div class="mt5 formstyle">
                        <p class="formlabel">{$allQ.food}</p>
                        <ul class="foodchoice">
{foreach from=json_decode($foodTypes) key=k item=foo} 
                        	<li><label class="radio">
                                    <input type="radio" name="food_status" id="optionsRadios{$k+1}" value="{$foo}">
                                    <p class="type"><span class="whattype {$foo}"></span>{$foo}</p>
                                </label>
                            </li>
{/foreach}
                        </ul>
<p id='food_status_Error' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt5 formstyle innerright">
                        <p class="formlabel">{$allQ.food_spouse}</p>
                        <span class="pink fs12 mt5">(Select Multiple)</span>
                        <ul class="foodchoice">
			{foreach from=json_decode($foodTypes) key=k item=foo}
                        	<li><label class="checkbox" for="checkbox{$k+1}_food">
                                    <input type="checkbox" value="{$foo}" id="checkbox{$k+1}_food" name='food_status_spouse'>
                                    <p class="type"><span class="whattype {$foo}"></span>{$foo}</p>
                                </label>
                            </li>
{/foreach}
				<li><label class="checkbox" for="checkbox_food_notMatter">
                                    <input type="checkbox" value="null" id="checkbox_food_notMatter" name='food_status_spouse_notMatter'>
                                    <p class="type"><span class="whattype doesmatter"></span>Doesn't  Matter</p>
                                </label>
                            </li>

                        </ul>
<p id='food_status_spouse_Error' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="backleft"><a onclick="onBack2(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue2(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
                <div class="continuert"><a  id="food_save"  onclick="onContinue2(4)">Save</a></div>
        </div>
{/if}
