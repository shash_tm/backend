{include file='templates/registration/header_registration.tpl'}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/psycho.css">
<script src="{$cdnurl}/js/register/core_psycho5.js"></script>

<div class="container">
   
   
   
   
   <!-- Questionaries Section Start    -->
        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">Questions for Psychoanalysis...</p>
<div id="progressbar"></div>



<!-- My work Start    -->
<div class="questsecframe mt40">

<!-- Questions section start -->



<!-- Question 47 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />In your ideal home who would be responsible for the following roles:</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li class="optlefttitle">Taking care of household chores:</li>
<li class="fl"><label class="radio"><input type="radio" value="1" id="optionsRadios4" name="answer_48"><span class="listitems">Myself</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="2" id="optionsRadios5" name="answer_48"><span class="listitems">Spouse</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="3" id="optionsRadios5" name="answer_48"><span class="listitems">Share</span></label></li>
 </ul>
</form>
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li class="optlefttitle">Taking financial decisions:</li>
<li class="fl"><label class="radio"><input type="radio" value="1" id="optionsRadios4" name="answer_49"><span class="listitems">Myself</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="2" id="optionsRadios5" name="answer_49"><span class="listitems">Spouse</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="3" id="optionsRadios5" name="answer_49"><span class="listitems">Share</span></label></li>
 </ul>
</form>
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li class="optlefttitle">Making investments:</li>
<li class="fl"><label class="radio"><input type="radio" value="1" id="optionsRadios4" name="answer_50"><span class="listitems">Myself</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="2" id="optionsRadios5" name="answer_50"><span class="listitems">Spouse</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="3" id="optionsRadios5" name="answer_50"><span class="listitems">Share</span></label></li>
 </ul>
</form>
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li class="optlefttitle">Looking after parents and in-laws:</li>
<li class="fl"><label class="radio"><input type="radio" value="1" id="optionsRadios4" name="answer_51"><span class="listitems">Myself</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="2" id="optionsRadios5" name="answer_51"><span class="listitems">Spouse</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="3" id="optionsRadios5" name="answer_51"><span class="listitems">Share</span></label></li>
 </ul>
</form>
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li class="optlefttitle">Earning money:</li>
<li class="fl"><label class="radio"><input type="radio" value="1" id="optionsRadios4" name="answer_52"><span class="listitems">Myself</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="2" id="optionsRadios5" name="answer_52"><span class="listitems">Spouse</span></label></li>
<li class="fl"><label class="radio"><input type="radio" value="3" id="optionsRadios5" name="answer_52"><span class="listitems">Share</span></label></li>
 </ul>
</form>
</div>
</div>


<!-- Question 48 -->
<!--
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">Willingness to share the role</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_53"><span class="listitems">YES</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_53"><span class="listitems">NO</span></label></li>
 </ul>
</form>
</div>
</div>
-->
<div id="tmquestions" class="tmquestions hide">
{include file='templates/registration/reloader.tpl'}
</div>

<!-- Questions section End -->
<!--<button class="hide fl mrg oopsbtn" id="back"><span style="font-size:18px;">&laquo;</span>Back</button> -->
</div>
<!-- My Work End    -->

        <div id='next' class="clearfix mt30 hide">    
        <div class="oopsleft hide" id="back">Back</div>    
        	<div class="continuert mr50"><a >Continue</a></div>
        </div>
              
   <!-- Questionaries Section end    -->
   </div>
   
   

     
     
     
</div>  
     
     
  </body>
</html>
