{if !$edit}
	<p class="caption">Tell us what you do...</p>
	<script type="text/javascript">
		linkedConnectedFrom = 'workRegister';
	</script>
{else}
	<script type="text/javascript">
		linkedConnectedFrom = 'workLater';
	</script>
{/if}
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="work">Work</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>   
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}

                    <p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
                    
                     <p class="formlabel clear">{$allQ.work}</p>
                        <ul class="formlist mt5"  style="margin:0 auto; width:95%;">
			{foreach from=json_decode($working_areas) key=k item=foo}
                            <li><label class="radio">
                                    <input type="radio" name="working_area" id="optionsRadios{$k+1}" value="{$foo}">
                                    <span class="listitems"><span>{$foo}</span></span>
                                </label>
                            </li>
{/foreach}
                    	</ul>
                    <p id='working_area_Error' style="color:red;" class="fs11"></p>
               
        <div class="clear">&nbsp;</div>
                    
<div class="lnkinsec">                    
<!--<p class="caption">Let’s save on some of your typing time!!</p> -->
<!--  <p class="formlabel mb5">Professional Details:</p> -->      
<div id='linkedin_loader' style="width:216px; margin:0 auto;" align="center"> 
<p  class='linkdinimport'><a><img src='images/register/linkedin_icon.png' align='absmiddle' class='mr5' width='25'/>IMPORT FROM LINKEDIN</a></p></div>
<!-- <img src="{$cdnurl}/images/register/in_tm_loader.gif" width='140' id='linkedin_loader' align="left"/>-->
<div><script type='in/login'></script></div><img src="images/register/in_tm_loader.gif" id='load_linkedin_date' class="hide" width="140" />
<p id='error_linkedin' class="fs11" style="color:red"></p>

<br><p class="fs11" style="color:#777;"> We will not post anything on your LinkedIn profile.</p>
<!-- <p class="fs11 mt5">Boost Profile <a href="#" style="color:#4293f0;">Trust Score</a> &nbsp; | &nbsp; Earn FREE Credits <img src="images/register/info_in_icon.png" align="absmiddle" onmouseover="$('#linkinben').show();" onmouseout="$('#linkinben').hide();" /> </p>
 <div id="linkinben" class="fs11 linkinfo">&raquo; Add 10% to Trust Score<br> &raquo; Earn 50 Credits </div> -->
</div>
               
                  
                       

                        <!--<p class="formlabel mt10">My employment details are</p>-->
       
			<p id='moreWork' class="addmore"><a href="javascript:void(0);">Add more</a></p>

                    </div>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle innerright">
                        <p class="formlabel">{$allQ.work_spouse}</p>
                        <span class="pink fs12">(Select Multiple)</span>
                        <ul class="formlist mt5"  style="margin:0 auto; width:95%;">
{foreach from=json_decode($working_areas) key=k item=foo}
                            <li><label class="checkbox" for="working_area_spouse{$k+1}">
                                    <input type="checkbox" value="{$foo}" id="working_area_spouse{$k+1}" name='working_area_spouse'>
                                    <span class="listitems"><span>{$foo}</span></span>
                                </label>
                            </li>
{/foreach}
                            </li>
				<li><label class="checkbox" for="checkbox_notMatter_work_area">
                                    <input type="checkbox" value="null" id="checkbox_notMatter_work_area" name='working_area_spouse_notMatter'>
                                    <span class="listitems"><span>Doesn't Matter</span></span>
                                </label>
                            </li>
                        </ul>
<p id='working_area_spouse_Error' style="color:red;" class="fs11"></p>
                        <p class="formlabel mt10">{$allQ.industry_spouse}<span class="pink fs12">&nbsp;(Optional)</span></p>
                        
                        <div >
                        	<p><select data-placeholder="Select Industry..." class="chzn-select span6" multiple="multiple" tabindex="6" id='industry_spouse'>
    <option value=""></option>
			{foreach from=json_decode($industries) key=k item=foo}
                        <option value='{$k}'>{$foo}</option>
{/foreach}

                                </select>
                            </p>
<p id='industry_spouse_Error' style="color:red;" class="fs11"></p>
                        </div>


                    </div>


                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
 <!--                <div class="backleft"><a onclick="onBack4(this)">Back</a></div>  -->
                <div class="continuert"><a onclick="onContinue4(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
                <div class="continuert"><a  id="work_save" onclick="onContinue4(0)">Save</a></div>
        </div>
{/if}

