<html>
<head>
<meta charset="utf-8" />
<title>Trulymadly</title>
<link rel="shortcut icon" href="{$cdnurl}/images/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, user-scalable=no" />
<!--<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/style.css?v=17">-->
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/newregister-min.css">
<!--<link href="{$cdnurl}/js/lib/select2/select2-bootstrap.css" rel="stylesheet"/>
<link href="{$cdnurl}/js/lib/select2/select2.css" rel="stylesheet"/>-->
<script>var cdn='{$cdnurl}';
var baseurl='{$baseurl}';
var gender='{$data.gender}';
var errors=JSON.parse('{$allE}');
var edit=false;
var time = new Date();
var uid = '{$uid}';
var server="{$SERVER}";
var fb_api="{$fb_api}";

window.fbAsyncInit = function() {
  FB.init({
    appId      : fb_api, // App ID
    channelUrl : server + '/templates/channel.html', // Channel File
    oauth      : true,
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
          });

  };

function signout(){
        trackRegistration("logout");
	if(typeof FB!==undefined && FB){
		FB.getLoginStatus(function(response){
			if(response.status == 'connected'){
				FB.logout(function(response) {
			  		// user is now logged out
					window.location.href = "{$baseurl}/logout.php";
				});
			}else{
				window.location.href = "{$baseurl}/logout.php";
			}
		});
	}else{
		window.location.href = "{$baseurl}/logout.php";
	}
}
  // Load the SDK asynchronously
  
  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<!--<script src="{$cdnurl}/js/lib/core_progressbar.js"></script>-->
<!--<script src="{$cdnurl}/js/lib/jquery-ui-1.10.3.custom-min.js"></script>-->
<!--<script src="{$cdnurl}/js/lib/jquery.ui.spinner.js"></script>-->
<script src="{$cdnurl}/js/lib/tm_core.js"></script>
<!--<script src="{$cdnurl}/js/lib/custom_checkbox_and_radio.js"></script>-->
<script src="{$cdnurl}/js/lib/chosen.jquery.min.js"></script>
<!--<script src="{$cdnurl}/js/lib/jquery_cookie.js"></script>-->
<!--<script src="{$cdnurl}/js/lib/select2/select2.js"></script>-->
<script src="{$cdnurl}/js/lib/function.js?v=43"></script>
<!--[if IE]>
     <link  rel="stylesheet" type='text/css' href="css/IE-only.css" >
<![endif]-->
<!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="css/ie8.css" />
<![endif]-->

<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
  
</script>
{if $first_step=='true'}
	{literal}
	<script type="text/javascript">
	var fb_param = {};
	fb_param.pixel_id = '6010952207208';
	fb_param.value = '0.01';
	fb_param.currency = 'USD';
	(function(){
	var fpw = document.createElement('script');
	fpw.async = true;
	fpw.src = '//connect.facebook.net/en_US/fp.js';
	var ref = document.getElementsByTagName('script')[0];
	ref.parentNode.insertBefore(fpw, ref);
	})();
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6010952207208&amp;value=0.01&amp;currency=USD" /></noscript>
	 {/literal}
	 {/if}
</head>
<body>
<div class="wrapper">
    <!--Header-->
    <div class="header">
        <div class="container">
            <div class="clearfix">
                <div class="logo"><a href="{$cdnurl}/register.php"><img src="{$cdnurl}/images/logo.png" alt="Trulymadly" title="Trulymadly"></a></div>
               
                
               <div class="fr mt10">
   
{if $data.gender eq 'M'}
<img style="width:40px;height:40px; float:left;" src="{$cdnurl}/images/dummy_male.gif" />
{else }
<img style="width:40px;height:40px; float:left;" src="{$cdnurl}/images/dummy_female.gif" />
{/if}

<p class="fl ml10">{$data.fname} {$data.lname}<br>  <a onclick="signout()" style="font-size:12px; color:#4293EF;">Logout</a></p>
</div>
                
            </div>
        </div>
    </div>

