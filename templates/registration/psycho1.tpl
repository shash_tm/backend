{include file='templates/registration/header_registration.tpl'}
	<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/psycho.css">
	<script src="{$cdnurl}/js/register/core_psycho1.js"></script>


   <div class="container">
   <!-- Questionaries Section Start    -->
        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">Questions for Psychoanalysis...</p>
<div id="progressbar"></div>



<!-- My work Start    -->
<div class="questsecframe mt40">

<!-- Questions section start -->

<!-- Question 1 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I speak softly.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_1"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_1"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_1"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_1"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_1"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 2-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I let others finish what they are saying.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_2"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_2"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_2"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_2"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_2"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>



<!-- Question 3-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I seldom mention about my accomplishments.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_3"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_3"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_3"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_3"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_3"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 4-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I dislike being the center of attention.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_4"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_4"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_4"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_4"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_4"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>


<!-- Question 5-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I tolerate a lot from others.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_5"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_5"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_5"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_5"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_5"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 6-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I take things as they come.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_6"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_6"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_6"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_6"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_6"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 7-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I think of others first.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_7"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_7"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_7"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_7"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_7"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 8-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I rarely exaggerate the truth.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_8"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_8"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_8"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_8"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_8"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 9-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I am interested in people.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_9"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_9"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_9"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_9"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_9"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 10-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I reassure others.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_10"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_10"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_10"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_10"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_10"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 11-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I inquire about other's well-being.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_11"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_11"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_11"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_11"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_11"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 12-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I get along well with others.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_12"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_12"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_12"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_12"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_12"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 13-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I demand to be the center of interest.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_13"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_13"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_13"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_13"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_13"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 14-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I do most of the talking.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_14"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_14"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_14"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_14"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_14"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 15-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I speak loudly.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_15"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_15"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_15"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_15"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_15"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 16-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I demand attention.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_16"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_16"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_16"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_16"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_16"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 17-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I am harsh with people.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_17"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_17"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_17"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_17"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_17"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 18-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I contradict others.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_18"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_18"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_18"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_18"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_18"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 19-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />I snap at people.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_19"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_19"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_19"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_19"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_19"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<!-- Question 20-->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />My words hurt others.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="1" id="optionsRadios1" name="answer_20"><span class="listitems">Very Accurate</span></label></li>
<li><label class="radio"><input type="radio" value="2" id="optionsRadios2" name="answer_20"><span class="listitems">Accurate</span></label></li>
<li class="mrglongli"><label class="radio"><input type="radio" value="3" id="optionsRadios3" name="answer_20"><span class="listitems listlong">Neither Accurate Nor Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="4" id="optionsRadios4" name="answer_20"><span class="listitems">Inaccurate</span></label></li>
<li><label class="radio"><input type="radio" value="5" id="optionsRadios5" name="answer_20"><span class="listitems">Very Inaccurate</span></label>
</li>
 </ul>
</form>
</div>
</div>

<div id="tmquestions" class="tmquestions hide">
{include file='templates/registration/reloader.tpl'}
</div>
<!-- Questions section End -->
<!--<button class="hide fl mrg oopsbtn" id="back"><span style="font-size:18px;">&laquo;</span>Back</button> -->
</div>

<!-- My Work End    -->
     
        
        
                
   <!-- Questionaries Section end    -->
          <div class="clearfix mt30">
	<div class="oopsleft hide" id="back">Back</div>
	</div>
   
     </div>
     
     


     
</div>  
     
     
  </body>
</html>
