{include file='templates/registration/header_registration.tpl'}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/psycho.css">
<script src="{$cdnurl}/js/register/core_psycho4_male.js"></script>

<div class="container">
   
   
   
   
   <!-- Questionaries Section Start    -->
        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">Questions for Psychoanalysis...</p>
<div id="progressbar"></div>



<!-- My work Start    -->
<div class="questsecframe mt40">

<!-- Questions section start -->


<!-- Question 45 - a  -->
<!--
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">After marriage my spouse and I Will...</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li class="fl mrg"><label class="radio"><input type="radio" value="7" id="optionsRadios4" name="answer_45"><span class="listitems aftrmrtxt">Live in a joint family (with other married siblings, parents, uncle, aunts and cousin etc.)</span></label></li>
</ul>
<ul class="formlist mt20 clearfix">
<li class="fl mrg"><label class="radio"><input type="radio" value="8" id="optionsRadios5" name="answer_45"><span class="listitems aftrmrtxt">Live in a joint family with just my parents and siblings (if any)</span></label></li>
</ul>
<ul class="formlist mt20 clearfix">
<li class="fl mrg"><label class="radio"><input type="radio" value="9" id="optionsRadios5" name="answer_45"><span class="listitems aftrmrtxt">Live  in the house my spouse and I arrange with my parents joining later</span></label></li>
</ul>
<ul class="formlist mt20 clearfix">
<li class="fl mrg"><label class="radio"><input type="radio" value="10" id="optionsRadios5" name="answer_45"><span class="listitems aftrmrtxt">Live independently (just two of us and kids in future)</span></label></li>
</ul>
</form>
</div>
</div>
-->
<!-- Question 54  -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"> <br />  Career-wise, I prefer my partner after marriage to be.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="11" id="optionsRadios4" name="answer_54"><span class="listitems">Working</span></label></li>
<li><label class="radio"><input type="radio" value="12" id="optionsRadios5" name="answer_54"><span class="listitems">Not Working</span></label></li>
<li><label class="radio"><input type="radio" value="13" id="optionsRadios5" name="answer_54"><span class="listitems">Doesn't Matter</span></label></li>
</ul>
</form>
</div>
</div>

<!-- Question 46 -->
<!--<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"> I am open if both or any of my wife’s parents live with us after marriage.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="5" id="optionsRadios4" name="answer_46"><span class="listitems">YES</span></label></li>
<li><label class="radio"><input type="radio" value="6" id="optionsRadios5" name="answer_46"><span class="listitems">NO</span></label></li>
 </ul>

</form>
</div>
</div>
-->

<!-- Question 47 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">To what extent do you want your partner to follow your family traditions and rituals?.</div>
<div class="ansquest clb mrg" align="center">
<form name="form1" method="post" action="">
<ul class="formlist mt10 clearfix">
<li><label class="radio"><input type="radio" value="5" id="optionsRadios4" name="answer_47"><span class="listitems">All of them</span></label></li>
<li><label class="radio"><input type="radio" value="6" id="optionsRadios5" name="answer_47"><span class="listitems">Most of them</span></label></li>
<li><label class="radio"><input type="radio" value="7" id="optionsRadios4" name="answer_47"><span class="listitems">Only the important ones</span></label></li>
<li><label class="radio"><input type="radio" value="8" id="optionsRadios5" name="answer_47"><span class="listitems">None of them/does not matter</span></label></li>

 </ul>
</form>
</div>
</div>

<div id="tmquestions" class="tmquestions hide">
{include file='templates/registration/reloader.tpl'}
</div>
<!-- Questions section End -->
<!--<button class="hide fl mrg oopsbtn" id="back"><span style="font-size:18px;">&laquo;</span>Back</button> -->
</div>
<!-- My Work End    -->
        
        
        <div class="clearfix mt30">
	<div class="oopsleft hide" id="back">Back</div>
	</div>

                </div>
                      
   <!-- Questionaries Section end    -->
     
     
     
     
     
     </div>
     
     
     
  </body>
</html>
