{if !$edit}    

<p class="caption"> Tell us more about yourself... </p>

{/if}
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="religion">Religion</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>    
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
<p class="youtext">YOU</p>
                    <div class="mt10 formstyle" id='religionAndCaste'>
                        <p class="formlabel">{$allQ.religion}</p>
                        <p class="blue"></p>
                        <ul class="formlist mt10 clearfix">
	
		{foreach from=json_decode($religions) key=k item=foo}
		<li><label class="radio">
                                    <input type="radio" name="religion" id="optionsRadios{$k}" value="{$k}" >
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>
	
		
{/foreach}
	
                        </ul>
			<p id='religionError' style="color:red;" class="fs11"></p>
<p class="formlabel mt15">{$allQ.tongue}</p>
                        <p>
                            <select class="span6 chzn-select" data-placeholder="Select Language..." tabindex="1" id='motherTongue'>
        <option value=""></option>
                        {foreach from=json_decode($tongues) key=k item=foo}
                        <option value="{$k}">{$foo}</option>
{/foreach}

                            </select>
                        </p>
<p id='motherTongueError' style="color:red;" class="fs11 mt5"></p>
<div id='my_caste' style="display: none;">
                        <p class="formlabel mt15 clearfix">{$allQ.caste}<span class="pink fs12">&nbsp;(Optional)</span></p>
                        <p>
                            <select data-placeholder="Select Caste..." class="chzn-select span6" tabindex="-1" id='caste' >

                            </select>
<p id='casteError' style="color:red;" class="fs11"></p>
     </p>
</div>


                    </div>
                </div>
            </div>						
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}
                    <div class="mt10 formstyle" id='religionAndCasteSpouse'>
                        <p class="formlabel">{$allQ.religion_spouse}</p>
                        <span class="pink fs12 mt5">(Select Multiple)</span>
                        <ul class="formlist mt10 clearfix">
			{foreach from=json_decode($religions) key=k item=foo}
			 <li>
                                <label class="checkbox" for="checkbox{$k}_religion">
                                    <input type="checkbox" name='religionSpouse' value="{$k}" id="checkbox{$k}_religion">
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>

{/foreach}
                            <li>
                                <label class="checkbox" for="checkbox_notMatter_religion">
                                    <input type="checkbox" name='religionSpouseDoesNotMatter' value="null" id="checkbox_notMatter_religion">
                                    <span class="listitems">Doesn't Matter</span>
                                </label>
                            </li>
                        </ul>
<p id='religionSpouseError' style="color:red;" class="fs11"></p>
<p class="formlabel mt15">{$allQ.tongue_spouse}<span class="pink fs12">&nbsp;(Optional)</span></p>
                        <p>
                            <select data-placeholder="Select Language..." class="chzn-select span6" multiple="multiple" tabindex="6" id='spouseMotherTongue'>
                                <option value=""></option>
{foreach from=json_decode($tongues) key=k item=foo}
                        <option value="{$k}">{$foo}</option>
{/foreach}

                            </select>
                        </p>
<p id='spouseMotherTongueError' style="color:red;" class="fs11"></p>
<div><div id='spouse_caste' style="display: none;">
                        <p class="formlabel mt15">{$allQ.caste_spouse}<span class="pink fs12">&nbsp;(Optional)</span></p>
                        <p>
                            <select data-placeholder="Enter Caste here..." class="chzn-select span6" tabindex="-1" multiple="multiple" id='casteSpouse'>
                            </select>
                        </p>

<p id='casteSpouseError' style="color:red;" class="fs11"></p>
</div></div>
                

    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
        	<div class="backleft"><a onclick="onBack(this)">Back</a></div>
        	<div class="continuert"><a onclick="onContinue(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
                <div class="continuert"><a  id="relgion_save" onclick="onContinue(1)">Save</a></div>
        </div>
{/if}

    
