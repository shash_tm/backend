<!DOCTYPE>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<script>
var time = new Date();
var time1 = new Date();
</script>
<link rel="stylesheet" href="{$cdnurl}/css/register_new.css?v=6" type="text/css">
<link rel="stylesheet" href="{$cdnurl}/css/common/fontstyle.css" type="text/css">
<link rel="stylesheet" href="{$cdnurl}/css/common/font-awesome.css" type="text/css">
<link rel="stylesheet" href="{$cdnurl}/css/common/headfooter.css?v=3" type="text/css">
<link rel="stylesheet" href="{$cdnurl}/css/register/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="{$cdnurl}/css/register/facebook_photo.css?v=4" type="text/css" />
<link rel="stylesheet" href="{$cdnurl}/css/photo/jquery.fileupload-ui.css" type="text/css" />
<script>
{if $edit_photo}
var edit=true;
{else}
var edit = false;
{/if}
var uid = '{$uid}';
var cdn = '{$cdnurl}';
var baseurl='{$baseurl}';
var cameraOn=false;
var server="{$SERVER}";
var fb_api="{$fb_api}";
var incomplete='{$incomplete}';
var imagePath="{$imagePath}";
var facebookScope = "{$facebook_scope}";
var profile_photo_exist = "{$profile_photo_exist}";
var   jcrop_api;
var sayCheese;
var connectedFrom = "photoRegister";
var photoJson='{$photos}';
var photosAll;
if(photoJson!="")
photosAll=JSON.parse(photoJson);
{if $isRegistration eq 1}
var isRegistration=true;
{else}
var isRegistration = false;
{/if}
</script>

</head>
<body>
{include file='templates/common/header.tpl'}

<div class="container photostepbg" id="container">
{include file='templates/registration/photo_pane_new.tpl'}
</div>
 <!--   </div>
</div>-->

{include file='templates/common/footer.tpl'}

<script id="_webengage_script_tag" type="text/javascript">
var _weq = _weq || {};
_weq['webengage.licenseCode'] = '76a9cca';
_weq['webengage.widgetVersion'] = "4.0";

(function(d){
  var _we = d.createElement('script');
  _we.type = 'text/javascript';
  _we.async = true;
  _we.src = (d.location.protocol == 'https:' ? "https://ssl.widgets.webengage.com" : "http://cdn.widgets.webengage.com") + "/js/widget/webengage-min-v-4.0.js";
  var _sNode = d.getElementById('_webengage_script_tag');
  _sNode.parentNode.insertBefore(_we, _sNode);
})(document);
</script>

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/lib/tm_core.js"></script>
<script src="{$cdnurl}/js/lib/function.js?v=46"></script>
<!--<script src="{$cdnurl}/js/lib/jquery.simplemodal.js"></script>-->
<!--<script src='{$cdnurl}/js/lib/say-cheese.js'></script>-->
<script src="{$cdnurl}/js/lib/jquery.Jcrop.min.js"></script>
<script src="{$cdnurl}/js/lib/facebook_photo.js"></script>
<!--<script src="{$cdnurl}/js/register/jquery.slimscroll.js"></script>-->
<script src="{$cdnurl}/js/register/photo_uploader.js?v=6"</script>
<!--<script src="{$cdnurl}/js/vendor/jquery.ui.widget.js"></script>-->
<script src="{$cdnurl}/js/file_uploader/jquery.iframe-transport.js"></script>
<script src="{$cdnurl}/js/file_uploader/jquery.fileupload.js"></script>
<script src="{$cdnurl}/js/file_uploader/jquery.fileupload-process.js"></script>
<script src="{$cdnurl}/js/file_uploader/jquery.fileupload-validate.js"></script>
<script src="{$cdnurl}/js/file_uploader/jquery.fileupload-ui.js"></script>

</body>
</html>

