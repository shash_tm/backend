    	{if !$edit}
<p class="caption">That’s  good... keep it rolling!!</p>
{/if}

        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="complexion">Complexion</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>    
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}

                    <p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
                        <p class="formlabel">{$allQ.complexion}</p>
                        <ul class="complexion mt20">
{foreach from=json_decode($skin_tones) key=k item=foo}
                <li><label class="radio">
                                    <input type="radio" name="skin_tone" id="optionsRadios{$k}" value="{$foo}" >
                                    <span class="listitems {$foo}">{$foo}</span>
                                </label>
                            </li>


{/foreach}

                        </ul>
			<p id='skin_tone_Error' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle innerright">
                        <p class="formlabel">{$allQ.complexion_spouse}</p>
                        <ul class="complexion mt20">
				<li>

                                <label class="checkbox" for="checkbox_skin_notMatter">

                                    <input type="checkbox" value="null" name='skin_tone_spouse_notMatter' id="checkbox_skin_notMatter">

                                    <span class="listitems allskins">All</span>

                                </label>

                            </li>
		{foreach from=json_decode($skin_tones) key=k item=foo}
 <li>        
                                <label class="checkbox" for="checkbox{$k}_skin">
                                    <input type="checkbox" name='skin_tone_spouse' value="{$foo}" id="checkbox{$k}_skin">
                                    <span class="listitems {$foo}">{$foo}</span>
                                </label>
                            </li>

{/foreach}

                        </ul>
                    </div>
<p id='skin_tone_Spouse_Error' style="color:red;" class="fs11"></p>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="backleft"><a onclick="onBack2(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue2(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
<div class="continuert"><a   id="complexion_save" onclick="onContinue2(1)">Save</a></div>
        </div>
{/if}
