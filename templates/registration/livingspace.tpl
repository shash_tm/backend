{if !$edit}
    	<p class="caption">Tell us your whereabouts...</p>
{/if}
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="livingspace">Livingspace</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>    
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
<p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
                    
                    
<p class="formlabel">{$allQ.stay}</p>


<div class="professionaldetails mt10" id='stayLocation'>
                            <p class="formlabelnew">Country</p>
                            <p><select class="span6 chzn-select" data-placeholder="Select Country..." tabindex="1" id='stayCountry'>
			<option value=""></option>
			<option value="113" >India</option>
			{foreach from=$countries item=foo}
			{if $foo.country_id neq 113}
			<option value="{$foo.country_id}">{$foo.name}</option>
			{/if}

{/foreach}
                                </select>
                            </p>
<p id='stayCountryError' style="color:red;" class="fs11"></p>
                            <p class="formlabelnew mt5">State</p>
                            <p><select class="span6 chzn-select" data-placeholder="Select State..." tabindex="1" id='stayState'>
                                </select>
                            </p>
<p id='stayStateError' style="color:red;" class="fs11"></p>
                            <p class="formlabelnew mt5">City</p>
                            <p><select class="span6 chzn-select" data-placeholder="Select City..." tabindex="1" id='stayCity'>
                                </select>
                            </p>
<p id='stayCityError' style="color:red;" class="fs11"></p>
                        </div>
                    
                    
                    
                        <p class="formlabel mt15"> </p>
                                            

                      <!--  <ul class="formlist mt10 clearfix">
                          <li><label id='buttonSameAsAbove' class="checkbox">
                                    <input type="checkbox" id="sameAsBirth" >
                                    <span class="listitems">Same as Above</span>
                                </label>
                            </li>
                            </ul> -->
                        
                      <!--    <ul class="formlist mt10">
			<li class="fl" style="margin-left:70px; height:auto;">
                         <input type="checkbox" name='choseBirth' id="sameAsBirth" value='sameAs'><span class="listitems"> &nbsp;&nbsp;Same as above</span>
                               </li>
                                                  
                        </ul>  -->

<div class="clear"></div>
<p class="formlabel">Where are you from?</p>
                        <div class="professionaldetails selectother mt10">
                         <ul>
			<li class="fl" style="margin-top:-5px;">
                         <input type="checkbox" name='choseBirth1' id='sameAsAbove' value='sameAs1'><span class="listitems fs13"> &nbsp;&nbsp;Same as above</span>
                               </li>
                                                  
                        </ul>
                        <div class="clear"></div>
                                                
				<p class="formlabelnew clear mt10">Country</p>
                            <p><select class="span6 chzn-select" data-placeholder="Select Country..." tabindex="1" id='birthCountry'>
                        <option value=""></option>
                        <option value="113" >India</option>
                        {foreach from=$countries item=foo}
                        {if $foo.country_id neq 113}
                        <option value="{$foo.country_id}">{$foo.name}</option>
                        {/if}

{/foreach}
                                </select>
                            </p>
<p id='birthCountryError' style="color:red;" class="fs11"></p>

                          <p class="formlabelnew mt5">State</p>
                            <p><select class="span6 chzn-select" data-placeholder="Select State..." tabindex="1" id='birthState'>
                                </select>
                            </p>
<p id='birthStateError' style="color:red;" class="fs11"></p>
                            <p class="formlabelnew mt5">City</p>
                            <p><select class="span6 chzn-select" data-placeholder="Select City..." tabindex="1" id='birthCity'>
                                </select>
                            </p>
<p id='birthCityError' style="color:red;" class="fs11"></p>
                        </div>
                       <!-- <div class="professionaldetails mt20 selectother">
                        </div> -->
                      
                            

                        
                   <!--     <p class="formlabel mt40">What is the pin code of your locality? <span>(optional)</span></p>
                        <p><input type="text" name="pincode" placeholder="Enter your pincode" class="inputwidth"></p>
	<br/><p id='pincodeError' style="color:red;"></p>
-->
                    </div>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle">
                        <p class="formlabel">{$allQ.location_spouse}</p>
                        <div class="professionaldetails mt10 heightmin1">
                            <p class="formlabelnew">Country <span class="pink fs12">(Optional)</span></p>
                            <p><select data-placeholder="Select Country..." class="chzn-select span6" multiple="multiple" tabindex="-1" id='spouseCountry'>
<option value=""></option>
<option value="113" >India</option>
                        {foreach from=$countries item=foo}
                        {if $foo.country_id neq 113}
                        <option value="{$foo.country_id}">{$foo.name}</option>
                        {/if}
{/foreach}

                                </select>
                            </p>
<p id='spouseCountryError' style="color:red;" class="fs11"></p>
                            <p class="formlabelnew mt5">State <span class="pink fs12">(Optional)</span></p>
                            <p><select class="chzn-select span6"  data-placeholder="Select State..." multiple="multiple" tabindex="-1" id='spouseState'>
                                </select>
                            </p>
<p id='spouseStateError' style="color:red;" class="fs11"></p>
                            <p class="formlabelnew mt5">City <span class="pink fs12">(Optional)</span></p>
                            <p><select class="chzn-select span6"  data-placeholder="Select City..." multiple="multiple" tabindex="-1" id='spouseCity'>
                                </select>
                            </p>
<p id='spouseCityError' style="color:red;" class="fs11"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
		<div class="backleft"><a onclick="onBack(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue(this)">Continue</a></div>
        </div>
   {else}
<div class="clearfix mt20">
<div class="continuert"><a id="livingspace_save"  onclick="onContinue(2)">Save</a></div>
        </div>
{/if}

