<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Top progress section start -->

    <div class="regprogress">
	<h2>HONOUR ROLL</h2>
	<div class="regscalesec" align="center">
           <div class="regprogbar pbarstep4"></div>
   	 </div>
    <!--<div class="regscalesec"><img src="http://dev.trulymadly.com/trulymadly/templates/new/images/regprogress.png"><div class="regprogbar"></div></div>-->
	</div>
<!-- Top progress section end -->
<!-- Form section start -->
	<div class="regformsec">
	   	<div class="formreg">
        <p>Highest Degree</p>
        <div class="regselect-style">
        <select name="degree" id="education">
            <option value="" disabled selected>Select highest degree</option>
		</select>
        </div>
		<p id='education_Error' style="color:red" class="fs11"></p>
        <p>Colleges/School <span>(Optional)</span></p>
       <div class="comdetails"><a id="addInstitute" class="hide"><span class="addicon">Add</span></a><input name="institutes" id="institutes" type="text" class="regtxtbox inputindex" placeholder="Enter most recent first">
       <ul id="institute"></ul>
       </div>
      
        </div>
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul> 
    </div>
<!-- Form section end -->
<div class="regfrmaction">
<a onclick="onBack(this)" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
<a id="activeEdu" onclick="onContinue(this)" class="regbcbtn fr hide">Continue &nbsp;<i class="icon-angle-right"></i></a>
<a id="inactiveEdu" class="diffregbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>
</div>
<!--Centered Container End -->
</div>