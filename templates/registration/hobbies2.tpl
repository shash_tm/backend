<div id="fb-root"></div> 
<script type="text/javascript">
var fbScope = '{$facebook_scope}';
</script>

{literal}
<script>
window.fbAsyncInit = function() {
	FB.init({
		appId      : fb_api, // App ID
		channelUrl : cdn + '/templates/channel.html', // Channel File
		oauth      : true,
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true  // parse XFBML
	});

};

// Load the SDK asynchronously
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

var accessToken = null;

function proceed() {
	FB.getLoginStatus(function(response) {
		if(response.status === 'connected') {
			callServer(response);
		} else if (response.status === 'not_authorized') {
			FB.login(
					function(response) {
						callServer(response);
					}, {
						scope : fbScope
					});
		} else {
			FB.login(function(response) {
				if (response.authResponse) {
					callServer(response);
				}
			}, {
				scope : fbScope
			});
		}
	});
}

function callServer(response) {
	$("#fb_import").hide();
	$("#fb_message").html('');
	$("#loader_image").show();
	var accessToken = response.authResponse.accessToken;
	$.post(baseurl+"/trustbuilder.php", {
		checkFacebook : 'check',
		token : accessToken,
		action : 'interest',
		connected_from : connectedFrom
	}, function(data) {
		if(data.status=='SUCCESS'){
			loadFacebookData(data.fb_likes);
			$("#loader_image").hide();
			$("#fb_message").html('Data imported successfully from Facebook');
		}else if(data.status=='FAIL'){
			$("#fb_import").show();
			$("#loader_image").hide();
			$("#fb_message").html(data.error);
		}
		else if(data.status=='REFRESH'){
			FB.init({
				appId      : fb_api, // App ID
				channelUrl : cdn + '/templates/channel.html', // Channel File
				oauth      : true,
				status     : true, // check login status
				cookie     : true, // enable cookies to allow the server to access the session
				xfbml      : true  // parse XFBML
			});
			proceed();
		}
	}, "json");


}

/*
function formatResult(movie) {
	return '<div>' + movie.text + '</div>';
};

function formatSelection(data) {
	return data.text;
};

*/
</script>
{/literal}
	

	{if !$edit} <p class="caption">Tell us a few of your favourite things...</p> {/if}
	<div id="progressbar"></div>
	<div class="clear mt40"></div>

	<h2 class="fl inttitle">My Interests and Hobbies</h2>
	{if isset($fb_loaded)==false}
	<div id='fb_import' class="gbmprtbtn" onclick='proceed()'><a><img  src="images/register/add_from_fb.png" align="absmiddle" />&nbsp;&nbsp;Import from Facebook</a>
	</div>
	{/if}
	
	<img src=" images/fbtm_import.gif"  class="hide"  id="loader_image"/>
	<p class='fl mrg' id='fb_message' style="padding:8px 25px; font-size:16px; color:#158a00;"> 
	{if (isset($fb_loaded) && !$edit)}
		Data imported from Facebook
	{/if}
	</p>
	<div class="clear"></div>
	<div class="clearfix mt10">

	<!--Hobbies Pop up-->
	<div id="addhobbies-music" align="center" class="intnhob">
    <div class="tall fl" style="margin-left:40px;">{$allQ.interest}</div><p id='interest_Error' style="color:red" class="fs12 fl ml20"></p>
    <div class="clear"></div>
	<!-- <p class="poptitle">Please Select</p> -->
	<ul class="popuplist inthobmbnev" >
	<li> <!-- class="popuplistactive"><img class='temp_arrow' src="images/register/arrow_down.png" />-->
	<a href="javascript:void(0)" >Music<br><span id='music' class="hobbies-icon music"></span></a>
	</li>
	<li>
	<a href="javascript:void(0)" >Books <br><span id='books' class="hobbies-icon reading"></span></a>
	</li>
	<li>
	<a  href="javascript:void(0)">Movies<br><span id='movies' class="hobbies-icon movies"></span></a>
	</li>
	<li><a href="javascript:void(0)">Food<br><span id='food' class="hobbies-icon eating"></span></a>
	</li>

	<li>
	<a href="javascript:void(0)" >Sports<br><span id='sports' class="hobbies-icon sports"></span></a>
	</li>
	<li>
	<a href="javascript:void(0)" >Travel<br><span id='travel' class="hobbies-icon travel"></span></a>
	</li>
	<li>
	<a href="javascript:void(0)" >Others<br><span id='others' class="hobbies-icon other"></span></a>
	</li>


	</ul>
    
    
    <div class="inthobselsec clear">
    
	<p class="inthobtitle" id="hobbies_title">What kind of music do you like listening to?</p>
	
	<ul name='music_preferences' class="formlist mt5 fl hidev favitemsec"></ul>
	<ul name='books_preferences' class="formlist mt5 fl hide favitemsec"></ul>
	<ul name='food_preferences' class="formlist mt5 fl hide favitemsec"></ul>
	<ul name='movies_preferences' class="formlist mt5 fl hide favitemsec"></ul>
	<ul name='sports_preferences' class="formlist mt5 fl hide favitemsec"></ul>
	<ul name='travel_preferences' class="formlist mt5 fl hide favitemsec"></ul>
	<ul name='others_preferences' class="formlist mt5 fl hide favitemsec"></ul>
	<div class="clear"></div>
	
	<p class="inthobtitle mt25" id='allFavorites'>Tell us about your favourite singers, bands and songs...</p>
	<div name='music_favorites_div' class="hide formstyle mt5 tc fl likesec">
	<input id='music_favorites_input' name='music_favorites' type='hidden'  />
	</div>
	<div name='books_favorites_div' class="hide formstyle mt5 tc fl likesec" >
	<input name='books_favorites' type='hidden'   />
	</div>
	<div name='food_favorites_div' class="hide formstyle mt5 tc fl likesec" >
	<input name='food_favorites' type='hidden'   />
	</div>
	<div name='movies_favorites_div' class="hide formstyle mt5 tc fl likesec" >
	<input name='movies_favorites' type='hidden'   />
	</div>
	<div name='sports_favorites_div' class="hide formstyle mt5 tc fl likesec" >
	<input name='sports_favorites' type='hidden'   />
	</div>
	<div name='travel_favorites_div' class="hide formstyle mt5 tc fl likesec" >
	<input name='travel_favorites' type='hidden' />
	</div>
	<div class="clear mt30">&nbsp;</div>
    
    </div>
    
	</div>

	<div class="clearfix mt20">
	{if !$edit}
	<div class="continuert"><a href="javascript:void(0)" onclick='onContinue6(this)'>Save</a></div>
	{else}
	<div class="continuert"><a href="javascript:void(0)" id='hobby_save' onclick='onContinue7(this)'>Save</a></div>
	{/if}
	
	</div>