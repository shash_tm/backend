<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<title>Hobbies</title>
<script>
var time = new Date();
</script>
<link href="{$cdnurl}/css/register_new.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/register/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css">

<script>
try{
var toSelect=JSON.parse('{$toSelect}');
}catch(err){
	console.log(err);
}
var uid = '{$uid}';
var baseurl='{$baseurl}';//"http://dev.trulymadly.com/trulymadly";
var rdUrl = '{$rdUrl}';
var stepsCompleted = '{$stepCompleted|json_encode}';
{if $fromRegister eq 1 }
var fromRegister = true;
{else}
var fromRegister = false;
{/if}
</script>

</head>
<body>
{include file='templates/common/header.tpl'}
<div class="new_wrapper favstepbg">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Top progress section start -->

    <div class="regprogress">
	<h2>Favorites</h2>
    <!--<div class="regscalesec">
    	<img src="http://dev.trulymadly.com/trulymadly/templates/new/images/regprogress.png">
    	<div class="regprogbar"></div>
    </div>-->
	</div>
<!-- Top progress section end -->
<!-- Form section start -->
	<div class="regformsec">
	   	<div class="formreg">
        <p>What you watch:</p>
        <div class="comdetails"><a id="addMovie"></a><input name="movies" id="movies" type="text" class="regtxtbox inputindex" placeholder="Favourite movies, actors, TV shows, channels...">
       		<ul id="movie"></ul>
       </div>
       <p>What you listen to:</p>
       <div class="comdetails"><a id="addMusic"></a><input name="music" id="music_box" type="text" class="regtxtbox inputindex" placeholder="Favourite music, bands, songs, artists, genres...">
       		<ul id="music"></ul>
       </div>
       <p>What you read:</p>
       <div class="comdetails"><a id="addBook"></a><input name="read" id="books" type="text" class="regtxtbox inputindex" placeholder="Favourite books, authors, magazines, genres...">
       		<ul id="book"></ul>
       </div>
       <p>Where you go, what you eat:</p>
       <div class="comdetails"><a id="addTravel"></a><input name="travelnfood" id="travelnfood" type="text" class="regtxtbox inputindex" placeholder="Favourite cuisines, places, restaurants, holiday spots...">
       		<ul id="travel"></ul>
       </div>
       <p>What you do, what you like:</p>
       <div class="comdetails"><a id="addOther"></a><input name="others" id="others" type="text" class="regtxtbox inputindex" placeholder="Favourite sports and hobbies...">
       		<ul id="other"></ul>
       </div>
        
        </div>
    </div>
<!-- Form section end -->
<div class="regfrmaction">
<!--<a href="#" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a> -->
<a onclick="Trulymadly.interest.onContinue()" class="regbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>

</div>
<!--Centered Container End -->
<div class="container hide" id="container">
       {include file='templates/registration/reloader.tpl'}
</div>

</div>
{include file='templates/common/footer.tpl'}

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<!--<script src="{$cdnurl}/js/lib/jquery-ui-1.10.3.custom-min.js"></script>-->
<script src="{$cdnurl}/js/lib/jquery-ui-1.10.4.autocomplete-min.js"></script>
<script src="{$cdnurl}/js/lib/function.js?ver=1"></script>
<script src="{$cdnurl}/js/register/trulymadly.js"></script>
<script src="{$cdnurl}/js/register/hobbies.js?ver=2"></script>
</body>
</html>