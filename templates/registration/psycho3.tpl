{include file='templates/registration/header_registration.tpl'}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/psycho.css">
<script src="{$cdnurl}/js/register/core_psycho3.js"></script>

<div class="container">
   
   
   
   
   <!-- Questionaries Section Start    -->
        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">Questions for Psychoanalysis...</p>
<div id="progressbar"></div>



<!-- My work Start    -->
<div class="questsecframe mt40">

<!-- Questions section start -->


<!-- Question 41 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />How important is it that your partner talks to you in a loving manner?</div>
<div class="ansquest clb mrg" align="center">
<ul class="smokingstatus">
						<li class="status1 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="1" id="optionsRadios0" name="answer_41">
                                    <span class="status">Not at all</span>
                                </label>
                            </li>
				<li class="status2 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="2" id="optionsRadios1" name="answer_41">
                                    <span class="status">Very Less</span>
                                </label>
                            </li>
                            
				<li class="status3 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="4" id="optionsRadios2" name="answer_41">
                                    <span class="status">Slightly</span>
                                </label>
                            </li>
			<li class="status4 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="5" id="optionsRadios3" name="answer_41">
                                    <span class="status">Moderately</span>
                                </label>
                            </li>
                            
				<li class="status5 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="6" id="optionsRadios4" name="answer_41">
                                    <span class="status">Important</span>
                                </label>
                            </li>
				<li class="status6 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="7" id="optionsRadios5" name="answer_41">
                                    <span class="status">Very</span>
                                </label>
                            </li>
				<li class="status7 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="8" id="optionsRadios6" name="answer_41">
                                    <span class="status">Extremely</span>
                                </label>
                            </li>                                                        
                            </ul>
                            
<ul align="center" class="lstoption mt10"><li>Not at all</li><li>Very Less</li><li>Slightly</li><li>Moderately</li><li>Important</li><li>Very</li><li>Extremely</li></ul>
                            

</div>
</div>


<!-- Question 42 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop"><br />How important is physical expression of love, like a kiss or a hug for you?</div>
<div class="ansquest clb mrg" align="center">
<ul class="smokingstatus">
						<li class="status1 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="1" id="optionsRadios0" name="answer_42">
                                    <span class="status">Not at all</span>
                                </label>
                            </li>
				<li class="status2 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="2" id="optionsRadios1" name="answer_42">
                                    <span class="status">Very Less</span>
                                </label>
                            </li>
				<li class="status3 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="4" id="optionsRadios2" name="answer_42">
                                    <span class="status">Slightly</span>
                                </label>
                            </li>
				<li class="status4 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="5" id="optionsRadios3" name="answer_42">
                                    <span class="status">Moderately</span>
                                </label>
                            </li>
				<li class="status5 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="6" id="optionsRadios4" name="answer_42">
                                    <span class="status">Important</span>
                                </label>
                            </li>
				<li class="status6 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="7" id="optionsRadios5" name="answer_42">
                                    <span class="status">Very</span>
                                </label>
                            </li>
				<li class="status7 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="8" id="optionsRadios6" name="answer_42">
                                    <span class="status">Extremely</span>
                                </label>
                            </li>                                                        
                            </ul>
                            
                            <ul align="center" class="lstoption mt10"><li>Not at all</li><li>Very Less</li><li>Slightly</li><li>Moderately</li><li>Important</li><li>Very</li><li>Extremely</li></ul>
</div>
</div>


<!-- Question 43 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">How important is it for you to spend quality time with your partner, without any distractions?</div>
<div class="ansquest clb mrg" align="center">
<ul class="smokingstatus">
						<li class="status1 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="1" id="optionsRadios0" name="answer_43">
                                    <span class="status">Not at all</span>
                                </label>
                            </li>
				<li class="status2 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="2" id="optionsRadios1" name="answer_43">
                                    <span class="status">Very Less</span>
                                </label>
                            </li>
				<li class="status3 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="4" id="optionsRadios2" name="answer_43">
                                    <span class="status">Slightly</span>
                                </label>
                            </li>
				<li class="status4 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="5" id="optionsRadios3" name="answer_43">
                                    <span class="status">Moderately</span>
                                </label>
                            </li>
				<li class="status5 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="6" id="optionsRadios4" name="answer_43">
                                    <span class="status">Important</span>
                                </label>
                            </li>
				<li class="status6 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="7" id="optionsRadios5" name="answer_43">
                                    <span class="status">Very</span>
                                </label>
                            </li>
				<li class="status7 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="8" id="optionsRadios6" name="answer_43">
                                    <span class="status">Extremely</span>
                                </label>
                            </li>                                                        
                            </ul>
                            
                            <ul align="center" class="lstoption mt10"><li>Not at all</li><li>Very Less</li><li>Slightly</li><li>Moderately</li><li>Important</li><li>Very</li><li>Extremely</li></ul>
</div>
</div>


<!-- Question 44 -->
<div id="tmquestions" class="tmquestions hide">
<div class="questiontop">Is it important that your partner expresses love by doing little things like cooking, driving or helping with household chores?</div>
<div class="ansquest clb mrg" align="center">
<ul class="smokingstatus">
						<li class="status1 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="1" id="optionsRadios0" name="answer_44">
                                    <span class="status">Not at all</span>
                                </label>
                            </li>
				<li class="status2 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="2" id="optionsRadios1" name="answer_44">
                                    <span class="status">Very Less</span>
                                </label>
                            </li>
				<li class="status3 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="4" id="optionsRadios2" name="answer_44">
                                    <span class="status">Slightly</span>
                                </label>
                            </li>
				<li class="status4 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="5" id="optionsRadios3" name="answer_44">
                                    <span class="status">Moderately</span>
                                </label>
                            </li>
				<li class="status5 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="6" id="optionsRadios4" name="answer_44">
                                    <span class="status">Important</span>
                                </label>
                            </li>
				<li class="status6 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="7" id="optionsRadios5" name="answer_44">
                                    <span class="status">Very</span>
                                </label>
                            </li>
				<li class="status7 sm"><label class="radio"><span class="icon"></span><span class="icon-to-fade"></span>
                                    <input type="radio" value="8" id="optionsRadios6" name="answer_44">
                                    <span class="status">Extremely</span>
                                </label>
                            </li>                                                        
                            </ul>
                            
                            <ul align="center" class="lstoption mt10"><li>Not at all</li><li>Very Less</li><li>Slightly</li><li>Moderately</li><li>Important</li><li>Very</li><li>Extremely</li></ul>
</div>
</div>

<div id="tmquestions" class="tmquestions hide">
{include file='templates/registration/reloader.tpl'}
</div>
<!-- Questions section End -->
<!--<button class="hide fl mrg oopsbtn" id="back"><span style="font-size:18px;">&laquo;</span>Back</button> -->
</div>
<!-- My Work End    -->
        
        
      
                
   <!-- Questionaries Section end    -->
            <div class="clearfix mt30">
	<div class="oopsleft hide" id="back">Back</div>
	</div>
     
     </div>
     
     
     </div>
     
     
     
  </body>
</html>
