<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Top progress section start -->
    <div class="regprogress">
	<h2>HABITS AND VICES</h2>
	<div class="regscalesec" align="center">
           <div class="regprogbar pbarstep3"></div>
   	 </div>
    <!--<div class="regscalesec"><img src="http://dev.trulymadly.com/trulymadly/templates/new/images/regprogress.png"><div class="regprogbar"></div></div>-->
	</div> 
<!-- Top progress section end -->
<!-- Form section start -->
	<div class="regformsec">
	   	<div class="formreg">
        <p>Smoking</p>
       	<ul>
		<li class="dobform fl"><input class="cutomradio" type="radio" name="smoking_status" value="Never"><label>Never</label></li>
        <li class="dobform padl1 fl"><input class="cutomradio" type="radio" name="smoking_status" value="Socially"><label>Socially</label></li>
        <li class="dobform fr"><input class="cutomradio" type="radio" name="smoking_status" value="Often"><label>Often</label></li>
        </ul>
        <p id='smoking_status_Error' style="color:red;" class="fs11"></p>
        <p>Drinking</p>
        <ul>
		<li class="dobform fl"><input class="cutomradio" type="radio" name="drinking_status" value="Never"><label>Never</label></li>
        <li class="dobform padl1 fl"><input class="cutomradio" type="radio" name="drinking_status" value="Socially"><label>Socially</label></li>
        <li class="dobform fr"><input class="cutomradio" type="radio" name="drinking_status" value="Often"><label>Often</label></li>
        </ul>
        <p id='drinking_status_Error' style="color:red;" class="fs11"></p>
        <p>Eating</p>
        <ul>
		<li class="dobform fl"><input class="cutomradio" type="radio" name="food_status" value="Vegetarian"><label>Veg</label></li>
        <li class="dobform padl1 fl"><input class="cutomradio" type="radio" name="food_status" value="Eggetarian"><label>Eggetarian</label></li>
        <li class="dobform fr"><input class="cutomradio" type="radio" name="food_status" value="Non-Vegetarian"><label>Non-Veg</label></li>
        </ul>
        <p id='food_status_Error' style="color:red;" class="fs11"></p>
       
        </div>
        
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul>
    </div>
<!-- Form section end -->
<div class="regfrmaction">
<a onclick="onBack(this)" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
<a id="activeLifeStyle" onclick="onContinue(this)" class="regbcbtn fr hide">Continue &nbsp;<i class="icon-angle-right"></i></a>
<a id="inactiveLifeStyle" class="diffregbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>
</div>
<!--Centered Container End -->
</div>