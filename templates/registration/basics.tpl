<!DOCTYPE>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Basics</title>

<script>
{literal}
errors = '{"age":"Please specify your birth date","marital":"Please specify your relationship status","children":"Please specify if you have children","marital_spouse":"Please specify the relationship status of your match","age_spouse":"Please specify the age range of your match","age_invalid":"Sorry, invalid age range","tongue":"Please specify your native language","religion":"Please specify your religion","religion_spouse":"Please specify the preferred religion of your match","stay_country":"Please specify country you are staying in...","born_country":"Please specify country in which you were born","born_state":"Please specify the state you are from","born_city":"Please specify the city you are from","stay_state":"Please specify the state you are currently residing in","stay_city":"Please specify the city you are currently residing in","height":"Please specify your height","height_spouse":"Please specify the height range of your match","height_invalid":"Invalid height range chosen","skin_tone":"Please specify your skin tone","skin_tone_spouse":"Please specify skin tone of your spouse","body":"Please specify your appearance","body_spouse":"Please specify body structure of your preffered spouse","smoke":"Please specify your smoking habit","smoke_spouse":"Please specify smoking habit of your preffered spouse","drink":"Please specify your drinking habit","drink_spouse":"Please specify drinking habit of your preffered spouse","food":"Please specify your food preference","food_spouse":"Please specify food habit of your preffered spouse","family_status":"Please specify your family status","family_status_spouse":"Please specify whether you are open to meet someone with the particular family status","income":"Please specify your household income","work":"Please specify your work status","industry":"Please specify your industry","designation":"Please specify your designation","work_spouse":"Please specify working sector of your preffered spouse","education":"Please specify your educational qualification","interest":"Please fill details for at least 4 categories","subjective1":"Please enter at least 20 characters","subjectivecontinue":"Please fill details for at least one question!"}';
errors=JSON.parse(errors);
{/literal}
var edit = false;
var gender='{$data.gender}';
var baseurl='{$baseurl}'; 
var cdnurl = '{$cdnurl}';
var time = new Date();
var time1 = new Date();
var uid = '{$uid}';
{if isset($fb_dob)}
var  fb_dob  = new Array(); 
fb_dob['fb_date']='{$fb_dob.fb_date}';
fb_dob['fb_month']='{$fb_dob.fb_month}';
fb_dob['fb_year']='{$fb_dob.fb_year}';
{/if}
{if isset($institute_details)}
var fb_institute = JSON.parse('{$institute_details.institute}');
{/if}

{if isset($location)}
var fbLoc = '{$location}';
var FBLocation = JSON.parse(fbLoc);
var fb_stay = new Array();
fb_stay['country']=FBLocation['country'];
fb_stay['state']=FBLocation['state'];
fb_stay['city']=FBLocation['city'];
{/if}
//alert(fb_stay['country']['country_id']);
{if isset($work_details)}
var fb_work = new Array();
fb_work['designation']='{$work_details.designation}';
fb_work['current']='{$work_details.current}';
fb_work['companies']=JSON.parse('{$work_details.companies}');
{/if}

{if isset($fb_relationship)}
var relationship_status = '{$fb_relationship}';
{/if}

var verify_connections_linkedin="{$linkedin_minimum_connections}";

</script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
api_key: {$linkedin_api}
onLoad: fetchLinkedIn
</script>



<link href="{$cdnurl}/css/register_new.css?v=6" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css?v=4" rel="stylesheet" type="text/css">

</head>
<body>
{include file='templates/common/header.tpl'}

<div class = "wrapper">
<div class="container basicstepbg hide" id="container">
     {include file='templates/registration/birthplace_new.tpl'}
</div>
<div class="container basicstepbg hide" id="container" >
     {include file='templates/registration/interest_new1.tpl'}
</div>
<div class="container basicstepbg hide" id="container" >
     {include file='templates/registration/work_new.tpl'}
</div>
<div class="container basicstepbg hide" id="container" >
     {include file='templates/registration/education_new.tpl'}
</div>
<div class="container basicstepbg hide" id="container">
       {include file='templates/registration/reloader.tpl'}
</div>

</div>

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/lib/icheck.js"></script>
<script src="{$cdnurl}/js/lib/tm_core_new.js"></script>
<script src="{$cdnurl}/js/lib/function.js?v=47"></script>
<script src="{$cdnurl}/js/register/core_register.js?v=3.06"></script>

{include file='templates/common/footer.tpl'}
</body>
</html>
