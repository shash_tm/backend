    	{if !$edit}
<p class="caption">Let's get to know you from the outside...</p>
{/if}

        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="height">Height</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>    
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
<p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
                        <p class="formlabel">{$allQ.height}</p>
<div class="clearfix heightcount">

                            <p class="selectsearch">

                                <select class="span6 chzn-select" data-placeholder="ft" tabindex="1" id="feet">

                                	<option value=""></option>
{foreach from=json_decode($heightFoot) item=foo}
			{if $foo<10}
                        <option value="0{$foo}">0{$foo}</option>
			{else} <option value="{$foo}">{$foo}</option>
			{/if}
			
{/foreach}


                               </select>

                           </p>

                           <p class="selectsearch ml15">

                                <select class="span6 chzn-select" data-placeholder="in" tabindex="1" id="inch">

                                	<option value=""></option>

<option value="00">00</option>
<option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>

                               </select>

                           </p>
                        </div>
<p id='heightError' style="color:red;" class="fs11 mt5"></p>


                        <!--<div id="slider2" class="mt30 slider"></div>

                        <div class="rangesliderfigure mt20">

                        	<p><input type="text" id="amount2" /><label for="amount2">ft</label></p>

                            <p class="ml10"><input type="number" name="" value="" placeholder="00" id="inch1"><label for="inch1">in</label></p>

                        </div>-->

                    </div>

                </div>

            </div>

            <div class="rt-her">

            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}


                    <div class="formstyle mt10 innerright">
<p class="formlabel">{$allQ.height_spouse}</p>
<!--<span class="clearfix">Should be between</span> -->

                        <div class="clearfix heightcount">

                            <p class="selectsearch">

                                <select class="span6 chzn-select" data-placeholder="ft" tabindex="1" id="feetSpouseStart">

                                	<option value=""></option>
{foreach from=json_decode($heightFoot) item=foo}
                        {if $foo<10}
                        <option value="0{$foo}">0{$foo}</option>
                        {else} <option value="{$foo}">{$foo}</option>
                        {/if}

{/foreach}

                               </select>

                           </p>

                           <p class="selectsearch ml10">

                                <select class="span6 chzn-select" data-placeholder="in" tabindex="1" id='inchSpouseStart'>

                                	<option value=""></option>

<option value="00">00</option>
<option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>

                               </select>

                           </p>

                           <p class="selectsearch mlr5">to</p>

                           <p class="selectsearch">

                               <select class="span6 chzn-select" data-placeholder="ft" tabindex="1" id="feetSpouseEnd">

                               		<option value=""></option>
		{foreach from=json_decode($heightFoot) item=foo}
                        {if $foo<10}
                        <option value="0{$foo}">0{$foo}</option>
                        {else} <option value="{$foo}">{$foo}</option>
                        {/if}

{/foreach}

                               </select>

                           </p>

                           <p class="selectsearch ml10">

                                <select class="span6 chzn-select" data-placeholder="in" tabindex="1" id='inchSpouseEnd'>

                                	<option value=""></option>

<option value="00">00</option>
<option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>

                               </select>

                           </p>

                        </div>	
<p id='heightSpouseError' style="color:red;"  class="fs11 mt5"></p>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="continuert"><a onclick="onContinue2(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">

                <div class="continuert"><a   id="height_save" onclick="onContinue2(0)">Save</a></div>
        </div>
{/if}

