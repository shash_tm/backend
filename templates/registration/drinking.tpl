    	{if !$edit}<p class="caption">Tell us about your habits... </p>{/if}
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="drinking">Drinking</span>
            </div>
        </div>
        <div class="maincontainer">
           	<div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>   
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}

                    <p class="youtext">YOU</p>
                    <div class="mt10 formstyle" >
                        <p class="formlabel">{$allQ.drink}</p>
                         <div class="drnktxt mrg fr" align="center"><ul><li>Often</li><li>Socially</li><li>Never</li></ul></div>
                        <div class="clearfix fr">	
                        <p class="drinkingimage drinkingimage1"></p>
                        <p class="drinkingimage drinkingimage2"></p>
                        <p class="drinkingimage drinkingimage4"></p>
                        <ul class="drinkingstatus">
{foreach from=json_decode($drinkingValues) key=k item=foo}
<li class="dstatus{$k+1} dr">
                                <label class="radio">
                                    <input type="radio" name="drinking_status" id="optionsRadios{$k+1}" value="{$foo}" >
                                    <!--<span class="status">{$foo}</span> -->
                                </label>
                            </li>
{/foreach}
                        </ul>
                        </div>
                        
                    </div>
<p id='drinking_status_Error' class="fs11 errmsg"></p>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle innerright">
                        <p class="formlabel">{$allQ.drink_spouse}</p>
                        <span style="color:red" class="fs11 mt5">(select all that apply)</span>
                        <ul class="formlist matchsmoke">
{foreach from=json_decode($drinkingValues) key=k item=foo}
			<li>
                                <label class="checkbox" for="checkbox{$k+1}_drink">
                                    <input type="checkbox" value="{$foo}" id="checkbox{$k+1}_drink" name='drinking_status_spouse'>
                                    <span class="listitems wd100">{$foo}</span>
                                </label>
                            </li>
{/foreach}
				<li>
                                <label class="checkbox" for="checkbox_drink_notMatter">
                                    <input type="checkbox" value="null" id="checkbox_drink_notMatter" name='drinking_status_spouse_notMatter'>
                                    <span class="listitems wd100">Doesn't Matter</span>
                                </label>
                            </li>
                        </ul>
<p id='drinking_status_spouse_Error' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="backleft"><a onclick="onBack2(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue2(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
                <div class="continuert"><a id="drinking_save"   onclick="onContinue2(3)">Save</a></div>
        </div>
{/if}
