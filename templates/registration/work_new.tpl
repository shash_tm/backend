{if !$edit}
	<script type="text/javascript">
		linkedConnectedFrom = 'workRegister';
	</script>
{else}
	<script type="text/javascript">
		linkedConnectedFrom = 'workLater';
	</script>
{/if}


<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Top progress section start -->

    <div class="regprogress">
	<h2>YOUR BREAD AND BUTTER</h2>
	<div class="regscalesec" align="center">
           <div class="regprogbar pbarstep3"></div>
   	 </div>
    <!--<div class="regscalesec"><img src="http://dev.trulymadly.com/trulymadly/templates/new/images/regprogress.png"><div class="regprogbar"></div></div>-->
	</div>
<!-- Top progress section end -->
<!-- Form section start -->
	<div class="regformsec">
	
<!--	<div class="lnkinsec">                    
<div id='linkedin_loader' style="width:216px; margin:0 auto;" align="center"> 
<p  class='linkdinimport'><a><img src='images/register/linkedin_icon.png' align='absmiddle' class='mr5' width='25'/>IMPORT FROM LINKEDIN</a></p></div>
<div><script type='in/login'></script></div>
<div id='load_linkedin_date' class="hide">
	<img src="images/register/in_tm_loader.gif" width="140" />
	<p>Olla! We're busy importing your info from LinkedIn. This might take a few seconds.</p>
</div>
<p id='error_linkedin' class="fs11" style="color:red"></p>

<br><p class="fs11" style="color:#777;"> We will not post anything on your LinkedIn profile.</p>
</div>
-->

	<!--<a class="regfbconnect"><i class="icon-linkedin-sign"></i>Import from <strong>Linkedin</strong></a>-->
	<!--<div><p  class='linkdinimport'><a><img src='images/register/linkedin_icon.png' align='absmiddle' class='mr5' width='25'/>IMPORT FROM LINKEDIN</a></p></div>-->
	
	<!--<div class="padt1"><script type='in/login'></script></div>-->
	
	<!--<img src="images/register/in_tm_loader.gif" id='load_linkedin_date' class="hide" width="140" />-->
	<!--
	<div id='load_linkedin_date' class="hide">
		<img src="images/register/in_tm_loader.gif" width="140" />
		<p>Olla! We're busy importing your info from LinkedIn. This might take a few seconds.</p>
	</div>
	<div id='error_linkedin' class="inerror"></div>

	<div id="linkmsg"><br><br>We will never post on your LinkedIn.</div>
   	<div class="orline"><span class="ortxt">OR</span></div>
   	-->
    	
    	<div class="formreg" style="margin-top:0px;">
        <p>Industry <span id="industry_optional" style="display:none;">(Optional)</span></p>
        <div class="regselect-style">
        <select name="industry" id="industry">
            <option value="" disabled selected>Select industry</option>
		</select>
        </div>
		<p id='industry_Error' style="color:red" class="fs11"></p>
        
        <p>Current Designation</p>
        <input name="designation" id="designation" type="text" class="regtxtbox" placeholder="Enter designation">
        <!--<div class="nowork"><label><input id="working_status" name="working_area" type="checkbox" value="Not Working">&nbsp;Currently not working</label></div>-->
        <p id='designation_Error' style="color:red" class="fs11"></p>
        
        <p>Companies <span>(Optional)</span></p>
       <div class="comdetails"><a id="addCompany" class="hide"><span class="addicon">Add</span></a><input name="companies" type="text" id="companies" class="regtxtbox inputindex" placeholder="Enter most recent first">
       		<ul id="company"></ul>
       </div>
       
       <p>Income <span>(Optional)</span></p>
        <div class="regselect-style">
        <select name="Income" id="income">
            <option value="" disabled selected>Select income</option>
            <option value="NA">Not Applicable</option>
            <option value="0-10">&lt; 10 lakhs</option>
			<option value="10-20">10-20 lakhs</option>
			<option value="20-50">20-50 lakhs</option>
			<option value="50-51">&gt; 50 lakhs</option>
		</select>
        </div>
		<p id='income_Error' style="color:red" class="fs11"></p>
       
        
        
      <!--  <p>Income</p>
        <select name="Income" class="regddselect" id="income">
            <option value="" disabled selected>Select your income (Personal+Household)</option>
            <option value="0-10">&lt; 10 lakhs</option>
			<option value="10-20">10-20 lakhs</option>
			<option value="20-50">20-50lakhs</option>
			<option value="50-51">&gt; 50 lakhs</option>
		</select>
		<p id='income_Error' style="color:red" class="fs11"></p>
	-->	
       
        </div>
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul> 
    </div>
<!-- Form section end -->
<div class="regfrmaction">
<a onclick="onBack(this)" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
<a id="activeWork" onclick="onContinue(this)" class="regbcbtn fr hide">Continue &nbsp;<i class="icon-angle-right"></i></a>
<a id="inactiveWork" class="diffregbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>
</div>
<!--Centered Container End -->
</div>
