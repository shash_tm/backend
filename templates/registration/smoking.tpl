    	{if !$edit}<p class="caption">Tell us about your habits... </p>
{/if}
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="smoking">Smoking</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>    

                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
                    <p class="youtext">YOU</p>
                    <div class="formstyle " >
                        <p class="smokingimage smokingimage1"><span class="formlabel">{$allQ.smoke}</span></p>
                        <p class="smokingimage smokingimage2"><span class="formlabel">{$allQ.smoke}</span></p>
                        <p class="smokingimage smokingimage4"><span class="formlabel">{$allQ.smoke}</span></p>
                        <ul class="smokingstatus">
		{foreach from=json_decode($smokingValues) key=k item=foo}
				<li class="status{$k+1} sm"><label class="radio">
                                    <input type="radio" name="smoking_status" id="optionsRadios{$k}" value="{$foo}" >
                                    <!--<span class="status">{$foo}</span> -->
                                </label>
                            </li>
{/foreach}	
                        </ul>
                        
           <div class="smkingtxt mrg" align="center"><ul><li>Never</li><li>Socially</li><li>Often</li></ul></div>
                        
			<!--<div id='smo'>
			<p class="nothing mt15">Please Select</p>
			</div>
                        <p class="never mt15">Never</p>
                        <p class="rarely mt15">Rarely</p>
                        <p class="occasionaly mt15">Occasionaly</p>
                        <p class="regularly mt15">Regularly</p> -->
                    </div>
	<p id='smoking_status_Error' style="color:red;" class="fs11"></p>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle innerright">
                        <p class="formlabel">{$allQ.smoke_spouse}</p>
                        <span style="color:red" class="fs11 mt5">(select all that apply)</span>
                        <ul class="formlist matchsmoke">
{foreach from=json_decode($smokingValues)|@array_reverse key=k item=foo}
<li>
                                <label class="checkbox" for="checkbox{$k}_smoke">
                                    <input type="checkbox" value="{$foo}" id="checkbox{$k}_smoke" name='smoking_status_spouse'>
                                    <span class="listitems wd100">{$foo}</span>
                                </label>
                            </li>

{/foreach}
				<li>
                                <label class="checkbox" for="checkbox_smoke_notMatter">
                                    <input type="checkbox" value="null" id="checkbox_smoke_notMatter" name='smoking_status_spouse_notMatter'>
                                    <span class="listitems wd100">Doesn't  Matter</span>
                                </label>
                            </li>

                        </ul>
<p id='smoking_status_spouse_Error' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="backleft"><a onclick="onBack2(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue2(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
     			<div class="continuert"><a   id="smoking_save" onclick="onContinue2(2)">Save</a></div>

        </div>
{/if}

