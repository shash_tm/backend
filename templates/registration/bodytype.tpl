<script type="text/javascript">
var bodyTypes=JSON.parse('{$bodyTypes}');
var bodyTypesOnceSet=0;
</script>

{if !$edit}<p class="caption">Let's get to know you from the outside...</p>
{/if}
        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="vitalstats">Vitalstats</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>   
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
                    <p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
                        <p class="formlabel">{$allQ.body_type} </p>
                        <ul class="formlist mt5">
			{foreach from=json_decode($bodyTypes) key=k item=foo}

		<li><label class="radio">
                                    <input type="radio" name="body_type" id="optionsRadios{$k}" value="{$foo}">
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>

{/foreach}
                        </ul>
<p id='body_type_Error' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}

                    <div class="mt10 formstyle innerright">
                        <p class="formlabel">{$allQ.body_type_spouse}</p>
                        <span class="pink fs12 mt5">(Select Multiple)</span>
                        <ul class="formlist mt5" style="width:95%; margin:0 auto;">
				{foreach from=json_decode($bodyTypes) key=k item=foo}
                        	<li>
                                <label class="checkbox" for="body_{$foo}">
                                    <input type="checkbox" value="{$foo}" id="body_{$foo}" class="body_preference" name="body_type_spouse">
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>
		{/foreach}
                            <li>
                                <label class="checkbox" for="checkbox_body_notMatter">
                                    <input type="checkbox" value="null" id="checkbox_body_notMatter" name="body_type_spouse_notMatter">
                                    <span class="listitems">Doesn't Matter</span>
                                </label>
                            </li>


                        </ul>
<p id='body_type_Spouse_Error' style="color:red;" class="fs11"></p>
                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="backleft"><a onclick="onBack2(this)">Back</a></div>
                <div class="continuert"><a onclick="onContinue2(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
                 <div class="continuert"><a   id="bodytype_save" onclick="onContinue2(1)">Save</a></div>
        </div>
{/if}

