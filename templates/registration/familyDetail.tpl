    	{if !$edit}
<p class="caption">Money isn't everything but it ranks right up there with oxygen!</p>
{/if}

        <div id="progressbar"></div>
        <div class="dividerlines">
        	<div class="centericon">
            	<span class="family">Family</span>
            </div>
        </div>
        <div class="maincontainer">
            <div class="left-you">
            	<div class="innerdiv innerleft">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/men.png"></div>   
                {else}  <div class="youicon"><img src="images/register/women.png"></div> 
                {/if}
                    <p class="youtext">YOU</p>
                    <div class="mt10 formstyle">
                        <p class="formlabel mt10">{$allQ.family_status}</p>
                        <ul class="formlist mt5">
{foreach from=json_decode($familyStatus) key=k item=foo}
                        	<li><label class="radio">
                                    <input type="radio" name="family_status" id="optionsRadios{$k+1}" value="{$foo}">
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>
{/foreach}
                        </ul>
<p id='family_status_Error' style="color:red;" class="fs11"></p>
 <p class="formlabel mt20">{$allQ.income}</p>
                            <p><select class="span6 chzn-select" data-placeholder="Select Yearly Income..." tabindex="1" id='income'>
                                    <option value=""></option>
                                {foreach from=json_decode($incomes) key=k item=foo}
                        <option value='{$k}'>{$foo}</option>
{/foreach}
                                </select>
                            </p>
<p id='income_Error' style="color:red;" class="fs11"></p>
                    </div>

                </div>

            </div>
            <div class="rt-her">
            	<div class="innerdiv youdivider">
{if $data.gender == 'M'}
                    <div class="youicon"><img src="images/register/women.png"></div>    <p class="youtext">HER</p>
                {else}  <div class="youicon"><img src="images/register/men.png"></div> <p class="youtext">HIS</p>
                {/if}
                    <div class="mt10 formstyle innerright">
                        <p class="formlabel mt10">{$allQ.family_status_spouse}</p>
                        <span class="pink fs12 mt5">(Select Multiple)</span>
                        <ul class="formlist mt5" style="margin:0 auto; width:95%;">
			{foreach from=json_decode($familyStatus) key=k item=foo}
                        	<li>
                                <label class="checkbox" for="checkbox{$k+1}_family_spouse">
                                    <input type="checkbox" value="{$foo}" id="checkbox{$k+1}_family_spouse" name='family_status_spouse'/>
                                    <span class="listitems">{$foo}</span>
                                </label>
                            </li>
{/foreach}
                            <li>
                                <label class="checkbox" for="checkbox_notMatter_family_spouse" >
                                    <input type="checkbox" value="null" id="checkbox_notMatter_family_spouse" name='family_status_spouse_notMatter'>
                                    <span class="listitems">Doesn't Matter</span>
                                </label>
                            </li>
                        </ul>
<p id='family_status_spouse_Error' style="color:red;" class="fs11"></p>
<p class="formlabel mt10">{$allQ.income_spouse} <span class="pink fs12">(Optional)</span></p>
<div class="clearfix heightcount ml15">

                            <p class="selectsearch_tm">

                                <select class="span6 chzn-select" data-placeholder="From..." tabindex="1" id="income_spouse_start">

                               </select>

                           </p>
                           <p class="selectsearch_tm ml15">

                                <select class="span6 chzn-select" data-placeholder="To..." tabindex="1" id="income_spouse_end">

                               </select>

                           </p>
                        </div>
<p id='income_spouse_start_Error' style="color:red;" class="fs11"></p>
<p id='income_spouse_end_Error' style="color:red;" class="fs11"></p>

                    </div>
                </div>
            </div>
        </div>
{if !$edit}
        <div class="clearfix mt20">
                <div class="continuert"><a onclick="onContinue3(this)">Continue</a></div>
        </div>
{else}
<div class="clearfix mt20">
<div class="continuert"><a  id="family_save"  onclick="onContinue3(0)">Save</a></div>
        </div>
{/if}

