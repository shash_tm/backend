<!DOCTYPE html>
<html lang="en">
<head>
  <title>Deals List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="{$baseurl}/js/common/jquery.min.js"></script>
	<script src="{$baseurl}/js/common/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{$baseurl}/css/common/jquery.dataTables.css">
</head>
<body>
<input type="hidden" id="event-id" value="{$event_id}" />
<input type="hidden" id="base-url" value="{$baseurl}" />

<script>
$(document).ready(function(){
	var event_id = $('#event-id').val();
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

	if(isAndroid) {
		window.location = 'https://play.google.com/store/apps/details?id=com.trulymadly.android.app';
	}else if(iOS){
		setTimeout(function() {
			window.location = 'https://itunes.apple.com/in/app/trulymadly/id964395424?mt=8';
		}, 1000);
		window.location = 'trulymadly://events/'+event_id;
	}else{
		window.location = $('#base-url').val();
	}
});



</script>
</body>
</html>
