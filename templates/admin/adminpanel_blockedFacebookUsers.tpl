<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Trulymadly - Admin</title>
<link rel="shortcut icon" href="{$baseurl}/images/dashboard/favicon.ico" type="image/x-icon">
<link type="text/css" rel="stylesheet" href="{$baseurl}/css/admin/adminstyle.css">
<link rel="stylesheet" href="{$baseurl}/css/jquery-ui.css">
<meta name="viewport" content="width=device-width" />
<script src="{$baseurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$baseurl}/js/lib/jquery-ui.js"></script>

</head>
<body>

<!--Popup Start -->
 
<div class="messagereply" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            <p>Are you sure you'd like to block/suspend this TrulyMadly member?</p>
            <p style="font-size:12px; padding:0; margin:0 0 10px 0;">Why would you like to report this TrulyMadly member?</p>
<input name="usrblock" type="radio" id = "1" value="Has sent abusive email/message" class="fl mrg" /> Has sent abusive email/message<br />

<input name="usrblock" type="radio" id = "7" value="usrblock7" class="fl mrg" />Other<br />
<textarea cols="30" id ="textArea" rows="1" placeholder="To help us safeguard you, please tell us why you would like to block this TrulyMadly member." class="txtareabox" ></textarea>
        </form>
        <div class="btnactions">
        	<p> <input type="button" value="Block" onclick = "blockfun(id)" class="fl mr10 suspension block">
        	<input type="button" value="suspend" onclick = "suspendfun(id)" class="fl mr10 suspension suspend"> 
        	<a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.messagereply').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>


<!--Popup End -->


<!-- Popup for edit name start -->

<div class="editname" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            Fname : <input name="fname" type="text" id = "pop_up_fname" /><br />
			Lname : <input name="lname" type="text" id = "pop_up_lname" /><br />
 		</form>
     	<div class="btnactions">
        	<p> <input type="button" value="Save"  onclick = "saveName(id)" class="fl mr10 suspension"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.editname').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>

<!-- Popup for edit name end -->
 
<!-- Popup for edit Phone Number start -->

<div class="editphoneNumber" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" > 
        <form id = "blockform" name="myForm">
            Phone Number : <input name="phoneNumber" type="text" id = "3" value=""/><br />
 		</form>  
     	<div class="btnactions">
        	<p> <input type="button" value="Save"  onclick = "savephoneNumber(id)" class="fl mr10 suspension"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.editphoneNumber').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>

<!-- Popup for edit Phone Number  END -->


<div class="container">
<!--<div class="admintabs"> 
<a href="adminpanel.php?type=1" class="tabactive">Verify Id Proofs</a>
<a href="adminpanel.php?type=2">Verify User Images</a>
<a href="adminpanel.php?type=3">Verify Address Proofs</a>
<a href="adminpanel.php?type=4">Verify Employment Proofs</a></div> -->



<div class="pagin">
{if $page_num eq 1}
  {if  $smarty.session.privileges eq null or in_array('search', $smarty.session.privileges)  }
<label class="fl" style="padding:3px 5px 0 0;">Sort By :</label> 
  <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}" class="fl">
    <select name="sortBy" id="select" class="fl" style=" margin:2px; width:150px;">  
      
        <option value="photos-under-moderation">Photos Under Moderation</option> 
            <option value="photos-under-moderation-authm">Photos Under Moderation Authentic Male</option>
            <option value="photos-under-moderation-authf">Photos Under Moderation Authentic Female</option>
            <option value="photos-approved">Photos Approved</option>
            <option value="photos-all-rejected">Photos All Rejected</option>
            <option value="employment-under-moderation">Employment Under Moderation</option>  
            <option value="employment-rejected">Employment Not Clear</option>  
            <option value="employment-fail">Employment Rejected</option>  
            <option value="employment-active">Employment Approved</option> 
            <option value="employment-verification">Employment Sent for Verification</option>
            <option value="employment-pending-verification">Employment to be Sent for Verification</option>
            <option value="photoIds-under-moderation">Photo Ids Under Moderation</option>  
            <option value="photoIds-rejected">Photo Ids Not Clear</option>  
            <option value="photoIds-fail">Photo Ids Rejected</option>  
            <option value="photoIds-active">Photo Ids Approved</option>  
            <option value="facebook">Facebook</option>     
            <option value="linkedin">Linkedin</option>  
            <option value="female-profiles">Female Profiles</option>
            <option value="abused-profiles">Abuse-Reported Profiles</option>
            <option value="females_users_under_review">Female Users under Review</option>
   <!--
            <option value="male-profiles">Male Profiles</option>
            <option value="address-under-moderation">Address Under Moderation</option>  
            <option value="address-rejected">Address Not Clear</option>  
            <option value="address-fail">Address Rejected</option>  
            <option value="address-active">Address Approved</option>    
            <option value="address-verification">Address Sent for Verification</option>
            <option value="address-pending-verification">Address to be Sent for Verification</option>
            
    --></select>
<input type="submit" value="Submit" class="fl">
  </form>
   
   
    <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=UserId" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">Search By UserId:</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
   
   <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?sortBy=day" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">History(enter no. of days):</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
    
    <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=email_id" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">Search By Email_Id:</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
   <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=name" class="fl">
     <label class="fl" style="padding:3px 5px 0 3px;">First Name:</label> <input type="text" name="fname" class="fl" style=" width:120px;">
     <label class="fl" style="padding:3px 5px 0 3px;">Last Name:</label> <input type="text" name="lname" class="fl" style=" width:120px;">
   <input type="submit" value="Search By Name" class="fl">
   </form>
   
  <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=email_id_blocked" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">Search By Blocked Facebook email_id:</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
   
    {/if}
      <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?sortBy=moderation" class="fl">
      <label class="fl" style="padding:3px 5px 0 20px;">Search by Moderation :</label>
    <select name="modType" id="select" class="fl" style=" margin:2px; width:150px;"> 
      {if  $smarty.session.privileges eq null or in_array('photo_male', $smarty.session.privileges)  }
            <option value="photos-male">Photos Male</option>
       {/if}
         {if  $smarty.session.privileges eq null or in_array('photo_female', $smarty.session.privileges)  }
            <option value="photos-female">Photos  Female</option>
          {/if}
            {if  $smarty.session.privileges eq null or in_array('photo_id', $smarty.session.privileges)  }
            <option value="photo-ids">Photo Ids</option> 
            {/if}
        </select>
<input type="submit" value="Submit" class="fl">
  </form>
   
   
  {/if}
  
<ul>

{if isset($sortBy)}
  {if $page_num neq 1}
    {if $page_num neq 2}
        {if isset($value)}
             <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy={$sortBy}&value={$value}">{$page_num-2}</a></li>
       {else}
             <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy={$sortBy}">{$page_num-2}</a></li>
       {/if}
    {/if}
    {if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy={$sortBy}&value={$value}">{$page_num-1}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy={$sortBy}">{$page_num-1}</a></li>
	{/if}
  {/if}


  {if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy={$sortBy}&value={$value}">{$page_num}</a></li>
  {else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy={$sortBy}">{$page_num}</a></li>
  {/if}


  {if isset($attributes)}
    {if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy={$sortBy}&value={$value}">{$page_num+1}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy={$sortBy}">{$page_num+1}</a></li>
	{/if}

	{if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy={$sortBy}">{$page_num+2}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy={$sortBy}">{$page_num+2}</a></li>
	{/if}
 {/if}

{else}
{if isset($search_by)} 
	{if $page_num neq 1}
		{if $page_num neq 2}
		     {if isset($fname) || isset($lname)}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&search_by={$search_by}&fname={$fname}&lname={$lname}">{$page_num-2}</a></li>
            {else}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&search_by={$search_by}&value={$value}">{$page_num-2}</a></li>
            {/if}

	{/if}
	   {if isset($fname) || isset($lname)}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&search_by={$search_by}&fname={$fname}&lname={$lname}">{$page_num-1}</a></li>
            {else}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&search_by={$search_by}&value={$value}">{$page_num-1}</a></li>
            {/if}
 	{/if}
      {if isset($fname) || isset($lname)}
        <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&search_by={$search_by}&fname={$fname}&lname={$lname}">{$page_num}</a></li>
    {else}
    {if isset ($value)}
         <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&search_by={$search_by}&value={$value}">{$page_num}</a></li>
     {/if}
    {/if}
	{if isset($attributes)}
		 <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&search_by={$search_by}&value={$value}">{$page_num+1}</a></li>
		 <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&search_by={$search_by}&value={$value}">{$page_num+2}</a></li>
	{/if}
  {/if}
{/if}
</ul>
</div>
<!-- Profile info sec start -->

<br><br><br><b>FIDs blocked for this email_id : {$attributes.email_id}</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
{foreach from=$attributes.fids  key=k item=v}
<tr>
<td align="center">Fid:</td> 
<td align="center"><a href="http://www.facebook.com/{$v}">{$v}</a></td>
<td align="center" style="display: block; text-align: center; margin: auto"><input type="button" value="unblock" id="userUnblock{$v}" onclick = "unblock('{$v}')" class="fr"></td>
</tr>
{/foreach}
</table>


<script>

function unblock(id){
	$.post("adminpanel.php",{
		action:'unblockFid',
		fid:id,  
	},function(data){ 
		if(data.status=='Success'){ 
			$("#userUnblock"+id).hide();
			var alert_msg = "Fid unblocked Successfully ";
			alert(alert_msg);		
		} 		   
	},'json');  
}
</script>
</body>
 
</html>
