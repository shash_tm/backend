<html>
<head>
<meta http-equiv="Pragma" content="no-cache">
<title>TrulyMadly Admin</title>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/admin/adminstyle.css">
</head>
<body>
<div class="container">
<div align="center" class="insideframe" style="width:300px;">
<h2>Admin Login</h2>
<form method="post">

{if isset($error_login)}
<p class="err">
You may have entered an invalid user ID
or password. To correct the problem, please enter
your correct user ID and password. If you have
forgotten your user ID or password, please contact
the server administrator.
</p>
{/if}
<p>Please enter user ID and password:</p> 

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="fs12">
<tr>
<td width="35%" align="right"><strong>User ID</strong></td>
<td><input type="text" size="20" name="user"></td>
</tr>
<tr>
<td align="right"><strong>Password</strong></td>
<td><input type="password" size="20" name="pass"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><input type="submit" name="signup" value="Login"></td>
</tr>
</table>

</form>
</div>
</div>
</body>
</html>