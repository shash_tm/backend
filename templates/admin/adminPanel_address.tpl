<!DOCTYPE>
<html>
<head>
<meta charset="utf-8" />
<title>Trulymadly</title>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
    <link type="text/css" rel="stylesheet" href="{$admin_baseurl}/css/admin/adminstyle.css">
<meta name="viewport" content="width=device-width" />
<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$admin_baseurl}/js/admin/adminCore.js?v=2"></script>
</head>
<body>

<!--<a href="adminpanel.php?type=1">Verify Id Proofs</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=2">Verify User Images</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=3">Verify Address Proofs</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=4">Verify Employment Proofs</a>
-->
{foreach from=$proofs item=proof}
<div id="id_proof_{$proof.user_id}" class="insideframe">
{if ($proof.img_flag eq 'yes')}
<img src = '{$imageurl}{$proof.img_location}' width="400" align="middle" class="fl"/>
{else}
<a href='{$imageurl}{$proof.img_location}' target="_blank">Download File to view</a>
{/if}
<ul class="fl ml20">
<li><strong>Document:</strong> {$proof.proof_type}</li>
<li><strong>First Name:</strong> {$proof.fname}</li>
<li><strong>Last Name</strong> {$proof.lname}</li>
<li><strong>User ID:</strong>{$proof.user_id}</li>
<li><strong>Mobile No:</strong> {$proof.user_number}</li>
<li><strong>Mobile Status:</strong> {$proof.status}</li>
{if $proof.action_time neq "0000-00-00"}
<li><strong>Current Action Time:</strong> {$proof.action_time}</li>
{/if} 
<li><strong>No. of Attempts:</strong> {$proof.number_of_trials}</li>

{if $proof.sent_for_verification eq 0}
<li>
<label><strong>AuthBridge Ref Number:</strong></label><input type="text" value="" id="auth_{$proof.user_id}"/>
</li>
<li id="authbridge_{$proof.user_id}">
<input type="button" value="authbridge" onClick="sendForVerify('{$proof.user_id}', 'address')" />&nbsp;&nbsp;
</li>
{else}
<li><strong>Sent for Verification on {$proof.verification_time}</strong></li>
<li><strong>verification_tracking_number is {$proof.ref_no}</strong></li>
{/if}
{if $proof.is_paid eq 'Y'}
<li><strong>Paid : Yes</strong></li>
{else}
<li id = "paid_{$proof.user_id}"><strong>Paid : No</strong></li>
<input id="paid123" type="button" value="Paid" onClick="isPaid('{$proof.user_id}','address')" />&nbsp;&nbsp;
{/if}

<li id="button_{$proof.user_id}">
{if $proof.proof_status eq 'under_moderation'}
<input type="button" value="Approve" onClick="verifyAddress('active','{$proof.user_id}')" />&nbsp;&nbsp;
<!--<input type="button" value="Not Clear" onClick="verifyAddress('rejected','{$proof.user_id}')"/>&nbsp;&nbsp;-->
<input type="button" value="Not Clear" onClick="showpopup('{$proof.user_id}')"/>&nbsp;&nbsp;
<input type="button" value="Reject" onClick="verifyAddress('fail','{$proof.user_id}')"/>
{else}
<strong>Address Proof Status: {if $proof.proof_status eq 'rejected'}Not Clear{else}{$proof.proof_status}{/if}</strong>
{/if}
</li>

</ul>


<div style="display: none;">Address : {$proof.address}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City: {$proof.city}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
State : {$proof.state}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pincode: {$proof.pincode}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Landmark : {$proof.landmark}<br></div>

</div>
{foreachelse}
No more proofs to be verified
{/foreach}

<!--Popup start -->
<div class="messagereply" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            <p>Are you sure you'd like to Reject this Address Proof?</p>
            <p style="font-size:12px; padding:0; margin:0 0 10px 0;">Why would you like to reject this Address Proof?</p>
<input name="idreject" type="radio" id = "1" value="not_clear" class="fl mrg" /> Document not clear or invalid document<br />
<input name="idreject" type="radio" id = "2" value="city_mismatch" class="fl mrg" /> City Mismatch<br />
<input name="idreject" type="radio" id = "3" value="name_mismatch" class="fl mrg" /> Name mismatch (Some other person's doc uploaded e.g. Landlord or parents)<br />
<input name="idreject" type="radio" id = "4" value=" password_required" class="fl mrg" /> Password protected file<br />
        </form>
        <div class="btnactions">
        	<p> <input type="button" value="Reject"  onclick = "rejectAddress(id)" class="fl mr10 rejectid"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.messagereply').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>
    
<!--Popup End --> 

</body>
</html>