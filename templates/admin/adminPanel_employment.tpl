<!DOCTYPE>
<html>
<head>
<meta charset="utf-8" />
<title>Trulymadly</title>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
    <link type="text/css" rel="stylesheet" href="{$admin_baseurl}/css/admin/adminstyle.css">
<meta name="viewport" content="width=device-width" />
<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$admin_baseurl}/js/admin/adminCore.js?v=3"></script>
<script src="{$admin_baseurl}/js/admin/imageRotate.js"></script>
</head>
<body>

<!--<a href="adminpanel.php?type=1">Verify Id Proofs</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=2">Verify User Images</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=3">Verify Address Proofs</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=4">Verify Employment Proofs</a>-->

{foreach from=$proofs key=i item=proof}
<div id="id_proof_{$proof.user_id}"  class="insideframe">
{if $i eq 0}
{if ($proof.img_flag eq 'yes')}
<img id='image_{$proof.user_id}' src = '{$imageurl}{$proof.img_location}' width="400" align="middle" class="fl"/>
{else}
<a href='{$imageurl}{$proof.img_location}' >Download File to view</a>
{/if}
<ul class="fl ml20">
<li><h2><strong>Status: </strong> {$proof.proof_status}</h2></li>

{if $proof.admin_name eq null}
<li><strong>By user:</strong>
 {if $proof.password eq null}
 New Document Uploded
 {else}
 Password Entered
 {/if}
</li>
{else}
<li><strong>By Admin:</strong> {$proof.admin_name} </li>
{/if}


<li><strong>Document:</strong> {$proof.proof_type}</li>
<li><strong>First Name:</strong> {$proof.fname}</li>
<li><strong>Last Name</strong> {$proof.lname}</li>
<li><strong>User ID:</strong> {$proof.user_id}</li>
<li><strong>Password:</strong> {$proof.password}</li>
<li><strong>Sync with Id:</strong> {$proof.mismatch_allow}</li>
{if $proof.tstamp neq "0000-00-00 00:00:00"}
<li><strong>Current Action Time:</strong> {$proof.tstamp}</li>
{/if}
<li><strong>No. of Attempts:</strong> {$proof.number_of_trials}</li>

{if $proof.sent_for_verification eq 0}

<li>
<label><strong>AuthBridge Ref Number:</strong></label><input type="text" value="" id="auth_{$proof.user_id}"/>
</li>
<li id="authbridge_{$proof.user_id}">
<input type="button" value="authbridge" onClick="sendForVerify('{$proof.user_id}', 'employment')" />&nbsp;&nbsp;
</li>

{else}
<li><strong>Sent for Verification on {$proof.verification_time}</strong></li>
<li><strong>verification_tracking_number is {$proof.ref_no}</strong></li>
{/if}
{if $proof.is_paid eq 'Y'}
<li><strong>Paid : Yes</strong></li>
{else}
<li id = "paid_{$proof.user_id}"><strong>Paid : No</strong></li>
<input id="paid123" type="button" value="Paid" onClick="isPaid('{$proof.user_id}','employment')" />&nbsp;&nbsp;
{/if}
<li id="button_{$proof.user_id}">
{if $proof.proof_status eq 'under_moderation'}
<input type="button" value="Approve" onClick="verifyEmployment('active','{$proof.user_id}')" />&nbsp;&nbsp;
<!--<input type="button" value="Not Clear" onClick="verifyEmployment('rejected','{$proof.user_id}')"/>&nbsp;&nbsp;-->
<input type="button" value="Not Clear" onClick="showpopup('{$proof.user_id}')"/>&nbsp;&nbsp;
<input type="button" value="Reject" onClick="verifyEmployment('fail','{$proof.user_id}')"/>
<input type="button" value="rotate" onClick="rotatePhoto('image_{$proof.user_id}')"/>
{/if}
{if $proof.proof_status eq 'rejected'}
<li><strong>Reject reason: </strong> {$proof.reject_reason}</li>
{/if}
</li>  
</ul>
{else}
<a href='{$imageurl}{$proof.img_location}' >Download File to view</a>
<ul class="fl ml20">
<li><h2><strong>Status: </strong> {$proof.proof_status}</h2></li>
{if $proof.admin_name eq null}
<li><strong>By user :</strong>
 {if $proof.password eq null}
 New Document Uploded
 {else}
 Password Entered
 {/if}
</li>
{else}
<li><strong>By Admin :</strong> {$proof.admin_name} </li>
{/if}
<li><strong>Document:</strong> {$proof.proof_type}</li>
<li><strong>First Name:</strong> {$proof.fname}</li>
<li><strong>Last Name</strong> {$proof.lname}</li>
<li><strong>User ID:</strong> {$proof.user_id}</li>
<li><strong>Password:</strong> {$proof.password}</li>
<li><strong>Sync with Id:</strong> {$proof.mismatch_allow}</li>
</ul>
<ul class="fl ml20">
<li><h2><strong>              </strong> </h2></li>
{if $proof.tstamp neq "0000-00-00 00:00:00"}
<li><strong>Current Action Time:</strong> {$proof.tstamp}</li>
{/if}
<li><strong>No. of Attempts:</strong> {$proof.number_of_trials}</li>
{if $proof.sent_for_verification neq 0}
<li><strong>Sent for Verification on {$proof.verification_time}</strong></li>
<li><strong>verification_tracking_number is {$proof.ref_no}</strong></li>
{/if}
{if $proof.is_paid eq 'Y'}
<li><strong>Paid : Yes</strong></li>
{else}
<li id = "paid_{$proof.user_id}"><strong>Paid : No</strong></li>
{/if}
{if   $proof.proof_status eq 'rejected'} 
 <li><strong>Reject reason: </strong> {$proof.reject_reason}</li>

{/if}
<li id="button_{$proof.user_id}">

</li>  
</ul>






{/if}


</div>

{foreachelse}
No more proofs to be verified
{/foreach}

<!--Popup start -->
<div class="messagereply" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            <p>Are you sure you'd like to set this Employment Proof as Not clear?</p>
            <p style="font-size:12px; padding:0; margin:0 0 10px 0;">Why would you like to reject this Employmnet Proof?</p>
<input name="idreject" type="radio" id = "1" value="not_clear" class="fl mrg" /> Document not clear or invalid document<br />
<input name="idreject" type="radio" id = "2" value="password_required" class="fl mrg" /> Password protected file<br />
<!--<input name="idreject" type="radio" id = "3" value="name_mismatch" class="fl mrg" /> Name mismatch <br /> -->

  
       </form>
       
            <div class="btnactions"> 
        	<p> <input type="button" value="Not Clear"  onclick = "rejectEmployment(id)" class="fl mr10 rejectid"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.messagereply').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>
    
<!--Popup End --> 

</body>
</html>