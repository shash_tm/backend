<!doctype html>
<html>
<head> 
<meta charset="UTF-8">
<title>TrulyMadly Admin panel Moderation</title>
<link href="{$admin_baseurl}/css/admin/adminstyle.css?v=1.1 " rel="stylesheet" type="text/css">
<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$admin_baseurl}/js/admin/adminCore.js?v=4.26"></script>
<script src="{$admin_baseurl}/js/admin/imageRotate.js"></script>
</head>

<body> 
 <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?sortBy=moderation" class="fl">
      <label class="fl" style="padding:3px 5px 0 0;">Search by Moderation :</label>
    <select name="modType" id="select" class="fl" style=" margin:2px; width:150px;"> 
           {if  $smarty.session.privileges eq null or in_array('photo_male', $smarty.session.privileges)  or $smarty.session.super_admin eq 'yes' }
               <option value="photos-male">Photos Male</option>
               <option value="photos-male-us">Photos Male US</option>
             {/if}
           {if  $smarty.session.privileges eq null or in_array('photo_female', $smarty.session.privileges) or $smarty.session.super_admin eq 'yes' }
            <option value="photos-female">Photos  Female</option>
            <option value="photos-female-us">Photos  Female US</option>
                        {/if}
             {if  $smarty.session.privileges eq null or in_array('photo_id', $smarty.session.privileges)  or $smarty.session.super_admin eq 'yes' }
            <option value="photo-ids">Photo Ids</option> 
            <option value="photo-ids-us">Photo Ids US</option>
            {/if}
        </select>
<input type="submit" value="Submit" class="fl">
  </form>
  
   <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?sortBy=moderation" class="fl">
    <label class="fl" style="padding:3px 5px 0 20px;">Search By UserId:</label> <input type="text" name="user_id" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
   
     
 <div class="clb">
 <ul class="pagination">
 <li> <a href="{$admin_baseurl}/admin/adminpanel.php"> &laquo; Back to Admin</a></li> 

   {if $page_num neq 1}
    {if $page_num neq 2}
        {if isset($modType)}
             <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy=moderation&modType={$modType}">{$page_num-2}</a></li>
       {else}
             <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy=moderation">{$page_num-2}</a></li>
       {/if}
    {/if}
    {if isset($modType)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy=moderation&modType={$modType}">{$page_num-1}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy=moderation">{$page_num-1}</a></li>
	{/if}
  {/if}


  {if isset($modType)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy=moderation&modType={$modType}">{$page_num}</a></li>
  {else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy=moderation">{$page_num}</a></li>
  {/if}


  {if isset($attributes)}
    {if isset($modType)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy=moderation&modType={$modType}">{$page_num+1}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy=moderation">{$page_num+1}</a></li>
	{/if}

	{if isset($modType)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy=moderation&modType={$modType}">{$page_num+2}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy=moderation">{$page_num+2}</a></li>
	{/if}
 {/if} 
   </ul> 
 <ul id="remaining_button" style="display: inline;"> <button type="button"  onclick = "getRemainingModerationData()">Show Remaining</button></ul>
 <ul id="remaining_data" style="display: none;" class="pagedetails">
 <li>Photos_Male</li>
<li>Photos_Female</li>
<li>Photo_Ids</li>
</ul>
</div>
 {foreach from=$attributes.name  key=k item=v}
<div class="container">
<!-- Moderation Section start -->
<div class="wrapper">
<!-- Profile info start -->
<div class="proinfo">
<ul>

<li class="fl">Profile ID: {$v.user_id} </li> 
<li class="fl">Name:  {$v.fname}  {$v.lname}</li>  
<li class="fl" bgcolor="#f1f1f1"><a href="https://www.facebook.com/{$v.fid}" target="_blank">Facebook Profile </a></li>
<li class="fl">Gender:  {$v.gender}</li>
<li class="fl">DOB:  {$v.DOB}</li>
<li class="fl">Age:  {$v.age}</li>
<li class="fl">Location:  {$v.city}, {$v.state}</li>
<li class="fr">Registered On:  {$v.registered_at}</li>
<li class="fr">Profile Status: {$v.status}</li>
<li class="fr">Colleges : {$v.institute_details}</li>
<li class="fr">Companies : {$v.company_name}</li>
<li class="fr">Designation: {$v.designation}</li>
{if $attributes.id[$k].password neq null} <li class="fr">Password: {$attributes.id[$k].password}</li> {/if}
</ul>
</div>
<!-- Profile info end -->
<!-- Profile Photos Section start -->
<div class="prophotosec">
<div class="fl" style="border-right:1px dashed #999;">
<!-- USER PHOTOS :<br> -->
<ul class="photopannel">
<li class="fl titlephoto"><p>Facebook Photo</p></li>
<li class="fl titlephoto"><p style="color:#ed0c6e;">Under Moderation Photos</p></li>
<li class="fl titlephoto"><p style="color:#00a651;">Approved Photos</p></li>
<li class="fl titlephoto"><p style="color:#ff0000;">Rejected Photos</p></li>
</ul>
{if  $smarty.session.privileges eq null or in_array('photo_female', $smarty.session.privileges) or in_array('photo_male', $smarty.session.privileges)  or $smarty.session.super_admin eq 'yes'  }
<ul class="photopannel">
<!-- FB profile photo Section -->
<li class="fl"><img src="{$attributes.fb_photo[$k]}" width="200">
</li>
<!-- FB profile photo Section end -->
<!-- Under Moderation Section -->
<li id=undermod_{$v.user_id} class="fl bggray scrollsec">
{if $attributes.photo[$k].under_moderation eq null}
<div class="fl"><img src="" width="200">
</div>
{else}
{foreach from=$attributes.photo[$k].under_moderation  key=r item=p}
<div id="button_{$p.photo_id}" class="photoedit">
<ul>

<a href="javascript:void(0);" title="Approve" onClick="verifyPhoto('active','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}','{$p.mapped_to}')"><li class="fl approve brdrleft"></li></a>
<a href="javascript:void(0);" title="Approve without profile" onClick="verifyPhoto('active_no_profile','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}','{$p.mapped_to}')"><li class="fl noproaprov"></li></a>

<a href="javascript:void(0);" title="Rotate" id="rotate_{$p.photo_id}"  onClick="rotatePhoto('image_{$p.photo_id}')"><li class="fl rotate"></li></a>
<a href="javascript:void(0);" title="Save" id="savePhoto_{$p.photo_id}"  onClick="saveRotatedImage('{$p.photo_id}','{$p.user_id}')"><li class="fl save"></li></a>


<a href="javascript:void(0);" title="Reject" onClick="reject_visible('{$p.photo_id}')"><li class="fl reject"></li></a>

<li id="outer_drop_down_reject_{$p.photo_id}" style="display: none; background: none; float: left;">
<select id="drop_down_reject_{$p.photo_id}" style=" margin:2px; width:170px;">
  <option value="Unclear/Face not visible">Unclear/Face not visible</option>
  <option value="Celebrity">Celebrity</option>
  <option value="Fake">Fake</option>
  <option value="Married">Married</option>
  <option value="Non Human">Non Human</option>
  <option value="Vulgar">Vulgar</option>
  <option value="Wrong Gender">Wrong Gender</option>
  <option value="Others">Others</option>
</select>
<button type="button" onClick="verifyPhoto('rejected','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}','{$p.mapped_to}')"><small>Reject</small></button>
</li>



</ul>
</div>

<div id="imagediv_{$p.photo_id}" class="photoedit">
{if $p.is_profile eq 'yes'}
<div class="primephoto"><img src="../images/usericon.png"></div>
{/if}
<img id="image_{$p.photo_id}" src="{$imageurl}{$p.name}">
</div>
{/foreach}  
{/if}

</li>
<!-- Under Moderation Section end -->

<!-- Approved Photo Section -->
<li id=approved_{$v.user_id} class="fl bggray scrollsec">
{if $attributes.photo[$k].approved eq null}
<div id="no_approved_{$v.user_id}" class="fl"><img src="" width="200">
</div>
{else}
{foreach from=$attributes.photo[$k].approved  key=r item=p}
<div id="button_{$p.photo_id}" class="photoedit">
<ul>

<!-- <a href="javascript:void(0);" title="Approve" onClick="verifyPhoto('active','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}')"><li class="fl approve brdrleft"></li></a> -->
<a href="javascript:void(0);" title="Reject" onClick="verifyPhoto('rejected','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}')"><li class="fl reject"></li></a>
<a href="javascript:void(0);" title="Rotate" id="rotate_{$p.photo_id}"  onClick="rotatePhoto('image_{$p.photo_id}')"><li class="fl rotate"></li></a>
<a href="javascript:void(0);" title="Save" id="savePhoto_{$p.photo_id}"  onClick="saveRotatedImage('{$p.photo_id}','{$p.user_id}')"><li class="fl save"></li></a>
<!-- <a href="javascript:void(0);" title="Approve without profile" onClick="verifyPhoto('active_no_profile','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}')"><li class="fl noproaprov"></li></a>  -->
  
</ul>
</div>
<div id="imagediv_{$p.photo_id}" class="photoedit">
{if $p.is_profile eq 'yes'}
<div class="primephoto"><img src="../images/usericon.png"></div>
{/if}
{if $p.can_profile eq 'no'}
<div class="usrphoto"><img src="../images/usernoprofile.png"></div>
{/if}

<img id="image_{$p.photo_id}" src="{$imageurl}{$p.name}">
</div>
{/foreach} 
{/if}
</li>
<!-- Approved Photo Section end -->

<!-- Rejected Photo Section -->
<li id="rejected_{$v.user_id}" class="fl bggray scrollsec">
{if $attributes.photo[$k].rejected eq null}
<div id="no_rejected_{$v.user_id}" class="fl"><img src="" width="200"> 
</div>
{else}
{foreach from=$attributes.photo[$k].rejected  key=r item=p}
<div id="button_{$p.photo_id}" class="photoedit">
<ul>

<a href="javascript:void(0);" title="Approve" onClick="verifyPhoto('active','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}')"><li class="fl approve brdrleft"></li></a>
<a href="javascript:void(0);" title="Approve without profile" onClick="verifyPhoto('active_no_profile','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}')"><li class="fl noproaprov"></li></a>
<!-- <a href="javascript:void(0);" title="Reject" onClick="verifyPhoto('rejected','{$p.photo_id}', '{$p.user_id}','{$p.is_profile}')"><li class="fl reject"></li></a> -->
<a href="javascript:void(0);" title="Rotate" id="rotate_{$p.photo_id}"  onClick="rotatePhoto('image_{$p.photo_id}')"><li class="fl rotate"></li></a>
<a href="javascript:void(0);" title="Save" id="savePhoto_{$p.photo_id}"  onClick="saveRotatedImage('{$p.photo_id}','{$p.user_id}')"><li class="fl save"></li></a>

 
</ul>
</div>
<div id="imagediv_{$p.photo_id}" class="photoedit">
<img id="image_{$p.photo_id}" src="{$imageurl}{$p.name}">
</div>
{/foreach}
{/if} 
</li>
<!-- Rejected Photo Section end -->

</ul>
{/if}
</div>

<div class="fr">
<ul class="photopannel">
<li class="fl titlephoto"><p style="color:#000;">Photo ID</p></li>
</ul>
<ul class="photopannel">
<!-- ID Under Moderation Section -->
<li class="fl bggray">
{if $attributes.id[$k] neq null}
 {if $smarty.session.privileges eq null or  in_array('photo_id', $smarty.session.privileges)  } 
 
{if $attributes.id[$k].status eq 'under_moderation'}
<div class="photoedit fl" id="buttonId_{$attributes.id[$k].user_id}"> 
<ul>
<a href="javascript:void(0);" title="Approve" onClick="verifyId('active','{$attributes.id[$k].user_id}')"><li class="fl approve brdrleft"></li></a>
<!--<a href="javascript:void(0);" title="Reject" onClick="verifyId('rejected','{$attributes.id[$k].user_id}')"><li class="fl reject"></li></a>-->
<a href="javascript:void(0)" title="Rotate" title="Rotate" id="rotate_{$attributes.id[$k].user_id}"  onClick="rotatePhoto('image_{$attributes.id[$k].user_id}')"><li class="fl rotate"></li></a>
<!-- <a href="#" title="Save"><li class="fl save"></li></a> --> 
<!-- <a title="Not Clear" onClick="document.getElementById('notclear').style.display='block';"><li class="fl reason"></li></a> -->
<a title="Not Clear" onClick="showpopup('{$attributes.id[$k].user_id}')"><li class="fl reason"></li></a>
</ul>
</div>
{/if}
{/if}



<div class="photoedit">


<!-- Not Clear Reasons -->
<div class="notclear" id="notclear" style="display:none;">
<br>
Are you sure you'd like to set this Photo ID as Not clear?<br><br>
Why would you like to reject this Photo ID:<br><br>

<label><input type="radio" name="radio1" id="radio1" value="radio1"> Radio Button File Password Protected</label>
<label><input type="radio" name="radio1" id="radio1" value="radio1"> Document not clear or invalid document</label>
<label><input type="radio" name="radio1" id="radio1" value="radio1"> Name Mismatch</label>
<label><input type="radio" name="radio1" id="radio1" value="radio1"> Age Mismatch</label>
<label><input type="radio" name="radio1" id="radio1" value="radio1"> Name and Age Mismatch</label>

<!-- <input type="button" value="Not clear"  onclick = "rejectId(id)" class="fl mr10 rejectid"> --> 
<input type="submit" name="submit" id="submit" value="Submit" class="sbmtbtn" onClick="document.getElementById('notclear').style.display='none';">&nbsp;&nbsp;<a onClick="document.getElementById('notclear').style.display='none';">Cancel</a>
</div>
<!-- Not clear reason end -->


{if ($attributes.id[$k].img_flag eq 'yes')}
<img id='image_{$attributes.id[$k].user_id}' src= "{$imageurl}{$attributes.id[$k].img_location}"> 
{else}
<a href='{$imageurl}{$attributes.id[$k].img_location}'  target="_blank">Download File to view</a> 
{/if}

</div>
{/if}
</li>
<!-- ID Under Moderation Section end -->
</ul>

</div>

</div>
<!-- Profile Photos Section end -->
</div>
<!-- Moderation Section end -->
</div>

{/foreach}
 
<div class="messagereply" style="display:none;">
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            <p>Are you sure you'd like to set this Photo ID as Not clear?</p>
            <p style="font-size:12px; padding:0; margin:0 0 10px 0;">Why would you like to reject this Photo ID?</p>
<input name="idreject" type="radio" id = "1" value="password_required" class="fl mrg" /> File Password Protected<br />
<input name="idreject" type="radio" id = "2" value="not_clear" class="fl mrg" /> Document not clear or invalid document<br />
<input name="idreject" type="radio" id = "3" value="name_mismatch" class="fl mrg" /> Name Mismatch<br />
<div id="name_mismatch" style="display:none; width:100%;  overflow:hidden;">  
    <div class="sync_name" style="float:left;" >
            Fname : <input name="fname" type="text" id = "fname" value=""/>
			Lname : <input name="lname" type="text" id = "lname" value=""/>
 		   </div>
 		   <div class="btnactions" style="float:left;">
        	 <input type="button" value="Save"  onclick = "rejectId(id)" class="fl mr10 rejectid">
           </div>
</div>
<input name="idreject" type="radio" id = "4" value="age_mismatch" class="fl mrg" /> Age Mismatch<br />
<div id="age_mismatch" style="display:none; width:100%; overflow:hidden;">  
      	<div class="sync_age" style="float:left;">
      	      Date ( YYYY-MM-DD): <input name="date" type="text"  id="dob" value=""/>
      	 </div>
      <div class="btnactions"  style="float:left;">
        	<input type="button" value="Save" id="btsubmit"  class="fl mr10 rejectid">
           </div>
 </div> 
<input name="idreject" type="radio" id = "5" value="name_age_mismatch" class="fl mrg" /> Name and Age Mismatch<br />
  <div id="name_age_mismatch" style="display:none; width:100%; overflow:hidden;">  
       Fname : <input name="fname1" type="text" id = "fname1" value=""/>
	   Lname : <input name="lname1" type="text" id = "lname1" value=""/>
   <p>Date (YYYY-MM-DD): <input name="date1" type="text" id="dob1"></p>  
    <div class="btnactions" style="float:left;">
        	<input type="button" value="Save" id="btsubmit1"  class="fl mr10 rejectid">
           </div>
         </div>
     </form>
        <div id="reject_button" class="btnactions">
        	<p> <input type="button" value="Not clear"  onclick = "rejectId(id)" class="fl mr10 rejectid"></p>
        	</div>
        	<div class="btnactions"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.messagereply').css('display','none')">Cancel</a>
        </div>
    </div>
    </div>
</body>
</html>
