<!DOCTYPE>
<html>
<head>
<meta charset="utf-8" />
<title>Trulymadly</title>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<link type="text/css" rel="stylesheet" href="{$admin_baseurl}/css/admin/adminstyle.css">
<meta name="viewport" content="width=device-width" />
<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$admin_baseurl}/js/admin/adminCore.js?v=4.4"></script> 
<script src="{$admin_baseurl}/js/admin/imageRotate.js"></script>
</head>
<body>
<!--<a href="adminpanel.php?type=1">Verify Id Proofs</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=2">Verify User Images</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=3">Verify Address Proofs</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=4">Verify Employment Proofs</a>
-->
{foreach from=$proofs key=i item=proof}
<div id="id_proof_{$proof.user_id}" class="insideframe">
{if $i eq 0}
{if ($proof.img_flag eq 'yes')}
<img id='image_{$proof.user_id}' src = '{$imageurl}{$proof.img_location}' width="400" align="middle"  class="fl"/>
{else}
<a href='{$imageurl}{$proof.img_location}' >Download File to view</a>
{/if}
<ul class="fl ml20">
<li><h2><strong>Status:</strong> {if $proof.proof_status eq 'rejected'}Not Clear{else} {$proof.proof_status}{/if}</h2></li>

{if $proof.admin_name eq null}
<li><strong>By user:</strong>
{if $proof.name_age_mismatch_allow eq 'user done the changes'}
 User made the changes
 {else}
 {if $proof.password eq null  && $proof.name_age_mismatch_allow eq null }
 New Document Uploded
 {else} 
 Password Entered
 {/if}{/if}
</li>
{else}
<li><strong>By Admin:</strong> {$proof.admin_name} </li>
{/if}

<li><strong>Document:</strong> {$proof.proof_type}</li>
<li><strong>First Name:</strong> {$proof.fname}</li>
<li><strong>Last Name:</strong> {$proof.lname}</li>
<li><strong>User ID:</strong> {$proof.user_id}</li>
<li><strong>No. of Attempts:</strong> {$proof.number_of_trials}</li>
<li><strong>Upload Time :</strong> {$proof.tstamp}</li>
<li><strong>Password :</strong> {$proof.password}</li>
<li><strong>Name or age sync with Id  :</strong> {$proof.name_age_mismatch_allow}</li>


<li id="buttonId_{$proof.user_id}">
{if $proof.proof_status eq 'under_moderation'}

<input type="button" value="Approve" onClick="verifyId('active','{$proof.user_id}')" />&nbsp;&nbsp;
<!--<input type="button" value="Not Clear" onClick="verifyId('rejected','{$proof.user_id}')"/>&nbsp;&nbsp;-->
<input type="button" value="Not Clear" onClick="showpopup('{$proof.user_id}')"/>&nbsp;&nbsp;
<input type="button" value="Reject" onClick="verifyId('fail','{$proof.user_id}')"/>
<input type="button" value="rotate" onClick="rotatePhoto('image_{$proof.user_id}')"/>
{/if}
</li>
</ul>
{else}
<img id='image_{$proof.user_id}' src = '{$imageurl}{$proof.img_location}' width="400" align="middle"  class="fl"/>
<ul class="fl ml20">
<li><h2><strong>Status:</strong> {if $proof.proof_status eq 'rejected'}Not Clear{else} {$proof.proof_status}{/if}</h2></li>
{if $proof.admin_name eq null}
<li><strong>By user:</strong>
 {if $proof.password eq null}
 New Document Uploded
 {else}
 Password Entered
 {/if}
</li>
{else}
<li><strong>By Admin:</strong> {$proof.admin_name} </li>
{/if}
<li><strong>Document:</strong> {$proof.proof_type}</li>
<li><strong>First Name:</strong> {$proof.fname}</li>
<li><strong>Last Name:</strong> {$proof.lname}</li>
<li><strong>User ID:</strong> {$proof.user_id}</li>
</ul>
<ul class="fl ml20">
<li><strong>              </strong> </li>
<li><strong>No. of Attempts:</strong> {$proof.number_of_trials}</li>
<li><strong>Upload Time :</strong> {$proof.tstamp}</li>
<li><strong>Password :</strong> {$proof.password}</li>
<li><strong>Name or age sync with Id  :</strong> {$proof.name_age_mismatch_allow}</li>



</ul>
{/if}
</div>
{foreachelse}
No more proofs to be verified
{/foreach}

<!--Popup start -->
<div class="messagereply" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            <p>Are you sure you'd like to set this Photo ID as Not clear?</p>
            <p style="font-size:12px; padding:0; margin:0 0 10px 0;">Why would you like to reject this Photo ID?</p>
<input name="idreject" type="radio" id = "1" value="password_required" class="fl mrg" /> File Password Protected<br />
<input name="idreject" type="radio" id = "2" value="not_clear" class="fl mrg" /> Document not clear or invalid document<br />
<input name="idreject" type="radio" id = "3" value="name_mismatch" class="fl mrg" /> Name Mismatch<br />
<div id="name_mismatch" style="display:none; width:100%;  overflow:hidden;">  
    <div class="sync_name" style="float:left;" >
            Fname : <input name="fname" type="text" id = "fname" value=""/>
			Lname : <input name="lname" type="text" id = "lname" value=""/>
 		   </div>
 		   <div class="btnactions" style="float:left;">
        	 <input type="button" value="Save"  onclick = "rejectId(id)" class="fl mr10 rejectid"></a>
           </div>
</div>
<input name="idreject" type="radio" id = "4" value="age_mismatch" class="fl mrg" /> Age Mismatch<br />
<div id="age_mismatch" style="display:none; width:100%; overflow:hidden;">  
      	<div class="sync_age" style="float:left;">
      	      Date ( YYYY-MM-DD): <input name="date" type="text"  id="dob" value=""/>
      	 </div>
      <div class="btnactions"  style="float:left;">
        	<input type="button" value="Save" id="btsubmit"  class="fl mr10 rejectid"></a>
           </div>
 </div>
<input name="idreject" type="radio" id = "5" value="name_age_mismatch" class="fl mrg" /> Name and Age Mismatch<br />
  <div id="name_age_mismatch" style="display:none; width:100%; overflow:hidden;">  
       Fname : <input name="fname1" type="text" id = "fname1" value=""/>
	   Lname : <input name="lname1" type="text" id = "lname1" value=""/>
   <p>Date (YYYY-MM-DD): <input name="date1" type="text" id="dob1"></p>  
    <div class="btnactions style="float:left;">
        	<input type="button" value="Save" id="btsubmit1"  class="fl mr10 rejectid"></a></p>
           </div>
         </div>
     </form>
        <div id="reject_button" class="btnactions">
        	<p> <input type="button" value="Not clear"  onclick = "rejectId(id)" class="fl mr10 rejectid">
        	</div>
        	<div class="btnactions"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.messagereply').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>
      
<!--Popup End -->    

</body>
</html>