<!DOCTYPE>
<html>
<head>

<meta charset="utf-8" />
<title>Trulymadly</title>

<link type="text/css" rel="stylesheet" href="{$admin_baseurl}/css/admin/adminstyle.css">
<meta name="viewport" content="width=device-width" />

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$admin_baseurl}/js/admin/adminCore.js?v=4.3"></script> 

</head>
<body>
{if $blocklist|@count>0}
  <table width="1050" border="1" cellspacing="5" cellpadding="5" align="center">
		<tr>
    		<th>User Id</th>
    		<th>First Name</th> 
    		<th>Block Reason</th> 
    		<th>Block Time</th>
    		<th>Option</th>
    		<th>Option 2</th>
  		</tr
	{foreach $blocklist as $user }
    	<tr>
    	 <td>{$user.user_id}</td>
  	 	 <td><a href={$user.link_to_conversation} target=_blank>{$user.fname}</a></td>
  	 	 <td>{$user.reason}</td>
  	 	 <td>{$user.block_time}</td>
  	 	 <td align="center">
  	 	 	{if $user_id eq $user.blocked_by}
  	 	 		<input type="button" value="unblock" id="block_{$user.user_id}" onclick="unblock({$user_id},{$user.user_id})">
  	 	 	{/if}
  	 	 </td>
  	 	 <td align="center">
  	 	 	{if ($user.reason eq "Inappropriate or offensive content" || $user.reason eq "Inappropriate content" || $user.reason eq "Offensive content") && $user_id eq $user.blocked_by }
  	 	 		<input type="button" value="Inoffensive" id="inoffensive_{$user.user_id}" onclick="inoffensive({$user_id},{$user.user_id})">
  	 	 	{/if}
  	 	 </td>
  	 	</tr>
	{/foreach}
  </table>
{else} Block List Empty !!!  
{/if} 

</body> 

<script>

 function unblock(id1,id2){       
	$.post("adminpanel.php",{
		action:'unblock',
		user_id1 :id1,
		user_id2 : id2
	},function(data){
		if(data.status=='Success'){
			var alert_msg = "Unblocked Successfully !!!\nNumber of Unblocked allowed : "+data.no_changes_allowed;
			alert(alert_msg);
		}else if(data.status='limit exceeded') {
		alert("Limit already exceeded !!!");
		}
	},'json');
 	//$.get("adminpanel.php", { type:5 ,todo :'unblock' ,user_id1 :id1 ,user_id2 :id2});   
 	$("#block_"+id2).hide();
 }

function inoffensive(id1,id2){       
	$.post("adminpanel.php",{
		action:'inoffensive',
		user_id1 :id1,
		user_id2 :id2
	},function(data){
		if(data.status=='Success'){
			var alert_msg = "Deemed inoffensive Successfully !!!";
			alert(alert_msg);
		}
	},'json');
     	
 	$("#inoffensive_"+id2).hide();
 }


</script>

</html>