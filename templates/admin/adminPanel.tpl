<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Trulymadly - Admin</title>
<link rel="shortcut icon" href="{$baseurl}/images/dashboard/favicon.ico" type="image/x-icon">
<link type="text/css" rel="stylesheet" href="{$baseurl}/css/admin/adminstyle.css">
<link rel="stylesheet" href="{$baseurl}/css/jquery-ui.css">
<meta name="viewport" content="width=device-width" />
<script src="{$baseurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$baseurl}/js/lib/jquery-ui.js"></script>

</head>
<body>

<!--Popup Start -->
 
<div class="messagereply" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            <p>Are you sure you'd like to block/suspend this TrulyMadly member?</p>
            <input name="block_device" type="checkbox" id = "block_all_devices" class="fl mrg" />Block All users from this device<br />
            <p style="font-size:12px; padding:0; margin:0 0 10px 0;">Why would you like to report this TrulyMadly member?</p>
<input name="usrblock" type="radio" id = "1" value="Has sent abusive email/message" class="fl mrg" /> Has sent abusive email/message<br />

<input name="usrblock" type="radio" id = "7" value="usrblock7" class="fl mrg" />Other<br />
<textarea cols="30" id ="textArea" rows="1" placeholder="To help us safeguard you, please tell us why you would like to block this TrulyMadly member." class="txtareabox" ></textarea>
        </form>
        <div class="btnactions">
        	<p> <input type="button" value="Block" onclick = "blockfun(id)" class="fl mr10 suspension block">
        	{*<input type="button" value="suspend" onclick = "suspendfun(id)" class="fl mr10 suspension suspend"> *}
        	<a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.messagereply').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>


<!--Popup End -->


<!-- Popup for edit name start -->

<div class="editname" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" >
        <form id = "blockform" name="myForm">
            Fname : <input name="fname" type="text" id = "pop_up_fname" /><br />
			Lname : <input name="lname" type="text" id = "pop_up_lname" /><br />
 		</form>
     	<div class="btnactions">
        	<p> <input type="button" value="Save"  onclick = "saveName(id)" class="fl mr10 suspension"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.editname').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>

<!-- Popup for edit name end -->
 
<!-- Popup for edit Phone Number start -->

<div class="editphoneNumber" style="display:none;" >
<div class="difusescreen"></div>
    <div class="sndmpopupframe" > 
        <form id = "blockform" name="myForm">
            Phone Number : <input name="phoneNumber" type="text" id = "3" value=""/><br />
 		</form>  
     	<div class="btnactions">
        	<p> <input type="button" value="Save"  onclick = "savephoneNumber(id)" class="fl mr10 suspension"><a href="javascript:void(0);" class="fl fs12 mt5" onClick="$('.editphoneNumber').css('display','none')">Cancel</a></p>
        </div>
    </div>
    </div>

<!-- Popup for edit Phone Number  END -->


<div class="container">
<!--<div class="admintabs"> 
<a href="adminpanel.php?type=1" class="tabactive">Verify Id Proofs</a>
<a href="adminpanel.php?type=2">Verify User Images</a>
<a href="adminpanel.php?type=3">Verify Address Proofs</a>
<a href="adminpanel.php?type=4">Verify Employment Proofs</a></div> -->



<div class="pagin">
{if $page_num eq 1}
  {if  $smarty.session.privileges eq null or in_array('search', $smarty.session.privileges)  or $smarty.session.super_admin eq 'yes' }
<label class="fl" style="padding:3px 5px 0 0;">Sort By :</label> 
  <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}" class="fl">
    <select name="sortBy" id="select" class="fl" style=" margin:2px; width:150px;">  
      
        <option value="photos-under-moderation">Photos Under Moderation</option> 
            <option value="photos-under-moderation-authm">Photos Under Moderation Authentic Male</option>
            <option value="photos-under-moderation-authf">Photos Under Moderation Authentic Female</option>
            <option value="photos-approved">Photos Approved</option>
            <option value="photos-all-rejected">Photos All Rejected</option>
            <option value="employment-under-moderation">Employment Under Moderation</option>  
            <option value="employment-rejected">Employment Not Clear</option>  
            <option value="employment-fail">Employment Rejected</option>  
            <option value="employment-active">Employment Approved</option> 
            <option value="employment-verification">Employment Sent for Verification</option>
            <option value="employment-pending-verification">Employment to be Sent for Verification</option>
            <option value="photoIds-under-moderation">Photo Ids Under Moderation</option>  
            <option value="photoIds-rejected">Photo Ids Not Clear</option>  
            <option value="photoIds-fail">Photo Ids Rejected</option>  
            <option value="photoIds-active">Photo Ids Approved</option>  
            <option value="facebook">Facebook</option>     
            <option value="linkedin">Linkedin</option>  
            <option value="female-profiles">Female Profiles</option>
            <option value="abused-profiles">Abuse-Reported Profiles</option>
            <option value="females_users_under_review">Female Users under Review</option>
   <!--
            <option value="male-profiles">Male Profiles</option>
            <option value="address-under-moderation">Address Under Moderation</option>  
            <option value="address-rejected">Address Not Clear</option>  
            <option value="address-fail">Address Rejected</option>  
            <option value="address-active">Address Approved</option>    
            <option value="address-verification">Address Sent for Verification</option>
            <option value="address-pending-verification">Address to be Sent for Verification</option>
            
    --></select>
<input type="submit" value="Submit" class="fl">
  </form>
   
   
    <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=UserId" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">Search By UserId:</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
   
   <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?sortBy=day" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">History(enter no. of days):</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
    
    <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=email_id" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">Search By Email_Id:</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
   <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=name" class="fl">
     <label class="fl" style="padding:3px 5px 0 3px;">First Name:</label> <input type="text" name="fname" class="fl" style=" width:120px;">
     <label class="fl" style="padding:3px 5px 0 3px;">Last Name:</label> <input type="text" name="lname" class="fl" style=" width:120px;">
   <input type="submit" value="Search By Name" class="fl">
   </form>
   
  <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?search_by=email_id_blocked" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">Search By Blocked Facebook email_id:</label> <input type="text" name="value" class="fl" style=" width:120px;">
   <input type="submit" value="Submit" class="fl">
   </form>
   
    {/if}
      <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?sortBy=moderation" class="fl">
      <label class="fl" style="padding:3px 5px 0 20px;">Search by Moderation :</label>
    <select name="modType" id="select" class="fl" style=" margin:2px; width:150px;"> 
      {if  $smarty.session.privileges eq null or in_array('photo_male', $smarty.session.privileges)  or $smarty.session.super_admin eq 'yes'  }
            <option value="photos-male">Photos Male</option>
       {/if}
         {if  $smarty.session.privileges eq null or in_array('photo_female', $smarty.session.privileges)  or $smarty.session.super_admin eq 'yes'  }
            <option value="photos-female">Photos  Female</option>
          {/if}
            {if  $smarty.session.privileges eq null or in_array('photo_id', $smarty.session.privileges)  or $smarty.session.super_admin eq 'yes' }
            <option value="photo-ids">Photo Ids</option> 
            {/if}
        </select>
<input type="submit" value="Submit" class="fl">
  </form>

  {/if}

    {if $smarty.session.super_admin eq 'yes'}
        <form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?type=6" class="fl">
            <label class="fl" style="padding:3px 5px 10px 20px;">Search By Blocked Device Id:</label> <input type="text" name="device_id" class="fl" style=" width:120px;">
            <input type="submit" value="Submit" class="fl" target="_blank">
        </form>
    {/if}
  
<ul>

{if isset($sortBy)}
  {if $page_num neq 1}
    {if $page_num neq 2}
        {if isset($value)}
             <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy={$sortBy}&value={$value}">{$page_num-2}</a></li>
       {else}
             <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy={$sortBy}">{$page_num-2}</a></li>
       {/if}
    {/if}
    {if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy={$sortBy}&value={$value}">{$page_num-1}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy={$sortBy}">{$page_num-1}</a></li>
	{/if}
  {/if}


  {if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy={$sortBy}&value={$value}">{$page_num}</a></li>
  {else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy={$sortBy}">{$page_num}</a></li>
  {/if}


  {if isset($attributes)}
    {if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy={$sortBy}&value={$value}">{$page_num+1}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy={$sortBy}">{$page_num+1}</a></li>
	{/if}

	{if isset($value)}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy={$sortBy}">{$page_num+2}</a></li>
	{else}
		<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy={$sortBy}">{$page_num+2}</a></li>
	{/if}
 {/if}

{else}
{if isset($search_by)} 
	{if $page_num neq 1}
		{if $page_num neq 2}
		     {if isset($fname) || isset($lname)}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&search_by={$search_by}&fname={$fname}&lname={$lname}">{$page_num-2}</a></li>
            {else}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&search_by={$search_by}&value={$value}">{$page_num-2}</a></li>
            {/if}

	{/if}
	   {if isset($fname) || isset($lname)}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&search_by={$search_by}&fname={$fname}&lname={$lname}">{$page_num-1}</a></li>
            {else}
               <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&search_by={$search_by}&value={$value}">{$page_num-1}</a></li>
            {/if}
 	{/if}
      {if isset($fname) || isset($lname)}
        <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&search_by={$search_by}&fname={$fname}&lname={$lname}">{$page_num}</a></li>
    {else}
    {if isset ($value)}
         <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&search_by={$search_by}&value={$value}">{$page_num}</a></li>
     {/if}
    {/if}
	{if isset($attributes)}
		 <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&search_by={$search_by}&value={$value}">{$page_num+1}</a></li>
		 <li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&search_by={$search_by}&value={$value}">{$page_num+2}</a></li>
	{/if}
  {/if}
{/if}
</ul>
</div>
<!-- Profile info sec start -->
{foreach from=$attributes.name  key=k item=v}
<div align="center" class="insideframe">
<div class="fl mrg bdrr pad5">

<h3 class="fl uInfo">User Info</h3>
{if ($v.status neq 'suspended' && $v.status neq 'blocked' && $v.deletion_status eq null) && $smarty.session.super_admin eq 'yes' && $under_review neq 'true'}
<!--
<a href="#" class="btnact fr mr10" id="susp_{$k}" onClick="sushide(id)">Suspend</a>
-->
<!--<form name="form5" id="form_{$k}"  class="fr">
 <input type="button" value="Suspend"  id="susp_{$k}" onclick="sushide(id)"  class="fr"> 
<select name="reason" id="select" class="fr" style=" margin:2px 10px 0 0; width:130px;">
<option value="" selected>Select a Reason </option>
<option value="abc">Find that special someone with India's modern matchmakers.</option>
<option value="xyz">xyz</option>
<option value="123">123</option>
<option value="other" id="other_{$k}" onselect= "selectForm(id)">other</option>
</select>

</form>
--> 
 <input type="button" value="Suspend/Block"  id="susp_{$k}" onclick = "showpopup(id)" class="fr"> 
{/if}


{if $under_review eq 'true' && $smarty.session.super_admin eq 'yes'}
<input type="button" value="NOT AN ESCORT"  id="userClear{$k}" onclick = "clear_femaleUnderReview({$k})" class="fr">
{/if}


{if $under_review eq 'true' && $smarty.session.super_admin eq 'yes'}
<input type="button" value="ESCORT"  id="userBlock{$k}"  onclick = "block_femaleUnderReview({$k})" class="fr" >
{/if}

{if $v.status eq 'suspended' && $smarty.session.super_admin eq 'yes' && $v.deletion_status eq NULL }
<input type="button" value="Restore"  id="restore_{$k}" onclick = "restoreState({$k})" class="fr">
{/if}

{if $v.status eq 'blocked' && $smarty.session.super_admin eq 'yes'&& $v.deletion_status eq NULL}
<input type="button" value="Restore"  id="restore_{$k}" onclick = "restoreState({$k})" class="fr">
{/if}  

<div class="clb"></div>
<table width="300" border="0" cellspacing="2" cellpadding="5">
  <tr>
    <td width="100" bgcolor="#f1f1f1">Profile ID</td>
    <td bgcolor="#f1f1f1"><strong>{$k}</strong></td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Name</td> 
    <td bgcolor="#f1f1f1">
    {if $smarty.session.super_admin eq 'yes'}
    	<strong><input type="text" id="name_{$k}" value="{$v.fname} {$v.lname}" readonly style="border-width:0px; border:none;"></strong>
       <a href="javascript:void(0);" onclick="showeditname({$k},'{$v.fname}','{$v.lname}')"><img src="{$admin_baseurl}/admin/images/edit.png" width="10" height="10" style="margin-left:75px;"/></a>
    {else}
    {$v.fname} {$v.lname}
    {/if} 
    </td>
  </tr>
  <tr>
  </tr>
    <td bgcolor="#f1f1f1">Last Login</td>
    <td bgcolor="#f1f1f1">{$v.last_l}</td>
  </tr>
  <tr>
    <td width="100" bgcolor="#f1f1f1">Registeration Date</td>
    <td bgcolor="#f1f1f1"><strong>{$v.registered_at}</strong></td>
  </tr>
    <td bgcolor="#f1f1f1">Birthdate</td>  
    <td bgcolor="#f1f1f1">
    {if $smarty.session.super_admin eq 'yes'}
    <strong><input type="text" id="bday_{$k}" value="{$attributes.data[$k].age}" onclick="show_calendar(this)" name="bdate" style="border-width:0px; border:none;" ></strong>
	<input type="button" value="Update" id="bday_button{$k}" onclick = "saveDOB({$k},'{$attributes.data[$k].age}')" class="fl mr10 suspension"><input type="button" value="Reset" id="bday_reset_button{$k}" onclick = "reset({$k},'{$attributes.data[$k].age}')" class="fl mr10 suspension">
	{else}
	{$attributes.data[$k].age}
	{/if}
    </td>
  </tr> 
  <tr> 
    <td bgcolor="#f1f1f1">Gender</td> 
    <td bgcolor="#f1f1f1">
     
    {if $smarty.session.super_admin eq 'yes'}
    	<strong><input type="text" id="gender_{$k}" value="{$v.gender}" readonly style="border-width:0px; border:none;"></strong>
    	<select id="sex_{$k}" >
    		<option value="M">Male</option>
    		<option value="F">Female</option>
    	</select>
       <a href="javascript:void(0);" onclick="saveGender({$k})">&nbsp UPDATE</a>
    {else}
    	{$v.gender}
    {/if}
    </td>
  </tr>
    <tr>
        <td bgcolor="#f1f1f1">Device Id</td>
        <td bgcolor="#f1f1f1">{$v.device_id} </td>
    </tr>
  <tr>
    <td bgcolor="#f1f1f1">Location</td>
    <td bgcolor="#f1f1f1">{$attributes.data[$k].city}, {$attributes.data[$k].state}, {$attributes.data[$k].country}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Industry</td>
    <td bgcolor="#f1f1f1">{$attributes.data[$k].industry}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Designation</td>
    <td bgcolor="#f1f1f1">{$attributes.data[$k].designation}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Company</td>
    <td bgcolor="#f1f1f1">{$attributes.data[$k].company}</td>
  </tr>
   {*<tr>*}
    {*<td bgcolor="#f1f1f1" >Deletion Status</td>*}
    {*<td bgcolor="#f1f1f1" id="deletion_status_{$k}">{$v.deletion_status}</td>*}
  {*</tr>*}
  <tr>
    <td bgcolor="#f1f1f1" >Status</td>
      {if $v.status eq 'deleted' || $v.status eq 'mark_deleted'}
          <td bgcolor="#B92B27" id="status_{$k}">{$v.status}</td>
       {else}
          <td bgcolor="#f1f1f1" id="status_{$k}">{$v.status}</td>
      {/if}
  </tr>
    <tr>
    <td bgcolor="#f1f1f1">Email</td> 
    <td bgcolor="#f1f1f1">{$v.email_id}</td>
  </tr>
  </tr>
   <tr>
    <td bgcolor="#f1f1f1">Phone number</td>
    <td id="PhoneButton_{$k}" style="display:inline;" bgcolor="#f1f1f1"><button type="button" onClick="getPhoneNumber({$k})">Show</button>
    <small>  {if {$attributes.phones[$k].status} eq 'active' } (Verified){/if} 
    </small>
    </td>
    <td id="PhoneData_{$k}" style="display:none;" bgcolor="#f1f1f1">
	    	<strong><input type="text" id="phoneNumber_{$k}" value="{$attributes.phones[$k].phone}" readonly style="border-width:0px; border:none;"></strong>
	    	<a href="javascript:void(0);" onclick="showeditphoneNumber({$k},phoneNumber_{$k}.value)"><img src="{$admin_baseurl}/admin/images/edit.png" width="10" height="10" style="margin-left:75px;"/></a>
             <small id="phoneStatus_{$k}"> (Verified)</small>
    
    </td>
  </tr> 
   
  <tr> 
    <td bgcolor="#f1f1f1">Source</td>  
    <td bgcolor="#f1f1f1">{$v.registered_from} </td>
  </tr> 
  
  <tr>
    <td bgcolor="#f1f1f1" >Version</td>
    <td bgcolor="#f1f1f1" >{$attributes.versions[$k].version}</td> 
  </tr>
   
  <tr>
    <td bgcolor="#f1f1f1">User Photo</td>
    <td bgcolor="#f1f1f1"><a href="{$admin_baseurl}/admin/adminpanel.php?type=2&user_id={$k}" target="_blank" class="fb">User Photographs:<br>
&bull;&nbsp;&nbsp;&nbsp;{if isset($attributes.photos_count[$k]['under_moderation'])}{$attributes.photos_count[$k]['under_moderation']}{else}0{/if} Under Moderation<br>
&bull;&nbsp;&nbsp;&nbsp;{if isset($attributes.photos_count[$k]['rejected'])}{$attributes.photos_count[$k]['rejected']} {else}0{/if} Rejected<br>
&bull;&nbsp;&nbsp;&nbsp;{if isset($attributes.photos_count[$k]['approved'])}{$attributes.photos_count[$k]['approved']}{else}0{/if} Approved</a></td>
  </tr>
</table>

</div>

<div class="fl mrg bdrr pad5"> 
{if $v.fid neq null}
<h3 bgcolor="#f1f1f1"><a href="https://www.facebook.com/{$v.fid}" target="_blank" class="fb">Facebook</a></h3>
{else}
<h3>Facebook</h3>
{/if}
<table width="250" border="0" cellspacing="2" cellpadding="5">
  <tr> 
    <td width="100" bgcolor="#f1f1f1">FB Profile Pic</td>
    <td bgcolor="#f1f1f1"><img src="{$attributes.fpic[$k]}" width="100" height="100"></td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Name</td>
    <td bgcolor="#f1f1f1">{$attributes.fb[$k].fname} {$attributes.fb[$k].lname}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Birthday</td>
    <td bgcolor="#f1f1f1">{$attributes.fb[$k].birthday}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Gender</td>
    <td bgcolor="#f1f1f1">{$attributes.fb[$k].gender}</td>
  </tr>
  <tr>
  <td bgcolor="#f1f1f1">Connected at</td>
    <td bgcolor="#f1f1f1">{$attributes.fb[$k].connected_from}</td>
  </tr>
  <tr>
  <td bgcolor="#f1f1f1">Relationship Status</td>
    <td bgcolor="#f1f1f1">{$attributes.fb[$k].relationship}</td>
  </tr>
   <tr>
  <td bgcolor="#f1f1f1">Connections</td>
    <td bgcolor="#f1f1f1">{$attributes.fb[$k].connection}</td>
  </tr>
  <!--
  <tr>
    <td bgcolor="#f1f1f1">Loaction</td>
    <td bgcolor="#f1f1f1">abc</td>  
  </tr>
  
--></table>

</div>
<div class="fl mrg bdrr pad5">
<h3>Linkedin</h3>
<table width="250" border="0" cellspacing="2" cellpadding="5"><!--
  <tr>
    <td width="100" bgcolor="#f1f1f1">In Profile Pic</td>
    <td bgcolor="#f1f1f1"><img src="images/dashboard/graydot.gif" width="100" height="100"></td>
  </tr>
  --><tr>
    <td bgcolor="#f1f1f1">Name</td>
    <td bgcolor="#f1f1f1">{$attributes.linkedin[$k].name}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Connected at</td>
    <td bgcolor="#f1f1f1">{$attributes.linkedin[$k].connected_from}</td>
  </tr>
  <!--
  <tr>
    <td bgcolor="#f1f1f1">Industry</td>
    <td bgcolor="#f1f1f1">abc</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Designation</td>
    <td bgcolor="#f1f1f1">abc</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Company</td>
    <td bgcolor="#f1f1f1">abc</td>
  </tr>
  
--></table>
<h3>Engagement</h3>
    <p>For  <input type="text" name="day_count" width="10" id="day_count_{$k}"> days </p>
<button type="button" id="engagement_button_{$k}" style="display: inline;" onclick = "show_engagement({$k})">Show Data</button>

<table id="engagement_matrix_{$k}" style="display: none;" width="250" border="0" cellspacing="2" cellpadding="5">
  <tr>
    <td bgcolor="#f1f1f1">Recommendations</td>
    <td bgcolor="#f1f1f1">{$attributes.engage[$k].recs}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Likes</td>
    <td bgcolor="#f1f1f1">{$attributes.engage[$k].likes}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Hides</td>
    <td bgcolor="#f1f1f1">{$attributes.engage[$k].hides}</td>
  </tr>
  <tr>
    <td bgcolor="#f1f1f1">Mutual Likes</td>
    <td bgcolor="#f1f1f1">{$attributes.mutual[$k].mutual}</td>
  </tr>
  
</table>
    <h3>Spark Stats</h3>
    <button type="button" id="sparks_{$k}" style="display: inline;" onclick = "show_sparks({$k})">Show Sparks Data</button>
    <table id="spark_matrix_{$k}" style="display: none;" width="250" border="0" cellspacing="2" cellpadding="5">
        <tr>
            <td bgcolor="#f1f1f1">Package Type</td>
            <td bgcolor="#f1f1f1"></td>
        </tr>
        <tr>
            <td bgcolor="#f1f1f1">Price</td>
            <td bgcolor="#f1f1f1"></td>
        </tr>
        <tr>
            <td bgcolor="#f1f1f1">Sparks Sent</td>
            <td bgcolor="#f1f1f1"></td>
        </tr>
        <tr>
            <td bgcolor="#f1f1f1">Sparks accepted</td>
            <td bgcolor="#f1f1f1"></td>
        </tr>
        <tr>
            <td bgcolor="#f1f1f1">Sparks Remaining</td>
            <td bgcolor="#f1f1f1"></td>
        </tr>

    </table>
</div>




    <div class="fl mrg pad5">
<h3>Photo ID</h3>
<table width="150" border="0" cellspacing="2" cellpadding="5">
  <tr>
        <td bgcolor="#f1f1f1"><a href="{$admin_baseurl}/admin/adminpanel.php?type=1&user_id={$k}" target="_blank" class="fb">Photo ID Proof<br>
({if isset($attributes.photoIds[$k].status)}
{if $attributes.photoIds[$k].status eq 'fail'} Rejected 
{else if $attributes.photoIds[$k].status eq 'rejected'} Not Clear 
{else if $attributes.photoIds[$k].status eq 'active'} Approved 
{else if $attributes.photoIds[$k].status eq 'under_moderation'} Under Moderation
{/if}
{else} 0 
{/if})
</a></td>
  </tr>
</table>

<h3><br>
Employment</h3>
<table width="150" border="0" cellspacing="2" cellpadding="5">
  <tr>
        <td bgcolor="#f1f1f1"><a href="{$admin_baseurl}/admin/adminpanel.php?type=4&user_id={$k}" target="_blank" class="fb">Employment Proof<br>
({if isset($attributes.employment[$k].status)}
{if $attributes.employment[$k].status eq 'fail'} Rejected 
{else if $attributes.employment[$k].status eq 'rejected'} Not Clear 
{else if $attributes.employment[$k].status eq 'active'} Approved 
{else if $attributes.employment[$k].status eq 'under_moderation'} Under Moderation
{/if}
{else} 0 
{/if})
</a></td>
  </tr>
</table>

<h3><br>
Photo Panel</h3>
<table width="150" border="0" cellspacing="2" cellpadding="5">
  <tr>
        <td bgcolor="#f1f1f1"><a href="{$baseurl}/photo.php?caller=admin&user_id={$k}" target="_blank" class="fb">Edit Photo<br></a>
        </td>
  </tr>
</table>

<h3><br>
TrustBuilder</h3>
<table width="150" border="0" cellspacing="2" cellpadding="5">
  <tr>
        <td bgcolor="#f1f1f1"><a href="{$baseurl}/trustbuilder.php?caller=admin&user_id={$k}" target="_blank" class="fb">Trust Builder<br></a>
        </td>
  </tr>
</table>

<h3><br>
Block List</h3>
<table width="150" border="0" cellspacing="2" cellpadding="5"> 
  <tr>  
        <td bgcolor="#f1f1f1"><a href="{$admin_baseurl}/admin/adminpanel.php?type=5&user_id={$k}" target="_blank" class="fb">Block List<br></a>
        </td>
  </tr> 
</table>

</div>

</div>
{/foreach}
<!-- Profile info sec end -->
<div class="pagin">
<ul>

{if isset($sortBy)}
{if $page_num neq 1}
{if $page_num neq 2}

{if isset($value)}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy={$sortBy}&value={$value}">{$page_num-2}</a></li>
{else}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}&sortBy={$sortBy}">{$page_num-2}</a></li>
{/if}


{/if}
{if isset($value)}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy={$sortBy}&value={$value}">{$page_num-1}</a></li>
{else}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}&sortBy={$sortBy}">{$page_num-1}</a></li>
{/if}
{/if}
 

{if isset($value)}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy={$sortBy}&value={$value}">{$page_num}</a></li>
{else}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}&sortBy={$sortBy}">{$page_num}</a></li>
{/if}


{if isset($attributes)}
{if isset($value)}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy={$sortBy}&value={$value}">{$page_num+1}</a></li>
{else}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}&sortBy={$sortBy}">{$page_num+1}</a></li>
{/if}

{if isset($value)}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy={$sortBy}&value={$value}">{$page_num+2}</a></li>
{else}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}&sortBy={$sortBy}">{$page_num+2}</a></li>
{/if}
{/if}


{else}
{if $page_num neq 1}
{if $page_num neq 2}

<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-2}">{$page_num-2}</a></li>
{/if}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num-1}">{$page_num-1}</a></li>
{/if}

<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num}">{$page_num}</a></li>
{if isset($attributes)}
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+1}">{$page_num+1}</a></li>
<li><a href="{$admin_baseurl}/admin/adminpanel.php?page_id={$page_num+2}">{$page_num+2}</a></li>
{/if}
{/if}

</ul>
</div>
</div>

<nav id="page_nav" style="display: none;">
    <a href="{$admin_baseurl}/admin/adminpanel.php"></a>
 </nav>

<script src="{$admin_baseurl}/js/admin/adminCore.js?ver=4.3"></script>

<script>

$(function() {
	var x = document.getElementsByName("bdate");
	var i;
	for (i = 0; i < x.length; i++) {
    	$(x[i]).datepicker();
	}
});
  
function selectForm(id){
	alert(id);
	var m = id.split('_');
	var userId = m[1];
	var form_id = "form_"+userId;
var app = '<<input type="text" name="value" class="fl">';
	$("#"+form_id).append(app);
}


function showpopup(id){
	//alert(id);
	var m = id.split('_');
	//var status = m[0];
	var userId = m[1];
	//$('.suspension').attr('status', status);
	$('.suspension').attr('id', userId);
	
	$('.messagereply').show();



	// alert('Person Blocked');
	
	

	//$('.messagereply_'+userId).show();
}



function suspendfun(id){
	 // alert(id);
	  //alert (status);
	 // var m = id.split('_');
	 // var user_id = m[1];
	  //var status = m[0];
var user_id = id;
var lid = $("input[name=usrblock]:radio:checked").attr('id');
//alert(labelid);
	//$("input[name=usrblock]").change(function() {
//alert('here');
		//var lid = $(this).attr("id");
//alert(lid);
	//	var post_url = window.location.href;
		//$('.send').click(function() {
var post_url = "{$admin_baseurl}";
			var text;
			if (lid == 7) {
				text = $('#textArea').val();
			} else
				text = $('#'+lid).val();

			//alert(text);
			$('.block').hide();
			$('.suspend').hide();
		
			$.post(post_url +"/admin/adminpanel.php", {
				action : 'block',
				userId : user_id,
				status : 'suspended',
				reason : text
			}
			, function(data, status) {
				$('.messagereply').hide();
				$('#status_'+user_id).html("suspended");
				$("#"+user_id).css("display","none");
				$("#susp_"+user_id).css("display","none");
				if(data.status == 'Success'){
					var alert_msg = "Suspended Successfully !!\n Number of suspensions allowed :"+data.no_changes_allowed;
					alert(alert_msg);  
					}
				else if(data.status=='Limit exceeded') {
					alert("Limit already exceeded !!!");	 
					}
					
			},'json');

}

function blockfun(id){
	var user_id = id;
	var lid = $("input[name=usrblock]:radio:checked").attr('id');
	var block_all_devices = $('#block_all_devices').is(":checked")
	$('.block').hide();
	$('.suspend').hide();
			
	var post_url = "{$admin_baseurl}";
			var text;
			if (lid == 7) {
				text = $('#textArea').val();
			} else
				text = $('#'+lid).val();

			//alert(text);
			$.post(post_url +"/admin/adminpanel.php", {
				action : 'block',
				userId : user_id,
				status : 'blocked',
				reason : text,
                block_all_devices : block_all_devices
			}
			, function(data) {
					  
				$('.messagereply').hide();
				$('#status_'+user_id).html("blocked");
				$("#susp_"+user_id).css("display","none");
				$("#"+user_id).css("display","none");
				if(data.status == 'Success'){
					var alert_msg = "Blocked Successfully !!\n Number of blocks allowed :"+data.no_changes_allowed;
					alert(alert_msg);  
					}
				else if(data.status=='Limit exceeded') {
					alert("Limit already exceeded !!!");	 
					}
				else{
					alert(data.status+" !!"); 
					}
			},'json');
}



function sushide(id)
{
var m = id.split('_');
var userId = m[1];
var form_id = "form_"+userId;

var y = $("#"+form_id +" option:selected").text();

var x = $("#"+id).val();

	//if(status == 'fail'){
		//var a = confirm("Are you sure you want to suspend?");
		//if(a == false){
			//return;
		//}
	//}
	$.post("adminpanel.php",{

		action:'suspend',
		status: 'suspended',
		reason: y,
		userId:userId
	},function(data){
	
		if(data.status=='Success'){
			$("#"+form_id).hide();
			
		}
	},'json');
	
//document.getElementById("susp").style.display="none";
}


function getPhoneNumber(id){
  $.post("adminpanel.php",{
  action:'getPhoneNumber',
  user_id:id
  },function(data){
  if(data.status=='Success'){

   if(data.phones.phone==null){
   document.getElementById("PhoneButton_"+id).style.display="none";
   }
   else{
   document.getElementById("PhoneButton_"+id).style.display="none";
   document.getElementById("PhoneData_"+id).style.display="inline";
   var elem=document.getElementById("phoneNumber_"+id);
  
   elem.value=data.phones.phone;
   
   var small_elem=document.getElementById("phoneStatus_"+id);
   if(data.phones.status="active"){
   small_elem.innerHTML="(Verified)";
   }
   
   
   }}
  if(data.status=='Limit exceeded'){
   alert("Limit Exceeded for the day");
   }
  if(data.status=='No access rights'){
   alert("Sorry Not Enough Access for this admin account");
   } 
  },'json');
}

function show_engagement(id){
       var days_count = $('#day_count_'+id).val();
       $.post("adminpanel.php",{
		action:'show_engagement',
        days_count:days_count,
		user_id:id,  
	    },function(data){ 
		if(data.status=='Success'){ 
			document.getElementById("engagement_button_"+id).style.display="none";
			var elem=document.getElementById("engagement_matrix_"+id);
			elem.style.display="inline";
			elem.rows[0].cells[0].innerHTML="Recommendations";elem.rows[0].cells[1].innerHTML=data.recs;
			elem.rows[1].cells[0].innerHTML="Likes";          elem.rows[1].cells[1].innerHTML=data.likes;
			elem.rows[2].cells[0].innerHTML="Hides";          elem.rows[2].cells[1].innerHTML=data.hides;
			elem.rows[3].cells[0].innerHTML="Mutual Likes";   elem.rows[3].cells[1].innerHTML=data.mutual;
		} 		   
	},'json');
}


function show_sparks(id){
    $.post("adminpanel.php",{
        action:'show_sparks',
        user_id:id,
    },function(data){
        if(data.status=='Success'){
            document.getElementById("sparks_"+id).style.display="none";
            var elem=document.getElementById("spark_matrix_"+id);
            elem.style.display="inline";
            elem.rows[0].cells[0].innerHTML="Package Type";elem.rows[0].cells[1].innerHTML=data.type;
            elem.rows[1].cells[0].innerHTML="Price";          elem.rows[1].cells[1].innerHTML=data.price;
            elem.rows[2].cells[0].innerHTML="Sparks Sent";          elem.rows[2].cells[1].innerHTML=data.sparks_sent;
            elem.rows[3].cells[0].innerHTML="Accepted";   elem.rows[3].cells[1].innerHTML=data.accepted;
            elem.rows[4].cells[0].innerHTML="Sparks Remaining";   elem.rows[4].cells[1].innerHTML=data.remaining;
        }
    },'json');
}


function clear_femaleUnderReview(id){
	 
	$.post("adminpanel.php",{
		action:'clear_femaleUnderReview',
		user_id:id,  
	},function(data){ 
		if(data.status=='Success'){  
			$("#userClear"+id).hide();
			var alert_msg = "User cleared Successfully ";
			alert(alert_msg);		
		} 		   
	},'json');  
} 

function block_femaleUnderReview(id){
	 
	$.post("adminpanel.php",{
		action:'block_femaleUnderReview',
		user_id:id,  
	},function(data){ 
		if(data.status=='Success'){  
			$("#userBlock"+id).hide();
			var alert_msg = "User blocked Successfully ";
			alert(alert_msg);		
		} 		   
	},'json');  
} 

function restoreState(id){
	 
	$.post("adminpanel.php",{
		action:'restore',
		user_id:id,  
	},function(data){ 
		if(data.status=='Success'){  
			$("#restore_"+id).hide();
			var alert_msg = "User Restored Successfully !!\n Number of restores allowed :"+data.no_changes_allowed;
			alert(alert_msg);		
		}
		else if(data.status=='Limit exceeded') { 
			alert("Limit already exceeded !!!");	 
			}
      else if(data.status=='Nolaststatus')
      {
        alert("The user has no previous status set, it can't be restored..");
      }
	},'json');  
} 


function showeditname(id,fname,lname){
	$('.suspension').attr('id', id);
	$("input[name='fname']").val(fname);
	$("input[name='lname']").val(lname);
	$('.editname').show();  
}
 
function showeditphoneNumber(id,phoneNumber){
	$('.suspension').attr('id', id);
	$("input[name='phoneNumber']").val(phoneNumber);
	$('.editphoneNumber').show();

}

function saveName(id){
	fname = $("input[name='fname']").val();
	lname = $("input[name='lname']").val();
	$.post("adminpanel.php",{
		action:'editName',
		user_id:id,
		fname : fname,
		lname : lname
	},function(data){
		if(data.status=='Success'){
			$("#name_"+id).val(fname+" "+lname);
			$(".editname").hide();
			var alert_msg = "Name Edited !!\n Number of changes allowed for Name field :"+data.no_changes_allowed;
			alert(alert_msg);
		}else if(data.status=='limit exceeded') {
		$(".editname").hide();
		alert("Limit already exceeded !!!");	 
		}
	},'json');
}
  
 // Function to Reset value of Calendar !!!
function reset(id,val){
	document.getElementById("bday_"+id).value=val;
} 
 
 // Function to show datepicket !!!
function show_calendar(obj){ 
  $(obj).datepicker();
  $(obj).change(function() {
  	$(obj).datepicker("option","dateFormat","yy-mm-dd");
  });
 }

function saveDOB(id,previous_value){
	
	var bdate = document.getElementById("bday_"+id).value; 
	
	if(previous_value==bdate)
	{	
	return ;}  
	if(!isDate(bdate)==true){
		alert("Invalid Date !!!");
		return ;
	}
	
	$.post("adminpanel.php",{
		action:'editDOB',
		user_id:id,
		bdate : bdate 
	},function(data){ 
		if(data.status=='Success'){
			$("#bday_"+id).val(bdate);
			var alert_msg = "Birthday Edited !!\n Number of changes allowed for Birthday field :"+data.no_changes_allowed;
			alert(alert_msg);
		}else if(data.status=='limit exceeded') {
		alert("Limit already exceeded !!!");	 
		} 
	},'json'); 
	$("#bday_button"+id).attr('disabled', true);
	$("#bday_reset_button"+id).attr('disabled', true);
}

function saveGender(id){ 
	var temp="sex_"+id;
	gender = $("#"+temp).val();
	$.post("adminpanel.php",{  
		action:'editGender',
		user_id:id,
		gender : gender 
	},function(data){
		if(data.status=='Success'){ 
			$("#gender_"+id).val(gender); 
			$(".editGender").hide();
			var alert_msg = "Gender Edited !!\n Number of changes allowed for Gender field :"+data.no_changes_allowed;
			alert(alert_msg);
		}else if(data.status=='limit exceeded') {
		alert("Limit already exceeded !!!");	 
		}
	},'json'); 
}

function savephoneNumber(id){
	phoneNumber = $("input[name='phoneNumber']").val();
	// Validation of Phone Number !!!!	
	if(!Validate_Phone_Number(phoneNumber))
	{
		alert("Invalid Number");
		return ;
	}
	
	$.post("adminpanel.php",{
		action:'editphoneNumber',
		user_id:id,
		phoneNumber : phoneNumber
	},function(data){
		if(data.status=='Success'){
			$("#phoneNumber_"+id).val(phoneNumber);
			$(".editphoneNumber").hide();
			var alert_msg = "Phone Number Edited !!\nNumber of changes allowed for Phone Number field "+data.no_changes_allowed;
			alert(alert_msg);
		}else if(data.status=='limit exceeded') {
		$(".editphoneNumber").hide();
		alert("Limit already exceeded !!!");	 
		} 
	},'json');
}

</script>

</body>
 
</html>
