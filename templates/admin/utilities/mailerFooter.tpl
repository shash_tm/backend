<table border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; color:#333;width:100%;">
  <tr>
    <td align="center" bgcolor="#eeeeee">
<div style="font-size: 14px; width:30%; height:auto; min-height:120px; text-align:center; padding:1.5%; float:left; font-family:'Myriad Pro',Arial,Helvetica,sans-serif;"><img src="http://cdn.trulymadly.com/email/images/seal.png" width="42" height="41" style="margin-bottom:5px;" /><br />
  <p style="margin:0 0 5px 0; padding:0 0 5px 0; border-bottom:1px solid #bbb;">Verified Profiles</p>
<span style="font-size: 12px;">All our members are assigned a Trust Score to ensure all profiles are real.</span></div>
<div style="font-size: 14px; width:30%; height:auto; min-height:120px; text-align:center; padding:1.5%; float:left; font-family:'Myriad Pro',Arial,Helvetica,sans-serif;  border-right:1px solid #fff;  border-left:1px solid #fff; "><img src="http://cdn.trulymadly.com/email/images/couple.png" width="43" height="41" style="margin-bottom:5px;" /><br />
  <p style="margin:0 0 5px 0; padding:0 0 5px 0; border-bottom:1px solid #bbb;">True Compatibility</p>
<span style="font-size: 12px;">Our relationship experts use scientific compatibility to find you suitable matches.</span></div>
<div style="font-size: 14px; width:30%; height:auto; min-height:120px; text-align:center; padding:1.5%; float:right; font-family:'Myriad Pro',Arial,Helvetica,sans-serif;"><img src="http://cdn.trulymadly.com/email/images/lock.png" width="30" height="41"  style="margin-bottom:5px;"/><br />
  <p style="margin:0 0 5px 0; padding:0 0 5px 0; border-bottom:1px solid #bbb;">Private & Safe</p>
<span style="font-size: 12px;">All personal contact information is kept private and secure.</span></div>

</td>
  </tr>
    <tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/bgmailer.jpg); background-color:#38434f;"><div style="padding:10px 0; color:#ccc; font-size:11px; text-align:center; line-height:18px;"><strong style="font-size:12px; font-family:Arial, Helvetica, sans-serif;">Follow us on:</strong>
        <br />
        <a href="http://www.facebook.com/trulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/fbicon.png" alt="fb" width="29" height="29" style="border:none; margin:5px;" title="Facebook" /></a><a href="http://blog.trulymadly.com/" target="_blank"><img src="http://cdn.trulymadly.com/email/images/blogicon.png" alt="blog" width="29" height="29" style="border:none; margin:5px;" title="Blog"/></a><a href="http://twitter.com/thetrulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/twtricon.png" alt="twitter" width="29" height="29" style="border:none; margin:5px;" title="twitter" /></a><a href="http://instagram.com/trulymadlycom" target="_blank"><img src="http://cdn.trulymadly.com/email/images/instagicon.png" alt="instagram" width="29" height="29" style="border:none; margin:5px;" title="instagram"/></a><br />
        <a href="http://www.trulymadly.com/terms.php" target="_blank" style="color:#ccc; text-decoration:none;">Terms of Use</a> | <a href="http://www.trulymadly.com/policy.php" target="_blank" style="color:#ccc; text-decoration:none;">Privacy Policy</a> | <a href="http://www.trulymadly.com/guidelines.php" target="_blank" style="color:#ccc; text-decoration:none;">Safety Guidelines</a> | <a href="{$analyticsLinks.analytics_unsubscribe_link}" target="_blank" style="color:#ccc; text-decoration:none;">Unsubscribe</a><br />
<a href="{$analyticsLinks.analytics_footer_link}" target="_blank" style="color:#ccc; text-decoration:none;">trulymadly.com</a> &copy; 2014. All rights reserved. F-313C, 3rd Floor, Lado Sarai, New Delhi, India 110030</div></td>
  </tr>
  </table>

