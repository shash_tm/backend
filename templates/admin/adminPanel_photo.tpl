<!DOCTYPE>
<html>
<head>
<meta charset="utf-8" />
<title>Trulymadly</title>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
    <link type="text/css" rel="stylesheet" href="{$admin_baseurl}/css/admin/adminstyle.css">
<meta name="viewport" content="width=device-width" />
<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$admin_baseurl}/js/admin/adminCore.js?v=4.4"></script>
<script src="{$admin_baseurl}/js/admin/imageRotate.js"></script>
<script>
var baseurl = '{$baseurl}';   
var user_id = '{$user_id}';
</script>
</head>
<body>
<!--<a href="adminpanel.php?type=1">Verify Id Proofs</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=2">Verify User Images</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=3">Verify Address Proofs</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminpanel.php?type=4">Verify Employment Proofs</a>
-->
{foreach from=$photos item=photo}
<div id="id_proof_{$photo.photo_id}" class="insideframe">
<!--<img src = '{$cdnurl}/files/images/profiles/{$photo.name}' width="400" align="middle" class="fl"/>-->
<img id="image_{$photo.photo_id}" src = '{$imageurl}{$photo.name}' width="400" align="middle" class="fl"/>
<ul class="fl ml20">
<li><strong>Name:</strong> {$photo.fname} {$photo.lname}</li>
<li><strong>User ID:</strong> {$photo.user_id} </li>
<li><strong>Gender:</strong> {$photo.gender}</li>
<li><strong>Status:</strong> {$photo.status}</li>
<li><strong>Profile Pic:</strong> {$photo.is_profile}</li>

<li id="button_{$photo.photo_id}">
{*if $photo.photo_status eq 'under_moderation' *}
{if $photo.admin_approved neq 'yes'}
<input type="button" value="Approve" onClick="verifyPhoto('active','{$photo.photo_id}', '{$photo.user_id}','{$photo.is_profile}','{$photo.mapped_to}')" />&nbsp;&nbsp;
 
<input type="button" value="Approve Without Profile" onClick="verifyPhoto('active_no_profile','{$photo.photo_id}', '{$photo.user_id}','{$photo.is_profile}','{$photo.mapped_to}')" />&nbsp;&nbsp;

<input type="button" value="Reject" onClick="verifyPhoto('rejected','{$photo.photo_id}', '{$photo.user_id}','{$photo.is_profile}','{$photo.mapped_to}')"/>

<input type="button" id="rotate_{$photo.photo_id}" value="Rotate" onClick="rotatePhoto('image_{$photo.photo_id}')"/>

<input type="button" id="savePhoto_{$photo.photo_id}" value="Save" onClick="saveRotatedImage('{$photo.photo_id}')"/>
{else}
<strong>Photo Status:</strong> {$photo.photo_status}
{/if}

</li>
</ul>
<!--<span>fb_id: {$photo.fid}</span>
<span>email_id: {$photo.email_id}</span>

-->
</div>
{foreachelse}
No more proofs to be verified
{/foreach}
</body>
</html>