<!DOCTYPE>
<html>
<head>

    <meta charset="utf-8" />
    <title>Trulymadly</title>

    <link type="text/css" rel="stylesheet" href="{$admin_baseurl}/css/admin/adminstyle.css">
    <meta name="viewport" content="width=device-width" />

    <script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
    <script src="{$admin_baseurl}/js/admin/adminCore.js?v=4.3"></script>

</head>
<body>
<li> <a href="{$admin_baseurl}/admin/adminpanel.php"> &laquo; Back to Admin</a></li>

<form name="form1" method="post" action="{$admin_baseurl}/admin/adminpanel.php?type=6" class="fl">
    <label class="fl" style="padding:3px 5px 10px 20px;">Search By Blocked Device Id:</label> <input type="text" name="device_id" class="fl" style=" width:120px;">
    <input type="submit" value="Submit" class="fl">
</form>
{if $device|@count>0}
    <table width="1050" border="1" cellspacing="5" cellpadding="5" align="center">
        <tr>
            <th>Device Id </th>
            <th>Blocked at </th>
            <th>Reason</th>
            <th>User Id</th>
            <th>Action</th>
        </tr>
            <tr>
                <td>{$device.device_id}</td>
                <td>{$device.tstamp}</td>
                <td>{$device.reason}</td>
                <td>{$device.user_id}</td>
                <td align="center">
                        <input type="button" value="unblock" id="block_{$device.device_id}" onclick="unblock('{$device.device_id}')">
                </td>
            </tr>
    </table>
    {if $device.all_users|@count > 0}
        <h3> Users Registered from this device</h3>
        <table width="800" border="1" cellspacing="2" cellpadding="2" align="center">
            <tr>
                <th>User_id </th>
                <th>Name </th>
                <th>Status</th>
                <th>Facebook</th>
            </tr>
            {foreach $device.all_users as $users }
                <tr>
                    <td>{$users.user_id}</td>
                    <td>{$users.fname}</td>
                    <td>{$users.user_status}</td>
                    <td>
                        {if $users.fid neq null }
                        <a href="https://www.facebook.com/{$users.fid}" target="_blank"> Facebook Link</a></td>
                    {else}
                    <td>Not available</td>
                    {/if}
                </tr>
            {/foreach}
        </table>

    {/if}
{else}<h3>
    No Devices found !!!

</h3> {/if}

</body>

<script>

    function unblock(device_id){
        $.post("adminpanel.php",{
            action:'unblock_device',
            device_id :device_id
        },function(data){
            if(data.status=='success'){
                var alert_msg = "Unblocked Successfully";
                alert(alert_msg);
            }
        },'json');
        $("#block_"+device_id).hide();
    }



</script>

</html>