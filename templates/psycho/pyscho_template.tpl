{include file='../registration/header_registration.tpl'}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/psycho.css">
<script type="text/javascript">
var progressBarPercent = "{$psycho_progressbar}";
</script>

<script src="{$cdnurl}/js/register/core_psycho.js?v=104"></script>

<script type="text/javascript">
var div_ids={$div_ids};
var psycho_step="{$psycho_step}";
</script>

{if ($psycho_step eq 'psycho1')}
<div id='physcoIntro'>

{include file='templates/registration/physcointro.tpl'}

</div>
{/if}
   <div class="container" id='physcoQuestions' {if ($psycho_step eq 'psycho1')} style='display: none;' {/if}>
   <!-- Questionaries Section Start    -->
        
<p class="caption" style="font-size: 20px; margin: 12px 0; text-align: center; text-shadow: 0 1px #F7F7F7;">{$psycho_topline}</p>
<div id="progressbar" ></div>



<!-- My work Start    -->
<div class="questsecframe mt40">

<!-- Questions section start -->

{$output}

<div id="loader" class="tmquestions hide">
{include file='templates/registration/reloader.tpl'}
</div>
<!-- Questions section End -->
<!--<button class="hide fl mrg oopsbtn" id="back"><span style="font-size:18px;">&laquo;</span>Back</button> -->
</div>

   <!-- Questionaries Section end    -->
   
   <div id='next' class="clearfix mt30 hide">    
   <div class="continuert mr50"><a >Save</a></div>
   </div>
          <div class="clearfix mt30">
	<div class="oopsleft hide" id="back">Back</div>
	</div>
   
     </div>
     
     


     
</div>  
     
     
  </body>
</html>
