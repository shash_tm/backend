<!DOCTYPE>

<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Personality Quiz</title>

<script>
var edit = false;
var time = new Date();
var uid = '{$uid}';
var psycho_step = '{$psycho_step}';
var rdUrl = '{$rdUrl}';
var baseurl='{$baseurl}';
var stepsCompleted = '{$stepCompleted|json_encode}';
{if $fromRegister eq 1 }
var fromRegister = true;
{else}
var fromRegister = false;
{/if}
var que = {};
</script>
<link href="{$cdnurl}/css/register_new.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css" rel="stylesheet" type="text/css">

</head>
<body>
{include file='templates/common/header.tpl'}

	
	{if ({$psycho_step} eq 'psycho1')}
    <div class="new_wrapper valuephychobg">
	<!--Centered Container Start -->
	<div class="new_container" align="center">

	<!-- Top progress section start -->
    <div class="regprogress">
   
		<h2>What do your values say about you?</h2>
	
	</div>
	<!-- Top progress section end -->
    
    
    
    
	<div id="physcoIntro" style="display:none;">
		<div class="psyformsec">
			<div class="psytxtbox">
			<h4>Let's get rolling!</h4>
			<ul class="tutinfo">
     			<li>Here are 10 statements with their 2 probable reactions. </li>
     			<li>Choose an option from the 6 given to you. </li>
     			<li>This quiz needs to be finished in one go. </li>
     		</ul>
   			We swear not to share your answers with anyone. And remember, there is no right or wrong answer. Only a perspective. 
    		</div>
    		<ul class="formbtmbrdr">
				<li class="bg6"></li>
				<li class="bg5"></li>
				<li class="bg4"></li>
				<li class="bg1"></li>
				<li class="bg2"></li>
				<li class="bg3"></li>
			</ul>  
		</div>
		<div class="regfrmaction">
			<a id="physcoCancel" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
			<a id="physcoIntroCnt" class="regbcbtn fr">Off you go! &nbsp;<i class="icon-angle-right"></i></a>
		</div>
	</div>
    
    
    
    
    	<!--question Start -->
	<div id="physcoQuestions" style="display:none;">
      <a class="doitlater" id="dolater"  onClick="document.body.style.overflow='hidden'; document.getElementById('diffframe2').style.display='block';">Do it later!</a>
			 {$output}
	</div>
	<img id="loader" style="display:none;" src="{$cdnurl}/images/ajax_loader.gif" align="absmiddle" width="100">
	<!--question end -->

	</div>
<!--Centered Container End -->
</div>
    
	{else}
    
        <div class="new_wrapper adaptphychobg">
	<!--Centered Container Start -->
	<div class="new_container" align="center">

	<!-- Top progress section start -->
    <div class="regprogress">
   
		<h2>How adaptable are you really?</h2>
	
	</div>
	<!-- Top progress section end -->
    
    
    
    
    
	<div id="physcoIntro" style="display:none;">
		<div class="psyformsec">
			<div class="psytxtbox">
			<h4>Ok guys, gear up!</h4>
			<ul class="tutinfo">
     			<li>Here are 10 different situations with 3 possible reactions each. </li>
     			<li>Order the reactions from what sounds like something you’d definitely do to the one that doesn’t appeal to you at all. </li>
     			<li>This quiz needs to be finished in one go. </li>
     		</ul>
   			Don’t worry, your responses will never be shared with anyone. And remember, there isn’t a right or wrong answer. 
    		</div>
    		<ul class="formbtmbrdr">
				<li class="bg6"></li>
				<li class="bg5"></li>
				<li class="bg4"></li>
				<li class="bg1"></li>
				<li class="bg2"></li>
				<li class="bg3"></li>
			</ul>  
		</div>
		<div class="regfrmaction">
			<a id="physcoCancel" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
			<a id="physcoIntroCnt" class="regbcbtn fr">Let's go! &nbsp;<i class="icon-angle-right"></i></a>
		</div>
	</div>
    
    
    
        	<!--question Start -->
	<div id="physcoQuestions" style="display:none;">
	<a class="doitlater" id="dolater"  onClick="document.body.style.overflow='hidden'; document.getElementById('diffframe2').style.display='block';">Do it later!</a>
			 {$output}
	</div>
	<img id="loader" style="display:none;" src="{$cdnurl}/images/ajax_loader.gif" align="absmiddle" width="100">
	<!--question end -->

	</div>
<!--Centered Container End -->
</div>

	{/if}
	
<!-- Poop Up -->
<div id="diffframe2" style="display:none;" >
<div class="difusescreen" align="center" onClick="document.body.style.overflow='visible'; document.getElementById('diffframe2').style.display='none';"></div>
<div class="photopopup">
<div class="photowraper" align="center">
<a onClick="document.body.style.overflow='visible'; document.getElementById('diffframe2').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<p class="pwraptxt">Oye! Are you sure you want to go back? You do know that you'll have to start all over again, right?</p>
<a id="continue" class="phactionbtn fl">Abort</a>
<a onClick="document.body.style.overflow='visible'; document.getElementById('diffframe2').style.display='none';" class="phactionbtn fl">Continue</a>
<div class="clb"></div>
</div>
</div>

</div>



{include file='templates/common/footer.tpl'}

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/lib/tm_core_new.js"></script>
<script src="{$cdnurl}/js/lib/function.js?ver=1"></script>
<script src="{$cdnurl}/js/register/core_psycho.js?v=1.5"></script>

</body>
</html>
