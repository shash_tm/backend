<div id="div_id_{$div_id-1}" class="hide">
<div class="psyformsec">
<script>
que[{$question_id}] = [];
que[{$question_id}].push('{$psycho_answers[1].bucket}');
que[{$question_id}].push('{$psycho_answers[2].bucket}');
que[{$question_id}].push('{$psycho_answers[3].bucket}');
</script>
<div class="psytxtbox">
<h5><span>{math equation="x - y" x=$question_id y=10}.</span> {$psycho_question}</h5>
<ul class="psycho1ans padt2">
<p class="txttopdd">Order your<br> preference</p>
	{foreach from=$psycho_answers key=k item=v name=foo}
    <hr size="1" noshade="noshade" />
		<li>
		<div class="qselect-style">
  		<select name="pa[{$question_id}][{$v.bucket}]">
        <option value=" " disabled selected>-</option>
    	<option name="hello" value="1">1st</option>
    	<option name="hello" value="2">2nd</option>
    	<option name="hello" value="3">3rd</option>
  		</select>
		</div>     
		<span>{$v.answer}</span></li>
	{/foreach}
    
</ul>
</div>
    
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul>    
</div>

<div class="regfrmaction">
{if $question_id neq 16}
<a id="back_{$div_id-1}" onclick="onBack()" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
{/if}
<a id="diff_continue_{$div_id-1}" class="diffregbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
<a id="continue_{$div_id-1}" onclick="onContinue()" class="regbcbtn fr" style="display:none;">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>

</div>
