
<div id="div_id_{$div_id-1}" class="hide">
<div class="psyformsec">
<script>
que[{$question_id}] = [];
que[{$question_id}].push('{$psycho_answers[1].bucket}');
que[{$question_id}].push('{$psycho_answers[2].bucket}');
</script>
<div class="psytxtbox psytxtwdth">
<h5><span>{$question_id}.</span> {$psycho_question}</h5>
<ul class="psycho1ans">
	{foreach from=$psycho_answers key=k item=v name=foo}
		<li>
        <!--<p>{if $smarty.foreach.foo.iteration eq 1}A{else}B{/if}</p> -->
        <span>{$v.answer}</span></li>
	{/foreach}
</ul>
</div>
    <div class="questans1">
    	<p>Choose the  one you agree with:</p>
        <label><input type="radio" name="pa_{$question_id}" value='3'><span>A slightly more</span></label>
        <label><input type="radio" name="pa_{$question_id}" value='1'><span>B slightly more</span></label>
        <label><input type="radio" name="pa_{$question_id}" value='2'><span>Both equally</span></label>
        <label><input type="radio" name="pa_{$question_id}" value='4'><span>Only A</span></label>
        <label><input type="radio" name="pa_{$question_id}" value='0'><span>Only B</span></label>
        <label><input type="radio" name="pa_{$question_id}" value='-1'><span>Neither</span></label>
  
    </div>
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul>    
</div>

<div class="regfrmaction">
{if $question_id neq 1}
<a id="back_{$div_id-1}" onclick="onBack()" class="regbcbtn fl"><i class="icon-angle-left"></i>&nbsp; Back</a>
{/if}
<a id="diff_continue_{$div_id-1}" class="diffregbcbtn fr">Continue &nbsp;<i class="icon-angle-right"></i></a>
<a id="continue_{$div_id-1}" onclick="onContinue()" class="regbcbtn fr" style="display:none;">Continue &nbsp;<i class="icon-angle-right"></i></a>
</div>

</div>
