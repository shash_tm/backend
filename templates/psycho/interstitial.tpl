<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Personality Quiz</title>
<script>
var time = new Date();
</script>
<link href="{$cdnurl}/css/register_new.css?v=5" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css?v=3" rel="stylesheet" type="text/css">

<script>
var uid = '{$uid}';
var baseurl = '{$baseurl}';
var stepsCompleted = '{$stepCompleted|json_encode}';
{if $isRegistration eq 1 }
var fromRegister = true;
{else}
var fromRegister = false;
{/if}
</script>
</head>

<body>
{include file='templates/common/header.tpl'}
<div class="new_wrapper comphychobg">

<!--Centered Container Start -->
<div class="new_container" align="center">

<!-- Top progress section start -->
<div class="regprogress">
<h2>Personality Quizzes</h2>
		{if $isRegistration eq 1 }
    	 <div class="regscalesec" align="center"><div class="regprogbar pbarstep9"></div></div>
         {else}
		 {/if}   
</div> 

<!-- Top progress section end -->

<!--Psycho box Start -->
<div class="psyformsec">
	<div class="psytxtbox">
    We want to find someone who is perfect for you. And though it's completely optional, we would love for you to take these super fun, quick personality quizzes and make the process slightly easy for us. 
    	<table {if in_array('psycho1', $stepCompleted)}class="psytest psytestdone" {else}id="psycho1" class="psytest"{/if} cellpadding="0" cellspacing="0"><!-- use class "psytestdone" with existing one if user take the test -->
        <tr><td class="psytesttd1"><p>What do your values say about you?</p></td>
        <td><span>10 Questions</span></td>
        <td align="right">{if in_array('psycho1', $stepCompleted)}<i class="icon-ok"></i>{else}<i class="icon-chevron-right"></i>{/if}<!-- replace <i> tag class "icon-chevron-right" to "icon-ok" if user take the test --></td>
    	</tr>
        </table>
       <table {if in_array('psycho2', $stepCompleted)}class="psytest psytestdone" {else}id="psycho2" class="psytest"{/if} cellpadding="0" cellspacing="0">
        <tr><td class="psytesttd1"><p>How adaptable are you really?</p></td>
        <td><span>10 Questions</span></td>
        <td align="right">{if in_array('psycho2', $stepCompleted)}<i class="icon-ok"></i>{else}<i class="icon-chevron-right"></i>{/if}</td>
        </tr>
    	</table>
        <table id="hobby" class="psytest" cellpadding="0" cellspacing="0">
        <tr><td class="psytesttd1"><p>What are your happy-do-to activities?</p></td>
        <td><span>Movies. Music etc.</span></td>
        <td align="right">{if in_array('hobby', $stepCompleted)}<i class="icon-ok"></i>{else}<i class="icon-chevron-right"></i>{/if}</td>
        </tr>
    	</table>
    	{if isset($no_of_likes)}
        <p class="psyfavimp"><i class="icon-hand-up"></i> {$no_of_likes} likes imported from Facebook</p>
        {/if}
    </div>
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul>    
</div>
<!--Psycho box end -->
<div class="regfrmaction" {if $isRegistration neq 1 }style="display:none;"{else}{/if}>
<a href="matches.php" class="showmatch">{if in_array('hobby', $stepCompleted) || in_array('psycho2', $stepCompleted) || in_array('psycho1', $stepCompleted)}Take Me To My Matches {else}Skip And Take Me To My Matches{/if}&nbsp;<i class="icon-double-angle-right" style="text-decoration:none;"></i></a>
</div>
</div>
<!--Centered Container End -->
</div>
{include file='templates/common/footer.tpl'}

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/lib/function.js?v=1"></script>
<script>
$(document).ready(function(){
	
	trackRegistration("psycho_interstitial","page_load",'');
	
	/*if(stepsCompleted.indexOf('psycho1')>-1 && stepsCompleted.indexOf('psycho2')>-1 && stepsCompleted.indexOf('hobby')>-1){
	 	window.location=baseurl + "/matches.php";
	}*/
	$("#psycho1").click(function(){
		if(fromRegister)
			window.location = baseurl+"/personality.php?step=psycho1&reg=true";
		else
			window.location = baseurl+"/personality.php?step=psycho1";
	});
	
	$("#psycho2").click(function(){
		if(fromRegister)
			window.location = baseurl+"/personality.php?step=psycho2&reg=true";
		else
			window.location = baseurl+"/personality.php?step=psycho2";
	});
	
	$("#hobby").click(function(){
		if(fromRegister)
			window.location = baseurl+"/personality.php?step=hobby&reg=true";
		else
			window.location = baseurl+"/personality.php?step=hobby";
	});
	
});
</script>

</body>
</html>
