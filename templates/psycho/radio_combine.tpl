<div class="tmquestions" id="psycho_radio_combine" align="center">
<div class="questiontop">Ideally, who would be responsible for the following roles after marriage?</div>
<div class="ansquest clb mrg" align="center">



{foreach from=$psycho_combine_questions key=k item=v}
<div class="optlefttitle">{$v.question}</div>
<ul class="formlist fl mrgl" id="{$v.div_id}">
{foreach from=$v.answers key=k item=answers}
<li class="fl">
<label class="radio">
<span class="icon">
</span>
<span class="icon-to-fade">
</span>
<input type="radio" value="{$k}" name="pa[{$v.id}]">
<span class="listitems">
{$answers->value}
</span>
</label>
</li>
{/foreach}
</ul>
{/foreach}

</div>
</div>
