<!doctype html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta charset="utf-8">
<title>TrulyMadly - Safety Guidelines</title>
<!--Stylesheet section start here -->
<link href="{$cdnurl}/css/login/reset.css" rel="stylesheet"
	type="text/css" />
<link href="{$cdnurl}/css/login/style.css" rel="stylesheet"
	type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400'
	rel='stylesheet' type='text/css'>
<!--Stylesheet section end here-->
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

</head>
<body>
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
  
</script>
	<!--wrapper secton start here-->
	<div id="wrapper">

		

			<!--Safe container-->
			<div class="safecontainer">
			<div class="termsContainer">
			{if $header.login_mobile neq true}
			<h2>Making User Experience Safe at TrulyMadly</h2>
			{/if}

<p>At TrulyMadly, we aspire to provide Users with recommendations and suggestions for matchmaking which could be most compatible based on the information provided by the User. Finding a highly compatible match and being in a lasting relationship online is a likely probability through TrulyMadly. However, the choices and decisions in the context always rest with the User. We are committed to assisting by providing guidance and support to the User for the User discovering User's best qualities and those of potential partners.</p>

<p>User's judgment, choices, decisions and instincts are necessary at all times and can not override the recommendations We make in good faith. We highly stress that there is no substitution for acting with caution and best judgement. Some guidelines for assisting with the User's interaction with probable matches through the Site are given below:</p>

<p><b>1. Please do not ignore your own Best Judgment</b></p>

<p>TrulyMadly monitors User account activity and investigates all complaints, however, verifying criminal background of each account is not possible for TrulyMadly. Users' should bear in mind it is always possible for people to misrepresent themselves. Some basic verification of information provided on the Site is conducted by TrulyMadly through engaging third party experts. Such verification is warranted only to the extent and quality, these third parties offer. However, assessing a match's authenticity and genuine interest in a lasting relationship is ultimately the User's responsibility. Users' are urged not to ignore any facts or feelings that seem inconsistent or "do not feel right". User has to trust one's own instincts and remember that User has control over the situation at all times. If during any interaction with any probable match, the User is suspicious, then TrulyMadly recommends it would be best for User to end such interactions. If during a personal interaction classified as a meeting for getting to know each other, the User is uncomfortable, then the best option would be to leave and discontinue future interaction.</p>

<p>We recommend the User should be aware of some of the below mentioned indications when getting to know someone new: </p>

<ul>
<li>Please be careful if anyone who you meet through the Services immediately requests to talk or chat on an outside email or messaging service than that offered through the Site. The Site offers a securing messaging service;</li>
<li>Please be watchful of people who claim that meeting you was a matter of destiny especially at early stages in communication/ interaction when you are just getting to know such probable match;</li>
<li>Please be very careful and vigilant about people you claim to be of certain location and all the time seem to be travelling and does not have any chances of being in a city as yours reducing chances of meeting of you feel ready for it;</li>
<li>Please do not believe any stories for need of money particularly someone who asks for money, goods or anything similar, whether or not you have met such a person; </li>
<li>Please be careful of people who ask you to assist/ support or engage in any manner with personal transactions (depositing money, transporting something etc.);</li>
<li>Please take any relationship at a pace most comfortable to your own self and be careful of someone wants the relationship to progress faster than your comfort;</li>
<li>Please verify, if someone indicates a sudden personal crisis and pressures you to provide any kind of assistance particularly financial; </li>
<li>Please be careful in interacting, if someone asks inappropriate questions. These could be anything which make you uncomfortable;</li>
<li>Please be careful of someone who requests for your home or work address under the pretext of sending flowers or gifts and particularly when you don't feel ready to share the same;</li>
<li>Any sort of inconsistency in what anyone says; </li>
<li>Any sort of vagueness in responding to specific questions or avoiding certain questions which feel important to you;</li>
<li>Please be very careful in interacting with someone who urges you, directly or indirectly, to compromise your principles;</li>
<li>Any person who doesn't feel responsible or right for you or seems to be blaming others all the time; </li>
<li>Please be careful in interacting with someone who insists on getting overly close, very quickly particularly when you do not feel ready for it;</li>
<li>Any person who claims to be recently widowed or divorced and this information was not already provided in his/ her profile with the Site and who gives explanation on why it was not provided. Someone who has recently been out of a committed relationship could also be grieving and may not be fully ready for a more committed relationship which you may want;</li>
<li>If someone disappears suddenly from the Site then reappears under a different name or is missing for large periods of time.</li>
</ul>

<p>If a match we recommend does any of the above activities or anything which makes you suspicious in any other way not listed above, please report this by emailing at contact@trulymadly.com so that TrulyMadly can take appropriate action against such User and such User's access to the Site. </p>

<p><b>2. We recommend NEVER Share Financial Information or Certain Personal Information with any match</b></p>

<ul>
<li>NEVER give credit card number or bank information or any passwords, including e-mail passwords;</li>
<li>NEVER share information which could reveal your passwords like, mother's maiden name, date of birth etc. or other private information that can be used to access your financial information.</li>
<li>NEVER send money to someone you meet online or to someone the person requests to, especially by wire transfer.</li>
</ul>

<p>Immediately stop communicating with anyone who pressures you for personal or financial information or attempts in any way to trick you into revealing it. </p>

<p><b>3. Always Protect Your Account at TrulyMadly</b></p>

<p>When accessing User account from a public or shared computer, use caution and be aware of your surroundings so that others are not able to view or record your password or other personal information. </p>

<p><b>4. Be very Cautious When Sharing Personal Information</b></p>

<p>We strongly encourage Users to be cautious when sharing personal information that could reveal identity. Never include last name, email address, home address, telephone number, place of work or any other identifying information in Internet profile or initial email messages. We urge Users to refer to Our security guidelines and resist the urge to provide a personal email or phone number right away, even if you feel a strong connection. We encourage you to get to know a match well before sharing personal details and only if you feel confident to do so. Sharing such information immediately, do not support trust in a relationship as most people seeking these for malicious reasons may convey. </p>

<p>We understand that speak on the phone with a prospective match can be important step in getting to know each other, however, before you share phone numbers, User should make sure that the need to respect each other's privacy and other important issues have been discussed and agreed upon. If either of the people in the match decide to end communication in the future, agree not to use the phone number of the other as a means to pursue an unwanted relationship. For added security, try using your phone's privacy features when you call, like private number blocking. </p>

<p><b>5. Do Your Own Research on any recommended match you wish to progress with</b></p>

<p>While we conduct certain types of screening and utilize a variety of screening technologies and verification methods through various resources, please note such screenings can not override what you wish to research or assess. Personal safety and securing a future in a relationship is always one's own responsibility. We urge Users to conduct any verification and screening they wish to perform for their safety on the matches, recommended/ suggested.</p>

<p>The background check conducted on the Users is very limited in nature, We expect honesty from Users when filling the Compatibility Assessment while providing personal information about themselves, to provide correct date of birth, marital status, city and state of residence, occupation, educational background and other information. </p>

<p>Regardless of the connection a User may feel with any of the matches, We encourage Users to do their own research before or even after meeting in person. This can include typing match's name into a search engine, or using a paid service to obtain a full background report. Also pay serious thought to gut feelings. Pay attention to the details someone shares. If the User feels anything that doesn't seem to be right, we strongly recommend Users to follow their intuition and stop communicating with that person. </p>

<p><b>6. Use Caution When Deciding to Click on Any URL Link</b></p>

<p>If any match shares a URL link to a favorite website or article as part of the get-to-know-you communication process User should always use good judgment and be cautious when deciding to click on any URL links. In particular, be aware of links that lead to web pages which look similar or identical to TrulyMadly homepage, including its log-in fields. These links are typically sent as attempts to 'phish' for log-in information from users in order to compromise user accounts. You should report any such phishing attempts to TrulyMadly immediately. The only way to log in to User account with Our Site is through TrulyMadly webpage and by directly accessing {if $header.login_mobile neq true}<a href="http://www.trulymadly.com">www.trulymadly.com</a>{else}www.trulymadly.com{/if} </p>

<p><b>7. Take Your Time to decide on a match or even progressing in a realtionship</b></p>

<p>Online communication can sometimes accelerate one's sense of comfort and intimacy and thus, makes it necessary to get to know each other better through other means. TrulyMadly offers secure anonymous messaging system for use for as long as it is required for getting to know the recommended match.</p>

<p>Even though TrulyMadly's Compatibility Assessment creates extremely compatible matches, it can not replace the need for your own experience. It is vital to meet and get to know each other in a meaningful way. So we recommend, allow time for a variety of experiences to occur. In cases of long-distance relationships, if possible, you should consider living in the same area for a significant time before committing to a more serious relationship. Move slowly and pay attention to the reality of the relationship. </p>

<p><b>8. Make Your meetings Safe </b></p>

<p>Meeting in person can be exciting. It is fun to explore the level of chemistry you share with the match, but always exercise reasonable caution. </p>

<p><b>While meeting a Match, Please Do:</b></p>

<ul>
<li>Choose the time and place of such a meeting. Meet in a public place at a decent hour during which lots of people will be around. Evening and night arrangements can be avoided for initial meetings;</li>
<li>It could be best to abstain from alcohol consumption in the first few meetings;</li>
<li>Using one's own transportation for initial meeting would be best. Wherever possible, try not to share place of residence immediately unless you are absolutely sure and comfortable of doing so; </li>
<li>Always share your plans and when you will return with a friend or a family member. Also, try to leave the address behind of the place where you going to meet. If the choice of place is not made by you, it would be good idea to check the same in advance a day or two before;</li>
<li>Carry a fully charged mobile phone with easily accessible emergency numbers. </li>
</ul>

<p><b>While meeting a Match, Don't:</b></p>

<ul>
<li>Leave unattended personal belongings such as purses, wallets, or jackets with pockets that may contain items that could reveal personal information about you, such as a driver's license, credit cards and ATM receipts or even in which the match could put something not belonging to you; </li>
<li>Try not to meet at your house or place of work or give that information out until you feel comfortable and feel know the match well;</li>
<li>Try not to go home with someone, even if it feels like everything is going great. You have not spent enough time with them to assess whether your safety is at risk. Revealing your residence address too early may not be a good idea.</li>
</ul>

<p><b>9. Always be Respectful and Kind</b></p>

<p>Users should always be respectful and treat matches as User would want them to treat User. Not every match is going to be right for you so closing communication with matches and having matches close communication with you is a natural and healthy part of the process. TrulyMadly is about bringing two compatible people together who have a solid foundation from which a long-term relationship would have a high probability of success. User still needs to carefully consider whether this particular person is the one with whom User would like to further a relationship. If User feels the need to end communication, then be honest, direct and polite. The sooner User addresses this determination, the better. </p>

<p>If a recommended match feels the need to close communication with User, please respect their wishes. If User still persists, then TrulyMadly could be constrained to take adequate steps to prevent such User to receive such Services.</p>

<p><b>10. Report Concerns About a Match to TrulyMadly</b></p>

<p>TrulyMadly works diligently to close Users who misrepresent to other Users on our Site, and member complaints are taken, seriously. If User has concerns about a particular match, please do not hesitate to contact us so that appropriate steps can be taken to keep TrulyMadly safe. Email your concerns to: contact@trulymadly.com.<a name="_GoBack"></a> </p>

<p>If We close an account for suspect activity, inappropriate behaviour or falsified identity, User may be notified via email to discontinue communicating with the match in question. We strongly encourage all of our members to review emails received from TrulyMadly to ensure this communication is received promptly.</p>

<p>We hope these guidelines will help to make User experience a success, and find a life partner/ soul mate, as so many others.</p>
			</div>
	
			</div>


</body>
</html>
