{include file='templates/header_dashboard.tpl'}

<script type="text/javascript">
$('.matches_header').removeClass('active');
</script>

	<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/verifypayment.css">
    <link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">

<div class="wrapper">
	<!--Header-->
	<div class="container">

		<!-- My work Start    -->

		<div class="clearfix" align="center">
			<div class="verifypaysec mt40">
            
            <div class="activevrfypay2 ovrf">
						<ul>
                        <li><div align="center" class="mrg"><p class="payf1"> See the<br />full name</p></div></li>
                        <!--<li><div align="center" class="mrg"><p class="payf2">View All<br />Profile Photos</p></div></li> -->
                        <li><div align="center" class="mrg"><p class="payf3">View Mutual<br />Facebook friends</p></div></li>
                        <li style=" background:none;"><div align="center" class="mrg"><p class="payf4">Send personalised<br />messages</p></div></li>
                        </ul>
			</div>
            
            <div class="payment_sec">
            <ul>
            {foreach from=$paymentPlans item = plan}
            	<a>
            		<li {if ($activeplan == $plan.plan_id)} class="payplanactive" {else} class="payplan" {/if} id="plan_{$plan.plan_id}">
            			{if $plan.plan_type == 'unlimited' }
            				<h3>1100<br />Credits</h3>
            				{else}
            				<h3>{$plan.credits}<br />Credits</h3>
            			{/if}
						<p class="pkgrate mt5 mb5"><span class="WebRupee">Rs.</span>{$plan.cost}</p>
            			<p class="pkgname">{$plan.description}</p>
            			<div id="plan_msg_{$plan.plan_id}" style="display: none;">
            			Total Amount inclusive of all taxes = <span class="WebRupee">Rs.</span> {$plan.cost}.            			</div>
					</li>
				</a>
            {/foreach}
            <li>
            	<ul class="pkgcndtn">
                <li><img src="images/register/conbullet.png" style=" float:left; padding:4px 5px 15px;" /><strong>Use 10 credits</strong> to unlock a profile & activate above features.</li>
                <li><img src="images/register/conbullet.png" style=" float:left; padding:4px 5px 15px;" /><strong>Valid for 6 months</strong>, all unused credits / unlimited access will expire after 180 days of purchase.</li>
                </ul>
            </li>
            </ul>
            <div align="left" class="vermobsec1">
            <span id='plan_msg'></span>
            {if ($userStatus eq 'incomplete')}
              
       		     {*if isset ($useremail)*}
<!--             		Enter your email address : <input type="text" id='emailId' value="{$useremail}" /><span id="email_error"></span><br/> -->
            	 {*/if*}
            	<!-- 	<span id='detailsFrame'> Please enter your mobile no. to proceed : <input type="text" id='phone' value="{$userphone}" style="padding:4px;" />&nbsp;&nbsp;&nbsp; --><input type="button" value="PAY" id="paybtn" style="background: #4794ED; border: medium none; color: #FFFFFF; font-family: 'RobotoBold'; padding: 5px 15px;"/><br />
<span id="phone_error" class="fs11 mt10" style="color:red;"></span></span>
            {/if}
            </div>
            <div id='paymentFrame' {if ($userStatus eq 'incomplete')} style="display: none;" {/if}>
            <iframe id='paymentiFrame' src="{$iframesrc}" width="889" height="480" style="border: 1px dashed #E7276B;" onerror="imgError()"></iframe>
            </div> 
            </div>
            
          </div>
        </div>
     </div>
</div>
<script type="text/javascript" src="{$cdnurl}/js/paymentcore.js?v=1.1"></script>
</body>
</html>