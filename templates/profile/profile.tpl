<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>TrulyMadly</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
//console.log(headLoadTimeStamp);
</script>
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css?v=7.1" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/profile/profiledisplay.css?v=10.2" rel="stylesheet" type="text/css">
</head>
<body>

<script>
var isMyProfile = "{if isset($smarty.get.me)}1{else}0{/if}";
var profileId = "{$match_id}";
var baseurl = "{$baseurl}";
var myId = "{$my_id}";
var msg_url = "{$profile_data.msg_url}";
var allImgSrcUrls=[];
{foreach $profile_data.other_pics  key=index item=val}
allImgSrcUrls[{$index}]="{$val}";
{/foreach}
var profilePicUrl = "{$profile_data.profile_pic}";
var hasProfile = false;
if(profilePicUrl.indexOf("dummy") < 0){
	hasProfile = true;
	allImgSrcUrls[allImgSrcUrls.length] = profilePicUrl; 
}
</script> 

{include file='templates/common/header.tpl'}

<div id="wrapper_container">
<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">

<a  class="backlink fl"><i class="icon-angle-left"></i> Back </a>
<!--{if $my_data.from_match eq true}
<a href="{$baseurl}/match.php" class="backlink fl"><i class="icon-angle-left txtpink"></i>&nbsp;Back to Recommendations</a>
{/if}
{if $my_data.from_mutual_like eq true}
<a href="{$baseurl}/likes.php" class="backlink fl" style="display:none;"><i class="icon-angle-left txtpink"></i>&nbsp;Back to Mutual Matches</a>
{/if}
<a href="{$baseurl}/messages.php" class="backlink fl" style="display:none;"><i class="icon-angle-left txtpink"></i>&nbsp;Back to Messages</a> -->


<!-- top section start -->
<div class="topprosec">
	
	 {if !isset($smarty.get.me) }
	 {if !isset($profile_data.already_liked)&&!isset($profile_data.already_hide) && $profile_data.canMsg neq 'true'}
	<div class="prointhide overf">
<a url="{$profile_data.like_url}" class="matchaction like_user" name="likebox"><i class="faicon likeicobg"></i>Like</a> 

<a url="{$profile_data.hide_url}" class="matchaction hide_user" name="hidebox"><i class="faicon nopeicobg"></i>Nope</a>
   	</div>
	{/if}
	{/if}
	<!--Photo Section Start -->
  <div class="prophoto overf">
  		<a href="#">
  		{if isset($profile_data.already_liked) && $my_data.from_match eq true}
  		        <img src="{$cdnurl}/images/profile/stampliked.png" class="stampicon likeStamp">
  		{else}
        <img src="{$cdnurl}/images/profile/stampliked.png" class="stampicon likeStamp" style="display:none;">
        {/if}
        {if isset($profile_data.already_hide)}
        <img src="{$cdnurl}/images/profile/stampreject.png?v=1" class="stampicon hideStamp">
        {else}
                <img src="{$cdnurl}/images/profile/stampreject.png?v=1" class="stampicon hideStamp" style="display:none;">
        {/if}
         {if isset($profile_data.already_maybe)}
        <img src="{$cdnurl}/images/profile/stampmaybe.png?v=1" class="stampicon maybeStamp">
        {else}
                <img src="{$cdnurl}/images/profile/stampmaybe.png?v=1" class="stampicon maybeStamp" style="display:none;">
        {/if}
		<img src="{$cdnurl}/images/profile/spacer.png" class="profile-pic-img"> </a>
	<!-- <div class = "main-gallery">        
       <a href="{$profile_data.profile_pic}" class="gallerylist1"> <img src="{$profile_data.profile_pic}" width="320" height="320"></a>
       {foreach $profile_data.other_pics  item=val}
        	<a href="{$val}" class="gallerylist1"><img src="{$val}" style="display:none;"></a> 
        	{/foreach}
	</div> -->

        {if $profile_data.other_pics|@count >0}
        <div class="photothumbs gallery">
         <!--	<a href="#" class="icon-chevron-sign-left fl nextprevbtn"></a> -->
        	{foreach $profile_data.thumbnail_pics  key=index item=val}
        	<img class="thumbnail-img" index="{$index}" src="{$val}"> 
        	{/foreach}
        	
           <!--  <a href="#" class="icon-chevron-sign-right fr nextprevbtn"></a>-->
         </div>
         {/if}
         
         
         
        <!--   <ul class="gallerylist">
                    {foreach from=$attributes.thumbnails item=thumb}
                   		<li><a href="{$thumb}"><img src="{$thumb}" style='width:47px!important; height:47px!important;'></a></li>
                   		{/foreach}
                   </ul> -->
         
         
         
         
	</div>
	<!--Photo Section End -->
  
        <!-- Right Pannel Start -->
    <div class="proinfosec1">

    	<div class="prodetsec">
    <!-- Basic Edit Icon Start -->
    <a href="{$baseurl}/editprofile.php?step=basics" class="icon-edit editbasicpro"></a>
    <!-- Basic Edit Icon End -->
      <!--  <div class="lockedetail"><i class="icon-lock lockicon"></i></div> -->
        	<div class="prodetleft">
            	<h3><span id="fname" {if !isset($profile_data.msg_url) && !isset($smarty.get.me)}class="non_mutual_popup"  {/if}>{$profile_data.fname}</span>,&nbsp; {$profile_data.age}</h3>
            	<p class="proagecity clb"><span class="commahide">From {$profile_data.city} | {$profile_data.height} |  {$profile_data.marital_status} {if $profile_data.marital_status neq 'Never Married' && $profile_data.haveChildren eq 'yes'}(With Children){/if} </p>
            	<p class="prolastseen">Last active: {$profile_data.last_login}</p>
            	
            </div>
            {if isset($smarty.get.me)}
            <div class="prodetright">
                 <a onClick="document.getElementById('diffframe2').style.display='block';" class="txtblue txuline">Deactivate Profile</a>
            </div>     
                 {/if}
            
            {if !isset($smarty.get.me)}
            <div class="prodetright">
       	  	  {if isset($profile_data.mutual_connections_count)}
       	  	  <div class="fbmutfrnd {if !isset($profile_data.msg_url)}non_mutual_popup {/if}"  onMouseOver="document.getElementById('promutual').style.display='block';" onMouseOut="document.getElementById('promutual').style.display='none';" ><div class="frndscount">{$profile_data.mutual_connections_count}</div><p>Friends</p>
              
              	<div class="promutfrnd" id="promutual" style="display:none;">
              	<ul>
              		{foreach $profile_data.mutual_connections  item=val}
              		<li><img src="{$val.pic}"><p>{$val.name}</p></li>
              		{/foreach}
                </ul>
              </div>
			  </div>
       	  	  {/if}

	          {if !isset($profile_data.already_hide)}
       	  	  <a href="#"><div class="sndmsg" id = "send_msg" flag="{if isset($profile_data.canMsg)}1{else}0{/if}"><img src="{$cdnurl}/images/profile/sndmessage.png" width="41" height="36" border="0"></div></a>
       	  	{/if}

            </div>
            {/if}
        </div>
      
     <!-- Compatibility Section -->
     {if !isset($smarty.get.me) } 
        <div class="titlecompb"><img src="{$cdnurl}/images/profile/compatibility.png" align="absbottom">&nbsp;Personality match</div>
        <div class="mscoresec">
       <div class="comvaltitle">
       
       
       
	    {if  !isset($my_data.values_filled)}<p class="pgbtxt1">On values<a href="{$baseurl}/personality.php?step=psycho1&rd={$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI|urlencode}" class="taketest bgblue fr">Take</a></p>
        <div class="scalesec"> 
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb1"></div>
        </div>{/if}
            
	    {if !isset($profile_data.match_details.values) && isset($my_data.values_filled)}<p class="pgbtxt1">On values<a class="taketest bgblue fr"  id="askValues">Ask</a><a class="infotxtwhy"></a></p>
        <div class="scalesec">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb1"></div>
        </div>{/if}
	    
	   {if isset($profile_data.match_details.values) && isset($my_data.values_filled)}
	   <p class="pgbtxt1">On values</p>
	    <div class="scalesec">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb1" style="width:calc({$profile_data.match_details.values}*25%);"></div>
        </div>
        {/if}
        
        
        
        <!-- adaptability -->
        
	     {if  !isset($my_data.interpersonal_filled)} <p class="pgbtxt2">On adaptability<a href="{$baseurl}/personality.php?step=psycho2&rd={$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI|urlencode}" class="taketest bgpink fr">Take</a></p>
           <div class="scalesec">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb2"></div>
        </div>
         {/if}
         {if !isset($profile_data.match_details.adaptability) && isset($my_data.interpersonal_filled)} <p class="pgbtxt2">On adaptability<a class="taketest bgpink fr" id="askAdaptability" >Ask</a><a class="infotxtwhy"></a></p>
           <div class="scalesec">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb2"></div>
        </div>
         {/if}
	   {if isset($profile_data.match_details.adaptability) && isset($my_data.interpersonal_filled)}
	   <p class="pgbtxt2">On adaptability</p>
	    <div class="scalesec">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb2" style="width:calc({$profile_data.match_details.adaptability}*25%);"></div>
        </div>
        {/if}
       
		    
	   			 <p class="pgbtxt4">Common likes <span> ({$profile_data.common_likes|@count})</span>
                 {if !isset($my_data.favourite_filled) }
            	<a href="{$baseurl}/personality.php?step=hobby&rd={$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI|urlencode}" class="taketest bgblue fr">Fill</a>
          {/if} </p>
			    	
                    <div class="comnlikes1">
                    {if isset($my_data.favourite_filled) || ($profile_data.common_likes|@count >0)}
                    {assign var=last value=$profile_data.common_likes|@end}     
	    			{foreach $profile_data.common_likes key=i item=val}
            			<a>{$val}{if $val neq $last}, {/if}</a> 
           			{/foreach}
                    {else}
                    &nbsp;
                    {/if}
	        		</div>
	        		
	     	 
            
            
          </div>
        </div>
        
        
        {else}
         {if  !isset($my_data.values_filled) || !isset($my_data.interpersonal_filled)}
        <div class="titlecompb"><img src="{$cdnurl}/images/profile/compatibility.png" align="absbottom">&nbsp;Personality match</div>
        <div class="mscoresec">
       <div class="comvaltitle">
       {/if}
       
          {if  !isset($my_data.values_filled)}<p class="pgbtxt1">On values<a href="{$baseurl}/personality.php?step=psycho1&rd={$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI|urlencode}" class="taketest bgblue fr">Take</a></p>
        <div class="scalesec"> 
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb1"></div>
        </div>{/if}
        
         
	     {if  !isset($my_data.interpersonal_filled)} <p class="pgbtxt2">On adaptability<a href="{$baseurl}/personality.php?step=psycho2&rd={$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI|urlencode}" class="taketest bgpink fr">Take</a></p>
           <div class="scalesec">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos1">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos2">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos3">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos4">
        <img src="{$cdnurl}/images/profile/scaledot.png" class="dotpos5">
        <div class="progbar pgb2"></div>
        </div>
         {/if}
        {/if}
        
 </div>
</div>
<!-- top section end -->
</div>
<!--Centered Container end -->
</div>


</div>

<div class="new_wrapper mrg0">
<div  class="bottomsec">
<!--Centered Container Start -->
<div class="new_container btmdetails" align="center">


<div class="btmprosec"> 
<p class="btmtitle">
{if isset($smarty.get.me)}
	About Me
{else}
	About {if $my_data.gender eq 'M'}Her{else}Him{/if}
{/if}
</p>

<div class="proinfosec overf">       

<!-- <ul class="prodettxt">
            	<li>{$profile_data.religion}</li>
            	<li>{$profile_data.mother_tongue}</li>
                <li>{$profile_data.height}</li>
                <li>{$profile_data.marital_status} {if $profile_data.marital_status neq 'Never Married' && $profile_data.haveChildren eq 'yes'}(With Children){/if} </li>
                <li>No Children</li>
				<li><i class="icon-inr"></i> {$profile_data.income}
                <li> {$profile_data.smoking_status}</li>
                <li> {$profile_data.drinking_status}</li>
                <li>{$profile_data.food_status}</li>
          	</ul> -->
            <!--<hr noshade size="1" color="#e6e7e8" class="hrline"> -->
<ul class="proworkedu overf mrgt0">
 	<!-- Work Edit Icon Start -->
    <a href="{$baseurl}/editprofile.php?step=work" class="icon-edit editewhpro"></a>
    <!-- Work Edit Icon End -->
            	<li class="wewidth1"><i class="icon-briefcase wrkeduicon"></i></li><li class="wewidth2"><h4>{if isset($profile_data.industry)}{$profile_data.industry}{/if}{if isset($profile_data.designation) && isset($profile_data.industry)}, {/if}{if isset($profile_data.designation)}{$profile_data.designation}{/if}{if isset($profile_data.income)},&nbsp;<i class="icon-inr" style="font-size:14px;"></i> {$profile_data.income}{/if}</h4> 
            	<!-- Industry -->
            	<span>
            	{assign var=last value=$profile_data.companies|@end}     
            	{foreach $profile_data.companies key=i item=val}{$val}{if $val neq $last},&nbsp;{/if}{/foreach}
            	</span></li>
            </ul>
             <ul class="proworkedu overf">
    <!-- Education Edit Icon Start -->
    <a href="{$baseurl}/editprofile.php?step=education" class="icon-edit editewhpro"></a>
    <!-- Education Edit Icon End -->
           	   <li class="wewidth1"><i class="icon-graduation-cap wrkeduicon"></i></li><li class="wewidth2"><h4>{$profile_data.highest_degree}</h4> 
           	   <span>
           	   {assign var=last1 value=$profile_data.institutes|@end}     
            	{foreach $profile_data.institutes key=i item=val}{$val}{if $val neq $last1},&nbsp;{/if}{/foreach}
           	   </span></li>
            </ul>
            
            {if !empty($profile_data.InterestsHobbies.preferences)}
           <ul class="proworkedu overf">
   <!-- Interest Edit Icon Start -->
    <a href="{$baseurl}/editprofile.php?step=interest" class="icon-edit editewhpro"></a>
    <!-- Interest Edit Icon End -->
           	   <li class="wewidth1"><div class="phashtag">#</div></li>
               <li class="wewidth2">
               {foreach $profile_data.InterestsHobbies.preferences item=val}
				<p>{$val}&nbsp;</p>
				{/foreach}
                </li>
               </ul>
               {/if}
               
               
               {if !empty($profile_data.favourite) }
                <ul class="proworkedu overf">
    <!-- Favourite Edit Icon Start -->
    <a href="{$baseurl}/personality.php?step=hobby" class="icon-edit editewhpro"></a>
    <!-- Favourite Edit Icon End -->
           	   <li class="wewidth1"  style="margin:10px 0;"><i class="icon-star wrkeduicon"></i></li>
               <li class="wewidth2"><div class="inhsec overf">
            	<div class="inhtiles overf">

               <!-- <ul class="inhtilessec overf">
				{foreach $profile_data.InterestsHobbies.preferences item=val}
				<li>{$val}</li>
				{/foreach}
           		</ul> -->
           		
           		{if !empty($profile_data.favourite) }
                	<div class="clb">
                    	<ul class="inticons">
                        	<li class="intmovie"><span class="hijack" key="0" onClick="document.getElementById('arrowtop').style.left='14px';"></span></li>
                        	<li class="intmusic"><span class="hijack" key="1" onClick="document.getElementById('arrowtop').style.left='60px';"></span></li>
                        	<li class="intbooks"><span class="hijack" key="2" onClick="document.getElementById('arrowtop').style.left='106px';"></span></li>
                        	<li class="intfood"><span class="hijack" key="3" onClick="document.getElementById('arrowtop').style.left='149px';"></span></li>
                        	<li class="intother"><span class="hijack" key="4" onClick="document.getElementById('arrowtop').style.left='190px';"></span></li>
                    	</ul>
                        
                        <div class="posrel">
                        <img src="{$cdnurl}/images/profile/arrowpointertab.png" border="0" class="indicatetop" id="arrowtop" style="left:14px;">

							<div class="comnlikes">
                            {if isset($profile_data.InterestsHobbies.movies_favorites)}
                        	
           					<span class="colorcode1" name="movie" value="0">
           					{assign var=last2 value=$profile_data.InterestsHobbies.movies_favorites|@end}     
	                		{foreach $profile_data.InterestsHobbies.movies_favorites item=val}
							{$val}
							{if $val neq $last2},{/if}
							{/foreach}
                            </span>
                            {/if}
                        
                        <span class="seperaterline">|</span>
                            {if isset($profile_data.InterestsHobbies.music_favorites)}
                            
                			<span class="colorcode2" name="music" value="1">
           				    {assign var=last3 value=$profile_data.InterestsHobbies.music_favorites|@end}     
                			{foreach $profile_data.InterestsHobbies.music_favorites item=val}
							{$val}
							{if $val neq $last3},{/if}
							{/foreach}
                            </span>
							{/if}
                            
                            <span class="seperaterline">|</span>
                			{if isset($profile_data.InterestsHobbies.books_favorites)}
                           
                			<span class="colorcode1" name="books" value="2">
                			{assign var=last4 value=$profile_data.InterestsHobbies.books_favorites|@end}     
                			{foreach $profile_data.InterestsHobbies.books_favorites item=val}
							{$val}							{if $val neq $last4},{/if}
							{/foreach}
                            </span>
                            
                            {/if}

							<span class="seperaterline">|</span>                            
                 			{if isset($profile_data.InterestsHobbies.travel_favorites)}
                           
                			<span class="colorcode2" name="food" value="3">
							{assign var=last4 value=$profile_data.InterestsHobbies.travel_favorites|@end}     
	                		{foreach $profile_data.InterestsHobbies.travel_favorites item=val}
							{$val}{if $val neq $last4},{/if}
							{/foreach}
                            </span>
                            {/if}
                            
                            <span class="seperaterline">|</span>
                			{if isset($profile_data.InterestsHobbies.other_favorites)}                            
                            
                			<span class="colorcode1" name="others" value="4">
                			{assign var=last4 value=$profile_data.InterestsHobbies.other_favorites|@end}     
	                		{foreach $profile_data.InterestsHobbies.other_favorites item=val}
							{$val}{if $val neq $last4},{/if}
							{/foreach}
                			</span>
                			{/if}
                            </div>
                       </div>
                        
                	</div>
                   {/if}  
                </div>
        	</div></li>
               </ul>
               {/if}
               
            
         
    
    </div>
    <!-- Right Pannel End -->
     <!-- Trust Score Section start -->
    <div class="protscore overf">
    <!-- Trustbuilder Edit Icon Start -->
    <a href="{$baseurl}/trustbuilder.php" class="icon-edit editewhpro"></a>
    <!-- Trustbuilder Edit Icon End -->
    	<div class="overf">
    		<div class="pieContainer">
    		 	<div class="pieBackground"></div>
     			<div class="hold" id="pieSlice1">
                	<!-- <div class="pie" style="-webkit-transform:rotate(-9deg);  -moz-transform:rotate(-9deg); transform: rotate(-9deg);"></div> -->
            	
            	{if isset($profile_data.trustMeter.trust_score)}
     <div id="pieSlice1" class="hold"><div class="pie" style="-webkit-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $profile_data.trustMeter.trust_score}deg); -moz-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $profile_data.trustMeter.trust_score}deg); -o-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $profile_data.trustMeter.trust_score}deg); transform:rotate({math equation='(( x *1.8 ) -90 )' x= $profile_data.trustMeter.trust_score}deg);"></div></div>
     <div class="persentbg"><p>{$profile_data.trustMeter.trust_score}%</p></div>
     {else}
      <div id="pieSlice1" class="hold"><div class="pie" style="-webkit-transform:rotate(-90deg); -moz-transform:rotate(-90deg); -o-transform:rotate(-90deg); transform:rotate(-90deg);"></div></div>
     <div class="persentbg"><p>0%</p></div>
     {/if}
     
     </div>
     		</div>
    		<p class="protstitle">TRUST SCORE</p>
    	</div>
    	<ul class="scoreplus">
      
        
        {if isset($profile_data.trustMeter.fb_connections)}
        <li class="activets"><i class="icon-facebook-sign protsicons"></i><p>{$profile_data.trustMeter.fb_connections} Friends</p></li>
       {else}
        <li><i class="icon-facebook-sign protsicons"></i><p>Facebook</p></li>
        {/if}
        
        {if isset($profile_data.trustMeter.linkedin_connections)}
        <li class="activets"><i class="icon-linkedin-sign protsicons"></i><p>{if isset($profile_data.trustMeter.linkedin_designation)}{$profile_data.trustMeter.linkedin_designation}{/if}</p></li>
		{else}
        <li><i class="icon-linkedin-sign protsicons"></i><p>Linkedin</p></li>
       {/if}
       
        {if isset($profile_data.trustMeter.id_proof)}
        <li class="activets"><i class="icon-user protsicons"></i><p>{if isset($profile_data.trustMeter.id_proof_type)}{$profile_data.trustMeter.id_proof_type}{/if}</p></li>
        {else}
        <li><i class="icon-user protsicons"></i><p>Photo ID</p></li>
        {/if}
        
          {if isset($profile_data.trustMeter.mobile_number)}
        <li class="activets"><i class="icon-phone-sign protsicons"></i><p>{$profile_data.trustMeter.mobile_number}</p></li>
       {else}
        <li><i class="icon-phone-sign protsicons"></i><p>Contact Number</p></li>
        {/if}
        
        
       <!--  {if isset($profile_data.trustMeter.address)}
        <li class="activets"><i class="icon-home protsicons"></i><p>Address Verified</p></li>
		{else}
        <li><i class="icon-home protsicons"></i><p>Address</p></li>
        {/if} -->
        
        {if isset($profile_data.trustMeter.employment)}
        <li class="activets"><i class="icon-briefcase protsicons"></i><p>{if isset($profile_data.trustMeter.employment_company)}{$profile_data.trustMeter.employment_company}{/if}</p></li>
        
		{/if}
        
               {if isset($profile_data.is_endorsement_verified) && $profile_data.is_endorsement_verified eq 'true'}
       <li class="activets"><i class="icon-ok-circle protsicons"></i><p>Endorsed by</p></li>
		
		{else}
				       <li><i class="icon-ok-circle protsicons"></i><p>{if isset($profile_data.endorsementData)}Endorsed by{else}Endorsement{/if}</p></li>
		{/if}
        </ul>
        <!-- Endorsment List -->
        {if isset($profile_data.endorsementData)}
        <div class="endorssec">
			<div class="slider_wrapper">
				<ul id="image_slider">
				{foreach $profile_data.endorsementData item=val}
					<li><img src="{$val.pic}"><p>{$val.fname}</p></li>
				{/foreach} 
				</ul>					
				
			</div>
            <span class="nvgt" id="prev" style="display:none"></span>
				<span class="nvgt" id="next"></span>
		</div>
		{/if}
        <!-- Endosrment List end -->
         
    </div>
    <!-- Trust Score end -->
</div>   
</div>
<!--Centered Container End -->
</div>
</div>

<!-- Pop Up Start -->
<div id="diffframe4" style="display:none;" >
	<div class="difusescreen" align="center" onClick="document.getElementById('diffframe4').style.display='none';"></div>
	<div class="popupframe" >
	<a onClick="document.getElementById('diffframe4').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
	<p>
		{if isset($systemMsgs.like_hide_action_msg)}{$systemMsgs.like_hide_action_msg}{/if}
	</p>
	<p>		<a href="{$baseurl}/trustbuilder.php" class="actionbtn">Verify Now</a></p>
	
	</div>
</div>

<div id="diffframe5" style="display:none;" >
<div class="difusescreen" align="center" onClick="document.getElementById('diffframe5').style.display='none';"></div>
<div class="popupframe">
<a onClick="document.getElementById('diffframe5').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<p>
	{if isset($systemMsgs.not_mutual_like_msg)}{$systemMsgs.not_mutual_like_msg}{/if}
</p>

</div>
</div>
<!-- Pop Up end -->


<!-- Deactivate Pop Up Start -->
<div id="diffframe2" style="display:none;" >
<div class="difusescreen" align="center" onClick="document.getElementById('diffframe2').style.display='none';"></div>
<div class="popupframe">
<a onClick="document.getElementById('diffframe2').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>

   <form action="{$baseurl}/logging/deactivateProfile.php" id="form1" method="post" name="lg-form" novalidate='novalidate'>
 <p>Sure you want to deactivate? Tell us why:<br><span class="prolastseen txtpink">NOTE: If you log back in, your account will be activated again.</span></p>
 <div class="regselect-style">
<select id="profileDeletionSelect" name="reason" onChange="deactForm()">
<option selected="selected" disabled="disabled" value="0">{$systemMsgs.deactivation_msgs.heading}</option>
{foreach $systemMsgs.deactivation_msgs.reasons as $val}
<option>{$val}</option>
{/foreach}
</select>
</div>
<textarea id="reasonDesc" name="reason_desc" style="display:none;" class="regtxtbox fl" placeholder="Type Reason"></textarea>
<span id ="error_deactivate" class="txtred fl clb" style="margin-left:20px;"></span>

<p><input type="submit" value="Deactivate" id='deactivateSubmit' class="actionbtn"></p>

</form>


</div>
</div>
<!-- Deactivate Pop Up End -->



<!-- Ask Pop up -->
<div id="diffframe6" style="display:none;" >
<div class="difusescreen" align="center" onClick="document.getElementById('diffframe6').style.display='none';"></div>
<div class="popupframe">
<a onClick="document.getElementById('diffframe6').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<p>
	Success! Your request is on its way.
</p>

</div>
</div>
<!-- Ask Pup up end -->

<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="{$cdnurl}/js/Image-Slider.js"></script>
<script src="{$cdnurl}/js/common/common.js?v=1"></script> 
<script src="{$cdnurl}/js/dashboard/jquery.magnific-popup.js"></script>
<script src="{$cdnurl}/js/lib/tm_core_new.js"></script>


<script>
$(document).ready(function() {
	deactivateFlag = 0;
	actionFlag = 0; 
	reasonFlag =0;

	if(isMyProfile == 1){
		$('.icon-edit').show();
	}

	finalTemplateRenderTime  = (new Date()).getTime();
	//console.log(myId);
	
	{literal}
	matchObj = {"data":{"time_taken":(finalTemplateRenderTime - headLoadTimeStamp)/1000, "activity":"profile", "user_id":myId, "event_type":"page_load" , "event_info":{"profile_url" : document.URL}, "source":"web"}};
	{/literal}
	logEvent(matchObj);
		
	
 $( ".prophoto" ).css("background-image", 'url('+profilePicUrl+')');
 //console.log($( ".prophoto" ));
	values_array = $.cookie('values');
	if(values_array){
		values_array = values_array.split(',');
		if($.inArray(profileId,values_array) > -1){
			//console.log(profileId);
			//console.log(values_array);
			$('#askValues').hide();
		}
	}

	adap_array = $.cookie('adaptability');
	if(adap_array){
		adap_array = adap_array.split(',');
		if($.inArray(profileId,adap_array) > -1){
			$('#askAdaptability').hide();
		}
	}


	$('#deactivateSubmit').click(function(event){
		reasonFlag =0;
		if($("#profileDeletionSelect option:selected").val() == 0){
			$("#error_deactivate").html("Please enter a reason.");
			reasonFlag =1;
			event.preventDefault();
		}
		if($("#profileDeletionSelect option:selected").val() == "Other"){
			console.log($('#reasonDesc').val().length);
			if($('#reasonDesc').val().trim().length<=0){
				console.log("in othere" );
				$("#error_deactivate").html("Please enter a reason.");
				reasonFlag =1;
				event.preventDefault();
			} 
		}
console.log(deactivateFlag);

		if(reasonFlag == 0){
			if(deactivateFlag == 1){
					event.preventDefault();
			}else{
				deactivateFlag = 1;
				{literal}
				GAObj = {"data":{"activity":"profile", "event_type":"deactivate"}};
				{/literal}
				logGA(GAObj);
			}
		}
		
	});
	
	
	$('.hijack').click(function(event){
		var left = 0;
		var key = parseInt($(event.target).attr('key'), 10);
		if(key>0){
			left = 30;
		}
		for(var i=0;i<key;i++){
			left += $('.comnlikes [value="'+i+'"]').width();
		}
		$('.comnlikes').scrollLeft(left);
	});

	allPicsObject = [];
	for (var i=0;i<allImgSrcUrls.length;i++){
		allPicsObject[i] = {
				src:allImgSrcUrls[i],
				type: 'image'
				};
	}
	if(hasProfile){
		$('.profile-pic-img').click(function(event){
			showPopup(allImgSrcUrls.length - 1);
		});
	}
	$('.thumbnail-img').click(function(event){
		showPopup(parseInt($(event.target).attr('index'),10));
	});
	function showPopup(index){
		$.magnificPopup.open({
	    	items: allPicsObject,
	          gallery: {
	            enabled: true,
	            navigateByImgClick : true,
				preload : [ 0, 1 ]
	          },
	          type: 'image',
	          callbacks: {
	        	    open: function(){
	        	  $(document).bind('touchmove', function(e) {
	        	       e.preventDefault();
	        	});
	        	    },
	        	    close: function(){
	        	    	$(document).unbind('touchmove');
		        	    }
	        	  }
	      }, index);
	}
});

function popupOpened(){
	$('#wrapper_container').hide();
}
function popupClosed(){
	$('#wrapper_container').show();
}
</script>

<script>
var wls = window.localStorage;
$('.prointhide').on('click', '.like_user', function(e){ 
	var elm = $(e.currentTarget); 
	var likeurl = elm.attr('url');
	//console.log(likeurl);

	if(likeurl){
		if(actionFlag == 0){
		actionFlag = 1;
	$.ajax({
		  url: likeurl
		}).done(function(dataJson) {
			//console.log(dataJson);
			var data = JSON.parse(dataJson);
			if(data.return_action){
				if(data.return_action["mutualLike"]){
					$('#mutualLike').html('<p>'+data.return_action["mutualLike"]+'</p>');
					msg_url = data.msg_url;
					var fname = data.name;
					$('#fname').html(fname);
					//alert("mutual like:" + data.return_action["mutualLike"]);
				}
			}
			/*if(data.flag){
			//	console.log(data.flag);
				msg_url = data.msg_url;
				var fname = data.name;
				$('#fname').html(fname);
			}*/
			$('.prointhide').hide();
			$('.maybeStamp').hide();
			
			$('.likeStamp').show();
		
			console.log('success');
			{literal}
			GAObj = {"data":{"activity":"profile", "event_type":"like"}};
			{/literal}
			logGA(GAObj);
			moveToHistory(true); 
			
			
		});}
	}
	else{
		$('#diffframe4').show();
	}
	
	}); 



$('.prointhide').on('click', '.hide_user, .maybe_user', function(e){
	var elm = $(e.currentTarget);
	var hideurl = elm.attr('url');
	var classClicked = elm.attr('class');
	console.log(hideurl);
	console.log(classClicked);
if(hideurl){
	if(actionFlag == 0){
		actionFlag = 1;
		
	$.ajax({
		  url: hideurl
		}).done(function() {
			$('.prointhide').hide();

			if(classClicked.indexOf("hide")>0){

				$('.maybeStamp').hide();
				
			$('.hideStamp').show();
			event = "hide";
			}
			else if(classClicked.indexOf("may")>0){
			$('.maybeStamp').show();
			event="maybe";
			if($('#bookmark').html() == "")
				var oldCountMaybe = 1;
			else{
				//var oldCountMaybe = parseInt($('#bookmark').html()) + 1;
				var r = /\d+/;
				var s = $('#bookmark').html();
				var oldCountMaybe = parseInt(s.match(r)) + 1;
			}
			$('#bookmark').html('('+ oldCountMaybe+ ')');
			}

			
			console.log('success');
			{literal}
			GAObj = {"data":{"activity":"profile", "event_type":event}};
			{/literal}
			logGA(GAObj);
			moveToHistory(true); 
			
		});
}}
	else{
		$('#diffframe4').show();
		}
	}); 


$('.non_mutual_popup').on('click',function(e){
	
if(msg_url){
	window.location = msg_url;
}
	else{
		$('#diffframe5').show();
		}
	});


$("#askValues, #askAdaptability").on('click', function(e){
	var elm = $(e.currentTarget); 
	//console.log(elm);
	$('#diffframe6').show();
	var flag = elm.attr('id');
	if(flag == "askValues"){
		 values_arr = $.cookie("values"); 
		if(values_arr){
			values_arr = values_arr.split(',');
			values_arr.push(profileId);
			$.removeCookie("values");
			$.cookie("values", values_arr);
		}
		else{
			var newValArr = new Array (profileId);
			$.cookie("values",newValArr);
		}
	}

	if(flag == "askAdaptability"){
		 values_arr = $.cookie("adaptability"); 
		if(values_arr){
			values_arr = values_arr.split(',');
			values_arr.push(profileId);
			$.removeCookie("adaptability");
			$.cookie("adaptability", values_arr);
		}
		else{
			var newValArr = new Array (profileId);
			$.cookie("adaptability",newValArr);
		}
	}

	
	elm.hide();
});

$('#send_msg').on('click', function(e){
	if(msg_url){
		window.location = msg_url;
	}
	else{
		$('#diffframe5').show();
	}
		
});
$('.backlink').on('click', function(e){
	moveToHistory();
//	parent.history.back();
});

</script>



<script>



function deactForm(){
	$("#error_deactivate").html("");
	
	var option1 =  $("#profileDeletionSelect option:selected").text();
	if(option1 == 'Other'){	
		$("#reasonDesc").css("display", "block");
	}
	else{
		$("#reasonDesc").css("display", "none");
	}
}

function moveToHistory(delay){

	
	var timeOut;
	if(delay == true){
		 timeOut = 2000;

			
	}
	else timeOut = 0;
	
	var history = document.referrer;

	
	setTimeout(function() {
		var redirection = "{$cdnurl}" + "/matches.php";
		if(history.indexOf("messages.php") > -1 || history.indexOf("message_full_conv.php") > -1 || history.indexOf("likes.php") > -1 || history.indexOf("bookmark.php") > -1 ){
			redirection = history;
		}		
		else{
			if(delay==true){
				if(wls && wls['CURRENT_SLIDE_INDEX']){
					var index = parseInt(wls['CURRENT_SLIDE_INDEX'], 10);
					index++;
					wls['CURRENT_SLIDE_INDEX'] = index;
				}
			}
		} 
		window.location.href= redirection;
		}, parseInt(timeOut));
	
	
}
</script>

{include file='templates/common/footer.tpl'}

</body>
</html>
