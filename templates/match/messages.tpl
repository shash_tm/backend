<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>TrulyMadly - My Messages</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
//console.log(headLoadTimeStamp);
</script>
<link href="{$cdnurl}/css/match/matches.css?v=7" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css?v=2" rel="stylesheet" type="text/css">
</head>

<body>
<script>
var baseurl = "{$baseurl}";
var myId = "{$my_id}";
</script>

{include file='../../templates/common/header.tpl'}

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">

{assign var="is_arr_empty" value="0"}

<!--Messages Section Start -->
<div class="mutmatchsec">
<div class="mmwraper">
<h3>My Messages</h3>
 <!-- Messages Tiles Start -->
   
   {foreach $arr.data key=i item=val}
   {if $val.message_list.is_blocked_shown eq "0" && isset($val.message_list.blocked_by)}
<!-- Blocked match -->
{assign var="is_arr_empty" value="1"}

<div class="mutual_like_blocked_div" id="blockshown_{if $arr.user_id eq $val.message_list.sender_id}{$val.message_list.receiver_id}{else}{$val.message_list.sender_id}{/if}">
<ul class="msgblue">
    		<span>{$val.message_list.timestamp}</span>
    <li class="msgtd1 fl"><img src="{$val.message_list.profile_pic}"></li>
	{if $val.message_list.select_status neq null}
		<li class="msgtd6 fl"><p>   {$val.message_list.select_status}</p></li>
	{/if}
    <li class="msgtd2 fl"><h4>{$val.message_list.fname}</h4><p>{$val.message_list.message}</p>
<li class="mmtd3 fr"><img src="{$cdnurl}/images/profile/blockedicon.png"></li>
</ul>
</div>
{else}

		{assign var="is_arr_empty" value="1"}

     {if $arr.user_id eq $val.message_list.sender_id}
     
     <ul class="msgblue blueClass" msgurl={$val.message_list.full_conv_link}>
    <span>{$val.message_list.timestamp}</span>
    <li class="msgtd1 fl"><img src="{$val.message_list.profile_pic}"></li>
		 {if $val.message_list.select_status neq null}
			 <li class="msgtd6 fl"><p>{$val.message_list.select_status}</p></li>
		 {/if}
    <li class="msgtd2 fl"><h4>{$val.message_list.fname}</h4><p>{$val.message_list.message}</p><i class="fr msgsend"></i>
    </li>
    <li class="msgtd3 fr"><p><a href={$val.message_list.full_conv_link}><i class="icon-chevron-sign-right"></i></a></p></li>
	    {if $val.message_list.spark_count neq null}
			<li class="msgtd5 fl"><p>Sparks- {$val.message_list.spark_count}</p></li>
		 {/if}
    </ul>
    
	{else}
	
	
		{if $val.message_list.seen eq false}
		
    		<ul class="msgblue msgdarkblue blueClass" msgurl={$val.message_list.full_conv_link}>
    	{else}
    	   <ul class="msgblue blueClass" msgurl={$val.message_list.full_conv_link}>
    	{/if}  
    		<span>{$val.message_list.timestamp}</span>
   			<li class="msgtd1 fl"><img src="{$val.message_list.profile_pic}"></li>
			   {if $val.message_list.select_status neq null}
				   <li class="msgtd6 fl"><p>{$val.message_list.select_status}</p></li>
			   {/if}
   			<li class="msgtd2 fl"><h4>{$val.message_list.fname}</h4><p>{$val.message_list.message}</p><i class="fr msgreceive"></i>
    		</li>
    		<li class="msgtd3 fr"><p><a href={$val.message_list.full_conv_link}><i class="icon-chevron-sign-right"></i></a></p></li>
			   {if $val.message_list.spark_count neq null}
				   <li class="msgtd5 fl"><p>Sparks- {$val.message_list.spark_count}</p></li>
			   {/if}
    		</ul>
    	
    	<!-- 	<ul class="msgblue">
    		<span>21-Jun-14 15:50</span>
    		<li class="msgtd1 fl"><img src="{$cdnurl}/images/profile/profilepic.jpg"></li>
    		<li class="msgtd2 fl"><h4>Karan</h4><p>Hi, This is a dummy copy please don't read this  copy please don't read this</p><i class="fr msgsend"></i>
    		</li>
    		<li class="msgtd3 fr"><p><i class="icon-chevron-sign-right"></i></p></li>
    		</ul> -->
    	
  	{/if}
  	
  {/if}	
   {/foreach}     
   {if $is_arr_empty eq "0"}
    
    	<div class="emptymsg">Uho! Nothing to see here.</div>
    	{/if}
  
<!-- Messages Tiles end -->
</div>
</div>
<!--Messages section end -->

</div>
<!--Centered Container End -->
</div>




<!-- Pop Up Start -->
<div id="diffframe10" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="mmpopupframe">
<p>Match is either not active anymore or has decided not to proceed further<br><br>
<a   class="mmactionbtn">OK</a></p>
</div>
</div>
<!-- Pop Up end -->


<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/common/common.js?v=1"></script>



<script>
$(document).ready(function() {
	finalTemplateRenderTime  = (new Date()).getTime();

	{literal}
	matchObj = {"data":{"time_taken":(finalTemplateRenderTime - headLoadTimeStamp)/1000, "activity":"messages", "user_id":myId, "event_type":"page_load" , "source":"web"}};
	{/literal}
	logEvent(matchObj);
});
</script>
	
<script>
$('.blueClass').on('click', function(e) {
	var elm = $(e.currentTarget);
	console.log(elm);
	var msgurl = elm.attr('msgurl');
	
	window.location =msgurl;
});


$('.mutual_like_blocked_div').on('click', function(e){
	$('#diffframe10').show();
	 elem = $(e.currentTarget);
		console.log(elem);
	 blockShownDiv = elem.attr('id');
	 splitDiv = blockShownDiv.split("_");
	 blockedId = splitDiv[1];
});

$('#diffframe10').click(function(){
	$('#diffframe10').hide();
	$("#"+blockShownDiv).fadeOut("slow");
	var pathname = window.location.pathname;
	console.log(pathname);
	if(blockedId){
		$.post(pathname, {
			block_shown : 'block_shown',
			block_shown_id : blockedId
		}, function(data, status) {
			if(data == "success")
			blockShownDiv = null;
		});
	}
});
</script>

{include file='../../templates/common/footer.tpl'}

</body>
</html>
