<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Message Conversation</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
</script>

<link href="{$cdnurl}/css/match/matches.css?v=3.1" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css?v=3.1" rel="stylesheet" type="text/css">

</head>

<body>
{include file='../../templates/common/header.tpl'}

<script>
cdnUrl = "{$cdnurl}";
var baseurl = "{$baseurl}";
var myId = "{$my_id}";
var matchId = "{$match_id}"
messagesLink = "{$cdnurl}/msg/messages.php";
var blockcall = 0;
last_msg_id = "{$last_msg_id}"
</script>

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
{if $isAdmin eq "true"}<div class="pagination"><h3>Feedback</h3></div>{/if}
{if $output.is_blocked eq true  || $is_blocked eq true}
<div class="verifyinfobox">Either of you don't strike the chord.</div>
{/if}
<!--Messages Section Start -->
<div class="msgthreadsec">
<div class="msgsndrtile" style="position: relative; margin:0 auto; text-align:center;">
				
		
		
		{if $isAdmin eq "true"}
		
		
		<h4>&nbsp;&nbsp;&nbsp;Trulymadly Admin</h4>
		{else}
		<p class="backmsgs">
			<a href ="{$baseurl}/msg/messages.php"><i class="icon-chevron-sign-left"></i></a> 
		</p>
		<a href = "{$output.receiver.profile_link}"><img src="{$output.receiver.profile_pic}" class="sndrimg"></a>
			<h4><a href = "{$output.receiver.profile_link}">{$output.receiver.fname}</a></h4>

			{if $iAmAdmin eq "true"}
			<button id ="seen_update" style=" margin: 10px 10% 0 0; text-align: center; width: 80px;">Seen</button>
		{/if}
			
		{if !isset($output.blocked_by) &&   $is_blocked neq true}
		<div class="msgaction fr" onMouseOver="document.getElementById('actionblock').style.display='block';" onMouseOut="document.getElementById('actionblock').style.display='none';"><i class="icon-ellipsis-vertical"></i><ul id="actionblock" style="display:none;"><a href="{$output.receiver.profile_link}"><li id="view_user_prof">View Profile</li></a><a><li id="report_abuse">Block User</li></a></ul></div>
		{/if}
		{/if}
		
		
</div>

<!-- Message Thread start -->
<div class="msgwraper">

{assign var="currentDate" value=""}
	{if $subs.select_status neq null}
	<h4>Select - {$subs.select_status}</h4>
	{/if}
{if $subs.spark_count neq null}
	<h4>Sparks - {$subs.spark_count} </h4>
{/if}
{foreach $output.message_list item=val}

	{if $val.time|date_format neq $currentDate} 
			<p class="msgdatetime">
			{assign var="currentDate" value=$val.time|date_format}
			{if $currentDate eq $smarty.now|date_format}Today{else} {$val.time|date_format}{/if}
			</p>
	{/if}

{if $val.sender eq 'Me'}
	<div class="sendmsg">
	{if $val.message_type eq 'IMAGE'}
		<img src="{$val.metadata.urls.jpeg}" align="right" height="300"  class="imagemsg">
	{else}
		<img src="{$cdnurl}/images/profile/sendarrow.png" class="sndicon">{$val.msg}
	{/if}

		{if $val.seen eq '1'}
			<p class="msgactico txtgray1"><i class="msgtyperead"></i>{$val.time|date_format:'%I:%M %p'}</p>
		{else}
			<p class="msgactico txtgray1"><i class="msgtypesnd"></i>{$val.time|date_format:'%I:%M %p'}</p>
		{/if} 
	</div>
{else}
	<div class="receivedmsg">
	{if $val.message_type eq 'IMAGE'}
		<img src="{$val.metadata.urls.jpeg}" align="left" height="300" class="imagemsg">
	{else}
	<img src="{$cdnurl}/images/profile/receivedarrow.png" class="recvicon">{$val.msg}
	{/if}
	<p class="msgactico txtgray">{$val.time|date_format:'%I:%M %p'}</p>
	</div>
{/if}
{/foreach}
<!-- 
<div class="receivedmsg"><img src="{$cdnurl}/images/profile/receivedarrow.png" class="recvicon">Hello, This is a dummy copy please don't read this.</div>
<div class="sendmsg"><img src="{$cdnurl}/images/profile/sendarrow.png" class="sndicon">Hello, This is a dummy copy please don't read this.</div>
<div class="sendmsg"><img src="{$cdnurl}/images/profile/sendarrow.png" class="sndicon">Hello, This is a dummy copy please don't read this.</div>
<div class="receivedmsg"><img src="{$cdnurl}/images/profile/receivedarrow.png" class="recvicon">Hello, This is a dummy copy please don't read this.</div>
<div class="receivedmsg"><img src="{$cdnurl}/images/profile/receivedarrow.png" class="recvicon">Hello, This is a dummy copy please don't read this.</div>
<p class="msgdatetime">21-Jun-14 18:22</p>
<div class="receivedmsg"><img src="{$cdnurl}/images/profile/receivedarrow.png" class="recvicon">Hello, This is a dummy copy please don't read this.</div>
<div class="sendmsg"><img src="{$cdnurl}/images/profile/sendarrow.png" class="sndicon">Hello, This is a dummy copy please don't read this.</div>
<div class="receivedmsg"><img src="{$cdnurl}/images/profile/receivedarrow.png" class="recvicon">Hello, This is a dummy copy please don't read this.</div>
<div class="sendmsg"><img src="{$cdnurl}/images/profile/sendarrow.png" class="sndicon">Hello, This is a dummy copy please don't read this.</div>
 -->
</div>
<!-- Message Thread start -->

<div class="msgsendingbox">
	<div class="msgtypebox"><img src="{$cdnurl}/images/profile/sendarrow1.png" class="sndicon">
		<input name="message" type="text" {if $output.is_blocked eq true || $is_blocked eq true } disabled {/if}class="msgtypearea"></div>

	<a class="msgsndbtn">Send</a>
		<label for="files" class="imgsndbtn">Image</label>
		<input id="files" style="visibility:hidden;" type="file" accept="image/x-png, image/gif, image/jpeg">

</div>
</div>
<!--Messages section end -->

</div>
<!--Centered Container End -->
</div>

<!-- Pop Up Start -->
<div id="diffframe10" style="display:none;">
<div class="difusescreen" align="center" onClick="document.getElementById('diffframe10').style.display='none';"></div>
<div class="mmpopupframe">
<a href="javascript:void(0);" onClick="document.getElementById('diffframe10').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<p>Are you sure you want to block this profile?<br><br>
<a  href="javascript:void(0);" class="mmactionbtn" id="call_block">Block</a></p>
</div>
</div>
<!-- Pop Up end -->



<!-- Report Abuse Pop Up -->
<div id="diffframe_abuse" style="display:none;">
<div class="difusescreen" align="center" onClick="document.getElementById('diffframe_abuse').style.display='none';"></div>
<div class="mmpopupframe">
<a href="javascript:void(0);" onClick="document.getElementById('diffframe_abuse').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<div class="rabusform">
<form id = "blockform" name="myForm">
Why would you like to block this TrulyMadly member?
<ul>
{foreach from=$block_reasons key=k item=v}
<li><input name="usrblock" type="radio" id = "userblock{$k}" value="usrblock{$k}" class="fl mrg1" /> <label class="fl mrg whspn" for="userblock{$k}">{$v}</label>
	{if $k eq '2'}
		<textarea cols="30" id ="textAreaBlock1" rows="1" placeholder="Let us know what is incorrect" class="otrrsntxtarea fl" description="desc" style="display:none;" ></textarea></li>
	{/if}
	{if $k eq '3'}
		<textarea cols="30" id ="textAreaBlock2" rows="1" placeholder="Type Reason" class="otrrsntxtarea fl" description="desc" style="display:none;"></textarea></li>
	{/if}
	</li>
{/foreach}
<!--<li><input name="usrblock" type="radio" id = "userblock1" value="usrblock1" class="fl mrg1" /> <label class="fl mrg whspn" for="userblock1">Has sent abusive email/message</label></li>
<li><input name="usrblock" type="radio" id = "userblock2" value="usrblock2" class="fl mrg1" /><label class="fl mrg whspn" for="userblock2">Misleading profile information</label></li>
<li><input name="usrblock" type="radio" id = "userblock3" value="usrblock3" class="fl mrg1" /><label class="fl mrg whspn" for="userblock3">Inappropriate content</label></li>
<li><input name="usrblock" type="radio" id = "userblock4" value="usrblock4" class="fl mrg1" /><label class="fl mrg whspn" for="userblock4">Already married/engaged</label></li>
<li><input name="usrblock" type="radio" id = "userblock5" value="usrblock5" class="fl mrg1" /><label class="fl mrg whspn" for="userblock5">Harassment</label></li>
<li><input name="usrblock" type="radio" id = "userblock6" value="usrblock6" class="fl mrg1" /><label class="fl mrg whspn" for="userblock6">Asking for Money</label></li>
<li><input name="usrblock" type="radio" id = "userblock7" value="usrblock7" class="fl mrg1" /><label class="fl mrg whspn" for="userblock7">Other</label>
		<textarea cols="30" id ="textAreaBlock" rows="1" placeholder="Type reason" class="otrrsntxtarea fl" style="display:none;"></textarea></li>
--></ul>

        </form>
        <a href="javascript:void(0);" class="mmactionbtn" id="confirm_abuse" >Confirm</a>
        </div>
       
    </div>
</div>
<!-- Pop up end -->




<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/common/common.js?v=1"></script>



<script>

$(window).load(function(e){
	$('.msgwraper').scrollTop($('.msgwraper')[0].scrollHeight);
});

$(document).ready(function(){
	finalTemplateRenderTime  = (new Date()).getTime();

	{literal}
	matchObj = {"data":{"time_taken":(finalTemplateRenderTime - headLoadTimeStamp)/1000, "activity":"message_full_conversation", "user_id":myId, "event_type":"page_load" , "event_info":{"message_conv_url": document.URL}, "source":"web"}};
	{/literal}
	logEvent(matchObj);

	
	 post_url = $(location).attr('href');

	 
	 
	 $('#call_block').click(function(){
		console.log('block');
		$.post(post_url,
			{
				type : 'block_user'
			},	function(data, status) {
				//		alert("user blocked");
				{literal}
				GAObj = {"data":{"activity":"message_full_conversation", "event_type":"block"}};
				{/literal}
				logGA(GAObj);
						$('#diffframe10').hide();
						window.location = messagesLink;
			});
		});

	$("input[name=usrblock]").change(function() {
		 text = null;
		if($(this).is(':checked')){
			var lid = $(this).attr("id");
			//console.log(lid);
			if(lid == "userblock2" ){
				$('#textAreaBlock2').hide();
				$('#textAreaBlock1').show();
			}else if(lid == "userblock3"){
				$('#textAreaBlock1').hide();
				$('#textAreaBlock2').show();
			}
			else{
				$('#textAreaBlock1').hide();
				$('#textAreaBlock2').hide();
			}
		}
	});
	

	$('#confirm_abuse').click(function() {
		if (blockcall == 0){
			
		var lid = $("input[name=usrblock]:checked").attr("id");
		var post_url = window.location.href;
		var desc = null;
		if (lid == "userblock2" || lid == "userblock3") {
			desc = $('[description="desc"]:visible').val();
			desc = desc.trim();
			if(desc.length<=0){
				alert("please enter a reason");
				return; 
			}
			text =  $("label[for='"+lid+"']").html();
//			text = $('#textAreaBlock').val();
		} else
			text = $("label[for='"+lid+"']").html();
		console.log(text);
		console.log(desc);
		if(text.length>0){
			/*if (lid == "userblock7") {
				var reason = "Other, ";
				reason = reason + text ;
				text = reason;
			}*/

			blockcall = 1;
		$.post(post_url, {
			type : 'report_abuse',
			label_id : lid,
			reason : text,
			description : desc
		}, function(data, status) {
		//	alert("abuse reported");
				{literal}
				GAObj = {"data":{"activity":"message_full_conversation", "event_type":"report_abuse"}};
				{/literal}
				logGA(GAObj);
			window.location = messagesLink;
		});
		}
		else{
			alert("please enter a reason");
		}
		}
	});
});

</script>

<script>


$(".msgtypearea").keyup(function(event){
    if(event.keyCode == 13){
        $(".msgsndbtn").click();
    }
});


$('#report_abuse').click(function(){
	$("#diffframe_abuse").show();
});
$('#block_user').click(function(){
	$('#diffframe10').show();
});

$('.msgsndbtn').click(function(){
	text = $('.msgtypearea').val();

	var post_url = $(location).attr('href');
	if(text){
	$.post(post_url,
			{
				msg : text,
				type : 'send_msg'
			},	function(dataJson, status) {
				console.log(dataJson);
				
				var data = JSON.parse(dataJson);
				if(data.is_blocked) alert("Either of you have blocked each other");
				else if (data.error)
					alert(data.error_msg);
				//console.log(data.error_msg);
				else{
					var appendDiv = '<div class="sendmsg"><img src="'+cdnUrl+'/images/profile/sendarrow.png" class="sndicon">'+data.msg+'<p class="msgactico txtgray1"><i class="msgtypesnd"></i>Now</p></div>';
						$('.msgwraper').append(appendDiv);
						$('.msgwraper').scrollTop($('.msgwraper')[0].scrollHeight);
						last_msg_id = data.msg_id;
				}
			});
	}
	$('.msgtypearea').val('');
});

$('#files').change(function(){
	uploadImageToServer();
})


function uploadImageToServer()
{
	var formData = new FormData();
	formData.append('file', $('#files')[0].files[0]);
	formData.append('action','upload_image');
	formData.append('rotate','0');
	formData.append('match_id',matchId);
	var actionUrl = baseurl + "/photo_share.php";
	console.log(baseurl);
	$.ajax({
		url: actionUrl,
		type: "POST",
		data : formData,
		processData: false,  // tell jQuery not to process the data
		contentType: false,
		success: function(result){
			console.log(result);
			result = JSON.parse(result);
			if(result.responseCode == 200){
				var metadata = new Object();
				metadata['urls']=result.urls;
				metadata['message_type']="IMAGE";
				sendImageMessage(metadata);
			}
			else
			alert('Something wrong, Please try again');
			if(result.error)
			{
				alert(result.error);
			}
		},
		error: function(result){
			console.log('error');
			console.log(result);
			alert(error);
		}
	});
}


function sendImageMessage(metadata)
{
	sentImageUrl = metadata.urls.jpeg;
    metadata_json = JSON.stringify(metadata);
	var post_url = $(location).attr('href');
	if(metadata){
		$.post(post_url,
				{
					msg : "You've received an image!",
					type : 'send_msg',
					metadata : metadata_json,
					msg_type : 'IMAGE'
				},	function(dataJson, status) {

					var data = JSON.parse(dataJson);
					console.log(data);
					if(data.is_blocked) alert("Either of you have blocked each other");
					else if (data.error)
						alert(data.error_msg);
					//console.log(data.error_msg);
					else{
						var appendDiv = '<div class="sendmsg"><img src="'+sentImageUrl+'" align="right" height="300" class="imagemsg"><i class="msgtypesnd"></i>Now</p></div>';
						$('.msgwraper').append(appendDiv);
						$('.msgwraper').scrollTop($('.msgwraper')[0].scrollHeight);
						last_msg_id = data.msg_id;

					}
				});
	}
	$('.msgtypearea').val('');

}

$('#seen_update').click(function(){
	var adminResponse = confirm("Are you sure you want to ignore it?");  
	if(adminResponse == true){
	$.post(post_url,
			{
				type : 'update_seenTs'
			},	function(data, status) {
						window.location = messagesLink;
			});
	}
});


var updateMsgs = function() {
	//console.log('making call to server after 5 seconds to get new messages');
	var post_url = $(location).attr('href');
	//console.log(post_url);
	$.post(post_url,
			{
				msg_id : last_msg_id,
				login_mobile : 'true',
				from_web_poll:'true'
			},	function(dataJson, status) {

				var data = JSON.parse(dataJson);
				var msg_list = data.data.message_list;
				var fname = data.data.receiver.fname;
				if(msg_list != undefined && msg_list.length >0)
				{
				//	console.log('new message received');
					for (i = 0; i <msg_list.length; i++)
					{
					//	console.log(msg_list[i].msg_id);
						last_msg_id = msg_list[i].msg_id;
						if(msg_list[i].sender == 'Me')
						{
							if(msg_list[i].message_type=='IMAGE')
							{
								var appendDiv = '<div class="sendmsg"><img src="'+msg_list[i].metadata.urls.jpeg+'" align="left" height="300" class="imagemsg"><i class="msgtypesnd"></i>Now</p></div>';
							}
							else
							{
								var appendDiv = '<div class="sendmsg"><img src="'+cdnUrl+'/images/profile/sendarrow.png" class="sndicon">'+msg_list[i].msg+'<p class="msgactico txtgray1"><i class="msgtypesnd"></i>Now</p></div>';
							}

							$('.msgwraper').append(appendDiv);
							$('.msgwraper').scrollTop($('.msgwraper')[0].scrollHeight);
						}
						else
						{
							if(msg_list[i].message_type=='IMAGE')
							{
								var appendDiv = '<div class="receivedmsg"><img src="'+msg_list[i].metadata.urls.jpeg+'" align="left" height="300" class="imagemsg"><i class="msgtypesnd"></i>Now</p></div>';
								var notification_text = " sent an Image";
							}
							else
							{
								var appendDiv = '<div class="receivedmsg"><img src="'+cdnUrl+'/images/profile/sendarrow.png" class="sndicon">'+msg_list[i].msg+'<p class="msgactico txtgray1"><i class="msgtypesnd"></i>Now</p></div>';
								var notification_text = ": " + msg_list[i].msg;
							}
							$('.msgwraper').append(appendDiv);
							$('.msgwraper').scrollTop($('.msgwraper')[0].scrollHeight);
						}

						notifyMe(fname+notification_text);
					}

				}
			});
};

var interval = 1000 * 3 ; // every 5 seconds

setInterval(updateMsgs, interval);

function notifyMe(msg_content) {
	// Let's check if the browser supports notifications
	if (!("Notification" in window)) {
		alert("This browser does not support desktop notification");
	}

	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === "granted") {
		// If it's okay let's create a notification
		var notification = new Notification(msg_content);
	}

	// Otherwise, we need to ask the user for permission
	else if (Notification.permission !== 'denied') {
		Notification.requestPermission(function (permission) {
			// If the user accepts, let's create a notification
			if (permission === "granted") {
				var notification = new Notification(msg_content);
			}
		});
	}
	notification.onclick = function(){
		window.focus();
		this.cancel();
	};

	// At last, if the user has denied notifications, and you
	// want to be respectful there is no need to bother them any more.
}


</script>

{include file='../../templates/common/footer.tpl'}

</body>
</html>
