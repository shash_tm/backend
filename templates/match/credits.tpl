<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<title>TrulyMadly - My Credits</title>
<link href="{$cdnurl}/css/match/matches.css?v=8" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="{$cdnurl}/css/common/headfooter.css?v=3" rel="stylesheet" type="text/css">

</head>

<body>
{include file='templates/common/header.tpl'}

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<div class="mutmatchsec">
<div class="mmwraper">
<h3>My credits</h3>

    	<div class="emptymsg">Hey you! Try our new features for free. Till then your credits are safe with us.</div>
<!-- Match Tiles end -->
</div>
</div>
</div>
</div>
<!--Mutualmatch end -->

<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>

{include file='templates/common/footer.tpl'}


</body>
</html>
