<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>TrulyMadly - Matches</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
//console.log(headLoadTimeStamp);
</script>
<!--<link href="../../css/match/matches.css" rel="stylesheet" type="text/css">
<link href="../../css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="../../css/common/font-awesome.css" rel="stylesheet" type="text/css">
--><link href="{$cdnurl}/css/match/matches.css?v=7" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css"> 
<!--<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font"> -->
<link href="{$cdnurl}/css/common/headfooter.css?v=3" rel="stylesheet" type="text/css">
<script type="text/javascript">
cssLoadTimeStamp  = (new Date()).getTime();
//console.log(cssLoadTimeStamp);
</script>
</head>

<body>
{include file='templates/common/header.tpl'}
<div class="new_wrapper bgmatchgrad">
<!--Centered Container Start -->
<div class="new_container" align="center">
<!-- Pagination -->
<div class="pagination"><h3>Matches</h3><p></p><p></p><p></p><p></p><p></p><p></p><div class="pagenos"><div id="progressFullContainer"><span id="progressContainer"></span> / <span id="totalItemContainer"></span></div></div><p></p><p></p><p></p><p></p><p></p><p></p></div>
<div id ="loader_image" class="loadermsg"><img src="{$cdnurl}/images/ajax_loader.gif"><p>Excited to see who we matched you with?<br>Populating list, please wait.</p></div>
<!-- Matches Scroll Start -->
<div class="crousouter">
{if $isAuthentic eq false}
<div class="arrowsec">
	<a class="arrowpoint arrowleft" style="display:none;"><i class="icon-angle-left"></i></a>
	<a class="arrowpoint arrowright"><i class="icon-angle-right"></i></a>
    </div>
{/if}

<div id="matchesContainer" class="coverflow">
{if isset($systemMsgs.first_tile)}
	<div class="newmatch" id="dummy-first-tile">
		<div class="nomatchblnk">
			{$systemMsgs.first_tile}
			{if isset($systemMsgs.first_tile_link) && $systemMsgs.first_tile_link eq 'show_matches'}
				<div class ="clb"></div><a id="show_matches">Show matches</a>
			{/if}
			
			{if isset($systemMsgs.first_tile_link) && $systemMsgs.first_tile_link eq 'trustbuilder'}
				<div class ="clb"></div><a href="{$baseurl}/trustbuilder.php">Verify now</a> 
                <br><a class="skiptxt1" id="non_auth_thanks">No thanks, will do it later <i class="icon-double-angle-right"></i></a>
			{/if}
			{if isset($systemMsgs.first_tile_link) && $systemMsgs.first_tile_link eq 'upload_photo'}
				<div class ="clb"></div><a href="{$baseurl}/photo.php">Upload photo</a>
			{/if}
		</div>
	</div>
{/if}

<div class="newmatch" id="dummy-tile">
	<div class="nomatchblnk">{$systemMsgs.last_tile_msg}
			{if isset($systemMsgs.last_tile_link) && $systemMsgs.last_tile_link eq 'trustbuilder'}
				<div class ="clb"></div><a href="{$baseurl}/trustbuilder.php">Verify now</a>
                 
			{/if}
			{if isset($systemMsgs.last_tile_link) && $systemMsgs.last_tile_link eq 'invite_friends'}
			<!-- <div class ="clb"></div><a id="invite_frnd" onclick="invite()" >Invite friends</a>-->
			
			 	  <div class ="clb"></div><a id="invite_frnd" onclick="invite_facebook_friend()" >Invite friends</a> 
			{/if}
	</div>
</div>




</div>
</div>
<script>
var cdnUrl='{$cdnurl}';
var baseurl='{$baseurl}';
var isAuthentic = {if $isAuthentic eq false }0{else}1{/if};
var allMatchedIdsServer = {$matches};
var isSetFirstTileServer = {if isset($systemMsgs.first_tile)}1{else}0{/if};
var stopMovement = {if isset($systemMsgs.stopMovement)}1{else}0{/if};
var oldUser = {if isset($oldUser) && $oldUser eq "1"}1{else}0{/if};
var hashTag = {if isset($isHashTagSet) && $isHashTagSet eq "0"}1{else}0{/if};
var firstHide = {if isset($systemMsgs.first_tile_link) && $systemMsgs.first_tile_link eq "show_matches"}1{else}0{/if};
var server='{$SERVER}';//"http://dev.trulymadly.com/trulymadly";
var fb_api="{$fb_api}";
var fb_scopes='{$facebook_scope}';
var FB_RESPONSE = {};
var firstMaybe = {if isset($firstMaybe) && $firstMaybe eq "0"}1{else}0{/if};
var user_id = "{$user_id}";
var myId = "{$user_id}";

</script> 
 
<!-- Matches Scroll End -->
 
</div>
<!--Centered Container End -->
</div>

<!-- Pop Up Start -->
<div id="maybematch" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="popupmaybe">
<p>Well, you can take a second look in your '<span>My Maybes</span>' list. <br>
<br>
<!-- <a  onClick="document.getElementById('hidematch').style.display='none';" class="actionbtn">Cancel</a><a class="actionbtn" id="yes_hide">Yes</a></p> -->
<!--  <p></p>-->
<a id="maybeaction" class="actionbtn" onClick="document.getElementById('maybematch').style.display='none';">Cancel</a><a class="actionbtn" id="yes_maybe">Ok</a></p>
<div class="clb">&nbsp;</div>

</div>
</div>
<!-- Pop Up end -->


<!-- Pop Up Start -->
<div id="diffframe4" style="display:none;">
<div class="difusescreen" align="center" onClick="document.getElementById('diffframe4').style.display='none';"></div>
<div class="popupframe">
<a onClick="document.getElementById('diffframe4').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<p>
{$systemMsgs.like_hide_action_msg}<br><br>
<a href="{$cdnurl}/trustbuilder.php" class="actionbtn">Verify now</a></p>
<div class="clb">&nbsp;</div>
<!--  <p></p>-->

</div>
</div>
<!-- Pop Up end -->

<!-- Pop Up Start -->
<div id="hidematch" style="display:none;">
<div class="difusescreen" align="center" onClick="document.getElementById('hidematch').style.display='none';"></div>
<div class="popupframe">
<a onClick="document.getElementById('hidematch').style.display='none';" class="closepopup"><i class="icon-remove-sign"></i></a>
<p>
Are you sure? This will permanently remove the match from your list.<br><br>
<a  onClick="document.getElementById('hidematch').style.display='none';" class="actionbtn">Cancel</a><a class="actionbtn" id="yes_hide">Yes</a></p>
<!--  <p></p>-->
<div class="clb">&nbsp;</div>

</div>
</div>
<!-- Pop Up end -->



<!--Interests Section Popup -->
<div id="regint" style="display:none; z-index:100;">
<div class="difusescreen" align="center"></div>
<div class="popupframe" align="center">

<div class="formreg1">
<p>&nbsp;Describe yourself in 5 hashtags:</p>
<ul>
				<li><input id="interest1" type="checkbox" class = "cutomradio" name="interest" value="#MovieManiac"><label>#MovieManiac</label></li>
				<li><input id="interest2" type="checkbox" class = "cutomradio" name="interest" value="#TVAddict"><label>#TVAddict</label></li>
				<li><input id="interest3" type="checkbox" class = "cutomradio" name="interest" value="#BollywoodBuff"><label>#BollywoodBuff</label></li>
				<li><input id="interest4" type="checkbox" class = "cutomradio" name="interest" value="#AvidTraveller"><label>#AvidTraveller</label></li>
				<li><input id="interest5" type="checkbox" class = "cutomradio" name="interest" value="#BeachBum"><label>#BeachBum</label></li>
				<li><input id="interest6" type="checkbox" class = "cutomradio" name="interest" value="#NatureSeeker"><label>#NatureSeeker</label></li>
				<li><input id="interest7" type="checkbox" class = "cutomradio" name="interest" value="#MusicSnob"><label>#MusicSnob</label></li>
				<li><input id="interest8" type="checkbox" class = "cutomradio" name="interest" value="#BookWorm"><label>#BookWorm</label></li>
				<li><input id="interest9" type="checkbox" class = "cutomradio" name="interest" value="#SportsFanatic"><label>#SportsFanatic</label></li>
				<li><input id="interest10" type="checkbox" class = "cutomradio" name="interest" value="#Foodie"><label>#Foodie</label></li>
				<li><input id="interest11" type="checkbox" class = "cutomradio" name="interest" value="#HealthFreak"><label>#HealthFreak</label></li>
				<li><input id="interest12" type="checkbox" class = "cutomradio" name="interest" value="#Artsy"><label>#Artsy</label></li>
				<li><input id="interest13" type="checkbox" class = "cutomradio" name="interest" value="#AnimalLover"><label>#AnimalLover</label></li>
				<li><input id="interest14" type="checkbox" class = "cutomradio" name="interest" value="#AdrenalineJunkie"><label>#AdrenalineJunkie</label></li>
				<li><input id="interest15" type="checkbox" class = "cutomradio" name="interest" value="#Scholar"><label>#Scholar</label></li>
				<li><input id="interest16" type="checkbox" class = "cutomradio" name="interest" value="#PartyHopper"><label>#PartyHopper</label></li>
				<li><input id="interest17" type="checkbox" class = "cutomradio" name="interest" value="#Homebody"><label>#Homebody</label></li>
				<li><input id="interest18" type="checkbox" class = "cutomradio" name="interest" value="#Trendy"><label>#Trendy</label></li>
				<li><input id="interest19" type="checkbox" class = "cutomradio" name="interest" value="#EDMPundit"><label>#EDMPundit</label></li>
				<li><input id="interest20" type="checkbox" class = "cutomradio" name="interest" value="#RealityShowAddict"><label>#RealityShowAddict</label></li>
				<li><input id="interest21" type="checkbox" class = "cutomradio" name="interest" value="#CricketFan"><label>#CricketFan</label></li>
				<li><input id="interest22" type="checkbox" class = "cutomradio" name="interest" value="#FootballCrazy"><label>#FootballCrazy</label></li>
				<li><input id="interest23" type="checkbox" class = "cutomradio" name="interest" value="#CoffeeLover"><label>#CoffeeLover</label></li>
				<li><input id="interest24" type="checkbox" class = "cutomradio" name="interest" value="#Chocaholic"><label>#Chocaholic</label></li>
				<li><input id="interest25" type="checkbox" class = "cutomradio" name="interest" value="#NightOwl"><label>#NightOwl</label></li>
				<li><input id="interest26" type="checkbox" class = "cutomradio" name="interest" value="#EarlyBird"><label>#EarlyBird</label></li>
</ul>
               <div class="clb">&nbsp;</div> 
               <p><a id="activeInterest" class="actionbtn" style="display:none;"><i class="icon-save"></i> Save</a>
				<a id="inactiveInterest" class="diffregbcbtn"><i class="icon-save"></i> Save</a>
               </p>
               <div class="clb">&nbsp;</div> 
</div>

</div></div>



<!-- What's New Start -->
<div id="whtnew" style="display:none;  z-index:999;">
<div class="difusescreen" align="center" onClick="document.getElementById('whtnew').style.display='none'; "></div>

<div class="popupframewn">
<a onClick="document.getElementById('whtnew').style.display='none';" class="closepopup1"><i class="icon-remove-sign"></i></a>
<!-- content Section -->
<div class="wnbgsec"><h5>Truly new.<br>Madly improved.</h5></div>
<ul class="whtnewtxt">
<li class="wntilebg">
    <h6><span>Shiny</span><br>new mobile app</h6>
    <ul>
    	<li>Easy accessibility</li>
        <li>Realtime chat</li>
        <li>Realtime notifications </li>
    </ul>
</li>
<li>
    <h6><span>New</span><br>look And feel</h6>
    <ul>
    	<li>Easy to use and navigate</li>
		<li>New carousel view to see matches</li>
		<li>Mobile responsive</li>
	</ul>
</li>
<li class="wntilebg">
    <h6><span>Deeper</span><br>compatibility analysis</h6>
    <ul>
    	<li>New personality match quizzes</li>
		<li>Focused common interests</li>
		<li>More matches per day</li>
    </ul>
</li>
<li>
    <h6><span>More</span><br>private and discreet</h6>
    <ul>
    	<li>'Like' or 'Hide' matches anonymously</li>
		<li>Chat with only matches you've liked</li>
		<li>Block and report abuse</li>
    </ul>
</li>
</ul>
<!-- content Section end -->

</div>
</div>
<!-- What's New end -->

<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/common/common.js?v=1"></script>
<script src="{$cdnurl}/js/blueimp/tmpl.min.js"></script>
<script src="{$cdnurl}/js/lib/icheck.js"></script>
<script src="{$cdnurl}/js/carousel/bespoke.min.js"></script>
<script src="{$cdnurl}/js/lib/tm_core_new.js"></script>
<script src="{$cdnurl}/js/match/match.js?v=3"></script>

{if isset($systemMsgs.last_tile_link) && $systemMsgs.last_tile_link eq 'invite_friends'}
<script src="{$cdnurl}/js/match/inviteFriends.js?v=2.2"></script>
{/if}
{include file='templates/common/footer.tpl'}

</body>
</html>
