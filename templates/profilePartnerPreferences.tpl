            	<div class="mt20 clearfix">
                	<h2>Partner Preferences</h2>
                	<div class="partner-outer">
                    	<!--<div class="partnerdivider"></div> -->
                        <div class="partnerdetails" align="center">
                        	<ul>
                            	<li class="first">
                                    <div class="pcirclewhite fl"><!--
                                    	<span>Age</span>
                                    </div>
                                    <p>{$preference.preference_demography.start_age} to {$preference.preference_demography.end_age}</p>
                                    
                                    
                                    --><span>{if isset($preference.preference_demography.start_age)&& isset($preference.preference_demography.end_age)}
                                    	{$preference.preference_demography.start_age} Yrs. <br> to <br> {$preference.preference_demography.end_age} Yrs.
                                    	{else}
                                    	Doesn't Matter
                                    	{/if}
                                    	</span>
                                    </div>
                                        <p class="clb">Age</p>
                                    
                                </li>
                                <li class="second">
                                    <div class="pcirclewhite">
                                    	<!--<span>Marital Status</span>
                                    </div>
                                    <p>{$preference.preference_family.marital_status}</p>
                                    
                                    -->
                                     
                                    <span>  {if !isset($preference.preference_work.income_start) && !isset($preference.preference_work.income_end)}
                                        Doesn't Matter
                                        {/if}
                                        
                                         {if $preference.preference_work.income_start eq '0 LPA' && $preference.preference_work.income_end eq '0 LPA'}
                                         Doesn't Matter
                                        {else}
                                         
                                   		 {if isset($preference.preference_work.income_start)}
                                   		 {$preference.preference_work.income_start}
                                       <br> to <br> 
                                       {if isset($preference.preference_work.income_end)}
                                       {$preference.preference_work.income_end}
                                       {else}
                                       ...
                                       {/if}
                                       {/if}
                                        {/if}
                                        </span>
                                    </div>
                                    <p>Income Range</p>
                                    
                                    
                                </li>
                                <li class="third">
                                    <div class="pcirclewhite fr">
                                    	<span>{if isset($preference.preference_traits.start_height)&& isset($preference.preference_traits.end_height)}
                                    	{$preference.preference_traits.start_height}<br>
                                    	 to<br> 
                                    	 {$preference.preference_traits.end_height}
                                    	{else}
                                    	Doesn't Matter
                                    	{/if}
                                    	</span>
                                    </div>
                                        <p class="clb">Height</p>
                                </li>
                            </ul>
                        </div>
                    </div>	
                    <ul class="profession-religion-physical clearfix">
                        <li class="effect4">
                            <div class="bottom_titlesed clearfix"><img src="{$cdnurl}/images/dashboard/profile/education.png" class="imgicon">Profession</div>
                            <div class="alldetailspartner boxedu">
                                <table>
                                	<tr>
                                    	<td class="td1">Education level</td></tr>
                                        <tr>
                                    	 <td class="tdpad">{if isset($preference.preference_work.education)}
                                        {$preference.preference_work.education}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td class="td1">Working Status</td></tr>
                                    	<tr> <td class="tdpad">{if isset($preference.preference_work.working_area)}
                                        {$preference.preference_work.working_area}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td class="td1">Industry</td></tr>
                                    	<tr> <td class="tdpad">{if isset($preference.preference_work.industries)}
                                        {$preference.preference_work.industries}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                   
                                    <!--<tr>
                                    	<td class="td1">Working status</td>
                                        <td>Working</td>
                                    </tr>
                                --></table>
                            </div>
                        </li>
                        <li class="effect4">
                            <div class="bottom_titlesed clearfix"><img src="{$cdnurl}/images/dashboard/profile/religion.png" class="imgicon" style="margin-right:5px;">Religion</div>
                            <div class="alldetailspartner boxedu">
                                <table>
                                	<tr>
                                    	<td class="td1">Religion</td></tr>
                                        <tr>
                                        <td class="tdpad">{if isset($preference.preference_demography.religions)}
                                        {$preference.preference_demography.religions}
                                        {else}
                                        Doesn't Matter
                                        {/if}</td>
                                    </tr><!--
                                    <tr>
                                    	<td class="td1">Caste</td></tr>
                                        <tr>
                                        <td class="tdpad">{if isset($preference.preference_demography.castes)}
                                        {$preference.preference_demography.castes}
                                        {else}
                                        Doesn't Matter
                                        {/if}</td>
                                    </tr>
                                    --><tr>
                                    	<td class="td1">Mother Tongue</td></tr>
                                        <tr>
                                        <td class="tdpad">{if isset($preference.preference_demography.languages_preference)}
                                        {$preference.preference_demography.languages_preference}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                    <!--<tr>
                                    	<td class="td1">Manglik</td></tr>
                                        <tr>
                                    	<td>{if isset($preference.preference_family.manglik_status)}
                                        {$preference.preference_family.manglik_status}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                     
                                --></table>
                            </div>
                        </li>
                        <li class="last effect4">
                            <div class="bottom_titlesed clearfix"><img src="{$cdnurl}/images/dashboard/profile/physical.png" class="imgicon">Physical</div>
                            <div class="alldetailspartner boxedu">
                                <table><!--
                                	
                                    <tr>
                                    	<td class="td1">Skin tone</td></tr>
                                        <tr>
                                    	 <td class="tdpad">{if isset($preference.preference_traits.skin_tone)}
                                        {$preference.preference_traits.skin_tone}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                    --><tr>
                                    	<td class="td1">Body type</td></tr>
                                        <tr>
                                    	 <td class="tdpad">{if isset($preference.preference_traits.body_type)}
                                        {$preference.preference_traits.body_type}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                    <!--<tr>
                                    	<td class="td1">Disability</td></tr>
                                        <tr>
                                    	 <td>{if isset($preference.preference_traits.disability)}
                                        {$preference.preference_traits.disability}
                                        {else}
                                        Doesn't Matter
                                        {/if}
                                        </td>
                                    </tr>
                                --></table>
                            </div>
                        </li>
                    </ul>	
                    <ul class="clearfix partnerhobbies">
                    	<li class="first">
                        	<ul class="smokinglist">
                            <li class="phheading"><span class="psmoking"></span></li>
                            {if count($preference.preference_demography.smoking_status) eq 0}
                            	   <li>Doesn't Matter</li>
                            {else}
                            {foreach from=$preference.preference_demography.smoking_status item=smoke}
                            	<li>{$smoke}</li>
                            	{/foreach}
                         
                            {/if}	
                            </ul>
                        </li>
                        <li class="second">
                        	
                            <ul class="smokinglist">
                            <li class="phheading"><span class="pdrinking"></span></li>
                                 {if count($preference.preference_demography.drinking_status) eq 0}
                            	  <li> Doesn't Matter</li>
                            	{else}
                            	{foreach from=$preference.preference_demography.drinking_status item=drink}
                            	<li>{$drink}</li>
                            	{/foreach}
                         
                            {/if}
                            </ul>
                        </li>
                        <li class="third">
                        	
                            <ul class="smokinglist">
                            <li class="phheading"><span class="pfood"></span></li>
                             {if count($preference.preference_demography.food_status) eq 0}
                            <li> Doesn't Matter</li>
                            	{else}
                            	{foreach from=$preference.preference_demography.food_status item=food}
                            	<li>{$food}</li>
                            	{/foreach}
                           
                            {/if}
                            	<!--<li><img src="{$cdnurl}/images/dashboard/icons/vegetarian_ic.png"> Vegetarian</li>
                                <li><img src="{$cdnurl}/images/dashboard/icons/eggetarian_ic.png"> Eggetarian</li>
                            -->
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix mt40">
                    	<div class="ploction effect4">
                        	<p class="bottom_titleslf clearfix mb15"><img src="{$cdnurl}/images/dashboard/icons/location_ic.png" class="imgicon">Location</p>
                            <div class="locationlist clearfix">
                                <span>Country</span>
                                <ul class="mrg fl">
                                {if isset($preference.preference_countries) }
                                 {foreach from=$preference.preference_countries item=country}
                            	 <li class="mrg fl"><p>{$country}</p></li>
                            	{/foreach}
                            	{else}
                            	<li class="mrg fl"><p>Doesn't Matter</p></li>
                            	{/if}
                                </ul>
                        	</div>
                            <div class="locationlist clearfix mt10">
                                <span>State</span>
                                <ul class="mrg fl">
                                 {if isset($preference.preference_states) }
                                {foreach from=$preference.preference_states item=state}
                            	 <li class="mrg fl"><p>{$state}</p></li>
                            	{/foreach}
                            	{else}
                            	 <li class="mrg fl"><p>Doesn't Matter</p></li>
                            	{/if}
                                </ul>
                            </div>
                            <div class="locationlist clearfix mt10">
                                <span>City</span>
                                 <ul class="mrg fl">
                                 {if isset($preference.preference_cities)}
                                    {foreach from=$preference.preference_cities item=city}
                            	  <li class="mrg fl"><p>{$city}</p></li>
                            	{/foreach}
                            	{else}
                            	 <li class="mrg fl"><p>Doesn't Matter</p></li>
                            	{/if}
                                </ul>
                            </div>
                        </div>
                        <div class="pfamily effect4">
                        	<p class="bottom_titleslf clearfix mb15"><img src="{$cdnurl}/images/dashboard/icons/family_ic.png" class="imgicon">Family</p>
                            <div class="familylist clearfix">
                                <span>Family type </span>
                                <ul class="mrg fl">
                                {if count($preference.preference_family.family_type) eq 0}
                                 <li class="mrg fl"><p>Doesn't Matter</p></li>
                            	
                            	{else}
                            	 {foreach from=$preference.preference_family.family_type item=ftype}
                            	 <li class="mrg fl"><p>{$ftype}</p></li>
                            	{/foreach}
                            	{/if}
                                </ul>
                        	</div><!--
                            <div class="familylist clearfix mt10">
                                <span>Living with family</span>
                                 <ul style="width:48%" class="mrg fl">
                                <li class="mrg fl"><p>Yes</p></li>
                                </ul>
                            </div>
                            --><div class="familylist clearfix mt10">
                                <span>Family Status</span>
                                <ul class="mrg fl">
                                 {if count($preference.preference_family.family_status) eq 0}
                                  <li class="mrg fl"><p>	Doesn't Matter </p></li>
                                {else}
                                
                                 {foreach from=$preference.preference_family.family_status item=fstatus}
                               <li class="mrg fl"> <p>{$fstatus}</p></li>

                                {/foreach}
                                
                         
                            	{/if}
                                </ul>
                            </div><!--
                            <div class="familylist clearfix mt10">
                                <span>Can Meet Someone with Children</span>
                                <ul style="width:48%" class="mrg fl">
                               <li class="mrg fl"> <p>
                                {if isset($preference.preference_family.meet_spouse_havingChildren)}
                                {$preference.preference_family.meet_spouse_havingChildren}
                                {else}
                                No
                                {/if}
                                </p></li>
                                
                                  </ul>
                            </div>
                            
                                -->
                                <div class="familylist clearfix mt10">
                                <span>Marital Status</span>
                                <ul class="mrg fl">
                               
                                {if count($preference.preference_demography.marital_status) eq 0}
                                  <li class="mrg fl"><p>	Doesn't Matter </p></li>
                                {else}
                                
                                 {foreach from=$preference.preference_demography.marital_status item=mstatus} 
                               <li class="mrg fl"> <p>{$mstatus}</p></li>

                                {/foreach}
                                
                         
                            	{/if}
                               
                               
                                    
                              
                        </div>
                    </div>
                </div>
