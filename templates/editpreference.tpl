<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Edit Preference</title>
<script>
var time = new Date();
var time1 = new Date();
</script>
<link href="{$cdnurl}/css/register_new.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/iThing.css" rel="stylesheet" media="screen">
<link href="{$cdnurl}/css/common/select2.css" rel="stylesheet"/>

<script>
var demo = JSON.parse('{$demo}');
var trait = JSON.parse('{$trait}');
var work = JSON.parse('{$work}');
var uid = demo.user_id;
var states = '{$states}';
var baseurl = '{$baseurl}';
</script>


</head>

<body>
{include file='templates/common/header.tpl'}

<div class="new_wrapper comphychobg">
<!--Centered Container Start -->
	<div class="new_container" align="center">
	<div class="regprogress"><h2>Partner Preferences</h2></div>
<!--White Frame start -->
	<div class="regformsec">
	<div class="formreg1">
<!--Age Range Start -->

		<p>Age Range</p>
		<div class="sliderContainer">
			<div class="ui-rangeSlider ui-rangeSlider-withArrows" style="position: relative;" id="basicSliderExample"><div style="position: absolute;" class="ui-rangeSlider-container"><div style="position: absolute; top: 0px; left: 0px;" class="ui-rangeSlider-innerBar"></div></div><div style="position: absolute; left: 0px;" class="ui-rangeSlider-arrow ui-rangeSlider-leftArrow"><div class="ui-rangeSlider-arrow-inner"></div></div><div style="position: absolute; right: 0px;" class="ui-rangeSlider-arrow ui-rangeSlider-rightArrow"><div class="ui-rangeSlider-arrow-inner"></div></div></div>
		</div>
<!--Age Range End -->

<!--Height Range Start -->
	<p>Height Range</p>
	<div class="sliderContainer">
		<div class="ui-rangeSlider ui-rangeSlider-withArrows" style="position: relative;" id="basicSliderExample1"><div style="position: absolute;" class="ui-rangeSlider-container"><div style="position: absolute; top: 0px; left: 0px;" class="ui-rangeSlider-innerBar"></div></div><div style="position: absolute; left: 0px;" class="ui-rangeSlider-arrow ui-rangeSlider-leftArrow"><div class="ui-rangeSlider-arrow-inner"></div></div><div style="position: absolute; right: 0px;" class="ui-rangeSlider-arrow ui-rangeSlider-rightArrow"><div class="ui-rangeSlider-arrow-inner"></div></div></div>
	</div>
<!--Height Range End -->

<!--Relationship Status Start -->

<p>Relationship Status</p>

<ul>
<li>
<input type="checkbox" value="Never Married" name='marital_status_spouse'>
<label>Never Married</label>
</li>
<li>
<input type="checkbox" value="Married Before" name='marital_status_spouse'>
<label>Married Before</label>
</li>
<!--
<li>
<input type="checkbox" value="Separated" name='marital_status_spouse'>
<label>Separated</label>
</li>
<li>
<input type="checkbox" value="Divorced" name='marital_status_spouse'>
<label>Divorced</label>
</li>
<li>
<input type="checkbox" value="Widowed" name='marital_status_spouse'>
<label>Widowed</label>
</li>
-->
<li>
<input type="checkbox" value="null" name='marital_status_spouse_notMatter'>
<label>Doesn't Matter</label>
</li>
</ul>


<!--Relationship Status End -->



<!--
<p>Religion</p>
<ul>
<li>
<input type="checkbox" name="religion" value="Hindu">
<label>Hindu</label>
</li>
<li>
<input type="checkbox" name="religion" value="Muslim">
<label>Muslim</label>
</li>
<li>
<input type="checkbox" name="religion" value="Sikh">
<label>Sikh</label>
</li>
<li>
<input type="checkbox" name="religion" value="Christian">
<label>Christian</label>
</li>
<li>
<input type="checkbox" name="religion" value="Buddhist">
<label>Buddhist</label>
</li>
<li>
<input type="checkbox" name="religion" value="Jain">
<label>Jain</label>
</li>
<li>
<input type="checkbox" name="religion_notMatter" value="null">
<label>Doesn't Matter</label>
</li>
</ul>

 -->

</div>

<!--Country Start -->
<div class="formreg padedit">
<!--<p>Country</p>
<div class="regselect-style">
<select tabindex="-1" name="country" id="country" data-placeholder="Please Select Country">
			<option value="113" >India</option>
			{foreach from=$countries item=foo}
				{if $foo.country_id neq 113}
					<option value="{$foo.country_id}">{$foo.name}</option>
				{/if}
			{/foreach}
</select>
</div>-->

<!--Country End -->

<!--State Start -->
<p>State</p>
<div class="comdetails nobrdr">
<select tabindex="-1" multiple="multiple" name="state" id="state" class="populate select2-offscreen newmultiselect" data-placeholder="Please Select States">
                        </select>
</div>

<!--State End -->


<!--City Start -->

<p>City</p>
<div class="comdetails nobrdr">
<select tabindex="-1" multiple="multiple" name="city" id="city" class="populate select2-offscreen newmultiselect" data-placeholder="Please Select Cities">
            </select>
</div>

<!--City End -->

<!--Income Start -->

<!-- <p>Annual Income</p>
<div class="comdetails nobrdr">
<select  name="State" class="" id="income" tabindex="-1" multiple="multiple">
            <option value="null">Doesn't Matter</option>
            {foreach from=$income item=foo}
				<option value="{$foo.id}">{$foo.value}</option>
			{/foreach}
</select>
</div> -->

<!--Income End -->

<!--Education Start -->

<p>Highest Degree</p>
<div class="comdetails nobrdr">
<select tabindex="-1" multiple="multiple" name="education" id="education" class="populate select2-offscreen newmultiselect" data-placeholder="Please Select">
            <option value='-5'>Doesn't Matter</option>
            <option value='-1'>Any Masters</option>
            <option value='-2'>Any Bachelors</option>
            {foreach from=$highest_degree item=foo}
				<option value="{$foo.id}">{$foo.value}</option>
			{/foreach}
            </select>
</div>

<!--Education End -->

<!--

<p>Smoking</p>

<ul>
<li class="fl">
<input type="radio" name="smoking_status" value="null">
<label>Doesn't Matter</label>
</li>
<li class="fr">
<input type="radio" name="smoking_status" value="Never">
<label>Never</label>
</li>
</ul>



<p>Drinking</p>
<ul>
<li class="fl">
<input type="radio" name="drinking_status" value="null">
<label>Doesn't Matter</label>
</li>
<li class="fr">
<input type="radio" name="drinking_status" value="Never">
<label>Never</label>
</li>
</ul>



<p>Eating</p>
<ul>
<li class="fl">
<input type="radio" name="food_status" value="null">
<label>Doesn't Matter</label>
</li>
<li class="fr">
<input type="radio" name="food_status" value="Vegetarian">
<label>Veg Only</label>
</li>
</ul>

-->


<!--White Frame End -->


</div>
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul> 
</div>

<div class="regfrmaction">
<a onclick="saveData()" id="savedata" class="regbcbtn fr"><i class="icon-save"></i> &nbsp;Save</a>
</div>

</div>
<!--Centered Container End -->


</div>
{include file='templates/common/footer.tpl'}


<!--<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>-->
<script src="{$cdnurl}/js/lib/icheck.js"></script>
  <script src="{$cdnurl}/js/lib/prettify.js"></script>
  <script src="{$cdnurl}/js/lib/jquery-ui.js"></script>
  <script src="{$cdnurl}/js/lib/jQRangeSliderMouseTouch.js"></script>
  <script src="{$cdnurl}/js/lib/jQRangeSliderDraggable.js"></script>
  <script src="{$cdnurl}/js/lib/jQRangeSliderHandle.js"></script>
  <script src="{$cdnurl}/js/lib/jQRangeSliderBar.js"></script>
  <script src="{$cdnurl}/js/lib/jQRangeSliderLabel.js"></script>
  <script src="{$cdnurl}/js/lib/jQRangeSlider.js"></script>
  <script src="{$cdnurl}/js/lib/function.js?ver=4"></script>
  <script src="{$cdnurl}/js/dashboard/core_preference.js?ver=4"></script>

<script>
$(document).ready(function(){
  $('input').each(function(){
    var self = $(this),
      label = self.next(),
      label_text = label.text();

    label.remove();
    self.iCheck({
      checkboxClass: 'icheckbox_line-blue',
      radioClass: 'iradio_line-blue',
      insert: '<div class="icheck_line-icon"></div>' + label_text
    });
  });
});
</script>



<script src="{$cdnurl}/js/lib/select2/select2.js"></script>
<script>
<!--$(document).ready(function() { $("#country").select2(); }); -->
$(document).ready(function() { $("#state").select2(); });
$(document).ready(function() { $("#city").select2(); });
$(document).ready(function() { $("#income").select2(); });
$(document).ready(function() { $("#education").select2(); });
</script>

</body>
</html>
