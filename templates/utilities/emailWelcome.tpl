<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./mailerHeader.tpl'}
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(http://cdn.trulymadly.com/email/images/bgmailer.jpg); background-color:#38434f; font-family:Arial, Helvetica, sans-serif; color:#333;">
<tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerimg1.jpg); background-position:center;"><table width="540" border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0;">
      <tr>
        <td style="background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:13px; line-height:18px; text-align:left;">
        Dear <strong style="font-family:Arial, Helvetica, sans-serif;">{$name}</strong>,<br /><br />
<strong style="font-family:Arial, Helvetica, sans-serif;">Congratulations on becoming a TrulyMadly.com member.</strong><br /><br />
You're now one step closer to finding that special someone!<br /><br />
To get started, <a href ="http://www.trulymadly.com" target="_blank" style="color:#0055cc;">Log on to TrulyMadly.com</a> with your registered account details. Remember, your account information is private and to avoid misuse of any kind, please don't share these details with anyone.<br />
<br />
Truly & a little Madly,<br />
Team TrulyMadly
        </td>
      </tr>
    </table>    </td>
  </tr>
  
</table>
  {include file='./mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
    <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
</body>
</html>
