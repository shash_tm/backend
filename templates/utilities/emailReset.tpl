<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333;width:100%;">
<tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerimg1.jpg); background-size: cover;">
    <table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
<tr>
<td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;">
Dear <strong>{$name}</strong>, <br /><br />
You have requested to reset your forgotten password for your account on TrulyMadly.com. <br /><br />
To verify that you did send us a request, please reset your password by clicking on the following link: <br /><br />
<a href="{$reset_link}" style="color:#0055cc;" target="_blank">{$reset_link}</a><br /><br />
You can click on the above link or just copy and paste it into your browser. If you did not send the request, kindly ignore this email.<br /><br />
Truly & a little Madly,<br />
Team TrulyMadly
   
</td></tr></table>
   </td>
  </tr>
  
</table>


 {include file='./mailerFooter.tpl'}
  <img src="{$analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
</body>
</html>
