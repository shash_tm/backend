<table border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#333; width:90%; min-width:360px;">
  <tr>
    <td align="center" bgcolor="#e6e7e8">
<div style="font-size: 16px; width:30%; height:auto; text-align:center; padding:1.5%; float:left; font-family:'Eurofurence',Arial,Helvetica,sans-serif;"><img src="http://cdn.trulymadly.com/email/images/seal.png" width="42" height="41" style="margin-bottom:5px;" /><br /><p style="margin:0; padding:0;">Verified Profiles</p>
</div>
<div style="font-size:16px; width:30%; height:auto; text-align:center; padding:1.5%; float:left; font-family:'Eurofurence',Arial,Helvetica,sans-serif;  border-right:1px solid #fff;  border-left:1px solid #fff; "><img src="http://cdn.trulymadly.com/email/images/couple.png" width="49" height="41" style="margin-bottom:5px;" /><br />
  <p style="margin:0; padding:0;">True Compatibility</p>
</div>
<div style="font-size: 16px; width:30%; height:auto; text-align:center; padding:1.5%; float:right; font-family:'Eurofurence',Arial,Helvetica,sans-serif;"><img src="http://cdn.trulymadly.com/email/images/lock.png" width="30" height="41"  style="margin-bottom:5px;"/><br />
  <p style="margin:0; padding:0;">Private & Safe</p>
</div>

</td>
  </tr>
    <tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/bgmailer.jpg); background-color:#38434f;"><div style="padding:10px; color:#fff; font-size:12px; text-align:center; line-height:18px; font-family:'Eurofurence',Arial,Helvetica,sans-serif;"><p style="font-size:14px; padding:0; margin:0 auto; font-family:'Eurofurence',Arial,Helvetica,sans-serif;">Follow us on:</p><a href="http://www.facebook.com/trulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/fbicon.png" alt="fb" width="29" height="29" style="border:none; margin:5px;" title="Facebook" /></a><a href="http://blog.trulymadly.com/" target="_blank"><img src="http://cdn.trulymadly.com/email/images/blogicon.png" alt="blog" width="29" height="29" style="border:none; margin:5px;" title="Blog"/></a><a href="http://twitter.com/thetrulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/twtricon.png" alt="twitter" width="29" height="29" style="border:none; margin:5px;" title="twitter" /></a><a href="http://instagram.com/trulymadlycom" target="_blank"><img src="http://cdn.trulymadly.com/email/images/instagicon.png" alt="instagram" width="29" height="29" style="border:none; margin:5px;" title="instagram"/></a><br />
        <a href="http://www.trulymadly.com/terms.php" target="_blank" style="color:#fff; text-decoration:underline;">Terms of Use</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/policy.php" target="_blank" style="color:#fff; text-decoration:underline;">Privacy Policy</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/guidelines.php" target="_blank" style="color:#fff; text-decoration:underline;">Safety Guidelines</a> &nbsp;|&nbsp; <a href="{$analyticsLinks.analytics_unsubscribe_link}" target="_blank" style="color:#fff; text-decoration:underline;">Unsubscribe</a><br />
<a href="{$analyticsLinks.analytics_footer_link}" target="_blank" style="color:#fff; text-decoration:underline;">trulymadly.com</a> &copy; 2014. All rights reserved. F-313C, 3rd Floor, Lado Sarai, New Delhi, India 110030</div></td>
  </tr>
  </table>

