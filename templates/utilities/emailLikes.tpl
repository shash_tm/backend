<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333;width:100%;">
    
  <tr>
    <td align="center" style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/mailerimg2.jpg); background-size: cover;">
<div style="margin:20px 30px 0 30px; line-height:30px; clear:both;">
<strong style="color:#333f4a; font-size:24px;">{$name}</strong><br />
<span style="font-size:18px;">Woo hoo! You have received likes from...</span>
</div>


<!--Profiles -->

<div style="clear:both; text-align:center; margin:0 25px 15px 25px; overflow:hidden;">

{foreach from=$data item=user}
<div style=" display:inline-block; margin:10px; background:#fff; border:1px solid #999999; -webkit-box-shadow: 1px 1px 5px #999; -moz-box-shadow: 1px 1px 5px #999; box-shadow: 1px 1px 5px #999; position:relative; width: 180px;">
<a href="{$user.profile_link}"><img src="{$user.pic}" width="180px" height="180px" border="0" style="display:block;"></img></a>
<p style="padding:5px 0; margin:0 auto; background:#eee; width:180px;"><a href="{$user.profile_link}" style="color:#222; text-decoration:none; font-size:12px; font-family:Helvetica,Arial,sans-serif;">{$user.name}, {$user.age} yrs.</a></p>
<div style="clear:both;"><img src="http://cdn.trulymadly.com/email/images/trustscore/ts{$user.trust_score}.gif" width="66" height="43" style="float:left; margin:10px 5px;" />
  <p style="font-size:12px; text-align:left; padding:15px 5px 0 5px; float:left; margin:0 auto; width: 90px; font-family:Helvetica,Arial,sans-serif;">{$user.height}, {$user.religion}<br />{$user.city}</p></div>
</div>
{/foreach}

</div>

<!--Profiles end -->

<div style="margin:0 auto; text-align:center; clear:both;">
<a href ="{$analyticsLinks.analytics_login_link}" style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; background-color: #307cd3; -webkit-box-shadow: 0 2px #8a8d91; -moz-box-shadow: 0 2px #8a8d91; box-shadow: 0 2px #8a8d91; color:#fff; font-size: 18px; padding:5px 20px; text-decoration:none;">View Likes</a><br />
<br />

</div>
  {if $isApproved neq 1}
  {include file='./WhyPhotoBlur.tpl'}
  {/if}
</td>
  </tr>
  
</table>
  
 
  
  {include file='./mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
