<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333; width:100%;">
<tr>
<td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerbgrej3.jpg); background-size: cover;">
<table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
<tr>
<td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;"> 
 Dear <strong>{$name}</strong>,<br /><br />
 Thank you for your interest in TrulyMadly.com.<br /><br />
<strong>We regret to inform you that your Employment proof is invalid.</strong> <br /><br />
You may <a href ="http://www.trulymadly.com" target="_blank" style="color:#0055cc;">Log on to TrulyMadly.com</a> with your registered account details and upload another Employment proof for verification.<br /><br />
Team TrulyMadly
</td></tr></table>
</td></tr></table>
 {include file='./../mailerFooter.tpl'}
  <img src="{$analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
</body>
</html>
