<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333; width:100%;">
<tr>
<td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerbgrej1.jpg); background-size: cover;"><table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
<tr>
<td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;">
Dear <strong>{$name}</strong>,<br /><br />
Thank you for your interest in TrulyMadly.com.<br /><br />
<strong>Hi, your current location in the profile does not match the location in the uploaded address proof. We only verify the current address. Please revert back to this mail with the correct document, so that we can approve your address and get down to work. </strong><br /><br />
  You may <a href ="http://www.trulymadly.com" target="_blank" style="color:#0055cc;">Log on to TrulyMadly.com</a> with your registered account details and upload another Address proof for verification.<br /><br />
Team TrulyMadly
</td></tr></table>    
</td></tr></table>
 {include file='./../mailerFooter.tpl'}
</body>
</html>
