<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">

<!-- Emailer Top Content Section -->
<div style="min-width:318px; max-width:600px; width:98%; margin:0 auto; font-family:Helvetica, Arial; text-align:center;"><p style="float:left; margin:10px 0 10px 2px; padding:0; text-align:left; clear:both; font-size:24px; font-weight:100;">Hello {$name},</p><div style="width:100%; min-height:360px; max-height:510px; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; background:url(http://cdn.trulymadly.com/email/images/msgreceivedbg.jpg) no-repeat center center; background-size:cover; -moz-background-size:cover; -webkit-background-size:cover; clear:both; display:table;"><div style="background:rgba(28,25,35,0.7); color:#fff; padding:15px; font-size:17px; text-align:center; margin:24% 0; font-weight:100; line-height:22px;">You have{if $data.msg_count eq "1"} a new message {else} {$data.msg_count} new messages {/if}!<br>

{foreach $data.data item=val}
<a href="{$val.message_link}" style="text-decoration:none;">
<div style="background:#fff; color:#000; padding:10px; clear:both; margin:15px 0 0 0; text-shadow:none; text-align:left; overflow:hidden; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;"><img src="{$val.profile_pic}" width="50" height="50" style="float:left; border:none; margin:0 10px 0 0; border-radius:100%; -moz-border-radius:100%; -webkit-border-radius:100%; "/><ul style="list-style:none; margin:0 auto; padding:0; overflow:hidden;"><li style="color:#777; text-align:left; border-bottom:1px solid #999; padding:0; margin:0 auto; height:25px;"><span style="text-overflow:ellipsis; white-space:nowrap; display:inline-block; width:45%; overflow:hidden; text-transform:uppercase;">{$val.fname}</span> <span style="float:right; font-size:11px; font-style:italic;">{$val.timestamp}</span></li><li style=" padding:5px 0 0 0; margin:0 auto; color:#000; font-size:15px; height:20px;"><span style="width:210px; text-overflow:ellipsis; white-space:nowrap; display:inline-block; overflow:hidden;">{$val.msg}</span> </li></ul></div>
</a>
{/foreach}

</div></div></div>

{include file='./../mailerFooter.tpl'}
<img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
<img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />

</body>
</html>
