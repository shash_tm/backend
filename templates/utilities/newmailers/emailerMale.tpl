<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;"> 


<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; width:90%; min-width:360px;">
  <tr>
    <td><h2 style="font-family:'Century Gothic', Arial; font-size:22px; color:#2e3f49; font-weight:normal; margin:0 0 0 30px;">Hello {$name},</h2></td>
  </tr>
  <tr>
    <td>
    <div style="clear:both;  margin:10px 30px; background:#000; border-radius:15px; -webkit-border-radius:15px; -moz-border-radius:15px; padding:0 10px 20px 10px; overflow:hidden;">
    <p style="color:#fde7ca; font-family:'Century Gothic', Arial; font-size:18px; font-weight:normal; text-align:center; padding:0; margin:0 auto;"><img src="http://cdn.trulymadly.com/email/images/imagetie.gif" width="165" height="222" alt=""/><br><br>
<span style="font-size:24px;">Did you know that women like dating lawyers?</span><br>
At least that’s what our members say.<br><br>
We also found out what really interests these women.<br>
Here, it says quite a bit about you: <a href="http://bit.ly/emaillawyer" target="_blank" style="color:#fde7ca; font-family:'Century Gothic', Arial; font-size:18px; font-weight:normal;">10 reasons to date a lawyer</a>
<br><br>
Feeling flattered already? Just wait till you read it whole.
</p>
    </div>
    </td>
  </tr>
  <tr>
    <td align="center"><a href="{$analyticsLinks.analytics_header_link}" target="_blank"><img src="http://cdn.trulymadly.com/email/images/tmlogo1.gif" width="250" height="50" alt="TrulyMadly.com" style="margin:10px 0 5px 0; clear:both; border:none;"/></a>
    <img src="http://cdn.trulymadly.com/email/images/btmline.png" style="height:1px; width:90%;"/><br>
<p style="color:#2e3f49; font-family:'Century Gothic', Arial; font-size:20px; font-weight:normal; text-align:center; padding:0; margin:10px 0;">Want to see better matches?<br>
Take the personality quiz to help us sort better matches for you.</p>
<img src="http://cdn.trulymadly.com/email/images/btmline.png" style="height:1px; width:90%;"/>
<div style="padding:10px; color:#444; font-size:12px; text-align:center; line-height:18px; font-family:'Century Gothic', Arial;"><p style="font-size:14px; padding:0; margin:0 auto; font-family:'Century Gothic', Arial;">Follow us on:</p><a href="http://www.facebook.com/trulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/fbicon.png" alt="fb" width="29" height="29" style="border:none; margin:5px;" title="Facebook" /></a><a href="http://blog.trulymadly.com/" target="_blank"><img src="http://cdn.trulymadly.com/email/images/blogicon.png" alt="blog" width="29" height="29" style="border:none; margin:5px;" title="Blog"/></a><a href="http://twitter.com/thetrulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/twtricon.png" alt="twitter" width="29" height="29" style="border:none; margin:5px;" title="twitter" /></a><a href="http://instagram.com/trulymadlycom" target="_blank"><img src="http://cdn.trulymadly.com/email/images/instagicon.png" alt="instagram" width="29" height="29" style="border:none; margin:5px;" title="instagram"/></a><br />
        <a href="http://www.trulymadly.com/terms.php" target="_blank" style="color:#444; text-decoration:underline;">Terms of Use</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/policy.php" target="_blank" style="color:#444; text-decoration:underline;">Privacy Policy</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/guidelines.php" target="_blank" style="color:#444; text-decoration:underline;">Safety Guidelines</a> &nbsp;|&nbsp; <a href="{$analyticsLinks.analytics_unsubscribe_link}" target="_blank" style="color:#444; text-decoration:underline;">Unsubscribe</a><br />
<a href="{$analyticsLinks.analytics_footer_link}" target="_blank" style="color:#444; text-decoration:none;">trulymadly.com</a> &copy; 2014. All rights reserved.<br>
F-313C, 3rd Floor, Lado Sarai, New Delhi, India 110030</div>
<img src="http://cdn.trulymadly.com/email/images/btmline.png" style="height:10px; width:100%;"/></td>
  </tr>
  </table>
  

  <!-- {include file='./../mailerFooter.tpl'} -->
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
