<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="http://www.trulymadly.com/css/common/fontstyle.css" rel="stylesheet" type="text/css">
</head>

<body style="margin:0; padding:0;">
{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#2e3f49; width:90%; min-width:360px;">
    
  <tr>
    <td align="center" style="background:url(http://cdn.trulymadly.com/email/images/bgmailer_other.jpg) no-repeat top center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
<div style="clear:both;  margin:40px 30px; background:rgba(255,255,255,0.9); border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; padding:20px; overflow:hidden;">

<p style="font-family:'Eurofurence',Arial,Helvetica,sans-serif; font-size:18px; color:#2e3f49; margin:0 auto; line-height:22px; padding:0; text-align:left;"> Hey you,  <br><br><span style="color:#ed0c6e;">What went wrong? We thought we were doing so good.</span><br><br>  Your registration process was stopped midway and we'd like to know why. Maybe it's something we could fix. Do reply to this mail and we'll get to it. And if that was just a mistake then how about giving it another chance? Come on.<br><br> Just <a href="{$analyticsLinks.analytics_login_link}" style="color:#0072bc; text-decoration:underline; font-size:20px;">click here</a> and get back on track to find that one true, mad love you've always been waiting for.</p>
<p style="font-family:'Eurofurence',Arial,Helvetica,sans-serif; margin:20px 0 0 0; padding:10px 0 0 0; font-size:15px; color:#2e3f49; border-top:1px dashed #888; display:inline-block; white-space:nowrap; line-height:20px; text-align:left; float:left;">Truly and a Little Madly,<br>
Team TrulyMadly</p>
</div>

</td>
  </tr>
  </table>
  {include file='./../mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
    <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
</body>
</html>
