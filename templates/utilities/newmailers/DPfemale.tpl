<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">

<!-- Emailer Top Content Section -->
<div style="min-width:318px; max-width:600px; width:98%; margin:0 auto; font-family:Helvetica, Arial; text-align:center;"><p style="float:left; margin:10px 0 10px 2px; padding:0; text-align:left; clear:both; font-size:24px; font-weight:100;">Hey {$name},</p><a href="http://blog.trulymadly.com/profile-picture/" style="text-decoration:none; cursor:pointer;"><div style="width:100%; min-height:360px; max-height:510px; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; background:url(http://cdn.trulymadly.com/email/images/photomailerbg.jpg) no-repeat center center; background-size:cover; -moz-background-size:cover; -webkit-background-size:cover; clear:both; display:table;"><div style="background:rgba(5,4,2,0.7); color:#fff; padding:15px 5px; font-size:17px; text-align:center; margin:30% 0; font-weight:100; line-height:18px; text-shadow: 1px 2px 1px #000;">Get more guys to like your Display Picture.<br><br>Pick the perfect one by <span style="color:#fd8cb5; font-style:italic; display:inline-block; border-bottom:1px solid #fd8cb5;">following some simple tips here</span></div></div></a></div>

{include file='./../mailerFooter.tpl'}
<img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
<img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />

</body>
</html>
