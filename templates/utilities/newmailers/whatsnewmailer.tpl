<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
@font-face {
    font-family: 'Eurofurence Light';
    src: url('http://www.trulymadly.com/css/fonts/eurof35.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'Eurofurence';
    src: url('http://www.trulymadly.com/css/fonts/eurof55.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}
</style>
</head>

<body style="margin:0; padding:0;">
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(http://cdn.trulymadly.com/email/images/bgmailer.jpg); background-color:#38434f; width:680px;">
  <tr>
    <td align="center"><a href="{$analyticsLinks.analytics_login_link}" target="_blank"><img src="http://cdn.trulymadly.com/email/images/trulymadly_logo.png" width="172" height="35" style="margin:10px 0 10px 10px; border:none; float:left" alt="TrulyMadly.com" /></a><div style="margin:5px 10px 0 0; float:right;"><img src="http://cdn.trulymadly.com/email/images/androidtmapp.png" width="27" height="45" style="margin:0 5px 0 0; float:left;"><p style="float:right; margin:5px 0 0 0; padding:0; text-align:left; white-space:nowrap; line-height:18px; font-size:12px; font-family:'Eurofurence','Open Sans',Arial,sans-serif; color:#fff; ">Trulymadly Android App<br><a target="_blank" href="https://play.google.com/store/apps/details?id=com.trulymadly.android.app" style="color:#fff; text-decoration:none; font-size:14px;">DOWNLOAD THE LOVE APP</a></div></td>
  </tr>
  </table>
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerbgwn.jpg); background-color:#38434f; color:#fff; width:680px;">
    
  <tr>
    <td align="center">
<div style="margin:20px; overflow:hidden; font-family:'Eurofurence','Open Sans',Arial,sans-serif; color:#fff; ">
<h3 style="font-family:'Eurofurence','Open Sans',Arial,sans-serif; font-size:22px; font-weight:normal; color:#fff; margin:0 auto; padding:0; text-align:left;">Hello {$name},</h3>
<p style="font-family:'Eurofurence','Open Sans',Arial,sans-serif; font-size:18px; color:#fff; margin:20px 0 0 0; line-height:22px; padding:0; text-align:left;">We've revamped our look, made it easy to use and added new features. <br>We also have a new app for you.</p>
<h2 style="font-family:'Eurofurence Light','Open Sans Light',Arial,sans-serif; font-size:50px; font-weight:normal; color:#fff; margin:60px 0; padding:0; text-align:left; clear:both;">TRULY NEW.<br>MADLY IMPROVED.</h2>
<a href="{$analyticsLinks.analytics_login_link}" target="_blank" style="background:#ed0c6e; color:#fff; font-family:'Eurofurence','Open Sans',Arial,sans-serif; font-size:26px; text-align:left; padding:10px 20px; text-decoration:none; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; display:inline-block; clear:both; float:left;">LOG IN</a>
<div style="clear:both;"></div>
<div style="border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; background:rgba(255,255,255,0.9); width:100%; overflow:hidden; margin:60px 0 0 0; clear:both;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#2e3f49; font-size:14px;">
  <tr>
    <td style="width:25%;" valign="top">
    <p style="font-size:18px; color:#ed0c6e; margin:0 auto; padding:10px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">Shiny<br><span style="color:#0072bc; font-size:17px;">New Mobile App</span></p>
    <ul style="margin:0 5px 10px 10px; text-align:left; padding:0 0 0 15px; font-size:13px; color:#2e3f49; overflow:hidden; line-height:14px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">
    <li style="padding:0 0 5px;  margin:0;">Easy accessibility</li>
    <li style="padding:0 0 5px;  margin:0;">Realtime chat</li>
    <li style="padding:0 0 5px;  margin:0;">Realtime notifications </li>
    </ul>
    </td>
    <td style="width:25%; border-left:1px solid #ddd;" valign="top">
    <p style="font-size:18px; color:#ed0c6e; margin:0 auto; padding:10px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">New<br><span style="color:#0072bc; font-size:17px;">Look and Feel</span></p>
    <ul style="margin:0 5px 10px 10px; text-align:left; padding:0 0 0 15px; font-size:13px; color:#2e3f49; overflow:hidden; line-height:14px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">
    <li style="padding:0 0 5px;  margin:0;">Easy to use and navigate</li>
    <li style="padding:0 0 5px;  margin:0;">New carousel view to see matches</li>
    <li style="padding:0 0 5px;  margin:0;">Mobile responsive</li>
    </ul>
    </td>
    <td style="width:25%; border-left:1px solid #ddd;" valign="top">
    <p style="font-size:18px; color:#ed0c6e; margin:0 auto; padding:10px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">Deeper<br><span style="color:#0072bc; font-size:17px;">Compatibility Analysis</span></p>
    <ul style="margin:0 5px 10px 10px; text-align:left; padding:0 0 0 15px; font-size:13px; color:#2e3f49; overflow:hidden; line-height:14px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">
    <li style="padding:0 0 5px;  margin:0;">New personality match quizzes</li>
    <li style="padding:0 0 5px;  margin:0;">Focused common interests</li>
    <li style="padding:0 0 5px;  margin:0;">More matches per day</li>
    </ul>
    </td>
    <td style="width:25%; border-left:1px solid #ddd;" valign="top">
    <p style="font-size:18px; color:#ed0c6e; margin:0 auto; padding:10px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">More<br><span style="color:#0072bc; font-size:17px;">Private and Discreet</span></p>
    <ul style="margin:0 5px 10px 10px; text-align:left; padding:0 0 0 15px; font-size:13px; color:#2e3f49; overflow:hidden; line-height:14px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">
    <li style="padding:0 0 5px;  margin:0;">'Like' or 'Nope' matches anonymously</li>
    <li style="padding:0 0 5px;  margin:0;">Chat with only matches you've liked</li>
    <li style="padding:0 0 5px;  margin:0;">Block and report abuse</li>
    </ul>
    </td>
  </tr>
</table>

</div>

</div>

</td>
  </tr>
  </table>
<table border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Eurofurence','Open Sans',Arial,sans-serif; color:#333; width:680px;">
  <tr>
    <td align="center" bgcolor="#e6e7e8">
<div style="font-size: 16px; width:30%; height:auto; text-align:center; padding:1.5%; float:left; font-family:'Eurofurence','Open Sans',Arial,sans-serif;"><img src="http://cdn.trulymadly.com/email/images/seal.png" width="42" height="41" style="margin-bottom:5px;" /><br /><p style="margin:0; padding:0;">Verified Profiles</p>
</div>
<div style="font-size:16px; width:30%; height:auto; text-align:center; padding:1.5%; float:left; font-family:'Eurofurence','Open Sans',Arial,sans-serif;  border-right:1px solid #fff;  border-left:1px solid #fff; "><img src="http://cdn.trulymadly.com/email/images/couple.png" width="49" height="41" style="margin-bottom:5px;" /><br />
  <p style="margin:0; padding:0;">True Compatibility</p>
</div>
<div style="font-size: 16px; width:30%; height:auto; text-align:center; padding:1.5%; float:right; font-family:'Eurofurence','Open Sans',Arial,sans-serif;"><img src="http://cdn.trulymadly.com/email/images/lock.png" width="30" height="41"  style="margin-bottom:5px;"/><br />
  <p style="margin:0; padding:0;">Private & Safe</p>
</div>

</td>
  </tr>
    <tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/bgmailer.jpg); background-color:#38434f;"><div style="padding:10px; color:#fff; font-size:12px; text-align:center; line-height:18px; font-family:'Eurofurence','Open Sans',Arial,sans-serif;"><p style="font-size:14px; padding:0; margin:0 auto; font-family:'Eurofurence','Open Sans',Arial,sans-serif;">Follow us on:</p><a href="http://www.facebook.com/trulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/fbicon.png" alt="fb" width="29" height="29" style="border:none; margin:5px;" title="Facebook" /></a><a href="http://blog.trulymadly.com/" target="_blank"><img src="http://cdn.trulymadly.com/email/images/blogicon.png" alt="blog" width="29" height="29" style="border:none; margin:5px;" title="Blog"/></a><a href="http://twitter.com/thetrulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/twtricon.png" alt="twitter" width="29" height="29" style="border:none; margin:5px;" title="twitter" /></a><a href="http://instagram.com/trulymadlycom" target="_blank"><img src="http://cdn.trulymadly.com/email/images/instagicon.png" alt="instagram" width="29" height="29" style="border:none; margin:5px;" title="instagram"/></a><br />
        <a href="http://www.trulymadly.com/terms.php" target="_blank" style="color:#fff; text-decoration:underline;">Terms of Use</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/policy.php" target="_blank" style="color:#fff; text-decoration:underline;">Privacy Policy</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/guidelines.php" target="_blank" style="color:#fff; text-decoration:underline;">Safety Guidelines</a> &nbsp;|&nbsp; <a href="{$analyticsLinks.analytics_unsubscribe_link}" target="_blank" style="color:#fff; text-decoration:underline;">Unsubscribe</a><br />
<a href="{$analyticsLinks.analytics_footer_link}" target="_blank" style="color:#fff; text-decoration:underline;">trulymadly.com</a> &copy; 2014. All rights reserved. F-313C, 3rd Floor, Lado Sarai, New Delhi, India 110030</div></td>
  </tr>
  </table>

  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
    <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
</body>
</html>
