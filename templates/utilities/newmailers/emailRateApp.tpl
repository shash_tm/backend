<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="http://www.trulymadly.com/css/common/fontstyle.css" rel="stylesheet" type="text/css">
</head>

<body style="margin:0; padding:0;"> 

{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#fff; width:90%; min-width:360px;">
    
  <tr>
    <td align="left" valign="top" style="background:url(http://cdn.trulymadly.com/email/images/rateappbg.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; height:360px;">
<div style="clear:both;  margin:30px; overflow:hidden; text-shadow: 1px 1px 0 rgba(0,0,0,0.9); -moz-text-shadow: 1px 1px 0 rgba(0,0,0,0.9); -webkit-text-shadow: 1px 1px 0 rgba(0,0,0,0.9);">
<h2 style="font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#333f4a; font-size:22px; font-weight:normal; color:#fff; margin:0 auto; padding:0;">Hi {$fname},</h2>
<div style="margin:20px 0 0 0;">
<p style="font-size:22px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; margin:20px 0 0 0; color:#fff; clear:both; line-height:24px;">
<a href="https://play.google.com/store/apps/details?id=com.trulymadly.android.app" style="color:#fff; font-size:20px; text-align:center; margin:0 auto; padding:3px 5px; border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px; background-color:#ed0c6e; display:inline-block; text-decoration:none; text-shadow:none; font-weight:bold;" target="_blank">Rate us</a>  and express your love for us.<br>
It'll hardly take a minute.<br><br>
We don't mind compliments too ;)
</p>

</div>
</div>


</td>
  </tr>
  </table>
  

  {include file='./../mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
