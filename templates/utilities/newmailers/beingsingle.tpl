<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;"> 


<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; width:90%; min-width:320px; max-width:720px;">
  <tr>
    <td><h2 style="font-family:'Century Gothic', Arial; font-size:22px; color:#2e3f49; font-weight:normal; margin:0 0 0 10px;">Hey {$name},</h2></td>
  </tr>
  <tr>
    <td><a href="http://blog.trulymadly.com/12-bahanas-make-remain-blissfully-single/" target="_blank" style="color:#fff; font-family:'Century Gothic', Arial; font-size:16px; font-weight:normal; text-decoration:none;"><div style="clear:both;  margin:10px; background:url(http://cdn.trulymadly.com/email/images/beingsinglebg.jpg) no-repeat center center; border-radius:15px; -webkit-border-radius:15px; -moz-border-radius:15px; padding:0; overflow:hidden; text-align:center; height:440px;"><img src="http://cdn.trulymadly.com/email/images/bahanaslogo.png" width="92" height="64" alt="" style="margin:20px 0 230px 0;"/>
      <p style="color:#fff; font-family:'Century Gothic', Arial; font-size:16px; font-weight:normal; text-align:center; padding:10px; margin:0 auto; line-height:22px; clear:both; background:rgba(0,0,0,0.4);">Everyone's got their bahana for being single. What's yours?<br><span style="text-decoration:underline;">Here are some popular bahanas</span><br>Feel free to use any of them!</p>
    </div></a>
    </td>
  </tr>
  <tr>
    <td align="center"><a href="{$analyticsLinks.analytics_header_link}" target="_blank"><img src="http://cdn.trulymadly.com/email/images/tmlogo1.gif" width="250" height="50" alt="TrulyMadly.com" style="margin:10px 0 5px 0; clear:both; border:none;"/></a>

<img src="http://cdn.trulymadly.com/email/images/btmline.png" style="height:1px; width:100%;"/><br>
<p style="color:#2e3f49; font-family:'Century Gothic', Arial; font-size:18px; font-weight:normal; text-align:center; padding:0; margin:10px 0;">Want to see better matches?<br>
You can change your preferences anytime and help us find you better matches.
</p>
<img src="http://cdn.trulymadly.com/email/images/btmline.png" style="height:1px; width:100%;"/>
<div style="padding:10px; color:#444; font-size:12px; text-align:center; line-height:18px; font-family:'Century Gothic', Arial;"><p style="font-size:14px; padding:0; margin:0 auto; font-family:'Century Gothic', Arial;">Follow us on:</p><a href="http://www.facebook.com/trulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/fbicon.png" alt="fb" width="29" height="29" style="border:none; margin:5px;" title="Facebook" /></a><a href="http://blog.trulymadly.com/" target="_blank"><img src="http://cdn.trulymadly.com/email/images/blogicon.png" alt="blog" width="29" height="29" style="border:none; margin:5px;" title="Blog"/></a><a href="http://twitter.com/thetrulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/twtricon.png" alt="twitter" width="29" height="29" style="border:none; margin:5px;" title="twitter" /></a><a href="http://instagram.com/thetrulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/instagicon.png" alt="instagram" width="29" height="29" style="border:none; margin:5px;" title="instagram"/></a><a href="https://www.youtube.com/user/trulymadlycom" target="_blank"><img src="http://cdn.trulymadly.com/email/images/youtubeicon.png" alt="YouTube" width="29" height="29" style="border:none; margin:5px;" title="YouTube"/></a><br />
        <a href="http://www.trulymadly.com/terms.php" target="_blank" style="color:#444; text-decoration:underline;">Terms of Use</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/policy.php" target="_blank" style="color:#444; text-decoration:underline;">Privacy Policy</a> &nbsp;|&nbsp; <a href="http://www.trulymadly.com/guidelines.php" target="_blank" style="color:#444; text-decoration:underline;">Safety Guidelines</a> &nbsp;|&nbsp; <a href="{$analyticsLinks.analytics_unsubscribe_link}" target="_blank" style="color:#444; text-decoration:underline;">Unsubscribe</a><br />
<a href="{$analyticsLinks.analytics_footer_link}" target="_blank" style="color:#444; text-decoration:none;">trulymadly.com</a> &copy; 2014. All rights reserved.<br>
F-313C, 3rd Floor, Lado Sarai, New Delhi, India 110030</div>
<img src="http://cdn.trulymadly.com/email/images/btmline.png" style="height:10px; width:100%;"/></td>
  </tr>
  </table>
  

  <!-- {include file='./../mailerFooter.tpl'} -->
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
