<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="http://www.trulymadly.com/css/common/fontstyle.css" rel="stylesheet" type="text/css">
</head>

<body style="margin:0; padding:0;">
{include file='templates/utilities/mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#2e3f49; width:90%; min-width:360px;">
    
  <tr>
    <td align="center" style="background:url(http://cdn.trulymadly.com/email/images/bgmailer_other.jpg) no-repeat top center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
<div style="clear:both;  margin:40px 30px; background:rgba(255,255,255,0.9); border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; padding:20px;">
<h2 style="font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#333f4a; font-size:24px; font-weight:normal; color:#2e3f49; margin:0 auto; padding:0">Hello {$name},</h2>
<div style="font-size:20px; font-family:'Eurofurence',Arial,Helvetica,sans-serif; margin:20px 0 0 0;"><span style="color:#ed0c6e;">Welcome to the TrulyMadly family.</span> <br /><br /><p style="font-size:18px; color:#2e3f49; margin:0 auto; line-height:22px; padding:0;"> Now that we have everything in place, let's get to business. We hope to find you your soulmate and we're gearing up for just that. <br /><br />
First things first, get ready to meet a lot of amazing people ☺</p>
<p style="margin:20px 0 0 0; padding:10px 0 0 0; font-size:15px; color:#2e3f49; border-top:1px dashed #888; display:inline-block; white-space:nowrap; line-height:20px;">Truly and a Little Madly,<br>
Team TrulyMadly</p></div>
</div>

</td>
  </tr>
  </table>
  {include file='templates/utilities/mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
    <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
</body>
</html>
