<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">

<!-- Emailer Top Content Section -->
<div style="min-width:318px; max-width:600px; width:98%; margin:0 auto; font-family:Helvetica, Arial; text-align:center;"><p style="float:left; margin:10px 0 10px 2px; padding:0; text-align:left; clear:both; font-size:24px; font-weight:100;">Hey {$name},</p><div style="width:100%; min-height:360px; max-height:510px; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; background:url(http://cdn.trulymadly.com/email/images/endorsement.jpg) no-repeat bottom center; background-size:cover; -moz-background-size:cover; -webkit-background-size:cover; clear:both; display:table;"><div style="background:rgba(59,104,162,0.5); color:#fff; padding:15px; font-size:17px; text-align:center; margin:34% 0; font-weight:100; line-height:18px; "><strong style="font-weight:600;">{$endorser_name}</strong> has given you a thumbs up!

{if isset($content)}
<br><br>
{$content}
{/if}

</div></div></div>

{include file='./../mailerFooter.tpl'}

<img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
<img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />

</body>
</html>
