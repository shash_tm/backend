<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">

<!-- Emailer Top Content Section -->
<div style="min-width:318px; max-width:600px; width:98%; margin:0 auto; font-family:Helvetica, Arial; text-align:center;"><p style="float:left; margin:10px 0 10px 2px; padding:0; text-align:left; clear:both; font-size:24px; font-weight:100;">Hey {$name},</p><div style="width:100%; min-height:360px; max-height:510px; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; background:url(http://cdn.trulymadly.com/email/images/passwordbg.jpg) no-repeat center center; background-size:cover; -moz-background-size:cover; -webkit-background-size:cover; clear:both; display:table;"><div style="background:rgba(16,39,22,0.7); color:#fff; padding:15px 5px; font-size:17px; text-align:center; margin:27% 0; font-weight:100; line-height:20px; ">Your Photo ID is password protected.<br>Send us the password, and we promise we'll keep our lips sealed.<br><a href="{$analyticsLinks.analytics_login_link}"  style="text-decoration:none; cursor:pointer; text-decoration:none; font-weight:600; border-bottom:1px solid #fff; padding-bottom:1px; color:#fff;"> Complete your verification process here </a></div></div></div>

{include file='./../mailerFooter.tpl'}
<img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
<img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />

</body>
</html>
