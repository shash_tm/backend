<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="http://www.trulymadly.com/css/common/fontstyle.css" rel="stylesheet" type="text/css">
</head>

<body style="margin:0; padding:0;"> 

{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#2e3f49; width:90%; min-width:360px;">
    
  <tr>
    <td align="center" valign="top" style="background:url(http://cdn.trulymadly.com/email/images/likesmailerbg.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; height:360px;">
<div style="clear:both;  margin:30px; overflow:hidden; text-shadow: 1px 1px 0 rgba(255,255,255,0.9); -moz-text-shadow: 1px 1px 0 rgba(255,255,255,0.9); -webkit-text-shadow: 1px 1px 0 rgba(255,255,255,0.9);">
<h2 style="font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#333f4a; font-size:22px; font-weight:normal; color:#2e3f49; margin:0 auto; padding:0;">Hello {$name},</h2>
<div style="font-size:22px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; margin:20px 0 0 0;">
<p style="font-size:22px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; margin:20px 0 0 0; color:#ed0c6e; clear:both;">You've been 'liked' by one of your matches.<br>
<span style="font-size:20px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#2e3f49;">Maybe you'll 'like' him too.</span><br><br>
<a href="#" style="color:#fff; font-size:20px; text-align:center; margin:0 10px 10px 10px; padding:7px 10px; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; background-color:#ed0c6e; display:inline-block; text-decoration:none; -webkit-box-shadow: 0 2px 1px #ca0058; -moz-box-shadow: 0 2px 1px #ca0058; box-shadow: 0 2px 1px #ca0058; text-shadow:none;">Find out right away!</a>

</p>

</div>
</div>


</td>
  </tr>
  </table>
  

  {include file='./../mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
