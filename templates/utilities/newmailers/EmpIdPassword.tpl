<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="http://www.trulymadly.com/css/common/fontstyle.css" rel="stylesheet" type="text/css">
</head>

<body style="margin:0; padding:0;">
{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#2e3f49; width:90%; min-width:360px;">
    
  <tr>
    <td align="center" style="background:url(http://cdn.trulymadly.com/email/images/bgmailer_match.jpg) no-repeat top center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
<div style="clear:both;  margin:40px 30px; background:rgba(255,255,255,0.9); border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; padding:20px;">
<div style="font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; margin:0 auto;"><p style="font-size:18px; color:#2e3f49; margin:0 auto; line-height:22px; padding:0;">Hello {$fname},<br><br> <span style="color:#ed0c6e;">Document you have submitted is password protected</span>. <br><br> How about you <a href="{$analyticsLinks.analytics_login_link}" style="color:#0072bc; text-decoration:underline;">send us the password</a> and we promise not to share it with anyone? Sounds good?</p>
<p style="margin:20px 0 0 0; padding:10px 0 0 0; font-size:15px; color:#2e3f49; border-top:1px dashed #888; display:inline-block; white-space:nowrap; line-height:20px;">Truly and a Little Madly,<br>
Team TrulyMadly</p></div>
</div>

</td>
  </tr>
  </table>
  {include file='./../mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
    <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
</body>
</html>
