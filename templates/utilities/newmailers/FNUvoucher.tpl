<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="http://www.trulymadly.com/css/common/fontstyle.css" rel="stylesheet" type="text/css">
</head>

<body style="margin:0; padding:0;"> 


<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(http://cdn.trulymadly.com/email/images/bgmailer.jpg); background-color:#38434f; width:90%; min-width:360px;">
  <tr>
    <td align="center"><a href="{$analyticsLinks.analytics_header_link}" target="_blank"><img src="http://cdn.trulymadly.com/email/images/trulymadly_logo.png" width="172" height="35" style="margin:10px 0 10px 10px; border:none; float:left" alt="TrulyMadly.com" /></a><a href="http://www.fashionandyou.com/" target="_blank"><img src="http://cdn.trulymadly.com/email/images/fnulogo.png" width="186" height="40" style="margin:10px 0 0 10px; border:none; float:left" alt="Fashion And You" /></a><div style="margin:5px 10px 0 0; float:right;"><img src="http://cdn.trulymadly.com/email/images/androidtmapp.png" width="27" height="45" style="margin:0 5px 0 0; float:left;"><p style="float:right; margin:5px 0 0 0; padding:0; text-align:left; white-space:nowrap; line-height:16px; font-size:12px; font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#fff; ">Trulymadly Android App<br><a target="_blank" href="https://play.google.com/store/apps/details?id=com.trulymadly.android.app" style="color:#fff; text-decoration:none;">DOWNLOAD NOW &raquo;</a></div></td>
  </tr>
  </table>



<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#2e3f49; font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#2e3f49; width:90%; min-width:360px;">
    
  <tr>
    <td align="center" style="background:url(http://cdn.trulymadly.com/email/images/fnubg.jpg?v=1) no-repeat top left; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
<div style="clear:both;  margin:30px; background:rgba(255,255,255,0.8); border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; padding:20px; overflow:hidden;">
<h2 style="font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#333f4a; font-size:18px; font-weight:normal; color:#2e3f49; margin:0 auto; padding:0;">Hello {$name},</h2>
<div style="font-size:18px; font-family:'Eurofurence',Arial,Helvetica,sans-serif; margin:20px 0 0 0;">
<p style="font-size:18px; font-family:'Eurofurence',Arial,Helvetica,sans-serif; margin:20px 0 0 0; color:#ed0c6e; clear:both;">You’ve just won yourself a FashionAndYou voucher worth Rs. 2500! </p>
<div style="text-align:center; background-color:#2e3f49; color:#fff; font-size:18px; text-align:center; width:250px; padding:5px 0; margin:15px 0 0 0; clear:both;">{$voucher.voucher_code}</div>
<p style="font-size:15px; font-family:'Eurofurence',Arial,Helvetica,sans-serif; margin:15px 0 0 0; color:#ed0c6e; clear:both; line-height:24px;">And, a chance to meet interesting singles on the TrulyMadly App.
  <br>
  <span style="font-size:14px; font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#2e3f49; clear:both;">We’ve got tons of verified singles waiting to see your profile. So get going already!</span><br>
<a href="https://play.google.com/store/apps/details?id=com.trulymadly.android.app" target="_blank" style="font-size:15px; color:#0072bc; font-family:'Eurofurence',Arial,Helvetica,sans-serif;">Don’t forget to show us some love by rating us here.</a></p>
<a href="http://www.trulymadly.com/mailerterms.php" target="_blank" style="float:right; font-size:12px; font-family:'Eurofurence',Arial,Helvetica,sans-serif; color:#777; font-style:italic; padding:0; margin:0 auto;">*T&C apply</a>
</div>
</div>


</td>
  </tr>
  </table>
  

  {include file='./../mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
