<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">

<!-- Emailer Top Content Section -->
<div style="min-width:318px; max-width:600px; width:98%; margin:0 auto; font-family:Helvetica, Arial; text-align:center;"><p style="float:left; margin:10px 0 10px 2px; padding:0; text-align:left; clear:both; font-size:24px; font-weight:100;">Hello {$name},</p><div style="width:100%; min-height:360px; max-height:510px; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; background:url(http://cdn.trulymadly.com/email/images/mutualmatch.jpg) no-repeat right center; background-size:cover; -moz-background-size:cover; -webkit-background-size:cover; clear:both; display:table;"><div style="background:rgba(55,53,52,0.5); color:#fff; padding:15px; font-size:17px; text-align:center; margin:20% 0; font-weight:100; line-height:22px;">You have a mutual match! <span style="display:inline-block;"><strong style="font-weight:600;">{$matchName}</strong> likes you too.</span><br>
{foreach $data item=val}
<a href="{$val.message_link}" style="text-decoration:none;">
<div style="background:rgba(255,255,255,1); color:#000; padding:10px 10px 5px 10px; clear:both; margin:10px 5px 5px 5px; text-align:left; overflow:hidden; display:inline-block; clear:both; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;"><img src="{$val.profile_pic}" width="60" height="60" style="float:left; border:none; margin:0 10px 0 0; border-radius:100%; -moz-border-radius:100%; -webkit-border-radius:100%; "/><ul style="list-style:none; margin:0 auto; padding:0; overflow:hidden; display:inline-block; line-height:20px;"><li style="text-align:left; padding:0; margin:0 auto; text-transform:uppercase; color:#000;">{$val.fname}</li><li style=" padding:0; margin:0 auto; color:#000; font-size:15px;">{$val.age}, {$val.city}</li><li style=" padding:0; margin:0 auto; color:#000;font-size:11px; font-style:italic;">Matched on: {$ts}</li></ul>

<div style="clear:both;">
{foreach $hashTags item=val1}
<p style=" background:#e6e7e9; font-size:13px; margin:0 4px 4px 0; padding:2px 8px; border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; float:left;">{$val1}</p>
{/foreach}
</div>

{/foreach} 

</div>
</a>
<br>
{if $ifHashTagMatches eq 'true'}
Looks like you got a lot in common! <br> <a href="{$val.message_link}" style="color:#fff; border-bottom:1px solid #fff; padding-bottom:1px; text-decoration:none;">Go ahead, strike a conversation!</a>
{else}
It's time to get to know each other better!
{/if}
</div></div></div>

{include file='./../mailerFooter.tpl'}
<img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
<img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />

</body>
</html>
