<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">

<!-- Emailer Top Content Section -->
<div style="min-width:318px; max-width:600px; width:98%; margin:0 auto; font-family:Helvetica, Arial; text-align:center;"><p style="float:left; margin:10px 0 10px 2px; padding:0; text-align:left; clear:both; font-size:24px; font-weight:100;">Hey {$name},</p><div style="width:100%; min-height:360px; max-height:510px; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; background:url(http://cdn.trulymadly.com/email/images/photorejmlrbg.jpg) no-repeat center center; background-size:cover; -moz-background-size:cover; -webkit-background-size:cover; clear:both; display:table;"><div style="background:rgba(43,19,13,0.7); color:#fff; padding:15px 5px; font-size:17px; text-align:center; margin:24% 0; font-weight:100; line-height:20px;">We cannot approve your profile picture.<br><br>Your profile picture says a lot about you so we want the best for you. <br><a href="http://blog.trulymadly.com/profile-picture/" style="text-decoration:none; cursor:pointer; color:#fff; border-bottom:1px solid #fff; padding-bottom:1px;">Here's some tips</a> that'll help you pick the perfect one<br><a href="{$analyticsLinks.analytics_photo_link}" style="background:#ffb3b9; color:#000; padding:10px; clear:both; margin:15px 0 0 0; display:inline-block; text-decoration:none; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;">Upload a new profile picture</a></div></div></div>

{include file='./../mailerFooter.tpl'}
<img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
<img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />

</body>
</html>
