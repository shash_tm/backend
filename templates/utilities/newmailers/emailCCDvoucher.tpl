<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="http://www.trulymadly.com/css/common/fontstyle.css" rel="stylesheet" type="text/css">
</head>

<body style="margin:0; padding:0;"> 

{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#2e3f49; width:90%; min-width:360px;">
    
  <tr>
    <td align="center" style="background:url(http://cdn.trulymadly.com/email/images/mailerccdbg.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
<div style="clear:both;  margin:30px; background:rgba(255,255,255,0.7); border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; padding:20px; overflow:hidden;">
<h2 style="font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#333f4a; font-size:24px; font-weight:normal; color:#2e3f49; margin:0 auto; padding:0;">Hello {$name},</h2>
<div style="font-size:22px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; margin:20px 0 0 0;">
<p style="font-size:22px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; margin:20px 0 0 0; color:#ed0c6e; clear:both;">Yay! You've won a Cafe Coffee Day voucher worth Rs 50.</p>
<div style="text-align:center; margin:0 auto; background-color:#2e3f49; color:#fff; font-size:22px; text-align:center; padding:7px 10px; margin:20px 0 0 0; clear:both; display:inline-block;">{$voucher.voucher_code}</div><br>
<span style="font-size:14px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#2e3f49; clear:both;">Redeemable at any CCD outlet in India.</span><br>
<p style="float:right; font-size:11px; font-family:'Eurofurence','Century Gothic',Arial,Helvetica,sans-serif; color:#777; font-style:italic; padding:10px 0 0 0; margin:0 auto;">*Valid for 6 months. T&C apply</p>
</div>
</div>


</td>
  </tr>
  </table>
  

  {include file='./../mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
