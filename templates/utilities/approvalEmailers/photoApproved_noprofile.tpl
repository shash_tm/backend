<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333; width:100%;">
<tr>
<td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerimg1.jpg); background-size: cover;">
	<table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
		<tr>
		<td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;">     
		Dear <strong>{$name}</strong>,<br /><br/>
		Your picture look great and we have approved them but this photo cannot be set as profile photo.This may be because the photo is not in accordance with our Photo Guidelines, which are:<br /><br />
		<img src="{$pic}" width="120" height="120" style="float:left; margin:0 auto; border:none;" />
  			<ul style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; float:left; margin:5px 0 0 30px; padding:0; list-style:disc; color:#000;">
    			<li> No photos with sunglasses</li>
    			<li> No group photos</li>
    			<li> No blurred photos</li>
    			<li> No obscene photos</li>
    			<li> No animated sketches </li>
    			<li> No copyrighted photos</li>
  			</ul>
	
<div style="clear:both;"><br /></div>
 <a href="http://www.trulymadly.com/photo.php" target="_blank" style="color:#0067dd; text-decoration:underline; font-size:14px; font-weight:bold; ">Upload more photos</a> to get more responses!<br /><br />
 
Truly & a little Madly,<br />
Team TrulyMadly
		</td>
		</tr>
	</table>
</td>
</tr>
</table>
 {include file='./../mailerFooter.tpl'}
  <img src="{$analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
</body>
</html>
