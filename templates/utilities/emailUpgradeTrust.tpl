<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./mailerHeader.tpl'}
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333;width:100%;">
  
  
  <tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerbgrej3.jpg); background-size: cover;">
    <table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
      <tr>
        <td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;">
        Hi <strong>{$name}</strong>,<br /><br />
You have made a great start and are closer to finding someone special, but because <strong>your trust score is below 25%, you are not visible to other members.</strong><br /><br />
This is the reason:<br />
<ul style="margin:15px; padding:0;">
<li>You have not received any interest / like or message till date</li>
<li>You are unable to express interest in profiles recommended to you</li>
</ul>
<strong>Only profiles with a Trust Score of 25% or more are shown to members, including you to keep up our promise of a real and safe community.</strong><br />
<br />
You can increase your trust score by going here:<br />
 <a href ="{$analytics_login_link}" target="_blank" style="color:#0055cc;">http://www.trulymadly.com/trustbuilder.php</a><br />
<br />
Don't worry all your private information is completely secure and confidential with us. So go ahead and connect with your soul mate on trulymadly.com now!<br />
<br />

Truly & a little Madly,<br />
Team TrulyMadly
        </td>
      </tr>
    </table>    </td>
  </tr>
  
  </table>
    {include file='./mailerFooter.tpl'}
  <img src="{$analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
</body>
</html>
