<!DOCTYPE html>
<html>
  <head>
    <style>
      #map_canvas {
        width: 285px;
        height: 200px;
      }
    </style>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script>
      function initialize() {
        var map_canvas = document.getElementById('map_canvas');
        var myLatlng = new google.maps.LatLng({$lat},{$lng});
        var map_options = {
          center: myLatlng,
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(map_canvas, map_options)
      
      
   // var marker = new google.maps.Marker({
     //   position: myLatlng,
       // map: map,
        //title: 'My location'
    //});
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
  <body>
    <div id="map_canvas"></div>
  </body>
</html>
