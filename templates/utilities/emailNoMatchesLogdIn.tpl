<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333;width:100%;">
<tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerimg1.jpg); background-size: cover;">
    <table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
      <tr>
        <td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;">
        Hey <strong>{$name}</strong>,<br /><br />
We just wanted to let you know that we are working on your matches. It’s taking longer than usual as some of your preferences are constraining us - how about you loosen out a bit :)<br />
<br />
<strong>Some Tips:</strong>
<ul style="margin:10px 0 20px 10px; clear:both;">
<li>relax the preferred age range by +/- 1 year</li>
<li>relax the preferred height range by +/- 1 inch</li>
<li>open up the industry and education criteria if set very specific</li>
</ul>

Truly & a little Madly,<br />
Team TrulyMadly
        </td>
      </tr>
    </table>    </td>
  </tr>
  
</table>
  {include file='./mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
