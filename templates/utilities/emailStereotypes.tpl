<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url(http://cdn.trulymadly.com/email/images/bgmailer.jpg); font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333; width:100%;">
<tr>
    <td align="center">
    <table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:5px 0 30px 0; width:90%; background-color:#ffffff;">
      <tr>
        <td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background:#fff; font-size:14px; line-height:18px; text-align:left;">
        Hello  <strong>{$name}</strong>!<br /><br />
If you have been following our <a href="https://www.facebook.com/trulymadly" style="font-weight:bold; color:#3b5998;" target="_blank">Facebook page</a>, you would have noticed our new campaign- <a href="https://www.facebook.com/trulymadly" style="color:#e6296d; font-weight:bold; text-decoration:none;" target="_blank">#BreakingStereotypes</a>. We have chosen a number of common misconceptions everyone faces and debunked them using fun and colourful pictures!<br /><br />
<div style="clear:both;"></div>
<div style="text-align:center;">
<a href="https://www.facebook.com/trulymadly" target="_blank"><img src="http://cdn.trulymadly.com/email/images/Stype1.jpg" alt="I am an Artist and I am Not Unemployed" width="169" height="167" border="0" style="margin:5px;"><img src="http://cdn.trulymadly.com/email/images/Stype2.jpg" alt="I wear Red Lipstik and I am not Easy" width="169" height="167" border="0" style="margin:5px;"><img src="http://cdn.trulymadly.com/email/images/Stype3.jpg" alt="I am a Man and
I Love to Cook" width="169" height="167" border="0" style="margin:5px;"></a></div>
<div style="clear:both;"></div>
 It is our belief that love can't be find using statistics like <em style="color:#666;">"5'11", Hindu, Engineer"</em>.
<br>
Love is about personalities and souls clicking. Hence, we have done our best to break stereotypes that directly affect love. For example, you'll see an artist telling the world that he is not unemployed, a man who loves to cook, a lady who wears red lipstick and isn't 'easy'. <br /><br />

<a href="https://www.facebook.com/trulymadly" style="color:#e6296d; font-weight:bold; text-decoration:none;" target="_blank">We would love to involve you in our project</a><strong>, so if you have broken a stereotype, do write in and we'll reach out to you. This would be a great way for us to get to know you better, understand your point of view and improve your experience on TrulyMadly.</strong><br /><br />

Do write to us at <a href ="mailto:smita.prakash@trulymadly.com" target="_blank" style="color:#0055cc; font-weight:bold;">smita.prakash@trulymadly.com</a> if you are interested to know more.<br /><br />

Truly & a little Madly,<br />
Team TrulyMadly
        </td>
      </tr>
    </table>    </td>
  </tr>
  
</table>
  {include file='./mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
</body>
</html>
