<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Email Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">

<tr> 
<td>Subject</td>
<td>Sent Mails Yesterday</td>
<td>Open Rate Yesterday</td>
<td>Open Rate (%)</td>
<td>Clicks converted to site visits Yesterday</td>
<td>Site Visit Conversion (%)</td>
<td>Sent Mails Today</td>
</tr>



{foreach from=$campaigns item=v}
<tr>
<td>
	{if $v eq 'incomplete_profile_nextDay'}Incomplete Registrations Yesterday (Daily)
	{elseif $v eq 'messages'} Unread Messages (Hourly)
    {elseif $v eq 'DPmale'} Get a new DP for weekly registered Male Users (Every Monday)
    {elseif $v eq 'DPfemale'} Get a new DP for weekly registered Female Users (Every Monday)
    {elseif $v eq 'FoundSomeone'} Deactivated with reason - Found Someone (Daily)
    {elseif $v eq 'check_new_match_nextDay'} Registered Yesterday and not logged in since then (Daily)
    {elseif $v eq 'endorsement_given'}Endorsement Received by a user (Real time)
    {elseif $v eq 'mutual_like_with_common_hashTags'}Mutual like with common hashTags (Real time)
    {elseif $v eq 'mutual_like_without_common_hashTags'}Mutual like without common hashTags (Real time)
    {elseif $v eq 'admin_photoId_approved'}Photo Id approved by Admin (Real time)
    {elseif $v eq 'admin_photoId_notClear'}Photo Id not clear by Admin (Real time)
    {elseif $v eq 'admin_photoId_name_age_mismatch'}Photo Id Name and age mismatch mailer by Admin (Real time)
    {elseif $v eq 'admin_photoId_age_mismatch'}Photo Id age mismatch mailer by Admin (Real time)
    {elseif $v eq 'admin_photoId_name_mismatch'}Photo Id Name mismatch mailer by Admin (Real time)
    {elseif $v eq 'admin_photoId_password_required'}Photo Id Password protected mailer by Admin (Real time)
    {elseif $v eq 'admin_all_photos_rejected'}All photos rejected mailer by Admin (Real time)
    {elseif $v eq 'admin_profile_photo_approved_others_rejected'}Profile photo approved but others rejected mailer by Admin (Real time)
    {elseif $v eq 'weekly_unread_messages'} Weekly Unread Messages if user not logged in (Every Monday)
    {elseif $v eq 'BoostPIScore'} Boost PI score to Low PI users (Every Monday)
    {else} {$v}
	{/if}
</td>

<td>{$sentMails[$v]}</td>
<td>{$openRate[$v]}</td>

<td>
{if isset($openRate[$v]) && isset($sentMails[$v])}
{math equation="(x / y)*100 " x=$openRate[$v] y=$sentMails[$v] format="%.2f"}%
{/if}
</td>
<td>
{$siteVisits[$v]}
</td>

<td>
{if isset($siteVisits[$v]) && isset($openRate[$v])}
{math equation="(x / y)*100 " x=$siteVisits[$v] y=$openRate[$v] format="%.2f"}%
{/if}
</td>


<td>
{$todayMails[$v]}
</td>


{/foreach}
</tr>




</table>
</body>
</html>