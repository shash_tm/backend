<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./../mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; font-family:'Myriad Pro',Arial,Helvetica,sans-serif; color:#333; width:100%;">
<tr>
<td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/sakley1.jpg); background-size: cover;">
<table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
<tr>
<td style=" font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;">          
Hello!<br /><br />
We've been receiving lots of requests for date suggestions, so we whipped up this list for you.<br /><br />
<strong>One of Delhi's biggest foodies has helped us compile this, so we really hope you enjoy it. Let us know which one you pick.</strong>
<br /><br />
<a href="{$blogLink}" target="_blank" style="color:#0055cc; font-weight:bold;">Our favourite date spots</a>
<br /><br />
Truly & a little Madly,<br />
Team TrulyMadly </td>
</tr></table>
</td>
  </tr>
 
</table>
 {include file='./../mailerFooter.tpl'}
 <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" /> 
</body>
</html>
