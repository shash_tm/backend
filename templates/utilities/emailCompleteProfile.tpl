<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="margin:0; padding:0;">
{include file='./mailerHeader.tpl'}
<table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff; color:#333; width:100%;">
<tr>
    <td align="center" style="background-image:url(http://cdn.trulymadly.com/email/images/mailerimg1.jpg); background-size:cover;">
    <table border="0" align="center" cellpadding="20" cellspacing="0" style="margin:30px 0; width:85%;">
      <tr>
        <td style="font-family:'Myriad Pro',Arial,Helvetica,sans-serif; background-image:url(http://cdn.trulymadly.com/email/images/bgtrans.png); font-size:14px; line-height:18px; text-align:left;">
        Dear <strong>{$name}</strong>,<br /><br />
<strong>You are a step away from finding someone special so why wait, go ahead and complete your profile.</strong><br /><br />
<strong style="color:#E7276B; font-size:16px;">{$rnum}</strong> members joined the TrulyMadly community last week to find their soul mates, so do hurry up!<br /><br />

This link will take you to right where you stopped:<br />
<a href ="{$analyticsLinks.analytics_login_link}" target="_blank" style="color:#0055cc;">http://www.trulymadly.com/register.php</a><br />
<br />
Truly & a little Madly,<br />
Team TrulyMadly
        </td>
      </tr>
    </table>    </td>
  </tr>
  
</table>
  {include file='./mailerFooter.tpl'}
  <img src="{$analyticsLinks.analytics_open_rate_link}" width="1" height="1" style="float:left;"/>
  <img src="{$analyticsLinks.analytics_open_rate_system_link}" width="1" height="1" style="float:left;" />
  
</body>
</html>
