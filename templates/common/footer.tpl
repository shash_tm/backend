<!-- Footer -->
<div align="center" class="ftrbg">
<div class="pagefooter" align="center">
    	<div class="footerlink" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
		<ul>
        	<li class="padt8"><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
            <li class="padt8"><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
        	<li class="padt8"><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
			<li class="padt8"><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
			<li class="padt8"><!--© 2014, trulymadly.com,  --><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>	
            <li class="padt8"><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>		
			<li class="padt8"><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
            <li><div class="followus">
    		<p>Follow us :</p>
    		<a href="https://www.facebook.com/trulymadly" class="fb" target="_blank" title="Facebook"></a>
    		<a href="http://blog.trulymadly.com/" class="tmblog" target="_blank" title="Our Blog"></a>
    		<a href="http://twitter.com/thetrulymadly" class="twitter" target="_blank" title="Twitter"></a>
    		<a href="http://instagram.com/thetrulymadly" class="insta" target="_blank" title="Instagram"></a>
            <a href="https://www.youtube.com/user/trulymadlycom" class="youtube" target="_blank" title="Youtube"></a>
    		</div></li>
		</ul>
		<p class="copyr">© 2015, trulymadly.com</p>
	</div>
</div>
</div>
<!-- Footer End-->
