<script>
{literal}
(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
_fbq.push(['addPixelId', '358181751034485']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
{/literal}
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=358181751034485&amp;ev=PixelInitialized" /></noscript>

<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
</script>
<script>
{literal}
window.mobilecheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check; 
}
{/literal}


function setBannerCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function hideBanner(){
	$('#dlapp').hide();
	setBannerCookie('isBannerShown','true',30);
}

function ga_tm(category,action){
	if(mobilecheck()){
		var source = 'mobile_web';
	}else{
		var source = 'web';
	}
	ga('send', 'event', category,action,source);
}

function logout(){
	time = new Date();
	if(typeof trackRegistration === 'function'){
		trackRegistration("logout",'click','');
	}else{
		{literal}
		matchObj1 = {"data":{"time_taken":0, "activity":"logout", "user_id":myId, "event_type":"click" ,"source":"web"}};
		{/literal}
		logEvent(matchObj1);
	}
	setTimeout(function(){
		window.location.href = "{$baseurl}/logout.php";
	},500);
	
}
</script>
<!-- Header -->
<div align="center" class="headbg">
	<div class="pageheader">
    	<!-- Dropdown 1 -->
		<div class="click-nav" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
			<ul class="no-js">
				<li>
					<a class="clicker icon-reorder ddicon"></a>
					<ul>
                    <li><a href="{$baseurl}/aboutus.php" target="_blank">About Us</a></li>
                    <li><a href="{$baseurl}/careers.php" target="_blank">Work With Us</a></li>
                    <li><a href="http://blog.trulymadly.com/" target="_blank">Our Blog</a></li>
                    <li><a href="{$baseurl}/trustnsecurity.php" target="_blank">Trust & Security</a></li>
					<li><a href="{$baseurl}/truecompatibility.php" target="_blank">True Compatibility</a></li>
					<li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					<li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
					</ul>
				</li>
			</ul>
		</div>
        <!-- Dropdown 1 end-->
       <a {if $isRegistration neq 1 }href="{$baseurl}/matches.php"{/if}><img src="{$cdnurl}/images/common/trulymadly_logo.png" width="172" height="35" class="sitelogo"></a>
        <!-- Right Links -->
        {if isset($header)}
        <div class="headright" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
        	<!-- Dropdown 2 -->
			<div class="click-nav1">
			<ul class="no-js1">
				<li>
					<p class="clicker1"><img src="{$header.profile_pic}" align="absmiddle"><span>{$header.name}</span></p>
					<ul>
                    <li><a href="{$baseurl}/profile.php?me=true">My Profile</a></li>
                    <li><a href="{$baseurl}/editprofile.php">Edit Profile</a></li>
                    <li><a href="{$baseurl}/photo.php">Edit Photos</a></li>
						<!--<li><a href="{$baseurl}/mylikes.php">My Likes</a></li>-->
					<li><a href="{$baseurl}/trustbuilder.php">Update Trust Score</a></li>
                    <li><a href="{$baseurl}/personality.php">Take Personality Quizzes</a></li>
                    <li><a href="{$baseurl}/editpreference.php">Edit Preferences</a></li>
                     {if isset($header.credits) && $header.credits > 0}<li><a href="{$baseurl}/credits.php">My Credits ({$header.credits})</a></li>{/if}
                    <li><a href="{$baseurl}/contact.php">Feedback</a></li>
                    <li><a onclick="logout()">Log out</a></li>    
                       
                    </ul>
				</li>
			</ul>
			</div>
        	<!-- Dropdown 2 end-->
        	<div class="headlinks">
            <a href="{$baseurl}/matches.php" class="homepage">Matches</a>
    		<a href="{$baseurl}/likes.php" class="matchpage" id="mutualLike">Mutual Likes{if $header.mutual_like_count > 0}<p>{$header.mutual_like_count}</p>{/if}</a>
    		<a href="{$baseurl}/msg/messages.php" class="msgpage">Messages{if $header.message_count > 0}<p>{$header.message_count}</p>{/if}</a>
            </div>
           
        </div>
        {/if}
        <!-- Right Links end -->
        <p class="regusrdet" {if $isRegistration eq 1 }{else}style="display:none;"{/if}><img {if isset($header.profile_pic)}src="{$header.profile_pic}"{else}src="{$cdnurl}/images/common/dummy1.jpg"{/if} align="absmiddle"><span>{if $inRegister eq 1}Welcome{/if} {$header.name}</span></p>
        
  	</div>
</div>
<!-- Header End -->



<!-- Header responsive -->
<div class="respheader">
	<!-- Dropdown 2 -->
<div class="downlapp" id="dlapp" style="display:none;">
<ul>
<li class="fl" onClick="hideBanner()"><span>&times;</span></li>
<li class="fl"><strong>TrulyMadly for Android</strong><br /> Available on Google Play</li>
<li class="fr"><a onClick="hideBanner()" href="https://play.google.com/store/apps/details?id=com.trulymadly.android.app&referrer=utm_source%3Dtrulymadly_mobile_banner" target="_blank"><img src="{$cdnurl}/images/common/dlappbtnicon.png" height="26" align="absmiddle" />&nbsp;install</a></li>
</ul>
</div>
			<div class="click-nav2" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
			<ul class="no-js2">
				<li>
					<a class="clicker2 icon-reorder"></a>
					<ul style="display:none;">
						 <!--<li><a href="{$baseurl}/mylikes.php" class="ddlike">My Likes</a></li>-->
                        <li><a href="{$baseurl}/profile.php?me=true" class="ddmpro">My Profile</a></li>
                        <li><a href="{$baseurl}/editprofile.php" class="ddepro">Edit Profile</a></li>
						<li><a href="{$baseurl}/photo.php" class="ddphoto">Edit Photos</a></li>
                        <li><a href="{$baseurl}/trustbuilder.php" class="ddtrustb">Update Trust Score</a></li>
                        <li><a href="{$baseurl}/personality.php" class="ddquiz">Take Personality Quizzes</a></li>
                        <li><a href="{$baseurl}/editpreference.php" class="ddepref">Edit Preferences</a></li>
                        {if isset($header.credits) && $header.credits > 0}<li><a href="{$baseurl}/credits.php" class="ddcredit">My Credits ({$header.credits})</a></li>{/if}
                        <!--<li><a href="#" class="ddepro">Edit Profile</a></li> -->
                        <!--<li><a href="#" class="ddinvfnd">Invite Friends</a></li> -->
						 <li><a href="{$baseurl}/contact.php" class="ddfeed">Feedback</a></li>
                        <!--<li><a href="{$baseurl}/logout.php" class="ddlogout">Logout</a></li>-->
                        <li><a onclick="logout()" class="ddlogout">Logout</a></li>
						<!--<li><a onclick="signout()" class="ddsett">Settings</a></li>-->
					</ul>
				</li>
			</ul>
			</div>
            <!-- Dropdown 2 end-->
<div class="headlinks" {if $isRegistration eq 1 }style="display:none;"{else}{/if}>
<a href="{$baseurl}/matches.php" class="homepage">Matches</a>
<a href="{$baseurl}/likes.php" class="matchpage" id="mutualLike">Mutual Likes{if $header.mutual_like_count > 0}<p>{$header.mutual_like_count}</p>{/if}</a>
<a href="{$baseurl}/msg/messages.php" class="msgpage">Messages{if $header.message_count > 0}<p>{$header.message_count}</p>{/if}</a>
</div>

 <p class="regusrdet" {if $isRegistration eq 1 }{else}style="display:none;"{/if}><img {if isset($header.profile_pic)}src="{$header.profile_pic}"{else}src="{$cdnurl}/images/common/dummy1.jpg"{/if} align="absmiddle"><span>{if $inRegister eq 1}Welcome{/if} {$header.name}</span></p>
        
</div>
<!-- Header End -->


		<script src="{$cdnurl}/js/common/jquery.min.js"></script>
			<script>
				{if $isRegistration eq 1 }
					var isRegistrationCompleted = false;
				{else}	
					var isRegistrationCompleted = true;
				{/if}
				
				function isCookieSet(name){
					var cookies = document.cookie.split(";");
    				for (var i in cookies)
    				{
        				if (cookies[i].indexOf(name + "=") > -1)
            				return true;
   			 		}
    					return false;
				}
				function isAndroidDevice(){
					var ua = navigator.userAgent.toLowerCase();
					var isAndroid = ua.indexOf("android") > -1; 
					return isAndroid;
				}
				
				$(document).ready(function(){
					if(isRegistrationCompleted && isCookieSet("isBannerShown")){
						$('#dlapp').hide();
					}
					if(isRegistrationCompleted && !isCookieSet("isBannerShown") && isAndroidDevice()){
						$('#dlapp').show();
					}
				});
				
		$(function() {
			// Clickable Dropdown
			$('.click-nav > ul').toggleClass('no-js js');
			$('.click-nav .js ul').hide();
			$('.click-nav .js').click(function(e) {
				$('.click-nav .js ul').slideToggle(200);
				$('.clicker').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav .js ul').is(':visible')) {
					$('.click-nav .js ul', this).slideUp();
					$('.clicker').removeClass('active');
				}
			});
		});
		
		$(function() {
			// Clickable Dropdown
			$('.click-nav1 > ul').toggleClass('no-js1 js');
			$('.click-nav1 .js ul').hide();
			$('.click-nav1 .js').click(function(e) {
				$('.click-nav1 .js ul').slideToggle(200);
				$('.clicker1').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav1 .js ul').is(':visible')) {
					$('.click-nav1 .js ul', this).slideUp();
					$('.clicker1').removeClass('active');
				}
			});
		});
		
		$(function() {
			// Clickable Dropdown
			$('.click-nav2 > ul').toggleClass('no-js2 js');
			$('.click-nav2 .js ul').hide();
			$('.click-nav2 .js').click(function(e) {
				$('.click-nav2 .js ul').slideToggle(200);
				$('.clicker2').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav2 .js ul').is(':visible')) {
					$('.click-nav2 .js ul', this).slideUp();
					$('.clicker2').removeClass('active');
				}
			});
		});
		</script>



