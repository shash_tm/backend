<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Spark payment Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Log Dump Report(SQS)</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th width="200"> Server </th>
        <th width="200"> Count </th>
        <th width="200"> Expected Count </th>
        <th width="200"> Is_Problem </th>

    </tr>

    {foreach from=$all_data key=k  item=v}
        {if $v|is_array}
            <tr>
                <td align="center"> {$v.hostname} </td>
                <td align="center"> {$v.count} </td>
                <td align="center"> {$v.expected_count} </td>
                <td align="center"> {$v.is_problem} </td>
            </tr>
        {else}
            <td></td>
            <th colspan="5" align="center" ></th>
        {/if}
    {/foreach}

</table>


</body>
</html>