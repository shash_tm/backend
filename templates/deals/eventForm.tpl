<!DOCTYPE html>
<html lang="en">
<head>
	<title>Datespot View</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet"
		href="{$baseurl}/css/common/bootstrap.min.css">
	<link rel="stylesheet"
		href="{$baseurl}/css/common/bootstrapdatetimepicker.min.css">
	<script
		src="{$baseurl}/js/common/jquery.min.js"></script>
	<script
		src="{$baseurl}/js/common/bootstrap.min.js"></script>
	<script src="{$baseurl}/js/lib/jquery-1.8.2.min.js"></script>
	<script src="{$baseurl}/js/lib/jquery-ui.js"></script>
	<script src="{$baseurl}/js/common/moment.min.js"></script>
	<script src="{$baseurl}/js/common/collapse.js"></script>
	<script src="{$baseurl}/js/common/transition.js"></script>
	<script src="{$baseurl}/js/common/bootstrapdatetimepicker.min.js"></script>
	<style>
		.glyphicon{
			    line-height: inherit;
		}
		
		.btn-group-hashtag{
		    position: relative;
		    display: inline-block;
		    vertical-align: middle;
		}

		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		#map {
			height: 100%;
		}
		.controls {
			margin-top: 10px;
			border: 1px solid transparent;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			height: 32px;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}

		#pac-input {
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			width: 300px;
		}

		#pac-input:focus {
			border-color: #4d90fe;
		}

		.pac-container {
			font-family: Roboto;
		}

		#type-selector {
			color: #fff;
			background-color: #4d90fe;
			padding: 5px 11px 0px 11px;
		}

		#type-selector label {
			font-family: Roboto;
			font-size: 13px;
			font-weight: 300;
		}
		#target {
			width: 345px;
		}

	</style>
</head>
<body>
	{include file='../deals/header.tpl'}
	<div class="container-fluid check-me">
		{if $deal.datespot_id neq null}
		<div class="page-header">
		  <h2>{$deal.name}</h2>
		</div>
		{/if}
		<form action="{$baseurl}/dealAdmin/dealFormAction.php" method="POST" id="datespot-form">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="col-md-4 col-lg-4 col-sm-4">
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Name</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.name}" name="name"/>
						</div>	
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Pretty Date</span>
							<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.date_pretty_text}" name="date_pretty_text"/>
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Event Date</span>
							<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.event_date}" name="event_date" id="event-date"/>
						</div>
					</div>

					<!-- <div class="form-group">
						 <div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">DateSpot ID</span>

						</div>
					</div> -->
					<input type="hidden"  value="{$deal.datespot_id}" name="datespot_id">



				</div>

				<div class="col-md-4 col-lg-4 col-sm-4">

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Rank</span>
							<input type="text" class="form-control" placeholder="higher rank shows this deal higher up in datespot list." aria-describedby="basic-addon1" value="{$deal.rank}" name="rank" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Status</span>
							<input type="text" class="form-control" placeholder="Test" aria-describedby="basic-addon1" value="{$deal.status}" name="status" id="deal-status" disabled>
							<!-- <option value="test">Test</option>
              <option value="active">Active</option>
              <option value="inactive">Inactive</option>
            </select> -->
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Category</span>
							<select class="form-control" placeholder="" aria-describedby="basic-addon1" name="category_id" id="category-select">
							</select>
						</div>

					</div>

				</div>
				
				<div class="col-md-4 col-lg-4 col-sm-4">

					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">State</span>
						  <select class="form-control" placeholder="" aria-describedby="basic-addon1" name="state_id" id="state-select">
						  	
						  </select>
						</div>
					</div>
					
					<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">City</span>
						  <select class="form-control" placeholder="" aria-describedby="basic-addon1" name="city_id" id="city-select">
						  </select>
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<div class="btn-group" role="group" id="add-category-button-group">
								<button type="button" class="btn btn-success" id="add-category-button" data-toggle="modal" data-target=".add_category_box" onclick="showCategoryModal()">Add New Category &nbsp;&nbsp;
									<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
								</button>
							</div>
						</div>
					</div>



					
					<!-- <div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Per Couple Text</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.is_per_couple}" name="is_per_couple" />
						</div>
					</div> -->
					
				</div>
		</div>
		<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="panel panel-default ">
					<div class="panel-heading">
						<h3 class="panel-title">Location</h3>
					</div>
					<div class="panel-body">
						<div id="location-body">
							<div class="col-md-3 col-lg-3 col-sm-3">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Address</span>
										<input type="text" class="form-control field_name_address" placeholder="" aria-describedby="basic-addon1" value="{$deal.address}" />
									</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 col-sm-3">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Location</span>
										<input type="text" class="form-control field_name_location" placeholder="" aria-describedby="basic-addon1" value="{$deal.location}" />
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Zone</span>
										<select class="form-control field_name_zone_id" placeholder="" aria-describedby="basic-addon1" id="zone-select">
											<input type="hidden" data-zone-value="{$deal.zone_id}" class="zone-val"/>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Phone</span>
										<input type="text" class="form-control field_name_phone_number" placeholder="" aria-describedby="basic-addon1" value="{$deal.phone_number}"  />
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Geo</span>
										<input type="text" class="form-control field_name_geo_location" placeholder="" aria-describedby="basic-addon1" value="{$deal.geo_location}"  id="geo-loc"/>
									</div>
								</div>
							</div>
							<div class="col-md-1 col-lg-1 col-sm-1 remove-button-div" style="display: none;">
								<button type="button" class="btn btn-primary location-remove">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</div>
						</div>
						<div id="new-location">
							{foreach from=$deal.locations item =location}
							<div class="new-location-obj">
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Address</span>
											<input type="text" class="form-control field_name_address" placeholder="" aria-describedby="basic-addon1" value="{$location.address}"  />
										</div>
									</div>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-3">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Location</span>
											<input type="text" class="form-control field_name_location" placeholder="" aria-describedby="basic-addon1" value="{$location.location}"  />
										</div>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Zone</span>
											<select class="form-control field_name_zone_id" placeholder="" aria-describedby="basic-addon1" >
											</select>
											<input type="hidden" data-zone-value="{$location.zone_id}" class="zone-val"/>
										</div>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Phone</span>
											<input type="text" class="form-control field_name_phone_number" placeholder="" aria-describedby="basic-addon1" value="{$location.phone_number}"  />
										</div>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Geo</span>
											<input type="text" class="form-control field_name_geo" placeholder="" aria-describedby="basic-addon1" value="{$location.geo_location}"  />
										</div>
									</div>
								</div>
								<div class="col-md-1 col-lg-1 col-sm-1">
									<button type="button" class="btn btn-primary location-remove">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</div>
							</div>
							{/foreach}
						</div>
						<div class="pull-right">
							<div class="btn-group" role="group" id="add-zone-button-group">
								<button type="button" class="btn btn-success" id="add-zone-button" data-toggle="modal" data-target=".add_zone_box" onclick="showZoneModal()">Add New Zone &nbsp;&nbsp;
									<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
								</button>
							</div>
							&nbsp;&nbsp;
							<div class="btn-group" role="group" id="add-location-button-group">
								<button type="button" class="btn btn-success" data-toggle="modal" data-target=".map_box" id="show-map">View Map &nbsp;&nbsp;
									<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
								</button>
							</div>

						{*	<div class="btn-group" role="group" id="add-location-button-group">
								<button type="button" class="btn btn-success" id="add-location-button">Add Location &nbsp;&nbsp;
									<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
								</button>
							</div>*}
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-12 col-sm-12">

				<div class="col-md-6 col-lg-6 col-sm-6">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Details</span>
							<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.offer}" name="offer" />
						</div>
					</div>
					<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Ticketing Url</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.event_ticket_url}" name="event_ticket_url" id="event-ticket-url" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Facebook Page</span>
							<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.fb_page_url}" name="fb_page_url" id="fb-page-url" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">TM Status</span>
							<input type="text" class="form-control" placeholder="Eg: Hurry! Few seats left!" aria-describedby="basic-addon1" value="{$deal.tm_status}" name="tm_status" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Buy Ticket Text</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.event_ticket_text}" name="event_ticket_text" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Ticket Price</span>
							<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.pricing}" name="pricing" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Spark Display Name</span>
							<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.spark_display_name}" name="spark_display_name" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox"  id="is_trulymadly_check">
								</span>
								<input type="text" class="form-control" aria-label="TrulyMadly Event?" value="Check if it is TrulyMadly Event !" disabled>
						</div>
					</div>

					<div class="form-group">
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Deal Images</h3>
						  	</div>
						  	<div class="panel-body" id="deal-image-panel-body">
							{foreach from=$deal.images_json item =image}
							  <div class="col-sm-6 col-md-4 image-box">
							    <div class="thumbnail">
							      <img src="{$image}" alt="" class="deal-image">
							      <div class="caption">
							        <p><a href="#" class="btn btn-danger remove-image-button" role="button">Remove</a></p>
							      </div>
							    </div>
							  </div>
							{/foreach}
								<div class="col-md-12 col-lg-12 col-sm-12">
									<div class="btn-group pull-right" role="group" id="add-deal-img-button-group">
										<button type="button" class="btn btn-success" id="add-deal-img-button">Add More &nbsp;&nbsp;
										<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



				<div class="col-md-6 col-lg-6 col-sm-6">
					
					
					<!-- <div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">HashTags</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" >
						  <span class="label label-default">Default</span>
						  </input>
						</div>
					</div> -->

					
					<div class="form-group">
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">List Image</h3>
						  	</div>
						  	<div class="panel-body">
							
							  <div class="col-sm-12 col-md-12">
							    <div class="thumbnail">
							      <img src="{$deal.list_view_image}" alt="" id="deal-image">
							      <!-- <div class="caption">
							        <p><a href="#" class="btn btn-danger " role="button">Remove</a></p>
							      </div> -->
							    </div>
							    <div class="form-group">
									 <div class="input-group">
									  <span class="input-group-addon" id="basic-addon1">New Image Url</span>
									  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" id="deal-image-new">
									</div> 
								</div>
							  </div>
							</div>
						</div>
					</div>
					
					
					<div class="form-group ">
						
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Terms And Conditions</h3>
						  	</div>
						  	<div class="panel-body">
						  		{foreach from=$deal.terms_and_conditions item =terms}
						  		<div class="form-group text-input-fld">
									<div class="input-group">
										<input type="text" class="form-control tnc_text" placeholder="T&C Point" value="{$terms}">
											<span class="input-group-btn">
												<button class="btn btn-default remove-text-input" type="button">
													<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												</button>
											</span>
										</input>
									</div>
								</div>
								{/foreach}
								
								<div class="btn-group pull-right" role="group" id="add-tnc-button-group">
									<button type="button" class="btn btn-success" id="add-tnc-button">Add More &nbsp;&nbsp;
									<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
									</button>
								</div>
							
							</div>
					</div>
					
					{if $deal.datespot_id eq null}
						<input type="hidden" name="action" value="add_new_DS" />
					{/if}
					
					<input type="hidden"  id="select-state-id" value="{$deal.state_id}" />
					<input type="hidden"  id="select-city-id"  value="{$deal.city_id}" />
					<input type="hidden"  id="select-zone-id"  value="{$deal.zone_id}" />
					<input type="hidden"  id="select-status" value="{$deal.status}" />
					<input type="hidden"  id="read-only-val"  value="{$read_only}" />
					<input type="hidden"  id="base-url"  value="{$baseurl}" />
					<input type="hidden"  id="ds-type"  name="type" value="event" />
					<input type="hidden"  id="select-category-id"  value="{$deal.category_id}" />

					<input type="hidden"  name="hash_id"  value="{$deal.hash_id}" />

					<input type="hidden" name="images_json" id="images-json"/>
					<input type="hidden" name="list_view_image" id="list-view-image"/>
					<input type="hidden" name="terms_and_conditions" id="t-n-c"/>
					<input type="hidden" name="is_trulymadly" value="{$deal.is_trulymadly}" id="is_tm_field"/>

					
					<!--  <div class="input-group">
					  <span class="input-group-addon" id="basic-addon1">Terms And Conditions</span>
					  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.terms_and_conditions}" />
					</div> -->
				</div>
				{if $read_only neq true}
				<div class="btn-group col-md-12 col-lg-12 col-sm-12" role="group" >
					<button type="button" class="btn btn-primary col-md-12 col-lg-12 col-sm-12" id="save-form-action">Save</button>
					<div class="progress">
					  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					    <span class="sr-only">Saving</span>
					  </div>
					</div>
				</div>	
				{/if}
			</div>
			</div>


	</form>

		<div class="modal fade add_zone_box" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" >
			<div class="modal-dialog modal-sm">
				<div class="modal-content" id="zone-bx">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="modal-title" id="mySmallModalLabel">Add Zone</h3>
					</div>
					<div class="modal-body">

						<!-- Single button -->
						<form action="{$baseurl}/dealAdmin/zoneData.php" method="POST" id="zone-form">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Zone Name</span>
									<input type="text" class="form-control" placeholder="Add new zone name." aria-describedby="basic-addon1" id="zone-new">
								</div>
							</div>
							<div class="btn-group" id='yes-button'>
								<button type="button" class="btn btn-default" onclick="addNewZone()">
									Save
								</button>
							</div>
							<div class="btn-group pull-right" id="cancel-button">
								<button type="button" class="btn btn-default"  data-dismiss="modal" aria-hidden="true">
									Cancel
								</button>
							</div>
							<div class="progress" id="show-wait" style="display:none;">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									<span class="sr-only">Saving</span>
								</div>
							</div>
							<div class="alert alert-success" role="alert" id="zone-success" style="display: none;">Zone Saved Successfully !</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade add_category_box" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" >
			<div class="modal-dialog modal-md">
				<div class="modal-content" id="category-bx">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="modal-title" id="mySmallModalLabel">Add Category</h3>
					</div>
					<div class="modal-body">

						<!-- Single button -->
						<form action="{$baseurl}/dealAdmin/dealFormAction.php" method="POST" id="category-form">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon-name">Category Name</span>
									<input type="text" class="form-control" placeholder="Add new Category name." aria-describedby="basic-addon-name" id="category-new">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon-background">Category Background</span>
									<input type="text" class="form-control" placeholder="Add background image" aria-describedby="basic-addon-background" id="category-new-background">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon-icon">Category Icon</span>
									<input type="text" class="form-control" placeholder="Add category icon" aria-describedby="basic-addon-icon" id="category-new-image">
								</div>
							</div>

							<div class="btn-group" id='category-yes-button'>
								<button type="button" class="btn btn-default" onclick="addNewCategory()">
									Save
								</button>
							</div>
							<div class="btn-group pull-right" id="category-cancel-button">
								<button type="button" class="btn btn-default"  data-dismiss="modal" aria-hidden="true">
									Cancel
								</button>
							</div>
							<div class="progress" id="category-show-wait" style="display:none;">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									<span class="sr-only">Saving</span>
								</div>
							</div>
							<div class="alert alert-success" role="alert" id="category-success" style="display: none;">Category Saved Successfully !</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade map_box" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" >
			<div class="modal-dialog modal-md">
				<div class="modal-content" id="zone-bx">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="modal-title" id="mySmallModalLabel">Select Coordinates</h3>
					</div>
					<div class="modal-body">
						<div id="main-map" style="width:100%; height:100%;">
							<input id="pac-input" class="controls" type="text" placeholder="Search Box">
							<div id="map" style="height: 400px"></div>
						</div>
						<!-- Single button -->
					</div>
				</div>
			</div>
		</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
				$('#event-date').datetimepicker({
					format: 'YYYY-MM-D HH:mm:ss'
				});
		});
	</script>
	<script src="{$baseurl}/js/dealAdmin/eventForm.js?ver=1"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvGCQPLFUyFB2TPYSiPT_cG8K50WOjRlk&libraries=places&callback=initAutocomplete"
					async defer></script>
</body>
</html>
