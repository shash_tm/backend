{if !isset($smarty.session.admin_id)}
  <input type="hidden" value="login_req" id="login-required" />
{/if}
<input type="hidden" id="admin-id" value="{$smarty.session.admin_id}" />
<div class="display_wait_overlay">
  <div class="dw_overlay box_sized">
    <div class="dw_data_container">
      <span class="dw_loader"><!----></span>
      <!--<div class="dw_text">Please Wait...</div>-->
    </div>
  </div>
</div>
<input type="hidden" id="app_base_url" value="{$baseurl}" />
<input type="hidden" id="login-url" value="{$baseurl}/dealAdmin/adminLogin.php" />
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="{$baseurl}/images/common/trulymadly_logo.png" width="172" height="35" class="sitelogo"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Datespots <span class="caret"></span></a>
          <ul class="dropdown-menu">
            {if $smarty.session.privileges neq null && in_array('deal_view', $smarty.session.privileges)  || $smarty.session.super_admin eq 'yes'}
              <li><a href="{$baseurl}/dealAdmin/dealList.php" >List</a></li>
            {/if}
            {if $smarty.session.privileges neq null && (in_array('deal_view', $smarty.session.privileges) && in_array('deal_edit', $smarty.session.privileges)) || $smarty.session.super_admin eq 'yes'}
              <li> <a href="{$baseurl}/dealAdmin/dealView.php?action=new"> Create New</a></li>
            {/if}
          </ul>
        </li>

        <li class="dropdown active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Events <span class="caret"></span></a>
          <ul class="dropdown-menu">
            {if $smarty.session.privileges neq null && in_array('deal_view', $smarty.session.privileges)  || $smarty.session.super_admin eq 'yes'}
              <li><a href="{$baseurl}/dealAdmin/eventList.php" >List</a></li>
            {/if}
            {if $smarty.session.privileges neq null && (in_array('deal_view', $smarty.session.privileges) && in_array('deal_edit', $smarty.session.privileges)) || $smarty.session.super_admin eq 'yes'}
            <li> <a href="{$baseurl}/dealAdmin/eventView.php?action=new"> Create New</a></li>
            {/if}
          </ul>
        </li>

        <li class="dropdown active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Quizes <span class="caret"></span></a>
          <ul class="dropdown-menu">
            {if $smarty.session.privileges neq null && in_array('quiz_view', $smarty.session.privileges)  || $smarty.session.super_admin eq 'yes'}
              <li><a href="{$baseurl}/quizAdmin/quizList.php" >List</a></li>
            {/if}
            {if $smarty.session.privileges neq null && (in_array('quiz_view', $smarty.session.privileges) && in_array('quiz_edit', $smarty.session.privileges)) || $smarty.session.super_admin eq 'yes'}
              <li> <a href="{$baseurl}/quizAdmin/quizView.php?action=new"> Create New</a></li>
            {/if}
          </ul>
        </li>

        <li class="dropdown active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reporting <span class="caret"></span></a>
          {if $smarty.session.privileges neq null && in_array('reporting', $smarty.session.privileges) || $smarty.session.super_admin eq 'yes'}
          <ul class="dropdown-menu">
            <li><a href="{$baseurl}/reporting/dashboard.php?action=new" >Compose Query</a></li>
            <li> <a href="{$baseurl}/reporting/queryList.php"> Saved Queries</a></li>
          </ul>
          {/if}
        </li>

        <li class="dropdown active ">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown " role="button" aria-haspopup="true" aria-expanded="false">Mailers <span class="caret"></span></a>
          {if $smarty.session.privileges neq null && in_array('mailer', $smarty.session.privileges) || $smarty.session.super_admin eq 'yes'}
            <ul class="dropdown-menu">
              <li><a href="{$baseurl}/mailers/mailerWizard.php" >Create campaign</a></li>
              <li> <a href="{$baseurl}/mailers/mailerDashboard.php"> See all Campaigns</a></li>
            </ul>
          {/if}
        </li>

        <li class="dropdown active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Videos <span class="caret"></span></a>
          {if $smarty.session.privileges neq null && in_array('videos', $smarty.session.privileges) || $smarty.session.super_admin eq 'yes'}
            <ul class="dropdown-menu">
              <li>
                <a href="{$baseurl}/videos/adminVideos.php?action=no_action1">New Videos</a>
              </li>
              <li>
                <a href="{$baseurl}/videos/adminVideos.php?action=no_action&page_no=0" >Action Already Taken</a>
              </li>
            </ul>
          {/if}
        </li>



        {if $smarty.session.privileges neq null && in_array('manage_admins', $smarty.session.privileges)  && $smarty.session.super_admin eq 'yes'}
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{$baseurl}/reporting/adminList.php">Manage Admins</a></li>
          </ul>
        {/if}

        <li><a role="button" aria-expanded="false" href="{$baseurl}/admin/adminpanel.php">Admin</a></li>
      </ul>
      {if !isset($smarty.session.admin_id)}
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#" data-toggle="modal" data-target=".login_modal">Login</a></li>
        </ul>
      {/if}
      {if isset($smarty.session.admin_id)}
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{$baseurl}/dealAdmin/adminLogin.php?action=logout">Logout</a></li>
        </ul>
      {/if}
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="modal fade upload_pictures_modal" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content" id="upload-bx">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="mySmallModalLabel">Upload Deal Data</h3>
            </div>
            <div class="modal-body">
             
                    <form class="bs-example" id="upload-form" enctype="multipart/form-data" action="{$baseurl}/dealAdmin/uploadDeals.php" method="POST">
                        <div class="form-group">
                            <label class="control-label">Upload a Valid Json File </label>
                            <input type="file" id="fileName" name="fileName">
                        </div>
                        <!--<div class="form-group">
                        	<h4>OR</hr>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="inputLarge">Upload</label>
                            <textarea class="form-control" maxlength="300" rows="5" id="textAreaTitle" name="title"></textarea>
                        </div> -->
                        <div class="form-group">
                            <button id="upload-image" class="btn btn-primary btn-lg" value="Upload">Upload</button>
                        </div>
                    </form>
               
            </div>
        </div>
    </div>
</div>

<div class="modal fade login_modal" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="login_bx_main">
  <div class="modal-dialog modal-md">
    <!-- Login start -->
    <div class="modal-content " id="login_bx">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" id="mySmallModalLabel-Login">Login</h3>
      </div>
      <div class="modal-body" align="center">
        <div class="divider"> &nbsp;</div>
        {*
        <div class="form-group text-left" >
          <label class="control-label" for="inputLarge">Email</label>
          <input type="text" class="form-control input-lg" id="login-email">
        </div>
        <div class="form-group text-left">
          <label class="control-label" for="inputLarge">Password</label>
          <input type="password" class="form-control input-lg" id="login-password">
        </div>
        *}{*<div class="text-right form-group"><a href="#" class="forgot_link">Forgot Password </a></div>*}{*
        <button class="btn btn-info form-control" style="margin-bottom: 10px;" id="pass-login-btn">Login</button>
        *}
        {*<div class="g-signin2" data-onsuccess="onSignIn"></div>*}
        <div class="alert alert-info funMesssage" role="alert" style="display: none;">You have  wandered into a dangerous territory. Better sign in! </div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >I solemnly swear that I am up to no good</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >TM Secrets: Someone clicked 217 selfies on a 3 day trip to McLeodganj.</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >We don't like Ti**er !</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >Dev team is pretty kick ass.</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >TM quiz: Who sweats the most here?</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >Hey Stranger !!! Reveal your identity and  We'll talk</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >TM suggestion: A beer fridge would be a nice to have in summers.</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >Pro Tip: Uninstall that Tinder from your phone to stay logged in.</div>
        <div class="alert alert-info funMesssage" role="alert" style="display: none;" >TM quiz: On a scale of 1 to Tarun, How proactive are you ?</div>

        <hr>

        <div class="alert alert-danger" role="alert" id="wrong-email" style="display: none">You can sign up only with a trulymadly email address.</div>
        <button class="btn btn-info form-control" style="margin-bottom: 10px;"  id="googleSigninButton">Sign in with Google</button>

        <div class="progress" id="show-login-wait" style="display:none;">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            <span class="sr-only">Saving</span>
          </div>
        </div>
      </div>
    <!--/ Login end -->
    </div>
  </div>
</div>

<div class="modal fade alert_box" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" id="alert-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" id="upload-bx">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" id="modal-title">Yo</h3>
      </div>
      <div class="modal-body">
        <div class="alert" role="alert" id="alert-text"> Oops</div>
        <button type="button" class="btn btn-default form-control"  data-dismiss="modal" aria-hidden="true">Okay</button>
      </div>
    </div>
  </div>
</div>