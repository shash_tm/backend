<!DOCTYPE html>
<html lang="en">
<head>
<title>Deals List</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<h6><a href="{$baseurl}/dealAdmin/dealList.php">Back to deal List </a></h6>
		{if $deal neq null} 
		<h2>{$deal.friendly_name} @ {$deal.name}</h2>
		<table class="table">
			<thead>
				<tr class="info">
					<th>Field</th> 
					<td>Value</td>
				</tr>
			</thead>
			<tbody>
				<tr class="success">
					<th>DateSpot ID</th>
					<td>{$deal.datespot_id}</td>
				</tr>
				<tr class="success">
					<th>Friendly Name</th>
					<td>{$deal.friendly_name}</td>
				</tr>
				<tr class="success">
					<th>Name</th>
					<td>{$deal.name}</td>
				</tr>
				<tr class="success">
					<th>Status</th>
					<td>{$deal.status}</td>
				</tr>
				<tr class="success">
					<th>City</th>
					<td>{$deal.city}</td>
				</tr>
				<tr class="success">
					<th>State</th>
					<td>{$deal.state}</td>
				</tr>
				<tr class="success">
					<th>Deal Type</th>
					<td>{$deal.deal_type}</td>
				</tr>
				<tr class="success">
					<th>Amount Type</th>
					<td>{$deal.amount_type}</td>
				</tr>
				<tr class="success">
					<th>Amount Value</th>
					<td>{$deal.amount_value}</td>
				</tr>
				<tr class="success">
					<th>Pricing</th>
					<td>{$deal.pricing}</td>
				</tr>
				<tr class="success">
					<th>Inclusive Text</th>
					<td>{$deal.is_inclusive}</td>
				</tr>
				<tr class="success">
					<th>Per Couple Text</th>
					<td>{$deal.is_per_couple}</td>
				</tr>
				<tr class="success">
					<th>Images</th>
					<td>{foreach from=$deal.images_json item =image}
					<img src={$image} class="img-thumbnail" alt="Cinque Terre" width="100" height="100">
					{/foreach}
					</td>
					
					
				</tr>
				<tr class="success">
					<th>Main image</th>
					<td><img src={$deal.list_view_image} class="img-thumbnail" alt="Cinque Terre" width="100" height="100"> </td>
				</tr>
				<tr class="success">
					<th>Menu Images</th>
					<td>{foreach from=$deal.menu_images_json item =image}
					<img src={$image} class="img-thumbnail" alt="Cinque Terre" width="100" height="100">
					{/foreach}
					</td>
				</tr>
				<tr class="success">
					<th>Offer</th>
					<td>{$deal.offer}</td>
				</tr>
				<tr class="success">
					<th>HashTags</th>
					<td>{$deal.hashtag_json}</td>
				</tr>
				<tr class="success">
					<th>Recommendation</th>
					<td>{$deal.recommendation}</td>
				</tr>
				<tr class="success">
					<th>Phone Number</th>
					<td>{$deal.phone_number}</td>
				</tr>
				<tr class="success">
					<th>Address</th>
					<td>{$deal.address}</td>
				</tr>
				<tr class="success">
					<th>Terms And Conditions</th>
					<td>{foreach from=$deal.terms_and_conditions item =tc} 
					    <p> {$tc} </p>
						{/foreach}
					</td> 
				</tr>
				

			</tbody>
		</table>
		{/if}
	</div>

</body>
</html>
