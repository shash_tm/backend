<!DOCTYPE html>
<html lang="en">
<head>
	<title>Datespot View</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet"
		href="{$baseurl}/css/common/bootstrap.min.css">
	<script
		src="{$baseurl}/js/common/jquery.min.js"></script>
	<script
		src="{$baseurl}/js/common/bootstrap.min.js"></script>
	<script src="{$baseurl}/js/lib/jquery-1.8.2.min.js"></script>
	<script src="{$baseurl}/js/lib/jquery-ui.js"></script>
	<style>
		.glyphicon{
			    line-height: inherit;
		}
		
		.btn-group-hashtag{
		    position: relative;
		    display: inline-block;
		    vertical-align: middle;
		}
		#map {
			height: 100%;
		}
	</style>
</head>
<body>
	{include file='../deals/header.tpl'}
	<div class="container-fluid check-me">
		{if $deal.datespot_id neq null}
		<div class="page-header">
		  <h2>{$deal.friendly_name} at {$deal.name}</h2>
		</div>
		{/if}
		<form action="{$baseurl}/dealAdmin/dealFormAction.php" method="POST" id="datespot-form">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="col-md-4 col-lg-4 col-sm-4">
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Name</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.name}" name="name"/>
						</div>	
					</div>
					<!-- <div class="form-group">
						 <div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">DateSpot ID</span>
						  
						</div> 
					</div> -->
					<input type="hidden"  value="{$deal.datespot_id}" name="datespot_id">
					
					<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Deal Type</span>
						  <select type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" name="deal_type" id="deal-type" >
						  	<option value="on_the_house">On The House</option>
						  	<option value="hand_picked_menu">Hand Picked Menu</option>
						  	<option value="special_discount">Special Discount</option>
						  </select>
						</div>
					</div>
					
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Pricing Symbol</span>
						  <!-- <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.pricing}" name="pricing" /> -->
						  <select type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" name="pricing" id="pricing-symbol" >
						  	<option value="1">1</option>
						  	<option value="2">2</option>
						  	<option value="3">3</option>
						  	<option value="4">4</option>
						  </select>	
						</div>
					</div>

				</div>
				
				<div class="col-md-4 col-lg-4 col-sm-4">
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Friendly Name</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.friendly_name}" name="friendly_name" />
						</div>
					</div>
					
					
					<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Amount Type</span>
						  <select type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" name="amount_type" id="amount-type">
						  	<option value="price">Price</option>
						  	<option value="percentage">Percentage</option>
						  </select>
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Rank</span>
							<input type="text" class="form-control" placeholder="higher rank shows this deal higher up in datespot list." aria-describedby="basic-addon1" value="{$deal.rank}" name="rank" />
						</div>
					</div>


					
					<!-- <div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Amount Value</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.amount_value}" name="amount_value" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Inclusive Text</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.is_inclusive}" name="is_inclusive" />
						</div>
					</div> -->
				</div>
				
				<div class="col-md-4 col-lg-4 col-sm-4">
					<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Status</span>
						  <input type="text" class="form-control" placeholder="Test" aria-describedby="basic-addon1" value="{$deal.status}" name="status" id="deal-status" disabled>
						  	<!-- <option value="test">Test</option>
						  	<option value="active">Active</option>
						  	<option value="inactive">Inactive</option>
						  </select> -->
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">State</span>
						  <select class="form-control" placeholder="" aria-describedby="basic-addon1" name="state_id" id="state-select">
						  	
						  </select>
						</div>
					</div>
					
					<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">City</span>
						  <select class="form-control" placeholder="" aria-describedby="basic-addon1" name="city_id" id="city-select">
						  </select>
						</div>

					</div>



					
					<!-- <div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Per Couple Text</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.is_per_couple}" name="is_per_couple" />
						</div>
					</div> -->
					
				</div>
		</div>
		<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="panel panel-default ">
					<div class="panel-heading">
						<h3 class="panel-title">Location</h3>
					</div>
					<div class="panel-body">
						<div id="location-body">
							<div class="col-md-3 col-lg-3 col-sm-3">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Address</span>
										<input type="text" class="form-control field_name_address" placeholder="Address" aria-describedby="basic-addon1" value="{$deal.address}" />
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Location</span>
										<input type="text" class="form-control field_name_location" placeholder="Location" aria-describedby="basic-addon1" value="{$deal.location}" />
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Zone</span>
										<select class="form-control field_name_zone_id" placeholder="" aria-describedby="basic-addon1" id="zone-select">
											<input type="hidden" data-zone-value="{$deal.zone_id}" class="zone-val"/>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Phone</span>
										<input type="text" class="form-control field_name_phone_number" placeholder="Phone Number" aria-describedby="basic-addon1" value="{$deal.phone_number}"  />
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Geo</span>
										<input type="text" class="form-control field_name_geo_location" placeholder="Geo Location" aria-describedby="basic-addon1" value="{$deal.geo_location}"  id="geo-loc"/>
									</div>
								</div>
							</div>
							<div class="col-md-1 col-lg-1 col-sm-1 remove-button-div" style="display: none;">
								<button type="button" class="btn btn-primary location-remove">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</div>
						</div>
						<div id="new-location">
							{foreach from=$deal.locations item =location}
							<div class="new-location-obj">
								<div class="col-md-3 col-lg-3 col-sm-3">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Address</span>
											<input type="text" class="form-control field_name_address" placeholder="" aria-describedby="basic-addon1" value="{$location.address}"  />
										</div>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Location</span>
											<input type="text" class="form-control field_name_location" placeholder="" aria-describedby="basic-addon1" value="{$location.location}"  />
										</div>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Zone</span>
											<select class="form-control field_name_zone_id" placeholder="" aria-describedby="basic-addon1" >
											</select>
											<input type="hidden" data-zone-value="{$location.zone_id}" class="zone-val"/>
										</div>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Phone</span>
											<input type="text" class="form-control field_name_phone_number" placeholder="" aria-describedby="basic-addon1" value="{$location.phone_number}"  />
										</div>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-sm-2">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1">Geo</span>
											<input type="text" class="form-control field_name_geo_location" placeholder="" aria-describedby="basic-addon1" value="{$location.geo_location}" />
										</div>
									</div>
								</div>
								<div class="col-md-1 col-lg-1 col-sm-1">
									<button type="button" class="btn btn-primary location-remove">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</div>
							</div>
							{/foreach}
						</div>
						<div class="pull-right">
							<div class="btn-group" role="group" id="add-zone-button-group">
								<button type="button" class="btn btn-success" id="add-zone-button" data-toggle="modal" data-target=".add_zone_box" onclick="showZoneModal()">Add New Zone &nbsp;&nbsp;
									<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
								</button>
							</div>
							&nbsp;&nbsp;

							<div class="btn-group" role="group" id="add-location-button-group">
								<button type="button" class="btn btn-success" id="add-location-button">Add Location &nbsp;&nbsp;
									<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
								</button>
							</div>

							&nbsp;&nbsp;
							<div class="btn-group" role="group" id="add-location-button-group">
								<button type="button" class="btn btn-success" data-toggle="modal" data-target=".map_box" id="show-map">View Map &nbsp;&nbsp;
									<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
								</button>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-12 col-sm-12">

				<div class="col-md-6 col-lg-6 col-sm-6">
					<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Offer</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.offer}" name="offer" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">Recommendation</span>
						  <input type="text" class="form-control" placeholder="Miss TM suggests ..." aria-describedby="basic-addon1" value="{$deal.recommendation}" name="recommendation" />
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Coupon Code</span>
							<input type="text" class="form-control" placeholder="Click on checkbox in case of comma multiple codes" aria-describedby="basic-addon1" value="{$deal.coupon_code}" name="coupon_code" id="coupon-code"/>
							<span class="input-group-addon">
								{if $deal.multiple_coupon eq 1}
									<input type="checkbox" name="multiple_coupon" aria-label="multiple" id="multiple-coupon" checked>
								{/if}
								{if $deal.multiple_coupon eq 0}
									<input type="checkbox" name="multiple_coupon" aria-label="multiple" id="multiple-coupon">
								{/if}
							</span>
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">SMS Text</span>
							<input type="text" class="form-control" placeholder="words COUPON_CODE, PHONE, NAME shall be replaced by actuals" aria-describedby="basic-addon1" value="{$deal.sms_text}" name="sms_text" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Deal Images</h3>
						  	</div>
						  	<div class="panel-body" id="deal-image-panel-body">
							{foreach from=$deal.images_json item =image}
							  <div class="col-sm-6 col-md-4 image-box">
							    <div class="thumbnail">
							      <img src="{$image}" alt="" class="deal-image">
							      <div class="caption">
							        <p><a href="#" class="btn btn-danger remove-image-button" role="button">Remove</a></p>
							      </div>
							    </div>
							  </div>
							{/foreach}
								<div class="col-md-12 col-lg-12 col-sm-12">
									<div class="btn-group pull-right" role="group" id="add-deal-img-button-group">
										<button type="button" class="btn btn-success" id="add-deal-img-button">Add More &nbsp;&nbsp;
										<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Menu Images</h3>
						  	</div>
						  	<div class="panel-body" id="menu-image-panel-body">
							{foreach from=$deal.menu_images_json item =image}
							  <div class="col-sm-6 col-md-4 image-box">
							    <div class="thumbnail">
							      <img src="{$image}" alt="" class="menu-image">
							      <div class="caption">
							        <p><a href="#" class="btn btn-danger remove-image-button" role="button">Remove</a></p>
							      </div>
							    </div>
							  </div>
							{/foreach}
								<div class="col-md-12 col-lg-12 col-sm-12">
									<div class="btn-group pull-right" role="group" id="add-menu-img-button-group">
										<button type="button" class="btn btn-success" id="add-menu-img-button">Add More &nbsp;&nbsp;
										<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



				<div class="col-md-6 col-lg-6 col-sm-6">
					
					
					<!-- <div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">HashTags</span>
						  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" >
						  <span class="label label-default">Default</span>
						  </input>
						</div>
					</div> -->
					<div class="form-group">
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Hashtags</h3>
						  	</div>
						  	<div class="panel-body">
						  	<div class="form-group">
							  	{foreach from=$deal.hashtag_list item =hashtagName}
							  		<div class="btn-group-hashtag hashtag-group" style="padding-top:2px">
									  <button type="button" class="btn btn-primary hashtag-val" disabled>{$hashtagName}</button>
									  <button type="button" class="btn btn-primary hashtag-remove">
									    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
									  </button>
									</div>
									&nbsp;&nbsp;
							  		<!-- <span class="label label-info hashtag-val">{$hashtagName}</span>&nbsp;&nbsp; -->
							  	{/foreach}
						  	</div>
						  	<div class="form-group">
									 <div class="input-group">
									  <span class="input-group-addon" id="basic-addon1">Hashtags</span>
									  <input type="text" class="form-control" placeholder="Add more hashtags (comma separated). eg #nice,#good" aria-describedby="basic-addon1" id="hashtag-new">
									</div> 
								</div>
						  	</div>
					  	</div>
				  	</div>
					
					<div class="form-group">
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">List Image</h3>
						  	</div>
						  	<div class="panel-body">
							
							  <div class="col-sm-12 col-md-12">
							    <div class="thumbnail">
							      <img src="{$deal.list_view_image}" alt="" id="deal-image">
							      <!-- <div class="caption">
							        <p><a href="#" class="btn btn-danger " role="button">Remove</a></p>
							      </div> -->
							    </div>
							    <div class="form-group">
									 <div class="input-group">
									  <span class="input-group-addon" id="basic-addon1">New Image Url</span>
									  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" id="deal-image-new">
									</div> 
								</div>
							  </div>
							</div>
						</div>
					</div>
					
					
					<div class="form-group ">
						
						<div class="panel panel-default ">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Terms And Conditions</h3>
						  	</div>
						  	<div class="panel-body">
						  		{foreach from=$deal.terms_and_conditions item =terms}
						  		<div class="form-group text-input-fld">
									<div class="input-group">
										<input type="text" class="form-control tnc_text" placeholder="T&C Point" value="{$terms}">
											<span class="input-group-btn">
												<button class="btn btn-default remove-text-input" type="button">
													<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												</button>
											</span>
										</input>
									</div>
								</div>
								{/foreach}
								
								<div class="btn-group pull-right" role="group" id="add-tnc-button-group">
									<button type="button" class="btn btn-success" id="add-tnc-button">Add More &nbsp;&nbsp;
									<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
									</button>
								</div>
							
							</div>
					</div>
					
					{if $deal.datespot_id eq null}
						<input type="hidden" name="action" value="add_new_DS" />
					{/if}
					
					<input type="hidden"  id="select-state-id" value="{$deal.state_id}" />
					<input type="hidden"  id="select-city-id"  value="{$deal.city_id}" />
					<input type="hidden"  id="select-zone-id"  value="{$deal.zone_id}" />
					<input type="hidden"  id="select-status" value="{$deal.status}" />
					<input type="hidden"  id="select-amount-type"  value="{$deal.amount_type}" />
					<input type="hidden"  id="select-deal-type"  value="{$deal.deal_type}" />
					<input type="hidden"  id="select-pricing-symbol"  value="{$deal.pricing}" />
					<input type="hidden"  id="read-only-val"  value="{$read_only}" />
					<input type="hidden"  id="base-url"  value="{$baseurl}" />

					<input type="hidden"  name="hash_id"  value="{$deal.hash_id}" />

					<input type="hidden" name="images_json" id="images-json"/>
					<input type="hidden" name="menu_images_json" id="menu-images-json" />
					<input type="hidden" name="hashtag_json" id="hashtag-json" />
					<input type="hidden" name="list_view_image" id="list-view-image"/>
					<input type="hidden" name="terms_and_conditions" id="t-n-c"/>

					
					<!--  <div class="input-group">
					  <span class="input-group-addon" id="basic-addon1">Terms And Conditions</span>
					  <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$deal.terms_and_conditions}" />
					</div> -->
				</div>
				{if $read_only neq true}
				<div class="btn-group col-md-12 col-lg-12 col-sm-12" role="group" >
					<button type="button" class="btn btn-primary col-md-12 col-lg-12 col-sm-12" id="save-form-action">Save</button>
					<div class="progress">
					  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					    <span class="sr-only">Saving</span>
					  </div>
					</div>
				</div>	
				{/if}
			</div>
			</div>


	</form>

		<div class="modal fade add_zone_box" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" >
			<div class="modal-dialog modal-sm">
				<div class="modal-content" id="zone-bx">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="modal-title" id="mySmallModalLabel">Add Zone</h3>
					</div>
					<div class="modal-body">

						<!-- Single button -->
						<form action="{$baseurl}/dealAdmin/zoneData.php" method="POST" id="zone-form">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Zone Name</span>
									<input type="text" class="form-control" placeholder="Add new zone name." aria-describedby="basic-addon1" id="zone-new">
								</div>
							</div>
							<div class="btn-group" id='yes-button'>
								<button type="button" class="btn btn-default" onclick="addNewZone()">
									Save
								</button>
							</div>
							<div class="btn-group pull-right" id="cancel-button">
								<button type="button" class="btn btn-default"  data-dismiss="modal" aria-hidden="true">
									Cancel
								</button>
							</div>
							<div class="progress" id="show-wait" style="display:none;">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									<span class="sr-only">Saving</span>
								</div>
							</div>
							<div class="alert alert-success" role="alert" id="zone-success" style="display: none;">Zone Saved Successfully !</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade map_box" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" >
			<div class="modal-dialog modal-md">
				<div class="modal-content" id="zone-bx">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="modal-title" id="mySmallModalLabel">Select Coordinates</h3>
					</div>
					<div class="modal-body">
						<div id="main-map" style="width:100%; height:100%;">
							<input id="pac-input" class="controls" type="text" placeholder="Search Box">
							<div id="map" style="height: 400px"></div>
						</div>
						<!-- Single button -->
					</div>
				</div>
			</div>
		</div>

		<script src="{$baseurl}/js/dealAdmin/dealForm.js"></script>

		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvGCQPLFUyFB2TPYSiPT_cG8K50WOjRlk&libraries=places&callback=initAutocomplete"
						async defer></script>
</body>
</html>
