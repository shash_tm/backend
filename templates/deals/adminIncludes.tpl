<link rel="stylesheet" href="{$baseurl}/css/common/bootstrap.min.css">
<script src="{$baseurl}/js/common/jquery.min.js"></script>
<script src="{$baseurl}/js/common/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="{$baseurl}/css/common/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="{$baseurl}/css/common/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{$baseurl}/css/admin/global.css">
<script type="text/javascript" charset="utf8" src="{$baseurl}/js/common/jquery.dataTables.js"></script>

<script src="{$baseurl}/js/common/DatatablePlugin/dataTables.buttons.min.js"></script>
<script src="{$baseurl}/js/common/DatatablePlugin/buttons.flash.min.js"></script>
<script src="{$baseurl}/js/common/DatatablePlugin/jszip.min.js"></script>
<script src="{$baseurl}/js/common/DatatablePlugin/pdfmake.min.js"></script>
<script src="{$baseurl}/js/common/DatatablePlugin/vfs_fonts.js"></script>
<script src="{$baseurl}/js/common/DatatablePlugin/buttons.html5.min.js"></script>
<script src="{$baseurl}/js/common/DatatablePlugin/buttons.print.min.js"></script>

{*<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>

<meta name="google-signin-client_id" content="133388128041-8onoc1jcrbkcblmm02pii0e7ld4ht062.apps.googleusercontent.com">*}
<script src="{$baseurl}/js/admin/global.js"></script>
<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>