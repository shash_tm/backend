<!DOCTYPE html>
<html lang="en">
<head>
	<title>Deals List</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	{include file='../deals/adminIncludes.tpl'}
	<script src="{$baseurl}/js/dealAdmin/eventList.js"></script>
</head>
<body>

{include file='../deals/header.tpl'}

<div class="container-fluid check-me">
	<table class="table table-responsive" id="datespot-list">
		<thead>
		<tr>
			<th>Event ID</th>
			<th>Name </th>
			<th>Date </th>
			<th>Status</th>
			<th>Location</th>
			<th>City</th>
			<th>Rank</th>
			<th style="display:none;">create_date</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody>
		{foreach from=$deals item=deal}
			<tr class="success">
				<td> {$deal.datespot_hash_id}</td>
				<td> {$deal.name}</td>
				<td> {$deal.date_pretty_text}</td>
				<td> {$deal.status}</td>
				<td> {if $deal.location_count eq '1'}
						{$deal.location}
					{/if}
					{if $deal.location_count neq '1'}
						Multiple Locations
					{/if}
				</td>
				<td> {$deal.city_name}, {$deal.state_name}</td>
				<td> <span>{$deal.rank} </span>&nbsp;&nbsp;
					<a href="#" data-id="{$deal.datespot_hash_id}" data-action="up" class="update-rank"> <span class="glyphicon glyphicon-collapse-up" aria-hidden="true"></span></a>
					<a href="#" data-id="{$deal.datespot_hash_id}" data-action="down" class="update-rank">	<span class="glyphicon glyphicon-collapse-down" aria-hidden="true"></span></a>
				</td>
				<td style="display:none;">{$deal.create_date}</td>
				<td>
					{if $read_only eq 'false'}
						<a href="{$baseurl}/dealAdmin/eventView.php?datespot_id={$deal.datespot_hash_id}" data-toggle="tooltip" title="Edit Or View" >
							<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
						</a>
						&nbsp;&nbsp;&nbsp;
						<a href="{$baseurl}/dealAdmin/eventView.php?datespot_id={$deal.datespot_id}&action=copy" data-toggle="tooltip" title="Copy To New" >
							<span class="glyphicon glyphicon-copy" aria-hidden="true"></span>
						</a>

						&nbsp;&nbsp;&nbsp;
						{if $deal.status eq active}
							&nbsp;&nbsp;
							<a href="#" data-toggle="modal" data-target=".confirm_box" class="update-action" data-id="{$deal.datespot_hash_id}" data-action="deactivate"><span>De-activate</span></a>
						{/if}

						{if $deal.status eq test || $deal.status eq inactive}
							<span class="glyphicon glyphicon-trash delete-it" aria-hidden="true"
										data-id="{$deal.datespot_hash_id}" style="cursor: pointer;color:#337ab7"></span>
							&nbsp;&nbsp;
							<a href="#" data-toggle="modal" data-target=".confirm_box" class="update-action" data-id="{$deal.datespot_hash_id}" data-action="activate"><span>Activate</span></a>

						{/if}
					{/if}
					{if $read_only eq 'true'}
						<a href="{$baseurl}/dealAdmin/eventView.php?datespot_id={$deal.datespot_hash_id}" data-toggle="tooltip" title="View" >
							<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
						</a>
					{/if}
				</td>
			</tr>
		{/foreach}
		</tbody>
	</table>
</div>


<div class="modal fade confirm_box" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" >
	<div class="modal-dialog modal-sm">
		<div class="modal-content" id="upload-bx">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 class="modal-title" id="mySmallModalLabel">Confirm Action</h3>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger" role="alert">Are You sure you want to <span id="action-name"></span> the deal?</div>
				<!-- Single button -->
				<form action="{$baseurl}/dealAdmin/dealFormAction.php" method="POST" id="datespot-form">
					<div class="btn-group" id='yes-button'>
						<button type="button" class="btn btn-default" onclick="updateDatespot()">
							Yes
						</button>
					</div>
					<div class="btn-group pull-right" id="cancel-button">
						<button type="button" class="btn btn-default"  data-dismiss="modal" aria-hidden="true">
							Cancel
						</button>
					</div>
					<div class="progress" id="show-wait" style="display:none;">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							<span class="sr-only">Saving</span>
						</div>
					</div>
					<input type="hidden" id="datespot-hash-id-for-update" val=""/>
					<input type="hidden" id="datespot-update-action" val=""/>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#datespot-list').DataTable({
			"bSort" : true,
			"order": [[ 7, "desc" ]],
			"bAutoWidth": false
		});
	});
</script>
</body>
</html>
