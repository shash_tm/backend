<!DOCTYPE html>
<html lang="en">
<head>
  <title>Reporting</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{$baseurl}/css/admin/reporting.css">
	{include file='../deals/adminIncludes.tpl'}

	<script src="{$baseurl}/js/reporting/reporting.js"></script>

	<style>


	</style>
</head>
<body>
<input type="hidden" value="{$baseurl}" id="action-url"/>


{include file='../deals/header.tpl'}

<div class="container-fluid check-me">
	<div class="col-lg-3 col-sm-3 col-md-3">
		{include file='../ReportingDashboard/tableMenu.tpl'}
	</div>
	<div class="col-lg-9 col-sm-9 col-md-9 ">
		<div class="col-lg-12 col-sm-12 col-md-12">
			<div class="alert alert-danger" role="alert" id="query-error" style="display: none">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				<span class="sr-only">Error:</span>
				<span class="error-text"></span>
			</div>
			<div class="form-group">
				<label for="query">Query:</label>
				<textarea class="form-control" rows="3" id="query">{$query.query}</textarea>
			</div>
		</div>

		<div class="col-lg-2 col-sm-2 col-md-2 pull-right">
			<div class="btn-group pull-right" role="group" >
				<button type="button" class="btn btn-primary" id="save-query" data-toggle="modal" data-target=".save_query">Save Query
					<span class="glyphicon glyphicon glyphicon-floppy-open" aria-hidden="true"></span>
				</button>
			</div>
		</div>
		{*<div class="col-lg-2 col-sm-2 col-md-2 pull-right">
			<div class="btn-group pull-right" role="group">
				<button type="button" class="btn btn-primary" id="cron-query">Set as cron
					<span class="glyphicon glyphicon glyphicon-time" aria-hidden="true"></span>
				</button>
			</div>
		</div>*}
		<div class="col-lg-2 col-sm-2 col-md-2 pull-right">
			<div class="btn-group pull-right" role="group">
				<button type="button" class="btn btn-primary" id="run-query">Run Query
					<span class="glyphicon glyphicon glyphicon-play" aria-hidden="true"></span>
				</button>
			</div>
		</div>


	</div>

	<div class="col-lg-12 col-sm-12 col-md-12" style="margin-top: 20px;">
		<table class="table table-responsive" id="query-result">

		</table>
		<div style="clear:both"></div>
	</div>

	<input type="hidden" value="{$query.set_as_cron}" id="set-is-cron">
	<input type="hidden" value="{$query.frequency}" id="set-frequency">
</div>



<div class="modal fade save_query" role="dialog" aria-labelledby="myBigModalLabel" aria-hidden="true" data-keyboard="false" >
    <div class="modal-dialog modal-md">
        <div class="modal-content" id="save-query">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="mySmallModalLabel">Save Query</h3>
            </div>
            <div class="modal-body">
							<form action="{$baseurl}/reporting/dashboard_api.php" method="POST" id="save-query-form">

								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Name</span>
										<input type="text" class="form-control" placeholder="Give a meaningful name" aria-describedby="basic-addon1"  name="query_name" value="{$query.query_name}"/>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Description</span>
										<input type="text" class="form-control" placeholder="Query Output etc" aria-describedby="basic-addon1"  name="query_description" value="{$query.query_description}"/>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<input type="checkbox" aria-label="Set As Cron" name="set_as_cron" id="is-cron">
										</span>
										<select type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" name="frequency" disabled id="query-frequency" name="query_frequency">
											<option value="60">Hourly</option>
											<option value="360">6 Hours</option>
											<option value="720">Twice Daily</option>
											<option value="1440">Daily</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Emails</span>
										<input type="text" class="form-control" placeholder="Comma Separated emails" aria-describedby="basic-addon1"  name="mailing_list" value="{$query.mailing_list}"/>
									</div>
								</div>
								<button class="btn btn-info form-control" id="save-query-button">Save</button>

								<div class="progress show-wait" style="display:none;">
									<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										<span class="sr-only">Saving</span>
									</div>
								</div>
								{if $query.query_id eq null}
									<input type="hidden" name="action" value="save_query" />
								{/if}
								<input type="hidden" name="query_text" id="query-text" value="">
								{if $query.query_id neq null}
									<input type="hidden" name="query_id" id="query-id" value="{$query.query_id}">
									<input type="hidden" name="action" value="edit_query" />
								{/if}
							</form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

});
</script>
</body>
</html>
