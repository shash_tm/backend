<!DOCTYPE html>
<html lang="en">
<head>
  <title>Query List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	{include file='../deals/adminIncludes.tpl'}
  <script src="{$baseurl}/js/reporting/queryList.js"></script>
</head>
<body>

{include file='../deals/header.tpl'}

<div class="container-fluid check-me">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Filters</h3>
    </div>
    <div class="panel-body">
      <form action="{$baseurl}/reporting/queryList.php" id="query-filter-form">
        {if $is_starred neq null}
          <input type="hidden" id="set-filter-starred" value="{$is_starred}" />
        {/if}
        {if $filter_admin_id neq null}
          <input type="hidden" id="set-filter-admin" value="{$filter_admin_id}" />
        {/if}
        <div class="col-md-3 col-lg-3 col-sm-3">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">User</span>
              <select type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" name="filter_admin_id" id="filter-admin-id" >
                <option value="0">=====Select=====</option>
               {foreach from=$admin_list item=admin}
                 <option value="{$admin.admin_id}">{$admin.user_name}</option>
               {/foreach}
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3">
          <div class="input-group">
            <span class="input-group-addon">
                <input type="checkbox" aria-label="Starred" name="is_starred" id="starred">
            </span>
            <input type="text" class="form-control" aria-label="Starred" value="Starred" disabled>
          </div>
        </div>
        <input type="hidden" value="filter-query" name="action">
        <div class="col-md-3 col-lg-3 col-sm-3">
          <button class="btn btn-info form-control" id="filter-query-button">Filter Results</button>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3">
          <button class="btn btn-info form-control" id="filter-my-query-button">My Queries</button>
        </div>
      </form>
    </div>
  </div>
  <table class="table table-responsive" id="query-list">
    <thead>  
      <tr>
        <th>Query ID</th>
        <th>Name</th>
        <th>Description </th>
        <th>Is Set Cron</th>
				<th>Frequency</th>
        <th>Mailing List</th>
        <th>Created By</th>
				<th>Action</th>
      </tr> 
    </thead>
    <tbody> 
    {foreach from=$query_list item=query}
      <tr class="success">
        <td> {$query.query_id}</td>
        <td> {$query.query_name}</td>
        <td> {$query.query_description}</td>
        <td> {$query.set_as_cron}</td>
				<td> {$query.frequency}</td>
        <td> {$query.mailing_list}</td>
				<td> {$query.admin}</td>
				<td>
          <div class="col-md-4 col-lg-4 col-sm-4">
            <a href="{$baseurl}/reporting/dashboard.php?query_id={$query.query_id}" data-toggle="tooltip" title="Edit Or View" >
              <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </a>
          </div>

          {if $query.is_starred eq 'yes'}
            <div class="col-md-4 col-lg-4 col-sm-4">
              <span class="glyphicon glyphicon-star unstar-it" aria-hidden="true" data-query-id="{$query.query_id}"
                    data-admin-id="{$query.created_by}" data-super-admin="{$smarty.session.super_admin}"></span>
            </div>
          {/if}


          {if $query.is_starred eq 'no'}
            <div class="col-md-4 col-lg-4 col-sm-4">
                <span class="glyphicon glyphicon-star-empty star-it" aria-hidden="true" data-query-id="{$query.query_id}"
                      data-admin-id="{$query.created_by}" data-super-admin="{$smarty.session.super_admin}"></span>
            </div>
          {/if}
          <div class="col-md-4 col-lg-4 col-sm-4">
            {if $query.created_by eq $smarty.session.admin_id || $smarty.session.super_admin eq 'yes'}
              <span class="glyphicon glyphicon-trash delete-it" aria-hidden="true"
                    data-query-id="{$query.query_id}" data-admin-id="{$query.created_by}" data-super-admin="{$smarty.session.super_admin}"></span>
            {/if}
          </div>
				</td>
      </tr>
      {/foreach}
    </tbody>
  </table>
</div>


<script>
$(document).ready(function(){
	$('#query-list').DataTable({
        "bSort" : true,
		"order": [[ 7, "desc" ]],
		"bAutoWidth": false
	});
});
</script>
</body>
</html>
