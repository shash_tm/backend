<!DOCTYPE html>
<html lang="en">
<head>
  <title>Reporting</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{$baseurl}/css/admin/reporting.css">
	{include file='../deals/adminIncludes.tpl'}

	<script src="{$baseurl}/js/reporting/adminList.js"></script>
	<style>
		.inactive{
			background-color: transparent;
		}
	</style>
</head>
<body>
<input type="hidden" value="{$baseurl}" id="action-url"/>


{include file='../deals/header.tpl'}

<td class="container-fluid check-me">
	<table class="table">
		<th>Id</th>
		<th>Name</th>
		<th>Super Admin</th>
		<th>Permissions</th>
	{foreach from=$admin_list item=admin}
		<tr>
			<td>{$admin.id}</td>
			<td>{$admin.user_name}</td>
			<td>{$admin.super_admin}</td>
			<td>
				<ul class="nav nav-pills" data-admin-id="{$admin.id}">
					{foreach from=$permissions item=permission}
						<li role="presentation" class="inactivate inactive"><a href="#">{$permission}</a></li>
					{/foreach}
				</ul>
				<input type="hidden" name="privileges" value="{$admin.privileges}" class="privileges"/>
			</td>
		</tr>
	{/foreach}
	</table>

	</div>



</body>
</html>
