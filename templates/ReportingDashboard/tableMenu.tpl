<div class="nav-side-menu">
  <div class="brand">
    Table List
    <a href="#" class="minimize-menu"><span class="pull-right  glyphicon glyphicon-minus pull-right" aria-hidden="true" style="padding: 15px 10px 0px 0px;"></span></a>
  </div>
  {*<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>*}

  <div class="menu-list">

    <ul id="menu-content" class="menu-content collapse out">

      {foreach from=$tables item=table}
        <li  data-toggle="collapse" data-target="#{$table['Tables_in_trulymadly_prod']}" class="collapsed active table-name-menu-item">
          <a href="#" class="table-name">{$table['Tables_in_trulymadly_prod']} <span class="glyphicon glyphicon-chevron-down pull-right" aria-hidden="true" style="padding: 10px 5px 0px 0px;"></span></a>
        </li>
         <ul class="sub-menu collapse" id="{$table['Tables_in_trulymadly_prod']}">
         </ul>
      {/foreach}

      {*<ul class="sub-menu collapse" id="products">
        <li class="active"><a href="#">CSS3 Animation</a></li>
        <li><a href="#">General</a></li>
        <li><a href="#">Buttons</a></li>
        <li><a href="#">Tabs & Accordions</a></li>
        <li><a href="#">Typography</a></li>
        <li><a href="#">FontAwesome</a></li>
        <li><a href="#">Slider</a></li>
        <li><a href="#">Panels</a></li>
        <li><a href="#">Widgets</a></li>
        <li><a href="#">Bootstrap Model</a></li>
      </ul>*}


    </ul>
  </div>
</div>