<!doctype html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta charset="utf-8">
<title>TrulyMadly - Terms of Use</title>
<!--Stylesheet section start here -->
<link href="{$cdnurl}/css/login/reset.css" rel="stylesheet"
	type="text/css" />
<link href="{$cdnurl}/css/login/style.css" rel="stylesheet"
	type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400'
	rel='stylesheet' type='text/css'>
<!--Stylesheet section end here-->
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

</head>
<body>
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
  
</script>
	<!--wrapper secton start here-->
	<div id="wrapper">

		

			<!--Safe container-->
			<div class="safecontainer">
			<div class="termsContainer"><h2>Terms &amp; Conditions</h2>
<ol>
<li>The value of the Voucher is Rs.2500.</li>
<li>The Voucher Code is valid for the next 3 months from the
date of issue.</li>
<li>The winner needs to be a registered FashionAndYou user to
avail the voucher.</li>
<li>The Voucher is valid on everything you purchase from
www.fashionandyou.com.</li>
<li>The Voucher can be availed on purchases of any value i.e. No
Minimum Cart Value is required to avail the Voucher.</li>
<li>It’s a single use voucher.</li>
<li>www.fashionandyou.com shall not be held accountable for authenticity of customers registering on www.trulymadly.com.</li>
<li>Fashionandyou.com reserves the right to stop this promotion or change/modify/add/delete any of the terms and conditions of the
offer at any point of time when required.</li>
<li>By placing an order, the customer accepts all terms and conditions specified on Fashionandyou.com</li>
<li>In case of any query pertaining to use of voucher or regarding the schemes, please email customer care at support@fashionandyou.com or call on 0124-4125000.</li>
<li>Additionally, Fashionandyou.com T&C shall apply for all purchases.</li>
</ol>

			</div>
			
			</div>


			

</body>
</html>
