<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="http://dev.trulymadly.com/trulymadly/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="Description" content="{$endorse_msg}">
<meta content="{$endorse_msg}" property="og:title">
<meta content="{$endorse_msg}" property="og:description">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<title>TrulyMadly</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
//console.log(headLoadTimeStamp);
</script>
<link href="{$cdnurl}/css/endorsement.css?v=2.7" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
</head>

<body>

<script type="text/javascript">
var server="{$SERVER}";
var fb_api="{$fb_api}";
var baseurl = "{$baseurl}";
var id = "{if isset($smarty.get.id)}{$smarty.get.id}{else}0{/if}";
</script>

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<div class="pageheader">
<div class="bgwhite"><img src="{$cdnurl}/images/tmlogoendors.png" class="endhdrlogo">
<ul class="fl appstore">
<li><p>install app:</p></li>
<li><a href ="https://itunes.apple.com/in/app/trulymadly/id964395424?mt=8" target="_blank"><img src="{$cdnurl}/images/appioslogo.png" class="appicon"></a></li>
<li><a href ="https://play.google.com/store/apps/details?id=com.trulymadly.android.app" target="_blank"><img src="{$cdnurl}/images/appandriodlogo.png" class="appicon"></a></li>
</ul></div>

<div class="endrsmsgtxt">
<!--<img src="{$cdnurl}/images/endorsementicon.png" class="fl">--><p>Is this {$attributes.fname}?</p>
</div>

</div>
<!--<p class="titletxt">Back up your friend and endorse {if $gender eq 'M'}him{else}her{/if} right away!</p>-->
<img src="{$attributes.profile_pic}" class="usrimage">
<div class="posr">
<div class="userdetails"><!--<p>{$attributes.fname}, {$attributes.age}</p>-->
<span>{$attributes.age} yrs, {$attributes.city}, {$attributes.height}</span></div>
</div>

<!-- Hashtag section start -->
{if !empty($attributes.InterestsHobbies.preferences)}
<h3>Hashtags</h3>
<div class="hashtagsec">
<ul>
{foreach $attributes.InterestsHobbies.preferences item=val}
<li>{$val}</li>
{/foreach}
</ul>
</div>
{/if}
<!-- Hashtag section end -->

<h3>Work & Education</h3>
<div class="usrwnedetail"><img src="{$cdnurl}/images/work.png?v=2" class="fl"><p>{if isset($attributes.industry)}{$attributes.industry}{/if}{if isset($attributes.designation) && isset($attributes.industry)}, {/if}{if isset($attributes.designation)}{$attributes.designation}{/if}
 {if isset($attributes.companies)}
              {assign var=last1 value=$attributes.companies|@end}     
            	<span>{foreach $attributes.companies key=i item=val}{$val}{if $val neq $last1},&nbsp;{/if}{/foreach}</span>
              {/if}	
</p></div>

<div class="usrwnedetail"><img src="{$cdnurl}/images/education.png?v=2" class="fl"><p>{$attributes.highest_degree}
{if isset($attributes.institutes)}
           	   {assign var=last1 value=$attributes.institutes|@end}     
            	<span>{foreach $attributes.institutes key=i item=val}{$val}{if $val neq $last1},&nbsp;{/if}{/foreach}</span>
            {/if}
</p></div>


<!-- Favourites section start -->  
{if !empty($attributes.favourite) } 
<h3>Favourites</h3>   
{/if} 
<div class="favitemsec">
{if !empty($attributes.favourite) }  
<div class="clb">

<ul class="inticons">
<li class="intmovie"><span class="hijack" key="0" onClick="document.getElementById('arrowtop').style.left='20px';"></span></li>
<li class="intmusic"><span class="hijack" key="1" onClick="document.getElementById('arrowtop').style.left='84px';"></span></li>
<li class="intbooks"><span class="hijack" key="2" onClick="document.getElementById('arrowtop').style.left='148px';"></span></li>
<li class="intfood"><span class="hijack" key="3" onClick="document.getElementById('arrowtop').style.left='213px';"></span></li>
<li class="intother" style="margin:0 auto;"><span class="hijack" key="4" onClick="document.getElementById('arrowtop').style.left='277px';"></span></li>
                    	</ul>
                        
                        <div class="posr">
                        <img src="{$cdnurl}/images/favpointertab.png?v=1" border="0" class="indicatetop" id="arrowtop" style="left:20px;">

							<div class="comnlikes">
                            {if isset($attributes.InterestsHobbies.movies_favorites)}
                        	
           					<span class="colorcode1" name="movie" value="0">
           					{assign var=last2 value=$attributes.InterestsHobbies.movies_favorites|@end}     
	                		{foreach $attributes.InterestsHobbies.movies_favorites item=val}
							{$val}
							{if $val neq $last2},{/if}
							{/foreach}
                            </span>
                            {/if}
                        
                        <span class="seperaterline">|</span>
                            {if isset($attributes.InterestsHobbies.music_favorites)}
                            
                			<span class="colorcode2" name="music" value="1">
           				    {assign var=last3 value=$attributes.InterestsHobbies.music_favorites|@end}     
                			{foreach $attributes.InterestsHobbies.music_favorites item=val}
							{$val}
							{if $val neq $last3},{/if}
							{/foreach}
                            </span>
							{/if}
                            
                            <span class="seperaterline">|</span>
                			{if isset($attributes.InterestsHobbies.books_favorites)}
                           
                			<span class="colorcode1" name="books" value="2">
                			{assign var=last4 value=$attributes.InterestsHobbies.books_favorites|@end}     
                			{foreach $attributes.InterestsHobbies.books_favorites item=val}
							{$val}							{if $val neq $last4},{/if}
							{/foreach}
                            </span>
                            
                            {/if}

							<span class="seperaterline">|</span>                            
                 			{if isset($attributes.InterestsHobbies.travel_favorites)}
                           
                			<span class="colorcode2" name="food" value="3">
							{assign var=last4 value=$attributes.InterestsHobbies.travel_favorites|@end}     
	                		{foreach $attributes.InterestsHobbies.travel_favorites item=val}
							{$val}{if $val neq $last4},{/if}
							{/foreach}
                            </span>
                            {/if}
                            
                            <span class="seperaterline">|</span>
                			{if isset($attributes.InterestsHobbies.other_favorites)}                            
                            
                			<span class="colorcode1" name="others" value="4">
                			{assign var=last4 value=$attributes.InterestsHobbies.other_favorites|@end}     
	                		{foreach $attributes.InterestsHobbies.other_favorites item=val}
							{$val}{if $val neq $last4},{/if}
							{/foreach}
                			</span>
                			{/if}
                            </div>
                       </div>
                        
                	
                </div>    {/if} 
</div> 

    <!-- Favourites section start -->   
    
  <div class="pagefooter">  <a  class="endorsebtn" id ="endorse">This is {if $gender eq 'M'}him{else}her{/if}!</a><a  class="nothankbtn" id="no_thanks">Nope not {if $gender eq 'M'}him{else}her{/if}!</a></div>
    
</div>
</div>


<div id="endorsweb" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="popupframe">
<div class="thnkumsg">
<p>Thanks for backing up your friend!</p>
<a onClick="displayMessage('endorse');">OK</a>
</div>
</div>
</div>


<div id="noendorse"  style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="popupframe">
<a onClick="displayMessage('nothanks');" class="closepopup"><i class="icon-remove-sign"></i></a>
<div class="thnkumsg">
<p>Oops! Whats not right?<br>
<input type="text" name="textfield" id="whynothanks" class="regtxtbox" placeholder="Please enter the reason">
</p>
<a onClick="displayMessage('nothanks');" id="endsubmit">Submit</a>
</div>
</div>
</div>


<div id="loaderfb"  style="display:none;">
<div class="difusescreenwht" align="center"></div>
<div class="loader">
<img src="{$cdnurl}/images/ajax_loader.gif">
</div>
</div>



<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<!-- <script src="{$cdnurl}/js/common/common.js?v=1"></script> --> 
<script src="{$cdnurl}/js/utilities/endorse.js?v=3.1"></script>
<script src="{$cdnurl}/js/common/common.js"></script>


<script type="text/javascript">
$(document).ready(function() {
	finalTemplateRenderTime  = (new Date()).getTime();
	activity = "page_load";
	logObj = {
		"data" : {
			"time_taken" : (finalTemplateRenderTime - headLoadTimeStamp) / 1000,
			"activity" : "endorsements",
			"event_type" : activity,
			"user_id" : id,
			"event_info" : {
				"cookie_id" : headLoadTimeStamp
			}
		}
	};
	logEvent(logObj, 1);
});

$("#endorse").click(function(){
	logEndorseClickCall();
	$(".endorsebtn").toggleClass('clickendorsebtn');
	fbCall();
});
$("#no_thanks").click(function(){
	$(".nothankbtn").toggleClass('clicknothankbtn');
	noThanks();
});
$("#endsubmit").click(function(){
	sendNoThanksReason();
});

$('.hijack').click(function(event){
		var left = 0;
		var key = parseInt($(event.target).attr('key'), 10);
		if(key>0){
			left = 30;
		}
		for(var i=0;i<key;i++){
			left += $('.comnlikes [value="'+i+'"]').width();
		}
		$('.comnlikes').scrollLeft(left);
	});

</script>
</body>
</html>
