<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta charset="utf-8" />
<title>Trulymadly</title>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width" /> 

<!--<link href="{$cdnurl}/js/lib/select2/select2-bootstrap.css" rel="stylesheet"/>
<link href="{$cdnurl}/js/lib/select2/select2.css" rel="stylesheet"/>-->

{if isset($editProfile)}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/style.css?v=19">
{/if}

{if isset($profile)}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/dashboard/profile/style.css?v=12">
{else}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/dashboard/style.css?v=19">
{/if}

{if isset($trustPage)}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/register.css?v=3">
{/if}


{if isset($trustbuilder)}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/flat-ui.css?v=2">
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/verifypayment.css?v=2">
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/chosen.css?v=1">
<script type="text/javascript">
var fbScore = "{$tbNumbers.score.facebook}";
var fbCredits = "{$tbNumbers.credits.facebook}";
var linkedScore = "{$tbNumbers.score.linkedin}";
var linkedCredits = "{$tbNumbers.credits.linkedin}";
var mobileScore = "{$tbNumbers.score.mobile}";
var mobileCredits = "{$tbNumbers.credits.mobile}";
var phoneVerified = "{$phoneVerified}";
</script>
{/if}


{if isset($photo_editor)==false}

<script>
var tm_baseurl="{$baseurl}";
if('{$allE}')
var errors=JSON.parse('{$allE}');
var baseurl='{$baseurl}';

</script>


{/if}


<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<!--<script src="{$cdnurl}/js/jquery.bxslider.min.js"></script>
--><script src="{$cdnurl}/js/jquery.bxslider.js"></script>

<script>
$(document).ready(function(){
$('img').bind('contextmenu', function(e) {
    return false;
});});
</script>


{if isset($editProfile)==false}
 <script src="{$cdnurl}/js/lib/jquery.infinitescroll.min.js?v=1"></script> 
<script src="{$cdnurl}/js/dashboard/matches.js?v=1"></script>
{/if}
<script src="{$cdnurl}/js/dashboard/jquery-ui-1.10.3.custom.js"></script>
<script src="{$cdnurl}/js/dashboard/modernizr.custom.17475.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.simplemodal.js"></script>
<script src="{$cdnurl}/js/dashboard/custom_checkbox_and_radio.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.notify.js"></script>

<script src="{$cdnurl}/js/dashboard/jquery.magnific-popup.js"></script>
{if isset($editProfile)==false}
<script src="{$cdnurl}/js/trustcore.js?v=1.2"></script>
<script src='{$cdnurl}/js/lib/say-cheese.js'></script>
{/if}

<script src="{$cdnurl}/js/lib/chosen.jquery.min.js"></script>
<script src="{$cdnurl}/js/lib/function.js?v=42"></script>

<!--<script src="{$cdnurl}/js/lib/select2/select2.js"></script>-->


{if isset($profile)||isset($editProfile)}
<script src="{$cdnurl}/js/dashboard/scrollpagination.js"></script>
<script src="{$cdnurl}/js/dashboard/easyResponsiveTabs.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.mCustomScrollbar.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.mousewheel.js"></script>
{/if}
	{if isset($editProfile)==false}
<script src="{$cdnurl}/js/dashboard/core.js?v=2.3"></script>
{/if}

<script type="text/javascript">
var cdn = "{$cdnurl}";
var cameraOn = false;
var server="{$SERVER}",verify="{$verifyEmail}";
var fb_api="{$fb_api}";
var likesCount = "{$header.like}";
</script>


<script>
{if isset($trustbuilder)}
var trustScore = "{$trustscore}";
{/if}
var fbScope = "{$facebook_scope}";

  window.fbAsyncInit = function() {
  FB.init({
    appId      : fb_api, // App ID
    channelUrl : server + '/templates/channel.html', // Channel File
    oauth      : true,
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
          });

  };

function signout(){
	if(typeof FB!==undefined){
		FB.getLoginStatus(function(response){
			if(response.status == 'connected'){
				FB.logout(function(response) {
			  		// user is now logged out
					window.location.href = "{$baseurl}/logout.php";
				});
			}else{
				window.location.href = "{$baseurl}/logout.php";
			}
		});
	}else{
		window.location.href = "{$baseurl}/logout.php";
	}
}
  // Load the SDK asynchronously
  
  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
{if isset($editProfile)==false}
<script type="text/javascript" src="https://platform.linkedin.com/in.js?async=true"></script>
<script type="text/javascript">
IN.init({
  api_key: "{$linkedin_api}",
  onLoad: "fetchLinkedIn",
  credentials_cookie: true
  });
</script>
<script async>
function fetchLinkedIn() {
$('a[id*=li_ui_li_gen_]').html('<p class="linkedin"><a>Verify Now</a></p>');
$("#linked_signin").show();
$("#linkedInConnect").hide();
IN.Event.on(IN, "auth", onLinkedInAuth);
}
function onLinkedInAuth() {
	IN.API.Profile("me","url=http://www.linkedin.com/in/jakobheuser").fields("id", "firstName", "lastName","num_connections","email-address","positions").result(submitLinkedIn);
		$("#linkedInConnect").show();
}
</script>
{/if}


{if isset($photo_editor)}
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/register/style.css?v=1">

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/lib/jquery-ui-1.10.3.custom.js"></script>
<script src="{$cdnurl}/js/lib/jquery.ui.spinner.js"></script>
<script src="{$cdnurl}/js/lib/custom_checkbox_and_radio.js"></script>
<script src="{$cdnurl}/js/lib/chosen.jquery.min.js"></script>
<script src="{$cdnurl}/js/lib/jquery_cookie.js"></script>
<script src="{$cdnurl}/js/lib/function.js?v=4"></script>
{/if}

<!--[if IE]>
     <link  rel="stylesheet" type='text/css' href="css/IE-only.css" >
<![endif]-->
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="{$cdnurl}/css/dashboard/ie8.css" />
<![endif]-->

</head>
<body>
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
</script>

{if isset($first_login)}
{literal}
	<script type="text/javascript">
	var fb_param = {};
	fb_param.pixel_id = '6018165400855';
	fb_param.value = '0.00';
	fb_param.currency = 'INR';
	(function(){
  	var fpw = document.createElement('script');
  	fpw.async = true;
  	fpw.src = '//connect.facebook.net/en_US/fp.js';
	var ref = document.getElementsByTagName('script')[0];
  	ref.parentNode.insertBefore(fpw, ref);
	})();
	</script>
	<noscript>
	<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6018165400855&amp;value=0&amp;currency=INR" />
	</noscript>
{/literal}
{if isset($smarty.session.user_id)}
<script type="text/javascript" src="https://track.in.omgpm.com/644771/transaction.asp?APPID={$smarty.session.user_id}&MID=644771&PID=12654&status="></script>
<noscript><img src="https://track.in.omgpm.com/apptag.asp?APPID={$smarty.session.user_id}&MID=644771&PID=12654&status=" border="0" height="1" width="1"></noscript>
{/if}
{/if}
<!--POP up frame Start-->
<!-- 
	<div align="center" class="difusescreen" style="display: none;"></div>
	<div align="center" class="popupframe" style="display: none;">
		<div id="photoUploadButton">
			<input type="button" value="Upload" id="uploadPic"
				onclick="uploadImage()" /> <input type="button" value="Cancel"
				id="cancelPic" onclick="closePopUp()" />
		</div>
		<div id="imageDiv"></div>
		<div align="center" id="say-cheese-container"></div>
	</div>
 -->
<div class="wrapper">
	<!--Header-->
	<div class="header">
    	<div class="container">  
        	<div class="clearfix">
                <div class="logo"><a href="{$baseurl}/dashboard.php"><img src="{$cdnurl}/images/dashboard/logo.png" alt="Trulymadly" title="Trulymadly"></a></div>
                <!--Menu-->
                <div class="mainmenu">
                    <a class="menu-link" href="#menu"><img src="{$cdnurl}/images/dashboard/menu-icon.png"></a>
                    <div class="menu" id="menu" {if isset($smarty.session.user_id_admin)} style="display: none;"{/if}>
                       <ul class="clearfix">
                            <li class="has-submenu"><a href="{$baseurl}/dashboard.php" class="dashboard_header">Home</a></li>
                            <li class="has-submenu"><a href="{$baseurl}/matches.php" class="matches_header">Matches  {if isset($header.matches)}<span class="notification">{$header.matches}</span>{/if}</a></li>
                            <li class="has-submenu"><a href="{$baseurl}/msg/messages.php" class="messages_header">Messages {if isset($header.messages)}<span class="notification">{$header.messages}</span>{/if}</a></li>
                            <li class="has-submenu clicklikes"><a href="{$baseurl}/likeme.php" class="likes_header">Likes {if isset($header.like)}<span class="notification">{$header.like}</span>{/if}</a>

                     <div class="likes_content" style="display:block;">
                    	<!--<div class="likeclose"><img src="../../images/dashboard/x.png"></div>-->
                    
                        <div class="likes-top-arrow"></div>
                        <div class="likes_dropdown_content"></div>
                    </div></li>
                      
                   
                            
<li class="has-submenu" onMouseOver="$('.credit_content').show();" onMouseOut="$('.credit_content').hide();">
<a  class="messages_header">Credits {if isset($header.credits)}
{if ($header.payment_plan eq 'unlimited')}
<span class="notification" id="credits_update">Unlimited</span>
{else}
<span class="notification" id="credits_update">{$header.credits}</span>
{/if}
{/if}</a>
<div class="credit_content" style="display: none;">
<div class="credit-top-arrow"></div>
<div class="credit_dropdown_content">
<ul>
{if ($header.creditlog)}
{foreach from=$header.creditlog item = log}
<li>{$log.message}<p class="fr creditno">{$log.credit_assgined}</p><p class="likes-info">{$log.time}</p></li>
{/foreach}
{else}
<div class="infocredit">
You don't have any credits.<br><a href="{$baseurl}/payment.php">Purchase Credits</a>
</div>
{/if}
<!-- <li>LinkedIn Verification <p class="fr creditno">+50</p><p class="likes-info">Jan 14,2014 02:58 PM</p></li> -->
<!-- <li>ID Verification <p class="fr creditno">+100</p><p class="likes-info">Jan 14,2014 02:58 PM</p></li> -->
<!-- <li>Address Verification <p class="fr creditno">+150</p><p class="likes-info">Jan 14,2014 02:58 PM</p></li> -->
<!-- <li>Employment Verification <p class="fr creditno">+150</p><p class="likes-info">Jan 14,2014 02:58 PM</p></li> -->
<!-- <li>Phone Verification <p class="fr creditno">+50</p><p class="likes-info">Jan 14,2014 02:58 PM</p></li> -->
</ul>
</div>
</div>
</li>
							
                        
                    
                    
                      </ul>
                    </div>
                    
                    
                </div>
                <!--User Details-->
                <div class="userdetails">
                    <img src="{$header.user_image}" height="27" width="27" class="photo"> <span>{$header.name}</span> <img src="{$cdnurl}/images/dashboard/down-arrow.png" class="downarrow">
                    <ul class="usersettings">
                    <li><a href="{$baseurl}/photo.php">My Photos</a></li>
                    <li><a href="{$baseurl}/trustbuilder.php">Trust Builder</a></li>
                    <li><a href="{$baseurl}/profile.php?me=true">My Profile</a></li>
                    <li><a href="{$baseurl}/editprofile.php">Edit Profile</a></li>
                    <li><a href="{$baseurl}/editprofile.php">Edit Preferences</a></li>
                    <li><a href="{$baseurl}/payment.php">Buy Credits</a></li>
                        <!--<li><a href="{$baseurl}/logout.php">Sign Out</a></li>-->
                         <li><a onclick="signout()">Sign Out</a></li>
                    </ul>
                </div>
                
                
                
                  
                
                
            </div>
    	</div>
    </div>
    
     </div>

<!--For Suspended Users -->
{if $header.status eq 'suspended'}
<div id="diffframe1" style="display:;">
<div class="difusescreen" align="center"></div>
<div class="sndmpopupframe">
<strong>Your profile is suspended from TrulyMadly.com<br />
You can't take any further action.<br/>
 Please contact <a href="mailto:yourstrulymadly@trulymadly.com">yourstrulymadly@trulymadly.com</a> for re-activation.</strong>
</div></div>
{/if}
