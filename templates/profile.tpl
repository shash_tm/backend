{include file='templates/header_dashboard.tpl'}


<!--
<!DOCTYPE>
<html>
<head>
<meta charset="utf-8" />
<title>Trulymadly</title>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width" />
<link type="text/css" rel="stylesheet" href="{$cdnurl}/css/dashboard/profile/style.css">
<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery-ui-1.10.3.custom.js"></script>
<script src="{$cdnurl}/js/dashboard/modernizr.custom.17475.js"></script>
<script src="{$cdnurl}/js/jquery.bxslider.js"></script>
<script src="{$cdnurl}/js/dashboard/scrollpagination.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.simplemodal.js"></script>
<script src="{$cdnurl}/js/dashboard/custom_checkbox_and_radio.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.notify.js"></script>
<script src="{$cdnurl}/js/dashboard/easyResponsiveTabs.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.mCustomScrollbar.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.mousewheel.js"></script>
<script src="{$cdnurl}/js/dashboard/core.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.magnific-popup.js"></script> 
--><!--[if IE]>
     <link  rel="stylesheet" type='text/css' href="{$cdnurl}/css/dashboard/IE-only.css" >
<![endif]-->
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="{$cdnurl}/css/dashboard/ie8.css" />
<![endif]-->

<script>
var tm_baseurl="{$baseurl}";
var is_locked = "{$is_locked}";
var likediv = "{$likedMe_div}";
var credits_link = "{$credits_link}";
var msg_url = "{$msg_url}";
var percent = "{$attributes.trust_meter.trust_score}";
var mutual_count = "{$attributes.mutual_connections_count}";
var info_msg = "{$info_msg}";

function fbtopen()
{
document.getElementById("fbtitle").style.display="block";
}
function fbtclose()
{
document.getElementById("fbtitle").style.display="none";
}

function phopen()
{
document.getElementById("phtitle").style.display="block";
}
function phclose()
{
document.getElementById("phtitle").style.display="none";
}
</script>

<script><!--
$(document).ready(function() {
		var angle = percent*1.8;
		angle = angle - 90;
		angle = angle+"deg";
		$("#pieSlice1 .pie").css("-webkit-transform","rotate("+angle+")");
		$("#pieSlice1 .pie").css("-moz-transform","rotate("+angle+")");
		$("#pieSlice1 .pie").css("-o-transform","rotate("+angle+")");
		$("#pieSlice1 .pie").css("transform","rotate("+angle+")");

		//$(".verifyPopup").mouseout(function(){
		//		$(".infocontainer").remove();
			//});
});
</script>
</head>

<script>
function fun_msg(i){
	
	if ($('.infocontainer').length == 0) {
	var app = '<div class="infocontainer">Your profile is not Verified yet. You can not take any further action. Please verify yourself. &nbsp;&nbsp;<a href="{$baseurl}/trustbuilder.php">Verify Now</a></div>';
	$(i).append(app);
	}
	else{
		$('.infocontainer').hide();
		var app = '<div class="infocontainer">Your profile is not Verified yet. You can not take any further action. Please verify yourself. &nbsp;&nbsp;<a href="{$baseurl}/trustbuilder.php">Verify Now</a></div>';
		$(i).append(app);}
}
</script>
<body>

<!--<div class="wrapper">
	Header
	<div class="header">
    	<div class="container">
        	<div class="clearfix">
                <div class="logo"><a href="{$baseurl}/dashboard.php"><img src="{$cdnurl}/images/dashboard/logo.png" alt="Trulymadly" title="Trulymadly"></a></div>
                Menu
                <div class="mainmenu">
                    <a class="menu-link" href="#menu"><img src="{$cdnurl}/images/dashboard/menu-icon.png"></a>
                   <div class="menu" id="menu">
                       <ul class="clearfix">
                            <li class="has-submenu"><a href="{$baseurl}/dashboard.php">Home</a></li>
                            <li class="has-submenu"><a href="{$baseurl}/matches.php" >Matches  {if isset($header.matches)}<span class="notification">{$header.matches}</span>{/if}</a></li>
                            <li class="has-submenu"><a href="{$baseurl}/msg/messages.php">Messages {if isset($header.messages)}<span class="notification">{$header.messages}</span>{/if}</a></li>
							<li class="has-submenu"><a href="{$baseurl}/likeme.php">Likes {if isset($header.like)}<span class="notification">{$header.like}</span>{/if}</a></li>
                      <li class="has-submenu">Credits {if isset($header.credits)}<span class="notification">{$header.credits}</span>{/if}</li>
                        </ul>
                    </div>
                  --><!--  <div class="likes_content">
                        <div class="likes-top-arrow"></div>
                        <ul class="customscroll vscroll">
                            <li class="clearfix">
                                <img src="{$cdnurl}/images/dashboard/likes/likes1.jpg" class="likes-pic">
                                <div class="likes-details">
                                    <p class="likes-name">Renee Joseph <span>has liked you</span></p>
                                    <p class="likes-info">Today at 4:14 PM</p>
                                </div>
                                <p class="likeback-onhover"><a href="javascript:void(0);">Like back</a>
                                    <div class="clicklikeback" style="display:none;">
                                        <div id="youliked">
                                            <p>text</p>
                                        </div>
                                	</div>
                                </p>
                            </li>
                            <li class="clearfix">
                                <img src="{$cdnurl}/images/dashboard/likes/likes2.jpg" class="likes-pic">
                                <div class="likes-details">
                                    <p class="likes-name">Priyanka Chawla <span>has liked you</span></p>
                                    <p class="likes-info">Yesterday at 2:56 AM</p>
                                </div>
                                <p class="likeback-onhover"><a href="javascript:void(0);">Like back</a></p>
                            </li>
                            <li class="clearfix">
                                <img src="{$cdnurl}/images/dashboard/likes/likes3.jpg" class="likes-pic">
                                <div class="likes-details">
                                    <p class="likes-name">Amita Sharma <span>has liked you</span></p>
                                    <p class="likes-info">9/10/13 at 4:14 PM</p>
                                </div>
                                <p class="likeback-onhover"><a href="javascript:void(0);">Like back</a></p>
                            </li>
                            <li class="clearfix">
                                <img src="{$cdnurl}/images/dashboard/likes/likes4.jpg" class="likes-pic">
                                <div class="likes-details">
                                    <p class="likes-name">Indira Gupta <span>has liked you</span></p>
                                    <p class="likes-info">4/09/13 at 2:56 AM</p>
                                </div>
                                <p class="likeback-onhover"><a href="javascript:void(0);">Like back</a></p>
                            </li>
                            <li class="clearfix">
                                <img src="{$cdnurl}/images/dashboard/likes/likes2.jpg" class="likes-pic">
                                <div class="likes-details">
                                    <p class="likes-name">Priyanka Chawla <span>has liked you</span></p>
                                    <p class="likes-info">Yesterday at 2:56 AM</p>
                                </div>
                                <p class="likeback-onhover"><a href="javascript:void(0);">Like back</a></p>
                            </li>
                            <li class="clearfix">
                                <img src="{$cdnurl}/images/dashboard/likes/likes3.jpg" class="likes-pic">
                                <div class="likes-details">
                                    <p class="likes-name">Amita Sharma <span>has liked you</span></p>
                                    <p class="likes-info">9/10/13 at 4:14 PM</p>
                                </div>
                                <p class="likeback-onhover"><a href="javascript:void(0);">Like back</a></p>
                            </li>
                            <li class="clearfix last">
                                <img src="{$cdnurl}/images/dashboard/likes/likes4.jpg" class="likes-pic">
                                <div class="likes-details">
                                    <p class="likes-name">Indira Gupta <span>has liked you</span></p>
                                    <p class="likes-info">4/09/13 at 2:56 AM</p>
                                </div>
                                <p class="likeback-onhover"><a href="javascript:void(0);">Like back</a></p>
                            </li>
                        </ul>
                    </div> --><!--
                </div>
                User Details
                <div class="userdetails">
                    <img src="{$header.user_image}" height="27" width="27" class="photo"> <span>{$header.name}</span> <img src="{$cdnurl}/images/dashboard/down-arrow.png" class="downarrow">
                    <ul class="usersettings">
                        <li><a href="partnerprefernces.html"><span class="partners"></span> Partner Prefernces</a></li>
                         <li><a href="{$baseurl}/trustbuilder.php"><span class="signout"></span>Trust Builder</a></li>                     
                        <li class="last"><a href="{$baseurl}/logout.php"><span class="signout"></span> Sign Out</a></li>
                    </ul>
                </div>
            </div>
    	--></div>
    </div>
    <div class="container clearfix mt80" id="mainclass"><!--
      {if isset($verify_div)}
    <div class="verifyinfobox effect2">
    Your profile is not Verified yet. You can not take any further action. Please verify yourself. &nbsp;&nbsp;<a href="{$baseurl}/trustbuilder.php">Verify Now</a>
    </div>
       {/if} 
        -->
        {if isset($reject_div)}
   <div class="verifyinfobox effect2">
    Either of you didn't strike a chord with the other. &nbsp;&nbsp;
    </div>
       {/if}
       
        {if $myStatus eq 'suspended' || $profileStatus eq 'suspended'}
   <div class="verifyinfobox effect2">
    {$info_msg} &nbsp;&nbsp;
    </div>
       {/if}
       
    {if isset($likedMe_div)}
    <div class="expintinfobox" >
    {$attributes.name.fname}  liked your profile and is interested in connecting with you. &nbsp;&nbsp;<a href="javascript:void()" id="{$link_like}" class ="accept_like">Accept</a> &nbsp;&nbsp;<a href="javascript:void()" id ="{$link_reject}" class="decline_like">Decline</a>
    </div>
     {/if}
     
  
     
        <div class="profilemainslider">
        <ul class="pbxslider">
            <li>
                <div class="leftpane mt20">
                        
             {if $attributes.isAnyApproved eq 0}
                <div class="blurimgnote" onMouseOver="fbtopen()" onMouseOut="fbtclose()">Why am I seeing blurred photos?
        <div class="hovrtitle1" style="display:none;" id="fbtitle">
       Because we do not have an approved photo of your's, it is only fair that we show you other member photos if they can see you! <br />
<a href="{$baseurl}/photo.php">Upload Your Photo </a>
       </div>
        </div>
        {/if}
               <!--
                
                
                
                  <button id="open-popup">Open popup</button>

<div id="my-popup" class="mfp-hide white-popup">
  Inline popup
</div>
                   --> 
                   
                   {if $attributes.is_profile_pic_clickable}
                   <div class ='main_image'>
                    <a href ='{$attributes.profile_pic}' class='pop-main-img' id='{$attributes.profile_pic}'>
                   	  <img src="{$attributes.profile_pic}" style='width:300px!important; height:300px!important;' class="mainpic"> 
                   	  	</a>
                   	  	</div>
                   	  
                   	  
                   {else}
                   <img src="{$attributes.profile_pic}" style='width:300px!important; height:300px!important;' class="mainpic"> 
                   {/if}
                          
                          {if $attributes.isAnyApproved eq 0 && !isset($smarty.get.me) && {$attributes.thumbnail_count}>0}
                   <div class="blurimgnote1" onMouseOver="phopen()" onMouseOut="phclose()">+{$attributes.thumbnail_count} more photos
                     <div class="hovrtitle1" style="display:none;" id="phtitle">
       You need to have an approved photo to view the album<br />
<a href="{$baseurl}/photo.php">Upload Your Photo </a>
       </div>
        </div>
                <div class="clb"></div>
        
        {else}
                   <ul class="gallerylist">
                    {foreach from=$attributes.thumbnails item=thumb}
                   		<li><a href="{$thumb}"><img src="{$thumb}" style='width:47px!important; height:47px!important;'></a></li>
                   		{/foreach}
                   </ul> 
                   
                   {/if}
                   <div class="trustmeterleft">
<!--Trust Meter Start -->
     <div class="tmtitle">
     <div class="pieContainer">
     <div class="pieBackground"></div>
     <div id="pieSlice1" class="hold"><div class="pie"></div></div>
     <div class="persentbg"><p>{if isset($attributes.trust_meter.trust_score)}{$attributes.trust_meter.trust_score}{else}0{/if}%</p></div>
     </div>
     </div>
 <!-- Trust Meter end -->
<p class="trust_title fl">Trust Score</p>
<div class="clb"></div>

                        <ul class="trustmeterlist">
                       
                       <!--      <li style="border-top:1px solid #F0F0F0;"><i class="icon-envelope iconsmall"></i> Email <i class="icon-ok-sign fr tickmarkico" ></i></li> -->
                        {if isset($attributes.trust_meter.fb_connections)}
                            <li><i class="icon-facebook-sign iconsmall fbcolor"></i> <p class="fl">Facebook<br><span class="tmitemtxt">{$attributes.trust_meter.fb_connections} Friends</span></p> <i class="icon-ok-sign fr tickmarkico" ></i></li>
                             {else}
                            <li class="textgray"><i class="icon-facebook-sign iconsmall"></i>  Facebook </li>
                             {/if}
                             
                               {if isset($attributes.trust_meter.linkedin_connections)}
                           <li><i class="icon-linkedin-sign iconsmall incolor"></i> <p class="fl">Linkedin<br><span class="tmitemtxt">{$attributes.trust_meter.linkedin_connections} Connections</span></p> <i class="icon-ok-sign fr tickmarkico" ></i></li>
                           {else}
                            <li class="textgray"><i class="icon-linkedin-sign iconsmall"></i>  Linkedin</li>
                             {/if}
                             
                        {if isset($attributes.trust_meter.id_type)}
                            <li><i class="icon-user iconsmall idcolor"></i> <p class="fl">Photo ID<br><span class="tmitemtxt">{$attributes.trust_meter.id_type}</span></p> <i class="icon-ok-sign fr tickmarkico" ></i></li>
                           {else}
                            <li class="textgray"><i class="icon-user iconsmall"></i>  Photo ID </li>
                             {/if}
                       
                       
                          {if isset($attributes.trust_meter.mobile_number)}
                            <li><i class="icon-phone iconsmall phcolor"></i> <p class="fl">Phone No<br><span class="tmitemtxt">{$attributes.trust_meter.mobile_number}</span></p> <i class="icon-ok-sign fr tickmarkico" ></i></li>
                            {else}
                            <li class="textgray"><i class="icon-phone iconsmall"></i>  Phone No </li>
                            {/if}
                         
                          {if isset($attributes.trust_meter.address_verified_on)}
                            <li ><i class="icon-home iconsmall adcolor"></i> <p class="fl">Address<br><span class="tmitemtxt">Date Verified: {$attributes.trust_meter.address_verified_on} </span></p> <i class="icon-ok-sign fr tickmarkico" ></i></li>
                            {else}
                            <li class="textgray"><i class="icon-home iconsmall"></i> Address</li>
                            {/if}
                            
                                                    
                           {if isset($attributes.trust_meter.employment_verified_on)}
                           <li ><i class="icon-briefcase iconsmall emcolor"></i> <p class="fl">Employment<br><span class="tmitemtxt">Date Verified: {$attributes.trust_meter.employment_verified_on} </span></p> <i class="icon-ok-sign fr tickmarkico" ></i></li>
                              {else}
                            <li class="textgray"><i class="icon-briefcase iconsmall"></i> Employment</li>
                            {/if}
                            
                           
                           
                        </ul>
                   </div> 
                    {if !isset($smarty.get.me)}
                   <div class="trustmeterleft mbmut" id="mutual">
                        <p class="trust_title" id="mutualConnDetails"><i class="icon-facebook-sign iconstyle"></i> Mutual Connections</p>
                                 {if isset($attributes.mutual_connections)}
               
                        <ul class="mutualcon" id="mutfbcon">
                        {foreach from=$attributes.mutual_connections item=mConn}
                            <li><img src="{$mConn.picture.data.url}"><p>{$mConn.first_name}</p></li>
                            
                            <!--
                            <li><img src="{$cdnurl}/images/dashboard/profile/profile2.jpg"></li>
                            <li><img src="{$cdnurl}/images/dashboard/profile/profile3.jpg"></li>
                            <li><img src="{$cdnurl}/images/dashboard/profile/profile1.jpg"></li>
                            <li><img src="{$cdnurl}/images/dashboard/profile/profile2.jpg"></li>
                            <li><img src="{$cdnurl}/images/dashboard/profile/profile3.jpg"></li>
                        -->
                        {/foreach}
                        </ul>	
                          {if $attributes.mutual_connections_count > 4}
                        <p class="rtviewall" id="vall"><a href="javascript:void(0)" onClick="fbmutcon()">View All</a></p>
                 {/if}
                    {else}
                    <div id="fbmcLoad">
                    {if $profileStatus eq 'suspended' || $myStatus eq 'suspended'}
                    <div class = "mcount1">
                    {else}
                    <div class = "mcount">
                    {/if}
                    {if isset($attributes.mutual_connections_count)}
              			{$attributes.mutual_connections_count} 
              			{else}No	
              			{/if} mutual connections
              			</div>
                       </div>
              			{/if}
                       </div>
                       {/if}
                </div>
                <div class="rightpane mt20">    	
                    <div class="clearfix">
                    	<div class="name-socialicon">
                        <div class="personname {if $msg_button eq 'grey' && !isset($reject_div) && $myStatus neq 'suspended' && $profileStatus neq 'suspended' && !isset($smarty.get.me)}verifyPopup {/if}" name = "{$attributes.name.fname}"><span class='fname fl'>{$attributes.name.fname} </span> ,&nbsp;<span>{$attributes.demo.age} Yrs.</span></div>
                        {if !isset($smarty.get.me)}
                        <ul class="profile_socialicon" >
                       
                            {if isset($reject_div) || $myStatus eq 'suspended' || $profileStatus eq 'suspended'}
                            <li class="deactiveicon" ><span class="pmessage">Message</span></li>
                             {else if $msg_button eq 'grey'}
                              <li class='verifyPopup'><span class="pmessage">Message</span></li>
                            {else if isset($is_locked)}
                            <li  id="lockedMsg"><span class="pmsglock">Locked Message</span></li>
                            {else if $msg_button eq 'green'}
                              <li class="actiontaken" ><a href="{$link_msg}"><span class="pmessage">Message</span></a></li>
                             
                              
                              {else}
                           <li><a href="{$link_msg}"><span class="pmessage">Message</span></a></li>
                           {/if}

                            {if isset($reject_div) || $myStatus eq 'suspended' || $profileStatus eq 'suspended'}
                              <li class="deactiveicon" ><span class="pthumb">Thumb</span></li>
                               {else if $like_button eq 'grey'}
                              <li class='verifyPopup'><span class="pthumb">Thumb</span></li>
                           {else if isset($double_like_button)}
                           <li class="actiontaken" ><span class="pmuthumb">mThumb</span></li>
                           
                             {else if $like_button eq 'green'}
                              <li class="actiontaken" ><span class="pthumb">Thumb</span></li>
                             
                             
                              {else}
                               <li  class="like_profile" id="{$link_like}"><span class="pthumb">Thumb</span></li>
                             {/if} 
               
                            
                           <!-- {if $fav_button eq 'grey'}
                            <li class="deactiveicon"><span class="pstar">Star</span></li>
                             {else if $fav_button eq 'green'}
                              <li class="actiontaken" ><span class="pstar">Star</span></li>
                            {else}
                            <li class="fav_profile" id="{$link_fav}"><span class="pstar">Star</span></li>
                            {/if} --><!--
                            {if isset($double_like_button) || ($like_button eq 'green')}
                              <li class='deactiveicon'><span class="pcancel">Cancel</span></li>
                             {else if $hide_button eq 'grey'}
                              <li class='verifyPopup'><span class="pcancel">Cancel</span></li>
                              {else if $hide_button eq 'green'}
                              <li class="actiontaken" ><span class="pcancel">Cancel</span></li>
                            {else}
                 			 <li class="hide_profile" id="{$link_hide}"><span class="pcancel">Cancel</span></li>
                 			 {/if}
                            --><!--
                              <li class="deactiveicon"><span class="pstar">Star</span></a></li>
                 			 <li class="deactiveicon"><span class="pcancel">Cancel</span></a></li>
                 			
                        --></ul>
                        {/if}
                  <!--      <div class="infocontainer" style="top:70px; right:1px;"><img src="{$cdnurl}/images/dashboard/profile/arrow_topdir.png" />To use this service you have to spent <strong>10 credit points</strong>. You have no credit points in your wallet. You need to buy some credit points.<br><a href="#">Buy Credit Points</a></div> -->
                        
                         <p class="lastcon">Last Login: {$attributes.name.last_login} 
                        {if isset($smarty.get.me)}
                        <a href="javascript:void(0);" onClick="document.getElementById('diffframe2').style.display='block';" class="dactivelink">Deactivate Profile</a>
                        {/if}
                        </p>
                        </div><!--
                        <h2 class="about-title">About Us</h2>
                        <p class="aboutprofiler">I’m an extrovert, love company and just can’t keep myself away from people. I like to go around places and meet new people! I’m a complete social butterfly!!</p>
                        
                        --><div class="myfamilysec">
                    <ul>
                    <!-- <li><p>Age</p><span>{$attributes.demo.age} Yrs.</span></li> -->
                    {if isset($attributes.trait.height)}
                    <li><span>
                    {$attributes.trait.height}</span></li>
                    {/if}
                   
                   {if isset($attributes.demo.marital_status)}
                    <li><span>
                    {$attributes.demo.marital_status}</span></li>
                   {/if}
                    
                 {if isset($attributes.family.no_of_children)}   <li><span> {$attributes.family.no_of_children} </span></li>{/if}
                   
                          {if isset($attributes.demo.religion)}        
                    <li><span>{$attributes.demo.religion} 
                    {if isset($attributes.demo.caste)}({$attributes.demo.caste}){/if}</span></li>{/if}
                   
                    {if isset($attributes.languages.mother_tongue)}
                     <li><span>
                     {$attributes.languages.mother_tongue}
                     </span></li>{/if}
                     
                     <li><span>
                    {if !isset($attributes.demo.stay_city_display) && !isset($attributes.demo.state) && !isset($attributes.demo.country)}
                    --
                    {else}
                    {$attributes.demo.stay_city_display}
                        {if isset($attributes.demo.state)}
                   		 ({$attributes.demo.state})
                    {/if}
                   
                    {/if}</span></li>
                    
                                      {if isset($attributes.trait.body_type)}   <li><span>{$attributes.trait.body_type}</span></li>    {/if}
                                      {if isset($attributes.trait.skin_tone)}   <li><span>{$attributes.trait.skin_tone}</span></li>    {/if}
                    
                    {if isset($attributes.work.income_start) && isset($attributes.work.income_end)}
                  <li><span>
                  {if $attributes.work.income_end eq '0 LPA' && $attributes.work.income_start eq '0 LPA'}
                  Not Earning
                  {else}
                    earns Rs. {$attributes.work.income_start} - {$attributes.work.income_end}
                    {/if}  
                     </span></li>                    
                     {/if}
                   
                        {if isset($attributes.family.family_status)}  
                       <li><span>{$attributes.family.family_status} Class </span></li>
                                     {/if} 
                     
                     
                 {if isset($attributes.family.manglik_status)}   <li><span>{$attributes.family.manglik_status}</span></li>{/if}
                    
                 {if isset($attributes.trait.disability_type)}   <li><span> {$attributes.trait.disability_type} </span></li>{/if}
                     
              
                   
                    <!--
                    
                     <li><span>
                    {if !isset($attributes.demo.birth_city) && !isset($attributes.demo.birth_state) && !isset($attributes.demo.birth_country)}
                    --
                    {else}
                    {$attributes.demo.birth_city}
                        {if isset($attributes.demo.birth_state)}
                    ( {$attributes.demo.birth_state}
                    {/if}
                    {if isset($attributes.demo.birth_country)}
                    , {$attributes.demo.birth_country} )
                    {else}
                    )
                    {/if}
                    {/if}</span></li>
                    
                      --> 
                     
                     
                     
                  
                     
                    </ul>
                    </div>
                    <div class="clb"></div>
                   
                     <ul class="education-work-salary mt20 clearfix">
                            <li>
                                <div class="proedu_title">Education<!--<div class="secexpendbtn" onClick="expndsec()" id="plusicon" style="display:block;"><p>+</p></div><div class="secexpendbtn" onClick="closec()" id="minicon" style="display:none;"><p style=" left:6px; top:-6px; position:absolute;">&ndash;</p></div> --></div>
                                <div class="boxedu">
                                
                                
                                    <div class="edubox">
                                    
                                    <ul class="pdpnkdot">
                                     {if $attributes.education.1.education neq null}
                                    <div class="pdewbtmline top0"><img src="images/dashboard/profile/dot.png" width="9"></div>
                                    {/if}
                                    <div class="pdewbtmline btm0"></div>
                                    <li style="width:13px;"><img src="images/dashboard/profile/pdpnk_dot.png"/></li>
                                    <li style="width:15px;"><hr noshade size="1" color="#e7276b" class="mt20"></li>
                                    </ul><p> {if isset($attributes.highest_education.education)}
                                  {$attributes.highest_education.education}
                                    {/if}
                                        {if !empty($attributes.highest_education.specialization)}
                                 <br><strong>{$attributes.highest_education.specialization}</strong>
                                    {/if}
                                 
                                    {if !empty($attributes.highest_education.institute)}
                                    <br><strong style="color:#333; font-family:'RobotoLight';">{$attributes.highest_education.institute}</strong>
                                    {/if}
                                 
                                </p>
                                  
                                    
                                   
                                    
                                    {assign var=last value=$attributes.education|@end}     
                                   {foreach from=$attributes.education item=otherEd} 
                                   {if isset($otherEd.education)}
                                   <div class="clb"></div>
                                    <ul class="pdpnkdot">
                                    {if $last.education neq $otherEd.education}
                                    <div class="pdewbtmline top0"><img src="images/dashboard/profile/dot.png" width="9"></div>
                                   {/if}
                                    <div class="pdewbtmline btm0"><img src="images/dashboard/profile/flow_indi.png"></div>
                                    <li style="width:13px;"><img src="images/dashboard/profile/pdpnk_dot.png"/></li>
                                    <li style="width:15px;"><hr noshade size="1" color="#e7276b" class="mt20"></li></ul><p> 
                                    {$otherEd.education} 
                                    {/if}
                                  
                                    
                                     {if !empty($otherEd.specialization)}
                                  <br><strong>{$otherEd.specialization}</strong>
                                    {/if}
                                    {if !empty($otherEd.institute)}
                                    <br><strong style="color:#333; font-family:'RobotoLight';">{$otherEd.institute}</strong>
                                    {/if}</p>
                                    {/foreach}
                                    </div>
                                    
                                </div>
                            </li>
                            
                            <li>
                             {assign var=lastWork value=$attributes.industries|@end} 
                                <div class="prowork_title">Work</div>
                                <div  class="boxedu">
                               
                               <!-- <div class="secexpendbtn">+</div> -->
                                    <div class="edubox">
                                    {if !isset($lastWork.industries)}
                                   <div class="clb"></div> 
                                 <ul class="pdpnkdot">
                                   <li style="width:13px;"><img src="images/dashboard/profile/pdylo_dot.png"/></li>
                                 <li style="width:15px;"><hr noshade size="1" color="#ff9b00" class="mt20"></li></ul>
                                 <p> {$attributes.work.working_area}</p>
                                  
                                  {else}
                                     
                                      
                                  {foreach from=$attributes.industries item=otherEd}
                                 {if isset($otherEd.companies) ||  isset($otherEd.industries)} 
                                 <div class="clb"></div> 
                                 <ul class="pdpnkdot">
                                 {if ($lastWork.industries neq $otherEd.industries) || ($lastWork.companies neq $otherEd.companies)}
                                 <div class="pdewbtmline top0"><img src="images/dashboard/profile/dot.png" width="9"></div>
                                 {/if}
                                    <div class="pdewbtmline btm0">
                                    {if $attributes.industries.0.industries neq $otherEd.industries }
                                    <img src="images/dashboard/profile/flow_indi.png">
                                    {/if}
                                    </div>
                                 <li style="width:13px;"><img src="images/dashboard/profile/pdylo_dot.png"/></li>
                                 <li style="width:15px;"><hr noshade size="1" color="#ff9b00" class="mt20"></li></ul>
                                 
                                  <p>
                                   {if  isset($otherEd.industries)}
									{$otherEd.industries}
                                  {/if}
                                  
                                   {if isset($attributes.work.working_area) && $attributes.industries.0.industries eq $otherEd.industries}
                                  <!--   {$attributes.work.designation},<br> -->
                               <span>({$attributes.work.working_area})</span>
                                  {/if}
                                  
                                  {if  isset($otherEd.designations)}
                               <br><strong>{$otherEd.designations}</strong>{/if}
                                  
                                  {if  isset($otherEd.companies)}<br><strong style="color:#333; font-family:'RobotoLight';">{$otherEd.companies}</strong>
                                  {/if}
                                  
                                  </p> {/if}
                                  {/foreach}
                                  {/if}
                                  
                                  <!--
                                  {if !isset($otherEd.industries) && !isset($otherEd.industries) }
                                   <p> Not Mentioned</p>
                                    {/if}
                                    
                                 -->
                                  
                                    
                                  </div>
                                    
                                </div>
                            </li>
                           <!-- <li>
                                <div class="bottom_titlesed clearfix"><img src="{$cdnurl}/images/dashboard/profile/salary.png" class="imgicon">Salary</div>
                                <div class="alldetails">
                                    <p class="salary-rupees">Rs. {$attributes.work.income_start} - {$attributes.work.income_end} <span> LPA</span></p>
                                </div>
                            </li> -->
                        </ul>
                        
                    </div>
                 
                    
                     
                    <ul class="threesections clearfix">
                    		<li class="mapsecframe">
                            <p class="trust_title clb"><i class="icon-map-marker iconstyle"></i> Location</p>
                        	<div class="pdlocationsec">
                                 <p class="trust_title fl" onClick="expndres()" id="tobtn">Current<br><img src="images/dashboard/profile/indi_btm.png" id="curindi" style="display:block; margin:0 auto;"></p><p class="trust_title fl" onClick="expndbp()" style="background:#4293F0; color:#fff;" id="frmbtn">From<br><img src="images/dashboard/profile/indi_btm.png" id="brtindi" style="display:none; margin:0 auto;"></p>
                              <div class="locmapsec clb" id="resplace" style="display:block;" ><p style="width: 100%; padding: 0px;">
                              {if isset($attributes.demo.stay_city_display)}{$attributes.demo.stay_city_display} {/if}
                              {if isset($attributes.demo.state)}, {$attributes.demo.state}{/if}
                              {if isset($attributes.demo.country)}, {$attributes.demo.country} {/if}
                               
                               </p>
        						<iframe src='{$baseurl}/google_maps.php?type=stay&pid={$profile_id}' frameborder="0" height="200px;" width="285px;" scrolling="no" style="margin-left: -14px;"> 
        						</iframe>  
    
                               
                                 </div>                 


                              <div class="locmapsec clb" id="birthplace" style="display:none;" > <p style="width: 100%; padding: 0px;">
                               {if isset($attributes.demo.birth_city_display)}{$attributes.demo.birth_city_display} {/if}
                               {if isset($attributes.demo.birth_state)}, {$attributes.demo.birth_state}{/if}
                              {if isset($attributes.demo.birth_country)}, {$attributes.demo.birth_country} {/if}
                              </p>
                                <iframe src='{$baseurl}/google_maps.php?type=birth&pid={$profile_id}' frameborder="0" height="200px;" width="285px;" scrolling="no" style="margin-left: -14px;"> 
        						</iframe>  </div>    

                            </div>
                           
                            
                        </li>

  
                    	<!--<li>
                        	<div class="trustmeter">
                                <p class="trust_title"><img src="{$cdnurl}/images/dashboard/profile/languages_ic.png" class="iconstyle"> Languages | Speak</p>
                                <ul class="languages">
                                {foreach from=$attributes.languages item=lang}
                                <li>{$lang}</li>
                                {/foreach}
                                </ul>
                            </div>
                        </li> -->
                        <li>
                         <p class="trust_title clb">Lifestyle</p>
                        	<div class="trustmeter">
                            
                                <ul class="qualities">
                                    <li><span class="nonveg"></span>
                                    {if isset($attributes.trait.food_status)}
                                    {$attributes.trait.food_status}
                                    {else}
                                    Doesn't Matter
                                    {/if}</li>
                                    <li><span class="occ"></span>
                                    {if isset($attributes.trait.drinking_status)}
                                    {$attributes.trait.drinking_status}
                                    {else}
                                    Doesn't Matter
                                    {/if}
                                    </li>
                                    <li><span class="never"></span>
                                    {if isset($attributes.trait.smoking_status)}
                                    {$attributes.trait.smoking_status} 
                                    {else}
                                    Doesn't Matter
                                    {/if}
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    
                    
                    
  </div>
            </li>
            <li style="display:inline-block; overflow:hidden;">
                <!--Carousel-->
             {include file='templates/profileInterests.tpl'}
            </li>
            <li>
            	 {include file='templates/profilePartnerPreferences.tpl'}
            </li>
       
       
       
       
        </ul>
        </div>
    </div><!--
    
    <div class="footer clearfix">
    	<div class="container footercontent">
        	<div class="clearfix">
                <div class="copyfootermenu">
                    <div class="copyright">2013 &copy;trulymadly.com</div>
                    <nav class="footernav">
                    	<a href="#">About</a> <a href="#">Privacy Policy</a> <a href="#">Terms to Use</a> <a href="#">Disclaimers</a> <a href="#">Contact Us</a>
                    </nav>
                </div>
                <div class="socialicons">
                    <ul class="clearfix">
                        <li class="join">Join us on:</li>
                        <li class="fbicon"><a href="#"><i class="icon-facebook"></i></a></li>
                        <li class="fbicon"><a href="#"><i class="icon-linkedin"></i></a></li>
                    </ul>
                </div>
			</div>
        </div>
    </div>
--></div>



<div id="diffframe2" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="sndmpopupframe">
<a href="javascript:void(0);" onClick="document.getElementById('diffframe2').style.display='none';" class="fr clb"><img src="{$cdnurl}/images/register/close.png" class="clsbtn"></a>
<div class="tbinfosec4txt formbox clb">
    
    {if !isset($taskDone)}
   <form action="{$baseurl}/logging/deactivate.php" id="form1" method="post" name="lg-form" novalidate='novalidate'>
 Are you sure you want to deactivate your profile.
 <div class="clb"></div>
	<select id="profileDeletionSelect" name="reason" onChange="deactForm()" class="ddlist clb" >
    <option selected="selected" disabled="disabled" value="0">Please select a reason</option>
<option>Not getting enough profiles</option>
<option>Don't like any of the profiles </option>
<option>I do not understand how this works</option>
<option>Was just testing, not really looking </option>
<option>I have found someone!</option>
<option>Other</option>
        </select>
<textarea id="reasonDesc" name="reason_desc" style="display:none;" class="ddlist clb" placeholder="Type Reason"></textarea>
                  
    <div class="clb"></div>
    <input type="submit" value="Deactivate" id='deactivateSubmit' class="subtndeact">
    <div class="clb"></div>
   
   </form>
{/if}
    <p>{$msg}</p>
    </div>
</div></div>


<script src="{$cdnurl}/js/dashboard/jquerypp.custom.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.elastislide.js"></script>
<script type="text/javascript">
	var current = 0,
		$preview = $( '#preview' ),
		$carouselEl = $( '#carousel' ),
		$carouselItems = $carouselEl.children(),
		carousel = $carouselEl.elastislide( {
			current : current,
			minItems : 4,
			onClick : function( el, pos, evt ) {
				changeImage( el, pos );
				evt.preventDefault();
			},
			onReady : function() {
				changeImage( $carouselItems.eq( current ), current );
			}
		} );

	function changeImage( el, pos ) {
		$preview.attr( 'src', el.data( 'preview' ) );
		$carouselItems.removeClass( 'current-img' );
		el.addClass( 'current-img' );
		carousel.setCurrent( pos );
	}
</script>

<script>
$('#circle').hover(function(){
	//var id=$('#circle').text();
	var id="1 kggjg fsfff";
    var textLength = id.length;
	//alert(textLength);
    var textWidth = parseInt($('#circle').css('font-size'), 7) * textLength + 'px';
    $('#circle').css('width', textWidth);
    $('#circle').css('height', textWidth);
	$('#circle').text(id);
}
,function(){
	var id="on hover";
	//var id=$('#circle').text();
    var textLength = id.length;
	//alert(textLength);
    var textWidth = parseInt($('#circle').css('font-size'), 8) * textLength + 'px';
	//alert(textWidth);
    $('#circle').css('width', textWidth);
    $('#circle').css('height', textWidth);
	$('#circle').text(id);
});
</script>

<script>
function expndsec()
{
document.getElementById("otrqlfy").style.display="block";
document.getElementById("plusicon").style.display="none";
document.getElementById("minicon").style.display="block";
}
function closec()
{
document.getElementById("otrqlfy").style.display="none";
document.getElementById("plusicon").style.display="block";
document.getElementById("minicon").style.display="none";
}


function expndbp()
{
document.getElementById("resplace").style.display="none";
document.getElementById("birthplace").style.display="block";
document.getElementById("brtindi").style.display="block";
document.getElementById("curindi").style.display="none";
document.getElementById("frmbtn").style.background="#fff";
document.getElementById("frmbtn").style.color="#333";
document.getElementById("tobtn").style.background="#4293F0";
document.getElementById("tobtn").style.color="#fff";
}

function expndres()
{
document.getElementById("resplace").style.display="block";
document.getElementById("birthplace").style.display="none";
document.getElementById("brtindi").style.display="none";
document.getElementById("curindi").style.display="block";
document.getElementById("frmbtn").style.background="#4293F0";
document.getElementById("frmbtn").style.color="#fff";
document.getElementById("tobtn").style.background="#fff";
document.getElementById("tobtn").style.color="#333";
}

function fbmutcon()
{
document.getElementById("mutfbcon").style.height="auto";
document.getElementById("vall").style.display="none";
}

</script>

<script type="text/javascript">
function deactForm(){
	var option1 =  $("#profileDeletionSelect option:selected").text();
	if(option1 == 'Other'){
		$("#reasonDesc").css("display", "block");
	}
	else{
		$("#reasonDesc").css("display", "none");
	}
}
$(document).ready(function(){
	$('#phide').click(function() {	
		 if($('#phide').is(':checked')) {
			 $('#profileDeletionSelect').css("display", "block");
		 }
	});
	$('#deactivateSubmit').click(function(event){
		if($("#profileDeletionSelect option:selected").val() == 0){
			event.preventDefault();
		}
	})
});

</script>


</body>
</html>


 
  