<!DOCTYPE html>
<html lang="en">
<head>
    <title>Deals List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {include file='./deals/adminIncludes.tpl'}
    <script src="{$baseurl}/js/videos/videoList.js"></script>
</head>
<body>

{include file='./deals/header.tpl'}

<div class="container-fluid check-me">
    <table class="table table-responsive" id="datespot-list">
        <thead>
        <tr>
            <th>Time of Saving</th>
            <th>Video ID</th>
            <th>UserID</th>
            <th>Play Video</th>
            <th>Current Status</th>
            <th>Approve</th>
            <th>Reject</th>
            <th>Google Status</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$videos item=video}
            <tr class="success">
                <td> {$video.time_of_saving}</td>
                <td> {$video.video_id}</td>
                <td><a target="_blank" href="{$baseurl}/admin/adminpanel.php?search_by=UserId&value={$video.user_id}"> {$video.user_id}</td>

                <td><a href="#" data-video-url="{$video.url}" data-toggle="modal" data-target=".confirm_box" class="play-action" data-action="play"><span>Play</span></a></td>
                <td> {$video.tm_approved}</td>

                <td>
                <a href="#" data-video-action="true" data-post-url="{$baseurl}/videos/adminVideos.php" onclick="updateVideoStatus('{$baseurl}/videos/adminVideos.php','true',{$video.video_id}, this,false)"  data-video-id="{$video.video_id}" class="video-activate" title="Approve" >
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                </a>

                </td>
                <td>
                    <a href="#" data-video-action="true" data-post-url="{$baseurl}/videos/adminVideos.php" onclick="updateVideoStatus('{$baseurl}/videos/adminVideos.php','false',{$video.video_id},this,false)"  data-video-id="{$video.video_id}" class="video-activate" title="Reject" >
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </a>

                </td>
                <td>


                    <span><span id="video_vision_{$video.video_id}"> {$video.is_approved} </span>
                    <a href="#" data-video-action="true" data-vision-status="{$video.is_approved}" data-post-url="{$baseurl}/videos/adminVideos.php" onclick="updateVideoStatus('{$baseurl}/videos/adminVideos.php','{$video.is_approved}',{$video.video_id},this,true)"  data-video-id="{$video.video_id}" class="video-activate" title="Change Google" >
                        <span class="glyphicon glyphicon-saved" aria-hidden="true"></span>
                    </a>
                    </span>

                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>

    <a href="#" id="next_video" data-video-count="{$video_count}" data-set-visibility="{$has_more}" onclick="HandleNext('{$baseurl}')"  class="video-next" title="Next Page" >
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    </a>
</div>


<div class="modal fade confirm_box" role="dialog" aria-labelledby="mySmallModalLabel" data-width="500" aria-hidden="true" data-keyboard="false" >

        <div class="modal-body modal-sm">

            <video controls src="">
                <source src="" id="video-url-curr">
            </video>
        </div>
</div>

<script >


    if(getParameterByName('action')!='no_action') {
        $(document).ready(function () {


            $('#datespot-list').DataTable();
            $('#next_video').hide();

        });


    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }



</script>
</body>
</html>
