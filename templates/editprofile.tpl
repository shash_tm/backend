<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Edit Profile</title>
<script>
var time = new Date();
</script>
<link href="{$cdnurl}/css/editprofile.css?v=2.1" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css?v=4.1" rel="stylesheet" type="text/css">

<script>
var baseurl='{$baseurl}';
{if $edit eq 1}
var edit = true;
{/if}
{if isset($status)}
var status = '{$status}';
{/if}
{if isset($user_data)}
var user_data = '{$user_data}';
user_data = JSON.parse(user_data);
var uid = user_data.user_id;
{/if}
var cdnurl = '{$cdnurl}';
var dob = user_data['DateOfBirth'].split("-");
var  fb_dob  = {}; 
fb_dob['fb_date']=dob[2];
fb_dob['fb_month']=dob[1];
fb_dob['fb_year']=dob[0];

var fb_work = {};
fb_work['designation']=user_data.designation;
fb_work['companies']=user_data.company_name;
var fb_institute = user_data.institute_details;
var relationship_status = user_data.marital_status_new;

</script>

</head>

<body>
{include file='templates/common/header.tpl'}

<div class="new_wrapper editprobg">

<!--Centered Container Start -->
<div class="new_container" align="center">

<div class="regprogress"><h2>Edit Profile</h2></div>

<!--Edit Profile Start -->
<div class="editpropanel">
	<div class="edpwraper">
    
    
<ul id="basicd" class="editpro plus">
    <li class="fl"><p class="fl">DOWN TO BASICS</p></li>
    <li class="fr">
    	<i id="basicd-minus" class="icon-minus difficon fr" style="display:none;"></i>
        <i id="basicd-plus" class="icon-plus difficon fr" style="display:block;"></i>
    </li>
</ul>
<div class="edpbelowtile" id="basicd-below" style="display:none;">
	<div class="epform" align="center">
    <p>Name<i class="icon-lock lockinfo hide"></i></p>
	<input name="fname" id="fname" type="text" tabindex="2" class="regtxtbox">
   <!-- <p>Last Name<i class="icon-lock lockinfo hide"></i></p>
	<input name="lname" id="lname" type="text" tabindex="3" class="regtxtbox" placeholder="Enter Last Name"> -->
    <p>Date of birth<i class="icon-lock lockinfo hide"></i></p>
    <ul>
		<li class="dobform fl">
        <div class="regselect-style regselddbg1">
        <select name="dd" class="regselwidth1" id="whenBornDay">
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
		    <option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option>
            <option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
		</select>
        </div>
        </li>
        <li class="dobform padl1 fl">
        <div class="regselect-style regselddbg1">
        <select name="mm" class="regselwidth1" id="whenBornMonth">
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
		</select>
        </div>
        </li>
        <li class="dobform fr">
        <div class="regselect-style regselddbg1">
        <select name="yy" class="regselwidth1" id="whenBornYear">
            <option value="" selected>Year</option>
		</select>
        </div>
        </li>
        </ul>
        
         <p id='whenBorn' style="color:red" class="errtxt"></p>
<!-- <p>Country</p>
        <div class="regselect-style">
          <select name="Country" id="stayCountry">
          </select>
        </div>
		<p id='stayCountryError' style="color:red;" class="errtxt"></p> -->
        <p>State</p>
        <div class="regselect-style">
          <select name="State" id="stayState">
          </select>
        </div>
   		<p id='stayStateError' style="color:red;" class="errtxt"></p>
        <p>City</p>
        <div class="regselect-style">
          <img id="loader" class="regloader hide" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="18px">
          <select name="City" id="stayCity">
          </select>
        </div>
		<p id='stayCityError' style="color:red;" class="errtxt"></p>
        
        <p>Height</p>
        <ul>
		<li class="fl mrg0">
        <div class="regselect-style regselddbg2">
        <select name="feet" id="feet" class="regselwidth2">
            <option value="" disabled selected>Feet</option>
            <option value="4">04</option>
            <option value="5">05</option>
            <option value="6">06</option>
            <option value="7">07</option>
            <option value="8">08</option>
		</select>
        </div>
        </li>
        <li class="fr mrg0">
        <div class="regselect-style regselddbg2">
        <select name="inches" id="inch" class="regselwidth2">
            <option value="" disabled selected>Inches</option>
            <option value="0">00</option>
            <option value="1">01</option>
            <option value="2">02</option>
            <option value="3">03</option>
            <option value="4">04</option>
            <option value="5">05</option>
            <option value="6">06</option>
            <option value="7">07</option>
            <option value="8">08</option>
            <option value="9">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
		</select>
        </div>
        </li>
        </ul>
        <p id='heightError' style="color:red;" class="errtxt mt5"></p>
		
		<p>Relationship status</p>
       	<ul>
		<li class="fl"><input class="cutomradio" type="radio" name="marital_status" value="Never Married"><label>Never Married</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="marital_status" value="Married Before"><label>Married Before</label></li>
        <!--<li class="fr"><input class="cutomradio" type="radio" name="marital_status" value="Separated"><label>Separated</label> </li>-->
        </ul>
       <!-- <ul>
        <li class="fl"><input class="cutomradio" type="radio" name="marital_status" value="Divorced"><label>Divorced</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="marital_status" value="Widowed"><label>Widowed</label></li>
        </ul>
        <p id='marital_Error' style="color:red;" class="errtxt mt5"></p>-->
     <div id='haveChildrenDiv'>
        <p>With children</p>
       <ul>
		<li class="fl"><input class="cutomradio" type="radio" name="haveChildren" value="yes"><label>Yes</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="haveChildren" value="no"><label>No</label></li>
        </ul>
        <p id='haveChildren_Error' style="color:red;" class="errtxt mt5"></p>
      <!--  <a class="edpactionbtn"><i class="icon-save"></i>&nbsp; Save</a>-->
     </div>
		<a id="activeBirth" onclick="onContinue(0)" class="edpactionbtn"><i class="icon-save"></i>&nbsp; Save</a>
		<a id="inactiveBirth" class="edpdeactbtn" style="display:none;"><i class="icon-save"></i>&nbsp; Save</a>
	</div>
</div>
    
    <ul id="inthob" class="editpro mt10 plus">
    <li class="fl"><p class="fl">TICKLE YOUR FANCY</p></li>
    <li class="fr">
    	<i id="inthob-minus" class="icon-minus difficon fr" style="display:none;"></i>
        <i id="inthob-plus" class="icon-plus difficon fr" style="display:block;"></i>
    </li>
</ul>
<div class="edpbelowtile" id="inthob-below" style="display:none">
	<div class="epintfrm" align="center">
    <p>Describe yourself in 5 hashtags:</p>
<ul>
				<li><input id="interest1" type="checkbox" class = "cutomradio" name="interest" value="#MovieManiac"><label>#MovieManiac</label></li>
				<li><input id="interest2" type="checkbox" class = "cutomradio" name="interest" value="#TVAddict"><label>#TVAddict</label></li>
				<li><input id="interest3" type="checkbox" class = "cutomradio" name="interest" value="#BollywoodBuff"><label>#BollywoodBuff</label></li>
				<li><input id="interest4" type="checkbox" class = "cutomradio" name="interest" value="#AvidTraveller"><label>#AvidTraveller</label></li>
				<li><input id="interest5" type="checkbox" class = "cutomradio" name="interest" value="#BeachBum"><label>#BeachBum</label></li>
				<li><input id="interest6" type="checkbox" class = "cutomradio" name="interest" value="#NatureSeeker"><label>#NatureSeeker</label></li>
				<li><input id="interest7" type="checkbox" class = "cutomradio" name="interest" value="#MusicSnob"><label>#MusicSnob</label></li>
				<li><input id="interest8" type="checkbox" class = "cutomradio" name="interest" value="#BookWorm"><label>#BookWorm</label></li>
				<li><input id="interest9" type="checkbox" class = "cutomradio" name="interest" value="#SportsFanatic"><label>#SportsFanatic</label></li>
				<li><input id="interest10" type="checkbox" class = "cutomradio" name="interest" value="#Foodie"><label>#Foodie</label></li>
				<li><input id="interest11" type="checkbox" class = "cutomradio" name="interest" value="#HealthFreak"><label>#HealthFreak</label></li>
				<li><input id="interest12" type="checkbox" class = "cutomradio" name="interest" value="#Artsy"><label>#Artsy</label></li>
				<li><input id="interest13" type="checkbox" class = "cutomradio" name="interest" value="#AnimalLover"><label>#AnimalLover</label></li>
				<li><input id="interest14" type="checkbox" class = "cutomradio" name="interest" value="#AdrenalineJunkie"><label>#AdrenalineJunkie</label></li>
				<li><input id="interest15" type="checkbox" class = "cutomradio" name="interest" value="#Scholar"><label>#Scholar</label></li>
				<li><input id="interest16" type="checkbox" class = "cutomradio" name="interest" value="#PartyHopper"><label>#PartyHopper</label></li>
				<li><input id="interest17" type="checkbox" class = "cutomradio" name="interest" value="#Homebody"><label>#Homebody</label></li>
				<li><input id="interest18" type="checkbox" class = "cutomradio" name="interest" value="#Trendy"><label>#Trendy</label></li>
				<li><input id="interest19" type="checkbox" class = "cutomradio" name="interest" value="#EDMPundit"><label>#EDMPundit</label></li>
				<li><input id="interest20" type="checkbox" class = "cutomradio" name="interest" value="#RealityShowAddict"><label>#RealityShowAddict</label></li>
				<li><input id="interest21" type="checkbox" class = "cutomradio" name="interest" value="#CricketFan"><label>#CricketFan</label></li>
				<li><input id="interest22" type="checkbox" class = "cutomradio" name="interest" value="#FootballCrazy"><label>#FootballCrazy</label></li>
				<li><input id="interest23" type="checkbox" class = "cutomradio" name="interest" value="#CoffeeLover"><label>#CoffeeLover</label></li>
				<li><input id="interest24" type="checkbox" class = "cutomradio" name="interest" value="#Chocaholic"><label>#Chocaholic</label></li>
				<li><input id="interest25" type="checkbox" class = "cutomradio" name="interest" value="#NightOwl"><label>#NightOwl</label></li>
				<li><input id="interest26" type="checkbox" class = "cutomradio" name="interest" value="#EarlyBird"><label>#EarlyBird</label></li>
</ul>
                
                <p id='hobby_Error' style="color:red;" class="errtxt mt5"></p>
                <!--<a class="edpactionbtn"><i class="icon-save"></i>&nbsp; Save</a>-->
                <a id="activeInterest" onclick="onContinue(1)" class="edpactionbtn"><i class="icon-save"></i>&nbsp; Save</a>
				<a id="inactiveInterest" class="edpdeactbtn" style="display:none;"><i class="icon-save"></i>&nbsp; Save</a>
    </div>
</div>    

    
 <ul id="workd" class="editpro mt10 plus">
    <li class="fl"><p class="fl">YOUR BREAD AND BUTTER</p></li>
    <li class="fr">
    	<i id="workd-minus" class="icon-minus difficon fr" style="display:none;"></i>
        <i id="workd-plus" class="icon-plus difficon fr" style="display:block;"></i>
    </li>
</ul>
<div class="edpbelowtile" id="workd-below" style="display:none">
	<div class="epform" align="center">
     <p>Industry <span id="industry_optional" style="display:none;">(Optional)</span></p>
        <div class="regselect-style">
        <select name="industry" id="industry">
            <option value="" disabled selected>Select industry</option>
		</select>
        </div>
		<p id='industry_Error' style="color:red" class="errtxt"></p>
    
     <p>Current Designation</p>
        <input name="designation" id="designation" type="text" class="regtxtbox" placeholder="Enter designation">
        <!--<div class="epintfrm" style="width:100%;"><ul><li><input id="working_status" name="working_area" type="checkbox" value="Not Working" class = "cutomradio"><label>Currently not working</label></li></ul></div>-->
        <!--<div class="nowork"><label><input id="working_status" name="working_area" type="checkbox" value="Not Working">&nbsp;Currently not working</label></div>-->
        
       
        <p id='designation_Error' style="color:red" class="errtxt"></p>
        
        
        <p>Companies <span>(Optional)</span></p>
       <div class="comdetails"><a id="addCompany" class="hide addicon">Add</a><input name="companies" type="text" id="companies" class="regtxtbox inputindex" placeholder="Enter most recent first">
       		<ul id="company"></ul>
       </div>
       
        <p>Income <span>(Optional)</span></p>
        <div class="regselect-style">
        <select name="Income" id="income">
            <option value="" disabled selected>Select income</option>
            <option value="NA">Not Applicable</option>
            <option value="0-10">&lt; 10 lakhs</option>
			<option value="10-20">10-20 lakhs</option>
			<option value="20-50">20-50 lakhs</option>
			<option value="50-51">&gt; 50 lakhs</option>
		</select>
        </div>
		<p id='income_Error' style="color:red" class="errtxt"></p>
        <!--<a class="edpactionbtn"><i class="icon-save"></i>&nbsp; Save</a>-->
        <a id="activeWork" onclick="onContinue(2)" class="edpactionbtn"><i class="icon-save"></i>&nbsp; Save</a>
		<a id="inactiveWork" class="edpdeactbtn" style="display:none;"><i class="icon-save"></i>&nbsp; Save</a>
    </div>
</div>        
    
 <ul id="edudet" class="editpro mt10 plus">
    <li class="fl"><p class="fl">HONOUR ROLL</p></li>
    <li class="fr">
    	<i id="edudet-minus" class="icon-minus difficon fr" style="display:none;"></i>
        <i id="edudet-plus" class="icon-plus difficon fr" style="display:block;"></i>
    </li>
</ul>
<div class="edpbelowtile" id="edudet-below" style="display:none">
	<div class="epform" align="center">
    <p>Highest Degree</p>
        <div class="regselect-style">
        <select name="degree" id="education">
            <option value="" disabled selected>Select highest degree</option>
		</select>
        </div>
		<p id='education_Error' style="color:red" class="fs11"></p>
        <p>Colleges/School <span>(Optional)</span></p>
       <div class="comdetails"><a id="addInstitute" class="hide addicon">Add</a><input name="institutes" id="institutes" type="text" class="regtxtbox inputindex" placeholder="Enter most recent first">
       <ul id="institute"></ul>
       </div>
       	<a id="activeEducation" onclick="onContinue(3)" class="edpactionbtn"><i class="icon-save"></i>&nbsp; Save</a>
		<a id="inactiveEducation" class="edpdeactbtn" style="display:none;"><i class="icon-save"></i>&nbsp; Save</a>
    </div>
</div>        
    
    
    </div>
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul>     
</div>
<!--Edit Profile End -->

</div>
</div>
{include file='templates/common/footer.tpl'}
</body>
<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/lib/icheck.js"></script>
<script src="{$cdnurl}/js/lib/function.js?v=3"></script>
<script src="{$cdnurl}/js/editProfile/editprofile.js?v=45"></script>
<script src="{$cdnurl}/js/register/core_register.js?v=2.9"></script>

</html>
