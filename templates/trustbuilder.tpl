<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Build Your TrustScore</title>
<script>
var time = new Date();
var time1 = new Date();
</script>
<link href="{$cdnurl}/css/trustbuilder.css?v=4.2" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/headfooter.css?v=4" rel="stylesheet" type="text/css">

<script>

function loadAsset(src) {
	    if(typeof(window.JSInterface) != 'undefined') {
            s = window.JSInterface.loadAsset(src);
            eval(s);
        }
    }
var uid = '{$user_id}';
var isEndorsed = '{if !empty($endorsementData.data)}1{else}0{/if}';
var fb_api="{$fb_api}";
var cdn = "{$cdnurl}";
var fbScope = "{$facebook_scope}";
var isFbVerified = '{$isFbVerified}';
var fbConnections = '{$fbConnection}';
var server="{$SERVER}";
var baseurl = "{$baseurl}";
var connectedFrom = 'trustbuilder';
var fbTMdiff = '{$fbTmDiff}';
var fbScore = "{$tbNumbers.score.facebook}";
var fbCredits = "{$tbNumbers.credits.facebook}";
var linkedScore = "{$tbNumbers.score.linkedin}";
var linkedCredits = "{$tbNumbers.credits.linkedin}";
var mobileScore = "{$tbNumbers.score.mobile}";
var mobileCredits = "{$tbNumbers.credits.mobile}";
var isphoneVerified = "{$phoneVerified}";
var isLinkedinVerified = '{$linkedinVerified}';
var linkedinConnections = '{$linkedinConnection}';
var linkedinDesignation = '{$linkedinDesignation}';
var trustScore = '{$trustscore}';
var phoneNumber = '{$phoneNumber}';
var no_of_trials = '{$phoneTrials}';
var idProof = '{$idproofType}';
var idProofStatus = '{$idVerified}';
var idReject = '{$idReject}';
var idScore = "{$tbNumbers.score.photoid}";
var isAnyprofilePic = '{$isAnyprofilePic}';
var isempVerified = '{$empVerified}';
var empproofType = '{$empproofType}';
var companyName = '{$companyName}';
var showName = '{$showName}';
var empScore = "{$tbNumbers.score.employment}";
var endorsementScore = "{$trustpoints.endorsement_score}";
var link_api = '{$linkedin_api}';
var fbMessages = new Array();
fbMessages['name_mismatch'] = '{$tbMessages.facebook.name_mismatch}';
fbMessages['age_mismatch'] = '{$tbMessages.facebook.age_mismatch}';
fbMessages['name_age_mismatch'] = '{$tbMessages.facebook.name_age_mismatch}';
var fb = new Array();
var tm = new Array();
fb['name'] = '{$diff.fbName}';
fb['age'] = '{$diff.fbAge}';
tm['name'] = '{$diff.TMname}';
tm['age'] = '{$diff.TMage}';
var idMessages = new Array();
idMessages['name_mismatch'] = '{$tbMessages.id.name_mismatch}';
idMessages['age_mismatch'] = '{$tbMessages.id.age_mismatch}';
idMessages['name_age_mismatch'] = '{$tbMessages.id.name_age_mismatch}';
var id = new Array();
var tm_id = new Array();
id['name'] = '{$id_diff.IDname}';
id['age'] = '{$id_diff.IDage}';
tm_id['name'] = '{$id_diff.TMname}';
tm_id['age'] = '{$id_diff.TMage}';
var from_mobile = '{$from_mobile}';
{if isset($isRegistration) && $isRegistration eq 1}
var isRegistration = true;
{else}
var isRegistration = false;
{/if}
var status = '{$status}';
var gender ="{$gender}";
var thresold = '{$thresold}';
var endorsement_link = "{$endorsementData.link}";
var endorse_count = "{$trustpoints.endorsement_count}";

</script>

<script type="text/javascript" src="//platform.linkedin.com/in.js">
api_key: {$linkedin_api}
onLoad: fetchLinkedIn
</script>

</head>

<body>

<!-- Pop Up Start -->
<div id="tutorial" {if $isRegistration eq 1 }style="display:block;"{else}style="display:none;"{/if} onClick="document.getElementById('tutorial').style.display='none';">
<div class="difusescreen" align="center">

<div class="chalktut" align="center">
<h4>Verification Time</h4>
<div class="whtverify">Why Verify?<br><ul><li>To become visible to other members</li><li>To start interacting with your matches</li></ul></div>
<div class="needscore"><img src="{$cdnurl}/images/tbarrowleft.png"> Need <span>{$thresold}% score</span><br>to become <span>Verified</span></div>
<div class="waystoverf">Ways to build your score</div>
</div>

</div>
</div>
<!-- Pop Up end -->


{if $from_mobile}
{else}
{include file='templates/common/header.tpl'}
{/if}
<div class="new_wrapper truststepbg">

<!--Centered Container Start -->
<div class="new_container" align="center">

<div class="regprogress">
		{if $isRegistration eq 1 }
    		<h2>Verification Time</h2>
            <div class="regscalesec" align="center"><div class="regprogbar pbarstep6"></div></div>
         {else}
            <h2>Build your trust score</h2>
         {/if}   
		</div>

<!--TrustBuilder Start -->
<div class="trstbuildsec">
	<div class="tbswraper">
	
    <div class="tbstitle"> 
    {if $status eq 'authentic'}
    	<div id="authentic"><h3>Status: <span class="txtgreen">Verified</span></h3><p>Build a higher score to get more responses <!--<span>(optional)</span> --></p></div>
    	<div id="non-authentic" style="display:none;"><h3>Status: <span class="txtred">Not Verified</span></h3><p>Get {$thresold}% score to become verified</p></div>
    {else}
    	<div id="authentic" style="display:none;"><h3>Status: <span class="txtgreen">Verified</span></h3><p>Build a higher score to get more responses <!--<span>(optional)</span> --></p></div>
    	<div id="non-authentic"><h3>Status: <span class="txtred">Not Verified</span></h3><p>Get {$thresold}% score to become verified</p></div>
    {/if}
		<!--
        <h3><span>Build</span> Trust Score</h3>
        <ul class="tbswhy1">
        <li>30% to Like/Hide matches</li>
		<li>a higher score to get more noticed</li>
        </ul>
		<p>To get verified, you need a minimum score of 25%. Why <i class="icon-question-sign"></i></p>
        <ul class="tbswhy">
        <li>Only verified members can interact with their matches.</li>
		<li>Only verified members are visible to their matches.</li>
		<li>The higher your Trust Score, the more we'll recommend you to our members.</li>
        </ul> -->
    </div>
	
	
    
    <!-- Trust Meter Section Start -->
    <div class="metersec">
  <h3>TRUST SCORE</h3>
    
    <div class="pieContainer">
    		 	<div class="pieBackground"></div>
     			<div class="hold" id="pieSlice1">
                	<div class="pie" style="-webkit-transform:rotate(0deg);  -moz-transform:rotate(0deg); transform: rotate(0deg);"></div>
            	</div>
     			<div class="persentbg"><p id="trust_percent">{$trustscore}%</p></div>
   </div>
   </div>
    <!-- Trust Meter Section end -->
    
    <!-- Facebook Tile Start -->
    <ul id="fb" class="tbstilesblue"><!-- Use 'tbsverified' class with existing class after verification done -->
    <li><div class="tbstd1"><i class="icon-facebook-sign"></i></div></li>
    <li><div class="tbstd2"><p class="fl">Facebook</p><p id="fb_connections" class="fr">{if isset($fbTmDiff)}<span class="tberrmsg fr"><i class="icon-warning-sign"></i></span>{/if}</p></div></li>
    <li><div class="tbstd3"><p id="fb_trust">{$tbNumbers.score.facebook}%</p><!-- Use 'verifiedts' class on p tag after verification done--></div></li>
    <li class="fr">
    <div class="tbstd4">
    	<i id="fb-ok"  style="display:none;"></i>
    	<i id="fb-minus" class="icon-angle-up difficon" style="display:none;"></i>
        <i id="fb-plus" class="icon-angle-down difficon" style="display:block;"></i>
    </div>
    </li>
    </ul>
    <div class="tbsbelowtile" id="fb-below" style="display:none;">
    
    <div id="fb_upper">
    <p id="fbmsg">You should know that this information is only used to verify you and import your profile photo and favourites if we don't already have them. Don't stress about your profile link being shared. We would never do that to you.</p>
    <img id="loader_fb" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px">
    <a id="verify_fb" onclick="proceed()" class="tbsactionbtn mt10">Verify</a>
    <br><span class="notetxt">We will never post on your Facebook</span>
    </div>
   <div id="logout_fb" style="display:none;"><p>Your facebook session is timed out. Please login again.</p></div>
    <!--Error message section--><div id="mismatch_fb" style="display:none;"><p class="tberrmsg" id="fb_error"><i class="icon-warning-sign"></i></p><img id="loader_fb2" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px"><a id="sync_fb" onclick="proceed('reimport')" class="tbsactionbtn mt10">Sync with Facebook</a></div>
    
    </div>
    <!-- Facebook Tile End -->
    
    <!-- LinkedIn Tile Start -->
    <ul id="linkedin" class="tbstilesblue"><!-- Use 'tbsverified' class with existing class after verification done -->
    <li><div class="tbstd1"><i class="icon-linkedin-sign"></i></div></li>
    <!--<li><div class="tbstd2"><p class="fl">Linkedin</p><p class="fr" id="linkedin_connections"></p></div></li>-->
    <li><div class="tbstd2"><p class="fl">Linkedin</p><p class="fr" id="linkedin_designation"></p></div></li>
    <li><div class="tbstd3"><p id="linkedin_trust">{$tbNumbers.score.linkedin}%</p><!-- Use 'verifiedts' class on p tag after verification done--></div></li>
    <li class="fr">
    <div class="tbstd4">
    	<i id="link-ok"  style="display:none;"></i>
    	<i id="link-minus" class="icon-angle-up difficon" style="display:none;"></i>
        <i id="link-plus" class="icon-angle-down difficon" style="display:block;"></i>
    </div>
    </li>
    </ul>
    <div id="link-below" class="tbsbelowtile tbsdettile" style="display:none;">
		<div id="link-msg">
    		{if ($linkedinVerified eq 'no')}
    			<p id="intxtdet">This information will only be used to verify your professional background and import your current designation to show your matches your fancy title. Nothing else, including the link to your profile, would ever be shared.</p>
    			<div class="inbtndiv"><script type="in/login"></script></div>
				<img src="images/register/in_tm_loader.gif" id='load_linkedin_date' style="display:none;" width="140" />
                <span class="notetxt">We will never post on your Linkedin</span>
			{/if}
    	</div>
   		<!--Error message section-->
    	<p id="error_linkedin" class="tberrmsg"></p>
    	<div id="re_verify" style="display:none;">
    			<p id='linkmsg'></p>
    			<div class="inbtndiv"><script type="in/login"></script></div>
				<img src="images/register/in_tm_loader.gif" id='load_linkedin_date' style="display:none;" width="140"/>
    	</div>
    </div>
    <!-- LinkedIn Tile End -->
    
    <!-- Photo ID Tile Start -->
    <ul id="photoId" class="tbstilesblue"><!-- Use 'tbsverified' class with existing class after verification done -->
    <li><div class="tbstd1"><i class="icon-user"></i></div></li>
    <li><div class="tbstd2"><p class="fl">Photo ID</p><p class="fr" id="idtype">{if ($idVerified eq 'rejected' || $idVerified eq 'fail')}<p class="tberrmsg fr"><i class="icon-warning-sign"></i></p>{/if}</p></div></li>
    <li><div class="tbstd3"><p id="id_trust">{$tbNumbers.score.photoid}%</p><!-- Use 'verifiedts' class on p tag after verification done--></div></li>
    <li class="fr">
    <div class="tbstd4">
    	<i id="id-ok"  style="display:none;"></i>
    	<i id="id-minus" class="icon-angle-up difficon" style="display:none;"></i>
        <i id="id-plus" class="icon-angle-down difficon" style="display:block;"></i>
    </div>
    </li>
    </ul>
    <div id="id-below" class="tbsbelowtile" style="display:none;">

    <div>
    
    
    <!--<img id="loader_photo" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px">-->
    <!-- if user photo not available -->
   
   <!-- <div id="picFirst" {if ($isAnyprofilePic >= 1)} style="display:none;"{/if}><a class="tbsactionbtn mt10 mb10">Upload Profile Photo First</a></div>-->
  <!--  <form method="post" id="upload-doc-form1" target="file_upload_frame1" enctype="multipart/form-data" class="dispnone">
		<input type="file" id="photo" name="file"/>
		<input type="hidden" id="action" name="action" value="add_from_computer" />
		<input type="hidden" id="crop" name="crop" value="no" />
		<input type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
	</form> -->
   <!-- <div id="photo-msg" class="tbsucess"></div>-->
    
    
    <div id="pwd_area" {if ($idReject eq 'password_required' && $idVerified eq 'rejected')} {else} style="display:none;"{/if}>
    <p class="tberrmsg"><i class="icon-warning-sign"></i> Hello hello! Your document is password protected. Please provide document password</p>
    <input name="docpw" id="pwd_1" type="text" class="tbstxtbox mt10" placeholder="Enter Document Password"><br>
    <!--<span>Enter password:</span><input type="text" id="pwd_1">-->
    <img id="loader_pwd_1" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px">
    <a id="send_pwd_1" onclick="sendPwd(1)" class="tbsactionbtn">Send to Re-verify</a><br>OR<br>Upload a new one
    </div>
    
    <div id="sync_area" style="display:none;">
    	<p class="tberrmsg" id="id_mismatch_error"><i class="icon-warning-sign"></i></p>
    	<img id="loader_sync_id" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px">
    	<a id="syncId" onclick="allowSync(1)" class="tbsactionbtn mt10">Sync with Photo ID</a>
    	<br>OR<br>
    	Upload a new Document
    </div>
    <!--<div id="sync_area" {if ($idReject eq 'name_age_mismatch' && $idVerified eq 'rejected')} {else} style="display:none;"{/if}>
    <span>There is name/age mismatch between your ID proof and TM profile</span>
    <img id="loader_sync_id" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px">
    <input type="button" id="syncId" onclick="allowSync(1)" value="Change to Name/Age from PhotoID">
    <br>OR<br>
    Upload a new Document
    </div>-->
    
     <div id="id-not-clear" {if ($idVerified eq 'rejected' && $idReject eq 'not_clear')} {else}style="display:none;"{/if}><p class="tberrmsg"><i class="icon-warning-sign"></i>Tsk! Tsk! Document is unclear or an invalid proof.</p></div>
     
    <div id="id-form" {if ($idVerified neq 'N' && $idVerified neq 'rejected')} style="display: none;"{/if}>
    <p id="id_msg" {if ($idVerified eq 'rejected')}style="display:none;"{/if}>We just need this to verify that you are who you say you are. We will never share this with anyone, and just like how things go in a spy movie, the document will be destroyed as soon as we verify it. </p>
    
    <img id="loader_photo" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px">
    <div id="picFirst" {if ($isAnyprofilePic >= 1)} style="display:none;"{/if}><a class="tbsactionbtn mt10 mb10">Upload Profile Photo First</a></div>
    <form method="post" id="upload-doc-form1" target="file_upload_frame1" enctype="multipart/form-data" class="dispnone">
		<!--<input type="file" id="photo" size="0" onchange="uploadPhoto()" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" />-->
		<input type="file" id="photo" name="file"/>
		<input type="hidden" id="action" name="action" value="add_from_computer" />
		<input type="hidden" id="crop" name="crop" value="no" />
		<input type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
	</form>
    <div id="photo-msg" class="tbsucess"></div>
    
    <div class="qselect-style">
    <select id="idSelectBox" name="phid"  onchange="idChange()">
            <option value="0" disabled selected>Select Photo ID</option>
			<option value="Passport">Passport</option>
			<option value="Driving License">Driving License</option>
			<option value="Pan Card">Pan Card</option>
			<option value="Voter ID">Voter ID</option>
			<option value="Aadhar Card">Aadhar Card</option>		
            </select>
            </div>
    <img id="loader_id" style="display:none;" src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px">        
    <a id="id_button" onclick="openFile(1)" class="tbsactionbtn"{if ($isAnyprofilePic neq 1)} style="display:none;"{/if}>Browse from Computer</a>
    
    <form method="post" id="upload-doc-form" target="file_upload_frame" enctype="multipart/form-data" class="dispnone">
		<!--<input type="file" id="file" size="0" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" />--> 
		<input type="file" id="file" name="file"/>
		<input type="hidden" value="" name="type" id="upload-file-type" /> 
		<input type="hidden" id="upload_proof_type" name="proofType" value="" />
		<input type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
	</form>
    
    {if $from_mobile}
		<p>Note: You can also mail us the documents at contact@trulymadly.com</p>
	{/if}
    </div>
    
    </div>
    
        <p id="id_error" class="tberrmsg"></p>
        <p id="sync_id_msg"></p>
        <p id="pwd_id_msg"></p>
        <p id="id_success"  class="tbsucess" {if ($idVerified neq 'under_moderation')} style="display: none;"{/if}>{if ($idVerified eq 'under_moderation')} Thank you for uploading your {$idproofType}. We will review it and contact you within 48 hours.{else}Yippee! Thank a ton for uploading your <span id='id_type'></span>. We will review it and contact you within 48 hours.{/if}</p>
        <p {if ($idVerified eq 'fail')} {else}style="display:none;"{/if}>Contact Trulymadly customer Care.</p> 
    </div>
    <!-- Photo ID Tile End -->
    
     <!-- Phone Number Tile Start -->
    <ul id="phone" class="tbstilesblue"><!-- Use 'tbsverified' class with existing class after verification done -->
    <li><div class="tbstd1"><i class="icon-phone-sign"></i></div></li>
    <li><div class="tbstd2"><p class="fl">Phone Number</p><p class="fr" id="mobile"></p></div></li>
    <li><div class="tbstd3"><p id="phone_trust">{$tbNumbers.score.mobile}%</p><!-- Use 'verifiedts' class on p tag after verification done--></div></li>
    <li class="fr">
    <div class="tbstd4">
    	<i id="phone-ok"  style="display:none;"></i>
    	<i id="phone-minus" class="icon-angle-up difficon" style="display:none;"></i>
        <i id="phone-plus" class="icon-angle-down difficon" style="display:block;"></i>
    </div>
    </li>
    </ul>
    <div id="phone-below" class="tbsbelowtile" style="display:none;">
    	<div><p>Don't stress about receiving calls from us. Neither would we share your phone number with anyone else.</p>
   		<input name="mobno" id="phoneNumber" type="text" class="tbstxtbox mt10" placeholder="99xxxxxxxx"><a id="verifyPhone" onclick="submitPhone()" class="tbsactionbtn mt10">Verify</a>
    <!-- will show after clicking verify now --><span id="phone_wait_loader" style="display:none;"><img src="{$cdnurl}/images/ajaxloader.gif" align="absmiddle" width="25px"> Call Placed. Verifying....</span></div>
    	<p id="phone_error" class="tberrmsg"></p>
    </div>
    <!-- Phone Number Tile End -->
    

    
    
    
    

    <!-- Endorsement Section Start -->
    <ul id="endorse" class="tbstilesblue {if $trustpoints.endorsement_count>1} tbsverified{/if}"><!-- Use 'tbsverified' class with existing class after verification done -->
    <li><div class="tbstd1"><i class="icon-ok-circle"></i></div></li>
    <li><div class="tbstd2"><p class="fl">{if $trustpoints.endorsement_count > 1}{$trustpoints.endorsement_count} Endorsements{else if $trustpoints.endorsement_count eq '1'}{$endorsementData.endorsement_message}{else}Endorsements{/if}</p></div></li>
    <li><div class="tbstd3"><p id="emp_trust" {if $trustpoints.endorsement_count>1}class= "verifiedts"{/if}>{$tbNumbers.score.endorsement}%</p><!-- Use 'verifiedts' class on p tag after verification done--></div></li>
    <li class="fr">
    <div class="tbstd4">
    	<i id="endorse-ok"  style="display:none;"></i>
    	<i id="endorse-minus" class="icon-angle-up difficon" style="display:none;"></i>
        <i id="endorse-plus" class="icon-angle-down difficon" style="display:block;"></i>
    </div>
    </li>
    </ul>
    <div id="endorse-below" class="tbsbelowtile"  style="display:none;">
    
    	<div>{if $trustpoints.endorsement_count < 1}<p id="endorse-msg">{$endorsementData.endorsement_message}</p>
        <a class="tbsactionbtn mt10" onclick="sendEndorsementRequest()">Send Request</a>{/if}</div>
        
         <!-- Endorsment List -->
         {if !empty($endorsementData.data)}
        <div class="endorssec">
			<div class="slider_wrapper">
				<ul id="image_slider">
                <li style="cursor:pointer;" onclick="sendEndorsementRequest()"><img src="{$cdnurl}/images/addmore.png"><p>Add More</p></li>
				{foreach $endorsementData.data item=val}
					<li><img src="{$val.pic}"><p>{$val.fname}</p></li>
				{/foreach} 
				</ul>					
				
			</div>
            <span class="nvgt" id="prev" style="display:none"></span>
				<span class="nvgt" id="next"></span>
		</div>
		{/if}
         <!-- Endorsment List end -->
        </div>
    <!-- Endorsement section end -->
	</div>
        
<!-- color bar -->    
<ul class="formbtmbrdr">
<li class="bg6"></li>
<li class="bg5"></li>
<li class="bg4"></li>
<li class="bg1"></li>
<li class="bg2"></li>
<li class="bg3"></li>
</ul> 
	</div>
<!--TrustBuilder end -->

<div class="regfrmaction"  {if $isRegistration eq 1 }{else}style="display:none;"{/if}>
<a id="trustCont" onclick="onContinue(this)" class="regbcbtn fr" {if $trustscore > 0 }{else}style="display:none;"{/if}>Continue &nbsp;<i class="icon-angle-right"></i></a>
<a onclick="onContinue(this)" id="nothnx" class="skiptxt1" {if $trustscore eq 0 }{else}style="display:none;"{/if}>Skip for now&nbsp;<i class="icon-double-angle-right" style="text-decoration:none;"></i> </a>
</div>

	
</div>
<!--Centered Container End -->
</div>
{if $from_mobile}
{else}
{include file='templates/common/footer.tpl'}
{/if}

<iframe name="file_upload_frame" id="file_upload_frame" height="0" width="0" style="height: 0px; width: 0px; border: none;"> </iframe>
<!--<form method="post" id="upload-doc-form" target="file_upload_frame" enctype="multipart/form-data" class="dispnone">
	<!--<form method="post" id="upload-doc-form" target="file_upload_frame" enctype="multipart/form-data">-->
	<input type="file" id="file" size="0" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" /> 
	<!--<input type="file" id="file" name="file"/>-->
	<input type="hidden" value="" name="type" id="upload-file-type" /> 
	<input type="hidden" id="upload_proof_type" name="proofType" value="" />
	<input type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
</form>-->


<iframe name="file_upload_frame1" id="file_upload_frame1" height="0" width="0" style="height: 0px; width: 0px; border: none;"> </iframe>
<!--	<form method="post" id="upload-doc-form1" target="file_upload_frame1" enctype="multipart/form-data" class="dispnone">
		<input type="file" id="photo" size="0" onchange="uploadPhoto()" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" /> 
		<input type="hidden" id="action" name="action" value="add_from_computer" />
		<input type="hidden" id="crop" name="crop" value="no" />
		<input type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
	</form>
-->

<iframe name="file_upload_frame2" id="file_upload_frame2" height="0" width="0" style="height: 0px; width: 0px; border: none;"> </iframe>
<!--	<form method="post" id="upload-doc-form2" target="file_upload_frame2" enctype="multipart/form-data" class="dispnone">
		<input type="file" id="file2" size="0" onchange="uploadFileEmployment()" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" />
		<input type="hidden" value="" name="type" id="upload-file-type2" /> 
		<input type="hidden" id="upload_proof_type2" name="proofType" value=""/>
		<input type="hidden" id="company_name" name="company_name" value=""/>
		<input type="hidden" id="emp_id" name="emp_id" value=""/>
		<input type="hidden" id="work_status" name="work_status" value=""/> 
		<input type="hidden" id="show_company" name="show_company" value=""/>
		<input type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
	</form>
-->			

<!-- Pop Up Start -->
<div id="diffframe4" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="tbpopupframe">
<p id="fbpicmsg" {if $isAnyprofilePic >= 1} style="display:none;"{/if}>We require a profile pic so will pick one from Facebook, you can always edit it later.<br></p><p>We will also pull your Likes for better matching<br><br>
<a onclick="proceed()" class="tbsactionbtn">Continue</a> <a onclick="hidepopup()" class="sorrytxt">Cancel</a></p>


</div>
</div>
<!-- Pop Up end -->

<!-- Pop Up Start -->
<div id="diffframe5" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="rspopup">
Request Sent
</div>
</div>
<!-- Pop Up end -->


{if $from_mobile}
<script>
//loadAsset("trustbuilder/js/jquery-1.8.2.min.js");
//loadAsset("trustbuilder/js/trustbuilder.js");
</script>
{else}
<!--<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/trustbuilder.js?v=0.6"></script>-->
{/if}

<script src="{$cdnurl}/js/lib/jquery-1.8.2.min.js"></script>
{if !empty($endorsementData.data)}
<script type="text/javascript" src="{$cdnurl}/js/Image-Slider.js"></script> 
{/if}
<script src="{$cdnurl}/js/lib/function.js?v=3"></script>
<script src="{$cdnurl}/js/trustbuilder.js?v=4.7"></script>



</body>
</html>