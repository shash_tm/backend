<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Daily Tracker</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h3> Photo Moderation Report </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="200">
<tr>
<th> Moderator </th>
<th> Approved Photos </th>
<th> Rejected Photos </th>
<th> Total </th>
</tr>
{foreach from=$photo_data item=v}
<tr>
<td> {$v.user_name} </td>
<td> {$v.pic_active} </td>
<td> {$v.pic_rejected} </td>
<td> {$v.c_p} </td>
</tr>
{/foreach}
</table>


</body>
</html>