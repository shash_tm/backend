<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Spark payment Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Select User Statistics </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th align="center"> User ID </th>
        <th align="center"> Gender </th>
        <th align="center"> Age </th>
        <th align="center"> City </th>
        <th align="center"> City Type </th>
        <th align="center"> Matches </th>
        <th align="center"> Matches Select </th>
        <th align="center"> Matches Non Select </th>
        <th align="center"> Likes </th>
        <th align="center"> Likes Select </th>
        <th align="center"> Likes Non Select </th>
        <th align="center"> Likebacks </th>
        <th align="center"> Likebacks Select </th>
        <th align="center"> Likebacks Non Select </th>
        <th align="center"> Hides </th>
        <th align="center"> Hides Select </th>
        <th align="center"> Hides Non Select </th>
        <th align="center"> Sparks </th>
        <th align="center"> Sparks Select </th>
        <th align="center"> Sparks Non Select </th>
        <th align="center"> Pack </th>
    </tr>
    {foreach from=$data_array key=k  item=v}
    <tr>
        <td align="center"> {$k} </td>
        <td align="center"> {$v.gender} </td>
        <td align="center"> {$v.age} </td>
        <td align="center"> {$v.city} </td>
        <td align="center"> {$v.city_type} </td>
        <td align="center"> {$v.matches} </td>
        <td align="center"> {$v.matches_select} </td>
        <td align="center"> {$v.matches_non_select} </td>
        <td align="center"> {$v.likes} </td>
        <td align="center"> {$v.likes_select} </td>
        <td align="center"> {$v.likes_non_select} </td>
        <td align="center"> {$v.likebacks} </td>
        <td align="center"> {$v.likebacks_select} </td>
        <td align="center"> {$v.likebacks_non_select} </td>
        <td align="center"> {$v.hides} </td>
        <td align="center"> {$v.hides_select} </td>
        <td align="center"> {$v.hides_non_select} </td>
        <td align="center"> {$v.sparks} </td>
        <td align="center"> {$v.sparks_select} </td>
        <td align="center"> {$v.sparks_non_select} </td>
        <td align="center"> {$v.price} </td>
    </tr>

    {/foreach}

</table>
</body>
</html>