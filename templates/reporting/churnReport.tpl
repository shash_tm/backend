<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Churn Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<h4> Female Churn Report </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0">
 {foreach from=$data.F key=ref item=arr}
 <tr> </tr>
<tr> 
<th> {$ref}  </th>
<th>NCR </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th >Mumbai </th> 
 {for $rep=1 to 17}
<th>   </th>
{/for}
<th>Bengaluru </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Hyderabad </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Chennai </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Kolkata </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Pune </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Lucknow </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Jaipur </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Other </th> 
{for $rep=1 to 17}
<th>   </th>
{/for} 
 </tr>

<tr>
 <th> PI </th>
 {for $foo=1 to 10}
 <th> New </th> 
 <th> 1 </th>
 <th> 2 </th>
 <th> 3 </th>
 <th> 4 </th>
 <th> 5 </th>
 <th> Total  </th>
 <th> Total Churn </th>
 <th> Total Churn % </th>
 <th> % Liked or Hid </th>
 <th> % Liked </th>
 <th> % Got mutual Matches </th>
 <th> 1 day </th>
 <th> 3 days </th>
 <th> 7 days </th>
 <th> 14 days </th>
 <th> 1 Month </th>
 <th> More </th>
 {/for}
 </tr>
{foreach from=$arr key=k item=v}
<tr> {if $k eq 'age_18'}
        <th> 18 - 21</th>
       {/if}
     {if $k eq 'age_22'}
        <th> 22 - 24</th>
       {/if}
      {if $k eq 'age_25'}
        <th> 25 - 28</th>
       {/if}
       {if $k eq 'age_29'}
        <th> 29 - 32</th>
       {/if}
       {if $k eq 'age_33'}
        <th> Above 33</th> 
       {/if}

{foreach from=$v key=city item=val}
{foreach from=$val key=pi item=var}
{if $var|is_array}
<td width="50">
{$var.perc}% </td> 
{/if}
{/foreach}
<td> {$val.6}  </td>
<td> {$val.7}  </td>
<td> {$val.8}  % </td>
<td> {$val.9} % </td>
<td> {$val.10} % </td>
<td> {$val.11} %</td>
<td> {$val.12} %</td>
<td> {$val.13} %</td>
<td> {$val.14} % </td>
<td> {$val.15} % </td>
<td> {$val.16} % </td>
<td> {$val.17}  % </td> 
{/foreach}
</tr> 
{/foreach}
{/foreach}
 </table>
 
 
 <h4> Active Male Stats in User Search </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0">
  
<tr> 
<th>   </th>
<th>NCR </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th >Mumbai </th> 
 {for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Bengaluru </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Hyderabad </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Chennai </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Kolkata </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Pune </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Lucknow </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Jaipur </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Other </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
 </tr>

<tr>
 <th> PI </th>
 {for $foo=1 to 10}
 <th> New </th> 
 <th> 1 </th>
 <th> 2 </th>
 <th> 3 </th>
 <th> 4 </th>
 <th> 5 </th>
 <th>   </th>
 {/for}
 </tr>
{foreach from=$user_search.M key=k item=v}
<tr>
{if $k eq 'age_18'}
        <th> 18 - 21</th>
       {/if}
     {if $k eq 'age_22'}
        <th> 22 - 24</th>
       {/if}
      {if $k eq 'age_25'}
        <th> 25 - 28</th>
       {/if}
       {if $k eq 'age_29'}
        <th> 29 - 32</th>
       {/if}
       {if $k eq 'age_33'}
        <th> Above 33</th>
       {/if}
{foreach from=$v key=city item=val}
{foreach from=$val item=var}
<td>
{$var} </td> 
{/foreach}
<td> </td>
{/foreach}
</tr> 
{/foreach}
 </table>
 
 
 <h4> Male Churn Report </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0">
{foreach from=$data.M key=ref item=arr}
 <tr> </tr>
<tr> 
<th> {$ref}  </th>  
<th>NCR </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th >Mumbai </th> 
 {for $rep=1 to 17}
<th>   </th>
{/for}
<th>Bengaluru </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Hyderabad </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Chennai </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Kolkata </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Pune </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Lucknow </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Jaipur </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}
<th>Other </th> 
{for $rep=1 to 17}
<th>   </th>
{/for}

 </tr>

<tr>
  <th> PI </th>
 {for $foo=1 to 10}
 <th> New </th> 
 <th> 1 </th>
 <th> 2 </th>
 <th> 3 </th>
 <th> 4 </th>
 <th> 5 </th> 
 <th> Total  </th>
 <th> Total Churn </th>
 <th> Total Churn % </th>
 <th> % Liked or Hid </th>
 <th> % Liked </th>
 <th> % Got mutual Matches </th>
 <th> 1 day </th>
 <th> 3 days </th>
 <th> 7 days </th>
 <th> 14 days </th>
 <th> 1 Month </th>
 <th> More </th>
 
 {/for}
 </tr>
{foreach from=$arr key=k item=v}
<tr>
{if $k eq 'age_18'}
        <th> 18 - 21</th>
       {/if}
     {if $k eq 'age_22'}
        <th> 22 - 24</th>
       {/if}
      {if $k eq 'age_25'}
        <th> 25 - 28</th>
       {/if}
       {if $k eq 'age_29'}
        <th> 29 - 32</th>
       {/if}
       {if $k eq 'age_33'}
        <th> Above 33</th>
       {/if}
{foreach from=$v key=city item=val}
{foreach from=$val key=pi item=var}
{if $var|is_array}
<td width="50">
{$var.perc}% </td> 
{/if}
{/foreach}
<td> {$val.6}  </td>
<td> {$val.7} </td>
<td> {$val.8} % </td>
<td> {$val.9} % </td>
<td> {$val.10} % </td>
<td> {$val.11} %</td>
<td> {$val.12} %</td>
<td> {$val.13} %</td>
<td> {$val.14} % </td>
<td> {$val.15} % </td>
<td> {$val.16} % </td>
<td> {$val.17} % </td>
{/foreach}
</tr> 
{/foreach}
{/foreach}
 </table>

 <h4> Active Female Stats in User Search </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0">
  
<tr> 
<th>   </th>
<th>NCR </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th >Mumbai </th> 
 {for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Bengaluru </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Hyderabad </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Chennai </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Kolkata </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Pune </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Lucknow </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Jaipur </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
<th>Other </th> 
{for $rep=1 to 6}
<th width='30'>   </th>
{/for}
 </tr>


<tr>
 <th> PI </th>
 {for $foo=1 to 10}
 <th> New </th>
 <th> 1 </th>
 <th> 2 </th>
 <th> 3 </th>
 <th> 4 </th>
 <th> 5 </th>
 <th>   </th>
 {/for}
 </tr>
{foreach from=$user_search.F key=k item=v}
<tr>
{if $k eq 'age_18'}
        <th> 18 - 21</th>
       {/if}
     {if $k eq 'age_22'}
        <th> 22 - 24</th>
       {/if}
      {if $k eq 'age_25'}
        <th> 25 - 28</th>
       {/if}
       {if $k eq 'age_29'}
        <th> 29 - 32</th>
       {/if}
       {if $k eq 'age_33'}
        <th> Above 33</th>
       {/if}
{foreach from=$v key=city item=val}
{foreach from=$val item=var}
<td>
{$var} </td> 
{/foreach}
<td> </td>
{/foreach}
</tr> 
{/foreach}
 </table>

</body>
</html>  