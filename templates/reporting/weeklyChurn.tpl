<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Performace Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<h4> Weekly Uninstalls </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
<tr> 
<th> Gender </th>
<th> City </th>
<th> Age Group </th>
<th> Registrations </th>
<th> Same week uninstalls </th>
<th> Same week uninstalls % </th>
<th> Total Uninstalls </th>
<th> Didn't get Authentic </th>
<th> Didn't get matches </th>
<th> Got matches but no action taken </th>
<!--<th> Got matches but no action taken without photo</th>-->
<th> Took action but didn't like </th>
<!--<th> Took action but didn't like without photo </th>-->
<th> Liked but no mutual like </th>
<!--<th> Liked but no mutual like without photo </th>-->
<th> Got mutual like  but not chatted </th>
<th> Chatted with matches </th>
</tr>
{foreach from=$uni key=k item=v}
<tr>
<td> {$v.gender} </td> 
<td> {$v.city} </td>
 {if {$v.age} eq 'age_18'}
        <td> 18 - 21</td>
       {/if}
     {if {$v.age} eq 'age_22'}
        <td> 22 - 24</td>
       {/if}
      {if {$v.age} eq 'age_25'}
        <td> 25 - 28</td>
       {/if}
       {if {$v.age} eq 'age_29'}
        <td> 29 - 32</td>
       {/if}
       {if {$v.age} eq 'age_33'}
        <td> Above 33</td> 
       {/if} 
<td> {$v.reg}  </td>
<td> {$v.swuni}  </td>
<td> {$v.sameweek_perc}  </td>
<td> {$v.uninstalls}  </td>
<td> {$v.auth} % </td>
<td> {$v.matches} % </td>
<td> {$v.like_or_hide}  %  </td>
<!--<td> {$v.like_or_hide_photo}  % </td>--> 
<td> {$v.like}  %  ({$v.like_photo}  %)</td>
<!--<td> {$v.like_photo}  % </td>-->
<td> {$v.mutual} % ({$v.mutual_photo} %) </td>
<!--<td> {$v.mutual_photo} % </td>-->
<td> {$v.chat}  % </td>
<td> {$v.chat_yes} % </td>
</tr>
{/foreach} 
</body>
</html>