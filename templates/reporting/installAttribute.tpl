<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Source wise attributes</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Attributes </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
  
<tr>
<th>
Source
</th>
<th >
Installs
</th>
<th >
Registrations
</th>
<th colspan="2">
Completed
</th>
<th colspan="2">
Authentic
</th>
</tr>
<tr>
<th>
</th>
<th >
</th>
<th >
</th>
<th >
Completed Male
</th>
<th >
Completed Female
</th>
<th >
Authentic Male
</th>
<th >
Authentic Female 
</th>
<th >
Via Facebook
</th>
<th >
Via Email
</th>
</tr>
{foreach from=$data.referrer.attributes key=k item=v}
<tr>
<td>
{$v.source} 
</td>
<td > 
{$v.installs}
</td>
<td > 
{$v.registrations}
</td>
<td >
{$v.male_complete} ( {$v.male_complete_percent} % ) 
</td>
<td >
{$v.female_complete} ( {$v.female_complete_percent} % )
</td>
<td >
{$v.male_auth} ( {$v.male_auth_percent} % ) 
</td>
<td >
{$v.female_auth} ( {$v.female_auth_percent} % ) 
</td>
<td >
{$v.via_fb_percent}  % 
</td>
<td >
{$v.via_email_percent}  % 
</td>
</tr>
{/foreach}
</table>
<!--  
{foreach from=$data.organic.attributes key=k item=v} 
<tr>
<th>
{$v.source} 
</th>
<th > 
{$v.installs}
</th>
<th > 
{$v.registrations}
</th>
<th >
{$v.male_complete} ( {$v.male_complete_percent} % ) 
</th>
<th >
{$v.female_complete} ( {$v.female_complete_percent} % )
</th>
<th >
{$v.male_auth} ( {$v.male_auth_percent} % ) 
</th>
<th >
{$v.female_auth} ( {$v.female_auth_percent} % ) 
</th>
<th >
{$v.via_fb_percent}  % 
</th>
<th >
{$v.via_email_percent}  % 
</th></tr>
{/foreach}



<h4> Demography </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr><th>Source</th>
<th>Gender</th>
<th colspan="3"  >Age Groups </th>
</tr>
<tr>
<th> </th>
<th> </th>
<th> Less than 23 </th>
<th> 24 to 28  </th>
<th> More than 28 </th>
</tr>
{foreach from=$data.referrer.age key=k item=l} 
<tr>
 <td rowspan="2">{$k}</td>

<td> {$l.M.gender}</td> 
<td> {$l.M.age_one}</td>
<td> {$l.M.age_two}</td>
<td> {$l.M.age_three}</td>  
</tr>
<tr>
<td> {$l.F.gender}</td> 
<td> {$l.F.age_one}</td>
<td> {$l.F.age_two}</td>
<td> {$l.F.age_three}</td>  
</tr>
{/foreach}
<tr></tr>

</table>

-->




</body>
</html>