<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Survey Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<b>Survey Stats:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr >






<td rowspan="2">
User Id
</td>

<td rowspan="2">
Gender
</td>

<td rowspan="2">
Age
</td>


<td colspan="4">
What attracted you first to register with us?
</td>

<td colspan="3">
 You registered with us for?
</td>


<td colspan="3">
Do you intend to get married any time soon?
</td>

<td colspan="3">
Would you refer TrulyMadly to other singles?
</td>
</tr>

<tr>
 <td>The focus on Verified Profiles is reassuring.</td>
  <td>  The Security features on TrulyMadly seem tight.</td>
<td>    TrulyMadly looks young and cool – I can relate to it! </td>
<td>    The Matching on Personality and Interests. </td>


 <td>To find a life partner.</td>
<td>    To simply date & see where it leads</td>
<td>    Make new friends </td>


<td> Yes, want to marry soon</td>
<td> No, just on the lookout for someone right!</td>
    <td> Years</td>
    
    <td> Absolutely</td>
<td>    Not yet. Maybe Later?</td>
<td>   Nope, sorry </td>


</tr>

    
    
</tr>

{foreach  from=$arr item=v}
<tr>
<td>
{$v.userId}
</td>
<td>
{$v.gender}
</td>

<td>
{$v.age}
</td>
<td>
{$v.q1.q1ans1}
</td>
<td>
{$v.q1.q1ans2}
</td>
<td>
{$v.q1.q1ans3}
</td>
<td>
{$v.q1.q1ans4}
</td>


<td>
{$v.q2.q2ans1}
</td>
<td>
{$v.q2.q2ans2}
</td>
<td>
{$v.q2.q2ans3}
</td>

<td>
{if $v.q3 eq '1'} x {/if}
</td>
<td>
{if $v.q3 eq '2'} x {/if}
</td>

<td>
{$v.years}
</td>

<td>
{if $v.q4 eq '1'} x {/if}
</td>
<td>
{if $v.q4 eq '2'} x {/if}
</td>
<td>
{if $v.q4 eq '3'} x {/if}
</td>

</tr>
{/foreach}

</table>


</body>
</html>