<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Performace Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<h4> Facebook Photo import status </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">

 <tr>
  <th>Platform </th>
  <th>Gender </th>
  <th> Total Users </th>
  <th> Photo Permissions Issue </th>
  <th> Photos not imported </th>
 </tr>
 {foreach from=$photo_data key=k item=v}
 <tr>
  <td> {$v.registered_from } </td>
  <td> {$v.gender }  </td>
  <td> {$v.allUsers }</td>
  <td> {$v.permission_issue }</td>
  <td> {$v.without_Photos }</td>
 </tr>
 {/foreach}
 </table>
<h4> Performance Report </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
  
<tr> 
<th>Activity </th> 
<th>Platform </th>
<th>Gender </th>
<th> Total Calls </th>
<th colspan="5"> 3G and 4G </th>
<th colspan="5"> 2G </th>
<th colspan="5"> WI-FI </th>
<!-- <th colspan="10"> CELLULAR </th>
<th colspan="10"> OTHER </th>  -->
 </tr>
<tr>
 <th> </th> 
 <th> </th>
 <th> </th>
 <th> </th>
  {for $limit=1 to 3}
<th > all calls </th> 
<!-- <th > Zero </th> -->
<th >Less than 5s </th>
<th > 5 to 10s </th>
<th > Above 10 </th>
<!-- <th > 20 to 30s </th>
<th > Above 30 </th>
<th > Error </th>
<th > Error Timeout </th> -->
<th > Error Code </th>
{/for}
</tr>
{foreach from=$data key=k item=v}
<tr>
<th> {$v.actions} </th>
<th> {$v.source} </th>
<th> {$v.gender} </th>
<th> {$v.all_calls} </th>
<th> {$v.3G4G.portion} %  </th>
<!-- <td> {$v.3G4G.zero_p} </td> -->
<td> {$v.3G4G.five_p} </td>
<td> {$v.3G4G.ten_p} </td>
<td> {$v.3G4G.twenty_p} </td>     
<!-- <td> {$v.3G4G.thirty_p} </td>
<td> {$v.3G4G.above_thirty_p} </td>
<td> {$v.3G4G.error_p} </td>
<td> {$v.3G4G.error_timeout_p} </td> -->
<td> {$v.3G4G.error_code_p} </td>

<th> {$v.2G.portion} %  </th>
<!-- <td> {$v.2G.zero_p} </td> -->
<td> {$v.2G.five_p} </td>
<td> {$v.2G.ten_p} </td>
<td> {$v.2G.twenty_p} </td>
<!-- <td> {$v.2G.thirty_p} </td>
<td> {$v.2G.above_thirty_p} </td>
<td> {$v.2G.error_p} </td>
<td> {$v.2G.error_timeout_p} </td>
 -->
 <td> {$v.2G.error_code_p} </td>

<th> {$v.WIFI.portion} %  </th>
<!-- <td> {$v.WIFI.zero_p} </td> -->
<td> {$v.WIFI.five_p} </td>
<td> {$v.WIFI.ten_p} </td>
<td> {$v.WIFI.twenty_p} </td>
<!-- <td> {$v.WIFI.thirty_p} </td>
<td> {$v.WIFI.above_thirty_p} </td>
<td> {$v.WIFI.error_p} </td>
<td> {$v.WIFI.error_timeout_p} </td> -->
<td> {$v.WIFI.error_code_p} </td>

<!-- <td> {$v.CELLULAR.portion} %  </td>
<td> {$v.CELLULAR.zero_p} </td>
<td> {$v.CELLULAR.five_p} </td>
<td> {$v.CELLULAR.ten_p} </td>
<td> {$v.CELLULAR.twenty_p} </td>
<td> {$v.CELLULAR.thirty_p} </td>
<td> {$v.CELLULAR.above_thirty_p} </td>
<td> {$v.CELLULAR.error_p} </td>
<td> {$v.CELLULAR.error_timeout_p} </td>
<td> {$v.CELLULAR.error_code_p} </td>

<td> {$v.other.portion} %  </td>
<td> {$v.other.zero_p} </td>
<td> {$v.other.five_p} </td>
<td> {$v.other.ten_p} </td>
<td> {$v.other.twenty_p} </td>
<td> {$v.other.thirty_p} </td>
<td> {$v.other.above_thirty_p} </td>
<td> {$v.other.error_p} </td>
<td> {$v.other.error_timeout_p} </td>
<td> {$v.other.error_code_p} </td> -->
{/foreach}

</table>

<h4>Error Counts Report</h4>

{foreach from=$Errors_data key=key item=value}
<h4> {$key} </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<th> Platform </th>
<th> Event_status </th>
<th> Count</th>
 </tr>
{foreach from=$value key=k item=v}
<tr> 
<td> {$v.source } </td>
<td> {$v.event_status}  </td>
<td> {$v.counts}</td> 
 </tr>
{/foreach}

</table>
{/foreach}

<h2>Chat Report</h2>
<h3>Android</h3>
<h4 align="center">Connection</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$connection.Android key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.AndroidWifi} </td>
<td> {$v.Android3G} </td>
<td> {$v.Android2G} </td>
</tr>
{/foreach}
</table>


<h4 align="center">Get Missed Messages</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$missed.Android key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.AndroidWifi} </td>
<td> {$v.Android3G} </td>
<td> {$v.Android2G} </td>
</tr>
{/foreach}
</table>



<h3 align="center">Chat sent</h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$chat.Android key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.AndroidWifi} </td>
<td> {$v.Android3G} </td>
<td> {$v.Android2G} </td>
</tr>
{/foreach}
</table>

<h3 align="center"> Disconnect </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$disconnect.Android key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.AndroidWifi} </td>
<td> {$v.Android3G} </td>
<td> {$v.Android2G} </td>
</tr>
{/foreach}
</table>


<h3>IOS</h3>
<h4 align="center">Connection</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$connection.IOS key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.IOSWifi} </td>
<td> {$v.IOS3G} </td>
<td> {$v.IOS2G} </td>
</tr>
{/foreach}
</table>


<h4 align="center">Get Missed Messages</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$missed.IOS key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.IOSWifi} </td>
<td> {$v.IOS3G} </td>
<td> {$v.IOS2G} </td>
</tr>
{/foreach}
</table>

<h3 align="center">Chat sent</h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$chat.IOS key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.IOSWifi} </td>
<td> {$v.IOS3G} </td>
<td> {$v.IOS2G} </td>
</tr>
{/foreach}
</table>

<h3 align="center"> Disconnect </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Event </th>
<th> Wifi </th>
<th> 3G </th>
<th> 2G </th>
</tr>
{foreach from=$disconnect.IOS key=k item=v}
<tr>
<td> {$k} </td> 
<td> {$v.IOSWifi} </td>
<td> {$v.IOS3G} </td>
<td> {$v.IOS2G} </td>
</tr>
{/foreach}
</table>




<h4> Time Taken</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Time  </th>
<th> Count </th>
</tr>
<tr>
<td> Less than 1 sec </td>
<td> {$msg_data.time_taken.one} </td>
</tr>
<tr>
<td> Less than 5 sec </td>
<td> {$msg_data.time_taken.five} </td>
</tr>  
<tr>
<td> Less than 10 sec </td>
<td> {$msg_data.time_taken.ten} </td>
</tr>
<tr>
<td> Greater than 10 sec </td>
<td> {$msg_data.time_taken.greater10} </td>
</tr>
</table> 

<h3 align="center"> Phone Verification </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Gender </th>
<th> Requested OTP </th>
<th> Entered OTP </th>
<th> Verified by OTP </th>
<th> Didn't enter OTP</th>
<th> Verified by Call </th>
</tr> 
{foreach from=$phone key=k item=v}
<tr>
<th> {$k} </th>
<td> {$v.requested} </td>
<td> {$v.otp_tried} </td>
<td> {$v.Verified_by_otp} </td>
<td> {$v.otp_not_tried} </td>
<td> {$v.by_call} </td>
</tr>
{/foreach}
</table>


<!--<h5>SDK Version wise messages</h5>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> SDK Version </th>
<th> Messages sent </th>
<th> Delivered </th>
<th> % Success </th>
</tr>
{foreach from=$msg_data.version key=k item=v}
<tr>
<td> {$v.sdk_version} </td>
<td> {$v.sent} </td>
<td> {$v.received} </td>
<td> {$v.perc} % </td>
</tr>
{/foreach}
</table>

<h5>SDK Version error connection</h5>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> SDK Version </th>
<th> Count </th>
</tr>
{foreach from=$msg_data.errors key=k item=v}
<tr>
<td> {$k} </td>
<td> {$v} </td>
</tr>
{/foreach}
</table>


<h4>Message Stats</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th>Data</th>
<th>Count</th>
</tr>
<tr>
<td> users on socket</td>
<td> {$msg_data.socket} </td>
</tr>
<tr>
<td> users on long poll</td>
<td> {$msg_data.poll} </td>
</tr>
<tr>
<td> No. of Retries</td>
<td> {$msg_data.retry}</td>
</tr>
<tr> 
<td> No. of messages not sent in first time</td>
<td> {$msg_data.one_fail}</td>
</tr>
</table>




<h4> Failed API calls</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th> Step</th>
<th> Count</th>
</tr>
{foreach from=$msg_data.api_fail key=k item=v}
<tr>
<td>{$v.event_type}</td> 
<td>{$v.c_u}</td>
</tr>
{/foreach}
</table>
-->
</body>
</html>