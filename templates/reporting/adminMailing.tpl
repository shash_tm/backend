<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Email Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


 <b>Photos Uploaded Today:</b><br><br>
 <table align="center" border="1" cellpadding="0" cellspacing="0" width="200">
  <tr>
   <th> Gender </th>
   <th> Count </th>
  </tr>
  {foreach  from=$uploadedPics item=v}
   <tr>
    <th> {$v.gender} </th>
    <td align="center"> {$v.cu} </td>
   </tr>
  {/foreach}
 </table>

<b>Photos Stats Today:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> count </td>
<td> Status </td>
<td> Gender </td>
<td> Admin Approved </td>
</tr> 
{foreach  from=$photos item=v}
<tr>
<td> {$v.count} </td>
<td> {$v.status} </td>
<td> {$v.gender} </td>
<td> {$v.admin_approved} </td>
</tr>
{/foreach}
</table>

<b>Photos Stats Last hour:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> count </td>
<td> Status </td>
<td> Gender </td>
<td> Admin Approved </td>
</tr>

{foreach  from=$photosh item=v}
<tr>
<td> {$v.count} </td>
<td> {$v.status} </td>
<td> {$v.gender}  </td>
<td> {$v.admin_approved} </td>
</tr>
{/foreach}
</table>
</b><br><br>
<b>Females Registered Yesterday:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="400">
<tr>
<th> Status </th>
<th> Count </th>
</tr> 
{foreach  from=$female_pics item=v}
<tr>
<td> {$v.pic_status} </td>
<td> {$v.c_u} </td>
</tr>
{/foreach}
</table>



<b>Photos under moderation for authentic:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> count </td>
<td> Status </td>
<td> Gender </td>
<td> Admin Approved </td>
</tr>
{foreach  from=$totalModPhoto item=v}
<tr>
<td> {$v.count} </td>
<td> {$v.status} </td>
<td> {$v.gender} </td>
<td> {$v.admin_approved} </td>
</tr>
{/foreach}
</table>

 

<br><b>ID proof Stats:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> count </td>
<td> Status </td>
<td> Gender </td>
</tr>

{foreach  from=$idProof item=v}
<tr>
<td> {$v.count} </td>
<td> {if $v.status eq 'fail'} Rejected {else if $v.status eq 'rejected'} Not Clear
{else} {$v.status} {/if} </td>
<td>  {$v.gender} </td>

</tr>
{/foreach}
{foreach  from=$IdsMod item=v}
<tr>
<td>{$v.count}</td>
<td>{$v.status}</td>
<td>{$v.gender}</td> 
</tr>
{/foreach}
</table>
<!-- 
<br><br><br><b>Employment proof Stats:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>

<td>
count
</td>

<td>

status
</td>

</tr>

{foreach  from=$empProof item=v}
<tr>
<td>
{$v.count}
</td>
<td>
{if $v.status eq 'fail'} Rejected
{else if $v.status eq 'rejected'} Not Clear
{else}
{$v.status}
{/if}
</td></tr>
{/foreach}
</table>
 -->

</body>
</html>