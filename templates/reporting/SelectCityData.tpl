<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Select Age</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Select Analysis by City</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th align="center"> City </th>
        <th align="center"> Gender </th>
        <th align="center"> Users </th>
        <th align="center"> Avg. Matches </th>
        <th align="center"> Avg.Matches Select </th>
        <th align="center"> Avg.Matches Non Select  </th>
        <th align="center"> Avg. Likes </th>
        <th align="center"> Avg. Hides </th>
        <th align="center"> Avg. Sparks </th>
        <th align="center"> Avg. Price </th>
    </tr>
    {foreach from=$city_data key=k  item=v}
        <tr>
            <td align="center"> {$v.city} </td>
            <td align="center"> {$v.gender} </td>
            <td align="center"> {$v.users} </td>
            <td align="center"> {$v.matches} </td>
            <td align="center"> {$v.matches_select} </td>
            <td align="center"> {$v.matches_non_select} </td>
            <td align="center"> {$v.likes} </td>
            <td align="center"> {$v.hides} </td>
            <td align="center"> {$v.sparks} </td>
            <td align="center"> {$v.price} </td>
        </tr>
    {/foreach}

</table>
</body>
</html>