<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>User Search Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<br><b>Total Count in User Search: {$count}</b><br><br> 
<br><b>Time taken in User Search generation: {$timeTaken}</b><br><br> 


<br><br><br><b>User Search Stats</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>Count</td>
</tr>
{foreach  from=$rows item=v}
<tr>
<td>{$v.gender}</td>
<td>{$v.count}</td>
</tr>
{/foreach}
</table>

</html>