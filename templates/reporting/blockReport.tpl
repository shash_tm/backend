<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Notification Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<br><b>Users blocked more than 3 times : </b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<th>User Id </th>
<th>Gender </th>
<th>Name </th>
<th>No. of times blocked</th>
<th>Last Reported on</th>
</tr>
{foreach from=$blocks item=v}
<tr>
<td> {$v.abuse_id} </td>
<td> {$v.gender} </td>
<td> {$v.name} </td>
<td> {$v.c_u} </td>
<td> {$v.last_rep}</td> 
</tr>
{/foreach}
</table> 