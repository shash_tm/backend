<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Email Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<br><b>Messages exchanged with Admin in last one hour:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>sender id</td>
<td>receiver id</td>
<td>msg</td>
<td>tstamp</td>
<td>Name</td>
<td>Age</td>
<td>gender</td>
<td>registered_at</td> 

</tr>
{foreach from=$data item=v}
<tr>
<td>{if $v.sender_id eq $admin_id} Admin {else} {$v.sender_id}{/if}</td>
<td>{if $v.receiver_id eq $admin_id} Admin {else} {$v.receiver_id}{/if}</td>
<td>{$v.msg_content}</td>
<td>{$v.ts}</td>
<td>{$v.fname}</td>
<td>{$v.age}</td>
<td>{$v.gender}</td>
<td>{$v.registered_at}</td> 
</tr>
{/foreach}
</table>

</body>
</html>