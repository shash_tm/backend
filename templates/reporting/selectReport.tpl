<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Spark payment Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Total Revenue {$revenue} ₹</h4>
<h4> Select Transactions For Today till {$date_now}</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th width="100"> Source </th>
        <th width="100"> Gender </th>
        <th width="100"> Gateway </th>
        {*<th width="100"> Bucket Id </th>*}
        <th width="100"> Price </th>
        <th width="100"> Initiated </th>
        <th width="100"> Completed </th>
        <th width="100"> Total Amount </th>
    </tr>
    {foreach from=$select_data key=k  item=v}
        <tr>
             <td align="center"> {$v.source} </td>
            <td align="center" > {$v.gender} </td>
            <td align="center"> {$v.payment_gateway} </td>
            {*<td align="center"> {$v.bucket_id} </td>*}
            <td align="center"> {$v.price} </td>
            <td align="center"> {$v.all_transactions} </td>
            <td align="center"> {$v.completed} </td>
            <td align="center"> {$v.total_price} </td>
        </tr>
    {/foreach}
    <tr>
        {*<td align="center"> {$v.package_id} </td>*}
        <th align="center"> Total </th>
        <th align="center">  </th>
        {*<td align="center"> {$v.spark_count} </td>*}
        {*<th align="center"> </th>*}
        <th align="center"> </th>
        <th align="center"> </th>
        <th align="center"> {$sum_payment_select.all_transactions} </th>
        <th align="center"> {$sum_payment_select.completed} </th>
        <th align="center"> {$sum_payment_select.total_price} </th>
    </tr>

</table>

</body>
</html>