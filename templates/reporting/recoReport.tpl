<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Recommendation Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<br><br><br><b>Matches Stats</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>Number of Matches</td>
<td>Count which took no action</td>
<td>Count which took action on atleast one profile</td>
<td>% which took action on ALL</td>
<td>% which took action on more than 70%</td>
<td>% which took action on more than 50%</td>


</tr>
{foreach  from=$matchesStats_male item=v}
<tr>
<td>{$v.gender}</td>
<td>{if !isset($v.matches) || $v.matches eq '0' }0{else}{$v.matches}{/if}</td>
<td>{$v.no_action}</td>
<td>{$v.count}</td>
<td>{if $v.matches eq '0'}0{else}{$v.full_perc|string_format:"%.2f"}{/if}</td>
<td>{if $v.matches eq '0'}0{else}{$v.70_perc|string_format:"%.2f"}{/if}</td>
<td>{if $v.matches eq '0'}0{else}{$v.50_perc|string_format:"%.2f"}{/if}</td>

</tr>
{/foreach}

{foreach  from=$matchesStats_female item=v}
<tr>
<td>{$v.gender}</td>
<td>{$v.matches}</td>
<td>{$v.no_action}</td>
<td>{$v.count}</td>
<td>{if $v.matches eq '0'}0{else}{$v.full_perc|string_format:"%.2f"}{/if}</td>
<td>{if $v.matches eq '0'}0{else}{$v.70_perc|string_format:"%.2f"}{/if}</td>
<td>{if $v.matches eq '0'}0{else}{$v.50_perc|string_format:"%.2f"}{/if}</td>
</tr>
{/foreach}
</table>
<br><br><br><b>No matches Reasoning for male users</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Preference</td>
<td>Already Actioned</td>
<td>Availability</td>
<td>Getting matches now</td>
</tr>
<tr>
<td>{$noMatchReport.pref}</td>
<td>{$noMatchReport.actioned}</td>
<td>{$noMatchReport.availability}</td>
<td>{$noMatchReport.will_get}</td>
</tr>
</table>

<br><br><b>No matches Cases of male users (by Bucket then by Age Group) :</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>bucket</td>
<td>age group</td>
<td>count</td>

</tr>
{foreach  from=$noMatch_age_bucket key=k item=v}
<tr>
<td>{$v.bucket}</td>
<td>{$v.age_group}</td>
<td>{$v.count}</td>

</tr>
{/foreach}
</table>


<br><br><br><b>Like Percent distribution</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>Bucket 0</td>
<td>Bucket 1</td>
<td>Bucket 2</td>
<td>Bucket 3</td>
<td>Bucket 4</td>
<td>Bucket 5</td>
</tr>
{foreach  from=$likesPercent key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.0}</td>
<td>{$v.1}</td>
<td>{$v.2}</td>
<td>{$v.3}</td>
<td>{$v.4}</td>
<td>{$v.5}</td>
</tr>
{/foreach}
</table>

<br><br><br><b>Mutual like distribution</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>ML-1</td>
<td>ML-2</td>
<td>ML-3</td>
<td>ML-4</td>
<td>ML-5</td>
<td>ML-5+</td>
<td>Unique Users</td>
<td>Total MLs</td>
<td>Mutual Like %(Users Installed)</td>
<td>Mutual Like %(Users Who took action)</td>
</tr>
{foreach  from=$mls key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.1}</td>
<td>{$v.2}</td>
<td>{$v.3}</td>
<td>{$v.4}</td>
<td>{$v.5}</td>
<td>{$v.5plus}</td>
<td>{$mls_userWise[$k]}</td>
<td>{$total_mls}</td>
<td>{$userWiseMls[$k]}</td>
<td>{$userWiseMlsUsage[$k]}</td>
</tr>
{/foreach}
</table>

<br><br><br><b>Mutual like distribution percent wise ( sum(mutual_like) / sum(likes_sent in a bucket) )</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>Bucket-0</td>
<td>Bucket-1</td>
<td>Bucket-2</td>
<td>Bucket-3</td>
<td>Bucket-4</td>
<td>Bucket-5</td>

</tr>
{foreach  from=$mls_perc key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.0}</td>
<td>{$v.1}</td>
<td>{$v.2}</td>
<td>{$v.3}</td>
<td>{$v.4}</td>
<td>{$v.5}</td>
</tr>
{/foreach}
</table>


<br><br><br><b>Mutual like distribution percent wise ( count(mutual_like) / count(likes_sent in a bucket) )</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>Bucket-0</td>
<td>Bucket-1</td>
<td>Bucket-2</td>
<td>Bucket-3</td>
<td>Bucket-4</td>
<td>Bucket-5</td>

</tr>
{foreach  from=$mls_perc_count key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.0}</td>
<td>{$v.1}</td>
<td>{$v.2}</td>
<td>{$v.3}</td>
<td>{$v.4}</td>
<td>{$v.5}</td>
</tr>
{/foreach}
</table>




<br><br><br><b>Bucket-wise distribution</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>Bucket</td>
<td>Count</td>

</tr>
{foreach  from=$bucketStats item=v}
<tr>
<td>{$v.gender}</td>
<td>{$v.bucket}</td>
<td>{$v.count}</td>
</tr>
{/foreach}
</table>


<br><br><br><b>Female Availability Distribution</b><br><br>
<br><br><b>By bucket:</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Bucket</td>
<td>count available</td>
<td>count unavailable</td>
<td>fresh count available</td>
<td>fresh count unavailable</td>

</tr>
{foreach  from=$SlotsFilledStats_bucket key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.free}</td>
<td>{$v.slots_full}</td>
<td>{$v.free_fresh}</td>
<td>{$v.full_fresh}</td>



</tr>
{/foreach}
</table>

<br><br><b>By Age Group:</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Age Group</td>
<td>count available</td>
<td>count unavailable</td>
<td>fresh count available</td>
<td>fresh count unavailable</td>

</tr>
{foreach  from=$SlotsFilledStats_age key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.free}</td>
<td>{$v.slots_full}</td>
<td>{$v.free_fresh}</td>
<td>{$v.full_fresh}</td>

</tr>
{/foreach}
</table>

<br><br><b>By Last Login:</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Last Active Date</td>
<td>count available</td>
<td>count unavailable</td>

</tr>
{foreach  from=$SlotsFilledStats_ll key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.free}</td>
<td>{$v.slots_full}</td> 
</tr>
{/foreach}
</table>

<br><br><b>By city: </b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>city</td>
<td>Females in Bucket 0</td>
<td>Females Unavailable in Bucket 0</td>
<td>Females in Bucket 1</td>
<td>Females Unavailable in Bucket 1</td>
<td>Females in Bucket 2</td>
<td>Females Unavailable in Bucket 2</td>
<td>Females in Bucket 3</td>
<td>Females Unavailable in Bucket 3</td>
<td>Females in Bucket 4</td>
<td>Females Unavailable in Bucket 4</td>
<td>Females in Bucket 5</td>
<td>Females Unavailable in Bucket 5</td>
<td>Total Males </td>

</tr>
{foreach  from=$SlotsFilledStats_city key=k item=v}
<tr>
<td>{$k}</td>
<td>{$v.0.total_count}</td>
<td>{$v.0.filled}</td>
<td>{$v.1.total_count}</td>
<td>{$v.1.filled}</td>
<td>{$v.2.total_count}</td>
<td>{$v.2.filled}</td>
<td>{$v.3.total_count}</td>
<td>{$v.3.filled}</td>
<td>{$v.4.total_count}</td>
<td>{$v.4.filled}</td>
<td>{$v.5.total_count}</td>
<td>{$v.5.filled}</td>
<td>{$v.4.male_count}</td>
</tr>
{/foreach}
</table>



<br><br><br><b>Total Count in User Search: {$count_us}</b>
<br><br><br><b>Last generation of User Search: {$checkpoint}</b>
<br><b>User Search Stats</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Gender</td>
<td>Count</td>
<td>Install_Status</td>
</tr>
{foreach  from=$rows_us item=v}
<tr>
<td>{$v.gender}</td>
<td>{$v.count}</td>
<td>{$v.install_status}</td>
</tr>
{/foreach}
</table>




<br><br><br><b>Last time for bucket resizing: {$last_bucket_generated}</b>
<br><b>Bucket Stats</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>Bucket</td>
<td>PI Male (cut-off)</td>
<td>PI Female (cut-off)</td>

</tr>
{foreach  from=$bucket_log item=v}
<tr>
<td>{$v.bucket}</td>
<td>{$v.pi_male}</td>
<td>{$v.pi_female}</td>
</tr>
{/foreach}
</table>

<br><b>Bucket Changes For Male Users:</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>From (Bucket)</td>
<td>To (Bucket)</td>
<td>Count</td>

</tr>
{foreach  from=$bucket_change_male item=v}
<tr>
<td>{$v.from}</td>
<td>{$v.to}</td>
<td>{$v.count}</td>
</tr>
{/foreach}
</table>


<br><b>Bucket Changes For Female Users:</b><br><br> 
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>From (Bucket)</td>
<td>To (Bucket)</td>
<td>Count</td>

</tr>
{foreach  from=$bucket_change_female item=v}
<tr>
<td>{$v.from}</td>
<td>{$v.to}</td>
<td>{$v.count}</td>
</tr>
{/foreach}
</table>

</body>
</html>
