<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Datelicious Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Datelicious Report </h4>
<h4> No. Of Users </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
<tr>
<th width="200">  </th> 
<th width="200"> City </th>
<th width="200"> Females </th>  
<th width="200"> Males </th>
</tr>
<tr> 
<th width="200"> Dates icon activated </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$icon key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach}
<!-- for sent suggestions (unique users and total count) -->
<tr> 
<th width="200"> (Unique users)Who sent at least one suggestion </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$suggest key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach}
<tr> 
<th width="200"> Total invites sent </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$suggest_all key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach}
<tr> 
<th width="200"> Total invites which were viewed(clicked on tell me more)  </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$viewed key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach}


<!-- (Unique users)Whose invite got accepted -->
<tr> 
<th width="200"> (Unique users)Whose invite got accepted </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$accept_suggest key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach} 

<tr> 
<th width="200"> Total invites that got accepted </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$accept_suggest_all key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach} 
<!-- (Pairs) Who are suggesting more than 1 venues without getting their invite accepted -->
<tr> 
<th width="200"> (Pairs) Who are suggesting more than 1 venues without getting their invite accepted </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$pair_not_accept key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach} 

<!-- (Users) Who are suggesting more than 1 venues without getting their invite accepted -->
<tr> 
<th width="200"> (Users) Who are suggesting more than 1 venues without getting their invite accepted </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

{foreach from=$pair_not_accept_users key=k item=v}
<tr>
<td></td>
<td> {$k} </th>
<td> {$v.F} </th>
<td> {$v.M} </th>
</tr>
{/foreach} 

<!-- (Pairs) Who are suggesting more than 1 venues and decided on a venue — i.e. got a coupon( state = accepted) -->
<tr> 
<th width="200"> (Pairs) Who are suggesting more than 1 venues and decided on a venue </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

<tr>
<td></td>
<td> All Cities </th>
<td> {$pair_accept} </th>
<td>  </th>
</tr>
<!-- (Pairs) Who are getting more than 1 coupons -->
<tr> 
<th width="200"> (Pairs) Who are getting more than 1 coupons </th> 
<th width="200"> </th>
<th width="200">  </th>  
<th width="200">  </th>
</tr>

<tr>
<td></td>
<td> All Cities </td>
<td> {$pair_accept_multi} </td>
<td>  </td>
</tr>
</table>
<h4> Venues </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
<tr>
<th width="200"> Venue </th> 
<th width="200"> City </th>
 <th width="50"> Location Count </th>
<th colspan="2" width="50"> Suggested By Female </th>   
<th colspan="2"width="50"> Suggested By Male </th>  
</tr>
<tr>
<th>  </th>
<th>  </th> 
<th>  </th>
<th> All </th>
<th> Accepted </th> 
<th> All </th> 
<th> Accepted </th>  
</tr>

{foreach from=$venues key=k item=v}
<tr>
<th> {$v.name} </th>
<td  align="center"> {$v.location_deal} </th>
<td  align="center"> {$v.location_count} </th>
<td  align="center"> {$v.fca} </th>
<td  align="center"> {$v.fcu} </th>
<td  align="center"> {$v.mca} </th>
<td  align="center"> {$v.mcu} </th>
</tr>
{/foreach} 

</table>



</body>
</html>