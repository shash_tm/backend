<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Performace Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<h4> Shared Photo Report </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<th> Gender </th>
<th> Total shared Photos </th>
<th> Unique users who shared photos </th>
<th> Unique pairs who shared photos </th>
 </tr>
{foreach from=$stats  item=v}
<tr>
<th> {$v.gender} </th>
<th> {$v.all_photos} </th>
<th> {$v.users} </th>
<th> {$v.pairs} </th>
 </tr>
{/foreach}
</table>
<h4> Users clicking on photo share icon </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<th> Gender </th>
<th> App </th>
<th> Unique users </th>
<th> Total actions </th>
 </tr>
{foreach from=$iconclick  item=v}
<tr>
<th> {$v.gender} </th>
<th> {$v.app} </th>
<th> {$v.users} </th>
<th> {$v.actions} </th>
 </tr>
{/foreach}
</table>
<h4> Users uploading photos </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<th> Status </th>
<th> Gender </th>
<th> App </th>
<th> Unique users </th>
<th> Total actions </th>
 </tr>
{foreach from=$upload  item=v}
<tr>
<th> {$v.status} </th>
<th> {$v.gender} </th>
<th> {$v.app} </th>
<th> {$v.users} </th>
<th> {$v.actions} </th>
 </tr>
{/foreach}
</table>

<h4> Source of photos(camera or gallery) </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<th> Gender </th>
<th> Source </th>
<th> Unique users </th>
<th> Total actions </th>
 </tr>
{foreach from=$source  item=v}
<tr>
<th> {$v.status} </th>
<th> {$v.gender} </th>
<th> {$v.app} </th>
<th> {$v.users} </th>
<th> {$v.actions} </th>
 </tr>
{/foreach}
</table>

<h4> Users who got the photos delivered </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<th> Status </th>
<th> Gender </th>
<th> App </th>
<th> Unique users </th>
<th> Total actions </th>
 </tr>
{foreach from=$deliver  item=v}
<tr>
<th> {$v.status} </th>
<th> {$v.gender} </th>
<th> {$v.app} </th>
<th> {$v.users} </th>
<th> {$v.actions} </th>
 </tr>
{/foreach}
</table>

<h4> Users who downloaded the photos delivered </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<th> Status </th>
<th> Gender </th>
<th> App </th>
<th> Unique users </th>
<th> Total actions </th>
 </tr>
{foreach from=$download  item=v}
<tr>
<th> {$v.status} </th>
<th> {$v.gender} </th>
<th> {$v.app} </th>
<th> {$v.users} </th>
<th> {$v.actions} </th>
 </tr>
{/foreach}
</table>


</body>
</html>
