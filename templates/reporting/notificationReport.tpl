<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Notification Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<br><b>Custom Notifications reporting:</b><br><br>
{foreach from=$data key=k item=source}
<h4>{$k}</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<th>Category</th>
<th>Gender</th>
<th>Sent To</th>
<th>Received By</th>
<th>Clicked and Opened the app</th>
</tr>
{foreach from=$source item=v}
<tr>
       {if $v.type eq 'ACTIVITY'}
         <th> PHOTO </th> 
       {/if}  
       {if $v.type eq 'Authentic_1'}
         <th> Authentic Users 1 Day Old </th>
       {/if}
       {if $v.type eq 'Authentic_3'}
         <th> Authentic Users 3 Days Old </th>
       {/if}
       {if $v.type eq 'Authentic_7'}
         <th> Authentic Users 7 Days Old </th>
       {/if}
       {if $v.type eq 'Authentic_'}
         <th> Authentic Users More than a week Old </th>
       {/if} 
       {if $v.type eq 'Authentic_'}
         <th> Authentic Users More than a week Old </th>
       {/if}
       {if $v.type eq 'low_pi'}
         <th> Authentic Users with Low PI registered 15 day back </th>
       {/if}
       {if $v.type eq 'messages_mutual_likes_1days'}
         <th> Unread messages or unseen mutual likes since last Login 1 day ago </th>
       {/if}
       {if $v.type eq 'messages_mutual_likes_3days'}
         <th> Unread messages or unseen mutual likes since last Login 3 days ago</th>
       {/if}
       {if $v.type eq 'messages_mutual_likes_7days'}
         <th> Unread messages or unseen mutual likes since last Login 7 days ago </th>
       {/if}
       {if $v.type eq 'got_likes_1days'}
         <th> Received like/likes since last Login 1 day ago </th>
       {/if}
       {if $v.type eq 'got_likes_3days'}
         <th> Received like/likes since last Login 3 days ago </th>
       {/if}
       {if $v.type eq 'got_likes_7days'}
         <th> Received like/likes since last Login 7 days ago </th>
       {/if}
       {if $v.type neq 'ACTIVITY' && $v.type neq 'Authentic_1' && $v.type neq 'Authentic_3' && $v.type neq 'Authentic_7'  && $v.type neq 'Authentic_' 
        && $v.type neq 'low_pi'  && $v.type neq 'messages_mutual_likes_1days' && $v.type neq 'messages_mutual_likes_3days' 
        && $v.type neq 'messages_mutual_likes_7days' && $v.type neq 'got_likes_1days' && $v.type neq 'got_likes_3days'  && $v.type neq 'got_likes_7days' } 
         <td>{$v.type}</td>
       {/if}
  
<td>{$v.gender}</td>
<td>{$v.count}</td>
<td>{$v.count_received}</td>
<td>{$v.count_open}</td>
</tr>
{/foreach}
</table>
{/foreach}
</body>
</html>