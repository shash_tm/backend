<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>US Daily Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<h3>Registrations</h3>
<h4>Locations</h4>
{if $m_loc neq null}
    <h4> Cities Male </h4>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="1000">
        <tr>
            <th>City</th>
            {foreach from=$m_loc item=v}
                <td> {$v.city_grp} </td>
            {/foreach}
        </tr>
        <tr>
            <th>Numbers </th>
            {foreach from=$m_loc item=v}
                <td> {$v.c_u}  ( {$v.perc} % ) </td>
            {/foreach}
        </tr>
    </table>
    <h4> Cities Female </h4>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="1000">
        <tr>
            <th>City</th>
            {foreach from=$f_loc item=v}
                <td> {$v.city_grp} </td>
            {/foreach}
        </tr>
        <tr>
            <th>Numbers </th>
            {foreach from=$f_loc item=v}
                <td> {$v.c_u}  ( {$v.perc} % ) </td>
            {/foreach}
        </tr>
    </table>
    <h4> Age groups </h4>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
        <tr>
            <th>Gender</th>
            <th>  18-21  </th>
            <th>  22-24  </th>
            <th>  25-28  </th>
            <th>  29-32  </th>
            <th>  32 +  </th>
        </tr>

        {foreach from=$age item=v}
            <tr>
                <th>{$v.gender}</th>
                <td>{$v.age_18}   ( {$v.age_18_p} % ) </td>
                <td>{$v.age_22}   ( {$v.age_22_p} % ) </td>
                <td>{$v.age_25}   ( {$v.age_25_p} % ) </td>
                <td>{$v.age_29}   ( {$v.age_29_p} % ) </td>
                <td>{$v.age_33}   ( {$v.age_33_p} % ) </td>
            </tr>
        {/foreach}
    </table>
{/if}


<h3>Active Users</h3>
<h4>Locations</h4>
{if $m_loc_active neq null}
    <h4> Cities Male </h4>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="1000">
        <tr>
            <th>City</th>
            {foreach from=$m_loc_active item=v}
                <td> {$v.city_grp} </td>
            {/foreach}
        </tr>
        <tr>
            <th>Numbers </th>
            {foreach from=$m_loc_active item=v}
                <td> {$v.c_u}  ( {$v.perc} % ) </td>
            {/foreach}
        </tr>
    </table>
    <h4> Cities Female </h4>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="1000">
        <tr>
            <th>City</th>
            {foreach from=$f_loc_active item=v}
                <td> {$v.city_grp} </td>
            {/foreach}
        </tr>
        <tr>
            <th>Numbers </th>
            {foreach from=$f_loc_active item=v}
                <td> {$v.c_u}  ( {$v.perc} % ) </td>
            {/foreach}
        </tr>
    </table>
    <h4> Age groups </h4>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
        <tr>
            <th>Gender</th>
            <th>  18-21  </th>
            <th>  22-24  </th>
            <th>  25-28  </th>
            <th>  29-32  </th>
            <th>  32 +  </th>
        </tr>

        {foreach from=$age_active item=v}
            <tr>
                <th>{$v.gender}</th>
                <td>{$v.age_18}   ( {$v.age_18_p} % ) </td>
                <td>{$v.age_22}   ( {$v.age_22_p} % ) </td>
                <td>{$v.age_25}   ( {$v.age_25_p} % ) </td>
                <td>{$v.age_29}   ( {$v.age_29_p} % ) </td>
                <td>{$v.age_33}   ( {$v.age_33_p} % ) </td>
            </tr>
        {/foreach}
    </table>
{/if}

<h3> Sparks </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="300">
    <tr>
        <th align="center"> Price </th>
        <th align="center" colspan="2"> No. of Packages purchased </th>
    </tr>
    <tr>
        <th align="center">  </th>
        <th align="center"> Male </th>
        <th align="center"> Female </th>
    </tr>
    {foreach from=$spark key=k item=v}
        <tr>
            <th align="center"> {$k} </th>
            {if $v|is_array}
                <td align="center"> {$v.M} </td>
                <td align="center"> {$v.F} </td>
            {else}
                <td align="center"  colspan="2"> {$v} </td>
            {/if}
        </tr>
    {/foreach}
</table>

<h3> Engagement </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <th align="center"> Gender 1 </th>
        <th align="center"> Country 1 </th>
        <th align="center"> Country 2 </th>
        <th align="center"  colspan="4"> Likes </th>
        <th align="center"  colspan="3"> Hides </th>
        <th align="center"  colspan="3"> Sparks </th>
        <th align="center"  colspan="3"> Mutual Likes </th>
    </tr>
    <tr>
        <th align="center">  </th>
        <th align="center">  </th>
        <th align="center">  </th>
        <th align="center"> unique user1 </th>
        <th align="center"> unique user2 </th>
        <th align="center"> total action count </th>
        <th align="center"> Inverse PI </th>
        <th align="center"> unique user1 </th>
        <th align="center"> unique user2 </th>
        <th align="center"> total action count </th>
        <th align="center"> unique user1 </th>
        <th align="center"> unique user2 </th>
        <th align="center"> total action count </th>
        <th align="center"> unique user1 </th>
        <th align="center"> unique user2 </th>
        <th align="center"> total action count </th>
    </tr>
    {foreach from=$engage key=k item=v}
        <tr>
            <th align="center"> {$v.gender} </th>
            <th align="center"> {$v.liker_country} </th>
            <th align="center"> {$v.likee_country} </th>
            <td align="center"> {$v.likes.unique_liker} </td>
            <td align="center"> {$v.likes.unique_likee} </td>
            <td align="center"> {$v.likes.all_actions} </td>
            <td align="center"> {$v.likes.inverse_pi} </td>
            <td align="center"> {$v.hides.unique_liker} </td>
            <td align="center"> {$v.hides.unique_likee} </td>
            <td align="center"> {$v.hides.all_actions} </td>
            <td align="center"> {$v.sparks.unique_liker} </td>
            <td align="center"> {$v.sparks.unique_likee} </td>
            <td align="center"> {$v.sparks.all_actions} </td>
            <td align="center"> {$v.mutual_likes.unique_liker} </td>
            <td align="center"> {$v.mutual_likes.unique_likee} </td>
            <td align="center"> {$v.mutual_likes.all_actions} </td>
        </tr>
    {/foreach}
</table>
</body>
</html>