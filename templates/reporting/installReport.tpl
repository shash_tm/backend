<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Source wise Retention</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h3> One Day </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="400">
 
<tr>
<th>
Source
</th>
<th>
Gender
</th>
<th >
Registrations
</th>
<th>
Active Today
</th>
<th>
% retention
</th>
</tr>
{foreach from=$data.1 key=k item=v}
<tr>
<td>
{$v.source}
</td>
<td>
Male
</td>
<td>
{$v.registrations_male}
</td>
<td>
{$v.active_today_male}
</td>
<td>
{$v.ret_percent_male} %
</td>
</tr>
<tr>
<td>
{$v.source}
</td>
<td>
Female
</td>
<td>
{$v.registrations_female}
</td>
<td>
{$v.active_today_female}
</td>
<td>
{$v.ret_percent_female} %
</td>
</tr>


{/foreach}
</table>

<h3> Three Days </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="400">
 
<tr>
<th>
Source
</th>
<th>
Gender
</th>
<th >
Registrations
</th>
<th>
Active Today
</th>
<th>
% retention
</th>
</tr>
{foreach from=$data.3 key=k item=v}
<tr>
<td>
{$v.source}
</td>
<td>
Male
</td>
<td>
{$v.registrations_male}
</td>
<td>
{$v.active_today_male}
</td>
<td>
{$v.ret_percent_male} %
</td>
</tr>
<tr>
<td>
{$v.source}
</td>
<td>
Female
</td>
<td>
{$v.registrations_female}
</td>
<td>
{$v.active_today_female}
</td>
<td>
{$v.ret_percent_female} %
</td>
</tr>

{/foreach}
</table>

<h3> Seven Days </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="400">
 
<tr>
<th>
Source
</th>
<th>
Gender
</th>
<th >
Registrations
</th>
<th>
Active Today
</th>
<th>
% retention
</th>
</tr>
{foreach from=$data.7 key=k item=v}
<tr>
<td>
{$v.source}
</td>
<td>
Male
</td>
<td>
{$v.registrations_male}
</td>
<td>
{$v.active_today_male}
</td>
<td>
{$v.ret_percent_male} %
</td>
</tr>
<tr>
<td>
{$v.source}
</td>
<td>
Female
</td>
<td>
{$v.registrations_female}
</td>
<td>
{$v.active_today_female}
</td>
<td>
{$v.ret_percent_female} %
</td>
</tr>

{/foreach}
</table>





</body>
</html>