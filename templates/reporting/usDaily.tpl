<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>US Daily Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<table align="center" border="1" cellpadding="0" cellspacing="0" width="300">
    <tr>
        <th align="center"> Metric </th>
        <th align="center" colspan="2"> Value </th>
    </tr>
    <tr>
        <th align="center">  </th>
        <th align="center"> Male </th>
        <th align="center"> Female </th>
    </tr>
    {foreach from=$us_data key=k item=v}
        <tr>
            <th align="center"> {$k} </th>
            {if $v|is_array}
               <td align="center"> {$v.M} </td>
               <td align="center"> {$v.F} </td>
            {else}
                <td align="center"  colspan="2"> {$v} </td>
            {/if}
        </tr>
    {/foreach}
</table>
</body>
</html>