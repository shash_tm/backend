<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Performace Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Scenes Report </h4>
<h4> Summary </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
 <tr>
  <th width="200"> City </th>
  <th width="200"> Tutorial </th>
  <th width="200"> Icon Clicked </th>
  <th width="200"> Category </th>
  <th width="200"> Event Details </th>
 </tr>
 {foreach from=$cities key=k  item=v}
 <tr>
  <td> {$k} </td>
  <td> {$v.Tutorial} </td>
  <td> {$v.IconCliked} </td>
  <td> {$v.CategoryClicked} </td>
  <td> {$v.EventDetails} </td>
 </tr>
 {/foreach}
</table>

<h4 Actions> Events</h4>
 <table align="center" border="1" cellpadding="0" cellspacing="0" >
  <tr>
   <th width="500"> Event Name </th>
   <th width="300"> Date created </th>
   <th width="210"> City </th>
   <th width="210"> Category Type </th>
   <th colspan="3" width="210"> Details Viewed </th>
   <th colspan="3 "width="210"> Followed </th>
   <th colspan="3 "width="210"> Shared to Match </th>
   <th colspan="3 "width="210"> Shared outside TM </th>
   {*<th colspan="3 "width="210"> Liked profiles </th>*}
   <th colspan="3 "width="210"> Sparks Sent </th>
  </tr>
  <tr>
   <th>  </th>
   <th>  </th>
   <th>  </th>
   <th>  </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
  </tr>
  {foreach from=$scenes key=k  item=v}
   <tr>
    <td> {$v.name} </td>
    <td> {$v.cd} </td>
    <td> {$v.location} </td>
    <td> {$v.category} </td>
    <td> {$v.details.M} </td>
    <td> {$v.details.F} </td>
    {if $v.details.MF neq null}  <td> {$v.details.MF} % </td>  {else}  <td>  </td>  {/if}
    <td> {$v.follow_event.M} </td>
    <td> {$v.follow_event.F} </td>
    {if $v.follow_event.MF neq null}  <td> {$v.follow_event.MF} % </td> {else}  <td>  </td>  {/if}
    <td> {$v.share_event.M} </td>
    <td> {$v.share_event.F} </td>
    {if $v.share_event.MF neq null}  <td> {$v.share_event.MF} % </td> {else}  <td>  </td>  {/if}
    <td> {$v.share_outside.M} </td>
    <td> {$v.share_outside.F} </td>
    {if $v.share_outside.MF neq null}  <td> {$v.share_outside.MF} % </td> {else}  <td>  </td>  {/if}
    <td> {$v.spark_sent.M} </td>
    <td> {$v.spark_sent.F} </td>
    {if $v.spark_sent.MF neq null}  <td> {$v.spark_sent.MF} % </td> {else}  <td>  </td>  {/if}
   </tr>
  {/foreach}

 </table>


<h4 Actions> Cumulative Events</h4>
 <table align="center" border="1" cellpadding="0" cellspacing="0" >
  <tr>
   <th width="500"> Event Name </th>
   <th width="300"> Date created </th>
   <th width="210"> City </th>
   <th width="210"> Category Type </th>
   <th colspan="3" width="210"> Details Viewed </th>
   <th colspan="3 "width="210"> Followed </th>
   <th colspan="3 "width="210"> Shared to Match </th>
   <th colspan="3 "width="210"> Shared outside TM </th>
   <th colspan="3 "width="210"> Spark sent </th>
  </tr>
  <tr>
   <th>  </th>
   <th>  </th>
   <th>  </th>
   <th>  </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
   <th width="70"> M </th>
   <th width="70"> F </th>
   <th width="70"> F/M </th>
  </tr>
  {foreach from=$cumulative_scenes key=k  item=v}
   <tr>
    <td> {$v.name} </td>
    <td> {$v.cd} </td>
    <td> {$v.location} </td>
    <td> {$v.category} </td>
    <td> {$v.details.M} </td>
    <td> {$v.details.F} </td>
    {if $v.details.MF neq null}  <td> {$v.details.MF} % </td>  {else}  <td>  </td>  {/if}
    <td> {$v.follow_event.M} </td>
    <td> {$v.follow_event.F} </td>
    {if $v.follow_event.MF neq null}  <td> {$v.follow_event.MF} % </td> {else}  <td>  </td>  {/if}
    <td> {$v.share_event.M} </td>
    <td> {$v.share_event.F} </td>
    {if $v.share_event.MF neq null}  <td> {$v.share_event.MF} % </td> {else}  <td>  </td>  {/if}
    <td> {$v.share_outside.M} </td>
    <td> {$v.share_outside.F} </td>
    {if $v.share_outside.MF neq null}  <td> {$v.share_outside.MF} % </td> {else}  <td>  </td>  {/if}
    <td> {$v.spark_sent.M} </td>
    <td> {$v.spark_sent.F} </td>
    {if $v.spark_sent.MF neq null}  <td> {$v.spark_sent.MF} % </td> {else}  <td>  </td>  {/if}
   </tr>
  {/foreach}

 </table>


</body>
</html>