<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Spark payment Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Video Report</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th width="200"> Gender </th>
        <th width="200"> Status </th>
        <th width="200"> Count </th>
    </tr>

    {foreach from=$all_data.data key=k  item=v}
        {if $v|is_array}
            <tr>
                <td align="center"> {$v.gender} </td>
                <td align="center"> {$v.status} </td>
                <td align="center"> {$v.totalCount} </td>

            </tr>
        {else}
            <td></td>
            <th colspan="5" align="center" ></th>
        {/if}
    {/foreach}

</table>

<h4> Errors</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th width="200"> Error </th>
        <th width="200"> Network Type </th>
        <th width="200"> Phone Model </th>
    </tr>

    {foreach from=$all_data.fail_reasons key=k  item=v}
        {if $v|is_array}
            <tr>
                <td align="center"> {$v.error_message} </td>
                <td align="center"> {$v.NetworkClass} </td>
                <td align="center"> {$v.device_name} </td>
            </tr>
        {else}
            <td></td>
            <th colspan="5" align="center" ></th>
        {/if}
    {/foreach}

</table>


<h4>Cumulative Video Count </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th width="200"> Gender </th>
        <th width="200"> Count </th>
    </tr>

    {foreach from=$all_data.cummulative key=k  item=v}
        {if $v|is_array}
            <tr>
                <td align="center"> {$v.gender} </td>
                <td align="center"> {$v.count} </td>
            </tr>
        {else}
            <td></td>
            <th colspan="5" align="center" ></th>
        {/if}
    {/foreach}

</table>


<h4> Total videos pending for admin action: {$all_data.pending} </h4>
<h4> Total unsuccessful attempts: {$all_data.attempt_fail} </h4>
<h4> Total successful attempts: {$all_data.attempt_success}</h4>



</body>
</html>