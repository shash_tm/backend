<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Performace Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Photo Data {$from} to {$to} </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
    <tr>
        <th width="200"> </th>
        <th width="200"> Male </th>
        <th width="200"> Male % </th>
        <th width="200"> Female </th>
        <th width="200"> Female % </th>
    </tr>
    {foreach from=$data_US key=k item=v}
        <tr>
            <th> {$k} </th>
            {if $v|is_array}
                <td align="center"> {$v.M} </td>
                {if $v.M_p neq null }
                    <td align="center"> {$v.M_p} % </td>
                {else}
                    <td></td>
                {/if}
                <td align="center"> {$v.F} </td>
                {if $v.F_p neq null }
                    <td align="center"> {$v.F_p} % </td>
                {else}
                    <td align="center"></td>
                {/if}
            {else}
                <td colspan="4" align="center">{$v}</td>
            {/if}
        </tr>
    {/foreach}
</table>
</body>
</html>