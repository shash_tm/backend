<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Email Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<!--<br><b>User Payment Activity:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">

<tr>
<td>
Visited Payments Page 
</td>
<td>
{$paymentVisitors}
</td>
</tr>

<tr>
<td>
Clicked on Pay 
</td>
<td>
{$payClickers} ({$ids})
</td>
</tr>


<tr>
<td>
Avg PayU Iframe loading time (in sec)
</td>
<td>
{$iFTime}
</td>
</tr>


<tr>
<td>
No. of Users faced errors in PayU Iframe loading  
</td>
<td>
{if !isset($ErrorCount)} 0 {else} $ErrorCount {/if}
</td>
</tr>

</table>


<br><br><br><b>Payments Yesterday:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>
No. of transactions
</td>
<td>
Plan
</td>
<td>
Status</td>
</tr>
{foreach  from=$paymentReport item=v}
<tr>
<td>
{$v.txns}
</td>
<td>
{$v.cost}
</td>
<td>
{$v.status}
</td>
</tr>
{/foreach}

</table>





<br><br><br><b>Payments Details:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>

<td>
Id
</td>

<td>
Response
</td>

</tr>
{foreach  from=$response item=v}
<tr>
<td>
{$v.user_id}
</td>
<td>
{$v.status}
</td>

<td>
{$v.response}
</td>

</tr>
{/foreach} 
</table>
-->
<br><b>Users blocked more than 3 times : </b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<th>User Id </th>
<th>Gender </th>
<th>Name </th>
<th>No. of times blocked</th>
<th>No. of times blocked in last 15 days</th>
<th>Last Reported on</th>
</tr>
{foreach from=$blocks item=v}
<tr>
<td> {$v.abuse_id} </td>
<td> {$v.gender} </td>
<td> {$v.name} </td>
<td> {$v.c_u} </td>
<td> {$v.c_u_fifteen} </td>
<td> {$v.last_rep}</td> 
</tr>
{/foreach}
</table> 




<br><br><br><b>Users who suspended themseleves:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> Id </td>
<td> Name </td>
<td> Gender </td>
<td> Previous Status </td>
<td> Reason </td>
<td> Is Fb Connected? </td>
<td> Email Id </td>
<td> Mobile </td>
<td> Registration Date </td>
<td> Age </td>
<td> City </td>
<td> State </td>
<td> Mutual likes </td>
<td> Two way messages </td>

</tr> {foreach  from=$details item=v} <tr>
<td> {$v.user_id} </td>
<td> {$v.fname} </td>
<td> {$v.gender} </td>
<td> {$v.last_status} </td>
<td> {if !empty($v.reason)} {$v.reason} {else} other {/if} </td>
<td> {$v.is_fb_connected} </td>
<td> {$v.email_id} </td> 
<td> {if !empty($v.user_number)} {$v.user_number} {else} {$v.phone_number} {/if} </td>
<td> {$v.reg_date} </td> 
<td> {$v.age} </td>
<td> {$v.city} </td>
<td> {$v.state} </td>
<td> {$v.likeback} </td>
<td> {$v.msgs} </td>
</tr>
{/foreach}
</table>

<br><b>Summary of Suspended Profiles:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> Count </td>
<td> Gender </td>
<td> Reason </td>
</tr> {foreach  from=$summary item=v} <tr>
<td> {$v.count} </td>
<td> {$v.gender} </td>
<td> {if !empty($v.reason)} {$v.reason} {else} other {/if} </td>
</tr>
{/foreach}
</table>


<br><br><br><b>Abuse Reported Profiles Yesterday:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> Reporting ID </td>
<td> Abuse ID </td>
<td> Reason </td>
</tr> 
{foreach  from=$abuses item=v}
<tr>
<td> {$v.sender_id} </td>
<td> {$v.abuse_id} </td>
<td> {if !empty($v.reason)} {$v.reason} {else} other {/if} </td>
</tr> {/foreach}
</table>


<br><br><br><b>Bad Messages Exchanged Yesterday:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td> Sender ID </td>
<td> Receiver ID </td>
<td> Reason </td>
</tr> 
{foreach  from=$badMsgs item=v}
<tr>
<td> {$v.sender_id} </td>
<td> {$v.receiver_id} </td>
<td> {$v.bad_msg} </td>
</tr>
{/foreach}
</table>

<br><br><br><b> Female Users uninstalled yesterday with phone numbers:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">

<tr>
<th> User Id </th>
<th> Name </th>
<th> Registration date </th>
<th> Phone Number </th>
<th> Age </th>
<th> City </th> 
<th> Mutual likes </th> 
<th> Not Chatted with </th> 
</tr>

{foreach  from=$uninstalls item=v}
<tr>
<td> {$v.user_id} </td>
<td> {$v.nameofuser} </td>
<td> {$v.registration_date} </td>
<td> {$v.user_number} </td>
<td> {$v.age} </td>
<td> {$v.city} </td>
<td> {$v.all_m} </td>
<td> {$v.likes_not_chatted} </td>
</tr>
{/foreach}

</table>


</body>
</html>