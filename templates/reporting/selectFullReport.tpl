<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Spark payment Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Select Report</h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th>  </th>
        <th width="200" colspan="2"> Android </th>
        <th width="200" colspan="2"> IOS </th>
    </tr>
    <tr>
        <th> Metrics </th>
        <th align="center"> Male </th>
        <th align="center"> Female </th>
        <th align="center"> Male </th>
        <th align="center"> Female </th>
    </tr>
    {foreach from=$select_data key=k  item=v}
        {if $v|is_array}
            <tr>
                <th align="center"> {$k} </th>
                {if $v.power eq 'one' }
                    <th colspan="4" align="center" ></th>
                {else}
                    <td align="center"> {$v.android_app.M} </td>
                    <td align="center"> {$v.android_app.F} </td>
                    <td align="center"> {$v.ios_app.M} </td>
                    <td align="center"> {$v.ios_app.F} </td>
                {/if}
            </tr>
        {else}
            <td></td>
            <th colspan="5" align="center" >{$v}</th>
        {/if}
    {/foreach}

</table>
</body>
</html>