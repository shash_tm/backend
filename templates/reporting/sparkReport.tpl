<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Spark payment Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Spark Transactions </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
    <tr>
        <th width="100"> Currency </th>
        <th width="100"> Source </th>
        <th width="100"> Gender </th>
        <th width="100"> Gateway </th>
        {*<th width="100"> Bucket Id </th>*}
        <th width="100"> Price </th>
        <th width="100"> Spark Count </th>
        <th width="100"> Initiated </th>
        <th width="100"> Completed </th>
        <th width="100"> Total Amount </th>
    </tr>
    {foreach from=$payment_data key=k  item=v}
        {if $v.currenc neq 'total'}
            <tr>
                <td align="center" > {$v.currenc} </td>
                <td align="center" > {$v.source} </td>
                <td align="center" > {$v.gender} </td>
                <td align="center"> {$v.payment_gateway} </td>
                {*<td align="center"> {$v.bucket_id} </td>*}
                <td align="center"> {$v.price} </td>
                <td align="center"> {$v.spark_count} </td>
                <td align="center"> {$v.all_transactions} </td>
                <td align="center"> {$v.completed} </td>
                <td align="center"> {$v.total_price} </td>
            </tr>
        {else}
            <tr>
                <th align="center" > {$v.currenc} </th>
                <th align="center"> </th>
                <th align="center"> </th>
                <th align="center"> </th>
                <th align="center"> </th>
                <th align="center"> </th>
                <th align="center"> {$v.all_transactions} </th>
                <th align="center"> {$v.completed} </th>
                <th align="center"> {$v.total_price} </th>
            </tr>
        {/if}
    {/foreach}


</table>
<h4> Errors </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" >
<tr>
        <th width="100"> Type </th>
        <th width="100"> Gateway </th>
        <th width="100"> Error Type </th>
        <th width="100"> Count </th>
        <th width="100"> % for gateway </th>
</tr>
  {foreach from=$error_data key=k  item=v}
  <tr>
      <td align="center"> {$v.activity} </td>
  	<td align="center"> {$v.gate} </td>
  	<td> {$v.error_type} </td>
   	<td align="center"> {$v.err_count} </td>
   	<td align="center"> {$v.percent}  % </td>
   	</tr>
  {/foreach}
</table>
</body>
</html>