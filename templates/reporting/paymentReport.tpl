<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Email Statistics</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<br><b>User Payment Activity:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">

<tr>
<td>
Visited Payments Page 
</td>
<td>
{$paymentVisitors}
</td>
</tr>

<tr>
<td>
Clicked on Pay 
</td>
<td>
{$payClickers}
</td>
</tr>


<tr>
<td>
Avg PayU Iframe loading time (in sec)
</td>
<td>
{$iFTime}
</td>
</tr>


<tr>
<td>
No. of Users faced errors in PayU Iframe loading  
</td>
<td>
{if !isset($ErrorCount)} 0 {else} $ErrorCount {/if}
</td>
</tr>

</table>


<br><br><br><b>Payments Yesterday:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>
No. of transactions
</td>
<td>
Plan
</td>
<td>
Status</td>
</tr>
{foreach  from=$paymentReport item=v}
<tr>
<td>
{$v.txns}
</td>
<td>
{$v.cost}
</td>
<td>
{$v.status}
</td>
</tr>
{/foreach}

</table>





<br><br><br><b>Payments Details:</b><br><br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>

<td>
Id
</td>

<td>
Response
</td>

</tr>
{foreach  from=$response item=v}
<tr>
<td>
{$v.user_id}
</td>
<td>
{$v.status}
</td>

<td>
{$v.response}
</td>

</tr>
{/foreach}
</table>




</body>
</html>