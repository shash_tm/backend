<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Performace Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h4> Photo Data {$from} to {$to} </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
<tr>
<th width="200"> </th> 
<th width="200"> Android FB </th>
<th width="200"> Android FB % </th>
<th width="200"> Android Email </th>
<th width="200"> Android Email % </th>
<th width="200"> IOS </th>
<th width="200">   IOS %   </th>
</tr>
{foreach from=$data key=k item=v}
<tr>
<th> {$k} </th>
<td> {$v.android_fb} </td>
{if $v.android_fb_p neq null }
<td> {$v.android_fb_p} % </td>
{else}
<td></td>
{/if}
<td> {$v.android_email} </td>
{if $v.android_email_p neq null }
<td> {$v.android_email_p} % </td>
{else}
<td></td>
{/if}
<td> {$v.ios_app} </td>
{if $v.ios_app_p neq null }
<td> {$v.ios_app_p} % </td>
{else}
<td></td>
{/if}
</tr>
{/foreach}

</table>


</body>
</html>