<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Performace Report</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<h4> Install/ Uninstall Report </h4>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr> 
<td > Installs </td>
<td > {$install.install} </td>
</tr>
<tr> 
<td > Uninstalls without Registrations </td>
<td > {$install.uninstall} </td>
</tr>
</table>

<h4> Registration/ Uninstall Report </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
<tr> 
<th colspan="4"> Top Cities </th>
<th colspan="4"> Other </th>
</tr>
<tr> 
<th> Gender</th>
<th> Registrations </th>
<th> Uninstalls </th>
<th> Same Day Uninstalls </th>
<th> Gender </th>
<th> Registrations </th>
<th> Uninstalls </th>
<th> Same Day Uninstalls </th>
</tr>
<tr>
<th> Male </th>
<td> {$data.top_ten.M.registration} </td>
<td> {$data.top_ten.M.uninstalls} </td>
<td> {$data.top_ten.M.one_day_uni} </td>
<th> Male </th>
<td> {$data.other.M.registration} </td>
<td> {$data.other.M.uninstalls} </td>
<td> {$data.other.M.one_day_uni} </td>
</tr>
<tr>
<th> Female </th>
<td> {$data.top_ten.F.registration} </td>
<td> {$data.top_ten.F.uninstalls} </td>
<td> {$data.top_ten.F.one_day_uni} </td>
<th> Female </th>
<td> {$data.other.F.registration} </td>
<td> {$data.other.F.uninstalls} </td>
<td> {$data.other.F.one_day_uni} </td>
</tr>

</body>
</html>