<!doctype html>
<html>
<head>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<meta charset="utf-8">
<title>Truly Madly | Home</title>
<!--Stylesheet section start here -->
<link href="{$cdnurl}/images/infog/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="{$cdnurl}/css/login/reset.css" rel="stylesheet"
	type="text/css" />
<link href="{$cdnurl}/css/login/style.css?v=15" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400'
	rel='stylesheet' type='text/css'>
<!--Stylesheet section end here-->
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->
<script>
var server="{$SERVER}",verify="{$verifyEmail}";
var fb_api="{$fb_api}";
var fb_scopes='{$facebook_scope}';

var loginViaEmail = '{$error_loginViaEmail}';
var canNot = '{$error_canNot}';
{if isset($error_login)}
var login_error = '{$error_login}';
{/if}
</script>
</head>
<body>
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  {/literal}
  ga('create', '{$google_analytics_code}', 'trulymadly.com');
  ga('send', 'pageview');
  
</script>


	<!--wrapper secton start here-->
	<div id="wrapper">

		<!--Login popup start here-->
		<div class="loginpopup">
			<div class="content">
				<div class='hide' id='x'></div>
				<a onClick="proceed('login')" class="fblogin">Login with Facebook</a>
				<p class="neverpost">we will never post anything on your Facebook profile</p>
				<p class="hrline"></p>
                
               <!-- Login Form start -->
                <div style="display:block;" id="linfrm">
                
				<p class="signupdetail">or sign in with email</p>
				<div class="formbox">
					<form id='form2' method='post' action='index.php'>
						<input type="text" name='email'
							{if $filled.login} value="{$filled.email}" {/if}
							placeholder="Email ID"  id='div1'>
						<input
							type="password" id='pswd'
							{if $filled.login} value="{$filled.password}"
							{/if} placeholder="Password" name='password'> <input type='hidden' name='login'
							value='1' />
						<div class="genderbox">
                        <label class="error clb">{$error_login}</label>
							<!-- <label class="checkbox" for="checkbox1">
							     <input type="checkbox" value="" id="checkbox1">
							      Keep me logged in </label>  -->
							 <input type="submit"
								class="bluebutton fl" value="Log In" id='login'>
						</div>
					</form>
					<a href="javascript:void(0);" class="forgot" onClick="fpwd()">Forgot password?</a>
				</div>
                
                </div>
                <!-- Login Form end -->
                <!-- Froget password start -->
                <div style="display:none;" id="fpfrm">
                
                
                <div class="formbox">
					<form id='form3' method="post" action="" style="display:block;">
                    <p class="signupdetail">Forgot Password</p>
						<input type="text" name='email' placeholder="Email ID"  id='div3'>
						<div class="genderbox">
                        <p class="fs11 txtgray" style="margin-top:-10px;">Email Id provided by you during registration</p>
                        <label id="error" class="error clb">{$error_login}</label><br>
                        <label id="error1" class="error clb" style="display:none;"></label>
                        <input type="submit" class="bluebutton fl" value="Submit" id='submit'>
                        </div>
					</form>
					<a href="javascript:void(0);" class="fr blue" onClick="btolin()">&laquo; Back to Log In</a>
                  <div style="display:none;" id="fpsmsg">
                    <p class="signupdetail">Check your e-mail.</p>
                        <p class="fs12 clb txtgreen">Reset password instructions has been sent to your email address that is registered with TrulyMadly.<br>
<!--<a href="javascript:void(0);" class="fr blue" onClick="btolin()">&laquo; Back to Log In</a>--> </p>
</div>
                        
						
				</div>
                
                
                </div>
                <!-- Froget password end -->

			</div>
		</div>
		<!--Login popup  end here-->

		<div class="headercontainer">
			<div id="headertop">
				<div class="container">
					<!--Menu-->
					<div class="mainmenu">
						<a class="menu-link" href="#"></a>
						<div class="topnav" id="menu">
							<ul class="clearfix">
								<li class="has-submenu"><a href="#" class="active">Home</a></li>
								<li class="has-submenu"><a href="{$baseurl}/infog/aboutus.html" target="_blank">About Us</a></li>
								<li class="has-submenu"><a href="{$baseurl}/infog/TrustSecurity.html" target="_blank">Trust & Security</a></li> 
                    			<li class="has-submenu"><a href="{$baseurl}/infog/ScientificMatching.html" target="_blank">Scientific Matching</a></li> 
                                <li class="has-submenu"><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li>
                                <li class="has-submenu"><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li>
								<li class="has-submenu"><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
							</ul>
						</div>
					</div>
					<!--Menu END-->
					<div class="logo">
						<img src="{$cdnurl}/images/login/truly-madly-logo.png"
							alt="truly-madly" />
					</div>
                    <div class="hdrsoclink">
<p>Follow us</p><a href="https://www.facebook.com/trulymadly" class="fb" target="_blank"></a><a href="http://blog.trulymadly.com/" class="tmblog" target="_blank"></a>
</div>
<div class="bannerimg"><a href="http://blog.trulymadly.com/breaking-stereotypes/" target="_blank"><img src="{$cdnurl}/images/login/stereotype_banner.gif" alt="Breaking Stereotypes" border="0" /></a></div>
                    
					<div class="loginright"><a href="#" class="bluebutton" id="openPopUp">Log In</a></div>
				</div>
			</div>
			<div class="container top">
				<div class="leftcol">
					<h1>
						Find True Love! <!--</br> <span class="fsi">Such Much</span> -->
					</h1>

					<p>
						Find that special someone with India's<br>
 modern matchmakers.
					</p>
				</div>

				<div class="rightcol">
					<div class="fbloginouter">
						<a onClick="proceed('signup')" class="fblogin">Sign up with Facebook</a>
						<p class="neverpost">we will never post anything on your Facebook profile</p>
                        <p class="error">{$error_canNot}</p>
						<p class="hrline"></p>
						<p class="signupdetail">or sign up with email</p>
						<div class="formbox">
							<form action="index.php" id="form1" method="post" name="lg-form"
								novalidate='novalidate'>
							<div id="namefld"> <input type="text" name='lname' id='lname' value="{$filled.lname}" placeholder="Last name" style=" width:42%; float:right;" onKeyPress="return alpha(event)" tabindex=2>         	
                   <input type="text" id='fname' name='fname' value="{$filled.fname}" placeholder="First name" style=" width:42%; float:left;" onKeyPress="return alpha(event)" tabindex=1>
                   </div>
								<div class="genderbox">
									<label class="gender">I'm a </label> <label class="radio male" id="generr">
										<input tabindex=3 type="radio" {if $filled.gender==
										'M'}  checked="checked" {/if} name="gender" id="gender"
										value="M">
									</label> <label class="radio female"> <input type="radio"
										name="gender" id="gender" {if $filled.gender==
										'F'}  checked="checked" {/if} value="F">
									</label>

									<!--checked=""-->

									<div class="genderusr">
									<p class="fs11 fl">Male</p>
                                    <p class="fs11 fl ml25">Female</p>
                                    </div>
                               
								</div>
                                <div class="clb">&nbsp;</div>
                                	<input tabindex=4 type="text" placeholder="Email Address" name='email'
									{if $filled.registerViaEmail} value="{$filled.email}"
									{/if} id='myDiv2'> <input tabindex=5 type="password"
									placeholder="Password" id='password' name='password'>
                                    <input type="text" tabindex=6 placeholder="Mobile Number" id='mobn' name='mobn' > 
                                    <input
									type='hidden' name='registerViaEmail' value=1 /> 
                                    <div class="clb"></div>
                                    <div id="tncreg" style="position:relative;">
   <label class="fs12 tall" ><input type="checkbox" checked="" name="termAgree" value="accepted" id="termAgree">I have read and agree to the <a target="_blank" href="{$baseurl}/policy.php" class="blue">Privacy Policy</a> and <a target="_blank" href="{$baseurl}/terms.php" class="blue">T &amp; C</a>.</label></div>
                                    <div class="clb"></div>
                                    <p class="error">{$error_alreadyExists}</p> 
						<p class="error">{$error_loginViaFacebook}</p>
						<p class="error">{$error_email_not_fetched}</p>
						<p class="error">{$error_relationshipError}</p>
						<p class="error">{$error_trulio}</p>
						<p class="error">{$error_loginViaEmail}</p>
						<p class="error">{$error_register}</p>
						
                                    <input type="submit" class="bluebutton" value="Sign Up" id='register1'>
							</form>
						</div>
						<br>
					</div>
				</div>
			</div>
			<!--Profile container-->

			<div class="profilecontainer">
				<div class="container">
					<div class="profiledes">
						<img src="{$cdnurl}/images/login/profile-pics.jpg">
						<h2>Verified Profiles</h2>
						<p class="profilecontent">To ensure that all our profiles are
							real, there is a stringent verification process. We authenticate
							ID proofs, social profiles, addresses and employment
							records.</p>
						<div class="devices">
							<img src="{$cdnurl}/images/login/trust-meter_pics.png" />
						</div>
					</div>
				</div>
			</div>
			<!--Real container-->
			<div class="realcontainer">
				<div class="container">
					<div class="box">
						<img src="{$cdnurl}/images/login/true_compatibility_img.jpg"
							alt="Real Compatibility" / class="largesize">
						<h2>True Compatibility</h2>
						<p class="realcontent">We find you the most
							suitable matches based on your preferences, personality
							attributes, likes, dislikes and values. The science behind our
							matching sets TrulyMadly apart from other such services.</p>
						<p class="rcscores">Our Compatibility Assessment Model</p>
						<ul>
							<li>Core Personality</li>
							<li>Interests and Hobbies</li>
							<li>Attitude and Values</li>
							<li>Marital Expectations</li>
							<li>Expression of Love</li>
						</ul>
						<img src="{$cdnurl}/images/login/true_compatibility_img.jpg"
							alt="Real Compatibility" / class="smallsize">
					</div>
				</div>
			</div>

			<!--Safe container-->
			<div class="safecontainer">
				<div class="container">
					<div class="safepic">
						<img src="{$cdnurl}/images/login/private-pics.jpg">
					</div>
					<div class="safefiledes">
						<h2>Private and Safe</h2>
						<p class="safefilecontent">
							Our platform is absolutely private and secure. Your profile can <u>only</u> be viewed by TrulyMadly recommended matches. All your personal contact information is kept confidential and never shared publicly. You can connect at your pace using a mobile, tablet or desktop.<br>
<br>

						</p>
						<div class="devices">
							
							<img src="{$cdnurl}/images/login/devicepic.gif"
								style="float: left" />
						</div>
					</div>
				</div>
			</div>


			<!-- Match step container container-->
			<div class="stepcontainer">
				<div class="container">
					<h2 class="center">The Matching Process</h2>
					<ul class="steps">
						<li>
							<p class="step">
								<img src="{$cdnurl}/images/login/step1.png" />
							</p>
							<!--<h3>Step1:</h3> -->
							<p class="matchdesc mt10">We learn about your personality and
								preferences</p>
						</li>

						<li>
							<p class="step">
								<img src="{$cdnurl}/images/login/step2.png" />
							</p>
							<!--<h3>Step2:</h3> -->
							<p class="matchdesc mt10">We regularly send you the most compatible matches</p>
						</li>

						<li>
							<p class="step">
								<img src="{$cdnurl}/images/login/step3.png" />
							</p>
							<!--<h3>Step3:</h3> -->
							<p class="matchdesc mt10">You review the recommended profiles and connect with them</p>
						</li>

						<li>
							<p class="step">
								<img src="{$cdnurl}/images/login/step4.png" />
							</p>
							<!--<h3>Step4:</h3> -->
							<p class="matchdesc mt10">Find <span style="font-style:italic;">such much</span> love</p>
						</li>
					</ul>

				</div>
			</div>

			<!-- Footer section start here -->
			<div id="footer">
				<div class="container">
					
					<ul class="footernav">
                    <li><a href="{$baseurl}/infog/aboutus.html" target="_blank">About Us</a></li> | 
                    <li><a href="{$baseurl}/infog/TrustSecurity.html" target="_blank">Trust & Security</a></li> | 
                    <li><a href="{$baseurl}/infog/ScientificMatching.html" target="_blank">Scientific Matching</a></li> | 
                    <li><a href="{$baseurl}/terms.php" target="_blank">Terms of Use</a></li> |
                    <li><a href="{$baseurl}/policy.php" target="_blank">Privacy Policy</a></li> | 
				    <li><a href="{$baseurl}/guidelines.php" target="_blank">Safety Guidelines</a></li>
					</ul>
				<div class="joinus">
						<p>Follow us</p> <a href="https://www.facebook.com/trulymadly" class="fb" target="_blank"></a>
                <a href="http://blog.trulymadly.com/" class="tmblog" target="_blank"></a>
						<!--<a href="#" class="in"></a>
            			<a href="#" class="gplus"></a>
						<a href="#" class="twitter"></a> -->
					</div>
                    <p class="copyright">&copy; 2014, trulymadly.com</p>
				</div>
			</div>
			<div id="goto-top"></div>
		</div>
		<!--wrapper secton end here-->
		<!-- Jquery section start-->
<script>
function fpwd()
{
document.getElementById("linfrm").style.display="none";
document.getElementById("fpfrm").style.display="block";
document.getElementById("error").innerHTML = "";
document.getElementById("form3").style.display="block";
document.getElementById("fpsmsg").style.display="none";
}

function btolin()
{
document.getElementById("linfrm").style.display="block";
document.getElementById("fpfrm").style.display="none";
}
</script>
        

        
		<script type="text/javascript"
			src="{$cdnurl}/js/login/jquery-1.10.2.min.js"></script>
		<script type="text/javascript"
			src="{$cdnurl}/js/login/custom_checkbox_and_radio.js"></script>
			<script type="text/javascript" src="{$cdnurl}/js/login/custom.js"></script>
			<script type="text/javascript" src="{$cdnurl}/js/login/core.js?v=22"></script>
			<script type="text/javascript" src="{$cdnurl}/js/login/jquery.validate.min.js"></script>
	<!--<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>-->
	
			<script id="_webengage_script_tag" type="text/javascript">
			  var _weq = _weq || {};
			  _weq['webengage.licenseCode'] = '76a9cca';
			  _weq['webengage.widgetVersion'] = "4.0";
			  
			  (function(d){
			    var _we = d.createElement('script');
			    _we.type = 'text/javascript';
			    _we.async = true;
			    _we.src = (d.location.protocol == 'https:' ? "https://ssl.widgets.webengage.com" : "http://cdn.widgets.webengage.com") + "/js/widget/webengage-min-v-4.0.js";
			    var _sNode = d.getElementById('_webengage_script_tag');
			    _sNode.parentNode.insertBefore(_we, _sNode);
			  })(document);
			</script>
</body>
</html>
