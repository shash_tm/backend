<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mailer DashBoard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .ques-body {
            background:#F7F7F9;
        }

    </style>
    {include file='../deals/adminIncludes.tpl'}

</head>
<body>
{include file='../deals/header.tpl'}



    {*<input type="hidden" id="action-url" value="{$baseurl}/quizAdmin/quizFormAction.php" />*}
    {*<input type="hidden" id="image-upload-url" value="{$baseurl}/quizAdmin/photoUploadUtil.php" />*}
    <input type="hidden" id="baseurl" value="{$baseurl}">
    {*<input type="hidden"  id="read-only-val"  value="{$read_only}" />*}

<div class="container-fluid check-me">
    <table class="table table-responsive" id="quiz-list">
        <thead>
        <tr>
            <th>Campaign ID</th>
            <th>Campaign Name </th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$campaigns item=campaign}
        <tr>
            <td> {$campaign.campaign_id}</td>
            <td>  {$campaign.campaign_name}</td>
            <td>  {$campaign.status}</td>
            <td>  <a href="{$baseurl}/mailers/mailerWizard.php?campaign_id={$campaign.campaign_id}" data-toggle="tooltip" title="View" >
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                </a></td>
        </tr>
        {/foreach}
        </tbody>
    </table>
</div>




</body>
</html>
