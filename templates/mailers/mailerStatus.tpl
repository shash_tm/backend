<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mailer DashBoard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .ques-body {
            background:#F7F7F9;
        }

    </style>
    {include file='../deals/adminIncludes.tpl'}
</head>
<body>
{include file='../deals/header.tpl'}
<input type="hidden" id="baseurl" value="{$baseurl}">
<input type="hidden" id="campaign_id" value="{$campaign.campaign_id}">

<div class="col-md-6 col-lg-6 col-sm-6">
    <div class="panel panel-default ">
        <div class="panel-heading">
            <h3 class="panel-title">Campaign Sent</h3>
        </div>
        <div class="panel-body" id="campaign-status">
            <div class="form-group final-status">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Campaign Id </span>
                    <input class="form-control " placeholder="" aria-describedby="basic-addon1"  name="gender" value="{$campaign.campaign_id}">
                    </input>
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Campaign Name </span>
                    <input class="form-control " placeholder="" aria-describedby="basic-addon1"  name="gender" value="{$campaign.campaign_name}">
                    </input>
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Subject </span>
                    <input class="form-control " placeholder="" aria-describedby="basic-addon1"  name="gender" value="{$campaign.mail_subject}">
                    </input>
                </div>
                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Sent to </span>
                                    <input class="form-control " placeholder="" aria-describedby="basic-addon1"  name="gender" value="{$campaign.sent_to}">
                                    </input>
                                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Opened by </span>
                    <input class="form-control " placeholder="" aria-describedby="basic-addon1"  name="opened_by" value="{$campaign.opened_by}">
                    </input>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6 col-lg-6 col-sm-6">
    <div class="panel panel-default html-hide">
        {$campaign.html}
    </div>
</div>

<script src="{$baseurl}/js/mailers/mailerWizard.js"></script>

</body>
</html>
