<!DOCTYPE html>
<html lang="en">
<head>
<title>Mailer DashBoard</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.ques-body {
	background: #F7F7F9;
}

textarea {
	width: 100% !important;
}

.max-width-button {
	width: 100%;
}

.panel-heading h3 {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	line-height: normal;
	width: 75%;
	padding-top: 8px;
}
</style>
{include file='../deals/adminIncludes.tpl'}
</head>
<body onload="update(); document.f.ta.select();">
	{include file='../deals/header.tpl'}
	<div class="container-fluid">

		<input type="hidden" id="baseurl" value="{$baseurl}"> <input
			type="hidden" id="image-url-new" value="{$imageurl}"> <input
			type="hidden" id="campaign_id" value="{$campaign.campaign_id}">
		<div class="col-md-12 col-lg-12 col-sm-12">
			<div class="page-header">
				<h1>
					Mailer Campaign : <small>{$campaign.campaign_name}</small>
				</h1>
			</div>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">Enter
						Subject</span> <input type="text" id="subject" class="form-control"
						placeholder="Subject for the mailer"
						aria-describedby="basic-addon1" value="{$campaign.mail_subject}"
						name="Subject" />
				</div>
			</div>
			<form name="f" method="post" target="dynamicframe">
				<script src="{$baseurl}/js/mailers/mailerWizard.js"></script>
				<script src="{$baseurl}/js/mailers/jsQuickTags.js"</script>
				<script type="text/javascript">edToolbar('canvas');</script>

				<div class="panel panel-default ">
					<div class="panel-heading">
						<h3 class="panel-title pull-left">Email HTML</h3>

						<button type="button" class="btn btn-success pull-right btn-info"
							data-toggle="modal" data-target="#image-upload-modal">
							Upload Image <span class="glyphicon glyphicon-envelope"
								aria-hidden="true"></span>
						</button>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body" id="deal-image-panel-body">
						<div class="form-group" style="width: 100% !important;">
							<div class="input-group" style="width: 100% !important;">
								<textarea id="canvas" name="ta" rows="10" cols="140"
									class="form-control" placeholder="Your HTML Goes here"
									aria-describedby="basic-addon1">
                                       {$campaign.html}
                                    </textarea>
							</div>
						</div>
					</div>
				</div>

			</form>

			<iframe name="dynamicframe" id="dynamicframe" width="100%"
				height="500" src="javascript:&#39;-&#39;"></iframe>
		</div>

		<div class="col-md-6 col-lg-6 col-sm-6">
			<div class="panel panel-default ">
				<div class="panel-heading">
					<h3 class="panel-title">Send Test Email</h3>
				</div>
				<div class="panel-body" id="deal-image-panel-body">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">Enter
								Email</span> <input type="text" id="test-email-receiver"
								class="form-control" placeholder="Test email id"
								aria-describedby="basic-addon1" value="" name="test-email" />
						</div>
					</div>
					<div class="btn-group max-width-button" role="group"
						id="add-location-button-group">
						<button type="button" class="btn btn-success max-width-button"
							id="send-test-email">
							Send <span class="glyphicon glyphicon-envelope"
								aria-hidden="true"></span>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6 col-lg-6 col-sm-6">
			<div class="panel panel-default save-and-proceed">
				<div class="panel-heading">
					<h3 class="panel-title">Mail OK ?</h3>
				</div>
				<div class="panel-body" id="deal-image-panel-body">

					<div class="btn-group" role="group" id="add-location-button-group">
						<button type="button" class="btn btn-success"
							id="save-mail-content">
							Save and Proceed <span class="glyphicon glyphicon-envelope"
								aria-hidden="true"></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="image-upload-modal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Upload Image</h4>
				</div>
				<div class="modal-body">
					<input type="file" class="attach-image">
					<button class="upload-image-server" style="display: none;">Upload</button>
					<img class="mail-image-tag to-show" src="" style="display: none;"
						data-mail-image="" id="image-server" width='100'>
					<div class="form-group to-show" style="display: none;">
						<div class="input-group">
							<span class="input-group-addon to-show" id="basic-addon1"
								style="display: none;">Copy url</span> <input type="text"
								class="form-control"  value="NO value"
								placeholder="some-image" id="image-url-only">
						</div>
						<h3>Or </h3>
						<button type="button" class="btn btn-default"
							style="display: none;" id='use-image'>Send this Image only</button>
					</div>
				</div>

				<div class="modal-footer">
				   <button type="button"  align='left' class="btn btn-default to-show" id="image-data-clear" style="display: none;">Clear</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>


</body>
</html>
