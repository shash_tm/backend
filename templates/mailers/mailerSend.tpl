<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mailer DashBoard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    {*{include file='../deals/adminIncludes.tpl'}*}

</head>
<body>
{include file='../deals/header.tpl'}
<input type="hidden" id="baseurl" value="{$baseurl}">
<input type="hidden" id="campaign_id" value="{$campaign.campaign_id}">



        <div class="col-md-6 col-lg-6 col-sm-6">
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <h3 class="panel-title">Send to users</h3>
                </div>
                <div class="panel-body" id="deal-image-panel-body">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Gender</span>
                            <select class="form-control " placeholder="" aria-describedby="basic-addon1" id="gender" name="gender">
                                <option selected value="" class="gender-val">Both</option>
                                <option value="M" class="gender-val">Males</option>
                                <option value="F" class="gender-val">Females</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Minimum Age</span>
                            <input type="text" class="form-control" placeholder="Minimum age of target user" aria-describedby="basic-addon1" value="" id="min-age"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Maximum Age</span>
                            <input type="text" class="form-control" placeholder="Maximum age of target user" aria-describedby="basic-addon1" value="" id="max-age"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="cities-addon1">Cities</span>
                            <select class="form-control chosen-select" multiple data-placeholder="All Cities" id="cities" name="cities" aria-describedby="cities-addon1">

                            </select>
                        </div>
                    </div>

					<div class="btn-group" role="group" id="add-location-button-group">
                        <button type="button" class="btn btn-success"  id="count-users">Count target users
                            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="btn-group" role="group" id="add-location-button-group">
                        <button type="button" class="btn btn-success"  style="display: none;" id="send-user-data">Send
                            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

<div class="col-md-6 col-lg-6 col-sm-6">
<div class="panel panel-default ">
    <div class="panel-heading">
        <h3 class="panel-title">Send to a list of Emails</h3>
    </div>
    <div class="panel-body" id="deal-image-panel-body">
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Enter the E-mail Ids</span>
                <input type="text" class="form-control" placeholder="Enter comma separated Email Ids here" aria-describedby="basic-addon1" value="" id="email-list"/>
            </div>
        </div>




        <div class="btn-group" role="group" id="add-location-button-group">
            <button type="button" class="btn btn-success"  id="send-to-emails">Send
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
            </button>
        </div>
    </div>
</div>
</div>

{include file='../deals/adminIncludes.tpl'}
<script src="{$baseurl}/js/utilities/chosen.jquery.min.js"></script>
<script src="{$baseurl}/js/mailers/mailerWizard.js"></script>
<link rel="stylesheet" type="text/css" href="{$baseurl}/css/common/chosen.min.css">
</body>
</html>
