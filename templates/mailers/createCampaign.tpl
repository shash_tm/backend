<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mailer DashBoard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .ques-body {
            background:#F7F7F9;
        }

    </style>
    {include file='../deals/adminIncludes.tpl'}
</head>
<body>
{include file='../deals/header.tpl'}
<input type="hidden" id="baseurl" value="{$baseurl}">
{*<h2> Create A New E-mail Campaign</h2>*}


<div class="col-md-6 col-lg-6 col-sm-6">{**}
    <div class="panel panel-default ">
        <div class="panel-heading">
            <h3 class="panel-title">Create A New E-mail Campaign</h3>
        </div>
        <div class="panel-body" id="campaign-create-body">

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Campaign Name</span>
                    <input type="text" class="form-control" placeholder="Give a unique name for your campaign" aria-describedby="basic-addon1" value="" id="Campaign-name-input"/>
                </div>
            </div>
            <div class="btn-group" role="group" id="add-location-button-group">
                <button type="button" class="btn btn-success"  id="save-form-action">Save
                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                </button>
            </div>
        </div>
    </div>
</div>


<script src="{$baseurl}/js/mailers/mailerWizard.js"></script>

</body>
</html>
