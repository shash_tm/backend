<!DOCTYPE html>
<html lang="en">
<head>
	<title>View/Edit Quiz</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{$baseurl}/css/common/bootstrap.min.css">
	<script src="{$baseurl}/js/common/jquery.min.js"></script>
	<script src="{$baseurl}/js/common/bootstrap.min.js"></script>
	{include file='../deals/adminIncludes.tpl'}
	<style>
		.ques-body {
			background:#F7F7F9;
		}

	</style>
</head>
<body>
	{include file='../deals/header.tpl'}
	<div class="container-fluid check-me">
		<input type="hidden" id="quiz-id" value="{$quiz.quiz_id}" />
		<input type="hidden" id="action-url" value="{$baseurl}/quizAdmin/quizFormAction.php" />
		<input type="hidden" id="image-upload-url" value="{$baseurl}/quizAdmin/photoUploadUtil.php" />
		<input type="hidden" id="image-url-new" value="{$imageurl}">
		<input type="hidden"  id="read-only-val"  value="{$read_only}" />
		<input type="hidden"  id="quiz-view-url"  value="{$baseurl}/quizAdmin/quizView.php" />
		<input type="hidden"  id="base-url"  value="{$baseurl}" /> 
		<input type="hidden"  id="select-state-id"  value="{$quiz.state_id}" /> 
		<input type="hidden"  id="select-city-id"  value="{$quiz.city_id}" /> 
		<div class="page-header">
		  <h2>{$quiz.display_name}</h2>
		</div>
			<div class="col-md-6 col-lg-6 col-sm-6" id="quiz-content-section">

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Quiz Name</span>
						<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$quiz.display_name}" name="display_name" id="quiz-display-name"/>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Comment</span>
						<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$quiz.title}" name="title" id="quiz-title"/>
					</div>
				</div>
				
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">State</span>
						  <select class="form-control" placeholder="" aria-describedby="basic-addon1" name="state_id" id="state-select">
						  	
						  </select>
						</div>
					</div> 
				
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Image</span>
						<img class="quiz-image-tag" src="{$imageurl}quiz/{$quiz.image}" data-quiz-image="{$quiz.image}" id="image-url" width="200">
						{if $read_only neq 'true'}
						<input type="file" class="attach-photo" data-type-image="image">
						<button class="upload-image" style="display: none;">Upload</button>
						{/if}
					</div>
				</div>
			

				<input type="hidden"  value="{$quiz.quiz_id}" name="quiz_id"  />
				<input type="hidden"  value="{$quiz.banner}" name="banner" id="banner" />
				<input type="hidden"  value="{$quiz.image}" name="image" id="image"/>
				<input type="hidden"  value="{$cdnurl}" name="cdnurl" />
			</div>

			<div class="col-md-6 col-lg-6 col-sm-6" id="quiz-content-section-new" >
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Description</span>
						<input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="{$quiz.description}" name="description" id="quiz-description"/>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Status</span>
						<input type="text" class="form-control" placeholder="" readonly aria-describedby="basic-addon1" value="{$quiz.status}" name="status" id="quiz-status"/>
					</div>
				</div>
				<div class="form-group">	
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1">City</span>
						  <select class="form-control" placeholder="" aria-describedby="basic-addon1" name="city_id" id="city-select">
						  </select>
						</div>
					</div>
				
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">Banner</span>
						<img  class="quiz-image-tag" src="{$imageurl}quiz/{$quiz.banner}"  data-quiz-image="{$quiz.banner}" id="banner_url" width="500">
						{if $read_only neq 'true'}
						<input type="file" class="attach-photo" data-type-image="banner">
						<button class="upload-image" style="display: none;">Upload</button>
						{/if}
					</div>
				</div>
			</div>

			<div class="col-md-12 col-lg-12 col-sm-12 quiz-body">
				{foreach from=$quiz.questions item=question}
					<div class="panel panel-default">
						<div class="panel-body ques-body">
							<div class="col-md-8 col-lg-8 col-sm-8">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Question</span>
										<input type="text" class="form-control question" placeholder="" aria-describedby="basic-addon1"
													 value="{$question.question_text}" data-question-id="{$question.question_id}" data-question-text="{$question.question_text}"/>
									</div>
								</div>
							</div>

							<div class="col-md-1 col-lg-1 col-sm-1">
								<div class="form-group">
									<div class="input-group">
										<select class="form-control question-status" placeholder="" aria-describedby="basic-addon1">
											<option selected value="active" class="question-status-val">active</option>
											<option value="deleted" class="question-status-val">deleted</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">Rank</span>
										<input type="text" class="form-control question-rank" placeholder="" aria-describedby="basic-addon1"
											   value="{$question.rank}"/>
									</div>
								</div>
							</div>
							<div class="col-md-1 col-lg-1 col-sm-1">
								<div class="form-group">
									<div class="input-group">
										<input type ="hidden" class="ques-type" data-ques-type="{$question.option_type}" />
										<select class="form-control question_type" placeholder="" aria-describedby="basic-addon1">
											<option value="image" class="question-type-val">image</option>
											<option selected value="text" class="question-type-val">text</option>
										</select>
									</div>
								</div>
							</div>



								{foreach from=$question.options item=option}
									<div class="col-md-3 col-lg-3 col-sm-3">
										<div class="form-group">
											<input type ="hidden" class="ques-type-for-option" data-ques-type-option="{$question.option_type}" />
											<div class="input-group option-text-area" >
												<span class="input-group-addon" id="basic-addon1">{$option.option_id}</span>
														<input type="text" class="form-control option" placeholder="" aria-describedby="basic-addon1"
																 value="{$option.option_text}" data-option-id="{$option.option_id}" data-option-text="{$option.option_text}"/>
											</div>
											<div class="input-group option-image-area" >
												<span class="input-group-addon" id="basic-addon1">{$option.option_id}</span>

												<img  class="option option-image-tag" src="{$imageurl}quiz/{$option.option_image}" data-option-id="{$option.option_id}"
													  data-option-image="{$option.option_image}" width="200" height="200">
												{if $read_only neq 'true'}
												<input type="file" class="attach-photo" data-type-image="image">
												<button class="upload-image" style="display: none;">Upload</button>
												{/if}
											</div>
										</div>

									</div>
								{/foreach}
						</div>
					</div>
				{/foreach}
				{if $read_only neq 'true'}
				<div class="btn-group" role="group" id="add-new-question-group">
					<button type="button" class="btn btn-success" id="add-new-question">Add Questions &nbsp;&nbsp;
						<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
					</button>
				</div>
			</div>
		<div class="btn-group col-md-12 col-lg-12 col-sm-12" role="group" >
			<button type="button" class="btn btn-primary col-md-12 col-lg-12 col-sm-12" id="save-form-action">Save</button>
			<div class="progress">
				<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					<span class="sr-only">Saving</span>
				</div>
			</div>
		</div>
	</div>
	{/if}

	<script src="{$baseurl}/js/quizAdmin/quizForm.js"></script>
</body>
</html>
