<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Untitled Document</title>
<link href="{$cdnurl}/css/register_new.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css">
<script>
var server='{$SERVER}';//"http://dev.trulymadly.com/trulymadly";
var fb_api="{$fb_api}";
var fb_scopes='{$facebook_scope}';
var loginViaEmail = '';
var canNot = '';
var baseurl = "{$baseurl}";//"http://dev.trulymadly.com/trulymadly";
</script>

</head>
<body>
<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">

<!-- Top progress section end -->
<!-- Form section start -->
	<div class="regformsec">
	You will need to verify yourself with Facebook to Like your matches<br>
	
	<p class="error" id="error_fb"></p>
	<img id="loaderfb" src='/images/spinnerLarge.gif' style="display:none"/>
    <a id="fbimage" class="regfbconnect" onClick="Trulymadly.login.registerfb();"><i class="icon-facebook-sign"></i>SignUp with <strong>Facebook</strong></a><br>
    *and get verified instantly<div class="orline"><span class="ortxt">OR</span></div>Will verify myself later once I see the matches<br>
    <span class="notetxt">NOTE: you can view matches but not take any action</span><br><br>	

<div class="formreg">
   <form action="" id="form1" method="post" name="lg-form" novalidate='novalidate'>
        <p>Email</p>
        <input name="email" id="email" type="text" class="regtxtbox" placeholder="Enter your email id">
        <p>First Name</p>
        <input name="fname" id="fname" type="text" class="regtxtbox" placeholder="Enter your first name">
        <p>Last Name</p>
        <input name="lname" id="lname" type="text" class="regtxtbox" placeholder="Enter your last name">
        <p>Gender</p>
        <ul>
		<li class="fl"><input class="cutomradio" type="radio" name="gender" id="gender" value="M" ><label>Male</label></li>
        <li class="fr"><input class="cutomradio" type="radio" name="gender" id="gender" value="F" ><label>Female</label></li>
        </ul>
        <input type='hidden' name='registerViaEmail' value=1 /> 
        <p>Password</p>
        <input name="password" id="password" type="password" class="regtxtbox" placeholder="Password">
        <p class="error" id="error"></p>
        <img id="loaderCon" src='/images/spinnerLarge.gif' style="display:none"/>
        <input id="continue" type="button" name ="send" value="Continue" class="continuebtn" onclick="return Trulymadly.login.onContinue(this.form);" >
       <!-- <a onclick = "return Trulymadly.login.onContinue(this.form);" class="continuebtn">Continue <span>|&nbsp;<i class="icon-chevron-right"></i></span></a>-->
    </form>
</div>
  </div>  
<!-- Form section end -->
</div>
<!--Centered Container End -->
</div>
<script type="text/javascript" src="{$cdnurl}/js/login/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="{$cdnurl}/js/login/jquery.validate.min.js"></script>
<script type="text/javascript" src="{$cdnurl}/js/lib/icheck.js"></script>
<script type="text/javascript" src="{$cdnurl}/js/register/trulymadly.js"></script>
<script type="text/javascript" src="{$cdnurl}/js/login/core_login.js?v=1.1.2"></script>
</body>
</html>
