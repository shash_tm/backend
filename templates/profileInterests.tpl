
                <!--Carousel-->
                <div class="mt20 clearfix">
                <h2>More About Me</h2>
                <div>
                <p class="subjq">Three things that I am grateful for are... </p>
                <p class="subja">"{$aboutMe.unique_text}"</p>
                <p class="subjq">My idea of a perfect date is...</p>
                <p class="subja">"{$aboutMe.weekend_text}"</p>
                <p class="subjq">My friends would describe me as...</p>
                <p class="subja">"{$aboutMe.friend_text}"</p>
                </div>
                
                
                   <h2 class="mt40">Interest and Hobbies</h2>
                   
                 {if !isset($smarty.get.me)}
                    <!--Matched Hobbies--> 
                    <div class="hobbies-matched clearfix mt20">
                        <div class="title"><span>Common between two of us</span></div>
                        <div class="your-img"><img src="{$header.user_image}" alt="" class="inthobimg"/></div>
                        <ul class="matched-list">
                            <li>
                            {if isset($commonInterests.common_music_preference) || isset($commonInterests.common_music_favorites)} 
                                <span class="music"></span>
                                <p>{$commonInterests.common_music_preference}</p>
                                 <p>{$commonInterests.common_music_favorites}</p>
                            {else}
                                 <span class="music iconopcty "></span>
                                 {/if}
                            </li>
                            <li>
                            {if isset($commonInterests.common_books_preference) || isset($commonInterests.common_books_favorites)}
                                <span class="books"></span>
                              <p>{$commonInterests.common_books_preference}</p>
                                 <p>{$commonInterests.common_books_favorites}</p>
                                 {else}
                                 <span class="books iconopcty "></span>
                                 {/if}
                            </li>
                             <li>
                            {if isset($commonInterests.common_movies_favorites) || isset($commonInterests.common_movies_preference)}
                                <span class="movies"></span>
                              <p>{$commonInterests.common_movies_preference}</p>
                                 <p>{$commonInterests.common_movies_favorites}</p>
                                 {else}
                                 <span class="movies iconopcty "></span>
                                 {/if}
                            </li>
                            <li>
                            {if isset($commonInterests.common_food_preference) || isset($commonInterests.common_food_favorites)}
                                <span class="food"></span>
                               <p>{$commonInterests.common_food_preference}</p>
                                 <p>{$commonInterests.common_food_favorites}</p>
                                 {else}
                                 <span class="food iconopcty "></span>
                                 {/if}
                            </li>
                            
                            <li>
                            {if isset($commonInterests.common_sports_preference) || isset($commonInterests.common_sports_favorites)}
                                <span class="sports"></span>
                               <p>{$commonInterests.common_sports_preference}</p>
                                 <p>{$commonInterests.common_sports_favorites}</p>
                                 {else}
                                 <span class="sports iconopcty "></span>
                                 {/if}
                            </li>
                           
                             <li>
                            {if isset($commonInterests.common_travel_preference) || isset($commonInterests.common_travel_favorites)}
                                <span class="travel"></span>
                               <p>{$commonInterests.common_travel_preference}</p>
                                 <p>{$commonInterests.common_travel_favorites}</p>
                                 {else}
                                 <span class="travel iconopcty "></span>
                                 {/if}
                            </li>
        
        				 <li>
                            {if isset($commonInterests.common_other_preference)}
                                <span class="other"></span>
                               <p>{$commonInterests.common_other_preference}</p>
                                 {else}
                                 <span class="other iconopcty "></span>
                                 {/if}
                            </li>
                        </ul>
                        <div class="matched-img"><img src="{$attributes.profile_pic}"  alt="" class="inthobimg"/></div>
                  </div>  
                  {/if}
                
                    <div class="profile-hobbies mt20">
                     
                        <ul class="bxslider">
                           
                           {if isset($InterestHobbies.music_preferences) || isset($InterestHobbies.music_favorites)}
                            <li>
                                <div class="profile-topwhite">
                                    <p class="profile-selectedicon music"></p>
                                    <p class="profile-hobbytitle" style="color:#ff4141;">Music</p>
                                </div>
                                <div class="profile-graybg" style="border-top:1px solid #ff4141;">
                                  {if isset($InterestHobbies.music_preferences)}
                                    <p class="profile-htitle">Like</p>
                                    <p class="phobovrautosmall">{$InterestHobbies.music_preferences}</p>
                                    {/if}
                                    {if isset($InterestHobbies.music_favorites)}
                                    <p class="profile-htitle mt10">Favourite</p>
                                    <p class="phobovrauto">{$InterestHobbies.music_favorites}</p>
                                    {/if}
                                </div>
                            </li>
                             {/if}
                             
       
       						 {if isset($InterestHobbies.books_preferences) || isset($InterestHobbies.books_favorites)}
                            <li>
                                <div class="profile-topwhite">
                                <p class="profile-selectedicon reading"></p>
                                <p class="profile-hobbytitle" style="color:#3fc0ea;">Reading</p>
                                    </div>
                                <div class="profile-graybg" style="border-top:1px solid #3fc0ea;">
                                {if isset($InterestHobbies.books_preferences)}
                                    <p class="profile-htitle">Like</p>
                                    <p class="phobovrautosmall">{$InterestHobbies.books_preferences}</p>
                                    {/if}
                                    {if isset($InterestHobbies.books_favorites)}
                                    <p class="profile-htitle mt10">Favourite</p>
                                    <p class="phobovrauto">{$InterestHobbies.books_favorites}</p>
                                    {/if}
                                </div>
                            </li>
                             {/if}
                            
                            
                             {if isset($InterestHobbies.movies_preferences) || isset($InterestHobbies.movies_favorites)}
                            <li>
                                <div class="profile-topwhite">
                                 <p class="profile-selectedicon movies"></p>
                                    <p class="profile-hobbytitle" style="color:#e13cff;">Movies</p>
                                   
                                </div>
                                <div class="profile-graybg" style="border-top:1px solid #e13cff;">
                                    {if isset($InterestHobbies.movies_preferences)}
                                    <p class="profile-htitle">Like</p>
                                    <p class="phobovrautosmall">{$InterestHobbies.movies_preferences}</p>
                                    {/if}
                                     {if isset($InterestHobbies.movies_favorites)}
                                    <p class="profile-htitle mt10">Favourite</p>
                                    <p class="phobovrauto">{$InterestHobbies.movies_favorites}</p>
                                    {/if}
                                </div>
                               </li>
                                {/if}
                                
                            {if isset($InterestHobbies.food_preferences) || isset($InterestHobbies.food_favorites)}
                            <li>
                                <div class="profile-topwhite">
                                <p class="profile-selectedicon eating"></p>
                                    <p class="profile-hobbytitle" style="color:#717bff;">Eating</p>
                                    
                                 </div>
                                 <div class="profile-graybg" style="border-top:1px solid #717bff;">
                                 	{if isset($InterestHobbies.food_preferences)}
                                 	 <p class="profile-htitle">Like</p>
                                    <p class="phobovrautosmall">{$InterestHobbies.food_preferences}</p>
                                     {/if}
                                   {if isset($InterestHobbies.food_favorites)}
                                    <p class="profile-htitle mt10">Favourite</p>
                                  <p class="phobovrauto">{$InterestHobbies.food_favorites}</p>
                                  {/if}
                                </div>
                            </li>
                             {/if}
                                                     
                           
                               
                                {if isset($InterestHobbies.sports_preferences) || isset($InterestHobbies.sports_favorites)}
                               <li> 
                                  <div class="profile-topwhite">
                                  <p class="profile-selectedicon sports"></p>
                                    <p class="profile-hobbytitle" style="color:#aad632;">Sports</p>
                                    
                                </div>
                                <div class="profile-graybg" style="border-top:1px solid #aad632;">
                                  {if isset($InterestHobbies.sports_preferences)}
                                    <p class="profile-htitle">Like</p>
                                    <p class="phobovrautosmall">{$InterestHobbies.sports_preferences}</p>
                                    {/if}
                                   {if isset($InterestHobbies.sports_favorites)}
                                    <p class="profile-htitle mt10">Favourite</p>
                                    <p class="phobovrauto">{$InterestHobbies.sports_favorites}</p>
                                    {/if}
                                </div>
                            </li>
                             {/if}
                            
                            {if isset($InterestHobbies.travel_preferences) || isset($InterestHobbies.travel_favorites)}
                             <li> 
                                  <div class="profile-topwhite">
                                  <p class="profile-selectedicon travel"></p>
                                    <p class="profile-hobbytitle" style="color:#ffae00;">Travel</p>
                                    
                                </div>
                                <div class="profile-graybg" style="border-top:1px solid #ffae00;">
                                   {if isset($InterestHobbies.travel_preferences)}
                                    <p class="profile-htitle">Like</p>
                                    <p class="phobovrautosmall">{$InterestHobbies.travel_preferences}</p>
                                    {/if}
                                    {if isset($InterestHobbies.travel_favorites)}
                                    <p class="profile-htitle mt10">Favourite</p>
                                    <p class="phobovrauto">{$InterestHobbies.travel_favorites}</p>
                                    {/if}
                                </div>
                            </li>
                             {/if}    
                             
                             
                             
                             {if isset($InterestHobbies.other_preferences)}
                             <li> 
                                  <div class="profile-topwhite">
                                  <p class="profile-selectedicon other"></p>
                                    <p class="profile-hobbytitle" style="color:#ff74e0;">Others</p>
                                </div>
                                <div class="profile-graybg" style="border-top:1px solid #ff74e0;">
                                   {if isset($InterestHobbies.other_preferences)}
                                    <p class="profile-htitle">Like</p>
                                    <p class="phobovrautosmall">{$InterestHobbies.other_preferences}</p>
                                    {/if}
                                </div>
                            </li>
                             {/if}    
                             
                                                
                        </ul>
                    </div>
                  
                </div>
          