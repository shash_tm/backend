<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<title>TrulyMadly - My Maybes</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
//console.log(headLoadTimeStamp);
</script>
<link href="{$cdnurl}/css/match/matches.css?v=8" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="{$cdnurl}/css/common/headfooter.css?v=3" rel="stylesheet" type="text/css">

</head>

<body>
{include file='templates/common/header.tpl'}

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<div class="mutmatchsec">
<div class="mmwraper">
<h3>My Maybes</h3>
{foreach from=$data item=mutual_like}
<!-- Blocked match -->
<div class="maybe_div">
<ul class="matchblue">
<span class="dayleft">{$mutual_like.profile_data.days_left} days left</span>
<li class="mmtd1 fl"><a><img src="{$mutual_like.profile_data.profile_pic}" class = "user_pic" url="{$mutual_like.profile_data.profile_link}"></a></li>
<li class="mmtd2 fl"><h4  url="{$mutual_like.profile_data.profile_link}" class ="user_name"><a>{$mutual_like.profile_data.fname}</a></h4><p>{$mutual_like.profile_data.age}, {$mutual_like.profile_data.city} <br> </p></li>
<li class="mmtd3 fr"><a class="prolike" url="{$mutual_like.profile_data.like_link}" title="Like"></a><a class="prohide" url="{$mutual_like.profile_data.hide_link}" title="Nope"></a></li>
</ul>
</div>
{/foreach}
{if $data|@count eq 0}
    	<div class="emptymsg">Good going! no one in this list.</div>
{/if}

<!-- Match Tiles end -->
</div>
</div>
<!--Mutualmatch end -->

</div>
<!--Centered Container End -->
</div>
 

<script>
var base_url='{$baseurl}';
var myId = "{$my_id}";
var baseurl='{$baseurl}';

</script>

<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/common/common.js?v=1"></script>
<script src="{$cdnurl}/js/dashboard/maybe.js?v=1.1"></script>

{include file='templates/common/footer.tpl'}

</body>
</html>
