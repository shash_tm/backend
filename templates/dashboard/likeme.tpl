{include file='../header_dashboard.tpl'}

<script type="text/javascript">
$('.matches_header').removeClass('active');
$('.likes_header').addClass('active');
</script>

<div class="container mt80 clearfix" style="margin-bottom:100px;">
        <h2 class="pagetitle"><br>Liked Me</h2><!--
  {if $isAuthentic neq 'authentic'}
    <div class="verifyinfobox effect2">
    To make your profile visibile to other TrulyMadly members, you need a Trust Score of 25%. &nbsp;&nbsp;<a href="{$baseurl}/trustbuilder.php">Build Trust</a>
    </div>
       {/if} 
    	--><div class="mt20">
    	
      <!--   <h2 class="pagetitle">Profiles that have liked you</h2> -->
      
      
      
        {if $myStatus eq 'suspended'}
   <div class="verifyinfobox effect2">
    {$info_msg} &nbsp;&nbsp;
    </div>
       
       {else}
       
      {if isset($no_likes)}
            <div class="verifyinfobox effect2">{$no_likes}</div>
              {/if}
        </div>
       
  
        <div class="clearfix mt20">
        
        	<ul class="communicate_mess" id="received">
        	 {foreach from=$data item=user}
         
            {if isset($user.status)==false}
        	<li>
        	     		<div class="icontoleft">
                    	<p class="csmallicons cthumb">Thumb</p>
                    </div>
                	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="45px" height="45px"></a></p>
                    <p class="mess_time1">{$user.timestamp}</p>
                     
                     <div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.fname} {$user.lname}</a></p>
                        <p class="comm_message" style="width:100%; margin-top:5px;" id="para_{$user.profile_link}">likes your profile. &nbsp;&nbsp;
                        <span class="showhide"></span>
                        {if $isAuthentic eq 'authentic'}
                        <a href="javascript:void()" id="{$user.link_like}" class ="accept_likeme">Like Back</a> &nbsp;&nbsp;<a href="javascript:void()" id ="{$user.link_reject}" class="decline_likeme">Decline</a>
                        {/if}
                        </p>					                                            
                    </div>
                </li>
               {else if $user.status eq 'likedBack' || $user.status eq 'likedBoth'} 
                <li class="grybg">
                          	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="45px" height="45px"></a></p>
                    <p class="mess_time1">{$user.timestamp}</p>
                               	<div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.fname} {$user.lname}</a></p>
                        <p class="comm_message" style="width:100%; margin-top:5px;">
                         Awesome! You’ve both liked each other.
                         <span class="showhide"></span>
                        <a href="javascript:void(0);" style="background-color:#999; cursor:url({$cdnurl}/images/dashboard/icons/liked_cursor.png), none;">Liked <img src="{$cdnurl}/images/dashboard/messages/tick.png"></a></p>					
                       </div>
                </li>
            	  {else if $user.status eq 'reject'} 
                <li class="grybg">
                          	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="45px" height="45px"></a></p>
                    <p class="mess_time1">{$user.timestamp}</p>
                               	<div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.fname} {$user.lname}</a></p>
                        <p class="comm_message" style="width:100%; margin-top:5px;">
                         has been declined by you.
                         <span class="showhide"></span>
                        <a href="javascript:void(0);" style="background-color:#999; cursor:url({$cdnurl}/images/dashboard/icons/liked_cursor.png), none;">Declined <img src="{$cdnurl}/images/dashboard/messages/tick.png"></a></p>					
                       </div>
                </li>
                
                {else if $user.status eq 'rejectMe'} 
                <li class="grybg">
                          	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="45px" height="45px"></a></p>
                    <p class="mess_time1">{$user.timestamp}</p>
                               	<div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.fname} {$user.lname}</a></p>
                        <p class="comm_message" style="width:100%; margin-top:5px;">
                        will like to pass on your interest.
                        <span class="showhide"></span>
                        <a href="javascript:void(0);" style="background-color:#999; cursor:url({$cdnurl}/images/dashboard/icons/liked_cursor.png), none;">Declined <img src="{$cdnurl}/images/dashboard/messages/tick.png"></a></p>					
                       </div>
                </li>
                
               	{/if}
                {/foreach}
            </ul>
            
        </div>
        {/if}
    </div><!--
   
    <div class="footer">
    	<div class="container footercontent">
        	<div class="clearfix">
                <div class="copyfootermenu">
                    <div class="copyright">2013 &copy;trulymadly.com</div>
                    <nav class="footernav">
                    	<a href="#">About</a> <a href="#">Privacy Policy</a> <a href="#">Terms to Use</a> <a href="#">Disclaimers</a> <a href="#">Contact Us</a>
                    </nav>
                </div>
                <div class="socialicons">
                    <ul class="clearfix">
                        <li class="join">Join us on:</li>
                        <li class="fbicon"><a href="#"><i class="icon-facebook"></i></a></li>
                        <li class="fbicon"><a href="#"><i class="icon-linkedin"></i></a></li>
                    </ul>
                </div>
			</div>
        </div>
    </div>
--></div>
</body>
</html>
