{include file='../header_dashboard.tpl'}

<script type="text/javascript">
$('.matches_header').removeClass('active');
$('.messages_header').addClass('active');
var is_authentic = "{$is_authentic}";
</script>

<div class="container mt80 clearfix" style="margin-bottom:100px;">


       
    	<div class="mt20">
        <h2 class="pagetitle">My Communications</h2>
      
        </div>
        
        <div class="clearfix">
            <ul class="innertabs mt20 tabpages">
                <li><a href="#received" class="active">Received</a></li>
                <li><a href="#sent" >Sent</a></li>
            </ul>
        </div>
  
        <div class="clearfix">
        	<ul class="communicate_mess" id="received">
        	
        	{if empty($received)}
       <div class="verifyinfobox effect2">You have no messages.</div>
       {/if}
            	 {foreach from=$received item=user}
            
            {if $user.is_seen eq 0}
            <li>
                	<div class="icontoleft">
                    	<p class="csmallicons cmessage">Message</p>
                    </div>
                	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="75px" height="75px"></a></p>
                    
                    <p class="leftarrow" id="leftArrow_received_{$user.user_id}_{$user.mesh}"><a href="javascript:void(0);"><img src="../images/dashboard/icons/left-arrow.png" border="0" /></a></p>               
                     	<div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.name}</a></p>
                        <p class="mess_time">{$user.tStamp}</p>
                        <div class="clb"></div>
                        <p class="comm_message">has sent you a message!</p>					
                        <div class="showmessage" id ="show_msg_{$user.user_id}_{$user.mesh}"><a href ="javascript:void(0);"></a>
                        	{$user.msg}</div>
                       
                    </div>
                </li>
            	{else}
            	<li class="grybg">
                	<div class="icontoleft"> 
                    	<p class="csmallicons cmessage">Message</p>
                    </div>
                	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="75px" height="75px"></a></p>
                    
                    <p class="leftarrow" id="leftArrow_received_{$user.user_id}_{$user.mesh}"><a href="javascript:void(0);"><img src="../images/dashboard/icons/left-arrow.png" border="0" /></a></p>                	<div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.name}</a></p>
                          <p class="mess_time">{$user.tStamp}</p>
                        <div class="clb"></div>
                        <p class="comm_message">has sent you a message!</p>
                        <div class="messageshowed" id="seen_Msg_{$user.user_id}_{$user.mesh}">
                        	{$user.msg}</p>
                        	<!--<p class="reply clickreply" id="rec_{$user.user_id}"><a href="javascript:void(0);">Reply</a></p>
                        --></div>
                    </div>
                </li>
            	{/if}
                {/foreach}
               
            </ul>
            <ul class="communicate_mess" id="sent" style="display: none;">
             {if empty($sent)}
       <div class="verifyinfobox effect2">You have no messages.</div>
       {/if}
            {foreach from=$sent item=user}
            
            {if $user.is_seen eq 0}
            <li>
                	<div class="icontoleft">
                    	<p class="csmallicons cmessage">Message</p>
                    </div>
                	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="75px" height="75px"></a></p>
                    
                     <p class="leftarrow" id="leftArrow_sent_{$user.user_id}_{$user.mesh}"><a href="javascript:void(0);"><img src="../images/dashboard/icons/left-arrow.png" border="0" /></a></p>                	<div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.name}</a></p>	
                          <p class="mess_time">{$user.tStamp}</p>
                        <div class="clb"></div>				
                        <p class="comm_message" id="show_Msg_{$user.user_id}_{$user.mesh}">
                        	{$user.msg}</p>
                       
                    </div>
                </li>
            	{else}
            	<li class="grybg">
                	<div class="icontoleft">
                    	<p class="csmallicons cmessage">Message</p>
                    </div>
                	<p class="communicate_pic"><a href="{$user.profile_link}"><img src="{$user.thumbnail}" width="75" height="75"></a></p>
                   
                     <p class="leftarrow" id="leftArrow_sent_{$user.user_id}_{$user.mesh}"><a href="javascript:void(0);"><img src="../images/dashboard/icons/left-arrow.png" border="0" /></a></p>                	<div class="communicate_details">
                        <p class="commname"><a href="{$user.profile_link}">{$user.name}</a></p>
                          <p class="mess_time">{$user.tStamp}</p>
                        <div class="clb"></div>
                        <div class="messageshowed" id="seen_Msg_{$user.user_id}_{$user.mesh}">
                        	{$user.msg}
                        	<!--<p class="reply clickreply" id="sent_{$user.user_id}"><a href="javascript:void(0);">Reply</a></p> -->
                        </div>
                    </div>
                </li>
            	{/if}
                {/foreach}
               
            </ul>
        </div>
        
       
       
    </div><!--
   
    <div class="footer">
    	<div class="container footercontent">
        	<div class="clearfix">
                <div class="copyfootermenu">
                    <div class="copyright">2013 &copy;trulymadly.com</div>
                    <nav class="footernav">
                    	<a href="#">About</a> <a href="#">Privacy Policy</a> <a href="#">Terms to Use</a> <a href="#">Disclaimers</a> <a href="#">Contact Us</a>
                    </nav>
                </div>
                <div class="socialicons">
                    <ul class="clearfix">
                        <li class="join">Join us on:</li>
                        <li class="fbicon"><a href="#"><i class="icon-facebook"></i></a></li>
                        <li class="fbicon"><a href="#"><i class="icon-linkedin"></i></a></li>
                    </ul>
                </div>
			</div>
        </div>
    </div>
--></div>
<!--Message Reply-->
<!--<div id="messagereply">
    <div class="messagereplycontent">
        <form>
            <p>Message to Deepika</p>
            <textarea cols="30" rows="4" placeholder="Type in your message here........" id="msg"></textarea>
        </form>
        <div class="btnactions">
        	<p><a href="javascript:void(0);" class="send">Send</a><a href="javascript:void(0);" class="simplemodal-close cancelpop">Cancel</a></p>
        </div>
    </div>
</div>
--></body>
</html>
