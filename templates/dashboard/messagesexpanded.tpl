{include file='../header_dashboard.tpl'}


<script>
var sender_image = '{$sender_image}';
var baseurl = '{$baseurl}';
function redirect(){
	window.location ="{$baseurl}/msg/messages.php";
}

$('.matches_header').removeClass('active');
//$('.messages_header').addClass('active');
</script>


<script>
$(document).ready(function(){ 
	$('html, body').animate({
    scrollTop: $(document).height()
},
1500);
	
});
</script>
    <div class="container mt80 clearfix">
    {if isset($non_authentic)}
    <div class="verifyinfobox effect2">
    You need a Trust Score of 25% to use this feature. &nbsp;&nbsp;<a href="../trustbuilder.php">Build Trust Score</a>
    </div>
       {/if} 
       
       {if $myStatus eq 'suspended' || $profileStatus eq 'suspended'}
   <div class="verifyinfobox effect2">
    {$info_msg} &nbsp;&nbsp;
    </div>
       {/if}
       
       {if isset($blockedMsg)}
   <div class="verifyinfobox effect2">
    {$blockedMsg} &nbsp;&nbsp;
    </div>
       {/if}
       
        <h2 class="pagetitle"><a style="font-weight:bold; font-size:40px" onclick="redirect()" href="#">&laquo;</a> {if !isset($non_authentic)}Me & {$receiver_name}{/if}</h2>
        <div class="individualmessages mt20">
        	<div class="comtitle">
            <!--  	<span>Yesterday</span> -->
            </div>
             {if !isset($noMoreLoad)}
            <div align="center" class="loadmoremsg"><a href="#" class ="staticblock_load" style="display:block;">Load More Messages</a>
            	<span class="dynamicblock_load" style="display:none;"><img src="images/dashboard/loader_msg.gif" width="20" height="20" align="absmiddle" />&nbsp;Loading</span>
            </div>       {/if}	
                    
                    <div class = "send_receive_container">
       	  {foreach from=$seen key=k item=user}
       	  <div class="messages-content">
       	  {if $user.sender eq 'sender'}
       	  <div class="messager-img"><img src="{$sender_image}" class="sender_img" width="60"></div> 
       	      <div class="message-text">
                	<div class="messageinner">
        	
        	        	    		           	
        	
        	                    	<div class="messagermessagesbefore"></div>
        	    		<div class="messagermessages">
{section name=i loop=$user.msg}        	
                           
           <p>{$user.msg[i]} </p>
                        	<!-- <p class="message-time">{$user.time[i]}</p> -->
                         	{if isset($user.unseen_msg_id[i])}
		                <p class="seenicon"><i class="icon-ok-sign"></i> Sent</p>   
		                    {else}
		                    <p class="message-time">Sent: {$user.time[i]}</p>
		                	<!--<p class="seenicon">Sent:</p> -->
							{/if}	
							
							{if $user.msg_id[i] eq $last_seen_msg_id}
							<div class ="clear"></div>
							<!--<p class="seenicon1">Seen at:</p> -->
							 <p class="message-time1">Seen at: {$last_seen_time}</p>
		                	
		                	{/if}
							
                        	
                       
       {/section}    
            </div>
      {elseif $user.sender eq 'other'}
      
                         		<div class="messager-img-gray"><img src="{$receiver_image}" class="rec_img" width="60"></div> 
        <div class="message-text">
                	<div class="messageinner">
        	
      	<div class="messagermessages-graybefore"></div>
                   		<div class="messagermessages-gray">
                   
           {section name=i loop=$user.msg} 
            	
              
                    <p>{$user.msg[i]} </p><!--
                        	
                        	{if isset($user.unseen_msg_id[i])}
		                    <p class="seenicon">Unseen</p>
		                    {else}
                              <p class="seenicon"></p>
		                    -->
		                  
		                    <p class="message-time-gray">Sent: {$user.time[i]}</p>
		                    
		                	<!--<p class="seenicon"><img src="{$cdnurl}/images/dashboard/messages/seen-icon-gray.png"> Seen</p>
							{/if}		                    
                  	
        -->   {/section}
            </div>
            
            {/if}
            </div>
            </div>
			</div>
            {/foreach}
           
           
           
           {if isset($unseen[0].msg)}
                  	 <div class="messages-content">
       	  <div class="messager-img"><img src="{$sender_image}" class="sender_img" width="60"></div> 
       	      <div class="message-text">
                	<div class="messageinner">
        	
        	        	    		           	
        	
        	                    	<div class="messagermessagesbefore"></div>
        	    		<div class="messagermessages">
        	    		        	{assign var=last value=$unseen|@end} 
        	    		
           {foreach from=$unseen item=user} 
         
     	
  
                           
            <p>{$user.msg}</p>
            {if $last.unseen_msg_id eq $user.unseen_msg_id}
                        	<p class="message-time1"><i class="icon-ok-sign"></i> Sent: {$user.time}</p>
		               <!-- <p class="seenicon"><i class="icon-ok-sign"></i>Sent</p>    -->
		               {/if}
                      {/foreach}	
                       
            </div>
		                </div>
		                </div>   
							</div>
           {/if}
           
           
            </div>
            </div>
        </div>
    
    <div class="sent-message-bottom">
    	<div class="container">
        	
           
            
            <div class="senttextareabefore"></div>
        	<textarea name="textarea" id="textarea"  {$canComm} placeholder = "Type in your message here..." cols="45" rows="2" class="senttextarea"  onfocus=" "></textarea>
        	<input type="button" name="" value="SEND" {$canComm} class="messagesend" id="msg_{$receiver_userid}"> 
            {if $canComm neq 'disabled'}
            <ul>
<li class="block"><a href="#"><i class="icon-remove" ></i> Block this person</a></li>
            </ul>
            {/if}
        </div>  
    </div><!--
    
    
    
    <div class="footer" style="position:fixed;">
    	<div class="container footercontent">
        	<div class="clearfix">
                <div class="copyfootermenu">
                    <div class="copyright">2013 &copy;trulymadly.com</div> 
                    <nav class="footernav">
                    	<a href="#">About</a> <a href="#">Privacy Policy</a> <a href="#">Terms to Use</a> <a href="#">Disclaimers</a> <a href="#">Contact Us</a>
                    </nav>
                </div>
                <div class="socialicons">
                    <ul class="clearfix">
                        <li class="join">Join us on:</li>
                        <li class="fbicon"><a href="#"><i class="icon-facebook"></i></a></li>
                        <li class="fbicon"><a href="#"><i class="icon-linkedin"></i></a></li>
                    </ul>
                </div>
			</div>
        </div>
    </div>
--></div>
<!--Message Reply-->
<div id="messagereply">
    <div class="messagereplycontent" id= "block_{$receiver_userid}">
        <form id = "blockform" name="myForm">
            <p>Are you sure you'd like to block this TrulyMadly member?</p>
            <p style="font-size:12px; padding:0; margin:0 0 10px 0;">Why would you like to report this TrulyMadly member?</p>
<input name="usrblock" type="radio" id = "1" value="usrblock1" class="fl mrg" /> <label class="fl mrg" style="font-size:12px;">&nbsp;&nbsp;Has sent abusive email/message</label><br />
<input name="usrblock" type="radio" id = "2" value="usrblock2" class="fl mrg" /><label class="fl mrg" style="font-size:12px;">&nbsp;&nbsp;Misleading profile information</label><br />
<input name="usrblock" type="radio" id = "3" value="usrblock3" class="fl mrg" /><label class="fl mrg" style="font-size:12px;">&nbsp;&nbsp;Inappropriate content</label><br />
<input name="usrblock" type="radio" id = "4" value="usrblock4" class="fl mrg" /><label class="fl mrg" style="font-size:12px;">&nbsp;&nbsp;Already married/engaged</label><br />
<input name="usrblock" type="radio" id = "5" value="usrblock5" class="fl mrg" /><label class="fl mrg" style="font-size:12px;">&nbsp;&nbsp;Harassment</label><br />
<input name="usrblock" type="radio" id = "6" value="usrblock6" class="fl mrg" /><label class="fl mrg" style="font-size:12px;">&nbsp;&nbsp;Asking for Money</label><br />
<input name="usrblock" type="radio" id = "7" value="usrblock7" class="fl mrg" /><label class="fl mrg" style="font-size:12px;">&nbsp;&nbsp;Other</label><br />
<textarea cols="30" id ="textArea" rows="1" placeholder="To help us safeguard you, please tell us why you would like to block this TrulyMadly member." style="height:40px; font-size:12px;" ></textarea>
        </form>
        <div class="btnactions">
        	<p><a href="javascript:void(0);" class="send" >Confirm</a><a href="javascript:void(0);" class="simplemodal-close cancelpop">Cancel</a></p>
        </div>
    </div>
</div>
</body>
</html>


 
  