{include file='../header_dashboard.tpl'}

<script type="text/javascript">
$('.matches_header').removeClass('active');
$('.dashboard_header').addClass('active');

var connectedFrom = 'dashboard';
</script>


<div class="container mt80">

	<link type="text/css" rel="stylesheet"
		href="css/register/verifypayment.css">

	{if ($status neq 'authentic')}
	<!-- verification section start -->
<!--	<div class="verifyinfobox effect2">
		Please authenticate yourself to communicate with your Matches. <a
			href="{$baseurl}/trustbuilder.php">Verify Now</a>

	</div> -->
	{/if}
	<!-- verification section end -->





<!--<div id="diffframe4" style="display:none;">
<div class="difusescreen" align="center"><img src="images/ajax_loader.gif" border="0" width="100" height="100" style="margin-top:20%; display:none;" class="loaderimg" /></div>
<div class="sndmpopupframe" id ="alertbox" style="display:none;">
<h4>ERROR MESSAGE <a href="javascript:void(0);" onClick="hideInviteBoxes();" class="fr"><i class="icon-remove-sign"></i></a></h4>
<div class="tbinfosec4txt formbox clb">
Oops! Unable to bring your Facebook Friend List.
</div>
</div>
</div>
--><!-- Pop Up end -->  




	<div class="content mt20">
    
    <div class="leftpanel mt20">
			<!--Missed Registration Steps start --> 
			<div class="misregstep mb40" id="pendreg" style="display:none;">
				<div class="mrstitle">
					<p class="fl mrg padg">Something is missing in your profile</p>
					<p class="fr mrg padg">
						<a href="javascript:void(0);"
							onClick="document.getElementById('pendreg').style.display='none';">&times;</a>
					</p>
				</div>
				<div class="regformdb">
					Registration Form Area <br> <br>
					<div class="clb"></div>
					<p class="rgsavebtn fl">
						<a href="#">Save</a>
					</p>
					<p class="rgskipbtn fl ml10 ">
						<a href="#">Skip &raquo;</a>
					</p>
				</div>
			</div>
			<!--Missed Registration Steps end -->

			<!--New Matches Start -->
			{if !isset($NoMatches)}
			<div class="nmatch">
				<p class="days mb10">
					<span>New</span> Matches <a href="{$baseurl}/infog/ScientificMatching.html" class="pink ttransl fs12" target="_blank" >How do we match?</a>
				</p>
				<div class="dbnmatch mb30">
					<ul class="dbprophoto">
						{foreach from=$userDetails key=k item=user}
						<li><a href="{$user.profile_link}"><img src="{$user.image_url}"	height="108" width="108" border="0" />
                                <div class="clb"></div>
								<p class="fnamelimit">{$user.fname}</p><p>, {$user.age}</p> </a></li> {/foreach}
					</ul>

					<p class="vall">
						<a href="{$baseurl}/matches.php">View All</a>
					</p>
				</div>
			</div>
            
            {else}
			           
            <div class="nmatch">
				<p class="days mb10">
					<span>My</span> Matches <a href="{$baseurl}/infog/ScientificMatching.html" class="pink ttransl fs12" target="_blank" >How do we match?</a>
				</p>
				<div class="dbnmatch mb30">
				<img src="images/dashboard/phcolagdb.png" width="108" height="112" class="fl mr5" /><p class="fl mt20 relexptxt">Our relationship experts are working on finding matches for you.<br /> You can modify your partner preferences at any time.</p>
				<p class="fl mt10 relexptxt"><a href="{$baseurl}/editprofile.php">EDIT PREFERENCES</a></p>
				</div>
			</div>
            
            {/if}
            
            
			<!--New Matches End -->

			<p class="days mb10">
				<span>Updates</span> 
			</p><!--
            
           <div class="dbbanner">
           		<h3>Find more common friends with your matches!</h3>
                <img src="images/dashboard/bannerdb1.gif" width="446" height="134" class="clb" />
                <p>find out YOU <i class="icon-long-arrow-right"></i> NEHA <i class="icon-long-arrow-right"></i> RAHUL <i class="icon-long-arrow-left"></i> YOUR MATCH</p>
                <a href="javascript:void(0);" onClick="invitefun1()"><i class="icon-facebook-sign"></i>&nbsp;&nbsp;Invite Friends Selectively</a>
           </div>
            
            --><!--
			<div class="lttitle">
				<span>Today</span>
			</div>
			Updates
			<div class="updates">
            <div class="tmsealico"></div>
				
				<div class="updates-white">
					<div class="updates-content">
						<p class="personimg">
							<img src="images/dashboard/updates/update1.jpg" />
						</p>
						<div class="updates-details">
							<p>
								<span><a href="#">Neha Sharma</a> </span> has updated her city
								to Mumbai
							</p>
							<p class="hours">
								<img src="images/dashboard/icons/time.png"> 1 Hours Ago
							</p>
						</div>
					</div>
				</div>
			</div>

			--><div class="lttitle mt20">
				<!--<span>{$regDate}</span>
			--></div>
			<div class="updates">
            <div class="tmsealico"></div>
				
				<div class="updates-white">
					<div class="updates-content">
						<!--    <p class="personimg"><img src="images/dashboard/updates/update1.jpg"></p> -->
						<div class="updates-details">
							<p>
								<span><a href="#">Trulymadly</a> </span> wishes you all
								the best for your matches
							</p>
							<p class="hours">
								<img src="images/dashboard/icons/time.png"> {$regDate}
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="updates">
				 <div class="tmsealico"></div>
				<div class="updates-white">
					<div class="updates-content">
						<!--      <p class="personimg"><img src="images/dashboard/updates/update2.jpg"></p> -->
						<div class="updates-details">
							<p>
								<span><a href="#">Trulymadly</a> </span> welcomes you.
							</p>
							<p class="hours">
								<img src="images/dashboard//icons/time.png"> {$regDate}
							</p>
							<!--     <ul class="photolist">
                                    <li><img src="images/dashboard/updates/photo1.jpg"></li>
                                    <li><img src="images/dashboard/updates/photo2.jpg"></li>
                                    <li><img src="images/dashboard/updates/photo3.jpg"></li>
                                </ul> -->
						</div>
					</div>
				</div>
			</div>



		</div>
    
    
    
    
    
		<div class="rtpanel mt20">
			<!--Alerts-->
			<!--{if isset($systemalert)}
			<div class="alert">
				<p class="days mb10">
					<span>System</span> Alerts
				</p>
				<div class="sysalertmsg">
					<i class="icon-info-sign pink fl"></i>
					<p class="ml20">{$systemalert}</p>
				</div>
			</div>
			{/if} -->
            
            <!--Payment-->
			<p class="days mb10">
				<span>My</span><!--<img src="images/dashboard/tmseal_med.png" class="fl" style="margin:-3px 10px 0 0;">  --> Credits
			</p>
		
			<div class="memftr mb40">
            <div class="crdtwallet"><p id='credits_update_dash'>
            {if ($header.payment_plan eq 'unlimited')}
Unlimited
{else}
{$header.credits}
{/if}
            
            </p><span>My Wallet</span></div>
            <div class="memftrline"><span>10</span> credits to unlock a member profile<!--<img src="images/dashboard/pinkline.png" /> --><p></p></div>
				<ul>
					
					<li><p></p> See the full name</li>
					<li><p class="vmfc"></p> View mutual Facebook friends</li>
					<li><p class="spm"></p> Send personalised messages</li>
                    <li class="talc"><br /><a href="{$baseurl}/payment.php">View Credit Plans</a></li>
					
				</ul>
               
			</div>
            

			<!--Trust Builder-->
			<p class="days mb10">
				<span>Build</span> Trust Score <a href="{$baseurl}/infog/TrustSecurity.html" class="pink" target="_blank"><sup>?</sup></a>
            </p>
                                
                <div class="tbconsec">
                <p><span>25%</span> score to get started<img src="images/dashboard/pinkline.png" /></p>
                <div class="mytscore">MY SCORE</div>
                <ul>
                <li><i class="tbconicon tbiconbg hrtico"></i>To become visible to your matches</li>
                <li><i class="tbconicon icon-thumbs-up"></i>To like your matches</li>
				<li><i class="tbconicon tbiconbg crdtico"></i>To earn free credits</li>
                </ul>
                
                <div class="dotline"></div>
                </div>
                    
			<div class="dbtbuild mb10">
				<div class="dbtrustmeter-reading">
					<div class="pieContainer">
						<div class="pieBackground"></div>
						<div id="pieSlice1" class="hold">
							<div class="pie"
								style="-webkit-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $trustpie}deg); -moz-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $trustpie}deg); -o-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $trustpie}deg); transform:rotate({math equation='(( x *1.8 ) -90 )' x= $trustpie}deg);"></div>
						</div>
						<div class="persentbg">
							<p id="trust_percent">{$trustscore}%</p>
						</div>
					</div>
					<!--<p>Trust Score</p> -->
				</div>

				<!--<div class="dbtrustdetails">
					<ul class="mt10">
						<li><p class="dbtrusttitle">Verify Yourself</p></li>
					</ul>
				</div> -->
                
                <div class="dbtrustdetails">
                         
<ul>
						<li class="td1"><i class="mt2 icon-facebook-sign iconsmall fbcolor {if ($fbVerified
											eq 'yes')}  {/if}"></i>
							<p class="fl">Facebook</p> <div class="icon-info-sign iconinfotb" onmouseover="fbtopen()" onmouseout="fbtclose()"><div class="hovrtitle" style="display:none;" id="fbtitle">Don't worry, we won't publish anything to your Facebook account. We need this as part of the verification process only.</div></div>
							<p class="dbfreecrdittxt fb_verified_no clb" {if ($fbVerified
								eq 'yes')} style="display: none;"{/if}>Earn {$tbNumbers.credits.facebook} credits</p>
							
							<p class="dbfreecrdittxt fb_verified clb" {if ($fbVerified
											neq 'yes')} style="display: none;"{/if}>
							{if ($fbVerified eq 'yes')}
										{$trustpoints.fb_credits}
										{else}
										{$tbNumbers.credits.facebook} 
										{/if}
										Credits Added				
							</p>
							
						</li>
						<li class="td2">
							<p class="dbfacebook fb_verified_no" {if ($fbVerified
								eq 'yes')} style="display: none;"{/if}>
								<img src="images/register/loader_msg.gif"
									style="position: absolute; margin-top: -2px; display: none; margin-left: 47px;"
									width="24" height="24" class="fl" id="loader_fb2"> <a
									onclick="proceed()" id="verify_fb2">Verify Now</a>
							</p>
							<p class="addwaletpnts fb_verified" id="fbConnections" {if ($fbVerified
								neq 'yes')} style="display: none;"{/if}>
							{if ($fbVerified eq 'yes')}
											{$fbConnection} Connections
							{/if}
							</p>
							<p class="trustBuilderError mt5" id="fb_error"
												style="display: none;"></p>
						</li>
						<!--										<li class="td3">
											<p class="addwaletpnts fb_verified" {if ($fbVerified
												neq 'yes')} style="display: none;"{/if}>
												<img src="images/register/walletplus.png" align="left"
													width="40" />10 Free credit points<br>added to your wallet
											</p>
											
										</li> -->
						<li class="td5"><p id="fb_verified_circle" {if ($fbVerified
								eq 'yes')} class="bluecircle" {else} class="graycircle"{/if}>
								+{$tbNumbers.score.facebook}%</p></li>
					</ul>
				</div>

				<div class="dbtrustdetails">

					<ul>

						<li class="td1"><i class="mt2 icon-linkedin-sign iconsmall incolor {if ($linkedinVerified eq 'yes')}  {/if}"></i>
							<p class="fl">LinkedIn</p><div class="icon-info-sign iconinfotb" onmouseover="litopen()" onmouseout="litclose()"><div class="hovrtitle" style="display:none;" id="lititle">Don't worry, we won't publish anything to your LinkedIn account. We need this as part of the verification process only.</div></div>
							<p class="dbfreecrdittxt linked_verified_no clb"
								{if ($linkedinVerified eq 'yes')} style="display: none;"{/if}>Earn {$tbNumbers.credits.linkedin} credits</p>
							<p class="dbfreecrdittxt linked_verified clb"
								{if ($linkedinVerified neq 'yes')} style="display: none;"{/if}>	
							{if ($linkedinVerified eq 'yes')}
										{$trustpoints.linked_credits}
										{else}
										{$tbNumbers.credits.linkedin} 
										{/if}
										Credits Added
							</p>	
								
						</li>
						<li class="td2">
							<div class="linked_verified_no">
								{if ($linkedinVerified eq 'no')}
								<div id="linked_signin" style="display: none;">
									<script type="IN/Login"></script>
									<img src="images/register/loader_msg.gif"
										style="position: absolute; margin-top: 4px; display: none; margin-left: 88px;"
										width="24" height="24" class="fl" id="loader_linkedIn">
								</div>
								<p class="linkedin" id="linkedInConnect"
									onclick="onLinkedInAuth()">
									<a>Verify Now</a>
								</p>
								{/if}
							</div>
							<p class="addwaletpnts linked_verified" id="linkedinConnections" {if ($linkedinVerified
								neq 'yes')} style="display: none;"{/if}>
								{if ($linkedinVerified eq 'yes')}
											{$linkedinConnection} Connections
								{/if}
							</p>
							<p class="trustBuilderError mt5" id="linked_error"
								style="display: none;"></p>
						</li>
						<!--										<li class="td3">
											<p class="addwaletpnts linked_verified"
												{if ($linkedinVerified neq 'yes')} style="display: none;"{/if}>
												<img src="images/register/walletplus.png" align="left"
													width="40" />10 Free credit points<br>added to your wallet
											</p>
										</li> -->
						<li class="td5"><p id="linked_verified_circle"
								{if ($linkedinVerified eq 'yes')} class="bluecircle"
								{else} class="graycircle"{/if}>+{$tbNumbers.score.linkedin}%</p></li>
					</ul>

				</div>
				<div class="dbtrustdetails">
                
					<ul>
						<li class="td1"><i class="mt2 icon-user iconsmall idcolor{if ($idVerified
											eq 'active')}  {/if}"></i> <p class="fl">Photo ID </p><div class="icon-info-sign iconinfotb" onmouseover="idtopen()" onmouseout="idtclose()"><div class="hovrtitle" style="display:none;" id="idtitle">We need this to verify that you are who you say you are. TrulyMadly.com will not share the ID copy with other members. </div></div>
							<p class="dbfreecrdittxt clb" {if ($idVerified
								eq 'active')} style="display: none;"{/if}>Earn {$tbNumbers.credits.photoid} credits</p>
							{if ($idVerified eq 'active')}
							<p class="dbfreecrdittxt clb">
								{$trustpoints.id_credits} Credits Added
							</p>	
							{/if}	
						</li>
						<li class="td2">
							<div class="dbstyled-select mrg id_verified_no clb fl"
								{if ($idVerified neq 'N' && $idVerified
								neq 'rejected')} style="display: none;"{/if}>
								<select id="idSelectBox" onchange="idChange()">
									<option selected="selected" value="0">Select & Upload</option>
									<optgroup label="-------------------"></optgroup>
									<option value="Passport">Passport</option>
									<option value="Driving License">Driving License</option>
									<option value="Pan Card">Pan Card</option>
									<option value="Voter ID">Voter ID</option>
									<option value="Aadhar Card">Aadhar Card</option>
								</select>
							</div>
                            <div class="clb"></div>
							<p class="id_verified tbsucess" {if ($idVerified
								neq 'under_moderation')} style="display: none;"{/if}>{if
											($idVerified eq 'under_moderation')} Thank you for uploading your  {$idproofType}. We will review it and contact you within 48 hours. {else} Thank you for uploading your <span id='id_type'></span>. We will review it and contact you within 48 hours.
							{/if}</p>
							<p>
										{if ($idVerified eq 'fail')}
											<span class="trustBuilderError">
											Your photo ID could not be verified. Please contact TrulyMadly customer care.
											</span>
										{/if}
							</p>
							<div class="id_verified_no">
								{if ($idVerified eq 'active')}

								<p class="addwaletpnts">
									{$idproofType} Verified
								</p>
								{elseif ($idVerified eq 'N' || $idVerified eq 'rejected')}
								<div id="id_upload">
									<div id="idVerificationButton" style="display: none;" class="mt10 mb5">
										<p class="uploadbtn fl">
											<a onclick="openFile(1)">Browse</a>
										</p>
										<p class="uploadbtn fl" style="display: none;">
											<a onclick="openWebCam(1)">Click & Upload</a>
										</p><br />
									</div>
									<div id="idVerificationButton_show">
										{if ($idVerified eq 'rejected')} This is an invalid document. Please upload a valid photo ID.
										{/if}
										<p></p>

										<!--														<p class="uploadbtn mt7 fl">
															<a style="background-color: #999;">Upload</a>
														</p>
														<p class="uploadbtn mt7 fl">
															<a style="background-color: #999;">Click & Upload</a>
														</p> -->
									</div>
								</div>
								<div style="display: none; margin-top:5px;" id="id_upload_loader1">
									<div class="pbartrust">
										<div id="id_upload_loader_percent" class="percent_loader"></div>
									</div>
									<span class="fl fs11 flnamehide" id="document_name1"></span>
								</div>
								{/if}
							</div>
						</li>
						<!--                            <li class="td3"><p class="uploadbtn fl"><a href="#">Upload</a></p><p class="uploadbtn fl"><a href="#">Click & Upload</a></p></li> -->
						<li class="td5"><p {if ($idVerified
								eq 'active')} class="bluecircle" {else} class="graycircle"{/if}>+{$tbNumbers.score.photoid}%</p>
						</li>
					</ul>
				</div>
				<div class="dbtrustdetails" id="phone_div" {if ($phoneVerified neq 'active' && ($empVerified eq 'under_moderation' || $addressVerified eq 'under_moderation'))} style="background:#d9e9fb; border:1px dashed #9fcbfd;"  {/if}>
					<ul>

						<li class="td1"><i class="mt2 icon-phone iconsmall phcolor {if ($phoneVerified eq 'active')}  {/if}"></i><p class="fl"> Phone</p><div class="icon-info-sign iconinfotb" onmouseover="phtopen()" onmouseout="phtclose()"><div class="hovrtitle" style="display:none;" id="phtitle">You're not going to get any calls from TrulyMadly.com and neither will we share your phone number with other members.</div></div>
                        
							<p class="dbfreecrdittxt clb phone_verified_no" {if ($phoneVerified eq 'active')} style="display: none;"{/if}">Earn {$tbNumbers.credits.mobile} credits</p>
							<p class="dbfreecrdittxt clb phone_verified"
								{if ($phoneVerified neq 'active')} style="display: none;"{/if}>	
							{if ($phoneVerified eq 'active')}
										{$trustpoints.mobile_credits}
										{else}
										{$tbNumbers.credits.mobile} 
										{/if}
										Credits Added
							</p>
						</li>
						<li class="td2">
						{if ($phoneVerified neq 'active')}
										{if ($phoneVerified eq 'rejected' && $phoneTrials>1)}
											
										{elseif ($phoneVerified neq 'under_moderation')}
							<p class="trustdropbox formstyle phone_verified_no">
								<input class="inputwidth" type="text"
									placeholder="Enter Phone #" name="" id="phoneNumber" onfocus="$('#phoneSubmitBtn').show();">
							</p>
							<p class="uploadbtn mt10 mb5 phone_verified_no" id="phoneSubmitBtn" style="display: none;">
											<a onclick="submitPhone()">Verify Now</a>
							</p>
						{/if}
						{/if}
						<p class="phone_verified addwaletpnts" {if ($phoneVerified neq 'active')} style="display: none;"{/if}>
										<span id="phoneNumberSpan">
										{if ($phoneVerified eq 'active')}
											{$phoneNumber}
										{/if}
										</span>
										</p>
						<div style="display:none;" id="phone_wait_loader">
											<img src="images/register/loader_msg.gif"
													width="24" height="24" class="fl">
											<span>Call Placed. Verifying....</span>
										</div>
						{if ($phoneVerified eq 'under_moderation')}
										<p class="trustbuildererror mt5">Your phone number is pending verification.  </p>
									{/if}
									
									{if ($phoneVerified eq 'rejected' && $phoneTrials>1)}
										<p class="trustbuildererror mt5">Your phone number could not be verified. Please contact TrulyMadly customer care.</p>
									{/if}
									
									<p class="trustBuilderError phone_verified_no mt5" id="phone_error"
											style="{if ($phoneVerified eq 'rejected' && $phoneTrials == 1)} {else} display: none; {/if}">
										{if ($phoneVerified eq 'rejected' && $phoneTrials == 1)}
											Your phone number could not be verified. Please try again.	
										{/if}	
										</p>
						</li>
						<!--										<li class="td3"><p class="uploadbtn">
												<a>Submit</a>
											</p></li> -->
						<li class="td5"><p id="phone_verified_circle" {if ($phoneVerified
											eq 'active')} class="bluecircle" {else} class="graycircle"{/if}>+{$tbNumbers.score.mobile}%</p>
							</p>
					
					</ul>
				</div>

				<div class="dbtrustdetails">
					<ul>

						<li class="td1"><i class="mt2 icon-home iconsmall adcolor {if ($addressVerified
											eq 'active')}  {/if}"></i></i>
							<p class="fl">Address </p><div class="icon-info-sign iconinfotb" onmouseover="adtopen()" onmouseout="adtclose()"><div class="hovrtitle" style="display:none; top:-97px;" id="adtitle">TrulyMadly.com will not share your address details with other members. It is only to verify your current location. As part of the process a TrulyMadly representative will visit the mentioned address within 7-10 business days.</div></div>
                            
							<p class="dbfreecrdittxt clb" {if ($addressVerified
											eq 'active')} style="display: none;"{/if}>Earn {$tbNumbers.credits.address} credits</p>
							{if ($addressVerified eq 'active')}

										<p class="dbfreecrdittxt clb">
											{$trustpoints.address_credits} Credits Added
										</p> 
							{/if}				
											
						</li>
						<li class="td2">
						{if ($addressVerified eq 'N' ||
										$addressVerified eq 'rejected')}
								<div>
										<div id="address_upload_type" class="address_verified_no fl">

											<div class="dbstyled-select mrg address_verified_no fl"
												{if ($addressVerified neq 'N' && $addressVerified
												neq 'rejected')} style="display: none;"{/if}>
												<select id="addressSelectBox" onchange="addressChange()">
													<option selected="selected" value="0">Select & Upload</option>
													<optgroup label="-----------------------------------"></optgroup>
<!-- 													<option value="1">Enter Address</option> -->
<!-- 													<optgroup label="---- OR Upload ----"></optgroup>	 -->
													<option value="Passport">Passport</option>
													<option value="Ration Card">Ration Card</option>
													<option value="Landline Telephone Bill">Landline Telephone Bill</option>
													<option value="Voter ID Card">Voter ID Card</option>
													<option value="Driving Licence">Driving Licence</option>
													<option value="Rent Agreement">Rent Agreement</option>
												</select>
											</div>
											<div class="address_verified_no">
												<div id="address_upload">
													<div id="address1" style="display: none;" class="mt10 mb5">
													<div class="phoneVerificationFlag" {if ($phoneVerified neq 'active')} style="display: none;" {/if}>
														<p class="uploadbtn fl">
															<a onclick="$('#file1').click()">Browse</a>
														</p>
														<p class="uploadbtn fl" style="display: none;">
															<a onclick="openWebCam(2)">Click & Upload</a>
														</p><br />
													</div> 
													{if ($phoneVerified neq 'active')}
										<p class="trustBuilderError phoneVerificationFlag_no">		Please verify your phone number to initiate address verification.</p>
{/if}
													</div>
												</div>
												<div style="display: none; padding-top: 5px;" id="address_upload_loader1">
													<div class="pbartrust">
														<div id="address_upload_loader_percent"
															class="percent_loader"></div>
													</div>
													<span class="fl fs11 flnamehide" id="document_name2"></span>
												</div>
											</div>
<!-- 											<div class="clear">------------ or ------------<br /><br /></div> -->
										</div>
										<div id="address_form_type" class="address_verified_no">
										<!-- 	<div class="ttrans"><p class="uploadbtn mt7 fl" style="margin-bottom: 10px;">
															<a onclick="$('#addressForm').show();$(this).hide();">Enter Address</a>
														</p></div>  -->
											<p class="trustdropbox formstyle" id="addressForm" style="display: none;">
												<input type="text" class="inputwidth" placeholder="Address"
													id="address" /> <input type="text" class="inputwidth"
													placeholder="City" id="city" /> <input type="text"
													class="inputwidth" placeholder="State" id="state" /> <input
													type="text" class="inputwidth" placeholder="Pincode"
													id="pincode" /> <input type="text" class="inputwidth"
													placeholder="Landmark" id="landmark" /> <input
													value="Submit" type="button" onclick="submitAddressForm()" class="sbmtbtn" />
												<span id="address_form_error"></span>
											</p>
										</div> </div> {/if}										
										<p class="address_verified" {if ($addressVerified
											neq 'under_moderation')} style="display: none;"{/if}>											
{if	($addressVerified eq 'under_moderation')}
											<span class="tbsucess">Thank you for uploading your {$addressproofType}. We will review it and contact you within 48 hours.
										{else} Thank you for uploading your <strong id='address_type'></strong>. We will review it and contact you within 48 hours.</span>{/if}
											<br>
										</p>
											
									{if ($addressVerified eq 'active')}
											<p class="addwaletpnts">{$addressproofType} Verified</p>
										{/if}
										{if ($addressVerified eq 'fail')}
											<span class="trustBuilderError">Your address could not be verified. Please contact TrulyMadly customer care.</span>
										{/if}
									<div id="addressVerificationButton_show">
													{if ($addressVerified eq 'rejected')}
													<p class="trustBuilderError">
													 This is an invalid document. Please upload a valid address proof.
													 </p>
													{/if}
									</div>
						</li>
						<!--                            <li class="td3"><p class="uploadbtn fl"><a href="#">Upload</a></p><p class="uploadbtn fl"><a href="#">Click & Upload</a></p></li> -->
						<li class="td5"><p {if ($addressVerified
											eq 'active')} class="bluecircle" {else} class="graycircle"{/if}>+{$tbNumbers.score.address}%</p></li>

					</ul>
				</div>

				<div class="dbtrustdetails brdrbtm">
					<ul>

						<li class="td1"><i class="mt2 icon-briefcase iconsmall emcolor {if ($empVerified
											eq 'active')}  {/if}"></i> <p class="fl">Employment</p><div class="icon-info-sign iconinfotb" onmouseover="emtopen()" onmouseout="emtclose()"><div class="hovrtitle" style="display:none; top:-112px;" id="emtitle">We will conduct a strictly non-employment verification check and to ensure 100% privacy, it will NOT be mentioned that it is being conducted for TrulyMadly.com or any matchmaking purpose. The company name will not be revealed to other members unless stated in your profile.</div></div>
							<p class="dbfreecrdittxt clb" {if ($empVerified
											eq 'active')} style="display: none;"{/if}>Earn {$tbNumbers.credits.employment} credits</p>
						{if ($empVerified eq 'active')}

										<p class="dbfreecrdittxt clb">
											{$trustpoints.employment_credits} Credits Added
										</p> {/if}					
						</li>
						<li class="td2">
							{if ($empVerified eq 'active')}
											<p class="addwaletpnts">{$empproofType} Verified</p>
							{/if}
							<p class="emp_verified " {if ($empVerified
											neq 'under_moderation')} style="display: none;"{/if}>
{if	($empVerified eq 'under_moderation')}
											<span class="tbsucess">Thank you for uploading your {$empproofType}. We will review it and contact you within 48 hours.
											
											{else} Thank you for uploading your <strong id='emp_type'></strong>. We will review it and contact you within 48 hours.</span>{/if}
											<br>
										</p> {if ($empVerified eq 'N' || $empVerified
										eq 'rejected')}
								<div class="">		
										<div id="emp_upload_type" class="emp_verified_no fl">
											<div class="dbstyled-select mrg emp_verified_no fl"
												{if (($empVerified neq 'N' && $empVerified
												neq 'rejected') || ($workarea eq 'Student' || $workarea eq 'Not Working'))} style="display: none;"{/if}>
												<select id="empSelectBox" onchange="empChange()">
													<option selected="selected" value="0">Select & Upload</option>
													{if ($workarea eq 'Salaried' || $workarea eq 'Public') }
<!-- 														<option value="1">Enter employment details</option> -->
														<optgroup label="-------------------"></optgroup>
														<option value="Employment Letter">Employment Letter</option>
													<option value="Experience Letter">Experience Letter</option>
													<option value="Last Salary Slip">Last Salary Slip</option>	
													{elseif ($workarea eq 'Business' || $workarea eq 'Self Employed')}
<!-- 													<option value="2">Enter bussiness details</option> -->
													<optgroup label="-------------------"></optgroup>
<!-- 													<optgroup label="---- OR Upload ----"></optgroup> -->
													<option value="Landline Telephone Bill">Landline Telephone Bill of Business Premises</option>
													<option value="MOA / AOA of Company">MOA / AOA of Company</option>
													<option value="Electricity Bill">Electricity Bill of Business Premises</option>
													<option value="Rent Agreement">Rent Agreement of Business Premises</option>
													<option value="Maintenance Bill">Any other Maintenance Bill with Address on it</option>
													{/if}
												</select>
											</div>
											<div class="emp_verified_no">
												<div id="emp_upload">
													<div id="emp1" style="display: none;" class="mt10 mb5">
													<div class="phoneVerificationFlag" {if ($phoneVerified neq 'active')} style="display: none;" {/if}>
														<p class="uploadbtn fl">
															<a onclick="$('#file2').click()">Browse</a>
														</p>
														<p class="uploadbtn fl" style="display: none;">
															<a onclick="openWebCam(3)">Click & Upload</a>
														</p><br />
													</div>
													{if ($phoneVerified neq 'active')}
													<p class="trustBuilderError phoneVerificationFlag_no">		Please verify your phone number to initiate employment verification.</p>
												{/if}
													</div>
												</div>
												<div style="display: none; padding-top: 5px;" id="emp_upload_loader1">
													<div
														class="pbartrust">
														<div id="emp_upload_loader_percent" class="percent_loader"></div>
													</div>
													<span class="fl fs11 flnamehide" id="document_name3"></span>
												</div>
											</div>
<!-- 											<div class="clear">------------ or ------------<br /><br /></div> -->
										</div>
										<div id="emp_form_type" class="emp_verified_no">
									<!-- 		<div class="ttrans">Enter employment</div>
											<div class="dbstyled-select fl mrg">
												<select onchange="changeEmployment()" id="employmentType">
													<option value="0" selected="selected">Employment Type</option>
													<option value="1">Employed</option>
													<option value="2">Self Employed</option>
												</select>
											</div>  -->
											<p class="trustdropbox formstyle emp_verified_no"
												id="employed_form" style="display: none;">
												<input type="text" class="inputwidth"
													placeholder="Company Name" id="cmp_name" /> <input
													type="text" class="inputwidth" placeholder="Employee Code"
													id="emp_code" /> <input type="text" class="inputwidth"
													placeholder="Designation" id="designation" /> <input
													type="text" class="inputwidth" placeholder="Tenure"
													id="tenure" /><input type="button" value="Submit"
													onclick="submitEmploymentForm(1)" class="sbmtbtn"/> <span
													id="emp1_form_error"></span>
											</p>
											<p class="trustdropbox formstyle emp_verified_no"
												style="display: none;" id="selfemployed_form">
												<input type="text" class="inputwidth"
													placeholder="Business Name" id="bs_name" /> <input
													type="text" class="inputwidth" placeholder="Business Type"
													id="bs_type" /> <input type="text" class="inputwidth"
													placeholder="Business Address" id="bs_address" /> <input
													type="text" class="inputwidth" placeholder="City"
													id="bs_city" /> <input type="text" class="inputwidth"
													placeholder="State" id="bs_state" /> <input type="text"
													class="inputwidth" placeholder="Pincode" id="bs_pincode" />
												<input type="text" class="inputwidth" placeholder="Tenure"
													id="bs_tenure" /> <input type="button" value="Submit"
													onclick="submitEmploymentForm(2)" class="sbmtbtn " /> <span
													id="emp2_form_error"></span>
											</p>
										</div></div> {/if}
										{if ($empVerified eq 'fail')}
											<p class="trustBuilderError">
											Your employment details could not be verified. Please contact TrulyMadly customer care.
											</p>
										{/if}
										<div id="empVerificationButton_show">
													{if ($workarea eq 'Student' || $workarea eq 'Not Working') }
														<p class="trustBuilderError">
														Not Applicable
														</p>
													{/if}
													{if ($empVerified eq 'rejected')} 
													<p class="trustBuilderError">
													This is an invalid document. Please upload a valid employment proof.
													</p>
													{/if}
										</div>
										
						</li>
						<!--                             <li class="td3"><p class="uploadbtn fl"><a href="#">Upload</a></p><p class="uploadbtn fl"><a href="#">Click & Upload</a></p></li> -->
						<li class="td5"><p {if ($empVerified
											eq 'active')} class="bluecircle" {else} class="graycircle"{/if}>+{$tbNumbers.score.employment}%</p></li>
					</ul>
				</div>
				
			</div>

			
			<!--Alerts-->
			<!--<div class="alert">
                    <p class="days"><span>Earn</span> Credits</p>
                    <p class="daysleft"> and Boost your ranking</p>
                    <p class="renewnow"><a href="{$baseurl_https}/trustbuilder.php">Trust Builder</a></p>
                </div> -->

		</div>
		
	</div>
</div>

<!--<div class="footer">
	<div class="container footercontent">
		<div class="clearfix">
			<div class="copyfootermenu">
				<div class="copyright">2013 &copy;trulymadly.com</div>
				<nav class="footernav">
					<a href="#">About</a> <a href="#">Privacy Policy</a> <a href="#">Terms
						to Use</a> <a href="#">Disclaimers</a> <a href="#">Contact Us</a>
				</nav>
			</div>
			<div class="socialicons">
				<ul class="clearfix">
					<li class="join">Join us on:</li>
					<li class="fbicon"><a href="#"><i class="icon-facebook"></i> </a></li>
					<li class="fbicon"><a href="#"><i class="icon-linkedin"></i> </a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
--></div>

<iframe name="file_upload_frame" id="file_upload_frame" height="0"
			width="0" style="height: 0px; width: 0px; border: none;"> </iframe>
		<form method="post" id="upload-doc-form" target="file_upload_frame"
			enctype="multipart/form-data">
			<input type="file" id="file" size="0" 
				onchange="uploadFile()" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" /> <input type="hidden" value=""
				name="type" id="upload-file-type" /> <input type="hidden"
				id="upload_proof_type" name="proofType" value="" /> <input
				type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
		</form>
		<iframe name="file_upload_frame1" id="file_upload_frame1" height="0"
			width="0" style="height: 0px; width: 0px; border: none;"> </iframe>
		<form method="post" id="upload-doc-form1" target="file_upload_frame1"
			enctype="multipart/form-data">
			<input type="file" id="file1" size="0" 
				onchange="uploadFileAddress()" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" /> <input type="hidden" value=""
				name="type" id="upload-file-type1" /> <input type="hidden"
				id="upload_proof_type1" name="proofType" value="" /> <input
				type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
		</form>
		<iframe name="file_upload_frame2" id="file_upload_frame2" height="0"
			width="0" style="height: 0px; width: 0px; border: none;"> </iframe>
		<form method="post" id="upload-doc-form2" target="file_upload_frame2"
			enctype="multipart/form-data">
			<input type="file" id="file2" size="0" 
				onchange="uploadFileEmployment()" name="file" style="opacity:0; height: 0px; width: 0px; overflow: hidden;" /> <input type="hidden" value=""
				name="type" id="upload-file-type2" /> <input type="hidden"
				id="upload_proof_type2" name="proofType" value="" /> <input
				type="hidden" name="fileSubmit" value="submit" class="sbmtbtn" />
		</form>


<link
	rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
    
    <!-- 
    <script>
    $('#sndpopupframeform').submit(
    	    function(e){
        	    var isError = false;
        	    // hide error div set display none
        	    $('#error_form').css('display','none');
            	 if( $('[name="CheckboxGroup1\[\]"]:checked').length ==0 ){
                	 isError = true;
            	 }
            	 else if( $('[name="CheckboxGroup2\[\]"]:checked').length ==0 ){
            		 isError = true;
            	 }
            	 else if( $('[name="RadioGroup1"]:checked').length ==0 ){
            		 isError = true;
            	 } /*else if($('[name="RadioGroup1"]:checked').val()=='q3ans1'){
            		 if($('[name="years"]').val()==""){
                		 isError = true;
            		 }
            	 }	*/
            	 else if( $('[name="RadioGroup2"]:checked').length ==0 ){
            		 isError = true;
            	 }
            	 if(isError){
                	 //show error div , display block
                	 
                	 $('#error_form').css('display','block');
                	 
                	// alert('error');
                	 e.preventDefault();
            	 } 
        	    
    	    });
    $('[name="RadioGroup1"]').change(function(e){
    	if($('[name="RadioGroup1"]:checked').val()=='q3ans1'){
        	$('#married_years_span').show();
    	}
    	else{
    		$('#married_years_span').hide();
    	}
        });
    </script> -->
<!--
 <script>
 function hideInviteBoxes(){
	if($('#alertbox').css('display') == 'block'){
	 document.getElementById('alertbox').style.display='none';
	 document.getElementById('diffframe4').style.display='none';
	}
	 }
 
 function invitefun1(){
$('#diffframe4').css('display','block');
$('.loaderimg').css('display','block');
 setTimeout(invitefun2, 2000);
 
 }
 function invitefun2(){
	 var pathname = "{$baseurl}/dashboard.php";
	 $.post(  pathname,{
			invite_friends:"invite-friends"
		}).done();
	  $('.loaderimg').css('display','none');
	 $('.sndmpopupframe').css('display','block');
 }
 $('.difusescreen').click(hideInviteBoxes);
 </script>   
 -->
 
 <script>
    
function fbtopen()
{
document.getElementById("fbtitle").style.display="block";
}
function fbtclose()
{
document.getElementById("fbtitle").style.display="none";
}

function litopen()
{
document.getElementById("lititle").style.display="block";
}
function litclose()
{
document.getElementById("lititle").style.display="none";
}

function idtopen()
{
document.getElementById("idtitle").style.display="block";
}
function idtclose()
{
document.getElementById("idtitle").style.display="none";
}

function phtopen()
{
document.getElementById("phtitle").style.display="block";
}
function phtclose()
{
document.getElementById("phtitle").style.display="none";
}

function adtopen()
{
document.getElementById("adtitle").style.display="block";
}
function adtclose()
{
document.getElementById("adtitle").style.display="none";
}

function emtopen()
{
document.getElementById("emtitle").style.display="block";
}
function emtclose()
{
document.getElementById("emtitle").style.display="none";
}
</script>
      
    

</body>
</html>
