<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<title>TrulyMadly - My Likes</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
//console.log(headLoadTimeStamp);
</script>
<link href="{$cdnurl}/css/match/matches.css?v=8" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="{$cdnurl}/css/common/headfooter.css?v=3" rel="stylesheet" type="text/css">

</head>

<body>
{include file='templates/common/header.tpl'}

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<div class="mutmatchsec">
<div class="mmwraper">
<h3>My Likes</h3>
{foreach from=$mutual_matches item=mutual_like}

<!--Mutualmatch Start -->

 <!-- Match Tiles Start -->
 <a href="{$mutual_like.profile_data.profile_link}"><div  id="like_div_{$mutual_like.profile_data.profile_id}">
    <ul class="matchblue">
    <li class="mmtd1 fl"><img src="{$mutual_like.profile_data.profile_pic}"></li>
    <li class="mmtd2 fl"><h4>{$mutual_like.profile_data.fname}</h4><p>{$mutual_like.profile_data.age}, {$mutual_like.profile_data.city} <br> <span>Liked on : {$mutual_like.profile_data.timestamp} </span></p></li>
    </ul>
    </div></a>
 
    {/foreach}
    	{if $mutual_matches|@count eq 0}
    	<div class="emptymsg">Hmmm...no one liked yet.</div>
    	{/if}
<!-- Match Tiles end -->
</div>
</div>
<!--Mutualmatch end -->

</div>
<!--Centered Container End -->
</div>

<script>
var base_url='{$baseurl}';
var myId = "{$my_id}";
var baseurl='{$baseurl}';

</script>

<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/common/common.js?v=1"></script>


<script>
$(document).ready(function() {

	finalTemplateRenderTime  = (new Date()).getTime();
	//console.log(finalTemplateRenderTime);
	{literal}
	matchObj = {"data":{"time_taken":(finalTemplateRenderTime - headLoadTimeStamp)/1000, "activity":"myLikes", "user_id":myId, "event_type":"page_load" , "source":"web"}};
	{/literal}
	logEvent(matchObj);
});
</script>	

{include file='templates/common/footer.tpl'}


</body>
</html>
