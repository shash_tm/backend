{include file='../header_dashboard.tpl'}

<script type="text/javascript">
$('.matches_header').removeClass('active');
$('.dashboard_header').addClass('active');
</script> 

 <div class="container mt80">
 <link type="text/css" rel="stylesheet" href="css/register/verifypayment.css"> 

 {if ($status neq 'authentic')}
 <!-- verification section start -->
 <div class="tabcontentsec ovrf mt30 shadowboxv">To view member pictures and be visible to other members we need to authenticate your ID:

<div class="clear mt20">
<div class="fbimportveribtn"><a href="#" onclick="proceed()" id="verify_fb"><img src="images/register/add_from_fb.png" align="absmiddle" />Profile Authentication</a>
<img src="images/register/loader_msg.gif" style="position: absolute; margin-top: -28px; margin-left: 86px; display: none;" width="24" height="24" class="fl" id="loader_fb">
</div>
{if ($idVerified neq 'P')}
<span class="fl" style="padding-top:5px;">&nbsp;&nbsp;or&nbsp;&nbsp;</span>
<div class="styled-select fl mrg">
							<select id="idSelectBox" onchange="idChange()">
										<option selected="selected" value="0">Select & Upload Photo ID</option>
										<option value="Passport">Passport</option>
										<option value="Driving License">Driving License</option>
										<option value="Pan Card">Pan Card</option>
										<option value="Voter ID">Voter ID</option>
										<option value="Aadhar Card">Aadhar Card</option>
									</select>
</div>
  
<img width="13" height="15" src="images/register/indicator_left.png" style="position:absolute; margin:10px 0 0 12px;">
<div class="vactionbox" id="idVerificationButton" style="display: none;"><a href="#" onclick="$('#verifyPhoto').click();">Upload from Computer</a><span>or</span><a href="#" onclick="openWebCam()">Click & Upload using Webcam</a>
<input type="file" id="verifyPhoto"	style="display: none;" onchange="photoUploader(1)">
<!--<img src="images/register/loader_msg.gif" width="24" height="24" class="fl"> <span class="fl">Myphotoid.jpg</span>-->
</div>
<div class="vactionbox" id="idVerificationButton_show"><a href="#" style="background-color: #999;" onclick="alert('Select Id type')">Upload from Computer</a><span>or</span><a href="#" style="background-color: #999;" onclick="alert('Select Id type')">Click & Upload using Webcam</a>
<!--<img src="images/register/loader_msg.gif" width="24" height="24" class="fl"> <span class="fl">Myphotoid.jpg</span>-->
</div>
{else}
Thanks for uploading image. Status will be updated in 24 hrs.
{/if}-->
</div>
{if ($idVerified eq 'R')}
<div style="clear: both;">Your image was rejected.</div>
{/if}
</div>
{/if}
 <!-- verification section end -->
 
 
    	<!--Carousel-->
    	<div class="slider mt10">
    	 {if !isset($NoMatches)}
        	<p class="viewall"><a href="{$baseurl}/matches.php">View All</a></p>{/if}
            <div class="sliderul">
              {if isset($NoMatches)}
             <div class="verifyinfobox effect2">
     No Matches found. Please relax your Partner Preference   <a href="{$baseurl}/editpartner.php">Edit Partner Preference</a>
    </div>
                {else}
                <ul>
                
                 {foreach from=$userDetails key=k item=user}
                    <li>
                    <div class="panel" >
						<div class="front">
							<div class="pad">
                        <div class="hover bigphoto">
                            <img src="{$user.image_url}" style="width:225px;height:260px;">
                            <p class="name">{$user.name}, <span>{$user.age}</span></p>
                        </div>
                        <div class="perosonal-details" style="margin-top:5px!important;">
                         
<div class="tmtitle">
     <div class="pieContainer">
     <div class="pieBackground"></div>
     {if isset($user.trust_score)}
     <div id="pieSlice1" class="hold"><div class="pie" style="-webkit-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $user.trust_score}deg); -moz-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $user.trust_score}deg); -o-transform:rotate({math equation='(( x *1.8 ) -90 )' x= $user.trust_score}deg); transform:rotate({math equation='(( x *1.8 ) -90 )' x= $user.trust_score}deg);"></div></div>
     <div class="persentbg"><p>{$user.trust_score}%</p></div>
     {else}
      <div id="pieSlice1" class="hold"><div class="pie" style="-webkit-transform:rotate(-90deg); -moz-transform:rotate(-90deg); -o-transform:rotate(-90deg); transform:rotate(-90deg);"></div></div>
     <div class="persentbg"><p>0%</p></div>
     {/if}
</div>
     <p class="mt2">Trust Meter</p>
     </div>

                            <div class="persontext">
                               
                                     <p><span>{$user.height}</span>, {$user.religion}</p>
                                      <p>{$user.city} </p>
                            </div>
                        </div>
                           
                        </div>
                        </div>
                        <div class="back fliphover">
                            <div class="pad">
                                <p class="fliptitle">{$user.name}, <span>{$user.age}</span></p>
                                <div class="flippara">
                                <p>Education:</p>
                                <span>
                                {$user.highest_education|truncate:90:"..":true}
                                </span>
                              
                                <p>Occupation:</p>
                                <span>
                                {if ($user.industry|count_characters:true)>75}
                               {$user.industry|truncate:60:"..":true}
                               {else}
                                {$user.industry}
                               

      {if isset($user.working_area)}

, {$user.working_area|truncate:(60-($user.industry|count_characters:true)):"..":true}
                         
                                {/if}{/if}
                                </span>
                                <p>Income:</p>
                                <span>{$user.income_start} to {$user.income_end} </span>
                              <!--   <p>time:{$user.last_login}</p>
                                <span>{$user.org}</span>
                                <p>Siblings: {$user.siblings}</p>
                                <span>{$user.mutually_connected}</span></p> -->
                             </div>
								<p class="viewprofile"><a href="{$user.profile_link}" >View Profile</a></p>
                            </div>
                        </div>
                        </div>
                    </li>
                    {/foreach}
                  <!--   <li>
                        <div class="bigphoto">
                            <img src="images/dashboard/slider/slider2.jpg">
                            <p class="mutual"><span>f </span> Mutually Connected</p>
                            <p class="new">New</p>
                            <p class="name">PRIYANKA CHAWLA, <span>26</span></p>
                        </div>
                        <div class="perosonal-details">
                            <img src="images/dashboard/slider/trust-meter.png">
                            <div class="persontext">
                                <p>Chandigarh</p>
                                <p>Content writer</p>
                                <p><span>5'4"</span>, Hindu</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="bigphoto">
                            <img src="images/dashboard/slider/slider3.jpg">
                            <p class="new">New</p>
                            <p class="name">INDIRA GUPTA, <span>23</span></p>
                        </div>
                        <div class="perosonal-details">
                            <img src="images/dashboard/slider/trust-meter.png">
                            <div class="persontext">
                                <p>Lucknow</p>
                                <p>Sales professional</p>
                                <p><span>5'6"</span>, Hindu</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="bigphoto">
                            <img src="images/dashboard/slider/slider4.jpg">
                            <p class="new">New</p>
                            <p class="name">RENEE JOSEPH, <span>25</span></p>
                        </div>
                        <div class="perosonal-details">
                            <img src="images/dashboard/slider/trust-meter.png">
                            <div class="persontext">
                                <p>Mumbai,</p>
                                <p>Programmer</p>
                                <p><span>5'7"</span>, Christian</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="bigphoto">
                            <img src="images/dashboard/slider/slider5.jpg">
                            <p class="new">New</p>
                            <p class="name">Kajal Aggarwal, <span>23</span></p>
                        </div>
                        <div class="perosonal-details">
                            <img src="images/dashboard/slider/trust-meter.png">
                            <div class="persontext">
                                <p>Chennai</p>
                                <p>Actor</p>
                                <p><span>5'5"</span>, Hindu</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="bigphoto">
                            <img src="images/dashboard/slider/slider6.jpg">
                            <p class="mutual"><span>f </span> Mutually Connected</p>
                            <p class="new">New</p>
                            <p class="name">Kajal Aggarwal, <span>23</span></p>
                        </div>
                        <div class="perosonal-details">
                            <img src="images/dashboard/slider/trust-meter.png">
                            <div class="persontext">
                                <p>Chennai</p>
                                <p>Actor</p>
                                <p><span>5'5"</span>, Hindu</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="bigphoto">
                            <img src="images/dashboard/slider/slider7.jpg">
                            <p class="new">New</p>
                            <p class="name">Kajal Aggarwal, <span>23</span></p>
                        </div>
                        <div class="perosonal-details">
                            <img src="images/dashboard/slider/trust-meter.png">
                            <div class="persontext">
                                <p>Chennai</p>
                                <p>Actor</p>
                                <p><span>5'5"</span>, Hindu</p>
                            </div>
                        </div>
                    </li>-->
                </ul> {/if}
        	</div>
        </div>
    	<div class="content mt40">
            <div class="rtpanel">
            	<!--Alerts-->
            	<div class="alert">
                    <p class="days"><span>Earn</span> Credits</p>
                    <p class="daysleft"> and Boost your ranking</p>
                    <p class="renewnow"><a href="{$baseurl_https}/trustbuilder.php">Trust Builder</a></p>
                </div>
                <!--Notification-->
          <!--       <div class="rtnotification">
                	<p class="ntitle">Notification</p>
                    <div class="clearfix notifylist">
                        <div class="notify">
                            <div class="icons time">
                                <span class="rtnotification-no">22</span>
                            </div>
                            <div class="notify-content">
                                <div class="details">
                                    <p>Voice call with <span><a href="#">Prachi</a></span> at 15:00 today</p>
                                    <p>Voice call with <span><a href="#">Uma</a></span> 9:00 tomorrow</p>
                                    <p>Shweta has accepted your chat request at 
                                    15:30, June 3, 2013</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify">
                            <div class="icons thumb">
                                <span class="rtnotification-no">22</span>
                            </div>
                            <div class="notify-content">
                                <div class="details">
                                    <p><span><a href="#">Neha, Anuradha, Vani</a></span> and 2 others have liked you</p>
                                    <p>LIKE THEM BACK</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify">
                            <div class="icons check">
                                <span class="rtnotification-no">22</span>
                            </div>
                            <div class="notify-content">
                                <div class="details">
                                    <p><span>Akanksha</span> has requested your address verification</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify">
                            <div class="icons message">
                                <span class="rtnotification-no">22</span>
                            </div>
                            <div class="notify-content">
                                <div class="details">
                                    <p><span><a href="#">Deepika</a></span> and <span><a href="#">Lakshmi</a></span> have sent you a new message</p>
                                    <p><span><a href="#">Vani</a></span> has replied to your message</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="leftpanel">
            	<div class="lttitle">
                	<span>TODAY</span>
                </div>
                <!--Updates-->
                <div class="updates">
                	<div class="updates-icon pointer"></div>
                    <div class="updates-white">
                        <div class="updates-content">
                         <!--    <p class="personimg"><img src="images/dashboard/updates/update1.jpg"></p> -->
                            <div class="updates-details">
                                <p><span><a href="#">Trulymadly Admin</a></span> wishes you all the best for your matches</p>
                                <p class="hours"><img src="images/dashboard/icons/time.png"> 1 Hours Ago</p>
                            </div>
                        </div>
					</div>
                </div>
                <div class="updates">
                	<div class="updates-icon pointer"></div>
                    <div class="updates-white">
                        <div class="updates-content">
                       <!--      <p class="personimg"><img src="images/dashboard/updates/update2.jpg"></p> -->
                            <div class="updates-details">
                                <p><span><a href="#">Trulymadly</a></span> welcomes you.</p>
                                <p class="hours"><img src="images/dashboard//icons/time.png"> 3 Hours Ago</p>
                            <!--     <ul class="photolist">
                                    <li><img src="images/dashboard/updates/photo1.jpg"></li>
                                    <li><img src="images/dashboard/updates/photo2.jpg"></li>
                                    <li><img src="images/dashboard/updates/photo3.jpg"></li>
                                </ul> -->
                            </div>
                        </div>
					</div>
                </div>
           <!--      <div class="updates">
                	<div class="updates-icon pencil"></div>
                    <div class="updates-white">
                        <div class="updates-content">
                            <p class="personimg"><img src="images/dashboard//updates/update3.jpg"></p>
                            <div class="updates-details">
                                <p><span><a href="#">Priyanka Sharma</a></span> has updated a section in her profile</p>
                                <p class="hours"><img src="images/dashboard/icons/time.png"> 3 Hours Ago</p>
                                <p class="mt20">- added her favorite music genre: �Blues�</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lttitle mt20">
                	<span>YESTERDAY, 21 JULY '13</span>
                </div>
                <div class="updates">
                	<div class="updates-icon pointer"></div>
                    <div class="updates-white">
                        <div class="updates-content">
                            <p class="personimg"><img src="images/dashboard/updates/update1.jpg"></p>
                            <div class="updates-details">
                                <p><span><a href="#">Neha Sharma</a></span> has updated her city to Mumbai</p>
                                <p class="hours"><img src="images/dashboard/icons/time.png"> 1 Hours Ago</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="updates">
                	<div class="updates-icon camera"></div>
                    <div class="updates-white">
                        <div class="updates-content">
                            <p class="personimg"><img src="images/dashboard/updates/update2.jpg"></p>
                            <div class="updates-details">
                                <p><span><a href="#">Nidhi Goel</a></span> has added 3 new pictures to her profile</p>
                                <p class="hours"><img src="images/dashboard/icons/time.png"> 3 Hours Ago</p>
                                <ul class="photolist">
                                    <li><img src="images/dashboard/updates/photo1.jpg"></li>
                                    <li><img src="images/dashboard/updates/photo2.jpg"></li>
                                    <li><img src="images/dashboard/updates/photo3.jpg"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    
    <div class="footer">
    	<div class="container footercontent">
        	<div class="clearfix">
                <div class="copyfootermenu">
                    <div class="copyright">2013 &copy;trulymadly.com</div>
                    <nav class="footernav">
                    	<a href="#">About</a> <a href="#">Privacy Policy</a> <a href="#">Terms to Use</a> <a href="#">Disclaimers</a> <a href="#">Contact Us</a>
                    </nav>
                </div>
                <div class="socialicons">
                    <ul class="clearfix">
                        <li class="join">Join us on:</li>
                        <li class="fbicon"><a href="#"><i class="icon-facebook"></i></a></li>
                        <li class="fbicon"><a href="#"><i class="icon-linkedin"></i></a></li>
                    </ul>
                </div>
			</div>
        </div>
    </div>
</div>
</body>
</html>
