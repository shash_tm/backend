<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="{$cdnurl}/images/dashboard/favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<title>TrulyMadly - Mutual likes</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
//console.log(headLoadTimeStamp);
</script>
<link href="{$cdnurl}/css/match/matches.css?v=8" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/fontstyle.css" rel="stylesheet" type="text/css">
<link href="{$cdnurl}/css/common/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="{$cdnurl}/css/common/headfooter.css?v=3" rel="stylesheet" type="text/css">

</head>

<body>
{include file='templates/common/header.tpl'}

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<div class="mutmatchsec">
<div class="mmwraper">
<h3>Mutual likes</h3>
{foreach from=$mutual_matches item=mutual_like}
{if $mutual_like.profile_data.is_blocked=="yes"}
<!-- Blocked match -->
<div class="mutual_like_blocked_div" id="blocked_div_{$mutual_like.profile_data.profile_id}{if $mutual_like.profile_data.ss eq '1'}_ss{/if}">
<ul class="matchblue">
<li class="mmtd1 fl"><img src="{$mutual_like.profile_data.profile_pic}"></li>
<li class="mmtd2 fl"><h4>{$mutual_like.profile_data.fname}</h4><p>{$mutual_like.profile_data.age}, {$mutual_like.profile_data.city} <br> <span>Matched on :{$mutual_like.profile_data.matched_on}</span></p></li>
<li class="mmtd3 fr"><img src="{$cdnurl}/images/profile/blockedicon.png"></li>
</ul>
</div>

{else}
<!--Mutualmatch Start -->

 <!-- Match Tiles Start -->
 <a href="{$mutual_like.profile_data.message_link}"><div  id="like_div_{$mutual_like.profile_data.profile_id}">
    <ul class="matchblue">
    <li class="mmtd1 fl"><img src="{$mutual_like.profile_data.profile_pic}"></li>
    <li class="mmtd2 fl"><h4>{$mutual_like.profile_data.fname}</h4><p>{$mutual_like.profile_data.age}, {$mutual_like.profile_data.city} <br> <span>Matched on : {$mutual_like.profile_data.matched_on} </span></p></li>
    <li class="mmtd3 fr"><img src="{$cdnurl}/images/profile/msgswhite.png">
    <img src="{$cdnurl}/images/profile/blockedicon.png" style="display:none;"></li>
    </ul>
    </div></a>
    {/if}
    {/foreach}
    	{if $mutual_matches|@count eq 0}
    	<div class="emptymsg">Aww! You don't have any mutual likes yet. But don't worry, good things come to those who wait.</div>
    	{/if}
<!-- Match Tiles end -->
</div>
</div>
<!--Mutualmatch end -->

</div>
<!--Centered Container End -->
</div>

<!-- Pop Up Start -->
<div id="diffframe10" style="display:none;">
<div class="difusescreen" align="center"></div>
<div class="mmpopupframe">
<p>Match is either not active anymore or has decided not to proceed further<br><br>
<a   class="mmactionbtn">OK</a></p>
</div>
</div>
<!-- Pop Up end -->
<script>
var base_url='{$baseurl}';
var myId = "{$my_id}";
var baseurl='{$baseurl}';

</script>

<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/common/common.js?v=1"></script>
<script src="{$cdnurl}/js/dashboard/mutual_like.js?v=1.2"></script>
<script src="{$cdnurl}/js/lib/jquery.infinitescroll.min.js?v=1"></script> 


<nav id="page_nav" style="display: none;">
    <a href="{$baseurl}/likes.php?page_id={$page_id}"></a>
 </nav>


{include file='templates/common/footer.tpl'}


</body>
</html>
