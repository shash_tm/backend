{if !empty($data)}
<ul class="customscroll vscroll" id="like_me_queue_list" style="width:100%;">
 {foreach from=$data item=user}
 <li class="clearfix">
<a href="{$user.profile_link}" class="fl mrg"><img src="{$user.thumbnail}" class="likes-pic" height="50" width="50" align="left"/> </a>
      <div class="likes-details fl mrg">
 <p class="likes-name">    <a href="{$user.profile_link}">{$user.fname} {$user.lname}</a>
 {if !isset($user.status)}
<span> likes your profile.</span>
{else if $user.status eq 'likedBoth'} 
<span>You’ve both liked each other.</span>
{else if $user.status eq 'likedBack'} 
<span>has liked you back.</span>
 {else if $user.status eq 'reject'} 
 <span>has been declined by you.</span>
 
 {else if $user.status eq 'rejectMe'} 
 <span>will like to pass on your interest.</span>
 
{/if}
</p>
         <p class="likes-info">{$user.timestamp}</p>
      </div>        
    </li>
  {/foreach}
                        </ul>
{/if}