<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="http://dev.trulymadly.com/trulymadly/images/dashboard/favicon.ico" type="image/x-icon">
<meta name="Description" content="{$endorse_msg}">
<meta content="{$endorse_msg}" property="og:title">
<meta content="{$endorse_msg}" property="og:description">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<title>TrulyMadly</title>
<script type="text/javascript">
headLoadTimeStamp  = (new Date()).getTime();
var allImgSrcUrls=[];
{foreach $attributes.other_pics  key=index item=val}
allImgSrcUrls[{$index}]="{$val}";
{/foreach}
var profilePicUrl = "{$attributes.profile_pic}";
var hasProfile = false;
if(profilePicUrl.indexOf("dummy") < 0){
	hasProfile = true;
	allImgSrcUrls[allImgSrcUrls.length] = profilePicUrl; 
}
//console.log(headLoadTimeStamp);
</script>
<link href="{$cdnurl}/css/shareProfile.css?v=1.3" rel="stylesheet" type="text/css">
</head>

<body>

<script type="text/javascript">
var baseurl = "{$baseurl}";
var id = "{if isset($smarty.get.id)}{$smarty.get.id}{else}0{/if}";
</script>

<div class="new_wrapper">
<!--Centered Container Start -->
<div class="new_container" align="center">
<div class="pageheader"><img src="{$cdnurl}/images/tmlogoendors.png" class="endhdrlogo">
<a href ="https://play.google.com/store/apps/details?id=com.trulymadly.android.app"><img src="{$cdnurl}/images/androidapp.png" class="appicon"></a></div>
<div class="prophoto overf">
<a href="#"><img src="{$cdnurl}/images/profile/spacer.png" class="profile-pic-img"> </a>
  {if $attributes.other_pics|@count >0}
        <div class="photothumbs gallery">
         <!--	<a href="#" class="icon-chevron-sign-left fl nextprevbtn"></a> -->
        	{foreach $attributes.thumbnail_pics  key=index item=val}
        	<img class="thumbnail-img" index="{$index}" src="{$val}"> 
        	{/foreach}
        	
           <!--  <a href="#" class="icon-chevron-sign-right fr nextprevbtn"></a>-->
         </div>
         {/if}
         </div>
<!--   <img src="{$attributes.profile_pic}" class="usrimage">-->
<div>
<div class="userdetails"><p>{$attributes.fname}, {$attributes.age}</p>
<span>From {$attributes.city} | {$attributes.height} | {$attributes.marital_status}</span></div>
</div>

<!-- Hashtag section start -->
{if !empty($attributes.InterestsHobbies.preferences)}
<h3>Hashtags</h3>
<div class="hashtagsec">
<ul>
{foreach $attributes.InterestsHobbies.preferences item=val}
<li>{$val}</li>
{/foreach}
</ul>
</div>
{/if}
<!-- Hashtag section end -->

<h3>Work & Education</h3>
<div class="wnedetails">
        <ul>
            <li class="iconcell"><img src="{$cdnurl}/images/work.png?v=1"></li>
            <li class="detcell"><p>{if isset($attributes.industry)}{$attributes.industry}{/if}{if isset($attributes.designation) && isset($attributes.industry)}, {/if}{if isset($attributes.designation)}{$attributes.designation}{/if}</p> 
              
              {if isset($attributes.companies)}
              {assign var=last1 value=$attributes.companies|@end}     
            	<span>{foreach $attributes.companies key=i item=val}{$val}{if $val neq $last1},&nbsp;{/if}{/foreach}</span>
              {/if}	
        </ul>
        <ul>
            <li class="iconcell"><img src="{$cdnurl}/images/education.png?v=1"></li>
            <li class="detcell"><p>{$attributes.highest_degree}</p> 
            <span>
            {if isset($attributes.institutes)}
           	   {assign var=last1 value=$attributes.institutes|@end}     
            	{foreach $attributes.institutes key=i item=val}{$val}{if $val neq $last1},&nbsp;{/if}{/foreach}
            {/if}
           	   </span></li>
        </ul>
    </div>
    
    <!-- Favourites section start -->  
{if !empty($attributes.favourite) } 
<h3>Favourites</h3>   
{/if} 
<div class="favitemsec">
{if !empty($attributes.favourite) }  
<div class="clb"  style="margin-top:10px;">

<ul class="inticons">
                        	<li class="intmovie"><span class="hijack" key="0" onClick="document.getElementById('arrowtop').style.left='20px';"></span></li>
                        	<li class="intmusic"><span class="hijack" key="1" onClick="document.getElementById('arrowtop').style.left='75px';"></span></li>
                        	<li class="intbooks"><span class="hijack" key="2" onClick="document.getElementById('arrowtop').style.left='130px';"></span></li>
                        	<li class="intfood"><span class="hijack" key="3" onClick="document.getElementById('arrowtop').style.left='185px';"></span></li>
                        	<li class="intother"><span class="hijack" key="4" onClick="document.getElementById('arrowtop').style.left='240px';"></span></li>
                    	</ul>
                        
                        <div class="posr">
                        <img src="{$cdnurl}/images/profile/arrowpointertab.png" border="0" class="indicatetop" id="arrowtop" style="left:20px;">

							<div class="comnlikes">
                            {if isset($attributes.InterestsHobbies.movies_favorites)}
                        	
           					<span class="colorcode1" name="movie" value="0">
           					{assign var=last2 value=$attributes.InterestsHobbies.movies_favorites|@end}     
	                		{foreach $attributes.InterestsHobbies.movies_favorites item=val}
							{$val}
							{if $val neq $last2},{/if}
							{/foreach}
                            </span>
                            {/if}
                            
                        
                        <span class="seperaterline">|</span>
                            {if isset($attributes.InterestsHobbies.music_favorites)}
                            
                			<span class="colorcode2" name="music" value="1">
           				    {assign var=last3 value=$attributes.InterestsHobbies.music_favorites|@end}     
                			{foreach $attributes.InterestsHobbies.music_favorites item=val}
							{$val}
							{if $val neq $last3},{/if}
							{/foreach}
                            </span>
							{/if}
                            
                            <span class="seperaterline">|</span>
                			{if isset($attributes.InterestsHobbies.books_favorites)}
                           
                			<span class="colorcode1" name="books" value="2">
                			{assign var=last4 value=$attributes.InterestsHobbies.books_favorites|@end}     
                			{foreach $attributes.InterestsHobbies.books_favorites item=val}
							{$val}							{if $val neq $last4},{/if}
							{/foreach}
                            </span>
                            
                            {/if}

							<span class="seperaterline">|</span>                            
                 			{if isset($attributes.InterestsHobbies.travel_favorites)}
                           
                			<span class="colorcode2" name="food" value="3">
							{assign var=last4 value=$attributes.InterestsHobbies.travel_favorites|@end}     
	                		{foreach $attributes.InterestsHobbies.travel_favorites item=val}
							{$val}{if $val neq $last4},{/if}
							{/foreach}
                            </span>
                            {/if}
                            
                            <span class="seperaterline">|</span>
                			{if isset($attributes.InterestsHobbies.other_favorites)}                            
                            
                			<span class="colorcode1" name="others" value="4">
                			{assign var=last4 value=$attributes.InterestsHobbies.other_favorites|@end}     
	                		{foreach $attributes.InterestsHobbies.other_favorites item=val}
							{$val}{if $val neq $last4},{/if}
							{/foreach}
                			</span>
                			{/if}
                            </div>
                       </div>
                        
                	
                </div>    {/if} 
</div> 

    <!-- Favourites section start -->  
        
</div>
</div>

<script src="{$cdnurl}/js/jquery-1.8.2.min.js"></script>
<script src="{$cdnurl}/js/common/common.js"></script>
<script src="{$cdnurl}/js/dashboard/jquery.magnific-popup.js"></script>


<script type="text/javascript">
$(document).ready(function() {

	/**
	code for logging on page load
	*/
	finalTemplateRenderTime  = (new Date()).getTime();
	activity = "page_load";
	logObj = {
		"data" : {
			"time_taken" : (finalTemplateRenderTime - headLoadTimeStamp) / 1000,
			"activity" : "shareProfile",
			"event_type" : activity,
			"user_id" : id,
			"event_info" : {
				"cookie_id" : headLoadTimeStamp
			}
		}
	};
	logEvent(logObj, 1);



/**
 * code for photo gallery
 */

 $( ".prophoto" ).css("background-image", 'url('+profilePicUrl+')');
	allPicsObject = [];
	for (var i=0;i<allImgSrcUrls.length;i++){
		allPicsObject[i] = {
				src:allImgSrcUrls[i],
				type: 'image'
				};
	}
	if(hasProfile){
		$('.profile-pic-img').click(function(event){
			showPopup(allImgSrcUrls.length - 1);
		});
	}
	$('.thumbnail-img').click(function(event){
		showPopup(parseInt($(event.target).attr('index'),10));
	});
	function showPopup(index){
		$.magnificPopup.open({
	    	items: allPicsObject,
	          gallery: {
	            enabled: true,
	            navigateByImgClick : true,
				preload : [ 0, 1 ]
	          },
	          type: 'image',
	          callbacks: {
	        	    open: function(){
	        	  $(document).bind('touchmove', function(e) {
	        	       e.preventDefault();
	        	});
	        	    },
	        	    close: function(){
	        	    	$(document).unbind('touchmove');
		        	    }
	        	  }
	      }, index);
	}


	
});

$('.hijack').click(function(event){
		var left = 0;
		var key = parseInt($(event.target).attr('key'), 10);
		if(key>0){
			left = 30;
		}
		for(var i=0;i<key;i++){
			left += $('.comnlikes [value="'+i+'"]').width();
		}
		$('.comnlikes').scrollLeft(left);
	});

</script>
</body>
</html>
