<?php
require_once dirname ( __FILE__ ).'/../include/aws/aws-autoloader.php';
require_once dirname ( __FILE__ ).'/../include/S3.php';
require_once dirname(__FILE__).'/../DBO/VideoDBO.php';
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
include_once (dirname ( __FILE__ ) . '/../photomodule/Photo.class.php');
require_once dirname(__FILE__).'/../logging/EventTrackingClass.php';

use Aws\Sqs\SqsClient;
use Aws\S3\S3Client;
use Aws\ElasticTranscoder\ElasticTranscoderClient;


Class VideoEncoder
{

    private $Encoder_Client=null;
    private $s3_input=null;
    private $s3_output=null;
    private $user_id=null;
    private $VideoDBO=null;
    private $filename=null;
    private $extension=null;
    private $complete_file_url;
    private $caption='';
    private $width=null;
    private $height=null;
    private $duration=null;
    private $properties_assigned=false;
    private $mute=false;
    private $min_allowed_time=-1;
    private $max_allowed_time=-1;
    private $is_rotate=null;
    private $is_crop=false;
    public $event_logging;
    private $s3;
    private $preset_id;
    public $isTrimmingUsed;
    function __construct($user_id)
    {
        global $config,$video_s3_output_prefix;
        //Extract the key and secret from
        $key=$config['amazon_s3']['access_key'];
        $secret=$config['amazon_s3']['secret_key'];
        $bucket_region=$config['video_profile']['s3_region'];
        $this->Encoder_Client = ElasticTranscoderClient::factory(array(


            'key' => "$key",
            'secret' => "$secret",
            'region' => 'ap-southeast-1',
            "version" => '2012-09-25'

        ));
        $this->isTrimmingUsed=false;
        $this->is_crop=false;
        $this->s3_input='videos/';
        $this->s3_output=$video_s3_output_prefix;
        $this->user_id=$user_id;
        $this->min_allowed_time=4;
        $this->max_allowed_time=25;
        $this->VideoDBO=new VideoDBO();
        $this->event_logging=new EventTrackingClass();
        $this->s3=new S3($key,$secret,true,"s3-$bucket_region.amazonaws.com");


    }

    public function Authenticate($filename)
    {
        //Return true if it is authenticated
        $explode=explode('.',$filename);
        if(count($explode)==2) {

            $filename = $explode[0];
            $extension = $explode[1];
            $this->extension=$extension;
            $params = $this->VideoDBO->GetHashParameters($this->user_id);
            $md5 = $params['myhash'];

            $explode = explode('_', $filename);
            if (count($explode) > 0) {
                $user_id = $explode[1];
                if($user_id!=$this->user_id)
                {
                    return false;
                }
                $md5_new = $explode[0];
                //echo $md5_new;
                //echo $md5;
                //TO:DO Remove this true in the OR condition

                if ($md5 == $md5_new) {
                    $this->filename = $filename;
                    return true;
                } else {
                    return false;
                    $this->filename = null;
                }
            }
            $this->filename = null;
            return false;
        }
        $this->filename = null;
        return false;
    }

    public function GetS3SignedUrl($key)
    {


    }

    public function DecideResolutions(){

        global $video_cdn;
        //Video Rule is the CDN URL
        $url_prefix=$video_cdn;


        //$url_prefix = 'https://s3-ap-southeast-1.amazonaws.com/trulymadlyvideostest/';
        //$video_cdn=$url_prefix;
        //$process_array = array(1080 => array("720" => "1351620000001-000010", "480" => "1351620000001-000020", "360" => "1351620000001-000040"), 720 => array("480" => "1351620000001-000020", "360" => "1351620000001-000040"), 480 => array("360" => "1351620000001-000040"), 360 => array("240" => "1468238166611-8xv6ec"),240=>array("240" => "1468238166611-8xv6ec"));

        $output=null;
        $file_name=$this->filename;
        if ($file_name!='')
        {
            //$complete_file_url=$url_prefix.$this->s3_input.$file_name.'.'.$this->extension;
            $this->complete_file_url=$this->getSignedURl($this->s3_input.$file_name.'.'.$this->extension);
            //echo $complete_file_url;
            $mini=-1;
            if(!$this->properties_assigned) {

                $time1=time();

                $command = "ffprobe -v quiet -print_format json -show_format -show_streams '$this->complete_file_url'";
                $status = 1;

                $json_output = shell_exec($command);
                $time_taken=time()-$time1;
                $log_array=array("user_id"=>$this->user_id,"activity"=>"videos","event_status"=>"success","time_taken"=>$time_taken,"event_type"=>"pramaters_calculate","event_info"=>null,"source"=>"backend");
                $this->event_logging->logActions(array($log_array));
                if ($json_output != null) {

                    $streams = json_decode($json_output, true);

                    $stream_index=0;
                    $video_stream_index=0;
                    //echo $json_output;


                    foreach($streams["streams"] as $stream) {

                        if ($stream['codec_type'] == 'video') {
                            $video_stream_index = $stream_index;
                        }
                        $stream_index++;
                    }

                    $stream = $streams["streams"][$video_stream_index];
                    $this->width = $stream["coded_width"];
                    $this->height = $stream["coded_height"];
                    $this->duration=$stream["duration"];


                    if($this->duration==null)
                    {
                        $output["responseCode"]=403;//
                        $output['error_code']="4";
                        $output["error"]="Video could not be uploaded. Please try again.";
                    }

                    if($this->duration<=$this->min_allowed_time||$this->duration>=$this->max_allowed_time)
                    {

                        $output["responseCode"]=403;
                        $output["error"]="Video duration should be in between 5 to 15 secs";
                        $output['error_code']="5";
                        $output["duration"]=$this->duration;
                        return $output;
                    }
                } else
                {
                    //Could not get the data from FFprobe
                    trigger_error("Not getting Data from FFProbe, Either Not Installed or Check $this->complete_file_url",E_USER_WARNING);
                }

            }
            if ($this->width > $this->height) {
                $mini = $this->height;
            } else {
                $mini = $this->width;

            }

            $job_id=-1;
            $process_array=null;//
            if($this->is_crop)
            {
                $square_fill=array(480=>"1474441817484-nplz6m",360=>"1474442002138-w6xoxh",240=>"1474442042496-f5ppkj");
                $square_fill_mute=array(480=>"1474441908406-zfprb5",360=>"1474441971139-b5sirh",240=>"1474442079570-113k44");

                $curr_array=null;
                if(!$this->mute)
                {
                    $curr_array=$square_fill;
                }
                else{
                    $curr_array=$square_fill_mute;
                }

                foreach($curr_array as $key=>$val)
                {
                    if($key<=$mini)
                    {
                        $process_array=array(1080=>array("low"=>$val));
                        break;
                    }
                }


            }

            if($process_array==null&&$mini<240)
            {
                //If the video is already uploaded in the lowest encoding that we support , for example 225 px, then manually give
                //preset id
                if(!$this->mute) {
                    $process_array = array(1080 => array("low" => "1474442042496-f5ppkj"));
                }
                else {
                    $process_array = array(1080 => array("low" => "1474442079570-113k44"));
                }
            }


            //First find out o
            if(intval($this->width)/intval($this->height)!=1&&$process_array==null) {

                $output['responseCode']=403;
                $output['error']="Video could not be uploaded. Please try again.";
                $output['error_code']="6";
                return $output;
                /*
                if($this->mute) {
                    $process_array = array(1080 => array("low" => "1469706779233-c4v2pz"));
                }else {
                    $process_array = array(1080 => array("low" => "1469682788508-3ddxoh"));
                }*/
            }
            /*
            else
            {
                //If this is a square aspect ratio then apply other preset
                if($this->mute) {
                    $process_array = array(1080 => array("low" => "1469707210528-vs447d"));
                }
                else {
                    $process_array = array(1080 => array("low" => "1469682788508-3ddxoh"));
                }
            }
            */
            
            //$lists=array_keys($process_array);
            foreach ($process_array as $keys => $versions) {
                if (intval($keys) >= intval($mini)) {
                    if ($versions != null && count($versions) > 0) {
                        $time1=time();
                        $job_id =$this->CallAWsTranscoder($versions);

                        $time_taken = time() - $time1;
                        $event_status='';
                        if($job_id!=null) {
                            $event_status='success';
                        }
                        else {
                            //Its a fail, log it as fail
                            $event_status='error';
                        }
                        $log_array = array("user_id" => $this->user_id, "activity" => "videos", "event_type" => "trancoder_server_call", "time_taken" => $time_taken, "event_status" => $event_status, "source" => "backend");
                        //Log it
                        $this->event_logging->logActions(array($log_array));

                    }
                    break;
                }
            }

            if($job_id==-1)
            {
                //encode it into the lowest possible rate that is 360 p
                $job_id=$this->CallAWsTranscoder(array("low" => "1474441817484-nplz6m"));
            }
            if($job_id==null)
            {
                trigger_error("Transcode Server Call Error: $this->user_id" ,E_USER_WARNING);
                $output['responseCode']=403;
                $output['error']="Video could not be uploaded. Please try again.";
                $output['error_code']="1";
                return $output;
            }

            //$video_id=-1;
            $video_id=$this->CreateVideoEntry($this->width,$this->height,$job_id);
            //No job was initiated, the video uploaded was already in the smallest resolution
            $output['video_id']=$video_id;
            $output['responseCode']=200;
            $output['UntranscodedUrl']=$this->complete_file_url;
            $output['width']=$this->width;
            $output['height']=$this->height;
            $output['preset_id']=$this->preset_id;
            $ouput['isTrimmingUsed']=$this->isTrimmingUsed;

        }
        else
        {
            $output['responseCode']=403;
            $output['error_code']="2";
            $output['error']="Video could not be uploaded. Please try again.";
        }

        //var_dump($output);
        return $output;
    }


    public function CreateVideoEntry($width,$height,$jobid)
    {
        //Create a new Video Id and Pass It on For Transcoding
        if(!isset($this->duration))
        {
            $this->duration=-1;
        }
        if(!isset($this->caption))
        {
            $this->caption=NULL;
        }
        if(!isset($this->mute))
        {
            $this->mute=false;
        }

        return $this->VideoDBO->InsertNewVideo($this->user_id,$jobid,$this->filename.'.'.$this->extension,$width,$height,$this->caption,$this->mute,$this->duration);
    }


    public function MarkDelete($video_id)
    {
        $this->VideoDBO->MarkDeleted($this->user_id,$video_id);
        $output['responseCode']=200;
        $output['response']="OK";
        return $output;

    }

    function CheckIfProgramInstalled()
    {
        //To check if FFprobe is installed or not
    }

    function CallAWsTranscoder($versions)
    {

        //This $filename is with extension
        try {
            //echo 'In function';
            global $aws_pipeline;
            $filename=$this->filename;
            $extension=$this->extension;
            $output=array();
            foreach ($versions as $reso => $preset) {
                $arr = array("Key" => "$filename" . "_" . $reso . ".mp4", 'PresetId' => "$preset", "ThumbnailPattern" => "$filename" . "_thumb_$reso-{count}");
                $allowed_values=array("0","90","180","270");
                //Apply trimming to extract only the first 15 secs of the video
                if($this->duration>15) {

                    $timespan = array("StartTime" => "00:00:00.000", "Duration" => "00:00:15.000");
                    $compostion = array("TimeSpan" => $timespan);
                    $arr['Composition'] = array($compostion);
                    $this->isTrimmingUsed=true;

                }

                if(in_array($this->is_rotate,$allowed_values)) {
                    $arr['Rotate'] = $this->is_rotate;
                }else {
                    $arr['Rotate']='auto';
                }

                array_push($output, $arr);

                $this->preset_id=$preset;
            }

            $output_resolutions=json_encode(array_keys($versions));
            /*
            echo json_encode(array(

                'PipelineId' => "$aws_pipeline",
                'OutputKeyPrefix' => "$this->s3_output",
                'Input' => array(
                    'Key' => "$this->s3_input$filename.$extension",
                    'FrameRate' => 'auto',
                    'Resolution' => 'auto',
                    'AspectRatio' => 'auto',
                    'Interlaced' => 'auto',
                    'Container' => 'auto',

                ),
                'Outputs' => $output,
                'UserMetadata'=>array("res"=>$output_resolutions,"user_id"=>$this->user_id)

            ));die;*/
            $job = $this->Encoder_Client->createJob(array(

                'PipelineId' => "$aws_pipeline",
                'OutputKeyPrefix' => "$this->s3_output",
                'Input' => array(
                    'Key' => "$this->s3_input$filename.$extension",
                    'FrameRate' => 'auto',
                    'Resolution' => 'auto',
                    'AspectRatio' => 'auto',
                    'Interlaced' => 'auto',
                    'Container' => 'auto',

                ),
                'Outputs' => $output,
                'UserMetadata'=>array("res"=>$output_resolutions,"user_id"=>$this->user_id)

            ));


            $jobData = $job->get('Job');
            $jobId = $jobData['Id'];
            //echo json_encode($jobData);die;
            //To Do: Remember to handle the null cases of JobId
            return $jobId;
        }
        catch(Exception $ex)
        {
            trigger_error("Transcode Server Call Error: $this->preset_id : ".$ex->getMessage(),E_USER_WARNING);
            return null;
        }


    }

    public function AssignVariables($height,$width,$duration)
    {
        $this->height=$height;
        $this->width=$width;
        $this->duration=$duration;
        $this->properties_assigned=true;

    }

    public function AssignCaption($caption)
    {
        $this->caption=$caption;
    }

    public function AssignMute($mute)
    {

        $this->mute=filter_var($mute,FILTER_VALIDATE_BOOLEAN);
    }

    public function AssignRotate($rotate)
    {
        $this->is_rotate=$rotate;
    }

    public function AssignCrop($crop)
    {
        $this->is_crop=filter_var($crop,FILTER_VALIDATE_BOOLEAN);;
    }

    public function getJobstatus($job_id)
    {
        $response=$this->Encoder_Client->readJob(array('Id'=>$job_id));
        return ($response->get('Job'));
    }


    public function process_output($output,$user_id)
    {
        if($output['responseCode']==200)
        {

            try {
                $preset=$output['preset_id'];
                $video_id=$output['video_id'];
                $isTrim=$output['isTrimmingUsed'];
                //Extract all the properties above, as output is nulled in the next line
                $output = null;
                $photoModule = new Photo($user_id);
                //Recreate the array,because the response of photo.php is needed here
                $encoded_data = $photoModule->getUserPhotos();
                if (!isset($encoded_data)) {
                    $encoded_data = array();
                }
                $video_data = $photoModule->getUserVideos();
                $allowSkip = false;
                if (count($encoded_data) > 0) {
                    $allowSkip = true;
                }
                $output = array("isTrimmingUsed"=>$isTrim,"responseCode" => 200,"presetUsed"=>$preset,"video_id"=>$video_id ,"data" => $encoded_data, "allowSkip" => $allowSkip, "videos" => $video_data);

            }
            catch (Exception $ex)
            {
                trigger_error($ex->getMessage(),E_USER_WARNING);
            }


        }

        return $output;

    }

    public function getSignedURl($key)
    {
        global $video_s3_bucket;
        $signed_url=$this->s3->getAuthenticatedURL($video_s3_bucket,$key,300,false,true);
        return $signed_url;

    }


}



?>
