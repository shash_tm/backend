<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname(__FILE__)."/../DBO/VideoDBO.php";
require_once dirname(__FILE__)."/../admin/logging.php";



try
{
//	$dealAdminObj  = new adminLogin("dealAdmin/dealView.php") ;
//	$dealAdminObj->checkAdminSession() ;///
    global $video_cdn;

    if($_POST['type']=='admin_action'&&($_SESSION['super_admin']=='yes'||in_array('video_edit',$_SESSION['privileges'])))
    {
        $action=$_POST['action'];
        $video_id=$_POST['video_id'];
        $manager=new AdminVideos();
        $manager->TakeAdminAction($video_id,$action,$_SESSION['admin_id']);
        echo json_encode(array("responseCode"=>200,status=>'success','approve'=>$action));
        die;
    }


    if($_POST['type']=='admin_action_vision'&&($_SESSION['super_admin']=='yes'||in_array('video_edit',$_SESSION['privileges'])))
    {
        $action=$_POST['action'];
        //This action will be in 'yes' or 'no'
        $video_id=$_POST['video_id'];
        $manager=new AdminVideos();
        $manager->VisionFromAdmin($video_id,$action,$_SESSION['admin_id']);
        echo json_encode(array("responseCode"=>200,status=>'success','approve'=>$action));
        die;
    }

    if($_SESSION['super_admin'] == 'yes' || in_array('video_view', $_SESSION['privileges']) )
    {


        $action=true;
        if($_GET['action']=='no_action') {
            $action=false;
        }
        $page_no=intval($_GET['page_no']);
        $manager = new AdminVideos($action);


        $videos = $manager->getVideoList($page_no);
        $smarty->assign("has_more",$manager->has_more);
        $smarty->assign("video_count",$manager->total_count);
        $smarty->assign ("baseurl" , $admin_baseurl);
        if(in_array('video_view',$_SESSION['privileges']) == false &&  $_SESSION['super_admin'] != 'yes') {
            $smarty->assign ("read_only" , "true");
        }else{
            $smarty->assign ("read_only" , "false") ;
        }
        $smarty->assign ("videos" , $videos) ;
    }


    $smarty->display ( "../templates/videos.tpl" );

} catch (Exception $e) {
    trigger_error( $e->getMessage(),E_USER_WARNING);
}

class AdminVideos
{
    private $is_action_taken=false;
    public $has_more=true;
    public $total_count=0;
    function __construct($action_taken)
    {
        $this->is_action_taken=$action_taken;
    }
    function getVideoList($page_no){
        global $video_cdn,$video_prefix,$video_s3_output_prefix;
        $videoDbo=new VideoDBO();
        $videos=$videoDbo->GetVideosForAdmin($this->is_action_taken,$page_no);

        //echo json_encode($videos);
        //die;
        $new_videos=array();
        foreach($videos as $video)
        {
            $file = explode('.', $video['filename'])[0];
            $ext = explode('.', $video['filename'])[1];
            $video['resolutions'] = json_decode($video['available_resolutions'], true);
            if (count($video['resolutions']) > 0) {
                $reso = $video['resolutions'][0];
            }

            $video['url'] = $video_cdn . $video_s3_output_prefix . $file . '_' . $reso . '.' . $ext;
            array_push($new_videos,$video);
        }
        if(count($new_videos)>=20)
        {
            $this->has_more=true;
        }
        else{$this->has_more=false;}
        $this->total_count=count($new_videos);
        return $new_videos;

    }
    function TakeAdminAction($video_id,$status,$admin_id)
    {
        $videoDbo=new VideoDBO();
        $admin_logging =new logging();
        $step="Video";
        $text='';
        if($status=='true'){
            $text='approved';
        }else{
            $text='rejected';
            $this->NotifyOnRejection($video_id);
        }
        $admin_logging->log($admin_id,$video_id,'',$step,$text);
        $videoDbo->setTMApproved($video_id,$status);
    }

    function VisionFromAdmin($video_id,$status,$admin_id)
    {
        $videoDbo=new VideoDBO();
        $admin_logging =new logging();
        $step="Video_Vision";
        $admin_logging->log($admin_id,$video_id,'',$step,$status);
        $videoDbo->ChangeVisionApproval($video_id,$status);
    }


    function NotifyOnRejection($video_id)
    {

        $ticker_text = "Message from Trulymadly";
        $data="Your video was rejected due to explicit or inappropriate content. Please visit our photo guidelines for more details.  ";
        $push_array = array("content_text"=>$data,"ticker_text"=>$ticker_text,
            "title_text"=>"Inappropriate Content Detected","push_type"=>"PHOTO","event_status" => "video_rejected_adult_content_detected") ;
        $videoDbo=new VideoDBO();
        $user_id=$videoDbo->fetchUserId(null,$video_id);
        $user_id=$user_id['user_id'];
        $pushnotify = new pushNotification();
        if($user_id!=null&&$user_id!='') {
            $pushnotify->notify($user_id, $push_array, 'video');
        }


    }

}