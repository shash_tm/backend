<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname(__FILE__).'/../DBO/VideoDBO.php';
require_once dirname ( __FILE__ ).'/../include/aws/aws-autoloader.php';
require_once dirname(__FILE__).'/../logging/EventTrackingClass.php';
require_once dirname(__FILE__).'/VideoEncoder.class.php';

use Aws\Sns\MessageValidator\Message;
use Aws\Sns\MessageValidator\MessageValidator;

class SNSVideo
{
    private $VideoDBO=null;
    private $s3_input=null;
    private $s3_output=null;
    private $tmp_directory=null;
    public $encoding_id=null;
    private $adult_score_arr=array();
    private $voilent_score_arr=array();
    private $user_id;
    public $event_logging;
    function __construct()
    {
        $this->VideoDBO=new VideoDBO();
        $this->s3_input='videos/';
        $this->s3_output='videos/';
        $this->tmp_directory='/tmp/';
        $this->event_logging=new EventTrackingClass();
    }


    public function call_vision($file_location)
    {

        global $vision_key;
        //for testing purposes keep it
        if(!isset($vision_key))
        {
            trigger_error("Vision Key Not Available",E_USER_WARNING);
            die;
        }
        $vision_url = 'https://vision.googleapis.com/v1/images:annotate?key=' . $vision_key;

        $data = file_get_contents($file_location);
        $base64 = base64_encode($data);
        //Create this JSON
        $r_json = '{
			  	"requests": [
					{
					  "image": {
					    "content":"' . $base64 . '"
					  },
					  "features":[
                                        {
                                          "type":"SAFE_SEARCH_DETECTION"

                                        }
                                 ]
					}
				]
			}';


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $vision_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $r_json);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        //var_dump($status);
        if ($status != 200) {
            //echo $json_response;
            Utils::sendEmail('raghav@trulymadly.com','admintm@trulymadly.com','Google Vision API not responding Properly:Video Profile',print_r($status,true));
            trigger_error("Google vision didn't responded well:Video Profiles",E_USER_WARNING);
            die("Error: $vision_url failed status $status" . $json_response);

        }

        //echo $json_response.PHP_EOL.PHP_EOL.PHP_EOL;
        return   $json_response;
    }

    public function GetImageFrames($filename)
    {


        $command="ffmpeg -i $this->tmp_directory/$filename -vf fps=1 $this->tmp_directory/$filename%d.png";
        //var_dump(exec($command));
        exec($command,$output,$ret);
        if($ret==0)
        {
            //It was a success
            //Now pick two images
            $files = glob("$this->tmp_directory/$filename*.{png,jpg}", GLOB_BRACE);
            $i=0;
            foreach($files as $file) {

              //Now send these files to Google API

                $i++;
            }
            //HardCode for 3 and 7 seconds
            $ran_array=array(3);
            if($i>7)
            {
                array_push($ran_array,7);
            }
            $res_array=array();
            foreach($ran_array as $ran)
            {


                $result=$this->call_vision($this->tmp_directory.$filename.$ran.'.'.'png');
                array_push($res_array, $this->ProcessVisionResponse(json_decode($result,true)));
                //Upload the file on S3 too from here
                if(!Utils::saveOnS3($this->tmp_directory.$filename.$ran.'.'.'png','admin_images/'.$filename.$ran.'.'.'png',1))
                {
                    //
                    Utils::sendEmail('raghav@trulymadly.com','admintm@trulymadly.com',"Not able to Upload Video Frame to s3",'Video Test');
                }
            }
            /*
             * Code for taking Random Frames Into Account
            $ran_array=array();
            $desire_count=2;

            while(true)
            {
                if(count($ran_array)==$desire_count)
                {
                    break;
                }
                $ran=rand(0,$i);
                if(!in_array($ran,$ran_array))
                {

                    array_push($ran_array,$ran);

                }
            }*/

            $is_approved=true;
            //Now loop the Res Array to see if it has all elements that are true
            foreach($res_array as $item)
            {
                if($item==false)
                {
                    $is_approved=false;
                    break;
                }
            }

            $vision_array=array("adult"=>$this->adult_score_arr,"voilent"=>$this->voilent_score_arr);
            //Mark the video Approved By the Encoding ID provided
            $this->VideoDBO->MarkApproved($this->encoding_id,$is_approved,$vision_array);
            $event_info='';
            if(!$is_approved)
            {
                $event_info='rejected';
                //$this->NotifyOnRejection();
            }
            else{ $event_info='approved';}
            $log_array=array("user_id"=>null,"activity"=>"videos","event_type"=>"admin_approval","event_info"=>json_encode(array("encoding_id"=>$this->encoding_id,"event_info"=>$event_info)),"event_status"=>"success");
            $this->event_logging->logActions(array($log_array));

        }
        else
        {
            //Fail , drop a mail

            Utils::sendEmail('raghav@trulymadly.com','admintm@trulymadly.com',"Not able to get the Frames for $filename",'Video Test');

        }

    }



    function NotifyOnRejection()
    {
        $ticker_text = "Message from TrulyMadly";
        $data="Oops! Your video got rejected. Upload another one now!";
        $push_array = array("content_text"=>$data,"ticker_text"=>$ticker_text,
            "title_text"=>"Inappropriate Content Detected","push_type"=>"PHOTO","event_status" => "video_rejected_adult_content_detected") ;
        $user_id=$this->VideoDBO->fetchUserId($this->encoding_id)['user_id'];

        $pushnotify = new pushNotification();
        if($user_id!=null&&$user_id!='') {
            $pushnotify->notify($user_id, $push_array, 'video');
        }


    }

    function process_likeliness($str)
    {
        //echo $str.PHP_EOL;
        if($str=='POSSIBLE')
        {
            return 2;
        }
        else if($str=='VERY_UNLIKELY')
        {
            return 0;
        }
        else if($str=='UNLIKELY')
        {
            return 1;
        }
        else if($str=='LIKELY')
        {
            return 3;
        }
        else if($str=='VERY_LIKELY')
        {
            return 4;
        }
        else if($str=='')
        {
            return -2;
        }
        return -100;


    }
    public function ProcessVisionResponse($resp)
    {
       $adult= $this->process_likeliness($resp['responses'][0]['safeSearchAnnotation']['adult']);
       $voilent= $this->process_likeliness($resp['responses'][0]['safeSearchAnnotation']['voilent']);
        array_push($this->adult_score_arr,$adult);
        array_push($this->voilent_score_arr,$voilent);
        if($adult==4||$voilent==4) {
            return false;
        }
        return true;
    }


    public function DownloadVideo($filename)
    {
        global $video_cdn,$video_prefix;
        //$video_cdn='https://s3-ap-southeast-1.amazonaws.com/trulymadlyvideostest/';
        //$result=$this->VideoDBO->GetFilename($encoding_id);

        //$video_url=$video_cdn.$filename;
        $encoder=new VideoEncoder(null);
        $video_url=$encoder->getSignedURl($filename);
        $explode=explode('/',$filename);

        if(count($explode)==2) {
            $file_to_be_saved = $explode[1];
        }
        else if(count($explode)==1) {
            $file_to_be_saved = $explode[0];
        }

        if(isset($video_url))
        {
            $time1=time();
            $data = file_get_contents($video_url);
            file_put_contents("$this->tmp_directory/$file_to_be_saved",$data);
            $time_taken=time()-$time1;
            $log_array=array("user_id"=>null,"activity"=>"videos","event_type"=>"download_video","event_status"=>"success","event_info"=>$video_url,"time_taken"=>$time_taken);
            $this->event_logging->logActions(array($log_array));

            $time1=time();
            $this->GetImageFrames($file_to_be_saved);
            $time_taken=time()-$time1;
            $log_array=array("user_id"=>null,"activity"=>"videos","event_type"=>"admin_check","event_status"=>"success","event_info"=>$video_url,"time_taken"=>$time_taken);
            $this->event_logging->logActions(array($log_array));

            $this->DeleteTempFiles($file_to_be_saved);

        }
       else
       {
            //Do Something
       }

    }
    public  function DelerteVideoFile($filename)
    {
        unlink($filename);
    }

    public function DeleteTempFiles($filename)
    {
        foreach(glob("$this->tmp_directory/$filename*") as $file)
        {
            unlink($file);
        }

    }

    //If the condition in this is true, then A mail will be sent to raghav containing the  Confirmation URL
    public function CheckIfSubscibeConfirm($var,$body)
    {
        //$var is the POST parameter
        if($var['x-amz-sns-message-type']=='SubscriptionConfirmation')
        {
            //Drop A Mail and return true
            $obj=json_decode($body,true);
            $message='';
            if($obj!=null&&$obj['SubscribeURL']!=null)
            {
                $message=$obj['SubscribeURL'];
                Utils::sendEmail('raghav@trulymadly.com','admintm@trulymadly.com','AWS Confirmation URL for SNS Video Encoder',$message);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            //This is not a confirmation Message
            return false;
        }

    }

    public function AuthenticateSNS($var,$body)
    {
            //$arr=json_decode($body,true);
            /*$base64_original=$arr['Signature'];
            $certi_url=$arr['SigningCertURL'];
            $pkey_details = openssl_pkey_get_details(openssl_pkey_get_public(file_get_contents($certi_url)));
            $my_key=$pkey_details['key'];
            openssl_public_encrypt("Raghav",$crypted,$my_key);
            //For now just return tr
            echo $crypted;
            return true;
            */

            try {
                //Verify the integrity of the message
                $message = Message::fromRawPostData();
                $validator = new MessageValidator();
                $validator->validate($message);
                return true;
            }
            catch (Exception $ex)
            {
                trigger_error( $ex->getMessage(),E_USER_WARNING);
                return false;
            }
    }

    public function UpdateStatusPending($arr)
    {

        //Is called by CLI only tu update pending statuses in case the sns callbacks from aws are missed

        if(isset($arr['Status'])&&$arr['Status']=='Complete')
        {
            $available_resolution=array();
            $thumbnail_arr=array();
            $job_id=$arr['Id'];
            $output=$arr['Outputs'][0];
            $this->encoding_id=$job_id;
            array_push($available_resolution,"low");
            array_push($thumbnail_arr,$output['ThumbnailPattern']);

            $thumbnail='';
            if(count($thumbnail_arr)>0)
            {
                $thumbnail=$thumbnail_arr[0];
                $thumbnail=str_replace('{count}','00001',$thumbnail).'.png';

            }
            $this->VideoDBO->MarkCompleted($job_id,json_encode($available_resolution),'completed',$thumbnail);
            $filename=$arr['Input']['Key'];
            $this->DownloadVideo($filename);

            $this->VideoDBO->MarkSnsCompleted($job_id);
        }
        else
        {
            //Log the Error
            trigger_error('',E_USER_WARNING);
        }
    }


    public function UpdateStatus($var,$body)
    {
        $arr=json_decode($body,true);
        if($arr!=null&&count($arr)>0)
        {
            if(isset($arr['Message'])&&$arr['Message']!='')
            {

                $message_arr=$arr['Message'];
                if($message_arr!=null&&count($message_arr)>0)
                {
                    $message_arr=json_decode($message_arr,true);

                    if(isset($message_arr['state']))
                    {

                        $job_id=$message_arr['jobId'];
                        $this->encoding_id=$job_id;
                        if($message_arr['state']=='COMPLETED')
                        {
                            $available_resolution=array();
                            $thumbnail_arr=array();
                            //Take out the job id and mark it as complete
                            /*
                            foreach($message_arr['outputs'] as $output)
                            {
                                $width=$output['width'];
                                $height=$output['height'];
                                if($width>$height)
                                {
                                    $mini=$height;
                                }
                                else
                                {
                                    $mini=$width;
                                }
                                array_push($available_resolution,$mini);
                                array_push($thumbnail_arr,$output['thumbnailPattern']);

                            }*/
                            $output=$message_arr['outputs'][0];

                            array_push($available_resolution,"low");
                            array_push($thumbnail_arr,$output['thumbnailPattern']);
                            $thumbnail='';
                            if(count($thumbnail_arr)>0)
                            {
                                $thumbnail=$thumbnail_arr[0];
                                $thumbnail=str_replace('{count}','00001',$thumbnail).'.png';

                            }
                            //Mark It is as complete
                            $this->VideoDBO->MarkCompleted($job_id,json_encode($available_resolution),'completed',$thumbnail);

                            $filename=$message_arr['input']['key'];

                            $this->VideoDBO->MarkApproved($this->encoding_id,true,array("DummyApproval"));
                            //$this->DownloadVideo($filename);

                            //$this->VideoDBO->MarkSnsCompleted($job_id);


                        }
                        else if($message_arr['state']=='ERROR')
                        {

                            $output_error=array();

                            foreach($message_arr['outputs'] as $output)
                            {
                                if($output['status']='Error')
                                {
                                    array_push($output_error,$output['statusDetail']);
                                }
                            }
                            $this->VideoDBO->LogError($output_error, $job_id);
                            Utils::sendEmail('raghav@trulymadly.com','admintm@trulymadly.com',"Video Encoding Error for Job Id =$job_id",json_encode($output_error));

                        }
                    }
                }
            }
        }
    }




}
try {


    if(php_sapi_name()=='cli')
    {
        $obj = new SNSVideo();
        $VideoDbo=new VideoDBO();
        $encoding_ids=$VideoDbo->GetVideosPendingCli();

        foreach($encoding_ids as $id)
        {
            $job_id=$id['encode'];
            echo $job_id.PHP_EOL;
            $VideoEncoder=new VideoEncoder(null);
            $arr=$VideoEncoder->getJobstatus($job_id);
            $obj->UpdateStatusPending($arr);
        }
        die;
    }


    $post = file_get_contents('php://input');
    if ($post != null) {
        $obj = new SNSVideo();
        $headers = getallheaders();
        if(!$obj->AuthenticateSNS(null,$post))
        {
            die;
            //UnAuthorized Request
        }
        if (!$obj->CheckIfSubscibeConfirm($headers, $post)) {
            //Move Forward
            //Authenticate AWS SNS CAll by checking the Signature of the msg
            try {
                $time1=time();
                $obj->UpdateStatus(null, $post);
                $time_taken=time()-$time1;
                $log_array=array("user_id"=>null,"activity"=>"videos","event_type"=>"overall_call_sns","event_status"=>"success","time_taken"=>$time_taken,"event_info"=>$obj->encoding_id);
                $obj->event_logging->logActions(array($log_array));

            } catch (Exception $ex) {
                echo($ex->getMessage());
                die;
            }
            //file_put_contents('/tmp/video_input.txt',($post));
            //Utils::sendEmail('raghav@trulymadly.com', 'admintm@trulymadly.com', 'AWS SNS Text 1', ($post));

        } else {
            die;
        }

    }
}
catch(Exception $ex)
{
    echo $ex->getMessage();
}

?>
