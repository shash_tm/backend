<?php

require_once dirname ( __FILE__ ).'/VideoEncoder.class.php';

try
{
    $user = functionClass::getUserDetailsFromSession ();
    $user_id = $user['user_id'];

    $filename=$_REQUEST['filename'];
    $request_type=$_REQUEST['type'];
    $is_delete=false;
    $output=null;
    if(isset($user_id)) {
        //Start time for logging
        $time_now=time();
        $VideoEncode = new VideoEncoder($user_id);
        if ($request_type == 'edit' || $request_type == 'delete') {
            //Old Video
            $old_video_id = $_REQUEST['old_video_id'];
            $output=$VideoEncode->MarkDelete($old_video_id);
            if ($request_type == 'delete') {
                $is_delete = true;
                $output=$VideoEncode->process_output($output,$user_id);
                print_r(json_encode($output));
                die;
            }

        }


        if (isset($filename)) {
            $authenticated = $VideoEncode->Authenticate($filename);
            if ($authenticated) {
                $width=-1;
                $height=-1;
                $duration=-1;
                if(isset($_REQUEST['height'])&&isset($_REQUEST['width'])&&isset($_REQUEST['duration']))
                {
                    $height=$_REQUEST['height'];
                    $width=$_REQUEST['width'];
                    $duration=$_REQUEST['duration'];
                    $VideoEncode->AssignVariables($height,$width,$duration);

                }
                if(isset($_REQUEST['ismuted']))
                {
                    $VideoEncode->AssignMute($_REQUEST['ismuted']);
                }
                if(isset($_REQUEST['caption']))
                {
                    $VideoEncode->AssignCaption($_REQUEST['caption']);
                }
                if(isset($_REQUEST['rotate']))
                {
                    //This is a integer ranging from 0 to 360
                    if(is_numeric($_REQUEST['rotate'])) {
                        $VideoEncode->AssignRotate($_REQUEST['rotate']);
                    }
                }
                if(isset($_REQUEST['crop_square']))
                {
                    //Bool variable to know whether we should crop or not
                    $VideoEncode->AssignCrop($_REQUEST['crop_square']);
                }

                $time1=time();
                $output=$VideoEncode->DecideResolutions();
                $output=$VideoEncode->process_output($output,$user_id);

            } else {
                //This was an authenticated Request,throw an error 403
                $output['responseCode']=403;
                $output['error_code']="3";
                $output['error']="Video could not be uploaded. Please try again.";
            }
        }
        else
        {
            $output['responseCode']=403;
            $output['error_code']="4";
            $output['error']='Video could not be uploaded. Please try again.';
        }


    }else
    {
        $output['responseCode']=401;
    }
    $time_taken=time()-$time_now;
    $event_status='';
    $event_info=array();
    if($output['responseCode']!=200) {
        $event_status='error';
        $event_info['status']=$output['error'];
        $event_info['status']=$output['error_code'];
        $event_info['filename']=$filename;
    }else{
        $event_status='success';
        $event_info['status']=$output['success'];
        $event_info['filename']=$filename;
    }
    $log_array=array("user_id"=>$user_id,"time_taken"=>$time_taken,"activity"=>"videos","event_type"=>"overall_call_post","event_info"=>json_encode($event_info),"event_status"=>$event_status);
    if($VideoEncode!=null) {
        $VideoEncode->event_logging->logActions(array($log_array));
    }
    print_r(json_encode($output));

}
catch (Exception $ex)
{
   trigger_error($ex->getMessage(),E_USER_WARNING);
    //echo $ex->getMessage();
}



