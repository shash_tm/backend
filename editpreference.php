<?php 
require_once (dirname ( __FILE__ ) . '/include/config.php');
require_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . '/UserData.php');
require_once (dirname ( __FILE__ ) . '/UserUtils.php');
require_once (dirname ( __FILE__ ) . "/include/header.php");

class editPreference{

	private $united_states = 254 ;
	private $india = 113 ;

	function update($data){
		global $login_mobile,$conn;
		$uu = new UserUtils();
		
		$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		
		$session=functionClass::getUserDetailsFromSession();
		$user_id=$session['user_id'];
		$status = functionClass::getUserStatus($user_id);
		
		$rs = $conn->Execute($conn->prepare ( "select start_age as lookingAgeStart,end_age as lookingAgeEnd,
				marital_status as marital_status_spouse,religionCaste,location,
				start_height,end_height,smoking_status as smoking_status_spouse,drinking_status as drinking_status_spouse,
				food_status as food_status_spouse,income,education as education_spouse from user_preference_data where user_id=?"),$user_id);
		$oldData = $rs->FetchRow();
		//var_dump($oldData);exit;
		/*
		 * for spouse age
		 */
		if(!$login_mobile){
			$data ['marital_status_spouse'] = implode(',',$data ['marital_status_spouse']);
		}	
		/*
		 * for spouse religion
		 */
		
		$values ['religionCaste'] = '';
		if($login_mobile){
			$spouseReligions = explode(',',$data ['religionSpouse']);
		}else
			$spouseReligions = $data ['religionSpouse'];
		if($spouseReligions!='null' && $spouseReligions!=''){
			foreach ( $spouseReligions as $religion ) {
				if ($religion == 'null' || $religion == '') {
					unset ( $values ['religionCaste'] );
					break;
				}
				$values ['religionCaste'] .= $religion;
				$values ['religionCaste'] .= ";";
			}
		}else{
			$values ['religionCaste'] = null;
		}
	
		/*
	 	 * for spouse country
	 	 */	
		
		$values ['location'] = '';
		if(isset($data['spouseState'])) {
			$tmpLoc = explode('-',$data['spouseState']);
			if($tmpLoc[1] == '756') {
				$tmpLoc[0] = 219;
				$data['spouseCountry'] = 219;
			}else if($tmpLoc[1] == '651') {
				$tmpLoc[0] = 114;
				$data['spouseCountry'] = 114;
			}
			$data['spouseState'] = implode('-',$tmpLoc);
		}
		if($login_mobile){
			//$spouseCountries = explode(',',$data['spouseCountry']);
			$spouseStates = explode(',',$data['spouseState']);
			$spouseCities = explode(',',$data['spouseCity']);
		}else{
			//$spouseCountries = $data ['spouseCountry'];
			$spouseStates = $data ['spouseState'];
			$spouseCities = $data ['spouseCity'];
		}
		
		$spouseCountries = explode(',',$data['spouseCountry']);
		if(!isset($data['show_us_profiles']) || (isset($data['show_us_profiles']) && $data['show_us_profiles'] == 'true') )
		{
			$spouseCountries[]= $this->united_states;
		}
		$spouseCountries = array_unique($spouseCountries);
		$demo_preference_array=array();
		
	if($spouseCountries!='null' && $spouseCountries!=''){
		foreach ( $spouseCountries as $country ) {
			$country_only=true;
			$values ['location'] .= $country;
			if ($country == 0) {
				unset ( $values ['location'] );
				break;
			}
			foreach ( $spouseStates as $spouseState ) {
				$stateArray = explode ( "-", $spouseState );
				if ($country == $stateArray [0]) {
					$country_only=false;
					$state_only=true;
		
					$values ['location'] .= ":" . $stateArray [1];
					foreach ( $spouseCities as $spouseCity ) {
						$cityArray = explode ( "-", $spouseCity );
						if ($stateArray [1] == $cityArray [1]){
							$values ['location'] .= "-" . $cityArray [2];
							$state_only=false;
							$demo_preference_array[]= $country."_" .$stateArray [1]."_". $cityArray [2];
						}
					}
					if($state_only==true){
						$demo_preference_array[]= $country."_" .$stateArray [1];
					}
				}
			}
			if($country_only==true){
				$demo_preference_array[]= $country;
			}
			$values ['location'] .= ";";
		}
	}else{
		$values ['location'] = null;
	}
		
		$location_preference_for_matching=null;
		if(count($demo_preference_array)>0){
			$location_preference_for_matching=implode(",",$demo_preference_array);
		}
	
		/*
		 * for spouse height
		 */
		
		$start_height = $data['start_height'];//12 * $data ['heightSpouseFeetStart'] + $data ['heightSpouseInchStart'];
		$end_height = $data['end_height'];//12 * $data ['heightSpouseFeetEnd'] + $data ['heightSpouseInchEnd'];
		
		/*
		 * for spouse smoking
		 */
		
		if($data ['smoking_status_spouse']=='' || $data ['smoking_status_spouse']=='null'){
			$data ['smoking_status_spouse']=null;
		}
		
		$smoking_preference=$data ['smoking_status_spouse'];//$registration_utils->getTraitPreferences($data ['smoking_status_spouse'],$smokingValues);
		
		/*
		 * for spouse drinking
		 */
		
		if($data ['drinking_status_spouse']=='' || $data ['drinking_status_spouse']=='null'){
			$data ['drinking_status_spouse']=null;
		}
		$drinking_preference=$data ['drinking_status_spouse'];//$registration_utils->getTraitPreferences($data ['drinking_status_spouse'],$drinkingValuesSearch);
		
		/*
		 * for spouse eating
		 */
		if($data ['food_status_spouse']=='' || $data ['food_status_spouse']=='null'){
			$data ['food_status_spouse']=null;
		}
		$food_preference=$data ['food_status_spouse'];//$registration_utils->getTraitPreferences($data ['food_status_spouse'],$foodTypes);
		
		/*
		 * for spouse income and education
		 */
		if($login_mobile){
			if(empty($data['income'])){
				$income = null;
			}else{
				$income = explode(',',$data['income']);
				$income = json_encode($income);
			}
		}else{
			if($data['income'] == 'null' || $data['income'] == ''){
				$income = null;
			}
			else{
				$income = $data['income'];
				$income = json_encode($income);
			}	
		} 
			
		if(!$login_mobile){
			$data ['education_spouse'] = implode(',',$data ['education_spouse']);
		}else{
			if($data ['education_spouse']==""||$data ['education_spouse']=="null")
				$data ['education_spouse'] = null;
		}
		
		if($data ['marital_status_spouse']==""||$data ['marital_status_spouse']=="null") {
			$data ['marital_status_spouse'] = null;
		}
		
		if(isset($data['marital_status_spouse'])) {
			$data['marital_status_spouse'] = $uu->getPreferenceMaritalStatus($data['marital_status_spouse']);
		}
		
		/*
		 * for diff
		 * 
		 */
		$data['religionCaste'] = $values ['religionCaste'];
		$data['location'] = $values['location'];
		$data['start_height'] = $start_height;
		$data['end_height'] = $end_height;
		$data['income'] = $income;
		unset($data['update']);
		unset($data['religionSpouse']);
		unset($data['spouseCountry']);
		unset($data['spouseState']);
		unset($data['spouseCity']);
		//var_dump($data);
		
		$result = array_diff_assoc($oldData, $data);
		if(count($result)==0){
			$output=array("responseCode"=>200);
			print_r(json_encode($output));
			exit;
		}
		else{
		$conn->Execute ( $conn->prepare ( "update user_preference_data set start_age=?,end_age=?,marital_status=?,
				location=?,start_height=?,end_height=?,education=?,location_preference_for_matching=? where user_id=?" ), array (
				$data ['lookingAgeStart'],$data ['lookingAgeEnd'],$data ['marital_status_spouse'],$values ['location'],
				$start_height,$end_height,$data ['education_spouse'],$location_preference_for_matching,$user_id
		) );
		
		
		
// 		if($status == 'authentic'){
			
// 			$conn->Execute ( $conn->prepare ( "update user_search set pref_start_age=?,pref_end_age=?,pref_marital_status=?,
// 				pref_location=?,pref_start_height=?,pref_end_height=?,pref_education=? where user_id=?" ), array (
// 									$data ['lookingAgeStart'],$data ['lookingAgeEnd'],$data ['marital_status_spouse'],
// 									$location_preference_for_matching,$start_height,$end_height,$data['education_spouse'],$user_id
// 							) );
// 		}
		
		
		$uu->clearRecommendationCache($user_id,$status);
		
		$output=array("responseCode"=>200);
		print_r(json_encode($output));
		die();
		}
	}

	function getData(){
		global $conn,$login_mobile,$smarty;
		
		$uu = new UserUtils();
		
		$religions_spouse=array("0"=>array("id"=>"Hindu","value"=>"Hindu"),"1"=>array("id"=>"Muslim","value"=>"Muslim"),"2"=>array("id"=>"Sikh","value"=>"Sikh"),"3"=>array("id"=>"Christian","value"=>"Christian"),"4"=>array("id"=>"Buddhist","value"=>"Buddhist"),"5"=>array("id"=>"Jain","value"=>"Jain"));
		$income_spouse=array("0"=>array("id"=>"0-10","value"=>"<10 lakhs"),"1"=>array("id"=>"10-20","value"=>"10 lakhs to 20 lakhs"),"2"=>array("id"=>"20-50","value"=>"20 lakhs to 50 lakhs"),"3"=>array("id"=>"50-51","value"=>">50 lakhs"));
		
		$session=functionClass::getUserDetailsFromSession();
		
		
		$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$user_id = $_SESSION ['user_id'];
		
		$userData = new UserData($user_id);
		$gender = $userData->fetchGender();
		
		if($login_mobile){
			$response['responseCode'] = 200;
			$response['gender'] = $gender;
		}else
			$smarty->assign ( 'gender', $gender );

		$rs = $conn->Execute("select id,degree_name as value from highest_degree order by id");
		$highest_degree = $rs->GetRows();
		
		$rs = $conn->Execute ( "select * from geo_country where country_id=113" );
		$allcountries = $rs->GetRows();
		//$allcountries = $rs->FetchRow();
		$rs = $conn->Execute ( "select * from geo_state where country_id=113" );
		$allstates = $rs->GetRows();
		//$rs = $conn->Execute ( "select * from geo_city where country_id=113 order by state_id" );
		$rs=$conn->Execute("select distinct * from geo_city where country_id = 113  and (display_name is NULL or display_name=name) order by state_id");
		$allcities = $rs->GetRows();
		foreach ($allcities as $key=>$value){
			$allcities[$key]['id']=$allcities[$key]['city_id'];
		}
		
		if(!$login_mobile){
			$header = new header($user_id);
			$header_values = $header->getHeaderValues();
			
			$smarty->assign ( 'countries', $allcountries); 
			$smarty->assign ( 'states', json_encode($allstates));
			//$smarty->assign ( 'cities', json_encode($allcities));
			$smarty->assign ( 'religion', json_encode($religions_spouse));
			$smarty->assign ( 'income', $income_spouse);
			$smarty->assign ( 'highest_degree', $highest_degree);
			$smarty->assign ( 'header',$header_values);
		}else{
			$response['countries'] = $allcountries;
			$response['states'] = $allstates;
			$response['cities'] = $allcities;
			$response['religion'] = $religions_spouse;
			$response['income'] = $income_spouse;
			$response['highest_degree'] = $highest_degree;
			$response['masters'] = array("id"=>"2-19");
			$response['bachelors'] = array("id"=>"22-39");
		}
	
		//$rs = $conn->Execute ( "select * from user_preference_demography where user_id='" . $user_id . "' limit 1" );
		$rs = $conn->Execute ( "select user_id,start_age,end_age,marital_status,religionCaste,location,start_height,end_height,smoking_status,drinking_status,food_status,income,education from user_preference_data where user_id='" . $user_id . "' limit 1" );
		if ($rs->_numOfRows > 0) {
			$demography = $rs->GetRows ();
			$countryWise = explode ( ";", $demography [0] ['location'] );
			$countries = $states = $cities = '';
			foreach ( $countryWise as $countrywise ) {
				$stateWise = explode ( ":", $countrywise );
				if ($countries == '' && $stateWise [0] != '')
					$countries = $stateWise [0];
				else if ($stateWise [0] != '')
					$countries .= "," . $stateWise [0];
				$country = $stateWise [0];
				unset ( $stateWise [0] );
				foreach ( $stateWise as $statewise ) {
					$citiWise = explode ( "-", $statewise );
					if ($states == '' && $citiWise [0] != '')
						$states = $country . "-" . $citiWise [0];
					else if ($citiWise [0] != '')
						$states .= "," . $country . "-" . $citiWise [0];
					$state = $citiWise [0];
					unset ( $citiWise [0] );
					foreach ( $citiWise as $citywise )
						if ($cities == '' && $citywise != '')
							$cities = $country . "-" . $state . "-" . $citywise;
						else if ($citywise != '')
							$cities .= "," . $country . "-" . $state . "-" . $citywise;
				}
			}
			$religionWise = explode ( ";", $demography [0] ['religionCaste'] );
			$religions = $castes = '';
			foreach ( $religionWise as $religionwise ) {
				$casteWise = explode ( ":", $religionwise );
				if ($religions == '' && $casteWise [0] != '')
					$religions = $casteWise [0];
				else if ($casteWise [0] != '')
					$religions .= "," . $casteWise [0];
				$religion = $casteWise [0];
				unset ( $casteWise [0] );
				foreach ( $casteWise as $castewise )
					if ($castes == '' && $castewise != '')
						$castes = $religion . "-" . $castewise;
					else if ($castewise != '')
						$castes .= "," . $religion . "-" . $castewise;
			}
			unset ( $demography [0] ['religionCaste'] );
			unset ( $demography [0] ['location'] );
			
			$demo [0]['user_id'] = $demography [0] ['user_id'];
			$demo [0]['start_age'] = $demography [0] ['start_age'];
			$demo [0]['end_age'] = $demography [0] ['end_age'];
			$demo [0]['marital_status_new'] = $demography [0] ['marital_status'];
			if(isset($demography [0]['marital_status'])) {
				$demo [0]['marital_status'] = $uu->getMaritalStatusForEditPage($demography[0]['marital_status']);
				$demo[0]['marital_status_new'] = $uu->getPreferenceMaritalStatus($demography[0]['marital_status']);
			}
			
			$demo [0] ['religions'] = $religions;
			//$demography [0] ['castes'] = $castes;
			$demo [0] ['countries'] = $countries;
			$demo [0] ['states'] = $states;
			$demo [0] ['cities'] = $cities;
			
			$trait[0]['start_height'] = $demography [0] ['start_height'];
			$trait[0]['end_height'] = $demography [0] ['end_height'];
			$trait[0]['smoking_status'] = $demography [0] ['smoking_status'];
			$trait[0]['drinking_status'] = $demography [0] ['drinking_status'];
			$trait[0]['food_status'] = $demography [0] ['food_status'];
			
			$work[0]['income'] = json_decode($demography [0] ['income'],true);
			//$work[0]['income_end'] = $demography [0] ['income_end'];
			$work[0]['education'] = $demography [0] ['education'];
			
			if($login_mobile){
				$response['demo'] = $demo [0];
				$response['trait'] = $trait [0];
				$response['work'] = $work [0];
			}else{
				$smarty->assign ( 'demo', json_encode ( $demo [0] ) );
				$smarty->assign ( 'trait', json_encode ( $trait [0] ) );
				$smarty->assign ( 'work', json_encode ( $work [0] ) );
			}
		}
	
	
	if(!$login_mobile)
		$smarty->display ( dirname ( __FILE__ ) . '/templates/editpreference.tpl' );
	
	if($login_mobile){
		print_r(json_encode($response));
		die;
	}
	}
}

try{
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	
	//if(!$login_mobile)
		functionClass::redirect ( 'editpartner',$login_mobile );
	
	$myProfile = new editPreference ();
	$data = $_REQUEST;
	
	if(isset($_REQUEST['update']) && $_REQUEST['update']==true){
		try{
			$myProfile->update($data);
		}catch(Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
	}else{
		try{
			$myProfile->getData();
		}catch(Exception $e){
			trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		}
	}
	
	
}catch(Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}

?>