<?php
require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";
include dirname ( __FILE__ ) . "/include/allValues.php";
//require_once  dirname ( __FILE__ ) . "/include/User.php";
require_once dirname ( __FILE__ ) . "/abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/DBO/sparkDBO.php";
require_once dirname ( __FILE__ ) . "/select/Select.class.php";
require_once dirname ( __FILE__ ) . "/include/matchConstants.php";
require_once  dirname ( __FILE__ ) . "/include/tbconfig.php";
require_once  dirname ( __FILE__ ) . "/include/constants.php";
require_once dirname ( __FILE__ ) . "/Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . '/fb.php';
require_once dirname ( __FILE__ ) . "/DBO/userFacebookDBO.php";
require_once dirname(__FILE__)."/DBO/VideoDBO.php";
require_once dirname(__FILE__)."/videos/VideoEncoder.class.php";
require_once dirname ( __FILE__ ) . "/facebook_likes/UserInterestHobbies.php";
require_once dirname ( __FILE__ ) . "/facebook_likes/FacebookLikesUtils.php";
require_once dirname(__FILE__) . "/user/UserFlags.class.php";

define('ANDROID_APP_VERSION_CODE_SUPPORTING_SELECT',614);
define('IOS_APP_VERSION_CODE_SUPPORTING_SELECT',501);


class UserUtils extends TMObject {
	/*
	 private $dummy_male_image;
	 private $dummy_female_image;*/

	private $uuDBO;
	private $redisEvenDB;
	private $redisOddDB;
	private $VideoDBO;
	//private $redis;
	
	function __construct() {
		//global $dummy_male_image,$dummy_female_image;
		global $redis_db_odd, $redis_db_even, $redis;
		parent::__construct (  );
		$this->uuDBO = new userUtilsDBO();
		$this->redisEvenDB = $redis_db_even;
		$this->redisOddDB = $redis_db_odd;
		$this->VideoDBO=new VideoDBO();
		//$this->redis = $redis;
		
		/*$this->dummy_male_image=$dummy_male_image;
		 $this->dummy_female_image=$dummy_female_image*/;
	}

	/**
	 * marital status for `married before` in case of other marital status
	 * @param $marital_status
	 */
	public static function getMaritalStatusString($marital_status){
		if(isset($marital_status)) {
			return ($marital_status == 'Never Married')?'Never Married ':'Married Before';
		}
		return "Not Applicable";
	}

	public function incrementNotificationCounter($id){
		$key = Utils::getNotificationKey($id);
		$this->redis->INCR($key);
	}

	public static function isRecommendationABAssigned ($user_id)
	{
		return  ($user_id%(Utils::$recoStagedReleaseConstant) == 0) ? true : false;
	}
	/**
	 * 
	 * @param $userId
	 * @param $status - needed as we need to check if the user is non-authentic, so as not to clear the cache on edit
	 * @param $flagFromNonAuthToAuth - in case a user turns authentic
	 */
	public function clearRecommendationCache($userId, $status = null, $flagFromNonAuthToAuth = false){
		
		//$headers = Utils::getAllSystemHeaderFields(); 
 	//	if($status == "non-authentic" ) return;
		
		$matchKey = Utils::$redis_keys['matches']. $userId;
		$matchActionKey = Utils::$redis_keys["match_action_count"].$userId;
		$this->redis->DEL($matchKey);
                //This is to delete the ids we keep for facebook mutual friends.
                $matchKeywithScore = Utils::$redis_keys['matches_with_scores'].$userId;
                $this->redis->DEL($matchKeywithScore);
		
		if($flagFromNonAuthToAuth == true)
		$this->redis->DEL($matchActionKey);
		
	}
        
        public function clearMatchCountData($user_id){
			global $redis;
		$matchActionKey = Utils::$redis_keys["match_action_count"].$user_id;
		$redis->DEL($matchActionKey);
	}
        
	/**
	 * generate a profile link for the user1 to user2 view the profile
	 */
	public function generateProfileLink($userId, $matchId, $campaign = null){
		$str = $userId .'|' . $matchId;
		//var_dump($str);
		$mid = Utils::calculateCheckSum($str);
		//var_dump($mid);
		if(isset($campaign)) return  $this->baseurl."/profile.php".$campaign."&utm_content=profile&uid=".$userId."&pid=$mid".'_'.$matchId;
		else return $this->baseurl."/profile.php?pid=$mid".'_'.$matchId;
	}

	/**
	 * generate a profile link for the user1 to user2 view the profile
	 */
	public function generateProfileLinkForEvent($userId, $matchId, $datespot_id, $campaign = null){
		$str = $userId .'|' . $matchId .'|'. $datespot_id;
		//var_dump($str);
		$mid = Utils::calculateCheckSum($str);
		//var_dump($mid);
		if(isset($campaign)) return  $this->baseurl."/profile.php".$campaign."&utm_content=profile&uid=".$userId."&pid=$mid".'_'.$matchId.'_'.$datespot_id;
		else return $this->baseurl."/profile.php?pid=$mid".'_'.$matchId.'_'.$datespot_id;
	}

	public function getCheckSumForProfileVisit($userId, $matchId){
		$str = $userId .'|' . $matchId;
		$mid = Utils::calculateCheckSum($str);
		return $mid;
	}

	/**
	 * generate a messaging link for the user1 to message user2
	 */
	public function generateMessageLink($userId, $matchId, $campaign = null){
		$str = $userId .'|' . $matchId;
		$mid = Utils::calculateCheckSum($str);
		if(isset($campaign)) return "$this->baseurl/msg/message_full_conv.php$campaign&match_id=$matchId&mesh=$mid&utm_content=messages&uid=".$userId;
		else return "$this->baseurl/msg/message_full_conv.php?match_id=$matchId&mesh=$mid";
	}

	public function generateLikeLink($user_id, $matchId, $likeFlag, $gender, $availableFlag,$repeatProfile ){
		$UserActions = new UserActions();
		//$reflector = new ReflectionClass('UserActions');
		//var_dump($UserActions->getFileName); // die;
		$query_like=$UserActions->generateQuery(UserActionConstantValues::like,$user_id, $matchId, $likeFlag,$gender, $availableFlag,$repeatProfile);
		$like_link="$this->baseurl/ua.php?$query_like";
		return $like_link;
	}

	public function generateHideLink($user_id, $matchId, $likeFlag, $gender, $availableFlag,$repeatProfile){
		$UserActions = new UserActions();
		$query_hide=$UserActions->generateQuery(UserActionConstantValues::hide,$user_id, $matchId,$likeFlag,$gender, $availableFlag,$repeatProfile);
		$hide_link="$this->baseurl/ua.php?$query_hide";
		return $hide_link;
	}
	

	public function generateMayBeLink($user_id, $matchId,$likeFlag, $gender){
		$UserActions = new UserActions();
		$query_maybe=$UserActions->generateQuery(UserActionConstantValues::maybe,$user_id, $matchId,$likeFlag,$gender);
		$maybe_link="$this->baseurl/ua.php?$query_maybe";
		return $maybe_link;
	}

	
	
	/**
	 * generate a link for a profile to be endorsed
	 * @param unknown_type $user_id
	 */
	public function generateEndorsementLink($user_id, $tstamp, $gender, $isWebCall = false){
		$checksum = $this->endorsementHash($user_id,$tstamp, $gender);
		$link =  "$this->baseurl/endorsements.php?id=$user_id&cs=$checksum&ts=$tstamp&g=$gender";

	//	if($isWebCall == true){
			//shorten the url
			$curl_res = Utils::getShortenedUrlFromGoogleAPI($link, $user_id);
			//var_dump($curl_res);
			$result = json_decode($curl_res, true);
			//var_dump($result);die;
			if(!isset($result['error'])){
				return $result['id'];
			}else{
				trigger_error("PHP WEB:" . $curl_res, E_USER_WARNING);
			}
	//	}
		return $link;
	}
	
	public function endorsementHash($user_id,$tstamp, $gender){
		return md5($user_id ."|". $tstamp . "|". Utils::$salt. "|". $gender);
	}
	

	/**
	 * process the match set stored in redis
	 * returning the array in the form of fresh, likes and if they are consuming and what type of availability
	 * @param unknown_type $ids
	 */
	public function processMatchSet ($ids)
	{
		
		$arr = array();
		$likeIds = array();
		$availabileIds = array();
		$i=0;
		$j=0;
		
		foreach ($ids as $val){
			//expected format 1001:1 or 1001:0 or 100l:0
			$idStr = explode(':', $val);
			$match = $idStr[0];
			$availabilityFactor = $idStr[1];
			if (strpos($match,'l') !== false) {
				$id = rtrim($match, 'l');
				$likeIds[$j] = $id;
				$arr[$i]= $id;
				$match = $id;
				$j++;
			}else{
				$arr[$i] = $match;
			}
			
			$availabileIds[$match] = $availabilityFactor;
			$i++;
		}
		//var_dump($ids);
		return array("all_ids" =>$arr, "like_ids" => $likeIds, "available_ids" => $availabileIds);
	
	}


	public function getMatchesWithScores($matches_list_with_scores) {
		$matches_scores_map = array();
		foreach ($matches_list_with_scores as $data) {
			$info = explode(':', $data);
			if (count($info) < 3) continue;
			$score = $info[2];
			$user_id = explode('l', $info[0])[0];
			$matches_scores_map[$user_id] = $score;
		}
		return $matches_scores_map;
	}

	public function getMatchesDataMap($matches_list_with_scores) {
		$matches_data_map = array();
		foreach ($matches_list_with_scores as $data) {
			$info = explode(':', $data);
			if (count($info) < 3) continue;
			$user_id = explode('l', $info[0])[0];
			$matches_data_map[$user_id] = $info[0] . ":" . $info[1];
		}
		return $matches_data_map;
	}

	public function getMatchesDataMapForFirstKey($matches_list) {
		$matches_data_map = array();
		foreach ($matches_list as $data) {
			$info = explode(':', $data);
			if (count($info) < 2) continue;
			$user_id = explode('l', $info[0])[0];
			$matches_data_map[$user_id] = $info[0] . ":" . $info[1];
		}
		return $matches_data_map;
	}

	public function getMatchesMutualFriendsData($matches_list_with_mf) {
		$matches_data_map = array();
		foreach ($matches_list_with_mf as $data) {
			$info = explode(':', $data);
			if (count($info) < 3) continue;
			$user_id = explode('l', $info[0])[0];
			$matches_data_map[$user_id] = array('fb_count' => $info[2]);
		}
		return $matches_data_map;
	}

	public function getUserIdFromMatchData($data) {
		$user_id = -1;
		$info = explode(':', $data);
		if (count($info) > 0) {
			$user_id = explode('l', $info[0])[0];
		}
		return $user_id;
	}

	public function getLikedMatchesCount($matches_list) {
		$count = 0;
		foreach ($matches_list as $data) {
			if (strpos($data, 'l') !== false) $count ++;
		}
		return $count;
	}
	
	
	public function shouldStoreMatchesWithScores($user_id, $gender) {
		if (!Utils::$mutual_friends_enabled) return False;
		$header = Utils::getAllSystemHeaderFields();
		$mutual_friends = isset($header['mutual_friends']) ? $header['mutual_friends'] : false;
		if (!$mutual_friends) return False;
		$configRecommendations = AB_Config::getRecommendationConfigs();
		$configRecommendationsEngines = $configRecommendations['engine'];
		$configRecommendationsEnginePercents = $configRecommendations['percent'];
		$configRecommendationsjsonParams = $configRecommendations['jsonParams'];
		$configRecommendationsEngineFilters = $configRecommendations['filter'];
		$rec_engines = array_combine($configRecommendationsEngines,array_map(null,$configRecommendationsEnginePercents,$configRecommendationsEngineFilters));
		$should_store = False;
		foreach ($rec_engines as $key => $value) {
			$regex = '/.*(_[0-9]+)/';
			$recommendationEngine = $key;
			if (preg_match($regex, $recommendationEngine, $matches)) {
				$recommendationEngine = substr_replace($recommendationEngine, '', 
					strrpos($recommendationEngine, $matches[1]), strlen($matches[1]));
			}
			$filter = json_decode($value[1], true);
			if ($recommendationEngine == Utils::$mutualFriendsEngine 
					&& ($filter['Gender'] == NULL || in_array($gender, $filter['Gender'])) 
					&& (($user_id % 100 < $value[0]) || ( isset($filter['test_users']) && in_array($user_id, $filter['test_users']))) ) {
					$should_store = True;
			}
		}
		return $should_store;
	}

	/**
	 * generate shortened share profile urls 
	 * @param unknown_type $user_ids
	 * @param unknown_type $source
	 */
	public function generateShareProfileLinks($user_ids, $source){
		$tstamp = time();
		foreach($user_ids as $user_id){
			$checksum = $this->getShareProfileHash($user_id,$tstamp, $source);
			$urls[$user_id] =  "$this->baseurl/shareProfile.php?id=$user_id&cs=$checksum&ts=$tstamp&s=$source";
		}

		return $urls;
		 
		/*$options = $this->getOptionsForShareProfileCurlCall($urls);
		$curl_res = Utils::performMultipleCurlCalls($urls, $options);

		$shortenedUrls = array();
		foreach ($curl_res as $key=>$val){
			$result = json_decode($curl_res[$key], true);
			if(!isset($result['error'])){
				$shortenedUrls[$key] = $result['id'];
			}else{
				$shortenedUrls[$key] = $urls[$key];
				trigger_error("PHP WEB:" . $curl_res[$key], E_USER_WARNING);
			}
		} 
		return $shortenedUrls;*/
	}

	public function getShareProfileHash($user_id,$tstamp, $source){
		return md5($user_id ."|". $tstamp . "|". Utils::$salt. "|". $source);
	}
	
	private function getOptionsForShareProfileCurlCall($urls){
		// set URL and other appropriate options
		global $freebase_key, $google_api_url;
		$options = array();
		foreach($urls as $key => $val){
			$options[$key] = array(CURLOPT_URL => "$google_api_url?key=$freebase_key",
			CURLOPT_POST => 1,
			CURLOPT_HTTPHEADER => array("Content-Type: application/json"),
			CURLOPT_RETURNTRANSFER =>1,
			CURLOPT_POSTFIELDS =>json_encode(array("longUrl"=>$val)));
		}
		return $options;
	}
	/**
	 * generate links by checking if the action is even permitted or not
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	public function generateLinksForPermittedActions($user1, $user2, $user1_status){

		$ua = new UserActions();
		$permittedActions = $ua->getPermittedActions($user1, $user2, $user1_status);
		$links = array("msg_url" => null, "like_url" => null , "hide_url" => null);
			
		if($permittedActions['canMsg'])
		$links['msg_url'] = $this->generateMessageLink($user1, $user2);

		if($permittedActions['canLike'])
		$links['like_url'] = $this->generateLikeLink($user1, $user2);
			
		if($permittedActions['canHide'])
		$links['hide_url'] = $this->generateHideLink($user1, $user2);
			
		return $links;
	}

	public function getMessagesBasedOnCurrentStatus($userStatus){
		$msgs = array();
		if($userStatus == 'non-authentic'){
			$msgs['info'] = "Be authentic" ;//READ from some class to get exact message
			$msg['like'] = "be authentic to like";
		}
	}

	/**
	 * returns all the permitted actions a user can take
	 * viz. like/hide/msg
	 * @param unknown_type $userId
	 * @param unknown_type $matchId
	 * @param unknown_type $status
	 */
	public function getPermittedActions($userId, $matchId, $status){
		$UserActions = new UserActions();
		$actions = $UserActions->getUserActionsDone($userId, $matchId);
		$actionsDone = $actions[$matchId];
		$sql = $this->conn->Prepare("SELECT * FROM user_mutual_like where user1=? and user2 = ?");
		$res = $this->conn->Execute($sql, array(($matchId> $userId)? array($matchId, $userId): array($userId, $matchId)));

		$permittedActions = array();
		if($status == 'authentic'){
			if(!in_array(UserActionConstantValues::rejected_or, $actionsDone)){
				$permittedActions['canLike'] = true;
				$permittedActions['canHide'] = true;
				$permittedActions['canMsg'] = false;
			}
			if(($res->RowCount() > 0) && in_array(UserActionConstantValues::can_communicate, $actionsDone)){
				$permittedActions['canMsg'] = true;
			}
		}
		else{
			$permittedActions['canLike'] = false;
			$permittedActions['canHide'] = false;
			$permittedActions['canMsg'] = false;
		}
		return $permittedActions;
	}

	/**
	 * returns if a user has any approved pic or not
	 * @param unknown_type $user_id
	 */
	function isAnyApprovedPic($user_id){

		$myprofile = new User();
		$myData = $myprofile->getUserDetails($user_id);
		$approvedPics = $myprofile->getApprovedPics($user_id);
		$isAnyApproved = (count($approvedPics) > 0)?1:0;
		return $isAnyApproved;

	}


	/**
	 * by shashwat
	 * @param unknown $photo_id
	 * @return string
	 */
	public function getImageFullPathFromDB($photo_id){
		global $imageurl ;
		return $imageurl.$photo_id;
	}

	public function getDummyImage($gender){
		global $dummy_male_image, $dummy_female_image ;
		return ($gender == 'F')?$dummy_female_image: $dummy_male_image;

	}

	public function getActiveVideos($user_array,$myprofile=false)
	{
		try {

			global $video_cdn,$video_prefix,$video_s3_output_prefix;
			$videos = null;
			if ($myprofile) {

				$videos = $this->VideoDBO->getActiveVideos($user_array);
			} else {
				$videos = $this->VideoDBO->getActiveAndModeratedVideos($user_array);
			}

			$video_out = array();
			$arr = array();

			foreach ($videos as $val) {
				$arr[$val['user_id']][] = $val;
			}

			foreach ($arr as $key => $val) {
				//var_dump($val[0]['width']);
				//continue;
				$video_coll = array();
				foreach ($val as $video_item) {
					//Each video of the user
					$video_obj = array();
					$video_obj['video_id'] = $video_item['video_id'];
					$video_obj['width'] = $video_item['width'];
					$video_obj['height'] = $video_item['height'];
					$video_obj['duration'] = $video_item['duration'];
					$video_obj['resolutions'] = json_decode($video_item['available_resolutions'], true);
					$reso = '';
					if (count($video_obj['resolutions']) > 0) {
						$reso = $video_obj['resolutions'][0];
					}
					$encoder=new VideoEncoder(null);
					$signed_url=$encoder->getSignedURl($video_prefix.$video_item['filename']);
					$file = explode('.', $video_item['filename'])[0];
					$ext = explode('.', $video_item['filename'])[1];

					$video_obj['EncodedURL'] = $video_cdn . $video_s3_output_prefix . $file . '_' . $reso . '.' . $ext;
					$video_obj['caption'] = $video_item['caption'];
					$video_obj['status'] = $video_item['status'];
					$video_obj['encoding_status'] = $video_item['job_status'];
					$video_obj['thumbnail'] = $video_cdn.$video_s3_output_prefix.$video_item['thumbnail'];
					if($video_item['job_status']=='pending'||$video_item['job_status']=='error')
					{
						$video_obj['thumbnail']=null;
						$video_obj['EncodedURL']=null;
						$video_obj['EncodedURL']=$signed_url;
					}
					else{
						$video_obj['UnEncodedURL'] = $signed_url;
					}

					if ($video_item['encoding_id'] == '-1') {
						$video_obj['resolutions'] = null;
					}
					if ($video_item['ismuted'] == 1) {
						$video_obj['mute'] = true;
					} else {
						$video_obj['mute'] = false;
					}
					array_push($video_coll, $video_obj);
				}


				$video_out[$key]['video_profile'] = $video_coll;
				$video_coll = array();
			}

			return $video_out;
			//var_dump($arr);
		}
		catch(Exception $ex)
		{
			trigger_error( $ex->getMessage(),E_USER_WARNING);
		}

	}

	/**
	 * returns images of a match id based on user id state
	 * @param unknown_type $user_id
	 * @param unknown_type $match_id
	 */
	public function getMatchProfileImages($user_id,$arr,&$photos,$webp,$user_gender){
		global $imageurl,$dummy_male_image;
		$other_pics_count=0;
		$other_pics_to_Remove=0;

		foreach ($arr as $key=>$val) {
			$photoCount = count($arr[$key]);
			for($i=0;$i<$photoCount;$i++) {
				if($val[$i]['system_profile']=='yes'&&$user_gender=='F') {
					$photos[$key]['profile_pic'] = $imageurl.$val[$i]['name'];
					$photos[$key]['profile_pic_thumbnail'] = $imageurl.$val[$i]['thumbnail'];
				}else{
					if(count($photos[$key]['other_pics'])>=6) continue;


					$photos[$key]['other_pics'][] = $imageurl.$val[$i]['name'];
					$photos[$key]['thumbnail_pics'][] = $imageurl.$val[$i]['thumbnail'];

					if($val[$i]['is_profile']=='yes')
					{
						$photos[$key]['other_to_remove']=$other_pics_count;
						$other_pics_to_Remove=$other_pics_count;
					}
					$other_pics_count++;
				}
			}

			if(!isset($photos[$key]['profile_pic'])){

				//This check is there to avoid the condition if there is no system_profile flag to true
				//then the is_profile pic will be fetched

				$profile_pic=$this->uuDBO->getOriginalProfilePic(array($key),false);

				if($profile_pic!=null&&count($profile_pic)>0)
				{

					$photos[$key]['profile_pic'] = $imageurl.$profile_pic[0]['name'];
					$photos[$key]['profile_pic_thumbnail']=$imageurl.$profile_pic[0]['thumbnail'];

					//remove the index
					//array_slice($photos[$key]['other_pics'],$other_pic_to_remove,1);

					$photos[$key]['other_pics']=Utils::custom_unset($photos[$key]['other_pics'],$other_pics_to_Remove);
					$photos[$key]['thumbnail_pics']=Utils::custom_unset($photos[$key]['thumbnail_pics'],$other_pics_to_Remove);

					if ($webp == true && $profile_pic['webp_flag'] == 'yes') {
						$photos[$user_id]['profile_pic'] = $imageurl . Utils::getWebpName($profile_pic['name']);
						$photos[$user_id]['profile_pic_thumbnail'] = $imageurl . Utils::getWebpName($profile_pic['thumbnail']);
					}
					//$photos[$key]['other_pics']=array_slice($photos[$key]['other_pics'],$other_pic_to_remove,1);
					//$photos[$key]['thumbnail_pics']=array_slice($photos[$key]['thumbnail_pics'],$other_pic_to_remove,1);

				}


			}

			if(count($photos[$key]['other_pics'])==0)
			{
				$photos[$key]["other_pics"]=NULL;
				$photos[$key]["thumbnail_pics"]=NULL;
			}
			if(true)
			{
				if(!isset($photos[$key]['profile_pic']))
				{
					//Insert the dummy image here
					global $dummy_female_no_pics;
					$photos[$key]['profile_pic'] = ($user_gender == 'F')? $dummy_male_image: $dummy_female_no_pics;

				}
			}





		}
	}

	public function getActiveImages($user_array, $user_gender = null, $myProfile = false, $webp = false,$version=-1,$extra_params=null){
		//return null;
		global $imageurl, $dummy_male_image, $dummy_female_image ;
		$this->uuDBO = new userUtilsDBO();
		//$isAnyApproved = $this->uuDBO->isAnyApprovedPic($user_id);
		
		//show only moderated photos when it's match profile
		if($myProfile == true)
		$images = $this->uuDBO->getActiveImages($user_array, false, $webp);
		else $images = $this->uuDBO->getActiveAndModeratedImages($user_array, false, $webp);


		$arr = array();
		foreach ($images as $val){
			$arr[$val['user_id']][] = $val;
		}
		$photos = array();
		if($extra_params['from_other_profile']&&count($images)>0) {
			//Handle from other profile in seperate module

			$this->getMatchProfileImages($user_array,$arr,$photos,$webp,$user_gender);

			return $photos;
		}

		$idsWithPhoto = array_keys($arr);
		$remainingIds = array_diff($user_array, $idsWithPhoto);
		$idsWithNoProfile=array();
        //var_dump($idsWithPhoto);var_dump($user_array);die;
		foreach ($remainingIds as $val){
			if($myProfile==false&&$user_gender=='M'&&!isset($photos[$val]['profile_pic']))
			{
				if($this->CheckMinVersion($version,330))
				{
					global $dummy_female_no_pics;
					//If this is an updated app then there will be some other pic ,other wise another image
					$photos[$val]['profile_pic']=$dummy_female_no_pics;
				}
				else
				{
					global $dummy_female_no_pics_old_version;
					$photos[$val]['profile_pic']=$dummy_female_no_pics_old_version;
				}
					
			}
			else
			{
				$photos[$val]['profile_pic'] = ($user_gender == 'F')? $dummy_male_image: $dummy_female_image;
					
			}
		
		}
		
		//var_dump($photos); die;
        $other_pic_to_remove=0;
		//var_dump($arr);die;
		foreach ($arr as $key=>$val){
			$other_pics_count=0;
			$photoCount = count($arr[$key]);
			for($i=0;$i<$photoCount;$i++){
			//	if($i>6) break;
				if($val[$i]['is_profile']=='yes'&&$myProfile==true) {
					$photos[$key]['profile_pic'] = $imageurl.$val[$i]['name'];
					$photos[$key]['profile_pic_thumbnail'] = $imageurl.$val[$i]['thumbnail'];
				}
				else if($val[$i]['system_profile']=='yes'&&$user_gender=='F'){

					/*if($val[$i]['is_moderated'] !="yes")
					$photos[$val]['profile_pic'] = ($user_gender == 'F')? $dummy_male_image: $dummy_female_image;
					else{*/
						$photos[$key]['profile_pic'] = $imageurl.$val[$i]['name'];
						$photos[$key]['profile_pic_thumbnail'] = $imageurl.$val[$i]['thumbnail'];
						
				//	}
				}
				else{
					if(count($photos[$key]['other_pics'])>=6) continue;
                 	

					$photos[$key]['other_pics'][] = $imageurl.$val[$i]['name'];
					$photos[$key]['thumbnail_pics'][] = $imageurl.$val[$i]['thumbnail'];
					
					if($val[$i]['is_profile']=='yes')
					{
						$photos[$key]['other_to_remove']=$other_pics_count;
						$other_pic_to_remove=$other_pics_count;
					}
					$other_pics_count++;
					
				}
				
			}
            
			
			if(!isset($photos[$key]['profile_pic'])){
				//This check is there to avoid the condition if there is no system_profile flag to true
				//then the is_profile pic will be fetched
				array_push($idsWithNoProfile,$key);
			}
			

		}
		$bring_original_pics=array();
		if(isset($extra_params)&&$extra_params['from_other_profile']==true) {
			$bring_original_pics=$user_array;
		}else if(!$myProfile) {
			$bring_original_pics=$idsWithNoProfile;
		}
		if(count($bring_original_pics)>0) {
			$profile_pics = $this->uuDBO->getOriginalProfilePic($bring_original_pics, $myProfile);

			foreach ($profile_pics as $profile_pic) {
				$user_id = $profile_pic['user_id'];
				if (!isset($photos[$user_id]['profile_pic'])) {
					$photos[$user_id]['profile_pic'] = $imageurl . $profile_pic['name'];
					$photos[$user_id]['profile_pic_thumbnail'] = $imageurl . $profile_pic['thumbnail'];
					if ($webp == true && $profile_pic['webp_flag'] == 'yes') {
						$photos[$user_id]['profile_pic'] = $imageurl . Utils::getWebpName($profile_pic['name']);
						$photos[$user_id]['profile_pic_thumbnail'] = $imageurl . Utils::getWebpName($profile_pic['thumbnail']);
					}

				}

				if (isset($photos[$user_id]['other_to_remove'])) {
					$other_pic_to_remove = $photos[$user_id]['other_to_remove'];
					$photos[$user_id]['other_pics'] = Utils::custom_unset($photos[$user_id]['other_pics'], $other_pic_to_remove);
					$photos[$user_id]['thumbnail_pics'] = Utils::custom_unset($photos[$user_id]['thumbnail_pics'], $other_pic_to_remove);
				}
			}
		}
		$keys=array_keys($photos);
		//Traverse the photo array again
		foreach($keys as $key) {
			if(count($photos[$key]['other_pics'])==0)
			{
				$photos[$key]["other_pics"]=NULL;
				$photos[$key]["thumbnail_pics"]=NULL;
			}
			if(true)
			{
				if(!isset($photos[$key]['profile_pic']))
				{
					//Insert the dummy image here
					global $dummy_female_no_pics;
					$photos[$key]['profile_pic'] = ($user_gender == 'F')? $dummy_male_image: $dummy_female_no_pics;

				}
			}
		}
		return $photos;
	}

	


	/**
	 * returns the array of ids for which i am a recommendation + my recommendations
	 */
	public function setMutualsInFBConnect($user_id){
		$sql = $this->conn->Prepare("SELECT user1, user2 from user_recommendation where user1 =? OR user2=?");
		$ex = $this->conn->Execute($sql, array($user_id, $user_id));
		$res = $ex->GetRows();

		$ids = array();
		foreach ($res as $row=>$col){
			if($user_id == $col['user1'])
			$ids[] = $col['user2'];
			else
			$ids[] = $col['user1'];
		}
		if(count($ids)>0)
		$this->setMutualConnections($user_id, $ids);
	}

	/*
	 * Updating Edit Profile In Redis
	 */
	public function setEditProfileInCache($user_id){
		$key_time="edit_preference_time:$user_id";
		$key="edit_preference:$user_id";
		$val=$this->redis->exists($key_time);
		//set the expiry time in case it is updated
		if($val==false){
			$this->redis->setex($key,3600*24,"true");
			$this->redis->setex($key_time,3600*24,"true");
		}
	}

	/*
	 * clearing the match set in case of non-authentic turning authentic
	 * first we will check if header contains mobile login call and if app version is as per our need
	 * @Himanshu
	 */
/*	public function clearMatchesForNonAuthToAuthForiOS($user_id){
	//	$headerParams = Utils::getAllSystemHeaderFields();

		//the below code checks for ttl to be -1, this is the additional write up due to phone calls
		$matchKey = Utils::$redis_keys['matches']. $user_id;
		$matchKeyTTL = $this->redis->TTL($matchKey);
		if($matchKeyTTL == -1) $phoneCallForiOS = true;   

		
		if(($headerParams['login_mobile'] == true && $headerParams['app_version_code'] >= Utils::$appVersionForNonAuthenticActionEnable )||($phoneCallForiOS==true))
		$this->clearRecommendationCache($user_id, null, true);
	}
	*/
	
	/*
	 * Updating Edit Profile In Redis
	 */
	public function getEditProfileInCache($user_id){
		$key_time="edit_preference_time:$user_id";
		$key="edit_preference:$user_id";
		$val=$this->redis->exists($key);
		return $val;
	}

	/*
	 * Updating Edit Profile In Redis
	 */
	public function setEditProfileMatchesFound($user_id){
		$key_time="edit_preference_time:$user_id";
		$key="edit_preference:$user_id";
		$this->redis->delete($key);
		//		return $val;
	}

	public function recoGenereatedToday($userId){
		$sql = $this->conn->Prepare("SELECT * FROM user_recommendation where date(timestamp) =  CURDATE() and user1=?");
		$res = $this->conn->Execute($sql, array($userId));
		return $res->RowCount();
	}

	public function setGetMutualConnections($user_id, $ids, $from_cron = false){
		global $conn;
		//$ids = array(6000132,6000144, 6000153);
		$ids = implode(',', $ids);
		/*$user_id = 5211749;
		 $ids = 6000220;*/
		//$user_id = 6000174;
		//$ids = 2100193;
		$sql = "SELECT * from user_facebook_connections where user_id in ($user_id)";
		$ex = $conn->Execute($sql);
		$userFriends = array();
		$revisedConnArr = array();
		
		$userFids = array();
		
		$arr = array();
		
		$mutualCountArr = null;
		
		if($ex) {
			while ($val = $ex->FetchRow()) {
				$pid = $val['user_id'];
				$connects = json_decode($val['connections'], true);
				if(is_array($connects)) {
					foreach ($connects as $e=>$k){
				 		$userFriends[$pid][$k['id']] = $k;
				 	}
				 	// user fids
				 	if(is_array($userFriends))
				 		$userFids = array_keys($userFriends[$user_id]);
		
				 	// fetch match connetctions
				 	$sql = "SELECT * from user_facebook_connections where user_id in ($ids)";
				 	$ex1 = $conn->Execute($sql);
				 	if($ex1) {
						while ($val1 = $ex1->FetchRow()) {
				 			$pid1 = $val1['user_id'];
				 			$connects1 = json_decode($val1['connections'], true);
				 			if(is_array($connects1)) {
				 				foreach ($connects1 as $e=>$k){
				 					$revisedConnArr[$pid1][$k['id']] = $k;
				 				}
				 				$connection_keys = array();
				 				if(is_array($revisedConnArr[$pid1]))
				 					$connection_keys =	array_keys($revisedConnArr[$pid1]);
				 				//find the mutual fids
				 				$mutualFids = array();
				 				if(is_array($connection_keys) && is_array($userFids))
				 					$mutualFids = array_intersect($userFids, $connection_keys);
				 				if(!empty($mutualFids)) {
				 					$temp = array();
				 					foreach ($mutualFids as $m=>$c){
				 						$temp[] = $userFriends[$user_id][$c];
				 					}
				 					$count =  count($temp);
				 					$mutuals = json_encode($temp);
				 					
				 					if($temp!= null && !empty($temp)){
				 					
				 						$mutualCountArr[$pid1] = $count;
				 						if($user_id > $pid1){
				 							$arr[] = array($user_id, $pid1, $mutuals, $count );
				 							//	$appendSql .= " ($user_id, $key, '$mutuals', $count, now()),";
				 							//$insertSql->bind_param
				 						}
				 						else{
				 							$arr[] = array($pid1, $user_id, $mutuals, $count );
				 						}
				 						//				$appendSql .= " ( $key, $user_id, '$mutuals', $count, now()),";
				 					}
				 				}
				 				unset($revisedConnArr[$pid1]); 
				 			}
				 		}
				 	}
				 }
			}
		}
		$fbTable = "user_facebook_mutual_connections";
		if($from_cron == true){
			$fbTable = Utils::getSlavefbConnTableName();
		
		}
		
		if(!empty($arr)){
			$insertSql = $conn->Prepare("INSERT ignore into $fbTable values (?,?,?,?,now())");
			$conn->bulkBind = true;
			$conn->Execute($insertSql, $arr );
		}
// 		var_dump($mutualCountArr);
// 		var_dump(memory_get_peak_usage());
// 		exit;
		
		return $mutualCountArr;
		
// 		$rows = $ex->GetRows();
		
		
// 		$sql = "SELECT * from user_facebook_connections where user_id in ($user_id,$ids)";
// 		$ex = $conn->Execute($sql);
// 		//$rows = $ex->GetRows();
// 		$revisedConnArr = array();
// // 		if($ex) {
// // 			while ($val = $ex->FetchRow()) {
// // 			$pid = $val['user_id'];
// // 			$connects = json_decode($val['connections'], true);
// // 			if(is_array($connects)) {
// // 				foreach ($connects as $e=>$k){
// // 					$revisedConnArr[$pid][$k['id']] = $k;
// // 				}
// // 			}
// // 			}
// // 		}
// // 		//var_dump(memory_get_peak_usage());
// // 		exit;

// // 		$rows = $ex->GetRows();
// // 		var_dump($rows[0]);
// // 		var_dump(memory_get_peak_usage());
// // 		exit;
// // 		foreach ($rows as $key =>$val){
// // 			$connects = json_decode($val['connections'], true);
// // 			$pid = $val['user_id'];
// // 			$connects = json_decode($val['connections'], true);
// // 			if(is_array($connects)) {
// // 				foreach ($connects as $e=>$k){
// // 					$revisedConnArr[$pid][$k['id']] = $k;
// // 				}
// // 			}
// // 		}

// 		$fbTable = "user_facebook_mutual_connections";
// 		if($from_cron == true){
// 			$fbTable = Utils::getSlavefbConnTableName();

// 		}

// 		$insertSql = $conn->Prepare("INSERT ignore into $fbTable values (?,?,?,?,now())");
// 		$appendSql = null;
// 		$arr = array();
// 		$conn->bulkBind = true;

// 		$mutualCountArr = null;

// 		$userFids = array();
// 		if(isset($revisedConnArr[$user_id]))
// 		$userFids = array_keys($revisedConnArr[$user_id]);
// 		foreach ($revisedConnArr as $key => $val){
// 			if($key == $user_id) continue;
// 			//get the fids in the matched connection
// 			$connection_keys = array();
// 			if(isset($revisedConnArr[$key]))
// 			$connection_keys =	array_keys($revisedConnArr[$key]);
// 			//find the mutual fids
// 			$mutualFids = array();
// 			$mutualFids = array_intersect($userFids, $connection_keys);
// 			$temp = array();
// 			foreach ($mutualFids as $m=>$c){
// 				$temp[] = $revisedConnArr[$user_id][$c];
// 			}
// 			$count =  count($temp);
// 			$mutuals = json_encode($temp);

// 			if($temp!= null && !empty($temp)){

// 				$mutualCountArr[$key] = $count;
// 				if($user_id > $key){
// 					$arr[] = array($user_id, $key, $mutuals, $count );
// 					//	$appendSql .= " ($user_id, $key, '$mutuals', $count, now()),";
// 					//$insertSql->bind_param
// 				}
// 				else{
// 					$arr[] = array($key, $user_id, $mutuals, $count );
// 				}
// 				//				$appendSql .= " ( $key, $user_id, '$mutuals', $count, now()),";
// 			}

// 		}

// 		/*echo $insertSql;
// 		 var_dump($arr);*/
// 		if(!empty($arr)){
// 			//$insertSql = $insertSql . rtrim($appendSql, ",");
// 			//$insertSql = rtrim($insertSql, ",");
// 			$conn->Execute($insertSql, $arr );
// 		}
		
// 		return $mutualCountArr;
		//die;
	}


	//return endorsement data
	public function getEndorsementData($user_array = array()){
		return $this->uuDBO->getEndorsementData($user_array);
	}
	
	/**
	 * log the no thanks response in survey table for endorsements
	 * @param unknown_type $user_id
	 * @param unknown_type $response
	 */
	public function logEndorsementNoThanksResponse($user_id, $response){
		$this->uuDBO->setEndorsementNoThanksResponse($user_id, $response);
	}
	
	/**
	 * return the correction on married before for rest other status and never married as it is
	 * @param unknown_type $pref_marital_status
	 */
	public function getPreferenceMaritalStatus($pref_marital_status){
		$ms_arr = explode(',', $pref_marital_status);
		$ms = null;
		if(count($ms_arr)>1 && in_array("Never Married", $ms_arr)){
			$ms = "Never Married,Married Before";
		}elseif($ms_arr[0] != "Never Married"){
			$ms = "Married Before";
		}else{
			$ms = "Never Married";
		}
		return $ms;
	}
	
	/*
	 * rajesh for backward compatibility old android app
	 */
	function getMaritalStatusForEditPage($marital_status_spouse) {
		$ms_arr = explode(',', $marital_status_spouse);
		$ms = null;
		if(count($ms_arr)>1 && in_array("Married Before", $ms_arr)) {
			$ms = "Never Married,Separated";
		}else if(in_array("Married Before", $ms_arr)) {
			$ms = "Separated,Divorced,Widowed";
		}else {
			$ms = $marital_status_spouse;
		}
		return $ms;
	}

	public function formatMobileData($attributes_mobile, $ids, &$output, $user_id, $from_profile= false, $from_match = false, $headerFields = array()){
		global $redis, $imageurl, $dummy_male_image, $dummy_female_image, $tbNumbers;
		$output["my_data"] = $attributes_mobile['my_data'];
		$uu = new UserUtils();
		$j=0;
		$userkeys = array_keys($attributes_mobile);
		/*echo '<pre>';
				var_dump(array_keys($attributes_mobile));die;
		
		var_dump($ids);die;*/
		//var_dump(count($ids)); die;
		for($i=0; $i<count($ids);$i++){
			if($userkeys[$i] == "my_data") break;
			if(!isset($attributes_mobile[$ids[$i]]) || ( $attributes_mobile[$ids[$i]]==null) ) continue;
			$output["data"][$j]['user_id']=$ids[$i];
			$interest = $attributes_mobile[$ids[$i]]['InterestsHobbies']['preferences'];
            	//intersts and hobbies is not required for mobile data
			$attributes_mobile[$ids[$i]]['interest'] = $interest;
			//adding the interests and hobbies category wise for windows app as well
			//since in future we need data section wise for android as well, so removing the check for everyone
		/*	if($_REQUEST['app'] != "iOS" &&	 $_REQUEST['source'] != AppHeaderConstants::Windows)
			unset($attributes_mobile[$ids[$i]]['InterestsHobbies']);
*/
			//Making common likes a list
			$common_likes =  $attributes_mobile[$ids[$i]]['common_likes'];
			$common_likes_list = array();
			if(isset($common_likes)){
				foreach ($common_likes as $val){
					$common_likes_list[] = $val;
				}
			}
			unset($attributes_mobile[$ids[$i]]['common_likes']);
			$attributes_mobile[$ids[$i]]['common_likes'] = $common_likes_list;

			//making favorites a list
			$favorite =  $attributes_mobile[$ids[$i]]['favourite'];
			$favorite_list = array();
			if(isset($favorite)){
				foreach ($favorite as $val){
					$favorite_list[] = $val;
				}
			}
			//var_dump($favorite_list); die;
			unset($attributes_mobile[$ids[$i]]['favourite']);
			$attributes_mobile[$ids[$i]]['favourite'] = $favorite_list;
			$attributes_mobile[$ids[$i]]['profile_url'] = $uu->generateProfileLink($user_id, $ids[$i]) ;
			if($from_match == true){ $attributes_mobile[$ids[$i]]['profile_url'] .= '&from_match=true';}

			//var_dump($attributes_mobile[$ids[$i]]['favourite']);die;
			$output["data"][$j]['profile_data'] = $attributes_mobile[$ids[$i]];
			
			$j++;

		}
		
		//TODO: if count of ids is 0 then fetch data from queries
		/*if(count($ids) == 0){
			$adaptability = $this->getCompatibility($user_id, array());
		*//*	$interests = $this->getInterestsAndHobbies(array($user_id), false);
			$total_interests = $this->mergeAllInterestAndLikes($profile_data[$user_id]['InterestsHobbies']);
		*/	
		/*	if($adaptability[$user_id]['personalValues']==true){
				$attributes_mobile['my_data']['values_filled'] = true;
			}*/
			
		/*	if(isset($profile_data[$user_id]['InterestsHobbies'])){
				
		 	$profile_data[$user_id]['favourite'] = array_filter($matchId_interests['favourite']);
		 $output['my_data']['favourite_filled'] ;
			}*/
			
			 
		/*	if($adaptability[$user_id]['adaptability']==true){
				$attributes_mobile['my_data']['interpersonal_filled'] = true;
			}
		}*/
		$selectObject = new Select($user_id);
		$output['my_data']['values_filled'] = ($attributes_mobile['my_data']['values_filled'] == null)?false:$attributes_mobile['my_data']['values_filled'];
		$output['my_data']['favourite_filled'] = ($attributes_mobile['my_data']['favourite_filled'] == null)?false:$attributes_mobile['my_data']['favourite_filled'];
		$output['my_data']['interpersonal_filled'] = ($attributes_mobile['my_data']['interpersonal_filled'] == null)?false:$attributes_mobile['my_data']['interpersonal_filled'];
		$trust_score = array("fb"=>$tbNumbers['score']['facebook'],"photo_id"=>$tbNumbers['score']['photoid'],"linkedin"=>$tbNumbers['score']['linkedin'],"employment"=>$tbNumbers['score']['employment'],"phone"=>$tbNumbers['score']['mobile'], "endorsements"=>$tbNumbers['score']['endorsement'] );
		$output['my_data']['trustmeter']=$trust_score;
		$output['my_data']['my_select']=$selectObject->getMySelectData();
	//	var_dump($from_profile);die;
		if($from_profile == false){

			$uuDBO = new userUtilsDBO();
			$myInfo = $uuDBO->getTileDate(array($user_id));
			$image = null;
			$image2= null;
			 
			//var_dump($myInfo); die; 
			if(!isset( $myInfo[0]['thumbnail'])){
				$image = ($myInfo[0]['gender'] == "M")? $dummy_male_image: $dummy_female_image;
				$image2 = ($myInfo[0]['gender'] == "M")? $dummy_male_image: $dummy_female_image;
			}
			else{
				$image = $imageurl . $myInfo[0]['thumbnail'];
				$image2 = $imageurl . $myInfo[0]['name'];
			}
		
			$output['my_data']['profile_pic'] = $image;
			$output['my_data']['profile_pic_full'] = $image2;
			$output['my_data']['age'] = $myInfo[0]['age'];
			$output['my_data']['fname'] = $myInfo[0]['fname'];
			$output['my_data']['gender'] = $myInfo[0]['gender']; 
			$output['my_data']['city'] =($myInfo[0]['stay_city_display']==null)?$myInfo[0]['state']:$myInfo[0]['stay_city_display'];
			//adding state for MoEngage 
			$output['my_data']['state'] = $myInfo[0]['state'];
			$output['my_data']['country_code'] = $myInfo[0]['country_id'];

		/*	$mutualLikeKey = Utils::$redis_keys['mutual_like_notification']. $user_id ;
			$messageKey = Utils::$redis_keys['messages'].$user_id;*/
			/*echo $mutualLikeKey;
			 var_dump($redis->GET($mutualLikeKey));*/
			
			/**
			 * if merged list then get the message list count
			 * else return mutual like counter and message count will be ( message list - mutual like )
			 */
			if ($headerFields['isMessagesMatchMerged'] == true) 
			{
				$notificationCounters = $this->getNotificationCounters($user_id, true);
				$output['my_data']['message_count'] = intval($notificationCounters['counter']);
			}
			else 
			{
				$notificationCounters = $this->getNotificationCounters($user_id, false);
				$output['my_data']['mutual_like_count'] = intval($notificationCounters['mutual_like']);
				$output['my_data']['message_count'] = intval($notificationCounters['message_count'] );
			}
/*			$output['my_data']['mutual_like_count'] = intVal($redis->GET($mutualLikeKey) );
			$output['my_data']['message_count'] = $redis->SCARD($messageKey) ;*/
		}
	}

	/**
	 * get notification counters based on version
	 * @param unknown_type $user_id
	 */
	public function getNotificationCounters ($user_id , $isMergedList = false)
	{
		if ($isMergedList == false)
		{
			$mutualLikeKey = Utils::$redis_keys['mutual_like_notification']. $user_id ;
			$mutual_like_count = intVal($this->redis->GET($mutualLikeKey) );
				
			$mutualLikeListKey = Utils::$redis_keys['mutual_like_only_list']. $user_id ;
			$messageKey = Utils::$redis_keys['messages'].$user_id;
				
			$message_ids = $this->redis->SMEMBERS($messageKey) ;
			$mutual_like_ids = $this->redis->SMEMBERS($mutualLikeListKey);

			return array("mutual_like" => $mutual_like_count, "message_count" => count(array_diff($message_ids, $mutual_like_ids)));
		}
		else
		{
			$messageKey = Utils::$redis_keys['messages'].$user_id;
			$total_count = $this->redis->SCARD($messageKey) ;
			return array("count" => $total_count);
		}

	}
	
	/**
	 * whenever a block action comes from admin 
	 * remove this id from list of every user's list 
	 * @param unknown_type $ids
	 */
	
	public function removeIdsFromMessagesAndMutualLikeList ($ids, $toRemoveFrom = array())
	{
		global $redis;
		$i = 1;
		$args = array();
		foreach ($ids as $value)
		{
			$args[$i] = $value;
			$i++;
		}

		if ( empty($toRemoveFrom) )
		{
			$key = Utils::$redis_keys['messages'] . "*";
			$mKeys = $redis->KEYS($key);
			if(count($mKeys)>0)
			{
				//get all the keys
				$values = $this->redis->MGET($mKeys);

				//remove these ids from each key
				foreach ($values as $msgKey)
				{
					$args[0] = $msgKey;
					call_user_func_array(array($redis,'SREM'),$args);
				}

			}
		}
	}
	
	
	public function getInterestsAndHobbies($user_array, $fetchRowsEnabled = true){

		$arr = null;
		$results = $this->uuDBO->getInterestsAndHobbies($user_array, $fetchRowsEnabled);
		//	echo '<pre>';var_dump($results); die;
		
		//changing the key value pair since we no longer need any fid or freebase id to be sent 
		//array_values is used to make it dictionary
		foreach($results as $key => $value){
			$music = json_decode($value['music_favorites'], true);
			$music_list = array();
			if(is_array($music)){
				foreach ($music as $val)
				$music_list[] = $val;
			}
			$arr[$key]['music_favorites'] = array_values(array_filter($music_list));
				
			$books = json_decode($value['books_favorites'], true);
			$books_list = array();
			if(is_array($books)){
				foreach ($books as $val)
				$books_list[] = $val;
			}
			$arr[$key]['books_favorites']= array_values(array_filter($books_list));
				
			//$arr[$key]['food_favorites'] =json_decode( $val['food_favorites'], true);
				
			$movies = json_decode($value['movies_favorites'], true);
			$movies_list = array();
			if(is_array($movies)){
				foreach ($movies as $val)
				$movies_list[] = $val;
			}
			
			$arr[$key]['movies_favorites'] = array_values(array_filter($movies_list)); 
				
			$sports =  json_decode($value['sports_favorites'], true);
			$sports_list = array();
			if(is_array($sports)){
				foreach ($sports as $val)
				$sports_list[] = $val;
			}
			$arr[$key]['sports_favorites'] = array_values(array_filter($sports_list));
				
			$travel = json_decode($value['travel_favorites'], true);
			$travel_list = array();
			if(is_array($travel)){
				foreach ($travel as $val){
					$travel_list[] = $val;
				}
			}
			$arr[$key]['travel_favorites'] = array_values(array_filter($travel_list));
				
			$others = json_decode($value['other_favorites'], true);
			$others_list = array();
			if(is_array($others)){
				foreach ($others as $val)
				$others_list[] = $val;
			}
			$arr[$key]['other_favorites'] = array_values(array_filter($others_list));
				
			$arr[$key]['preferences'] = json_decode($value['preferences'], true);
		}
		//var_dump($arr);die;
		return $arr;
	}

	public function getCompatibility($user_id, $match_id_array){
		$compatibility = null;
		$user_array = array_merge(array($user_id), $match_id_array);
		$psycho = $this->uuDBO->getPsychoResult($user_array, false);
		$data = array();
		foreach ($psycho as $key => $val){
			$data[$val['user_id']] = $val;
		}

		$myData = $data[$user_id];
		//var_dump($myData); die;
		//if($myData == null) return false;
		$myAdaptability = $this->getAdaptability($myData['interpersonal_score']);
		foreach ($data as $key=>$val ){
			if($key==$user_id) {
				if(isset($myData['sum_value_A']))
				$compatibility[$key]['personalValues'] = true;
				else $compatibility[$key]['personalValues'] = false;

				if(isset($myData['interpersonal_score']))
				$compatibility[$key]['adaptability'] = true;
				else $compatibility[$key]['adaptability'] = false;

				continue;
			}

			$personalValues = null;
			$adaptabililtyScore = null;


			//values matching
			if(isset($val['sum_value_A'])){
					
				$deviation = null;
				//$deviation =  ( abs($myData['sum_value_A'] - $val['sum_value_A']) + abs($myData['sum_value_B'] - $val['sum_value_B']) + abs($myData['sum_value_C'] - $val['sum_value_C']) + abs($myData['sum_value_D'] - $val['sum_value_D']) + abs($myData['sum_value_E'] - $val['sum_value_E']) + abs($myData['sum_value_F'] - $val['sum_value_F']))/6;

				$deviation =  ( abs($myData['sum_value_A'] - $val['sum_value_A']) + abs($myData['sum_value_C'] - $val['sum_value_C']) + abs($myData['sum_value_D'] - $val['sum_value_D']) + abs($myData['sum_value_E'] - $val['sum_value_E']) + abs($myData['sum_value_F'] - $val['sum_value_F']))/5;
				
				if ($deviation <= 2){
					$personalValues = 4;  
				}elseif ($deviation<=4){
					$personalValues =3;
				}elseif ($deviation<=7){
					$personalValues=2;
				}else{
					$personalValues = 1;
				}
			}
			$compatibility[$key]['personalValues'] = $personalValues;

			//interpersonal matching

			if(isset($val['interpersonal_score'])){
					
				$matchAdaptability = $this->getAdaptability($val['interpersonal_score']);
				if(($myAdaptability==InterpersonalConstants::HighAdaptability) || ($matchAdaptability==InterpersonalConstants::HighAdaptability)){
					if(($matchAdaptability==InterpersonalConstants::LowAdaptability)||($myAdaptability==InterpersonalConstants::LowAdaptability))
					$adaptabililtyScore = 3;
					else $adaptabililtyScore = 4;
				}else if(($myAdaptability==InterpersonalConstants::MediumAdaptability)||($matchAdaptability==InterpersonalConstants::MediumAdaptability)){
					if(($matchAdaptability==InterpersonalConstants::LowAdaptability)||($myAdaptability==InterpersonalConstants::LowAdaptability))
					$adaptabililtyScore = 2;
					else $adaptabililtyScore = 3;
				}else {
					$adaptabililtyScore = 1;
				}
			}
			$compatibility[$key]['adaptability'] = $adaptabililtyScore;
		}
		return $compatibility;
	}


	private function getAdaptability($score){
		if($score<=13){
			return  InterpersonalConstants::HighAdaptability;
		}elseif ($score<=19){
			return InterpersonalConstants::MediumAdaptability;
		}else{
			return InterpersonalConstants::LowAdaptability;
		}
	}

	
	public function getMailerTile($match_id_array, $user_id, $campaignStr = null){

		global $admin_id;
		$result = array();
		$data=array();
		$resultSet = $this->uuDBO->getTileDate($match_id_array);
		foreach ($resultSet as $val){
			
			$data[$val['user_id']]['fname']=$val['fname'];
			$data[$val['user_id']]['age']=$val['age'];
			$data[$val['user_id']]['city']=($val['stay_city_display']==null)?$val['state']:$val['stay_city_display'];
			if(isset($val['thumbnail'])){
				$data[$val['user_id']]['profile_pic']=$this->getImageFullPathFromDB($val['thumbnail']);
			}
			else if($val['user_id']!=$admin_id)
			{
				$data[$val['user_id']]['profile_pic']=$this->getDummyImage($val['gender']);
			}
				
			$data[$val['user_id']]['profile_link']=$this->generateProfileLink($user_id,$val['user_id'], $campaignStr);
			$data[$val['user_id']]['message_link']=$this->generateMessageLink($user_id,$val['user_id'], $campaignStr);

			
		}
		return $data;

	}

	/*public function updateUserSearch($data, $user_id, $table){
		$keys = null;
		$values  = null;
		$updateStr = null;
		var_dump($data);
		foreach ($data as $key =>$val){
			if($key == "user_id") continue;
			$keys[] = $key;
			$values[] = $val;
			$updateStr = $key . "=". $val . ",";
		}
		if($updateStr != null){
			$updateStr = rtrim($updateStr, ",");
			$sql = "UPDATE user_search set $updateStr where user_id = $user_id";
			Query::UPDATE($sql, null);
		}
	}*/ 
	
	public function updateDiffInMatches($user_id, $user_array,$likedIds){
		
		$i=1;
		$args[0] =  Utils::$redis_keys['matches']. "$user_id";
		foreach ($user_array as $key=>$val){
				if(isset($likedIds) && in_array($val, $likedIds))
				$args[$i] = $val .'l';
				else
				$args[$i] = $val;
				$i++;
		}
		//call_user_func_array(array($this->redis,'SREM'),$args);

		if(isset($args[1]))
		call_user_func_array(array($this->redis,'LREM'),$args);
		//$this->redis->SADD($this->key,  $user_array);
	
	}

	/**
	 * returns the hashtags of all the users in parameter
	 * @param unknown_type $user_array
	 */
	public function getHashTags($user_array){
		if(isset($user_array) && count($user_array) > 1)
		{
			$hashTagRows =  $this->uuDBO->getHashTags($user_array);
			foreach ($hashTagRows as $val)
			{
				$hashTags[$val['user_id']] = $val['preferences'];
			}
			return $hashTags;
		}
		else return array();
	}
	
	/**
	 * returns the matching hashtag between user1 and all the users in user_array
	 * @param $user1
	 * @param $user_array
	 */
	public function getHashTagsIntersection ($user1, $user_array = array()){
		$matchedHashTags = array();

		if(is_array($user_array))
		{
			if(count($user_array)>=1)
			{
				array_push($user_array, $user1);
				$hashTags = $this->getHashTags($user_array);
				foreach ($hashTags as $key => $val)
				{
					if($key == $user1) continue;
					$matchedHashTags[$key] = array_intersect(json_decode($hashTags[$user1], true), json_decode($hashTags[$key],true));
				}

			}
		}
		return $matchedHashTags;
	}
	
		public function getAttribute($usage, $user_id, $match_id_array, $from_match = false, $isAuthentic = false,
			 $myProfile = false, $login_mobile, $mutualLikeFlag = false, $likedIds = null, $setFlag = null,
			 $availability_using_ids = array(),$version=-1, $extra_params = array()){
            
		global $dummy_male_image, $dummy_female_image, $dummy_female_image_new,$dummy_male_image_new;
                $adCampaigns = array();
                if(ProfileCampaign::isProfileCampaigningActive()) {
                    $adCampaigns = ProfileCampaign::getUserIdsForActiveCampaigns();
                }
		$profile_data = array();
		$userInfo = null;
		if(count($match_id_array) == 0 || empty($match_id_array )) return null ;
		$ids = array_merge(array($user_id), $match_id_array);
		$row_data1 = $this->uuDBO->getUserBasicInfo($ids,false);
                $myUserData = $this->uuDBO->getUserData(array($user_id), false);
                $myCountry = $myUserData[0]['stay_country'];

                #App info 
                $headers = null;
		$app_version_code = null;
		$source = null;
		//check the versions for app code  - may be something temporarily till the time every field is properly stable
		if($login_mobile == true){
			$headers = Utils::getAllSystemHeaderFields();
			$app_version_code = $headers['app_version_code'];
			$source = $headers['source'];
		}
                $userFlags = new userFlags($user_id,false);
                $userSelectFlag = $userFlags->getSelectFlags(true);
                
		$selectObject = new Select($user_id);
		$selectData = $selectObject->getSelectCommanalities($match_id_array);
		foreach ($row_data1 as $val){
                        /* Select Tag :
                         * The following is when the tag can come TRUE when all the below conditions are true
                         * 0. If the extra_params is not set, simply return the select data. No logic required
                         * 1. The match should be a select
                         * 2. I should have the new app
                         * 3. Either I am a select 
                         *      or 
                         *          (I am a non-select and have the new app and the feature is switched on for me
                         *              (The profile is sprinkled) or 
                         *              (There is no likedId list) or 
                         *              ( The list there and the match has not liked me before)
                         *          )
                         */
                        # Check for users for which the select data comes empty
                        if(!isset($selectData[$val['user_id']])) {
                            $selectData[$val['user_id']] = array("is_tm_select"=>false);
                        }
			$userInfo[$val['user_id']] = array( "user_id" => $val['user_id'],
                                                            "fname" => $val['fname'],
                                                            "gender" => $val['gender'],
                                                            "status" => $val['status'],
                                                            "last_login" => Utils::humanTime($val['last_login']),
                                                            "spark_hash" => SparkDBO::getMd5Hash($user_id, $val['user_id'], "spark"),
                                                            "select" => (  (isset($myCountry) && $myCountry != '113') ? array("is_tm_select"=>false) :
                                                                            (
                                                                                !isset($extra_params['isSelect']) ? $selectData[$val['user_id']] :
                                                                                (
                                                                                    isset($selectData[$val['user_id']]) && 
                                                                                    isset($extra_params['isSelect']) &&
                                                                                    ($extra_params['isSelect'] || 
                                                                                        (!$extra_params['isSelect'] && 
                                                                                            $source != null &&
                                                                                            $app_version_code != null &&
                                                                                            (($source == 'androidApp' && $app_version_code > ANDROID_APP_VERSION_CODE_SUPPORTING_SELECT) || ($source == 'iOSApp' && $app_version_code > IOS_APP_VERSION_CODE_SUPPORTING_SELECT)) && $userSelectFlag &&
                                                                                            (   in_array($val['user_id'],$extra_params['selectSprinkledProfiles']) || 
                                                                                                !isset($likedIds) ||
                                                                                                (isset($likedIds) &&  !in_array($val['user_id'], $likedIds))
                                                                                            )
                                                                                         )
                                                                                    )
                                                                                    ? $selectData[$val['user_id']] : array("is_tm_select"=>false)
                                                                                )
                                                                            )
                                                                         ),
                                                            "nri" => ((isset($myCountry) && $myCountry != '113') ? array("is_nri"=>false) : array("is_nri" => ($val['stay_country'] != '113'))));

		}

                if($_REQUEST['webpSupport'] == 'yes') {$webp = TRUE;}

		if($usage=='profile') { $version=331;}

		$images_arr = $this->getActiveImages($match_id_array, $userInfo[$user_id]['gender'], $myProfile,$webp,$version,$extra_params);
		$video_arr=$this->getActiveVideos($match_id_array,$myProfile);
		foreach($video_arr as $key=>$value) {
			$profile_data[$key]['video_profile']=$video_arr[$key]['video_profile'];
		}

		$non_authentic_ids = array();
		foreach ($images_arr as $key => $val){
			//if($from_match == true){
			if($myProfile == false){
				if($from_match == true){
					if($userInfo[$user_id]['gender']=='F')
					{
						//If the user is female, she will not be able to see all the males, who dont have any pic or their status anything other than authentic
						if((strpos( $images_arr[$key]['profile_pic'], "dummy") !== false) || $userInfo[$key]['status'] != 'authentic' ){
							$non_authentic_ids[] = $key;
							continue;
						}
					}
					else 
					{
						//If the user is male , then he can see feamles with no profile pic,so one condition from above gets removed
						 if($userInfo[$key]['status'] != 'authentic' ){
							$non_authentic_ids[] = $key;
							continue;
						}
					}
				}
			}
			//}
			$profile_data[$key]['profile_pic'] = $images_arr[$key]['profile_pic'];
			$profile_data[$key]['other_pics'] = $images_arr[$key]['other_pics'];
			$profile_data[$key]['thumbnail_pics'] = $images_arr[$key]['thumbnail_pics'];
		}
		
		if($myProfile == true){
			$myGender = $userInfo[$user_id]['gender'];
			if(strpos( $profile_data[$key]['profile_pic'], "dummy") !== false){
				$profile_data[$key]['profile_pic'] = ($myGender == "M")? $dummy_male_image_new: $dummy_female_image_new;
				$profile_data[$key]['isDummySet'] = true;
			}
		} 
		
		if(!empty($non_authentic_ids)){
			$junk_ids = array_intersect($match_id_array, $non_authentic_ids);
			$match_id_array = array_diff($match_id_array, $non_authentic_ids);
			
		//remove from redis availability
			if($userInfo[$user_id]['gender'] == "F"){
                                $nonAvailableIds = array_intersect($junk_ids, $likedIds);
				$count_junk_ids = count($nonAvailableIds);
				if($count_junk_ids !=0){

					$minuteOfHour = floor(date("i")/5)%2;
					if($minuteOfHour == 0)$db =$this->redisOddDB; else $db =$this->redisEvenDB;
					$this->redis->SELECT($db);
					$this->redis->DECR(Utils::$redis_keys['like_count'].$user_id, $count_junk_ids);
					$this->redis->SELECT(0);
					$sentinel_key = array_search("-1", $junk_ids); //$sentinel_key is -1 
					unset($junk_ids[$sentinel_key]);  
					$this->updateDiffInMatches($user_id, $junk_ids, $likedIds);
				}
			}
			
			foreach ($junk_ids as $val){
				unset($userInfo[$val]);
			}
		}
        
		//final set of ids including my id
		$allValidIdsWithMyId =array();
		$allValidIdsWithMyId = array_merge(array($user_id), $match_id_array);
		 
		if(count($match_id_array) == 0 || empty($match_id_array )) return null ;

		/*
		 * POC Mode hack being used here.Ideally this should get picked from a SQL table.
		 */ 
		$videoProfiles = array(	580941  => 'LIMzOPbxrCQ',
							   	1556082 => 'y37nQZ4TuL0',
							   	1534814 => '4l4OxbhVlhQ',
							   	1534310 => '_SnPOogUAJk',
							   	1534528 => 'LjaAfYOGTfI',
							   	1534390 => 'joGWwgwtVaA',
							   	1534492 => 'a7K9hiKb54I');

		foreach ($userInfo as $key => $value) {
			if($key!= $user_id){
				if(in_array($key, $adCampaigns) && !key_exists($key,$videoProfiles)) {
					$profile_data[$key]['isProfileAdCampaign'] = true;
					$profile_data[$key]['campaign_links'] = ProfileCampaign::getAdCampaignLinks($key);
				}
				else {
					$profile_data[$key]['isProfileAdCampaign'] = false;
				}

				if($profile_data[$key]['isProfileAdCampaign'] == true)
				{
					$event_details = ProfileCampaign::getEventDetailsForAdCampaign($key);
					if($event_details['is_event'] == 'yes')
					{
						$event_details = json_decode($event_details['event_details'],true);
						$event_text = $event_details['details_text'] ;
						$profile_data[$key]['isProfileAdCampaignForScenes'] =  true ;
						$profile_data[$key]['sponsoredEventDetails'] =  $event_text ;
					}
					else
					{
						$profile_data[$key]['isProfileAdCampaignForScenes'] =  false;
					}
				}
				else
				{
					$profile_data[$key]['isProfileAdCampaignForScenes'] =  false ;
				}

				if($from_match == true && $profile_data[$key]['isProfileAdCampaign'] == false ){
					//$profile_data[$key]['fname'] = ucfirst(substr($value['fname'] , 0 ,1)) .  str_repeat("x",strlen($value['fname'])-1 );//$value['fname'];
					$profile_data[$key]['fname'] =  str_repeat("x",strlen($value['fname']));//$value['fname'];
				}
				else $profile_data[$key]['fname'] = $value['fname'];
				$profile_data[$key]['last_login'] = $value['last_login'] ;
				$profile_data[$key]['status'] =  $value['status'];
				$profile_data[$key]['mutual_events'] =  $value['mutual_events'];
				$profile_data[$key]['select'] =  $value['select'];
				$profile_data[$key]['nri'] =  $value['nri'];

				$profile_data[$key]['spark_hash'] =  $value['spark_hash'];

				if(key_exists($key,$videoProfiles)) { 
                                    $profile_data[$key]['videoLink'] = $videoProfiles[$key];
                                }
			}
			else{
				$myStatus = $value['status'];
				$myGender = $value['gender'];
				$myName = $value['fname'];
			}

			if($myProfile == true){
				$profile_data[$key]['fname'] = $value['fname'];
				$profile_data[$key]['last_login'] = $value['last_login'] ;
				$profile_data[$key]['status'] =  $value['status'];
			}
		}

		$row_data = $this->uuDBO->getUserData($match_id_array, false);
		foreach ($row_data as $key => $val){
			if($val['state_code'] != null)
				$val['stay_city_display'] .= ", ".$val['state_code'];
			$profile_data[$val['user_id']]['marital_status'] = $this->getMaritalStatusString($val['marital_status']);
			$profile_data[$val['user_id']]['age'] = $val['age'];
			$profile_data[$val['user_id']]['city'] = ($val['stay_city_display']==null)?$val['state']:$val['stay_city_display'];
			$profile_data[$val['user_id']]['country_id'] = $val['stay_country'];
			$profile_data[$val['user_id']]['country_code'] = $val['stay_country']; 
			$profile_data[$val['user_id']]['country'] = $val['country'];
			$profile_data[$val['user_id']]['height'] = Utils::heightInchToFeet($val['height']);
			$profile_data[$val['user_id']]['highest_degree'] = $val['degree'];
			$profile_data[$val['user_id']]['height_inch'] = $val['height'];
			$profile_data[$val['user_id']]['industry'] =$val['industry_name'];
			$profile_data[$val['user_id']]['designation'] =(!$login_mobile)?ucfirst(htmlentities($val['designation'])):ucfirst($val['designation']);
			$profile_data[$val['user_id']]['work_status'] = $val['work_status'];
			if(isset($val['is_verified_celeb'])&&$val['is_verified_celeb']==1)
			{$profile_data[$val['user_id']]['celeb_status'] = true;}
			if($profile_data[$val['user_id']]['work_status'] == 'no'){
				$profile_data[$val['user_id']]['designation'] = "Not Working";
			}

			$incomeKey = null;
			if(isset($val['income_start']) && isset($val['income_end']))
			$incomeKey = $val['income_start']."-".$val["income_end"];
			$incomeArr = array("0-10"=>"<10 lakhs","10-20"=>"10-20 lakhs","20-50"=>"20-50 lakhs","50-51"=>">50 lakhs");

			if($login_mobile == true){
				if((isset($source) && ($source == AppHeaderConstants::IOS || $source == AppHeaderConstants::Windows))){
					if ($incomeKey!= null && isset($incomeArr[$incomeKey]))
					$profile_data[$val['user_id']]['income'] = $incomeArr[$incomeKey];
				}else{
					$profile_data[$val['user_id']]['income'] = (!isset($incomeArr[$incomeKey])  && $app_version_code <= Utils::$appVersionForDefaultIncome)?"Income: ".Utils::$NAstring : $incomeArr[$incomeKey] ;
				}
			}else{
				if ($incomeKey!=null && isset($incomeArr[$incomeKey]))
				$profile_data[$val['user_id']]['income'] = $incomeArr[$incomeKey];
			}

			$insti = null;
			$newInstiArr = null;
			$capsComp = null;

			$insti = json_decode($val['institute_details'], true);
			$comps = json_decode($val['company_name'], true);
			
			$tmpCmp = array();
			$i=0;
			if(is_array($comps)){
				foreach ($comps as $id=>$value) {
					if(isset($value)) {
						$tmpCmp[$i] = $value;
						$i++;
					}
				}
			}
			$i=0;
			$comps = $tmpCmp;
			$tmpIns = array();
			if(is_array($insti)){
				foreach ($insti as $id=>$value) {
					if(isset($value)) {
						$tmpIns[$i] = $value;
						$i++;
					}
				}
			}
			$i=0;
			$insti = $tmpIns;
			
			if(!$login_mobile){
				$cmp = array();
				$inst = array();
				if(is_array($comps)){
					foreach($comps as $id=>$value){
						if(isset($value) && strlen($value)>0)
						$cmp[$id] = htmlentities($value);
					}
				}
				$comps = $cmp;
					
				if(is_array($insti)){
					foreach($insti as $id=>$value){
						if(isset($value) && strlen($value)>0)
						$inst[$id] = htmlentities($value);
					}
				}
				$insti = $inst;
			}
	
			$profile_data[$val['user_id']]['institutes'] = $insti;
			$profile_data[$val['user_id']]['companies'] = $comps;
		}
		//TrustBuilder
		$trustMeter = $this->uuDBO->getTrustBuilderData($match_id_array, false);
		foreach ($trustMeter as $val){
			
			$profile_data[$val['user_id']]['trustMeter']['fb_connections'] = ($val['fb_connections'] == -1)?"Some":$val['fb_connections'];
			$profile_data[$val['user_id']]['trustMeter']['linkedin_connections'] = $val['linkedin_connections'];
			$profile_data[$val['user_id']]['trustMeter']['mobile_number'] = (isset($val['mobile_number'])?(substr_replace($val['mobile_number'], 'xxxxx', 5)):null);
			$profile_data[$val['user_id']]['trustMeter']['trust_score'] = $val['trust_score'];
			$profile_data[$val['user_id']]['trustMeter']['employment'] = ($val['employment'] == 'active')?'active':null;
			$profile_data[$val['user_id']]['trustMeter']['id_proof'] = ($val['id_proof'] == 'active')?'active':null;
			if($val['id_proof'] == 'active'){
				$profile_data[$val['user_id']]['trustMeter']['id_proof_type'] = $val['id_type'];
			}
			if($val['employment']=='active'){
				if($val['company_name'])
				$profile_data[$val['user_id']]['trustMeter']['employment_company'] = $val['company_name'];
				else $profile_data[$val['user_id']]['trustMeter']['employment_company'] = "Date Verified: ". date('Y/m/d', strtotime($val['employment_verified_on']));
			}
			if(isset($val['linkedin_connections'])){
				if(isset($val['linkedin_designation']))
				$profile_data[$val['user_id']]['trustMeter']['linkedin_designation'] = $val['linkedin_designation'];
				else $profile_data[$val['user_id']]['trustMeter']['linkedin_designation'] = $val['linkedin_connections'] ." Connections";
			}

		}
		//$intHobs = $this->uuDBO->getInterestsAndHobbies(array($user_id));
		$user_array = array_merge(array($user_id), $match_id_array);

		/*$intHobs = $this->uuDBO->getPreferenceInterests($match_id_array, false);
		 foreach ($intHobs as $val){
			$profile_data[$val['user_id']]['preferredInterests'] = json_decode($val['preferences'], true);
			}*/
	// SATISH TODO: pass this as param
	if (isset($_REQUEST['fav_v2']) && $_REQUEST['fav_v2']) {
		$uih = new UserInterestHobbies();
		$fav_data = $uih->get_user_interest_hobbies($user_array);
		$my_fav = $fav_data[$user_id];
		foreach ($fav_data as $key => $val) {
			if ($myProfile && $key == $user_id) {
				$profile_data[$key]['InterestsHobbies'] = FacebookLikesUtils::process_response($val);
			}
			if ($key != $user_id) {
				$profile_data[$key]['InterestsHobbies'] = FacebookLikesUtils::process_response($uih->process_common_favorites($my_fav, $val));
				// common hashtags
				$my_pref = (isset($my_fav['preferences']))? array_values($my_fav['preferences']) : array();
				$match_pref = (isset($val['preferences']))? array_values($val['preferences']) : array();
				$common_hashtags = array_filter(array_intersect($my_pref, $match_pref));
				$common_hastags_list = array();
				foreach ($common_hashtags as $hashtag)
					$common_hastags_list[] = $hashtag;
				$profile_data[$key]['common_hashTags'] = $common_hastags_list;
				$profile_data[$key]['common_favourites']['preferences'] = $common_hastags_list;
				$common_hastags_list_without_hash = array();
				if (!empty($common_hastags_list)){
					foreach ($common_hastags_list as $hashtag){
						$common_hastags_list_without_hash[] = ltrim($hashtag, '#');
					}
				}
				$profile_data[$key]['common_likes'] = $common_hastags_list_without_hash;
			}
		}
	}

	else {
		$interests = $this->getInterestsAndHobbies($user_array, false);
		foreach ($interests as $key=>$val){
			$profile_data[$key]['InterestsHobbies'] = $val;
		}


		/*echo '<pre>';
		 var_dump($interests); die;
		 */
		$myFavorites = array();
		if(isset($profile_data[$user_id]['InterestsHobbies'])){
			$myFavorites = $profile_data[$user_id]['InterestsHobbies'];
		    $myInterests = $this->mergeAllInterestAndLikes($profile_data[$user_id]['InterestsHobbies']);
		if($myProfile == false){
			unset($profile_data[$user_id]['InterestsHobbies']);
		}
		 }
		 else $myInterests = null;
		 //var_dump($myInterests); die;
		 if($myProfile == false){
		 	foreach ($interests as $key => $val){
		 		if($key==$user_id)continue;
		 		$matchId_interests = $this->mergeAllInterestAndLikes($profile_data[$key]['InterestsHobbies']);
		 		$profile_data[$key]['favourite'] = array_filter($matchId_interests['favourite']);
		 		if($matchId_interests['all'] != null){
		 			$commonLikes = array_intersect( $myInterests['all'], $matchId_interests['all']);
		 			$profile_data[$key]['common_likes'] = array_filter($commonLikes);
		 		}
		 		//}

		 		if($matchId_interests['hashTags']!= null && $myInterests['hashTags']!=null){
		 			$commonHashTags = array_intersect( $myInterests['hashTags'], $matchId_interests['hashTags']);
		 			$commonHashTags = array_filter($commonHashTags);
		 			$commonHashTagsList = array();
		 			foreach ($commonHashTags as $val)
		 			$commonHashTagsList[] = $val;
		 			$profile_data[$key]['common_hashTags'] = $commonHashTagsList;
		 			
		 		}
		 		if($myInterests == null || $matchId_interests == null){
		 			$profile_data[$key]['common_likes'] = null;
		 		}

		 		// common favourites
		 		if(count($myFavorites['music_favorites'])>0 || count($myFavorites['movies_favorites'])>0 || count($myFavorites['books_favorites'])>0
		 			|| count($myFavorites['travel_favorites'])>0 || count($myFavorites['other_favorites'])>0) {
		 			$profile_data[$key]['common_favourites'] = $this->commonfavorites($myFavorites,$profile_data[$key]['InterestsHobbies']);
		 		}
		 				 
		 		if(!(count($profile_data[$key]['common_favourites']['music_favorites'])>0  || count($profile_data[$key]['common_favourites']['movies_favorites'])>0
							|| count($profile_data[$key]['common_favourites']['books_favorites'])>0 || count($profile_data[$key]['common_favourites']['travel_favorites'])>0
		 					|| count($profile_data[$key]['common_favourites']['other_favorites'])>0)) {
		 			unset($profile_data[$key]['common_favourites']);
		 		}
		 						 
		 	}
		 }
		 else{
		 	$matchId_interests = $this->mergeAllInterestAndLikes($profile_data[$user_id]['InterestsHobbies']);
		 	$profile_data[$user_id]['favourite'] = array_filter($matchId_interests['favourite']);
		 }
	}

		 $adaptability = $this->getCompatibility($user_id, $match_id_array);
			//My psycho is not filled
			$myValues = null;
			$myInterpersonal = null;


			if($adaptability[$user_id]['personalValues']==true){
				$myValues = true;
			}
			if($adaptability[$user_id]['adaptability']==true){
				$myInterpersonal = true;
			}
			if($myProfile == false){		
				//to run for loop the variable shud be an array first
				if (is_array($adaptability)){
					foreach ($adaptability as $key=>$val){
						if($key==$user_id) continue;
						$profile_data[$key]['match_details'] =array("values"=>$val['personalValues'],"adaptability"=>$val['adaptability']);
					}
				}

				$mutual_connections = $this->uuDBO->getMutualConnections($user_id, $match_id_array);
				foreach ($mutual_connections as $key => $val){
					$profile_data[$key]["mutual_connections_count"] = $val['mutual_connections_count'];

						$mutuals = null;
						$i=0;
						foreach ($val['mutual_connections'] as $mdata){
							$mutuals[$i]["name"] = $mdata["first_name"];
							$mutuals[$i]["pic"] = $mdata["picture"]["data"]["url"];
							$i++;
						}
						$profile_data[$key]["mutual_connections"] = $mutuals;

				}
			}
			//get endorsement data
			$endorsement_data = $this->uuDBO->getEndorsementData($match_id_array);
			foreach ($endorsement_data as $key => $val){
				$profile_data[$key]["endorsementData"] = $val;
				if(count($val) >= EndorsementConstants::MinimumEndorsementCount) 
				$profile_data[$key]["is_endorsement_verified"]=  true;
			}
			
			if($myGender == "F" && $myProfile == false && $login_mobile == true && $isAuthentic == true ){
				
				$shareLinks = $this->generateShareProfileLinks($match_id_array, $setFlag);
				foreach ($shareLinks as $key => $val){
					$profile_data[$key]['share_profile'] = array(
                                            "share_link" => $shareLinks[$key],
                                            "share_message" => "Hey lemme know what you think of this guy I met on TrulyMadly",
                                            "alert_message"=> "Ask your besties about what they think of him!...Shh this feature is only for girls!",
                                            "hide_alert_message" => "How about you like him instead till you hear from your friend. You can always block him later.");
				}
			}
			//for my profile get activity dashboard
			if($login_mobile == true && $myProfile == true && $myGender == "M"){
				$activity_data = $this->getActivityDashboard(array($user_id));
				foreach ($activity_data as $key=>$val){
					$popularity = $val['bucket'];
					if( $popularity >=4 ){
						$profile_data[$key]['activityData']['popularity_text'] = "Wow, looks like you are really popular!";
						$profile_data[$key]['activityData']['popularity'] = $popularity;
					}else if($popularity>=1){
						$profile_data[$key]['activityData']['popularity_text'] = "Boost popularity with a new profile picture!";
						$profile_data[$key]['activityData']['click_action'] = "PHOTO";
						$profile_data[$key]['activityData']['popularity'] = $popularity;
					}

					//get visibility of 14 days from table and today's from redis
					if($popularity > 0){
						$profile_data[$key]['activityData']['activity'] = $val['visibility'] + $this->getTodaysActivity($key);
						$profile_data[$key]['activityData']['activity_text'] = "Views in last 15 days";
					}

				}
			}


			$profile_data['my_data']=array(
                            "fname"=> $myName,
                            "user_id" => $user_id,
                            "status"=>$myStatus,
                            "gender" => $myGender,
                            "favourite_filled"=> (!empty($myInterests['favourite']))?true:null,
                            "values_filled"=>$myValues,
                            "interpersonal_filled"=>$myInterpersonal,
                            "from_match" => $from_match
			);
			if(count($match_id_array) == 1 && $match_id_array[0] == $user_id){
				return $profile_data;
			}
			else{
				//TODO: ids without action need can be liked or hide loop on that// Check for mutual like and message link
				foreach ($match_id_array as $val){
					//var_dump($val);
					//TODO: get Actions done for ids array and same for can communicate re-write the function
				//if(($isAuthentic == true)&&!(isset($alreadyLikedIds)&&in_array($val, $alreadyLikedIds))&&!(isset($alreadyHideIds)&&in_array($val, $alreadyHideIds))  &&!(isset($alreadyMaybeIds)&&in_array($val, $alreadyMaybeIds))){
	//			if(($isAuthentic == true)&&!(isset($alreadyLikedIds)&&in_array($val, $alreadyLikedIds))&&!(isset($alreadyHideIds)&&in_array($val, $alreadyHideIds)) ){

				//	if($isAuthentic == true || ($status == "non-authentic" && $app_version_code >= Utils::$appVersionForNonAuthenticActionEnable  )){
										//var_dump($val);
						if($setFlag == "matches"){
						$profile_data[$val]['canHide'] = true;
						$profile_data[$val]['canLike'] = true;
						
						if(isset($likedIds) && in_array($val, $likedIds)) $flag = 1; else $flag =0;
						if ( in_array($val,$extra_params['repeat_likes'])){
							$repeat_like_flag=true;
						}
						else{
							$repeat_like_flag=null;
						}
						$profile_data[$val]['like_url'] = $this->generateLikeLink($user_id, $val, $flag,$myGender, $availability_using_ids[$val],$repeat_like_flag);
						$profile_data[$val]['hide_url'] = $this->generateHideLink($user_id, $val, $flag, $myGender, $availability_using_ids[$val],$repeat_like_flag);
						}
						
						
						if($setFlag == "like"){
							$profile_data[$val]['already_liked'] = true;
						}

						//not to show any link if non-authentic user is from web
						if($login_mobile == false && $isAuthentic == false) {
							$profile_data[$val]['canHide'] = false;
							$profile_data[$val]['canLike'] = false;
							$profile_data[$val]['canMayBe'] = false;
							$profile_data[$val]['like_url'] = null;
							$profile_data[$val]['hide_url'] = null;
							$profile_data[$val]['maybe_url'] = null;
						}
						
					if(($setFlag == "mutual_like" && $isAuthentic == true)||($mutualLikeFlag == true)){
						$profile_data[$val]['canMsg'] = true;
						$profile_data[$val]['msg_url'] = $this->generateMessageLink($user_id, $val);
					} else if($profile_data[$val]['isProfileAdCampaign'] == true)
					{
						$profile_data[$val]['canMsg'] = false;
						$profile_data[$val]['msg_url'] = null;
					}

					else {
						$profile_data[$val]['canMsg'] = false;
						$profile_data[$val]['msg_url'] = null;
						$profile_data[$val]['fname'] = str_repeat("x",strlen($profile_data[$val]['fname']));//	ucfirst(substr($profile_data[$val]['fname'] , 0 ,1)) .  str_repeat("x",strlen($profile_data[$val]['fname'])-1 );
					}
				}
			}
			foreach ($match_id_array as $match_id) {
				if ( in_array($match_id,$extra_params['repeat_likes'])) {
					$profile_data[$match_id]['has_liked_before'] =true;
				} else {
					$profile_data[$match_id]['has_liked_before'] = false;
				}
			}
			
		// Sending FB Mutual Friends Data
	if (Utils::$mutual_friends_enabled) {
		foreach ($match_id_array as $match_id) {
			if ($extra_params != null && isset($extra_params['mutual_friends_data']) && isset($extra_params['mutual_friends_data'][$match_id])) {
				$profile_data[$match_id]['fb_mf_count'] = $extra_params['mutual_friends_data'][$match_id]['fb_count'];
			} else {
				$profile_data[$match_id]['fb_mf_count'] = 0;
			}
		}
		
		// on viewing profile from conversation list send actual mutual friends data
		if ($usage == "profile" && count($match_id_array) == 1 && !$myProfile) {
			$user_fb = new UserFacebookDBO();
			$access_token = $user_fb->get_facebook_token($user_id);
			if ($access_token != null) {
				$fb = new fb($access_token);
				foreach ($match_id_array as $match_id) {
					if ($extra_params != null && isset($extra_params['fetch_mutual_friends_data']) && $extra_params['fetch_mutual_friends_data']) {
						$target_user_fb_id = $user_fb->get_facebook_id($match_id);
						if ($target_user_fb_id != null) {
							$mutual_friends = $fb->get_mutual_friends($user_id, $match_id, $target_user_fb_id);
							$profile_data[$match_id]['fb_mf_count'] = isset($mutual_friends['count']) ? $mutual_friends['count'] : 0;
							$profile_data[$match_id]['fb_mf'] = isset($mutual_friends['mutual_friends']) ? $mutual_friends['mutual_friends'] : array();
						}
					}
				}
			}
		}
	}
			
			if($userInfo[$user_id]['gender']=='M')
			{
				
				foreach ($likedIds as $id)
				{
					//echo $id;
				
					if($this->uuDBO->CheckIfFemaleWithNoPics($id))
					{
						//Create a flag here,also set the dummy image of the female according to the version of the user(male)
						
						$profile_data[$id]['photo_visibility_toast']=true;
					}
				}
			}
			
			
			
                return $profile_data;

	}

   public function CheckMinVersion($myversion,$version_to_be_checked_upon)
   {
	   	if($myversion>=$version_to_be_checked_upon)
	   	{
	   		return true;
	   	}
   	    return false;
   	
   }

		public function getMutualEventsData($user1, $ids, $limit=3){
		global $imageurl;
		$user_ids = implode(",", $ids);
		$result = array();
		$sql = " select t2.user2, substring_index(GROUP_CONCAT(datespot_hash_id SEPARATOR '|'), '|', 3) as datespots, substring_index(GROUP_CONCAT(image SEPARATOR '|'), '|', 3) ".
				" as images, substring_index(GROUP_CONCAT(name SEPARATOR '|'), '|', 3) as names from ".
				" (select t.user_id user1, t1.user_id user2, t1.datespot_hash_id, d.name, CONCAT('$imageurl' ,d.list_view_image) as image from".
				" ( select ue.datespot_hash_id, ue.user_id from user_event ue where ue.user_id = ?) t JOIN ".
				" ( select ued.datespot_hash_id, ued.user_id from user_event ued where ued.user_id in ($user_ids) and ued.user_id != ?) t1 ".
				" on t.datespot_hash_id=t1.datespot_hash_id JOIN datespots d on d.hash_id=t1.datespot_hash_id order by t1.user_id) t2 group by t2.user2";


		$mutualEvents = Query_Wrapper::SELECT($sql, array($user1,$user1), Query::$__slave);
		if($mutualEvents != null && count($mutualEvents) > 0){
			foreach($mutualEvents as $event){
				$result[$event['user2']] = array();
			//	$result[$event['user2']]['mutual_events'][] = array("name" => explode(",",$event['names'][]))
				$nameArr = explode("|", $event['names']);
				$imageArr = explode("|", $event['images']);
				$idArr = explode("|", $event['datespots']);
				$ctr = count($nameArr);
				for($i=0;$i<$ctr;$i++){
					$temp = array();
					$temp['name'] = $nameArr[$i];
					$temp['image'] = $imageArr[$i];
					$temp['datespot_hash_id'] = $idArr[$i];
					$result[$event['user2']][] = $temp;
				}
			}
		}
		return $result;
	}

	//get common favourites
	
	public function commonfavorites($myFavorites, $matchFavorites) {
		$Favorites = array();
		foreach ($myFavorites as $key => $val) {
			if(is_array($val) && $matchFavorites[$key]) {
				$Favorites[$key] = array_values(array_filter(array_intersect($val,$matchFavorites[$key])));
			}else {
				$Favorites[$key] = array();
			}
		}
		return $Favorites;
	}
	
	//get the activity dashboard for a user 
	public function getActivityDashboard($user_array = array()){
		$activityData = array();
		if(!empty($user_array)){
			$result = $this->uuDBO->getUserBucketData($user_array);
			foreach ($result as $val){
				$activityData[$val['user_id']] = $val;
			}
		}
		return $activityData;
	}
	
	public function getTodaysActivity($user_id){
		global $redis_db_userActivity;
		$activityKey = "u:ac:".$user_id;
		$this->redis->SELECT ($redis_db_userActivity);
		$activity = $this->redis->GET($activityKey);
		$this->redis->SELECT(0);
		if(isset($activity)) return intval($activity);
		else return 0 ;
	
	}
	
	/**
	 * returns true or false if a user is endorsing self
	 * @param unknown_type $data, $keyToVerify is like array ("user_id" =>1026)
	 */
	public function isSelfEndorsing($data, $keyToVerify){
		$result = $this->uuDBO->getUserDataWithPattern($data);
		$paramTocheck = array_keys($keyToVerify);
		$valueTocheck = array_values($keyToVerify);
		$key = $paramTocheck[0];
		$value= $valueTocheck[0];
		if(isset($result) && count($result)>0){
			foreach ($result as $val){
				$requiredSet[] = $val[$key];
			}
			if(in_array($value, $requiredSet)) return true;
		}

		
		return false;
	}

	public function getProfilePic($pic, $other_gender){
		if(isset($pic)){
			return $this->getImageFullPathFromDB($pic);
		}
		else
		{
			return $this->getDummyImage($other_gender);
		}
	}
	public function mergeAllInterestAndLikes($profile_data){

		$music = (isset($profile_data['music_favorites']))? array_values($profile_data['music_favorites']):array();
		$food = (isset($profile_data['food_favorites']))? array_values($profile_data['food_favorites']):array();
		$movies = (isset($profile_data['movies_favorites']))? array_values($profile_data['movies_favorites']):array();
		$sports = (isset($profile_data['sports_favorites']))? array_values($profile_data['sports_favorites']):array();
		$travel = (isset($profile_data['travel_favorites']))? array_values($profile_data['travel_favorites']):array();
		$books = (isset($profile_data['books_favorites']))? array_values($profile_data['books_favorites']):array();
		$preferences = (isset($profile_data['preferences']))? array_values($profile_data['preferences']):array();
		$others = (isset($profile_data['other_favorites']))? array_values($profile_data['other_favorites']):array();
		$withoutHashPref = array();
		if(!empty($preferences)){
			foreach ($preferences as $key => $val){
				$withoutHashPref[$key] = ltrim($val, '#');
			}
		}
		$mergedArray = array("all"=>array_unique(array_merge($withoutHashPref, $music, $books, $food, $movies, $sports, $travel,$others)),
							"favourite" => array_unique(array_merge($music, $food, $books, $movies, $sports, $travel,$others)),
							"hashTags"=>array_unique($preferences));
		return $mergedArray;
	}
	/**
	 * sets expiry of redis key
	 * @param unknown_type $key
	 * @param unknown_type $time in sec
	 */
	public function setExpiry($key, $time){
		$flag =  $this->redis->EXPIRE($key, $time);
	}

	public function setKeyAndExpire($key,  $time, $val){
		$this->redis->SETEX($key, $time, $val);
	}
	

	public function setKey($key, $val){
		$this->redis->SET($key, $val);
	} 

	public function ifKeyExists($key){
		return $this->redis->EXISTS($key);
	}

	public function deleteKey($key){
		return  $this->redis->DEL($key);
	}

	public function getAttributes($usage, $id_arrays, $user_id, $action = null, $user_comm_with = null) {
		//echo $user_id;
		//var_dump($action);
		//var_dump($id_arrays);
		global  $educations_arr, $industries_arr, $educations_subarr;

		$myprofile = new User();
		$myData = $myprofile->getUserDetails($user_id);
		$approvedPics = $myprofile->getApprovedPics($user_id);
		$isAnyApproved = (count($approvedPics) > 0)?1:0;

		$output = array ();
		$payment_plan = $_SESSION['plan'];
		if (count ( $id_arrays ) == 0)
		return $output;

		$ids = implode ( ',', $id_arrays );

		if($usage == 'messages_full_conversation' || $usage == 'messages'){

			$attributes = array();
			$nQuery = "SELECT fname, lname, gender, user_id, plan, status from user where user_id in (" . $ids .')';
			$res = $this->conn->Execute($nQuery);
			$names = $res->GetRows();

			/*			if($usage == 'messages_full_conversation'){
				foreach ($names as $row){
				if($row['user_id'] == $user_comm_with)
				$otherUserPaymentPlan = $row['plan'];
				}
				}*/
			$names_arr =array();
			$imgs = array();
			foreach ($names as $row){

				if(((isset($action[$row['user_id']]) && in_array(UserActionConstantValues::credit_used, $action[$row['user_id']])))||($payment_plan == 'unlimited')){
					$fname = $row['fname'] ;
					//	$lname = $row['lname'];
				}
				else {
					$fname  = ucfirst(substr($row['fname'] , 0 ,1)) .  str_repeat("x",strlen($row['fname'])-1 );
					//$lname = ucfirst(substr($row['lname'] , 0 ,1)) . str_repeat("x",strlen($row['lname'])-1 );
				}
				$names_arr[$row['user_id']]['name'] =$fname . ' '.$lname;
				$names_arr[$row['user_id']]['gender'] = $row['gender'];
				$names_arr[$row['user_id']]['status'] = $row['status'];

				if($row['user_id']!=$user_id){
					if($row['gender'] =='M'){
						/*if($isAnyApproved == 0){
							$imgs[$row['user_id']]  = $this->dummy_unapproved_male_image;
							}else*/
						$imgs[$row['user_id']]  =$this->dummy_male_image;
					}
					else{
						/*if( $isAnyApproved == 0){
							$imgs[$row['user_id']] = $this->dummy_unapproved_female_image;
							}else*/
						$imgs[$row['user_id']]  =$this->dummy_female_image;
					}
				}
				else{
					$imgs[$row['user_id']] = $row['gender'] =='M'? $this->dummy_male_image:$this->dummy_female_image;
				}
				$user_ids[] = $row['user_id'];
			}

			$sql = "SELECT user_id,thumbnail as name , blur_thumbnail FROM user_photo where user_id in (" . $ids.") AND is_profile='yes' AND status = 'active' ";
			$exec = $this->conn->Execute($sql);
			$image_names = $exec->GetRows();
			/*var_dump($image_names);
			 echo $this->imageurl;
			 echo $isAnyApproved;*/
			foreach ($image_names as $key=>$val){
				if( $isAnyApproved == 1)
				$imgs[$val['user_id']] = $this->imageurl.$val['name'];
				elseif($val['blur_thumbnail']!=null)
				$imgs [$val ['user_id']] = $this->imageurl.$val['blur_thumbnail'];

			}
			//var_dump($imgs);
			$attributes = array("names" => $names_arr, "images" => $imgs);
			return $attributes;


		}

		if ($usage == "likeMe") {
			$name_data = $this->conn->Execute ( "SELECT user_id, fname,gender,lname from user where user_id in ($ids) " );
			$image_data = $this->conn->Execute ( "SELECT user_id, thumbnail, blur_thumbnail from user_photo where is_profile='yes' and status='active' and user_id in ($ids)" );

			$name_data = $name_data->GetRows ();
			$image_data = $image_data->GetRows ();
			$output = array ();

			foreach ( $name_data as $val ) {

				$output [$val ['user_id']] ['id'] = $val ['user_id'];

				if((isset($action[$val['user_id']]) && in_array(UserActionConstantValues::credit_used, $action[$val['user_id']]))||($payment_plan == 'unlimited')){
					$output [$val ['user_id']] ['fname'] = $val['fname'] ;
					//$output [$val ['user_id']] ['lname'] = $val['lname'];
				}
				else{
					$output [$val ['user_id']] ['fname'] =ucfirst(substr($val['fname'], 0 ,1)).str_repeat("x",strlen($val['fname'])-1 );// $val ['fname'];
					//$output [$val ['user_id']] ['lname'] = ucfirst(substr($val ['lname'], 0 ,1)).str_repeat("x",strlen($val ['lname'])-1 );//$val ['lname'];
				}

				if($val['gender']=="M"){
					/*if( $isAnyApproved == 0){
						$output [$val ['user_id']] ['thumbnail'] = $this->dummy_unapproved_male_image;
						}else*/
					$output [$val ['user_id']] ['thumbnail'] =$this->dummy_male_image;
				}
				else{
					/*if($isAnyApproved == 0){
						$output [$val ['user_id']] ['thumbnail'] = $this->dummy_unapproved_female_image;
						}else*/
					$output [$val ['user_id']] ['thumbnail'] =$this->dummy_female_image;
				}
			}

			foreach ( $image_data as $val ) {
				if($isAnyApproved == 1)
				$output [$val ['user_id']] ['thumbnail'] = $this->imageurl.$val ['thumbnail'];

				else{
					if($val['blur_thumbnail']!=null)
					$output [$val ['user_id']] ['thumbnail'] = $this->imageurl.$val['blur_thumbnail'];
				}
			}

			return $output;
		}
		if($usage=="matches"){
			//	echo 'Himanshu is working on matches! Pls Hold. :)';


			/*echo $isAnyApproved;
			 var_dump($myData);*/
			$name_data = $this->conn->Execute ( "SELECT user.user_id, fname,gender,lname,user_lastlogin.last_login from user left join user_lastlogin
					on user.user_id=user_lastlogin.last_login where user.user_id in ($ids) order by field( user.user_id, $ids)" );

			$res  = $name_data->GetRows();
			//var_dump($res);
			$active_ids = array();
			foreach ($res as $r => $e){
				$active_ids[] = $e['user_id'];
			}
			//var_dump($res);
			//var_dump($active_ids);
			if(!empty($active_ids)){
				$ids = implode(',', $active_ids);
					
					
				$image_data = $this->conn->Execute ( "SELECT user_id, name, blur_image from user_photo where is_profile='yes' and status='active' and user_id in ($ids) order by field( user_id, $ids)" );
				$demo_data = $this->conn->Execute ("SELECT ud.user_id, FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age, re.name as religion, ud.caste, gc.name as city from user_demography ud LEFT JOIN religions re ON ud.religion= re.id LEFT JOIN geo_city gc on ud.stay_city_display = gc.id where ud.user_id in ($ids) order by field( user_id, $ids) ");
				$height_data =  $this->conn->Execute ("SELECT user_id, height from user_trait where user_id in ( $ids ) order by field( user_id, $ids )");
				//	$siblings_data = $this->conn->Execute ("SELECT user_id, no_of_brothers, no_of_sisters  from user_family where user_id in ($ids) order by field( user_id, $ids)");;
				$trustScore = $this->conn->Execute ("SELECT user_id, trust_score from user_trust_score where user_id in($ids ) order by field( user_id, $ids)");

				$greater_userIds = array();
				$lesser_userIds = array();
					
				$id_arrays = explode(',', $ids);
				foreach ($id_arrays as $val){
					($val>$user_id)?($greater_userIds[]=$val):($lesser_userIds[]=$val);
				}
					
				$sql5 = "SELECT user1, user2, mutual_connections from user_facebook_mutual_connections
											where (";
				if($lesser_userIds != null) $sql5.="(user1 = $user_id AND user2 in(". implode ( ',', $lesser_userIds )  ."))";

				if($lesser_userIds!= null && $greater_userIds!=null) $sql5.=" OR ";
				if($greater_userIds != null)	$sql5.=	" (user1 in(". implode ( ',', $greater_userIds ). ") AND user2 = $user_id)";
					
				$sql5.=")";
				//echo $sql5;
				$mConns = $this->conn->Execute($sql5);
					
				$name_data = $res;//name_data->GetRows ();
					
				$image_data = $image_data->GetRows ();
				$demo_data = $demo_data->GetRows ();
				$height_data = $height_data->GetRows ();
				//	$siblings_data = $siblings_data->GetRows ();
				$trustScore = $trustScore->GetRows ();
				$mutualConnections = $mConns->GetRows();

				$output = array ();
				//var_dump($name_data);
				foreach ( $name_data as $val ) {
					$output [$val ['user_id']] ['id'] = $val ['user_id'];
					$output [$val ['user_id']] ['last_active'] = Utils::humanTime($val ['last_login']);

					if(((isset($action[$val['user_id']]) && in_array(UserActionConstantValues::credit_used, $action[$val['user_id']])))||($payment_plan == 'unlimited')){
						$output [$val ['user_id']] ['fname'] = $val['fname'] ;
						//$output [$val ['user_id']] ['lname'] = $val['lname'];
					}
					else{
						$output [$val ['user_id']] ['fname'] =ucfirst(substr($val['fname'], 0 ,1)).str_repeat("x",strlen($val['fname'])-1 );// $val ['fname'];
						//$output [$val ['user_id']] ['lname'] = ucfirst(substr($val ['lname'], 0 ,1)).str_repeat("x",strlen($val ['lname'])-1 );//$val ['lname'];
					}

					User::getRefinedName($output [$val ['user_id']] ['fname']);

					$output [$val ['user_id']] ['name'] =$output [$val ['user_id']] ['fname']." ".$output [$val ['user_id']] ['lname'];
					//$output [$val ['user_id']] ['lname'] = ;
					if($val['gender']=="M"){
						/*if( $isAnyApproved == 0){
						 $output [$val ['user_id']] ['image_url'] = $this->dummy_unapproved_male_image;
						 }else*/
						$output [$val ['user_id']] ['image_url'] =$this->dummy_male_image;
					}
					else{
						/*if( $isAnyApproved == 0){
						 $output [$val ['user_id']] ['image_url'] = $this->dummy_unapproved_female_image;
						 }else*/
						$output [$val ['user_id']] ['image_url'] =$this->dummy_female_image;
					}
				}
					
					
				//var_dump($output); die;
				foreach ( $image_data as $val ) {
					if( $isAnyApproved == 1)
					$output [$val ['user_id']] ['image_url'] = $this->imageurl.$val ['name'];
					else{
						if($val['blur_image']!=null){
							//$output[$user_id]['blurSet'] = 1;
							$output [$val ['user_id']] ['image_url'] = $this->imageurl.$val['blur_image'];
						}
					}
				}
				foreach ( $demo_data as $val ) {
					//$output [$val ['user_id']] ['age'] =  floor ( (strtotime ( date ( 'Y-m-d' ) ) - strtotime ($val ['DateOfBirth'] )) / 31556926 );;
					$output [$val ['user_id']] ['age'] = $val['age'];
					$output [$val ['user_id']] ['religion'] = ucfirst($val ['religion']);
					$output [$val ['user_id']] ['caste'] = $val ['caste'];
					$output [$val ['user_id']] ['city'] = ucfirst($val ['city']);
				}
				foreach ( $height_data as $val ) {
					$output [$val ['user_id']] ['height'] = Utils::heightInchToFeet ( $val ['height']);
				}
				/*	foreach ( $siblings_data as $val ) {
				 $output [$val ['user_id']] ['no_of_brothers'] = $val ['no_of_brothers'];
				 $output [$val ['user_id']] ['no_of_sisters'] = $val ['no_of_sisters'];
				 }*/
				foreach ( $trustScore as $val ) {
					$output [$val ['user_id']] ['trust_score'] = $val ['trust_score'];
				}

				foreach ($mutualConnections as $val){
					if($val['user1'] == $user_id)
					$temp[$val['user2']] = 	(isset($val['mutual_connections'])?'Y':null);
					else
					$temp[$val['user1']] = 	(isset($val['mutual_connections'])?'Y':null);
				}

				foreach ($id_arrays as $val){
					$output [$val] ['mutual_connections'] = $temp[$val];
				}
				/*
				 foreach ($mutualConnections as $val){
				 $output [$val ['user_id']] ['mutual_connections'] = (isset($val['mutual_connections'])?'Y':null);
				 }
				 */



				/**
				 * Education data
				 */

				$sql_work = "SELECT * FROM user_work uw where user_id in ( $ids ) order by field( user_id, $ids )";
				$ex = $this->conn->Execute($sql_work);
				$workRows = $ex->GetRows();
				//	echo '<pre>';


				foreach ($workRows as $key=>$work){
					//	var_dump($work);
					//	$output [$val ['user_id']] ['income_end']
					if(intval($work['income_end']) >= 100){
						if(intval($work['income_end']) == 100){
							$output [$work ['user_id']] ['income_end']= '1 Crore';
							//$work['income_start'] .= 'LPA';
						}
						if(intval($work['income_end']) > 100){
							$output [$work ['user_id']] ['income_end']= '1 Crore+';
							//$work['income_start'] .= 'LPA';
						}
					}
					else $output [$work ['user_id']] ['income_end']= $work ['income_end'] .' LPA';

					if(intval($work['income_start']) >= 100){

						if(intval($work['income_start']) == 100){
							$output [$work ['user_id']] ['income_start']= '1 Crore';
							//$work['income_start'] .= 'LPA';
						}
						if(intval($work['income_start']) > 100){
							$output [$work ['user_id']] ['income_start']= '1 Crore+';
							//$work['income_start'] .= 'LPA';
						}
					}
					else $output [$work ['user_id']] ['income_start']= $work ['income_start'] . ' LPA';

					//EDUCATION section

					/*if($work['other_fields']!=null){
					 $edu = json_decode($work['other_fields'], true);
					 //var_dump($edu);

					 $sql_education = $this->conn->Prepare("SELECT name as education from courses where education_id =?");
					 $ex = $this->conn->Execute($sql_education, $edu['highest_education']['category']);
					 $rs = $ex->FetchRow();

					 $output [$work ['user_id']] ['highest_education']= $rs ['education'];
					 //	echo $output [$work ['user_id']] ['highest_education'];
					 $output [$work ['user_id']] ['working_area']= $work ['working_area'];
					 //echo $output [$work ['user_id']] ['working_area'];
					 $output [$work ['user_id']] ['industry']= $work ['industry'];
					 //echo $output [$work ['user_id']] ['industry'];
					 }
					 */
					$edu = json_decode($work['education_details'], true);
					$ind = json_decode($work['industry_details'], true);

					$edArr = $edu['education'][0];
					$arr = explode('-', $edArr);
					$cat = $arr[0];
					$sub_cat = $arr[1];


					if($educations_arr[$cat]!=null){
						if(isset($educations_subarr[$cat][$sub_cat])){
							$output [$work ['user_id']] ['highest_education'] = $educations_subarr[$cat][$sub_cat];
						}
						else{
							$output [$work ['user_id']] ['highest_education'] = $educations_arr[$cat];
						}
					}
					//$output [$work ['user_id']] ['highest_education']= $educations_arr[$edu['education'][0]];
					$output [$work ['user_id']] ['working_area']= $work ['working_area'];
					$output [$work ['user_id']] ['industry']= $industries_arr[$ind['industries'][0]];;

					/*$output [$work ['user_id']] ['highest_education']= $work ['highest_education'];
					 $output [$work ['user_id']] ['working_area']= $work ['working_area'];
					 $output [$work ['user_id']] ['industry']= $work ['industry'];*/
					/*
					 $other_education = array();
					 foreach ($edu['other'] as $key =>$val){
					 //var_dump($val); die;
					 $other_education[] = array("category" => $val['category'],
					 "sub_category" => $val['sub_category'],
					 "institute" => $val['institute'],
					 "specialization" => $val['specialization']
					 );

					 $categories[] = $val['category'];
					 $sub_categories[] = $val['sub_category'];
					 }
					 */

					/*echo '<pre>';
					 var_dump(count($categories));
					 var_dump($sub_categories);*/

					/*	if(count($categories)>0 && count($sub_categories)>0)
					 $sql_education = $this->conn->Prepare("SELECT education_id as category, id as sub_category, name as education from courses where education_id IN (?," . implode(',', $categories) .") and id IN (?," . implode(',', $sub_categories) .")");
					 else
					 $sql_education = $this->conn->Prepare("SELECT education_id as category, id as sub_category, name as education from courses where education_id = ? and id =?");
					 $res_ed = $this->conn->Execute($sql_education, array($edu['highest_education']['category'], $edu['highest_education']['sub_category']));
					 $educations = $res_ed->GetRows();

					 if(count($categories)>0)
					 $sql_ed = $this->conn->Prepare("SELECT name as highest_qualification from education_levels where id IN (?," . implode(',', $categories) .")");
					 else
					 $sql_ed = $this->conn->Prepare("SELECT name as highest_qualification from education_levels where id = ?");
					 $res_hed = $this->conn->Execute($sql_ed, array($edu['highest_education']['category']));
					 $highest_education = $res_hed->GetRows();

					 $education['education'] = $educations[0]['education'];
					 $education['highest_education'] = $highest_education[0]['highest_qualification'];
					 $education['college'] = $edu['highest_education']['institute'];
					 $education['specialization'] = $edu['highest_education']['specialization'];*/
					/*
					 $i =1;
					 foreach ($other_education as $key =>$val){
					 $other_education[$key]['category'] = $highest_education[$i]['highest_qualification'];
					 $other_education[$key]['sub_category'] = $educations[$i]['education'];
					 $i++;
					 }*/
				}
			}
			//var_dump($mutualConnections);
			/*	echo '<pre>';
			var_dump($output); die;*/
			return $output;
		}
	}


	public function getFbConnectionCount($user_id){
		$sql = $this->conn->Prepare("select connection from user_facebook where user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->FetchRow();
		return  $res['connection'];
	}

	public  function canCommunicate($actions, $payment_mode){
		//	echo 'here';
		if(in_array(UserActionConstantValues::rejected_or, $actions))
		return  false;
		//echo 'here';

		if(in_array(UserActionConstantValues::can_communicate, $actions)|| $payment_mode == 'unlimited'	|| in_array(UserActionConstantValues::credit_used, $actions) )
		return true;
		//	echo 'here';

		return false;
	}

	/*	public function getProfileStatus($userId){
		$sql = $this->conn->Prepare("SELECT status FROM user WHERE user_id = ?");
		$ex = $this->conn->Execute($sql, array($userId));
		$rs = $ex->FetchRow();

		return $rs;
		}*/
	/*
	 * get the type of both the user and then check the interactivity in rule engine
	 */
	public static function canMessage($user1, $user2) {

		/*
		 * $query = "select * from user_to_user_permissions where attribute = 'reply_to_messages' where " .$type ." = "; $exec = $this->conn-Execute($query); $result = $exec->GetRows();
		 */
		return true; 
	}
	public static function getMatchCount($user_id) {
		$key = "user:match:$user_id";
		// $usrQ = new userQueue();
		// return $usrQ->getMatchesCount($key);
	}

        public function getProfilePicForUser($user_id) {
		$sql = $this->conn->Prepare("select photo_id,name from user_photo where is_profile='yes' and user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->FetchRow();
		return  $res;
	}
        
        public function getSystemProfilePicForUser($user_id) {
		$sql = $this->conn->Prepare("select photo_id,name from user_photo where system_profile ='yes' and user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->FetchRow();
		return  $res;
	}

        
	public function getNamenPic($user_id){
		$sql = $this->conn->Prepare("select u.fname as name,up.thumbnail,u.gender from user u LEFT JOIN user_photo up on u.user_id=up.user_id and up.is_profile='yes' where u.user_id = ?");
		$ex = $this->conn->Execute($sql, array($user_id));
		$res = $ex->FetchRow();
		return  $res;
	}
	
	public function getUserStatus($user_id){
		$sql = $this->conn->Prepare("select status from user where user_id= ?");
		$ex= $this->conn->Execute($sql,$user_id);
		$res=$ex->FetchRow();
		return $res;
	}
	
	public function swap_profile_pic(&$output) {
		global $imageurl,$redis ;
		$i=0;
		$user_flag=$redis->hgetall('profile_pic_shuffling');
		$match_id=$output['my_data']['user_id'];
		$match_gender=$output['my_data']['gender'];
		if ($match_gender=='M') {
			return ;
		}
		
		for($i=0;$i<count($output['data']);$i++)
		{   $flag=0;
			$user_id=$output['data'][$i]['user_id'];
			$user_id=(int)$user_id;
			$bucket=$this->uuDBO->getBucket($user_id);
			$rem=$user_id%10;
			
			foreach($user_flag as $key=>$value)
			{
				if(($bucket==$key)&&$rem<$value)
				{
					$flag=1;
				}
			}
			if($flag==0) 
			{
				continue;
			}
			$actual=$output['data'][$i]['profile_data']['profile_pic'];
			//$actual=str_replace($imageurl,'',$actual);
			
			$count_photos=count($output['data'][$i]['profile_data']['other_pics'])+1;
			//var_dump($count_photos);
			$random_index=rand(0,$count_photos-1);
			//var_dump($random_index);
			if($random_index==0)
			{
				$profile=$actual;
			}
			else 
			{
				$profile=$output['data'][$i]['profile_data']['other_pics'][$random_index-1];
				
				$output['data'][$i]['profile_data']['profile_pic']=$profile;
				$output['data'][$i]['profile_data']['other_pics'][$random_index-1]=$actual;
			}
			

			$actual=str_replace($imageurl,'',$actual);
			$profile=str_replace($imageurl,'',$profile);
			
			$this->uuDBO->log_profile_pic($user_id,$match_id,$profile,$actual,$random_index);
			
		}
	}
	
}

/*
 $educations_arr = $educations;
 $educations_subarr = $educationsSub;
 $religions_arr = $religions;
 $interests_arr = $interests;
 $industries_arr = $industries;
 $tongue_arr = $tongues;*/
