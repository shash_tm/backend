<?php 

require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/tbconfig.php";
require_once dirname ( __FILE__ ) . "/../TrustBuilder.class.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";


try {
if(isset($_REQUEST['caller_number'])){
	$data = $_REQUEST;
	if($data['is_verified'] == '1'){
		$status = 'active';
	}elseif($data['caller_status'] == 'NotConnected'){
		$status = 'notpick';
	}else{
		$status = 'rejected';
	}
	
	$conn->Execute($conn->Prepare("update user_phone_number set is_ndnc=?,is_verified = ?,time = ?,date = ?,status = ? where user_number = ?"),
			array($data['is_ndnc'],$data['is_verified'],$data['time'],$data['date'],$status,$data['caller_number']));
	
	if($status == 'notpick'){
		$conn->Execute($conn->Prepare("update user_phone_number set number_of_trials = (number_of_trials - 1) where user_number = ?"),
				array($data['caller_number']));
	}
	
	$conn->Execute($conn->Prepare("update user_phone_log set caller_status = ?, caller_duration = ?, is_ndnc = ?, is_verified = ?, time = ?, date = ?, request_recieved = ? where call_id = ?"),
			array($data['caller_status'],$data['call_duration'],$data['is_ndnc'],$data['is_verified'],$data['time'],$data['date'],$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],$data['call_id']));
	
	if($data["is_verified"] == '1'){
		
		$rs = $conn->Execute($conn->prepare("select user_id from user_phone_number where user_number=?"),array($data['caller_number']));
		$session = $rs->FetchRow();
		
		$trustbuilder = new TrustBuilder($session['user_id']);
		$trustbuilder->updateMobileTrustScore($data['caller_number']);
		$trustbuilder->creditPoint($tbNumbers['credits']['mobile'],'mobile_verified',$tbNumbers['validity']['mobile']);
		
		$status = functionClass::getUserStatus($session['user_id']);
		
		if($status == 'non-authentic'){
			if($trustbuilder->authenticateUser()){
				functionClass::setSessionStatus('authentic');
			}
		}
	}
	
	
	print_r(json_encode(array("response"=>"success")));
}
}catch (Exception $e){
// 	echo $e->getMessage();
	print_r(json_encode(array("response"=>"fail")));
}
?>