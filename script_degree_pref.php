<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	$isUpdate = false;
	$options = getopt("u:");
	if(isset($options["u"]) && $options["u"]=='yes') {
		$isUpdate = true;
	}
	
	$degree_arr = array("1"=>1,"2"=>10,"3"=>10,"4"=>2,"5"=>3,"6"=>4,"7"=>10,"8"=>5,"9"=>10,"10"=>6,"11"=>10,"12"=>7,"13"=>3,"14"=>10,
			"15"=>8,"16"=>10,"17"=>9,"18"=>6,"19"=>10,"20"=>21,"21"=>21,"22"=>19,"23"=>19,"24"=>11,"25"=>12,"26"=>13,"27"=>19,"28"=>14,"29"=>19,
			"30"=>15,"31"=>19,"32"=>16,"33"=>12,"34"=>19,"35"=>17,"36"=>19,"37"=>18,"38"=>15,"39"=>19,"40"=>21,"41"=>20,"42"=>21);
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	//$sql = $conn->Execute("select user_id,education from user_preference_data where education is not NULL");
	$sql = "select user_id,education from user_preference_data where education is not NULL ";
	
	$rs = $conn->Execute($sql);
	if ($rs){
		while ($arr = $rs->FetchRow()) {
			$new_degree = array();
			$old_degree = $arr["education"];
			echo "old degree = ".$old_degree."\n";
			$degree = explode(',',$old_degree);
			foreach ($degree as $deg) {
				if(!in_array($degree_arr[$deg],$new_degree) && $degree_arr[$deg]!='') {
					$new_degree[] = $degree_arr[$deg];
				}
			}
			$newDeg = implode(',',$new_degree);
			if($newDeg == '') {
				$newDeg = NULL;
			}
			echo "new degree = ".$newDeg."\n";
			if($isUpdate) {
				$conn->Execute($conn->prepare("update user_preference_data set education=? where user_id=?"),array($newDeg,$arr["user_id"]));
				echo "updated \n";
			}
		}
	}
	
// 	$data = $sql->GetRows();
	
// 	foreach($data as $key=>$value){
// 		$new_degree = array();
// 		$old_degree = $data[$key]["education"];
// 		echo "old degree = ".$old_degree."\n";
// 		$degree = explode(',',$old_degree);
// 		foreach ($degree as $deg) {
// 			if(!in_array($degree_arr[$deg],$new_degree) && $degree_arr[$deg]!='') {
// 				$new_degree[] = $degree_arr[$deg];
// 			}
// 		}
// 		$newDeg = implode(',',$new_degree);
// 		if($newDeg == '') {
// 			$newDeg = NULL;
// 		}
// 		echo "new degree = ".$newDeg."\n";
// 		if($isUpdate) {
// 			$conn->Execute($conn->prepare("update user_preference_data set education=? where user_id=?"),array($newDeg,$data[$key]["user_id"]));
// 			echo "updated \n";
// 		}
// 	}
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}

?>