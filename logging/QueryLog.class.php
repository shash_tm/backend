<?php 

class QueryLog {
	
	private function getFile($name = '') {
		date_default_timezone_set('UTC');
		$suffix = intval((intval(date('i'))/5))*5;
		if($name == "rankLog") {
			$filename = "rank_log_".date('Y-m-d')."_".date('H')."_".$suffix;
		}else {
			$filename = "query_log_".date('Y-m-d')."_".date('H')."_".$suffix;
		}
		$fullPath = dirname ( __DIR__ )."/action_log/".$filename.".txt";
		return $fullPath;
	}
	
	public function queryLogData($data,$current_timestamp = null){
                $current_timestamp = ($current_timestamp == null ? date('Y-m-d H:i:s') : $current_timestamp);
		$filename = $this->getFile();
		$str = '';
		date_default_timezone_set('UTC');
		foreach ($data as $val){
			
			// user_id
			if(isset($val['user_id'])){
				if($val['user_id']== "undefined" || $val['user_id']=='')
					$val['user_id'] = '\N';
				else {
					$val['user_id'] = intval($val['user_id']);
				}
			}else{
				$val['user_id'] = '\N';
			}
			
			// query
			$val['query'] = str_replace("\n", '', $val['query']);
			$val['query'] = str_replace("\t", '', $val['query']);
			$val['query'] = substr($val['query'], 0, 65535);
			//$val['query'] = json_encode($val['query']);
			
			//result set
			if(!isset($val['result'])) {
				$val['result'] = '\N';
			}else {
				$val['result'] = substr($val['result'], 0, 65535);
			}
			
			// isLikeQuery
			if(!isset($val['isLikeQuery'])) {
				$val['isLikeQuery'] = '\N';
			}else {
				$val['isLikeQuery'] = intval($val['isLikeQuery']);
			}
			
			// countToFetch
			if(!isset($val['countToFetch'])) {
				$val['countToFetch'] = '\N';	
			}else {
				$val['countToFetch'] = intval($val['countToFetch']);
			}
			
			// countFetched
			if(!isset($val['countFetched'])) {
				$val['countFetched'] = '\N';
			}else {
				$val['countFetched'] = intval($val['countFetched']);
			}
                        
                        // recommender
			if(!isset($val['recommender'])) {
                            // Default engine is "RecommendationEngine"
                            $val['recommender'] = 'RecommendationEngine';
			}
							
			$str = $str.$val['user_id'].";".$val['query'].";".$val['result'].";".$current_timestamp.";".$val['isLikeQuery'].";".$val['countToFetch'].";".$val['countFetched'].";".$val['recommender']."end-line";

		}
		
		file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
	}
	
	public function rankLogData($data,$current_timestamp = null){
                $current_timestamp = ($current_timestamp == null ? date('Y-m-d H:i:s') : $current_timestamp);
		$filename = $this->getFile("rankLog");
		$str = '';
		date_default_timezone_set('UTC');
		foreach ($data as $val){
				
			// user1
			if(isset($val['user1'])){
				if($val['user1']== "undefined" || $val['user1']=='')
					$val['user1'] = '\N';
				else {
					$val['user1'] = intval($val['user1']);
				}
			}else{
				$val['user1'] = '\N';
			}
			// user2
			if(isset($val['user2'])){
				if($val['user2']== "undefined" || $val['user2']=='')
					$val['user2'] = '\N';
				else {
					$val['user2'] = intval($val['user2']);
				}
			}else{
				$val['user2'] = '\N';
			}	
			// query
				
			//result set
			if(!isset($val['result'])) {
				$val['result'] = '\N';
			}
				
			// isLikeQuery
			if(!isset($val['isLikeQuery'])) {
				$val['isLikeQuery'] = '\N';
			}else {
				$val['isLikeQuery'] = intval($val['isLikeQuery']);
			}

			//religionScore 
			// HACK: mutualFriendsScore as religionScore 
			$val['religionScore'] = isset($val['mutualFriendsScore']) ? $val['mutualFriendsScore'] : '\N'; 
			
			// HACK: maritalStatusScore as tmSelectStringRank score 
			$val['maritalStatusScore'] = isset($val['tmSelectStringRank']) ? $val['tmSelectStringRank'] : '\N'; 
			
			//locationScore
			if(!isset($val['locationScore'])) {
				$val['locationScore'] = '\N';
			}
			
			// HACK: incomeScore as nriStringRank score 
			$val['incomeScore'] = isset($val['nriStringRank']) ? $val['nriStringRank'] : '\N'; 
			
			//educationScore
			if (!isset($val['educationScore']))
				$val['educationScore'] = '\N';
			
			//likeScore
			$val['likeScore'] = '\N';
			
			//deviationScore
			$val['deviationScore'] = '\N';
			
			//totalScore
			if(!isset($val['totalScore'])) {
				$val['totalScore'] = '\N';
			}
			
			$val['is_match_psycho_filled'] = '\N';
			$val['is_my_psycho_filled'] = '\N';
			$val['trust_score'] = '\N';
			$val['maybe_score'] = '\N';
			if(!isset($val['piScore'])) {
				$val['piScore'] = '\N';
			}else {
				$val['piScore'] = floatval($val['piScore']);
			}
			if(!isset($val['activityScore'])) {
				$val['activityScore'] = '\N';
			}else {
				$val['activityScore'] = floatval($val['activityScore']);
			}
			if(!isset($val['availabilityScore'])) {
				$val['availabilityScore'] = '\N';
			}else {
				$val['availabilityScore'] = floatval($val['availabilityScore']);
			}
			if(!isset($val['likeFlag'])) {
				$val['likeFlag'] = '\N';
			}else {
				$val['likeFlag'] = intval($val['likeFlag']);
			}
			
			if(!isset($val['pi'])) {
				$val['pi'] = '\N';
			}else {
				$val['pi'] = floatval($val['pi']);
			}
			
			//$val['countFetched'] = '\N';
			
			if(!isset($val['bucket'])) {
				$val['bucket'] = '\N';
			}else {
				$val['bucket'] = intval($val['bucket']);
			}
			//$val['ABtype'] = '\N';
                        
                        // recommender
			if(!isset($val['recommender'])) {
                            // Default engine is "RecommendationEngine"
                            $val['recommender'] = 'RecommendationEngine';
			}
				
			$str = $str.$val['user1'].";".$val['user2'].";".$val['religionScore'].';'.$val['maritalStatusScore'].';'.$val['locationScore'].';'.$val['incomeScore'].';'.$val['educationScore'].';'.$val['likeScore'].';'.$val['deviationScore'].';'.$val['totalScore'].';'.$current_timestamp.';'.$val['is_match_psycho_filled'].";".$val['is_my_psycho_filled'].";".$val['trust_score'].";".$val['maybe_score'].";".$val['likeFlag'].";".$val['availabilityScore'].";".$val['activityScore'].";".$val['piScore'].";".$val['pi'].";".$val['bucket']."end-line";
	
		}
	
		file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
	}
}
?>