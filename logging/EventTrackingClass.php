<?php

//require_once dirname ( __FILE__ ) . "/../include/function.php";
//require_once dirname ( __FILE__ ) . "/../include/config.php";
//require_once dirname ( __FILE__ ) . "/../abstraction/query.php";

class EventTrackingClass {
	
	private function getFile() {
		date_default_timezone_set('UTC');
// 		if(!is_dir(dirname ( __DIR__ )."/action_log")) {
// 			mkdir(dirname ( __DIR__ )."/action_log", 0777);
// 		}
		$minute=intval((intval(date('i'))/5))*5;
		$filename = "action_log_".date('Y-m-d')."_".date('H')."_".$minute;
		$fullPath = dirname ( __DIR__ )."/action_log/".$filename.".txt";
		return $fullPath;
	}

	public function logActions($data){
		//global $conn;
		$filename = $this->getFile();
		$str = '';
		date_default_timezone_set('UTC');
		foreach ($data as $val){

			if(isset($val['event_type'])) {
				if($val['event_type'] == "undefined" ||$val['event_type'] == "null" || $val['event_type']=='') {
					$val['event_type'] = '\N';
				}
			}else {
				$val['event_type'] = '\N';
			}
			
				
			if($val['source'] == "web"){
				$isMobile = $this->getAgent();
				if($isMobile == 1) $val['source'] = "mobile_web";
			}
			if(isset($val['event_info'])) {
				if($val['event_info'] == "undefined" ||$val['event_info'] == "null") {
					$val['event_info'] = '\N';
				}else {
					$val['event_info']=str_replace(array("\n","\r"), '', $val['event_info']);
					$val['event_info'] = json_encode($val['event_info']);

				}
			}else {
				$val['event_info'] = '\N';
			}
			
			if(isset($val['event_status'])){
				$val['event_status'] = substr($val['event_status'],0,128);
				$val['event_status']=str_replace(array("\n","\r"), '', $val['event_status']);
				if($val['event_status']== "undefined" || $val['event_status']=='')
					$val['event_status'] = '\N';
			}else {
				$val['event_status'] = '\N';
			}
			if(isset($val['admin_id'])){
				if($val['admin_id']== "undefined" || $val['admin_id']=='' || $val['admin_id'] == 'null')
					$val['admin_id'] = '\N';
				else {
					$val['admin_id'] = intval($val['admin_id']);
				}
			}else{
				$val['admin_id'] = '\N';
			}
			if(isset($val['user_agent'])) {
				$val['user_agent_n']=$val['user_agent'];
			} else {
				$val['user_agent_n']=$_SERVER['HTTP_USER_AGENT']; 
			}
			
			if(isset($val['time_taken'])) {
				if($val['time_taken']=='undefined' || $val['time_taken']=='' || $val['time_taken']=='null') {
					$val['time_taken'] = '\N';
				}else {
					$val['time_taken'] = floatval($val['time_taken']);
					if(is_float($val['time_taken'])) {
					     $val['time_taken'] = round($val['time_taken'], 2);
					}else { $val['time_taken'] = '\N'; }
				}
			}else {
				$val['time_taken'] = '\N';
			}
			
			if(isset($val['user_id'])){
				if($val['user_id']== "undefined" || $val['user_id']=='')
					$val['user_id'] = '\N';
				else {
					$val['user_id'] = intval($val['user_id']);
					if(!is_int($val['user_id'])) {
                                            $val['user_id'] = 0;
                                        }
				}
			}else{
				$val['user_id'] = '\N';
			}
		
			if(!isset($val['source'])) {
				$val['source'] = '\N';
			}
			if(!isset($val['device_id'])) {
				$val['device_id'] = '\N';
			}
			if(!isset($val['sdk_version'])) {
				$val['sdk_version'] = '\N';
			}
			if(!isset($val['os_version'])) {
				$val['os_version'] = '\N';
			}
			if(!isset($val['activity'])){
				$val['activity'] = '\N';
			}

			$ipAddress = $_SERVER['REMOTE_ADDR'] ;
			if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
				$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
			}
			//$str = $str.$val['user_id']."\t".$val['user_agent_n']."\t".'\N'."\t".$val['activity']."\t".$val['event_type']."\t".$val['time_taken']."\t".$val['event_status']."\t".$val['event_info']."\t".$val['source']."\t".$val['admin_id']."\t".date('Y-m-d H:i:s')."\t".$val['device_id']."\t".$val['sdk_version']."\t".$val['os_version']."\n";
			
			$str = $str.$val['user_id']."\t".$val['user_agent_n']."\t".$ipAddress."\t".$val['activity']."\t".$val['event_type']."\t".$val['time_taken']."\t".$val['event_status']."\t".$val['event_info']."\t".$val['source']."\t".$val['admin_id']."\t".date('Y-m-d H:i:s')."\t".$val['device_id']."\t".$val['sdk_version']."\t".$val['os_version']."\n";
			//$arr[] = array($val['user_id'],$val['user_agent_n'],$_SERVER['REMOTE_ADDR'], $val['activity'], $val['event_type'], $val['time_taken'], $val['event_status'], $val['event_info'],  $val['source'], $val['admin_id'], $val['device_id'], $val['sdk_version'], $val['os_version']);
		}
		
		file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
		
		//if($arr != null){
			//Query::BulkINSERT($sql, $arr);
		//}
	}
	
	public function getAgent(){
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		return  (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)));
	}
	
}

?>
