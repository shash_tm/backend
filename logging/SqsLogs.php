<?php
/*
 * Author Raghav
 * This file fetches the logs files from S3 , the names and paths of which are fetched from
 * SQS Polling. The files after being downloaded from the S3 are decompressed and a Load infile
 * query is executed on the reporting server to ingest the data into the MYSQL server.
 * Also there is a function which sends a report , about how many log files are being dumped from each of the server.
 * If the count is less than the expected count , then admin is alerted.
 */

require_once  dirname(__FILE__) . '/../include/aws/aws-autoloader.php';
require_once  dirname(__FILE__) . '/../include/config.php';
require_once  dirname(__FILE__) . '/../include/Utils.php';
require_once  dirname(__FILE__) . '/../include/config_log.php';
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";

use Aws\Sqs\SqsClient;
use Aws\Ec2\Ec2Client;

class sqsLogs
{

    private $sqsClient=null;
    private $sqsUrl=null;
    private $queueName=null;
    private $bucketName=null;
    private $fptr=null;
    private $key=null;
    private $secret=null;
    private $ec2_client=null;
    private $reporting_array=null;
    private $smarty=null;
    private $samplestring;
    private $_ssh2;
    private $env=null;

    function __construct ()
    {
        global $config,$smarty;
        $key = $config['amazon_s3']['access_key'];
        $secret = $config['amazon_s3']['secret_key'];
        $this->key=$key;
        $this->secret=$secret;
        $this->queueName=$config['amazon_s3']['queue_name'];
        //$this->queueName='LogsQueueDev';
        $this->fptr=fopen("/tmp/sqsLogsHandle.txt", 'c');
        $this->reporting_array=array();
        $this->smarty=$smarty;
        $this->env='';
    }


    public function isDevorT1()
    {
        global $baseurl;
        if(strpos($baseurl, 'dev')!==false)
        {
            return true;
        }
        if(strpos($baseurl, 't1')!==false)
        {
            return true;
        }
        return false;
    }
    public function instantiate($type)
    {

        if($type=='reporting') {
            if(!$this->isDevorT1())
            {
                $this->ec2_client = Ec2Client::factory(array(
                    'key' => "$this->key",
                    'secret' => "$this->secret",
                    'region' => 'ap-southeast-1',
                    "version" => '2013-10-15'

                ));
                $this->reporting();
                return;
            }
            echo 'Reporting not allowed on dev and t1';
            return;
        }
        $isRunning=$this->isScriptRunning();
        if($isRunning==0) {
            $this->sqsClient = SqsClient::factory(array(
                'key' => "$this->key",
                'secret' => "$this->secret",
                'region' => 'ap-southeast-1',
                "version" => '2012-11-05'

            ));
            $this->ec2_client = Ec2Client::factory(array(
                'key' => "$this->key",
                'secret' => "$this->secret",
                'region' => 'ap-southeast-1',
                "version" => '2013-10-15'

            ));
            $this->fetchFiles();
        }
        else {
            //Take no action for now
            return;
        }
    }

    public function insertIntoCheckpoint($filename,$type,$forDate,$filepath)
    {
        $param_array=array($filename,$type,$this->fetchServerHost($filename),$forDate,$filepath);
        $sql="insert into sqsLogsCheckpoint(filename,log_type,hostname,fordate,s3_file_path) values (?,?,?,?,?)";
        $insert_id = Query_Wrapper::INSERT($sql, $param_array, 'sqsLogsCheckpoint' );
        return $insert_id;
    }

    public function fetchServerHost($filename)
    {
        //Fetches server host from filename
        $filename=basename($filename);
        $file=explode('_ip-',$filename);
        $file=explode('.',$file[1]);
        $ip=(str_replace('-','.',$file[0]));
        if(ip2long($ip)){
            //Ip is valid
            return $ip;
        }
        else {
            trigger_error("Cannot convert hostname to ip address",E_USER_WARNING);
            return null;
        }

    }

    public function readFromCheckPoint($server_host)
    {
        $isttime = date ( 'Y-m-d H:00');
        $start_time=date('Y-m-d H:i', strtotime('-2 hour', strtotime($isttime)));
        $end_time=date('Y-m-d H:i', strtotime('-1 hour', strtotime($isttime)));
        $sql="select count(*) as cu from sqsLogsCheckpoint where hostname=? and fordate>=? and fordate<=?";
        $result=Query_Wrapper::SELECT($sql, array($server_host,$start_time,$end_time), Query::$__slave,true);
        return $result['cu'];
    }

    private function exitScript()
    {
        fclose($this->ptr);
        exit;
    }

    private function isScriptRunning() {
        global $baseurl;
        $fptr=$this->fptr;
        $isLocked=-1;
        if (!flock($fptr, LOCK_EX|LOCK_NB, $wouldblock)) {
            if ($wouldblock) {
                echo "already locked";
                $isLocked = 1;
            }
            else {
                echo "could not lock";
            }
        }
        else {
            echo "obtain lock";
            $isLocked = 0;
            if(flock($fptr,LOCK_EX) == false){
                Utils::sendEmail("shashwat@trulymadly.com,raghav@trulymadly.com", "admintm@trulymadly.com", "Unable to acquire lock for SQSLogs.php $baseurl", "" );
                throw new Exception("unable to acquire lock");

            }else{
                echo "lock acquired";
            }
        }
        return $isLocked;
    }

    public function reporting()
    {
        global $baseurl;
        $now=time();
        $servers=$this->fetchServers();
        if(count($servers)>0) {
            foreach ($servers as $server) {
                $count = $this->readFromCheckPoint($server['Hostname']);
                if ($now - $server['LaunchTime'] > 3600) {
                    //It is running for more than one hour
                    //Since 3 files are being dumped every 5 minutes from each server, the total count should be 12 from each server
                    $this->detectProblem($count, $server['Hostname']);
                } else {
                    $this->detectProblem($count, $server['Hostname'], false, $server['LaunchTime']);

                }
            }
            $is_problem = false;
            $subject = '';
            foreach ($this->reporting_array as $item) {
                if ($item['is_problem'] == true) {
                    $is_problem = true;
                }
            }
            if ($is_problem) {
                $subject = 'Alert: SQS logs';
            } else {
                $subject = 'Success: SQS logs';
            }
            $file = dirname(__FILE__) . "/../templates/SqsLogs.tpl";
            $this->smarty->assign("all_data", $this->reporting_array);
            $data = $this->smarty->fetch($file);
            Utils::sendEmail('raghav@trulymadly.com,shashwat@trulymadly.com', 'admintm@trulymadly.com', $subject, $data.PHP_EOL.$baseurl, true);
        }
        else{
            //throw some error by informing in some way
        }
    }

    public function detectProblem($count,$server_host,$one_hour=true,$launch_time=null)
    {
        $admin=false;
        if($server_host=='172.31.30.84') {
            $admin=true;
        }
        $expected_count=39;
        if(!$one_hour) {
            $expected_count=((time()-$launch_time)/3600)*$expected_count;//
        }
        $isproblem=false;
        if($count<$expected_count&&!$admin){
            $isproblem=true;
        }
        array_push($this->reporting_array,array("expected_count"=>$expected_count,"count"=>$count,"is_problem"=>$isproblem,"hostname"=>$server_host));
    }

    public function fetchServers()
    {
        //This function fetches private addresses of the server under our loadbalacer
        if($this->ec2_client!=null)
        {
            $result = $this->ec2_client->DescribeInstances(array(
                'Filters' => array(
                    array('Name' => 'tag-value', 'Values' => array('www_prod_spot_launch_config','www_prod_launch_config','admin'))
                )
            ));
            $list_servers=array();
            foreach($result['Reservations'] as $item) {
                foreach($item['Instances'] as $instance) {

                    $launch_time=strtotime($instance['LaunchTime']);
                    $private_address=$instance['PrivateIpAddress'];
                    array_push($list_servers,array("LaunchTime"=>$launch_time,"Hostname"=>$private_address));

                }

            }
            return $list_servers;
        }
        else{
            return null;
        }


    }

    private function decompressFile($file_name)
    {
        // Raising this value may increase performance
        $buffer_size = 4096; // read 4kb at a time
        $out_file_name = str_replace('.gz', '', $file_name);

        // Open our files (in binary mode)
        $file = gzopen($file_name, 'rb');
        $out_file = fopen($out_file_name, 'wb');

        // Keep repeating until the end of the input file
        while (!gzeof($file)) {
            // Read buffer-size bytes
            // Both fwrite and gzread and binary-safe
            fwrite($out_file, gzread($file, $buffer_size));
        }

        // Files are done, close files
        fclose($out_file);
        gzclose($file);

        return $out_file_name;


        //It should be a compressed gz file
        //The return of the function will be the filename of the text file
        //var_dump(explode('.',$filename)[0]);die;
        $file_without_extension=explode('.',$filename)[0].'.'.explode('.',$filename)[1];
        //touch($file_without_extension);
        $extension=explode('.',$filename)[1];

        $gz = gzopen($filename, 'rb');
        if (!$gz) {
            throw new \UnexpectedValueException(
                'Could not open gzip file'
            );
        }
        $dest = fopen($file_without_extension, 'wb');
        //echo $file_without_extension;die;
        if (!$dest) {
            gzclose($gz);
            throw new \UnexpectedValueException(
                'Could not open destination file'
            );
        }
        echo $file_without_extension;die;
        return $file_without_extension;
    }

    private function fetchVideoData()
    {
        $sql="select min(video_id),u.user_id as myid,min(last_updated_row) as mytime from user_video uv join user u on u.user_id=uv.user_id where uv.status='active' and
            u.gender='M' group by uv.user_id order by uv.user_id
        ";
        $str='';
        $result=Query_Wrapper::SELECT($sql, null, Query::$__liveReporting,true);
        foreach($result as $item) {
            $video_date=$item['mytime'];
            $user_id=$item['myid'];
            $like_before="select count(*) as mycount from user_like where timestamp>'2016-09-01' and timestamp<'$video_date' and user2=$user_id";
            $like_before=Query_Wrapper::SELECT($like_before, null, Query::$__liveReporting,true);
            $like_before=$like_before['mycount'];


            $hide_before="select count(*) as mycount from user_hide where timestamp>'2016-09-01' and timestamp<'$video_date' and user2=$user_id";
            $hide_before=Query_Wrapper::SELECT($hide_before, null, Query::$__liveReporting,true);
            $hide_before=$hide_before['mycount'];

            $like_after="select count(*) as mycount from user_like where  timestamp>'$video_date' and user2=$user_id";
            $like_after=Query_Wrapper::SELECT($like_after, null, Query::$__liveReporting,true);
            $like_after=$like_after['mycount'];

            $hide_after="select count(*) as mycount from user_hide where  timestamp>'$video_date' and user2=$user_id";
            $hide_after=Query_Wrapper::SELECT($hide_after, null, Query::$__liveReporting,true);
            $hide_after=$hide_after['mycount'];

            $str=$str.$user_id.','.$like_before.','.$hide_before.','.$like_after.','.$hide_after.','.$video_date.PHP_EOL;



        }
        file_put_contents('video_data.csv',$str);
    }


    private function makeDateFromPath($filepath)
    {
        $path_array=explode('/',$filepath);
        $year=$path_array[2];
        $month=$path_array[3];
        $date=$path_array[4];
        $hour=$path_array[5];
        $minute=explode('_',$path_array[6])[0];
        return  $year.'/'.$month.'/'.$date.' '.$hour.':'.$minute;
    }

    /*
    public function testssh($source_filename,$dest_path)
    {

        $command="aws s3 cp s3://$source_filename $dest_path";
        //echo "$command";

        if (!function_exists("ssh2_connect")) die("function ssh2_connect doesn't exist");
        if(ssh2_auth_pubkey_file($this->_ssh2, 'root', '/code_git/raghav/mysql_upgrade_live.pub', '/code_git/raghav/mysql_upgrade_live')) {
            $stream = $this->exec($command);
            stream_set_blocking($stream, true);
            $base_file_name = basename($source_filename);
            $result = stream_get_contents($stream);
            echo 'Executing';
            if (strpos($result, 'error') === false) {
                //echo $result;
                $command = "gunzip $dest_path$base_file_name";
                $stream = $this->exec($command);
                stream_set_blocking($stream, true);
                $result = stream_get_contents($stream);


                $command="";

            } else {
                //send an error
            }
        }
        else{
            //Not authorised
        }
    }


    function exec( $command )
    {
        $result = $this->rawExec( $command.';echo -en "\n$?"' );
        if( ! preg_match( "/^(.*)\n(0|-?[1-9][0-9]*)$/s", $result[0], $matches ) ) {
            throw new RuntimeException( "output didn't contain return status" );
        }
        if( $matches[2] !== "0" ) {
            throw new RuntimeException( $result[1], (int)$matches[2] );
        }
        return $matches[1];
    }

    function rawExec( $command )
    {
        $stream = ssh2_exec( $this->_ssh2, $command );
        $error_stream = ssh2_fetch_stream( $stream, SSH2_STREAM_STDERR );
        stream_set_blocking( $stream, TRUE );
        stream_set_blocking( $error_stream, TRUE );
        $output = stream_get_contents( $stream );
        $error_output = stream_get_contents( $error_stream );
        fclose( $stream );
        fclose( $error_stream );
        return array( $output, $error_output );
    }
    */


    private function fetchFiles()
    {
        //$result = $this->sqsClient->listQueues();
        global $conn_log;
        $queue_url = $this->sqsClient->getQueueUrl(array('QueueName' => $this->queueName));
        $this->sqsUrl = $queue_url->get('QueueUrl');
        $start_time = time();
        while (true) {

            $messages = $this->sqsClient->receiveMessage(array(
                'QueueUrl' => $this->sqsUrl
            ));
            $time_now = time();
            if ($time_now - $start_time >= 86400) {
                //Object of SQS may expire, exit the script and the cron will restart it in specified time, remove the locks if necessary
                $this->exitScript();
            }

            $messages = $messages->get('Messages');
            if ($messages == null) {
                // No message to process,wait for some time and then restart
                sleep(30);
                continue;
            }

            foreach ($messages as $message) {
                $records = json_decode($message['Body'], true);
                //print_r($records['Records'][0]);die;
                foreach ($records['Records'] as $record) {
                    $info = $record['s3'];
                    $bucketname = $info['bucket']['name'];
                    $this->bucketName=$bucketname;
                    $filepath = $info['object']['key'];
                    //To delete the message from SQS use this receipt handle
                    $receipt_handle = $message['ReceiptHandle'];
                    $filename = basename($filepath);
                    //echo $filepath.PHP_EOL;
                    //Download the file from the S3 and store it somewhere
                    $field_delimeter = '';
                    $line_delimeter = '';
                    $type='';
                    $tablename = explode('/', $filepath)[1];
                    if ($tablename == 'action_log') {
                        $tablename = 'action_log_new';
                        $type='action_log';
                        $field_delimeter = '\t';
                        $line_delimeter = '\n';
                    } else if ($tablename == 'user_recommendation_query_log') {
                        $field_delimeter = ';';
                        $type='rank_log';
                        $line_delimeter = 'end-line';
                    } else if ($tablename == 'user_recommendation_rank_log') {
                        $field_delimeter = ';';
                        $type='query_log';
                        $line_delimeter = 'end-line';
                    }
                    $temp_filename='/tmp/'.$filename;
                    //$this->testssh($bucketname.'/'.$filepath,'/tmp/');die;
                    if (Utils::getFromS3($filepath,$temp_filename, 1,$bucketname)) {
                        $filename=$this->decompressFile($temp_filename);
                        global $baseurl;
                        $sql = "LOAD DATA LOCAL INFILE '$filename' INTO TABLE $tablename fields terminated by '$field_delimeter' lines terminated by '$line_delimeter'";
                        $exe = $conn_log->Execute($sql);
                        $warningQuery = "SHOW WARNINGS";
                        $warnings = $conn_log->Execute($warningQuery);
                        $warningCount = $warnings->RowCount();
                        if ($warningCount > 0) {
                            $msg = "Warnings in loading data:" . $warningCount . "\n";
                            for ($j = 0; $j < $warningCount; $j++) {
                                while ($row = $warnings->FetchRow()) {
                                    foreach ($row as $val) {
                                        $msg .= "$val\t";
                                    }
                                    $msg .= "\n";
                                }
                            }
                            $msg.="\n\n".$filepath;
                            trigger_error("PHP Web: Warning has occurred while loading the data from file into $filename", E_USER_WARNING);
                            Utils::sendEmail("raghav@trulymadly.com", "admintm@trulymadly.com", "Warning has occurred while loading data from file into $tablename. ($filename) " . $baseurl, $msg);
                            //If warnings occur let file stay to be analysed

                        }
                        unlink('/tmp/'.basename($filename));
                        unlink('/tmp/'.basename($filename.'.gz'));
                        $str=$this->makeDateFromPath($filepath);
                        $this->insertIntoCheckpoint(basename($filename),$type,$str,$filepath);
                        $this->sqsClient->deleteMessage(array(
                            'QueueUrl' => $this->sqsUrl,
                            'ReceiptHandle' => $receipt_handle
                        ));
                    } else {
                        echo 'Facing error while downloading from s3';
                        trigger_error("Can't download the file from s3 while loading infile", E_USER_WARNING);
                    }

                    //echo $filename;echo PHP_EOL;
                }

            }
            sleep(2);
        }

    }


}



try
{
    if(php_sapi_name()=='cli') {
        $sqsLogs = new sqsLogs();

        if(date('i')==10) {
            $sqsLogs->instantiate('reporting');die;
        }
        $sqsLogs->instantiate('fetchfiles');

    }else {
        echo 'Not allowed to run other than CLI';
        die;
    }

}
catch(Exception $ex)
{
    echo 'Exception';
    trigger_error($ex->getMessage(),E_USER_WARNING);
}










?>

