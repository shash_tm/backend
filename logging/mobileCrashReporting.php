<?php

require_once dirname(__FILE__).'/../include/config.php';
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";




try{

	global $crash_report_url;

	$param_array[0] = $_POST['PHONE_MODEL'];
	$param_array[1]  = $_POST['ANDROID_VERSION'];
	$param_array[2] = $_POST['PACKAGE_NAME'];
	$param_array[3]=  $_POST['APP_VERSION_CODE'];
	$param_array[4] = $_POST['PRODUCT'];
	$param_array[5] = $_POST['APP_VERSION_NAME'];
	$param_array[6] = $_POST['AVAILABLE_MEM_SIZE'];
	$param_array[7] =  $_POST['BRAND'];
	$param_array[8] = $_POST['TOTAL_MEM_SIZE'];
	$param_array[9] =  $_POST['DEVICE_FEATURES'];
	$param_array[10] = $_POST['CRASH_CONFIGURATION'];
	$param_array[11] = $_POST['USER_CRASH_DATE'];
	$param_array[12] = $_POST['STACK_TRACE'];
	$param_array[13] = $_POST['INITIAL_CONFIGURATION'];
	$sharedPref = $_POST['SHARED_PREFERENCES'];

	$user_id = null;
	if(strpos( $sharedPref, "default.USER_ID") !== false){
		$regex = "/default.USER_ID=([0-9]+)/";
		preg_match($regex, $sharedPref, $user_id);
	}

	$dump = null;
	if(!isset($user_id)) $user_id[1] = -1;
	foreach($_POST as $key => $value) {
		if ($key != 'SHARED_PREFERENCES') {
			$dump .= $key." = ".$value."\n";
		}
	}
	$param_array[14] = $dump;
	$sql = "INSERT INTO mobile_crash_report (user_id, phone_model, android_version,package_name,app_version_code, product, app_version_name,available_mem_size,  brand, total_mem_size, device_features, crash_configuration, user_crash_date, stack_trace, initial_configuration, dump, timestamp )values ($user_id[1],?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
	Query::INSERT($sql, $param_array);

	$response = Utils::curlCall($crash_report_url, $_POST);
}
catch(Exception $e){
	//echo $e->getMessage();
	trigger_error("PHP WEB:".$e->getTraceAsString(),E_USER_WARNING);
}
?>
