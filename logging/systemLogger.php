<?php
require_once dirname(__FILE__).'/../include/config.php';
/*
 * Generic class to log all the mailers sent by the system
 * @author Himanshu
 */

class  SystemLogger{

	public function logSystemMail($data){
		global $conn;
		$sql = $conn->Prepare("INSERT INTO system_mailer_log (subject, user_id, email_id, tstamp)values (?,?,?,now())");
		$conn->Execute($sql, array($data['subject'], $data['user_id'], $data['email_id']));
	}

}
?>