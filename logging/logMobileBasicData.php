<?php

require_once  dirname ( __FILE__ ) . "/../include/config.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";


/**
 * file to log the mobile basic data
 * @author himanshu
 *
 */
class logMobileBasics{

	private $user_id;

	function __construct($user_id){
		$this->user_id = $user_id;
	}

	public function insertLog($data){
		if(!isset($data['referrer'])){
			$data['referrer'] = null;
		}
		//		var_dump($data);
		$sql = "insert into mobile_data values (?,?,?,?,?,?,?,?) on duplicate key update referrer = ?,device_id =?,device_name =?,android_version_name =?,android_version_code =?,app_version_name=?,app_version_code =?  ";
		//Query::INSERT($sql, array($this->user_id, $data['referrer'],$data['device_id'],$data['device_name'], $data['android_version_name'],$data['android_version_code'], $data['app_version_name'],$data['app_version_code'],  $data['referrer'],$data['device_id'],$data['device_name'], $data['android_version_name'],$data['android_version_code'], $data['app_version_name'],$data['app_version_code']));
	}

}

try{
	$user = functionClass::getUserDetailsFromSession();
	$user_id = $user['user_id'];
	$logger = new logMobileBasics($user_id);
	if($_REQUEST['insert_mobile_log'] == "insert"){
		$logger->insertLog($_REQUEST['data']);
	}

}
catch(Exception $e){
	trigger_error("PHP WEB:" . $e->getTraceAsString(), E_USER_WARNING);
}
?>