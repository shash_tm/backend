<?php 

require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../TMObject.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";

try{
$sess = functionClass::getUserDetailsFromSession();
$rs = $conn->Execute ($conn->prepare("select status,gender,plan from user where user_id=?"),$sess['user_id']);
$res = $rs->FetchRow();
if(isset($_REQUEST['reason'])){
	//query
	if($_REQUEST['reason'] == 'Other'){
		$desc = $_REQUEST['reason_desc']; 
	}
	else $desc = $_REQUEST['reason'];
	
	//var_dump($sess);
	
	$query="UPDATE user SET status = 'suspended' where user_id=?";
	$param_array=array($sess['user_id']);
	$tablename='user';
	$ex=Query_Wrapper::UPDATE($query, $param_array, $tablename);
	
	/*$sql = $conn->Prepare("UPDATE user SET status = 'suspended' where user_id=?");
	$ex = $conn->Execute($sql, array($sess['user_id'])); */

	$query="INSERT INTO user_suspension_log(user_id,reason, previous_status, current_status, timestamp ) values(?,?,?,'suspended',now())";
	$param_array=array($sess['user_id'],$desc, $sess['status'] );
	$tablename='user_suspension_log';
	Query_Wrapper::INSERT($query, $param_array, $tablename);
	
	
	/*$sql2 = $conn->Prepare("INSERT INTO user_suspension_log(user_id,reason, previous_status, current_status, timestamp ) values(?,?,?,'suspended',now())");
	$conn->Execute($sql2, array($sess['user_id'],$desc, $sess['status'] ));*/
	
	functionClass::refreshSession($sess['user_id']);
	$smarty->assign("taskDone", "taskDone");
	$smarty->assign("msg", "Profile deactivated successfully");
}
if($res['status'] == 'suspended'){
		$smarty->assign("taskDone", "taskDone");
}
$obj = new TMObject();
$header = $obj->getHeaderValues($sess['user_id']);
$smarty->assign('header', $header);
$smarty->display(dirname ( __FILE__ ). '/../templates/utilities/PrivacySetting.tpl');
}
catch(Exception $e){
	echo $e->getMessage();
}


?>