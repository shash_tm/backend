<?php

require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/User.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";


/*
 * Unsubscription from Emailers
 * @Himanshu
 */

class Unsubscription{

	private $email;
	private $conn;
	private $user_id;
	private $baseurl;
	private $user;

	function __construct($email){
		global $conn, $baseurl;
		$this->email = $email;
		$this->conn= $conn;
		$this->baseurl = $baseurl;
		$user = new User();
		$this->user = $user;
	}

	private function generateChecksum(){
		$row = $this->user->getUserDetailsOnEmailBasis($this->email);
		$checksum = md5($row['user_id'] . '|' . Utils::$salt . '|' . $this->email);
		return $checksum;
	}

	public function generateLink(){
		$checksum = $this->generateChecksum();
		$link = $this->baseurl . "/logging/unsubscribe.php?m=$checksum&email=$this->email";
		return $link;
	}


	private function verifyChecksum($checksum){
		$post = $this->generateChecksum();
		if($post == $checksum){
			return true;
		}
		
		return false;
	}
	
	public function unsubscribe($checksum){
		
		if($this->verifyChecksum($checksum)){
			/*$row = $this->user->getUserDetailsOnEmailBasis($this->email);
			$user_id = $row['user_id'];*/

			$query="UPDATE user SET email_status = 'unsubscribe' WHERE email_id = ?";
			$param_array=array($this->email);
			$tablename='user';
			Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
			/*$sql = $this->conn->Prepare("UPDATE user SET email_status = 'unsubscribe' WHERE email_id = ?");
			$this->conn->Execute($sql,array($this->email));*/
			
			$eventTrack = new EventTrackingClass();
			$data_logging = array();
			if(isset($_REQUEST['utm_campaign']) && isset($_REQUEST['utm_content']))
			{
				$data_logging [] = array( "user_id"=> $_REQUEST['uid'], "activity" =>$_REQUEST['utm_campaign'], "event_type" => $_REQUEST['utm_content'], "event_status" => "success" );
				$eventTrack->logActions($data_logging);
			}
			
// 			$eventTrack = new EventTracking($_REQUEST['uid']);
// 			$eventTrack->logAction(null);
			return "success";
		}
		return "error";
		
		//http://killbill.in/x/?cid=1394078041&action=click&sid=dh2&mle=him26.89@gmail.com
		//&utm_source=YM-AMXPSMailer&utm_medium=email&utm_campaign=ym-amxpsmailer&listname=C1-CIB&typ=2
	}
}




?>