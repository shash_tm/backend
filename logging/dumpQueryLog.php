<?php 
$fptr = fopen("/tmp/queryLog.txt", 'c');

try{
	if(php_sapi_name() === 'cli'){
		$isLocked  = isScriptRunning($fptr);
		if($isLocked == false){
			ini_set('max_execution_time', 0);
			//require_once  dirname ( __FILE__ ) . "/../include/config_log.php";
			require_once  dirname ( __FILE__ ) . "/../include/Utils.php";
			loadFromFile();
		}
	}
}
catch(Exception $e) {
	global $fptr;
	releaseLock($fptr);

	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	trigger_error("PHP Web:".$e->getMessage(), E_USER_WARNING);
}

function releaseLock($fptr) {
	flock($fptr, LOCK_UN);
}

function isScriptRunning() {
	global $fptr,$baseurl;
	if (!flock($fptr, LOCK_EX|LOCK_NB, $wouldblock)) {
		if ($wouldblock) {
			echo "already locked";
			$isLocked = 1;
			Utils::sendEmail("shashwat@trulymadly.com,rajesh.singh@trulymadly.com,arpan@trulymadly.com", "admintm@trulymadly.com", "Already Locked: Load From file into user_recommendation_query_log ".$baseurl, "" );
		}
		else {
			echo "could not lock";
		}
	}
	else {
		echo "obtain lock";
		$isLocked = 0;
		if(flock($fptr,LOCK_EX) == false){
			throw new Exception("unable to acquire lock");
		}else{
			echo "lock acquired";
		}
	}
	return $isLocked;
}

function getFile() {
	$suffix = intval((intval(date('i'))/5))*5;
	$preSuffix = $suffix-5;
	$currentFile = "query_log_".date('Y-m-d')."_".date('H')."_".$suffix.'.txt';
	$prevFile = "query_log_".date('Y-m-d')."_".date('H')."_".$preSuffix.'.txt';
	
	$dir = dirname ( __DIR__ ).'/action_log';
	$files = scandir($dir);

	$fullPath = array();

	foreach ($files as $file) {
		if(preg_match('/query_log/',$file) && $file != $currentFile && $file != $prevFile && ($file != '.' || $file != '..')) {
			$fullPath[] = $dir.'/'.$file;
		}
	}
	return $fullPath;
}

function loadFromFile() {
	global $conn_log,$baseurl,$config;
	$filename = getFile();
	for ($i = 0; $i < count($filename); $i++) {
		if(file_exists($filename[$i]) && filesize($filename[$i]) > 0) {
			Utils::putLogs($filename[$i],'user_recommendation_query_log');
		}else {
			//unlink($filename[$i]);
		}
	}

	if(count($filename)>10) {
		$env = $config['env']['type'];
		if (!($env == 't1' || $env == 'dev')) {
			Utils::sendEmail("raghav@trulymadly.com", "admintm@trulymadly.com", "Alert some problem has occurred. More than 10 files of log." . $baseurl, "");
		}//Utils::sendEmail("rajesh.singh@trulymadly.com", "admintm@trulymadly.com", "Alert some problem has occurred. More than 10 files of log (user_recommendation_query_log). ".$baseurl, "" );
	}

}

?>