<?php

require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../UserData.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";




try{
	$func = new functionClass();
	$login_mobile = $func->isMobileLogin();
	functionClass::redirect ('deactivate',$login_mobile);
	
	$sess = functionClass::getUserDetailsFromSession();
	$user_id = $sess['user_id'];
	//if(isset($_REQUEST['user_id'])) $user_id = $_REQUEST['user_id'];
	if(isset($user_id)){
		$ud = new UserData($user_id);

		$uuDBO = new userUtilsDBO();
		$currentStatus = $ud->fetchStatus();

		if(isset($_REQUEST['reason'])){
			if($_REQUEST['reason'] == 'Other'){
				$desc = $_REQUEST['reason_desc'];
			}
			else $desc = $_REQUEST['reason'];

			$uuDBO->suspend($user_id, $desc, $currentStatus);
			$out[Utils::$responseCode] = 200;
			echo json_encode($out);
			
			if(!isset($_REQUEST['login_mobile']))
			header("Location:../logout.php"); die;
		}
	}
	else{
		$out[Utils::$responseCode] = 401;
		echo json_encode($out);
	}
}
catch(Exception $e){
	//echo $e->getMessage();
	$out[Utils::$responseCode] = 401;
	echo json_encode($out);
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);

}


?>