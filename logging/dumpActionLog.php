<?php 
$fptr = fopen("/tmp/log1.txt", 'c');

try{
	if(php_sapi_name() === 'cli'){
		$isLocked  = isScriptRunning($fptr);
		if($isLocked == false){
			ini_set('max_execution_time', 0);
			//require_once  dirname ( __FILE__ ) . "/../include/config_log.php";
			require_once  dirname ( __FILE__ ) . "/../include/Utils.php";
			loadFromFile();
		}
	}
}
catch(Exception $e) {
	global $fptr;
	releaseLock($fptr);
	
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
	trigger_error("PHP Web:".$e->getMessage(), E_USER_WARNING);
}

function releaseLock($fptr) {
	flock($fptr, LOCK_UN);
}

function isScriptRunning() {
	global $fptr,$baseurl;
	if (!flock($fptr, LOCK_EX|LOCK_NB, $wouldblock)) {
		if ($wouldblock) {
			echo "already locked".PHP_EOL;
			$isLocked = 1;
			Utils::sendEmail("shashwat@trulymadly.com,rajesh.singh@trulymadly.com,poonam.kumawat@trulymadly.com", "rajesh.singh@trulymadly.com", "Already Locked: Load From file into action log ".$baseurl, "" );
		}
		else {
			echo "could not lock";
		}
	}
	else {
		echo "obtain lock".PHP_EOL;
		$isLocked = 0;
		if(flock($fptr,LOCK_EX) == false){
			throw new Exception("unable to acquire lock");
		}else{
			echo "lock acquired".PHP_EOL;//
		}
	}
	return $isLocked;
}

function getFile() {

	if(date('i')%5!=0) {
		//Return an empty array
		echo 'I run only on minutes which are multiples of 5';
		return array();
	}
	$suffix = intval((intval(date('i'))/5))*5;
	$preSuffix = $suffix-5;
	$currentFile = "action_log_".date('Y-m-d')."_".date('H')."_".$suffix.'.txt';
	$prevFile = "action_log_".date('Y-m-d')."_".date('H')."_".$preSuffix.'.txt';

	$dir = dirname ( __DIR__ ).'/action_log';
	$files = scandir($dir);
	$fullPath = array();
	foreach ($files as $file) {
		if(preg_match('/action_log/',$file) && $file != $currentFile && $file != $prevFile && ($file != '.' || $file != '..')) {
			$fullPath[] = $dir.'/'.$file;	
		}
	}

	return $fullPath;
}


function loadFromFile() {
	global $conn_log,$baseurl,$config;
	$filename = getFile();
	$isRunning = false;
	for ($i = 0; $i < count($filename); $i++) {
		if(file_exists($filename[$i]) && filesize($filename[$i]) > 0) {
			$isRunning = true;
			Utils::putLogs($filename[$i],'action_log');
		}else {
			//unlink($filename[$i]);
		}
	}
	
	if(count($filename)>10 && $isRunning) {
		$env = $config['env']['type'];
		if (!($env == 't1' || $env == 'dev')) {
			Utils::sendEmail("raghav@trulymadly.com", "admintm@trulymadly.com", "Alert some problem has occurred. More than 10 files of log." . $baseurl, "");
		}
	}
	
}
?>