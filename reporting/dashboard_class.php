<?php 
require_once dirname ( __FILE__ ) . "/../abstraction/reporting_query.php";
require_once dirname ( __FILE__ ) . "/../DBO/dashboardDBO.php";


/*
 * Class file for Reporting Dashoard
 * Author :: Sumit
 * 🖖 Live Long and Prosper.
 */

class ReportingDashboard 
{	
	public static $saved_query_count = 10 ;
	public function saveQuery()
	{
		$data = $_REQUEST ;
		if($_REQUEST['set_as_cron'] == true && isset($_REQUEST['mailing_list']) && isset($_REQUEST['frequency']) )
			$data['set_as_cron'] = 'yes';
		else
			$data['set_as_cron'] = 'no';
		
		$data['admin_id'] = $_SESSION['admin_id'] ;
       $insert_id = dashboardDBO::saveNewQuery($data) ;
       return $insert_id ; 
	}
	
	public function getSavedQueries() 
	{
		$filter = array('admin' =>$_REQUEST['filter_admin_id'] ,
						'starred' => $_REQUEST['is_starred'] );
		$data = dashboardDBO::fetchSavedQueries($filter);
		return $data ;
	}

	public function getQueryById($query_id)
	{
		$data = dashboardDBO::getQueryById($query_id);
		return $data ;
	}
	
	public function editQuery()
	{
		$data = $_REQUEST ;
		if($_REQUEST['set_as_cron'] == true && isset($_REQUEST['mailing_list']) && isset($_REQUEST['frequency']) )
			$data['set_as_cron'] = 'yes';
		else
			$data['set_as_cron'] = 'no';	
		dashboardDBO::updateQuery($data) ; 
	}
	
	public function deleteQuery()
	{
		
	}

	public function showTables(){
		return dashboardDBO::showTables();
	}

	public function getTableDetails($table_name){
		return dashboardDBO::getTableDetails($table_name);
	}

	public function getAdminList()
	{
		$data = dashboardDBO::getAdminList();
		return $data ;
	}
	
	public function changeQueryStatus($query_id, $column, $value) 
	{
		dashboardDBO::changeQueryStatusDBO($query_id, $column, $value);
	}

	public function getAllAdmins()
	{
		return dashboardDBO::getAllAdmins();
	}

	public function updateAdminPermissions($admin_id, $privileges)
	{
		return dashboardDBO::updateAdminPermissions($admin_id, $privileges);
	}
}


 
?>