<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/dashboard_class.php";

/* 
 * Exposed APIs for reporting dashboard 
 * Author :: Sumit
 * 🖖 Live Long and Prosper.
 */

try 
{
	$dealAdminObj  = new adminLogin() ;
	$dealAdminObj->checkAdminSession() ;

	$data = array();
	// check if the user actually has permissions to run the queries on DB 
	if($_SESSION['super_admin'] == 'yes' || in_array('reporting', $_SESSION['privileges'] ) )
	{
		if($_REQUEST ['action'] == 'run_query'  && isset($_REQUEST['query']) && $_REQUEST['query'] != null)
		{ 
			$sql = $_REQUEST['query'] ;
			$countQuery = "select count(*) as total ".substr($sql,stripos($sql, "from"));
			$start = $_REQUEST['iDisplayStart'];
			$end = $_REQUEST['iDisplayLength'];
			if((is_numeric($start) && is_numeric($end)) && strpos(trim($sql),"select") == 0)
				$sql .= " limit $start,$end ";
			$queryObj = new ReportingQuery($sql);
			$countQueryObj = new ReportingQuery($countQuery, FALSE);
			if ($error = $queryObj->getErrorMsg())
			{
				$data['aaData'] = array();
				$data['aaData']['responseCode'] = 200;
				$data['aaData']['status'] = "error" ;
				$data['aaData']['error'] = $error;
			}
			else
			{
				$countArr = $countQueryObj->getRowsWithLimit();
				$count = $countArr[0]['total'];
			//	$fields = $queryObj->getFields();
				$result = $queryObj->getRowsWithLimit();
				$data['iTotalRecords'] = $count;
				$data['iTotalDisplayRecords'] = $count;
				$data['aaData'] = array();
				$i=0;
				foreach($result as $key => $value){
					$j=0;
					foreach($value as $colName => $colValue) {
						$data['aaData'][$i][$j] = $colValue;
						$j++;
					}
					$i++;
				}
		//		$data['responseCode'] = 200;
			//	$data['status'] = "success" ;
		//		$data['fields'] = $fields ;
		//		$data['resultSet'] = $result ;
				
			}			
		} 
		else if ($_REQUEST['action'] == 'save_query' && isset($_REQUEST['query_text']) && $_REQUEST['query_text'] != NULL ) 
		{
			$dashboard = new ReportingDashboard() ;
			$insert_id = $dashboard->saveQuery();
			$data['responseCode'] = 200;
			$data['status'] = "success" ;
			$data['query_id'] = $insert_id ;
		}
		else if ($_REQUEST['action'] == 'edit_query' && isset($_REQUEST['query_text']) && $_REQUEST['query_text'] != NULL && isset($_REQUEST['query_id']))
		{
			$dashboard = new ReportingDashboard() ;
			$dashboard->editQuery();
			$data['responseCode'] = 200;
			$data['status'] = "success" ; 
			$data['query_id'] = $_REQUEST['query_id'] ;
		}
		else if ($_REQUEST['action'] == 'delete' && isset($_REQUEST['query_id']) && $_REQUEST['query_id'] != NULL)
		{ 
			$dashboard = new ReportingDashboard() ;
			$dashboard->changeQueryStatus($_REQUEST['query_id'], "status", "deleted");
			$data['responseCode'] = 200;
			$data['status'] = "success" ;
			$data['query_id'] = $_REQUEST['query_id'] ;
		}
		else if ($_REQUEST['action'] == 'undo_delete' && isset($_REQUEST['query_id']) && $_REQUEST['query_id'] != NULL)
		{
			$dashboard = new ReportingDashboard() ;
			$dashboard->changeQueryStatus($_REQUEST['query_id'], "status", "active");
			$data['responseCode'] = 200;
			$data['status'] = "success" ;
			$data['query_id'] = $_REQUEST['query_id'] ;
		}
		else if ($_REQUEST['action'] == 'star' && isset($_REQUEST['query_id']) && $_REQUEST['query_id'] != NULL)
		{
			$dashboard = new ReportingDashboard() ;
			$dashboard->changeQueryStatus($_REQUEST['query_id'], "is_starred", "yes");
			$data['responseCode'] = 200;
			$data['status'] = "success" ;
			$data['query_id'] = $_REQUEST['query_id'] ;
		}
		else if ($_REQUEST['action'] == 'unstar' && isset($_REQUEST['query_id']) && $_REQUEST['query_id'] != NULL)
		{
			$dashboard = new ReportingDashboard() ;
			$dashboard->changeQueryStatus($_REQUEST['query_id'], "is_starred", "no");
			$data['responseCode'] = 200;
			$data['status'] = "success" ;
			$data['query_id'] = $_REQUEST['query_id'] ;
		}
		else if ($_REQUEST['action'] == 'get_table_details' && isset($_REQUEST['table_name']) && $_REQUEST['table_name'] != NULL)
		{
			$sql = "desc " . $_REQUEST['table_name'] ;
			$queryObj = new ReportingQuery($sql, null, false);
			$data['tableDetails'] = $queryObj->getRowsWithLimit();
			$data['responseCode'] = 200;
			$data['status'] = "success" ;
		}
		else if($_REQUEST ['action'] == 'get_headers'  && isset($_REQUEST['query']) && $_REQUEST['query'] != null){
			$sql = $_REQUEST['query'] ;
			if(strpos(trim($sql),"select") == 0)
				$sql .= " limit 1";
			$queryObj = new ReportingQuery($sql);
			$fields = $queryObj->getFields();
			$data['responseCode'] = 200;
			$data['status'] = "success" ;
			$data['fields'] = $fields ;
			if ($error = $queryObj->getErrorMsg())
			{
				$data['status'] = "error" ;
				$data['error'] = $error;
			}
		}
		else 
		{
			$data['responseCode'] = 403;
			$data['status'] = 'UnknownRequest';
		}
		

	} else 
	{
		$data['responseCode'] = 401;
		$data['status'] = 'Mind you own fuckin business \'ese' ;
	//	$dealAdminObj->goToDefault();
	}

	print_r(json_encode($data));


} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
}

?>   