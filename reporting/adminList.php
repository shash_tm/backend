<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/dashboard_class.php";
try 
{
	///
	if($_SESSION['super_admin'] == 'yes' && in_array('manage_admins', $_SESSION['privileges'] ) ) {
		if (isset($_POST['action']) && $_POST['action'] == "update_privileges") {
			$data = array();
			$dashboard = new ReportingDashboard();
			$dashboard->updateAdminPermissions($_POST['admin_id'], $_POST['privileges']);
			$data['responseCode'] = 200;
			$data['status'] = "success";
			print_r(json_encode($data));
		} else {
			$dashboard = new ReportingDashboard();
			$admin_list = $dashboard->getAllAdmins();
			$permissions = array("reporting", "deal_view", "deal_edit", "photo_male", "photo_female", "photo_id","quiz_view","quiz_edit","quiz_status", "manage_admins","search","mailer","video_view","video_edit","videos");
			$smarty->assign("admin_list", $admin_list);
			$smarty->assign("permissions", $permissions);
			$smarty->display("../templates/ReportingDashboard/adminList.tpl");
		}
	}else{
		$smarty->display("../templates/ReportingDashboard/adminList.tpl");
	}

} catch (Exception $e) {
trigger_error($e->getMessage(),E_USER_WARNING) ; 
} 