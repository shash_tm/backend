<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/dashboard_class.php";
try 
{
	if($_SESSION['super_admin'] == 'yes' || in_array('reporting', $_SESSION['privileges'] ) )
	{
		$dashboard = new ReportingDashboard() ;
		$queries = $dashboard->getSavedQueries();
		$admin_list = $dashboard->getAdminList();
		if(isset($_REQUEST['filter_admin_id']) && $_REQUEST['filter_admin_id'] !=NULL )
		{
			$smarty->assign ("is_starred" , $_REQUEST['is_starred']) ;
			$smarty->assign ("filter_admin_id" , $_REQUEST['filter_admin_id']) ;
		}
		$smarty->assign ("baseurl" , $admin_baseurl) ;
		$smarty->assign ("admin_list" , $admin_list) ;
		$smarty->assign ("query_list" , $queries) ;
	}
	
	$smarty->display ( "../templates/ReportingDashboard/queryListNew.tpl" );

} catch (Exception $e) {
trigger_error($e->getMessage(),E_USER_WARNING) ; 
} 