<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../dealAdmin/adminLoginClass.php";
require_once dirname ( __FILE__ ) . "/dashboard_class.php"; 
require_once dirname ( __FILE__ ) . "/../abstraction/reporting_query.php";

/*
 * Main Url to show the reporting dashboard
 * Author :: Sumit
 * 🖖 Live Long and Prosper.
 */

try
{
	$dealAdminObj  = new adminLogin() ;
	$dealAdminObj->setRedirectStatus(false);
	$dealAdminObj->checkAdminSession() ;
	 // check if the user actually has permissions to run the queries on DB
	if($_SESSION['super_admin'] == 'yes' || in_array('reporting', $_SESSION['privileges'] ) )
	{
		// This admin has the permissions to view the page. Now show the dashboard.
		$smarty->assign ("baseurl" , $admin_baseurl) ;
		$dashboard = new ReportingDashboard() ;
		if(isset($_GET['query_id'])){
			$query = $dashboard->getQueryById($_GET['query_id']);
			$smarty->assign ("query" , $query) ;
		}
	//	var_dump($dashboard->showTables());
		$queryObj = new ReportingQuery('show tables', NULL, FALSE);
		$smarty->assign ("tables" ,$queryObj->getRowsWithLimit()) ;
	}
	$smarty->display ( "../templates/ReportingDashboard/reporting.tpl" );
} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
}



?>