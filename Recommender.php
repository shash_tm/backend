<?php

require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/include/header.php";
require_once dirname ( __FILE__ ) . "/abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/DBO/matchesDBO.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/include/systemMessages.php";
require_once dirname ( __FILE__ ) . "/logging/emailerLogging.php";
require_once dirname ( __FILE__ ) . "/utilities/counter.php";
require_once dirname ( __FILE__ ) . "/AB_Profiler/Logger/AB_Logger.php";
require_once dirname ( __FILE__ ) . "/AB_Profiler/Config/AB_Config.php";
require_once dirname ( __FILE__ ) . "/Advertising/ProfileCampaign.php";
require_once dirname ( __FILE__ ) . "/RecommendationEngines/Utils/RecommendationUtils.php";

foreach( glob(dirname( __FILE__ ) . "/SelectionStrategy/*.php") as $selection_strategy ) {
    require_once( $selection_strategy );
}
foreach( glob(dirname( __FILE__ ) . "/AB_Profiler/Selector/*.php") as $selector ) {
    require_once( $selector );
}

class Recommender  {

	private $userId;
        private $gender;
        private $age;
        private $location;
        private $city;
	private $numberOfResults = 16;
	private $key;
	private $redisEvenDB;
	private $redisOddDB;
	private $matchCountKey;
	private $redis;
	private $uu;

	function __construct($user_id , $gender, $age, $location,$city) {
		global $redis, $redis_db_even, $redis_db_odd;
		$this->redis = $redis;
		$this->userId = $user_id;
                $this->gender = $gender;
                $this->age = $age;
                $this->location = $location;
                $this->city = $city;
		$this->redisEvenDB = $redis_db_even;
		$this->redisOddDB = $redis_db_odd;		
		$this->uu = new UserUtils();
		$this->key = Utils::$redis_keys['matches']. "$user_id";
		$this->matchCountKey = Utils::$redis_keys['match_action_count']. "$user_id";
	}

	public function getAttributes($match_array, $isAuthentic = false, $likeIds = null ){
		$profile_data = array();
		$uuDBO = new userUtilsDBO();
		$uu = new UserUtils();
		
		$match_id_array = $match_array["all_ids"];
		$availableIds = $match_array["available_ids"];
		$ids = array_merge(array($this->userId), $match_id_array);
		$row_data1 = $uuDBO->getUserBasicInfo($ids, false);

		foreach ($row_data1 as $val){
			$userInfo[$val['user_id']] = array(
                            "user_id" => $val['user_id'],
                            "fname" => $val['fname'], 
                            "gender" => $val['gender'], 
                            "status" => $val['status'],
                            "last_login" => Utils::humanTime($val['last_login']));
		}
		if($_REQUEST['webpSupport'] == 'yes')
			$webp = TRUE;
		$images_arr = $uu->getActiveImages($match_id_array, $userInfo[$this->userId]['gender'],$webp);
		$non_authentic_ids = array();
		foreach ($images_arr as $key => $val){
			if((strpos( $images_arr[$key]['profile_pic'], "dummy") !== false)|| $userInfo[$key]['status'] != 'authentic'){
				$non_authentic_ids[] = $key;
				continue;
			}

			$profile_data[$key]['profile_pic'] = $images_arr[$key]['profile_pic'];
			$profile_data[$key]['other_pics'] = count($images_arr[$key]['other_pics']) +1;
		}

		if(!empty($non_authentic_ids)){
			$junk_ids = array_intersect($match_id_array, $non_authentic_ids);
			$match_id_array = array_diff($match_id_array, $non_authentic_ids);
			//remove from redis availability
			if($userInfo[$this->userId]['gender'] == "F"){
				$nonAvailableIds = array_intersect($junk_ids, $likeIds);
				$count_junk_ids = count($nonAvailableIds);
				if($count_junk_ids !=0){
					$minuteOfHour = floor(date("i")/5)%2;
                                        if($minuteOfHour == 0)$db =$this->redisOddDB; else $db =$this->redisEvenDB;
					$this->redis->SELECT($db);
					$this->redis->DECR(Utils::$redis_keys['like_count'].$this->userId, $count_junk_ids);
					$this->redis->SELECT(0);
					$sentinel_key = array_search("-1", $junk_ids); //$sentinel_key is -1 
					unset($junk_ids[$sentinel_key]);  
					$uu->updateDiffInMatches($this->userId, $junk_ids, $likeIds);
				}
			}
			foreach ($junk_ids as $val){
				unset($userInfo[$val]);
			}
		}
		if(count($match_id_array) == 0 ) return null ;
                $adCampaigns = array();
                if(ProfileCampaign::isProfileCampaigningActive()) {
                    $adCampaigns = ProfileCampaign::getUserIdsForActiveCampaigns();
                }
               
		/*
		 * POC Mode hack being used here.Ideally this should get picked from a SQL table.
		 */ 
		$videoProfiles = array( 580941  => 'LIMzOPbxrCQ',
                                1556082 => 'y37nQZ4TuL0',
                                1534814 => '4l4OxbhVlhQ',
                                1534310 => '_SnPOogUAJk',
                                1534528 => 'LjaAfYOGTfI',
                                1534390 => 'joGWwgwtVaA',
                                1534492 => 'a7K9hiKb54I');
		foreach ($userInfo as $key => $value) {
			if($key!= $this->userId){
				if(in_array($key, $adCampaigns) && !key_exists($key,$videoProfiles)) {
                                    $profile_data[$key]['fname'] = $value['fname'];
                                    $profile_data[$key]['isProfileAdCampaign'] = true;
                                    $profile_data[$key]['campaignLikes'] = ProfileCampaign::getAdCampaignLinks($key);
                                } else {
                                    $profile_data[$key]['fname'] = str_repeat("x",strlen($value['fname']) );
                                    $profile_data[$key]['isProfileAdCampaign'] = false;
                                }
				if(key_exists($key,$videoProfiles)) {
                    $profile_data[$key]['videoLink'] = $videoProfiles[$key];
                }
				$profile_data[$key]['last_login'] = $value['last_login'] ;
				$profile_data[$key]['status'] =  $value['status'];
			}
			else{
				$myInfo['status'] = $value['status'];
				$myInfo['gender']=$value['gender'];
			}
		}
		$row_data = $uuDBO->getUserData($match_id_array, false);
		$insti = null;
		foreach ($row_data as $val){

			$profile_data[$val['user_id']]['id'] = $val['user_id'];
			$profile_data[$val['user_id']]['age'] = $val['age'];
			$profile_data[$val['user_id']]['city'] = ($val['stay_city_display']==null)?$val['state']:$val['stay_city_display'];

			$insti = json_decode($val['institute_details'], true);
			$profile_data[$val['user_id']]['institutes'] = $insti;
			$profile_data[$val['user_id']]['highest_degree'] = $val['degree'];
			$profile_data[$val['user_id']]['industry'] =$val['industry_name'];;
			$profile_data[$val['user_id']]['designation'] =$val['designation'];
			if($val['work_status'] == 'no'){
				$profile_data[$val['user_id']]['designation'] = "Not Working";
			}

		}			
		$trustMeter = $uuDBO->getTrustBuilderData($match_id_array, false);
		foreach ($trustMeter as $val){
			$profile_data[$val['user_id']]['trust_score'] = $val['trust_score'];
			$profile_data[$val['user_id']]['trust_score_angle'] = ($val['trust_score']*1.8) -90;
		}

		$intHobs = $uuDBO->getPreferenceInterests($match_id_array, false);
		foreach ($intHobs as $val){
			$profile_data[$val['user_id']]['preferredInterests'] = json_decode($val['preferences'], true);
		}
		$mutual_connections = $uuDBO->getMutualConnections($this->userId, $match_id_array);
		foreach ($mutual_connections as $key => $val){
			$profile_data[$key]["mutual_connections_count"] = $val['mutual_connections_count'];
			$mutuals = null;
			$i=0;
			foreach ($val['mutual_connections'] as $mdata){
				$mutuals[$i]["name"] = $mdata["first_name"];
				$mutuals[$i]["pic"] = $mdata["picture"]["data"]["url"];
				$i++;
			}
			$profile_data[$key]["mutual_connections"] = $mutuals;

		}

		foreach ($match_id_array as $val){
			if($isAuthentic == true){
				$likeFlag = 0;
				$profile_data[$val]['canHide'] = true;
				$profile_data[$val]['canLike'] = true;
				if(isset($likeIds) && in_array($val, $likeIds)){
					$likeFlag = 1;
				}
				$profile_data[$val]['like_url'] = $uu->generateLikeLink($this->userId, $val, $likeFlag,$myInfo['gender'], $availableIds[$val]);
				$profile_data[$val]['hide_url'] = $uu->generateHideLink($this->userId, $val, $likeFlag,$myInfo['gender'], $availableIds[$val]);
			}
			else{
				$profile_data[$val]['canHide'] = false;
				$profile_data[$val]['canLike'] = false;
				$profile_data[$val]['like_url'] = null;
				$profile_data[$val]['hide_url'] = null;
			}
			$profile_data[$val]['profile_url'] = $uu->generateProfileLink($this->userId, $val) . '&from_match=true';
		}
		return $profile_data;
	} 

	/**
	 * return the match ids
	 */
	public function fetchResults() {
                            
                $header = Utils::getAllSystemHeaderFields();
                $batchCount = isset($header['batchCount']) ? $header['batchCount'] : -1;
                // RETURN EMPTY sets if it's an ad campaign
                if(ProfileCampaign::isUserAdCampaign($this->userId)) {
                    $recommendations_data = array();
                    $recommendations_data['recommendations'] = array();
                    $recommendations_data['recommendations_information'] = array();
		    $recommendations_data['sessionId'] = 0;
		    //For Ad campaigns do not worry about the Validity of the profile pic
		    $recommendations_data['validProfilePic'] = true;
		    $recommendations_data['noPhotos'] = false;
		    $recommendations_data['likedInThePast'] = false;
                    return $recommendations_data;
                }
                else {
                    $sessionId  = RecommendationUtils::getSetUserCurrentSession($this->userId,$batchCount);
                
                    $no_photo_flow_sql = "select count(*) as flag from user_photo where user_id = ?";
                    $photo_flow_sql = "select sum(if(is_profile = 'yes' and status = 'active',1,0)) as flag from user_photo where user_id = ?";
		    $past_like_sql = "select if(count(*)>0,true,false) as flag from user_like where user1 =  ?";

		    /*
		     * Cases : 
		     * Not uploaded a profile pic - Set in a variable
		     * Uploaded a pic which got rejected - Set in another variable
		     * Uploaded a profile pic which got rejected(Only for people who uploaded a profile pic which has been rejected - Set in another variable
		     * */
		    $photoFlow = Query::SELECT($photo_flow_sql, array($this->userId), Query::$__slave, true);
		    $noPhotoFlow = Query::SELECT($no_photo_flow_sql, array($this->userId), Query::$__slave, true);
		    
		    $validProfilePic = true;
		    $likedInThePast = false;
		    $noPhotos = false;
		    if(sizeof($noPhotoFlow) > 0) {
			    if($noPhotoFlow['flag'] == 0) {
				    $noPhotos = true;
			    }
		    }
		    if(sizeof($photoFlow) > 0) {
			    if($photoFlow['flag'] > 0) {
				    //User has valid profile pic
				    $validProfilePic = true;
			    } else {
				    //Invalid profile pic
				    $validProfilePic = false;
				    if(!$validProfilePic) {
					    //3 cases, rejected profile pic, deleted a pic, approved pic but not as profile pic, now check if there are past likes or not
					    $pastLikeCount = Query::SELECT($past_like_sql, array($this->userId), Query::$__slave, true);
					    if((sizeof($pastLikeCount) > 0) and ($pastLikeCount['flag'] != null) and ($pastLikeCount['flag'] == 1)) {
						    //Past likes value assigned
						    $likedInThePast = true;
					    }
				    }
			    }
		    }

                    $configRecommendations = AB_Config::getRecommendationConfigs();
                    $configRecommendationsEngines = $configRecommendations['engine'];
                    $configRecommendationsEnginePercents = $configRecommendations['percent'];
                    $configRecommendationsjsonParams = $configRecommendations['jsonParams'];
                  //var_dump($configRecommendationsjsonParams);exit;
                    $configRecommendationsEngineFilters = $configRecommendations['filter'];
                
                    $configAesthetics = AB_Config::getAestheticsConfigs();
                    $configAestheticsTypes = $configAesthetics['aesthetic'];
                    $configAestheticsPercents = $configAesthetics['percent'];
                    $configAestheticsjsonParams = $configAesthetics['jsonParams'];
                    $configAestheticsFilters = $configAesthetics['filter'];
                
                   
         
                    $logger = new AB_Logger();
                    
                    // This is the hack to choose UserSpecificFilterStrategy when batchCount=2 and mutual_friends=true
                    // Mutual friends Recommendation Engine should be selected only in 2nd matches call
                    $mutual_friends = isset($header['mutual_friends']) ? $header['mutual_friends'] : false;
                    if (Utils::$mutual_friends_enabled && $mutual_friends && $batchCount == 2) {
                    	$ab_recomendation = new AB_Recommender(
                    			new UserSpecificFilterStrategy(),
                    			array_combine($configRecommendationsEngines,array_map(null,$configRecommendationsEnginePercents,$configRecommendationsEngineFilters)),
                    			array_combine($configRecommendationsEngines,$configRecommendationsjsonParams),
                    			$logger,
                    			new AB_Aesthetics(
                    					new SessionSpecificFilterStrategy(),
                    					array_combine($configAestheticsTypes,array_map(null,$configAestheticsPercents,$configAestheticsFilters)),
                    					array_combine($configAestheticsTypes,$configAestheticsjsonParams),
                    					$logger,
                    					NULL));
                    }
                    else {
                    	$ab_recomendation = new AB_Recommender(
                                            new SessionSpecificFilterStrategy(), 
                                            array_combine($configRecommendationsEngines,array_map(null,$configRecommendationsEnginePercents,$configRecommendationsEngineFilters)),
                                            array_combine($configRecommendationsEngines,$configRecommendationsjsonParams),
                                            $logger,
                                            new AB_Aesthetics(
                                                new SessionSpecificFilterStrategy(),
                                                    array_combine($configAestheticsTypes,array_map(null,$configAestheticsPercents,$configAestheticsFilters)),
                                                    array_combine($configAestheticsTypes,$configAestheticsjsonParams),
                                                    $logger,
                                                    NULL));
                    }
                    $ids = $ab_recomendation->fetchResultsForUser($this->userId, $this->gender , $this->age, $this->location, $this->city, $sessionId);
                   
                    //var_dump($ids); exit;
                    $logData = $ab_recomendation->getSessionSpecificLogData($sessionId);
                    $recommendations_data = array();
                    $recommendations_data['recommendations'] = $ids['recommendations'];
                    $recommendations_data['hasMoreRecommendations'] = $ids['hasMoreRecommendations'];
                    $recommendations_data['isNewRecommendations'] = $ids['isNewRecommendations'];
                    if (isset($ids['mutualFriendsData']))
                        $recommendations_data['mutualFriendsData'] = $ids['mutualFriendsData'];
                    $recommendations_data['recommendations_information'] = $logData;
		    $recommendations_data['repeat_like'] = $ids['repeat_like'];;
		    $recommendations_data['sessionId'] = $sessionId;
		    $recommendations_data['validProfilePic'] = $validProfilePic;
		    $recommendations_data['noPhotos'] = $noPhotos;
		    $recommendations_data['likedInThePast'] = $likedInThePast;
                    $recommendations_data['repeat_likes'] = $ids['repeat_likes'];
                    $recommendations_data['selectSprinkledProfiles'] = $ids['selectSprinkledProfiles'];
                    $recommendations_data['profilesAdCampaigns'] = $ids['profilesAdCampaigns'];
                    $recommendations_data['isSelect'] = $ids['isSelect'];
                    
                    return $recommendations_data;
                }
        }

	public function fetchResultsWithinUserList($containingUsers, $passedLikeLimit, $passedFreshLimit) {
                $configRecommendations = AB_Config::getRecommendationConfigs();
                $configRecommendationsEngines = $configRecommendations['engine'];
                $configRecommendationsEnginePercents = $configRecommendations['percent'];
                $configRecommendationsjsonParams = $configRecommendations['jsonParams'];
                $configRecommendationsEngineFilters = $configRecommendations['filter'];
                $configAesthetics = AB_Config::getAestheticsConfigs();
                    $configAestheticsTypes = $configAesthetics['aesthetic'];
                    $configAestheticsPercents = $configAesthetics['percent'];
                    $configAestheticsjsonParams = $configAesthetics['jsonParams'];
                    $configAestheticsFilters = $configAesthetics['filter'];
                
                    $logger = new AB_Logger();
                    $ab_recomendation = new AB_Recommender(
                                            new SessionSpecificFilterStrategy(), 
                                            array_combine($configRecommendationsEngines,array_map(null,$configRecommendationsEnginePercents,$configRecommendationsEngineFilters)),
                                            array_combine($configRecommendationsEngines,$configRecommendationsjsonParams),
                                            $logger,
                                            new AB_Aesthetics(
                                                new SessionSpecificFilterStrategy(),
                                                    array_combine($configAestheticsTypes,array_map(null,$configAestheticsPercents,$configAestheticsFilters)),
                                                    array_combine($configAestheticsTypes,$configAestheticsjsonParams),
                                                    $logger,
                                                    NULL));
                    $ids = $ab_recomendation->fetchResultsForUserFromContainingUsers($this->userId, $this->gender , $this->age, $this->location, $this->city, -1,$containingUsers,$passedFreshLimit,$passedLikeLimit);
                    $recommendations_data = array();
                    $recommendations_data['recommendations'] = $ids[0];
                    $recommendations_data['likedIds'] = $ids[1];
                    return $recommendations_data;
	}

	public function getActionCount(){
		return $this->redis->GET($this->matchCountKey);
	}

	public function processIds($ids){
		return  $this->uu->processMatchSet($ids);
	}
	
	public static function socketEnabledCheck($mobile_header_values,$systemFlags){
		$mobile_version_code=intval($mobile_header_values['app_version_code']);
		
		//default false
		$socket_enabled=false;

        if( $mobile_header_values['source'] == "iOSApp" && $mobile_version_code == 240)
        {
            $os_version = $mobile_header_values['osversion'] ;
            if($os_version < "8")
                return false ;
        }
		if(isset($systemFlags["is_socket_enabled"]) && $systemFlags["is_socket_enabled"] == "no")
		{
			return false;			
		}
		if(isset($systemFlags["is_socket_enabled"]) && $systemFlags["is_socket_enabled"] == "yes")
		{
			return true;
		}
               if($mobile_header_values['source']=="iOSApp"){

                        if($mobile_version_code>=139){
                                $socket_enabled=true;
                        }
                }
		if($mobile_header_values['source']=="androidApp"){
			
			if($mobile_version_code>=79){
				$socket_enabled=true;
			}
		}
	return $socket_enabled;	
	}
}
