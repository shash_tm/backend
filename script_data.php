<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;


try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
	$industry_arr = array("1"=>29,"2"=>1,"3"=>29,"4"=>29,"5"=>8,"6"=>23,"7"=>21,"8"=>2,"9"=>7,"10"=>20,"11"=>5,"12"=>6,"13"=>14,"14"=>9,"15"=>10,"16"=>12,"17"=>13,"18"=>15,"19"=>14,"20"=>16,"21"=>27,"22"=>18,"23"=>20,"24"=>15,"25"=>14,"26"=>25,"27"=>11,"28"=>29,"29"=>15,"30"=>29,"31"=>29,"32"=>16,"33"=>24,"34"=>19,"35"=>26,"36"=>29);
	
	$degree_arr = array("1"=>1,"2"=>18,"3"=>3,"4"=>4,"5"=>17,"6"=>6,"7"=>5,"8"=>15,"9"=>15,"10"=>12,"11"=>19,"12"=>38,"13"=>23,"14"=>24,"15"=>37,"16"=>26,"17"=>25,"18"=>35,"19"=>35,"20"=>32,"21"=>39,"22"=>21,"23"=>20,"24"=>40,"25"=>40,"26"=>42,"27"=>41);
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute("select ud.user_id as user_id ,ud.DateOfBirth as DateOfBirth ,
			ud.marital_status as marital_status,ud.haveChildren as haveChildren,ud.religion as religion,
			ud.mother_tongue as mother_tongue,ud.stay_country as stay_country,ud.stay_state as stay_state,
			ud.stay_city as stay_city,ud.stay_city_display as stay_city_display
			,uw.working_area as working_area,
			uw.industry as industry,uw.industry_details as industry_details,
			uw.income_start as income_start,
			uw.income_end as income_end,
			uw.education as education,uw.education_details as education_details,
			ut.height as height,ut.smoking_status as smoking_status ,ut.food_status as food_status,ut.drinking_status as drinking_status,ud.registered_at as tstamp 
			from user_demography ud LEFT JOIN user_work uw on ud.user_id=uw.user_id 
			LEFT JOIN user_trait ut on ud.user_id=ut.user_id");
	
	$data = $sql->GetRows();
	
	foreach($data as $key=>$value){
			if($data[$key]["religion"]==1){
				$data[$key]["religion"] = 'Hindu';
			}else if($data[$key]["religion"]==2){
				$data[$key]["religion"] = 'Muslim';
			}else if($data[$key]["religion"]==3){
				$data[$key]["religion"] = 'Sikh';
			}else if($data[$key]["religion"]==4){
				$data[$key]["religion"] = 'Jain';
			}else if($data[$key]["religion"]==5){
				$data[$key]["religion"] = 'Christian';
			}else if($data[$key]["religion"]==6){
				$data[$key]["religion"] = 'Other';
			}else if($data[$key]["religion"]==9){
				$data[$key]["religion"] = 'Buddhist';
			}
			
			if($data[$key]["working_area"]!=NULL){
				if($data[$key]["working_area"]=='Not Working'){
					$data[$key]["working_area"]='no';
				}else{
					$data[$key]["working_area"]='yes';
				}
			}
			
			if($data[$key]["smoking_status"]=="Often"){
				$data[$key]["smoking_status"] = 'Habitually';
			}
			
			if($data[$key]["drinking_status"]=="Often"){
				$data[$key]["drinking_status"] = 'Habitually';
			}
			
			if($data[$key]["food_status"]=="Non-Veg"){
				$data[$key]["food_status"] = 'Non-Vegetarian';
			}else if($data[$key]["food_status"]=="Eggetarion"){
				$data[$key]["food_status"] = 'Eggetarian';
			}else if($data[$key]["food_status"]=="Veg"){
				$data[$key]["food_status"] = 'Vegetarian';
			}
			
			if($data[$key]['income_start']!=null){
				if($data[$key]['income_start']>=0 && $data[$key]['income_start']<10){
					$data[$key]['income_start']=0;
				}else if($data[$key]['income_start']>=50){
					$data[$key]['income_start']=50;
				}
			}
			
			if($data[$key]['income_end']!=null){
				if($data[$key]['income_end']>50){
					$data[$key]['income_end']=51;
				}else if($data[$key]['income_end']<=10){
					$data[$key]['income_end']=10;
				}
			}
			
			if($data[$key]['industry']!='' && $data[$key]['industry']!=null){
				$industry = explode(',',$data[$key]['industry']);
				$data[$key]['industry'] = $industry_arr[$industry[0]];
			}else 
				$data[$key]['industry'] = null;
			
			if($data[$key]['education']!='' && $data[$key]['education']!=null){
				$education = explode(',',$data[$key]['education']);
				$data[$key]['education'] = $degree_arr[$education[0]];
			}else 
				$data[$key]['education'] = null;
			
			$industry_details = json_decode($data[$key]['industry_details'],true);
			
			if($industry_details['designations'][0]!='' && $industry_details['designations'][0]!=null){
				$designation = $industry_details['designations'][0];
			}else{
				$designation = null;
			}
			
			$companies = array();
			for($i=0;$i<count($industry_details['companies']);$i++){
				if($industry_details['companies'][$i]!=null){
					$companies[] = $industry_details['companies'][$i];
				}
			}
			if(empty($companies)){
				$company = null;
			}else
				$company = json_encode($companies);
			
			
			$education_details = json_decode($data[$key]['education_details'],true);
			
			$institutes = array();
			for($i=0;$i<count($education_details['institute']);$i++){
				if($education_details['institute'][$i]!=null){
					$institutes[] = $education_details['institute'][$i];
				}
			}
			if(empty($institutes)){
				$institute = null;
			}else
				$institute = json_encode($institutes);
			
		
		$conn->Execute($conn->Prepare("insert into user_data(user_id,DateOfBirth,marital_status,haveChildren,religion,mother_tongue,
				stay_country,stay_state,stay_city,height,smoking_status,drinking_status,food_status,income_start,income_end,industry,
				designation,work_status,company_name,highest_degree,institute_details,tstamp,stay_city_display) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"),
				array($data[$key]['user_id'],$data[$key]['DateOfBirth'],$data[$key]['marital_status'],$data[$key]['haveChildren'],$data[$key]['religion'],
						$data[$key]['mother_tongue'],$data[$key]['stay_country'],$data[$key]['stay_state'],$data[$key]['stay_city'],$data[$key]['height'],
						$data[$key]['smoking_status'],$data[$key]['drinking_status'],$data[$key]['food_status'],$data[$key]['income_start'],$data[$key]['income_end'],
						$data[$key]['industry'],$designation,$data[$key]['working_area'],$company,$data[$key]['education'],$institute,$data[$key]['tstamp'],$data[$key]['stay_city_display']));
	}
	
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}

?>