<?php

/**
 * Utility class for array manipulation functionalitites.
 */
class ArrayUtils {
    
    /*
     * The function calculates the percentile value in an array of numbers.
     * @param - $array - array of numbers.
     * @param - $percentile - the percentile to be computed.
     * @result - The value at that percentile
     */
    function percentile($array,$percentile) {
        sort($array);
        $index = ($percentile/100) * count($array);
        if (floor($index) == $index) {
            $result = ($array[$index-1] + $array[$index])/2;
        }
        else {
            $result = $array[floor($index)];
        }
        return $result;
    }

    /*
     * The function pushes a value at the end of an array.
     * Used for initiations inside an array of arrays.
     * @param - $arr = input array.
     * @param - $val = input value to be pushed.
     * @result - new final array.
     */
    public static function concat($arr,$val) { 
        if(sizeof($arr) == 0) {
            return array($val);
        }
        else {
            array_push($arr,$val);
            return $arr;
        }
    }
    
    /*
     * The function is used to compute summary statistics of an array.
     * @param - $arr - input array.
     * @result - array(min,25%,50%,mean,75%,max,count) 
     */
    public static function standardSummary($arr) {
        $size = sizeof($arr);
        if($size > 0) {
            sort($arr);
            return array(
                $arr[0],
                ArrayUtils::percentile($arr,0.25),
                ArrayUtils::percentile($arr,0.5),
                round(array_sum($arr)/$size,1, PHP_ROUND_HALF_UP),
                ArrayUtils::percentile($arr,0.75),
                $arr[$size-1],
                $size);
        }
        else {
            return array();
        }
    }
    
    /*
     * Inserts a value at the given position.
     * @param - $array - array in which the element is to be inserted.
     * @param - $pos: The position where the inserted value should be inserted. 
     * @param - $value: The value that should be inserted.
     * @result - changes will be made in place
     */
    public static function insertValueAtPos(array &$array, $pos, $value) {
        $maxIndex = count($array);

        if ($pos === 0) {
            array_unshift($array, $value);
        } elseif (($pos > 0) && ($pos <= $maxIndex)) {
            $firstHalf = array_slice($array, 0, $pos);
            $secondHalf = array_slice($array, $pos);
            $array = array_merge($firstHalf, array($value), $secondHalf);
        }
    }
}