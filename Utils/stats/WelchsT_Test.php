<?php

/**
 * Welch's t-test implementation.
 * The test is used to test the hypothesis that the two populations/samples have equal means or not.
 * Welch's test is more reliable when th two samples have unequal variances and unequal sample sizes
 * Please refer to : https://en.wikipedia.org/wiki/Welch%27s_t_test
 * P.S. The library has a dependency on the PHP stats module for cdf of t distribution.
 * Installtion using ("pecl install stats") needs to be done.
 */
class WelchsT_Test {
    
    /*
     * The function computes the statistics (p-value and stuff) required for 
     * NULL hypothesis validation using the Welch's method.
     * @param - array1 - array representation of the first sample
     * @param - array2 - array representation of the second sample
     * @return - array
     *              t_stats = t statistic representing the deviation of the difference in means of the 2 sample w.r.t t-distribution
     *              df      = degree of freedom
     *              p-value = probability of observing the data as extreme as this purely by chance.
     */
    static function computeStatistics($array1,$array2) {
        $result = array();
        $count1     = count($array1);
        $count2     = count($array2);
        
        if($count1 > 1 and $count2 > 1) {
            $mean1      = array_sum($array1)/$count1;
            $mean2      = array_sum($array2)/$count2;
            $difference = $mean1-$mean2;
            $meandifference = abs($difference);
        
            foreach ($array1 as $arr1) {
                $var1[] = pow($arr1-$mean1,2);
            }
            $std1 = sqrt((array_sum($var1))/($count1-1));
            foreach ($array2 as $arr2) {
                $var2[] = pow($arr2-$mean2,2);
            }
            $std2 = sqrt((array_sum($var2))/($count2-1));
        
            $var1 = $std1*$std1;
            $var2 = $std2*$std2;
            $stdError1 = $var1/$count1;
            $stdError2 = $var2/$count2;

            $sumStdError=$stdError1 + $stdError2;
            $SqrtSumStdError=sqrt($sumStdError);
            
            if($SqrtSumStdError > 0 and $stdError1 > 0 and $stdError1 > 0) {
                $tvalue=$meandifference/$SqrtSumStdError;
        
                $df=($sumStdError*$sumStdError)/((($stdError1*$stdError1)/($count1-1))+(($stdError2*$stdError2)/($count2-1)));
   
                $result["t_stats"]  = $tvalue;
                $result["df"]       = $df;
                $result["p_value"]  = (1-stats_cdf_t($tvalue,$df,1))*2;
            }
        }
        return $result;
    }
}