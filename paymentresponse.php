<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/Payment.class.php";

$data = $_POST;

if(isset($data['hash'])){
	$hash = hash('sha512',$config['payment']['salt']."|".$data['status']."||||||||||".$data['udf1']."|".$data['email']."|".$data['firstname']."|".$data['productinfo']."|".$data['amount']."|".$data['txnid']."|".$data['key']);
	if($hash != $data['hash']){
		trigger_error('Hash not matched :'.json_encode($data));
		header("Location:".$baseurl."/payment.php");
		exit();
	}
}else{
	trigger_error('Hash not set :'.json_encode($data));
	header("Location:".$baseurl."/payment.php");
	exit();
}

try{
$user = functionClass::getUserDetailsFromSession ();
$response = false;
$payment = null;

if(isset($user['user_id'])){
		$payment = new Payment($user["user_id"]);
		$response = $payment->handleTransaction($data);
}else{
	$conn->Execute($conn->Prepare("insert into user_payment_callback (trnx_id,user_id,response,status,plan_id) values (?,?,?,?,?)"),
							array($data['txnid'],'-1',json_encode($data),'error',$data['productinfo']));
							echo 'exec';
}

functionClass::redirect ( 'payment' );


	functionClass::refreshSession($user['user_id']);
	$header= $payment->getHeaderValues($user['user_id']);
	$smarty->assign('header',$header);
	if($response){
		$smarty->assign('response',$response);
	}else{
		$smarty->assign('response',"Some error occured. We'll check and respond");
	}
	$smarty->display ("templates/paymentresponse.tpl" ) ;
}catch(Exception $e){
	trigger_error($e->getMessage());
		//echo $e->getMessage();
}

?>