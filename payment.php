<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/Payment.class.php";

$data = $_POST;
$func=new functionClass();
$user = functionClass::getUserDetailsFromSession ();
functionClass::redirect ( 'payment' );
$payment = new Payment($user["user_id"]);

$userId= $user["user_id"];
if(isset($userId)){
	$eventTrack = new EventTracking($userId);
	$eventTrack->logAction("payments");
}
$data = $_POST;

$payment_plans = $payment->getPaymentPlans($_SESSION ['admin_id']);

if(isset($_GET['plan'])){
	$planId = $_GET['plan'];
}elseif(isset($data['planId'])){
	$planId = $data['planId'];
}else{
	$planId = $payment_plans[0]['plan_id'];
}

$userDetails = $payment->getUserDetails();

if(isset($data['action'])&&$data['action'] == 'getSrcWithData'){
	//$eventTrack = new EventTracking($userId);
	$eventTrack->logAction("pay-now", null, $data['planId']);

	$userOb['name'] = $userDetails['fname'];
	//if(!isset($data['email'])){
		$data['email'] = $userDetails['email_id'];
	//}
	/**
	 * Sending phone number dummy if not present in the system
	 * @Himanshu
	 */
	//if(!isset($data['phone'])){
		if(isset($userDetails['phone']))
			$data['phone'] = $userDetails['phone'];
		else 
			$data['phone'] =  9999999999;
	//}
	$userOb['email'] = $data['email'];
	$userOb['phone'] = $data['phone'];
	$trnxId = $payment->requesttrnxId($planId , $userOb);
	print_r(json_encode(array("status"=>"success","src"=>$config['payment']['iframe_url']."?txtid=".$trnxId."&key=".$config['payment']['merchant_key'])));
	die;
}

/*if($userDetails['email_id']!=null && isset($userDetails['phone'])){
	$userOb['name'] = $userDetails['fname'];
	$userOb['email'] = $userDetails['email_id'];
	$userOb['phone'] = $userDetails['phone'];
	$trnxId = $payment->requesttrnxId($planId , $userOb);
	if($trnxId == null){
		$trnxId = 0;
	}
}else{
	$trnxId = null;
}*/

if(isset($data['action'])&&$data['action'] == 'getSrc'){

	$trnxId = $payment->requesttrnxId($planId , $userOb);
	if($trnxId!=null){
		print_r(json_encode(array("status"=>"success","src"=>$config['payment']['iframe_url']."?txtid=".$trnxId."&key=".$config['payment']['merchant_key'])));
	}else{
		print_r(json_encode(array("status"=>"fail")));
	}
	die;
}


// if(!(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')){
// 	header("Location:".$baseurl_https."/trustbuilder.php");
// }

$device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
if ($device_type == 'mobile') {
	$authentication = functionClass::redirect ( 'trustbuilder', true );
	if ($authentication ['error'] != null)
		echo json_encode ( $authentication );
} else {
// 	functionClass::redirect ( 'trustbuilder' );
}

try {
	$user_id = $user ['user_id'];
	$header= $payment->getHeaderValues($user_id);
} catch ( Exception $e ) {
	trigger_error($e->getMessage());
// 	echo $e->getMessage();
}

if($device_type == 'html'){
	try{
		$smarty->assign('header',$header);
		$smarty->assign('activeplan',$planId);
		$smarty->assign('paymentPlans',$payment_plans);
		/*echo $planId;die;
		echo $trnxId; die;*/
		if($trnxId != null){
			$smarty->assign('iframesrc',$config['payment']['iframe_url']."?txtid=".$trnxId."&key=".$config['payment']['merchant_key']);
			$smarty->assign('userStatus','complete');
		}else{
			$smarty->assign('iframesrc',"");
			$smarty->assign('userStatus','incomplete');
			if($userDetails['email_id']==null){
				$smarty->assign('useremail','');
			}
			if(!isset($userDetails['phone'])){
				$smarty->assign('userphone','');
			}
		}
		$smarty->display ("templates/payment.tpl" ) ;
	}catch(Exception $e){
		trigger_error($e->getMessage());
// 		echo $e->getMessage();
	}
}else{
	echo "hi";
}
?>
