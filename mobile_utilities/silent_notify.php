<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/pushNotification.php";
require_once dirname ( __FILE__ ) . "/feedbackApns.php";


/* Author: Sumit
 * Cron Job to track uninstall
 */

function checkAndroidUninstall()
{
	global $admin_id, $conn_reporting;
	//$e_status="'uninstall','canonical','expired','missing'";
	$e_status="'install','login','logout'";
	$sql ="SELECT registration_id, device_id FROM user_gcm_current where  status   IN ($e_status)  AND app_version_code>=38  and (source='androidApp' or source is null) ";
	//$sql ="SELECT registration_id, device_id FROM user_gcm_current where  app_version_code>=38  OR status='install' ";
	$silentnoti = new pushNotification();
	$countpas=0;
	$countfail=0;
	$countuni=0;
	$countmis=0;
	$countcan=0;
	$res = $conn_reporting->Execute($sql);
	$allcalls = $res->RowCount();
	$current_count = 0 ;
	$i=0;
	$regIDs = array();
	

	while ($col = $res->FetchRow())
		{
		 $current_count++ ;

		 $regIDs[$i] = $col['registration_id'];
		 $i++; 
	
		 if ($i == 100 || $allcalls == $current_count)
			{
				$count=$silentnoti->sendSilentNotification($regIDs, array("push_type"=>"SILENT"),$admin_id);
				$countpas=$countpas+$count["success"];
				$countfail=$countfail+$count["fail"];
				$countuni=$countuni+$count["uninstall"];
				$countmis=$countmis+$count["missing"];
				$countcan=$countcan+$count["canonical"];
				$i =0 ;
				$regIDs = array();
              }
         }
         
         return array ( "all"=> $allcalls,
   				  "success"=> $countpas,
                    "fail" => $countfail,
   				"uninstall"=> $countuni,
   				  "missing"=> $countmis,
   				"canonical"=> $countcan );

}
$count_android= checkAndroidUninstall();
$message = "All android  ".$count_android['all'].
      "<br>"."Total success ".$count_android['success'].
      "<br>"."Failures ".$count_android['fail'].
      "<br>"."Uninstalls ".$count_android['uninstall'].
	  "<br>"."Missing Registrations ".$count_android['missing'].
	  "<br>"."Canonical ".$count_android['canonical'];

$feedback = new feedbackApns();
$count = $feedback->receiveFeedback();
$message .= "<br>"."Total IOS uninstalls ".$count;

$to = "sumit.kumar@trulymadly.com,shashwat@trulymadly.com";
//$to = "sumit.kumar@trulymadly.com";
$subject = "Uninstall statistics for ". Utils::GMTTOISTfromUnixTimestamp(time());
$from = "sumit.kumar@trulymadly.com";
Utils::sendEmail($to, $from, $subject, $message,true);




?>
