<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/User.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/pushNotification.php";
require_once dirname ( __FILE__ ) . "/../geoIPTM.php";

class Gcm {
	private $source;
	private $app_version_code;
	private $functionClass ;
	private $conn ;
	private $country_id;
	
	function __construct()
	{
		global $conn ;
		$this->functionClass =   new functionClass();
		$this->app_version_code = $this->functionClass->getHeader ();
		$this->source = $this->functionClass->getSourceFromHeader ();
		$this->conn = $conn ;
		$this->country_id = $this->getIPCountry();
	}

	private function getIPCountry()
	{
		try{
			$country_name = geoIPTM::getCountryName();
			if($country_name == 'India')
				$country_id = 113 ;
			else if($country_name =='United States')
				$country_id = 254 ;

			if(!isset($country_id))
			{
				$sql ="Select country_id from geo_country where name = ?";
				$country_data = Query_Wrapper::SELECT($sql,array($country_name),Query::$__slave,true);
				$country_id = $country_data['country_id'];
			}

			return $country_id;
		}
		catch (Exception $e)
		{
			trigger_error($e->getMessage(),E_USER_WARNING);
			return null;
		}
	}

	public function checkStatus($user_id, $request_data)
    {     
    	
	     if($request_data['status'] == 'install' && isset($user_id))
	       {
	    	//if status is install and user_id is set then perform no update
	    	$response ['responseCode'] = 200;
				print_r ( json_encode ( $response ) );
		     	return ;
	       }  
	       else if ($request_data['status'] == 'login' && !isset($user_id))
	       {
	       	$response ['responseCode'] = 403;
				print_r ( json_encode ( $response ) );
		     	return ;
	       } else 
	       {
	            $this->saveStatus($user_id, $request_data);
	       
	       }    
    }

    private function saveStatus ( $user_id, $request_data)
    { 
    	 $updated = false ;
    	
		if ($this->source == "windows_app")
		{
			//update the row if it returns a number then row exists else follow an insert
			$updated = $this->__replacePreviosIdWindows($user_id, $request_data);
										
		}
		
	if($updated == false)
	{ 
		$sql = $this->conn->Prepare ( "insert into user_gcm_current(user_id,registration_id,tstamp,device_id,status,app_version_code,source, install_tstamp,country_id)
									   values(?,?,now(),?,?,?,?, now(),? ) on duplicate key update
										user_id=?,tstamp=now(),device_id=?,status=?,app_version_code=?,source=?" );
		$this->conn->Execute ( $sql, array (
				$user_id, 
				$request_data['registration_id'],
				$request_data['device_id'],
				$request_data['status'],
				$this->app_version_code,
				$this->source,
				$this->country_id,
				$user_id,
				$request_data['device_id'],
				$request_data['status'],
				$this->app_version_code ,
				$this->source
		) );
		$response ['responseCode'] = 200;
		print_r ( json_encode ( $response ) );
		return ;
		} else 
		{
			return ;
		}
    }
    
	public function __replacePreviosIdWindows ($user_id, $request_data)
	{  	
           $wusql = $this->conn->Prepare("UPDATE `user_gcm_current` 
							SET `user_id`= ?, `registration_id` = ? , `status` = ?, `app_version_code`= ?, `tstamp` = now() 
							WHERE `device_id` = ? AND `source` = ? ");
			$wures = $this->conn->Execute($wusql, array (
											$user_id,
											$request_data['registration_id'],
											$request_data['status'],
											$this->app_version_code,
											$request_data['device_id'],
											$this->source
										) );
		     if($this->conn->Affected_Rows() > 0 )
		     {
		     	//$updated = false;
		     	$response ['responseCode'] = 200;
				print_r ( json_encode ( $response ) );
		     	return true;
		     } 
		     else 
		        return false;
	} 
 
	public function checkRegistrationIdStatus ($registration_id){
		$data = array();
		
		$sql = "select * from user_gcm_current where registration_id = '$registration_id' " ;
		$res = $this->conn->Execute($sql);
		if($res->RowCount()>0)
		{
			$data = $res->FetchRow() ;
		}
		return $data;
	} 

}


try {
//	$user = new User ();
	
	//$pushNotification = new pushNotification ();
	//$app_version_code= functionClass::getHeader();
// 	$functionClass = new functionClass();
// 	$app_version_code = $functionClass->getHeader ();
// 	$source = $functionClass->getSourceFromHeader ();
    
	$gcm = new Gcm();
//	$session = $user->getUserDetailsFromSession ();
	
    if (isset ( $_REQUEST ['registration_id'] ))
        $gcm->checkStatus($_SESSION['user_id'], $_REQUEST );
    
} catch ( Exception $e ) {
	trigger_error ( $e->getMessage (), E_USER_WARNING );
}

?>