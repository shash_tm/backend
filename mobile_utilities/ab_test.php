<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";

/**
 * A|B Handler file for apps
 */
try {
	
 	$functionClass = new functionClass();
 	$app_version_code = $functionClass->getHeader();
 	
 	$response ['responseCode'] = 200;
 	// AB handlers for FORCE_FB A|B scenario
 	if (isset ( $_REQUEST ['ab_type'] ) && $_REQUEST ['ab_type'] == 'FORCE_FB') {
 		$response ['isActive'] = false;
 		//ONLOGIN, BEFOREMATCHES, NONE
 		$randomKey = rand(1, 3);
 		switch ($randomKey) {
 			case 1:
 				$ab_type="ONLOGIN";
 				break;
 			case 2:
 				$ab_type="BEFOREMATCHES";
 				break;
 			case 3:
 				$ab_type="NONE";
 				break;
 		}
 		$response ['ab_type'] = $ab_type;
 	}
 	print_r ( json_encode ( $response ) );
	
} catch ( Exception $e ) {
	trigger_error ( $e->getMessage (), E_USER_WARNING );
}

?>