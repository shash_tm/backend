<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";

/* Author : Tarun
 * sends custom push notifications to different set of users based on certain conditions  */

class newCustomSparkNotifications {
	public $conn_reporting;
	public $admin_id;
	private $imageurl;
	private $eventNotificationMessage;
	public $ticker_text;
	public $title_text;
	public $firstSparkNotified;
	public $joinsForRecommendation;
	public $push_notification;

	function __construct() {
		global $admin_id,$imageurl,$conn_reporting;
		$this->conn_reporting = $conn_reporting;
		$this->admin_id = $admin_id;
		$this->imageurl = $imageurl;
		$this->ticker_text= 'Message from TrulyMadly';
		$this->title_text= 'TrulyMadly';
		$this->sparkNotificationMessage = array ( 0 => array( "title"=>"Things are heating up!","content"=>"You have PENDING_SPARKS Sparks waiting.") ,
			1 => array( "title"=>"Yay! You received your first Spark!","content"=>"Aren’t you curious?") ,
			2 => array( "title"=>"You've got Sparked!","content"=>"Respond quickly or your potential match disappears."),
			3 => array( "title"=>"SENDER_NAME has Sparked you!","content"=>"Say something or you'll miss out!")
		);
		$this->firstSparkNotified = array();
		$this->push_notification = new pushNotification();

		$this->joinsForRecommendation = " join user_search un on un.user_id = us.user2  ".
			" join user_search un1 on un1.user_id = us.user1  ".
			" join user u on u.user_id = us.user1  ".
			" join user_data ud on ud.user_id = us.user1   ".
			" join user_photo up on up.user_id=us.user1 and up.is_profile='yes' and up.admin_approved = 'yes' ";
	}


	/*
	 * 0
	 * Returns list of users and corresponding event ids, which are starting tomorrow !!
	 */
	public function getPendingSparks($user_id, $spark_count, $title){
		if($spark_count > 0) {
			$data = array(
				'title_text' => $this->sparkNotificationMessage[0]['title'],
				'content_text' => $spark_count == 1 ?"You have a Spark waiting." :str_replace("PENDING_SPARKS", $spark_count ,$this->sparkNotificationMessage[0]['content']),
				'type' => 'CONVERSATION_LIST',
				'push_type' => 'CONVERSATION_LIST',
				'category_id' => NULL,
				'categoryTitle' => NULL,
				'event_status' => 'PENDING_SPARKS'
			);
			return $data;
		}
		return array();
	}

	/*
	 * 1
	 * Returns data if first spark recieved.
	 */
	public function getFirstSparkReceivedNotification($user_id){

		$data = array(
			'title_text' => $this->sparkNotificationMessage[1]['title'],
			'content_text' => $this->sparkNotificationMessage[1]['content'],
			'type' => 'CONVERSATION_LIST',
			'push_type' => 'CONVERSATION_LIST',
			'category_id' => NULL,
			'categoryTitle' => NULL,
			'event_status' => 'FIRST_SPARK'
		);
		return $data;
			//	return $res->getRows();

	}

	public function notifyAllSparkReceivers(){
		$count = 0;
		$sql = "SELECT u.fname, us.user1, us.user2 from user_spark us ".$this->joinsForRecommendation.
				" where us.tstamp >= DATE_SUB(now(), INTERVAL 30 MINUTE) and us.tstamp < now() ".
				" and us.status = 'sent' ";

		$res = $this->conn_reporting->Execute($sql);
		$totalResults = $res->RowCount();

		if ($res->RowCount() > 0) {
			while ($row = $res->FetchRow()) {
				$user_id = $row['user2'];
				$sender_name = $row['fname'];
				if(!in_array($user_id, $this->firstSparkNotified)) {		//not to send spark received notification, if first spark notification is received.
					$count++;
					$data = array(
						'title_text' => str_replace("SENDER_NAME", $sender_name, $this->sparkNotificationMessage[3]['title']),
						'content_text' => str_replace("SENDER_NAME", $sender_name, $this->sparkNotificationMessage[3]['content']),
						'type' => 'CONVERSATION_LIST',
						'push_type' => 'CONVERSATION_LIST',
						'category_id' => NULL,
						'categoryTitle' => NULL,
						'event_status' => 'NEW_SPARK'
					);

					$data['ticker_text'] = $this->ticker_text;
					$this->push_notification->notify($user_id,
						$data,
						$this->admin_id);
				}

			}
		}
		$mailOutput = " Total eligible receivers: ".$totalResults;
		$mailOutput .= "  New spark notifications sent: ".$count;

		return $mailOutput;
	}

	public function notifyAllSparksExpiring(){
		$sql = "SELECT u.fname, us.user1, us.user2 from user_spark us ".$this->joinsForRecommendation.
			" where us.expired_date >= DATE_ADD(now(), INTERVAL 30 MINUTE) and us.expired_date < DATE_ADD(now(), INTERVAL 60 MINUTE) ".
			" and us.status = 'seen' ";
		$res = $this->conn_reporting->Execute($sql);

		if ($res->RowCount() > 0) {
			while ($row = $res->FetchRow()) {
				$user_id = $row['user2'];
				$sender_name = $row['fname'];
				if(!in_array($user_id, $this->firstSparkNotified)) {		//not to send spark received notification, if first spark notification is received.
					$data = array(
						'title_text' => str_replace("SENDER_NAME", $sender_name, $this->sparkNotificationMessage[2]['title']),
						'content_text' => str_replace("SENDER_NAME", $sender_name, $this->sparkNotificationMessage[2]['content']),
						'type' => 'CONVERSATION_LIST',
						'push_type' => 'CONVERSATION_LIST',
						'category_id' => NULL,
						'categoryTitle' => NULL,
						'event_status' => 'SPARK_EXPIRING'
					);

					$data['ticker_text'] = $this->ticker_text;
					$this->push_notification->notify($user_id,
						$data,
						$this->admin_id);
				}

			}
		}
	}

}

try {



	if(php_sapi_name() === 'cli') {
		$date= date('Y-m-d',strtotime('today'));
		$sparkNotification = new newCustomSparkNotifications();

		if ($data == null && isset($argv[1]) && $argv[1] != null && $argv[1] == "daily" ) {
			$userSql = "Select un.user_id, count(distinct us.user1) as spark_count from user_spark us ".
				$sparkNotification->joinsForRecommendation.
				" where us.status in ('seen','sent') and (us.expired_date is null or us.expired_date > now())  ".
				" and u.status not in ('incomplete','non-authentic','blocked','suspended') and u.deletion_status is null".
				" group by un.user_id having spark_count > 0 ";
		}

		else if ($data == null && isset($argv[1]) && $argv[1] != null && $argv[1] == "first" ) {
			$userSql = "Select un.user_id, count(*) as spark_count, " .
				" sum(if(us.status='sent' and us.tstamp >= DATE_SUB(now(), INTERVAL 30 MINUTE) , 1, 0)) as `fresh_spark`, " .
				" sum(if(us.status!='sent'  and us.tstamp >= DATE_SUB(now(), INTERVAL 30 MINUTE), 1, 0)) as `old_spark`, " .
				" sum(if(us.tstamp < DATE_SUB(now(), INTERVAL 30 MINUTE) , 1, 0)) as `oldest_spark` " .
				" from user_spark us  " .
				$sparkNotification->joinsForRecommendation.
				" group by un.user_id having fresh_spark > 0 and old_spark = 0 and oldest_spark = 0 ";
		}

		$res = $sparkNotification->conn_reporting->Execute($userSql);

		if ($res->RowCount() > 0) {
			$mailmessage = "Total FIRST Spark notifications sent: ";
			$count=0;
			while ($row = $res->FetchRow()) {
				$user_id = $row['user_id'];
				//start calling notifications in order of preferences
				if ($data == null && isset($argv[1]) && $argv[1] != null && $argv[1] == "first" ) {
					$data = $sparkNotification->getFirstSparkReceivedNotification($user_id);    //first spark notification
					$sparkNotification->firstSparkNotified[] = $user_id;
				}
				//need to make sure following run once a day at our given time
				if ($data == null && isset($argv[1]) && $argv[1] != null && $argv[1] == "daily" ) {														// pending spark notification
					$data = $sparkNotification->getPendingSparks($user_id, $row['spark_count']);
				}
				//	var_dump($data);
				if ($data != null) {
					$count++;
					$data['ticker_text'] = $sparkNotification->ticker_text;
					$sparkNotification->push_notification->notify($user_id,
						$data,
						$sparkNotification->admin_id);
				}
			}


		}
		$mailOutput = "";
		if (isset($argv[1]) && $argv[1] != null && $argv[1] == "first" ) {
			$mailOutput = $sparkNotification->notifyAllSparkReceivers();
			$sparkNotification->notifyAllSparksExpiring();
		}
		$mailOutput .= " ----   " .$mailmessage." : ".$count;
		$to = "tarun@trulymadly.com";
		$subject = "Spark Notifications sent ".$date;
		$from = "admintm@trulymadly.com";
		Utils::sendEmail($to, $from, $subject, $mailmessage." : ".$count,true);
	}

} catch (Exception $e) {
	echo $e->getTraceAsString();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	Utils::sendEmail($techTeamIds .",backend@trulymadly.com", "sumit@trulymadly.com", "Spark Notification script failed".$baseurl, $e->getTraceAsString());
}


?>