<?php
require_once dirname ( __FILE__ ) . "/sparkNotificationCrons.php";

if(php_sapi_name() === 'cli')
{
    $options = getopt("d:");

    if (isset($options[d])) {
        $day = $options[d];
    } else {
        $day = date('w', strtotime('today'));
    }
    // run only on saturday
    if($day == 6)
    {
        $sparkCrons = new SparkNotificationCrons();
        $no_spark_purchased = $sparkCrons->sparkNeverPurchased($day);
        $mailmessage .= "Spark never purchased ".$no_spark_purchased;

        $no_spark_purchased = $sparkCrons->sparkExpired($day);
        $mailmessage .= "Sparks Purchased but Expired ".$no_spark_purchased;

    }
    echo $mailmessage;
}

?>