<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";

/* Author : Sumit
    Needs to be run every 5 minutes
 * sends custom push notifications to different set of users based on certain conditions  */

class TriggerNotifications
{
    private $conn_reporting;
    private $admin_id;
    private $imageurl;
    private $ticker_text = 'Message from TrulyMadly';
    private $title_text = 'TrulyMadly';
    public $count_sent;
    private $isttime;
    private $yesterday;
    private $pushNotification;
    private $now;
    private $timeStarts;
    private $timeEnds;
    private $paymentDropOffData;
    private $lockFile;
    private $fptr;

    function __construct()
    {
        global $admin_id, $imageurl, $conn_reporting;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->lockFile =  "/tmp/trigger";
        $this->admin_id = $admin_id;
        $this->imageurl = $imageurl;
        $this->count_sent = 0;
        $this->isttime = date('Y-m-d H:i:s', strtotime('+330 minutes'));
        $this->yesterday = date('Y-m-d 18:30:00', strtotime('-1 day', strtotime($this->isttime)));
        $this->now = date('Y-m-d H:i:s');
        $this->timeEnds = date('Y-m-d H:i:s', strtotime('-15 minutes'));
        $this->timeStarts = date('Y-m-d H:i:s', strtotime('-30 minutes'));
        $this->pushNotification = new pushNotification();
    }

    public function checkIfAlreadyRunning()
    {
        $this->fptr = fopen($this->lockFile, "c");

        if (flock($this->fptr, LOCK_EX|LOCK_NB, $wouldblock))
//		  if (flock($fp, LOCK_EX | LOCK_NB))
        {
            $isLocked = 0 ;
            // lock obtained
            if(flock($this->fptr,LOCK_EX) == false){
                throw new Exception("unable to acquire lock");
            }else{
                echo "lock acquired";
                return true;
            }
        }
        else
        {
            if ($wouldblock)
            {
                echo "already locked";
            }
            else
            {
                echo "could not lock";
            }
            return false;

        }

        return $isLocked;
    }

    private function getPaymentDropOffPayload($type="spark", $gender)
    {
        if($type == "spark") {
            $content_text = $gender == "M" ? "Don't miss out on this chance to grab her attention! Complete your Spark purchase now."
                : "Don't miss out on this chance to grab his attention! Complete your Spark purchase now.";
            $this->paymentDropOffData = array('title_text' => $this->title_text,
                'ticker_text' => $this->ticker_text,
                'content_text' => $content_text,
                'push_type' => 'BUY_SPARKS',
                'event_status' => 'SPARK_PAYMENT_DROP_OFFS');
        }else if($type=="select"){
            $content_text = $gender == "M" ? "The One is waiting. Complete your TM Select purchase to meet her now!"
                : "Mr. Right is waiting. Complete your TM Select purchase to meet him now!";
            $this->paymentDropOffData = array('title_text' => $this->title_text,
                'ticker_text' => $this->ticker_text,
                'content_text' => $content_text,
                'push_type' => 'BUY_SELECT',
                'event_status' => 'SELECT_PAYMENT_DROP_OFFS');
        }
    }

    /*
     * To send notifications to those users who visited the payment page but did't make any payments.
     * Exclude users who have purchased a spark today.
     */
    public function paymentDropOff()
    {
        $sql = "Select u.user_id, u.gender, t.package_type from (select ust.user_id, sp.type as package_type, count(*) as all_t, sum(if(ust.status='consumed',1,0)) as consumed,
              sum(if(ust.status='initiated' && ust.tstamp<'$this->timeEnds',1,0)) as initiated from user_spark_transaction ust
              join spark_packages sp on sp.package_id=ust.package_id
              where ust.tstamp >='$this->timeStarts' group by ust.user_id, package_type having initiated>0 and consumed =0)t
                 join user u on u.user_id = t.user_id";
        $res = $this->conn_reporting->Execute($sql);
        while ($row = $res->FetchRow())
        {
            $user_id = $row['user_id'];
            $package_type = $row['package_type'];
            $gender = $row['gender'];
            $this->getPaymentDropOffPayload($package_type, $gender);
            $this->pushNotification->notify($user_id, $this->paymentDropOffData, 'payment_drop_off');
            //echo PHP_EOL . "Sent to " . $user_id;
        }

//        $user_id = 1809;
//        $this->pushNotification->notify($user_id, $this->paymentDropOffData, 'payment_drop_off');
//        echo PHP_EOL . "Sent to " . $user_id;
    }

    public function registrationDropOff()
    {
        $sql = "select distinct(u.user_id), u.fname from user u join user_gcm_current ugc on ugc.user_id=u.user_id
 				where  u.status ='incomplete' and ugc.status in ('login','logout') and u.registered_at >= '$this->timeStarts'
 				and u.registered_at < '$this->timeEnds' ";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $user_id = $row['user_id'];
            $name = $row['fname'];
            $data= "Hey ".$name."! You're almost done with registration. Finish it to see who's waiting to say HI !";
            $this->pushNotification->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,
                "event_status" => "REG_DROP_OFFS_TRIGGERED", "title_text"=>$this->title_text,"push_type"=>"PROMOTE"),'REG_DROP_OFFS_TRIGGERED');
                echo "Sending to $user_id".PHP_EOL;
        }
    }


}

try {

    if(php_sapi_name() === 'cli')
    {
        $triggerNotification = new TriggerNotifications();
        if( $triggerNotification->checkIfAlreadyRunning() == true )
        {
            $triggerNotification->paymentDropOff();
            $triggerNotification->registrationDropOff();
        }
        else
        {
            echo "Script already running";
        }

    }

} catch (Exception $e) {
    echo $e->getMessage();
    trigger_error($e->getMessage(), E_USER_WARNING);
    Utils::sendEmail("sumit@trulymadly.com", "sumit@trulymadly.com", "Trigger Notification script failed".$baseurl, $e->getMessage());
}


?>