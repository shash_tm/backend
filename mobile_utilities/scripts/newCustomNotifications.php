<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/sparkNotificationCrons.php";

/* Author : Sumit 
 * sends custom push notifications to different set of users based on certain conditions  */
class newCustomNotifications {
	private $conn_reporting;
	private $admin_id;
	private $imageurl;
	private $pushNotificationObject;
	private $photoNotificationMessage;
	 

	function __construct() { 
		global $admin_id,$imageurl,$conn_reporting;
		$this->conn_reporting = $conn_reporting;
		$this->admin_id = $admin_id;
		$this->imageurl = $imageurl;
		$this->pushNotificationObject = new pushNotification();
		$this->ticker_text= 'Message from TrulyMadly';
		$this->title_text= 'TrulyMadly'; 
		$this->photoNotificationMessage = array ( 0 => "Hey! You're missing out on your great matches. Upload your pics!" ,
		                                          1 => "We can't see you and so can't your awesome matches :(  Upload your pics, please!",
		                                          2 => "Your pics are supersafe with us, promise!" ,
		                                          3 => "Want to go hush hush? Check our Profile Discovery settings.",
		                                          4 => "Have no fear! Your pics are in safe hands.",
		                                          5 => "Want to be seen only to the guys you've liked? Check our Profile Discovery settings." );
	}
 
	/*Sends push notifications to users who have unread messages and unseen mutual likes since last_login 
	 * $day = no. of days since last login */

	public function messagesMutualLikes ($day){
		$pic_url= $this->getTMLogo();
		$day2= $day + 1;
		$successIds= "";
		$count=0;

		$sql="select distinct(u.user_id),u.fname, ml_count as mutual_likes, msg_count from user u 
		join user_gcm_current ups on u.user_id=ups.user_id 
		join user_lastlogin ull on u.user_id=ull.user_id 
		join (select receiver_id, sum(if(msg_type='MUTUAL_LIKE',1,0)) AS ml_count , sum(if(msg_type != 'MUTUAL_LIKE',1,0)) as msg_count 
		from  messages_queue where tstamp >  DATE_SUB(CURDATE(), INTERVAL $day2 DAY) AND last_seen < tstamp and blocked_by is null 
		and msg_type != 'SPARK' group by receiver_id )t on t.receiver_id = u.user_id
		 where  ups.status in ('login','logout')  and  ull.last_login <  DATE_SUB(CURDATE(), INTERVAL $day DAY) and  
 		 ull.last_login >  DATE_SUB(CURDATE(), INTERVAL $day2 DAY)"; 

		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0){
			while ($row = $res->FetchRow()){
				$user_id = $row['user_id'];
				$row['mutual_likes'];
				$row['msg_count'];
				$data= $row['fname'].", you have ";
				//compose message for new messages and new mutual likes both
				if ($row['msg_count'] >0 && $row['mutual_likes'] >0) {
					$data .= $row['msg_count'];
					if($row['msg_count'] == 1)
					$data  .= " unread message and ";
					else 
					$data .= " unread messages and ";
					
					$data .= $row['mutual_likes'];
					if($row['mutual_likes'] == 1)
					$data  .= " new mutual like !"; 
					else 
					$data .= " new mutual likes !";	
				} else if ( $row['msg_count'] >0 ){
					//compose message for new messages only 
					if($row['msg_count'] == 1)
					$data .= $row['msg_count']." unread message !";
					else 
					$data .= $row['msg_count']." unread messages !";
				} else {
					//compose message for  new mutual likes only 
					if ($row['mutual_likes'] == 1)
					$data .= $row['mutual_likes']." new mutual like !";
					else 
					$data .= $row['mutual_likes']." new mutual likes !";	
				}
				//echo $data.PHP_EOL;
				$this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "messages_mutual_likes_".$day."days","title_text"=>$this->title_text,"push_type"=>"CONVERSATION_LIST"),$this->admin_id);
				$successIds .= $user_id.",";
				$count++;
			}
		}
		$successIds = rtrim($successIds, ",");
		return array("count"=> $count, "ids"=>$successIds);
			
	}
	//get TrulyMadly Logo to be shown to the user as sender DP
	private function getTMLogo(){
		$uu = new UserUtils();
		$user_data = $uu->getNamenPic($this->admin_id);
		if(isset($user_data['thumbnail']))
		$pic_url = $this->imageurl.$user_data['thumbnail'];
		return $pic_url;
	}



     /*send notifications to users who registered 7 days ago and have low PI*/
	public function authLowPI($day) {
		$pic_url= $this->getTMLogo();
		$day2= $day + 1;
		$successIds= "";
		$count=0;
		$sql= "select distinct(u.user_id), u.fname,last_14_days_visibility as visibility from user u join user_gcm_current ugc on ugc.user_id=u.user_id
join user_pi_activity upa on u.user_id=upa.user_id
where upa.bucket in (1,2) and u.status ='authentic' and ugc.status in ('login','logout')  and u.gender='M' and
u.registered_at < DATE_SUB(CURDATE(), INTERVAL $day DAY) and  u.registered_at>=DATE_SUB(CURDATE(), INTERVAL $day2 DAY)";
//    echo $sql;
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0){ 
			while ($row = $res->FetchRow()){
				$user_id = $row['user_id'];
				$data= $row['fname'].",How about giving your profile a makeover? Upload a fresh photo!";
				//$data="hello test notification";
				$this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "low_pi_registered_users", "title_text"=>$this->title_text,"push_type"=>"PHOTO"),$this->admin_id);
				$successIds .= $user_id.",";
				$count++; 
			}
		}
		 
		return array("count"=> $count, "ids"=>$successIds);


	}

	

	
	public function highPIUsers($day)
	{ 	
		$pic_url= $this->getTMLogo();
		$successIds= "";
		$count=0;
		$day2 = $day+1 ;
		$sql= "select distinct(u.user_id), u.fname,u.gender from user u join user_gcm_current ugc on ugc.user_id=u.user_id
join user_pi_activity upa on u.user_id=upa.user_id
join user_lastlogin ull on u.user_id = ull.user_id
where upa.bucket in (4,5) and u.status ='authentic' and ugc.status in ('login','logout')  and
ull.last_login < DATE_SUB(CURDATE(), INTERVAL $day DAY) and  ull.last_login >=DATE_SUB(CURDATE(), INTERVAL $day2 DAY)";
//    echo $sql;
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0){ 
			while ($row = $res->FetchRow()){
				$user_id = $row['user_id'];
				if($day == 3)
					$data= "Wowza! You're turning heads on TrulyMadly. Fire up the app now!";
				else
					$data = "You've created quite a stir on TrulyMadly! Fire up the app and see for yourself!";
				$this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "High_pi_".$day." _days", "title_text"=>$this->title_text,"push_type"=>"PROMOTE"),$this->admin_id);
				$successIds .= $user_id.",";
				$count++; 
			}
		}
		 
		return array("count"=> $count, "ids"=>$successIds);
	}
	
	public function NoProfilePhoto($day)
	{
	     $pic_url= $this->getTMLogo();
	     $successIds= "";
		 $count=0;
		 $no_of_msgs = count($this->photoNotificationMessage);
		 $key = $day % $no_of_msgs ;

		$sql = "select distinct(u.user_id), u.fname from user u join user_gcm_current ugc on ugc.user_id=u.user_id
                left join user_photo up on u.user_id=up.user_id and is_profile='yes' and up.status='active'
                 where  u.status ='authentic' and ugc.status in ('login','logout')  and up.user_id is null and gender='F' " ;
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0){ 
			while ($row = $res->FetchRow()){
				$user_id = $row['user_id'];
				$data= $this->photoNotificationMessage[$key] ;
				//$data="hello test notification";
				$this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "No_Profile_Pic", "title_text"=>$this->title_text,"push_type"=>"PHOTO"),$this->admin_id);
				$successIds .= $user_id.",";
				$count++; 
			}
		}
		 
		return array("count"=> $count, "ids"=>$successIds);
		
	}

	public function incompleteRegistration($day)
	{
		$day2 = $day+1 ;
		$count = 0;
		$pic_url= $this->getTMLogo();
		$sql = "select distinct(u.user_id), u.fname from user u join user_gcm_current ugc on ugc.user_id=u.user_id
 				where  u.status ='incomplete' and ugc.status in ('login','logout') and u.registered_at < DATE_SUB(CURDATE(), INTERVAL $day DAY)
 				and u.registered_at >= DATE_SUB(CURDATE(), INTERVAL $day2 DAY)";
		$res = $this->conn_reporting->Execute($sql);
		while($row = $res->FetchRow())
		{
			$user_id = $row['user_id'];
			$data= "You're a minute away from all those cuties! Finish registering now!";
			$this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,"pic_url"=>$pic_url,
				"event_status" => "incomplete_users", "title_text"=>$this->title_text,"push_type"=>"PROMOTE"),$this->admin_id);
			$count++;
		}
		return $count;
	}

	public function sparkExpireToday()
	{
		$count=0;
		$sql ="select u.user_id from user_spark_counter usc join user u on usc.user_id = u.user_id  join user_app_status uas on uas.user_id = u.user_id
				where expiry_date <= DATE_ADD(now(),INTERVAL 1 DAY)  and expiry_date > now() and u.status='authentic' and uas.android_app='install'
				and usc.spark_count>0";
		$res =  $this->conn_reporting->Execute($sql);
		while($row = $res->FetchRow())
		{
			$user_id = $row['user_id'];
			$data = "Your Sparks are about to expire. Use them wisely! Don't forget to fuel up your pack.";
			$this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,
				"event_status" => "SPARK_EXPIRES_TODAY", "title_text"=>$this->title_text,"push_type"=>"BUY_SPARKS"),"SPARK_EXPIRES_TODAY");
			$count++;
		}
		return $count;
	}

}

try {
	
	if(php_sapi_name() === 'cli'){
	$options=getopt ( "d:y:" );

	if (isset($options[d])){
		$day = $options[d];
	} else {
		$day= date ( 'w' ,strtotime ('today'));
	}
	
		if ($options[y]){
		$day_of_year = $options[y];
	} else {
		$day_of_year = date('z');
	}

	$date= date('Y-m-d',strtotime('today'));
	echo $day;
	$mailmessage ="";
	$customNotifications = new newCustomNotifications();

	$incomplete = $customNotifications->incompleteRegistration(2);
	$mailmessage .= "Incomplete registrations ".$incomplete." <br>";
	$frequency = array(1,3,7);
	foreach ($frequency as $val){
		$messagesMutualLikes = $customNotifications->messagesMutualLikes($val);
		$exIds= $messagesMutualLikes['ids'];
		$mailmessage .= "MessagesMutualLikes_".$val."  ".$messagesMutualLikes['count']."<br>";
	} 


	//$high_pi_freqency_arr = array(3,7);
	//foreach($high_pi_freqency_arr as $val)
//	{
//		$highPI = $customNotifications->highPIUsers($val);
//		$mailmessage .= "High_PI $val days ".$highPI['count']."<br>";
//	}

	$lowPI = $customNotifications->authLowPI(7);
	$mailmessage .= "Low_PI ".$lowPI['count']."<br>";
	
	$noPhoto = $customNotifications->NoProfilePhoto($day_of_year);
	$mailmessage .= "No Photo ".$noPhoto['count']."<br>" ;

	// this notfication is removed as the spark will not expire (for now at least)
//	$spark_expiry = $customNotifications->sparkExpireToday();
//	$mailmessage .= "spark expiry ".$spark_expiry ."<br>" ;

		// if Tuesday then send the notification
	if($day == 2)
	{
		$sparkCrons = new SparkNotificationCrons();
		$no_spark_purchased = $sparkCrons->sparkNeverPurchased($day);
		$mailmessage .= "Spark never purchased ".$no_spark_purchased;

		$no_spark_purchased = $sparkCrons->sparkExpired($day);
		$mailmessage .= "Sparks Purchased but Expired ".$no_spark_purchased;


	}

	$to = "sumit@trulymadly.com";
	$subject = "Custom Push Notifications for ".$date;
	$from = "admintm@trulymadly.com";
	Utils::sendEmail($to, $from, $subject, $mailmessage,true);



	//echo "sent";
	}
} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
	Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "Custom Notification script failed".$baseurl, $e->getMessage());
}


?>