<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";

// to send spark related notifications from crons
class SparkNotificationCrons
{
    private $conn_reporting;
    private $admin_id;
    private $imageurl;
    private $pushNotificationObject;
    private $sparkNeverPurchasedMessages;
    private $sparkExpiredMessages;
    private $chunkSize = 1000;
    function __construct()
    {
        global $admin_id,$imageurl,$conn_reporting;
        $this->conn_reporting = $conn_reporting;
        $this->admin_id = $admin_id;
        $this->imageurl = $imageurl;
        $this->pushNotificationObject = new pushNotification();
        $this->ticker_text= 'Message from TrulyMadly';
        $this->title_text= 'TrulyMadly';
        $this->sparkNeverPurchasedMessages = array(2 => "Now you can grab her attention! Start Sparking now!",
                                                   6 => "A Spark gives you 5x more chances of getting a like back. Send a Spark!");
        $this->sparkExpiredMessages= "We've got new profiles for you to Spark. Fuel up your Spark pack!";

    }

    private function getSparkNeverPurchasedMessage($country)
    {
        if($country = 254)
        {
            $msg_array = array("Grab her attention! Start Sparking now",
                "A Spark gives you 5x more chances of getting a like back. Send a Spark!",
                "Reach the top of her conversation list. Spark her now!",
                "Make a great first impression!" .PHP_EOL. "Get tips from our TM expert");
        }
        else
        {
            $msg_array = array("Now you can grab her attention! Start Sparking now!",
                "A Spark gives you 5x more chances of getting a like back. Send a Spark!",
                "Make a great first impression!" .PHP_EOL. "Get tips from our TM expert",
                "Get more Like backs!".PHP_EOL."Chat with Nikita, the TM expert for profile tips");
        }
        $key = array_rand($msg_array);
        return $msg_array[$key];
    }
    public function sparkNeverPurchased($day)
    {
        $count=0;
        $chunkCounter=0;
        $sql ="select u.user_id, ud.stay_country from user_data ud join user u  on u.user_id = ud.user_id
            left join user_spark_transaction ust on u.user_id = ust.user_id and ust.status='consumed'
            join user_app_status uas on uas.user_id = u.user_id
            where  uas.android_app ='install' and ust.user_id is null
            and u.status ='authentic' and u.gender='M'";
        $res =  $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $user_id = $row['user_id'];
            $data = $this->getSparkNeverPurchasedMessage($row['stay_country']);
            $this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,
                "event_status" => "SPARK_NEVER_PURCHASED", "title_text"=>$this->title_text,"push_type"=>"BUY_SPARKS"),"SPARK_NEVER_PURCHASED");
            $count++;
            $chunkCounter++;
            if($chunkCounter >= $this->chunkSize)
            {
                echo "sleeping for 20 seconds before pushing more notifications".PHP_EOL;
                sleep(20);
                $chunkCounter= 0;
            }
        }
        return $count;
    }

    public function sparkExpired($day)
    {
        $count = 0 ;
        $chunkCounter=0;
        $sql = "select t.user_id from
                (select distinct(user_id) from user_spark_transaction where status='consumed')t
                join user_spark_counter usc on usc.user_id = t.user_id
                join user u on u.user_id = t.user_id
                WHERE spark_count = 0 and u.status='authentic'";
        $res =  $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $user_id = $row['user_id'];
            $data = $this->sparkExpiredMessages;
            $this->pushNotificationObject->notify($user_id, array("content_text"=>$data,"ticker_text"=>$this->ticker_text,
                "event_status" => "SPARKS_PURCHASED_AND_EXPIRED", "title_text"=>$this->title_text,"push_type"=>"BUY_SPARKS"),"SPARKS_PURCHASED_AND_EXPIRED");
            $count++;
            $chunkCounter++;
            if($chunkCounter >= $this->chunkSize)
            {
                echo "sleeping for 20 seconds before pushing more notifications".PHP_EOL;
                sleep(20);
                $chunkCounter= 0;
            }
        }
        return $count;
    }


}

?>


