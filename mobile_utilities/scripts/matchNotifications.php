<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";

/* Author : Sumit
    Needs to be run everyday in the evening around 7
 * sends custom push notifications to different set of users based on certain conditions  */

class MatchNotifications {
    private $conn_reporting;
    private $admin_id;
    private $imageurl;
    private $matchNoticationMessages ;
    private $ticker_text = 'Message from TrulyMadly';
    private $title_text = 'TrulyMadly';
    public $count_sent ;
    private $isttime ;
    private $yesterday;
    private $pushNotification;
    private $chunkSize= 1000 ;
    private $msg_count;

    function __construct()
    {
        global $admin_id,$imageurl,$conn_reporting;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->admin_id = $admin_id;
        $this->imageurl = $imageurl;
        $this->count_sent = 0 ;
        $this->matchNoticationMessages = array( 1 => "It's rude to keep a lady waiting. Fire up the app now!",
                                                2 => "Aren't you a teeny bit curious about who's out there? Fire up the app!",
                                                3 => "Roses are red, violets are blue, find out who's waiting for you! Fire up the app!",
                                                4 => "What if she's already made the first move? Find out!",
                                                5 => "Twinkle, twinkle, little star, don't you wonder who your fans are? Check now!",
                                                6 => "It takes two to tango. Start chatting then think about tango ;)");
        $this->matchNoticationMessagesForUS = array( 1 => "It's rude to keep a lady waiting. Fire up the app now!",
                                                    2 => "Aren't you a teeny bit curious about who's out there? Fire up the app!",
                                                    3 => "Roses are red, violets are blue, find out who's waiting for you! Fire up the app!",
                                                    4 => "What if she's already made the first move? Find out!");
        $this->msg_count = count($this->matchNoticationMessages);
        $this->msg_count_us = count($this->matchNoticationMessagesForUS);
        $this->isttime= date ( 'Y-m-d H:i:s' ,strtotime ( '+330 minutes' ) );
        $this->yesterday =date('Y-m-d 18:30:00', strtotime('-1 day', strtotime($this->isttime)));
        $this->pushNotification = new pushNotification();
    }


    public function sendMatchNotifications()
    {
        $sql = "select u.user_id, u.fname, ud.stay_country from user u
                join user_app_status uas on uas.user_id=u.user_id
                JOIN user_data ud on u.user_id = ud.user_id
                join user_lastlogin ull on u.user_id = ull.user_id
                where u.status ='authentic' and uas.android_app= 'install' and  ull.last_login < '$this->yesterday' and u.gender='M'";
        $chunkCounter = 0 ;
        $data = array('title_text' => $this->title_text,
                        'ticker_text' => $this->ticker_text,
                    'content_text' => $this->matchNoticationMessages[1],
                    'push_type' => 'PROMOTE',
                    'event_status' => 'MALE_NEW_RECOMMENDATIONS');
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $user_id = $row['user_id'];
            $data['content_text'] = $this->getRandomMessage($row['stay_country']);
            $this->pushNotification->notify($user_id,$data,'match_notification');
            $this->count_sent++;
            $chunkCounter++;
            if($chunkCounter >= $this->chunkSize)
            {
                echo "sleeping for 20 seconds before pushing more notifications".PHP_EOL;
                sleep(20);
                $chunkCounter= 0;
            }
          // echo "sent to $user_id".PHP_EOL;
        }
        echo "total sent to $this->count_sent";
    }

    private function getRandomMessage($country)
    {
        if($country == 254)
        {
            $index = rand(1, $this->msg_count_us);
            return $this->matchNoticationMessagesForUS[$index];
        }
        else
        {
            $index = rand(1, $this->msg_count);
            return $this->matchNoticationMessages[$index];
        }
    }
}

try {
    if (php_sapi_name() === 'cli')
    {
        $options=getopt ( "d:" );

        if (isset($options[d])){
            $day = $options[d];
        } else {
            $day= date ( 'w' ,strtotime ('today'));
        }
        echo "running for ".$day .PHP_EOL;
        if($day == 1 || $day == 3 || $day == 5)
        {
            echo "sending match Notification";
            $matchNotification = new MatchNotifications();
            $matchNotification->sendMatchNotifications();
            $mailMessage= "Match Notification sent to $matchNotification->count_sent";
            $date= date('Y-m-d',strtotime('today'));
            $to = "sumit@trulymadly.com";
            $subject = "Match Notification sent ".$date;
            $from = "admintm@trulymadly.com";
            Utils::sendEmail($to, $from, $subject, $mailMessage);
        }
    }
    }
catch(Exception $e) {
        trigger_error($e->getMessage(), E_USER_WARNING);
        Utils::sendEmail($techTeamIds . ",sumit@trulymadly.com", "sumit@trulymadly.com", "Custom Notification script failed" . $baseurl, $e->getMessage());
    }



?>