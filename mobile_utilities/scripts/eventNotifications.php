<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";

/* Author : Tarun
 * sends custom push notifications to different set of users based on certain conditions  */

class newCustomEventNotifications {
	public $conn_reporting;
	public $admin_id;
	private $imageurl;
	private $eventNotificationMessage;
	public $ticker_text;
	public $title_text;

	function __construct() {
		global $admin_id,$imageurl,$conn_reporting;
		$this->conn_reporting = $conn_reporting;
		$this->admin_id = $admin_id;
		$this->imageurl = $imageurl;
		$this->ticker_text= 'Message from TrulyMadly';
		$this->title_text= 'TrulyMadly';
		$this->eventNotificationMessage = array ( 0 => array( "title"=>"Only one day to go!","content"=>"Experience EVENT_NAME at EVENT_LOCATION") ,
												  1 => array( "title"=>"Look who joined the party!","content"=>"MATCH_NAME is now following EVENT_NAME") ,
												  2 => array( "title"=>"New Experience Alert!","content"=>"We just added EVENT_NAME to CATEGORY_NAME experiences.") ,
												  3 => array( "title"=>"Looks like your matches love EVENT_NAME!","content"=>"Check it out.") ,
												  4 => array( "title"=>"Whoops! We're really sorry :(","content"=>"EVENT_NAME has been cancelled.")
												);
	}




	public function getCommaSeperatedEventsFollowed($user_id){
		$sql = "select group_concat(datespot_hash_id) as events from user_event where user_id=$user_id";
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0) {
			$row = $res->getRows();
			$output = $row[0]['events'];
			$output = str_replace(",","','", $output);
			return $output != NULL ? $output : -1;
		}
		return -1;
	}


	/*
	 * 0
	 * Returns list of users and corresponding event ids, which are starting tomorrow !!
	 */
	public function eventStartingTomorrow($user_id){
		$sql = "Select d.name, d.event_date, ue.user_id, d.hash_id, dl.location from datespots d join user_event ue on ue.datespot_hash_id=d.hash_id ".
			" join datespot_location dl on d.datespot_id=dl.datespot_id ".
			" where date(d.event_date) = date(DATE_ADD(now(), INTERVAL 1 DAY)) and d.status='active' and ue.user_id = $user_id";
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0) {
			$rowData = $res->getRows();
			$data = array(
				'title_text' => "Only one day to go!",
				'content_text' => "Experience ".$rowData[0]['name']." at ".$rowData[0]['location'].".",
				'type' => 'EVENT',
				'push_type' => 'EVENT',
				'event_id' => $rowData[0]['hash_id'],
				'category_id' => NULL,
				'categoryTitle' => NULL,
				'event_status' => 'EVENT_TOMORROW'
			);
			return $data;
			//	return $res->getRows();
		}
		return array();
	}

	/*
	 * 1
	 * Returns list of events which I follow, and mutual match followed it after me.
	 */
	public function getEventUserAndMatchFollow($user_id){
		$datespot_ids = array();
		$sql = "select d.name as datespot_name, ue.datespot_hash_id, ue.update_date, ue.user_id, u.fname from messages_queue um join user_event ue on ue.user_id=um.receiver_id  ".
			" join datespots d on d.hash_id = ue.datespot_hash_id join user u on ue.user_id=u.user_id where d.status='active' ".
			"  and um.blocked_by is null and um.sender_id = $user_id and date(ue.update_date) = date(DATE_SUB(now(),INTERVAL 1 DAY))  group by ue.datespot_hash_id";
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0) {
			while ($row = $res->FetchRow()){
				$datespot_id =  $row['datespot_hash_id'];
				$time =  $row['update_date'];
				$innerSql = "select datespot_hash_id from user_event where user_id = $user_id and datespot_hash_id = '$datespot_id' and update_date is not null and update_date < '$time'";
				$innerRes = $this->conn_reporting->Execute($innerSql);
				if($innerRes->RowCount()>0) {
					$rowData = $innerRes->getRows();
					$data = array(
						'title_text' => "Look who joined the party!",
						'content_text' => $row['fname']." is now following ".$row['datespot_name'].".",
						'type' => 'EVENT',
						'push_type' => 'EVENT',
						'event_id' => $row['datespot_hash_id'],
						'category_id' => NULL,
						'categoryTitle' => NULL,
						'event_status' => 'MATCH_FOLLOWED_YOUR_EVENT'
					);
					return $data;
				}
			}
		}
		return null;
	}

	/*
	 * 2
	 * Returns list of users who follow events in certain category, and that category got new events yesterday
	 * 2nd parameter is minimum number of events i've followed of particular category to make it as my fav category
	 */
	public function getUsersForEventAddedYesterday($user_id, $minFollowRequired=1)
	{
		//following query returns events along with their category, which were added yesterday
		$sql = "select d.hash_id, d.name, e.category_id, e.name as category_name from datespots d join event_category_mapping ecm on ecm.datespot_id=d.datespot_id " .
			" join event_category e on ecm.category_id = e.category_id where date(d.update_date) = date(DATE_SUB(now(),INTERVAL 1 DAY)) and d.status='active'";
		$res = $this->conn_reporting->Execute($sql);

		//following query returns user, category, and count of events followed in that category.
		$innerSql = "select ue.user_id, ecm.category_id, count(ecm.category_id) as count from user_event ue join datespots d on d.hash_id=ue.datespot_hash_id " .
			" join event_category_mapping ecm on ecm.datespot_id = d.datespot_id where ue.user_id = $user_id group by ecm.category_id order by count desc";

		$innerRes = $this->conn_reporting->Execute($innerSql);

		if ($res->RowCount() > 0 && $innerRes->RowCount() > 0) {
			while ($row = $res->FetchRow()) {
				while ($innerRow = $innerRes->FetchRow()) {
					if ($row['category_id'] == $innerRow['category_id'] && $innerRow['count'] >= $minFollowRequired) {
						$data = array(
							'title_text' => "New Experience Alert!",
							'content_text' => "We just added ".$row['name']." to ".$row['category_name']." experiences.",
							'type' => 'EVENT_LIST',
							'push_type' => 'EVENT_LIST',
							'event_id' => NULL,
							'category_id' => $row['category_id'],
							'categoryTitle' => $row['category_name'],
							'event_status' => 'NEW_EVENTS_YESTERDAY'
						);
						return $data;
					}
				}
			}
			return null;
		}
	}

	/*
	 * Case 3
	 * Get's list of users, whose mutual matches have followed some event, but they haven't followed it yet.
	 * Takes the input user, and comma seperated list of eventIds followed by that user.
	 * returns number of matches going to a event (maximum number of matches going to which event_
	 */
	public function getUsersToGetThemToFollow($userId){
		$userEventIds = $this->getCommaSeperatedEventsFollowed($userId);
		$sql = "select d.name,d.rank, d.hash_id, count(d.hash_id) as count from messages_queue um join user_event ue on ue.user_id=um.receiver_id ".
			" join datespots d on d.hash_id = ue.datespot_hash_id where d.status='active' ".
			" and um.blocked_by is null and um.sender_id = $userId and ue.user_id != $userId and um.msg_type != 'SPARK' and ue.datespot_hash_id not in ('$userEventIds') ".
			" group by ue.datespot_hash_id order by count desc,d.rank desc limit 1";
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0) {
			$rows = $res->getRows();
			$data = array(
				'title_text' => "Guess what?",
				'content_text' => "Looks like your matches love ".$rows[0]['name']."!",
				'type' => 'EVENT',
				'push_type' => 'EVENT',
				'event_id' => $rows[0]['hash_id'],
				'category_id' => NULL,
				'categoryTitle' => NULL,
				'event_status' => 'MATCHES_LOVE_EVENT'
			);
			return $data;
		}
		return null;
	}



	/*
	 * 4
	 * Returns list of user ids following events, which was cancelled.
	 */
	public function getCancelledEventForUser($user_id){
		/*
		 * NEED TO ADD cancel_date column
		 *
		 * $sql = "select ue.user_id, d.hash_id, d.name, d.status from user_event ue left join datespots d ".
			" on d.hash_id = ue.datespot_hash_id where d.status='cancel' and date(d.cancel_date) = date(NOW()) and ue.user_id=$user_id";
		$res = $this->conn_reporting->Execute($sql);
		if($res->RowCount()>0) {
			$data = array(
				'content_text' => $this->eventNotificationMessage[4],
				'type' => 'EVENT',
				'push_type' => 'EVENT',
				'event_id' => $res->getRows()[0]['hash_id'],
				'category_id' => NULL,
				'categoryTitle' => NULL
			);
			return $data;
		//	return $res->getRows();
		}*/
		return null;
	}

}

try {

	if(php_sapi_name() === 'cli') {
		$date= date('Y-m-d',strtotime('today'));
		$userSql = "SELECT user_id, gender from user_search where country= 113 ";
		$eventNotification = new newCustomEventNotifications();
		$res = $eventNotification->conn_reporting->Execute($userSql);
		$push_notification = new pushNotification();

		if ($res->RowCount() > 0) {
			$mailmessage = "Total Event notifications sent: ";
			$count=0;
			while ($row = $res->FetchRow()) {
				$user_id = $row['user_id'];
				//start calling notifications in order of preferences
				$data = $eventNotification->eventStartingTomorrow($user_id);
				if ($data == null) {
					$data = $eventNotification->getEventUserAndMatchFollow($user_id);
					/*
					** commented out on Maria's Request:
						if ($data == null && $row['gender'] == 'M') {
							$data = $eventNotification->getUsersToGetThemToFollow($user_id,1);
							if ($data == null) {
								$data = $eventNotification->getUsersForEventAddedYesterday($user_id);
								if ($data == null) {
									$data = $eventNotification->getCancelledEventForUser($user_id);
								}
							}
						}
					*/
				}
			//	var_dump($data);
				if ($data != null) {
					$count++;
					$data['ticker_text'] = $eventNotification->ticker_text;
					$push_notification->notify($user_id,
						$data,
						$eventNotification->admin_id);
				}
			}

			$to = "tarun@trulymadly.com";
			$subject = "Events Notifications sent ".$date;
			$from = "admintm@trulymadly.com";
			Utils::sendEmail($to, $from, $subject, $mailmessage." : ".$count,true);
		}
	}

} catch (Exception $e) {
	echo $e->getTraceAsString();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "Custom Notification script failed".$baseurl, $e->getTraceAsString());
}


?>