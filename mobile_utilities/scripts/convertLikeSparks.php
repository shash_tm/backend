<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../spark/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../../DBO/sparkDBO.php";
require_once dirname ( __FILE__ ) . "/../../msg/MessageFullConversation.class.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/userActions.php";

/* Author : Tarun
 * sends custom push notifications to different set of users based on certain conditions  */

class newSparkObject {
	public $conn_reporting;
	public $conn_master;
	public $admin_id;
	public $data;
	public $ticker_text;
	public $push_notification;
	public $user_id;
	private $uu ;

	function __construct() {
		global $admin_id,$conn_reporting,$miss_tm_id, $conn_master;
		$this->conn_reporting = $conn_reporting;
		$this->conn_master = $conn_master;
		$this->admin_id = $admin_id;

		$this->misstm= $miss_tm_id;
		$this->uu = new UserUtils();
	}

	function hasUserSparked($user1, $user2){
		$sql = "SELECT * from user_like where user1=$user1 and user2 = $user2";
		$res = $this->conn_reporting->Execute($sql);
		if ($res->RowCount() > 0){
			return true;
		}
		return false;
	}

}

try {

	if(php_sapi_name() === 'cli') {
		$date= date('Y-m-d',strtotime('today'));
		$sparkObj = new newSparkObject();

		$action = isset($argv[1]) && $argv[1] != null ? $argv[1] : null;
		if($action == null || !isset($action)){
			echo "pass action as first param. Accepted values: 'daily', 'once'";
			die;
		}
		$userSql =  "select us.user_spark_id,us.user1,us.user2,us.tstamp,us.status as status,us.expiry_time, ".
					"us.expired_date,ul.user1 as ul_user1,ul.user2 as ul_user2,ul.timestamp ".
					"from user_spark us join user_like ul on us.user2 = ul.user1 and us.user1 = ul.user2 and ul.timestamp <= us.tstamp ".
					"where ul.timestamp is not null and ((us.status = 'seen' and us.expired_date < now()))";

		if($action == "daily") {
			$userSql .= " AND DATE(us.expired_date) = DATE(DATE_SUB(now(), INTERVAL 1 DAY)) ";
		}

		$res = $sparkObj->conn_reporting->Execute($userSql);
		if ($res->RowCount() > 0) {
			$userAction = new UserActions();
			while ($row = $res->FetchRow()) {
				$user1 = $row['user1'];
				$user2 = $row['user2'];
				if (!$sparkObj->hasUserSparked($user1, $user2)) {
					$userAction->performAction($user1, $user2, 'like', 0, null, 0, false);
				}
			}
			$to = "tarun@trulymadly.com, shashwat@trulymadly.com";
			$subject = "5 free spark given to users: " . $date;
			$from = "admintm@trulymadly.com";
			//	$mailmessage = "5 free spark given to Target males.";
			//	Utils::sendEmail($to, $from, $subject, $mailmessage." : ".$count,true);
		}
	}

} catch (Exception $e) {
	echo $e->getMessage();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	Utils::sendEmail("backend@trulymadly.com" , "tarun@trulymadly.com", "Free sparks script failed".$baseurl, $e->getTraceAsString());
}


?>