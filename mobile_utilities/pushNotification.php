<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/iosConnection.php";
require_once dirname ( __FILE__ ) . "/windows_pushnotification/sendNotifications.php";
require_once dirname ( __FILE__ ) . "/windows_pushnotification/WPNTypesEnum.php";
require_once dirname ( __FILE__ ) . "/windows_pushnotification/WPNResponseConstants.php";





/**
 * Deals with all the actions related to `push notification` on mobile
 * @author himanshu
 */

class pushNotification{

	private $push_notify_url;
	private $push_notification_auth_key;
	private $ios_notification_passphrase;
	private $ios_notification_filepath;
	private $ios_notification_conn_url;
	private $_windows_pushnotification_clientId;
	private $_windows_pushnotification_clientSecret;
	private $_wpn;
	private $redis;
	private $event_tracking;
	//private $iosMaxAttemptCount = 3;

	//private $conn;


	function __construct(){
		global $windows_pushnotification_clientId, $windows_pushnotification_clientSecret, $push_notification_url, $push_notification_auth_key, $ios_notification_passphrase, $ios_notification_filepath, $ios_notification_conn_url, $redis;//, $conn;
		$this->push_notify_url = $push_notification_url;
		$this->push_notification_auth_key = $push_notification_auth_key;
		$this->ios_notification_passphrase = $ios_notification_passphrase;
		$this->ios_notification_filepath = $ios_notification_filepath;
		$this->ios_notification_conn_url  =  $ios_notification_conn_url;
		$this->iosMaxAttemptCount = 3;
		$this->_windows_pushnotification_clientId = $windows_pushnotification_clientId;
		$this->_windows_pushnotification_clientSecret = $windows_pushnotification_clientSecret;
		$this->redis = $redis;
		$this->_wpn = new WPN($this->_windows_pushnotification_clientId, $this->_windows_pushnotification_clientSecret);
		$this->event_tracking = new EventTrackingClass();
		//$this->conn = $conn;
	}

	/**
	 *
	 * pushes data to GCM server
	 * handles two different calls as well for compatible and incompatible versions
	 * @param unknown_type $regData
	 * @param unknown_type $userId
	 * @param unknown_type $push_data
	 * @param unknown_type $sender_id
	 * @param unknown_type $multipleNotificationsForSameUser - should be 1 if different notifications need to be sent
	 * @param unknown_type $supportedAppVersion - pass this for different notification
	 * NOTE: scope of improvement
	 * 		 - in case more than two types of support is needed for more than two devices then change the below to for loop for the call
	 * 		 - also keep the negotiated parameters to a constant file
	 */
	public function pushAndLog($regIds, $appCodes, $userId, $push_data,$sender_id, $multipleNotificationsForSameUser = 0, $supportedAppVersion = array("lower"=>"42", "higher"=>"52") ){
		$headers=array("Authorization: key=$this->push_notification_auth_key",'Content-Type: application/json');

	   $push_data = $this->_parsePushDataforAndroid($push_data);

		if($multipleNotificationsForSameUser != 0){
			foreach ($regIds as $key => $val){
				if($supportedAppVersion['lower'] <= $appCodes[$key] && $appCodes[$key]<$supportedAppVersion['higher'] ){
					$regIDsIncompatible[] = $val;
					//$fields[] = array('registration_ids'=>$val['registrationIds'],'data'=>$push_data['incompatible'],'user_id'=>$userId,'collapse_key'=>"TM_PUSH_NOTIFICATION_".$sender_id);
				}
				else{
					$regIDsCompatible[] = $val;
					//$fields[] = array('registration_ids'=>$val['registrationIds'],'data'=>$push_data['compatible'],'user_id'=>$userId,'collapse_key'=>"TM_PUSH_NOTIFICATION_".$sender_id);
				}
			}


			if(isset($regIDsCompatible)){
				$fieldsForCompatibleAppVersion = array('registration_ids'=>$regIDsCompatible,'data'=>$push_data['compatible'],'user_id'=>$userId,'collapse_key'=>"TM_PUSH_NOTIFICATION_".$sender_id);
				$result1 = Utils::curlCall($this->push_notify_url, json_encode($fieldsForCompatibleAppVersion), $headers);
				$param[] = array("user_id"=>$userId,"activity" => "push_sent", "event_info"=> json_encode($result1),"source"=> "android_app","event_type"=>$push_data['compatible']['push_type'],"event_status" =>$push_data['compatible']['event_status']);
				$this->updateCanonicalIds($regIDsCompatible, $result1, $userId);
			}
			if(isset($regIDsIncompatible)){
				$fieldsForIncompatibleAppVersion = array('registration_ids'=>$regIDsIncompatible,'data'=>$push_data['incompatible'],'user_id'=>$userId,'collapse_key'=>"TM_PUSH_NOTIFICATION_".$sender_id);
				$result2 = Utils::curlCall($this->push_notify_url, json_encode($fieldsForIncompatibleAppVersion), $headers);
				$param[] = array("user_id"=>$userId,"activity" => "push_sent", "event_info"=> json_encode($result2),"source"=> "android_app","event_type"=>$push_data['incompatible']['push_type'],"event_status" =>$push_data['incompatible']['event_status']);
				$this->updateCanonicalIds($regIDsIncompatible, $result2, $userId);
			}
			//var_dump($result);
			/*var_dump($result1);
			var_dump($result2);die;*/

			$result = array(json_encode($result1),json_encode($result2) );

		}else{
			$fields=array('registration_ids'=>$regIds,'data'=>$push_data,'user_id'=>$userId,'collapse_key'=>"TM_PUSH_NOTIFICATION_".$sender_id);
			if(isset($push_data['priority']) && $push_data['priority'] == 'high'){
				$fields['priority'] = 'high';
			}
			$result = Utils::curlCall($this->push_notify_url, json_encode($fields), $headers);
			$param[] = array("user_id"=>$userId,"activity" => "push_sent", "event_info"=>json_encode($result),"source"=>"android_app","event_type"=>$push_data['push_type'],"event_status" =>$push_data['event_status']);
			$this->updateCanonicalIds($regIds, $result, $userId);
		}
		//$result= json_decode($result,true) ;
		//$sql = "INSERT DELAYED INTO user_mobile_gcm_log VALUES(?,now(),?,?,?,?,?)";
		//var_dump($param); die;
		//$data_logging [] = array( "activity" => "spark_payment" , "event_type" => $event, "event_status" => $status, "event_info" => $event_info, "time_taken"=>0, "user_id" => $this->user_id);

		$this->event_tracking->logActions($param);
		//var_dump($result);
		return $result;
	}
	private function _parsePushDataforAndroid ($push_data){
		if ($push_data['push_type'] == "PHOTO" ){
			$push_data['class_name'] = "com.trulymadly.android.app.UserPhotos";
			$push_data['push_type'] = 'ACTIVITY';
		} else if ($push_data['push_type'] == "CONVERSATION_LIST" ){
			$push_data['class_name'] = "com.trulymadly.android.app.ConversationListActivity";
			$push_data['push_type'] = 'ACTIVITY';
		} else if ($push_data['push_type'] == "MLLIST" ){
			$push_data['class_name'] = "com.trulymadly.android.app.MutualLikesActivity";
			$push_data['push_type'] = 'ACTIVITY';
		}else if ($push_data['push_type'] == "EDIT_PROFILE" ){
			$push_data['class_name'] = "com.trulymadly.android.app.ProfileNew";
			$push_data['push_type'] = 'ACTIVITY';
		}else if ($push_data['push_type'] == "BUY_SPARKS" ){
			$push_data['class_name'] = "com.trulymadly.android.app.activities.BuyPackagesActivity";
			$push_data['push_type'] = 'ACTIVITY';
		}

		return $push_data;
	}




	public function notify($userId, $push_data,$sender_id, $multipleNotificationsForSameUser = 0, $supportedAppVersion = array("lower"=>"42", "higher"=>"52"), $platform =  array('androidApp','iOSApp','windows_app')){ 
		$push_array = json_encode(array("user_id"=> $userId,
    			                        "push_data"=> $push_data, 
    			                        "sender_id"=>$sender_id, 
    			                        "multipleNotificationsForSameUser"=> $multipleNotificationsForSameUser,
    			                        "supportedAppVersion" => $supportedAppVersion ,
				                        "platform" => $platform
		));

		//should do lpush only when implemented RPOP on listener end @Himanshu
		$this->redis->lpush(Utils::$redis_keys['push_notification'], $push_array);
	}

	/**
	 * to push data to mobile against a user ID
	 * @param int $userId
	 * @param array(key=>value) $push_data e.g. $msg=array("message"=>$data, "sender_id" => $this->user_id, "receiver_id" => $this->user_communicated_with,"sender_thumb" => $sender_thumb, "sender_name" => $send_name, "redirected_url" => $this->baseurl."/msg/message_full_conv.php?match_id=$this->user_id&mesh=$CheckSum")
	 */
	public function notifySubscribe($userId, $push_data,$sender_id, $multipleNotificationsForSameUser = 0, $supportedAppVersion = array("lower"=>"42", "higher"=>"52"), $platform =  array('androidApp','iOSApp','windows_app')){
		$userUtils = new UserUtils();
		
		$user_status = $userUtils->getUserStatus($userId);
	
        if($user_status['status'] != "suspended" && $user_status['status'] != "blocked"){
        	
		$regData= $this->getRegisterationIds($userId,$push_data['push_type']);
		$regID = $regData['registrationIds'];
		$appCodes = $regData['appCodes'];
		$source =  $regData['source'];
		$result = null;  
        if ($platform == null )
        	$platform = array('androidApp','iOSApp','windows_app') ;
		if(count($regID['androidApp']) && in_array('androidApp', $platform) ){
			//push data for both compatible and incompatible versions of Android
			$result['android']= $this->pushAndLog($regID['androidApp'],$appCodes['androidApp'], $userId, $push_data, $sender_id, $multipleNotificationsForSameUser,$supportedAppVersion);
		}
		if(count($regID['iOSApp']) && in_array('iOSApp', $platform) ){
			// push data for IOS
			if(isset($push_data['compatible'])) $push_data = $push_data['compatible'];
			$result['ios'] = $this->iosNotify ($regID['iOSApp'],$appCodes['iOSApp'], $userId, $push_data, $sender_id);
		}

		//var_dump($regID);
		//	var_dump($regID['windows_app']);
		if(count($regID['windows_app'])  && in_array('windows_app', $platform)){
			
			$result['windows_app'] = $this->_windowsNotify ($regID['windows_app'],$appCodes['windows_app'], $userId, $push_data, $sender_id);
		}
        }
		return($result);
	}

	public function iosNotify ($regID, $appCodes, $userId, $push_data, $sender_id, $attempts= 1  ){
	
		try{
	
			$iosConnection = new iosConnection();
	
			//  for one on one messages the senders name should be should in push notification text
			if ($push_data['push_type'] ==  'MESSAGE' && $push_data['msg_type'] != 'STICKER' ){
				$msg_text = $push_data['title_text'].": ".$push_data['content_text'];
			}else if ($push_data['push_type'] ==  'EVENT' || $push_data['push_type'] == 'EVENT_LIST' ){
				$msg_text = $push_data['title_text']." \n".$push_data['content_text'];
			}else if ($push_data['push_type'] ==  'CONVERSATION_LIST' || $push_data['push_type'] == 'CONVERSATION_LIST' ) {
				$msg_text = $push_data['title_text'] . " \n" . $push_data['content_text'];
			}
			else {
				$msg_text = $push_data['content_text'];
			}
			 
			// to handle special characters e.g.  &
			$msg_text = htmlspecialchars_decode($msg_text);
			 
			// for opening photo page in app push_type is to be sent as PHOTO
		 /*	if ($push_data['push_type'] ==  'ACTIVITY' && $push_data['class_name'] == 'com.trulymadly.android.app.UserPhotos' ){
		  $push_type= 'PHOTO';
		 	} else {*/
			$push_type= $push_data['push_type'];
		 /*	}*/
			$i=0;
			foreach ($regID as $row => $col){
				$body['aps'] = array(
						'alert' => $msg_text,
						'sound' => 'default',
						'type' => $push_type,
						'message_url' =>  $push_data['message_url'],
						'match_url' =>  $push_data['match_profile_url'],
						'web_url' =>  $push_data['web_url'],
						'is_admin_set' => $push_data['is_admin_set'],
						'sender_id' => $sender_id
				);
				if(isset($push_data['event_status']))
				{
					$body['aps']['event_status'] = $push_data['event_status'] ;
				}

				if(isset($push_data['event_id']))
				{
					$body['aps']['event_id'] = $push_data['event_id'] ;
				}

				if(isset($push_data['category_id']))
				{
					$body['aps']['category_id'] = $push_data['category_id'] ;
				}

				if(isset($push_data['categoryTitle']))
				{
					$body['aps']['categoryTitle'] = $push_data['categoryTitle'] ;
				}

				if(isset($push_data['previousLikeStatus']))
				{
					$body['aps']['previousLikeStatus'] = $push_data['previousLikeStatus'] ;
				}
				//var_dump($push_data);
				$payload = json_encode($body);
				//var_dump($payload);
				$deviceToken = $col;
				$id=0;
				if(isset($push_data['msg_id']))
				{
					$id=intval($push_data['msg_id']);
				}
				else 
				{
					$id=rand(0,9999);
				}
				
				$expireTime=time()+(2*3600*24);
				//$appCode = $val['app_version_code'];
				//$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
				$pn = pack('CnH*', 1, 32, $deviceToken)
				. pack('CnA*', 2, strlen($payload), $payload)
				. pack('CnN', 3, 4, $id)
				. pack('CnN',  4, 4, $expireTime)
				. pack('CnC',  5, 1, 10);
	
				$msg = pack('CN', 2, strlen($pn)) . $pn;
	
				// Send it to the server
				$fp = $iosConnection->createConnection();
				$result = fwrite($fp, $msg, strlen($msg));
				if(!($result)){
					
					$errorid=0;
					//$errorid corresponds to the identifier, that is returned when an error occurs
					$error= $this->checkApnsErrorResponse($fp,$errorid);						
					// reset the old connection
				
					$iosConnection->removeConnection();  
					// log the result and registration id
					fclose($fp);
					$result_arr= array("failure"=>$result, "registration_id"=>$deviceToken, "error"=> $error,"attempt_count"=>$attempts,"type"=>$push_type, "payload" => $payload,"identifier"=>$errorid);
					$this->resendIosNotification (array($col),array($appCodes[$i]),$userId,$push_data, $sender_id, $attempts);
					//$status='sent_failed';
				} else {
					// fwrite performed successfully . Notification was sent
					$result_arr= array("success bytes written"=> $result,"registration_id"=>$deviceToken, "attempt_count"=>$attempts,"type"=>$push_type, "payload" => $payload);
					//$status='sent';
				}
				$log_array[] = array("user_id"=>$userId, "activity" => "push_sent", "event_info"=> json_encode($result_arr),"source"=>"ios_app","event_type" =>$push_data['push_type'],"event_status"=>$push_data['event_status']);
				//var_dump($log_array);
	
				$i++;
					
			}
			$this->event_tracking->logActions($log_array);
			return $result;
		}
		catch (Exception $e){
			trigger_error($e->getTraceAsString(), E_USER_WARNING);
		}
	}
	
	
	
	
	private function resendIosNotification ($regID, $appCodes, $userId, $push_data, $sender_id, $attempts){
		if ($attempts <= $this->iosMaxAttemptCount){
			$attempts ++;
			$this->iosNotify ($regID, $appCodes, $userId, $push_data, $sender_id, $attempts );

		}

	}

	public function checkApnsErrorResponse($fp,&$errorid) {

		//byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID).
		// Should return nothing if OK.

		//NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait
		// forever when there is no response to be sent.

		$apple_error_response = fread($fp, 6);
		
		if ($apple_error_response) {
		
			// unpack the error response (first byte 'command" should always be 8)
			$error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);	         
           
			if ($error_response['status_code'] == '0') {
				$error_response['status_code'] = '0-No errors encountered';

			} else if ($error_response['status_code'] == '1') {
				$error_response['status_code'] = '1-Processing error';

			} else if ($error_response['status_code'] == '2') {
				$error_response['status_code'] = '2-Missing device token';

			} else if ($error_response['status_code'] == '3') {
				$error_response['status_code'] = '3-Missing topic';

			} else if ($error_response['status_code'] == '4') {
				$error_response['status_code'] = '4-Missing payload';

			} else if ($error_response['status_code'] == '5') {
				$error_response['status_code'] = '5-Invalid token size';

			} else if ($error_response['status_code'] == '6') {
				$error_response['status_code'] = '6-Invalid topic size';

			} else if ($error_response['status_code'] == '7') {
				$error_response['status_code'] = '7-Invalid payload size';

			} else if ($error_response['status_code'] == '8') {
				$error_response['status_code'] = '8-Invalid token';

			} else if ($error_response['status_code'] == '255') {
				$error_response['status_code'] = '255-None (unknown)';

			} else {
				$error_response['status_code'] = $error_response['status_code'].'-Not listed';

			}
			//To set the identifier, from the error response,Raghav
			if(isset($error_response['identifier']))
			{
				$errorid=$error_response['identifier'];
			}
			else
			{
				$errorid=0;
			}

			//echo '<br><b>+ + + + + + ERROR</b> Response Command:<b>' . $error_response['command'] . '</b>&nbsp;&nbsp;&nbsp;Identifier:<b>' . $error_response['identifier'] . '</b>&nbsp;&nbsp;&nbsp;Status:<b>' . $error_response['status_code'] . '</b><br>';


			return $error_response['status_code'];
		}
		return false;
	}


	/**
	 * to remove the canonical ids problem (sometimes multiple reg ids are returned)
	 * or if there's any error in the push notification above
	 */
	private function updateCanonicalIds($regID, $result, $userId){
		$result = json_decode($result, true); 
		for($i=0; $i<count($regID); $i++){
			if($result['results'][$i]['error'] || $result['results'][$i]['registration_id']){

				$logMsg = null;
				if($result['results'][$i]['error']){
					$logMsg = 'Error: '.$result['results'][$i]['error'];
					if ($result['results'][$i]['error']=='NotRegistered') {
						$sql = "update user_gcm_current set status='uninstall' where registration_id=?";
						Query::UPDATE($sql, array($regID[$i]));
						$this->dataforactionlog($regID[$i],$userId);


					} else if ($result['results'][$i]['error']=='MissingRegistration') {
						$sql = "update user_gcm_current set status='missing' where registration_id=?";
						Query::UPDATE($sql, array($regID[$i]));


					} /* else  {
					$sql = "DELETE from user_gcm_current where registration_id=?";
					Query::DELETE($sql, array($regID[$i]));

					} */
				}

				if($result['results'][$i]['registration_id']){
					$logMsg = 'Canonical Id: '. $result['results'][$i]['registration_id'];
					$sql="select count(*) as count from user_gcm_current where registration_id=?";
					$res= Query::SELECT($sql, array($result['results'][$i]['registration_id']));

					if($res[0]['count']== 0){
						$sql = "update user_gcm_current set registration_id=? where registration_id=?";
						Query::UPDATE($sql, array($result['results'][$i]['registration_id'],$regID[$i]));
					} else {
						$sql = "update user_gcm_current set status='expired' where registration_id=?";
						Query::UPDATE($sql, array($regID[$i]));

					}


				}
			}
		}
			
	}

	/* to send  push notifications against a registraion_id */
	public function sendSilentNotification($regID, $push_data,$sender_id){
		$fields=array('registration_ids'=>$regID,'data'=>$push_data,'collapse_key'=>"TM_PUSH_NOTIFICATION_".$sender_id);

		$headers=array("Authorization: key=$this->push_notification_auth_key",'Content-Type: application/json');
		$result = Utils::curlCall($this->push_notify_url, json_encode($fields), $headers);
		$result = json_decode($result,true);
		$pass=$result['success'];
		$failure=$result['failure'];
		$uni=0;
		$can=0;
		$mis=0;
		/**
		 * to remove the canonical ids problem (sometimes multiple reg ids are returned)
		 * or if there's any error in the push notification above
		 */
		for($i=0;$i<count($regID); $i++){

			if($result['results'][$i]['error'] || $result['results'][$i]['registration_id']){
					
				if($result['results'][$i]['error']){
					if ($result['results'][$i]['error']=='NotRegistered') {
						$sql = "update user_gcm_current set status='uninstall' where registration_id=?";
						Query::UPDATE($sql, array($regID[$i]));
						$this->dataforactionlog($regID[$i],$userId);
						$uni++;
							
					} else if ($result['results'][$i]['error']=='MissingRegistration') {
						$sql = "update user_gcm_current set status='missing' where registration_id=?";
						Query::UPDATE($sql, array($regID[$i]));
						$mis++;

					}
				}

				if($result['results'][$i]['registration_id']){
					$sql="select count(*) as count from user_gcm_current where registration_id=?";
					$res= Query::SELECT($sql, array($result['results'][$i]['registration_id']));
					$can++;
					if($res[0]['count']== 0){
						$sql = "update user_gcm_current set registration_id=? where registration_id=?";
						Query::UPDATE($sql, array($result['results'][$i]['registration_id'],$regID[$i]));
					} else {
						$sql = "update user_gcm_current set status='expired' where registration_id=?";
						Query::UPDATE($sql, array($regID[$i]));
							
					}


				}

			}
		}
		return array("success"=>$pass,"fail"=>$failure,"uninstall"=>$uni,"missing"=>$mis,"canonical"=>$can,"result"=>$result['results']);

	}
	/* Store uninstall in the action_log_new table
	 * */
	public function dataforactionlog ($reg_id,$userId){
		$uninTrk = new EventTrackingClass();
		$sql="select device_id from user_gcm_current where registration_id=?";
		$res= Query::SELECT($sql, $reg_id);

		$data[] = array("user_id" =>$userId,"user_agent"=>"TrulyMadly/ (Android)", "activity" => "GCM" , "event_type" => "uninstall", "source" => "android_app", "device_id"=>$res[0]['device_id'],"event_status" => $reg_id);
		//var_dump($data);
		$uninTrk->logActions($data);
			
		//echo "new ";
	}

	/**
	 * returns all the registeration Ids against a userid
	 * @param unknown_type $userId
	 */
	public function getRegisterationIds($userId,$type){
		$regIDs = array();
		$registrationData = array();
		if($type=='ACTIVITY') { 
			$ex_status="'uninstall','expired','missing','install'";
			$sql = "SELECT registration_id, app_version_code, source FROM user_gcm_current where ( ( app_version_code>=60 and ( source='androidApp' or source is null )) or source='iOSApp') and status IN ('login','logout') and user_id = ?";
		} else if($type=='TRUST' || $type=='VOUCHER' || $type=='PROMOTE'){
			$ex_status="'uninstall','expired','missing','logout','install'";
			$sql = "SELECT registration_id, app_version_code, source FROM user_gcm_current where ( ( app_version_code>=35 and ( source='androidApp' or source is null )) or source IN ('windows_app', 'iOSApp')) and status IN ('login','logout') and user_id = ?";
		} else {
			$ex_status="'uninstall','expired','missing','logout','install'";
			$sql = "SELECT registration_id, app_version_code, source FROM user_gcm_current where   status IN ('login','logout') and  user_id = ?";
		}
		
        $sql .= " order by tstamp desc" ;
		$res = Query::SELECT($sql, array($userId));
		if(count($res)>0){
			foreach ($res as $key=>$val){
				if ( $val['source'] == null){
					$regIDs['androidApp'][] = $val['registration_id'];
					$appCodes['androidApp'][] = $val['app_version_code'];
				} else {
					$regIDs[$val['source']][] = $val['registration_id'];
					$appCodes[$val['source']][] = $val['app_version_code'];
				}
			}
		}
		return array("registrationIds" => $regIDs, "appCodes"=>$appCodes);
		//return $regIDs;
	}




	/**
	 * windows push notification functions block
	 * starts here
	 * TODO: move below code to a class if it exceeds and change the windownotify() to call that class
	 *
	 */

	/**
	 * send the response array to log the response into user_mobile_gcm_log
	 * @param $response_arr
	 */
	private function _logResponse($response_arr){
		try
		{
			$sql = "INSERT DELAYED INTO user_mobile_gcm_log VALUES(?,now(),?,?,?,?,?)";
			Query::BulkINSERT($sql, $response_arr);
		}
		catch (Exception $e)
		{
			trigger_error($e->getTraceAsString(), E_USER_WARNING);
		}

	}

	private function _setStatus($registration_id, $source, $status){
		try
		{
			$sql = "update user_gcm_current set status=? where source = ? and registration_id=?";
			Query::UPDATE($sql, array($status, $source, $registration_id));
		}
		catch (Exception $e)
		{
			trigger_error($e->getTraceAsString(), E_USER_WARNING);
		}
	}
	/**
	 * parse the whole multicurl response and send to logger
	 * Do repeat the calls if the curl reported any connection error at initial stage
	 * @param unknown_type $registration_id
	 * @param unknown_type $response
	 * @param unknown_type $source
	 * @param unknown_type $push_data
	 * @param unknown_type $user_id
	 */
	private function _parseWindowsNotificationResponse($registration_id, $response, $source, $push_data, $user_id,$attempt = 1 ){

		$log_array = array();

		foreach ($response as $value)
		{
			$reflection = null;
			//TODO: check for infinite loop
			if($value['code'] == UNAUTHORIZED::code)
			{
				$reflection = new ReflectionClass('UNAUTHORIZED');

				// token is expired repeat curl call by getting new token
				$this->_wpn->reset_access_token();				
				$this->resendWindows($registration_id,$push_data,$attempt);
				
			}

			//the uri is expired
		/*	else if($value['code'] == GONE::code )
			{
				$reflection  =  new ReflectionClass ('GONE') ;
				$this->_setStatus($registration_id, "windows_app", "expired");
			}*/
			else if ($value['code'] == NOTFOUND::code || $value['code'] == GONE::code)
			{
				//if the uri is invalid or the WPNS is asking not to send further notifcations in case of expiry
				$class = ($value['code'] == NOTFOUND::code ) ? 'NOTFOUND' : 'GONE';
				$reflection =  new ReflectionClass ($class);
				$this->_setStatus($registration_id, "windows_app", "uninstall");
			}
			else if ($value['code'] == FORBIDDEN::code)
			{
				$reflection = new ReflectionClass ('FORBIDDEN');
			}
			else if ( $value['code'] == NOTACCEPTABLE::code)
			{
				$reflection = new ReflectionClass ('NOTACCEPTABLE');
			}
			else if ($value['code'] == TOOLARGEREQUEST::code)
			{
				$reflection = new ReflectionClass ('TOOLARGEREQUEST');
			}
			else if ($value['code'] == INTERNALSERVERERROR::code || $value['code'] == SERVICEUNAVAILABLE::code )
			{
				$reflection = ($value['code'] == INTERNALSERVERERROR::code)? new ReflectionClass ('INTERNALSERVERERROR'): new ReflectionClass ('SERVICEUNAVAILABLE');
			}
			else if ($value['code'] == OK::code)
			{
				$reflection = new ReflectionClass('OK');
				$status='sent';
			}
            if($status != 'sent')
            $status='sent_failed';
			//$log_array [] = array($user_id, json_encode($reflection->getConstants()) . " curl response: " . json_encode($value) ,"windows_app",$status,$push_data['push_type'],$push_data['event_status']);
			$log_array [] = array("user_id"=>$user_id,"activity" => "push_sent", "event_info"=> "curl response: " . json_encode($value) ,"source"=>"windows_app","event_type"=>$push_data['push_type'],"event_status" =>$push_data['event_status']);

		}

		$this->event_tracking->logActions($log_array);

	}

	
	private function resendWindows($registerationId,$push_data,$attempt)
	{
		if($attempt<=3)
		{
			$attempt++;
			$this->_sendWindowsNotification($registerationId, $push_data,$attempt);
		}
		else
		{
			trigger_error("Exceeded 3 Times 401 Windows Notification", E_USER_WARNING);
			return;
		}
		
		
	}
	/**
	 * build the respective xmls for push notification and make call for notification
	 * Enter description here ...
	 * @param $regIds
	 * @param $push_data
	 * @param $user_id
	 */
	private function _sendWindowsNotification ($regIds, $push_data, $user_id,$attempt=1){

		foreach ($regIds as $val)
		{
			//DO NOT change the ordering of building the xml
			$tile_xml = $this->_wpn->build_tile_xml($push_data);
			$toast_xml = $this->_wpn->build_toast_xml($push_data);

			$urls = array();
			$options = array();
			$options = $this->_wpn->setOptionsForCurl($val, array(0=>$toast_xml), array(0=>WPNTypesEnum::Toast), $urls);

			if(!empty($options))
			{
				$results = Utils::performMultipleCurlCalls($urls, $options);
				$this->_parseWindowsNotificationResponse( $val, $results, "windows_app", $push_data, $user_id,$attempt);
			}
			else
			{
				throw Exception ("unable to set options in multi curl for windows push notification for: ". json_encode($regIds) ." with push data: ". json_encode($push_data));
			}
		}
	}


	/**
	 * driving function for push notification
	 * @param unknown_type $regID
	 * @param unknown_type $appCodes
	 * @param unknown_type $userId
	 * @param unknown_type $push_data
	 * @param unknown_type $sender_id
	 */
	private function _windowsNotify($regID, $appCodes, $userId, $push_data, $sender_id){
		try
		{
			
			//if push data contains two types which comes due to android then pick the compatible one
			if(isset($push_data['compatible'])) $push_data = $push_data['compatible'];
			$push_data['user_id'] = $userId;
			$this->_wpn->set_activity_launch_url($push_data);
			$this->_sendWindowsNotification( $regID, $push_data, $userId);
		}
		catch (Exception $e)
		{
			trigger_error($e->getTraceAsString(), E_USER_WARNING);
		}

		return null;
	}



}


?>
