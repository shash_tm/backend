<?php 
require_once  dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";

/* @Sumit:
 * Stats on all the custom notifications sent in the previous week   */
function getnotificationData($day) {
	global $conn_reporting;
	$sql="select t.source,t.gender,t.type, t.count, case when r1.count_received is null then r2.count_received else 
(case when r2.count_received is null then r1.count_received else 
(r1.count_received + r2.count_received)end ) end  as count_received, 
case when o1.count_open is null then o2.count_open else 
(case when o2.count_open is null then o1.count_open else 
(o1.count_open + o2.count_open)end ) end  as count_open from  (select case when event_status is not null then event_status else event_type  end as type, source, gender, count(*) as count
from action_log_new aln join user u on u.user_id = aln.user_id  
where tstamp > DATE_SUB(curdate(), INTERVAL $day DAY) and activity='push_sent'
group by source, gender, type) t
left join ( select source,gender,event_status,  COUNT(*) as count_received
FROM action_log_new aln join user u 
on  aln.user_id=u.user_id 
where  aln.user_id is not null and aln.activity='push' AND aln.event_type='push_received' 
 AND aln.tstamp >= DATE_SUB(curdate(), INTERVAL $day DAY) group by source,gender,event_status) r1 on  
r1.event_status= t.type and r1.source=t.source and r1.gender=t.gender
left join (select source,gender,event_status,  COUNT(*) as count_received
FROM action_log_new aln join user u 
on  aln.device_id=u.device_id 
where  aln.user_id is null and aln.activity='push' AND aln.event_type='push_received' 
 AND aln.tstamp >= DATE_SUB(curdate(), INTERVAL $day DAY) group by source,gender,event_status ) r2 on 
r2.event_status= t.type and r2.source=t.source and r2.gender=t.gender
left join ( select source,gender,event_status,  COUNT(*) as count_open
FROM action_log_new aln join user u 
on  aln.user_id=u.user_id 
where  aln.user_id is not null and aln.activity='push' AND aln.event_type='push_open' 
 AND aln.tstamp >= DATE_SUB(curdate(), INTERVAL $day DAY) group by source,gender,event_status) o1 on  
o1.event_status= t.type and o1.source=t.source and o1.gender=t.gender
left join (select source,gender,event_status,  COUNT(*) as count_open
FROM action_log_new aln join user u 
on  aln.device_id=u.device_id 
where  aln.user_id is null and aln.activity='push' AND aln.event_type='push_received' 
 AND aln.tstamp >= DATE_SUB(curdate(), INTERVAL $day DAY) group by source,gender,event_status ) o2 on 
o2.event_status= t.type and o2.source=t.source and o2.gender=t.gender";
	
	$res = $conn_reporting->Execute($sql);
	$stats_array= array();
		if($res->RowCount()>0){
			while ($row = $res->FetchRow()){
				$stats_array[$row['source']][]= $row;
			}
		}
		return $stats_array;
}



   $date = date ( 'Y-m-d' );
  
try{
	if(php_sapi_name() === 'cli'){
	$options=getopt ( "d:" );
	$date= date('Y-m-d',strtotime('today'));
	if ($options[d]){
		$day = $options[d];
		$subject="Push Notifications Reporting for last $day days :".$date;
	} else {
		$day= 1; 
		$subject="Push Notifications Reporting for last 1 day :".$date;
	}
	
	$stats= array(); 
	$stats= getnotificationData($day);
	
	
   $smarty->assign("data", $stats);
    $final=$smarty->Fetch(dirname ( __FILE__ ).'/../templates/reporting/notificationReport.tpl');
   // echo $final;
   $to = $emailIdsForReporting['notification_reporting'] ;
		//$to= "sumit.kumar@trulymadly.com";
    $from = "admintm@trulymadly.com";
    Utils::sendEmail($to, $from, $subject, $final,true); 
    
   }
}
catch (Exception $e){
	echo $e;
}



?>