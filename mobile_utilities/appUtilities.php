<?php

require_once dirname ( __FILE__ ) . "/../include/function.php";
//require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../DBO/userUtilsDBO.php";


/**
 * all the app related data activities like fetching data or updating the data
 * @author himanshu
 *
 */
class appUtilities{

	private $user_id;

	function __construct($userId){
		$this->user_id = $userId;
	}
	public function fetchGcmDate(){
		$sql =" SELECT registration_id FROM user_gcm_current where user_id = ?";
		$result = Query::SELECT($sql, array($this->user_id));
		return $result;
	}
	
	public function suspendUser(){
		$sql = "SELECT status from user where user_id = ?";
		$row = Query::SELECT($sql, array($this->user_id), Query::$__slave,true );
		
		$status= $row['status'];
		$uuDBO = new userUtilsDBO();
		$uuDBO->suspend($this->user_id, null, $status);
		
		/*$sql = "UPDATE user set status = 'suspended' where user_id = ?";
		$id = Query::UPDATE($sql, array($this->user_id));*/
	}
}

try {
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	if(isset($user_id)){
		$appUtil = new appUtilities($user_id);

		if(isset($_REQUEST['get_gcm_id']) && $_REQUEST['get_gcm_id'] =="true"){
			$data = $appUtil->fetchGcmDate();
			echo json_encode($data);
			die;
		}
		if(isset($_REQUEST["suspend"]) && $_REQUEST["suspend"] == "true"){
			$appUtil->suspendUser();
			$output['responseCode'] = 200;
			echo json_encode($output); die;
		}
	}
}
catch (Exception $e){
	trigger_error("PHP WEB: ". $e->getTraceAsString(), E_USER_WARNING);
}

?>