<?php 
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/pushNotification.php";
require_once dirname ( __FILE__ ) . "/../logging/EventTrackingClass.php";

/* Author : Sumit   */
class customNotifications {
	protected $conn_reporting;
	protected $admin_id;
	protected $imageurl;
	  
	
	function __construct() {
		global $admin_id,$imageurl,$conn_reporting;
		$this->conn_reporting = $conn_reporting;
		$this->admin_id = $admin_id;
		$this->imageurl = $imageurl;
	}
	
 /*Sends push notifications to devices only who have installed the app but not logged in  */
function installedonly () {
	$Trk = new EventTrackingClass();
	$countpas=0;
	$sql="SELECT registration_id,device_id  FROM user_gcm_current ugc WHERE ugc.status='install' and app_version_code>=42 and ugc.user_id is null and (source='androidApp' or source is null)";  
	//$sql="SELECT registration_id,device_id  FROM user_gcm_current ugc WHERE device_id ='d19e9a9edd6aa3d4'";
	$res = $this->conn_reporting->execute($sql);
	$resu= $res->GetRows();
	$allcalls=count($resu);
	/* divide this array into chunks of 500 */
	$sub_res=array_chunk($resu, 500);
	$data="Register to meet tons of interesting singles.";
	$title_text='TrulyMadly';
	$ticker_text="Message from TrulyMadly";
	$uu = new UserUtils();
	$user_data = $uu->getNamenPic($this->admin_id);
	if(isset($user_data['thumbnail']))
		$pic_url = $this->imageurl.$user_data['thumbnail'];
	foreach ($sub_res as $r=>$c){
		$i=0;
		$regIDs = array();
		/*make an array of all registration Ids in this chunk of array  */
		foreach ($c as $row=>$col){
            $regIDs[$i] = $col['registration_id'];
			$i++;
		}
		//var_dump($regIDs);
		$silentnoti = new pushNotification();
		$count=$silentnoti->sendSilentNotification($regIDs,  array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => "installedOnly_push",
 			"title_text"=>$title_text,"push_type"=>"PROMOTE"),$this->admin_id);
		$countpas=$countpas+$count["success"];
		$result=$count["result"]; 
		$r_count= count($result);
		$deviceIds="";
		/*if the notification was sent , then append the device ids to $deviceIds  */
		for($i=0;$i<$r_count; $i++){
			if($result[$i]["message_id"]){
					$deviceIds .= "'".$c[$i]["device_id"]."',";
					} 
		}
		$deviceIds = rtrim($deviceIds, ",");
		//var_dump($deviceIds);
		/*save the success device ids to action_log_new  */
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent", "event_status" => "installedOnly_push", "source" => "android_app", "event_info" => array("ids"=>$deviceIds) );
		$Trk->logActions($data_logging);
	}
	return  array ('count'=>$countpas);
}

/*Sends push Notifications to users who have registered but not completed the profile  */
function registeredNotComplete () {
	$Trk = new EventTrackingClass();
   $sql="SELECT u.user_id, u.fname,u.registered_at, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE u.status='incomplete' AND ugc.status='login' AND
 u.registered_at<=DATE_SUB(NOW(), INTERVAL 2 HOUR) AND ugc.app_version_code>=35 and (source='androidApp' or source is null) GROUP BY u.user_id";
  // $sql="SELECT u.user_id, u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE ugc.status='login' AND ugc.app_version_code>=38 GROUP BY u.user_id LIMIT 1200";
   $res = $this->conn_reporting->Execute($sql);
   $alluserIds= $res->GetRows();
   $allcalls = 0;
   $allSIds="";
   $title_text='TrulyMadly';
	$ticker_text="Message from TrulyMadly";
   $uu = new UserUtils();
   $user_data = $uu->getNamenPic($this->admin_id);
  if(isset($user_data['thumbnail']))
	$pic_url = $this->imageurl.$user_data['thumbnail'];
  /*split the the array into chunks of 1000  */
  $user_ids=array_chunk($alluserIds, 1000);
  foreach ($user_ids as $row=>$col){
     $successIds="";
     $scount=0; 
       foreach ($col as $r=>$c){
 		$registration_ids =array () ;
 		$user_id=$c['user_id'];
 		$data= $c['fname'].", will you let an incomplete registration keep you from finding The One?";
 		/*make an array of all registration_ids against a user_id  */
 		$registration_ids= explode("," , $c['regIds']);
 	
 		//var_dump ($registration_ids); 
 		$silentnoti = new pushNotification();
 		$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" =>"incomplete_push",
 			"title_text"=>$title_text,"push_type"=>"PROMOTE"),$this->admin_id);
 		
 		/*if the notification was sent , then append the user ids to $successIds  */
 			if ($result['success']>0){
 			$successIds .= $user_id.","; 
 			$scount++;
 			}			
      	}
 	$successIds = rtrim($successIds, ",");
 
 
   /*save the success users ids to action_log_new  */	
 	$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent", "event_status" =>"incomplete_push", "source" => "android_app", "event_info" => array("ids"=>$successIds));
 	$Trk->logActions($data_logging);
 	$allcalls = $allcalls + $scount;
 	$allSIds .= $successIds.",";
    }
    $allSIds= rtrim($allSIds,",");
    return array('count'=>$allcalls, 'userIds'=>$allSIds);
  }

  
  /*Sends push Notifications to users who have completed the profile but have not become authentic  */
   
function registeredNotAuthentic () { 
	$Trk = new EventTrackingClass(); 
	$sql="SELECT u.user_id, u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE 
	u.status='non-authentic'  and (source='androidApp' or source is null) AND (( ugc.status='login' AND ugc.app_version_code>=35) OR (ugc.status ='logout' AND ugc.app_version_code>=42) ) GROUP BY u.user_id";
	//$sql="SELECT u.user_id, u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE u.status='non-authentic' and ugc.status='login' AND ugc.app_version_code>=38 GROUP BY u.user_id LIMIT 1200";
	$res = $this->conn_reporting->Execute($sql);
	$alluserIds= $res->GetRows();
	 $allcalls = 0;
	 $allSIds="";
	$title_text='TrulyMadly';
	$ticker_text="Message from TrulyMadly";
	$uu = new UserUtils();
	$user_data = $uu->getNamenPic($this->admin_id);
	if(isset($user_data['thumbnail']))
		$pic_url = $this->imageurl.$user_data['thumbnail'];
	/*split the the array into chunks of 1000  */
	$user_ids=array_chunk($alluserIds, 1000);

	foreach ($user_ids as $row=>$col){
		 $successIds="";
          $scount=0;
	foreach ($col as $r=>$c){
		$registration_ids =array () ;
		$user_id=$c['user_id'];
		$data= $c['fname'].", Verify yourself to see who has liked you!";
		
		/*make an array of all registration_ids against a user_id  */
		$registration_ids= explode("," , $c['regIds']);
		
		$silentnoti = new pushNotification();
		$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => "non_authentic_push",
 				"title_text"=>$title_text,"push_type"=>"TRUST"),$this->admin_id);
		
		/*if the notification was sent , then append the user ids to $successIds  */
	    if ($result['success']>0){
	    	$successIds .= $user_id.",";
	        $scount++; 
      	}
	  }
		$successIds = rtrim($successIds, ","); 
		
		/*save the success users ids to action_log_new  */
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => "non_authentic_push", "source" => "android_app", "event_info" => array("ids"=>$successIds));
		$Trk->logActions($data_logging);
		$allcalls = $allcalls + $scount;
		$allSIds .= $successIds.",";
	} 
	$allSIds= rtrim($allSIds,",");
	return array('count'=>$allcalls, 'userIds'=>$allSIds);
}

/*Sends push notifications to users who have not logged in from last one week and have unread messages.  */

function AuthInactiveMessages (){
	$Trk = new EventTrackingClass();
	$sql="SELECT DISTINCT(u.user_id), u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id
JOIN messages_queue mq ON ugc.user_id=mq.receiver_id WHERE u.status='authentic' AND (( ugc.status='login' AND ugc.app_version_code>=35) OR (ugc.status ='logout' AND ugc.app_version_code>=42) )
 AND u.last_login<=DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND mq.tStamp>=u.last_login and (source='androidApp' or source is null) GROUP BY u.user_id"; 
   //$sql = "SELECT DISTINCT(u.user_id), u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE u.user_id IN (16690) AND ugc.app_version_code>=35 GROUP BY u.user_id"; 
   $res = $this->conn_reporting->Execute($sql);
   $alluserIds= $res->GetRows();
   $allcalls=0;
 
   $title_text='TrulyMadly';
   $ticker_text="Message from TrulyMadly";
   $uu = new UserUtils();
   $user_data = $uu->getNamenPic($this->admin_id);
   if(isset($user_data['thumbnail']))
   	$pic_url = $this->imageurl.$user_data['thumbnail'];
   /*split the the array into chunks of 1000  */
   $user_ids=array_chunk($alluserIds, 1000);
   
   foreach ($user_ids as $row=>$col){
	   	$successIds="";
	   	$scount=0;
	   foreach ($col as $r=>$c){
	   		$registration_ids =array () ;
			$user_id=$c['user_id'];
			$data= $c['fname'].",  you have unread messages!";
			
			/*make an array of all registration_ids against a user_id  */
			$registration_ids= explode("," , $c['regIds']);
			
			$silentnoti = new pushNotification();
	//		$result=$silentnoti->sendSilentNotification($registration_ids, array("push_type"=>"SILENT"),$this->admin_id);
			$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => "inactive_messages_push",
	 				"title_text"=>$title_text,"class_name"=>"com.trulymadly.android.app.MessageListActivity","push_type"=>"ACTIVITY"),$this->admin_id);
			/* if the notification was sent , then append the user ids to $successIds  */
		    if ($result['success']>0) {
	 			$successIds .= $user_id.",";
		        $scount++;
		    }
		        // echo "<br> success <br>";
      	}
		$successIds = rtrim($successIds, ","); 
		
		/*save the success users ids to action_log_new  */
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => "inactive_messages_push", "source" => "android_app", "event_info" => array("ids"=>$successIds));
		$Trk->logActions($data_logging);
		$allcalls = $allcalls + $scount;
		$allSIds .= $successIds.",";
	} 
	$allSIds= rtrim($allSIds,",");
	return array('count'=>$allcalls, 'userIds'=>$allSIds);
}

/* sends push notifications to users who have have not logged in from last one week and have received a mutual like since their last login  */

function AuthInactiveMutualLikes($exIds){
   $Trk = new EventTrackingClass();
   if ($exIds == null || $exIds == "")
   {
   	    $NewexIds= $this->admin_id;
   	    $allSIds="";
   }
    else {
    	$NewexIds= $exIds;
    	$allSIds =$exIds.",";
     }
	$sql="SELECT DISTINCT(u.user_id), u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id
JOIN user_mutual_like uml ON (ugc.user_id=uml.user1 OR ugc.user_id=uml.user2) WHERE u.status='authentic' and (source='androidApp' or source is null) AND 
 (( ugc.status='login' AND ugc.app_version_code>=35) OR (ugc.status ='logout' AND ugc.app_version_code>=42) ) AND u.status='authentic' 
	AND ugc.user_id NOT IN ($NewexIds) AND u.last_login<=DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND uml.timestamp>=u.last_login GROUP BY u.user_id ";
	// $sql = "SELECT DISTINCT(u.user_id), u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE u.user_id IN (16690) AND ugc.app_version_code>=35 GROUP BY u.user_id"; 
   $res = $this->conn_reporting->Execute($sql); 
   $alluserIds= $res->GetRows(); 
   
   $allcalls=0;
   $title_text='Trulymadly';
   $ticker_text="Message from TrulyMadly";
   $uu = new UserUtils();
   $user_data = $uu->getNamenPic($this->admin_id);
   if(isset($user_data['thumbnail']))
   	$pic_url = $this->imageurl.$user_data['thumbnail'];

   /*split the the array into chunks of 1000  */
   $user_ids=array_chunk($alluserIds, 1000); 
   foreach ($user_ids as $row=>$col){
   	$successIds="";
   	$scount=0;
   foreach ($col as $r=>$c){
   $registration_ids =array () ;
		$user_id=$c['user_id'];
		$data= $c['fname'].", someone you liked has liked you too!";
		
		/*make an array of all registration_ids against a user_id  */
		$registration_ids= explode("," , $c['regIds']);
		
		$silentnoti = new pushNotification();
//		$result=$silentnoti->sendSilentNotification($registration_ids, array("push_type"=>"SILENT"),$this->admin_id);
		$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => "inactive_mutual_like_push",
 				"title_text"=>$title_text,"class_name"=>"com.trulymadly.android.app.MutualLikesActivity","push_type"=>"ACTIVITY"),$this->admin_id);
		
		/*if the notification was sent , then append the user ids to $successIds  */
	    if ($result['success']>0){
 			$successIds .= $user_id.",";
	        $scount++;
	    } 
      	}
		$successIds = rtrim($successIds, ","); 
		
		/*save the success users ids to action_log_new  */
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => "inactive_mutual_like_push", "source" => "android_app", "event_info" => array("ids"=>$successIds));
		$Trk->logActions($data_logging);
		$allcalls = $allcalls + $scount;
		$allSIds .= $successIds.",";
	} 
	$allSIds= rtrim($allSIds,",");
	return array('count'=>$allcalls, 'userIds'=>$allSIds);
}

/* sends push notifications to users who have have not logged in from last one week and have received a like since their last login  */
function AuthInactiveOnlyLikes($exIds){
	   $Trk = new EventTrackingClass();
 if ($exIds == null || $exIds == "")
   {
   	    $NewexIds= $this->admin_id;
   	    $allSIds="";
   }
    else {
    	$NewexIds= $exIds;
    	$allSIds =$exIds.",";
     }
	$sql="SELECT DISTINCT(u.user_id), u.fname , GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user u JOIN user_like ul ON u.user_id=ul.user2 
	JOIN user_gcm_current ugc ON ugc.user_id=u.user_id WHERE u.status='authentic'  and (source='androidApp' or source is null) AND  
	(( ugc.status='login' AND ugc.app_version_code>=35) OR (ugc.status ='logout' AND ugc.app_version_code>=42) ) 
	AND u.user_id NOT IN ($NewexIds) AND u.last_login<=DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND ul.timestamp>=u.last_login GROUP BY u.user_id";
	//$sql = "SELECT DISTINCT(u.user_id), u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE u.user_id IN (16690) AND ugc.app_version_code>=35 GROUP BY u.user_id";
   $res = $this->conn_reporting->Execute($sql);
   $alluserIds= $res->GetRows();
   $allcalls=0;
 $title_text='Trulymadly';
	$ticker_text="Message from TrulyMadly";
   $uu = new UserUtils();
   $user_data = $uu->getNamenPic($this->admin_id);
   if(isset($user_data['thumbnail']))
   	$pic_url = $this->imageurl.$user_data['thumbnail'];
   
   /*split the the array into chunks of 1000 each  */
   $user_ids=array_chunk($alluserIds, 1000);
   
   foreach ($user_ids as $row=>$col){
   	$successIds="";
   	$scount=0;
   foreach ($col as $r=>$c){
   $registration_ids =array () ;
		$user_id=$c['user_id'];
		$data= $c['fname'].", someone in your matches has liked you!";
		
		/*make an array of all registration_ids against a user_id  */
		$registration_ids= explode("," , $c['regIds']);
		
		$silentnoti = new pushNotification();
//		$result=$silentnoti->sendSilentNotification($registration_ids, array("push_type"=>"SILENT"),$this->admin_id);
		$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => "inactive_likes_push",
 				"title_text"=>$title_text,"push_type"=>"PROMOTE"),$this->admin_id);

		/*if the notification was sent , then append the user ids to $successIds  */
	    if ($result['success']>0) 
	     {
 			$successIds .= $user_id.",";
	        $scount++;
	       // echo "<br> Success <br>";
	     }
      	}
		$successIds = rtrim($successIds, ","); 
		
		/*save the success users ids to action_log_new  */
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => "inactive_likes_push", "source" => "android_app", "event_info" => array("ids"=>$successIds));
		$Trk->logActions($data_logging);
		$allcalls = $allcalls + $scount;
		$allSIds .= $successIds.",";
	} 
	$allSIds= rtrim($allSIds,",");
	return array('count'=>$allcalls, 'userIds'=>$allSIds);
}
/* sends push notifications to users who have have not logged in from last one week and have received no likes since their last login  */

function AuthIncativeNolikes($exIds){
	$Trk = new EventTrackingClass();
 if ($exIds == null || $exIds == "")
   {
   	    $NewexIds= $this->admin_id;
   	    $allSIds="";
   }
    else {
    	$NewexIds= $exIds;
    	$allSIds =$exIds.",";
     }
	$sql="SELECT DISTINCT(u.user_id), u.fname , GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user u  JOIN user_gcm_current ugc ON ugc.user_id=u.user_id
WHERE u.status='authentic' AND  (( ugc.status='login' AND ugc.app_version_code>=35) OR (ugc.status ='logout' AND ugc.app_version_code>=42) )
 AND u.last_login<=DATE_SUB(CURDATE(), INTERVAL 7 DAY) and (source='androidApp' or source is null) AND u.user_id NOT IN ($NewexIds) GROUP BY u.user_id";
	// $sql = "SELECT DISTINCT(u.user_id), u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE u.user_id IN (16690) AND ugc.app_version_code>=35 GROUP BY u.user_id";
	$res = $this->conn_reporting->Execute($sql);
   $alluserIds= $res->GetRows();
   $allcalls=0;
   $title_text='Trulymadly';
	$ticker_text="Message from TrulyMadly";
   $uu = new UserUtils();
   $user_data = $uu->getNamenPic($this->admin_id);
   if(isset($user_data['thumbnail']))
   	$pic_url = $this->imageurl.$user_data['thumbnail'];
   
   /*split the the array into chunks of 1000 each  */
   $user_ids=array_chunk($alluserIds, 1000); 
   foreach ($user_ids as $row=>$col){
   	$successIds="";
   	$scount=0;
   foreach ($col as $r=>$c){
   $registration_ids =array () ;
		$user_id=$c['user_id'];
		$data= $c['fname'].", checkout your new matches!";
		
		/*make an array of all registration_ids against a user_id  */
		$registration_ids= explode("," , $c['regIds']);
		
		$silentnoti = new pushNotification();
//		$result=$silentnoti->sendSilentNotification($registration_ids, array("push_type"=>"SILENT"),$this->admin_id);
		$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => "inactive_nolikes_push",
 				"title_text"=>$title_text,"push_type"=>"PROMOTE"),$this->admin_id);
		
		/*if the notification was sent , then append the user ids to $successIds  */
	    if ($result['success']>0) {
 			$successIds .= $user_id.","; 
	        $scount++;
	       // echo "<br> Success <br>";
	     }
      	}
		$successIds = rtrim($successIds, ","); 
		
		/*save the success users ids to action_log_new  */
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => "inactive_nolikes_push", "source" => "android_app", "event_info" => array("ids"=>$successIds));
		$Trk->logActions($data_logging);
		$allcalls = $allcalls + $scount;
		$allSIds .= $successIds.",";
	} 
	$allSIds= rtrim($allSIds,",");
	return array('count'=>$allcalls, 'userIds'=>$allSIds);
	}	
	
	
	/* sends push notifications with link to photo blog to authentic users who registered in last one week   */
	 
function authenticPhoto (){ 
		$Trk = new EventTrackingClass(); 
		
		$sql="SELECT DISTINCT(u.user_id), u.fname , GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds, u.gender FROM user u  JOIN user_gcm_current ugc ON ugc.user_id=u.user_id
		WHERE u.status='authentic' AND  ugc.status='login' AND ugc.app_version_code>=42 and (source='androidApp' or source is null)
		AND u.registered_at>=DATE_SUB(CURDATE(), INTERVAL 7 DAY)  GROUP BY u.user_id";
		// $sql = "SELECT DISTINCT(u.user_id), u.fname, GROUP_CONCAT(DISTINCT(ugc.registration_id)) AS regIds FROM user_gcm_current ugc JOIN user u ON u.user_id=ugc.user_id WHERE u.user_id IN (16690) AND ugc.app_version_code>=35 GROUP BY u.user_id";
		$res = $this->conn_reporting->Execute($sql);
		$alluserIds= $res->GetRows();
		$allcalls=0;
		$title_text='TrulyMadly';
		$ticker_text="Message from TrulyMadly";
		$uu = new UserUtils();
		$user_data = $uu->getNamenPic($this->admin_id);
		if(isset($user_data['thumbnail']))
			$pic_url = $this->imageurl.$user_data['thumbnail'];
		
		/*split the the array into chunks of 1000 each  */
		$user_ids=array_chunk($alluserIds, 1000);
		foreach ($user_ids as $row=>$col){
			$successIds="";
			$scount=0;
			foreach ($col as $r=>$c){
				$registration_ids =array () ;
				$user_id=$c['user_id'];
				if($c['user_id']=='M') {
					$data= "Get more girls to like your DP. Here's some tips that'll help you pick the perfect one";
					$link = 'http://bit.ly/tmdpguys';
				} else {
					$data= "Get more guys to like your DP. Here's some tips that'll help you pick the perfect one.";
					$link = 'http://bit.ly/tmdpgirls';
				}
				
				/*make an array of all registration_ids against a user_id  */
				$registration_ids= explode("," , $c['regIds']);
				
				$silentnoti = new pushNotification();
				//		$result=$silentnoti->sendSilentNotification($registration_ids, array("push_type"=>"SILENT"),$this->admin_id);
				$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => "authentic_photo_push",
						"title_text"=>$title_text,"push_type"=>"WEB","web_url" => $link),$this->admin_id);
				
				/*if the notification was sent , then append the user ids to $successIds  */
				if ($result['success']>0){
					$successIds .= $user_id.",";
				   $scount++;
				// echo "<br> Success <br>";
				}
			}
			$successIds = rtrim($successIds, ",");
			
			/*save the success users ids to action_log_new  */
			$data_logging = array();
			$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => "authentic_photo_push", "source" => "android_app", "event_info" => array("ids"=>$successIds));
			$Trk->logActions($data_logging);
			$allcalls = $allcalls + $scount;
			$allSIds .= $successIds.",";
		}
		$allSIds= rtrim($allSIds,",");
		return array('count'=>$allcalls, 'userIds'=>$allSIds);
		}
		
}


try {
	$options=getopt ( "d:" );
	
	if ($options[d]){
		$day = $options[d]; 
	} else {
		$day= date ( 'w' ,strtotime ('today'));
	}
	$date= date('Y-m-d',strtotime('today'));
	echo $day;
	$customNotifications = new customNotifications();
	$mailmessage="";
	  /* 2 Days a week , Monday and Thursday */
 	if ($day==1 || $day==4) {
 	echo "<br>  Installed Only";
    $installedOnly=$customNotifications->installedonly();
    var_dump($installedOnly);
	$mailmessage .= "Installed Only   ".$installedOnly['count']."<br>";
 	}
	  /* 3 Days a week, Monday, Wednesday and Friday */
	if ($day==1 || $day==3 || $day==5) {
	echo "<br>  Incomplete";
	$incomplete=$customNotifications->registeredNotComplete ();
	var_dump($incomplete);
	$mailmessage .= "Incomplete   ".$incomplete['count']."<br>";
	}
    /*  2 Days a week, Monday, Thursday */
	if ($day==1 || $day==4) {
	echo "<br>  Non-Authentic";
	$nonAuthentic=$customNotifications->registeredNotAuthentic (); 
	var_dump($nonAuthentic);
	$mailmessage .= "Non_authentic   ".$nonAuthentic['count']."<br>";
	}
 	/* 2 Days a week, Tuesday and friday */
	if ($day== 2 || $day ==5) {
     echo "<br> Unread messages";
	$messages=$customNotifications->AuthInactiveMessages ();
	var_dump($messages);
	$mailmessage .= "Unread messages  ".$messages['count']."<br>";
	
	echo "<br>  Mutual Likes";
	$mutualLikes=$customNotifications-> AuthInactiveMutualLikes ($messages['userIds']);
	var_dump($mutualLikes);
	$mailmessage .= "Mutual Likes   ".$mutualLikes['count']."<br>";
	
	echo "<br> received likes";
	$likesonly=$customNotifications-> AuthInactiveOnlyLikes ($mutualLikes['userIds']);
	var_dump($likesonly);
	$mailmessage .= "Received Likes  ".$likesonly['count']."<br>";

	echo "<br>  No likes";
	$nolikes= $customNotifications-> AuthIncativeNolikes ($likesonly['userIds']);
	var_dump($nolikes);
	$mailmessage .= "No likes   ".$nolikes['count']."<br>";
  	}
  	/* Once a week, Wednesday */
  	if ($day== 3){
  		echo "<br>  Authentic Photo";
  		$authPhoto=$customNotifications->authenticPhoto ();
  		var_dump($authPhoto);
  		$mailmessage .= "Authentic Photo   ".$authPhoto['count']."<br>";
  	} 

	$to = "sumit.kumar@trulymadly.com,shashwat@trulymadly.com";  
	$subject = "Custom Push Notifications Count for ".$date;
	$from = "sumit.kumar@trulymadly.com"; 
	Utils::sendEmail($to, $from, $subject, $mailmessage,true);
	
} catch (Exception $e) {
	echo $e;
}


?>