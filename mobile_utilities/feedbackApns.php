<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";



/* Author : Sumit
 * To track the uinstalls of IOS devices by APNS feedback service */
class feedbackApns {
	private $ios_notification_passphrase;
	private $ios_notification_filepath;
	private $ios_notification_feedback_url;
	private  $conn_slave;
	private  $conn_master;

	function __construct (){
		global $ios_notification_passphrase, $ios_notification_filepath, $ios_notification_feedback_url, $conn_master, $conn_slave;
		$this->ios_notification_passphrase = $ios_notification_passphrase;
		$this->ios_notification_filepath = $ios_notification_filepath;
		$this->ios_notification_feedback_url  =  $ios_notification_feedback_url;
		$this->conn_master = $conn_master;
		$this->conn_slave = $conn_slave;
	}
	
public function receiveFeedback() {
    //connect to the APNS feedback servers
    $stream_context = stream_context_create();
    stream_context_set_option($stream_context, 'ssl', 'local_cert', $this->ios_notification_filepath);
    stream_context_set_option($stream_context, 'ssl', 'passphrase', $this->ios_notification_passphrase);
    $apns = stream_socket_client($this->ios_notification_feedback_url, $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $stream_context);
    if(!$apns) {
    	echo "connection not created";
        echo "ERROR $errcode: $errstr\n";
        Utils::sendEmail($techTeamIds .",sumit.kumar@trulymadly.com", "sumit.kumar@trulymadly.com", 
                                     "Feedback  connection not created ".$baseurl, "ERROR $errcode: $errstr\n");
        return;
    }


    $feedback_tokens = array();
    echo "connection created".PHP_EOL;
    //and read the data on the connection:
    while(!feof($apns)) {
        $data = fread($apns, 38);
        if($data != null && strlen($data) >0 ) {
        	//echo "tuple present";
            $feedback_tokens[] = unpack("N1timestamp/n1length/H*device_token", $data);
        }
    }
    fclose($apns);
    /*if($feedback_tokens == array()){
    	$oneInsert['device_token']= "0abba8765c3d1505ce3891d29ce9c8f3e5e9843b51817774946a620cd660f478";
    	$oneInsert['timestamp']= 31536000;
    	$feedback_tokens[] =$oneInsert;
    }*/
    $count= $this->saveApnsResponse ($feedback_tokens);
    return $count;
}
private function saveApnsResponse($feedback_tokens){
	$count = count($feedback_tokens);
	$uni_prep= $this->conn_master->prepare("update user_gcm_current set status='uninstall', tstamp=? where registration_id = ? and status != 'uninstall'");
	foreach ($feedback_tokens as $row=>$col){
		//var_dump($col);
		$registration_id = $col['device_token']; 
		$timestamp = $col['timestamp'];
		$basetime= "1970-01-01 00:00:00";
		$uniDate = date ( 'Y-m-d H:i:s', strtotime('+'.$timestamp.' second', strtotime($basetime)));	
    	$this->conn_master->execute($uni_prep, array($uniDate,$registration_id));
	}
	return  $count;
	
}

}


?>