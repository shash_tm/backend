<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/**
 * push notification for female users registered after 18th august
 * @author Sumit
 * 
 */

function sendSurvey () {
	global $admin_id, $conn_reporting, $imageurl;
	$uu = new UserUtils(); 

	$pushNotification = new pushNotification();

	$title_text='TrulyMadly';
	$ticker_text = "Message from TrulyMadly";
	$data = "Ladies, help us help you UnSingle by answering these few questions! ";
	$url = array ( 1 => "https://www.surveymonkey.com/r/XVXWPXJ",
	               2 => "https://www.surveymonkey.com/r/X79MMF8" );
  
	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
	$pic_url = $imageurl.$user_data['thumbnail'];

	$successIds= "";
	$count=0;

	$sql= "Select distinct(u.user_id) as user_id  ,u.fname ,
			case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) < 26 then 1  else 2 end as age 
			 from user u 
join user_gcm_current ugc on ugc.user_id=u.user_id 
join user_data ud on ud.user_id = u.user_id
where  ugc.status in ('login','logout')   and u.registered_at> '2015-08-18 00:00:00'
and EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=35 and gender='f' and u.status='authentic'";  
	$res = $conn_reporting->Execute($sql); 
	
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{
			$user_id = $row['user_id'];
			$web_url = $url [$row ['age']];	
			//var_dump($web_url); die ;
			$pushNotification->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "female_survey", "title_text"=>$title_text,"push_type"=>"WEB","web_url" =>$web_url ),$admin_id);
			$successIds .= $user_id.",";
			$count++;
		}
	}

	echo "sent to ".$count;
	return array("count"=> $count, "ids"=>$successIds);

}
try
{
	sendSurvey (); 
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?> 