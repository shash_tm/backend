<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/**
 * push notification for remaining spark
 * @author Sumit
 * 
 */

function getUnsingle () {
	global $admin_id, $conn_reporting, $imageurl;
	$uu = new UserUtils();

	$pushNotification = new pushNotification();

	$title_text='TrulyMadly';
	$ticker_text = "Message from TrulyMadly";

	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
	$pic_url = $imageurl.$user_data['thumbnail'];

	$successIds= "";
	$count=0;

	$sql= "select u.user_id  from user u join user_spark_counter upc on u.user_id = upc.user_id  where spark_count > 0 and u.gender='M' ";  
	$res = $conn_reporting->Execute($sql);
	
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{
			$user_id = $row['user_id'];
			$push_data = "What are you waiting for? You still have Sparks remaining! ";
			$pushNotification->notify($user_id, array("content_text"=>$push_data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "SPARK_REMAINING", "title_text"=>$title_text,"push_type"=>"PROMOTE" ),$admin_id);
			$successIds .= $user_id.",";
			$count++;
		}
	}

	echo "sent to ".$count;
	return array("count"=> $count, "ids"=>$successIds);

}
try
{
	getUnsingle ();
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?>