<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/**
 *Push speed dating notifications
 * 
 */

function CityWise () {
	global $admin_id, $conn_reporting, $imageurl;
	$uu = new UserUtils();

	$pushNotification = new pushNotification();

	$title_text='TrulyMadly';
	$ticker_text = "Message from TrulyMadly";

	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
	$pic_url = $imageurl.$user_data['thumbnail'];

	$successIds= "";
	$count=0;

	$sql= "select  distinct(u.user_id) as user_id , u.fname , case when gc.name = 'Delhi' || gc.name ='Faridabad' || gc.name ='Gurgaon' || gc.name ='Noida' || 
gc.name ='Greater Noida' || gc.name ='Ghaziabad' then 'NCR' else  gc.name end as city from user u 
	 join user_gcm_current ugc on ugc.user_id=u.user_id 
	 join user_data ud on ud.user_id = u.user_id
     join geo_city gc on gc.city_id= ud.stay_city
	where  u.status in ('authentic', 'non-authentic') and ugc.status in ('login','logout')  and ugc.source in ('iosapp','windows_app') 
	 ";  
	$res = $conn_reporting->Execute($sql);
	
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{

			$user_id = $row['user_id'];
			if ($row['city'] == "NCR" )
			{
				$push_data = "Join India's first ever speed dating event in Delhi and UnSingle! Hosted by Tanya Nambiar" ;
				$web_url = "http://bit.ly/1JPPRfB" ;
			}
			else 
			{
				$push_data =  "Here’s how to ace that first date to make sure you get a second one!" ;
				$web_url = "http://bit.ly/1giFY01" ;
			}
				
			$pushNotification->notify($user_id, array("content_text"=>$push_data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "speed_dating_notification", "title_text"=>$title_text,"push_type"=>"WEB", "web_url" =>$web_url  ),$admin_id);
			$successIds .= $user_id.",";
			$count++;
		}
	}

	echo "sent to ".$count;
	return array("count"=> $count, "ids"=>$successIds);

}
try
{
	if(php_sapi_name() == 'cli')
	{
	   CityWise ();	
	}
	
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?>