<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";

/* Author : Sumit
    Needs to be run everyday in the evening around 7
 * sends custom push notifications to different set of users based on certain conditions  */

class EventBuyNotification {
    private $conn_reporting;
    private $matchNoticationMessage = "Spark her to get noticed faster. Buy a Sparks package now.";
    private $ticker_text = 'Message from TrulyMadly';
    private $title_text = "TrulyMadly";
    public $count_sent ;
    private $pushNotification;
    private $chunkSize = 2000;


    function __construct()
    {
        global $admin_id,$imageurl,$conn_reporting;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->pushNotification = new pushNotification();
    }


    public function sendMatchNotifications($target = null)
    {
        $sql = "select u.user_id, u.fname, gc.display_name as city from user u
                join user_app_status uas on uas.user_id=u.user_id
                join user_lastlogin ull on u.user_id = ull.user_id
                join user_data ud on u.user_id = ud.user_id
                join geo_city gc on gc.city_id = ud.stay_city and gc.state_id = ud.stay_state
                where u.status ='authentic' and uas.android_app= 'install'" ;
        if($target != null)
            $sql .= " and u.user_id = $target";
        else
            $sql .= " and ud.stay_city in (4058,4062,6453 )";
        $chunkCounter = 0 ;
//        echo $sql ;



        $web_url= array('Pune' => 'https://trulymad.ly/evnt.php?e=869_22195',
                        'Bengaluru' => 'https://trulymad.ly/evnt.php?e=848_44874',
                        'Hyderabad' => 'https://trulymad.ly/evnt.php?e=868_32593' ) ;
        $title_text = array('Pune' => 'EVC is back and is bigger than ever!',
                            'Bengaluru' => 'Find fellow EDM lovers!',
                             'Hyderabad' => 'Find fellow EDM lovers!') ;
        $body = array('Pune' => 'Interested? Learn more here.',
                        'Bengaluru' => 'Discover fun singles going for Sunburn.',
                        'Hyderabad' => 'Discover fun singles going for Sunburn.') ;

        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $user_id = $row['user_id'];
            $city = $row['city'];
            $data = array('title_text' => $title_text[$city],
                'content_text' => $body[$city],
                'push_type' => 'WEB',
                'web_url' => $web_url[$city],
                'event_status' => 'manual_events_notifications');
            if($target != null)
            {
                $data = array('title_text' => 'Find fellow EDM lovers!',
                    'content_text' => "Discover fun singles going for Sunburn.",
                    'push_type' => 'WEB',
                    'web_url' => "https://trulymad.ly/evnt.php?e=848_44874",
                    'event_status' => 'manual_events_notifications');
            }

            $this->pushNotification->notify($user_id,$data,'EVENT_PUSH');
            $this->count_sent++;
            $chunkCounter++;
            if($chunkCounter >= $this->chunkSize)
            {
                echo "sleeping for 60 seconds before pushing more notifications".PHP_EOL;
                sleep(60);
                $chunkCounter= 0;

            }
             echo "sent to $user_id".PHP_EOL;
        }
        echo "total sent to $this->count_sent";
    }






}

try {

    if(php_sapi_name() === 'cli')
    {
        $target = $argv[1];
        $matchNotification = new EventBuyNotification();
        if($target == 'all')
             $matchNotification->sendMatchNotifications();
        else if (isset($target) && $target != null)
            $matchNotification->sendMatchNotifications($target);
        else
            echo "No target";

    }




} catch (Exception $e) {
    echo $e->getMessage();
    trigger_error($e->getMessage(), E_USER_WARNING);
    Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "Custom Notification script failed".$baseurl, $e->getMessage());
}


?>