<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/**
 * push notification for females  user base aged 22-28 years in Delhi, Mumbai, Bangalore, Pune and Hyderabad
 * @author Sumit
 * 
 */

function sendSurvey () {
	global $admin_id, $conn_reporting, $imageurl;
	$uu = new UserUtils(); 

	$pushNotification = new pushNotification();

	$title_text='TrulyMadly';
	$ticker_text = "Message from TrulyMadly";
	$data = "Ladies, help us paint your town red and make your TrulyMadly experience even better!";
//	$url = array ( "first" => "https://www.surveymonkey.com/r/D8VC5CQ",
//	                   "second" => "https://www.surveymonkey.com/r/DJ58QQS" );
  
	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
	$pic_url = $imageurl.$user_data['thumbnail'];

	$successIds= "";
	$count=0;

	$sql= "Select distinct(u.user_id) as user_id , u.fname  from user u 
join user_gcm_current ugc on ugc.user_id=u.user_id 
join user_data ud on ud.user_id = u.user_id
join geo_city gc on ud.stay_city = gc.city_id 
where  ugc.status in ('login','logout')  and ugc.source in ('androidapp','iosapp') 
and gc.name in ('Delhi','Faridabad','Greater Faridabad','Gurgaon','Noida','Greater Noida','Ghaziabad','Bengaluru','Hyderabad','Pune','Mumbai')
and EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=28
and EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >21 and gender='f' and u.status='authentic'";  
	$res = $conn_reporting->Execute($sql);
	
	if($res->RowCount()>0)   
	{
		while ($row = $res->FetchRow())
		{
			$user_id = $row['user_id'];
			$web_url = "https://www.surveymonkey.com/r/SVNVC5J";	
			$pushNotification->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "source_survey", "title_text"=>$title_text,"push_type"=>"WEB","web_url" =>$web_url ),$admin_id);
			$successIds .= $user_id.",";
			$count++;
		} 
	}

	echo "sent to ".$count;
	return array("count"=> $count, "ids"=>$successIds);

}
try
{
	sendSurvey (); 
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?> 