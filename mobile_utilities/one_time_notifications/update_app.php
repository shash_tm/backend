<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/*To send one time push notifications to users with different conditions */
function newManualNotification() {
	global $imageurl, $admin_id, $conn_reporting;
	$Trk = new EventTrackingClass();
	$countpas=0;
	echo "Getting gcm ids".PHP_EOL; 
	//$sql="SELECT ugc.registration_id, ugc.device_id FROM user_gcm_current ugc WHERE ugc.app_version_code < 63 and ugc.app_version_code >=42 AND (ugc.status NOT IN ('expired','missing','uninstall'))";
	$sql= "SELECT DISTINCT(ugc.registration_id), ugc.device_id FROM user_gcm_current ugc JOIN messages_queue mq ON ugc.user_id=mq.sender_id
WHERE ugc.app_version_code <= 71  AND (ugc.status NOT IN ('expired','missing','uninstall'))";
	$res = $conn_reporting->Execute($sql);
	$resu= $res->GetRows();
	
	//$data="Fast texting, with 'awww'some stickers. Update the app now!" ;  
	$data="Lightning fast chat and expressive stickers. Update NOW!" ;
	$title_text='TrulyMadly';
	$ticker_text="Message from TrulyMadly";
	$push_type="WEB"; 
	$uu = new UserUtils();
	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
		$pic_url = $imageurl.$user_data['thumbnail'];
	
	$web_url="market://details?id=com.trulymadly.android.app";
	
	/* for logging    */
	//$event_status="app_update_52"; 
	$event_status="app_update_71";
	
	$allcalls=count($resu);
	$sub_res=array_chunk($resu, 500);
	echo "sending Notification".PHP_EOL;
	foreach ($sub_res as $r=>$c){
		$i=0;
		$regIDs = array();
		foreach ($c as $row=>$col){
			$regIDs[$i] = $col['registration_id'];
			$i++;
		} 
		$silentnoti = new pushNotification();
		$count=$silentnoti->sendSilentNotification($regIDs, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => $event_status,
							"title_text"=>$title_text,"push_type"=>$push_type,"web_url" =>$web_url ),$admin_id);
		$countpas=$countpas+$count['success'];
		echo "Sent to ".$count['success'].PHP_EOL;
		$result=$count["result"]; 
		$r_count= count($result);
		$deviceIds=""; 
		for($i=0;$i<$r_count; $i++){
			if($result[$i]["message_id"]){
					$deviceIds .= "'".$c[$i]["device_id"]."',";
					}
		} 
		$deviceIds = rtrim($deviceIds, ",");
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent", "event_status" => $event_status , "source" => "android_app", "event_info" => array("ids"=>$deviceIds));
		$Trk->logActions($data_logging);
	}
	echo "Total sent to :".$countpas." Devices"; 
}

newManualNotification();
?>