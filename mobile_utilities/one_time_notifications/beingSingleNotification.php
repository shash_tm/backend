<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/*To send one time push notifications to all authentic and non-authentic members  .
 * Jira Id- BACKEND-181 */
function newManualNotification() {
	global $imageurl, $admin_id, $conn_reporting;
	$Trk = new EventTrackingClass();
	$countpas=0;
	
	$sql="SELECT u.user_id,u.fname, u.gender, GROUP_CONCAT(DISTINCT (ugc.registration_id)) AS regIds FROM user u 
JOIN user_gcm_current ugc ON u.user_id = ugc.user_id 
WHERE u.status IN ('authentic','non-authentic') AND ugc.app_version_code >= 42 AND ugc.status IN ('login' , 'logout') 
GROUP BY u.user_id";
	 
	$res = $conn_reporting->Execute($sql);
	$resu= $res->GetRows();
	

	$title_text='TrulyMadly';
	$ticker_text="Message from TrulyMadly";
	$push_type="WEB";
	$uu = new UserUtils();
	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
		$pic_url = $imageurl.$user_data['thumbnail'];
	
	$web_url="https://www.youtube.com/watch?v=6XKXUiZwr7k";
	/* for logging    */
	$event_status="being_single_video";
	
	
	$sub_res=array_chunk($resu, 1000);
	$scount=0;
	foreach ($sub_res as $row=>$col){
		$successIds="";
		foreach ($col as $r=>$c){
		
				$data= $c['fname'].", Watch what Delhi has to say about being single.";
		
			//echo $data;
			$registration_ids = array() ;
			$registration_ids= explode("," , $c['regIds']);
			
			$silentnoti = new pushNotification(); 
			$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => $event_status,
								"title_text"=>$title_text,"push_type"=>$push_type,"web_url" =>$web_url ),$admin_id);
              var_dump($result);
			if ($result['success'] > 0) {
				echo  $c['user_id'];
				echo "<br>";
 				$successIds .= $c['user_id'].",";
	        	$scount++;
			}
      	}
		$successIds = rtrim($successIds, ","); 
		var_dump($successIds);
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => $event_status, "source" => "android_app", "event_info" => array("ids"=>$successIds));
		$Trk->logActions($data_logging);
	} 
	echo $scount;
}

newManualNotification();
?>