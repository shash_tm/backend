<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/**
 *Second push notification to users from delhi and hyderabad for unsingle mixer
 * 
 */

function getUnsingle () {
	global $admin_id, $conn_reporting, $imageurl;
	$uu = new UserUtils();

	$pushNotification = new pushNotification();

	$title_text='TrulyMadly';
	$ticker_text = "Message from TrulyMadly";
	$data = array( "delhi" => "The BIG Day has arrived folks! Click for details in case you missed it. ",
					"hybd" => "TrulyMadly's UnSingle Mixer is just a day away! In case you haven't saved the date.");
	$urls = array( "delhi" => "https://www.facebook.com/events/375221412669444/",
					"hybd" => "https://www.facebook.com/events/470161909807175/");

	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
	$pic_url = $imageurl.$user_data['thumbnail'];

	$successIds= "";
	$count=0;

	$sql= "select distinct(u.user_id) as user_id , u.fname , case when stay_city = 4062 then 'pune' else
	 (case when stay_city in (6453, 47948) then 'hybd' else 'delhi' end ) end as city from user u 
	 join user_gcm_current ugc on ugc.user_id=u.user_id 
	 join user_data ud on ud.user_id = u.user_id
	where  u.status in ('authentic', 'non-authentic') and ugc.status in ('login','logout')  and u.gender='F' and ugc.source in ('iosapp','windows_app') 
	and ((stay_city  in (47965,47968, 47964, 47967,47966 ) and stay_state = 2168) or stay_city in (6453, 47948) )";  
	$res = $conn_reporting->Execute($sql);
	 
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{ 
			$user_id = $row['user_id'];
			$push_data = $data[$row['city']];
			$web_url = $urls[$row['city']];
				
			$pushNotification->notify($user_id, array("content_text"=>$push_data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "unsingle_mixer", "title_text"=>$title_text,"push_type"=>"WEB","web_url" =>$web_url ),$admin_id);
			$successIds .= $user_id.",";
			$count++;
		}
	}

	echo "sent to ".$count;
	return array("count"=> $count, "ids"=>$successIds);

}
try
{
	if(php_sapi_name() == 'cli')
	{
	   getUnsingle ();	
	}
	
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?>