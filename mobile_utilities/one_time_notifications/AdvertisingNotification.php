<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/*To send one time push notifications to all advertising and media persons  .
 * Jira Id-  */
function newManualNotification() {
	global $imageurl, $admin_id, $conn_reporting;
	$Trk = new EventTrackingClass();
	$countpas=0;
	
	$sql="SELECT u.user_id,u.fname, u.gender, GROUP_CONCAT(DISTINCT (ugc.registration_id)) AS regIds FROM user u 
JOIN user_gcm_current ugc ON u.user_id = ugc.user_id 
JOIN user_data ud ON u.user_id=ud.user_id
WHERE u.status IN ('authentic','non-authentic') AND ugc.app_version_code >= 42 AND ugc.status IN ('login' , 'logout') 
AND (ud.industry in (1,8,21) OR ud.highest_degree IN (2,22) )
GROUP BY u.user_id";
	 
	$res = $conn_reporting->Execute($sql);
	$resu= $res->GetRows();
	

	$title_text='TrulyMadly';
	$ticker_text="Message from TrulyMadly";
	$push_type="WEB";
	$uu = new UserUtils();
	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
		$pic_url = $imageurl.$user_data['thumbnail'];
	
	$web_url="http://blog.trulymadly.com/heres-date-someone-media-world/";
	
	/* for logging    */
	$event_status="date_advertising_prof";
	
	
	$sub_res=array_chunk($resu, 500);
	$scount=0;
	foreach ($sub_res as $row=>$col){
		$successIds="";
		foreach ($col as $r=>$c){
			if($c['gender'] == 'F'){
				$data= $c['fname'].", know what's making you so popular with the guys.";
			} else {
				$data= $c['fname'].", know what's making you so popular with the girls.";
			}
			echo $data;
			$registration_ids = array() ;
			$registration_ids= explode("," , $c['regIds']);
			
			$silentnoti = new pushNotification(); 
			$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => $event_status,
								"title_text"=>$title_text,"push_type"=>$push_type,"web_url" =>$web_url ),$admin_id);

			if ($result['success'] > 0) {
 				$successIds .= $c['user_id'].",";
	        	$scount++;
			}
      	}
		$successIds = rtrim($successIds, ","); 
		$data_logging = array();
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => $event_status, "source" => "android_app", "event_info" => $successIds);
		$Trk->logActions($data_logging);
	} 
	echo $scount;
}

newManualNotification();
?>