<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/**
 * push notification for all female and male users about the survey 
 * @author Sumit
 * 
 */

function sendSurvey () {
	global $admin_id, $conn_reporting, $imageurl;
	$uu = new UserUtils(); 

	$pushNotification = new pushNotification();

	$title_text='TrulyMadly';
	$ticker_text = "Message from TrulyMadly";
	$data = "We'd love to hear about your dates with your TrulyMadly matches!";
	$male = array( 1 => "https://www.surveymonkey.com/r/YSXW2QR" ,
			       2 => "https://www.surveymonkey.com/r/YSPZRV5" ,
			       3 => "https://www.surveymonkey.com/r/YRLFRJV" ) ;
			
	$female = array( 1 => "https://www.surveymonkey.com/r/HBH7S8Q" ,
			         2 => "https://www.surveymonkey.com/r/YDNLR98" ,
			         3 => "https://www.surveymonkey.com/r/YY8MRXC" ) ;
	
	$url = array ( 'M' => $male,
	               'F' => $female );
  
	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
	$pic_url = $imageurl.$user_data['thumbnail'];

	$successIds= "";
	$count=0;

	$sql= "Select distinct(u.user_id) as user_id  ,u.fname , u.gender,
case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) < 23 then 1  else 
(case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) < 28 then 2 else 3 end ) end as age 
			 from user u 
join user_gcm_current ugc on ugc.user_id=u.user_id 
join user_data ud on ud.user_id = u.user_id
where  ugc.status in ('login','logout')    
and EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=35 and u.status='authentic' ";  
	$res = $conn_reporting->Execute($sql); 
	
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{
			$user_id = $row['user_id'];
			$web_url = $url [$row ['gender']] [$row ['age']];	
			//echo $web_url ;
			//var_dump($web_url); die ;
			$pushNotification->notify($user_id, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "all_user_survey", "title_text"=>$title_text,"push_type"=>"WEB","web_url" =>$web_url ),$admin_id);
			$successIds .= $user_id.",";
			$count++;
		}
	}

	echo "sent to ".$count;
	return array("count"=> $count, "ids"=>$successIds);

}
try
{
	sendSurvey (); 
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?> 