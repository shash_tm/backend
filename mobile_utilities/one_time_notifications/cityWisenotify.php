<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/**
 *Push notification to users from 
 * 
 */

function CityWise () {
	global $admin_id, $conn_reporting, $imageurl;
	$uu = new UserUtils();

	$pushNotification = new pushNotification();

	$title_text='TrulyMadly';
	$ticker_text = "Message from TrulyMadly";

	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
	$pic_url = $imageurl.$user_data['thumbnail'];

	$successIds= "";
	$count=0;

	$sql= "select  distinct(u.user_id) as user_id  from user u join user_gcm_current ugc on ugc.user_id=u.user_id 
	 join user_data ud on ud.user_id = u.user_id     join geo_city gc on gc.city_id= ud.stay_city
	where  u.status in ('authentic', 'non-authentic') and 
    ugc.status in ('login','logout')  and ugc.source in ('iosapp','windows_app') AND gc.name in ('Mumbai','Pune') ";  
	$res = $conn_reporting->Execute($sql);
	
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{

			$user_id = $row['user_id'];
			$push_data = "It's never too late to speed date. Click to sign up!" ;
			$web_url = "http://bit.ly/PuneSpeedDate" ;
				
			$pushNotification->notify($user_id, array("content_text"=>$push_data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,
               	"event_status" => "speedDatePune", "title_text"=>$title_text,"push_type"=>"WEB", "web_url" =>$web_url  ),$admin_id);
			$successIds .= $user_id.",";
			$count++;
		}
	}

	echo "sent to ".$count;
	return array("count"=> $count, "ids"=>$successIds);

}
try
{
	if(php_sapi_name() == 'cli')
	{
	   CityWise ();	
	}
	
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?>