<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";

/* Author : Sumit
    Needs to be run everyday in the evening around 7
 * sends custom push notifications to different set of users based on certain conditions  */

class ImageNotification {
    private $conn_reporting;
    public $count_sent ;
    private $pushNotification;
    private $chunkSize = 2000;
    private $day ;


    function __construct($day)
    {
        global $conn_reporting;
        $this->day = $day ;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->pushNotification = new pushNotification();
    }


    public function sendImageNotifications($target = null)
    {
        global $cdnurl;
        $sql = "select u.user_id, u.fname, gc.display_name as city from user u
                join user_app_status uas on uas.user_id=u.user_id
                join user_lastlogin ull on u.user_id = ull.user_id
                join user_data ud on u.user_id = ud.user_id
                join geo_city gc on gc.city_id = ud.stay_city and gc.state_id = ud.stay_state
                where u.status ='authentic' and uas.android_app= 'install'" ;
        if($target != null)
            $sql .= " and u.user_id = $target";

        $chunkCounter = 0 ;
//        echo $sql ;

        $push_arr["push_type"] = "MATCHES_IMAGE_MULTI";
        $push_arr["title_text"] = "";

        if($this->day == 10)
        {
            $pic_url =  $cdnurl."/images/notifications/ProfileVideo1.jpg";
            $content_text = "Introducing Video Profiles on the app!";
        }
        else if ($this->day == 12 )
        {
            $pic_url =  $cdnurl."/images/notifications/ProfileVideo2.jpg";
            $content_text = "FOMO on the app?";
        }
        else if ($this->day == 13 )
        {
            $pic_url =  $cdnurl."/images/notifications/ProfileVideo3.jpg";
            $content_text = "Want to express yourself better?";
        }
        else
        {
            echo "invalid day"; die ;
        }
        //Generic title text for backward compatibility

        $push_arr[1] = array (

            "content_text"  => $content_text,
            "sub_text"      => "random Stuff",
            "pic_url"       => $pic_url,
            "ticker_text"   => "Message from TrulyMadly",
            "title_text"    => "TrulyMadly");

        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $user_id = $row['user_id'];


            $this->pushNotification->notify($user_id,$push_arr,'EVENT_PUSH',0,array(),array('androidApp'));
            $this->count_sent++;
            $chunkCounter++;
            if($chunkCounter >= $this->chunkSize)
            {
                echo "sleeping for 60 seconds before pushing more notifications".PHP_EOL;
                sleep(60);
                $chunkCounter= 0;

            }
            echo "sent to $user_id".PHP_EOL;
        }
        echo "total sent to $this->count_sent";
    }






}

try {

    if(php_sapi_name() === 'cli')
    {
        $target = $argv[1];
        $day = $argv[2] ;
        $matchNotification = new ImageNotification($day);
        if($target == 'all')
            $matchNotification->sendImageNotifications();
        else if (isset($target) && $target != null)
            $matchNotification->sendImageNotifications($target);
        else
            echo "No target";

    }




} catch (Exception $e) {
    echo $e->getMessage();
    trigger_error($e->getMessage(), E_USER_WARNING);
    Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "Custom Notification script failed".$baseurl, $e->getMessage());
}


?>