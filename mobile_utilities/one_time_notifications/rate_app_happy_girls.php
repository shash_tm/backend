<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../pushNotification.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";

/*To send one time push notifications to happy girls asking them to rate the app. */
function newManualNotification() {
	global $imageurl, $admin_id, $conn_reporting;
	$Trk = new EventTrackingClass();
	$countpas=0;
	
	$sql="select u.user_id, count(distinct (cmn.msg_id)) as count_messages_sent, GROUP_CONCAT(DISTINCT (ugc.registration_id)) AS regIds from user u JOIN current_messages_new cmn ON u.user_id = cmn.sender_id JOIN user_gcm_current ugc ON u.user_id = ugc.user_id where u.gender = 'f' AND u.status = 'authentic' and cmn.tStamp >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) and ugc.app_version_code >= 42 AND ugc.status IN ('login' , 'logout') group by u.user_id order by count_messages_sent desc limit 100";
	$res = $conn_reporting->Execute($sql);
	$resu= $res->GetRows();
	
	$data="Love us? Express it by reviewing us on Playstore";
	$title_text='Please rate TrulyMadly';
	$ticker_text="Message from TrulyMadly";
	$push_type="WEB";
	$uu = new UserUtils();
	$user_data = $uu->getNamenPic($admin_id);
	if(isset($user_data['thumbnail']))
		$pic_url = $imageurl.$user_data['thumbnail'];
	
	$web_url="market://details?id=com.trulymadly.android.app";
	
	/* for logging    */
	$event_status="rate_app_happy_girls"; 
	
	$sub_res=array_chunk($resu, 500);
	$scount=0;
	foreach ($sub_res as $row=>$col){
		$successIds="";
		foreach ($col as $r=>$c){
			$registration_ids = array() ;
			$registration_ids= explode("," , $c['regIds']);
			
			$silentnoti = new pushNotification();
			$result=$silentnoti->sendSilentNotification($registration_ids, array("content_text"=>$data,"ticker_text"=>$ticker_text,"pic_url"=>$pic_url,"event_status" => $event_status,
								"title_text"=>$title_text,"push_type"=>$push_type,"web_url" =>$web_url ),$admin_id);
			if ($result['success'] > 0) {
 				$successIds .= $c['user_id'].",";
	        	$scount++;
			}
      	}
		$successIds = rtrim($successIds, ","); 
		$data_logging [] = array("user_agent"=>"TrulyMadly/ (Android)", "activity" => "push" , "event_type" => "push_sent","event_status" => $event_status, "source" => "android_app", "event_info" => $successIds);
		$Trk->logActions($data_logging);
	} 
	echo $scount;
}

newManualNotification();
?>