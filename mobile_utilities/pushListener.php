<?php

require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/pushNotification.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";

ini_set('max_execution_time', 0);
/**
 *
 * this class continuously listens to redis server for push notification for 100s till the last notification was sent
 * @author sumit
 *
 */
class PushListener{

	private $file;
	private $fptr;
	private $redis;
	private $waitForBlockingReadInRedis =0;
	private $pushNotificationObject;
	private $baseurl; 
	private $lockStatus ;
	private $no_of_scripts ;
	private $count_allowed_pending_msg ;

	function __construct(){
		global $push_notification_temp_file, $redis, $baseurl;

//		$this->file = $push_notification_temp_file;
//		if(!file_exists($push_notification_temp_file))
		 $this->file = "/tmp/push_notification";
		$this->fptr =  null;
		$this->redis = $redis;
		$this->redis->setOption(Redis::OPT_READ_TIMEOUT, -1);
		$this->pushNotificationObject = new pushNotification();
		$this->baseurl = $baseurl; 
		$this->lockStatus = false ;
		$this->no_of_scripts = 10 ;
		$this->count_allowed_pending_msg = 1000 ;
		register_shutdown_function(array($this, 'shutdownReport'));
	}  

	/**
	 * acquire lock on temp file and if it doesnt exist then open one in read mode
	 */
	public function setLockOnTempFile( $file_name )
	{
        global $techTeamIds;
       
        $this->fptr = fopen($file_name, "c");
	  	
		if (flock($this->fptr, LOCK_EX|LOCK_NB, $wouldblock)) 
//		  if (flock($fp, LOCK_EX | LOCK_NB))  
		 {		
			$isLocked = 0 ;
			// lock obtained
			if(flock($this->fptr,LOCK_EX) == false){
				throw new Exception("unable to acquire lock");
			}else{
				echo "lock acquired"; 
				$this->lockStatus = true ;
//				$gmtTime = $this->getTime();
//				Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "No previous lock found ".$this->baseurl , " Lock acquired ". $gmtTime  );
			} 
		}
		else  
		{
		   if ($wouldblock) {
				echo "already locked";
				$isLocked = 1 ;
				$this->lockStatus = false ;
				// something already has a lock
			}
			else 
			{ 
				echo "could not lock";
				// couldn't lock for some other reason
			}
	 
		}
 
		return $isLocked;
	}
	
	public function checkLockOnAllFiles () 
	{
		global $techTeamIds;
		for ($i = 1 ; $i <= $this->no_of_scripts ; $i++) 
		{
			$file_name = $this->file . $i . ".txt" ;
			//echo $file ;
			//$fptr = $this->openFile( $file ) ;
			$lock_status = $this->setLockOnTempFile($file_name );
//            echo "Lock Status " . $lock_status ;	
//            echo PHP_EOL ;		
			if ($lock_status == 0 )  
			{
				$gmtTime = $this->getTime();
				$subject = "No previous lock found on file ". $i . " "  .$this->baseurl ;
//				echo $subject ;
//				echo PHP_EOL . $file_name ; 
				Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", $subject , " Lock acquired ". $gmtTime  );

				$this->startNotifications();
			}
			
		}
		
		if($i >= $this->no_of_scripts )
		{
			$count = $this->checkNotificationCount() ;
			if ($count >= $this->count_allowed_pending_msg )
			{
				$subject = "Warning :: no. of  pending messages in the queue " . $count  . " on  " . $this->baseurl;
				$body = "All " . $this->no_of_scripts . " scripts already running " . $this->getTime() ;
				Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", $subject , $body  );
//				echo $subject .PHP_EOL ;  
//				echo $body ;
			}
		}
	}
	
	private function checkNotificationCount () 
	{
	   $count = $this->redis->LLEN (Utils::$redis_keys['push_notification'])	;
	   return $count ;
	}
	
	private function startNotifications ()
	{
//		echo "sending notifications" ;
	   while (true)
	   {	
		$this->sendNextPushNotificationData() ;
		}
		
	}

	private function openFile ($file) 
	{
	  	$fptr = fopen($file, 'c');
	  	return $fptr ; 
	}
	
	public function shutdownReport () 
	{
		
		if ( $this->lockStatus == true )
		{
		$gmtTime = $this->getTime();
		$last_error_arr = error_get_last();
		$last_error = json_encode($last_error_arr);
		Utils::sendEmail("shashwat@trulymadly.com, sumit@trulymadly.com", "sumit@trulymadly.com", "Shutdown Registered ".$this->baseurl, $gmtTime . $last_error );	
		//echo "callback registered"; 
		}
		 
	} 
	
	public function getTime ()
	{
		$gmtTime = $isttime= date ( 'Y-m-d H:i:s') ;
		return $gmtTime;
	}
	
	public function releaseLock(){
		if(flock($this->fptr,LOCK_UN) == false){
			throw new Exception("unable to release lock");
		}
	}
	/**
	 * push the data array by calling the notification function in the main notification file (object in constructor)
	 * @param unknown_type $data
	 */
	private function push($data){
		$push_array = json_decode($data,true);
		if(isset($push_array['user_id']) && $push_array['user_id'] != null)
		{
			$result=$this->pushNotificationObject->notifySubscribe($push_array['user_id'], $push_array['push_data'],$push_array['sender_id'], $push_array['multipleNotificationsForSameUser'],$push_array['supportedAppVersion'], $push_array['platform']);
		} 

	//	echo $push_array ['user_id'] . PHP_EOL ;
	}

	/**
	 * read the next push array in redis queue and send notification and wait for `x` seconds till the time there is no more notifications
	 */
	public function sendNextPushNotificationData(){
		$data = $this->redis->BRPOP(Utils::$redis_keys['push_notification'], $this->waitForBlockingReadInRedis); //brpop('push_notification', 100);
		if($data[0] == Utils::$redis_keys['push_notification']){
			$this->push($data[1]);
		}
	}
}


try{
	if(php_sapi_name() === 'cli')
	{
		$listener = new PushListener();
		$checkLock = $listener->checkLockOnAllFiles() ;
/*		$checkLock = $listener->setLockOnTempFile();
		if($checkLock == false){
			$i =0;
			//$t1 = time();

				
		} */
	}
}
catch (Exception $e){
	$listener->releaseLock(); 
	echo $e->getTraceAsString();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	trigger_error($e->getMessage(), E_USER_WARNING); 
	
 
	Utils::sendEmail("shashwat@trulymadly.com, sumit@trulymadly.com", "sumit@trulymadly.com", "Listener script failed".$baseurl, $e->getMessage() . $e->getTraceAsString() );

}
?>