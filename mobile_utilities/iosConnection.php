<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
/* @Sumit ::
 * for maintaing the connection to APNS server for sending apple push Notifications  */
class iosConnection {

	private $ios_notification_passphrase;
	private $ios_notification_filepath;
	private $ios_notification_conn_url;

	public static $fp;
	function __construct (){
		global $ios_notification_passphrase, $ios_notification_filepath, $ios_notification_conn_url;
		$this->ios_notification_passphrase = $ios_notification_passphrase;
		$this->ios_notification_filepath = $ios_notification_filepath;
		$this->ios_notification_conn_url  =  $ios_notification_conn_url;
	}
	// to return the connection resource (if not present then create one )
	public function createConnection (){
		if(!iosConnection::$fp){
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $this->ios_notification_filepath);
			stream_context_set_option($ctx, 'ssl', 'passphrase', $this->ios_notification_passphrase);

			// Open a connection to the APNS server
			iosConnection::$fp = stream_socket_client(
			$this->ios_notification_conn_url , $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			stream_set_blocking (iosConnection::$fp, 0);

			if($err){
				echo "Failed to connect: $err $errstr";
				trigger_error($err, E_USER_WARNING);
				trigger_error($errstr, E_USER_WARNING);
			} /*else {
			echo "Connection created again".PHP_EOL;
			}*/
		}
		//if there is an error in creating connetion

		return iosConnection::$fp;

	}
	public function removeConnection (){
		iosConnection::$fp = null;
		//echo "Connection Closed".PHP_EOL;
	}


}

?>