<?php

require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/WPNTypesEnum.php";
require_once dirname ( __FILE__ ) . "/createConnection.php";

/**
 *
 * all the functions required for windows push notification
 * this layer is the view to programmer - any new functionality or alteration needs to be here only
 * @Himanshu
 */


/*
 class WPNResponse
 {
 public $message = '';
 public $error = false;
 public $httpCode = '';

 function __construct($message, $httpCode, $error = false)
 {
 $this->message  = $message;
 $this->httpCode = $httpCode;
 $this->error    = $error;

 echo $this->message . '<br/>';
 }
 }*/

class WPN
{
	public $access_token = '';
	private $client_id = '';
	private $client_secret = '';
	private $windows_pushnotification_baseurl;
	private $windows_connection;
	private $activity_launch_url;

	function __construct($client_id, $client_secret)
	{
		global $windows_pushnotification_baseurl;
		$this->client_id     = $client_id;
		$this->client_secret = $client_secret;
		$this->windows_pushnotification_baseurl = $windows_pushnotification_baseurl;
		$this->windows_connection = new windowsConnection($this->client_id, $this->client_secret);
		$this->get_access_token();
	}

	/**
	 * reads access token from singleton connection in createConnection.php
	 */
	public function get_access_token()
	{
		$this->access_token = $this->windows_connection->getAccessToken();	
	}

	/**
	 * reset the connection to null
	 * and make new call to fetch latest access token
	 */
	public function reset_access_token()
	{
		$this->windows_connection->removeConnection();
		$this->get_access_token();
	}

	/**
	 * to set up the headers of each curl call to prepare it for multi curl
	 * will return the options corresponding to each curl in an array
	 * @param $uri
	 * @param $xml_data_array
	 * @param $type_array
	 * @param $urls
	 * @param $tileTag
	 */
	public function setOptionsForCurl($uri, $xml_data_array, $type_array , &$urls, $tileTag = ''){
		
		$options = array();
	

		//get access token first to set it as a parameter in curl
		if ($this->access_token == null)
		{
			$this->get_access_token();
		}

		//set options
		if(isset($this->access_token)){
			foreach ($xml_data_array as $key => $val){
				
				
				if(strpos($uri,'https://')!==false)
				{
					//This is the type of url 
					$urls[$key]=$uri;
				}
				else 
				{
				    $urls[$key] = $this->windows_pushnotification_baseurl . $uri;
				}
				
				$headers = array();
				$headers = array(
            			   "Content-Type: text/xml",
            			   "Content-Length:" . strlen($val),
            			   "X-WNS-Type: $type_array[$key]",
               			   "X-WNS-RequestForStatus: true",
          				   "Authorization: Bearer $this->access_token"
							);

				if ($tileTag != '')
				{
					array_push($headers, "X-WNS-Tag: $tileTag");
				}

				$options[$key] = array(
							   	 CURLOPT_URL => $urls[$key],
								 CURLOPT_SSL_VERIFYHOST => 0,
				  				 CURLOPT_SSL_VERIFYPEER => 0,
								 CURLOPT_POSTFIELDS =>$val,
								 //CURLOPT_VERBOSE => 1,
								 CURLOPT_POST => 1,
								 CURLOPT_HTTPHEADER => $headers,
								 CURLOPT_RETURNTRANSFER =>1,
				);
			}
		}

		return $options;
	}

	
	/**
	 * which activity to launch on the tap of push notification
	 * send the url along with an extra parameter "launch_url" so that no parsing is needed at windows end
	 * @param unknown_type $push_data
	 */
	public function set_activity_launch_url(&$push_data)
	{
		$url = null;
		$push_type = $push_data['push_type'];
		if ($push_type == 'MESSAGE')
		{
			$url = $push_data['message_url'];
			$push_data['message_url'] = urlencode($url);
		}
		else if ($push_type == 'MATCH')
		{
			$url = $push_data['match_profile_url'];
			$push_data['match_profile_url'] = urlencode($url);
		}
		else if ($push_type == 'WEB')
		{
			$url = $push_data['web_url'];
			$push_data['web_url'] =urlencode($url);
		}
		
		//encoded the url since it's not readable in xml otherwise
		$push_data['launch_url'] = urlencode($url);
		//$this->activity_launch_url = $url;
	}


	/**
	 * build tile xml
	 * change template or parameters as and when required 
	 * @param unknown_type $push_data
	 */
	public function build_tile_xml($push_data)
	{
		return '<?xml version="1.0" ?>
				<tile launch='."'" .json_encode($push_data). "'>".'
					<visual>
						 <binding template="TileSquarePeekImageAndText02">
		 					<image id="1" src="'.$push_data['pic_url'].'"/>
							<text id="1">' . $push_data['title_text'] . '</text>
                     	    <text id="2">' . $push_data['content_text'] . '</text>
		   				 </binding>
		   			 </visual> 
		   	 	</tile>';
	}

	
	/**
	 * build toast xml
	 * @param unknown_type $push_data
	 */
	
	public function build_toast_xml($push_data)
	{
		
		return '<?xml version="1.0" encoding="UTF-8"?>
				<toast launch ='."'" .json_encode($push_data). "'>".'
					<visual lang="en-US">
						<binding template="ToastImageAndText01"><image id="1" src="'.$push_data['pic_url'].'"/>
							<text id="1">' . $push_data['title_text'] . '</text>
          	                <text id="2">' . $push_data['content_text'] . '</text>
						</binding>
					</visual>
				</toast>';

	}

	
}


?>