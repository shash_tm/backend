<?php

/**
 * constants for responses we receive curl calls for windows push notification
 * @author himanshu
 * NOTE: DO NOT EDIT UNLESS YOU'RE SURE ENOUGH
 *
 */

class OK
{
	const code = 200;
	const description ="The notification was accepted by WNS.";
	const action = "NONE";
}
class BADREQUEST
{
	const code = 400;
	const description = "One or more headers were specified incorrectly or conflict with another header.";
	const action = 	"Log the details of your request. Inspect your request and compare against this documentation.";
}
class UNAUTHORIZED {
	const code = 401;
	const description="The cloud service did not present a valid authentication ticket. The OAuth ticket may be invalid.";
	const action="Request a valid access token by authenticating your cloud service using the access token request.";
}
class FORBIDDEN {
	const code = 403;
	const description="The cloud service is not authorized to send a notification to this URI even though they are authenticated.";
	const action="The access token provided in the request does not match the credentials of the app that requested the channel URI. Ensure that your package name in your app's manifest matches the cloud service credentials given to your app in the Dashboard.";
}
class NOTFOUND {
	const code = 404;
	const description=	"The channel URI is not valid or is not recognized by WNS.";
	const action ="Log the details of your request. Do not send further notifications to this channel; notifications to this address will fail.";
}
class METHODNOTALLOWED {
	const code = 405;
	const description=	"Invalid method (GET; CREATE} only POST (Windows or Windows Phone) or DELETE (Windows Phone only) is allowed.";
	const action=	"Log the details of your request. Switch to using HTTP POST.";
}
class NOTACCEPTABLE {
	const code = 406;
	const description=	"The cloud service exceeded its throttle limit.";
	const action="Log the details of your request. Reduce the rate at which you are sending notifications.";
}
class GONE {
	const code = 410;
	const description="The channel expired.";
	const action="Log the details of your request. Do not send further notifications to this channel. Have your app request a new channel URI.";
}
class TOOLARGEREQUEST {
	const code = 413;
	const description=	"The notification payload exceeds the 5000 byte size limit.";
	const action="Log the details of your request. Inspect the payload to ensure it is within the size limitations.";
}
class INTERNALSERVERERROR {
	const code = 500;
	const description=	"An internal failure caused notification delivery to fail.";
	const action="Log the details of your request. Report this issue through the developer forums.";
}
class SERVICEUNAVAILABLE {
	const code = 503;
	const description="The server is currently unavailable.";
	const action="Log the details of your request. Report this issue through the developer forums.";
}


?>