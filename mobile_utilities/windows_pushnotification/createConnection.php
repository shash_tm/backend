<?php
require_once dirname ( __FILE__ ) . "/../../include/config.php";


/**
 *  @Himanshu
 *  kind of singleton implementation of windows connection class
 * 	using persistent connection by returning the same access token since it's independent of user properties
 *  NOTE: DO NOT EDIT UNLESS YOU'RE SURE ENOUGH 
 **/


class windowsConnection
{

	private $grant;
	private $scope;
	private $url;
	private $client_id = '';
	private $client_secret = '';
	private $redis;

	//static variable for singletone
	public static $access_token;


	function __construct ($client_id, $client_secret)
	{
		global $windows_pushnotification_url, $windows_pushnotification_scope,$windows_pushnotification_grant,$redis;
		$this->grant = $windows_pushnotification_grant;
		$this->scope = $windows_pushnotification_scope;
		$this->url   = $windows_pushnotification_url;
		$this->redis = $redis;
		$this->client_id     = $client_id;
		$this->client_secret = $client_secret;

	}

	//function to be called
	public function getAccessToken()
	{

		if ($this->redis->exists('windows_auth_url')==false)
		{
			$fields = array(
            		'grant_type' => $this->grant,
            		'client_id' => $this->client_id,
          		    'client_secret' => $this->client_secret,
          			'scope' => $this->scope
			);

			$postvars = '';
			$sep      = '';
			foreach ($fields as $key => $value)
			{
				$postvars .= $sep . urlencode($key) . '=' . urlencode($value);
				$sep = '&';
			}

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $output = json_decode($result);

            if (isset($output->error))
            {
            	throw new Exception($output->error_description);
            }
            else{
            	
            	    windowsConnection::$access_token = $output->access_token;               
            		if($this->redis!=null)
            		{
            			$this->redis->set('windows_auth_url',windowsConnection::$access_token);
            			$this->redis->setTimeout('windows_auth_url',86440);
            			
            		}
            	
            	
            }
		}
		else
		{
			if($this->redis!=null)
			{
				windowsConnection::$access_token=$this->redis->get('windows_auth_url');
			}
		}
		
		
		return windowsConnection::$access_token;

	}


	/**
	 * sets token to null
	 */
	public function removeConnection (){
		
		windowsConnection::$access_token = null;
	    $this->redis->delete('windows_auth_url');
		
	}


}

/*function IsNullOrEmptyString($question){
	echo($question);
	return (!isset($question) || trim($question)==='');
}
//trigger_error('making connection <br/>',E_USER_WARNING);

if(IsNullOrEmptyString(windowsConnection::$access_token))
{
	$wn=new windowsConnection('ms-app://s-1-15-2-1454113247-1467752240-400082692-2361208256-1175470422-3655568352-3760466184','1Ro2dUqf5uVEvVvDXoIeY4g+YRyCFmGm');
	
	$wn->getAccessToken();
	
	
	//var_dump($redis);
	//trigger_error('The token is <br/>'.windowsConnection::$access_token,E_USER_WARNING);
	
	//echo('The token is <br/>'.windowsConnection::$access_token);
}
else
{
	echo('<br/>The token is not null and is  <br/>'.windowsConnection::$access_token);
}
*/





?>