<?php
require_once dirname ( __FILE__ ) . "/TMObject.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname(__FILE__)."/LikeMeClass.php";
include_once dirname ( __FILE__ ) . "/include/communicationMessages.php";
require_once dirname(__FILE__)."/EventTracking.php";



try{
	$device_type = ($_REQUEST['login_mobile'] == true)? 'mobile' :'html';
	
	$user = functionClass::getUserDetailsFromSession();
	$user_id = $user['user_id'];
	
if ($device_type == 'mobile') {
	$authentication = functionClass::redirect ( 'likes', true );
	if ($authentication ['error'] != null)
	echo json_encode ( $authentication );
} else {
	functionClass::redirect ( 'likes' );
}

if(isset($_REQUEST['uid'])){
	$eventTrack = new EventTracking($_REQUEST['uid']);
	$eventTrack->logAction("likes");
	}
	else if(isset($user_id)){
		$eventTrack = new EventTracking($user_id);
		$eventTrack->logAction("likes");
	}

	$status = $user['status'];
	$LikeMe = new LikeMeClass($user_id); 
	$page_id=$_REQUEST['page_id'];
	/**
	 ids will contain values on liked and likedme both
	 */

	
	$output=$LikeMe->showLikes($page_id, $status);
// 	echo '<pre>';
// 	var_dump($output); die;
	if ($device_type == "html") {
		//$smarty->display ("../templates/dashboard/messagesexpanded.tpl" );
		//	$smarty->display ("../templates/dashboard/messagesexpanded.tpl" );

		$header=$LikeMe->getHeaderValues($user_id);
		$smarty->assign('header',$header);
		$smarty->assign('data',$output);
		$smarty->assign('myStatus', $status);
	
		if($status == "suspended"){
			$output['info_msg'] = $infoMessage['likes']['userSuspended'];
			$smarty->assign('info_msg', $output['info_msg']);
		}
		if(empty($output))
		$smarty->assign("no_likes", $infoMessage['likes']['no_likes']);

		$smarty->assign('isAuthentic', $status);
		$smarty->display ( "templates/dashboard/likeme.tpl" );
	}

	else{
		$mobile_arr['is_authentic'] = $status;
	
		if($status == "suspended"){
			$mobile_arr['info_msg'] = $infoMessage['likes']['userSuspended'];
		}
		if(empty($output)){
			$mobile_arr['no_likes'] = $infoMessage['likes']['no_likes'];
		}else
			$mobile_arr['like_data'] = $output;
		
		echo json_encode($mobile_arr);
		
	}
}catch(Exception $e){
	trigger_error ( $e->getMessage (), E_USER_WARNING );
	}

?>








