<?php

require_once dirname(__FILE__) . "/../Utils/ArrayUtils.php";
require_once dirname(__FILE__) . "/../include/config_admin.php";
require_once dirname(__FILE__) . "/../include/Utils.php";

define('FEMALE_TO_MALE_SELECT_PERCENT_MULTIPLIER',3);
define('MAX_FREE_FEMALES_ALLOWED_PER_STATE_PERCENTAGE',15);

/*
 * The class shall define all the dynamic pricing strategies for various features
 */
class DynamicPricing {
    
    /*
     * TM_Select has paid packages for males for subscription. Since it's a bi-partitite graphical 
     * situation, we need enough females in each segment (or rather for each male at a more granular level)
     * to drive it ahead.
     * The segment can be defined by the user in the filter string that is expected as a parameter.
     * The conditions for a free package are :
     * 1. The female should not have seen a paid pack price point before.
     * 2. Once we have enough mass (defined by maximum cap or a male:female select ratio heuristics) we can switch off the free feature
     * 
     * @params :    $filter                                         : Eg. install_status = 'install' and ((gender = 'F' and age >= 24) or (gender = 'M' and age >= 26)) and state = 2185
     *              $maxFreeFemalesAllowedPerStatePercentage        : Upper cap on max. female percentage allowed as Select for free
     *              $femaleToMaleSelectPercentMultiplier            : female to male ratio
     * 
     * @return :    Boolean of whether we should show the free pack to the female or not
     */
    public static function isSelectFemaleSubscribersSufficientFilterBased($filter,$maxFreeFemalesAllowedPerStatePercentage = -1,$femaleToMaleSelectPercentMultiplier = -1) {
        if($maxFreeFemalesAllowedPerStatePercentage < 0) {
            $maxFreeFemalesAllowedPerStatePercentage = MAX_FREE_FEMALES_ALLOWED_PER_STATE_PERCENTAGE;
        }
        if($femaleToMaleSelectPercentMultiplier < 0) {
            $femaleToMaleSelectPercentMultiplier = FEMALE_TO_MALE_SELECT_PERCENT_MULTIPLIER;
        }
        $sql = "select gender,round(sum(isSelect)*100/count(*),1) as selectPercentage from user_search where $filter group by gender";
        $selectPercentages = Query::SELECT($sql);
        $maleSelectCoverage     = 0;
        $femaleSelectCoverage   = 0;
        foreach ($selectPercentages as $select) {
            if ($select['gender'] == 'M') {
                $maleSelectCoverage = floatval($select['selectPercentage']);
            }
            else if ($select['gender'] == 'F') {
                $femaleSelectCoverage = floatval($select['selectPercentage']);
            }
        }
        
        $shouldSelectBeFreeForFemale = false;
        #Check here if the female has already seen the paid Select pack. If yes we shouldn't be showing her a free pack.
        #\\TODO - @Tarun. Please finalize on a way to keep a flag for each user.
        if(true) {
            $shouldSelectBeFreeForFemale = false;
        }
        else if ($femaleSelectCoverage <= min($maxFreeFemalesAllowedPerStatePercentage, $femaleToMaleSelectPercentMultiplier * $maleSelectCoverage) ) {
            $shouldSelectBeFreeForFemale = true;
        }
        
        if($shouldSelectBeFreeForFemale == false) {
            #The system is exposing the paid pack to the female. We need to remember this decision so as to not show her free pack from now on.
        }
        
        return($shouldSelectBeFreeForFemale); 
    }

    /*
     * Returns the states, in which number of female select users have crossed a particular thresh hold,
     * will turn off free trial in such states. Consumed by a cron.
     * Also, Fonzie is the best !
     */
    public static function getStateWithSelectFemaleThreshold($maxFreeFemalesAllowedPerStatePercentage = -1,$femaleToMaleSelectPercentMultiplier = -1){
        if($maxFreeFemalesAllowedPerStatePercentage < 0) {
            $maxFreeFemalesAllowedPerStatePercentage = MAX_FREE_FEMALES_ALLOWED_PER_STATE_PERCENTAGE;
        }
        if($femaleToMaleSelectPercentMultiplier < 0) {
            $femaleToMaleSelectPercentMultiplier = FEMALE_TO_MALE_SELECT_PERCENT_MULTIPLIER;
        }
        $sql = "select state,gender,round(sum(isSelect)*100/count(*),1) as selectPercentage from user_search where install_status = 'install' group by gender,state";
        $result =  Query_Wrapper::SELECT($sql, array(), Query::$__slave);
        $stateList = array();
        foreach($result as $row){
            if(isset($stateList[$row['state']])){
                $stateList[$row['state']][$row['gender']] = $row['selectPercentage'] == 0.0 ? 0.1 : floatval($row['selectPercentage']);
            }else{
                $stateList[$row['state']] = array();
                $stateList[$row['state']][$row['gender']] = $row['selectPercentage'] == 0.0 ? 0.1 : floatval($row['selectPercentage']);
            }
        }
        $nonFreeStateList = array();
        foreach($stateList as $key => $value){
            $maleSelectCoverage = isset($value['M']) ? $value['M'] : .1;
            $femaleSelectCoverage = isset($value['F']) ? $value['F'] : .1;
            if (!($femaleSelectCoverage <= min($maxFreeFemalesAllowedPerStatePercentage, $femaleToMaleSelectPercentMultiplier * $maleSelectCoverage))) {
                $nonFreeStateList[] = $key;
            }
        }
        return $nonFreeStateList;
    }
}
