<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

global $conn;

try{
	if(PHP_SAPI !== 'cli') {
		die();
	}
	
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql = $conn->Execute("select * from user_trust_score");
	$data = $sql->GetRows();
	
	foreach ($data as $key=>$value){
		$trust_score = 0;
		if(isset($data[$key]['fb_connections'])){
			$trust_score += 25;
		}
		if(isset($data[$key]['linkedin_connections'])){
			$trust_score += 20;
		}
		if(isset($data[$key]['mobile_number'])){
			$trust_score += 10;
		}
		if(isset($data[$key]['id_credits'])){
			$trust_score += 25;
		}
		if(isset($data[$key]['employment_credits'])){
			$trust_score += 20;
		}
		
		$conn->Execute($conn->prepare("update user_trust_score SET trust_score=? where user_id=? "),
				array($trust_score,$data[$key]['user_id']));
			
		
	}
}catch(Exception $e){
	echo $e->getMessage();
	trigger_error($e->getMessage(),E_USER_WARNING);
}


?>