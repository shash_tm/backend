<?php
require_once dirname ( __FILE__ ) . "/include/Utils.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/DBO/userUtilsDBO.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/include/header.php";



class MayBe {

	private $user_id;
	private $conn;
	private $user_utils_dbo;
	private $user;
	private $user_utils;

	function __construct($user) {
		$this->user=$user;
		$this->user_id = $user->getUserID();
		$this->user_utils_dbo = new userUtilsDBO();
		$this->user_utils=new UserUtils();
	}

	public function getData(){
		global $baseurl;

		$data = null;
		$mayBeData = $this->user_utils_dbo->getMayBeQueue($this->user_id);
		$mayBeIds = array(); 
		$daysLeft = null;

		$x_days = Utils::$mayBeExpiryDays;
		foreach ($mayBeData as $val){
			$mayBeIds[] = $val['user2'];
			$str =  strtotime($val['timestamp']) - strtotime('-'.$x_days.' days', strtotime(date('Y-m-d'))) ;
			$mayBeData[$val['user2']]['days_left'] = floor($str/3600/24);//date_offset_get($val['timestamp'], time());
			$mayBeData[$val['user2']]['timestamp'] = $val['timestamp'];
		}
		//var_dump($mayBeData);die;
		if($mayBeIds != null)
		$tileData = $this->user_utils_dbo->getTileDate($mayBeIds);
		else $tileData = null;

		$my_gender=$this->user->fetchGender();
		$other_gender=($my_gender=="F")?"M":"F";
		$result=array();

		$newTileData = null;
		foreach ($tileData as $v){
			$newTileData[$v['user_id']]= $v;
		}
		
		$mayBeIds = array_unique($mayBeIds);
		
		foreach($mayBeIds as $value){

			$val = $newTileData[$value];  
			if($val['status'] != "authentic") continue;
				
			$data=array();
			$user_id=$val['user_id'];
			$data['user_id']=$user_id;
			$data['profile_data']['days_left']=$mayBeData[$user_id]['days_left'];
			$data['profile_data']['fname']=str_repeat("x",strlen($val['fname']));
			$data['profile_data']['age']=$val['age'];
			$data['profile_data']['city']=($val['stay_city_display']==null)?$val['state']:$val['stay_city_display'];
			$data['profile_data']['profile_pic'] = $this->user_utils->getProfilePic($val['thumbnail'], $other_gender);
			$data['profile_data']['profile_id']=$user_id;
			$data['profile_data']['profile_link']=$this->user_utils->generateProfileLink($this->user_id,$user_id) . '&from_maybe=true';;
			$data['profile_data']['like_link'] = $this->user_utils->generateLikeLink($this->user_id, $user_id,0,$my_gender) . "&src=maybe";
			$data['profile_data']['hide_link'] = $this->user_utils->generateHideLink($this->user_id, $user_id,0,$my_gender) . "&src=maybe";
				
				
			$result[]=$data;
		}
		//var_dump($result);
		return $result;
	}

}


try {
	//get user details from session should return if user is valid or not

	$device_type = Utils::getDeviceType();
	if($device_type == "mobile") $flag = true; else $flag=false;
	functionClass::redirect("maybe", $flag);
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$user=new UserData($user_id);

	$maybeObj = new MayBe($user);
	$data = $maybeObj->getData();

	//	var_dump($data); die;
	/*	$device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
	if ($device_type == 'mobile') {
	$response=array();
	$response['responseCode']=200;
	$response['data']=$data;

	echo json_encode($response);
	exit;
	}
	*/
	$header = new header($user_id);
	$header_values = $header->getHeaderValues();
	$smarty->assign("header", $header_values);
	$smarty->assign("data",$data);
	$smarty->assign("my_id", $user_id);

	$smarty->display("templates/dashboard/maybe.tpl");

	//$status = $user['status'];


}
catch(Exception $e){
	trigger_error("PHP WEB:".$e->getTraceAsString(), E_USER_WARNING);
}

?>








