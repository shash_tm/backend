function initMap() {

    
	$('#contact-map').gmap3({
 map: {
    options: {
      	maxZoom:15,
	   //	mapTypeId: 'grey_scale',
  		mapTypeControl: false
    } 
 },
 styledmaptype: {
            id: "grey_scale",
            options: {
                name: "grey_scale"
            },
            styles: [
                {
                    featureType: "all",
                    stylers: [
                    {saturation: -100 },
                    { gamma: 0.20 }
                  ]
                }
              ]
        },
 marker:{
   values: [

                { latLng: [28.525244500000000000,77.195962199999940000], data: "<p> Your Company Name</p>" },

            ],
    options: {
     icon: new google.maps.MarkerImage(
       "http://thinkdesign.in/TD/tm//images/location-icon.png",
       new google.maps.Size(62, 46, "px", "px")
     )
    }
 }
},
"autofit" );
}
/***********************************************************Google Map End*********************************/


$(function () {
    initMap();
});
