function verifyId(status,userId){
	if(status == 'fail'){
		var a = confirm("Are you sure you want to reject?");
		if(a == false){
			return;
		}
	}
	$("#buttonId_"+userId).hide();
	$.post("adminpanel.php",{
		action:'verify_id',
		status:status,
		userId:userId
	},function(data){
		if(data.error){
			alert(data.error);
		}if(data.status=='Success'){
			$("#buttonId_"+userId).html("Id Status:"+data.action); 
		}
	},'json');
}

function Validate_Phone_Number(phno)
{
	var rxPattern = /^\d{10}$/;  
    return rxPattern.test(phoneNumber); // is format OK? 
}

function isDate(txtDate)
{
    var currVal = txtDate;
    console.log(currVal);
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    if (dtArray == null) 
        return false;
    //Checks for yyyy/mm/dd format.
    dtMonth = dtArray[3];
    dtDay= dtArray[5];
    dtYear = dtArray[1];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    else 
    {
    	var todayDate = new Date();
    	var todayYear = todayDate.getFullYear();
    	var todayMonth = todayDate.getMonth() +1;
    	var todayDay = todayDate.getDate();
    	var flag=0;
    	if(todayYear-dtYear<18)
    	{ flag=1;
    	}
    	else if(todayYear-dtYear==18)
         {
    		if(todayMonth-dtMonth<0)
    		{	flag=1;}
    		else if(todayMonth-dtMonth==0)
    			{
    			if(todayDate-dtDay<0)
        		 {flag=1;}
        		 }
    			
         }
    	if(flag==1)
    		{
    		alert('Age less than 18 years');
    		return false;
    		}
    }
    return true;
}


function verifyAddress(status,userId){
	if(status == 'fail'){
		var a = confirm("Are you sure you want to reject?");
		if(a == false){
			return;
		}
	}
	$.post("adminpanel.php",{
		action:'verify_address',
		status:status,
		userId:userId
	},function(data){
		if(data.error){
			alert(data.error);
		}if(data.status=='Success'){
			$("#button_"+userId).html("Address Status:"+data.action);
		}
	},'json');
}

function verifyEmployment(status,userId){
	if(status == 'fail'){
		var a = confirm("Are you sure you want to reject?");
		if(a == false){
			return;
		}
	}
	$("#button_"+userId).hide();
	$.post("adminpanel.php",{
		action:'verify_employment',
		status:status,
		userId:userId
	},function(data){
		if(data.error){
			alert(data.error);
		}if(data.status=='Success'){
			$("#button_"+userId).html("Employment Status:"+data.action);
		}
	},'json');
}

function reject_visible(photo_id){
	document.getElementById("outer_drop_down_reject_"+photo_id).style.display = "inline";
}

function getRemainingModerationData(){
	$.post("adminpanel.php",{
		action:'getRemainingModerationData' 
	},function(data){
		if(data.error){
			alert(data.error);
		}if(data.status=='Success'){
			document.getElementById("remaining_button").style.display="none";
			var elem=document.getElementById("remaining_data");
			elem.children[0].innerHTML="Photos Male : "+data.rem_photos.M;
			elem.children[1].innerHTML="Photos Female : "+data.rem_photos.F;
			elem.children[2].innerHTML="Photos Ids : "+data.rem_ids.count;
			elem.style.display="inline";
			}
	},'json');
}

function verifyPhoto(status, photo_id, user_id, is_profile, mapped_to) {
	if (status == 'fail') {
		var a = confirm("Are you sure you want to reject?");
		if (a == false) {
			return;
		}
	}
	
	
	/*if (status == 'active') {
			alert (""+mapped_to);
	}*/
	
	
	if (status == 'rejected') {
		var e = document.getElementById("drop_down_reject_"+photo_id);
		if(e==null)
		{ 
			var a =confirm("Are you sure you want to reject ?");
			if (a == false) {
				return;
			 }	
		}
		else
		{	
		var reason = e.options[e.selectedIndex].value;
		/*var a = confirm("Are you sure you want to reject with this reason : "+reason+" ?");
		
		if (a == false) {
			document.getElementById("outer_drop_down_reject").style.display = "none";
			return;
		 }	*/
		}
	}
	
	//alert (mapped_to);
	$.post("adminpanel.php", {
		action : 'verify_photo',
		status : status,
		photo_id : photo_id,
		user_id : user_id,
		is_profile : is_profile,
		mapped_to : mapped_to,
		reason : reason
	}, function(data) {

		if (data.status == 'Success') {
			var x = document.getElementById("imagediv_" + photo_id);
			if (status == 'active' || status == 'active_no_profile') {
				var y = document.getElementById("approved_" + user_id);
				var rej = document.getElementById("no_approved_" + user_id);
			} else if (status == 'rejected') {
				var y = document.getElementById("rejected_" + user_id);
				var rej = document.getElementById("no_rejected_" + user_id);
			}
			// move the photo to the new approved or rejected section
			$(y).append(x);
			$(rej).hide();
		    // $("#button_"+photo_id).hide(); 
			 $("#button_"+photo_id).html("Photo Status:"+status);
		}
	}, 'json');
}

function sendForVerify(id, type){
	var ref = $("#auth_"+id).val();
	$.post("adminpanel.php",{
		action:'authbridge_verification',
		user_id:id,
		type: type,
		ref: ref
	},function(data){
		//alert(userId);
		if(data.status=='Success'){
			$("#authbridge_"+id).html("Sent for verification");
		}
	},'json');
}

function isPaid(id,type){
	$.post("adminpanel.php",{
		action:'paid',
		user_id:id,
		type:type
	},function(data){
		if(data.status=='Success'){
			$("#paid_"+id).html("paid : yes");
			$("#paid123").hide();
		}
	},'json');
}

function rotatePhoto(id){
	angle += 90;
	if(angle>=360){
		angle = angle-360;
	}
    $("#"+id).rotate(angle);
}

function saveRotatedImage( id, UserId ){
	  if (UserId){
		  user_id = UserId;  
	  }
	if($("#savePhoto_"+id).attr("disabled")=="disabled")
    {
        return false;
    } 
	$('#savePhoto_'+id).attr("disabled","disabled");
	
	var url = "adminpanel.php";
	$.ajax({
		  type: "POST",
		  url: url,
		  data: {id:id,action:"rotate",angle:angle,user_id:user_id},
		  dataType: "JSON",
		  beforeSend: function() { 
			  $('#rotate_'+id).hide();
			  $('#savePhoto_'+id).prop('value',"Saving...");
		  },
		  success: function(data){
			  //window.location.reload();
			  $('#savePhoto_'+id).prop('value',"Saved");
			  $('#savePhoto_'+id).removeAttr("disabled");
			  $('#button_'+id).hide();
			  $('#savePhoto_'+id).hide();
		  }
		});
}
function showpopup(id){
	//alert(id);
	var userId = id;
	$('.rejectid').attr('id', userId);
	$('.messagereply').show();
	//$('#'+id).show();
}


function rejectId(id){
	var userId = id;
	var reason = $("input[name=idreject]:radio:checked").val();
	var fname = $("input[name='fname']").val();
	var lname = $("input[name='lname']").val();
	var dob = $("input[name='date']").val();
	var fname1 = $("input[name='fname1']").val();
	var lname1 = $("input[name='lname1']").val();
	var dob1 = $("input[name='date1']").val();
	if(reason==undefined || reason==''){
		alert("Choose reason");
	}
	else if (reason=='name_mismatch')
		{
		if (fname.trim()=='' || lname.trim()=='' )
			{
			 alert("Choose First name and last name");
			}
		else {
			$('.messagereply').hide();
			$("#buttonId_"+userId).hide();
			$.post("adminpanel.php",{
				                       action : 'verify_id',
				                       userId : userId,
				                       status : 'rejected',
				                       reason : reason,
				                       fname : fname,
				                       lname : lname
			}, function(data) {
				if(data.error){
					alert(data.error);
				}if(data.status=='Success'){
					$('.messagereply').hide();
					$("#buttonId_"+userId).show();
					$("#buttonId_"+userId).html("Id Status:"+data.action);
				}
			},'json');
		}
		}
	else if (reason=='age_mismatch')
		
		
	{	$('.messagereply').hide();
		$("#buttonId_"+userId).hide();
		$.post("adminpanel.php",{
			                       action : 'verify_id',
			                       userId : userId,
			                       status : 'rejected',
			                       reason : reason,
			                       dob : dob
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#buttonId_"+userId).show();
				$("#buttonId_"+userId).html("Id Status:"+data.action);
			}
		},'json');
	}
	
	
else if (reason=='name_age_mismatch')
	
	{   if (fname1.trim()=='' || lname1.trim()=='' )
	     {
		    alert("Choose First name and last name");
		 }
	else { $('.messagereply').hide();
	       $("#buttonId_"+userId).hide();
		$.post("adminpanel.php",{
			                       action : 'verify_id',
			                       userId : userId,
			                       status : 'rejected',
			                       reason : reason,
			                       dob : dob1,
			                       fname : fname1,  
			                       lname : lname1
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#buttonId_"+userId).show();
				$("#buttonId_"+userId).html("Id Status:"+data.action);
			}
		},'json');
	  }
	}
		
	else{
		$('.messagereply').hide();
		$("#buttonId_"+userId).hide();
		$.post("adminpanel.php",{
			                       action : 'verify_id',
			                       userId : userId,
			                       status : 'rejected',
			                       reason : reason
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#buttonId_"+userId).show();
				$("#buttonId_"+userId).html("Id Status:"+data.action);
			}
		},'json');
	}
}


/*function rejectAddress(id){
	var userId = id;
	var reason = $("input[name=idreject]:radio:checked").val();
	if(reason==undefined || reason==''){
		alert("Choose reason");
	}else{
		$.post("adminpanel.php",{
			action : 'verify_address',
			userId : userId,
			status : 'rejected',
			reason : reason
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#button_"+userId).html("Address Status:"+data.action);
			}
		},'json');
	}
}
*/

function rejectEmployment(id){
	var userId = id;
	var reason = $("input[name=idreject]:radio:checked").val();
//	var fname = $("input[name='fname']").val();
//	var lname = $("input[name='lname']").val();
//	var cname = $("input[name='cname']").val();
//	var fname1 = $("input[name='fname1']").val();
//	var lname1 = $("input[name='lname1']").val();
//	var cname1 = $("input[name='cname1']").val();
	if(reason==undefined || reason==''){
		alert("Choose reason");
	}
	/*else if(reason=='name_mis'){
		$.post("adminpanel.php",{
			action : 'verify_employment',
			userId : userId,
			status : 'rejected',
			reason : reason,
			fname  : fname,
			lname  : lname
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#button_"+userId).html("Employment Status:"+data.action);
			}
		},'json');
	}
	
	else if(reason=='company_mis'){
		$.post("adminpanel.php",{
			action : 'verify_employment',
			userId : userId,
			status : 'rejected',
			reason : reason,
			cname  : cname,
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#button_"+userId).html("Employment Status:"+data.action);
			}
		},'json');
	}
	
	else if(reason=='name_company_mis'){
		$.post("adminpanel.php",{
			action : 'verify_employment',
			userId : userId,
			status : 'rejected',
			reason : reason,
			fname  : fname1,
			lname  : lname1,
			cname  : cname1,
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#button_"+userId).html("Employment Status:"+data.action);
			}
		},'json');
	}
	
	*/
	else{
		$('.messagereply').hide(); 
		$("#button_"+userId).hide();
		$.post("adminpanel.php",{
			action : 'verify_employment',
			userId : userId,
			status : 'rejected',
			reason : reason
		}, function(data) {
			if(data.error){
				alert(data.error);
			}if(data.status=='Success'){
				$('.messagereply').hide();
				$("#button_"+userId).show();
				$("#button_"+userId).html("Employment Status:"+data.action);
			}
		},'json');
	}
}


$(function() {
    $('#btsubmit').bind('click', function(){
        var txtVal =  $('#dob').val();
        if(isDate(txtVal)==false)
            alert('Invalid Date');
        else{
        	var id = $(this).attr('id');
        	rejectId(id);
        }
       
    });
    
    $('#btsubmit1').bind('click', function(){
        var txtVal =  $('#dob1').val();
        if(isDate(txtVal)==false)
            alert('Invalid Date');
        else{
        	var id = $(this).attr('id');
        	rejectId(id);
        }
       
    });
    
});


$(document).ready(function() {
	angle = 0;
	$("input[name='idreject']").on('change', function(event){
		var idreject=$("[name='idreject']:checked").val();
		if(idreject=='name_mismatch'){ 
			$("#name_mismatch").show();
			$("#age_mismatch").hide();
			$("#name_age_mismatch").hide();
			$("#reject_button").hide();
		}
		else if(idreject=='age_mismatch'){
			$("#age_mismatch").show();
			$("#name_mismatch").hide();
			$("#name_age_mismatch").hide();
			$("#reject_button").hide();
		}else if(idreject=='name_age_mismatch'){
			$("#age_mismatch").hide();
			$("#name_mismatch").hide();
			$("#name_age_mismatch").show();
			$("#reject_button").hide();
		}else {
			$("#age_mismatch").hide();
			$("#name_mismatch").hide();
			$("#name_age_mismatch").hide();
			$("#reject_button").show();  
		}
		
		
		/*if(idreject=='name_mis'){ 
			$("#name_mis").show();
			$("#company_mis").hide();
			$("#name_company_mis").hide();
			$("#reject_button1").hide();
		}
		else if(idreject=='company_mis'){
			$("#company_mis").show();
			$("#name_mis").hide();
			$("#name_company_mis").hide();
			$("#reject_button1").hide();
		}else if(idreject=='name_company_mis'){
			$("#comapny_mis").hide();
			$("#name_mis").hide();
			$("#name_company_mis").show();
			$("#reject_button1").hide();
		}else {
			$("#company_mis").hide();
			$("#name_mis").hide();
			$("#name_company_mis").hide();
			$("#reject_button1").show();  
		}*/
		
		
	});
	
	
});