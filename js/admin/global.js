var application = {
    global: {
        globalWaitDialog: function (action, displayText) {
            if (!displayText) {
                displayText = "Please wait...";
            }
            /*$("div.display_wait_overlay p.dw_text").text(displayText);*/
            if (action === "show")
                $("div.display_wait_overlay").show();
            /*application.global.repositionOverlay();*/
            if (action === "hide")
                $("div.display_wait_overlay").hide();
        },
        alertModal: function(title, body, type){
            var modalElement = $("#alert-modal");
            modalElement.find('#modal-title').text(title);
            modalElement.find('#alert-text').text(body);
            if(type == undefined || type ==  null)
                type="alert-danger";

            modalElement.find('#alert-text').removeClass();
            modalElement.find('#alert-text').addClass("alert");
            modalElement.find('#alert-text').addClass("alert-"+type);
            modalElement.modal();
        }
    }
};

function start() {
    var googleId = "";
    $.ajax({
        url: $('#app_base_url').val()+'/dealAdmin/adminLogin.php',
        type: "POST",
        // contentType: "application/json",
        data: {"action":"get_google_id"},
        success: function(result){
            result = JSON.parse(result);
            if(result.status == "success"){
                googleId = result.google_id;
            }
            gapi.load('auth2', function() {
                auth2 = gapi.auth2.init({
                    client_id: googleId
                });
            });

        },
        error: function(result){
            application.global.alertModal("Error","Unable to fetch google id","danger");
        }
    });
}

function signInCallback(authResult) {
    var loginUrl = $('#login-url').val();
    console.log(authResult);
    if (authResult['code']) {

        // Hide the sign-in button now that the user is authorized, for example:
        $('#signinButton').attr('style', 'display: none');

        // Send the code to the server
        $.ajax({
            type: 'POST',
            url: loginUrl,
            success: function(result) {
                $('#login_bx_main').modal('toggle');
                result = JSON.parse(result);
                if(result.login == true){
                    if(result.new_user == true){
                        application.global.alertModal("Sign Up Success", "Congratulations, you are now an Admin. Contact Shashwat to grant you permissions here", "success");
                    }else{
                        window.location.reload();
                    }
                }
                if(result.tm_email_error == true) {
                    application.global.alertModal("Houston ! We have a problem !", "You need a TrulyMadly email address to sign up here.", "danger");
                }
            },
          //  processData: false,
            data:{"authCode": authResult['code']}
        });
    } else {
        // There was an error.
    }
}

$(document).ready(function(){
    if($('#login-required') != undefined && $('#login-required').val() == 'login_req'){
        $('.check-me').empty();
        $('#login_bx_main').modal('toggle');
        $('.funMesssage').random().show();
    }

    $("#pass-login-btn").click(function(event){
        loginViaPassword();
    });

    $('#googleSigninButton').click(function() {
        // signInCallback defined in step 6.
        auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then(signInCallback);
    });
});

$.fn.random = function() {
    return this.eq(Math.floor(Math.random() * this.length));
}

function loginToServer(jsonData){
    var loginUrl = $('#login-url').val();
    application.global.globalWaitDialog("show");
    $.ajax({
        url: loginUrl,
        type: "POST",
        // contentType: "application/json",
        data: jsonData,
        success: function(result){
            result = JSON.parse(result);
            if(result.status == "success"){
                window.location.reload();
            }
            if(result.status == "failure"){
                hideWait();
               $('#wrong-email').text("Invalid Username or Password");
                $('#wrong-email').slideDown();
            }

        },
        error: function(result){
            console.log('error');
            console.log(result);
        }
    });
}

function loginViaPassword(){
    showLoginWait()
    var email=$('#login-email').val();
    var password=$('#login-password').val();
    var data =  {"email_id":email,"password":password,"action":"login_new"};
    console.log(data);
    loginToServer(data);
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

function showLoginWait(){
    $('#show-login-wait').show();
    $('#my-signin2').hide();
    $('#pass-login-btn').hide();
    $('#wrong-email').hide();
}

function hideWait(){
    application.global.globalWaitDialog("hide");
    $('#show-login-wait').hide();
    $('#my-signin2').show();
    $('#pass-login-btn').show();
}
