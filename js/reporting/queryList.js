$(document).ready(function(){
    $('#filter-my-query-button').click(function(event){
        event.preventDefault();
        var admin_id=$('#admin-id').val();
        $('#filter-admin-id').val(admin_id);
        submitFilterForm();
    });

    $('#filter-admin-id').change(function(event){
        application.global.globalWaitDialog("show");
        submitFilterForm();
    });

    $('#starred').change(function(event){
        application.global.globalWaitDialog("show");
        submitFilterForm();
    });


    if($('#set-filter-starred') != undefined){
        if($('#set-filter-starred').val() == 'yes'){
            $('#starred').prop("checked", true);
        }else{
            $('#starred').prop("checked", false);
        }
    }

    if($('#set-filter-admin') != undefined){
        $('#filter-admin-id').val($('#set-filter-admin').val());
    }
})

$(document).on("click", ".undo-delete", function(event){
    console.log("yoyo");
    updateQueryStar($(this), "undo_delete");
});

$(document).on("click", ".star-it", function(event){
    updateQueryStar($(this), "star");
});

$(document).on("click", ".unstar-it", function(event){
    updateQueryStar($(this), "unstar");
});

$(document).on("click", ".delete-it", function(event){
    updateQueryStar($(this), "delete");
});


function submitFilterForm(){
    if($('#starred').prop("checked") == true)
        $('#starred').val('yes');
    else
        $('#starred').val('no');
    $('#query-filter-form').submit();
}

function updateQueryStar(element, action){
    application.global.globalWaitDialog("show");
    var createdBy = element.data("admin-id");
    var isSuperSuper = element.data("super-admin");
    var loggedInUser = $('#admin-id').val();
    var queryId = element.data("query-id");
    if(loggedInUser != createdBy && isSuperSuper != 'yes') {
        application.global.globalWaitDialog("hide");
        application.global.alertModal("Unauthorized action", "You can only " + action + " queries created by you. Unless you're super admin.", "danger");
    }
    else{
        var url = $('#app_base_url').val();
        $.ajax({
            url: url+"/reporting/dashboard_api.php",
            type: "POST",
            // contentType: "application/json",
            data: {"query_id":queryId,"action":action},
            success: function(result){
                application.global.globalWaitDialog("hide");
                result = JSON.parse(result);
                if(result.status == "success"){
                    if(action=='star'){
                        element.removeClass();
                        element.addClass('glyphicon');
                        element.addClass('glyphicon-star');
                        element.addClass('unstar-it');
                    }else if(action=='unstar'){
                        element.removeClass();
                        element.addClass('glyphicon');
                        element.addClass('glyphicon-star-empty');
                        element.addClass('star-it');
                    }else if(action == 'delete'){
                      //  element.closest('tr').slideUp();
                        element.removeClass();
                        element.addClass("glyphicon");
                        element.addClass("glyphicon-repeat");
                        element.addClass("undo-delete");
                        element.closest('tr').removeClass("success");
                        element.closest('tr').addClass("danger");
                    }else if(action == "undo_delete"){
                        element.closest('tr').removeClass("danger");
                        element.closest('tr').addClass("success");
                        element.removeClass();
                        element.addClass("glyphicon");
                        element.addClass("glyphicon-trash");
                        element.addClass("delete-it");
                    }
                }
                if(result.status == "failure"){
                    application.global.alertModal("Error", "Some error occured while updating. Please try again later.", "danger")
                }

            },
            error: function(result){
                console.log('error');
                console.log(result);
            }
        });
    }

}