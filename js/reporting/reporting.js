var dTable = undefined;
var getCols = 1;
function runQuery(){
	application.global.globalWaitDialog("show");
	$('.menu-list').slideUp();
	var baseUrl = $('#action-url').val();
	var query=$('#query').val();
	var thead = $('#query-table-head');
	var tbody = $('#query-table-body');
	sanitizeQuery();
	var columnNameArray = getColumnsFromServer(); 			//getColumnNameArray();

	/*if(dTable != undefined) {
		dTable.destroy();
		thead.empty();
		tbody.empty();
	}
	$.ajax({
		url: baseUrl+"/reporting/dashboard_api.php",
		type: "POST",
		// contentType: "application/json",
		data: {'action':"run_query",'query':query},
		success: function(result){
			result = JSON.parse(result);
			if(result.status == "success"){
				$('#query-error').slideUp();
				thead.append("<tr></tr>");
				$.each(result.fields, function(i, item) {
					thead.find('tr').last().append("<th>"+item+"</th>");
				});
				$.each(result.resultSet, function(i, item){
					tbody.append("<tr class=\"success\"></tr>");
					$.each(item, function(j, value){
						tbody.find('tr').last().append("<td>"+value+"</td>");
					});
				});
				dTable = $('#query-result').DataTable(
					{
						dom: 'Bflrtip',
						buttons: [
							'copy', 'csv', 'excel', 'pdf', 'print'
						],
						"lengthMenu": [ 10, 25, 50, 75, 100 ]
					}
				);
				application.global.globalWaitDialog("hide");
			}
			if(result.status == "error"){
				$('#query-error').slideDown();
				$('#query-error').find('.error-text').text(result.error);
				application.global.globalWaitDialog("hide");
			}

		},
		error: function(result){
			console.log('error');
			console.log(result);
		}
	});*/
}

function sanitizeQuery(){
	var query = $('#query').val();
	if(query.indexOf('limit') != -1)
		query = query.substr(0,query.indexOf('limit'));
	if(query.indexOf(';') != -1)
	query = query.substr(0,query.indexOf(';'));
	$('#query').val(query);
}

function saveQuery(){
	$('#save-query-button').hide();
	$('.show-wait').show();
	var actionUrl = $('#save-query-form').attr('action');
	$('#query-text').val($('#query').val());
	$('#is-cron').val($('#is-cron').prop("checked"));
	$.ajax({
		url: actionUrl,
		type: "POST",
		// contentType: "application/json",
		data: $('#save-query-form').serializeArray(),
		success: function(result){
			result = JSON.parse(result);
			if(result.status == "success"){
				console.log(result);
				window.location = $('#action-url').val()+'/reporting/dashboard.php?query_id='+result.query_id;
			}
			if(result.status == "error"){
				$('#query-error').slideDown();
				$('#query-error').find('.error-text').text(result.error);
			}

		},
		error: function(result){
			console.log('error');
			console.log(result);
		}
	});
}

function updateTableDetails(tableNameElement, tableName){
	var baseUrl = $('#action-url').val();
	application.global.globalWaitDialog("show");

	$.ajax({
		url: baseUrl+"/reporting/dashboard_api.php",
		type: "POST",
		// contentType: "application/json",
		data: {"table_name":tableName,"action":"get_table_details"},
		success: function(result){
			application.global.globalWaitDialog("hide");
			result = JSON.parse(result);
			if(result.status == "success"){
				$.each(result.tableDetails, function(i, item) {
					$('#' + tableName).append('<li class="table-field-name"><a href="#"> '+item.Field+'</a></li>');
				});
			}
			if(result.status == "error"){
				$('#query-error').slideDown();
				$('#query-error').find('.error-text').text(result.error);
			}

		},
		error: function(result){
			application.global.globalWaitDialog("hide");
			console.log('error');
			console.log(result);
		}
	});
}


$(document).ready(function(){
	$('#run-query').click(function(event){
		runQuery();
	})

	if($('#set-is-cron').val() === 'yes'){
		$('#is-cron').prop("checked", true);
		$("#query-frequency").val($('#set-frequency').val());
		$("#query-frequency").prop("disabled", false);
	}

	$('#is-cron').click(function(event){
		$("#query-frequency").prop("disabled", (_, val) => !val) ;
	})

	$('#save-query-button').click(function(event){
		event.preventDefault();
		saveQuery();
	})

	$('.table-name-menu-item').click(function(event){
		event.preventDefault();
		var tableName = $(this).find('.table-name').text();
		if($('#'+tableName).children().length == 0){
			updateTableDetails($(this),tableName);
		}
	})

	$('.minimize-menu').click(function(event){
		event.preventDefault();
		$('.menu-list').slideToggle();
	})
})

$(document).on("click", ".table-field-name", function(event){
	event.preventDefault();
	insertAtCaret('query',$(this).text().trim()+",");
});

function insertAtCaret(areaId,text) {
	var txtarea = document.getElementById(areaId);
	var scrollPos = txtarea.scrollTop;
	var strPos = 0;
	var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
		"ff" : (document.selection ? "ie" : false ) );
	if (br == "ie") {
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		strPos = range.text.length;
	}
	else if (br == "ff") strPos = txtarea.selectionStart;

	var front = (txtarea.value).substring(0,strPos);
	var back = (txtarea.value).substring(strPos,txtarea.value.length);
	txtarea.value=front+text+back;
	strPos = strPos + text.length;
	if (br == "ie") {
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		range.moveStart ('character', strPos);
		range.moveEnd ('character', 0);
		range.select();
	}
	else if (br == "ff") {
		txtarea.selectionStart = strPos;
		txtarea.selectionEnd = strPos;
		txtarea.focus();
	}
	txtarea.scrollTop = scrollPos;
}


function drawTable(divId, actionUrl, columnNames){
	application.global.globalWaitDialog("show");
	aoCols = new Array();
	for(var i=0;i<columnNames.length;i++){
		var obj;
		aoCols.push({ "sTitle": columnNames[i]});
	}
	if(dTable != undefined) {
		console.log("destroying");
		dTable.fnDestroy();
		$('#'+divId).empty();
		dTable = undefined;
	}
	dTable = $('#'+divId).dataTable({
		"bProcessing": false,
		"bFilter": false,
		"aaSorting": [[ 0, "desc" ]],
		"bServerSide": true,
		"bInfo": true,
		"sServerMethod": "POST",
		"dom": 'Bflrtip',
		"buttons": [
			'copy', 'csv', 'excel', 'pdf', 'print'
		],
		"pagingType": "full_numbers",
		"lengthMenu": [ 10, 25, 50, 75, 100, 'All' ],
		"fnServerParams":function (aoData) {
			aoData.push(
				{"name":"query", "value":$('#query').val()}
			);
		},
		"sAjaxSource": actionUrl,
		"fnDrawCallback": function() {},
		"fnInitComplete": function(aaData){
			application.global.globalWaitDialog("hide");
			if(aaData.json.aaData.status == "error") {
				$('#query-error').slideDown();
				$('#query-error').find('.error-text').text(aaData.json.aaData.error);
			}
		},
		"aoColumns" : aoCols,
	});

}

function getColumnsFromServer(){
	var colName = new Array();
	var baseUrl = $('#action-url').val();
	application.global.globalWaitDialog("show");

	$.ajax({
		url: baseUrl+"/reporting/dashboard_api.php",
		type: "POST",
		// contentType: "application/json",
		data: {"query":$('#query').val(),"action":"get_headers"},
		success: function(result){
			result = JSON.parse(result);
			if(result.status == "success"){
				colName = result.fields;
				drawTable("query-result",baseUrl+"/reporting/dashboard_api.php?action=run_query",colName);
				$('#query-error').slideUp();
			}
			if(result.status == "error"){
				$('#query-error').slideDown();
				$('#query-error').find('.error-text').text(result.error);
				application.global.globalWaitDialog("hide");
			}

		},
		error: function(result){
			application.global.globalWaitDialog("hide");
			console.log('error');
			console.log(result);
		}
	});
}
