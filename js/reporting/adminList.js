/**
 * Created by tarun on 07/03/16.
 */


$(document).ready(function(){
    $('.privileges').each(function() {
        var privileges = $(this).val();
        if($(this).val()) {
            $(this).siblings('ul').children('li').each(function () {
                if (privileges.indexOf($(this).text()) > -1) {
                    $(this).addClass('active');
                    $(this).addClass('activate');
                    $(this).removeClass('inactivate');
                    $(this).removeClass('inactive');
                }
            });
        }
    });
});

$(document).on("click", ".activate", function(event){
    event.preventDefault();
    $(this).removeClass('active');
    $(this).removeClass('activate');
    $(this).addClass('inactive');
    $(this).addClass('inactivate');
    updatePermissions($(this).closest('ul'));
});

$(document).on("click", ".inactivate", function(event){
    event.preventDefault();
    $(this).removeClass('inactive');
    $(this).removeClass('inactivate');
    $(this).addClass('active');
    $(this).addClass('activate');
    updatePermissions($(this).closest('ul'));
});


function updatePermissions(element){
    var privileges = "";
    element.children('li').each(function(){
        if($(this).hasClass('active')){
            privileges += $(this).text()+",";
        }
    });
    privileges = privileges.replace(/,\s*$/, "");
    element.siblings('.privileges').val(privileges);
    var admin_id = element.data('admin-id');

    $.ajax({
        url: $('#app_base_url').val()+"/reporting/adminList.php",
        type: "POST",
        // contentType: "application/json",
        data: {"admin_id":admin_id,"privileges":privileges,"action":"update_privileges"},
        success: function(result){
            result = JSON.parse(result);
            if(result.status == "success"){
       //         application.global.alertModal("updated permission","Permissions have been updated Yo !","info");
            }
            if(result.status == "error"){
                application.global.alertModal("Yo","There was an error updating the Permissions Yo!","danger")
            }

        },
        error: function(result){
            console.log('error');
            console.log(result);
        }
    });

}