var wls = window.localStorage;
var prevLikeUrl;
var prevHideUrl;
var prevmaybeUrl;
var getAttributeTime;
var cachedObj;
var actionFlag = 0;
var finalTemplateRenderTime;
function getDataAttributes(callback) {
	var currentTime = (new Date()).getTime();
	if (wls && !wls['IS_DATA_ATTRIBUTES_DIRTY']
			&& wls['MATCHES_DATA_ATTRIBUTES']
			&& (currentTime - parseInt(wls['LAST_UPDATE_TIME'], 10)) < 1800000) {
		// HACK TO CHECK IF SAME MATCHES PAGE IS OPEN FROM MULTIPLE DEVICES
		// If first match of server doesn't match first match from local storage
		// then refresh data from server.
		var allMatchedIdsLocal = wls['ALL_MATCH_IDS'].split(',');
		var currentIndex = parseInt(wls['CURRENT_SLIDE_INDEX'], 10);
		var isSetFirstTileLocal = parseInt(wls['IS_SET_FIRST_TILE'], 10);
		// HACK for special caseto reduce one tile from deck
		if (isSetFirstTileLocal == 1 && isSetFirstTileServer == 0) {
			isSetFirstTileLocal = isSetFirstTileServer;
			wls['IS_SET_FIRST_TILE'] = isSetFirstTileServer;
			if (currentIndex > 0) {
				currentIndex--;
				wls['CURRENT_SLIDE_INDEX'] = currentIndex;
			}
		}
		if (isAuthentic == 0
				|| (allMatchedIdsLocal[currentIndex] == allMatchedIdsServer[0])) {
			cachedObj = 1;
			callback(JSON.parse(wls['MATCHES_DATA_ATTRIBUTES']),
					allMatchedIdsLocal, isSetFirstTileLocal);
			getAttributeTime = (new Date()).getTime();
		} else {
			cachedObj = 0;
			console.log('fetching new data from server 1');
			getDataAttributesFromServer(callback);
			getAttributeTime = (new Date()).getTime();
		}
	} else {
		cachedObj = 0;
		console.log('fetching new data from server 2');
		getDataAttributesFromServer(callback);
		getAttributeTime = (new Date()).getTime();
	}

}

function getDataAttributesFromServer(callback) {
	$.getJSON('?getAttributes=true', function(data) {
		if (wls) {
			wls['MATCHES_DATA_ATTRIBUTES'] = JSON.stringify(data);
			wls['ALL_MATCH_IDS'] = allMatchedIdsServer;
			wls['IS_SET_FIRST_TILE'] = isSetFirstTileServer;
			wls['LAST_UPDATE_TIME'] = (new Date()).getTime();
			wls['CURRENT_SLIDE_INDEX'] = 0;
			wls.removeItem('IS_DATA_ATTRIBUTES_DIRTY');
		}
		data
		callback(data, allMatchedIdsServer, isSetFirstTileServer);
	});
}

var processDataAttribute = function(data, allMatchedIds, isSetFirstTile) {
	totalItems = 1;
	totalSlides = 0;
	if (isSetFirstTile) {
		totalItems++;
		$('#progressFullContainer').hide();
	}

	if (data != null) {
		for ( var i = 0; i < allMatchedIds.length; i++) {
			var key = allMatchedIds[i];
			if (data.hasOwnProperty(key)) {
				totalSlides++;
				// console.log(data[key]);
				data[key].profileImgIcon = cdnUrl
						+ '/images/profile/viewprofile.png';
				data[key].photoCount = cdnUrl
						+ "/images/profile/cameraicon.png";
				data[key].matchScore = cdnUrl
						+ "/images/profile/MATCH_SCORE3.png";
				data[key].likeStamp_url = cdnUrl
						+ '/images/profile/stampliked.png';
				data[key].hideStamp_url = cdnUrl
						+ '/images/profile/stampreject.png';
				data[key].maybeStamp_url = cdnUrl
						+ '/images/profile/stampmaybe.png';
				data[key].spacerImgIcon = cdnUrl + '/images/profile/spacer.png';
				var result = tmpl(templateMatches, data[key]);
				$(result).insertBefore($('#dummy-tile'));
			}
		}
	}
	totalItems += totalSlides;

	$("#loader_image").hide();

	if (totalSlides > 0) {
		$('#totalItemContainer').html(totalSlides);
	} else {
		$('#progressFullContainer').hide();
	}

	initializeDeck(isSetFirstTile);

	var index = parseInt(wls['CURRENT_SLIDE_INDEX'], 10);
	deck.slide(index);

	if (stopMovement == 1) {
		$('.arrowright').hide();
	}

	if (totalItems == 1) {
		$('.arrowright').hide();
		$('.arrowleft').hide();
	}

	$('body').swipe(
			{
				swipeLeft : function(event, direction, distance, duration,
						fingerCount) {
					// console.log(event,direction,distance,duration,fingerCount);
					deck.next();
				},
				swipeRight : function(event, direction, distance, duration,
						fingerCount) {
					// console.log(event,direction,distance,duration,fingerCount);
					deck.prev();
				}
			})

	finalTemplateRenderTime = (new Date()).getTime();
	if (cachedObj == 1) {
		activity = "cache";
	} else
		activity = "without_cache";
	matchObj = {
		"data" : {
			"time_taken" : (finalTemplateRenderTime - headLoadTimeStamp) / 1000,
			"activity" : "matches",
			"user_id" : user_id,
			"event_type" : "page_load_" + activity,
			"source" : "web"
		}
	};
	logEvent(matchObj);
	// ga('send', 'event', 'matches',"page_load_"+activity);

	/*
	 * $.ajax( { url : "eventTrack.php", data: matchObj }).done(function() {
	 * console.log("logged"); });
	 */// make an ajax to trk.php to save the data
}
jsLoadTimeStamp = (new Date()).getTime();
// console.log(jsLoadTimeStamp);
$(document)
		.ready(
				function() {

					//
					docReadyTimeStamp = (new Date()).getTime();
					// console.log(docReadyTimeStamp);

					$('#matchesContainer').on('click', '.propic, .viewprofile',
							function(e) {
								var elm = $(e.currentTarget);
								var profileurl = elm.attr('url');
								// console.log(elm);
								var allIds = wls['ALL_MATCH_IDS'].split(',');
								var curntIndex = wls['CURRENT_SLIDE_INDEX'];
								var userIdClicked = allIds[curntIndex];
								// console.log(userIdClicked);
								matchObj = {
									"data" : {
										"activity" : "matches",
										"user_id" : user_id,
										"event_type" : "view_profile_clicked",
										"source" : "web",
										"event_info" : {
											"user_id_clicked" : userIdClicked,
											"profile_url" : profileurl
										}
									}
								};
								// ga('send', 'event',
								// 'matches',"view_profile_clicked");
								logEvent(matchObj);
								setTimeout(function() {
									window.location.href = profileurl;
								}, 500);

							});

					$('.cutomradio')
							.each(
									function() {
										var self = $(this), label = self.next(), label_text = label
												.text();

										label.remove();
										self
												.iCheck( {

													checkboxClass : 'icheckbox_line-blue',
													radioClass : 'iradio_line-blue',
													insert : '<div class="icheck_line-icon"></div>'
															+ label_text
												});
									});

					if (hashTag == 1) {
						$("#regint").show();
					}

					if (firstHide == 1) {
						$.cookie("first_hide", 1);
					}
					
					if (firstMaybe == 1) {
						$.cookie("first_maybe", 1);
					} 

					/*
					 * rajesh changes start
					 * 
					 */
					$("input[name='interest']").on(
							'ifChecked',
							function(event) {
								if ($("input[name='interest']").filter(
										":checked").length == 5) {
									$("input[name='interest']").not(":checked")
											.iCheck("disable");
									$("#activeInterest").show();
									$("#inactiveInterest").hide();
								}
							});

					$("input[name='interest']").on(
							'ifUnchecked',
							function(event) {
								if ($("input[name='interest']").filter(
										":checked").length < 5) {
									$("input[name='interest']").not(":checked")
											.iCheck("enable");
									$("#activeInterest").hide();
									$("#inactiveInterest").show();
									// document.getElementById('hobby_Error').innerHTML="";
								}
							});

					$("#activeInterest").click(function() {
						savehashtags();
					});

					/*
					 * 
					 * rajesh changes end
					 * 
					 */

					$("#show_matches, #non_auth_thanks").click(function() {
						deck.next( {
							force : true
						});
					});

					if (oldUser == 1) {
						$("#whtnew").show();
					} else {
						$("#whtnew").hide();
					}

					var url = baseurl + "/js/blueimp/templateMatches.html?v=2.0";
					$.get(url, function(templateMatchesTemp) {
						templateMatches = templateMatchesTemp;
						getDataAttributes(processDataAttribute);
					});

					$('#matchesContainer')
							.on(
									'click',
									'.like_user',
									function(e) {

										var elm = $(e.currentTarget);
										var parent = elm.closest('.newmatch');
										var likeurl = elm.attr('url');
										// console.log(likeurl);
										if (likeurl) {
											if(actionFlag == 0){
												actionFlag = 1; 
											if (likeurl != prevLikeUrl) {
												prevLikeUrl = likeurl;

												parent.find('.likeStamp')
														.show();

												$
														.ajax( {
															url : likeurl
														})
														.done(
																function(
																		dataJson) {
																	console
																			.log(dataJson);
																	prevLikeUrl = null;
																	var data = JSON
																			.parse(dataJson);
																	if (data.return_action) {
																		if (data.return_action["mutualLike"]) {
																			$(
																					'#mutualLike')
																					.html(
																							'<p>'
																									+ data.return_action["mutualLike"]
																									+ '</p>');
																			// alert("mutual
																			// like:"
																			// +
																			// data.return_action["mutualLike"]);
																		}
																		// console.log(data.flag);
																		/*
																		 * msg_url =
																		 * data.msg_url;
																		 * var
																		 * fname =
																		 * data.name;
																		 * $('#fname').html(fname);
																		 */
																	}

																	// console.log('success');
																	deck
																			.next( {
																				force : true
																			});
																	parent
																			.find(
																					'.likeStamp')
																			.hide();

																	GAObj = {
																		"data" : {
																			"activity" : "matches",
																			"event_type" : "like"
																		}
																	};
																	logGA(GAObj);
																	actionFlag = 0;

																});
											}
											}

										} else {
											$('#diffframe4').show();
										}

									});

					$('#matchesContainer').on('click', '.hide_user',
							onClickHide);
					$('#yes_hide').on('click', function() {
						$('#hidematch').hide();
						onClickHide();
					});
					
					$('#matchesContainer').on('click', '.maybe_user',
							onClickMayBe);
					$('#yes_maybe').on('click', function() {
						$('#maybematch').hide();
						onClickMayBe();
					});	

					$('.arrowleft').click(function() {
						deck.prev();
					});

					$('.arrowright').click(function() {
						deck.next();
					});

				});

function onClickHide(e) {

	if ($.cookie("first_hide") == 1) {
		$.removeCookie("first_hide");
		$('#hidematch').show();
	} else {
		var elm = $('.newmatch.bespoke-active .hide_user');
		var parent = elm.closest('.newmatch');
		var hideurl = elm.attr('url');
		// console.log(hideurl);
		if (hideurl) {
			if(actionFlag == 0){
				actionFlag = 1;
			if (hideurl != prevHideUrl) {
				prevHideUrl = hideurl;

				parent.find('.hideStamp').show();
				// $('.hideStamp').show();
				$.ajax( {
					url : hideurl
				}).done(function() {
					prevHideUrl = null;
					// console.log('success');
					deck.next( {
						force : true
					});
					parent.find('.hideStamp').hide();

					GAObj = {
						"data" : {
							"activity" : "matches",
							"event_type" : "hide"
						}
					};
					logGA(GAObj);
					actionFlag = 0;
				});
			}
			}
		} else {
			$('#diffframe4').show();
		}
	}
}

function onClickMayBe(e) {
	

	if ($.cookie("first_maybe") == 1) {
		$.removeCookie("first_maybe");
		$('#maybematch').show();
	} else {
		/*
	if ($.cookie("first_maybe") == 1) {
		$.removeCookie("first_maybe");
		$('#maybematch').show();
		$('#maybeaction').click(function(){
			$('#maybematch').hide();
			performMayBe(e);
		});*/
/*		setTimeout(function() {
			$('#maybematch').hide();
			performMayBe(e);
		}, 2000);
*/
/*	} else {*/
		performMayBe(e);
	}
}

function performMayBe(e){
	var elm = $('.newmatch.bespoke-active .maybe_user');
	var parent = elm.closest('.newmatch');
	var maybeurl = elm.attr('url');
	 console.log(maybeurl);
	if (maybeurl) {
		if(actionFlag == 0){
			actionFlag = 1;
		if (maybeurl != prevmaybeUrl) {
			prevmaybeUrl = maybeurl;

			parent.find('.maybeStamp').show();
			// $('.hideStamp').show();
			$.ajax( {
				url : maybeurl
			}).done(function() {
				prevmaybeUrl = null;
				// console.log('success');
				deck.next( {
					force : true
				});
				parent.find('.maybeStamp').hide();

				GAObj = {
					"data" : {
						"activity" : "matches",
						"event_type" : "maybe"
					}
				};
				logGA(GAObj);

				actionFlag = 0;
				if($('#bookmark').html() == "")
					var oldCountMaybe = 1;
				else{
					//var oldCountMaybe = parseInt($('#bookmark').html()) + 1;
					var r = /\d+/;
					var s = $('#bookmark').html();
					var oldCountMaybe = parseInt(s.match(r)) + 1;
				}
				$('#bookmark').html('('+ oldCountMaybe+ ')');
				
			});
		}
		}
	} else {
		$('#diffframe4').show();
	}

}

function showProgress(index, isSetFirstTile) {
	if (index + 1 >= totalItems || (isSetFirstTile && index == 0)) {
		$('#progressFullContainer').hide();
	} else {
		$('#progressFullContainer').show();
		if (totalItems && totalItems > 0) {
			$('#progressContainer').html(isSetFirstTile ? index : index + 1);
		}
	}

}

function initializeDeck(isSetFirstTile) {
	deck = bespoke.from('#matchesContainer');

	deck.on('activate', function(event) {
		// event.slide; // Relevant slide
		// event.index; // Index of relevant slide

		if (event.index == 0) {
			$('.arrowleft').hide();
		} else {
			$('.arrowleft').show();
		}
		if (event.index == totalItems - 1) {
			$('.arrowright').hide();
		} else {
			$('.arrowright').show();
		}
		showProgress(event.index, isSetFirstTile);
	});
	deck.on('next', function(event) {
		if (isAuthentic == 1 && event.force !== true) {
			return false
		} else if (stopMovement == 1 && event.force !== true) {
			return false;
		} else {
			if (wls && window.localStorage['CURRENT_SLIDE_INDEX']) {
				var index = parseInt(wls['CURRENT_SLIDE_INDEX'], 10);
				index++;
				wls['CURRENT_SLIDE_INDEX'] = index;
			}
		}
	});
	deck.on('prev', function(event) {
		if (isAuthentic == 1) {
			return false;
		} else {
			if (wls && window.localStorage['CURRENT_SLIDE_INDEX']) {
				var index = parseInt(wls['CURRENT_SLIDE_INDEX'], 10);
				index--;
				wls['CURRENT_SLIDE_INDEX'] = index;
			}
		}
	});

}

/*
 * function logEvent(matchObj){ var track = new Image();
 * track.src=baseurl+"/trk.php?data[activity]="+matchObj['data']['activity']+"&data[event_type]="+matchObj['data']['event_type']+"&data[time_taken]="+matchObj['data']['time_taken']+"&data[user_id]="+matchObj['data']['user_id']+"&data[source]="+matchObj['data']['source']+"&data[event_info]="+JSON.stringify(matchObj['data']['event_info'])+"&time="+Date.now() }
 */

/*
 * rajesh change start
 * 
 */
function savehashtags() {
	var interestVal = new Array();
	$("input[name='interest']:checked").each(function() {
		interestVal.push($(this).val());
	});
	$.ajax( {
		url : baseurl + "/update.php",
		type : "POST",
		dataType : 'json',
		data : {
			param : 'interest',
			TM_interest : JSON.stringify(interestVal)
		},
		beforeSend : function() {
			$('#activeInterest').text("Saving...");
		},
		success : function(data) {
			if (data.responseCode == 200) {
				$('#activeInterest').text("Saved");
				$("#regint").hide();
			} else {
				$('#activeInterest').text("Not Saved");
			}
		}
	});
}
/*
 * rajesh change end
 */
