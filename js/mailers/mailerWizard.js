

$(document).ready(function(){

    $('#send-to-emails').click(function(event)
    {
       sendToEmails();
    });
    $('#save-form-action').click(function(event)
    {
       saveCampaignName();
    });

    $('#send-test-email').click(function(event)
    {
       sendTestMail();
    });

    $('#save-mail-content').click(function(event)
    {
        saveMailContents();
    });

    $('#start-campaign').click(function(event)
    {
        sendCampaign();
    });
    $('#send-user-data').click(function(event)
    {
        sendUserData();
    });
    $('#img-upload').click(function(event)
    {
        showImagePopUp();
    });
    $('#use-image').click(function(event)
    {
    	generateHTMLFromImage();
     });
    
    $('#image-data-clear').click(function(event)
    {
       	clearImageData();
     });
    	
    $('#count-users').click(function(event)
    {
       	countTargetUsers();
    });

    
    $('.final-status input').attr('readonly', 'readonly');
    $('.save-and-proceed').hide();

    baseurl = $('#baseurl').val();
    campaign_id = $('#campaign_id').val();
    mailerApiUrl = baseurl + '/mailers/mailerApi.php' ;
    mailerWizardUrl = baseurl + '/mailers/mailerWizard.php';
    //console.log(mailerApiUrl);
});

$.getJSON($('#baseurl').val()+'/register_data.json', function( data ) {
    geoData = data;
    var cityData = geoData.cities;
   // console.log(cityData);
    var count = 0;
    cityData.forEach(function (item) {
        $('#cities').append('<option value="' + item.city_id + '">' + item.name + '</option>');
       // console.log(item);
        count ++;
        if(count == cityData.length){
            $('.chosen-select').chosen(); 
            $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
        }
    });

});

var old = '';
function update()
{
  //  console.log('Update called') ;
    var textarea = document.f.ta;
    var d = parent.dynamicframe.document;
    if (old != textarea.value)
    {
        old = textarea.value;
        d.open();
        d.write(old);
        d.close();
      //  console.log('writing to windows') ;
    }
    window.setTimeout(update, 150);
}


function clearImageData()
{
	$('.attach-image').show();
	$('.to-show').hide();
	 
}

function generateHTMLFromImage()
{
	 var imageUrl = $('#image-url-only').val() ;

	 $.ajax({
	        url: mailerApiUrl,
	        type: "POST",
	        data: {'image_url':imageUrl,
	            'action':'generate_html'},
	        success: function(result){
	           // console.log(result);
	            result = JSON.parse(result);
	            if(result.status == "success")
	            {
	                //console.log(atob(result.html));  
	                $('#canvas').val(atob(result.html));
	                //window.location.href = mailerWizardUrl + '?campaign_id=' + campaignId;
	            }

	            if(result.error)
	            {
	                alert(result.error);
	            }
	        },
	        error: function(result){
	            console.log('error');
	            console.log(result);
	            alert(error);
	        }
	    });
	 
}

$(document).on("change", ".attach-image", function(event)
		{
			var fileUpload = $(this)[0];
			var fileElem = $(this);
			//IsValidImage = false ;
			//var alertMessage = "Empty";
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
			if (regex.test(fileUpload.value.toLowerCase()))
			{
				//Check whether HTML5 is supported.
				if (typeof (fileUpload.files) != "undefined")
				{
					//Initiate the FileReader object.
					var reader = new FileReader();
					//Read the contents of Image File.
					reader.readAsDataURL(fileUpload.files[0]);
					reader.onload = function (e)
					{
						//Initiate the JavaScript Image object.
						var image = new Image();
						//Set the Base64 string return from FileReader as source.
						image.src = e.target.result;
						image.onload = function ()
						{
							//Determine the Height and Width.
							var height = this.height;
							var width = this.width;
							if (height >=100  && width  >= 100)
							{
								fileElem.siblings('.upload-image-server').show(); 
							}
							else
							{
								alertMessage = 'Image must be 100 X 100 at least. Your image is '+ width + ' x ' + height;
								application.global.alertModal('Error',alertMessage, "danger");
								fileElem.val("");
							}
						};
					}
				}
				else
				{
					alertMessage = 'This browser does not support HTML5.' ;
					application.global.alertModal('Error',alertMessage, "danger");
					fileElem.val("");
				}
			}
			else
			{
				 alertMessage = 'Please select a valid Image file.' ;
				 application.global.alertModal('Error',alertMessage, "danger");
				 fileElem.val("");
				 $('#image-upload-modal').modal('toggle');
			}

		});

$(document).on("click", "#copy-url", function(event)
{
	  var emailLink = document.querySelector('#image-url-only');  
	  var range = document.createRange();  
	  range.selectNode(emailLink);  
	  window.getSelection().addRange(range);    
    try { 
      var success = document.execCommand('copy'); 
      if (success) {
      console.log('text copied');
      }
    }
    catch (err) {
        console.log(err); 
      }
});

$(document).on("click", ".upload-image-server", function(event){
	var button= $(this);
	var actionUrl = baseurl + '/quizAdmin/photoUploadUtil.php';
	var imagePrefix = $('#image-url-new').val();

	var formData = new FormData();
	formData.append('file', $(this).siblings('.attach-image')[0].files[0]);
	formData.append('action','upload_image');
	formData.append('path','mailers/');
	$.ajax({
		url: actionUrl,
		type: "POST",
		data : formData,
		processData: false,  // tell jQuery not to process the data
		contentType: false,
		success: function(result){
			//console.log(result);
			result = JSON.parse(result);
			if(result.status == "success"){
				var completeImage = imagePrefix+'mailers/'+result.image_url ;
				 $('.upload-image-server').hide(); 
				 $('.attach-image').hide();
				button.siblings('.mail-image-tag').attr('src',completeImage);
				button.siblings('.mail-image-tag').attr('data-mail-image',completeImage);		
				 $('.mail-image-tag').show(); 
				 $('#image-url-only').attr('value',completeImage);
				 $('#image-url-only').attr('readonly','readonly')
				 $('#image-url-only').show();
				 $('.to-show').show(); 
				 $('#use-image').show(); 
			}

			if(result.error)
			{
				alert(result.error);
			}
		},
		error: function(result){
			console.log('error');
			console.log(result);
			alert(error);
		}
	});
	//console.log($(this).siblings('.option-image-tag').data('option-image'));

});



function sendToEmails()
{
    var EmailsList = $('#email-list').val();
    if(verifyEMailList(EmailsList) == true)
        sendEmailListToServer(EmailsList);
    else
        application.global.alertModal('Error',"Not a valid list of Email Ids", "danger");

}


function sendEmailListToServer(EmailsList)
{

    $.ajax({
        url: mailerApiUrl,
        type: "POST",
        data: {'campaign_id':campaign_id,
            'email_ids':EmailsList,
            'action':'send_to_emails'},
        success: function(result){
            console.log(result);
            result = JSON.parse(result);
            if(result.status == "success")
            {
                campaignId = result.campaign_id;
                window.location.href = mailerWizardUrl + '?campaign_id=' + campaignId;
            }

            if(result.error)
            {
                alert(result.error);
            }
        },
        error: function(result){
            console.log('error');
            console.log(result);
            alert(error);
        }
    });
}

function verifyEMailList(emailList)
{
    var emails = emailList.split(",");
    var valid = true;
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    for (var i = 0; i < emails.length; i++) {
        if( emails[i] == "" || ! regex.test(emails[i])){
            valid = false;
            console.log(emails[i]);
        }
    }
    return valid;
}


function saveMailContents()
{
    var mailContent = $('#canvas').val().trim();
    var mailSubject = $('#subject').val().trim();
    if(mailContent == undefined || mailContent == '')
    {
        alertMessage = "Please Enter some HTML";
        application.global.alertModal('Error',alertMessage, "danger");
    }else if(mailSubject == undefined || mailSubject == '')
    {
        alertMessage = "Subject can not be empty";
        application.global.alertModal('Error',alertMessage, "danger");
    }
    else
    {
        sendMailContentToServer(mailContent, mailSubject);
    }
}



/*
send the selected segment to server for a particular campaign
 */
function sendUserData()
{
    var gender = $('#gender').val() ;
    var minAge = $('#min-age').val() ;
    var maxAge = $('#max-age').val() ;
    var cities = $('.chosen-select').val()
    var isValidData = validateUserSegmentData(minAge, maxAge) ;
    if(isValidData == true)
    {
       // console.log('data verfied now send it to sever');
        sendUserSegmentDataToServer(gender, minAge, maxAge, cities);
    }
}


function countTargetUsers()
{
    var gender = $('#gender').val() ;
    var minAge = $('#min-age').val() ;
    var maxAge = $('#max-age').val() ;
    var cities = $('.chosen-select').val()
    var isValidData = validateUserSegmentData(minAge, maxAge) ;
    if(isValidData == true)
    {
       // console.log('data verfied now send it to sever');
        sendUserSegmentDataToServerForCount(gender, minAge, maxAge, cities);
    }
}

function sendUserSegmentDataToServer(gender, minAge, maxAge, cities, html,mailSubject)
{
    //console.log('Now send the data to server');
    $.ajax({
        url: mailerApiUrl,
        type: "POST",
        data: {'action':'save_segment',
            'campaign_id':campaign_id,
            'gender':gender,
            'min_age':minAge,
            'max_age':maxAge,
            'cities':cities},
        success: function(result){
            console.log(result);
            result = JSON.parse(result);
            if(result.status == "success")
            {
                campaignId = result.campaign_id;
                window.location.href = mailerWizardUrl + '?campaign_id=' + campaignId;
            }

            if(result.error)
            {
                alert(result.error);
            }
        },
        error: function(result){
            console.log('error');
            console.log(result);
            alert(error);
        }
    });

}



function sendUserSegmentDataToServerForCount(gender, minAge, maxAge, cities, html,mailSubject)
{
    //console.log('Now send the data to server');
    $.ajax({
        url: mailerApiUrl,
        type: "POST",
        data: {'action':'count_users',
            'campaign_id':campaign_id,
            'gender':gender,
            'min_age':minAge,
            'max_age':maxAge,
            'cities':cities},
        success: function(result){
           // console.log(result);
            result = JSON.parse(result);
            if(result.status == "success")
            {
                //campaignId = result.campaign_id;
                $('#send-user-data').show();
                application.global.alertModal('Done',"The mailer will be sent to around "+ result.count + " users"  , "info");
              //  window.location.href = mailerWizardUrl + '?campaign_id=' + campaignId;
            }

            if(result.error)
            {
                alert(result.error);
            }
        },
        error: function(result){
            console.log('error');
            console.log(result);
            alert(error);
        }
    });

}



function validateUserSegmentData(minAge, maxAge)
{
   // console.log('verify min age , max age and the mail content');
    if(minAge == undefined || maxAge == undefined || minAge == '' || maxAge == '')
    {
        application.global.alertModal('Error',"Please specify the age bracket of target users!!!!!!", "danger");
        return false ;
    }
    else if(isNaN(minAge) || isNaN(maxAge))
    {
        application.global.alertModal('Error','Age Should be a number', "danger");
        return false ;
    }
    else if(minAge < 18)
    {
        application.global.alertModal('Error','Minimum age can not be less than 18', "danger");
        return false ;
    }
    else if(maxAge > 100)
    {
        application.global.alertModal('Error','Maximum age can not be more than 100!!!!! LOL', "danger");
        return false ;
    }
    else if(minAge > maxAge)
    {
        console.log(minAge, maxAge);
        application.global.alertModal('Error','Minimum age can not be more than Max Age', "danger");
        return false ;
    }
    else
    {
        return true;
    }
    //alertMessage = "Please Enter an email id";
    //application.global.alertModal('Error',alertMessage, "danger");
}

function sendTestMail()
{
    // first check if test Email is filled
    testEMailId =    $('#test-email-receiver').val().trim();
    mailContent = $('#canvas').val().trim();
    mailSubject = $('#subject').val().trim();
    if(testEMailId == undefined || testEMailId == '')
    {
        alertMessage = "Please Enter an email id";
        application.global.alertModal('Error',alertMessage, "danger");
    }
    else if(mailContent == undefined || mailContent == '')
    {
        alertMessage = "Please Enter some HTMl";
        application.global.alertModal('Error',alertMessage, "danger");
    }else if(mailSubject == undefined || mailSubject == '')
    {
        alertMessage = "Subject can not be empty";
        application.global.alertModal('Error',alertMessage, "danger");
    }
    else
    {
        sendTestToServer(testEMailId, mailContent, mailSubject);
    }
}

function sendTestToServer(testEMailId, mailContent, mailSubject)
{

    $.ajax({
        url: mailerApiUrl,
        type: "POST",
        data: {'action':'send_test_mail',
                'email_id':testEMailId,
                'mail_subject':mailSubject,
                'mail_content':mailContent},
        success: function(result){
            console.log(result);
            result = JSON.parse(result);
            if(result.status == "success")
            {
                application.global.alertModal('Success','Test mail sent to '+testEMailId, "success");
                $('.save-and-proceed').show();
                //window.location.href = mailerWizardUrl + '?campaign_id=' + campaignId;
            }

            if(result.error)
            {
                alert(result.error);
            }
        },
        error: function(result){
            console.log('error');
            console.log(result);
            alert(error);
        }
    });
}

function sendMailContentToServer(mailContent, mailSubject)
{

    $.ajax({
        url: mailerApiUrl,
        type: "POST",
        data: {'action':'save_mail_content',
                'campaign_id':campaign_id,
                'mail_subject':mailSubject,
                'mail_content':mailContent},
        success: function(result){
            console.log(result);
            result = JSON.parse(result);
            if(result.status == "success")
            {
                window.location.href = mailerWizardUrl + '?campaign_id=' + campaign_id;
            }

            if(result.error)
            {
                alert(result.error);
            }
        },
        error: function(result){
            console.log('error');
            console.log(result);
            alert(error);
        }
    });
}


function sendCampaign()
{
    $.ajax({
        url: mailerApiUrl,
        type: "POST",
        data: {'campaign_id':campaign_id,'action':'send_campaign'},
        success: function(result){
            console.log(result);
            result = JSON.parse(result);
            if(result.status == "success")
            {
                campaignId = result.campaign_id;
                window.location.href = mailerWizardUrl + '?campaign_id=' + campaignId;
            }

            if(result.error)
            {
                alert(result.error);
            }
        },
        error: function(result){
            console.log('error');
            console.log(result);
            alert(error);
        }
    });
}

function saveCampaignName()
{
   // $.(#Campaign-name-input).val();
    name = $('#Campaign-name-input').val().trim();
    if(name == undefined || name =="" )
    {
        alertMessage = "Please Enter a name for the campiagn";
        application.global.alertModal('Error',alertMessage, "danger");
    }
    else
    {
        $.ajax({
            url: mailerApiUrl,
            type: "POST",
            data: {'campaign_name':name,'action':'save_new_campaign'},
            success: function(result){
                console.log(result);
                result = JSON.parse(result);
                if(result.status == "success")
                {
                    campaignId = result.campaign_id;
                    window.location.href = mailerWizardUrl + '?campaign_id=' + campaignId;
                }

                if(result.error)
                {
                    alert(result.error);
                }
            },
            error: function(result){
                console.log('error');
                console.log(result);
                alert(error);
            }
        });
    }
}



