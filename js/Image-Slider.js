//1. set ul width 
//2. image when click prev/next button
var ul;
var li_items;
var imageNumber;
var imageWidth;
var prev, next;
var currentPostion = 0;
var currentImage = 0;
var NO_OF_VISIBLE_IMAGES = 5;
var isAnimating = false;

function ImageSliderInit() {
	ul = document.getElementById('image_slider');
	if(!ul) return ;
	
	parentWidth = $('#image_slider').parent().width();
	li_items = ul.children;
	imageNumber = li_items.length;
	imageWidth = li_items[0].children[0].clientWidth;
	NO_OF_VISIBLE_IMAGES = parentWidth/imageWidth;
	
	ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
	prev = document.getElementById("prev");
	next = document.getElementById("next");
	prev.onclick = function() {
		onClickPrev();
	};
	next.onclick = function() {
		onClickNext();
	};
	if (imageNumber <= NO_OF_VISIBLE_IMAGES) {
		next.style.display = "none";
	} else {
		next.style.display = "";
	}
}

function animate(opts) {
	var start = new Date;
	var id = setInterval(function() {
		var timePassed = new Date - start;
		var progress = timePassed / opts.duration;
		if (progress > 1) {
			progress = 1;
		}
		var delta = opts.delta(progress);
		opts.step(delta);
		if (progress == 1) {
			clearInterval(id);
			opts.callback();
		}
	}, opts.delay || 17);
}

function slideTo(imageToGo) {
	if (isAnimating)
		return;
	isAnimating = true;
	var direction;
	var numOfImageToGo = Math.abs(imageToGo - currentImage);
	// slide toward left

	direction = currentImage > imageToGo ? 1 : -1;
	currentPostion = -1 * currentImage * imageWidth;
	var opts = {
		duration : 400,
		delta : function(p) {
			return p;
		},
		step : function(delta) {
			ul.style.left = parseInt(currentPostion + direction * delta
					* imageWidth * numOfImageToGo)
					+ 'px';
		},
		callback : function() {
			currentImage = imageToGo;
			if (currentImage <= 0) {
				prev.style.display = 'none';
			} else {
				prev.style.display = '';
			}
			if (currentImage >= imageNumber - NO_OF_VISIBLE_IMAGES) {
				next.style.display = 'none';
			} else {
				next.style.display = '';
			}
			isAnimating = false;
		}
	};
	animate(opts);
}

function onClickPrev() {
	if (currentImage == 0) {
		slideTo(imageNumber - 1);
	} else {
		slideTo(currentImage - 1);
	}
}

function onClickNext() {
	if (currentImage == imageNumber - 1) {
		slideTo(0);
	} else {
		slideTo(currentImage + 1);
	}
}

$(document).ready(function() {
	ImageSliderInit();
});
