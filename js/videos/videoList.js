function updateVideoStatus(actionUrl,status,video_id, ele,google_bool){

    console.log(ele);

    if(google_bool)
    {
        curr_status=$(ele).data('vision-status');
        //console.log(curr_status);
        var next_status;
        if(curr_status=='yes')
        {
            next_status='no';
        }
        else
        {
            next_status='yes';
        }

        //then this is another request,return after done in this if condition
        $.ajax({
            url: actionUrl,
            type: "POST",
            // contentType: "application/json",
            data: {'video_id':video_id,'action':next_status,type:'admin_action_vision'},
            success: function(result){
                //console.log(status);
                result = JSON.parse(result);
                if(result.status == "success"){
                    //console.log(result);
                    //console.log(next_status);
                    $(ele).data('vision-status',next_status);
                    var id="video_vision_"+video_id;
                    $('#'+id).text(next_status);
                    //console.log(ele);
                }
            },
            error: function(result){
                console.log('error');
                console.log(result);


            }
        });
        return ;
    }
    $(ele).closest('tr').slideUp();
    $.ajax({
        url: actionUrl,
        type: "POST",
        // contentType: "application/json",
        data: {'video_id':video_id,'action':status,type:'admin_action'},
        success: function(result){
            console.log(result);
            result = JSON.parse(result);
            if(result.status == "success"){
               console.log(result);
            }
        },
        error: function(result){
            console.log('error');
            console.log(result);
        }
    });

}

function HandleNext(baseUrl)
{

    var page_no=getParameterByName('page_no');

    var newUrl = location.href.replace("page_no="+page_no, "page_no="+(++page_no));
    console.log(newUrl);
    window.location.href =newUrl;

    // console.log(newUrl);
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}



$(document).on("click", ".delete-it", function(event){
    deleteDatespotAction($(this), "delete");
});


$(document).on("click", ".play-action", function(event){
    var source = $(this).data('video-url');
    console.log(source);
    $('#video-url-curr').parent('video').attr('src',source);
    $('#video-url-curr').attr('src', source);
});

$(document).on("load","#next_video",function(event){


});

$(document).on("click", ".play-activate", function(event){
    var source = $(this).data('video-id');
    //This is the video_id



});

$(document).on("click", ".undo-delete", function(event){
    deleteDatespotAction($(this), "undo_delete");
});

function deleteDatespotAction(element, action){
    application.global.globalWaitDialog("show");
    var actionUrl=$('#datespot-form').attr('action');
    var datespotHashId = element.data('id')
    $.ajax({
        url: actionUrl,
        type: "POST",
        // contentType: "application/json",
        data: {"datespotHashId": datespotHashId, "action": action},
        success: function (result) {
            result = JSON.parse(result);
            if(result.status == "success") {
                application.global.globalWaitDialog("hide");
                if (action == 'delete') {
                    //  element.closest('tr').slideUp();
                    element.removeClass();
                    element.addClass("glyphicon");
                    element.addClass("glyphicon-repeat");
                    element.addClass("undo-delete");
                    element.closest('tr').removeClass("success");
                    element.closest('tr').addClass("danger");
                } else if (action == "undo_delete") {
                    element.closest('tr').removeClass("danger");
                    element.closest('tr').addClass("success");
                    element.removeClass();
                    element.addClass("glyphicon");
                    element.addClass("glyphicon-trash");
                    element.addClass("delete-it");
                }
            }
            if(result.status == "failure"){
                application.global.alertModal("Error", "Some error occured while updating. Please try again later.", "danger")
            }
        }
    });
}

$(document).ready(function(){
    var x=document.getElementById('next_video');
    var visible= $(x).data('set-visibility');
    var count=$(x).data('video-count');
    if(visible!='1')
    {
        $(x).hide();
    }else
    {
        //Put in some condition
    }
    if(count==0)
    {
        alert('There are no videos to be displayed');

    }


})