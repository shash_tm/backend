function safeString(str) {

str=str.replace("'","&#039;");
return str;
}
function fillIndustry(idToFill) {
var data=industries;
$("#"+idToFill).append("<option value=''></option>");
$.each(industries,function(index,val) {
//$("#"+idToFill).append("<option value='"+index+"'>"+val+"</option>");
$("#"+idToFill).append("<option value='"+val+"'>"+val+"</option>");
});
$("#"+idToFill).trigger("liszt:updated");
}

function trackRegistration(activity,event_type,status,reset_time) {	
	//if(edit !==null && edit==false){
	reset_time = typeof reset_time === 'undefined' ? true : reset_time;
	var track = new Image();
	if(reset_time==false)
		var timeTaken = (new Date()).getTime() - time1.getTime();
	else
		var timeTaken = (new Date()).getTime() - time.getTime();
	timeTaken = timeTaken/1000;
	//if(reset_time==true)
	//time = new Date();
	track.src=baseurl+"/trk.php?data[activity]="+activity+"&data[event_type]="+event_type+"&data[time_taken]="+timeTaken+"&data[event_status]="+status+"&data[user_id]="+uid+"&data[source]=web&rand="+Math.random();
	if(reset_time==true)
		time = new Date();
	//}
	if(mobilecheck()){
		var source = 'mobile_web';
	}else{
		var source = 'web';
	}
	
	ga('send', 'event', activity,event_type,source);
}


function refreshInterests(initialize,importing_from_fb) {
var toAppend='',current='',toDisplay='';
if(initialize) {
$.each(allInterests,function(index,val){
current=index,toDisplay=val,toAppend='';
$("[name='"+current+"_preferences']").html('');
for(var i=0;i<toDisplay.length;i++,global_id++) {
toAppend+="<li class='fl'><label class='checkbox' for='checkbox_"+global_id+"'><input type='checkbox' name='"+current+"_preferences' value='"+toDisplay[i]+"' id='checkbox_"+global_id+"' ><span class='listitems'>"+toDisplay[i]+"</span> </label>  </li>";
} 

$("[name='"+current+"_preferences']").html(toAppend);
});

}
if(!initialize) {
if(toSelect) {
var count=0;
$.each(toSelect,function(index,val){
count++;
index=index.toLowerCase();
if(!(val.preferences==''&&val.favorites==''))
$("#"+index).parent().addClass("active").parent().addClass("popuplistactive");

toFill=val.preferences;
if(toFill!=undefined) {
for(var i=0;i<toFill.length;i++) {
$("input[name='"+index+"_preferences'][value='"+toFill[i]+"']").prop("checked",true).parent().addClass("checked");
}
}

toFill=val.favorites;
if(toFill!=undefined) {
	var fav_array=new Array();
//current_value=$("input[name='"+index.toLowerCase()+"_favorites']").val();
str='';
//[{id:1,text:'bug'},{id:2,text:'duplicate'}]
for(var i=0;i<toFill.length;i++) {
	fav_array.push({id:i,text:toFill[i]});
if(str=='')
str=toFill[i];
else str+=","+toFill[i];
/*if($("input[name='"+index+"_favorites']").val()=='')
$("input[name='"+index+"_favorites']").val(toFill[i]).trigger('change');
else $("input[name='"+index+"_favorites']").val($("input[name='"+index+"_favorites']").val()+","+toFill[i]).trigger('change');
*/
}

initial_values[index]=fav_array;
if($("input[name='"+index+"_favorites']").val()=='')
{
	$("input[name='"+index+"_favorites']").select2("val",["1"]);
}
else {
	$("input[name='"+index+"_favorites']").val($("input[name='"+index+"_favorites']").val()+","+str).trigger('change');
}
}
});
}
}
if(initialize) {
$("#music").parent().addClass("active").parent().addClass("popuplistactive").prepend("<img class='temp_arrow' src='images/register/arrow_down.png' />");
$("[name='music_preferences']").show();
$("[name='music_favorites_div']").show();
current_selected="music";
}

$(document).on('click',".checkbox, .radio",function(){
	//$(".checkbox, .radio").click(function(){
	    setupLabel();
	});
}

function validateDate(dd,mm,yy) {
var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
if (mm==1 || mm>2)
    if (dd>ListofDays[mm-1])
  	return false;
  
  if (mm==2)
  {
	
  var lyear = false;
  if ( (!(yy % 4) && yy % 100) || !(yy % 400))
  lyear = true;
  if ((lyear==false) && (dd>=29))
  return false;
  if ((lyear==true) && (dd>29))
  return false;
  }
return true; 

}

function fillEducation(toFill){
arrayTofill=educations;

var dataEducation=JSON.parse(arrayTofill),allEducations=[],key;
var data=JSON.parse(courses);
//$('#' + toFill).chosen();
var j=data[0].education_id;
var str='';
           str+="<option value=''></option><optgroup label='"+dataEducation[data[0].education_id]+"'>";
                        for(var i=0;i<data.length;i++) {
				if(j!=data[i].education_id) {
				str+="</optgroup>";
				j=data[i].education_id;
 str+="<optgroup label='"+dataEducation[j]+"'>";
	

			}
str+="<option value="+data[i]['id']+">"+data[i]['name']+"</option>"
//                            $('#' + toFill).append("<option value="+data[i]['id']+">"+data[i]['name']+"</option>");

}
$('#' + toFill).append(str+"</optgroup>");
//$('#' + toFill).append(str);
//$('#' + toFill).chosen();
$('#' + toFill).trigger("liszt:updated");

}



function fillEducationSpouse(toFill) {
arrayTofill=educations;
var data=JSON.parse(arrayTofill);
$('#' + toFill).chosen();
                        $('#' + toFill).append("<option value=''></option>");
			for(key in data) {
			
				$('#' + toFill).append("<option value="+key+">"+data[key]+"</option>");

}

if(toFill == 'education_spouse'){
	$('#' + toFill).prepend("<option value='-1'>Only Masters</option>");
	$('#' + toFill).prepend("<option value='-2'>Only Bachelors</option>");
	$('#' + toFill).prepend("<option value='-3'>Bachelors & Above</option>");
	$('#' + toFill).prepend("<option value='-4'>Masters & Above</option>");
	var spouse_value = $("#education_spouse").val();
	if(spouse_value==null && !edit)
		$('#' + toFill).prepend("<option value='-5' selected=selected>Any</option>");
	else
		$('#' + toFill).prepend("<option value='-5'>Any</option>");
}
			
$('#' + toFill).trigger("liszt:updated");
//$('#' + toFill).trigger("chosen:updated");
}

function fillEducation(toFill) {
	arrayTofill=educations;
	var data=JSON.parse(arrayTofill);
	//var subData = JSON.parse(educationsub);
	$('#' + toFill).chosen();
	$('#' + toFill).append("<option value=''></option>");
	var optionHtml = "";
	for(i=0;i<data.length;i++){ 
		optionHtml = optionHtml + "<option value='"+data[i]['id']+"'>"+data[i]['value']+"</option>";
	}
	$('#' + toFill).append(optionHtml);
	//			for(key in data) {
		//			console.log(data[key]);
					//if(subData[key]!=undefined){
//						console.log(data[key]);
			//			var optionHtml = "";
						//optionHtml = optionHtml + "<optgroup label='"+data[key]+"'>";
						
						//for(subkey in subData[key]){
				//			optionHtml = optionHtml + "<option value='"+key+"'>"+data[key]+"</option>";
						//}
						//optionHtml = optionHtml + "</optgroup>";
				//		$('#' + toFill).append(optionHtml);
				/*	}else{
						$('#' + toFill).append("<option value='"+key+"-1'>"+data[key]+"</option>");
					}*/
			//	}
				
	$('#' + toFill).trigger("liszt:updated");
}


function fillIncomeSpouse() {
toFill='income_spouse_start';
var income_spouse=JSON.parse(income_spouse_array);
$('#' + toFill).chosen();
                        $('#' + toFill).append("<option value=''></option>");
                        for(var i=0;i<income_spouse.length;i++) {
				if(income_spouse[i]==100)
				$('#' + toFill).append("<option value="+income_spouse[i]+">Rs. 1 Crore</option>");
				else  if(income_spouse[i]==0)
				$('#' + toFill).append("<option value=0>Any</option>");
				else  $('#' + toFill).append("<option value="+income_spouse[i]+">Rs. "+income_spouse[i]+" lakhs</option>");
}
				$('#' + toFill).append("<option value='101'>1 Cr plus</option>");
				$('#' + toFill).trigger("liszt:updated");
}

function setAutoCompleteForEducationInstitute(id){
	//$("#"+id).autocomplete({
	$("#"+id).catcomplete({
		source:function( request, response ) {
			$.ajax({
				url: "colleges.php?value="+$("#"+id).val()+"&institute=true",
				dataType: "json",
				success:function(data) {
					response($.map( data, function( item ) {
						return {
							label: item.name,
							value: item.name,
							category:"Suggestions"
						}
					}));
				}
			});
		},
		minlength:2
	});
/*	return {
        tags:true,
         tokenSeparators: [","," "],
        minimumInputLength:3,
    ajax: {
        url: baseurl+"/colleges.php",
        dataType: "json",
        quietMillis:500,
        cache:true,
        data: function(term, page) {
          return {
              value:term,
              institute:true
          };
        },
        results: function(data, page) {
        	return {
            results: data
          };
        }
      },
      initSelection : function (element, callback) {
    	  var data = [];
          $(element.val().split(",")).each(function () {
              data.push({id: this, name: this});
          });
          //console.log(data);
          callback(data);
          //var data = [{id:1,text:'bug'},{id:2,text:'duplicate'}];
         //callback(fav_array);
      },
      formatResult: function(results) { 
        	 return '<div><div>' + results.name +'</div></div>' ;
      },
      formatSelection: function(results) { 
    	  	 return results.name; 
      },
      multiple: true,	
      placeholder:"Enter Institute/Schools"
	};*/
	
}



function setAutoCompleteForEducationSpecialization(id,choosen_id){
	
	//$("#"+id).autocomplete({
	$("#"+id).catcomplete({
		source:function( request, response ) {
			var splitId = $("#"+choosen_id).val().split("-");
			$.ajax({
				url: "colleges.php?value="+$("#"+id).val()+"&education="+splitId[0]+"&specialisation=true",
				dataType: "json",
				success:function(data) {
					response($.map( data, function( item ) {
						return {
							label: item.name,
							value: item.name,
							category:"Suggestions"
						}
					}));
				}
			});
		},
		minlength:2
	});
}

function setAutoCompleteForDesignation(id){
	//for(var i=0;i<=addMoreWork;i++) {
		//$('#designation_'+addMoreWork).autocomplete({
	
	
	
	 
//	$("#"+id).autocomplete({
		$("#"+id).catcomplete({
			source:function( request, response ) {
				$.ajax({
					url: "colleges.php?value="+$("#"+id).val()+"&designation=true",
					dataType: "json",
					success:function(data) {
						response($.map( data, function( item ) {
							return {
								label: item.name,
								value: item.name,
								category:"Suggestions"
							}
						}));
					}
				});
			},
			minlength:2
		});
	//}
}

function prefillDateOfBirth(){
	if(typeof fb_dob != 'undefined' && fb_dob){
//	$("#whenBornDay").
	$("#whenBornDay option[value='" + fb_dob['fb_date']  + "']").attr("selected",'selected');
	$('#whenBornDay').trigger("liszt:updated");
	$("#whenBornMonth option[value='" + fb_dob['fb_month']  + "']").attr("selected",'selected');
	$('#whenBornMonth').trigger("liszt:updated");
	$("#whenBornYear option[value='" + fb_dob['fb_year']  + "']").attr("selected",'selected');
	$('#whenBornYear').trigger("liszt:updated");
	$("#whenBornYear").change();
	}
	
	if(typeof relationship_status != 'undefined' && relationship_status){
		if(relationship_status=='Never Married'){
			$("input[name='marital_status'][value='Never Married']").iCheck('check');//attr('checked','checked');
			$("#haveChildrenDiv").hide();
		}else{// if(relationship_status=='Divorced' || relationship_status=='Separated'){
			$('input[name="marital_status"][value="Married Before"]').iCheck('check');//attr('checked','checked');
			$("#haveChildrenDiv").show();
			if(edit)
			if(user_data.haveChildren!=null && user_data.haveChildren!=undefined){
				$('input[name="haveChildren"][value='+user_data.haveChildren+']').iCheck('check');
			}
		}
	}
	
}
function prefillStay(id){
	if(typeof fb_stay != 'undefined' && fb_stay){
		if(id == "country"){
			$("#stayCountry option[value='" + fb_stay['country']['country_id'] +"']").prop('selected',true);
			$("#stayCountry").trigger("liszt:updated");
			$("#stayCountry").change();
		}else if(id == "state"){
			$("#stayState option[value='" + fb_stay['country']['country_id'] + "-" +fb_stay['state']['state_id'] +"']").prop('selected',true);
			$("#stayState").trigger("liszt:updated");
			$("#stayState").change();
		}else if(id == "city"){
			$("#stayCity option[value='" + fb_stay['country']['country_id']+ "-" +fb_stay['state']['state_id'] + "-" +fb_stay['city']['city_id'] +"']").prop('selected',true);
			$("#stayCity").trigger("liszt:updated");
			$("#stayCity").change();
		}
	}
	
}

function removeInstitute(obj){
	var com = $(obj).children('li').children('div')[0].innerHTML;
	if(institute_arr.indexOf(com)!=-1){
		institute_arr.splice(institute_arr.indexOf(com), 1);
	}
	$(obj).remove();
}
function removeCompany(obj){
	var com = $(obj).children('li').children('div')[0].innerHTML;
	if(company_arr.indexOf(com)!=-1){
		company_arr.splice(company_arr.indexOf(com), 1);
	}
	$(obj).remove();
}
function prefillWork(){
	if(typeof fb_work != 'undefined' && fb_work){
		$("#companies").attr("placeholder", "Add More");
		$("#designation").val(unescape(fb_work['designation']));
		if(typeof fb_work['companies'] != 'undefined' && fb_work['companies']){
		for(var i=0;i<fb_work['companies'].length;i++){
			//company_arr.push(fb_work['companies'][i]);
			addCompany(unescape(fb_work['companies'][i]));
		}
		}
	}
	if(edit){
		if(user_data.industry != undefined && user_data.industry != null){
			$("#industry option[value='" + user_data.industry +"']").prop('selected',true);
		}
		checkWork();
	}
}
function prefillInstitute(){
	if(typeof fb_institute != 'undefined' && fb_institute){
		$("#institutes").attr("placeholder", "Add More");
		for(var i=0;i<fb_institute.length;i++){
			//institute_arr.push(fb_institute[i]);
			addInstitute(unescape(fb_institute[i]));
		}
	}
	if(edit)
	if(user_data.highest_degree != undefined && user_data.highest_degree != null){
		$("#education option[value='" + user_data.highest_degree +"']").prop('selected',true);
	}
}

function displayProfiles(profiles) {
	//trackRegistration("linkedin_server");
	trackRegistration('register_basics',"linkedin_popup",'success',false);
	var current=false,member=profiles.values[0].positions.values;
	var num_connections=profiles.values[0].numConnections;
	var firstname=profiles.values[0].firstName;
	var lastname=profiles.values[0].lastName;
	var member_id=profiles.values[0].id;
	var educations = profiles.values[0].educations.values;
	var industry=profiles.values[0].industry;
	var position = "";
	
	if(member!=undefined){
	for(var i=0;i<member.length;i++) {
		//var company_name=member[i].company.name; 
		//addCompany(member[i].company.name);
		if(member[i].isCurrent && !current) {
			current=true;
			position=member[i].title;
			break;
		}
	}
	}
	
	
	
	$("#load_linkedin_date").hide();
//	if(num_connections<verify_connections_linkedin){
//		$('#error_linkedin').html("<p>Sorry You don't have enough linkedin connections to be verified</p>");
//		return;
//	}
	time1 = new Date();
	var error=$.ajax({async:false,type:"post",url:baseurl+"/linkedin2.php",data:{member_id:member_id,firstName:firstname,lastName:lastname,numConnections:num_connections,connected_from:linkedConnectedFrom,linkedin_data:profiles.values[0],current_designation:position},success:function(data,status) {
	}}).responseText;
	var error=JSON.parse(error);
	if(error.responseCode!="200")
	{
		trackRegistration('register_basics',"linkedin_server_call",error.error,false);
		$('#error_linkedin').html("<p>"+error.error+"</p>");
		$('#linkmsg').hide();
		return;
	}else{
		trackRegistration('register_basics',"linkedin_server_call",'success',false);
		$('#error_linkedin').html("<p>Data Imported Successfully</p>");
		$('#linkmsg').hide();
	}
	
	if(member!=undefined){
		if(member.length>0){
			company_arr.length=0;           //empty the company array to remove facebook imported companies if any
			$("#company").empty();
		}
		for(var i=0;i<member.length;i++) {
			//var company_name=member[i].company.name; 
			addCompany(member[i].company.name);
			if(member[i].isCurrent && !current) {
				current=true;
				var position=member[i].title;
				//break;
			}
		}
	}
	
	if(educations!='' || educations!=undefined){
		if(educations.length>0){
			institute_arr.length=0;
			$("#institutes").empty();
		}
	//if(institute_arr.length===0){
		for(var i=0;i<educations.length;i++){
			addInstitute(educations[i].schoolName);
		}
	}
	//}
		
	/*if(!current) {
		for(var i=0;i<member.length;i++) { 
			var industry=member[0].company.industry;
			var company_name=member[0].company.name;
			company_arr.push(member[i].company.name);
			addCompany(member[i].company.name);
			var position=member[0].title;

		}
	}*/
		if(industry!='' || industry!=undefined){
			$("#industry option[value='" +json_l[industry]+ "']").prop("selected",true);
			$("#industry").trigger("liszt:updated");
		}
		
	if(current){
		//$("#industry option[value='" +linkedin_industry[industry]+ "']").prop("selected",true);
		//$("#industry option[value='" +json_l[industry]+ "']").prop("selected",true);
		//$("#industry").trigger("liszt:updated");
		$("#designation").val(position);
	    $("input[name='working_area']").removeAttr('checked');
	}
	checkWork();
}

function fetchLinkedIn() {
	$("#linkedin_loader").remove();
//	$('a[id*=li_ui_li_gen_]').html("<p class='linkdinimport'><a><img src='images/register/linkedin_icon.png' align='absmiddle' class='mr5' width='25'/>IMPORT FROM LINKEDIN</a></p>"); 
	$('a[id*=li_ui_li_gen_]').html("<a class='regfbconnect'><i class='icon-linkedin-sign'></i>Import from <strong>Linkedin</strong></a>");
	IN.Event.on(IN, "auth", onLinkedInAuth);
}
function onLinkedInAuth() {
	$("#load_linkedin_date").show();
	//trackRegistration('register_basics',page,'success','');
	time1 = new Date();
	IN.API.Profile("me","url=http://www.linkedin.com/in/jakobheuser").fields("id", "firstName", "lastName","num_connections","email-address","positions","industry").result(displayProfiles);
}



function refresh() {
$("#chop").hide();
document.getElementById('imageDiv').innerHTML='';

if(incomplete) {
document.getElementById('imageDiv').innerHTML="<img  src='"+JSON.parse(profile)+"' id='showImage'>";
document.getElementById('showImage').onload=function() {
var maxWidth=700;
var maxHeight=450;
var width=document.getElementById('showImage').width;
var height=document.getElementById('showImage').height;
if(height > maxHeight)
var ratio = maxHeight / height;
else if(width > maxWidth)
var ratio = maxWidth / width;
height = height * ratio;
width = width * ratio;
document.getElementById('showImage').style.height=height+'px';
document.getElementById('showImage').style.width=width+'px';
$("#crop").click();
}

$(".tmpcaction").show();
$("#save_p").hide();
$("#new_img_cancel").show();

}
else if(photosAll) {
var photos=photosAll;
document.getElementById('testDiv1').innerHTML="";
if($("#profile_star").length>0) {
$("#profile_star").remove();

}
for(var i=0;i<photos.length;i++) {
var appendCode = "<div class='crjs5'>";
if(photos[i].is_profile=='yes') {
	appendCode = appendCode + "<img style='background:#c1215d;' src='"+photos[i].thumbnail+"' id='thumbNail_"+i+"_profile'/><img id='profile_star' class='phprim' src='images/register/pfavbtn.png'/>";
	if(photos[i].status == 'Under Moderation'){
		appendCode = appendCode +'<div id="thumbNailDiv_'+i+'_profile" class="undrmod"><br><br>'+photos[i].status+'</div>'; 
	}
}
else {
	appendCode = appendCode + "<img src='"+photos[i].thumbnail+"' id='thumbNail_"+i+"'/>";
	if(photos[i].status == 'Under Moderation'){
		appendCode = appendCode + '<div id="thumbNailDiv_'+i+'" class="undrmod1"><br><br>'+photos[i].status+'</div>';
	}
}
appendCode = appendCode + "</div>"; 
$("#testDiv1").append(appendCode);

}
}
}


