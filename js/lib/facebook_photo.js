window.log = function () {
    log.history = log.history || [];
    log.history.push(arguments);
    this.console && console.log(Array.prototype.slice.call(arguments))
};
(function (c) {
    var s = function (q, h) {
        var b = c(q);
        b.append(c('<div id="jfmps-inner-header"><div id="jfmps-breadcrumb"><span id="jfmps-default-crumb" class="fl mrg">Facebook Photo Albums:</span><span id="jfmps-photos-list-crumb" class="fr mrg fbbkbtn"></span></div></div><div id="jfmps-album-covers"></div><div id="jfmps-album-photos"></div><div id="jfmps-selected-container"><a class="jfmps-meta fl mrg" id="jfmps-num-selected" href="javascript:void(0);" style="color:#333; font-size:13px;">Selected &nbsp;<span class="fr mrg">)</span><span id="jfmps-selected-count" class="fr mrg">0</span><span class="fr mrg">(</span></a><a class="jfmps-meta fr mrg" id="jfmps-clear-button" href="javascript:void(0);" style="font-size:11px; color:#2f4a7f; padding:2px 0 0 0;">Clear All</a><div class="clb"></div><div id="jfmps-selected-photos"></div></div>'));
        var l =
            c("#jfmps-album-covers", b),
            i = c("#jfmps-album-photos", b),
            m = c("#jfmps-selected-photos", b),
            s = c("#jfmps-selected-count", b),
            n = c("#jfmps-photos-list-crumb", b),
            r = c("#jfmps-crumb-separator", b),
            o = c("#jfmps-clear-button", b),
            d = {
                maxPhotosSelected: 10,
                numAlbumColumns: 5,
                numPhotosColumns: 6,
                imageSubmitButton: c("#jfmps-submit-button"),
                submitCallback: function (a) {
                    alert(a)
                },
                noAlbumImagesText: "You have no images in this album.",
                noAlbumsText: "You do not have any albums.",
                selectedImageCallback: null,
                debug: !1
            }, p, g, f = 0,
            d = c.extend(!0,
                d, h || {}),
            y = function (a) {
                d.debug && log("FB API Response /me/albums:", a);
                if (a.data && 0 < a.data.length) {
                    var a = a.data,
                        e;
                    p = {};
                    g = {};
                    n.hide();
                    r.hide();
                    i.hide();
                    d.imageSubmitButton.bind("click", w);
                    o.bind("click", t);
                    for (e = 0; e < a.length; e++)(1 === (e + 1) % d.numAlbumColumns || 1 === d.numAlbumColumns) && l.append('<div class="image-row" />'), x(a[e])
                } else b.append("<h2>" + d.noAlbumsText + "</h2>")
            }, x = function (a) {
                var e = c('<div id="fb-albumcover-' + a.id + '" class="jfmps-albumcover" />');
                e.data("album_id", a.id);
                var d = c('<div class="jfmps-albumname" />').text(a.name),
                    b = c('<img width="50" src="https://graph.facebook.com/' + a.id + "/picture?access_token=" + FB.getAuthResponse().accessToken + '&amp;type=thumbnail" />');
                e.append(b);
                e.append(d);
                e.bind("click", function (c) {
                    A(c, e, a.name)
                });
                c("div.image-row", l).last().append(e)
            }, A = function (a, c, z) {
                var b = c.data("album_id");
//                n.empty().html("{album_name} [x]".replace("{album_name}", z));
                n.html("&laquo; Back");
                void 0 === p[b] ? FB.api("/" + b + "/photos", function (a) {
                    d.debug && log("FB API Response /" + b + "/photos:", a);
                    p[b] = a.data;
                    u(b)
                }) : u(b)
            }, u = function (a) {
                var a = p[a],
                    e;
                if (0 < a.length)
                    for (e = 0; e < a.length; e++) {
                        1 === (e + 1) % d.numPhotosColumns && i.append('<div class="image-row" />');
                        var b = c('<div id="fb-albumimage-' + a[e].id + '" class="jfmps-albumimage" />');
                        b.data("image_id", a[e].id);
                        var f = c('<img id="fb-check-image-' + a[e].id+'" src="'+cdn+'/images/register/icon_unchecked.png" style="position:relative; left:2px; top:15px; float:left;" /><img width="75" src="https://graph.facebook.com/' + a[e].id + "/picture?access_token=" + FB.getAuthResponse().accessToken + '&amp;type=thumbnail" />');
                        b.append(f);
                        c("div.image-row", i).last().append(b);
                        var chk = 'fb-check-image-' + a[e].id;
                        void 0 !== g[a[e].id] && b.addClass("selected") && $("#"+chk).attr("src",cdn+'/images/register/icon_checked.png');
                        B(b, a[e].images, chk)
                    } else i.append("<h2>" + d.noAlbumImagesText +
                        "</h2>");
                n.bind("click", C).show();
                r.show();
                l.hide();
                i.show()
            }, B = function (a, e,chk) {
                var b = a.data("image_id");
                a.bind("click", function () {
                    if (void 0 === g[b]) {
                        if (f < d.maxPhotosSelected) {
                            g[b] = e;
                            a.addClass("selected");
                            $("#"+chk).attr("src",cdn+'/images/register/icon_checked.png');
                            var i = c("img", a),
                                j = a.data("image_id"),
                                j = c('<li id="selected-' + j + '"/>'),
                                h;
                            0 >= c("ul", m).length ? (h = c("<ul/>"), m.append(h)) : h = c("ul", m);
                            i.clone().appendTo(j);
                            h.append(j);
                            D(j);
                            f += 1;
                            k();
                            null !== d.selectedImageCallback && d.selectedImageCallback()
                        }
                    } else a.removeClass("selected"), delete g[b], i = a.data("image_id"), $("#"+chk).attr("src",cdn+'/images/register/icon_unchecked.png'),
                    j = c("ul", m).eq(0), c("li#selected-" + i, j).remove(), f -= 1, k();
                    v()
                })
            }, D = function (a) {
                var b = c('<span class="jfmps-selected-unselect" style=" background:none;"/>').html('<img src="images/register/fbphotodel.png" style="border:none;"/>');
                b.hide();
                a.css("position", "relative");
                b.bind("click", function (b) {
                    b.preventDefault();
                    b.stopPropagation();
                    b = a.attr("id").split("selected-")[1];
                    d.debug && log("deleted: " + b);
                    c("#fb-albumimage-" + b).removeClass("selected");
//                    $("#fb-check-image-"+b).attr("src",cdn+'/images/register/icon_unchecked.png');
                    delete g[b];
                    a.remove();
                    f -= 1;
                    k()
                });
                a.bind("mouseenter", function () {
                    b.show()
                });
                a.bind("mouseleave", function () {
                    b.hide()
                });
                a.append(b)
            }, C = function () {
                i.empty().hide();
                n.unbind("click").hide();
                r.hide();
                l.show()
            }, w = function () {
                if (0 < f) {
                    var a = 0 < f ? JSON.stringify(g) : !1;
                    d.debug && log(a);
                    d.submitCallback && d.submitCallback(a)
                }
            }, t = function () {
                for (var a in g) g.hasOwnProperty(a) && (delete g[a], c("#fb-albumimage-" + a, b).removeClass("selected"), c("li#selected-" + a, b).remove());
                f = 0;
                k();
                o.hide()
            }, v = function () {
                0 < f ? (d.imageSubmitButton.show(), o.show()) : (d.imageSubmitButton.hide(), o.hide())
            }, k = function () {
                s.html(f + " / " + d.maxPhotosSelected);
                v()
            };
        1 > d.numAlbumColumns || 1 > d.numPhotosColumns ?
            d.debug && log("settings.numAlbumColumns & settings.numPhotosColumns must be greater than 0") : (k(), FB.api("/me/albums", y));
        return {
            clearSelectedImages: function () {
                return t()
            },
            getSelectedImages: function () {
                return 0 < f ? JSON.stringify(g) : !1
            }
        }
    };
    c.fn.jfmps = function (q) {
        return this.each(function () {
            var h = c(this),
                b;
            if (h.data("jfmps")) return h.data("jfmps");
            b = new s(this, q);
            h.data("jfmps", b);
            return this
        })
    }
})(jQuery);