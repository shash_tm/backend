var fb_flag = 0;

function proceed() {
	if (fb_flag == 0) {
		fb_flag = 1;
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				facebook_redirect(response);
			} else if (response.status === 'not_authorized') {
				FB.login(function(response) {
					facebook_redirect(response);
				}, {
					scope : fbScope
				});
			} else {
				FB.login(function(response) {
					if (response.authResponse) {
						facebook_redirect(response);
					}
				}, {
					scope : fbScope
				});
			}
		});
	}
}

function facebook_redirect(response) {
	$("#verify_fb").css("opacity", '0.5');
	$("#loader_fb").show();
	$("#verify_fb2").css("opacity", '0.5');
	$("#loader_fb2").show();
	var accessToken = response.authResponse.accessToken;
	
	$.post("trustbuilder.php", {
		checkFacebook : 'check',
		token : accessToken,
		mcFlag : 'true',
		connected_from : connectedFrom
	}, function(data) {
		if (data.status == 'SUCCESS') {
			$(".fb_verified_no").hide();
			$(".fb_verified").show();
			$("#fb_verified_circle").attr("class", "bluecircle");
			var trustPercent = $("#trust_percent").html().split("%");
			$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(fbScore)) + "%");
			setTrustAngle(parseInt(trustPercent[0]) + parseInt(fbScore));
			var credits = parseInt($("#credits_update").html());
			if(!isNaN(credits))
				{
					$("#credits_update").html(parseInt(credits) + parseInt(fbCredits));
					$("#credits_update_dash").html(parseInt(credits) + parseInt(fbCredits));
				}
			$("#fb_error").hide();
			$("#fbConnections").html(data.connections + " Connections");
		} else if (data.status == 'FAIL') {
			$("#fb_error").html(data.error);
			$("#fb_error").show();
		} else if (data.status == 'REFRESH') {
			FB.init({
				appId : fb_api, // App ID
				channelUrl : cdn + '/templates/channel.html', // Channel File
				oauth : true,
				status : true, // check login status
				cookie : true, // enable cookies to allow the server to access
								// the session
				xfbml : true
			// parse XFBML
			});
			proceed();
		}
		$("#verify_fb").css("opacity", '1');
		$("#loader_fb").hide();
		$("#verify_fb2").css("opacity", '1');
		$("#loader_fb2").hide();
		fb_flag = 0;
	}, "json");
}

var proofFrom = 0;

var globaltype = 0;

function openFile(type) {
	globaltype = type;
	$('#file').click();
}

function uploadFileAddress(){
	var fileSize = $("#file1")[0].files[0].size;
	fileSize = fileSize / 1024;
	var percentPerSec = 100 / (fileSize / 10);
	var percent = 0;
	$("#address_upload_loader_percent").css("width", "0%");
	var interval = setInterval(function() {
		percent = percent + percentPerSec;
		if (percent < 90) {
			$("#address_upload_loader_percent").css("width", percent + "%");
		} else {

		}
	}, 500);
	var file = $("#file1").val();
	var fileName = file.split("\\");
	var form = document.getElementById('upload-doc-form1');
	$("#upload-file-type1").val(2);
		$("#upload_proof_type1").val($("#addressSelectBox").val());
		$("#document_name2").html(fileName[fileName.length - 1]);
		$("#address_upload").hide();
		$("#address_upload_loader1").show();
	form.setAttribute('action', "trustbuilder.php");
	form.submit();

	$("#file_upload_frame1").load(
			function() {
				window.clearInterval(interval);
				var uploadResponse = $.parseJSON($("#file_upload_frame1")
						.contents().text());
				if (uploadResponse.status == 'success') {
					$("#address_upload_loader_percent").css("width", "100%");
						setTimeout(function(){
							$(".address_verified_no").hide();
							$(".address_verified").show();
							$("#address_type").html($("#addressSelectBox").val());
							if(phoneVerified!='active'){
								$("#phone_div").css("border","1px dashed #9fcbfd");
								$("#phone_div").css("background","#d9e9fb");
							}
						},1000);
				} else {
					alert(data.error);
				}
			});
}

function uploadFileEmployment() {
	var fileSize = $("#file2")[0].files[0].size;
	fileSize = fileSize / 1024;
	var percentPerSec = 100 / (fileSize / 10);
	var percent = 0;
	$("#emp_upload_loader_percent").css("width", "0%");
	var interval = setInterval(function() {
		percent = percent + percentPerSec;
		if (percent < 90) {
			$("#emp_upload_loader_percent").css("width", percent + "0%");
		} else {

		}
	}, 500);
	var file = $("#file2").val();
	var fileName = file.split("\\");
	var form = document.getElementById('upload-doc-form2');
	$("#upload-file-type2").val(3);
		$("#upload_proof_type2").val($("#empSelectBox").val());
		$("#document_name3").html(fileName[fileName.length - 1]);
		$("#emp_upload").hide();
		$("#emp_upload_loader1").show();
	form.setAttribute('action', "trustbuilder.php");
	form.submit();

	$("#file_upload_frame2").load(
			function() {
				window.clearInterval(interval);
				var uploadResponse = $.parseJSON($("#file_upload_frame2")
						.contents().text());
				if (uploadResponse.status == 'success') {
						$("#emp_upload_loader_percent").css("width", "100%");
						setTimeout(function(){
							$(".emp_verified_no").hide();
							$(".emp_verified").show();
							$("#emp_type").html($("#empSelectBox").val());
							if(phoneVerified!='active'){
								$("#phone_div").css("border","1px dashed #9fcbfd");
								$("#phone_div").css("background","#d9e9fb");
							}
						},1000);
				} else {
					alert(data.error);
				}
			});
}


function uploadFile() {
	var fileSize = $("#file")[0].files[0].size;
	fileSize = fileSize / 1024;
	var percentPerSec = 100 / (fileSize / 10);
	var percent = 0;
	$("#id_upload_loader_percent").css("width", "0%");
	var interval = setInterval(function() {
		percent = percent + percentPerSec;
		if (percent < 90) {
			$("#id_upload_loader_percent").css("width", percent + "%");
		} else {

		}
	}, 500);
	var file = $("#file").val();
	var fileName = file.split("\\");
	var form = document.getElementById('upload-doc-form');
	$("#upload-file-type").val(1);
		$("#upload_proof_type").val($("#idSelectBox").val());
		$("#document_name1").html(fileName[fileName.length - 1]);
		$("#id_upload").hide();
		$("#id_upload_loader1").show();
	form.setAttribute('action', "trustbuilder.php");
	form.submit();

	$("#file_upload_frame").load(
			function() {
				window.clearInterval(interval);
				var uploadResponse = $.parseJSON($("#file_upload_frame")
						.contents().text());
				if (uploadResponse.status == 'success') {					
						$("#id_upload_loader_percent").css("width", "100%");
						setTimeout(function(){
							$(".id_verified_no").hide();
							$(".id_verified").show();
							$("#id_type").html($("#idSelectBox").val());
						},1000);
				} else {
					alert(data.error);
				}
			});
}

function openWebCam(type) {
	globaltype = type;
	document.getElementById('imageDiv').innerHTML = '';
	$(".difusescreen").show();
	$(".popupframe").show();
	$("#photoUploadButton").hide();
	var sayCheese = new SayCheese('#say-cheese-container');
	sayCheese
			.on(
					'start',
					function() {
						cameraOn = true;
						document.getElementById('imageDiv').innerHTML = "<button id='take-snapshot' class='webcbtncap'>Take snap!</button> <button id='cancel-snapshot' >Cancel</button>";
						$('#take-snapshot').on('click', function(evt) {
							sayCheese.takeSnapshot();
						});
						$("#cancel-snapshot").on('click', function(evt) {
							if (cameraOn == true) {
								cameraOn = false;
								sayCheese.stop();
								$('video').remove();
							}
							closePopUp();
						});

					});

	sayCheese
			.on(
					'error',
					function(error) {
						alert('Sorry,action could not be completed because of some internal error');
						sayCheese.stop();
						$('video').remove();
						cameraOn = false;
						return;
						// handle errors, such as when a user denies the request
						// to use the
						// webcam,
						// or when the getUserMedia API isn't supported
					});

	sayCheese.on('snapshot', function(snapshot) {
		var img = document.createElement('img');
		$(img).on('load', function() {
			sayCheese.stop();
			$('video').remove();
			cameraOn = false;
			document.getElementById('imageDiv').innerHTML = '';
			$('#imageDiv').prepend(img);
			$(".difusescreen").show();
			$(".popupframe").show();
			$("#showImage").css("max-height", "450px");
			$("#showImage").css("max-width", "640px");
			$("#photoUploadButton").show();
		});
		img.src = snapshot.toDataURL('image/png');
		img.id = "showImage";
	});
	sayCheese.start();
}

function uploadImage() {
	var src = document.getElementById('showImage').src;
	var proofType = "";
	if (globaltype == 1) {
		proofType = $("#idSelectBox").val();
	}else if(globaltype == 2){
		proofType = $("#addressSelectBox").val();
	}
	document.getElementById('imageDiv').innerHTML = "<img id='load' src='images/register/loader_photo.gif' class='fl mrg brdr' style='margin-left:35px;' />";
	$("#photoUploadButton").hide();
	$.post("trustbuilder.php", {
		photoUpload : 'yes',
		src : src,
		proofType : proofType,
		type : globaltype
	}, function(data) {
		if (data.status == 'success') {
			if (globaltype == 1) {
				$(".id_verified_no").hide();
				$(".id_verified").show();
			} else if (globaltype == 2) {
				$(".address_verified_no").hide();
				$(".address_verified").show();
			}
		} else {
			alert(data.error);
		}
		closePopUp();
	}, 'json');
}

function closePopUp() {
	$(".difusescreen").hide();
	$(".popupframe").hide();
	$("#verifyPhoto").val("");
	if (cameraOn == true) {
		cameraOn = false;
		sayCheese.stop();
		$('video').remove();
	}
}

function idChange() {

	var value = $("#idSelectBox").val();
	if (value == 0) {
		$("#idVerificationButton").hide();
		$("#idVerificationButton_show").show();
	} else {
		$("#idVerificationButton").show();
		$("#idVerificationButton_show").hide();
	}
}

function addressChange() {
	var value = $("#addressSelectBox").val();
	if (value == 0) {
		$("#addressVerificationButton_show").show();
		$("#address1").hide();
		$("#addressForm").hide();
	} else if(value == 1){
		$("#addressVerificationButton_show").hide();
		$("#address1").hide();
		$("#addressForm").show();
	}else {
		$("#addressVerificationButton_show").hide();
		$("#address1").show();
		$("#addressForm").hide();
	}
}

function empChange() {
	var value = $("#empSelectBox").val();
	if (value == 0) {
		$("#emp1").hide();
		$("#selfemployed_form").hide();
		$("#employed_form").hide();
		$("#empVerificationButton_show").show();
	} else if (value == 1) {
		$("#emp1").hide();
		$("#selfemployed_form").hide();
		$("#employed_form").show();
		$("#empVerificationButton_show").hide();
	} else if (value == 2) {
		$("#emp1").hide();
		$("#selfemployed_form").show();
		$("#employed_form").hide();
		$("#empVerificationButton_show").hide();
	}else {
		$("#emp1").show();
		$("#selfemployed_form").hide();
		$("#employed_form").hide();
		$("#empVerificationButton_show").hide();
	}
}



function setTrustAngle(percent) {
	if(percent > 100){
		percent = 100;
	}
	var angle = percent * 1.8;
	angle = angle - 90;
	angle = angle + "deg";
	$("#pieSlice1 .pie").css("-webkit-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("-moz-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("-o-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("transform", "rotate(" + angle + ")");
}


function changeEmployment() {
	var value = $("#employmentType").val();
	if (value == 0) {
		$("#selfemployed_form").hide();
		$("#employed_form").hide();
	} else if (value == 1) {
		$("#selfemployed_form").hide();
		$("#employed_form").show();
	} else if (value == 2) {
		$("#selfemployed_form").show();
		$("#employed_form").hide();
	}

}

var linked_flag = 0;

function submitLinkedIn(profiles) {
	if (linked_flag == 0) {
		linked_flag = 1;
		$("#linkedInConnect").css("opacity", '0.5');
		$("#loader_linkedIn").show();
		var num_connections = profiles.values[0].numConnections;
		var firstname = profiles.values[0].firstName;
		var lastname = profiles.values[0].lastName;
		var member_id = profiles.values[0].id;
		$.post(baseurl + "/linkedin2.php", {
			member_id : member_id,
			firstName : firstname,
			lastName : lastname,
			numConnections : num_connections,
			connected_from : connectedFrom
		}, function(dataJson, status) {
			var data = JSON.parse(dataJson);
			if (data.error != 'ok') {
				$("#linked_error").html(data.error);
				$("#linked_error").show();
			} else {
				$(".linked_verified_no").hide();
				$(".linked_verified").show();
				$("#linked_verified_circle").attr("class", "bluecircle");
				var trustPercent = $("#trust_percent").html().split("%");
				$("#trust_percent")
						.html((parseInt(trustPercent[0]) + parseInt(linkedScore)) + "%");
				setTrustAngle(parseInt(trustPercent[0]) + parseInt(linkedScore));
				var credits = parseInt($("#credits_update").html());
				if(!isNaN(credits)){
					$("#credits_update").html(parseInt(credits) + parseInt(linkedCredits));
					$("#credits_update_dash").html(parseInt(credits) + parseInt(linkedCredits));
				}
				$("#linked_error").hide();
				$("#linkedinConnections").html(num_connections + " Connections");
			}
			$("#linkedInConnect").css("opacity", '1');
			$("#loader_linkedIn").hide();
			linked_flag = 0;
		});
	}
}

function addressType(type) {
	if (type == 1) {
		$("#address_upload_type").hide();
		$("#address_form_type").show();
	} else {
		$("#address_upload_type").show();
		$("#address_form_type").hide();
	}
}

function empType(type) {
	if (type == 1) {
		$("#emp_upload_type").hide();
		$("#emp_form_type").show();
	} else {
		$("#emp_upload_type").show();
		$("#emp_form_type").hide();
	}
}

function submitAddressForm() {
	var address = $("#address").val();
	var city = $("#city").val();
	var state = $("#state").val();
	var pincode = $("#pincode").val();
	var landmark = $("#landmark").val();
	var flag = 0;
	if (address == "" || address.length == 0) {
		$("#address").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#address").css("border", "1px solid #cecccc");
	}

	if (city == "" || city.length == 0) {
		$("#city").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#city").css("border", "1px solid #cecccc");
	}

	if (state == "" || state.length == 0) {
		$("#state").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#state").css("border", "1px solid #cecccc");
	}

	if (pincode == "" || pincode.length == 0) {
		$("#pincode").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#pincode").css("border", "1px solid #cecccc");
	}

	if (landmark == "" || landmark.length == 0) {
		$("#landmark").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#landmark").css("border", "1px solid #cecccc");
	}

	if (flag == 1) {
		$("#address_form_error").html("Please complete your form");
	} else {
		$.post("trustbuilder.php", {
			action : "addressForm",
			address : address,
			city : city,
			state : state,
			pincode : pincode,
			landmark : landmark
		}, function(data) {
			if (data.status == 'success') {
				$(".address_verified_no").hide();
				$(".address_verified").show();
			} else {
				alert("Some error occured. Please try again");
			}
		}, 'json');
	}
}


function submitEmploymentForm(empType) {
	var flag = 0;
	var name = "";
	var code_type = "";
	var address_desi = "";
	var tenure = "";
	var pincode = "";
	var city = "";
	var state = "";
	if(empType == 1){
		name = $("#cmp_name").val();
		code_type = $("#emp_code").val();
		address_desi = $("#designation").val();
		tenure = $("#tenure").val();
		
		if (name == "" || name.length == 0) {
			$("#cmp_name").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#cmp_name").css("border", "1px solid #cecccc");
		}
		
		if (code_type == "" || code_type.length == 0) {
			$("#emp_code").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#emp_code").css("border", "1px solid #cecccc");
		}
		
		if (address_desi == "" || address_desi.length == 0) {
			$("#designation").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#designation").css("border", "1px solid #cecccc");
		}
		
		if (tenure == "" || tenure.length == 0) {
			$("#tenure").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#tenure").css("border", "1px solid #cecccc");
		}
	}else{
		name = $("#bs_name").val();
		code_type = $("#bs_type").val();
		address_desi = $("#bs_address").val();
		pincode = $("#bs_pincode").val();
		city = $("#bs_city").val();
		state = $("#bs_state").val();
		tenure = $("#bs_tenure").val();
		
		if (pincode == "" || pincode.length == 0) {
			$("#bs_pincode").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_pincode").css("border", "1px solid #cecccc");
		}
		
		if (city == "" || city.length == 0) {
			$("#bs_city").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_city").css("border", "1px solid #cecccc");
		}
		
		if (state == "" || state.length == 0) {
			$("#bs_state").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_state").css("border", "1px solid #cecccc");
		}
		
		
		if (name == "" || name.length == 0) {
			$("#bs_name").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_name").css("border", "1px solid #cecccc");
		}
		
		if (code_type == "" || code_type.length == 0) {
			$("#bs_type").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_type").css("border", "1px solid #cecccc");
		}
		
		if (address_desi == "" || address_desi.length == 0) {
			$("#bs_address").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_address").css("border", "1px solid #cecccc");
		}
		
		if (tenure == "" || tenure.length == 0) {
			$("#bs_tenure").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_tenure").css("border", "1px solid #cecccc");
		}
	}

	if (flag == 1) {
		$("#emp1_form_error").html("Please complete your form");
	}else if(flag == 2){
		$("#emp2_form_error").html("Please complete your form");
	} else {
		$.post("trustbuilder.php", {
			action : "employmentForm",
			name : name,
			city : city,
			state : state,
			pincode : pincode,
			code_type : code_type,
			tenure : tenure,
			address : address_desi
		}, function(data) {
			if (data.status == 'success') {
				$(".emp_verified_no").hide();
				$(".emp_verified").show();
			} else {
				alert("Some error occured. Please try again");
			}
		}, 'json');
	}
}


function submitPhone() {
	var number = $("#phoneNumber").val();
	var verify = isNaN(number);
	if(verify){
		$("#phone_error").html("Please enter a valid 10 digit mobile number.");
		$("#phone_error").show();
		return;
	} if(number.length != 10){
		$("#phone_error").html("Please enter a valid 10 digit mobile number.");
		$("#phone_error").show();
	}else{
		$("#phone_error").hide();
		$.post("trustbuilder.php",{
			action : "numberVerify",
			number : number
		},function(data){
			if(data.status == 'success'){
				$(".phone_verified_no").hide();
				$("#phone_wait_loader").show();
				var interval = null;
				var time = 0;
				interval = setInterval(function() {
					time = time + 10;
					if (time<180) {
						$.post("trustbuilder.php",{
							action:"checkNumber"
							},function(data){
								if(data.status=='active'){
									$("#phone_wait_loader").hide();
									$(".phone_verified").show();
									$("#phone_verified_circle").attr("class", "bluecircle");
									$("#phoneNumberSpan").html(number);
									var trustPercent = $("#trust_percent").html().split("%");
									$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(mobileScore)) + "%");
									setTrustAngle(parseInt(trustPercent[0]) + parseInt(mobileScore));
									var credits = parseInt($("#credits_update").html());
									if(!isNaN(credits)){
										$("#credits_update").html(parseInt(credits) + parseInt(mobileCredits));
										$("#credits_update_dash").html(parseInt(credits) + parseInt(mobileCredits));
									}
									$("#phone_div").css("border","none");
									phoneVerified = "active";
									$(".phoneVerificationFlag").show();
									$(".phoneVerificationFlag_no").hide();
									window.clearInterval(interval);
								}else if(data.status == 'rejected'){
									if(data.number_of_trials==1){
										$(".phone_verified_no").show();
										$("#phone_error").html("Your phone number could not be verified. Please try again.");
										$("#phone_error").show();
									}else{
										$("#phone_error").html("Your phone number could not be verified. Please contact TrulyMadly customer care.");
										$("#phone_error").show();
									}
									$("#phone_wait_loader").hide();
									window.clearInterval(interval);
								}else if(data.status == 'notpick'){
									$(".phone_verified_no").show();
									$("#phone_error").html("Your phone number could not be verified as there was no response.");
									$("#phone_error").show();
									$("#phone_wait_loader").hide();
									window.clearInterval(interval);
								}
							},"json");
					} else {
						window.clearInterval(interval);
						$("#phone_error").html("Your phone number is pending verification.");
						$("#phone_error").show();
						$("#phone_wait_loader").hide();
				}}, 10000);
			}else if(data.status == 'FAIL'){
				$("#phone_error").html(data.error);
				$("#phone_error").show();
			}
		},'json');		
	}
}

