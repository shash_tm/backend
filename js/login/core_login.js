window.fbAsyncInit = function() {
	FB.init({
		appId : fb_api, // App ID
		channelUrl : server + '/templates/channel.html', // Channel File
		oauth : true,
		status : true, // check login status
		cookie : true, // enable cookies to allow the server to access the
						// session
		xfbml : true,
		version: 'v2.0'
	// parse XFBML
	});

    response = {};
    if(navigator.userAgent.match('CriOS')){
    	
    }else{
    	if(!loginViaEmail && !canNot){
    		FB.getLoginStatus(function(response){
    			response = response;
    			if(response.status === 'connected'){
    				//	Trulymadly.login.facebook_redirect(response);
    			}
    		},true);
    	}
    }
};
// Load the SDK asynchronously
(function(d) {
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement('script');
	js.id = id;
	js.async = true;
	//js.src = "//connect.facebook.net/en_US/all.js";
    js.src = "//connect.facebook.net/en_US/sdk.js";
	ref.parentNode.insertBefore(js, ref);
}(document));


Trulymadly.login = function($){
	
	
	$(document).ready(function() {
		if(mobilecheck()){
			source = 'mobile_web';
		}else{
			source = 'web';
		}
		trackLogin("index","page_load");
		ga('send', 'event', 'index', 'page_load',source);
		
		if(window.localStorage){
			window.localStorage['IS_DATA_ATTRIBUTES_DIRTY']="1";
		}
		
		$('.cutomradio').each(function(){
		    var self = $(this),
		      label = self.next(),
		      label_text = label.text();

		    label.remove();
		    self.iCheck({
		      checkboxClass: 'icheckbox_line-blue',
		      radioClass: 'iradio_line-blue',
		      insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
		  });
		
		$('#form1').validate({
			rules : {
				fname : "required",
				password : {
					required : true,
					minlength : 6
				},
				lname : "required",
				password : {
					required : true,
					minlength : 6
				},
				mobn : {
					required : true,
					minlength : 10,
					maxlength : 10,
					digits : true
				},
				gender : "required",
				email : {
					required : true,
					email : true
				},
				termAgree : {
					required : true
				}
			},
			messages : {
				fname : "Please specify your first name",
				lname : "Please specify your last name",
				password : {
					required : "Please specify a password",
					minlength : "Your password must have at least 6 characters"
				},
				passwordAgain : {
					required : "Please retype your password"
				},
				mobn : {
					required : "Please specify a mobile number",
					minlength : "Please enter a valid 10 digit mobile number",
					number : "Please enter a valid 10 digit mobile number",
					maxlength : "Please enter a valid 10 digit mobile number"
				},
				gender : "Please select your gender",
				email : {
					required : "Please specify a valid email address",
					email : "This email addresss is not valid"
				},
				termAgree : {
					required : "Please accept our terms and conditions"
				}
			},

		});
		$('#mobn').keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		
		$('#form2').validate({
			rules : {
				email : {
					required : true,
					email : true
				},
				password : {
					required : true,
					minlength : 6
				}
			},
			messages : {
				email : {
					required : "Please specify a valid email address",
					email : "This email addresss is not valid"
				},
				password : {
					required : "Please specify a password",
					minlength : "Your password must have at least 6 characters"
				}
			},
		});
		
		$('#form3').validate({
			rules : {
				email : {
					required : true,
					email : true
				}
			},
			messages : {
				email : {
					required : "Please specify a valid email address",
					email : "This email addresss is not valid"
				}
			},
		});
		
		var form3 = $('#form3');
		//if(form3.valid()){
			$("#form3").submit(function(e){
				e.preventDefault();
				form3.validate();
				var post_link = server+"/login/resetPass.php";
				if(form3.valid()){
					$.ajax({
						url : post_link,
						data : $("#form3").serialize(),
						success : function(data){
							result = jQuery.parseJSON(data);
							if(result.status=="OK"){
								$("#error").text("");
									document.getElementById("error1").style.display="none";
									document.getElementById("fpsmsg").style.display="block";
									document.getElementById("form3").style.display="none";
							}else{
								document.getElementById("error1").style.display="block";
								document.getElementById("error1").innerHTML=result.error;
								//$("#error").text("Email Address is not verified");
							}
						}
					})
				}
			});
		//}
			
			$("#openPopUp").click(function(e){
				e.preventDefault();
				document.getElementById("linfrm").style.display="block";
				document.getElementById("fpfrm").style.display="none";
			});
			
		if(typeof login_error!= 'undefined' && login_error){
			$("#openPopUp").click();
		}
			
		$("#passwordAgain").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				onContinue($('#form1'));	
			}
		});
		
		$("#password").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				Signin($('#form2'));	
			}
		});
		
		$("#suspend").click(function(e){
			activateUser();
		});
		$("#suspend1").click(function(e){
			activateUser();
		});
		
		if(show_deactivate){
			$("#logform").hide();
			$("#signform").hide();
			$("#diffframe2").show();
			$("#suspendform").show();
		}
	});
	
	function activateUser(){
		var url = baseurl+"/index.php";
		$.ajax({
			  type: "POST",
			  url: url,
			  data: {activate:true},
			  dataType: "JSON",
			  beforeSend: function() { 
				  $("#suspend").hide();
				  $("#loaderSus").show();
			  },
			  success: function(data){
				  if(data.responseCode==403){
					  
				  }else if(data.responseCode==200){
					  window.location=baseurl + "/register.php";
				  }
			  }
			});
	}
	
	
	function checkPwd(){
		var check = checkPasswords();
		if(check){
			$("#error").html('Password do not match');
			$("#error").show();
			return;
		}else{
			$("#error").html('');
		}
	}
	
	function onContinue(form){
		//var baseurl = "http://dev.trulymadly.com/trulymadly";
		
		$("#error").html('');
		var url = baseurl+"/index.php";
		var formData = $(form).serializeArray();
		var check = checkPasswords();
		$(form).validate();
		if(check){
			$("#error").html('Password do not match');
			$("#error").show();
			return;
		}
		if($(form).valid()){
			time = new Date();
			$.ajax({
				  type: "POST",
				  url: url,
				  data: formData,
				  dataType: "JSON",
				  beforeSend: function() { 
					  $("#continue").hide();
					  $("#loaderCon").show();
				  },
				  success: function(data){
					  if(data.responseCode==403){
						  trackLogin("signUp","signup_via_email",data.error);
						  ga('send', 'event', 'signUp', 'signup_via_email',source);
						  $("#error").text(data.error);
						  $("#continue").show();
						  $("#loaderCon").hide();
					  }else if(data.responseCode==200){
						  trackLogin("signUp","signup_via_email","success");
						  ga('send', 'event', 'signUp', 'signup_via_email',source);
						  $("#error").text("");
						  setTimeout(function(){
							  window.location=baseurl + "/register.php";
						  },200);
					  }
				  }
				});
		}
	}
	
	function Signin(form){
		var url = baseurl+"/index.php";
		var formData = $(form).serializeArray();
		$("#error_log").html('');
		if($(form).valid()){
			time = new Date();
			$.ajax({
				  type: "POST",
				  url: url,
				  data: formData,
				  dataType: "JSON",
				  beforeSend: function() { 
					  $("#login").hide();
					  $("#loaderlog").show();
				  },
				  success: function(data){
					  if(data.responseCode==403){
						  trackLogin("login","login_via_email",data.error);
						  ga('send', 'event', 'login', 'login_via_email',source);
						  if(data.status=="blocked"){
							  $("#logform").hide();
							  $("#blockmsg").html(data.error);
						  }else if(data.status=="suspended"){
							  $("#logform").hide();
							  $("#suspendform").show();
						  }
						  else{
							  $("#error_log").html(data.error);
							  $("#login").show();
							  $("#loaderlog").hide();
						  }
					  }else if(data.responseCode==200){
						  trackLogin("login","login_via_email","success");
						  ga('send', 'event', 'login', 'login_via_email',source);
						  setTimeout(function(){
							  window.location=baseurl + "/register.php";
						  },200);
						  $("#error").text("");
					  }
				  }
				});
		}
	}
	
	function registerfb(obj){
		if(obj.id=="fbimage"){
			 category = "signUp";
		}else if(obj.id=="fbimage_log"){
			category = "login";
		}
		ga('send', 'event', category, 'fb_click',source);
		time = new Date();
		trackLogin(category,"fb_click","");
		
		if(navigator.userAgent.match('CriOS')){
			$("#fbimage").hide();
			  $("#fbimage_log").hide();
			  $("#loaderfb").show();
			  $("#loaderfb_login").show();
			  $("#fb1").hide();
			  $("#fb2").hide();
			 window.location = 'https://www.facebook.com/dialog/oauth?client_id='+fb_api+'&redirect_uri='+baseurl+'/facebook_response.php&response_type=token&scope='+fb_scopes;
	    }else{
	    	if (response.status === 'connected') {
	    		facebook_redirect(response);
	    	} else if (response.status === 'not_authorized') {
	    		FB.login(function(response) {
	    			facebook_redirect(response);
	    		}, {
	    			scope : fb_scopes
	    		});
	    	} else {
	    		FB.login(function(response) {
	    			if (response.authResponse) {
		    			facebook_redirect(response);
		    		}else{
		    			trackLogin(category,"fb_popup_cancel",'');
						ga('send', 'event', category, 'fb_popup_cancel',source);
		    		}
		    		}, {
		    			scope : fb_scopes
		    		});
		    }
	    }
	}
	
	function checkPasswords() {
		var value1 = $('#password1').val();
		var value2 = $('#passwordAgain').val();
		if (value1 != value2) {
			document.getElementById('passwordAgain').value = '';
			return true;
		}else return false;

	}
	
	function facebook_redirect(response){
		trackLogin(category,"fb_popup","success");
		ga('send', 'event', category, 'fb_popup',source);
		time  = new Date();
		var accessToken = response.authResponse.accessToken;
		$.ajax({
			  type: "POST",
			  url: server+"/index.php",
			  data: {from_fb:true,token:accessToken},
			  dataType: "JSON",
			  beforeSend: function() { 
				  if(category=='signUp'){
					  $("#fbimage").hide();
					  $("#loaderfb").show();
					  $("#fb1").hide();
				  }if(category=='login'){
					  $("#fbimage_log").hide();
					  $("#loaderfb_login").show();
					  $("#fb2").hide();
				  }
			},
			  success: function(data){
				  if(data.responseCode==403){
					  trackLogin(category,"fb_server_call",data.error);
					  ga('send', 'event', category, 'fb_server_call',source);
					  if(data.status=="blocked"){
						  $("#logform").hide();
						  $("#signform").hide();
						  $("#blockmsg").html(data.error);
						  $("#blockmsg1").html(data.error);
					  }else if(data.status=="suspended"){
						  $("#logform").hide();
						  $("#signform").hide();
						  $("#suspendform").show();
						  $("#suspendform1").show();
					  }else{
						  FB.api('/me', {fields: 'last_name'}, function(response) {
							  if(category=='signUp'){
								  $("#error_fb").text(data.error);
								  $("#loaderfb").hide();
								  $("#fbimage").show();
							  }if(category=='login'){
								  $("#error_fb_log").text(data.error);
								  $("#loaderfb_login").hide(); 
								  $("#fbimage_log").show();
							  }
							  $("#fbmsg").hide();
						  });
					  }
				  }else if(data.responseCode==200){
					  $("#error").text("");
					  trackLogin(category,"fb_server_call","success");
					  ga('send', 'event', category, 'fb_server_call',source);
					  setTimeout(function(){
						  window.location=baseurl + "/register.php";
					  },200);
				  }
			  }
			});
		
	}
	
	function trackLogin(activity,event_type,status,user_id){
		
		var timeTaken = (new Date()).getTime() - time.getTime();
		timeTaken = timeTaken/1000;
		var track = new Image();
		track.src=baseurl+"/trk.php?data[activity]="+activity+"&data[event_type]="+event_type+"&data[time_taken]="+timeTaken+"&data[event_status]="+status+"&data[source]=web&rand="+Math.random();
	}
	$(window).load(function() {
		setTimeout(function(){
		    if(performance.timing!=undefined){
		      var t = performance.timing;
		      var dnsLookup = (t.domainLookupEnd-t.domainLookupStart)/1000;
		      var pageload = (t.loadEventEnd-t.navigationStart)/1000;
		      var data = {activity:"index",event_type:"dns_lookup",time_taken:dnsLookup};
		      var track = new Image();
		  	  track.src=baseurl+"/trk.php?data[activity]=index&data[event_type]=dns_lookup&data[time_taken]="+dnsLookup+"&data[source]=web&rand="+Math.random();
		    }
		  },0);
	});
	return{
		onContinue:onContinue,
		registerfb:registerfb,
		facebook_redirect:facebook_redirect,
		Signin:Signin,
		checkPwd:checkPwd
	}
	
}(jQuery);