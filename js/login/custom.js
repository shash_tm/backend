$(document).ready(function() {
	
<!-- top menu -->

	$("#headertop .mainmenu a.menu-link").click(function(e) {
	   $("#headertop .mainmenu a.menu-link").toggleClass('menuactive');
       $("#headertop .mainmenu #menu").slideToggle();
	    e.stopPropagation()
    });
	
	$(document).bind('click', function(e) {
	var $clicked = $(e.target);
	if (! $clicked.parents().hasClass("mainmenu"))
		$("#menu").hide();
	if (! $(e.target).hasClass('a.menu-link')) $("a.menu-link").removeClass('menuactive');
	});

<!-- Login popup -->
	
	var overlay = $('<div id="overlay"></div>');
	$('.hide').click(function(){
	$('.loginpopup').hide();
	overlay.appendTo(document.body).remove();
	return false;
	});

	$('#headertop .loginright a').click(function(){
	overlay.show();
	overlay.appendTo(document.body);
	$('.loginpopup').show();
	return false;
	});
	
	$('.hide').click(function(){
	$('.loginpopup').hide();
	overlay.appendTo(document.body).remove();
	return false;
	});
	
<!-- Goto top-->
	
	$("#goto-top").hide();
	$(function () {
	$(window).scroll(function () {
	if ($(this).scrollTop() > 100) {
	$('#goto-top').fadeIn();
	} else {
	$('#goto-top').fadeOut();
	}
	});
	
	$('#goto-top').click(function () {
	$('body,html').animate({
	scrollTop: 0
	}, 800);
	return false;
	});
	});
	
});



	
