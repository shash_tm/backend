window.fbAsyncInit = function() {
	FB.init({
		appId : fb_api, // App ID
		channelUrl : server + '/templates/channel.html', // Channel File
		oauth : true,
		status : true, // check login status
		cookie : true, // enable cookies to allow the server to access the
						// session
		xfbml : true
	// parse XFBML
	});

    response = {};
    if(!loginViaEmail && !canNot){
    	FB.getLoginStatus(function(response){
    		response = response;
    		if(response.status === 'connected'){
    			facebook_redirect(response);
    		}
    	},true);
    }
};
// Load the SDK asynchronously
(function(d) {
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement('script');
	js.id = id;
	js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

$(document).ready(function() {
        trackLogin("index");
	$('#form1').validate({
		rules : {
			fname : "required",
//			lname : "required",
			password : {
				required : true,
				minlength : 6
			},
			mobn : {
				required : true,
				minlength : 10,
				maxlength : 10,
				digits : true
			},
			gender : "required",
			email : {
				required : true,
				email : true
			},
			termAgree : {
				required : true
			}
		},
		messages : {
			fname : "Please specify your name",
//			lname : "Please specify your last name",
			password : {
				required : "Please specify a password",
				minlength : "Your password must have at least 6 characters"
			},
			passwordAgain : {
				required : "Please retype your password"
			},
			mobn : {
				required : "Please specify a mobile number",
				minlength : "Please enter a valid 10 digit mobile number",
				number : "Please enter a valid 10 digit mobile number",
				maxlength : "Please enter a valid 10 digit mobile number"
			},
			gender : "Please select your gender",
			email : {
				required : "Please specify a valid email address",
				email : "This email addresss is not valid"
			},
			termAgree : {
				required : "Please accept our terms and conditions"
			}
		},

	});
	$('#mobn').keyup(function () { 
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	
/*	$('#fname,#lname').keydown(function (e) {
		if (e.shiftKey || e.ctrlKey || e.altKey) {
			e.preventDefault();
		}
		else {
			var key = e.keyCode;
			if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
				e.preventDefault();
			}
		}
	});
	*/
	$('#form2').validate({
		rules : {
			email : {
				required : true,
				email : true
			},
			password : {
				required : true,
				minlength : 6
			}
		},
		messages : {
			email : {
				required : "Please specify a valid email address",
				email : "This email addresss is not valid"
			},
			password : {
				required : "Please specify a password",
				minlength : "Your password must have at least 6 characters"
			}
		},
	});
	
	$('#form3').validate({
		rules : {
			email : {
				required : true,
				email : true
			}
		},
		messages : {
			email : {
				required : "Please specify a valid email address",
				email : "This email addresss is not valid"
			}
		},
	});
	
	var form3 = $('#form3');
	//if(form3.valid()){
		$("#form3").submit(function(e){
			e.preventDefault();
			form3.validate();
			var post_link = server+"/login/resetPass.php";
			if(form3.valid()){
				trackLogin("forgot_password");
				$.ajax({
					url : post_link,
					data : $("#form3").serialize(),
					success : function(data){
						result = jQuery.parseJSON(data);
						if(result.status=="OK"){
							$("#error").text("");
								document.getElementById("error1").style.display="none";
								document.getElementById("fpsmsg").style.display="block";
								document.getElementById("form3").style.display="none";
						}else{
							document.getElementById("error1").style.display="block";
							document.getElementById("error1").innerHTML=result.status;
							//$("#error").text("Email Address is not verified");
						}
					}
				})
			}
		});
	//}
		
		$("#openPopUp").click(function(e){
			e.preventDefault();
			document.getElementById("linfrm").style.display="block";
			document.getElementById("fpfrm").style.display="none";
		});
		
	if(login_error){
		$("#openPopUp").click();
	}
		
});

function checkPasswords() {
	var value1 = document.getElementById('password1').value;
	var value2 = document.getElementById('password2').value;
	if (value1 != value2) {
		document.getElementById('password2').value = '';
		alert('password do not match');
	}

}

function facebook_redirect(response) {
	var accessToken = response.authResponse.accessToken;
	var form = $("<form id='formX' action=" + server + "/index.php"
			+ " method=post >"
			+ "<input type='text' name='from_fb' value='true'/><br/> "
			+ "<input type='text' name='token' value=" + accessToken
			+ " /></form>");
	$('body').append(form);
	document.getElementById('formX').submit();
}

function proceed(str) {
	
	if (response.status === 'connected') {
		facebook_redirect(response);
	} else if (response.status === 'not_authorized') {
		FB.login(function(response) {
			facebook_redirect(response);
		}, {
			scope : fb_scopes
		});
	} else {
		FB.login(function(response) {
			if (response.authResponse) {
				facebook_redirect(response);
			}
		}, {
			scope : fb_scopes
		});
	}
}

function getCookie(cname)
{
var name = cname + "=";
var ca = document.cookie.split(';');
for(var i=0; i<ca.length; i++) 
  {
  var c = ca[i].trim();
  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
}
return "";
}
function trackLogin(step){
	var session = getCookie('PHPSESSID');	
	var track = new Image();
	track.src=server+"/trk.php?step="+step+"&timeTaken=&status=&uid=&session="+session;
}
