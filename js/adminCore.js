function verifyId(status,userId){
	if(status == 'fail'){
		var a = confirm("Are you sure you want to reject?");
		if(a == false){
			return;
		}
	}
	$.post("adminpanel.php",{
		action:'verify_id',
		status:status,
		userId:userId
	},function(data){
		if(data.error){
			alert(data.error);
		}if(data.status=='Success'){
			$("#button_"+userId).html("Id Status:"+data.action);
		}
	},'json');
}


function verifyAddress(status,userId){
	if(status == 'fail'){
		var a = confirm("Are you sure you want to reject?");
		if(a == false){
			return;
		}
	}
	$.post("adminpanel.php",{
		action:'verify_address',
		status:status,
		userId:userId
	},function(data){
		if(data.error){
			alert(data.error);
		}if(data.status=='Success'){
			$("#button_"+userId).html("Address Status:"+data.action);
		}
	},'json');
}

function verifyEmployment(status,userId){
	if(status == 'fail'){
		var a = confirm("Are you sure you want to reject?");
		if(a == false){
			return;
		}
	}
	$.post("adminpanel.php",{
		action:'verify_employment',
		status:status,
		userId:userId
	},function(data){
		if(data.error){
			alert(data.error);
		}if(data.status=='Success'){
			$("#button_"+userId).html("Employment Status:"+data.action);
		}
	},'json');
}

function verifyPhoto(status,userId, user_id){
	if(status == 'fail'){
		var a = confirm("Are you sure you want to reject?");
		if(a == false){
			return;
		}
	}
	$.post("adminpanel.php",{
		action:'verify_photo',
		status:status,
		userId:userId,
		user_id:user_id
	},function(data){
		//alert(userId);
		if(data.status=='Success'){
			$("#button_"+userId).html("Photo Status:"+status);
		}
	},'json');
}

function sendForVerify(id, type){
	var ref = $("#auth_"+id).val();
	$.post("adminpanel.php",{
		action:'authbridge_verification',
		user_id:id,
		type: type,
		ref: ref
	},function(data){
		//alert(userId);
		if(data.status=='Success'){
			$("#authbridge_"+id).html("Sent for verification");
		}
	},'json');
}

function isPaid(id,type){
	$.post("adminpanel.php",{
		action:'paid',
		user_id:id,
		type:type
	},function(data){
		if(data.status=='Success'){
			$("#paid_"+id).html("paid : yes");
			$("#paid123").hide();
		}
	},'json');
}