var fb_flag = 0;

function proceed() {
	if (fb_flag == 0) {
		fb_flag = 1;
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				facebook_redirect(response);
			} else if (response.status === 'not_authorized') {
				FB.login(function(response) {
					facebook_redirect(response);
				}, {
					scope : fbScope
				});
			} else {
				FB.login(function(response) {
					if (response.authResponse) {
						facebook_redirect(response);
					}
				}, {
					scope : fbScope
				});
			}
		});
	}
}

function facebook_redirect(response) {
	$("#verify_fb").css("opacity", '0.5');
	$("#loader_fb").show();
	$("#verify_fb2").css("opacity", '0.5');
	$("#loader_fb2").show();
	var accessToken = response.authResponse.accessToken;
	$.post("trustbuilder.php", {
		checkFacebook : 'check',
		token : accessToken
	}, function(data) {
		if (data.status == 'SUCCESS') {
			$(".fb_verified_no").hide();
			$(".fb_verified").show();
			$("#fb_verified_circle").attr("class", "bluecircle");
			var trustPercent = $("#trust_percent").html().split("%");
			$("#trust_percent").html((parseInt(trustPercent[0]) + 25) + "%");
			setTrustAngle(parseInt(trustPercent[0]) + 25);
			var credits = $("#credits_update").html();
			$("#credits_update").html(parseInt(credits) + 25);
			$("#fb_error").hide();
		} else if(data.status=='FAIL') {
			$("#fb_error").html(data.error);
			$("#fb_error").show();
		}else if(data.status=='REFRESH'){
			FB.init({
				appId      : fb_api, // App ID
				channelUrl : cdn + '/templates/channel.html', // Channel File
				oauth      : true,
				status     : true, // check login status
				cookie     : true, // enable cookies to allow the server to access the session
				xfbml      : true  // parse XFBML
				});
				proceed();
			}
		$("#verify_fb").css("opacity", '1');
		$("#loader_fb").hide();
		$("#verify_fb2").css("opacity", '1');
		$("#loader_fb2").hide();
		fb_flag = 0;
	}, "json");
}

var proofFrom = 0;

var globaltype = 0;

function openFile(type) {
	globaltype = type;
	$('#file').click();
}

function uploadFile() {
	var file = $("#file").val();
	var fileName = file.split("\\");
	var form = document.getElementById('upload-doc-form');
	$("#upload-file-type").val(globaltype);
	if (globaltype == 1) {
		$("#upload_proof_type").val($("#idSelectBox").val());
		$("#document_name1").html(fileName[fileName.length - 1]);
		$("#id_upload").hide();
		$("#id_upload_loader1").show();
	}else if(globaltype == 2){
		$("#upload_proof_type").val($("#addressSelectBox").val());
		$("#document_name2").html(fileName[fileName.length - 1]);
		$("#address_upload").hide();
		$("#address_upload_loader1").show();
	}
	form.setAttribute('action', "trustbuilder.php");
	form.submit();

	$("#file_upload_frame").load(
			function() {
				var uploadResponse = $.parseJSON($("#file_upload_frame")
						.contents().text());
				if (uploadResponse.status == 'success') {
					if (globaltype == 1) {
						$(".id_verified_no").hide();
						$(".id_verified").show();
					}else if(globaltype == 2){
						$(".address_verified_no").hide();
						$(".address_verified").show();
					}
				} else {
					alert(data.error);
				}
			});
}

function openWebCam(type) {
	globaltype = type;
	document.getElementById('imageDiv').innerHTML = '';
	$(".difusescreen").show();
	$(".popupframe").show();
	$("#photoUploadButton").hide();
	var sayCheese = new SayCheese('#say-cheese-container');
	sayCheese
			.on(
					'start',
					function() {
						cameraOn = true;
						document.getElementById('imageDiv').innerHTML = "<button id='take-snapshot' class='webcbtncap'>Take snap!</button> <button id='cancel-snapshot' >Cancel</button>";
						$('#take-snapshot').on('click', function(evt) {
							sayCheese.takeSnapshot();
						});
						$("#cancel-snapshot").on('click', function(evt) {
							if (cameraOn == true) {
								cameraOn = false;
								sayCheese.stop();
								$('video').remove();
							}
							closePopUp();
						});

					});

	sayCheese
			.on(
					'error',
					function(error) {
						alert('Sorry,action could not be completed because of some internal error');
						sayCheese.stop();
						$('video').remove();
						cameraOn = false;
						return;
						// handle errors, such as when a user denies the request
						// to use the
						// webcam,
						// or when the getUserMedia API isn't supported
					});

	sayCheese.on('snapshot', function(snapshot) {
		var img = document.createElement('img');
		$(img).on('load', function() {
			sayCheese.stop();
			$('video').remove();
			cameraOn = false;
			document.getElementById('imageDiv').innerHTML = '';
			$('#imageDiv').prepend(img);
			$(".difusescreen").show();
			$(".popupframe").show();
			$("#showImage").css("max-height", "450px");
			$("#showImage").css("max-width", "640px");
			$("#photoUploadButton").show();
		});
		img.src = snapshot.toDataURL('image/png');
		img.id = "showImage";
	});
	sayCheese.start();
}

function uploadImage() {
	var src = document.getElementById('showImage').src;
	var proofType = "";
	if (globaltype == 1) {
		proofType = $("#idSelectBox").val();
	}
	document.getElementById('imageDiv').innerHTML = "<img id='load' src='images/register/loader_photo.gif' class='fl mrg brdr' style='margin-left:35px;' />";
	$("#photoUploadButton").hide();
	$.post("trustbuilder.php", {
		photoUpload : 'yes',
		src : src,
		proofType : proofType,
		type : globaltype
	}, function(data) {
		if (data.status == 'success') {
			$(".id_verified_no").hide();
			$(".id_verified").show();
		} else {
			alert(data.error);
		}
		closePopUp();
	}, 'json');
}

function closePopUp() {
	$(".difusescreen").hide();
	$(".popupframe").hide();
	$("#verifyPhoto").val("");
	if (cameraOn == true) {
		cameraOn = false;
		sayCheese.stop();
		$('video').remove();
	}
}

function idChange() {

	var value = $("#idSelectBox").val();
	if (value == 0) {
		$("#idVerificationButton").hide();
		$("#idVerificationButton_show").show();
	} else {
		$("#idVerificationButton").show();
		$("#idVerificationButton_show").hide();
	}
}

function addressChange() {
	var value = $("#addressSelectBox").val();
	if (value == 0) {
		$("#address1").hide();
	} else {
		$("#address1").show();
	}
}

function setTrustAngle(percent) {
	var angle = percent * 1.8;
	angle = angle - 90;
	angle = angle + "deg";
	$("#pieSlice1 .pie").css("-webkit-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("-moz-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("-o-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("transform", "rotate(" + angle + ")");
}

function becomeUnlimitedMember() {
	$.post("trustbuilder.php", {
		unlimited : 'yes'
	}, function(data) {
		if (data.status == 'success') {
			location.reload();
		} else {
			alert(data.error);
		}
	}, 'json');
}

function changeEmployment() {
	var value = $("#employmentType").val();
	if (value == 0) {
		$("#selfemployed_form").hide();
		$("#employed_form").hide();
	} else if (value == 1) {
		$("#selfemployed_form").hide();
		$("#employed_form").show();
	} else if (value == 2) {
		$("#selfemployed_form").show();
		$("#employed_form").hide();
	}

}

var linked_flag = 0;

function submitLinkedIn(profiles) {
	if (linked_flag == 0) {
		linked_flag = 1;
		$("#linkedInConnect").css("opacity", '0.5');
		$("#loader_linkedIn").show();
		var num_connections=profiles.values[0].numConnections;
		var firstname=profiles.values[0].firstName;
		var lastname=profiles.values[0].lastName;
		var member_id=profiles.values[0].id;		
		$.post(cdn + "/linkedin2.php", {
			member_id:member_id,
			firstName:firstname,
			lastName:lastname,
			numConnections:num_connections
		}, function(dataJson, status) {
			var data = JSON.parse(dataJson);
			if (data.error != 'ok') {
					$("#linked_error").html(data.error);
					$("#linked_error").show();
			} else {
				$(".linked_verified_no").hide();
				$(".linked_verified").show();
				$("#linked_verified_circle").attr("class", "bluecircle");
				var trustPercent = $("#trust_percent").html().split("%");
				$("#trust_percent")
						.html((parseInt(trustPercent[0]) + 10) + "%");
				setTrustAngle(parseInt(trustPercent[0]) + 10);
				var credits = $("#credits_update").html();
				$("#credits_update").html(parseInt(credits) + 10);
				$("#linked_error").hide();
			}
			$("#linkedInConnect").css("opacity", '1');
			$("#loader_linkedIn").hide();
			linked_flag = 0;
		});
	}
}