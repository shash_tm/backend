var geoData = "";
map = null;

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	var readOnly = $('#read-only-val').val();
	if(readOnly === "true"){
		$('#datespot-form input').attr('readonly', 'readonly');
		$('#datespot-form .btn-group').hide();
		$('#datespot-form .caption').hide();
		$('#datespot-form .glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-star');
		$('#datespot-form .remove-text-input').removeClass('remove-text-input');
		$('#datespot-form .hashtag-remove').remove();
	//	$('#datespot-form .input-group-btn').hide();
		
	}
			//Following html variable is used to add menu image and deal image url.
	var image_field='<div class="form-group text-input-fld">'+
						'<div class="input-group">'+
							'<input type="text" class="form-control IMAGE_TYPE_CLASS" placeholder="Enter New Value">'+
								'<span class="input-group-btn">'+
									'<button class="btn btn-default  remove-text-input" type="button">'+
										'<span class="glyphicon glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>'+
									'</button>'+
								'</span>'+
							'</input>'+
						'</div>'+
					'</div>';
					
	$('#add-menu-img-button').click(function(event){
		$(image_field.replace('IMAGE_TYPE_CLASS', 'menu-image-url-val')).insertBefore('#add-menu-img-button-group');
	});
	
	$('#add-deal-img-button').click(function(event){
		$(image_field.replace('IMAGE_TYPE_CLASS', 'deal-image-url-val')).insertBefore('#add-deal-img-button-group');
	});
	
	$('#add-tnc-button').click(function(event){
		$(image_field.replace('IMAGE_TYPE_CLASS', 'tnc_text')).insertBefore('#add-tnc-button-group');
	});
	
	$('.remove-image-button').click(function(event){
		event.preventDefault();
		removeImageButton($(this));
	});
	
	$('#state-select').change(function(event){
		var stateId = $(this).val();
		var currentCity = $('#select-city-id').val();
		var currentZone = $('#select-zone-id').val();
		$('#city-select').html('');
		$('.field_name_zone_id').each(function() {
			$(this).html('');
		})
    	$.each( geoData, function( key, val ) {
		    if(key==='cities'){
		    	$('#city-select').append('<option value="0">===Select City===</option>');
		    	$.each( val, function( skey, sval ) {
		    		if(sval.state_id == stateId)
		    			$('#city-select').append('<option value="'+sval.city_id+'">'+sval.name+'</option>');
		    	});
		    	$('#city-select').val(currentCity);
		    }

			if(key==='zones' && stateId == '2168'){
				$('.field_name_zone_id').each(function(){
					$(this).append('<option value="0">===Zone===</option>');
					var selectObj = $(this);
					$.each( val, function( skey, sval ) {
						if(sval.state_id == stateId)
							selectObj.append('<option value="'+sval.zone_id+'">'+sval.name+'</option>');
					});
					selectObj.val(selectObj.siblings('.zone-val').data('zone-value'));		//Yes, that's how i traverse.
				})

			}
    	});
		$('#city-select').trigger('change');
	})

	$('#city-select').change(function(event){
		var cityId = $(this).val();
		var stateId = $('#state-select').val();
		if(stateId == '2168')
			return false;

		var currentZone = $('#select-zone-id').val();
		$('.field_name_zone_id').each(function() {
			$(this).html('');
		});

		$.each( geoData, function( key, val ) {
			if(key==='zones'){
				$('.field_name_zone_id').each(function() {
					var zoneSelectObj = $(this);
					zoneSelectObj.append('<option value="0">===Select Zone===</option>');
					$.each(val, function (skey, sval) {
						if (sval.city_id == cityId)
							zoneSelectObj.append('<option value="' + sval.zone_id + '">' + sval.name + '</option>');
					});
					zoneSelectObj.val(zoneSelectObj.siblings('.zone-val').data('zone-value'));
					//$('#city-select').trigger('change');
				})
			}
		});
	})

	$('.hashtag-remove').click(function(event){
		$(this).parent('.hashtag-group').remove();
	})
	
	
	$('#save-form-action').click(function(event){
		$(this).hide();
		event.preventDefault();
		var dealImgUrl = $('#deal-image-new').val();
		console.log("deal-view-im  "+dealImgUrl);
		if(dealImgUrl != undefined && dealImgUrl != null && dealImgUrl.length > 0){
			$('#list-view-image').val(dealImgUrl);
		}else{
			$('#list-view-image').val($('#deal-image').attr('src'));
		}
			
		
		var imagesJson='[';
		$('.deal-image').each(function() {
		    imagesJson+= '"'+$(this).attr('src')+'",';
		});
		$('.deal-image-url-val').each(function() {
			imagesJson+= '"'+$(this).val()+'",'
		});
		imagesJson += ']';
		imagesJson = imagesJson.replace('",]', '"]');
		$('#images-json').val(imagesJson);
		
		
		var menuImagesJson='[';
		$('.menu-image').each(function() {
			menuImagesJson+= '"'+$(this).attr('src')+'",';
		});
		$('.menu-image-url-val').each(function() {
			menuImagesJson+= '"'+$(this).val()+'",'
		});
		menuImagesJson += ']';
		menuImagesJson = menuImagesJson.replace('",]', '"]');
		$('#menu-images-json').val(menuImagesJson);
		
		
		var hashtagJson='[';
		$('.hashtag-val').each(function() {
			hashtagJson+= '"'+$(this).text()+'",';
		});
		
		var newHashtagVal = $('#hashtag-new').val();
		var hashArray = newHashtagVal.replace(/ /g,'').split(",");
		for(var i=0;i<hashArray.length;i++){
			if (hashArray[i].replace(/\s/g, '').length) {
				hashtagJson+= '"'+hashArray[i]+'",';
			}
		}
		
		hashtagJson += ']';
		hashtagJson = hashtagJson.replace('",]', '"]');
		$('#hashtag-json').val(hashtagJson);
		
		
		var tncJson='[';
		$('.tnc_text').each(function() {
			tncJson+= '"'+$(this).val()+'",';
		});
		tncJson += ']';
		tncJson = tncJson.replace('",]', '"]');
		$('#t-n-c').val(tncJson);

		var coupons = $('#coupon-code').val();
		$('#coupon-code').val(coupons.replace(/ /g,''));

		var multipleCouponVal = $('#multiple-coupon').prop('checked') ? 1:0;
		$('#multiple-coupon').val(multipleCouponVal);
		
		var formJson = $('#datespot-form').serializeArray();
		
		var jsonObj = convertToJson(formJson);
		jsonObj.locations_json = getLocationJson();
		jsonObj.location_count = getLocationCount();
		jsonObj.multiple_coupon = multipleCouponVal;
		submitForm(jsonObj);
	})

	$('#add-location-button').click(function(event){
		addLocationHtml();
	})

	
	$.getJSON($('#base-url').val()+'/register_data.json', function( data ) {
	  	geoData = data;
		$.getJSON($('#base-url').val()+'/dealAdmin/zoneData.php', function( zoneData ) {
			geoData.zones = zoneData;
			var currentState = $('#select-state-id').val();
			$.each(geoData, function (key, val) {
				if (key === 'states') {
					$.each(val, function (skey, sval) {
						$('#state-select').append('<option value="' + sval.key + '">' + sval.value + '</option>');
					});
					$('#state-select').val(currentState);
					$('#state-select').trigger('change');
				}
			});
		});
	});
	
	$('#amount-type').val($('#select-amount-type').val());
	$('#deal-status').val($('#select-status').val());
	$('#deal-type').val($('#select-deal-type').val());
	$('#pricing-symbol').val($('#select-pricing-symbol').val());
});	

$(document).on("click", ".remove-text-input", function(event){
	event.preventDefault();
	removeTextInput($(this));
});

$(document).on("click", ".location-remove", function(event){
	event.preventDefault();
	$(this).closest('.new-location-obj').remove();
});

$(document).on("click", "#show-map", function(event){
	setTimeout(function(){ google.maps.event.trigger(map, "resize"); }, 1000);

});

function removeImageButton(removeBtn){
	removeBtn.parents('.image-box').remove();
}

function removeTextInput(removeBtn){
	removeBtn.parents('.text-input-fld').remove();
}


function convertToJson(a){
	var o = {};
	$.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}

function showZoneModal(){
	$('#yes-button').show();
	$('#cancel-button').show();
	$('#zone-new').val('');
	$('#zone-success').hide();
}

function addNewZone(){
	$('#yes-button').hide();
	$('#cancel-button').hide();
	$('#show-wait').show();
	var zoneName = $('#zone-new').val();
	var stateId = $('#state-select').val();
	var cityId = $('#city-select').val();
	var actionUrl = $('#zone-form').attr('action');
	console.log("hitting"+actionUrl);
	$.ajax({
		url: actionUrl,
		type: "POST",
		// contentType: "application/json",
		data: {'zoneName':zoneName,'stateId':stateId,'cityId':cityId},
		success: function(result){
			result = JSON.parse(result);
			console.log(result);
			if(result.status == "success"){
				geoData["zones"].push(result.zone_data);
				$('#state-select').trigger('change');
				$('#show-wait').hide();
				$('#zone-success').show();
			//	location.reload();
			}
		},
		error: function(result){
			console.log('error');
			console.log(result);
		}
	});

}

function addLocationHtml(){
	var htmlText = $('#location-body').html();
	htmlText = '<div class="new-location-obj">'+htmlText+'</div>';
	var htmlObj = $('#new-location').append($.parseHTML(htmlText));
	htmlObj = $('.new-location-obj').last();
	var textChildren = htmlObj.find('input');
	textChildren.push(htmlObj.find('select'));
	$.each(textChildren, function(){
		var nameVal = $(this).attr('name');
		$(this).attr('value','');
		$(this).attr('name','');
		$(this).attr('id','');
		if(nameVal) {
			nameVal = "field_name_" + nameVal;
			$(this).addClass(nameVal);
		}
	});
	htmlObj.find('.remove-button-div').attr('style','');

}

function getLocationCount(){
	var locations = $('.new-location-obj');
	var location_count=1;
	locations.each(function()
	{
		location_count++;
	})
	console.log(location_count);
	return location_count;
}

function getLocationJson(){
	var jsonData = {
		locations : []
	};
	var locations = $('.new-location-obj');
	locations.push($('#location-body'));
	locations.each(function()
	{
		var locData = {};
		var textChildren = $(this).find('input');
		textChildren.push($(this).find('select'));
		var location_count=0;
		$.each(textChildren, function(){
			var classArray = $(this).attr('class').split(' ');
			var key;
			for(var i=0;i<classArray.length;i++){
				if(classArray[i].startsWith('field_name_'))
					key = classArray[i].substring(11);
			}
			locData['city_id']=$('#city-select').val();
			locData['state_id']=$('#state-select').val();
			locData[key] = $(this).val();
		});
		jsonData.locations.push(locData);
	});
	return JSON.stringify(jsonData);
}

function submitForm(jsonObj){
	var actionUrl=$('#datespot-form').attr('action');
//	console.log(jsonObj);
	  $.ajax({
		 url: actionUrl, 
		 type: "POST",
		// contentType: "application/json",
		 data: jsonObj,
		 success: function(result){
			 result = JSON.parse(result);
			// console.log(result);
			 if(result.status == "success"){
				 if(result.new_ds != undefined && result.new_ds=='true'){
				//	 console.log('redirecting  '+$('#base-url').val()+'?datespot_id='+result.datespot_id);
					 window.location = $('#base-url').val()+'/dealAdmin/dealView.php?datespot_id='+result.datespot_id;
				 }else{
					 location.reload();
				 }
			 }
	    },
	    error: function(result){
	    	console.log('error');
	    	console.log(result);
	    }
	 }); 
}

function htmlspecialchars(str) {
	if (typeof(str) == "string") {
		str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
		str = str.replace(/"/g, "&quot;");
		str = str.replace(/'/g, "&#039;");
		str = str.replace(/</g, "&lt;");
		str = str.replace(/>/g, "&gt;");
	}
	return str;
}

// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initAutocomplete() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 28.523263618013818, lng: 77.19524681568146},
		zoom: 13,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});

	var markers = [];
	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Clear out the old markers.
		markers.forEach(function(marker) {
			marker.setMap(null);
		});
		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				icon: icon,
				title: place.name,
				position: place.geometry.location
			}));

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});
		map.fitBounds(bounds);
	});

	google.maps.event.addListener(map, "click", function (event) {
		var latitude = event.latLng.lat();
		var longitude = event.latLng.lng();
		var locBox =  $('#geo-loc');
		var flag=0;
		$('.field_name_geo_location').each(function(){
			if(!$(this).val() ) {
				locBox = $(this);
				locBox.val(latitude+","+longitude);
				flag ++;
				return false;
			}
		});
		if(flag === 0 )
			locBox.val(latitude+","+longitude);
	});
}
