function logEvent(matchObj, skipGA){
	//console.log('log call');
	var track = new Image();
	var admin_id = getCookie("admin_id");
	track.src = baseurl + "/trk.php?data[activity]="
			+ matchObj['data']['activity'] + "&data[event_type]="
			+ matchObj['data']['event_type'] + "&data[time_taken]="
			+ matchObj['data']['time_taken'] + "&data[user_id]="
			+ matchObj['data']['user_id'] + "&data[source]="
			+ matchObj['data']['source'] + "&data[event_info]="
			+ encodeURIComponent(JSON.stringify(matchObj['data']['event_info']))
			+ "&data[admin_id]=" + admin_id + "&time=" + Date.now();
	
	if(!skipGA)
	logGA(matchObj);

}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function logGA(matchObj) {
	//console.log("log GA");
	ga_tm( matchObj['data']['activity'],
			matchObj['data']['event_type']);
}

$(document).on('contextmenu', "img", function (e) {
//	  e.preventDefault();
	  //alert('Right Click is not allowed');  
return false;	
});