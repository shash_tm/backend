window.fbAsyncInit = function() {
  FB.init({
    appId      : fb_api, // App ID
    channelUrl : server + '/templates/channel.html', // Channel File
    oauth      : true,
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true,  // parse XFBML
    version    : 'v2.0'   // fb sdk version
          }); 
  };
  
  (function(d, s, id){
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) {
	        return;
	    }
	    js = d.createElement(s);
	    js.id = id;
	    //js.src = "//connect.facebook.net/en_US/all.js";
	    js.src = "//connect.facebook.net/en_US/sdk.js";
	    fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));



var fb_flag = 0;

function proceed(reimport) {
	time1 = new Date();
	trackRegistration(activity,'fb_click','',false);
	time1 = new Date();
	//if (fb_flag == 0) {
		//fb_flag = 1;
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				facebook_redirect(response, reimport);
			} else if (response.status === 'not_authorized') {
				FB.login(function(response) {
					facebook_redirect(response);
				}, {
					scope : fbScope
				});
			} else {
				FB.login(function(response) {
					if (response.authResponse) {
						facebook_redirect(response,reimport);
					}else{
						trackRegistration(activity,'fb_popup_cancel','',false);
					}
				}, {
					scope : fbScope
				});
			}
		});
	//}
}

function facebook_redirect(response, reimport) {
	hidepopup();
	$("#verify_fb").hide();
	$("#loader_fb").show();
	$("#loader_fb2").show();
	$("#sync_fb").hide();
	trackRegistration(activity,'fb_popup','success',false);
	var accessToken = response.authResponse.accessToken;
	time1 = new Date();
	trackRegistration(activity,'fb_server_call_start','success',false);
	$.post("trustbuilder.php", {
		checkFacebook : 'check',
		token : accessToken,
		mcFlag : 'true',
		connected_from : connectedFrom,
		reimport : reimport
	}, function(data) {
		if (data.status == 'success') {
			trackRegistration(activity,'fb_server_call','success',false);
			if(isFbVerified!="1"){
			var trustPercent = $("#trust_percent").html().split("%");
			$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(fbScore)) + "%");
			setTrustAngle(parseInt(trustPercent[0]) + parseInt(fbScore));
			var credits = parseInt($("#credits_update").html());
			if(!isNaN(credits))
				{
					$("#credits_update").html(parseInt(credits) + parseInt(fbCredits));
					$("#credits_update_dash").html(parseInt(credits) + parseInt(fbCredits));
				}
			}
			$("#fb_connections").html('');
			$("#fb_error").hide();
			$("#fb_connections").html(data.connections + " Connections");
			fbVerified(data.connections);
			isFbVerified = "1";
		} else if (data.status == 'fail') {
			if(reimport != undefined){
				$("#fb_error").html("");
				$("#fb_error").html("<i class='icon-warning-sign'></i> "+data.error);
				$("#loader_fb2").hide();
				$("#sync_fb").show();
			}else{
				$("#fbmsg").html('');
				$("#fbmsg").addClass("tberrmsg");
				$("#fbmsg").html(data.error);
				$("#verify_fb").html("Verify Again");
				$("#verify_fb").show();
			}
			trackRegistration(activity,'fb_server_call',data.error,false);
		}else if(data.status == 'mismatch'){
			trackRegistration(activity,'fb_server_call',data.error,false);
			$("#fb_upper").hide();
			$("#mismatch_fb").show();
			var msg='';
			if(data.error=="Age mismatch"){
				msg = fbMessages['age_mismatch'];
				msg = msg.replace("tmAge",data.diff.TMage);
				msg = msg.replace("fbAge",data.diff.fbAge);
				$("#fb_error").html("<i class='icon-warning-sign'></i> "+msg);
			}else if(data.error=="Name mismatch"){
				msg = fbMessages['name_mismatch'];
				msg = msg.replace("fbName",data.diff.fbName);
				msg = msg.replace("tmName",data.diff.TMname);
				$("#fb_error").html("<i class='icon-warning-sign'></i> "+msg);
			}else if(data.error=="Name and Age mismatch"){
				msg = fbMessages['name_age_mismatch'];
				msg = msg.replace("tmAge",data.diff.TMage);
				msg = msg.replace("fbAge",data.diff.fbAge);
				msg = msg.replace("fbName",data.diff.fbName);
				msg = msg.replace("tmName",data.diff.TMname);
				$("#fb_error").html("<i class='icon-warning-sign'></i> "+msg);
			}else if(data.error=="Gender mismatch"){
				$("#fb_error").html("<i class='icon-warning-sign'></i> "+"Er, apologies, but there seems to be a mismatch in the gender as selected by you.");
			}
			
			$("#loader_fb").hide();
			$("#loader_fb2").hide();
			$("#sync_fb").show();
		}
		else if (data.status == 'refresh') {
			FB.api('/me', {fields: 'last_name'}, function(response) {
				//proceed();
				$("#fbmsg").hide();
				if(reimport!=undefined){
					$("#verify_fb").hide();
					$("#loader_fb").hide();
					$("#loader_fb2").hide();
					$("#sync_fb").show();
				}else{
					$("#verify_fb").show();
					$("#loader_fb").hide();
					$("#loader_fb2").hide();
					$("#sync_fb").hide();
				}
				$('#logout_fb').show();
				//showFbReloginPopup();
			});
		}
		$("#verify_fb").show();
		$("#loader_fb").hide();
		fb_flag = 0;
	}, "json");
}

var proofFrom = 0;

var globaltype = 0;

function openFile(type) {
	globaltype = type;
	var value = $("#idSelectBox").val();
	if (value == 0 || isAnyprofilePic=="0") {
		$("#id_button").hide();
		//$("#idVerificationButton_show").show();
	} else{
		if(from_mobile=="1"){
			$("#upload-doc-form").show();
		}else
			$('#file').click();
	}
}

function uploadEmp(){
	var error = false;
	var status = $("[name='Salaried']:checked").val();
	if(status!='self' && status!='salaried')
		error=true;
	
	var cmpName = $("#cmpName").val();
	if(cmpName=='' || cmpName==undefined)
		error=true;
	
	var value = $("#empSelectBox").val();
	if(value==0){
		error=true;
	}
	if(error){
		$("#emp_id_error").html('<p class="tberrmsg"><i class="icon-warning-sign"></i>error in form data</p>');
		$("#emp_id_error").show();
	}else{
		//console.log("file2");
		$("#emp_id_error").html('');
		$("#emp_id_error").hide();
		//$("#emp_upload").show();
		if(from_mobile=="1"){
			$("#upload-doc-form2").show();
		}else
			$('#file2').click();
	}
	
}

function uploadFileEmployment() {
	var fileSize = $("#file2")[0].files[0].size;
	fileSize = fileSize / 1024;
	var percentPerSec = 100 / (fileSize / 10);
	var percent = 0;
	
	
	var cmp = $("#cmpName").val();
	if($.trim(cmp)=="" || cmp==undefined){
		$("#emp_id_error").html('<p class="tberrmsg"><i class="icon-warning-sign"></i>Please Enter Company Name</p>');
		$("#emp_id_error").show();
		//$("#emp_upload").hide();
		return;
	}if($("#empSelectBox").val()==0){
		$("#emp_id_error").html('<p class="tberrmsg"><i class="icon-warning-sign"></i>Please Select Employment Proof</p>');
		$("#emp_id_error").show();
		return;
	}
	$("#loader_emp").show();
	$("#file2").hide();
	$("#emp_upload").hide();
	$("#emp_id_error").hide();
	$("#empSelectBox").prop("disabled", true);
	//$("#emp_upload_loader_percent").css("width", "0%");
	/*var interval = setInterval(function() {
		percent = percent + percentPerSec;
		if (percent < 90) {
			$("#emp_upload_loader_percent").css("width", percent + "0%");
		} else {

		}
	}, 500);*/
	time1 = new Date();
	var file = $("#file2").val();
	var fileName = file.split("\\");
	var form = document.getElementById('upload-doc-form2');
	$("#upload-file-type2").val(3);
		$("#upload_proof_type2").val($("#empSelectBox").val());
		$("#company_name").val($("#cmpName").val());
		$("#show_company").val($("[name='showname']:checked").val());
		$("#work_status").val($("[name='Salaried']:checked").val());
		$("#emp_id").val($("#empid").val());
		$("#emp_upload").hide();
		$("#emp_upload_loader1").show();
	form.setAttribute('action', "trustbuilder.php");
	form.submit();

	$("#file_upload_frame2").load(
			function() {
				//window.clearInterval(interval);
				var uploadResponse = $.parseJSON($("#file_upload_frame2")
						.contents().text());
				if (uploadResponse.status == 'success') {
					trackRegistration(activity,'employment_server_save_call','success',false);
						/*$("#emp_upload_loader_percent").css("width", "100%");
						setTimeout(function(){
							$(".emp_verified_no").hide();
							$(".emp_verified").show();
							$("#emp_type").html($("#empSelectBox").val());
							if(phoneVerified!='active'){
								$("#phone_div").css("border","1px dashed #9fcbfd");
								$("#phone_div").css("background","#d9e9fb");
							}
						},1000);*/
					$("#emp_id_error").hide();
					$("#loader_emp").hide();
					$("#emp_pwd_area").hide();
					$("#emp-form").hide();
					$("#emp-not-clear").hide();
					$("#emp-msg").hide();
					$(".emp_verified").show();
					$("#emp_type").html($("#empSelectBox").val());
				} else {
					alert(data.error);
				}
			});
}


function uploadFile() {
	var fileSize = $("#file")[0].files[0].size;
	fileSize = fileSize / 1024;
	var percentPerSec = 100 / (fileSize / 10);
	var percent = 0;
	
	$("#loader_id").show();
	$("#id_button").hide();
	$("#photo-msg").hide();
	$("#file").hide();
	$("#idSelectBox").prop("disabled", true);
//	$("#id_upload_loader_percent").css("width", "0%");
/*	var interval = setInterval(function() {
		percent = percent + percentPerSec;
		if (percent < 90) {
			$("#id_upload_loader_percent").css("width", percent + "%");
		} else {

		}
	}, 500);*/
	time1 = new Date();
	var file = $("#file").val();
	var fileName = file.split("\\");
	var arr1 = new Array;
	arr1 = file.split("\\");
	var len = arr1.length;
	var img1 = arr1[len-1];
	var filext = img1.substring(img1.lastIndexOf(".")+1);
	var filesize = $("#file")[0].files[0].size;
	
	// Checking Extension
	if(!(filext == "doc" || filext == "docx" || filext == "pdf" || filext == "jpg" || filext == "jpeg" || filext == "png" || filext == "gif"))
	{
		alert("Invalid File Format Selected");
		$("#loader_id").hide();
		$("#photo-msg").hide();
		$("#file").show();
		$("#idSelectBox").prop("disabled", false);
		return false;
	}
	else if(filesize>8388608) {
		alert("File is too big to upload. Maximum size can be 8 MB.");
		$("#loader_id").hide();
		$("#photo-msg").hide();
		$("#file").show();
		$("#idSelectBox").prop("disabled", false);
		return false;
	}
	else
	{
		var form = document.getElementById('upload-doc-form');
		$("#upload-file-type").val(1);
			$("#upload_proof_type").val($("#idSelectBox").val());
			$("#document_name1").html(fileName[fileName.length - 1]);
			$("#id_upload").hide();
			$("#id_upload_loader1").show();
		form.setAttribute('action', "trustbuilder.php");
		form.submit();

		$("#file_upload_frame").load(
				function() {
				//	window.clearInterval(interval);
					var uploadResponse = $.parseJSON($("#file_upload_frame")
							.contents().text());
					if (uploadResponse.status == 'success') {					
						/*	$("#id_upload_loader_percent").css("width", "100%");
							setTimeout(function(){
								$(".id_verified_no").hide();
								$(".id_verified").show();
								$("#id_type").html($("#idSelectBox").val());
							},1000);*/
						trackRegistration(activity,'id_server_call','success',false);
						$("#id-form").hide();
						$("#loader_id").hide();
						$("#id_button").hide();
						$("#id-not-clear").hide();
						$("#pwd_area").hide();
						$("#sync_area").hide();
						$("#id_type").html($("#idSelectBox").val());
						$("#id_success").show();
					} else {
						alert(data.error);
					}
				});
	}
	
}

function uploadPhoto(){
	var fileSize = $("#photo")[0].files[0].size;
	fileSize = fileSize / 1024;
	var percentPerSec = 100 / (fileSize / 10);
	var percent = 0;
	
	$("#loader_photo").show();
	$("#picFirst").hide();
	$("#id_button").hide();
	$("#photo").hide();
	
	var file = $("#photo").val();
	var fileName = file.split("\\");
	var form = document.getElementById('upload-doc-form1');
		
	time1 = new Date();
	
	form.setAttribute('action', "photo.php");
	form.submit();

	$("#file_upload_frame1").load(
			function() {
			//	window.clearInterval(interval);
				 uploadResponse = $.parseJSON($("#file_upload_frame1").contents().text());
				//console.log(uploadResponse);return;
				if (uploadResponse[0]['photo_id']) {
					
					trackRegistration(activity,'uploadpic_server_save_call','success',false);
					
					isAnyprofilePic = "1";
					$("#idSelectBox").prop("disabled", false);
					$("#fbpicmsg").hide();
					/*	$("#id_upload_loader_percent").css("width", "100%");
						setTimeout(function(){
							$(".id_verified_no").hide();
							$(".id_verified").show();
							$("#id_type").html($("#idSelectBox").val());
						},1000);*/
					$("#loader_photo").hide();
					$("#picFirst").hide();
					$("#upload-doc-form1").hide();
					$("#photo-msg").html('User profile photo uploaded sucessfully');
					$("#photo-msg").show();
				} else {
					alert(data.error);
				}
			});
}

function sendPwd(type){
	var pwd = $("#pwd_"+type).val();
	if(pwd=='')
		return;
	else{
		$("#send_pwd_"+type).hide();
		$("#loader_pwd_"+type).show();
		if(type==1)
			$("#id-form").hide();
		if(type==3)
			$("#emp-form").hide();
		$.post("trustbuilder.php",{
			action : "sendpassword",
			password : pwd,
			type : type
		},function(data){
			if (data.status == 'success') {
				if(type==1){
					$("#pwd_area").hide();
					$("#pwd_id_msg").html('Thanks! We will revert within 48 hours after reviewing your document.');
				}else if(type==3){
					$("#emp_pwd_area").hide();
					$("#emp_id_msg").html('Thanks! We will revert within 48 hours after reviewing your document.');
				}
				
			} else {
				alert("Some error occured. Please try again");
			}
		},'json');
	}
}

function allowSync(type){
	//$("#syncId").attr("disabled", true);
	$("#syncId").hide();
	$("#loader_sync_id").show();
	$("#id-form").hide();
	
	$.post("trustbuilder.php",{
		action : "syncNamewithID",
		type : type,
		update : idReject
	},function(data){
		if (data.status == 'success') {
			$("#sync_area").hide();
			idVerified(idProof);
			var trustPercent = $("#trust_percent").html().split("%");
			$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(idScore)) + "%");
			setTrustAngle(parseInt(trustPercent[0]) + parseInt(idScore));
			//$("#sync_id_msg").html('Thanks! Will revert within 48 hours after updating your Name/Age');
		} else {
			alert("Some error occured. Please try again");
		}
	},'json');
}
function openWebCam(type) {
	globaltype = type;
	document.getElementById('imageDiv').innerHTML = '';
	$(".difusescreen").show();
	$(".popupframe").show();
	$("#photoUploadButton").hide();
	var sayCheese = new SayCheese('#say-cheese-container');
	sayCheese
			.on(
					'start',
					function() {
						cameraOn = true;
						document.getElementById('imageDiv').innerHTML = "<button id='take-snapshot' class='webcbtncap'>Take snap!</button> <button id='cancel-snapshot' >Cancel</button>";
						$('#take-snapshot').on('click', function(evt) {
							sayCheese.takeSnapshot();
						});
						$("#cancel-snapshot").on('click', function(evt) {
							if (cameraOn == true) {
								cameraOn = false;
								sayCheese.stop();
								$('video').remove();
							}
							closePopUp();
						});

					});

	sayCheese
			.on(
					'error',
					function(error) {
						alert('Sorry,action could not be completed because of some internal error');
						sayCheese.stop();
						$('video').remove();
						cameraOn = false;
						return;
						// handle errors, such as when a user denies the request
						// to use the
						// webcam,
						// or when the getUserMedia API isn't supported
					});

	sayCheese.on('snapshot', function(snapshot) {
		var img = document.createElement('img');
		$(img).on('load', function() {
			sayCheese.stop();
			$('video').remove();
			cameraOn = false;
			document.getElementById('imageDiv').innerHTML = '';
			$('#imageDiv').prepend(img);
			$(".difusescreen").show();
			$(".popupframe").show();
			$("#showImage").css("max-height", "450px");
			$("#showImage").css("max-width", "640px");
			$("#photoUploadButton").show();
		});
		img.src = snapshot.toDataURL('image/png');
		img.id = "showImage";
	});
	sayCheese.start();
}

function uploadImage() {
	var src = document.getElementById('showImage').src;
	var proofType = "";
	if (globaltype == 1) {
		proofType = $("#idSelectBox").val();
	}else if(globaltype == 2){
		proofType = $("#addressSelectBox").val();
	}
	document.getElementById('imageDiv').innerHTML = "<img id='load' src='images/register/loader_photo.gif' class='fl mrg brdr' style='margin-left:35px;' />";
	$("#photoUploadButton").hide();
	$.post("trustbuilder.php", {
		photoUpload : 'yes',
		src : src,
		proofType : proofType,
		type : globaltype
	}, function(data) {
		if (data.status == 'success') {
			if (globaltype == 1) {
				$(".id_verified_no").hide();
				$(".id_verified").show();
			} else if (globaltype == 2) {
				$(".address_verified_no").hide();
				$(".address_verified").show();
			}
		} else {
			alert(data.error);
		}
		closePopUp();
	}, 'json');
}

function closePopUp() {
	$(".difusescreen").hide();
	$(".popupframe").hide();
	$("#verifyPhoto").val("");
	if (cameraOn == true) {
		cameraOn = false;
		sayCheese.stop();
		$('video').remove();
	}
}

function idChange() {

	var value = $("#idSelectBox").val();
	if (value == 0 || isAnyprofilePic=="0") {
		$("#id_button").hide();
		//$("#idVerificationButton_show").show();
	} else {
		$("#sync_area").hide();
		$("#pwd_area").hide();
		if(from_mobile=="1"){
			$("#upload-doc-form").show();
		}else
			$("#id_button").show();
		//$("#idVerificationButton_show").hide();
	}
}

function addressChange() {
	var value = $("#addressSelectBox").val();
	if (value == 0) {
		$("#addressVerificationButton_show").show();
		$("#address1").hide();
		$("#addressForm").hide();
	} else if(value == 1){
		$("#addressVerificationButton_show").hide();
		$("#address1").hide();
		$("#addressForm").show();
	}else {
		$("#addressVerificationButton_show").hide();
		$("#address1").show();
		$("#addressForm").hide();
	}
}

function empChange() {
	var value = $("#empSelectBox").val();
	if (value == 0) {
		$("#emp1").hide();
		$("#selfemployed_form").hide();
		$("#employed_form").hide();
		$("#empVerificationButton_show").show();
	} else if (value == 1) {
		$("#emp1").hide();
		$("#selfemployed_form").hide();
		$("#employed_form").show();
		$("#empVerificationButton_show").hide();
	} else if (value == 2) {
		$("#emp1").hide();
		$("#selfemployed_form").show();
		$("#employed_form").hide();
		$("#empVerificationButton_show").hide();
	}else {
		$("#emp1").show();
		$("#selfemployed_form").hide();
		$("#employed_form").hide();
		$("#empVerificationButton_show").hide();
	}
}



function setTrustAngle(percent) {
	if(percent > 100){
		percent = 100;
	}
	//if(gender == 'F'){
		if(percent>=parseInt(thresold)){
			$("#authentic").show();
			$("#non-authentic").hide();
		}else{
			$("#authentic").hide();
			$("#non-authentic").show();
		}
//	}else{
//		if(percent>=30){
//			$("#authentic").show();
//			$("#non-authentic").hide();
//		}else{
//			$("#authentic").hide();
//			$("#non-authentic").show();
//		}
//	}
	trustScore = percent;
	var angle = percent * 1.8;
	angle = angle - 90;
	angle = angle + "deg";
	$("#pieSlice1 .pie").css("-webkit-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("-moz-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("-o-transform", "rotate(" + angle + ")");
	$("#pieSlice1 .pie").css("transform", "rotate(" + angle + ")");
	if(percent>0){
		$("#nothnx").hide();
		$("#trustCont").show();
	}
}


function changeEmployment() {
	var value = $("#employmentType").val();
	if (value == 0) {
		$("#selfemployed_form").hide();
		$("#employed_form").hide();
	} else if (value == 1) {
		$("#selfemployed_form").hide();
		$("#employed_form").show();
	} else if (value == 2) {
		$("#selfemployed_form").show();
		$("#employed_form").hide();
	}

}

var linked_flag = 0;

function submitLinkedIn(profiles) {
	if (linked_flag == 0) {
		trackRegistration(activity,"linkedin_popup",'success',false);
		linked_flag = 1;
		
		var num_connections = profiles.values[0].numConnections;
		var firstname = profiles.values[0].firstName;
		var lastname = profiles.values[0].lastName;
		var member_id = profiles.values[0].id;
		var member=profiles.values[0].positions.values;
		var position="";
		if(member!=undefined){
		for(var i=0;i<member.length;i++) {
			if(member[i].isCurrent) {
				var position=member[i].title;
				break;
			}
		}
		}
		
		$("#load_linkedin_date").hide();
		
		time1 = new Date();
		var error=$.ajax({async:false,type:"post",url:baseurl+"/linkedin2.php",data:{member_id:member_id,firstName:firstname,lastName:lastname,numConnections:num_connections,connected_from:connectedFrom,linkedin_data:profiles.values[0],current_designation:position},success:function(data,status) {
		}}).responseText;
		
		var error=JSON.parse(error);
		if(error.responseCode!="200")
		{	
			trackRegistration(activity,"linkedin_server_call",error.error,false);
			$('.inbtndiv').hide();
			$('#intxtdet').hide();
			$('#error_linkedin').html("<p class='tberrmsg'><i class='icon-warning-sign'></i>"+error.error+"</p>");
		}else{
			trackRegistration(activity,"linkedin_server_call",'success',false);
			linkedinVerified(position);
			$("#link-msg").hide();
			$("#linkmsg").html('Thank You.You can update your current designation later by clicking on re-verify.');
			$("#re_verify").show();
			
			if(isLinkedinVerified=='no'){
				var trustPercent = $("#trust_percent").html().split("%");
				$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(linkedScore)) + "%");
				setTrustAngle(parseInt(trustPercent[0]) + parseInt(linkedScore));
			/*	var credits = parseInt($("#credits_update").html());
				if(!isNaN(credits)){
					$("#credits_update").html(parseInt(credits) + parseInt(linkedCredits));
					$("#credits_update_dash").html(parseInt(credits) + parseInt(linkedCredits));
				}*/
			}
			
			
			//$("#linked_error").hide();
			//$('a[id*=li_ui_li_gen_]').html("<a class='tbsactionbtn mt10'>Re-Verify</a>");
			$("#linkedin_designation").html(position);
			isLinkedinVerified='yes';
			fetchLinkedIn();
		}
		linked_flag = 0;
	}
}

function addressType(type) {
	if (type == 1) {
		$("#address_upload_type").hide();
		$("#address_form_type").show();
	} else {
		$("#address_upload_type").show();
		$("#address_form_type").hide();
	}
}

function empType(type) {
	if (type == 1) {
		$("#emp_upload_type").hide();
		$("#emp_form_type").show();
	} else {
		$("#emp_upload_type").show();
		$("#emp_form_type").hide();
	}
}

function submitAddressForm() {
	var address = $("#address").val();
	var city = $("#city").val();
	var state = $("#state").val();
	var pincode = $("#pincode").val();
	var landmark = $("#landmark").val();
	var flag = 0;
	if (address == "" || address.length == 0) {
		$("#address").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#address").css("border", "1px solid #cecccc");
	}

	if (city == "" || city.length == 0) {
		$("#city").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#city").css("border", "1px solid #cecccc");
	}

	if (state == "" || state.length == 0) {
		$("#state").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#state").css("border", "1px solid #cecccc");
	}

	if (pincode == "" || pincode.length == 0) {
		$("#pincode").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#pincode").css("border", "1px solid #cecccc");
	}

	if (landmark == "" || landmark.length == 0) {
		$("#landmark").css("border", "1px solid red");
		flag = 1;
	} else {
		$("#landmark").css("border", "1px solid #cecccc");
	}

	if (flag == 1) {
		$("#address_form_error").html("Please complete your form");
	} else {
		$.post("trustbuilder.php", {
			action : "addressForm",
			address : address,
			city : city,
			state : state,
			pincode : pincode,
			landmark : landmark
		}, function(data) {
			if (data.status == 'success') {
				$(".address_verified_no").hide();
				$(".address_verified").show();
			} else {
				alert("Some error occured. Please try again");
			}
		}, 'json');
	}
}


function submitEmploymentForm(empType) {
	var flag = 0;
	var name = "";
	var code_type = "";
	var address_desi = "";
	var tenure = "";
	var pincode = "";
	var city = "";
	var state = "";
	if(empType == 1){
		name = $("#cmp_name").val();
		code_type = $("#emp_code").val();
		address_desi = $("#designation").val();
		tenure = $("#tenure").val();
		
		if (name == "" || name.length == 0) {
			$("#cmp_name").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#cmp_name").css("border", "1px solid #cecccc");
		}
		
		if (code_type == "" || code_type.length == 0) {
			$("#emp_code").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#emp_code").css("border", "1px solid #cecccc");
		}
		
		if (address_desi == "" || address_desi.length == 0) {
			$("#designation").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#designation").css("border", "1px solid #cecccc");
		}
		
		if (tenure == "" || tenure.length == 0) {
			$("#tenure").css("border", "1px solid red");
			flag = 1;
		} else {
			$("#tenure").css("border", "1px solid #cecccc");
		}
	}else{
		name = $("#bs_name").val();
		code_type = $("#bs_type").val();
		address_desi = $("#bs_address").val();
		pincode = $("#bs_pincode").val();
		city = $("#bs_city").val();
		state = $("#bs_state").val();
		tenure = $("#bs_tenure").val();
		
		if (pincode == "" || pincode.length == 0) {
			$("#bs_pincode").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_pincode").css("border", "1px solid #cecccc");
		}
		
		if (city == "" || city.length == 0) {
			$("#bs_city").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_city").css("border", "1px solid #cecccc");
		}
		
		if (state == "" || state.length == 0) {
			$("#bs_state").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_state").css("border", "1px solid #cecccc");
		}
		
		
		if (name == "" || name.length == 0) {
			$("#bs_name").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_name").css("border", "1px solid #cecccc");
		}
		
		if (code_type == "" || code_type.length == 0) {
			$("#bs_type").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_type").css("border", "1px solid #cecccc");
		}
		
		if (address_desi == "" || address_desi.length == 0) {
			$("#bs_address").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_address").css("border", "1px solid #cecccc");
		}
		
		if (tenure == "" || tenure.length == 0) {
			$("#bs_tenure").css("border", "1px solid red");
			flag = 2;
		} else {
			$("#bs_tenure").css("border", "1px solid #cecccc");
		}
	}

	if (flag == 1) {
		$("#emp1_form_error").html("Please complete your form");
	}else if(flag == 2){
		$("#emp2_form_error").html("Please complete your form");
	} else {
		$.post("trustbuilder.php", {
			action : "employmentForm",
			name : name,
			city : city,
			state : state,
			pincode : pincode,
			code_type : code_type,
			tenure : tenure,
			address : address_desi
		}, function(data) {
			if (data.status == 'success') {
				$(".emp_verified_no").hide();
				$(".emp_verified").show();
			} else {
				alert("Some error occured. Please try again");
			}
		}, 'json');
	}
}


function submitPhone() {
	
	var number = $("#phoneNumber").val();
	var verify = isNaN(number);
	if(verify){
		$("#phone_error").html("Please enter a valid 10 digit mobile number.");
		$("#phone_error").show();
		return;
	} if(number.length != 10){
		$("#phone_error").html("Please enter a valid 10 digit mobile number.");
		$("#phone_error").show();
	}else{
		time1 = new Date();
		trackRegistration(activity,'phone_click','',false);
		time1 = new Date();
		$("#verifyPhone").hide();
		$("#phone_error").hide();
		$.post("trustbuilder.php",{
			action : "numberVerify",
			number : number
		},function(data){
			if(data.status == 'success'){
				$("#verifyPhone").hide();
				$("#phone_wait_loader").show();
				var interval = null;
				var time = 0;
				interval = setInterval(function() {
					time = time + 10;
					if (time<180) {
						$.post("trustbuilder.php",{
							action:"checkNumber"
							},function(data){
								if(data.status=='active'){
									trackRegistration(activity,'phone_server_call','success',false);
									$("#phone_wait_loader").hide();
									//$(".phone_verified").show();
									//$("#phone_verified_circle").attr("class", "bluecircle");
									//$("#phoneNumberSpan").html(number);
									phoneVerified(number);
									var trustPercent = $("#trust_percent").html().split("%");
									$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(mobileScore)) + "%");
									setTrustAngle(parseInt(trustPercent[0]) + parseInt(mobileScore));
									var credits = parseInt($("#credits_update").html());
									if(!isNaN(credits)){
										$("#credits_update").html(parseInt(credits) + parseInt(mobileCredits));
										$("#credits_update_dash").html(parseInt(credits) + parseInt(mobileCredits));
									}
									//$("#phone_div").css("border","none");
									isphoneVerified = "active";
									//$(".phoneVerificationFlag").show();
									//$(".phoneVerificationFlag_no").hide();
									window.clearInterval(interval);
								}else if(data.status == 'rejected'){
									trackRegistration(activity,'phone_server_call','rejected',false);
									if(data.number_of_trials<3){
										$("#verifyPhone").show();
										$("#phone_error").html("Your phone number could not be verified. Please try again.");
										$("#phone_error").show();
									}else{
										$("#phone_error").html("Your phone number could not be verified. Please contact TrulyMadly customer care.");
										$("#phone_error").show();
									}
									$("#phone_wait_loader").hide();
									window.clearInterval(interval);
								}else if(data.status == 'notpick'){
									trackRegistration(activity,'phone_server_call','notpick',false);
									$("#verifyPhone").show();
									$("#phone_error").html("Your phone number could not be verified as there was no response.");
									$("#phone_error").show();
									$("#phone_wait_loader").hide();
									window.clearInterval(interval);
								}
							},"json");
					} else {
						window.clearInterval(interval);
						trackRegistration(activity,'phone_server_call','pending',false);
						$("#phone_error").html("Your phone number is pending verification.");
						$("#phone_error").show();
						$("#phone_wait_loader").hide();
				}}, 10000);
			}else if(data.status == 'fail'){
				trackRegistration(activity,'phone_server_call',data.error,false);
				$("#phone_error").html(data.error);
				$("#phone_error").show();
				$("#verifyPhone").show();
			}
		},'json');		
	}
}
function showbelowItem(show){
	switch(show){
	case "fb":
		$("#fb").removeClass('fbplus').addClass('fbminus');
		$("#fb-below").show();
		$("#fb-minus").show();
		$("#fb-plus").hide();
		break;
	case "linkedin":
		$("#linkedin").removeClass('linkplus').addClass('linkminus');
		$("#link-below").show();
		$("#link-minus").show();
		$("#link-plus").hide();
		break;
	case "phone":
		$("#phone").removeClass('phoneplus').addClass('phoneminus');
		$("#phone-below").show();
		$("#phone-minus").show();
		$("#phone-plus").hide();
		break;
	case "id":
		$("#photoId").removeClass('idplus').addClass('idminus');
		$("#id-below").show();
		$("#id-minus").show();
		$("#id-plus").hide();
		break;
	case "emp":
		$("#emp").removeClass('empplus').addClass('empminus');
		$("#emp-below").show();
		$("#emp-minus").show();
		$("#emp-plus").hide();
		break;
		
		
	case "endorse":
		$("#endorse").removeClass('endorseplus').addClass('endorseminus');
		$("#endorse-below").show();
		if(endorse_count<1)
		$("#endorse-minus").show();
		$("#endorse-plus").hide();
		if(isEndorsed == 1)
		ImageSliderInit();
		break;
	}
}
function hidebelowItem(show){
	switch(show){
	case "fb":
		$("#fb").removeClass('fbminus').addClass('fbplus');
		$("#fb-below").hide();
		$("#fb-plus").show();
		$("#fb-minus").hide();
		break;
	case "linkedin":
		if(isLinkedinVerified=='yes'){
			$("#link-ok").show();
			$("#linkedin").removeClass('linkminus linkplus').addClass('linkok');
		}else{
			$("#linkedin").removeClass('linkminus').addClass('linkplus');
			$("#link-plus").show();
		}
		$("#link-below").hide();
		$("#link-minus").hide();
		break;
	case "phone":
		$("#phone").removeClass('phoneminus').addClass('phoneplus');
		$("#phone-below").hide();
		$("#phone-plus").show();
		$("#phone-minus").hide();
		break;
	case "id":
		$("#photoId").removeClass('idminus').addClass('idplus');
		$("#id-below").hide();
		$("#id-plus").show();
		$("#id-minus").hide();
		break;
	case "emp":
		$("#emp").removeClass('empminus').addClass('empplus');
		$("#emp-below").hide();
		$("#emp-plus").show();
		$("#emp-minus").hide();
		break;
		
	case "endorse":
		$("#endorse").removeClass('endorseminus').addClass('endorseplus');
		$("#endorse-below").hide();
		$("#endorse-plus").show();
		$("#endorse-minus").hide();
		break;
		
	
	}
}

function hidepopup(){
	$("#diffframe4").hide();
}

function showpopup(){
	$("#diffframe4").show();
}

function fbVerified(conn){
	$("#fb-plus").hide();
	$("#fb-minus").hide();
	$("#fb-ok").show();
	$("#fb-below").hide();
	$("#fb_trust").addClass('verifiedts');
	$("#fb").addClass('tbsverified');
	$("#fb").removeClass('fbminus fbplus');
	$("#fb_connections").html(conn + " Connections");
}

function fetchLinkedIn(){
	if(isLinkedinVerified=='yes'){
		$('a[id*=li_ui_li_gen_]').html("<a class='tbsactionbtn mt10'>Re-Verify</a>");
	}else if(isLinkedinVerified=='no'){
		$('a[id*=li_ui_li_gen_]').html("<a class='tbsactionbtn mt10'>Verify</a>");
	}
	IN.Event.on(IN, "auth", onLinkedInAuth);
}

function onLinkedInAuth() {
	$('.inbtndiv').hide();
	$("#load_linkedin_date").show();
	time1 = new Date();
	IN.API.Profile("me","url=http://www.linkedin.com/in/jakobheuser").fields("id", "firstName", "lastName","num_connections","email-address","positions","industry").result(submitLinkedIn).error(onError);
}

//Handle an error response from the API call
function onError(error) {
	time1 = new Date();
	trackRegistration(activity,"linkedin_server_call",error,false);
}

function phoneVerified(phoneNumber){
	$("#phone-plus").hide();
	$("#phone-minus").hide();
	$("#phone-ok").show();
	$("#phone-below").hide();
	$("#phone_trust").addClass('verifiedts');
	$("#phone").removeClass('phoneminus phoneplus');
	$("#mobile").html("+91-"+phoneNumber);
	$("#phone").addClass('tbsverified');
} 
function linkedinVerified(position){
	$("#link-plus").hide();
	$("#link-minus").hide();
	$("#link-ok").show();
	$("#link-below").hide();
	$("#linkedin_trust").addClass('verifiedts');
	$("#linkedin_designation").html(position);
	$("#linkedin").addClass('tbsverified');
	$("#linkedin").removeClass('linkminus linkplus').addClass('linkok');
}
function idVerified(idType){
	$("#id-plus").hide();
	$("#id-minus").hide();
	$("#id-ok").show();
	$("#id-below").hide();
	$("#id_trust").addClass('verifiedts');
	$("#idtype").html('');
	$("#idtype").html(idType);
	$("#photoId").addClass('tbsverified');
	$("#photoId").removeClass('idminus idplus');
	//var trustPercent = $("#trust_percent").html().split("%");
	//$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(idScore)) + "%");
	//setTrustAngle(parseInt(trustPercent[0]) + parseInt(idScore));
}


function endorseVerified(){
/*	$("#emp-plus").hide();
	$("#emp-minus").hide();
	$("#emp-ok").show();
	$("#emp-below").hide();*/
	$("#emp_trust").addClass('verifiedts');
	$("#emp").addClass('tbsverified');
	//var trustPercent = $("#trust_percent").html().split("%");
	//$("#trust_percent").html((parseInt(trustPercent[0]) + parseInt(empScore)) + "%");
	//setTrustAngle(parseInt(trustPercent[0]) + parseInt(empScore));
}

function fbmismatcherror(fbTMdiff){
	if(fbTMdiff=="Age mismatch"){
		msg = fbMessages['age_mismatch'];
		msg = msg.replace("tmAge",tm['age']);
		msg = msg.replace("fbAge",fb['age']);
		$("#fb_error").html("<i class='icon-warning-sign'></i> "+msg);
	}else if(fbTMdiff=="Name mismatch"){
		msg = fbMessages['name_mismatch'];
		msg = msg.replace("fbName",fb['name']);
		msg = msg.replace("tmName",tm['name']);
		$("#fb_error").html("<i class='icon-warning-sign'></i> "+msg);
	}else if(fbTMdiff=="Name and Age mismatch"){
		msg = fbMessages['name_age_mismatch'];
		msg = msg.replace("tmAge",tm['age']);
		msg = msg.replace("fbAge",fb['age']);
		msg = msg.replace("fbName",fb['name']);
		msg = msg.replace("tmName",tm['name']);
		$("#fb_error").html("<i class='icon-warning-sign'></i> "+msg);
	}else if(fbTMdiff=="Gender mismatch"){
		$("#fb_upper").hide();
		$("#sync_fb").hide();
		$("#fb_error").html("<i class='icon-warning-sign'></i> Er, apologies, but there seems to be a mismatch in the gender as selected by you.");
	}
}

function idmismatcherror(idReject){
	var msg='';
	if(idReject=="age_mismatch"){
		msg = idMessages['age_mismatch'];
		msg = msg.replace("tmAge",tm_id['age']);
		msg = msg.replace("fbAge",id['age']);
		$("#id_mismatch_error").html("<i class='icon-warning-sign'></i> "+msg);
	}else if(idReject=="name_mismatch"){
		msg = idMessages['name_mismatch'];
		msg = msg.replace("fbName",id['name']);
		msg = msg.replace("tmName",tm_id['name']);
		$("#id_mismatch_error").html("<i class='icon-warning-sign'></i> "+msg);
	}else if(idReject=="name_age_mismatch"){
		msg = idMessages['name_age_mismatch'];
		msg = msg.replace("tmAge",tm_id['age']);
		msg = msg.replace("fbAge",id['age']);
		msg = msg.replace("fbName",id['name']);
		msg = msg.replace("tmName",tm_id['name']);
		$("#id_mismatch_error").html("<i class='icon-warning-sign'></i> "+msg);
	}
}

function showUpload(){
	$('#file2').show();
}
$(document).ready(function() {
	
	if(isRegistration){
		activity = 'trustbuilder_register';
	}else
		activity = 'trustbuilder';
	
	trackRegistration(activity,'page_load','');
	
	if(window.localStorage){
		window.localStorage['IS_DATA_ATTRIBUTES_DIRTY']="1";
	}
	
	$("#endorse").addClass('endorseplus');

	
	$('#file').on('change', function(event) { 
		time1 = new Date();
		trackRegistration(activity,'id_click','',false);
	    uploadFile();
	});
	
	$("#picFirst").click(function(){
		if(from_mobile=="1"){
			$("#upload-doc-form1").show();
		}else{
			$('#photo').click();
		}
	});
	$('#photo').on('change', function(event) { 
		time1 = new Date();
		trackRegistration(activity,'uploadpic_click','',false);
		uploadPhoto();
	});
	$('#file2').on('change', function(event) { 
		//uploadFileEmployment();
		time1 = new Date();
		trackRegistration(activity,'employment_click','',false);
		$('#emp_upload').show();
	});
	
	
	$("#id_button").hide();
	setTrustAngle(parseInt(trustScore));
	
	if(isFbVerified!="1"){
		$("#fb-plus").show();
		$("#fb-minus").hide();
		$("#fb-ok").hide();
		$("#fb").addClass('fbplus');
	}
	
	if((idReject=='name_age_mismatch' || idReject=='name_mismatch' || idReject=='age_mismatch') && idProofStatus=='rejected'){
		$("#sync_area").show();
		idmismatcherror(idReject);
	}
	
	
	if(!(fbTMdiff=='' || fbTMdiff==undefined)){
		var msg='';
		$("#fb_upper").hide();
		$("#mismatch_fb").show();
		fbmismatcherror(fbTMdiff);
	}
	if(isFbVerified==="1"){
		fbVerified(fbConnections);
	}
	//$("#fb-plus").click(function(){
	/*$(".fbplus").click(function(){
		showbelowItem("fb");
	});*/
	//$("#fb-minus").click(function(){
	//$("#fb").on('click','.tbstilesblue.fbminus',function(){console.log("rajesh");});
	$("#fb").click(function(){
		if($("#fb").hasClass('fbplus')){
			showbelowItem("fb");
		}else if($("#fb").hasClass('fbminus')){
			hidebelowItem("fb");
		}
		//console.log("rajesh");
	});
	
	$("#linkedin").click(function(){
		if($("#linkedin").hasClass('linkplus')){
			showbelowItem("linkedin");
		}else if($("#linkedin").hasClass('linkminus')){
			hidebelowItem("linkedin");
		}else if($("#linkedin").hasClass('linkok')){
			$("#link-below").show();
			$("#link-minus").show();
			$("#link-plus").hide();
			$("#link-ok").hide();
			$("#linkedin").addClass('linkminus');
		}
	});
	
	/*$("#link-plus").click(function(){
		showbelowItem("linkedin");
	});*/
	/*$("#link-ok").click(function(){
		if(isLinkedinVerified=='yes'){
			$("#link-below").show();
			$("#link-minus").show();
			$("#link-plus").hide();
			$("#link-ok").hide();
		}else{
			showbelowItem("linkedin");
		}
	});
	$("#link-minus").click(function(){
		hidebelowItem("linkedin");
	});*/
	
	if(isLinkedinVerified=='yes'){
		$("#re_verify").show();
		linkedinVerified(linkedinDesignation);
	}else if(isLinkedinVerified=='no'){
		$("#link-plus").show();
		$("#link-ok").hide();
		$("#link-minus").hide();
		$("#link-below").hide();
		$("#linkedin").addClass('linkplus');
	}
	
	/*if(phoneVerified=="N" || (phoneVerified=="rejected" && no_of_trials<=3) || phoneVerified=="notpick"){
		hidebelowItem("phone");
	}else */
	if(isphoneVerified=="active"){
		phoneVerified(phoneNumber);
	}else{
		$("#phone").addClass('phoneplus');
	}
	
	$("#phone").click(function(){
		if($("#phone").hasClass('phoneplus')){
			if(isphoneVerified=="rejected" && no_of_trials>=3){
				hidePhone();
			}else
				showbelowItem("phone");
		}else if($("#phone").hasClass('phoneminus')){
			hidebelowItem("phone");
		}
	});
	
	
	/*$("#phone-plus").click(function(){
		if(isphoneVerified=="rejected" && no_of_trials>=3){
			hidePhone();
		}else
			showbelowItem("phone");
	});
	$("#phone-minus").click(function(){
		hidebelowItem("phone");
	});
	*/
	
	$("#photoId").click(function(){
		if($("#photoId").hasClass('idplus')){
			showbelowItem("id");
		}else if($("#photoId").hasClass('idminus')){
			hidebelowItem("id");
		}
	});
	
	
	$("#emp").click(function(){
		if($("#emp").hasClass('empplus')){
			showbelowItem("emp");
		}else if($("#emp").hasClass('empminus')){
			hidebelowItem("emp");
		}
	});

	$("#endorse").click(function(){
		if($("#endorse").hasClass('endorseplus')){
			showbelowItem("endorse");
		}else if($("#endorse").hasClass('endorseminus')){
			if(endorse_count<1)
			hidebelowItem("endorse");
		}
	});
	
	
	
	$("input[name='Salaried']").change(function(event){
		$("#empform").show();
		$("#file2").hide();
		var employed = $("[name='Salaried']:checked").val();
		$("#emp_pwd_area").hide();
		//$("#emp_pwd_area").hide();
		if(employed=='self'){  
			$("#salary").hide();
			$("#emp_id_error").hide();
			buildDropDown("self");
		}
		else if(employed=='salaried'){
			$("#salary").show();
			$("#emp_id_error").hide();
			buildDropDown("salaried");
		}
	});
	
	/*$("#id-plus").click(function(){
		showbelowItem("id");
	});
	$("#id-minus").click(function(){
		hidebelowItem("id");
	});
	*/
	
	if(isAnyprofilePic=="0"){
		$("#idSelectBox").prop("disabled", true);
	}
	
	if(idProofStatus=='active'){
		idVerified(idProof);
	}else if(idProofStatus=='under_moderation'){
		showbelowItem("id");
	}else{
		$("#photoId").addClass('idplus');
	}
	
	
	
	if(endorse_count >=1){
		showbelowItem("endorse");
		if(endorsementScore > 0){
			endorseVerified();
		}
	}else{
		$("#emp").addClass('empplus');
	}
	
	
});

function buildDropDown(type){
	if(type=='self'){
		$("#empSelectBox").html('');
		var str="<option value='0' disabled selected>Select Employment Proof</option><option value='MOM'>MOM</option><option value='Telephone Bill'>Telephone Bill(In The Name Of Company)</option><option value='Electricity Bill'>Electricity Bill(In The Name Of Company)</option><option value='Water Bill'>Water Bill(In The Name Of Company)</option>";
		$("#empSelectBox").append(str);
		str='';
	}else if(type=='salaried'){
		$("#empSelectBox").html('');
		var str="<option value='0' disabled selected>Select Employment Proof</option><option value='Salary Slip'>Salary Slip</option><option value='Offer Letter'>Offer Letter</option>";
		$("#empSelectBox").append(str);
		str='';
	}
}

function hidePhone(){
	$("#phone-plus").hide();
	$("#phone-minus").show();
	$("#phone-ok").hide();
	$("#phone-below").show();
	$("#verifyPhone").hide();
	$("#phoneNumber").hide();
	$("#phone_error").html("Your phone number could not be verified. Please contact TrulyMadly customer care.");
	$("#phone_error").show();
	//$("#phone_trust").addClass('verifiedts');
	//$("#mobile").html("+91-"+phoneNumber);
	//$("#phone").addClass('tbsverified');
}
function checkCompany(){
	var cmp = $("#cmpName").val();
	var file2 = $("#file2").val();
	if($.trim(cmp) !='' && cmp != undefined){
		if(file2 !='' && file2 != undefined){
			$("#emp_id_error").html('');
			$("#emp_id_error").hide();
			$("#emp_upload").show();
		}
	}/*else{
		$("#emp_upload").hide();
	}*/
}

function onContinue(obj){
	if(trustScore==0){
		time1 = new Date();
		trackRegistration(activity,'skip','',false);
		setTimeout(function(){
			window.location=baseurl+ "/personality.php?step=hobby";
		},200);
	}else{
		window.location=baseurl+ "/personality.php?step=hobby";
	}
	
}



function sendEndorsementRequest() {

	/*
	 * var params: FBLinkShareParams = FBLinkShareParams()
	   params.link = NSURL(string: url)
	   params.name = 
	   params.caption = "TrulyMadly"
	   params.picture = NSURL(string: "http://dev.trulymadly.com/trulymadly/images/TMSeal.png")
	   params.linkDescription = "Trulymadly"
	 */
	/*
	 requestCallback = function(b) {
	 createObjectAndLog(b);
	 };*/

	var b = "Your friend has picked you to endorse him on TrulyMadly. Click here to endorse:";
	FB.ui( {
		method : "send",
		link : endorsement_link
	},
			function(response) {
				if (response && !response.error_code) {
					trackRegistration(activity, 'endorsements_click','success', false);
					$('#diffframe5').show();
					setTimeout(function() {
						$('#diffframe5').hide();
					}, 3000);
				} else if (response && response.error_code) {
					trackRegistration(activity, 'endorsements_click', 'error',false);
				} else {
					trackRegistration(activity, 'endorsements_click','no_invites', false);
				}
			});
}
