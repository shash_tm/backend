// Load the SDK asynchronously
(function(d) {
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement('script');
	js.id = id;
	js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

// call the closure for fb init
window.fbAsyncInit = function() {
	FB.init( {
		appId : fb_api, // App ID
		channelUrl : server + '/templates/channel.html', // Channel File
		oauth : true,
		status : false, // check login status
		cookie : true, // enable cookies to allow the server to access the
		// session
		xfbml : true
	// parse XFBML
	});

}

function fbCall() {
	FB.login(onFbLogggedIn);
}

function logEndorseClickCall(){
	logObj = {
			"data" : {
				"time_taken" : 0,
				"activity" : "endorsements",
				"event_type" : "endorse_click",
				"user_id" : id,
				"event_info" : {
					"cookie_id" : headLoadTimeStamp
				}
			}
		};
		logEvent(logObj, 1);
}
function onFbLogggedIn(response) {
	// check the login status - if not connected then ask for login
	FB.getLoginStatus(onFbStatusFetched, true);
}

function onFbStatusFetched(response) {
	logObj = {
			"data" : {
				"time_taken" : 0,
				"activity" : "endorsements",
				"user_id" : id,
				"event_info" : {
					"cookie_id" : headLoadTimeStamp
				}
			}
		};
	response = response;
	if (response.status === 'connected') {
		logObj.data.event_type = "endorse_success";
		getData(response);
	}

	else if (response.status === 'not_authorized') {
		// app not_authorized
		logObj.data.event_type = "fb_authorization_cancelled";

	} else {
		// not_logged_in to Facebook
		logObj.data.event_type = "fb_popup_cancelled";
	}
	logEvent(logObj, 1);

	/*
	 * else { FB.login(function(response) { //parse the response if
	 * (response.status === 'connected') { getData(response); } else {
	 * console.log("unauth"); } }, true); }
	 */
}

/*
 * else if (response.status === 'not_authorized') { FB.login(function(response) {
 * facebook_redirect(response); }, { scope : fb_scopes }); } else {
 * FB.login(function(response) { if (response.authResponse) {
 * facebook_redirect(response); }else{
 * trackLogin(category,"fb_popup_cancel",''); ga('send', 'event', category,
 * 'fb_popup_cancel',source); } }, { scope : fb_scopes }); }
 */

/*
 * console.log("fb cl"); if (navigator.userAgent.match('CriOS')) {
 *  } else { if (!loginViaEmail && !canNot) {
 * FB.getLoginStatus(function(response) { response = response; if
 * (response.status === 'connected') { getData(); } }, true); } } };
 */

function getData() {
	FB.api('/me', {
		fields : 'first_name, email,  gender, picture.type(normal)'
	}, function(response) {
		console.log(response);

		endorseObj = {
			"endorse_response" : {
				"fname" : response.first_name,
				"fid" : response.id,
				"gender" : (response.gender == "male") ? "M" : "F",
				"pic" : response.picture.data.url,
				"is_endorsed" : "yes",
				"email_id" : response.email
			}
		};

		$.ajax( {
			url : location.href,
			data : endorseObj
		}).done(function(result) {
			a = JSON.parse(result);
			if (a.has_already_endorsed) {
				console.log("here");
				logObj = {
					"data" : {
						"time_taken" : 0,
						"activity" : "endorsements",
						"event_type" : "already_endorsed",
						"user_id" : id,
						"event_info" : {
							"cookie_id" : headLoadTimeStamp
						}
					}
				};
				logEvent(logObj, 1);
			}
			if (a.message) {
				logObj = {
					"data" : {
						"time_taken" : 0,
						"activity" : "endorsements",
						"event_type" : "self_endorsement",
						"user_id" : id,
						"event_info" : {
							"cookie_id" : headLoadTimeStamp
						}
					}
				};
				logEvent(logObj, 1);
				alert(a.message);
			} else {
				
				$("#endorsweb").show();
				//displayMessage('endorse');
			}

			console.log("logged");
			$("#endorse").removeClass("clickendorsebtn");
		});

	});
}

function noThanks() {
	logObj = {
		"data" : {
			"time_taken" : 0,
			"activity" : "endorsements",
			"event_type" : "no_thanks",
			"user_id" : id,
			"event_info" : {
				"cookie_id" : headLoadTimeStamp
			}
		}
	};
	logEvent(logObj, 1);
	$("#noendorse").show();
	//displayMessage('nothanks');

}
function sendNoThanksReason() {
	var data = {
		"nothanks" : 1,
		"reason" : $('#whynothanks').val()
	};
	$.get(location.href, data, function(){
		displayMessage('nothanks');
	});	
}



function displayMessage(actionItem) {

	/*
	 * {literal} GAObj = {"data":{"activity":"endorsements",
	 * "event_type":"endorse"}}; {/literal} logGA(GAObj);
	 * 
	 * 
	 * {literal} GAObj = {"data":{"activity":"endorsements",
	 * "event_type":"no_thanks"}}; {/literal} logGA(GAObj);
	 */
	setTimeout(function(){
		window.location = baseurl + "/index.php?"+actionItem+"=1";
	}, 300);
	// alert("thank you for your time!");
}