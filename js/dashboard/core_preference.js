$(document).ready(function() {
	
	trackRegistration("edit_preference","page_load",'');
	
		prettyPrint();
		$("#basicSliderExample").rangeSlider({
			  bounds: {min: 18, max: 70},
			  defaultValues: {min: demo['start_age'], max: demo['end_age']}
		});
		$("#basicSliderExample1").rangeSlider({
	    	 bounds: {min: 48, max: 96},
	    	 defaultValues: {min: trait['start_height'], max: trait['end_height']},
	    	 
	    	 formatter:function(val){
	    		 			var start_feet = Math.floor(val/12);
	    		 			var start_inch = Math.floor(val%12);
			        		return start_feet+"'"+start_inch+"''";
			    		}
		});
		
		$("#basicSliderExample1").bind("userValuesChanged", function(e, data){
			//console.log(data.values.min+" and "+data.values.max);
		});
		
//		function changeMaritalStatus() {
//			var val=$("input[type='checkbox'][value='Never Married']:checked").val();
//			var val1 = $("input[type='checkbox'][value='Married Before']:checked").val();
//			console.log(val);
//			console.log(val1);
//			if(val != undefined && val1 != undefined) {
//				$("[name='marital_status_spouse']").iCheck('uncheck');
//			}
//		}
		
		$("[name='marital_status_spouse_notMatter']").on('ifChecked', function(event){
			  var toReduce='';
				if(this.name=='marital_status_spouse_notMatter')
					toReduce='marital_status_spouse';
					$("[name="+toReduce+"]").iCheck('uncheck');
		});
		$("[name='marital_status_spouse']").on('ifChecked', function(event){
			var toReduce='';
			if(this.name=='marital_status_spouse')
				toReduce='marital_status_spouse_notMatter';
			$("[name="+toReduce+"]").iCheck('uncheck');
			
			//changeMaritalStatus();
		});		
		$("[name='marital_status_spouse']").on('ifUnchecked', function(event){
			var toReduce='';
			if(this.name=='marital_status_spouse')
				toReduce='marital_status_spouse_notMatter';

			var value=$("input[name="+this.name+"]:checked").val();
			if(value!=undefined && $("input[name="+toReduce+"]:checked").val()!=undefined)
				$("[name="+toReduce+"]").iCheck('uncheck');
			else if(value==undefined)
				$("[name="+toReduce+"]").iCheck('check');
		});
		
		
		$("[name='religion_notMatter']").on('ifChecked', function(event){
			  var toReduce='';
				if(this.name=='religion_notMatter')
					toReduce='religion';
					$("[name="+toReduce+"]").iCheck('uncheck');
		});
		$("[name='religion']").on('ifChecked', function(event){
			var toReduce='';
			if(this.name=='religion')
				toReduce='religion_notMatter';
		    	$("[name="+toReduce+"]").iCheck('uncheck');
		});		
		$("[name='religion']").on('ifUnchecked', function(event){
			var toReduce='';
			if(this.name=='religion')
				toReduce='religion_notMatter';

			var value=$("input[name="+this.name+"]:checked").val();
			console.log(value);
			if(value!=undefined && $("input[name="+toReduce+"]:checked").val()!=undefined)
				$("[name="+toReduce+"]").iCheck('uncheck');
			else if(value==undefined){
				$("[name="+toReduce+"]").iCheck('check');
			}
		});
		//$('#country').on('change', function(event) {
			function changeCountry() {
				var change=this.id;
				var call = true;
				var country_id=113;//$("#country").val();
				var stateSelected=$("#state").val();
				if(stateSelected!=null&&stateSelected!=''&&stateSelected!=0)
					var state=stateSelected;
				var toFill='state';
				$("#state").html('');
				$("#city").trigger("liszt:updated");
						
				if(call==true){
					var str='';
					var data=JSON.parse(states);
					str="<option value=''></option>";
					var current_label='India';
					//str+="<optgroup label='"+$('#'+change+" option[value='"+current_label+"']").html()+"'>";
					str+="<optgroup label = '"+current_label+"'>";
					for(var i=0;i<data.length;i++) {
						str+="<option value="+data[i]['country_id']+"-"+data[i]['state_id']+">"+data[i]['name']+"</option>";
					}
					str+="</optgroup>";
					$('#' + toFill).append(str);
					if(toFill=='state'&&state!=undefined)
						for(var k=0;k<state.length;k++)
							$("#" + toFill+" option[value='" + state[k]  + "']").prop("selected",true);

					$('#' + toFill).trigger("liszt:updated");
					
			    }
			//});
			}
		
		var previousCountries = new Array('113');
		
		$("#country option[value=113]").prop("selected",true);
		//$('#country').trigger("liszt:updated");
		//$('#country').change();
		changeCountry();
		
	//	$('body').on('change',"#country",function() {});
	/*	$('body').on('change',"#country",function() {
			call = false;
			var change=this.id;
			var country_id = $("#country").val();
			var stateSelected=$("#state").val();
			if(stateSelected!=null&&stateSelected!=''&&stateSelected!=0)
				var state=stateSelected;
			if(country_id!=null){
				for (var i = 0; i < country_id.length; i++) {
					if (country_id[i] == '113') {
				        call = true;
					}
				}
			}
			
			var toFill='state';
			$("#state").html('');
			$("#city").trigger("liszt:updated");
	
			if(call==true){
				var str='';
				var data=JSON.parse(states);
				str="<option value=''></option>";
				var current_label=data[0]['country_id'];
				str+="<optgroup label='"+$('#'+change+" option[value='"+current_label+"']").html()+"'>";
				for(var i=0;i<data.length;i++) {
					str+="<option value="+data[i]['country_id']+"-"+data[i]['state_id']+">"+data[i]['name']+"</option>";
				}
				str+="</optgroup>";
				$('#' + toFill).append(str);
				if(toFill=='state'&&state!=undefined)
					for(var k=0;k<state.length;k++)
						$("#" + toFill+" option[value='" + state[k]  + "']").prop("selected",true);

				$('#' + toFill).trigger("liszt:updated");
		    }else{
		    	$("#state").select2('val','');
		    	$("#state").html('');
		    	$("#state").trigger("liszt:updated");
		    	$("#city").select2('val','');//val('');
		    	$("#city").html('');
		    	$("#city").trigger("liszt:updated");
		    }
		
	});
		*/
		
	
		
		
		
		$('body').on('change',"#state",function() {
			var change=this.id;
			var country_id=113;//$("#country").val();
			pref=true;
			var states=$("#state").val();	
			states=$("#state").val();
			if($("#city").val()!=null)
				var city=$("#city").val();
			var toFill='city';
			$('#' + toFill).html('');
			$('#' + toFill).trigger("liszt:updated");
			if(states!=null) {
				var state_values=states;
			}
			if(state_values==undefined ||(state_values!=undefined && state_values==0))  {
				$("#city").select2('val','');
				$('#' + toFill).html("");
				$('#' + toFill).trigger("liszt:updated");
				return;
			}
			
			if(city==undefined||city==0)
				document.getElementById(toFill).innerHTML="";
			else document.getElementById(toFill).innerHTML="";
			var state_id=[];
			for(var i=0;i<state_values.length;i++) {
				var id=state_values[i].split("-");
				state_id.push(id[1]);
			}
		
			$.ajax({async:false,url:"geolocations.php?state_id="+state_id+"&country_id=113&pref="+pref,success:function(dataJson){
				
//				console.log(dataJson);
				try{
					var data=JSON.parse(dataJson);
				
				var str='';
				str+="<option value=''></option>";
				if(data!='') {

					var current_label=+data[0]['country_id']+"-"+data[0]['state_id'];
					var current_opt=data[0]['display_name'];
					if(pref)
						str+="<optgroup label='"+$("#"+change+" option[value='"+current_label+"']").html()+"'>";
					var j=0;
					if(!pref) {
						if(data[0]['name']!=data[0]['display_name']) {
							str+="<optgroup label='"+data[0]['display_name']+"'>";
							var current_opt=data[0]['display_name'];
							for(var j=0;j<data.length&&data[j]['name']!=data[j]['display_name'];j++) {	
								if(current_opt!=data[j]['display_name']) {
									current_opt=data[j]['display_name'];
									str+="</optgroup><optgroup label='"+data[j]['display_name']+"'>";	
								}
								str+="<option value="+data[j]['country_id']+"-"+data[j]['state_id']+"-"+data[j]['city_id']+">"+data[j]['name']+"</option>";

							}
							str+="</optgroup>";
						}
					}
					for(var i=j;i<data.length;i++) {
						if(pref&&current_label!=data[i]['country_id']+"-"+data[i]['state_id']) {
							current_label=data[i]['country_id']+"-"+data[i]['state_id'];
							str+="</optgroup>";
							str+="<optgroup label='"+$("#"+change+" option[value='"+current_label+"']").html()+"'>";
						}
							str+="<option value="+data[i]['country_id']+"-"+data[i]['state_id']+"-"+data[i]['city_id']+">"+data[i]['name']+"</option>";
					}
					
				}
				if(pref)
					str+="</optgroup>";
				$('#' + toFill).append(str);
				
				if(toFill=='city'&&city!=undefined){
					for(var k=0;k<city.length;k++){
						$("#" + toFill+" option[value='" + city[k]  + "']").prop("selected",true);
					}
				}
				
				$('#' + toFill).trigger("liszt:updated");
				
				
			}catch(err){
				console.log(err);
			}
			}
				
			});	
			
			cities = $("#city").val();
			if(toFill=='city'&&cities!=undefined){
				for(var k=0;k<cities.length;k++){
					$("#" + toFill+" option[value='" + cities[k]  + "']").prop("selected",true);
				}
				$("#city").select2("val", cities);
			}else{
				$("#city").select2('val',"null");
			}
			
		});
		
		var previousIncome = new Array("null");
		$("#income").change(function(){
			var income = $("#income").val();
			//console.log(income);
			if(income){
				var index3 = income.indexOf("null");
				var index4 = previousIncome.indexOf("null");
		
				if(index3 > -1 && index4 == -1 && income.length>1){
					income = new Array();
					income.push("null");
					$("#income").val(income);
					$("#income").select2("val", "null");
					$('#income').trigger("liszt:updated");
				}else if(index3 > -1 && index4 > -1){
					income.splice(index3,1);
					$("#income").val(income);
					$("#income").select2("val", income);
					$('#income').trigger("liszt:updated");
				}
				previousIncome = income;
			}else{
				//values = new Array();
				//values.push("null");
				//$("#income").val(values);
				$('#income').trigger("liszt:updated");
			}
			//previousIncome = income;
		});
		
		var previousValues =  new Array("-5");
		$("#education").change(function(){
			var values = $("#education").val();
			if(values){
				var index = values.indexOf("-1");
				if(index > -1){
					values.splice(index,1);
					for(var i=2;i<=19;i++){    //for master only
						values.push(i);
					}
					$("#education").select2("val", values);
					$("#education").val(values);
					$('#education').trigger("liszt:updated");
				}
				
				var index2 = values.indexOf("-2");
				if(index2 > -1){
					values.splice(index2,1);
					for(var i=22;i<=39;i++){      //for bachelors only
						values.push(i);
					}
					$("#education").select2("val", values);
					$("#education").val(values);
					$('#education').trigger("liszt:updated");
				}
				
				var index3 = values.indexOf("-5");
				var index4 = previousValues.indexOf("-5");
			//	console.log("previous : "+previousValues);
			//	console.log("values : "+values);
			//	console.log("3 = "+index3+" 4 = "+index4);
			
				if(index3 > -1 && index4 == -1 && values.length>1){
					values = new Array();
					values.push("-5");
					$("#education").val(values);
					$("#education").select2("val", "-5");
					$('#education').trigger("liszt:updated");
				}else if(index3 > -1 && index4 > -1){
					values.splice(index3,1);
					$("#education").val(values);
					$("#education").select2("val", values);
					$('#education').trigger("liszt:updated");
				}
				previousValues = values;
			}else{
			//	values = new Array();
			//	values.push("-5");
			//	$("#education").val(values);
				$('#education').trigger("liszt:updated");
			}
			//previousValues = values;
		});
	
		prefill();
});


function prefill(){
	if(demo['marital_status']==null) {
		$("input[name='marital_status_spouse_notMatter']").iCheck('check');
	}
	else {
		var marital_status_spouseArray=demo['marital_status_new'].split(",");
		$.each(marital_status_spouseArray,function(key,value) {
		    $("input[name='marital_status_spouse'][value='"+value+"']").iCheck('check');
		});
	}
	
	if(demo['religions']==null || demo['religions']==''){
		$("input[name='religion_notMatter']").iCheck('check');
	}else {
		var religion_spouseArray=demo['religions'].split(",");
		$.each(religion_spouseArray,function(key,value) {
		    $("input[name='religion'][value='"+value+"']").iCheck('check');
		});
	}
	
	if(demo['countries']=='') {
		//$("#country option[value='0']").prop("selected",true);
		//$('#country').trigger("liszt:updated");
	}
	else {
			spouse_country=demo['countries'].split(",");
			for(var i=0;i<spouse_country.length;i++) {
				if($("#country option[value='" +spouse_country[i] + "']").length>0) {
		                $("#country option[value='" +spouse_country[i] + "']").prop("selected",true);
				}
			}
			$('#country').trigger("liszt:updated");
			$('#country').change();
			$('#state').change();
			$('#city').change();
	}
	
	$('#country').change();
	$('#state').change();
	$('#city').change();
	
	if(demo['states']=='') {
		//$("#state option[value='0']").prop("selected",true);
		//$('#state').trigger("liszt:updated");
	}
	else {
			spouse_state=demo['states'].split(",");
		    for(var i=0;i<spouse_state.length;i++)  {
				if($("#state option[value='"+spouse_state[i]+"']").length>0) {
		                $("#state option[value='"+spouse_state[i]+"']").prop("selected",true);
				}
		    }
		    $("#state").trigger("liszt:updated");
		    $('#state').change();
		    $('#city').change();
	}
	
	if(demo['cities']=='') {
		//$("#city option[value='0']").prop("selected",true);
		//$('#city').trigger("liszt:updated");
	}
	else{
			spouse_cities=demo['cities'].split(",");
			for(var i=0;i<spouse_cities.length;i++)  {
				if($("#city option[value='"+spouse_cities[i]+"']").length>0) {
					$("#city option[value='"+spouse_cities[i]+"']").prop("selected",true);
				}
			}
			$('#city').trigger("liszt:updated");
			$('#city').change();
	}
	
	/*if(work['income_start']==null && work['income_end']==null){
		$("#income option[value=null]").prop("selected",true);
	}else{
		$("#income option[value='"+work['income_start']+"-"+work['income_end']+"']").prop("selected",true);
	}*/
	
	if(work['income']==null){
		$("#income option[value=null]").prop("selected",true);
		$('#income').trigger("liszt:updated");
	}else{
		//var spouse_income=work['income'].split(",");
		for(var i=0;i<work['income'].length;i++)  {
			if($("#income option[value='"+work['income'][i]+"']").length>0) {
				$("#income option[value='"+work['income'][i]+"']").prop("selected",true);
			}
		}
		//$("#income option[value='"+work['income']+"']").prop("selected",true);
		$('#income').trigger("liszt:updated");
	}
	
	
	if(trait['smoking_status']=='Never'){
		$("input[name='smoking_status'][value='Never']").iCheck('check');
	}else{
		$("input[name='smoking_status'][value='null']").iCheck('check');
	}
	
	if(trait['drinking_status']=='Never'){
		$("input[name='drinking_status'][value='Never']").iCheck('check');
	}else{
		$("input[name='drinking_status'][value='null']").iCheck('check');
	}
	
	if(trait['food_status']=='Vegetarian'){
		$("input[name='food_status'][value='Vegetarian']").iCheck('check');
	}else{
		$("input[name='food_status'][value='null']").iCheck('check');
	}
	
	if(work['education']!=null) {
		var education_spouseArray=work['education'].split(",");
		for(var i=0;i<education_spouseArray.length;i++) {
			$("#education option[value="+education_spouseArray[i]+"]").prop('selected',true);	
		}
		$("#education").trigger("liszt:updated");
	}else{
		$("#education option[value=-5]").prop('selected',true);
		$("#education").trigger("liszt:updated");
	}
}

function saveData(){
	
	if($('#savedata').attr("disabled")=="disabled")
    {
        return false;
    } 
	$('#savedata').attr("disabled","disabled");
	time1 = new Date();
	
	var age = $("#basicSliderExample").rangeSlider("values");
	start_age = Math.round(age.min);
	end_age = Math.round(age.max);
	
	var height = $("#basicSliderExample1").rangeSlider("values");
	start_height = Math.round(height.min);
	end_height = Math.round(height.max);
	
	if($("input[name='marital_status_spouse_notMatter']:checked").length==0) {
		var marital_status_spouse=new Array();
		$("input[name='marital_status_spouse']:checked").each(function() {
			marital_status_spouse.push($(this).val());
		});
	}
	else var marital_status_spouse=$("[name='marital_status_spouse_notMatter']:checked").val();
	
	if($("input[name='religion_notMatter']:checked").length==0) {
		var religionSpouse=new Array();
		$("input[name='religion']:checked").each(function() {
			religionSpouse.push($(this).val());
		});
	}
	else var religionSpouse=$("[name='religion_notMatter']:checked").val();
	
	var countries = "113";//$("#country").val();
	var states = $("#state").val();
	var cities = $("#city").val();
	
	var income = $("#income").val();
	if(income!=null){
	var income2 = income.indexOf("null");
	if(income2 > -1 && income.length == 1){
		income = null;
	}
	}else income = null;
	//var salary = income.split("-");
	//var income_start = salary[0];
	//var income_end = salary[1];
	
	var smoking_status = $("input[name='smoking_status']:checked").val();
	var drinking_status = $("input[name='drinking_status']:checked").val();
	var food_status = $("input[name='food_status']:checked").val();
	var education_spouse = $("#education").val();
	if(education_spouse!=null){
	var index2 = education_spouse.indexOf("-5");
	if(index2 > -1 && education_spouse.length == 1){
		education_spouse = null;
	}
	}else education_spouse = null;
	$.ajax({
		url: baseurl+"/editpreference.php",
		type: "POST",
		dataType:'json',
		data: {update:true,lookingAgeStart:start_age,lookingAgeEnd:end_age,marital_status_spouse:marital_status_spouse,religionSpouse:religionSpouse,spouseCountry:countries,spouseState:states,spouseCity:cities,start_height:start_height,end_height:end_height,smoking_status_spouse:smoking_status,drinking_status_spouse:drinking_status,food_status_spouse:food_status,income:income,education_spouse:education_spouse},
		beforeSend: function() { 
			$('#savedata').text("Saving...");
		},
		success: function(data) {
			if(data.responseCode==200){
				
				trackRegistration("edit_preference","save_call",'success',false);
				
				$('#savedata').text("Saved");
				$('#savedata').removeAttr("disabled");
				setTimeout( function() {
				     window.location = baseurl + "/matches.php";
				}, 1000);
			}else{
				$('#savedata').text("Not Saved");
				$('#savedata').removeAttr("disabled");
			}
		}
	});
	
	if(window.localStorage){
		window.localStorage['IS_DATA_ATTRIBUTES_DIRTY']="1";
	}

	
}