$(function(){
	$('#content').scrollPagination({
		'contentPage': 'democontent.html', 
		'contentData': {}, 
		'scrollTarget': $(window), 
		'heightOffset': 10, 
		'beforeLoad': function(){ 
			$('#loading').fadeIn();	
		},
		'afterLoad': function(elementsLoaded){ 
			 $('#loading').fadeOut();
			 var i = 0;
			 $(elementsLoaded).fadeInWithDelay();
			 if ($('#content').children().size() > 50){ 
			 	$('#nomoreresults').fadeIn();
				$('#content').stopScrollPagination();
			 }
		}
	});
	
	// code for fade in element by element
	$.fn.fadeInWithDelay = function(){
		var delay = 0;
		return this.each(function(){
			$(this).delay(delay).animate({opacity:1}, 200);
			delay += 500;
		});
	};
});

function create( template, vars, opts ){
	return $container.notify("create", template, vars, opts);
}

$(function(){
	$container = $(".clicklikeback,.clicklikeback1, .containernot").notify();
	var clicklike = "<div class='likedliked' id='likedliked' style='display:none;'><div id='youliked1'<p>#{text}</p></div></div>"
	
	//Likes click on Like back 
	$(".likeback-onhover").click(function(){
        $container = $(".clicklikeback").notify();										  
		create("youliked", { text:'You clicked like back!'});
	});
	
	//Messages Click on Like Back
	$(".likeback").click(function(){
		$container = $(".clicklikeback1").notify();
		create("youliked2", { text:'You liked back Shalini Srivastava !'});
	});
	
	$(".likedliked").remove();	
	$(".clicklike").after(clicklike);
	$container = $(".likedliked").notify();
	$(".first").click(function(){
		$container = $(this).parent().find(".likedliked").notify();						   
		create("youliked1", { text:'You have cliked Fav'});
	});
	$(".clicklike").click(function(){
		$container = $(this).parent().find(".likedliked").notify();		
		create("youliked1", { text:'You have cliked Like'});
	});
	$(".last").click(function(){
		$container = $(this).parent().find(".likedliked").notify();										
		create("youliked1", { text:'You have cliked Hide'});
	});

});

$(document).ready(function() {

	//Bx slider for Home page
	$('.bxslider').bxSlider({
	  maxSlides: 4,
	  auto :true,
	});
	
	//Bx slider for Profile page
	$('.pbxslider').bxSlider({
	  maxSlides: 1,
	  infiniteLoop: false,
  	  hideControlOnEnd: true
	});
	
	//Menu
	 $('body').addClass('js');
	  var $menu = $('#menu'),
		  $menulink = $('.menu-link');

	$menulink.click(function(e) {
		e.preventDefault();
		$menulink.toggleClass('active');
		$menu.toggleClass('active');
	});
	
	// Custom Scroll - Vertical Scroll
	$(".vscroll").mCustomScrollbar({scrollInertia:0}); 
	
	// Tab Pages--------------------------------------
	$('.tabpages').each(function(){
	 var $active, $content, $links = $(this).find('a');
	 $active = $links.first().addClass('active');
	 $content = $($active.attr('href'));
	 $links.not(':first').each(function () {
	$($(this).attr('href')).hide();
	 });
	 
	 $(this).on('click', 'a', function(e){
	$active.removeClass('active');
	$content.hide();
	$active = $(this);
	$content = $($(this).attr('href'));
	$active.addClass('active');
	$content.show();
	e.preventDefault();
	 });
	});
	
	//Likes
	$('.likes_content').hide();
	$(".clicklikes").mouseover(function() {
    $(".likes_content").show();
	});
	
	$(".likes_content").mouseleave(function() {
		if ($('.likes_content').is(':visible')) {
    $('.likes_content').hide();
	}
	});
	/*$('.likes_content').hide();
	$('.clicklikes').click(function(e){
    	$('.likes_content').show();
		e.stopPropagation();
	});
	$('.likeclose').click(function(){
    	$('.likes_content').hide();
	});*/
	
	//Filters
	$('.filterbox').hide();
	$('.fliterleft').click(function() {
	$('.fliterleft').addClass('active');
	$('.filterbox').slideUp(500);
	if($('.filterbox').is(':hidden') == true) {
		$(this).addClass('active');
		$('.filterbox').slideDown(500);
	} 
	else
	{
		$(this).removeClass('active');
	}
	});
	  
	//Flip 
	 $('.hover').click(function(e){
		$(this).parents('.panel').addClass('flip');
	});
	$('.flippara').click(function(e){
		$(this).parents('.panel').removeClass('flip');
	});

	//Matches Fav, Like Ignore Actions
	$('.actions p.first,.actions p.last').click(function(){
    	$(this).removeClass('active');
    	$(this).addClass('active');
	});
	
	//Show Message
	$('.mess1').hide();
	$('.showmessage').click(function(){
		$('.showmessage').hide();
    	$('.mess1').show();
    	$('.communicate_mess li:first').addClass('grybg');
	});
	
	//Pop Up
	$('.clickreply').click(function (e) {
		$('#messagereply').modal();
		return false;
	});
	
	//Matches page click on like
	$(".clicklike").click(function(){
		$(this).text( "" );
		$(this).append( "<span class='like likedst'>Liked</span>" );
	});
	
	//Matches page click on top LIKE
	$(".thumbcliked").click(function(){
		$(this).addClass('active');	
		$(".clicklike").text( "" );
		$(".clicklike").append( "<span class='like likedst'>Liked</span>" );
	});
	
	//Partners Perferences Tabs
	$('#horizontalTab').easyResponsiveTabs({
		type: 'default', 
		width: 'auto', 
		fit: true   
	});
	
	
	// Accordin Content
	$(".pdg").hide();
	$(".accordionopen").click(function(){
		$(".pdg").hide();
	 $(this).parents().next(".pdg").show();	
	});
	
	$(".closeacc").click(function(){
	  $(this).parents(".pdg").hide();
	});
	
	//chosen select
    $(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});

if(demo_own['marital_status']=='Never Married') 
$("#haveChildrenDiv").hide();

preFill();
});

function refill() {
preFill();
}

function preFill() {
if(demo_own!=undefined) {
//	$("#whenBornDay option[value='"+demo_own['birth_date']+"']").remove();
	$("#whenBornDay option[value!='"+demo_own['birth_date']+"']").remove();//prop("selected",true);
	$('#whenBornDay').trigger("liszt:updated");
	//$('#whenBornDay').prop('disabled', true).trigger("liszt:updated");
	//$("#whenBornMonth option[value='"+demo_own['birth_month']+"']").prop("selected",true);
	$("#whenBornMonth option[value!='"+demo_own['birth_month']+"']").remove();//prop("selected",true);
	$('#whenBornMonth').trigger("liszt:updated");
	$("#whenBornYear option[value!='"+demo_own['birth_year']+"']").remove();//prop("selected",true);
	$('#whenBornYear').trigger("liszt:updated");
	//whenBornDay
	
//$("#age option[value='"+demo_own['age']+"']").prop("selected",true);
//$('#age').trigger("liszt:updated");


$("label:has(input[name='marital_status'][value='"+demo_own['marital_status']+"'])").addClass('checked');
$("input[name='marital_status'][value='"+demo_own['marital_status']+"']").attr('checked','checked');

$("label:has(input[name='haveChildren'][value='"+demo_own['haveChildren']+"'])").addClass('checked');
$("input[name='haveChildren'][value='"+demo_own['haveChildren']+"']").attr('checked','checked');

$("label:has(input[name='religion'][value='"+demo_own['religion']+"'])").addClass('checked');
$("input[name='religion'][value='"+demo_own['religion']+"']").attr('checked','checked');
$("[name='religion']").change();

$("#caste option[value='" +demo_own['caste'] + "']").prop("selected",true);
$("#caste").trigger("liszt:updated");

$("#stayCountry option[value='"+demo_own['stay_country']+"']").prop("selected",true);
$('#stayCountry').trigger("liszt:updated");
$('#stayCountry').change();


$("#stayState option[value='"+demo_own['stay_state']+"']").prop("selected",true);
$("#stayState").trigger("liszt:updated");
$('#stayState').change();

$("#stayCity option[value='"+demo_own['stay_city']+"']").prop("selected",true);
$('#stayCity').trigger("liszt:updated");

if(demo_own['stay_country']==demo_own['birth_country']&&demo_own['stay_state']==demo_own['birth_state']&&demo_own['stay_city']==demo_own['birth_city']) 
$("input[name='choseBirth']").prop('checked',true);
else {
$("input[name='choseBirth']").prop('checked',false);

$("#birthCountry option[value='"+demo_own['birth_country']+"']").prop("selected",true);
$('#birthCountry').trigger("liszt:updated");
$('#birthCountry').change();

$("#birthState option[value='"+demo_own['birth_state']+"']").prop("selected",true);
$("#birthState").trigger("liszt:updated");
$('#birthState').change();

$("#birthCity option[value='"+demo_own['birth_city']+"']").prop("selected",true);
$('#birthCity').trigger("liszt:updated");
}
$("[name='choseBirth']").change();

$("#motherTongue option[value='" +demo_own['mother_tongue'] + "']").prop("selected",true);
$("#motherTongue").trigger("liszt:updated");
}

if(demo!=undefined) {
$("#lookingAgeStart option[value='"+demo['start_age']+"']").prop("selected",true);
                $('#lookingAgeStart').trigger("liszt:updated");
                $("#lookingAgeEnd option[value='" +demo['end_age']+ "']").prop("selected",true);
                $('#lookingAgeEnd').trigger("liszt:updated");

if(demo['marital_status']==null) {
$("label:has(input[name='marital_status_spouse_notMatter'])").addClass('checked');
$("input[name='marital_status_spouse_notMatter']").attr('checked','checked');
}
else {
var marital_status_spouseArray=demo['marital_status'].split(",");
                $.each(marital_status_spouseArray,function(key,value) {
                        $("label:has(input[name='marital_status_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='marital_status_spouse'][value='"+value+"']").attr('checked','checked');
                });
}

//alert(demo['religions']);
if(demo['religions']==null||demo['religions']==''||demo['religions']==0) {
//alert("here");
$("label:has(input[name='religionSpouseDoesNotMatter'])").addClass('checked');
$("input[name='religionSpouseDoesNotMatter']").attr('checked','checked');
}
else {
var religionSpouseArray=demo['religions'].split(",");
                $.each(religionSpouseArray,function(key,value) {
                        $("label:has(input[name='religionSpouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='religionSpouse'][value='"+value+"']").attr('checked','checked');
                });
}
$("[name='religionSpouse']").change();
var castes=JSON.parse(casteAll);
if(demo['castes']!=''&&demo['castes']!=0) {
		spouse_caste=demo['castes'].split(",");
                for(var i=0;i<spouse_caste.length;i++) {
                $("#casteSpouse option[value='" +spouse_caste[i] + "']").prop("selected",true);
		var toshow=spouse_caste[i].split("-");
		for(var j=0;j<castes[toshow[0]].length;j++) {
		if(castes[toshow[0]][j]['id']==toshow[1]) {
}}

}
                $('#casteSpouse').trigger("liszt:updated");
}
$("#casteSpouse").change();
if(demo['countries']=='') {
$("#spouseCountry option[value='0']").prop("selected",true);
$('#spouseCountry').trigger("liszt:updated");
}
else {
		spouse_country=demo['countries'].split(",");
                for(var i=0;i<spouse_country.length;i++) {
		if($("#spouseCountry option[value='" +spouse_country[i] + "']").length>0) {
                $("#spouseCountry option[value='" +spouse_country[i] + "']").prop("selected",true);
}
}

$('#spouseCountry').trigger("liszt:updated");
$('#spouseCountry').change();
$('#spouseState').change();
$('#spouseCity').change();
}
if(demo['states']=='') {
$("#spouseState option[value='0']").prop("selected",true);
$('#spouseState').trigger("liszt:updated");
}
else {
		spouse_state=demo['states'].split(",");
                for(var i=0;i<spouse_state.length;i++)  {
		if($("#spouseState option[value='"+spouse_state[i]+"']").length>0) {
                $("#spouseState option[value='"+spouse_state[i]+"']").prop("selected",true);
}
}
$("#spouseState").trigger("liszt:updated");
$('#spouseState').change();
$('#spouseCity').change();
}
if(demo['cities']=='') {
$("#spouseCity option[value='0']").prop("selected",true);
$('#spouseCity').trigger("liszt:updated");
}
else {
spouse_cities=demo['cities'].split(",");
for(var i=0;i<spouse_cities.length;i++)  {
if($("#spouseCity option[value='"+spouse_cities[i]+"']").length>0) {
$("#spouseCity option[value='"+spouse_cities[i]+"']").prop("selected",true);
}
}
$('#spouseCity').trigger("liszt:updated");
$('#spouseCity').change();
}


if(demo['languages_preference']!=null)  {
		var spouse_tongue=demo['languages_preference'].toString().split(",");
                for(var i=0;i<spouse_tongue.length;i++) {
                $("#spouseMotherTongue option[value='"+spouse_tongue[i]+"']").prop("selected",true);

}
                $('#spouseMotherTongue').trigger("liszt:updated");
		$('#spouseMotherTongue').change();
}
}


if(trait_own!=undefined) {
var start_feet=parseInt(trait_own['height']/12);
var start_inch=trait_own['height']%12;
$("#feet option[value=0"+start_feet+"]").prop('selected',true);
$("#feet").trigger("liszt:updated");
if(start_inch<10)
$("#inch option[value=0"+start_inch+"]").prop('selected',true);
else $("#inch option[value="+start_inch+"]").prop('selected',true);
$("#inch").trigger("liszt:updated");

$("label:has(input[name='skin_tone'][value='"+trait_own['skin_tone']+"'])").addClass('checked');
$("input[name='skin_tone'][value='"+trait_own['skin_tone']+"']").attr('checked','checked');

$("label:has(input[name='body_type'][value='"+trait_own['body_type']+"'])").addClass('checked');
$("input[name='body_type'][value='"+trait_own['body_type']+"']").attr('checked','checked');

$("label:has(input[name='smoking_status'][value='"+trait_own['smoking_status']+"'])").addClass('checked');
$("input[name='smoking_status'][value='"+trait_own['smoking_status']+"']").attr('checked','checked');
$("input[name='smoking_status'][value='"+trait_own['smoking_status']+"']").click();
$("label:has(input[name='drinking_status'][value='"+trait_own['drinking_status']+"'])").addClass('checked');

$("input[name='drinking_status'][value='"+trait_own['drinking_status']+"']").attr('checked','checked');
$("input[name='drinking_status'][value='"+trait_own['drinking_status']+"']").click();
$("label:has(input[name='food_status'][value='"+trait_own['food_status']+"'])").addClass('checked');
$("input[name='food_status'][value='"+trait_own['food_status']+"']").attr('checked','checked');

}

if(trait!=undefined) {
var start_feet=parseInt(trait['start_height']/12);
var start_inch=trait['start_height']%12;
var end_feet=parseInt(trait['end_height']/12);
var end_inch=trait['end_height']%12;
$("#feetSpouseStart option[value=0"+start_feet+"]").prop('selected',true);
$("#feetSpouseStart").trigger("liszt:updated");
$("#feetSpouseEnd option[value=0"+end_feet+"]").prop('selected',true);
$("#feetSpouseEnd").trigger("liszt:updated");
if(start_inch<10) 
$("#inchSpouseStart option[value=0"+start_inch+"]").prop('selected',true);
else $("#inchSpouseStart option[value="+start_inch+"]").prop('selected',true);
$("#inchSpouseStart").trigger("liszt:updated");
if(end_inch<10)
$("#inchSpouseEnd option[value=0"+end_inch+"]").prop('selected',true);
else $("#inchSpouseEnd option[value="+end_inch+"]").prop('selected',true);
$("#inchSpouseEnd").trigger("liszt:updated");

if(trait['skin_tone']==null) {
$("label:has(input[name='skin_tone_spouse_notMatter'])").addClass('checked');
$("input[name='skin_tone_spouse_notMatter']").attr('checked','checked');
}
else {
var skin_tone_spouseArray=trait['skin_tone'].split(",");
                $.each(skin_tone_spouseArray,function(key,value) {
                        $("label:has(input[name='skin_tone_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='skin_tone_spouse'][value='"+value+"']").attr('checked','checked');
                });
}

if(trait['body_type']==null) {
$("label:has(input[name='body_type_spouse_notMatter'])").addClass('checked');
$("input[name='body_type_spouse_notMatter']").attr('checked','checked');
}
else {
var body_type_spouse_spouseArray=trait['body_type'].split(",");
                $.each(body_type_spouse_spouseArray,function(key,value) {
                        $("label:has(input[name='body_type_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='body_type_spouse'][value='"+value+"']").attr('checked','checked');
                });
}

if(trait['smoking_status']==null) {
$("label:has(input[name='smoking_status_spouse_notMatter'])").addClass('checked');
$("input[name='smoking_status_spouse_notMatter']").attr('checked','checked');
}
else {
var smoking_status_spouse_spouseArray=trait['smoking_status'].split(",");
                $.each(smoking_status_spouse_spouseArray,function(key,value) {
                        $("label:has(input[name='smoking_status_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='smoking_status_spouse'][value='"+value+"']").attr('checked','checked');
                });
}

if(trait['drinking_status']==null) {
$("label:has(input[name='drinking_status_spouse_notMatter'])").addClass('checked');
$("input[name='drinking_status_spouse_notMatter']").attr('checked','checked');
}
else {
var drinking_status_spouse_spouseArray=trait['drinking_status'].split(",");
                $.each(drinking_status_spouse_spouseArray,function(key,value) {
                        $("label:has(input[name='drinking_status_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='drinking_status_spouse'][value='"+value+"']").attr('checked','checked');

                });
}

if(trait['food_status']==null) {
$("label:has(input[name='food_status_spouse_notMatter'])").addClass('checked');
$("input[name='food_status_spouse_notMatter']").attr('checked','checked');
}
else {
var food_status_spouse_spouseArray=trait['food_status'].split(",");
                $.each(food_status_spouse_spouseArray,function(key,value) {
                        $("label:has(input[name='food_status_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='food_status_spouse'][value='"+value+"']").attr('checked','checked');
                });
}

}

if(family_own!=undefined) {
$("label:has(input[name='family_status'][value='"+family_own['family_status']+"'])").addClass('checked');
$("input[name='family_status'][value='"+family_own['family_status']+"']").attr('checked','checked');

}

if(family!=undefined) {
if(family['family_status']==null) {
$("label:has(input[name='family_status_spouse_notMatter'])").addClass('checked');
$("input[name='family_status_spouse_notMatter']").attr('checked','checked');
}
else {
var family_status_spouse_spouseArray=family['family_status'].split(",");
                $.each(family_status_spouse_spouseArray,function(key,value) {
                        $("label:has(input[name='family_status_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='family_status_spouse'][value='"+value+"']").attr('checked','checked');
                });
}
}

if(work_own!=undefined) {

$("label:has(input[name='working_area'][value='"+work_own['working_area']+"'])").addClass('checked');
$("input[name='working_area'][value='"+work_own['working_area']+"']").attr('checked','checked');
if(work_own['working_area']=='Not Working' || work_own['working_area']=='Student')
$("#industry_label").html('Industry (optional)');
else $("#industry_label").html('Industry');

$.each(work_own['education_details'],function(index,val) {
	if(index=='specialisation')
		index='specialization';
	$.each(val,function(index2,value) {
		if(value!=null&&value!='') {
			if(index=='education') {
				$("[id='moreeducation']").click();
				//increase the array value
				$("#"+index+"_"+index2+" [value='"+value+"']").prop('selected',true);
				$("#"+index+"_"+index2).trigger("liszt:updated");
			}
			else $("#"+index+"_"+index2).val(value);
		}
	});
});

var no_indutry_fill=true;

$.each(work_own['industry_details'],function(index,val) {
	if(index=='industries')
		index='industry';
	if(index=='designations')
		index='designation';
	if(index=='companies')
		index='company_name';
	$.each(val,function(index2,value) {
		if(value!=null&&value!='') {
			if(index=='industry') {
				no_indutry_fill=false;
				$("[id='moreWork']").click();
				$("#"+index+"_"+index2+" [value='"+value+"']").prop('selected',true);
				$("#"+index+"_"+index2).trigger("liszt:updated");
			}
			else $("#"+index+"_"+index2).val(value);
		}
	});
});

if(no_indutry_fill==true){
	$("[id='moreWork']").click();	
}

if(work_own['income_start']==null)
work_own['income_start']=0;
if(work_own['income_end']==null)
work_own['income_end']=0;
$("#income option[value='" +work_own['income_start']+ "-"+work_own['income_end']+"']").prop("selected",true);
$("#income").trigger("liszt:updated");

}


if(work!=undefined) {
working_area_spouse=work['working_area'];
if(working_area_spouse==null) { 
$("label:has(input[name='working_area_spouse_notMatter'][value="+working_area_spouse+"])").addClass('checked');
$("input[name='working_area_spouse_notMatter'][value="+working_area_spouse+"]").attr('checked','checked');
}
else {
var working_area_spouseArray=working_area_spouse.split(",");
                $.each(working_area_spouseArray,function(key,value) { 
                        $("label:has(input[name='working_area_spouse'][value='"+value+"'])").addClass('checked');
                        $("input[name='working_area_spouse'][value='"+value+"']").attr('checked','checked');
                });
}
if(work['industry']!=null) {
	//	$("#industry_spouse option[value=0]").prop("selected",false);
		industry_spouse=work['industry'].split(",");
                for(var i=0;i<industry_spouse.length;i++) {
 		              $("#industry_spouse option[value='" +industry_spouse[i] + "']").prop("selected",true);
	}
                $('#industry_spouse').trigger("liszt:updated");
}

if(work['education']!=null) {
		var education_spouseArray=work['education'].split(",");
		for(var i=0;i<education_spouseArray.length;i++) {
		$("#education_spouse option[value="+education_spouseArray[i]+"]").prop('selected',true);	
}
		$("#education_spouse").trigger("liszt:updated");
}else{
	$("#education_spouse option[value=-5]").prop('selected',true);
	$("#education_spouse").trigger("liszt:updated");
}
if(work['income_start']!=null) {
                $("#income_spouse_start option[value=" +work['income_start']+ "]").prop("selected",true);
                $('#income_spouse_start').trigger("liszt:updated");
		var start_income=work['income_start'],end_income=work['income_end'];
		if(work['income_start']==100)
		start_income='1 Crore';
		if(work['income_start']==101)
                start_income='1 Cr plus';
		if(work['income_end']==100)
                end_income='1 Crore';
                if(work['income_end']==101)
                end_income='1 Cr plus';
}
else {
$("#income_spouse_start option[value=0]").prop("selected",true);
$('#income_spouse_start').trigger("liszt:updated");
}
$("#income_spouse_start").change();
if(work['income_end']!=null) { 
                $("#income_spouse_end option[value=" +work['income_end']+ "]").prop("selected",true);
                $('#income_spouse_end').trigger("liszt:updated");
}
else { 
$("#income_spouse_end option[value=0]").prop("selected",true);
$('#income_spouse_end').trigger("liszt:updated");
}


}
	
}


