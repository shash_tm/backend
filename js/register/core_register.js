$(document).ready(function() {
	if(!edit){
		trackRegistration("register_basics","page_load",'');
	}
	else{
		trackRegistration("edit_profile","page_load",'');
	}
	
	var bar = 5;
	toSave=false;
	unsavedChanges = false;
	company_arr = new Array();
	institute_arr = new Array();
	// $.removeCookie("cookie");
	if(!edit)
		var pageInd = $.cookie("pageInd");

	if( pageInd == undefined ){
		pageInd = 0;
	}
	$("body").find("div#container").eq( pageInd ).show(); //show starting page


	//Progess Bar
	/*$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: bar
	});*/

	$('.cutomradio').each(function(){ 
	    var self = $(this),
	      label = self.next(),
	      label_text = label.text();

	    label.remove();
	    self.iCheck({
	      checkboxClass: 'icheckbox_line-blue',
	      radioClass: 'iradio_line-blue',
	      insert: '<div class="icheck_line-icon"></div>' + label_text
	    });
	  });
	
	/*if(gender=='M'){
		var str='';
		//var yearStart = new Date().getFullYear()-21;
		var yearStart = new Date().getFullYear()-18;
		var i = yearStart;
		var yearEnd = new Date().getFullYear()-70;
		for(;i>=yearEnd;i--) {
			str+="<option value="+i+" >"+i+"</option>";
		}
		$("#whenBornYear").append(str);
	}else if(gender=='F'){*/
		var str='';
		var yearStart = new Date().getFullYear()-18;
		var i = yearStart;
		var yearEnd = new Date().getFullYear()-70;
		for(;i>=yearEnd;i--) {
			str+="<option value="+i+" >"+i+"</option>";
		}
		$("#whenBornYear").append(str);
//	}
	
	
	$.get(baseurl+'/register_data.json?ver=10.6',function(a){
		var religion = a.religion;
		var ethnicity = a.ethnicity;
		var smokingValues = a.smokingValues;
		var drinkingValues = a.drinkingValues;
		var foodTypes = a.foodTypes;
		var maritalStatus = a.maritalStatus;
		var income = a.income;
		var countries = a.countries;
		 in_states = a.states;
		var highest_degree = a.highest_degree;
		var industries = a.industries;
		 json_l = a.linkedin_mapping_industry;
		var str='';
		
		str+="<option value=113 >India</option>";
		str+="<option value='' disabled>Outside India - Coming Soon...</option>";
		
		$("#stayCountry").append(str);
		str='';
		$("#stayCountry option[value=113]").prop('selected',true);
		$("#stayCountry").trigger("liszt:updated");
		$("#stayCountry").change();
		
		changeCountry();
		
		for(var i=0;i<religion.length;i++){
			str+="<option value='"+religion[i].key+"'>"+religion[i].value+"</option>";
		}
		$("#religion").append(str);
		str='';
	
		str+="<option value='Not Working'>Not Working</option>";
		for(var i=0;i<industries.length;i++){
			str+="<option value='"+industries[i].key+"'>"+industries[i].value+"</option>";
		}
		$("#industry").append(str);
		str='';
		
		for(var i=0;i<highest_degree.length;i++){
			str+="<option value='"+highest_degree[i].key+"'>"+highest_degree[i].value+"</option>";
		}
		$("#education").append(str);
		str='';
		
		if(edit){
			prefill();   //for edit profile only , written in js/editprofile/editprofile.js
		}
		prefillWork();
		prefillInstitute();
	});
	
/*	prefillWork();
	prefillInstitute();*/
	
	
	//autocomplete suggestion box
	/*$.widget( "custom.catcomplete", $.ui.autocomplete, {
	    _renderMenu: function( ul, items ) {
	      var that = this,
	        currentCategory = "";
	      $.each( items, function( index, item ) {
	        if ( item.category != currentCategory ) {
	          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
	          currentCategory = item.category;
	        }
	        that._renderItemData( ul, item );
	      });
	    }
	  });
	*/
	if(!edit) {
		if($.cookie("TM_Marital_status")==undefined || $.cookie("TM_Marital_status")=="Never Married") {
			$("#haveChildrenDiv").hide();
		}
		$("#haveChildrenDiv").hide();
	}
	
	$("input[name='marital_status']").on('ifChanged', function(event){
		unsavedChanges = true;
		var marital_status=$("[name='marital_status']:checked").val();
		if(marital_status=='Never Married')  
			$("#haveChildrenDiv").hide();
		else{
			$("input[name='haveChildren']").iCheck('uncheck');
			$("#haveChildrenDiv").show();
		}
		checkAge();
	});
	
	$("input[name='haveChildren']").on('ifChanged', function(event){
		unsavedChanges = true;
		checkAge();
	});

	$('body').on('change',"#whenBornYear,#whenBornDay,#whenBornMonth",function() {
		unsavedChanges = true;
		checkAge();
	});
	

	prefillDateOfBirth();

	$("input[name='interest']").on('ifChecked',function(event){
		if($("input[name='interest']").filter(":checked").length==5){
			$("input[name='interest']").not(":checked").iCheck("disable");
			$("#activeInterest").show();
			$("#inactiveInterest").hide();
		}
	});
	
	$("input[name='interest']").on('ifUnchecked',function(event){
		if($("input[name='interest']").filter(":checked").length<5){
			$("input[name='interest']").not(":checked").iCheck("enable");
			$("#activeInterest").hide();
			$("#inactiveInterest").show();
			document.getElementById('hobby_Error').innerHTML="";
		}
	});
	
	$("input[name='smoking_status']").on('ifChanged', function(event){
		checkLifeStyle();
	});
	$("input[name='drinking_status']").on('ifChanged', function(event){
		checkLifeStyle();
	});
	$("input[name='food_status']").on('ifChanged', function(event){
		checkLifeStyle();
	});
	
	
	//$('body').on('change',"#stayCountry,#spouseCountry,#birthCountry",function() {});
	//$('body').on('change',"#stayCountry",function() {
	function changeCountry() {
		checkAge();
		unsavedChanges = true;
		var change=this.id;
		var country_id=113;//$("#stayCountry").val();
		var toFill='stayState';
		$("#stayCity").html('');
		$("#stayState").html('');
		$("#stayCity").html("<option value='' disabled selected>Select current city</option>");
		$("#stayCity").trigger("liszt:updated");
		
		if(country_id==undefined||country_id=='other' ) {
		   $("#"+toFill).html('');
		  // $("#"+toFill).html("<option value='' disabled selected>Select your state</option>");
		   $("#"+toFill).trigger("liszt:updated");
	  	   return;
		}		
		
		document.getElementById(toFill).innerHTML='';
		var async = false;
		if(!edit){
			var async = true;
		}
		var str='';
		str+="<option value='' disabled selected>Select current state</option>";
        if(country_id==113){
        	//str+="<optgroup label='"+$('#'+change+" option[value=113]").html()+"'>";
            for(var i=0;i<in_states.length;i++){
                str+="<option value=113-"+in_states[i].key+" >"+in_states[i].value+"</option>";
            }
           // str+="</optgroup>";
            $('#'+toFill).append(str);
            $('#' + toFill).trigger("liszt:updated");
            
            if(edit){
				prefillLocation("state");
			}else{
				prefillStay("state");    // to prefill facebook location
			}
		}else{
			$.ajax({async:async,url:"geolocations.php?country_id="+country_id,success:function(dataJson){
				var data=JSON.parse(dataJson);
				var current_label=data[0]['country_id'];
				//str+="<optgroup label='"+$('#'+change+" option[value='"+current_label+"']").html()+"'>";
				for(var i=0;i<data.length;i++) {
					if(current_label!=data[i]['country_id']) {
						current_label=data[i]['country_id'];
						//str+="</optgroup>";
						//str+="<optgroup label='"+$('#'+change+" option[value='"+current_label+"']").html()+"'>";
					}
					str+="<option value="+data[i]['country_id']+"-"+data[i]['state_id']+">"+data[i]['name']+"</option>";
				}
				//str+="</optgroup>";
				$('#' + toFill).append(str);
				$('#' + toFill).trigger("liszt:updated");
				
				if(edit){
					prefillLocation("state");
				}else
					prefillStay("state");        // to prefill facebook location
				}
			});
		}
	/*	if(this.id=='stayCountry'){
			sameAsAbove();
		}*/
	//});
	}
	
	

	
	
//	city dropdown filling for all
	$("#stayState").change(function() {
		setTimeout(function(){
		checkAge();
		var pref=false;
		var change=this.id;
		//if(this.id=='stayState') {
			var country_id=113;//$("#stayCountry").val();
			var toFill='stayCity';
			if($("#stayState").val()!=undefined&&$("#stayState").val()!=null)  {
				var state_idArray=$("#stayState").val().split("-");
				var state_id=state_idArray[1];
			}
			else return;	
			$('#' + toFill).html('');
			$('#' + toFill).trigger("liszt:updated");
	//	}
		
		$.ajax({async:true,url:"geolocations.php?state_id="+state_id+"&country_id="+country_id+"&pref="+pref,beforeSend:function(){$("#loader").show();},success:function(dataJson){
			
			//console.log(dataJson);
			try{
				$("#loader").hide();
				var data=JSON.parse(dataJson);
				var str='';
				str+="<option value='' disabled selected>Select current city</option>";
				if(data!='') {
					
					var current_label=+data[0]['country_id']+"-"+data[0]['state_id'];
					var current_opt=data[0]['display_name'];
					if(pref)
						str+="<optgroup label='"+$("#"+change+" option[value='"+current_label+"']").html()+"'>";
					var j=0;
					if(!pref) {
						if(data[0]['name']!=data[0]['display_name']) {
							//str+="<optgroup label='"+data[0]['display_name']+"'>";
							var current_opt=data[0]['display_name'];
							for(var j=0;j<data.length&&data[j]['name']!=data[j]['display_name'];j++) {	
								if(current_opt!=data[j]['display_name']) {
									current_opt=data[j]['display_name'];
									//str+="</optgroup><optgroup label='"+data[j]['display_name']+"'>";	
								}
								str+="<option value="+data[j]['country_id']+"-"+data[j]['state_id']+"-"+data[j]['city_id']+">"+data[j]['name']+"</option>";
							}
							//str+="</optgroup>";
						}	
					}
					for(var i=j;i<data.length;i++) {
						if(pref&&current_label!=data[i]['country_id']+"-"+data[i]['state_id']) {
							current_label=data[i]['country_id']+"-"+data[i]['state_id'];
							str+="</optgroup>";
							str+="<optgroup label='"+$("#"+change+" option[value='"+current_label+"']").html()+"'>";
						}
						str+="<option value="+data[i]['country_id']+"-"+data[i]['state_id']+"-"+data[i]['city_id']+">"+data[i]['name']+"</option>";
					}
				}else{
					str+="<option value='999999'>Other</option>";
				}
				if(pref)
					str+="</optgroup>";
				$('#' + toFill).append(str);
				$('#' + toFill).trigger("liszt:updated");
		}catch(err){
			console.log(err);
		}
		if(edit){
			prefillLocation("city");
		}else
			prefillStay("city");          // to prefill facebook location
		}
			
		});
		}, 100);
	});
	
	$("#stayCity").change(function() {
		checkAge();
	});
	
	$('body').on('change', "#feet,#inch",function() {
		setTimeout(function() {
			checkAge();
		}, 200);
	});
	$('body').on('change',"#religion",function() {
		setTimeout(function() {
			checkReligion();
		}, 500);
		
	});
	
	$('body').on('change',"#industry,#designation",function() {
		checkWork();
	});
	
	$("#working_status").change(function() {
		checkWork();
		$("#designation").prop('disabled',false);
		$("#industry_optional").hide();
		var val=$("input[type='checkbox'][name='working_area']:checked").val();
		if(val=='Not Working') {
			$("#designation").prop('disabled',true);
			$("#industry_optional").show();
			//$('#industry').html('Industry <span class="fr">(optional)</span>');
		}
	});
	
	$('#designation').on('input', function() {
		checkWork();
	});
	
	$('body').on('change',"#education",function() {
		checkEducation();
	});
	
	$("#addCompany").click(function(){
		var val = $("#companies").val();
		$("#addCompany").hide();
		addCompany(val);
	});
	
	$("#addInstitute").click(function(){
		var val = $("#institutes").val();
		$("#addInstitute").hide();
		addInstitute(val);
	});
	
	
	//$("#companies").keypress(function(event){
	$('#companies').bind('input keyup', function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$("#addCompany").hide();
			var val = $("#companies").val();
			addCompany(val);	
		}else{
			var cmp = $("#companies").val();
			if($.trim(cmp).length>0)
				$("#addCompany").show();
			else
				$("#addCompany").hide();
		}
			
	});
	
/*	$("#companies").keydown(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);

		if (keycode == 8 || keycode == 46) {
			var cmp = $("#companies").val();
			if($.trim(cmp).length>0)
				$("#addCompany").show();
			else
				$("#addCompany").hide();
		}
	});*/
	
	
	//$("#institutes").keypress(function(event){
	$('#institutes').bind('input keyup', function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var cmp = $("#institutes").val();
		
		if(keycode == '13'){
			$("#addInstitute").hide();
			var val = $("#institutes").val();
			addInstitute(val);	
		}else{
			if($.trim(cmp).length>0)
				$("#addInstitute").show();
			else
				$("#addInstitute").hide();
		}
	});
	/*$("#institutes").keydown(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var cmp = $("#institutes").val();
		
		if (keycode == 8 || keycode == 46) {
			if($.trim(cmp).length>0)
				$("#addInstitute").show();
			else
				$("#addInstitute").hide();
		}
	});*/
	
	refreshPage( pageInd );    
});



//fetch values from cookies and show values filled in all pages
function refreshPage( ind ){
	var objParent = $("div.wrapper").find("div#container").eq(ind);

}

function addCompany(val){
	if($.trim(val)=='' || company_arr.indexOf(val)>-1)
		return;
	else{
		$("#companies").val("");
		$("#companies").attr("placeholder", "Add More");
		val = val.replace(/"/g, "'");
		company_arr.push($.trim(val));
	}
	
	//if($('ul#company a').length<1){
/*	var html = $("<a onclick='return removeCompany(this);'><li><div class='fl'></div><i class='icon-remove removeicon'></i></li></a>");
	html.find('.fl').text(val); */
	
		$("#company").append("<a onclick='return removeCompany(this);'><li><div class='fl'>"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	//}else{
		//$("#company").prepend("<a onclick='return removeCompany(this);'><li><div class='fl'>"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	//}
}

function addInstitute(val){
	if($.trim(val)=='' || institute_arr.indexOf(val)>-1)
		return;
	else{
		$("#institutes").val("");
		$("#institutes").attr("placeholder", "Add More");
		val = val.replace(/"/g, "'");
		institute_arr.push($.trim(val));
	}
	//if($('ul#institute a').length<1){
		$("#institute").append("<a onclick='return removeInstitute(this);'><li><div class='fl'>"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	//}else{
		//$("#institute").prepend("<a onclick='return removeInstitute(this);'><li><div class='fl'>"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	//}
}

function sameAsAbove(){
	if($("#sameAsAbove").attr('checked')){
		$("#birthCountry").val($("#stayCountry").val());
		$("#birthCountry").change();
		
		$("#birthState").val($("#stayState").val());
		$("#birthState").change();
		
		$("#birthCity").val($("#stayCity").val());
		
		$('#birthCountry').trigger("liszt:updated");
		$('#birthState').trigger("liszt:updated");
		$('#birthCity').trigger("liszt:updated");
	}
}

function checkAge(){
	var error = false;
	var whenBornDay = $("#whenBornDay").val();
	var whenBornMonth = $("#whenBornMonth").val();
	var whenBornYear = $("#whenBornYear").val();
	var marital_status=$("[name='marital_status']:checked").val();
	var haveChildren=$("[name='haveChildren']:checked").val();
	var heightFoot=$("#feet").val();
	var heightInch=$("#inch").val();
	
	if( whenBornDay == '' || whenBornMonth == '' ||whenBornYear == '') {
		var error=true;
	}
	else if(!validateDate(whenBornDay,whenBornMonth,whenBornYear))        {
		error=true;
	}

	var stayCountry=113;//$("#stayCountry").val();
	var stayState=$("#stayState").val();
	var stayCity=$("#stayCity").val();
	
	if(stayCountry=='' ||stayCountry==undefined) {
		error=true;
	}

	if(stayState==''||stayState==null){
		error=true;
	}
	if(stayCity==''||stayCity==null){
		error=true;
	}
	
	if(marital_status=='Never Married') {
		haveChildren=undefined;
		if(!edit)
			$.cookie("TM_HaveChildren",'',{ expires: 0 });
	}
	if(marital_status==undefined) {
		error=true;
	}
	if(marital_status!='Never Married'&&haveChildren==undefined) {
		error=true;
	}
	if(heightFoot == '' || heightFoot == undefined || heightInch == '' || heightInch == undefined) {
		error = true;
	}
	
	if(error){
		$("#activeBirth").hide();
		$("#inactiveBirth").show();
		return;
	}
	else{
		$("#activeBirth").show();
		$("#inactiveBirth").hide();
	}
}

function checkLifeStyle(){
	var smoking_status=$("input[name='smoking_status']:checked").val();
	var drinking_status=$("input[name='drinking_status']:checked").val();
	var food_status=$("input[name='food_status']:checked").val();
	
	var error=false;

	if(smoking_status==undefined) {
		error=true;
	}
	if(drinking_status==undefined) {
    	error=true;
    }	
	if(food_status==undefined) {
    	error=true;
    }
	
	if(error){
		$("#activeLifeStyle").hide();
		$("#inactiveLifeStyle").show();
		return;
	}
	else{
		$("#activeLifeStyle").show();
		$("#inactiveLifeStyle").hide();
	}
}

function checkReligion(){
	var religion = $("#religion").val();
	var heightFoot=$("#feet").val();
	var heightInch=$("#inch").val();
	var income = $("#income").val();
	
	var error=false;
	if(religion== undefined || religion=="") {
		error=true;
	}
	if(heightFoot == '' || heightInch == '') {
		error=true;
	}
	if(income== undefined || income=="") {
		error=true;
	}
	if(error){
		$("#activeReligion").hide();
		$("#inactiveReligion").show();
		return;
	}else{
		$("#activeReligion").show();
		$("#inactiveReligion").hide();
	}
	
}

function checkWork(){
	var working_area=$("[name='working_area']:checked").val();
	var industry = $("#industry").val();
	var designation = $("#designation").val();
	
	var error=false;
	$("#designation").prop('disabled',false);
	
	if(industry == 'Not Working') {
		$("#designation").prop('disabled',true);
	}
	
	if(industry != 'Not Working') {
		if($.trim(designation)==''||$.trim(designation)==undefined) {
			error=true;
		}
	}
	if(industry==''||industry==undefined) {			
		error=true;
	}
			
	if(error){
		$("#activeWork").hide();
		$("#inactiveWork").show();
		return;
	}else{
		$("#activeWork").show();
		$("#inactiveWork").hide();
	}
	
}

function checkEducation(){
	var education=$("#education").val();;
	var error=false;
	
	if(education=='' || education==undefined) {
		error=true;
	}	
	if(error){
		$("#activeEdu").hide();
		$("#inactiveEdu").show();
		return;
	}else{
		$("#activeEdu").show();
		$("#inactiveEdu").hide();
	}
	
	
}
//saving values to cookies,putting validations and refreshing page for next show up
function onContinue( obj ){
    toSave=false;
	unsavedChanges = true;
	var page = "";
	if(edit){
		var ind=obj;
		if(window.localStorage){
			window.localStorage['IS_DATA_ATTRIBUTES_DIRTY']="1";
		} 
	}
	else {
		var objParent = $(obj).parents("div#container");
		var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	}
	if( ind == 0 ){
		//page = "age";
		page = "basics";
	//	checkAge();
		
		var whenBornDay = $("#whenBornDay").val();
		var whenBornMonth = $("#whenBornMonth").val();
		var whenBornYear = $("#whenBornYear").val();
		var marital_status=$("[name='marital_status']:checked").val();
		var haveChildren=$("[name='haveChildren']:checked").val();
		var stayCountry=113;//$("#stayCountry").val();
		var stayState=$("#stayState").val();
		var stayCity=$("#stayCity").val();
		var heightFoot=$("#feet").val();
		var heightInch=$("#inch").val();
		
	
		if(!edit) {
			$.cookie("TM_S1_WhenBornDay", whenBornDay, { expires: 7 });
			$.cookie("TM_S1_WhenBornMonth", whenBornMonth, { expires: 7 });
			$.cookie("TM_S1_WhenBornYear", whenBornYear, { expires: 7 });
			$.cookie("TM_S3_StayCountry", stayCountry, { expires: 7 });
			$.cookie("TM_S3_StayState", stayState, { expires: 7 });
			$.cookie("TM_S3_StayCity", stayCity, { expires: 7 });
			$.cookie("TM_Marital_status",marital_status,{ expires: 7 });
			$.cookie("TM_HaveChildren",haveChildren,{ expires: 7 });
			$.cookie("TM_S1_HeightFoot",heightFoot, { expires: 7 });
			$.cookie("TM_S1_HeightInch", heightInch, { expires: 7 });
		}
		else {
			if($('#activeBirth').attr("disabled")=="disabled")
		    {
		        return false;
		    } 
			$('#activeBirth').attr("disabled","disabled");
			var fname = $("#fname").val();
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'basics',TM_S3_StayCountry:stayCountry,TM_S3_StayState:stayState,TM_S3_StayCity:stayCity,TM_Marital_status:marital_status,TM_HaveChildren:haveChildren,TM_S1_HeightFoot:heightFoot,TM_S1_HeightInch:heightInch},
				beforeSend: function() { 
					$('#activeBirth').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						trackRegistration('edit_profile',page,'success');
						$('#activeBirth').text("Saved");
						$('#activeBirth').removeAttr("disabled");
						setTimeout(function(){
							hidebelowItem("basics");
						},200);
					}else{
						$('#activeBirth').text("Not Saved");
						$('#activeBirth').removeAttr("disabled");
						
					}
				}
			});
		}
	}else if(ind==1) {
		page = 'interest';
	 
	   	var interestVal=new Array();
		$("input[name='interest']:checked").each(function() {
			interestVal.push($(this).val());
		});
		
		if(!edit) {
			$.cookie("TM_interest",JSON.stringify(interestVal),{ expires: 7 });
		}else{
			if($('#activeInterest').attr("disabled")=="disabled")
		    {
		        return false;
		    } 
			$('#activeInterest').attr("disabled","disabled");
			time = new Date();
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'interest',TM_interest:JSON.stringify(interestVal)},
				beforeSend: function() { 
					$('#activeInterest').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						trackRegistration('edit_profile',page,'success');
						$('#activeInterest').text("Saved");
						$('#activeInterest').removeAttr("disabled");
						setTimeout(function(){
							hidebelowItem("interest");
						},200);
					}else{
						$('#activeInterest').text("Not Saved");
						$('#activeInterest').removeAttr("disabled");
					}
				}
			});
		}
	}else if( ind == 100){
		page = "habit";
		var smoking_status=$("input[name='smoking_status']:checked").val();
		var drinking_status=$("input[name='drinking_status']:checked").val();
		var food_status=$("input[name='food_status']:checked").val();
		
		//checkLifeStyle();

		if(!edit){
			$.cookie("TM_S2_Smoking_status", smoking_status, { expires: 7 });
			$.cookie("TM_S2_Drinking_status", drinking_status, { expires: 7 });
			$.cookie("TM_S2_Food_status", food_status, { expires: 7 });
		}
		else {
			if($('#activeLifeStyle').attr("disabled")=="disabled")
		    {
		        return false;
		    } 
			$('#activeLifeStyle').attr("disabled","disabled");
			time = new Date();
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'lifestyle',TM_S2_Smoking_status:smoking_status,TM_S2_Drinking_status:drinking_status,TM_S2_Food_status:food_status},
				beforeSend: function() { 
					$('#activeLifeStyle').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						trackRegistration('edit_profile',page,'success');
						$('#activeLifeStyle').text("Saved");
						$('#activeLifeStyle').removeAttr("disabled");
						setTimeout(function(){
							hidebelowItem("lifestyle");
						},200);
					}else{
						$('#activeLifeStyle').text("Not Saved");
						$('#activeLifeStyle').removeAttr("disabled");
					}
				}
			});
		}
	}else if( ind == 300){
		page = "other_details";
		
		var religion = $("#religion").val();
		var heightFoot=$("#feet").val();
		var heightInch=$("#inch").val();
		
		if($("#income").val()!='') {
			var incomeRange=$("#income").val().split("-");
			var income_start=incomeRange[0];
			var income_end=incomeRange[1];
		}
	            
		if(!edit) {
			$.cookie("TM_S2_Religion", religion, { expires: 7 });
			//$.cookie("TM_S3_MotherTongue", motherTongue, { expires: 7 });
			$.cookie("TM_S1_HeightFoot",heightFoot, { expires: 7 });
			$.cookie("TM_S1_HeightInch", heightInch, { expires: 7 });
			$.cookie("TM_Income_start",income_start,{ expires: 7 });
			$.cookie("TM_Income_end",income_end,{ expires: 7 });
		}
		else {
			if($('#activeReligion').attr("disabled")=="disabled")
		    {
		        return false;
		    } 
			$('#activeReligion').attr("disabled","disabled");
			time = new Date();
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'religion',TM_S2_Religion:religion,TM_S1_HeightFoot:heightFoot,TM_S1_HeightInch:heightInch,TM_Income_start:income_start,TM_Income_end:income_end},
				beforeSend: function() { 
					$('#activeReligion').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						trackRegistration('edit_profile',page,'success');
						$('#activeReligion').text("Saved");
						$('#activeReligion').removeAttr("disabled");
						setTimeout(function(){
							hidebelowItem("religion");
						},200);
					}else{
						$('#activeReligion').text("Not Saved");
						$('#activeReligion').removeAttr("disabled");
					}
				}
			});
		}
	}else if(ind == 2){
		page = 'work';
		var working_area=$("[name='working_area']:checked").val();
		var industry = $("#industry").val();
		var designation = $("#designation").val();
		designation = designation.replace(/"/g, "'");
		addCompany($("#companies").val());
		
		var income_start='';
		var income_end=''
		
		if($("#income").val()!='' && $("#income").val()!='NA') {
			var incomeRange=$("#income").val().split("-");
			income_start=incomeRange[0];
			income_end=incomeRange[1];
		}
		
		var error=false;
		if(designation=='' || designation==undefined || industry=='Not Working'){
			designation = '';
		}
		if((industry=='' || industry==undefined) || industry=='Not Working'){
			industry='';
		}
		
		if(company_arr== '' || company_arr==undefined){
			company_arr=null;
		}
		
		working_area = "yes";
		
		if(industry == '') {
			working_area = "no";
		}
	
		if(!edit) {
			$.cookie("TM_Industry_0",industry,{ expires: 7 });
			$.cookie("TM_Designation_0",designation,{ expires: 7 });
			$.cookie("TM_Working_area",working_area,{ expires: 7 });
			$.cookie("TM_Company_name_0",JSON.stringify(company_arr),{ expires: 7 });
			$.cookie("TM_Income_start",income_start,{ expires: 7 });
			$.cookie("TM_Income_end",income_end,{ expires: 7 });
		}
			else {
				if($('#activeWork').attr("disabled")=="disabled")
			    {
			        return false;
			    } 
				$('#activeWork').attr("disabled","disabled");
				time = new Date();
				$.ajax({
				    url: baseurl+"/update.php",
				    type: "POST",
				    dataType:'json',
				    data: {param:'worknew',TM_Industry_0:industry,TM_Designation_0:designation,TM_Working_area:working_area,TM_Company_name_0:JSON.stringify(company_arr),TM_Income_start:income_start,TM_Income_end:income_end},
				    beforeSend: function() { 
				    	$('#activeWork').text("Saving...");
				    	},
				    success: function(data) {
				    	if(data.responseCode==200){
				    		trackRegistration('edit_profile',page,'success');
				        $('#activeWork').text("Saved");
				        $('#activeWork').removeAttr("disabled");
				        setTimeout(function(){
							hidebelowItem("work");
						},200);
				    }else{
				    	$('#activeWork').text("Not Saved");
				    	$('#activeWork').removeAttr("disabled");
				    }
				    }
				});
			}
	}else if(ind==3) {
		page = 'education';
		var education=$("#education").val();
		
		addInstitute($("#institutes").val());
		
		if(institute_arr== '' || institute_arr==undefined){
			institute_arr=null;
		}
		
		if(!edit) {
			$.cookie("TM_Education_0",education,{ expires: 7 });
			$.cookie("TM_Institute_0",JSON.stringify(institute_arr),{ expires: 7 });
			toSave=true;
		}
		else {
			if($('#activeEducation').attr("disabled")=="disabled")
		    {
		        return false;
		    } 
			$('#activeEducation').attr("disabled","disabled");
			time = new Date();
			$.ajax({
			    url: baseurl+"/update.php",
			    type: "POST",
			    dataType:'json',
			    data:{param:'education',TM_Education_0:education,TM_Institute_0:JSON.stringify(institute_arr)},
			    beforeSend: function() { 
			    	$('#activeEducation').text("Saving...");
			    	},
			    success: function(data) {
			    	if(data.responseCode==200){
			    		trackRegistration('edit_profile',page,'success');
			    		$('#activeEducation').text("Saved");
			    		$('#activeEducation').removeAttr("disabled");
			    		setTimeout(function(){
							hidebelowItem("education");
						},200);
			    	}else{
			    		$('#activeEducation').text("Not Saved");
			    		$('#activeEducation').removeAttr("disabled");
			    	}
			    }
			});
		}
	}
	
	if(!edit)
		$.cookie("pageInd", ind + 1, { expires: 7 });
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).next().show();
	
	if(!edit){
		trackRegistration('register_basics',page,'');
	}

	if(toSave) { 
			time = new Date();
			$.get("register.php?save=basics",function(data,status) {
				if(status=='success'){
					trackRegistration('register_basics','register_server_save_call','success');
					setTimeout(function(){
						window.location=baseurl + "/register.php";	
					},200);
				}
			});
	}

	refreshPage( ind + 1);
}

//funtion to take back pages
function onBack( obj ){
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if(!edit) 
		$.cookie("pageInd", ind - 1, { expires: 7 });    
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).prev().show();
}

$(window).bind('beforeunload',function(){
	if(!toSave && unsavedChanges && !edit){
		return 'You have unsaved changes to your profile.';
	}
});


$(window).unload(function(){
	var cookies = $.cookie();
	$.removeCookie("pageInd");
	for(var cookie in cookies) {
	//   $.removeCookie(cookie);
	}
});
