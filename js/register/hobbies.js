Trulymadly.interest = function($){
	var favourites = {};
	favourites['movies'] = {};
	favourites['music'] = {};
	favourites['books'] = {};
	favourites['travel'] = {};
	favourites['others'] = {};
	
	function onContinue(){
		var data={favourites:favourites};
		
		data=JSON.stringify(data);
		time1 = new Date();
		//if(toSave) { 
			$.ajax({
				url: baseurl+"/personality.php?save=hobby",
				type: "POST",
				data: {data:data},
				success: function(data) {
					trackRegistration("hobby","save_call",'success',false);
				setTimeout(function(){
			//		alert(data);
					//window.location=baseurl + "/register.php";
					if(rdUrl!='' && rdUrl!=undefined)
						window.location="http://"+rdUrl;
					else if(stepsCompleted.indexOf('psycho1')>-1 && stepsCompleted.indexOf('psycho2')>-1 && fromRegister){
						window.location=baseurl + "/matches.php";
					}else{
						if(fromRegister)
							window.location=baseurl + "/personality.php?from_trust=true";
						else
							window.location=baseurl + "/personality.php";
					}
				},200);
				}
			});
		//}
	}
	
	
		function setAutoComplete(id){
			var category = "";
			if(id=='music_box'){
				category = 'music';
			}else if(id=='travelnfood'){
				category = 'travel';
			}else if(id=='others'){
				category = 'sports';
			}else{
				category = id;
			}
				$("#"+id).catcomplete({
					source:function( request, response ) {
						$.ajax({
							url: "dictionary.php?value="+$("#"+id).val()+"&category="+category,
							dataType: "json",
							success:function(data) {
								response($.map( data, function( item ) {
									return {
										label: item.text,
										value: item.text,
										key: item.id,
										notable_name:item.notable_name,
										category:"Suggestions"
									}
								}));
							}
						});
					},
					minLength:2,
				    select: function(event, ui ) {
				    	switch(id){
				    	case 'movies':
				    		addMovies(ui.item.key,ui.item.value);
				    		break;
				    	case 'music_box':
				    		addMusic(ui.item.key,ui.item.value);
				    		break;
				    	case 'books':
				    		addRead(ui.item.key,ui.item.value);
				    		break;
				    	case 'travelnfood':
				    		addTravel(ui.item.key,ui.item.value);
				    		break;
				    	case 'others':
				    		addOthers(ui.item.key,ui.item.value);
				    		break;
				    	}
				    },
				    close: function( event, ui ) {
				    	$("#"+id).val('');
				    }
				});
		}
		
	$(document).ready(function(){
		trackRegistration("hobby","page_load",'');
	/*	$(document).ajaxStart(function() {
			  $( "#container" ).show();
		});*/
		
		$.widget( "custom.catcomplete", $.ui.autocomplete, {
		    _renderMenu: function( ul, items ) {
		      var that = this,
		        currentCategory = "";
		      $.each( items, function( index, item ) {
		    	 // console.log(item);
		        if ( item.category != currentCategory ) {
		          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
		          currentCategory = item.category;
		        }
		        that._renderItemData( ul, item );
		      });
		    },
		    _renderItem: function( ul, item ) {
		    	//console.log(item);
		    	  return $( "<li class='ui-menu-item'>" ).append("<div class='mrgdiv'><a>"+item.value+"</a><a style='float:right;'>"+item.notable_name+"</a></div>").appendTo(ul);
		    }
		  });
		
		
		setAutoComplete("movies");
		setAutoComplete("music_box");
		setAutoComplete("books");
		setAutoComplete("travelnfood");
		setAutoComplete("others");
		
		if(toSelect!=undefined || toSelect!=''){
			prefillFavourites(toSelect);
		}
		
	});
	
	function prefillFavourites(toSelect){
		
		for(var key in toSelect.movies.favorites)
		{ 
			//console.log(key, toSelect.movies.favorites[key]);
			addMovies(key,toSelect.movies.favorites[key]);
		}
		
		for(var key in toSelect.music.favorites)
		{ 
			//console.log(key, toSelect.music.favorites[key]);
			addMusic(key,toSelect.music.favorites[key]);
		}
		
		for(var key in toSelect.books.favorites)
		{ 
			addRead(key,toSelect.books.favorites[key]);
		}
		
		for(var key in toSelect.travel.favorites)
		{ 
			addTravel(key,toSelect.travel.favorites[key]);
		}
		
		for(var key in toSelect.others.favorites)
		{ 
			addOthers(key,toSelect.others.favorites[key]);
		}
	}
	
	
	function addMovies(key,val){
		if(val=='' || favourites['movies'].hasOwnProperty(key))
			return;
		else{
			$("#movies").val('');
			favourites['movies'][key] = val;
		}
		$("#movie").prepend("<a onclick='return Trulymadly.interest.removeMovies(this);'><li><div class='fl' key="+key+">"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	}
	function addMusic(key,val){
		if(val=='' || favourites['music'].hasOwnProperty(key))
			return;
		else{
			$("#music_box").val("");
			favourites['music'][key] = val;
		}
		$("#music").prepend("<a onclick='return Trulymadly.interest.removeMusic(this);'><li><div class='fl' key="+key+">"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	}
	function addRead(key,val){
		if(val=='' || favourites['books'].hasOwnProperty(key))
			return;
		else{
			$("#books").val("");
			favourites['books'][key] = val;
		}
		$("#book").prepend("<a onclick='return Trulymadly.interest.removeRead(this);'><li><div class='fl' key="+key+">"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	}
	function addTravel(key,val){
		if(val=='' || favourites['travel'].hasOwnProperty(key))
			return;
		else{
			$("#travelnfood").val("");
			favourites['travel'][key] = val;
		}
		$("#travel").prepend("<a onclick='return Trulymadly.interest.removeTravel(this);'><li><div class='fl' key="+key+">"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	}
	function addOthers(key,val){
		if(val=='' || favourites['others'].hasOwnProperty(key))
			return;
		else{
			$("#others").val("");
			favourites['others'][key] = val;
		}
		$("#other").prepend("<a onclick='return Trulymadly.interest.removeOthers(this);'><li><div class='fl' key="+key+">"+val+"</div><i class='icon-remove removeicon'></i></li></a>");
	}
	
	function removeMovies(obj){
		var key = $(obj).children('li').children('div')[0].getAttribute('key');
		if(favourites['movies'].hasOwnProperty(key)){
			delete favourites['movies'][key];
		}
		$(obj).remove();
	}
	function removeMusic(obj){
		var key = $(obj).children('li').children('div')[0].getAttribute('key');
		if(favourites['music'].hasOwnProperty(key)){
			delete favourites['music'][key];
		}
		$(obj).remove();
	}
	function removeRead(obj){
		var key = $(obj).children('li').children('div')[0].getAttribute('key');
		if(favourites['books'].hasOwnProperty(key)){
			delete favourites['books'][key];
		}
		$(obj).remove();
	}
	function removeTravel(obj){
		var key = $(obj).children('li').children('div')[0].getAttribute('key');
		if(favourites['travel'].hasOwnProperty(key)){
			delete favourites['travel'][key];
		}
		$(obj).remove();
	}
	function removeOthers(obj){
		var key = $(obj).children('li').children('div')[0].getAttribute('key');
		if(favourites['others'].hasOwnProperty(key)){
			delete favourites['others'][key];
		}
		$(obj).remove();
	}
	
	return{
		onContinue:onContinue,
		removeMovies:removeMovies,
		removeMusic:removeMusic,
		removeRead:removeRead,
		removeTravel:removeTravel,
		removeOthers:removeOthers
	}
}(jQuery);