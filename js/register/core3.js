$(document).ready(function() {

	if(!edit)
	var pageInd = $.cookie("pageInd");

	if( pageInd == undefined ){
		pageInd = 0;
	}

	$("div.wrapper").find("div#container").eq( pageInd ).show(); //show starting page

	//Progess Bar
	$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: 25
	});


//	jquery chozen plugin
	$(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});
//More Children 
fillIncomeSpouse();
$("#income_spouse_start").change(function() {
var start=$("#income_spouse_start").val();
var income_spouse=JSON.parse(income_spouse_array);
document.getElementById('income_spouse_end').innerHTML='';
var toFill='income_spouse_end';
$('#' + toFill).chosen();
$('#' + toFill).append("<option value=''></option>");
if(start==0) {
$('#' + toFill).append("<option value=0 selected='selected'>Any</option>");
$('#' + toFill).trigger("liszt:updated");
return;
}
for(var i=0;i<income_spouse.length;i++) {
if(Number(start)<=Number(income_spouse[i])) {
                        if(income_spouse[i]==100)
                                $('#' + toFill).append("<option value="+income_spouse[i]+">Rs. 1 Crore</option>");
                                else  if(income_spouse[i]==0)
                                $('#' + toFill).append("<option value=0>Any</option>");
                                else  $('#' + toFill).append("<option value="+income_spouse[i]+">Rs. "+income_spouse[i]+" lakhs</option>");
}
}
$('#' + toFill).append("<option value='101'>1 Cr Plus</option>");
$('#' + toFill).trigger("liszt:updated");
});

if(!edit) {
if($.cookie('TM_Family_status_spouse')==undefined) {
$("label:has(input[name='family_status_spouse_notMatter'])").addClass('checked');
$("input[name='family_status_spouse_notMatter']").attr('checked','checked');
}
}
	$("[name='family_status_spouse_notMatter']").change(function() {
                var toReduce='';
if(this.name=='family_type_spouse_notMatter')
                toReduce='family_type_spouse';
		else if(this.name=='family_status_spouse_notMatter')
		toReduce='family_status_spouse';

		var value=$("input[name="+this.name+"]:checked").val();
                if(value != undefined)
                        $("[name="+toReduce+"]").prop("checked", false);		
		else $("[name="+this.name+"]").prop("checked", true);

        });

        $("[name='family_status_spouse']").change(function() {
		var toReduce='';
		if(this.name=='family_type_spouse')
		toReduce='family_type_spouse_notMatter';
		 if(this.name=='family_status_spouse')
                toReduce='family_status_spouse_notMatter';

                var value=$("input[name="+this.name+"]:checked").val();
                if(value!=undefined )//&& $("input[name="+toReduce+"]:checked").val()!=undefined)
                $("[name="+toReduce+"]").prop("checked", false);
		else $("[name="+toReduce+"]").prop("checked", true);

});
});

//fetch values from cookies and show values filled in all pages
function refreshPage( ind ){
	var objParent = $("div.wrapper").find("div#container").eq(ind);

}

//saving values to cookies,putting validations and refreshing page for next show up
function onContinue3( obj ){
var toSave=false;
var page = "";
	var objParent = $(obj).parents("div#container");
	if(edit)
	var ind=obj;
	else 	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
 if( ind == 0){
	 page = 'family';
var family_status=$("[name='family_status']:checked").val();
if($("input[name='family_status_spouse_notMatter']:checked").length==0) {
        var family_status_spouse=new Array();
                $("input[name='family_status_spouse']:checked").each(function() {
                       family_status_spouse.push($(this).val());
                });
}
else var family_status_spouse=$("[name='family_status_spouse_notMatter']:checked").val();
if($("#income").val()!='') {
var incomeRange=$("#income").val().split("-");
var income_start=incomeRange[0];
var income_end=incomeRange[1];
}
var income_spouse_start=$("#income_spouse_start").val();
var income_spouse_end=$("#income_spouse_end").val();

var error=false;
if(family_status==undefined) {
error=true;
document.getElementById('family_status_Error').innerHTML=errors.family_status;
}
else document.getElementById('family_status_Error').innerHTML='';

if(family_status_spouse==''||family_status_spouse==undefined) {
error=true;
document.getElementById('family_status_spouse_Error').innerHTML=errors_family_status_spouse;
}
else document.getElementById('family_status_spouse_Error').innerHTML='';

if(income_start==undefined||income_start==''||income_end==''||income_end==undefined) {
error=true;
document.getElementById('income_Error').innerHTML=errors.income;
}
else document.getElementById('income_Error').innerHTML='';


if(error)
return;
if(!edit) {
$.cookie("TM_Family_status",family_status,{ expires: 7 });
$.cookie("TM_Family_status_spouse",family_status_spouse,{ expires: 7 });
$.cookie("TM_Income_start",income_start,{ expires: 7 });
$.cookie("TM_Income_end",income_end,{ expires: 7 });
$.cookie("TM_Income_spouse_start",income_spouse_start,{ expires: 7 });
$.cookie("TM_Income_spouse_end",income_spouse_end,{ expires: 7 });
toSave=true;
}
else {

 	$.ajax({
	    url: baseurl+"/update.php",
	    type: "POST",
	    dataType:'json',
	    data: {param:'family1',family_status:family_status,family_status_spouse:family_status_spouse,income_start:income_start,income_end:income_end,income_spouse_start:income_spouse_start,income_spouse_end:income_spouse_end},
	    beforeSend: function() { 
	    	$('#family_save').text("Saving...");
	    	},
	    success: function(data) {
	    	if(data.responseCode==200){
	        $('#family_save').text("Saved");
	    }else{
	    	$('#family_save').text("Not Saved");
	    }
	    	
	        // ...
	    }
	});
//$.post(baseurl+"/update.php",{param:'family1',family_status:family_status,family_status_spouse:family_status_spouse,income_start:income_start,income_end:income_end,income_spouse_start:income_spouse_start,income_spouse_end:income_spouse_end},function(data,status) {
// });
}
} 
 
if(!edit)
$.cookie("pageInd", ind + 1, { expires: 7 });
        $(obj).parents("div.container").eq(0).hide();
        $(obj).parents("div.container").eq(0).next().show();

        if(!edit){
    		trackRegistration(page,'');
    	}
        
if(toSave) {
$.get("register.php?save=family",function(data,status) {
                if(status=='success')
                window.location=baseurl+"/register.php";

                });

}

	refreshPage( ind + 1);
}

//funtion to take back pages
function onBack3( obj ){
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if(!edit)
	$.cookie("pageInd", ind - 1, { expires: 7 });    
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).prev().show();
	refreshPage( ind - 1 );
}

$(window).load(function() {
	setTimeout(function(){
	    if(performance.timing!=undefined){
	      var t = performance.timing;
	      var dnsLookup = (t.domainLookupStart-t.domainLookupEnd)/1000;
	      var pageload = (t.loadEventEnd-t.navigationStart)/1000;
	      var pageCreate = (t.loadEventEnd-t.responseEnd)/1000;
	      var networklatency = (t.responseEnd-t.fetchStart)/1000;
	      trackPerformance("family_dnsLookup",dnsLookup);
	      trackPerformance("family_networkLatency",networklatency);
	      trackPerformance("family_pageLoad",pageload);
	      trackPerformance("family_pageCreate",pageCreate);
	    }
	},0);
});
