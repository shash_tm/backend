$(document).ready(function() {
	// $.removeCookie("cookie");
	if(!edit){
	var pageInd = $.cookie("pageInd");
	if( pageInd == undefined ){
		pageInd = 0;
	}
	$("div.wrapper").find("div#container").eq( pageInd ).show(); //show starting page
	}else{
		$("#container").show();
	}
	//Progess Bar
	$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: 95
	});
//	jquery chozen plugin
	$(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});
// Load dialog on click

refreshInterests(true);


$("[name='music_favorites']").select2(freebase_selector("music"));
$("[name='books_favorites']").select2(freebase_selector("books"));
$("[name='movies_favorites']").select2(freebase_selector("movies"));
$("[name='food_favorites']").select2(freebase_selector("food"));
$("[name='sports_favorites']").select2(freebase_selector("sports"));
$("[name='travel_favorites']").select2(freebase_selector("travel"));



if(from_fb) {
$("#fb_import").hide();
$("#loader_image").show();
$.get(baseurl+"/fetchFb.php",function(data,status) {
if(status=='success'){
loadFacebookData(data.fb_likes);
}},"json");
$("#loader_image").hide();
$("#fb_message").html('Data imported from Facebook');
}else{
	loadDBData(toSelect);
}

$(".hobbies-icon").click(function() {
if(this.id=='others') {
$("#allFavorites").hide();
}
else $("#allFavorites").show();

current_selected=this.id;
switch(this.id){
case "music":
	$("#hobbies_title").text("What kind of music do you like listening to?");
	$("#allFavorites").text("Tell us about your favourite singers, bands and songs...");
	break;
case "books":
	$("#hobbies_title").text("What kind of books do you like to read?");
	$("#allFavorites").text("Tell us about your favourite authors, magazines and books...");
	break;
case "movies":
	$("#hobbies_title").text("What kind of movies and TV shows do you like to watch?");
	$("#allFavorites").text("Tell us about your favourite movies, shows and actors...");
	break;
case "food":
	$("#hobbies_title").text("What kind of food do you like to eat?");
	$("#allFavorites").text("Tell us about your favourite cuisines, dishes and restaurants...");
	break;
case "sports":
	$("#hobbies_title").text("What kind of sports do you like to play or watch?");
	$("#allFavorites").text("Tell us about your favourite athletes, sports teams and physical activity...");
	break;
case "travel":
	$("#hobbies_title").text("What's your holiday style?");
	$("#allFavorites").text("Tell us about your favourite holiday destinations...");
	break;

case "others":
	$("#hobbies_title").text("What are your other hobbies and interests?");
	break;
}


$(".temp_arrow").remove();
activateFilledInData();
$(this).parent().addClass('active').parent().removeClass("filledinthob");
$(this).parent().addClass("active").parent().addClass("popuplistactive").prepend("<img class='temp_arrow "+this.id+"_arrow' src='images/register/arrow_down.png' />");

$("[name$='_preferences']").hide();
$("[name='"+this.id+"_preferences']").show();
$("[name$=_favorites_div]").hide();
$("[name='"+this.id+"_favorites_div']").show();
});

$("select-input").hide();

});

function activateFilledInData(){
		$(".hobbies-icon").each(function(index) {
		if($("[name='"+this.id+"_preferences'] :checked").length==0&&(this.id!=='other'&&($("[name='"+this.id+"_favorites']").val()=='')||($("[name='"+this.id+"_favorites']").length==0)))
		$(this).parent().removeClass('active').parent().removeClass("popuplistactive").parent().removeClass("filledinthob");
		else
		$(this).parent().addClass('active').parent().addClass("filledinthob");
		});
}

function getActiveCount(){
	var count=0;
	$(".hobbies-icon").each(function(index) {
	if($("[name='"+this.id+"_preferences'] :checked").length!=0||(this.id!=='other'&&($("[name='"+this.id+"_favorites']").val()!='')&&($("[name='"+this.id+"_favorites']").length!=0)))
	{
		count++;
	}
	});
	return count;
}

/*
 * upload data from fb
 */
function loadFacebookData(data) {

	$.each(data,function(category,values){	
			category=category.toLowerCase();
			if(values.favorites!=undefined) {
				var fav_array=new Array();
				for(var i=0;i<values.favorites.length;i++) {
					fav_array.push({id:"fid:"+values.favorites[i]['id'],text:values.favorites[i]['name']});
				}
				//get old values already filled in there
				var old_values=$("input[name='"+category+"_favorites']").select2("data");
				for (i= 0; i< old_values.length; i++) {
					fav_array.push({id:old_values[i].id,text:old_values[i].text});
				}
				initial_values[category]=fav_array;
				$("input[name='"+category+"_favorites']").select2("val",["1"]);
				if(current_selected==category){
					$("#"+category).parent().addClass("active").parent().addClass("popuplistactive");
				}
				else{
					$("#"+category).parent().addClass("active").parent().addClass("popuplistactive").addClass("filledinthob");
				}
				//$(this).parent().addClass('active').parent().removeClass("filledinthob");


			}
			});
	//activateFilledInData();
}

function loadDBData(data) {
	if(data!=undefined) 
	$.each(data,function(category,values){	
		category=category.toLowerCase();
		if(values.favorites!=undefined && values.favorites.length!=0) {
			var fav_array=new Array();
			for (var x in values.favorites){
				fav_array.push({id:x,text:values.favorites[x]});
			}
			
			//get old values already filled in there
			var old_values=$("input[name='"+category+"_favorites']").select2("data");
			for (i= 0; i< old_values.length; i++) {
				fav_array.push({id:old_values[i].id,text:old_values[i].text});
			}
			initial_values[category]=fav_array;
			$("input[name='"+category+"_favorites']").select2("val",["1"]);
			if(current_selected==category){
				$("#"+category).parent().addClass("active").parent().addClass("popuplistactive");
			}
			else{
				$("#"+category).parent().addClass("active").parent().addClass("popuplistactive").addClass("filledinthob");
			}


		}
		
		if(values.preferences!=undefined && values.preferences.length!=0){
			for(var x in values.preferences){
				$("input[name='"+category+"_preferences'][value='"+values.preferences[x]+"']").prop("checked",true).parent().addClass("checked");
//				$('#checkbox_'+values.preferences[x]).prop("checked",true).parent().addClass("checked");
			}
			
			if(current_selected==category){
				$("#"+category).parent().addClass("active").parent().addClass("popuplistactive");
			}
			else{
				$("#"+category).parent().addClass("active").parent().addClass("popuplistactive").addClass("filledinthob");
			}
		}
		});
}

function formatResultInterestandHobbies(movie) {
    return '<div><div class="fl">' + movie.text +"</div><div class='fr fs11'>"+ movie.notable_name+ '</div></div>';
}

function formatSelectionInterestandHobbies(data) {
	//alert("herenow");
    return data.text;
}

function freebase_selector(category){

	return {
        tags:true,
         tokenSeparators: [],
        minimumInputLength:2,
    ajax: {
        url: baseurl+"/dictionary.php?category="+category,
        dataType: "json",
        quietMillis:500,
        cache:true,
        data: function(term, page) {
          return {
                  value: term
          };
        },
        results: function(data, page) {
          return {
            results: data
          };
        }
      },
      initSelection : function (element, callback) {
    	  //alert(element);
         // var data = [{id:1,text:'bug'},{id:2,text:'duplicate'}];
    	  //if(typeof tag_values!="undefined")
          callback(initial_values[category]);
         // callback(data);
      },
          formatResult: formatResultInterestandHobbies,
          formatSelection: formatSelectionInterestandHobbies,
    multiple: true,
placeholder:"select your favorite one"
	};
}


//fetch values from cookies and show values filled in all pages
function refreshPage( ind ){
	var objParent = $("div.wrapper").find("div#container").eq(ind);
	if( ind == 0 ){

	}else if( ind == 1 ){
	}
	else if(ind==2) {
}
else if(ind==3) {
}

}

//saving values to cookies,putting validations and refreshing page for next show up
function onContinue6( obj ){
var toSave=false;
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if( ind == 0 ){ 
		var error=false;
		var count=getActiveCount();
		if(count<4){
			error=true;
			document.getElementById('interest_Error').innerHTML=errors.interest;
			}
			else document.getElementById('interest_Error').innerHTML='';
			if(error)
			return;
	var categories=["music","food","sports","movies","travel","books","others"];
	var favourites={};//new Array();
	var preferences={};//new Array();

	for(var i=0;i<categories.length;i++){
		var fav=$("[name='"+categories[i]+"_favorites']").select2("data");
		favourites[categories[i]]={};//new Array();
		for(var j=0;j<fav.length;j++){
			favourites[categories[i]][fav[j].id]=fav[j].text;
		}
		var j=0;
		preferences[categories[i]]={};//new Array();
		$("[name='"+categories[i]+"_preferences'] :checked").each(function() {
			//preferences.push($(this).val());
			preferences[categories[i]][j]=$(this).val();
			j++;
			});
	}

	var data={preferences:preferences,favourites:favourites};
	data=JSON.stringify(data);
	
	trackRegistration('hobby','');
	
	$.ajax({
		url: baseurl+"/register.php?save=hobby",
		type: "POST",
		//dataType:'json',
		data: {data:data},		
		success: function(data) {
			window.location=baseurl + "/register.php";
			}
	});
	}
	refreshPage( ind + 1);
}

function onContinue7(){ 
		var error=false;
		var count=getActiveCount();
		if(count<4){
				error=true;
				document.getElementById('interest_Error').innerHTML=errors.interest;
				}
				else document.getElementById('interest_Error').innerHTML='';
				if(error)
				return;
		var categories=["music","food","sports","movies","travel","books","others"];
		var favourites={};
		var preferences={};

		for(var i=0;i<categories.length;i++){
			var fav=$("[name='"+categories[i]+"_favorites']").select2("data");
			favourites[categories[i]]={};//new Array();
			for(var j=0;j<fav.length;j++){
				favourites[categories[i]][fav[j].id]=fav[j].text;
			}
			var j=0;
			preferences[categories[i]]={};//new Array();
			$("[name='"+categories[i]+"_preferences'] :checked").each(function() {
				//preferences.push($(this).val());
				preferences[categories[i]][j]=$(this).val();
				j++;
				});
		}

		var data={preferences:preferences,favourites:favourites};
		data=JSON.stringify(data);
		
		
		$('#hobby_save').text("Saving...");
		$.post("update.php",{
			data : data,
			param:'hobby'
		},function(response){
			if(response.responseCode==200){
				$('#hobby_save').text("Saved");
			}else{
				$('#hobby_save').text("Not Saved");
			}
		},'json');
	}

//funtion to take back pages
function onBack( obj ){
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	$.cookie("pageInd", ind - 1, { expires: 7 });    
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).prev().show();
	refreshPage( ind - 1 );
}
