$(document).ready(function() {
	$("div.wrapper").find("div#progressbar").progressbar({
		value : parseInt(98)
	});
	
	var uniqueLength = $("#unique_text").val().length;
	var weekendLength = $("#weekend_text").val().length;
	var friendLength = $("#friend_text").val().length;
	
	if(uniqueLength < minimumword){
		$('#unique_char').html("Minimum "+(20 - uniqueLength)+" characters.");
	}
	
	if(uniqueLength < minimumword){
		$('#weekend_char').html("Minimum "+(minimumword - weekendLength)+" character.");
	}
	
	if(uniqueLength < minimumword){
		$('#friend_char').html("Minimum "+(minimumword - friendLength)+" character.");
	}
	
	$("#unique_text").keyup(function(){
		uniqueLength = $("#unique_text").val().length;
		if(uniqueLength < minimumword){
			$('#unique_char').html("Minimum "+(minimumword - uniqueLength)+" character.");
		}else{
			$("#error_class").hide();
			$('#unique_char').html("");
			$("#unique_error").html("");
		}
	});
	
	$("#weekend_text").keyup(function(){
		weekendLength = $("#weekend_text").val().length;
		if(weekendLength < minimumword){
			$('#weekend_char').html("Minimum "+(minimumword - weekendLength)+" character.");
		}else{
			$("#error_class").hide();
			$('#weekend_char').html("");
			$("#weekend_error").html("");
		}
	});
	
	$("#friend_text").keyup(function(){
		friendLength = $("#friend_text").val().length;
		if(friendLength < minimumword){
			$('#friend_char').html("Minimum "+(minimumword - friendLength)+" character.");
		}else{
			$("#error_class").hide();
			$('#friend_char').html("");
			$("#friend_error").html("");

		}
	});
	
});

function onContinue9() {
	var uniqueText = $("#unique_text").val();
	var weekendText = $("#weekend_text").val();
	var friendText = $("#friend_text").val();

	var proceed = 3;

	if (uniqueText.length < minimumword) {
	//	$("#unique_error").html(errors.subjective1);
		proceed --;
	} 
	/*else {
		$("#unique_error").html("");
	}
*/
	if (weekendText.length < minimumword) {
	//	$("#weekend_error").html(errors.subjective1);
		proceed--;
	} 
	/*else {
		$("#weekend_error").html("");
	}
*/
	if (friendText.length < minimumword) {
		//$("#friend_error").html(errors.subjective1);
		proceed--;
	} 
	/*else {
		$("#friend_error").html("");
	}
*/
	if (proceed>0){
		$("#error_class").hide();
		trackRegistration('subjective','');
		$.post("register.php", {
			last_page : 'end',
			uniqueText : uniqueText,
			weekendText : weekendText,
			friendText : friendText
		}, function(data, status) {
			if (status == 'success' && data.status == 'success')
				window.location = baseurl + "/register.php";
			else if (data.status == 'foulerror') {
				$("#unique_error").html(data.error_msg_unique);
				$("#weekend_error").html(data.error_msg_weekend);
				$("#friend_error").html(data.error_msg_friend);
			}
		}, 'json');
	}	else{
		$("#error_class").show();
	}
}

function onContinue5() {
	var uniqueText = $("#unique_text").val();
	var weekendText = $("#weekend_text").val();
	var friendText = $("#friend_text").val();

	var proceed = 3;
	
	if (uniqueText.length < minimumword) {
		//$("#unique_error").html(errors.subjective1);
		proceed --;
		//proceed = false;
	} 
	/*else {
		$("#unique_error").html("");
	}
*/
	if (weekendText.length < minimumword) {
	//	$("#weekend_error").html(errors.subjective1);
		proceed --;
	//	proceed = false;
	} 
	/*else {
		$("#weekend_error").html("");
	}
*/
	if (friendText.length < minimumword) {
	//	$("#friend_error").html(errors.subjective1);
		proceed --;
		//proceed = false;
	}
	/*else {
		$("#friend_error").html("");
	}
*/
	if (proceed>0){
		$("#error_class").hide();
		$('#subjective_save').text("Saving...");
		$.post("update.php", {
			param : 'subjective1',
			uniqueText : uniqueText,
			weekendText : weekendText,
			friendText : friendText
		}, function(data, status) {
			if (status == 'success' && data.responseCode == '200') {
				$('#subjective_save').text("Saved");
			} else if (data.status == 'foulerror') {
				$("#unique_error").html(data.error_msg_unique);
				$("#weekend_error").html(data.error_msg_weekend);
				$("#friend_error").html(data.error_msg_friend);
				$('#subjective_save').text("Save");
			}
		}, 'json');
	}
	else{
		$("#error_class").show();
	}
}
