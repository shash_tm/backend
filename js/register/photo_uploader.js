var interval = null;
  window.fbAsyncInit = function() {
  FB.init({
    appId      : fb_api, // App ID
    channelUrl : server + '/templates/channel.html', // Channel File
    oauth      : true,
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true,  // parse XFBML
    version    : 'v2.0'
          });

  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
 //js.src = "//connect.facebook.net/en_US/all.js";
   js.src = "//connect.facebook.net/en_US/sdk.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  
imageCounter = 0;  
function readURL(input) {
	
	document.getElementById('photoerr').innerHTML='';
	var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
    if (input.files && input.files[0]) {
    	if(input.files[0].size>8388608){
    		alert("File is too big to upload. Maximum size can be 8 MB.");
    		return;
    	}
    	if(!acceptFileTypes.test(input.files[0].type)){
    		alert("Only JPG, GIF or PNG format not larger than 8 MB");
    		return;
    	}
    	if(fileReaderApiPresent()){
        var reader = new FileReader();

        reader.onload = function (e) {
        	
        	document.getElementById('phuploadpopup').style.display='block';
    		document.getElementById('phuploadoption').style.display='none';
        	document.getElementById('imageDiv').innerHTML="<img src='"+e.target.result+"' id='photo_pane' class='pane_style' style='max-width:700px;  max-height:350px;'>";
        	
        	$('#photo_pane').load(function() { 
        		
        		var image_jquery=document.getElementById('photo_pane');//$("#photo_pane");
        		//or however you get a handle to the IMG
        		var width = image_jquery.naturalWidth;//width();
        		var height = image_jquery.naturalHeight;//height();
        		
        		if(width<200||height<200) {
        			setMessages("small_image");
        			document.getElementById('phuploadpopup').style.display='none';
        			alert("The minimum picture size should be 200 x 200");
        		return;
        		}
        		showActions("new");
            	 cropPhoto();
            });  
        }
        reader.readAsDataURL(input.files[0]);
    	}
    	
    }
}


function showActions(type){
	$(".tmpcaction").show();
	
	showContinueButton();
	//hide all messages to start with
	//showContinueButton();
	//hide all intially
	hideActionButtons();
	
	//hiding messages
	
	
	if(type=="new"){
		$("#new_img_cancel").show();
		$("#upload_image").show();
		hideContinueButton();
	}
	else if(type=="profile"){
		$("#save").show();
		$("#new_img_cancel").show();
		hideContinueButton();
		
	}
	else if(type=="view"){
		hideContinueButton();
		$("#save_as_profile").show();
		$("#save").show();
		$("#chop").show();
		$("#new_img_cancel").show();
	}else if (type=="fb_profile_pic"){
		$("#new_img_cancel").show();
		$("#upload_image_fromfacebook").show();
		$("#showactionbtn").hide();
		$("#hideactionbtn").show();

		hideContinueButton();
	}else if(type=="image_saving"){
		setImageSavingLoader();
		 $("#upload_image").hide();
		 $("#new_img_cancel").show();
		 $("#showactionbtn").hide();
			$("#hideactionbtn").show();
		 hideContinueButton();
	}else if(type=="rejected"){
		$("#chop").show();
		$("#new_img_cancel").show();
		hideContinueButton();

	}
	else if(type=="import_from_facebook"){
		hideContinueButton();
		
	}
	else if(type=="active_no_profile"){
		hideContinueButton();
		$("#save").show();
		$("#chop").show();
		$("#new_img_cancel").show();
	}
}

function hideActionButtons(){
	$("#new_img_cancel").hide();
	$("#upload_image").hide();
	$("#save_as_profile").hide();
	$("#save").hide();
	$("#chop").hide();
	$("#upload_image_fromfacebook").hide();
}

function setMessages(type){
	document.getElementById('imageDiv').innerHTML="";
	hideActionButtons();
	$("#showactionbtn").show();
	$("#hideactionbtn").hide();
	showContinueButton();
	//activate continue button
	
	if(type=="photoupsave"){
		$('#imageDiv').append($('#photoupsave').html());	
	}
	else if(type=="photosetprofile"){
		$('#imageDiv').append($('#photosetprofile').html());	
	}
	else if(type=="photoupdelete"){
		$('#imageDiv').append($('#photoupdelete').html());	
	}
	else if(type=="upload_stopped"){
		$('#imageDiv').append($('#photoupcancel').html());
	}
	else if(type=="photoupload"){
		$('#imageDiv').append($('#photoupload').html());
	}
	else if(type=="small_image"){
		$('#imageDiv').append($('#small_image').html());
	}
	else if(type=="fbphotoimport"){
		$('#imageDiv').append($('#fbphotoimport').html());
	}
	else{
		$('#imageDiv').append($('#nophoto').html());
	}
}


function cropPhoto(){
	if($("#photo_pane")==null)  {
		return;
	}
	var cropFlag = false;
	//var widthArray=$('#photo_pane').style.width.split("px");
	var img1 = document.getElementById('photo_pane');
	var img = $("#photo_pane");//document.getElementById('photo_pane'); 
	//or however you get a handle to the IMG
	var width = img.width();//img.clientWidth;
	var height = img.height();//img.clientHeight;
	var naturalWidth = img.naturalWidth; 
	var naturalHeight = img.naturalHeight;

	var size=200;
	if(height<width&& height>200) 
		size=height;
	else if(height>=width&&width>200)
		size=width;



	var crop_x1=0;
	var crop_y1=0;
	var crop_x2=170;
	var crop_y2=170;
	if(width>170&&height>170){
		if(width>=height){
			crop_y1=5;
			crop_y2=height-5;
			temp = crop_y2-crop_y1;
			crop_x1 = (width-temp)/2;
			crop_x2 = crop_x1+temp;
		}else{
			crop_x1 = 5;
			crop_x2 = width-5;
			temp = crop_x2-crop_x1;
			crop_y1 = (height - temp)/2;
			crop_y2 = crop_y1+temp;
		}
	}
	$('#photo_pane').Jcrop({
		minSize: [150,150],
//		trueSize: [,size],
		bgOpacity:   0.2,
		setSelect:   [ crop_x1,crop_y1,crop_x2,crop_y2 ],
		aspectRatio: 1,
		onSelect: updateInfo,
		//onRelease:releaseCrop
		//        onChange: updateInfo
	},function() {
		// Store the API in the jcrop_api variable
		jcrop_api = this;
	});		
}

function updateInfo(c) {
	var cropFlag = false;
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#w').val(c.w);
	$('#h').val(c.h);
}

	function releaseCrop() {
		 var cropFlag = false;
        $('#x').val('');
        $('#y').val('');
        $('#w').val('');
        $('#h').val('');
}

function refresh_later(photos,fromDelete){
	if(typeof(fromDelete)==='undefined') fromDelete = false;
	imageCounter=0;
	/*if(null==photos)
		photos=photosAll;
	*/
	if (null == photos)
		{
		showEmptyThumbnail();
		return;
		}
	
	if(photos) {
		photosAll=photos;
		for(var i=0;i<photos.length;i++) {
			if(i==0)addThumbNail(photos[i],true);
			else addThumbNail(photos[i]);
		}if(photos.length>=1 && fromDelete){
			for(var j=photos.length-1;j<5;j++){
				$("#testdiv_"+j).html('');
				$("#testdiv_"+j).append("<div class='addmore'><i class='icon-plus'></i><br>Add More</div>");
			}
		}
	}
	
	$("#testDiv1").show();
		
}

/*
 * show empty pane
 */
function showEmptyThumbnail(){
	$("#testDiv1").html('');
	$("#testDiv1").append("<div class='profilepicsec' id='testdivprofile'><div class='safhake addmore'><i class='icon-plus'></i></div></div>");
	for(var i=0;i<5;i++){
		$("#testDiv1").append("<div class='addotherpicsec' id='testdiv_"+i+"'><div class='addmore'><i class='icon-plus'></i><br>Add More</div></div>");
	}
}


function addThumbNail(photo_details,bool){
	//check for fb profile
	if(typeof(bool)==='undefined') bool = false;
	
	
	var id="thumbNail_"+photo_details.photo_id;
	var thumbnaildiv_id="thumbNailDiv_"+photo_details.photo_id;
	//var appendCode = "<div>"+"<img src='"+photo_details.thumbnail+"' id='"+id+"'/>";
	var appendCode = "";
	if(photo_details.is_profile=='yes') {
		appendCode = "<div>"+"<img src='"+photo_details.name+"' id='"+id+"'/>";
		//appendCode+="<img id='profile_star' class='phprim' src='images/register/pfavbtn.png'/>";
	}else{
		appendCode = "<div>"+"<img src='"+photo_details.thumbnail+"' id='"+id+"'/>";
	}
		
		if(photo_details.status == 'active' && photo_details.admin_approved == 'no'){
			appendCode = appendCode +'<div id="'+thumbnaildiv_id+'" class="undrmodphoto"><p>Under Moderation</p></div>'; 
		}
		else if(photo_details.status == 'Rejected'){
			appendCode = appendCode +'<div id="'+thumbnaildiv_id+'" class="undrmodphoto"><p>'+photo_details.status+'</p></div>'; 
		}
		else
			appendCode = appendCode +'<div id="'+thumbnaildiv_id+'" class="undrmodphoto"><p>'+photo_details.status+'</p></div>';
	appendCode = appendCode + "</div>"; 
	
	if(photo_details.is_profile=='yes') {
		$("#testdivprofile").html('');
		$("#testdivprofile").append(appendCode);
	}else{
		$("#testdiv_"+imageCounter).html('');
		$("#testdiv_"+imageCounter).append(appendCode);
		imageCounter++;
	}
	//$("#testDiv1").append(appendCode);

	$("#"+thumbnaildiv_id).click(function(){
		var idArray=this.id.split("_");
		$("#thumbNail_"+idArray[1]).click();
		});
	
	$("#"+id).click(function(){
		//setting current photo id
		if(photo_details.status=="from_fb"){
			window.current_photo_id = "from_fb";
		}else
			window.current_photo_id=photo_details.photo_id;
		if(!edit){
		//	trackRegistration('photo',"thumbnail_clicked",false);
		}
	//this.style.background='#4293f0';	
	
	if(photo_details.is_profile=='yes') {
		showActions("profile");
	}
	else if(photo_details.status=='from_fb'){
		//showActions("fb_profile_pic");	
	}
	else if(photo_details.status=='Rejected'){
		showActions("rejected");	
	}
	else if(photo_details.can_profile=='no'){
		showActions("active_no_profile");
	}
	else{
		showActions("view");
	}
	document.getElementById('imageDiv').innerHTML="<img src='"+photo_details.name+"' class='imgloader pane_style' id='photo_pane' style='max-width:700px;  max-height:350px;'>";
	document.getElementById('phuploadpopup').style.display='block';
	document.getElementById('photo_pane').onload=function() {
		 cropPhoto();
		};
	
	});
	
	if(photo_details.status=='from_fb' && bool){
		$("#"+id).click();
		return;
	}
	
	
}


function processCurrentImage(type,fb_data){
	if(type=="save"){
		//alert(window.current_photo_id);

		var x=$("#x").val();
		var y=$("#y").val();
		var w=$("#w").val();
		var h=$("#h").val();
		var img = document.getElementById('photo_pane'); 
		var image_jquery=$("#photo_pane");
		//or however you get a handle to the IMG
		var width = image_jquery.width();
		var height = image_jquery.height();
		var naturalWidth = img.naturalWidth;
		var naturalHeight = img.naturalHeight;
		$.ajax({
			url: "photo.php",
			type: "post",
			data: {id:window.current_photo_id,x:x,y:y,w:w,h:h,action:'save',width:width,height:height,naturalWidth:naturalWidth,
				naturalHeight:naturalHeight	},
				dataType: "json",
				success : function (response) {
					document.getElementById('phuploadpopup').style.display='none';
					refresh_later(response);
					if(fb_data!=null&&fb_data=="upload_image_fromfacebook")
					setMessages("fbphotoimport");
					else{
						setMessages("photoupsave");
					}
				}
		});
		setImageSavingLoader();
	}
	if(type=="set_as_profile"){
		$.ajax({
			url: "photo.php",
			type: "post",
			data: {id:window.current_photo_id,action:'set_as_profile'},
				dataType: "json",
				success : function (response) {
					document.getElementById('phuploadpopup').style.display='none';
					refresh_later(response);
					setMessages("photosetprofile");
				}
		});
		setImageSavingLoader();
	}
	else if(type=='delete'){
		if(window.current_photo_id=="from_fb"){
			//showEmptyFrame();			
			return;
		}
		
		$.ajax({
			url: "photo.php",
			type: "post",
			data: {id:window.current_photo_id,action:'delete'},
				dataType: "json",
				success : function (response) {
					photosAll=response;
					document.getElementById('phuploadpopup').style.display='none';
					refresh_later(photosAll,true);
					setMessages("photoupdelete");
				}
		});
		setImageSavingLoader();

	}
	
	else if(type=='saveFb'){
		$.ajax({
			url: "photo.php",
			type: "post",
			data: {data:fb_data,action:'saveFb'},
				dataType: "json",
				success : function (response) {
					document.getElementById('phuploadpopup').style.display='none';
					refresh_later(response);
					setMessages("fbphotoimport");
					//trackRegistration('photo','facebook_upload_successful',false);
				}
		});
		//setImageSavingLoader();

	}
}

function setImageSavingLoader(){
	document.getElementById('imageDiv').innerHTML="<img id='load' src='images/register/loader_photo.gif' class='mrg brdr' />";
}



function imageLoader(){
	document.getElementById('imageDiv').innerHTML="<img id='load' src='images/register/loader_photo.gif' class='mrg brdr' />";
}



function facebook_redirect(response) {
	
	trackRegistration(activity,'fb_popup','success',false);
	time1 = new Date();
	
var facebook_flag = false;

if(photosAll==null){
	maxPhoto= 0;
}else
	maxPhoto= photosAll.length;
//$('#fbuploadpopup').show();
$('#phuploadoption').hide();
//$("#right_panel").hide();
//$("#photoStatus").html('');
//$("#imageDiv").show();
$("#phuploadpopup").show();
document.getElementById('imageDiv').innerHTML="<img id='load' src='images/ajax_loader.gif' width='100' class='mrg brdr' />";

trackRegistration(activity,'fb_server_call_start','success',false);

$.post("trustbuilder.php", {
	checkFacebook : 'check',
	token : response.authResponse.accessToken,
	connected_from : connectedFrom
}, function(data) {
	if(data.status=='success'){
		trackRegistration(activity,'fb_server_call','success',false);
		
		if(data.status=='new'){
			facebook_flag = true;
		}
		document.getElementById('imageDiv').innerHTML='';
		document.getElementById('imageDiv').innerHTML='<div id="jfmps-container" align="center"></div><div id="jfmps-submit-button" class="phactionbtn"><i class="icon-upload-alt"></i>Upload Photo</div>';
		$("#jfmps-submit-button").show();
		showActions("import_from_facebook");
		$('#jfmps-container').jfmps({
						noAlbumImagesText:  'You have no images in this album.',
						noAlbumsText:  'You do not have any albums.',
						submitCallback: function(jsonData){
		var toSubmit=[];
		var i=0;
		var jsonData=JSON.parse(jsonData);
		for(key in jsonData)  {
						toSubmit[i]=jsonData[key][0].source+" "+jsonData[key][0].height+" "+jsonData[key][0].width;
						i++;
						}
		time1 = new Date();
		showActions("image_saving");
		processCurrentImage("saveFb",toSubmit);
		if(isRegistration){
			$("#continue").show();
			$("#nothnx").hide();
		}
		trackRegistration(activity,'fb_photo_save','success',false);
		//trackRegistration('photo','upload_from_facebook_clicked',false);
		 },
						imageSubmitButton: $('#jfmps-submit-button'),
						//maxPhotosSelected: 10,
						maxPhotosSelected: 6-maxPhoto,
						numAlbumColumns: 4,
						numPhotosColumns: 6,
						debug: false
					});
		$(".tmpcaction").hide();
		}else if(data.status=='fail'){
			trackRegistration(activity,'fb_server_call',data.error,false);
			//document.getElementById('imageDiv').innerHTML=data.error;
			document.getElementById('imageDiv').innerHTML='';
			document.getElementById('photoerr').innerHTML=data.error;
		}else if(data.status=='refresh'){
			trackRegistration(activity,'fb_server_call','access token invalid',false);
			document.getElementById('imageDiv').innerHTML='<p style="color:#fff; padding:25% 0 0 0; margin:0 auto;">Your facebook session is timed out. Please login again.</p>';
			/*FB.init({
			appId      : fb_api, // App ID
			channelUrl : cdn + '/templates/channel.html', // Channel File
			oauth      : true,
			status     : true, // check login status
			cookie     : true, // enable cookies to allow the server to access the session
			xfbml      : true  // parse XFBML
			});
			proceed();*/
			FB.api('/me', {fields: 'last_name'}, function(response) {
				
			});
		}
}, "json");
return;

//alert(document.getElementById('imageDiv').innerHTML);



}


function cancelFacebook(){
	flag = 0;
	window.top.$('#upload_iframe').attr('src','about:blank');
	showActions("upload_stopped");
	setMessages("upload_stopped");
	//trackRegistration('photo','add_from_facbook_canceled',false);
	
}


function proceed() {
	document.getElementById('photoerr').innerHTML='';
	if(!edit){
		//trackRegistration('photo','add_from_facebook_clicked',false);
	}
	$('.phactionbtn').hide();
	time1 = new Date();
	trackRegistration(activity,'fb_click','',false);
	
		//if(navigator.userAgent.match('CriOS')){
		//	window.location = 'https://www.facebook.com/dialog/oauth?client_id='+fb_api+'&redirect_uri='+baseurl+'/facebook_photo.php&response_type=token&scope='+facebookScope;
		//}else{
                FB.getLoginStatus(function(response) {
                        if(response.status === 'connected') {
                                facebook_redirect(response);
                        } else if (response.status === 'not_authorized') {
                                FB.login(
                                                function(response) {
                                                        facebook_redirect(response);
                                }, {
                                        scope : facebookScope
                                });
                        } else {
                                FB.login(function(response) {
                                        if (response.authResponse) {
                                                facebook_redirect(response);
                                        }else{
                                        	trackRegistration(activity,'fb_popup_cancel','',false);
                                        }
                                }, {
                                        scope : facebookScope
                                });
                        }
                });
			//}
        }

function goToNextPage(){
	time1 = new Date();
	$.get("register.php?save=photo",function(data,status) {
        if(status=='success'){
        	if(photosAll){
        		trackRegistration(activity,'photo_save_call','success',false);
        	}
        	setTimeout(function(){
        		if(isRegistration)
            		window.location=baseurl+ "/trustbuilder.php?from_registration=true";
            	else
            		window.location=baseurl+ "/matches.php";
        	},200);
        	
        }
        });	
}

function onContinue( obj ){
	if(photosAll){
		goToNextPage();
		//go to next page
	}
	else{
		$("#diffframe").show();
		//alert("Hey Buddy.Upload 1 photo atleast");
	}
}

function openwhoc()
{
document.getElementById("whosee").style.display="block";
}

function closewhoc()
{
document.getElementById("whosee").style.display="none";
}

function openptips()
{
document.getElementById("ptips").style.display="block";
}

function closeptips()
{
document.getElementById("ptips").style.display="none";
}

function hideContinueButton(){
	$(".continuert").hide();
	$(".diffcontinue").show();
	$("#showactionbtn").hide();
	$("#hideactionbtn").show();
}

function showContinueButton(){
	$(".diffcontinue").hide();	
	$(".continuert").show();
	$("#showactionbtn").show();
	$("#hideactionbtn").hide();
}

function fileReaderApiPresent(){
	return typeof FileReader != "undefined";
}

$(document).ready(function() {
	if(isRegistration){
		activity = "register_photo";
	}else
		activity = "photo";
	trackRegistration(activity,'page_load','');
	
	if(window.localStorage){
		window.localStorage['IS_DATA_ATTRIBUTES_DIRTY']="1";
	}
	$('#testDiv1').on('click', '.addmore', function(e){
			document.getElementById('phuploadpopup').style.display='none';
			document.getElementById('phuploadoption').style.display='block';
	});
	
	//assigngin on click functions to save/delete/set_as_profile
	$('#save_as_profile').click(function (e) {
		//trackRegistration('photo','save_as_profile_clicked',false);
		processCurrentImage("set_as_profile");
	});
	
	$('#save').click(function (e) {
		//trackRegistration('photo','save_image_clicked',false);
		processCurrentImage("save");
	});
	
	$('#chop').click(function (e) {
		//trackRegistration('photo','delete_image_clicked',false);
        //console.log(e);
		processCurrentImage("delete");
	});
	
	$('#upload_image_fromfacebook').click(function (e) {
		//trackRegistration('photo','upload_image_fromfacebook_clicked',false);
		processCurrentImage("save","upload_image_fromfacebook");
	});
	
	setMessages("nophoto");
	refresh_later(photosAll);
	
	$("#add_from_computer").change(function(){
		time1 = new Date();
		trackRegistration(activity,'add_from_computer_clicked','',false);
	    readURL(this);
	});
	
	/**
	 * binding file uploader
	 */
	$('#add_from_computer_form').bind('fileuploadsubmit', function (e, data) {
		if(fileReaderApiPresent()==false){
			//send the file directly
			 data.formData = {action:'add_from_computer_without_file_reader_api'};	 
			    showActions("image_saving");
			    return;
		}
	    // The example input, doesn't have to be part of the upload form:
		var x=$("#x").val();
		var y=$("#y").val();
		var w=$("#w").val();
		var h=$("#h").val();
		var img = document.getElementById('photo_pane'); 
		var image_jquery=$("#photo_pane");
		//or however you get a handle to the IMG
		var width = image_jquery.width();
		var height = image_jquery.height();
		var naturalWidth = img.naturalWidth;
		var naturalHeight = img.naturalHeight;
	    data.formData = {x:x,y:y,w:w,h:h,action:'add_from_computer',width:width,height:height,naturalWidth:naturalWidth,
	    		naturalHeight:naturalHeight	};	 
	    showActions("image_saving");
	});
	
	var jqXHR=null;
	time1 = new Date();
	$('#add_from_computer_form').fileupload({
      dataType: 'json',
      singleFileUploads:1,
      maxNumberOfFiles: 1,
		maxFileSize: 8388608,//4000000000,
		acceptFileTypes:'/(\.|\/)(gif|jpe?g|png)$/i',
		add: function (e, data) {
			//hideContinueButton();
				$("#upload_image").unbind("click");
				$("#upload_image").on('click', function () {
	        
					//trackRegistration('photo','upload_image_computer_clicked',false);
					jqXHR=data.submit().error(function (jqXHR, textStatus, errorThrown) {
						if (errorThrown === 'abort') {
							alert('File Upload has been canceled');
							//refresh_later();
							//showActions("upload_stopped");
						}
					});
					
					document.getElementById('imageDiv').innerHTML="<div class='progresspup'><div class='progresspupbar' id='progress'></div></div>";
				});
				if(fileReaderApiPresent()==false){
					$("#upload_image").click();
				}
	    },
	    	
	    progressall: function (e, data) {
	    	
	    	var progress = parseInt(data.loaded / data.total * 100, 10);
	    	console.log(progress);
	    	$('#progress').css(
	            "width",
	            progress + '%'
	        );
	    },
	    done: function (e, data) {
	    	$('#progress').css(
		            'width',
		            0 + '%'
		        );
	    	trackRegistration(activity,'add_from_computer_upload','success',false);
	    	document.getElementById('phuploadpopup').style.display='none';
	    	if(isRegistration){$("#continue").show();
	    	$("#nothnx").hide();}
	    	refresh_later(data.result);
	    	showActions("upload_successful");
	    	setMessages("photoupload");
	        },
        url:baseurl+'/photo.php',      
        
    });
	
	
	$('#cancelPhoto').click(function (e) {
    	//trackRegistration('photo','new_img_cancel_clicked',false);
		if(null!=jqXHR){
	    jqXHR.abort();
		}
	/*	$('#progress .bar').css(
	            'width',
	            0 + '%'
	        );
		*/
		//showContinueButton();
		
		//refresh_later();
		
	});

	//no thanks skipping the page
	$("#nothnx").click(function (e) {
		//goToNextPage();
		onContinue();
	});
	
	$("#nothnx1").click(function (e) {
		time1 = new Date();
		trackRegistration(activity,'photo_skip','',false);
		goToNextPage();
	});
	
	
	
	
	//$("#photo").change(addFromComputer);
	
});