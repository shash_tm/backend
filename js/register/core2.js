$(document).ready(function() {
	
	if(!edit)
	var pageInd = $.cookie("pageInd");

	if( pageInd == undefined ){
		pageInd = 0;
	}
	$("div.wrapper").find("div#container").eq( pageInd ).show(); //show starting page

	//Progess Bar
	$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: 15
	});



//	jquery chozen plugin
	$(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});

//prefilling spouse height range as per chosen height 

$("#feet,#inch").change(function() {
if(edit)
return;
var feet=parseInt($("#feet").val()),inch=parseInt($("#inch").val());

if(feet===NaN||inch===NaN)
return;
var height=12*feet+inch,startHeight,endHeight,startFeet,endFeet,startInch,endInch;
if(gender=='M') {
	startHeight=height-10;
	endHeight=height;
}
else if(gender=='F') {
	startHeight=height;
	endHeight=height+10;
}
if(startHeight<4*12)
{
	startHeight=4*12;
}
if(endHeight>8*12)
{
	endHeight=8*12;
}
startFeet=Math.floor((startHeight)/12);
endFeet=Math.floor((endHeight)/12);
startInch=startHeight%12;
endInch=endHeight%12;
var toFill='';
toFill='feetSpouseStart';
$("#"+toFill).chosen();
if(startFeet<10)
$("#"+toFill+" option[value='0" + startFeet + "']").attr("selected",'selected');
else $("#"+toFill+" option[value='" + startFeet + "']").attr("selected",'selected');
$("#"+toFill).trigger("liszt:updated");

toFill='inchSpouseStart';
$("#"+toFill).chosen();
if(startInch<10)
$("#"+toFill+" option[value='0" + startInch + "']").attr("selected",'selected');
else $("#"+toFill+" option[value='" + startInch + "']").attr("selected",'selected');
$("#"+toFill).trigger("liszt:updated");

toFill='feetSpouseEnd';
$("#"+toFill).chosen();
if(endFeet<10)
$("#"+toFill+" option[value='0" + endFeet + "']").attr("selected",'selected');
else $("#"+toFill+" option[value='" + endFeet + "']").attr("selected",'selected');
$("#"+toFill).trigger("liszt:updated");

toFill='inchSpouseEnd';
$("#"+toFill).chosen();
if(endInch<10)
$("#"+toFill+" option[value='0" + endInch + "']").attr("selected",'selected');
else $("#"+toFill+" option[value='" + endInch + "']").attr("selected",'selected');
$("#"+toFill).trigger("liszt:updated");

});

if(!edit) {
if($.cookie("TM_S2_Skin_tone_spouse")==undefined) {
$("label:has(input[name='skin_tone_spouse_notMatter'])").addClass('checked');
$("input[name='skin_tone_spouse_notMatter']").attr('checked','checked');
}
if($.cookie("TM_S2_Body_type_spouse")==undefined){ 
$("label:has(input[name='body_type_spouse_notMatter'])").addClass('checked');
$("input[name='body_type_spouse_notMatter']").attr('checked','checked');
}
if($.cookie("TM_S2_Smoking_status_spouse")==undefined) {
$("label:has(input[name='smoking_status_spouse_notMatter'])").addClass('checked');
$("input[name='smoking_status_spouse_notMatter']").attr('checked','checked');
}
if($.cookie("TM_S2_Drinking_status_spouse")==undefined) {
$("label:has(input[name='drinking_status_spouse_notMatter'])").addClass('checked');
$("input[name='drinking_status_spouse_notMatter']").attr('checked','checked');
}
if($.cookie("TM_S2_Food_status_spouse")==undefined) {
$("label:has(input[name='food_status_spouse_notMatter'])").addClass('checked');
$("input[name='food_status_spouse_notMatter']").attr('checked','checked');
}
}
        //ChangeSmokingStatus
        $(".smokingimage2, .smokingimage3, .smokingimage4").hide();
        $(".never,.rarely,.occasionaly,.regularly").hide();
	$(".sm input").click(function(){
		$("#smo .nothing").hide();
		
           if ($(this).val() === "Never")
                {
                        $(".smokingimage1,.never").show();
                        $(".smokingimage2, .smokingimage3, .smokingimage4, .rarely, .occasionaly, .regularly").hide();
                }
          else if ($(this).val() === "Socially")
                {
                        $(".smokingimage2, .rarely").show();
                        $(".smokingimage1, .smokingimage3, .smokingimage4, .never, .occasionaly, .regularly").hide();
                }
          else if ($(this).val() === "Occasionally")
                {
                        $(".smokingimage3, .occasionaly").show();
                        $(".smokingimage1, .smokingimage2, .smokingimage4, .never, .rarely, .regularly").hide();
                }
                else if ($(this).val() === "Often")
                {
                        $(".smokingimage4, .regularly").show();
                        $(".smokingimage1, .smokingimage2, .smokingimage3, .never, .rarely, .occasionaly").hide();
                }
        }); 

        //ChangeDrinkingStatus
        $(".drinkingimage2, .drinkingimage3, .drinkingimage4").hide();
        $(".neverDrink,.rarelyDrink,.occasionalyDrink,.regularlyDrink").hide();
        $(".dr input").click(function(){
		$("#dro .nothing").hide();
	if ($(this).val() === "Never")
                {
                        $(".drinkingimage1, .neverDrink").show();
                        $(".drinkingimage2, .drinkingimage3, .drinkingimage4, .rarelyDrink, .occasionalyDrink, .regularlyDrink").hide();
                }
          else if ($(this).val() === "Socially")
                {
                        $(".drinkingimage2, .rarelyDrink").show();
                        $(".drinkingimage1, .drinkingimage3, .drinkingimage4, .neverDrink, .occasionalyDrink, .regularlyDrink").hide();
                }
          else if ($(this).val() === "Occasionally")
                {
                        $(".drinkingimage3, .occasionalyDrink").show();
                        $(".drinkingimage1, .drinkingimage2, .drinkingimage4, .neverDrink, .rarelyDrink, .regularlyDrink").hide();
                }
                else if ($(this).val() === "Often")
                {
                        $(".drinkingimage4, .regularlyDrink").show();
                        $(".drinkingimage1, .drinkingimage2, .drinkingimage3, .neverDrink, .rarelyDrink, .occasionalyDrink").hide();
                }
        });
	
	

	$("[name='skin_tone_spouse_notMatter'],[name='body_type_spouse_notMatter'], [name='food_status_spouse_notMatter'], [name='drinking_status_spouse_notMatter'], [name='smoking_status_spouse_notMatter']").change(function() {
		var toReduce='';
		if(this.name=='body_type_spouse_notMatter')		
		toReduce='body_type_spouse';
		else if(this.name=='food_status_spouse_notMatter')
		toReduce='food_status_spouse';
		else if(this.name=='drinking_status_spouse_notMatter')
                toReduce='drinking_status_spouse';
		else if(this.name=='smoking_status_spouse_notMatter')
                toReduce='smoking_status_spouse';
		 else if(this.name=='skin_tone_spouse_notMatter')
                toReduce='skin_tone_spouse';
                var value=$("input[name="+this.name+"]:checked").val();
                if(value != undefined)  
                        $("[name="+toReduce+"]").prop("checked", false);
		else $("[name="+this.name+"]").prop("checked", true);


        });
	$("[name='skin_tone_spouse'],[name='body_type_spouse'],[name='food_status_spouse'],[name='drinking_status_spouse'],[name='smoking_status_spouse'] ").change(function() {	
		var toReduce='';
		if(this.name=='body_type_spouse')
                toReduce='body_type_spouse_notMatter';
                else if(this.name=='food_status_spouse')
                toReduce='food_status_spouse_notMatter';
                else if(this.name=='drinking_status_spouse')
                toReduce='drinking_status_spouse_notMatter';
                else if(this.name=='smoking_status_spouse')
                toReduce='smoking_status_spouse_notMatter';
		else if(this.name=='skin_tone_spouse')
                toReduce='skin_tone_spouse_notMatter';
		var value=$("input[name="+this.name+"]:checked").val();
		if(value!=undefined)// && $("input[name="+toReduce+"]:checked").val()!=undefined)
		$("[name="+toReduce+"]").prop("checked", false);	
		else $("[name="+toReduce+"]").prop("checked", true);
		
});

	refreshPage( pageInd );    
});

//fetch values from cookies and show values filled in all pages
function refreshPage( ind ){
	var objParent = $("div.wrapper").find("div#container").eq(ind);

}
//saving values to cookies,putting validations and refreshing page for next show up
function onContinue2( obj ){
var toSave=false;
var page = '';
	var objParent = $(obj).parents("div#container");
	if(edit) 
	var ind=obj;
	else	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if( ind == 0 ){
		page = 'height';
	var heightFoot=$("#feet").val();
	var heightInch=$("#inch").val();
	var heightSpouseFeetStart=$("#feetSpouseStart").val();
	var heightSpouseInchStart=$("#inchSpouseStart").val();
	var heightSpouseFeetEnd=$("#feetSpouseEnd").val();
        var heightSpouseInchEnd=$("#inchSpouseEnd").val();

var error=false;

if(heightFoot == '' || heightInch == '') {
error=true;
document.getElementById("heightError").innerHTML=errors.height;
}
else document.getElementById("heightError").innerHTML='';
if(heightSpouseFeetStart == '' || heightSpouseInchStart == '' || heightSpouseFeetEnd=='' || heightSpouseInchEnd == '') {
error=true;
document.getElementById("heightSpouseError").innerHTML=errors.height_spouse;
}
else if((heightSpouseFeetStart>heightSpouseFeetEnd) || (heightSpouseFeetStart==heightSpouseFeetEnd && heightSpouseInchStart > heightSpouseInchEnd)) {
error=true;
document.getElementById("heightSpouseError").innerHTML=errors.height_invalid;
}
else document.getElementById("heightSpouseError").innerHTML='';
if(error)
return;
		
		if(!edit) {
		$.cookie("TM_S1_HeightFoot",heightFoot, { expires: 7 });
		$.cookie("TM_S1_HeightInch", heightInch, { expires: 7 });
		$.cookie("TM_S1_HeightSpouseFeetStart", heightSpouseFeetStart, { expires: 7 });
		$.cookie("TM_S1_HeightSpouseInchStart", heightSpouseInchStart, { expires: 7 });
		$.cookie("TM_S1_HeightSpouseFeetEnd", heightSpouseFeetEnd, { expires: 7 });
		$.cookie("TM_S1_HeightSpouseInchEnd", heightSpouseInchEnd, { expires: 7 });
		}
		else {
			$.ajax({
			    url: baseurl+"/update.php",
			    type: "POST",
			    dataType:'json',
			    data: {param:'trait1',heightFoot:heightFoot,heightInch:heightInch,heightSpouseFeetStart:heightSpouseFeetStart,heightSpouseInchStart:heightSpouseInchStart,heightSpouseFeetEnd:heightSpouseFeetEnd,heightSpouseInchEnd:heightSpouseInchEnd},
			    beforeSend: function() { 
			    	$('#height_save').text("Saving...");
			    	},
			    success: function(data) {
			    	if(data.responseCode==200){
			        $('#height_save').text("Saved");
			    }else{
			    	$('#height_save').text("Not Saved");
			    }
			    	
			        // ...
			    }
			});
	//	$.post(baseurl+"/update.php",{param:'trait1',heightFoot:heightFoot,heightInch:heightInch,heightSpouseFeetStart:heightSpouseFeetStart,heightSpouseInchStart:heightSpouseInchStart,heightSpouseFeetEnd:heightSpouseFeetEnd,heightSpouseInchEnd:heightSpouseInchEnd},function(data,status) {    
      //          });

		}

	}
//else if( ind == 9){
//var skin_tone=$("input[name='skin_tone']:checked").val();
//if($("input[name='skin_tone_spouse_notMatter']:checked").length==0) {
//var skin_tone_spouse=new Array();
//                $("input[name='skin_tone_spouse']:checked").each(function() {
//                       skin_tone_spouse.push($(this).val());
//                });
//}
//else var skin_tone_spouse=$("input[name='skin_tone_spouse_notMatter']:checked").val();
//
//		var error=false;
//if(skin_tone==undefined) {
//error=true;
//document.getElementById("skin_tone_Error").innerHTML=errors.skin_tone;
//}
//else document.getElementById("skin_tone_Error").innerHTML="";
//if(skin_tone_spouse=='') {
//error=true;
//document.getElementById("skin_tone_Spouse_Error").innerHTML=errors.skin_tone_spouse;
//}
//else document.getElementById("skin_tone_Spouse_Error").innerHTML="";
//
//		if(error)
//			return;
//		if(!edit) {
//		$.cookie("TM_S2_Skin_tone", skin_tone, { expires: 7 });
//		$.cookie("TM_S2_Skin_tone_spouse", skin_tone_spouse, { expires: 7 });
//		}
//		else {
//	
//			$.ajax({
//			    url: baseurl+"/update.php",
//			    type: "POST",
//			    dataType:'json',
//			    data:{param:'trait2',skin_tone:skin_tone,skin_tone_spouse:skin_tone_spouse},
//			beforeSend: function() { 
//			    	$('#complexion_save').text("Saving...");
//			    	},
//			    success: function(data) {
//			    	if(data.responseCode==200){
//			        $('#complexion_save').text("Saved");
//			    }else{
//			    	$('#complexion_save').text("Not Saved");
//			    }
//			    	
//			        // ...
//			    }
//			});
//			
//			
//			//$.post(baseurl+"/update.php",{param:'trait2',skin_tone:skin_tone,skin_tone_spouse:skin_tone_spouse},function(data,status) {
////                });
//
//		}
//	}
else if( ind == 1){
	page = 'appearance';
	var body_type=$("input[name='body_type']:checked").val();
if($("input[name='body_type_spouse_notMatter']:checked").length==0) {
var body_type_spouse=new Array();
                $("input[name='body_type_spouse']:checked").each(function() {
                       body_type_spouse.push($(this).val());
                }); 
}
else var body_type_spouse=$("input[name='body_type_spouse_notMatter']:checked").val();
var error=false;
if(body_type==undefined) {
error=true;
document.getElementById("body_type_Error").innerHTML=errors.body;
}
else document.getElementById("body_type_Error").innerHTML="";
if(body_type_spouse=='') {
error=true;     
document.getElementById("body_type_Spouse_Error").innerHTML=errors.body_spouse;
}
else document.getElementById("body_type_Spouse_Error").innerHTML="";

                if(error)
                        return;
		if(!edit) {
                $.cookie("TM_S2_Body_type", body_type, { expires: 7 });
                $.cookie("TM_S2_Body_type_spouse", body_type_spouse, { expires: 7 });
		}
else {

	
	$.ajax({
	    url: baseurl+"/update.php",
	    type: "POST",
	    dataType:'json',
	    data:{param:'trait3',body_type:body_type,body_type_spouse:body_type_spouse},
	beforeSend: function() { 
	    	$('#bodytype_save').text("Saving...");
	    	},
	    success: function(data) {
	    	if(data.responseCode==200){
	        $('#bodytype_save').text("Saved");
	    }else{
	    	$('#bodytype_save').text("Not Saved");
	    }
	    	
	        // ...
	    }
	});
	
	
	//$.post(baseurl+"/update.php",{param:'trait2',skin_tone:skin_tone,skin_tone_spouse:skin_tone_spouse},function(data,status) {
//        });


//$.post(baseurl+"/update.php",{param:'trait3',body_type:body_type,body_type_spouse:body_type_spouse},function(data,status) {
     //           });
}

	}
	else if(ind==2) {
		page = 'smoking';
	var smoking_status=$("input[name='smoking_status']:checked").val();
if($("input[name='smoking_status_spouse_notMatter']:checked").length==0) {
	var smoking_status_spouse=new Array();
                $("input[name='smoking_status_spouse']:checked").each(function() {
                       smoking_status_spouse.push($(this).val());
                });
}
else var smoking_status_spouse=$("input[name='smoking_status_spouse_notMatter']:checked").val();

var error=false;

if(smoking_status==undefined) {
error=true;
document.getElementById("smoking_status_Error").innerHTML=errors.smoke;
}
else document.getElementById("smoking_status_Error").innerHTML="";
if(smoking_status_spouse=='') {
error=true;
document.getElementById("smoking_status_spouse_Error").innerHTML=errors.smoke_spouse;
}
else document.getElementById("smoking_status_spouse_Error").innerHTML="";

                if(error)
                        return;
		if(!edit) {
                $.cookie("TM_S2_Smoking_status", smoking_status, { expires: 7 });
                $.cookie("TM_S2_Smoking_status_spouse", smoking_status_spouse, { expires: 7 });
		}
		else {
			$.ajax({
			    url: baseurl+"/update.php",
			    type: "POST",
			    dataType:'json',
			    data:{param:'trait4',smoking_status:smoking_status,smoking_status_spouse:smoking_status_spouse},
			beforeSend: function() { 
			    	$('#smoking_save').text("Saving...");
			    	},
			    success: function(data) {
			    	if(data.responseCode==200){
			        $('#smoking_save').text("Saved");
			    }else{
			    	$('#smoking_save').text("Not Saved");
			    }
			    	
			        // ...
			    }
			});
			
//$.post(baseurl+"/update.php",{param:'trait4',smoking_status:smoking_status,smoking_status_spouse:smoking_status_spouse},function(data,status) {
  //              });
}


	} 
else if(ind==3) {
	page = 'drinking';
        var drinking_status=$("input[name='drinking_status']:checked").val();
if($("input[name='drinking_status_spouse_notMatter']:checked").length==0) {
        var drinking_status_spouse=new Array();
                $("input[name='drinking_status_spouse']:checked").each(function() {
                       drinking_status_spouse.push($(this).val());
                }); 
}
else var drinking_status_spouse=$("input[name='drinking_status_spouse_notMatter']:checked").val();
var error=false;
if(drinking_status==undefined) {
error=true;
document.getElementById("drinking_status_Error").innerHTML=errors.drink;
}
else document.getElementById("drinking_status_Error").innerHTML="";
if(drinking_status_spouse=='') {
error=true;
document.getElementById("drinking_status_spouse_Error").innerHTML=errors.drink_spouse;
}
else document.getElementById("drinking_status_spouse_Error").innerHTML="";

                if(error)
                        return;
		if(!edit) {
                $.cookie("TM_S2_Drinking_status", drinking_status, { expires: 7 });
                $.cookie("TM_S2_Drinking_status_spouse", drinking_status_spouse, { expires: 7 });
		}
                else {
                	$.ajax({
        			    url: baseurl+"/update.php",
        			    type: "POST",
        			    dataType:'json',
        			    data: {param:'trait5',drinking_status:drinking_status,drinking_status_spouse:drinking_status_spouse},
        			    beforeSend: function() { 
        			    	$('#drinking_save').text("Saving...");
        			    	},
        			    success: function(data) {
        			    	if(data.responseCode==200){
        			        $('#drinking_save').text("Saved");
        			    }else{
        			    	$('#drinking_save').text("Not Saved");
        			    }
        			    	
        			        // ...
        			    }
        			});
//$.post(baseurl+"/update.php",{param:'trait5',drinking_status:drinking_status,drinking_status_spouse:drinking_status_spouse},function(data,status) {
  //                });
}


}
else if(ind==4) {
	page = 'food';
        var food_status=$("input[name='food_status']:checked").val();
if($("input[name='food_status_spouse_notMatter']:checked").length==0) {
        var food_status_spouse=new Array();
                $("input[name='food_status_spouse']:checked").each(function() {
                       food_status_spouse.push($(this).val());
                }); 
}
else var food_status_spouse=$("input[name='food_status_spouse_notMatter']:checked").val();
var error=false;
if(food_status==undefined) {
error=true;
document.getElementById("food_status_Error").innerHTML=errors.food;
}
else document.getElementById("food_status_Error").innerHTML="";
if(food_status_spouse=='') {
error=true;
document.getElementById("food_status_spouse_Error").innerHTML=errors.food_spouse;
}
else document.getElementById("food_status_spouse_Error").innerHTML="";
                if(error)
                        return;
		if(!edit) {
                $.cookie("TM_S2_Food_status", food_status, { expires: 7 });
                $.cookie("TM_S2_Food_status_spouse", food_status_spouse, { expires: 7 });
		toSave=true;
		}
                else {
                	$.ajax({
        			    url: baseurl+"/update.php",
        			    type: "POST",
        			    dataType:'json',
        			    data: {param:'trait6',food_status:food_status,food_status_spouse:food_status_spouse},
        			    beforeSend: function() { 
        			    	$('#food_save').text("Saving...");
        			    	},
        			    success: function(data) {
        			    	if(data.responseCode==200){
        			        $('#food_save').text("Saved");
        			    }else{
        			    	$('#food_save').text("Not Saved");
        			    }
        			    	
        			        // ...
        			    }
        			});
//$.post(baseurl+"/update.php",{param:'trait6',food_status:food_status,food_status_spouse:food_status_spouse},function(data,status) {
  //           });
}
	
		}
		if(!edit)
		$.cookie("pageInd", ind + 1, { expires: 7 });
        $(obj).parents("div.container").eq(0).hide();
        $(obj).parents("div.container").eq(0).next().show();

        if(!edit){
    		trackRegistration(page,'');
    	}
        
	if(toSave) {	
	$.get("register.php?save=trait",function(data,status) {
                if(status=='success')
                window.location=baseurl + "/register.php";
                });

}


	refreshPage( ind + 1);
}

function refreshPage( ind ){
        var objParent = $("div.wrapper").find("div#container").eq(ind);
}

//funtion to take back pages
function onBack2( obj ){
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if(!edit)
	$.cookie("pageInd", ind - 1, { expires: 7 });    
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).prev().show();
	refreshPage( ind - 1 );
}

$(window).load(function() {
	setTimeout(function(){
	    if(performance.timing!=undefined){
	      var t = performance.timing;
	      var dnsLookup = (t.domainLookupStart-t.domainLookupEnd)/1000;
	      var pageload = (t.loadEventEnd-t.navigationStart)/1000;
	      var pageCreate = (t.loadEventEnd-t.responseEnd)/1000;
	      var networklatency = (t.responseEnd-t.fetchStart)/1000;
	      trackPerformance("trait_dnsLookup",dnsLookup);
	      trackPerformance("trait_networkLatency",networklatency);
	      trackPerformance("trait_pageLoad",pageload);
	      trackPerformance("trait_pageCreate",pageCreate);
	    }
	},0);
});

