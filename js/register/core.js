$(document).ready(function() {

	// $.removeCookie("cookie");
	if(!edit)
		var pageInd = $.cookie("pageInd");

	if( pageInd == undefined ){
		pageInd = 0;
	}
	$("div.wrapper").find("div#container").eq( pageInd ).show(); //show starting page


	//Progess Bar
	$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: 5
	});

//	jquery chozen plugin
	$(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});
	$(".chzn-select-dob").chosen({no_results_text:" "});
	if(!edit){
		if($.cookie("TM_S2_ReligionSpouse")==undefined) {
			$("label:has(input[name='religionSpouseDoesNotMatter'])").addClass('checked');
			$("input[name='religionSpouseDoesNotMatter']").attr('checked','checked');
		}
	}
//	$("input[name='choseBirth'][value='sameAs']").prop("checked",true);
//	$('.selectother').hide();

	$("[name='choseBirth']").change(function() {
		value=$("[name='choseBirth']:checked").val();
		if(value!='sameAs') {
			$('.selectother').show();
		}
		else {
//			$('.selectother').hide();
		}
	});
	if(!edit) {
		if($.cookie("TM_Marital_status_spouse")==undefined) {
			$("label:has(input[name='marital_status_spouse'][value='Never Married'])").addClass('checked');
			$("input[name='marital_status_spouse'][value='Never Married']").attr('checked','checked');
		}
		if($.cookie("TM_Marital_status")==undefined) {
			$("label:has(input[name='marital_status'][value='Never Married'])").addClass('checked');
			$("input[name='marital_status'][value='Never Married']").attr('checked','checked');
			$("#haveChildrenDiv").hide();
		}
	}
	$("input[name='marital_status']").change(function() {
		var marital_status=$("[name='marital_status']:checked").val();
		if(marital_status=='Never Married')  
			$("#haveChildrenDiv").hide();
		else
			$("#haveChildrenDiv").show();

	});

	$("#whenBornYear").change(function() {
		var year=new Date().getFullYear()-Number($("#whenBornYear").val()),start=0,end=0;
		if(gender=='M') {
		if(year-7<18)
		start=18;
		else start=year-7;

		if(year+1>70)
		end=70;
		else end=year+1;
		}
		else if(gender=='F') {
		if(year-1<21)
		start=21;
		else start=year-1;

		if(year+7>70)
		end=70;
		else end=year+7;
		}
		$('#lookingAgeStart').chosen();
		$("#lookingAgeStart option[value='" + start  + "']").attr("selected",'selected');
		$('#lookingAgeStart').trigger("liszt:updated");
		$('#lookingAgeEnd').chosen();
		$("#lookingAgeEnd option[value='" + end  + "']").attr("selected",'selected');
		$('#lookingAgeEnd').trigger("liszt:updated");
	});
		
	prefillDateOfBirth();
	/*
//	change age range of spouse as per own age
	$("#age").change(function() {
		if(edit) 
			return;
		var year=Number($("#age").val()),start=0,end=0;
//		var year=new Date().getFullYear()-Number($("#whenBornYear").val()),start=0,end=0;
		if(gender=='M') {
			if(year-5<18)
				start=18;
			else start=year-5;

			if(year+1>70)
				end=70;
			else end=year+1;
		}
		else if(gender=='F') {
			if(year-1<21)
				start=21;
			else start=year-1;

			if(year+5>70)
				end=70;
			else end=year+5;
		}
		$('#lookingAgeStart').chosen();
		$("#lookingAgeStart option[value='" + start  + "']").attr("selected",'selected');
		$('#lookingAgeStart').trigger("liszt:updated");
		$('#lookingAgeEnd').chosen();
		$("#lookingAgeEnd option[value='" + end  + "']").attr("selected",'selected');
		$('#lookingAgeEnd').trigger("liszt:updated");

	});
*/

//	caste dropdown starts
//	$("[name='religion']").change(function() {
//		var value=$("input[name='religion']:checked").val();
//		if(value>5) {
//			$("#my_caste").hide();
//
//		}
//		else $("#my_caste").show();
//		var caste=JSON.parse(casteAll);
//		$('#caste').chosen();
//		document.getElementById('caste').innerHTML='';
//		if(caste[value]!=undefined) {
//			$("#caste").append("<option value=''></option>");
//			for(var i=0;i<caste[value].length;i++) 
//				$("#caste").append("<option value='"+value+"-" + caste[value][i]['id'] +"' >" +caste[value][i]['name'] +"</option>");
//		}
//		$('#caste').trigger("liszt:updated");
//	});
//	caste dropdown filling ends

//	changing spouse religion's on selecting religion=doesn't matter
	$("[name='religionSpouseDoesNotMatter']").change(function() { 
		var religionSpouseDoesNotMatter=$("#religionAndCasteSpouse input[name='religionSpouseDoesNotMatter']:checked").val();
		if(religionSpouseDoesNotMatter != undefined) {
			$("#spouse_caste").hide();
			$("[name='religionSpouse']").prop("checked", false);
			document.getElementById('casteSpouse').innerHTML='';
		}
		else $("[name='religionSpouseDoesNotMatter']").prop('checked',true);

	});

//	caste dropdown for spouse filling starts
	$("[name='religionSpouse']").change(function() {
//		var need=false;
//		var casteSpouse=$("#casteSpouse").val();
		document.getElementById('casteSpouse').innerHTML="";
		var religionSpouse=new Array();
		$("#religionAndCasteSpouse input[name='religionSpouse']:checked").each(function() {
			if($(this).val()<=5)
				need=true;	
			religionSpouse.push($(this).val());
		});
		if(religionSpouse=='')
			$("[name='religionSpouseDoesNotMatter']").prop("checked", true);
		if(religionSpouse!='') {	
//			if(need==true)
//				$("#spouse_caste").show();
//			else $("#spouse_caste").hide();
			$("[name='religionSpouseDoesNotMatter']").prop("checked", false);
		}
		return ;
		var caste=JSON.parse(casteAll);
		var value='',str='';
		$('#casteSpouse').chosen();

		for(var i=0;i<religionSpouse.length;i++) {
			value=religionSpouse[i];

			if(caste[value]!=undefined) {
				str+="<optgroup label='" + $("input[name='religionSpouse'][value="+value+"]").next().html()+"' >";
				for(var j=0;j<caste[value].length;j++)
					str+="<option value='"+value+"-" + caste[value][j]['id'] + "'>" +caste[value][j]['name']+ "</option>";
				str+="</optgroup>";			
			}
		}
		$("#casteSpouse").append(str);
		if(casteSpouse!=undefined)
			for(var k=0;k<casteSpouse.length;k++)
				$("#casteSpouse option[value='" + casteSpouse[k]  + "']").prop("selected",true);
		$('#casteSpouse').trigger("liszt:updated");

	});

	$('body').on('change',"#stayCountry,#spouseCountry,#birthCountry",function() {});
	$('body').on('change',"#stayCountry,#spouseCountry,#birthCountry",function() {
		var change=this.id;
		if(this.id=='birthCountry') {
			var country_id=$("#birthCountry").val();
			var toFill='birthState';
			$("#birthCity").html('');
			$("#birthCity").trigger("liszt:updated");
			//var birth_state=$.cookie("TM_S3_BirthState");
		}
		else if(this.id=='stayCountry') {
			var country_id=$("#stayCountry").val();
			var toFill='stayState';
			$("#stayCity").html('');
			$("#stayCity").trigger("liszt:updated");
			//var stay_state=$.cookie("TM_S3_StayState");
		}
		else if(this.id=='spouseCountry') {
			var countriesSeleted=$("#spouseCountry").val();
			$("#spouseCity").html('');
			$("#spouseCity").trigger("liszt:updated");
			countriesSeleted=$("#spouseCountry").val();
			var stateSelected=$("#spouseState").val();
			if(countriesSeleted!=null&&countriesSeleted!='')
				var country_id=countriesSeleted;
			var toFill='spouseState';
			if(stateSelected!=null&&stateSelected!=''&&stateSelected!=0)
				var spouseState=stateSelected;
		}
		if(country_id==undefined||country_id=='other' ) {
			$("#"+toFill).html('');
			$("#"+toFill).trigger("liszt:updated");
			return;
		}		
		if(toFill=='spouseState'&&(spouseState==''||spouseState==undefined||spouseState==0))
			document.getElementById(toFill).innerHTML="";
		else document.getElementById(toFill).innerHTML='';
		var async = false;
		if(!edit){
			var async = true;
		}
		$.ajax({async:async,url:"geolocations.php?country_id="+country_id,success:function(dataJson){
			var data=JSON.parse(dataJson);
			var str='';
			str="<option value=''></option>";
			var current_label=data[0]['country_id'];
			str+="<optgroup label='"+$('#'+change+" option[value='"+current_label+"']").html()+"'>";
			for(var i=0;i<data.length;i++) {
				if(current_label!=data[i]['country_id']) {
					current_label=data[i]['country_id'];
					str+="</optgroup>";
					str+="<optgroup label='"+$('#'+change+" option[value='"+current_label+"']").html()+"'>";
				}
				str+="<option value="+data[i]['country_id']+"-"+data[i]['state_id']+">"+data[i]['name']+"</option>";

			}
			str+="</optgroup>";
			//str+="<optgroup label='Other'><option value='-1'>Other</option></optgroup>";
			$('#' + toFill).append(str);
			if(toFill=='spouseState'&&spouseState!=undefined)
				for(var k=0;k<spouseState.length;k++)
					$("#" + toFill+" option[value='" + spouseState[k]  + "']").prop("selected",true);

			$('#' + toFill).trigger("liszt:updated");
			if($("#sameAsAbove").attr('checked')){
				$("#birthState").val($("#stayState").val());
				$('#birthState').trigger("liszt:updated");
				$("#birthState").change();
			}
		}
		});
		
		if(this.id=='stayCountry'){
			sameAsAbove();
		}
	});
	if(!edit){
		$("#birthCountry option[value=113]").prop('selected',true);
		$("#birthCountry").trigger("liszt:updated");
		$("#birthCountry").change();

		$("#stayCountry option[value=113]").prop('selected',true);
		$("#stayCountry").trigger("liszt:updated");
		$("#stayCountry").change();

		$("#spouseCountry option[value=113]").prop('selected',true);
		$("#spouseCountry").trigger("liszt:updated");
		$("#spouseCountry").change();
	}
//	city dropdown filling for all
	$("#stayState, #birthState, #spouseState").change(function() {
		var pref=false;
		var change=this.id;
		if(this.id=='stayState') {
			var country_id=$("#stayCountry").val();
			var toFill='stayCity';
			if($("#stayState").val()!=undefined&&$("#stayState").val()!=null)  {
				var state_idArray=$("#stayState").val().split("-");
				var state_id=state_idArray[1];
			}
			else return;	
			$('#' + toFill).html('');
			$('#' + toFill).trigger("liszt:updated");
		}
		else if(this.id=='birthState') {
			var country_id=$("#birthCountry").val();
			if($("#birthState").val()!=undefined&&$("#birthState").val()!=null) {
				var state_idArray=$("#birthState").val().split("-");
				var state_id=state_idArray[1];
			}
			else return;
			var toFill='birthCity';
			$('#' + toFill).html('');
			$('#' + toFill).trigger("liszt:updated");
		}
		else if(this.id=='spouseState') {
			var country_id=$("#spouseCountry").val();
			pref=true;
			var states=$("#spouseState").val();	
			states=$("#spouseState").val();
			if($("#spouseCity").val()!=null)
				var spouseCity=$("#spouseCity").val();
			var toFill='spouseCity';
			$('#' + toFill).html('');
			$('#' + toFill).trigger("liszt:updated");
			if(states!=null) {
				var state_values=states;
			}
			if(state_values==undefined ||(state_values!=undefined && state_values==0))  {
				$('#' + toFill).html("");
				$('#' + toFill).trigger("liszt:updated");
				return;
			}
			
			if(spouseCity==undefined||spouseCity==0)
				document.getElementById(toFill).innerHTML="";
			else document.getElementById(toFill).innerHTML="";
			var state_id=[];
			for(var i=0;i<state_values.length;i++) {
				var id=state_values[i].split("-");
				state_id.push(id[1]);
			}

		}

		$.ajax({async:false,url:"geolocations.php?state_id="+state_id+"&country_id="+country_id+"&pref="+pref,success:function(dataJson){
			
//			console.log(dataJson);
			try{
				var data=JSON.parse(dataJson);
			
			var str='';
			str+="<option value=''></option>";
			if(data!='') {

				var current_label=+data[0]['country_id']+"-"+data[0]['state_id'];
				var current_opt=data[0]['display_name'];
				if(pref)
					str+="<optgroup label='"+$("#"+change+" option[value='"+current_label+"']").html()+"'>";
				var j=0;
				if(!pref) {
					if(data[0]['name']!=data[0]['display_name']) {
						str+="<optgroup label='"+data[0]['display_name']+"'>";
						var current_opt=data[0]['display_name'];
						for(var j=0;j<data.length&&data[j]['name']!=data[j]['display_name'];j++) {	
							if(current_opt!=data[j]['display_name']) {
								current_opt=data[j]['display_name'];
								str+="</optgroup><optgroup label='"+data[j]['display_name']+"'>";	
							}
							str+="<option value="+data[j]['country_id']+"-"+data[j]['state_id']+"-"+data[j]['city_id']+">"+data[j]['name']+"</option>";

						}
						str+="</optgroup>";
					}



				}
				for(var i=j;i<data.length;i++) {
					if(pref&&current_label!=data[i]['country_id']+"-"+data[i]['state_id']) {
						current_label=data[i]['country_id']+"-"+data[i]['state_id'];
						str+="</optgroup>";
						str+="<optgroup label='"+$("#"+change+" option[value='"+current_label+"']").html()+"'>";
					}
					str+="<option value="+data[i]['country_id']+"-"+data[i]['state_id']+"-"+data[i]['city_id']+">"+data[i]['name']+"</option>";
				}
			}
			if(pref)
				str+="</optgroup>";
			//if(!pref)
				//str+="<option value='-1'>Other</option>";
			$('#' + toFill).append(str);
			if(toFill=='spouseCity'&&spouseCity!=undefined)
				for(var k=0;k<spouseCity.length;k++)
					$("#" + toFill+" option[value='" + spouseCity[k]  + "']").prop("selected",true);
			
			$('#' + toFill).trigger("liszt:updated");
			
			
		}catch(err){
			console.log(err);
		}
		
		if($("#sameAsAbove").attr('checked')){
			$("#birthCity").val($("#stayCity").val());
			$('#birthCity').trigger("liszt:updated");
		}
		
		}
			
		});
		
		if(this.id=='stayState') {
			sameAsAbove();
		}
	});
	
	$("#stayCity").change(function() {
		sameAsAbove();
	});


	$("[name='marital_status_spouse_notMatter']").change(function() {
		var toReduce='';
		if(this.name=='marital_status_spouse_notMatter')
			toReduce='marital_status_spouse';

		var value=$("input[name="+this.name+"]:checked").val();
		if(value != undefined)
			$("[name="+toReduce+"]").prop("checked", false);
		else $("[name="+this.name+"]").prop("checked", true);

	});

	$("[name='marital_status_spouse']").change(function() {
		var toReduce='';
		if(this.name=='marital_status_spouse')
			toReduce='marital_status_spouse_notMatter';

		var value=$("input[name="+this.name+"]:checked").val();
		if(value!=undefined && $("input[name="+toReduce+"]:checked").val()!=undefined)
			$("[name="+toReduce+"]").prop("checked", false);
		else if(value==undefined)
			$("[name="+toReduce+"]").prop("checked", true);


	});
	
	$("#sameAsAbove").click(function(){
		sameAsAbove();
	});


	refreshPage( pageInd );    
});

//fetch values from cookies and show values filled in all pages
function refreshPage( ind ){
	var objParent = $("div.wrapper").find("div#container").eq(ind);

}

function sameAsAbove(){
	if($("#sameAsAbove").attr('checked')){
		$("#birthCountry").val($("#stayCountry").val());
		$("#birthCountry").change();
		
		$("#birthState").val($("#stayState").val());
		$("#birthState").change();
		
		$("#birthCity").val($("#stayCity").val());
		
		$('#birthCountry').trigger("liszt:updated");
		$('#birthState').trigger("liszt:updated");
		$('#birthCity').trigger("liszt:updated");
	}
}

//saving values to cookies,putting validations and refreshing page for next show up
function onContinue( obj ){
	var toSave=false;
	var page = "";
	if(edit)
		var ind=obj;
	else {
		var objParent = $(obj).parents("div#container");
		var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	}
	if( ind == 0 ){
		page = "age";
		var whenBornDay = $("#whenBornDay").val();
		var whenBornMonth = $("#whenBornMonth").val();
		var whenBornYear = $("#whenBornYear").val();
		if( whenBornDay == '' || whenBornMonth == '' ||whenBornYear == '' ) {
			var error=true;
			document.getElementById('whenBorn').innerHTML=errors.age;
		}
		else if(!validateDate(whenBornDay,whenBornMonth,whenBornYear))        {
			error=true;
			document.getElementById('whenBorn').innerHTML='Invalid birth date...';
		}
		else document.getElementById('whenBorn').innerHTML='';
		//var age=$("#age").val();
		var marital_status=$("[name='marital_status']:checked").val();
		var haveChildren=$("[name='haveChildren']:checked").val();
		var lookingAgeStart = $("#lookingAgeStart").val();
		var lookingAgeEnd = $("#lookingAgeEnd").val();
		if($("input[name='marital_status_spouse_notMatter']:checked").length==0) {
			var marital_status_spouse=new Array();
			$("input[name='marital_status_spouse']:checked").each(function() {
				marital_status_spouse.push($(this).val());
			});
		}
		else var marital_status_spouse=$("[name='marital_status_spouse_notMatter']:checked").val();
		if(marital_status=='Never Married') {
			haveChildren=undefined;
			if(!edit)
				$.cookie("TM_HaveChildren",'',{ expires: 0 });
		}
	//	var error=false;
	/*	if(age=='') {
			error=true;
			document.getElementById('age_Error').innerHTML=errors.age;
		}
		else document.getElementById('age_Error').innerHTML='';
	*/
		if(marital_status==undefined) {
			error=true;
			document.getElementById('marital_Error').innerHTML=errors.marital;
		}
		else document.getElementById('marital_Error').innerHTML="";
		if(marital_status!='Never Married'&&haveChildren==undefined) {
			error=true;
			document.getElementById('haveChildren_Error').innerHTML=errors.children;
		}
		else document.getElementById('haveChildren_Error').innerHTML='';
		if(marital_status_spouse==undefined||marital_status_spouse=='') {
			error=true;
			document.getElementById('marital_spouse_Error').innerHTML=errors.marital_spouse;
		}
		else document.getElementById('marital_spouse_Error').innerHTML='';

		if(lookingAgeStart== '' || lookingAgeEnd == '') {
			var error=true;
			document.getElementById('ageRange').innerHTML=errors.age_spouse;
		}
		else if( lookingAgeStart > lookingAgeEnd ) {
			var error=true;
			document.getElementById('ageRange').innerHTML=errors.age_invalid;
		}
		else document.getElementById('ageRange').innerHTML='';
		if(error)
			return;

		if(!edit) {
			$.cookie("TM_S1_WhenBornDay", whenBornDay, { expires: 7 });
			$.cookie("TM_S1_WhenBornMonth", whenBornMonth, { expires: 7 });
			$.cookie("TM_S1_WhenBornYear", whenBornYear, { expires: 7 });
			
			//$.cookie("TM_S1_Age",age, { expires: 7 });
			$.cookie("TM_Marital_status",marital_status,{ expires: 7 });
			$.cookie("TM_HaveChildren",haveChildren,{ expires: 7 });
			$.cookie("TM_S1_LookingAgeStart", lookingAgeStart, { expires: 7 });
			$.cookie("TM_S1_LookingAgeEnd", lookingAgeEnd, { expires: 7 });
			$.cookie("TM_Marital_status_spouse",marital_status_spouse,{ expires: 7 });
		}
		else {
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'demo1',marital:marital_status,haveChildren:haveChildren,lookingAgeStart:lookingAgeStart,lookingAgeEnd:lookingAgeEnd,marital_status_spouse:marital_status_spouse},
				beforeSend: function() { 
					$('#birthplace_save').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						$('#birthplace_save').text("Saved");
					}else{
						$('#birthplace_save').text("Not Saved");
					}

					// ...
				}
			});


			//$.post(baseurl+"/update.php",{param:'demo1',age:age,marital:marital_status,haveChildren:haveChildren,lookingAgeStart:lookingAgeStart,lookingAgeEnd:lookingAgeEnd,marital_status_spouse:marital_status_spouse},function(data,status)	{
			//	});


		}
	}else if( ind == 1){
		page = "religion";
		var religion=$("#religionAndCaste input[type='radio']:checked").val();//undefined
		var caste=$("#caste").val();
		var religionSpouse=new Array();
		$("#religionAndCasteSpouse input[name='religionSpouse']:checked").each(function() { 
			religionSpouse.push($(this).val());
		});
		var religionSpouseDoesNotMatter=$("#religionAndCasteSpouse input[name='religionSpouseDoesNotMatter']:checked").val();
		var casteSpouse=$("#casteSpouse").val();
		var motherTongue=$("#motherTongue").val();
		var spouseMotherTongue=$("#spouseMotherTongue").val();
                
		var error=false;
		if(motherTongue=='') {
			error=true;
			document.getElementById('motherTongueError').innerHTML=errors.tongue;
		}
		else document.getElementById('motherTongueError').innerHTML='';

		if(religion== undefined) {
			error=true;
			document.getElementById('religionError').innerHTML=errors.religion;
		}
		else document.getElementById('religionError').innerHTML='';
		if(religionSpouse=='' && religionSpouseDoesNotMatter==undefined) {
			error=true;
			document.getElementById('religionSpouseError').innerHTML=errors.religion_spouse;

		}
		else document.getElementById('religionSpouseError').innerHTML='';
		if(error)
			return;
		if(!edit) {
			$.cookie("TM_S2_Religion", religion, { expires: 7 });
			$.cookie("TM_S2_Caste", caste, { expires: 7 });
			$.cookie("TM_S2_ReligionSpouse", religionSpouse, { expires: 7 });
			$.cookie("TM_S2_CasteSpouse", casteSpouse, { expires: 7 });
			$.cookie("TM_S3_MotherTongue", motherTongue, { expires: 7 });
			$.cookie("TM_S3_SpouseMotherTongue", spouseMotherTongue, { expires: 7 });
		}
		else {
			if(religionSpouse==''){
				religionSpouse.push(null);
			}
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'demo2',religion:religion,caste:caste,motherTongue:motherTongue,religionSpouse:religionSpouse,casteSpouse:casteSpouse,spouseMotherTongue:spouseMotherTongue},
				beforeSend: function() { 
					$('#relgion_save').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						$('#relgion_save').text("Saved");
					}else{
						$('#relgion_save').text("Not Saved");
					}

					// ...
				}
			});

			//		$.post(baseurl+"/update.php",{param:'demo2',religion:religion,caste:caste,motherTongue:motherTongue,religionSpouse:religionSpouse,casteSpouse:casteSpouse,spouseMotherTongue:spouseMotherTongue},function(data,status) {
//			});

		}
	}else if( ind == 2){
		page = "stay";
		var stayCountry=$("#stayCountry").val();
		var stayState=$("#stayState").val();
		var stayCity=$("#stayCity").val();
		var chosen=$("input[name='choseBirth']:checked").val();

		if(chosen=='sameAs') {
			var birthCountry=stayCountry;
			var birthState=stayState;
			var birthCity=stayCity;
		}
		else {
			var birthCountry=$("#birthCountry").val();
			var birthState=$("#birthState").val();
			var birthCity=$("#birthCity").val();

		}
		var spouseCountry=$("#spouseCountry").val();
		var spouseState=$("#spouseState").val();
		var spouseCity=$("#spouseCity").val();
		var error=false;
		if(stayCountry=='' ||stayCountry==undefined) {
			error=true;
			document.getElementById('stayCountryError').innerHTML=errors.stay_country;
		}
		else document.getElementById('stayCountryError').innerHTML='';

		if(birthCountry==undefined||birthCountry=='') {
			$("label:has(input[name='choseBirth'][value='sameAs'])").removeClass("checked");
			$("input[name='choseBirth'][value='sameAs']").prop("checked",false);
			$("label:has(input[name='choseBirth'][value='other'])").addClass("checked");
			$("input[name='choseBirth'][value='other']").prop("checked",true);

			$('.selectother').show();	
			error=true;
			document.getElementById('birthCountryError').innerHTML=errors.born_country;
		}
		else document.getElementById('birthCountryError').innerHTML='';
		
		 var sameasbirth = false;
//         alert(sameasbirth);
		
		if((birthState==''||birthState==null) && !sameasbirth){
			$("input[name='choseBirth']").prop("checked",false);

			$('.selectother').show();
			error=true;
			document.getElementById('birthStateError').innerHTML=errors.born_state;
		}
		else document.getElementById('birthStateError').innerHTML='';
		if((birthCity==''||birthCity==null) && !sameasbirth){
			$("input[name='choseBirth'][value='sameAs']").prop("checked",false);

			$('.selectother').show();
			error=true;
			document.getElementById('birthCityError').innerHTML=errors.born_city;
		}
		else document.getElementById('birthCityError').innerHTML='';

		if(stayState==''||stayState==null){
			$('.selectother').show();
			error=true;
			document.getElementById('stayStateError').innerHTML=errors.stay_state;
		}
		else document.getElementById('stayStateError').innerHTML='';
		if(stayCity==''||stayCity==null){
			$('.selectother').show();
			error=true;
			document.getElementById('stayCityError').innerHTML=errors.stay_city;
		}
		else document.getElementById('stayCityError').innerHTML='';

		if(error)
			return;
		if(!edit) {
			$.cookie("TM_S3_BirthCountry", birthCountry, { expires: 7 });
			$.cookie("TM_S3_BirthState", birthState, { expires: 7 });
			$.cookie("TM_S3_BirthCity", birthCity, { expires: 7 });
			$.cookie("TM_S3_StayCountry", stayCountry, { expires: 7 });
			$.cookie("TM_S3_StayState", stayState, { expires: 7 });
			$.cookie("TM_S3_StayCity", stayCity, { expires: 7 });	
			$.cookie("TM_S3_SpouseCountry", spouseCountry, { expires: 7 });
			$.cookie("TM_S3_SpouseState", spouseState, { expires: 7 });
			$.cookie("TM_S3_SpouseCity", spouseCity, { expires: 7 });
			toSave=true;
		}
		else {
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'demo3',birthCountry:birthCountry,birthState:birthState,birthCity:birthCity,stayCountry:stayCountry,stayState:stayState,stayCity:stayCity,spouseCountry:spouseCountry,spouseState:spouseState,spouseCity:spouseCity},
				beforeSend: function() { 
					$('#livingspace_save').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						$('#livingspace_save').text("Saved");
					}else{
						$('#livingspace_save').text("Not Saved");
					}

					// ...
				}
			});


//			$.post(baseurl+"/update.php",{param:'demo3',birthCountry:birthCountry,birthState:birthState,birthCity:birthCity,stayCountry:stayCountry,stayState:stayState,stayCity:stayCity,spouseCountry:spouseCountry,spouseState:spouseState,spouseCity:spouseCity},function(data,status) {
//			});

		}
	}
	if(!edit)
		$.cookie("pageInd", ind + 1, { expires: 7 });
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).next().show();
	
	if(!edit){
		trackRegistration(page,'');
	}

	if(toSave) { 
		$.get("register.php?save=demographic",function(data,status) {

			if(status=='success')
				window.location=baseurl + "/register.php";;	
		});
	}

	refreshPage( ind + 1);
}

//funtion to take back pages
function onBack( obj ){
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if(!edit) 
		$.cookie("pageInd", ind - 1, { expires: 7 });    
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).prev().show();
//	refreshPage( ind - 1 );
}



$(window).load(function() {
               //console.log("Time until everything loaded: ", Date.now()-timeStart);
setTimeout(function(){
    if(performance.timing!=undefined){
      var t = performance.timing;
      var dnsLookup = (t.domainLookupStart-t.domainLookupEnd)/1000;
      var pageload = (t.loadEventEnd-t.navigationStart)/1000;
      var pageCreate = (t.loadEventEnd-t.responseEnd)/1000;
      var networklatency = (t.responseEnd-t.fetchStart)/1000;
      trackPerformance("demographic_dnslookup",dnsLookup);
      trackPerformance("demographic_networkLatency",networklatency);
      trackPerformance("demographic_pageLoad",pageload);
      trackPerformance("demographic_pageCreate",pageCreate);
    }
//console.log(t.loadEventEnd - t.responseEnd);
//console.log(t.responseEnd-t.fetchStart);
//console.log(t.loadEventEnd-t.navigationStart);
  },0);
});
