$(document).ready(function() {
	trackRegistration(psycho_step,"page_load",'');
	//back = false;
	var pageInd;
	if(psycho_step=='psycho1'){
		pageInd = $.cookie("psycho2_pageInd");
		if(pageInd!=undefined){
			//pageInd = 0;
			deleteCookies();
		}
		pageInd = $.cookie("psycho1_pageInd");
		if(pageInd == undefined)
			pageInd = 0;
	}else if(psycho_step=="psycho2"){
		pageInd = $.cookie("psycho1_pageInd");
		if(pageInd!=undefined){
			//pageInd = 15;
			deleteCookies();
		}
		pageInd = $.cookie("psycho2_pageInd");
		if(pageInd == undefined)
			pageInd = 10;
	}
	
	/*if(!edit)
		var pageInd = $.cookie("pageInd");

	if( pageInd == undefined ){
		if(psycho_step=='psycho1')
			pageInd = 0;
		else if(psycho_step=='psycho2')
			pageInd = 15;
		else 
			pageInd = 0;
	}*/
	$("#back_"+pageInd).hide();
	
	var physcoShown = $.cookie('physcoIntro');
	
	if(physcoShown != undefined && physcoShown=='shown'){
		$("#physcoQuestions").show();
		$("#physcoIntro").hide();
		$("body").find("div#div_id_"+pageInd).show();
	}else{
		$("#physcoQuestions").hide();
		$("#physcoIntro").show();
	}
	
	$("#physcoIntroCnt").click(function(){
		$("#physcoQuestions").show();
		$("#physcoIntro").hide();
		$.cookie('physcoIntro', 'shown', { expires: 7 });
		$("body").find("div#div_id_"+pageInd).show();
		
		//trackRegistration('physcoIntro','');
	});
	
	$("#physcoCancel").click(function(){
		time1 = new Date();
		trackRegistration(psycho_step,"introCancel",'',false);
		setTimeout(function(){
			if(rdUrl!='' && rdUrl!=undefined)
				window.location="http://"+rdUrl;
			else{
				if(fromRegister)
					window.location=baseurl + "/personality.php?from_trust=true";
				else
					window.location=baseurl + "/personality.php";
			}
		},200);
		//trackRegistration('physcoIntro','');
	});
	
	$("#continue").click(function(){
		deleteCookies();
		$("#diffframe2").hide();
		time1 = new Date();
		trackRegistration(psycho_step,"abort",'',false);
		setTimeout(function(){
			if(rdUrl!='' && rdUrl!=undefined)
				window.location="http://"+rdUrl;
			else{
				if(fromRegister)
					window.location=baseurl + "/personality.php?from_trust=true";
				else
					window.location=baseurl + "/personality.php";
			}
		},200);
	});

	$("[name^=pa]").click(function(event){
		if(psycho_step=='psycho1'){
			psychoRadioClick(event);
		}
	});
	
	$("[name^=pa]").on('change',function(event){
		if(psycho_step=='psycho2'){
			setTimeout(function() {
				psychoSelect(event);
			}, 200);
			//psychoSelect(event);
		}
	});
});

function psychoRadioClick(e){
	id = e.target.name.split('_');
	var question_id = id[1];
	
	if(e.target.value==-1){
		$.cookie("pa[" + question_id + "][" + que[question_id][0] + "]",0,{expires:7});
		$.cookie("pa[" + question_id + "][" + que[question_id][1] + "]",0,{expires:7});
	}else{
		$.cookie("pa[" + question_id + "][" + que[question_id][0] + "]",e.target.value,{expires:7});
		$.cookie("pa[" + question_id + "][" + que[question_id][1] + "]",4-e.target.value,{expires:7});
	}
	var pageInd = $.cookie("psycho1_pageInd");
	if(pageInd==undefined)
		pageInd = 0;
	
	if(pageInd==9 && psycho_step=='psycho1'){
		savePyschoQuestions();
	}else{
		$("#diff_continue_"+pageInd).hide();
		$("#continue_"+pageInd).show();
	}
		//updatePage(pageInd);
}

function psychoSelect(e){
	$.cookie(e.target.name,e.target.value,{expires:7});
	var primary_div = $(e.target).closest('li');
	var sibling_div = primary_div.siblings('li')[0];
	var sibling_div1 = primary_div.siblings('li')[1];
	var select_sibling = $(sibling_div).find("div.qselect-style").children();
	var select_sibling1 = $(sibling_div1).find("div.qselect-style").children();
	var select_primary = primary_div.find("div.qselect-style").children();
	var sib_val = select_sibling.val();
	var sib_val1 = select_sibling1.val();
	var pri_val = select_primary.val();
	//return;
	//radio_primary.closest("[value='"+e.target.value+"']").parent("li").removeClass("unselectnum deactivenum").addClass("activenum");
	
	var index = $.cookie("psycho2_pageInd");
	if( index == undefined ){
		index = 10;
	}
	if(!(sib_val==undefined || sib_val=='' || pri_val==undefined || pri_val=='' || sib_val1==undefined || sib_val1=='')){
		//error = false;
		$("#continue_"+index).show();
		$("#diff_continue_"+index).hide();
	}else{
		$("#continue_"+index).hide();
		$("#diff_continue_"+index).show();
		//error = true;
	}
	
	disableRadio(e,e.target.value);
}

function disableRadio(e,value){
	var index = $.cookie("psycho2_pageInd");
	if( index == undefined ){ 
		index = 10;
	}
	
	 primary_table = $(e.target).closest('li');
	
	if(primary_table.is('[secondary]') || primary_table.is('[tertary]')){
		//primary_table.attr("primary","true");
	}else
		primary_table.attr("primary","true");
	 sibling_table = primary_table.siblings('li')[0];
	 sibling_table1 = primary_table.siblings('li')[1];
	
	if($(sibling_table).is('[primary]') || $(sibling_table).is('[tertary]')){
		//
	}else
		$(sibling_table).attr("secondary","true");
	if($(sibling_table1).is('[primary]') || $(sibling_table1).is('[secondary]')){
		//
	}else{
		$(sibling_table1).attr("tertary","true");
	}
	
	 radio = $(sibling_table).find("div.qselect-style").children();
	 radio1 = $(sibling_table1).find("div.qselect-style").children();
	 primary = primary_table.find("div.qselect-style").children();
	//primary_table.find("input[type=radio]").closest('input[type=radio]').parent("li").removeClass("activenum").addClass("unselectnum");
	//primary_table.find("input[type=radio]").closest('[value="'+value+'"]').parent("li").removeClass("unselectnum").addClass("activenum");
	
	if(primary_table.is('[primary]')){
		switch(value){
		case "1":
			$(radio).find('[value="1"]').removeAttr('selected');
			$(radio1).find('[value="1"]').removeAttr('selected');
			$(radio).find('[value=" "]').attr("disabled","true").prop("selected",'selected');
			$(radio1).find('[value=" "]').attr("disabled","true").prop("selected",'selected');
			$(radio).find('[value="1"]').attr("disabled", "true");
			$(radio1).find('[value="1"]').attr("disabled", "true");
			$(radio).find('[value="2"],[value="3"]').attr("disabled",false).prop("selected",'');
			$(radio1).find('[value="2"],[value="3"]').attr("disabled",false).prop("selected",'');
			$("#continue_"+index).hide();
			$("#diff_continue_"+index).show();
			break;
		case "2":
			$(radio).find('[value="2"]').removeAttr('selected');
			$(radio1).find('[value="2"]').removeAttr('selected');
			$(radio).find('[value=" "]').attr("disabled","true").prop("selected",'selected');
			$(radio1).find('[value=" "]').attr("disabled","true").prop("selected",'selected');
			$(radio).find('[value="2"]').attr("disabled", "true");
			$(radio1).find('[value="2"]').attr("disabled", "true");
			$(radio).find('[value="1"],[value="3"]').attr("disabled",false).prop("selected",'');
			$(radio1).find('[value="1"],[value="3"]').attr("disabled",false).prop("selected",'');
			$("#continue_"+index).hide();
			$("#diff_continue_"+index).show();
			break;
		case "3":
			$(radio).find('[value="3"]').removeAttr('selected');
			$(radio1).find('[value="3"]').removeAttr('selected');
			$(radio).find('[value=" "]').attr("disabled","true").prop("selected",'selected');
			$(radio1).find('[value=" "]').attr("disabled","true").prop("selected",'selected');
			$(radio).find('[value="3"]').attr("disabled", "true");
			$(radio1).find('[value="3"]').attr("disabled", "true");
			$(radio).find('[value="1"],[value="2"]').attr("disabled",false).prop("selected",'');
			$(radio1).find('[value="1"],[value="2"]').attr("disabled",false).prop("selected",'');
			$("#continue_"+index).hide();
			$("#diff_continue_"+index).show();
			break;
			
		}
	}else if($(primary_table).is('[secondary]') || $(primary_table).is('[tertary]')){
		switch(value){
		case "1":
			
			if($(sibling_table1).is('[primary]')){
				radio1 = radio;
			}
			
			$(radio1).find('[value="1"]').removeAttr('selected');
			$(radio1).find('[value="1"]').attr("disabled", "true");
			if($(radio1).find('[value="2"]').is(':enabled')) {
				$(radio1).find('[value="2"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'2',{expires:7});
			}else if($(radio1).find('[value="3"]').is(':enabled')){
				$(radio1).find('[value="3"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'3',{expires:7});
			}
			if(primary.find('[value="2"]').is(':enabled')){
				$(radio1).find('[value="2"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'2',{expires:7});
			}else if(primary.find('[value="3"]').is(':enabled')){
				$(radio1).find('[value="3"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'3',{expires:7});
			}
			
			$("#diff_continue_"+index).hide();
			$("#continue_"+index).show();
			break;
		case "2":
			
			if($(sibling_table1).is('[primary]')){
				radio1 = radio;
			}
			$(radio1).find('[value="2"]').removeAttr('selected');
			$(radio1).find('[value="2"]').attr("disabled", "true");
			if($(radio1).find('[value="1"]').is(':enabled')) {
				$(radio1).find('[value="1"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'1',{expires:7});
			}else if($(radio1).find('[value="3"]').is(':enabled')){
				$(radio1).find('[value="3"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'3',{expires:7});
			}
			
			if(primary.find('[value="1"]').is(':enabled')){
				$.cookie($(radio1).attr('name'),'1',{expires:7});
			}else if(primary.find('[value="3"]').is(':enabled')){
				$(radio1).find('[value="3"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'3',{expires:7});
			}
			
			$("#diff_continue_"+index).hide();
			$("#continue_"+index).show();
			break;
		case "3":
			
			if($(sibling_table1).is('[primary]')){
				radio1 = radio;
			}
			$(radio1).find('[value="3"]').removeAttr('selected');
			$(radio1).find('[value="3"]').attr("disabled", "true");
			if($(radio1).find('[value="1"]').is(':enabled')) {
				$(radio1).find('[value="1"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'1',{expires:7});
			}else if($(radio1).find('[value="2"]').is(':enabled')){
				$(radio1).find('[value="2"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'2',{expires:7});
			}
			
			if(primary.find('[value="1"]').is(':enabled')){
				$(radio1).find('[value="1"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'1',{expires:7});
			}else if(primary.find('[value="2"]').is(':enabled')){
				$(radio1).find('[value="2"]').attr("disabled",false).prop("selected",'selected');
				$.cookie($(radio1).attr('name'),'2',{expires:7});
			}
			
			$("#diff_continue_"+index).hide();
			$("#continue_"+index).show();
			break;
			
		}
	}else{
		console.log("I love you");
	}
}

function updatePage(pageInd){
	if(pageInd== undefined){
		if(psycho_step=='psycho1')
			pageInd = 0;
		else if(psycho_step=='psycho2')
			pageInd = 10;
		else 
			pageInd = 0;
	}
	var ind = pageInd;
	 var ind2 = parseInt(pageInd)+1;
		$("body").find("div#div_id_"+ind).hide();
		$("body").find("div#div_id_"+ind2).show();
		
		if(!edit){
			if(psycho_step=='psycho1')
				$.cookie("pageInd", parseInt(ind)+parseInt(1), { expires: 7 });
			else if(psycho_step=='psycho2')
				$.cookie("pageInd", parseInt(ind)+parseInt(16), { expires: 7 });
		}
}

function savePyschoQuestions(){
	$("#physcoQuestions").hide();
	$("#loader").show();
//	trackRegistration("save: "+psycho_step,'');
	$.get("personality.php?save="+psycho_step,function(data,status) {
		if(status=='success'){
			trackRegistration(psycho_step,"save_call",'');
		setTimeout(function(){	
			if(rdUrl!='' && rdUrl!=undefined)
				window.location="http://"+rdUrl;
			/*else if(((stepsCompleted.indexOf('psycho1')>-1 && stepsCompleted.indexOf('hobby')>-1) || (stepsCompleted.indexOf('psycho2')>-1 && stepsCompleted.indexOf('hobby')>-1)) && fromRegister){
				window.location=baseurl + "/matches.php";
			}*/
			else if((stepsCompleted.indexOf('psycho1')>-1 || stepsCompleted.indexOf('psycho2')>-1) && stepsCompleted.indexOf('hobby')>-1 && fromRegister){
				window.location=baseurl + "/matches.php";
			}
			else if(fromRegister)
				window.location=baseurl + "/personality.php?from_trust=true";
			else
				window.location=baseurl + "/personality.php";
		},200);
		}		
	});
}

function onBack(){
	if(psycho_step=='psycho1'){
		var pageInd = $.cookie("psycho1_pageInd");
	}else if(psycho_step=='psycho2'){
		var pageInd = $.cookie("psycho2_pageInd");
	}
	var ind = parseInt(pageInd)-1;
	$("body").find("div#div_id_"+pageInd).hide();
	$("body").find("div#div_id_"+ind).show();
	$("#back_"+ind).hide();
	if(psycho_step=='psycho2'){
		$("#continue_"+ind).show();
		$.cookie("psycho2_pageInd", ind, { expires: 7 });
	}else{
		if($("input[name=pa_"+pageInd+"]:checked").size() > 0){
			$("#continue_"+ind).show();
			$("#diff_continue_"+ind).hide();
		}else{
			$("#continue_"+ind).hide();
			$("#diff_continue_"+ind).show();
		}
		$.cookie("psycho1_pageInd", ind, { expires: 7 });
	}
	//$.cookie("pageInd", ind, { expires: 7 });
}

function onContinue(){
	if(psycho_step=='psycho1'){
		var pageInd = $.cookie("psycho1_pageInd");
	}else if(psycho_step=='psycho2'){
		var pageInd = $.cookie("psycho2_pageInd");
	}
	//var pageInd = $.cookie("pageInd");
	if(psycho_step=='psycho1'){
		if(pageInd==undefined){
			pageInd = 0;
		}
		var ind = parseInt(pageInd)+1;
		var ind1 = parseInt(ind)+1;
		$("body").find("div#div_id_"+pageInd).hide();
		$("body").find("div#div_id_"+ind).show();
		$("#back_"+ind).show();
		if($("input[name=pa_"+ind1+"]:checked").size() > 0){
			$("#continue_"+ind).show();
			$("#diff_continue_"+ind).hide();
		}else{
			$("#continue_"+ind).hide();
			$("#diff_continue_"+ind).show();
		}
		
		$.cookie("psycho1_pageInd", ind, { expires: 7 });
	}
	else if(psycho_step=='psycho2'){
		if(pageInd==undefined){
			pageInd = 10;
		}
		if(pageInd==19){
			savePyschoQuestions();
		}else{
			var ind = parseInt(pageInd)+1;
			$("body").find("div#div_id_"+pageInd).hide();
			$("body").find("div#div_id_"+ind).show();
			$("#back_"+ind).show();
			var a = $("div#div_id_"+ind).find("div.qselect-style").children();
			if($(a[0]).val()!=" " && $(a[1]).val()!=" " && $(a[2]).val()!=" "){
				$("#continue_"+ind).show();
				$("#diff_continue_"+ind).hide();
			}else{
				$("#continue_"+ind).hide();
				$("#diff_continue_"+ind).show();
			}
		
			$.cookie("psycho2_pageInd", ind, { expires: 7 });
		}
	}
	
}

function deleteCookies(){
	var cookies = $.cookie();
	for(var cookie in cookies) {
		if(cookie.indexOf("PHPSESSID")==-1)
			$.removeCookie(cookie);
		/*if(cookie.indexOf("pa")!=-1 || cookie.indexOf("psycho")!=-1)
			$.removeCookie(cookie);*/
	}
}

/*$(window).unload(function(){
	deleteCookies();
});*/


function checkTime(){
	
}
