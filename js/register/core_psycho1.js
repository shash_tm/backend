
function updatePage(saveprevious){
	saveprevious=(typeof saveprevious !== 'undefined') ? true : false;
	var pageInd = $.cookie("pageInd");
	if( pageInd == undefined ){
		pageInd = 0;
	}

	if(saveprevious==true){
		if(psycho_step!='psycho3'){
			$("#"+div_ids[pageInd]).hide();
		}
		pageInd++;
	}

	if(psycho_step!='psycho3'){
		$("#"+div_ids[pageInd]).show();
		//$("#"+div_ids[pageInd]).show("slide", { direction: "right" }, 500);

		if(pageInd>0)
			$('#back').show();
	}
	updateCookieForPage(pageInd);
//	$.cookie("pageInd", pageInd, { expires: 7 });
}

function updateCookieForPage(pageInd)
{
	$.cookie("pageInd", pageInd, { expires: 7 });
}

function getCookieForPage()
{
	return $.cookie("pageInd");
}

function savePyschoQuestions(){
	$("#loader").show();
	$.get("register.php?save="+psycho_step,function(data,status) {
		if(status=='success'){
			window.location=baseurl + "/register.php";
		}		
	});
}

function psychoRadioClick() {
	$.cookie(this.name,this.value,{expires:7});	
	if(psycho_step=='psycho3'){
		var all_checked=true;	
		//check how many are checked
		for (var i = 0; i < div_ids.length; i++) {
			if($("#"+div_ids[i]+" [name^=pa]").is(':checked')==false){
				all_checked=false;
				break;
			}
		}
		if(all_checked==true){
			//$("#psycho_radio_combine").hide();
			$('#next').click(psychoNextClickButtonClick);
			$('#next').show(); 
			//savePyschoQuestions();
		}
	}else{
		updatePage(true);
		var pageInd=getCookieForPage();
		if(pageInd==div_ids.length) {
			$('#back').hide();
			$("#"+div_ids[pageInd]).hide();
			savePyschoQuestions();
		}
	}
}

function psychoBackButtonClick() {
	var pageInd= getCookieForPage();		
	$("#"+div_ids[pageInd]).hide();
	pageInd--;
	//$("#"+div_ids[pageInd]).show("slide", { direction: "left" },1000);
	$("#"+div_ids[pageInd]).show();
	$('#back').hide();
	updateCookieForPage(pageInd);
}


function psychoNextClickButtonClick() {
	$("#psycho_radio_combine").hide();
	$("#next").hide();
	savePyschoQuestions();
}
	
	
$(document).ready(function() {
	var pageInd = $.cookie("pageInd");
	updatePage();
//	Progess Bar
	$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: 60
	});
	$('#back').click(psychoBackButtonClick);
	//$("[name^=pa]").click();
	$("[name^=pa]").click(psychoRadioClick);

});
