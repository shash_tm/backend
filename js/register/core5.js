var continueFlag = false;

$(document).ready(function() {
	// $.removeCookie("cookie"); 
	var pageInd = $.cookie("pageInd");

	if( pageInd == undefined ){
		pageInd = 0;
	}

	$("div.wrapper").find("div#container").eq( pageInd ).show(); //show starting page

	//Progess Bar
	$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: 50
	});

//	jquery chozen plugin
//$(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});
refresh();
$('body').on('click',"div[id^=thumbNailDiv_]",function()  {
	var idArray=this.id.split("_");
	if(idArray[2]!=undefined) {
		$("#thumbNail_"+idArray[1]+"_profile").click();
	}else{
		$("#thumbNail_"+idArray[1]).click();
	}	
});

$('body').on('click',"img[id^=thumbNail_]",function()  {
	
	if(!edit){
		trackRegistration('photo',"thumbnail_clicked",false);
	}
$(".tmpcaction").show();
$("#new_img_sav").hide();
$("#camera_btn").hide();
$("#new_img_cancel").hide();
//$("#crop").show();
$("#save").hide();
$("#save_w").hide();
$("#chop").show();
$("#save_u").hide();
document.getElementById("imageDiv").style.height=445;
if(cameraOn==true) {
	cameraOn=false;
	sayCheese.stop();
	$('video').remove();
	}
$("img[id^=thumbNail_]").each(function() {
var ids=this.id.split("_");
if(ids[2]==undefined) {
this.style.background='';
}
else { 
this.style.background="#c1215d";
//$("#"+this.id).after("<img id='profile_star' style='width:18!important; border:none; position:absolute; left:5; padding:0; background:none;' src='images/register/primery_photo.png'>");
}
});

this.style.background='#4293f0';
document.getElementById('imageDiv').innerHTML='';
var idArray=this.id.split("_");
if(idArray[2]!=undefined) {
$("#chop").hide();
$("#save_w").hide();
$("#dummy_primary").show();
$("#dummy_delete").show();
//$("#save_p").hide();
}else{
	$("#save_w").show();
	$("#dummy_primary").hide();
	$("#dummy_delete").hide();
}
$("#save").show();
$("#dummy_save").hide();
//else $("#save_p").show();
var photos=photosAll;
document.getElementById('imageDiv').innerHTML="<img style='opacity:0' src='"+photos[Number(idArray[1])].name+"' id='showImage'>";
$("#photoStatus").html(photos[Number(idArray[1])].status);
document.getElementById('showImage').onload=function() {
var maxWidth=700;
var maxHeight=450;
var width=document.getElementById('showImage').width;
var height=document.getElementById('showImage').height;

if(height > maxHeight)
var ratio = maxHeight / height;
else if(width > maxWidth)
var ratio = maxWidth / width;
height = height * ratio;
width = width * ratio;
document.getElementById('showImage').style.height=height+'px';
document.getElementById('showImage').style.width=width+'px';
$("#imageDiv").show();
$("#showImage").fadeTo(0,1);
$("#crop").click();
};
});

if($("#thumbNail_0_profile").length){
	$("#thumbNail_0_profile").click();
}
if($("#thumbNail_0").length){
	$("#thumbNail_0").click();
}

if(photosAll.length!=0){
	$("#right_panel").show();
}

$("#jfmps-submit-button").hide();
sayCheese = new SayCheese('#say-cheese-container');
sayCheese.on('start', function() {
document.getElementById('imageDiv').innerHTML='';
$(".tmpcaction").hide();
$("#photoStatus").html('');
document.getElementById('imageDiv').innerHTML="";
$("#imageDiv").hide();
$("#camera_btn").show();
$('#take-snapshot').on('click', function(evt) {
	if(!edit){
		trackRegistration('photo',"take_snapshot_clicked",false);
	}
            sayCheese.takeSnapshot();
         });
$("#cancel-snapshot").on('click',function(evt){
	if(!edit){
		trackRegistration('photo',"cancel_snapshot_clicked",false);
	}
	if(cameraOn==true) {
		cameraOn=false;
		sayCheese.stop();
		$('video').remove();
		}
	$("#testDiv1").show();
	$("#camera_btn").hide();
	flag = 0;
	if($("#thumbNail_0_profile").length){
		$("#thumbNail_0_profile").click();
	}	
	else if($("#thumbNail_0").length){
		$("#thumbNail_0").click();
	}else if($("#default_1").length){
		$("#default_1").click();
		flag = 1;
	}
	if(flag==0)
	$(".tmpcaction").show();
});

});


sayCheese.on('error', function(error) {
document.getElementById('imageDiv').innerHTML="Webcam support not available";
$("#photoStatus").html('');
$("#camera_btn").hide();
$("#right_panel").hide();
$("#testDiv1").show();
if(cameraOn==true) {
	cameraOn=false;
	if(sayCheese!=null)
	sayCheese.stop();
	$('video').remove();
}
$("#webcamDiv").show();
return;
 // handle errors, such as when a user denies the request to use the webcam,
 // or when the getUserMedia API isn't supported
});

sayCheese.on('snapshot', function(snapshot) {
document.getElementById("imageDiv").style.height=450;
var img = document.createElement('img');
$("#testDiv1").show();
$(".tmpcaction").show();
$("#save_p").hide();
$("#new_img_cancel").show();
$("#chop").hide();
//	img.setAttribute("id","showImage");	
          $(img).on('load', function() {
//img.setAttribute("id","showImage");
        	  if(cameraOn==true) {
        		  cameraOn=false;
        		  sayCheese.stop();
        		  $('video').remove();
        		  }
$("#webcamDiv").show();
document.getElementById('imageDiv').innerHTML='';
$("#photoStatus").html('');
//document.getElementById('imageDiv').innerHTML="<img src='"+e.target.result+"' id='showImage'>";
            $('#imageDiv').prepend(img);
            $('#imageDiv').show();
            $("#camera_btn").hide();
var ratio =1;
var maxWidth=700;
var maxHeight=450;
var width=document.getElementById('showImage').width;
var height=document.getElementById('showImage').height;
if(height > maxHeight)
ratio = maxHeight / height;
else if(width > maxWidth)
ratio = maxWidth / width;
height = height * ratio;
width = width * ratio;

//document.getElementById('showImage').style.height=height+'px';
//document.getElementById('showImage').style.width=width+'px';
$("#crop").click();
          });
          img.src = snapshot.toDataURL('image/png');
		img.id="showImage";
});

$("#webcam").click(function() {
	if(!edit){
		trackRegistration('photo','add_from_webcam_clicked',false);
	}
document.getElementById("imageDiv").style.height=50;
if(cameraOn==true) {
cameraOn=false;
sayCheese.stop();
$('video').remove();
}
cameraOn=true;
//$("#webcamDiv").hide();
sayCheese.start();
      });
		
$("#photo").click(
		function(){
			if(!edit){
				trackRegistration('photo','add_from_computer_clicked',false);
			}	
			
		});

//Add from computer photo handling
$("#photo").change(function() {
	if(!edit){
		trackRegistration('photo','add_from_computer_photo_selected',false);
	}	
	if (typeof FileReader !== "undefined"){
	var input=document.getElementById(this.id);
	
if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
            	
if(cameraOn) {
sayCheese.stop();
cameraOn=false;
$("#webcamDiv").show();
$('video').remove();
document.getElementById("imageDiv").style.height=450;
}
$("#camera_btn").hide();
$("#testDiv1").show();
$(".tmpcaction").show();
$("#save_p").hide();
$("#new_img_cancel").show();
//$("#crop").hide();
$("#chop").hide();
$("#save").hide();
$("#save_u").show();

		document.getElementById('imageDiv').innerHTML="<img src='"+e.target.result+"' id='showImage'>";
		$("#photoStatus").html('');
                document.getElementById('showImage').onload=function() {
var maxWidth=700;
var maxHeight=450;
var width=document.getElementById('showImage').width;
var height=document.getElementById('showImage').height;
if(width<200||height<200) {
document.getElementById('imageDiv').innerHTML='';
$("#photoStatus").html('');
alert("The minimum picture size should be 200 x 200");
return;
}
if(height > maxHeight)
var ratio = maxHeight / height;
else if(width > maxWidth)
var ratio = maxWidth / width;
height = height * ratio;    
width = width * ratio;    
document.getElementById('showImage').style.height=height+'px';
document.getElementById('showImage').style.width=width+'px';
$("#crop").click();
};
};
reader.readAsDataURL(input.files[0]);
}
}else{
	uploadFile();
}

$("#photo").val('');

});


$("#new_img_cancel").click(function(){
	if(!edit){
		trackRegistration('photo','image_cancel_click',false);
	}	
	flag = 0;
	if(incomplete){
		$("#imageDiv").html("");
		$("#photoStatus").html('');
		$(".tmpcaction").hide();
	}else{
		if($("#thumbNail_0_profile").length){
			$("#thumbNail_0_profile").click();
		}	
		else if($("#thumbNail_0").length){
			$("#thumbNail_0").click();
		}else if($("#default_1").length){
			$("#default_1").click();
			flag = 1;
		}
		$("#testDiv1").show();
		if(flag==0)
		$(".tmpcaction").show();
	}	
});

if($.browser.safari){
	$("#webcamDiv").hide();
	$(".file-wrapper").css("margin-left","170px");
}
if($.browser.msie){
	$("#webcamDiv").hide();
	$(".file-wrapper").css("margin-left","170px");
}

$('img').bind('contextmenu', function(e) {
    return false;
}); 




$("[id^=save]").click(saveClicked);


$("#chop").click(function() {
	if(!edit){
		trackRegistration('photo',"photo_uploaded_succcessfully",false);
	}
if(document.getElementById('showImage')==null)  {
alert('No Image Selected');
return;
}
var name=document.getElementById('showImage').src;
$.post('photo.php',{action:'chop',url:name},function(data,status) {
var index = -1;
for(var i=0;i<photosAll.length;i++) {
if(name==photosAll[i].name)
{photosAll.splice(i,1);
index = i;
}
}
refresh();
if($("#thumbNail_"+index+"_profile").length){
	$("#thumbNail_"+index+"_profile").click();
}	
else if($("#thumbNail_"+index).length){
	$("#thumbNail_"+index).click();
}
else if($("#thumbNail_0_profile").length){
	$("#thumbNail_0_profile").click();
}	
else if($("#thumbNail_0").length){
	$("#thumbNail_0").click();
}
});
/*
 * var form=$("<form id='formY' action='photo.php'  method=post ><input type=hidden name='action' value='chop'/><input type=hidden name='url' value='"+name+"'/></form>" );
 * */
});

$("#fb_submit_iframe").load(function(){
		var uploadResponse = $("#fb_submit_iframe").contents().text();
		window.top.$('#upload_iframe').attr('src','about:blank');
		document.getElementById('imageDiv').innerHTML='';
		$("#photoStatus").html('');
		$("#formZ").remove();
		refresh_later(uploadResponse);
});


$("#crop").click(function() {
if(document.getElementById('showImage')==null)  {
//alert('No Image Selected');
return;
}

var widthArray=document.getElementById('showImage').style.width.split("px");
var heightArray=document.getElementById('showImage').style.height.split("px");
var size=200;
if(heightArray[0]<widthArray[0]&& heightArray[0]>200) 
	size=heightArray[0];
else if(heightArray[0]>=widthArray[0]&&widthArray[0]>200)
size=widthArray[0];



var crop_x1=0;
var crop_y1=0;
var crop_x2=170;
var crop_y2=170;
	naturalWidth = document.getElementById('showImage').width;
	naturalHeight = document.getElementById('showImage').height;
	if(naturalWidth>170&&naturalHeight>170){
		if(naturalWidth>=naturalHeight){
			crop_y1=15;
			crop_y2=naturalHeight-15;
			temp = crop_y2-crop_y1;
			crop_x1 = (naturalWidth-temp)/2;
			crop_x2 = crop_x1+temp;
		}else{
			crop_x1 = 15;
			crop_x2 = naturalWidth-15;
			temp = crop_x2-crop_x1;
			crop_y1 = 15;
			crop_y2 = crop_y1+temp;
		}
	}
//}else{
//	if(widthArray[0]>170&&heightArray[0]>170){
//		if(widthArray[0]>=heightArray[0]){
//			crop_y1=10;
//			crop_y2=heightArray[0]-10;
//			temp = crop_y2-crop_y1;
//			crop_x1 = (widthArray[0]-temp)/2;
//			crop_x2 = crop_x1+temp;
//		}else{
//			crop_x1 = 10;
//			crop_x2 = widthArray[0]-10;
//			temp = crop_x2-crop_x1;
//			crop_y1 = 10;
//			crop_y2 = crop_y1+temp;
//		}
//	}
//}

//alert(size);
//	$("#crop").hide();
//	$("#save").show();
//	$("#save_w").show();
//	$("#chop").hide();
//	$("#new_img_cancel").show();
                $('#showImage').Jcrop({
		minSize: [150,150],
//	             trueSize: [,size],
            bgOpacity:   0.5,
            setSelect:   [ crop_x1,crop_y1,crop_x2,crop_y2 ],
            aspectRatio: 1,
            onSelect: updateInfo,
	    onRelease:releaseCrop
  //        onChange: updateInfo
},function() {
      // Store the API in the jcrop_api variable
      jcrop_api = this;

      // Move the preview into the jcrop container for css positioning
      //$("#imageDiv2").appendTo(jcrop_api.ui.holder);

});

                var cropFlag = false;

function releaseCrop() {
        $('#x').val('');
                $('#y').val('');
                $('#w').val('');
                $('#h').val('');
//                $("#crop").show();
            	$("#save").hide();
            	$("#dummy_save").show();
            	if($("#save_w").css("display")!='none'){
                	cropFlag = true;
                }
            	$("#save_w").hide();
            	$("#dummy_primary").show();
}
function updateInfo(c) {
		$('#x').val(c.x);
                $('#y').val(c.y);
                $('#w').val(c.w);
                $('#h').val(c.h);
                $("#showImage").hide();
//                $("#crop").hide();
                if($("#save_w").css("display")=='none'){
                	$("#save").show();
                	$("#dummy_save").hide();
                }
                if(cropFlag){
                	$("#save_w").show();
                	$("#dummy_primary").hide();
                }
                
                if($("#new_img_cancel").css("display")!='none'){
                	$("#dummy_primary").hide();
                	$("#dummy_delete").hide();
                	$("#save").hide();
                	$("#save_u").show();
                	$("#save_w").show();
                }
};

});
 	refreshPage( pageInd );
 	
 	
 	if(photosAll.length==0 && !incomplete){
		$("#testDiv1").append("<div class='crjs5'><img src='"+cdn+"/images/phalbum_dummy.gif' id='default_1'></div><div class='crjs5'><img src='"+cdn+"/images/phalbum_dummy.gif' id='default_2'></div>");
		document.getElementById('imageDiv').innerHTML="<img src='"+cdn+"/images/phalbum_dummy.gif' id='showImage' style='margin:75px 0;'>";
		$("#default_1").css("background","#4293f0");
		$("#photoStatus").html('');
	}
 	
 	$('body').on('click',"img[id^=default_]",function()  {
 		if(this.id=="default_1"){
 			document.getElementById('imageDiv').innerHTML="<img src='"+cdn+"/images/phalbum_dummy.gif' id='showImage' style='margin:75px 0;'>";
 			$("#default_1").css("background","#4293f0");
 			$("#default_2").css("background","none");
 		}else if(this.id=="default_2"){
 			document.getElementById('imageDiv').innerHTML="<img src='"+cdn+"/images/phalbum_dummy.gif' id='showImage' style='margin:75px 0;'>";
 			$("#default_2").css("background","#4293f0");
 			$("#default_1").css("background","none");
 		}
 		$("#right_panel").hide();
 	}); 	
});

//fetch values from cookies and show values filled in all pages
function refreshPage( ind ){
	var objParent = $("div.wrapper").find("div#container").eq(ind);
	if( ind == 0 ){
//		setSelectedPos( $(objParent).find("#importantForYou"), importantForYou);
	}else if( ind == 1 ){


	}
	else if(ind==3) {


	}
}

//saving values to cookies,putting validations and refreshing page for next show up
function onContinue( obj ){
		
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
if(ind==0) {
var response='';
var serverResponse=$.ajax({type:'post',url:'photo.php',data:{action:'check'},global:false,async:false,success:function(data){return data;}}).responseText;
jsonresponse = JSON.parse(serverResponse);
if(jsonresponse.status!='ok') {
continueFlag = true;
if($("#right_panel").css('display')!='none')
{ 
	if(!edit){
		trackRegistration('photo','click_continue_with_photo_saving',false);
	}
	
	//$("[id^=save]").click(saveClicked);

	saveClicked("true");
	//$('#save_w').trigger('click', ["true"]);

//	$("#save_w").click({continue1:"true"},saveClicked); 
	}
else {
	if(!edit){
		trackRegistration('photo','click_continue_without_anyphoto',false);
	}
alert('Please upload at least one picture');	
}
return;
}

if(!edit){
	trackRegistration('photo_save','click_continue_without_photo_saving');
}
$.get("register.php?save=photo",function(data,status) {
                if(status=='success')
                window.location=baseurl+ "/register.php";
                });
}
	$.cookie("pageInd", ind + 1, { expires: 7 });
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).next().show();

	refreshPage( ind + 1);
}

//funtion to take back pages
function onBack( obj ){
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	$.cookie("pageInd", ind - 1, { expires: 7 });    
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).prev().show();
	refreshPage( ind - 1 );
}

function refresh_later(data){
		var photos = JSON.parse(data);
		if(photos) {
			document.getElementById('testDiv1').innerHTML="";
			if($("#profile_star").length>0) {
			$("#profile_star").remove();

			}
			for(var i=0;i<photos.length;i++) {
				
				var appendCode = "<div class='crjs5'>";
				if(photos[i].is_profile=='yes') {
					appendCode = appendCode + "<img style='background:#c1215d;' src='"+photos[i].thumbnail+"' id='thumbNail_"+i+"_profile'/><img id='profile_star' class='phprim' src='images/register/pfavbtn.png'/>";
					if(photos[i].status == 'Under Moderation'){
						appendCode = appendCode +'<div id="thumbNailDiv_'+i+'_profile" class="undrmod"><br><br>'+photos[i].status+'</div>'; 
					}
				}
				else {
					appendCode = appendCode + "<img src='"+photos[i].thumbnail+"' id='thumbNail_"+i+"'/>";
					if(photos[i].status == 'Under Moderation'){
						appendCode = appendCode + '<div id="thumbNailDiv_'+i+'" class="undrmod1"><br><br>'+photos[i].status+'</div>';
					}
				}
				appendCode = appendCode + "</div>"; 
				$("#testDiv1").append(appendCode);
			
//			if(photos[i].status == 'Under moderation'){
//				$("#testDiv1").append('<div style="background:#444; width:110px; height:110px; position:absolute; margin:11px 0 0 20px; opacity:0.7; filter:alpha(opacity=70); color:FFF; text-shadow:2px 2px #000;"><br><br>'+photos[i].status+'</div>');
//			}
//				
//			if(photos[i].is_profile=='yes') {
//				$("#testDiv1").append("<div style='height:130px; overflow:visible;'><img style='background:#c1215d;' src='"+photos[i].thumbnail+"' id='thumbNail_"+i+"_profile'><img id='profile_star' style='width:18!important; border:none; position:relative; top:-137px; left:-58px; padding:0; background:none;' src='images/register/primery_photo.png'></div>");
//			}
//			else $("#testDiv1").append("<img src='"+photos[i].thumbnail+"' id='thumbNail_"+i+"'>");
			}
			
			
			
			}
			photosAll = photos;
			
			if(photosAll.length!=0){
				$("#right_panel").show();
			}
			
			if(photosAll.lenght!=0){
				incomplete = false;
			}
			if($("#thumbNail_0_profile").length){
				$("#thumbNail_0_profile").click();
			}	
			if($("#thumbNail_0").length){
				$("#thumbNail_0").click();
			}
			$("#testDiv1").show();
			$(".tmpcaction").show();
			$(".upload_loader_percent").css("width", "0%");
			
}


function cancelFacebook(){
	if(!edit){
		trackRegistration('photo',"facebook_cancel_click",false);
	}
	flag = 0;
	window.top.$('#upload_iframe').attr('src','about:blank');
	document.getElementById('imageDiv').innerHTML='';
	$("#photoStatus").html('');
	$("#formZ").remove();
	$("#testDiv1").show();
	if($("#thumbNail_0_profile").length){
		$("#thumbNail_0_profile").click();
	}	
	else if($("#thumbNail_0").length){
		$("#thumbNail_0").click();
	}else if($("#default_1").length){
		$("#default_1").click();
		flag = 1;
	}
	$("#testDiv1").show();
	if(flag==0)
	$(".tmpcaction").show();
}


function uploadFile() {
	var file = $("#photo").val();
	var fileName = file.split("\\");
	var form = document.getElementById('upload-safari-photo');
	fileName = fileName[fileName.length - 1];
	var extention = fileName.split(".");
	extention = extention[extention.length-1];
	var allowedTypes = ["jpg", "JPG", "JPEG", "jpeg" , "png", "PNG", "GIF" , "gif"];
	if(allowedTypes.indexOf(extention)== -1){
		alert("File type not allowed. Please select image only");
		return;
	}
	$("#photoFile").val(file);	
	form.setAttribute('action', "photo.php");
	form.submit();
	$("#photoStatus").html('');
	document.getElementById('imageDiv').innerHTML="<img id='load' src='images/register/loader_photo.gif' class='mrg brdr'  />";

	$("#file_upload_frame").load(
			function() {
				var uploadResponse = $("#file_upload_frame").contents().text();
				refresh_later(uploadResponse);
			});
}



function saveClicked(event) {
	
	
	if(!edit){
		trackRegistration('photo',this.id+"_clicked",false);
	}
var x=$("#x").val();
var y=$("#y").val();
var w=$("#w").val();
var h=$("#h").val();
var is_profile='';
if(!($('#showImage').length>0))  {
if(continueFlag){
	$("#imageDiv").html("Please upload at least one picture");
	$("#imageDiv").show();
	continueFlag = false;
}
return;
}

//return;
if(this.id=='save_p') {
is_profile='profile';
img_width = document.getElementById('showImage').width;
img_height = document.getElementById('showImage').height;
if(img_width!=img_height) { 
$("#crop").click();
$("#save_w").show();
$("#save_p").hide();
$("#save").hide();
return;
}else{
}
}
else {
	is_profile=false;
	if(w==''||h==''||w!=h){
//		alert("Please select crop area on photo in order to save.");
		$("#crop").click();
		return;
	}
}
var change='no';
var src=document.getElementById('showImage').src;
if(photosAll=='')
photosAll=new Array();
if(photosAll) {
var photos=photosAll;
for(var i=0;i<photos.length;i++) {
if(photos[i].name==src) {
 change='yes';
}}
}
var width=document.getElementById('showImage').naturalWidth;
var height=document.getElementById('showImage').naturalHeight;
var fake_width=document.getElementById('showImage').style.width;
var fake_height=document.getElementById('showImage').style.height;
if(fake_width.substr(-2,2)=='px') {
var fake_widthArray=fake_width.split("px");
fake_width=fake_widthArray[0];
}
if(fake_height.substr(-2,2)=='px') {
var fake_heightArray=fake_height.split("px");
fake_height=fake_heightArray[0];
}
var from_fb='';
//if($("#showImage").attr('name')!=undefined && $("#showImage").attr('name')=='fb_profile')
//from_fb='yes';
if(change=='yes') {
for(var i=0;i<photosAll.length;i++) {
if(src==photosAll[i].name)
photosAll.splice(i,1);
}
}
$(".upload_loader_percent").css("width", "0%");
$("#imageDiv").html($("#image_upload_loader").html());
percentLoader(byteCount(src));
$("#photoStatus").html('');
var profileCrop = 'no';


if(($("#save_p").css("display")=='none' && $("#new_img_cancel").css("display")=='none')||
	incomplete || this.id=='save_w'){
	is_profile='profile';
	profileCrop = 'yes';
}

if(photosAll.length==0){
	is_profile='profile';
	profileCrop = 'yes';
}

//refresh_later();
//return;

$("#right_panel").hide();

$.post("photo.php",
		{x:x,y:y,w:w,h:h,action:'save',src:src,width:width,height:height,is_profile:is_profile,fake_width:fake_width,fake_height:fake_height,from_fb:from_fb,change:change,profile_crop:profileCrop}
,function(dataJson,status) {
	if(!edit){
		trackRegistration('photo',"photo_uploaded_succcessfully",false);
	}
	if(event==="true"){
		$("#continuebtn").click();
		return;
	}
	window.clearInterval(interval);
	$(".upload_loader_percent").css("width", "100%");
	setTimeout(function(){
		refresh_later(dataJson);
	},1000);
	return;
});
/*
var form=$("<form id='formX' action='photo.php'  method=post ><input type=hidden  name='x' value='"+x+"' /><input type=hidden  name='y' value='"+y+"' /><input type=hidden  name='w' value='"+w+"' /><input type=hidden  name='h' value='"+h+"' /><input type=hidden name='action' value='save'/><input type=hidden name='src' value='"+src+"'/><input type=hidden name='fake_width' value='"+fake_width+"'/><input type=hidden name='fake_height' value='"+fake_height+"'/><input type=hidden name='width' value='"+width+"'/><input type=hidden name='height' value='"+height+"'/><input type=hidden name='is_profile' value='"+is_profile+"'/><input type=hidden name='from_fb' value='"+from_fb+"'/><input type=hidden  name='change' value='"+change+"' /></form>" );
*/
}


function byteCount(s) {
    return encodeURI(s).split(/%..|./).length - 1;
}

function percentLoader(size){
	size = size / 1024;
	if(size<1){
		size = 150;
	}
	var percentPerSec = 100 / (size / 10);
	var percent = 0;
	interval = setInterval(function() {
		percent = percent + percentPerSec;
		if (percent < 90) {
			$(".upload_loader_percent").css("width", percent + "%");
		} else {
			window.clearInterval(interval);
		}
	}, 500);
	
}