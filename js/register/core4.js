$(document).ready(function() {
	if(!edit)
		var pageInd = $.cookie("pageInd");
	if( pageInd == undefined ){
		pageInd = 0;
	}
	$("div.wrapper").find("div#container").eq( pageInd ).show(); //show starting page
	//Progess Bar
	$("div.wrapper").find( "div#progressbar" ).progressbar({ 	
		value: 30
	});
	
	//autocomplete suggestion box
	$.widget( "custom.catcomplete", $.ui.autocomplete, {
	    _renderMenu: function( ul, items ) {
	      var that = this,
	        currentCategory = "";
	      $.each( items, function( index, item ) {
	        if ( item.category != currentCategory ) {
	          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
	          currentCategory = item.category;
	        }
	        that._renderItemData( ul, item );
	      });
	    }
	  });

//	jquery chozen plugin
	$(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});
	if(!edit) {
		if($.cookie('TM_Working_area_spouse')==undefined) {
			$("label:has(input[name='working_area_spouse_notMatter'])").addClass('checked');
			$("input[name='working_area_spouse_notMatter']").attr('checked','checked');
		}
	}
	
	previousValues = new Array("-5");
	//if(!edit)
		//$('#education_spouse').prepend("<option value='-5' selected=selected>Any</option>");	
	$("#education_spouse").change(function(){
		var values = $("#education_spouse").val();
		if(values){
			var index = values.indexOf("-2");
			if(index > -1){
				values.splice(index,1);
				for(var i=12;i<=21;i++){
					values.push(i);
				}
				$("#education_spouse").val(values);
				$('#education_spouse').trigger("liszt:updated");
			}
			
			var index2 = values.indexOf("-1");
			if(index2 > -1){
				values.splice(index2,1);
				for(var i=2;i<=11;i++){
					values.push(i);
				}
				$("#education_spouse").val(values);
				$('#education_spouse').trigger("liszt:updated");
			}
			
			var index2 = values.indexOf("-3");
			if(index2 > -1){
				values.splice(index2,1);
				for(var i=1;i<=21;i++){
					values.push(i);
				}
				$("#education_spouse").val(values);
				$('#education_spouse').trigger("liszt:updated");
			}
			
			var index2 = values.indexOf("-4");
			if(index2 > -1){
				values.splice(index2,1);
				for(var i=1;i<=11;i++){
					values.push(i);
				}
				$("#education_spouse").val(values);
				$('#education_spouse').trigger("liszt:updated");
			}
			
//			console.log("current :"+values);
//			console.log("previous :"+previousValues);
			
			var index2 = values.indexOf("-5");
			var index3 = previousValues.indexOf("-5");
			if(index2 > -1 && index3 == -1 && values.length>1){
				values = new Array();
				values.push("-5");
				$("#education_spouse").val(values);
				$('#education_spouse').trigger("liszt:updated");
			}else if(index2 > -1 && index3 > -1){
				values.splice(index2,1);
//				console.log(values);
				$("#education_spouse").val(values);
				$('#education_spouse').trigger("liszt:updated");
			}
			
		}else{
			values = new Array();
			values.push("-5");
			$("#education_spouse").val(values);
			$('#education_spouse').trigger("liszt:updated");
		}
		
		previousValues = values;
	});

	//fillEducationSpouse('education_0');
	fillEducationSpouse('education_spouse');
	$("input[name='working_area']").change(function() {
		var val=$("input[type='radio'][name='working_area']:checked").val();
		if(val=='Not Working' || val=='Student') {
			for(var i=0;i<work_ids_array.length;i++) {
				var val=work_ids_array[i];
				$('#indus_'+val).html('Industry <span class="pink">(optional)</span>');
				$('#desig_'+val).html('Designation <span class="pink">(optional)</span>');
			}
		}
		else {
			for(var i=0;i<work_ids_array.length;i++) {
				var val=work_ids_array[i];
				$('#indus_'+val).html('Industry');
				$('#desig_'+val).html('Designation');
			}}
	});

//	Education


	$("[id='moreeducation']").click(function(){
		if(education_ids_array.length==maxAddMores) 
			return;
		if(education_ids_array.length==maxAddMores-1)
			$(".addmoreed").hide();
		current_education_id++;
		var education_block_id="educationblock_"+current_education_id;
		var education_id="education_"+current_education_id;
		var specialization_id="specialization_"+current_education_id;
		var institute_id="institute_"+current_education_id;
		var education_error_message_id="education_"+current_education_id+"Error";
		var place_holder_val=(current_education_id==0)?"Highest":"Additional";
		var close_button_html=(current_education_id==0)?'':'<p class="close closee"><img src="images/register/close.png"></p>';
		$("#moreeducation").before(
				'<div class="designation mt20 eduqua" id="'+education_block_id+'">'+
				close_button_html+
				'<p class="formlabelnew">Education Level</p>'+
				'<p><select class="span6 chzn-select" data-placeholder="'+place_holder_val+' Education level..." tabindex="1" id='+education_id+'>'+ 
				'</select></p> <p style="color:red" id="'+education_error_message_id+'"class="fs11"></p><p class="formlabelnew mt10">Specialisation<span class="pink"> (optional)</span></p>'+
				'<p><input class="inpboxwdth" id="'+specialization_id+'" rows="1"/></p><p style="color:red" class="fs11"></p>'+
				'<p class="formlabelnew mt10">Institute<span class="pink"> (optional)</span></p><p>'+
				'<input class="inpboxwdth" id="'+institute_id+'" rows="1"/></p><p style="color:red" class="fs11"></p></div>');
		fillEducation(education_id);		
		setAutoCompleteForEducationSpecialization(specialization_id,education_id);
		setAutoCompleteForEducationInstitute(institute_id);
		education_ids_array.push(current_education_id);
		$("#"+education_block_id+" .close").click(function(){
			var idToRemoveArray=education_block_id.split("_");
			var addMoreToRemove=idToRemoveArray[1];
			var index = education_ids_array.indexOf(parseInt(addMoreToRemove));
			if (index > -1) {
				education_ids_array.splice(index, 1);
			}
			$("#"+education_block_id).remove();
			$(".addmoreed").show();
		});
	});

	$("#moreWork").click(function(){
		if(work_ids_array.length==maxAddMores) 
			return;
		if(work_ids_array.length==maxAddMores-1)
			$("#moreWork").hide();
		current_work_id++;
		
		var close_button_html=(current_work_id==0)?'':'<p class="close closee"><img src="images/register/close.png"></p>';
		var industry_block_id="industryblock_"+current_work_id;

		$("#moreWork").before(
				'<div class="designation mt20 eduqua" id="'+industry_block_id+'">'+
				close_button_html+
				'<p id="indus_'+current_work_id+'" class="formlabelnew">Industry <span class="pink">(optional)</span></p><p class="formlabelnew"></p>'+
//				'<p id="workCloseDiv_'+current_work_id+'" class="close close_work"><img src="images/register/close.png"></p>		
				'<p><select class="span6 chzn-select" data-placeholder="Select Industry" tabindex="1" id="industry_'+current_work_id+'"> </select></p>'+
				'<p id="industry_'+current_work_id+'Error" style="color:red" class="fs11"></p>'+
				'<p id="desig_'+current_work_id+'" class="formlabelnew mt10">Designation<span class="pink"> (optional)</span></p>'+
				'<p><input id="designation_'+current_work_id+'" class="inpboxwdth" placeholder="Designation"></p>'+
				'<p id="designation_'+current_work_id+'Error" style="color:red" class="fs11"></p>'+
				'<p class="formlabelnew mt10">Company Name<span class="pink"> (optional)</span></p>'+
				'<p><input id="company_name_'+current_work_id+'" class="inpboxwdth" placeholder="Company Name"></p>'+
				'<p id="company_name_'+current_work_id+'Error" style="color:red" class="fs11"></p>'+
				'</div>');
		
		work_ids_array.push(current_work_id);

		$("input[name='working_area']").change();
		setAutoCompleteForDesignation("designation_"+current_work_id);
		fillIndustry("industry_"+current_work_id);
		$(".chzn-select").chosen();
		$(".chzn-select-deselect").chosen({allow_single_deselect:true});



		$("#"+industry_block_id+" .close").click(function(){
			var idToRemoveArray=industry_block_id.split("_");
			var addMoreToRemove=idToRemoveArray[1];
			var index = work_ids_array.indexOf(parseInt(addMoreToRemove));
			if (index > -1) {
				work_ids_array.splice(index, 1);
			}
			$("#"+industry_block_id).remove();
			$("#moreWork").show();
		});
	});
	
	

	$("[name='working_area_spouse_notMatter']").change(function() {
		var toReduce='';
		if(this.name=='working_area_spouse_notMatter')
			toReduce='working_area_spouse';

		var value=$("input[name="+this.name+"]:checked").val();
		if(value != undefined)
			$("[name="+toReduce+"]").prop("checked", false);
		else $("[name="+this.name+"]").prop("checked", true);


	});
	$("[name='working_area_spouse']").change(function() {
		var toReduce='';
		if(this.name=='working_area_spouse')
			toReduce='working_area_spouse_notMatter';

		var value=$("input[name="+this.name+"]:checked").val();
		if(value!=undefined)// && $("input[name="+toReduce+"]:checked").val()!=undefined)
			$("[name="+toReduce+"]").prop("checked", false);
		else $("[name="+toReduce+"]").prop("checked", true);

	});

	/*
	$('#designation_0').autocomplete({
		source:function( request, response ) {
			$.ajax({
				url: "colleges.php?value="+$('#designation_0').val()+"&designation=true",
				dataType: "json",
				success:function(data) {
					response($.map( data, function( item ) {
						return {
							label: item.name,
							value: item.name
						}
					}));
				}
			});
		},
		minlength:2
	});
	*/
});


//saving values to cookies,putting validations and refreshing page for next show up
function onContinue4( obj ){
	var toSave=false;
	var page = '';

	var objParent = $(obj).parents("div#container");
	if(edit)
		var ind=obj;
	else 	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if( ind == 0 ){
		page = 'work';
		var working_area=$("[name='working_area']:checked").val();
		var industry=[];
		var designation=[];
		var company_name=[];
		for(var i=0;i<work_ids_array.length;i++) {
			var val=work_ids_array[i];
			industry[i]=$("#industry_"+val).val();
			designation[i]=$("#designation_"+val).val();
			company_name[i]=$("#company_name_"+val).val();
		}

		if($("input[name='working_area_spouse_notMatter']:checked").length==0) {
			var working_area_spouse=new Array();
			$("input[name='working_area_spouse']:checked").each(function() {
				working_area_spouse.push($(this).val());
			});
		}
		else var working_area_spouse=$("[name='working_area_spouse_notMatter']:checked").val();
		var industry_spouse=$("#industry_spouse").val();
		var error=false;
		if(working_area==undefined) {
			error=true;
			document.getElementById('working_area_Error').innerHTML=errors.work;
		}
		else document.getElementById('working_area_Error').innerHTML='';
		if(working_area!='Not Working'&&working_area!='Student') {

			for(var i=0;i<work_ids_array.length;i++) {
				var val=work_ids_array[i];
				if(industry[i]==''||industry[i]==undefined) {
					error=true;
					document.getElementById('industry_'+val+'Error').innerHTML=errors.industry;
				}
				else document.getElementById('industry_'+val+'Error').innerHTML='';

				if(designation[i]==''||designation[i]==undefined) {
					error=true;
					document.getElementById('designation_'+val+'Error').innerHTML=errors.designation;
				}
				else document.getElementById('designation_'+val+'Error').innerHTML='';
			}
		}
		else {
			for(var i=0;i<work_ids_array.length;i++) {
				var val=work_ids_array[i];
				document.getElementById('industry_'+val+'Error').innerHTML='';
				document.getElementById('designation_'+val+'Error').innerHTML='';
			}

		}
		if(working_area_spouse==undefined || working_area_spouse=='') {
			error=true;
			document.getElementById('working_area_spouse_Error').innerHTML=errors.work_spouse;
		}
		else document.getElementById('working_area_spouse_Error').innerHTML='';
		if(error)
			return;
		if(!edit) {
			for(var i=0;i<work_ids_array.length;i++) {
				$.cookie("TM_Industry_"+i,industry[i],{ expires: 7 });
				$.cookie("TM_Designation_"+i,designation[i],{ expires: 7 });
				$.cookie("TM_Company_name_"+i,company_name[i],{ expires: 7 });
			}
			$.cookie("TM_Working_area",working_area,{ expires: 7 });
			$.cookie("TM_Working_area_spouse",working_area_spouse,{ expires: 7 });
			$.cookie("TM_Industry_spouse",industry_spouse,{ expires: 7 });
		}
		else {
			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'work1',industry:industry,designation:designation,company_name:company_name,working_area:working_area,working_area_spouse:working_area_spouse,industry_spouse:industry_spouse},
				beforeSend: function() { 
					$('#work_save').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						$('#work_save').text("Saved");
					}else{
						$('#work_save').text("Not Saved");
					}
				}
			});

		}
	}
	else if( ind == 1){
		page = 'education';
		var education=[];
		var institute=[];
		var specialization=[];
		for(var i=0;i<education_ids_array.length;i++) {
			var val=education_ids_array[i];
			education[i]=$("#education_"+val).val();
			institute[i]=$("#institute_"+val).val();
			specialization[i]=$("#specialization_"+val).val();
		}
		
		var education_spouse=$("#education_spouse").val();
		console.log(education_spouse);
		var index2 = education_spouse.indexOf("-5");
		if(index2 > -1 && education_spouse.length == 1){
			education_spouse = null;
		}
		
		var error=false;
		for(var i=0;i<education_ids_array.length;i++) { 
			var val=education_ids_array[i];
			if(education[i]=='') {
				error=true;
				document.getElementById('education_'+val+'Error').innerHTML=errors.education;
			}
			else document.getElementById('education_'+val+'Error').innerHTML='';
		}
		if(error)
			return;
		if(!edit) {
			for(var i=0;i<education_ids_array.length;i++) {
				$.cookie("TM_Education_"+i,education[i],{ expires: 7 });
				$.cookie("TM_Institute_"+i,institute[i],{ expires: 7 });
				$.cookie("TM_Specialization_"+i,specialization[i],{ expires: 7 });
			}
			$.cookie("TM_Education_spouse",education_spouse,{ expires: 7 });
			toSave=true;
		}
		else {

			$.ajax({
				url: baseurl+"/update.php",
				type: "POST",
				dataType:'json',
				data: {param:'work2',education:education,institute:institute,specialization:specialization,education_spouse:education_spouse},
				beforeSend: function() { 
					$('#education_save').text("Saving...");
				},
				success: function(data) {
					if(data.responseCode==200){
						$('#education_save').text("Saved");
					}else{
						$('#education_save').text("Not Saved");
					}
				}
			});
		}
	}
	if(!edit)	
		$.cookie("pageInd", ind + 1, { expires: 7 });
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).next().show();
	
	if(!edit){
		trackRegistration(page,'');
	}

	if(toSave) {
		$.get("register.php?save=work",function(data,status) {
			if(status=='success')
				window.location=baseurl + "/register.php";
		});
	}	

	refreshPage( ind + 1);
}

function refreshPage( ind ){
        var objParent = $("div.wrapper").find("div#container").eq(ind);
}


//funtion to take back pages
function onBack4( obj ){
	var objParent = $(obj).parents("div#container");
	var ind = $(obj).parents("div.wrapper").eq(0).find("div#container").index( $(objParent) );
	if(!edit)
		$.cookie("pageInd", ind - 1, { expires: 7 });    
	$(obj).parents("div.container").eq(0).hide();
	$(obj).parents("div.container").eq(0).prev().show();
	refreshPage( ind - 1 );
}

$(window).load(function() {
	setTimeout(function(){
	    if(performance.timing!=undefined){
	      var t = performance.timing;
	      var dnsLookup = (t.domainLookupStart-t.domainLookupEnd)/1000;
	      var pageload = (t.loadEventEnd-t.navigationStart)/1000;
	      var pageCreate = (t.loadEventEnd-t.responseEnd)/1000;
	      var networklatency = (t.responseEnd-t.fetchStart)/1000;
	      trackPerformance("work_dnsLookup",dnsLookup);
	      trackPerformance("work_networklatency",networklatency);
              trackPerformance("work_pageload",pageload);
              trackPerformance("work_pageCreate",pageCreate);
	    }
	},0);
});
