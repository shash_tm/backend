function prefill(){
	//if(status=='authentic'){
		$('.lockinfo').show();
		$("#fname,#whenBornDay,#whenBornMonth,#whenBornYear").prop('disabled',true);
	//}
		var name = '';
	if(user_data.fname!=null){
		name = user_data['fname'];
		//$("#fname").val(user_data['fname']);
	}
	if(user_data.lname!=null){
		name = name + " " + user_data['lname'];
		//$("#lname").val(user_data['lname']);
	}
	$("#fname").val(name);
	
	if(user_data['hashtags']!=null)
		for(i=0;i<user_data['hashtags'].length;i++){
			$('input[name="interest"][value='+user_data['hashtags'][i]+']').iCheck('check');
		}
	
	if(user_data['smoking_status']!=null){
		$('input[name="smoking_status"][value='+user_data['smoking_status']+']').iCheck('check');
	}
	
	if(user_data['drinking_status']!=null){
		$('input[name="drinking_status"][value='+user_data['drinking_status']+']').iCheck('check');
	}
	
	if(user_data['food_status']!=null){
		$('input[name="food_status"][value='+user_data['food_status']+']').iCheck('check');
	}

	if(user_data['work_status']!=null){
		if(user_data['work_status']=='no'){
			//$("input[type='checkbox'][name='working_area']").iCheck('check');
			//$("#working_status").change();
			$("#industry option[value='Not Working']").attr("selected",'selected');
			$("#industry").change();
		}
	}
	if(user_data['height_foot']!=null && user_data['height_inch']!=null){
		$("#feet option[value='" + user_data['height_foot']  + "']").attr("selected",'selected');
		$("#inch option[value='" + user_data['height_inch']  + "']").attr("selected",'selected');
		$("#feet").change();
		$("#inch").change();
	}
	if(user_data.religion!=null){
		$("#religion option[value='" + user_data['religion']  + "']").attr("selected",'selected');
		$("#religion").change();
	}
	if(user_data.income_start!=null && user_data.income_end!=null){
		$("#income option[value='" + user_data['income_start']+"-"+user_data['income_end'] + "']").attr("selected",'selected');
		$("#income").change();
	}
	else {
		$("#income option[value='NA']").attr("selected",'selected');
		$("#income").change();
	}
	if(user_data.highest_degree!=null){
		$("#education option[value='" + user_data['highest_degree']  + "']").attr("selected",'selected');
		$("#education").change();
	}
	if(user_data.industry!=null){
		$("#industry option[value='" + user_data['industry']  + "']").attr("selected",'selected');
		$("#industry").change();
	}else {
		$("#industry option[value='Not Working']").attr("selected",'selected');
		$("#industry").change();
	}
}

function prefillLocation(id){
	if(id=="state"){
		if(user_data['stay_state']!=null){
			$("#stayState option[value='" +user_data['stay_country']+ "-" +user_data['stay_state'] +"']").attr("selected",'selected');//prop('selected',true);
			//$("#stayState").trigger("liszt:updated");
			$("#stayState").change();
		}
	}
	if(id=="city"){
		if(user_data['stay_city']!=null){
			$("#stayCity option[value='" +user_data['stay_country']+ "-" +user_data['stay_state']+"-"+user_data['stay_city'] +"']").prop('selected',true);
			$("#stayCity").trigger("liszt:updated");
			$("#stayCity").change();
		}
	}
}

function showbelowItem(show){
 switch(show){
	case "basics":
		$("#basicd").removeClass('plus').addClass('minus');
		$("#basicd-minus").show();
		$("#basicd-plus").hide();
		$("#basicd-below").show();
		break;
	case "interest":
		$("#inthob").removeClass('plus').addClass('minus');
		$("#inthob-minus").show();
		$("#inthob-plus").hide();
		$("#inthob-below").show();
		break;
	case "lifestyle":
		$("#lifes").removeClass('plus').addClass('minus');
		$("#lifes-minus").show();
		$("#lifes-plus").hide();
		$("#lifes-below").show();
		break;
	case "religion":
		$("#othdet").removeClass('plus').addClass('minus');
		$("#othdet-minus").show();
		$("#othdet-plus").hide();
		$("#othdet-below").show();
		break;
	case "work":
		$("#workd").removeClass('plus').addClass('minus');
		$("#workd-minus").show();
		$("#workd-plus").hide();
		$("#workd-below").show();
		break;
	case "education":
		$("#edudet").removeClass('plus').addClass('minus');
		$("#edudet-minus").show();
		$("#edudet-plus").hide();
		$("#edudet-below").show();
		break;
 }
}

function hidebelowItem(show){
	switch(show){
	case "basics":
		$("#basicd").removeClass('minus').addClass('plus');
		$("#basicd-minus").hide();
		$("#basicd-plus").show();
		$("#basicd-below").hide();
		break;
	case "interest":
		$("#inthob").removeClass('minus').addClass('plus');
		$("#inthob-minus").hide();
		$("#inthob-plus").show();
		$("#inthob-below").hide();
		break;
	case "lifestyle":
		$("#lifes").removeClass('minus').addClass('plus');
		$("#lifes-minus").hide();
		$("#lifes-plus").show();
		$("#lifes-below").hide();
		break;
	case "religion":
		$("#othdet").removeClass('minus').addClass('plus');
		$("#othdet-minus").hide();
		$("#othdet-plus").show();
		$("#othdet-below").hide();
		break;
	case "work":
		$("#workd").removeClass('minus').addClass('plus');
		$("#workd-minus").hide();
		$("#workd-plus").show();
		$("#workd-below").hide();
		break;
	case "education":
		$("#edudet").removeClass('minus').addClass('plus');
		$("#edudet-minus").hide();
		$("#edudet-plus").show();
		$("#edudet-below").hide();
		break;
 }
}

$(document).ready(function() {
	if (location.search) {
				var queryString = location.search.substring(1);
				var queryObject = JSON.parse('{"'
						+ decodeURI(queryString).replace(/"/g, '\\"').replace(
								/&/g, '","').replace(/=/g, '":"') + '"}');

				if (queryObject.step) {
					showbelowItem(queryObject.step);
				}
			}
	$('body').on('change',"#fname,#lname",function() {
		checkAge();
	});
	
	$("#basicd").click(function(){
		if($("#basicd").hasClass('plus')){
			showbelowItem("basics");
		}else if($("#basicd").hasClass('minus')){
			hidebelowItem("basics");
		}
	});
	
	$("#inthob").click(function(){
		if($("#inthob").hasClass('plus')){
			showbelowItem("interest");
		}else if($("#inthob").hasClass('minus')){
			hidebelowItem("interest");
		}
	});
	
	$("#lifes").click(function(){
		if($("#lifes").hasClass('plus')){
			showbelowItem("lifestyle");
		}else if($("#lifes").hasClass('minus')){
			hidebelowItem("lifestyle");
		}
	});
	
	$("#othdet").click(function(){
		if($("#othdet").hasClass('plus')){
			showbelowItem("religion");
		}else if($("#othdet").hasClass('minus')){
			hidebelowItem("religion");
		}
	});
	
	$("#workd").click(function(){
		if($("#workd").hasClass('plus')){
			showbelowItem("work");
		}else if($("#workd").hasClass('minus')){
			hidebelowItem("work");
		}
	});
	
	$("#edudet").click(function(){
		if($("#edudet").hasClass('plus')){
			showbelowItem("education");
		}else if($("#edudet").hasClass('minus')){
			hidebelowItem("education");
		}
	});
	
});
