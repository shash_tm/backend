$(document).ready(function() {
	 d = new Date();
	activePlan = 1;
	$('body').on('click',"li[id^=plan_]",function()  {
		$("#paybtn").show();
		$("#paymentFrame").css('display', 'none');
		if($(this).attr("class") == 'payplan'){
			var id = this.id;
			var ids=this.id.split("_");
			activePlan = ids[1];
			$(".payplanactive").attr('class','payplan');
			$(this).attr('class','payplanactive');
			$("#plan_msg").html($("#plan_msg_"+activePlan).html());
			
			if($("#detailsFrame").length>0 && $("#detailsFrame").css('display')=='inline'){
				if($("#paymentFrame").length>0 && $("#paymentFrame").css('display')=='block'){
					$("#paybtn").click();
				}
			}else{
				/*$.post("payment.php",{
					planId:activePlan,
					action : "getSrc"
				},function(data){
					if(data.status=='success'){
						$('#paymentiFrame').attr('src',data.src);
						
					}else{
						alert("Some error occured");
					}
				},'json');*/
			}
		}
	});
	
	initialMsg();
	
	$('#paybtn').click(function() {
	     t1 = d.getTime();

		var email = "" , phone = "" , flag = true;
	/*	if($("#emailId").length>0){
			email = $("#emailId").val();
			var a = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
			if(!a){
				$("#email_error").html("Please enter email id correctly.");
				flag = false;
			}else{
				$("#email_error").html("");
			}
		}*/
		
	/*	if($("#phone").length>0){
			phone = $("#phone").val();
			var verify = isNaN(phone);
			if(verify){
				$("#phone_error").html("Enter number correctly");
				flag = false;
			}else if(phone.length != 10){
				$("#phone_error").html("Please enter 10 digits of mobile number.");
				flag = false;
			}else{
				$("#phone_error").html("");
			}
		}*/
		
		if(flag){
			$.post("payment.php",{
				planId:activePlan,
				action : "getSrcWithData"
				//email : email,
				//phone : phone
			},function(data){
				if(data.status=='success'){
					$('#paymentiFrame').attr('src',data.src);
					$("#paymentFrame").show();
					$("#paybtn").hide(); 
				}else{
					alert("Some error occured 2");
				}
			},'json');
		}
	});
});

$("#paymentiFrame").load(function(){
	 d1 = new Date();
	t2 = d1.getTime();
	var diff = t2 - t1;
	$.post("payments/paymentLog.php",{
		action : "iframe_load_time",
		time : diff
	},function(data){
			//alert("Iframe error");
		});
});

function initialMsg() {
	var id = $(".payplanactive").attr('id');
	var ids=id.split("_");
	activePlan = ids[1];
	$("#plan_msg").html($("#plan_msg_"+activePlan).html());	
}


function iframeError(){
	$.post("payments/paymentLog.php",{
		error_type : "iframe_error"
	},function(data){
			//alert("Iframe error");
		});

}