

$(document).ready(function(){

	var readOnly = $('#read-only-val').val();
	if(readOnly === "true")
	{
		$('#quiz-content-section input').attr('readonly', 'readonly');
		$('#quiz-content-section-new input').attr('readonly', 'readonly');
		$('.quiz-body input').attr('readonly', 'readonly');
	}




	$('#save-form-action').click(function(event)
	{
		validateFormAndSubmit(questionJson());
	});

	$('#add-new-question').click(function(event)
	{
		addQuestionHtml();
	});
	
	$.getJSON($('#base-url').val()+'/register_data.json', function( data ) {
	  	geoData = data;
			var currentState = $('#select-state-id').val(); 
			$('#state-select').append('<option value="0">===Select State===</option>'); 
			$.each(geoData, function (key, val) { 
				if (key === 'states') {
					$.each(val, function (skey, sval) {
						$('#state-select').append('<option value="' + sval.key + '">' + sval.value + '</option>');
					});
					$('#state-select').val(currentState);
					$('#state-select').trigger('change');
				}
			}); 
	});
	
	$('#state-select').change(function(event){
		var stateId = $(this).val();
		var currentCity = $('#select-city-id').val();
		$('#city-select').html('');
    	$.each( geoData, function( key, val ) {
		    if(key==='cities'){
		    	$('#city-select').append('<option value="0">===Select City===</option>');
		    	$.each( val, function( skey, sval ) {
		    		if(sval.state_id == stateId)
		    			$('#city-select').append('<option value="'+sval.city_id+'">'+sval.name+'</option>');
		    	});
		    	$('#city-select').val(currentCity);
		    }
    	});
		$('#city-select').trigger('change'); 
	})

	updateQuestionType();
	hideUnselected();

});




function validateFormAndSubmit(jsonObject){
	var ranks = [];
	var validation= true;
	var count=0;
	$('.question-rank').each(function(){
		count++;
	});
	$('.question-rank').each(function(){
		if($.inArray($(this).val(), ranks) === -1){
			ranks.push($(this).val());
		}else{
			application.global.alertModal("Duplicate Rank", "Rank "+$(this).val()+" has been set for more than one element", "danger");
			validation = false;
		}
		if(isNaN($(this).val())){
			application.global.alertModal("Non numeric Rank", "Rank "+$(this).val()+" is not a valid number", "danger");
			validation = false;
		}
		count--;
		if (count ==0  && validation) submitForm(jsonObject);
	});
	return true;
}

$(document).on("change", ".question_type", function(event){
	$(this).closest('.ques-body').find('.option-text-area').each(function(){
		$(this).toggle();
	});
	$(this).closest('.ques-body').find('.option-image-area').each(function(){
		$(this).toggle();
	})
});

/*
 To Validate the uploaded image dimensions for quizzes
Source :: http://www.aspsnippets.com/Articles/Validate-Check-dimensions-Height-and-Width-of-Image-before-Upload-using-HTML5-JavaScript-and-jQuery.aspx
 */
$(document).on("change", ".attach-photo", function(event)
{
	var ImageType = $(this).data('type-image');
	var reqHeight = 0;
	var reqWidth = 0  ;

	if(ImageType == 'image')
	{
		var reqHeight = 200 ;
		var reqWidth = 200 ;
		//console.log('Type Image');
	}
	else if (ImageType == 'banner')
	{
		var reqHeight = 300 ;
		var reqWidth = 823 ;
		//console.log('Type banner');
	}

	var fileUpload = $(this)[0];
	var fileElem = $(this);
	//IsValidImage = false ;
	//var alertMessage = "Empty";
	var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
	if (regex.test(fileUpload.value.toLowerCase()))
	{
		//Check whether HTML5 is supported.
		if (typeof (fileUpload.files) != "undefined")
		{
			//Initiate the FileReader object.
			var reader = new FileReader();
			//Read the contents of Image File.
			reader.readAsDataURL(fileUpload.files[0]);
			reader.onload = function (e)
			{
				//Initiate the JavaScript Image object.
				var image = new Image();
				//Set the Base64 string return from FileReader as source.
				image.src = e.target.result;
				image.onload = function ()
				{
					//Determine the Height and Width.
					var height = this.height;
					var width = this.width;
					if ((height == reqHeight && width  == reqWidth)|| ImageType == 'banner' )
					{
						fileElem.siblings('.upload-image').show();
						alertMessage = 'Image is accepatable. Click Upload';

					}
					else
					{
						alertMessage = 'Image must be  '+ reqWidth + ' x ' + reqHeight + '. Your image is '+ width + ' x ' + height;
						application.global.alertModal('Error',alertMessage, "danger");
						fileElem.val("");
					}
				};
			}
		}
		else
		{
			alertMessage = 'This browser does not support HTML5.' ;
			application.global.alertModal('Error',alertMessage, "danger");
			fileElem.val("");
		}
	}
	else
	{
		 alertMessage = 'Please select a valid Image file.' ;
		 application.global.alertModal('Error',alertMessage, "danger");
		 fileElem.val("");
	}

});


$(document).on("click", ".upload-image", function(event){
	var button= $(this);
	var actionUrl = $('#image-upload-url').val();
	var imagePrefix = $('#image-url-new').val();

	var formData = new FormData();
	formData.append('file', $(this).siblings('.attach-photo')[0].files[0]);
	formData.append('action','upload_image');
	formData.append('path','quiz/');
	$.ajax({
		url: actionUrl,
		type: "POST",
		data : formData,
		processData: false,  // tell jQuery not to process the data
		contentType: false,
		success: function(result){
			//console.log(result);
			result = JSON.parse(result);
			if(result.status == "success"){
				var completeImage = imagePrefix+'quiz/'+result.image_url ;
				//console.log('complete image'+ completeImage);
				button.siblings('.option-image-tag').attr('src',completeImage);
				button.siblings('.quiz-image-tag').attr('src',completeImage);
				button.siblings('.option-image-tag').attr('data-option-image',result.image_url);
				button.siblings('.quiz-image-tag').attr('data-quiz-image',result.image_url);
			}

			if(result.error)
			{
				alert(result.error);
			}
		},
		error: function(result){
			console.log('error');
			console.log(result);
			alert(error);
		}
	});
	//console.log($(this).siblings('.option-image-tag').data('option-image'));

});

function updateQuestionType(){
	$('.ques-type').each(function(){
		$(this).siblings('.question_type').val($(this).data('ques-type'));
	})
}

function hideUnselected(){
	$('.ques-type-for-option').each(function(){
		if($(this).data('ques-type-option') == 'text')
		{
			$(this).siblings('.option-image-area').hide();
		}
		else
		{
			$(this).siblings('.option-text-area').hide();
		}

	})
}



function questionJson(){
	var jsonData = {
		quiz_id: $('#quiz-id').val(),
		title: $('#quiz-title').val(),
		display_name: $('#quiz-display-name').val(),
		description: $('#quiz-description').val(),
		status: $('#quiz-status').val(),
		image: $('#image-url').data('quiz-image'),
		banner: $('#banner_url').data('quiz-image'),
		state_id:$('#state-select').val(),
		city_id:$('#city-select').val(),
		questions : []
	};
	var questions = $('.ques-body');
	questions.each(function()
	{
		var quesData = {};
		var ques = $(this).find('.question')
		quesData['question_id'] = ques.data('question-id');
		quesData['question_text'] = ques.val();
		quesData['rank'] = $(this).closest('.ques-body').find('.question-rank').val() ;
		quesData['status'] = $(this).closest('.ques-body').find('.question-status').val() ;
		quesData['options'] = [];
		var typeOption = $(this).closest('.ques-body').find('.question_type').val() ;
		$('.option', $(this)).each(function(){
			var option = {};
			option.option_id = $(this).data('option-id');
			if( typeOption == 'text')
			{
				option.option_text = $(this).val();
				option.option_image = null;
				if($(this).is(':visible') == true)
					quesData.options.push(option);

			}
			else
			{
				option.option_image = $(this).data('option-image');
				option.option_text = null;
				if($(this).is(':visible') == true)
					quesData.options.push(option);
			}
		});
		jsonData.questions.push(quesData);
	});
	//console.log(JSON.stringify(jsonData));
	return JSON.stringify(jsonData);
}

function submitForm(jsonObj){
	var actionUrl=$('#action-url').val();
	var quizViewUrl=$('#quiz-view-url').val();
	  $.ajax({
		 url: actionUrl, 
		 type: "POST",
		 contentType: "application/json",
		 data: jsonObj,
		 success: function(result){
			// console.log(result);
			 result = JSON.parse(result);
			 if(result.status == "success"){
				window.location.href = quizViewUrl + '?quiz_id=' + result.quiz_id;
			 }
			 else if(result.error)
			 {
				 alert(result.error);
			 }
	    },
	    error: function(result){
			alert(result.error);
	    	console.log('error');
	    	console.log(result);
	    }
	 }); 
}

function addQuestionHtml()
{
	var QuestionHtml =  '<div class="panel-body ques-body">' +
		'							<div class="col-md-8 col-lg-8 col-sm-8">' +
		'								<div class="form-group">' +
		'									<div class="input-group">' +
		'										<span class="input-group-addon" id="basic-addon1">Question</span>' +
		'										<input type="text" class="form-control question" placeholder="" aria-describedby="basic-addon1" value="" data-question-text="">' +
		'									</div>' +
		' 						</div>' +
		' 							</div>' +
		' 					<div class="col-md-1 col-lg-1 col-sm-1">'+
		'					<div class="form-group">'+
		'					<div class="input-group">'+
		'					<select class="form-control question-status" placeholder="" aria-describedby="basic-addon1">'+
		'					<option selected value="active" class="question-status-val">active</option>'+
		'					<option value="deleted" class="question-status-val">deleted</option>'+
		'					</select>'+
		'					</div>'+
		'					</div>'+
		'					</div>'+
		'                   <div class="col-md-2 col-lg-2 col-sm-2">'+
		'					<div class="form-group">'+
		'					<div class="input-group">'+
		'					<span class="input-group-addon" id="basic-addon1">Rank</span>'+
		'					<input type="text" class="form-control question-rank" placeholder="" aria-describedby="basic-addon1" value=""/>'+
		'					</div>'+
		'					</div>'+
		'					</div>'+
		' 							<div class="col-md-1 col-lg-1 col-sm-1">' +
		'								<div class="form-group">' +
		'									<div class="input-group">' +
		'										<input type="hidden" class="ques-type-val" data-ques-type="text">' +
		'										<select class="form-control question_type" placeholder="" aria-describedby="basic-addon1">' +
		'											<option value="image" class="question-type-val">image</option>' +
		'											<option selected="" value="text" class="question-type-val">text</option>' +
		'										</select>' +
		'									</div>' +
		'								</div>' +
		'							</div>' +
		'																	<div class="col-md-3 col-lg-3 col-sm-3">' +
		'										<div class="form-group">' +
		'											<input type="hidden" class="ques-type-for-option" data-ques-type="text">' +
		'											<div class="input-group option-text-area">' +
		'												<span class="input-group-addon" id="basic-addon1">A</span>' +
		'														<input type="text" class="form-control option" placeholder="" aria-describedby="basic-addon1" value="" data-option-id="A" data-option-text="">' +
		'											</div>' +
		'											<div class="input-group option-image-area" style="display: none;">' +
		'												<span class="input-group-addon" id="basic-addon1">A</span>' +
		'												<img class="option option-image-tag" src="" data-option-id="A" data-option-image="" width="200" height="200">' +
		'												<input type="file" class="attach-photo" data-type-image="image">'+
		'        										<button class="upload-image" style="display: none;">Upload</button>'+
		'											</div>' +
		'										</div>' +
		'									</div>' +
		'																	<div class="col-md-3 col-lg-3 col-sm-3">' +
		'										<div class="form-group">' +
		'											<input type="hidden" class="ques-type-for-option" data-ques-type="text">' +
		'											<div class="input-group option-text-area">' +
		'												<span class="input-group-addon" id="basic-addon1">B</span>' +
		'														<input type="text" class="form-control option" placeholder="" aria-describedby="basic-addon1" value="" data-option-id="B" data-option-text="">' +
		'											</div>' +
		'											<div class="input-group option-image-area" style="display: none;">' +
		'												<span class="input-group-addon" id="basic-addon1">B</span>' +
		'												<img class=" option option-image-tag" src="" data-option-id="B" data-option-image="" width="200" height="200">' +
		'												<input type="file" class="attach-photo" data-type-image="image">'+
		'        										<button class="upload-image" style="display: none;">Upload</button>'+
		'											</div>' +
		'										</div>' +
		'									</div>' +
		'																	<div class="col-md-3 col-lg-3 col-sm-3">' +
		'										<div class="form-group">' +
		'											<input type="hidden" class="ques-type-for-option" data-ques-type="text">' +
		'											<div class="input-group option-text-area">' +
		'												<span class="input-group-addon" id="basic-addon1">C</span>' +
		'														<input type="text" class="form-control option" placeholder="" aria-describedby="basic-addon1" value="" data-option-id="C" data-option-text="">' +
		'											</div>' +
		'											<div class="input-group option-image-area" style="display: none;">' +
		'												<span class="input-group-addon" id="basic-addon1">C</span>' +
		'												<img class=" option option-image-tag" src="" data-option-id="C" data-option-image="" width="200" height="200">' +
		'												<input type="file" class="attach-photo" data-type-image="image">'+
		'        										<button class="upload-image" style="display: none;">Upload</button>'+
		'											</div>' +
		'										</div>' +
		'									</div>' +
		'																	<div class="col-md-3 col-lg-3 col-sm-3">' +
		'										<div class="form-group">' +
		'											<input type="hidden" class="ques-type-for-option" data-ques-type="text">' +
		'											<div class="input-group option-text-area">' +
		'												<span class="input-group-addon" id="basic-addon1">D</span>' +
		'														<input type="text" class="form-control option" placeholder="" aria-describedby="basic-addon1" value="" data-option-id="D" data-option-text="">' +
		'											</div>' +
		'											<div class="input-group option-image-area" style="display: none;">' +
		'												<span class="input-group-addon" id="basic-addon1">D</span>' +
		'												<img class=" option option-image-tag" src="" data-option-id="D" data-option-image="" width="200" height="200">' +
		'												<input type="file" class="attach-photo" data-type-image="image">'+
		'        										<button class="upload-image" style="display: none;">Upload</button>'+
		'												</div>' +
		'										</div>' +
		'									</div>' +
		'						</div>' ;

	$('.quiz-body').append(QuestionHtml);

}
