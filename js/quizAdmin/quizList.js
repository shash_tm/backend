function updateDatespot(){
	$('#yes-button').hide();
	$('#cancel-button').hide();
	$('#show-wait').show();
	var actionUrl=$('#quiz-list-form').attr('action');
	var quiz_id = $('#datespot-hash-id-for-update').val();
	var action = $('#datespot-update-action').val();

	$.ajax({
		 url: actionUrl, 
		 type: "POST",
		// contentType: "application/json",
		 data: {'quiz_id':quiz_id,'action':action},
		 success: function(result){
			 result = JSON.parse(result);
			 if(result.status == "success"){
				 location.reload();
			 }
	    },
	    error: function(result){
	    	console.log('error');
	    	console.log(result);
	    }
	 }); 
}

function updateRank(elem){
	// not being used right now
	var quiz_id = elem.data('id');
	var action = elem.data('action');
	var rank = parseInt(elem.parents('td').children('span').text().trim());
	var actionUrl=$('#quiz-list-form').attr('action');
	var origRank = rank;
	if(action == 'up')
		rank += 1;
	else if(action == 'down')
		rank -= 1;


	elem.parents('td').children('span').text(rank);
	$.ajax({
		url: actionUrl,
		type: "POST",
		// contentType: "application/json",
		data: {'quiz_id':quiz_id,'action':'update_rank','rank':rank},
		success: function(result){
			result = JSON.parse(result);
			if(result.status == "success"){
			//	location.reload();
			}
		},
		error: function(result){
			console.log('error');
			console.log(result);
		}
	});
}

$(document).on("click", ".delete-it", function(event){
	deleteQuizAction($(this), "delete");
});

$(document).on("click", ".undo-delete", function(event){
	deleteQuizAction($(this), "undo_delete");
});

function deleteQuizAction(element, action){
	application.global.globalWaitDialog("show");
	var actionUrl=$('#quiz-list-form').attr('action');
	var quizId = element.data('id')
	$.ajax({
		url: actionUrl,
		type: "POST",
		// contentType: "application/json",
		data: {"quiz_id": quizId,
			   "action": action},
		success: function (result) {
			result = JSON.parse(result);
			if(result.status == "success") {
				application.global.globalWaitDialog("hide");
				if (action == 'delete') {
					//  element.closest('tr').slideUp();
					element.removeClass();
					element.addClass("glyphicon");
					element.addClass("glyphicon-repeat");
					element.addClass("undo-delete");
					element.closest('tr').removeClass("success");
					element.closest('tr').addClass("danger");
				} else if (action == "undo_delete") {
					element.closest('tr').removeClass("danger");
					element.closest('tr').addClass("success");
					element.removeClass();
					element.addClass("glyphicon");
					element.addClass("glyphicon-trash");
					element.addClass("delete-it");
				}
			}
			if(result.status == "failure"){
				application.global.alertModal("Error", "Some error occured while updating. Please try again later.", "danger")
			}
		}
	});
}

$(document).ready(function(){
	$('.update-action').click(function(event){
		event.preventDefault();
		$('#datespot-hash-id-for-update').val($(this).data('id'));
		$('#datespot-update-action').val($(this).data('action'));
		$('#action-name').text($(this).data('action'));
	})


})