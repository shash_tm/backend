<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";
require_once dirname ( __FILE__ ) . "/../Utils/ArrayUtils.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";

/**
 * ProfileCampaign class shall be used to serve advertising campaigns as user profiles
 * as a part of the current recommendations shown.
 * The SQL table 'advertisement_campaign' is used to specify the details of the ad campaigns.
 * The following are the details :
 * 1. Profile Id of the campaign (created as a new registered user)
 * 2. Profile Name
 * 3. Status - 'active'/'inactive' - only active ones shall be used.
 * 4. Filters - A string representation of json object that acts as a filter.
 *              Accepted keys: Gender,Age and Location (State) and City.
 *              In case of no preferences on a field , do not include it in the json object.
 *              NOTE: The filters on State and City are not hierarchical and are independent i.e. both State and Location should pass for the filter to get successful.
 *              In a way the City filter if specified will override the ones mentioned in the State filter.
 * 5. Timestamp - for logging
 * 6. Message - the promotional message to be received
 */
class ProfileCampaign {
    
    private static $isProfileCampaigningActive = true;
    
    public static function isProfileCampaigningActive() {
        return ProfileCampaign::$isProfileCampaigningActive;
    }
    
    /*
     * Retrieve the display message for an ad campaign
     */
    public static function getAdCampaignMessage($user_id) {
        global $conn_slave;
        $query = "SELECT custom_message,coupon_code_message,msg_type, is_event, event_details from advertisement_campaign where user_id = $user_id";
        $rows = $conn_slave->Execute($query)->FetchRow();
        return $rows;
    }
    
    /*
     * Retrieve the json representing the ad campaign links
     */
    public static function getAdCampaignLinks($user_id) {
        global $conn_slave;
        $query = "SELECT campaign_links from advertisement_campaign where user_id = $user_id";
        $row = $conn_slave->Execute($query)->FetchRow();
        return json_decode($row['campaign_links'],true);
    }

    /*
  * Retrieve the event details for that particular campaign
  */
    public static function getEventDetailsForAdCampaign($user_id) {
        global $conn_slave;
        $query = "SELECT is_event,event_details from advertisement_campaign where user_id = $user_id";
        $row = $conn_slave->Execute($query)->FetchRow();
        return $row;
    }
    /*
     * Check if the current user is a part of the ad campaigning
     */
    public static function isUserAdCampaign($user_id) {
        global $conn_slave;
        $query = "SELECT user_id from advertisement_campaign where user_id = $user_id";
        $rows = $conn_slave->Execute($query)->GetRows();
        return (sizeof($rows))>0?true:false;
    }
    
    /*
     * Check if the current user is a part of the ad campaign and isMutualMatchAllowed is true
     */
    public static function isUserMutualMatchableAdCampaign($user_id) {
        global $conn_slave;
        $query = "SELECT user_id from advertisement_campaign where isMutualMatchAllowed = 'true' and user_id = $user_id";
        $rows = $conn_slave->Execute($query)->GetRows();
        return (sizeof($rows))>0?true:false;
    }
    
    public static function getAllCampaignIds() {
        global $conn_slave;
        $query = "SELECT user_id from advertisement_campaign";
        $rows = $conn_slave->Execute($query)->GetRows();
        $result = array();
        foreach($rows as $row) {
            $result[] = $row['user_id'];
        }
        return $result;
    }
    
    /*
     * Select query to retrieve campaign ids with filters for all active campaigns
     */
    public static function getUserIdWithFiltersForActiveCampaigns() {
        global $conn_slave;
        $query = "SELECT user_id,filters from advertisement_campaign where status = 'active'";
        $rows = $conn_slave->Execute($query)->GetRows();
        return $rows;
    }
    
    /*
     * Select query to retrieve new (the ones that have not been viewed before) 
     * campaign ids with filters for all active campaigns
     */
    public static function getNewUserIdWithFiltersForActiveCampaigns($user_id) {
        global $conn_slave;
        $query = "SELECT user_id,filter from advertisement_campaign where status = 'active' and "
                . "user_id not in (select user2 from user_like where user1 = $user_id) and "
                . "user_id not in (select user2 from user_hide where user1 = $user_id);";
        $rows = $conn_slave->Execute($query)->GetRows();
        return $rows;
    }
    
    /*
     * Select query to retrieve all active campaign ids
     */
    public static function getUserIdsForActiveCampaigns() {
        global $conn_slave;
        $query = "SELECT user_id from advertisement_campaign where status = 'active'";
        $rows = $conn_slave->Execute($query)->GetRows();
        $result = array();
        foreach($rows as $row) {
            $result[] = $row['user_id'];
        }
        return $result;
    }
    
    /*
     * The function places ad campaigns at appropriate locations in the current recommendations.
     * Campaigns that are active and relevant (as defined by the respective filter) are used.
     * Each campaing is inserted at interval of 5 or ratio of number of recommendations to active campaigns
     * whichever is minimum.
     * @param - $recommendations - input recommendations
     * @param - $user_id,$gender,$age,$location - details of the user to whom the matches are being shown
     * @return - array containg the user ids with merged ad campaigns
     */
    public static function mergeRecommendationResultsWithActiveCampaigns(
            $recommendations,
            $user_id,
            $gender,
            $age,
            $location,
            $myCity) {
        
        error_reporting(E_ERROR);
        $adCampaigns = ProfileCampaign::getAllCampaignIds();
        $result = array_diff($recommendations, $adCampaigns);
        
        $recommendationCount = sizeof($result);
        $relevantCampaignIds = ProfileCampaign::getRelevantProfileAdCampaigns($user_id,$gender,$age,$location,$myCity);
        $relevantCampaignCount = sizeof($relevantCampaignIds);
        if($relevantCampaignCount > 0) {
            $ratio = ceil($recommendationCount/$relevantCampaignCount);
            $insertionGap  = ($ratio < 4)?(($recommendationCount < 4)?ceil($recommendationCount/2):$ratio):4;
            if($ratio >= 0) {
                for($i=0; $i < $relevantCampaignCount && $insertionGap*($i+1)+$i <= sizeof($result); $i++) {
                    ArrayUtils::insertValueAtPos($result,$insertionGap*($i+1)+$i,$relevantCampaignIds[$i]);
                }
            }
        }
        return $result;
    }
    
    public function getRelevantProfileAdCampaigns(
            $user_id,
            $gender,
            $age,
            $location,
            $myCity) {
        
        error_reporting(E_ERROR);
        $activeCampaigns = ProfileCampaign::getNewUserIdWithFiltersForActiveCampaigns($user_id);
        $relevantCampaignIds = array();
        if(sizeof($activeCampaigns) > 0) {
            // This functionality will work only when the call is made from the user's app since the source is coming from header values
            $functionClass =   new functionClass();
            $source = $functionClass->getSourceFromHeader();
            foreach($activeCampaigns as $campaign) {
                $campaign_id = $campaign['user_id'];
                $filt = $campaign['filter'];
                $filter = json_decode($filt, true);
                if ($filter == NULL | 
                        (  ($filter['Gender'] == NULL   | in_array($gender, $filter['Gender']))
                         & ($filter['Age'] == NULL      | in_array($age, $filter['Age']))
                         & ($filter['Location'] == NULL | in_array($location, $filter['Location']))
                         & ($filter['Source'] == NULL   | in_array($source, $filter['Source']))
                         & ($filter['City'] == NULL     | in_array($myCity, $filter['City'])))) {
                    array_push($relevantCampaignIds,$campaign_id);
                }
            }
        }
        return($relevantCampaignIds);
    }
    
    public function getUserCity($user_id) 
    {
    	$sql = "SELECT user_id, stay_city FROM user_data WHERE user_id = ? ";
    	$city= Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
    	return $city;
    }
    
    public function getCouponCode($user_id, $city_id) 
    {
    	$sql = "select coupon_id, coupon_code from campaign_coupon_codes where campaign_id= ?  and status='available' and valid_after < now() limit 1";
    	$coupon= Query_Wrapper::SELECT($sql, array ($user_id, $city_id), Query::$__slave,true) ;
    	return $coupon ;
    }
    
    public function setCouponStatus($coupon_code, $status, $user_id) 
    {
    	$table= "campaign_coupon_codes" ;
    	$sql = "UPDATE campaign_coupon_codes SET status= ?, assigned_to = ? WHERE coupon_code =?";
    	Query_Wrapper::UPDATE($sql, array($status, $user_id, $coupon_code), $table) ;
    }
}
