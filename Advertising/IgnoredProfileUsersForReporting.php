<?php

require_once dirname ( __FILE__ ). "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../Utils/ArrayUtils.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
/**
 * [ad_campaigns]
 * allUserIds[] = ""
 */
class IgnoredProfileUsersForReporting {
    
    static public function getUserIds() {
        global $config_ignore_user_ids;
	$adCampaigns = $config_ignore_user_ids['ad_campaigns'];
	$allUserIds = $adCampaigns['allUserIds'];
	return $allUserIds;
    }
    
    static public function getUserIdsString() {
        global $miss_tm_id;
        $ignoredAdCampaignUserIds = IgnoredProfileUsersForReporting::getUserIds();
	$ignoredProfileCampaignsUserIds = explode(",",$ignoredAdCampaignUserIds[0]);
	$ignoredProfileCampaignsUserIds[] = $miss_tm_id;
        
        #Pick up Ad CampaignIds dynamically
        global $conn_slave;
        $query = "SELECT user_id from advertisement_campaign";
        $rows = $conn_slave->Execute($query)->GetRows();
        foreach($rows as $row){
            $ignoredProfileCampaignsUserIds[] = $row['user_id'];
        }
        $ignoredProfileCampaignsUserIds = array_unique($ignoredProfileCampaignsUserIds);
        
	$ignoredAdCampaignUserIds = "(" . implode(",",$ignoredProfileCampaignsUserIds) . ")";
        return $ignoredAdCampaignUserIds;
    }
}
