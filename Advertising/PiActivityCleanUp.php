<?php

require_once dirname(__FILE__) . "/IgnoredProfileUsersForReporting.php";
require_once dirname(__FILE__) . "/../abstraction/query.php";

/*
 * The class is meant for one time use to eliminate inadvertent like/hides 
 * incorporated in the user_pi_activity table total_likes/total_hides summations.
 */
class PiActivityCleanUp {

    public function cleanTable() {
        $ignoredAdCampaignUserIds = IgnoredProfileUsersForReporting::getUserIdsString();
        $query = "select user2 as user_id,count(*) as likes from user_like "
                . "where user1 in $ignoredAdCampaignUserIds group by user2;";
        $likes = Query::SELECT($query, array(), Query::$__slave, true);
        foreach ($likes as $like) {
            $likeTable[$like['user_id']] = $like['likes'];
        }

        $query = "select user2 as user_id,count(*) as hides from user_hide "
                . "where user1 in $ignoredAdCampaignUserIds group by user2;";
        $hides = Query::SELECT($query, array(), Query::$__slave, true);
        foreach ($hides as $hide) {
            $hideTable[$hide['user_id']] = $hide['hides'];
        }

        $tablename = "user_pi_activity";
        $userIdsToUpdate = array_unique(array_merge(array_keys($likeTable), array_keys($hideTable)));
        foreach ($userIdsToUpdate as $user_id) {
            if (array_key_exists($user_id, $hideTable) && array_key_exists($user_id, $likeTable)) {
                $query = "update user_pi_activity set total_likes = total_likes - ? ,total_hides = total_hides - ? where user_id = ?;";
                Query::UPDATE($query, array($likeTable[$user_id], $hideTable[$user_id], $user_id), $tablename, Query::$__master);
            } else if (array_key_exists($user_id, $likeTable)) {
                $query = "update user_pi_activity set total_likes = total_likes - ? where user_id = ?;";
                Query::UPDATE($query, array($likeTable[$user_id], $user_id),$tablename, Query::$__master);
            } else if (array_key_exists($user_id, $hideTable)) {
                $query = "update user_pi_activity set total_hides = total_hides - ? where user_id = ?;";
                Query::UPDATE($query, array($hideTable[$user_id], $user_id),$tablename, Query::$__master);
            }
        }
    }
}

try {
    echo "Starting";
    $notifier = new PiActivityCleanUp();
    $notifier->cleanTable();
} catch (Exception $e) {
    echo $e->getTraceAsString();
    trigger_error($e->getTraceAsString(), E_USER_WARNING);
    trigger_error($e->getMessage(), E_USER_WARNING);
}
?>