<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
include dirname ( __FILE__ ) . "/../include/allValues.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";



/**
 * Matches Database Object
 * Contains all the db queries related to a match Set
 * @author himanshu
 */

class MatchesDBO{

	/**
	 * get profiles liked by a user
	 * @param $user_id
	 */
	public function getLikesData($user_id, $start_range = null, $end_range = null){

		$sql = "select user2 from user_like where user1=?
				AND user2 not in (select user2 from user_reject where user1= ?) 
				AND user2 not in (select user1 from user_reject where user2 = ?) 
				order by timestamp desc";

		if(isset($start_range) && isset($end_range)){
			$sql .= " Limit $start_range, $end_range";
		}
			
		$param_array = array($user_id, $user_id, $user_id);
		$data = Query::SELECT($sql, $param_array);

		$ids =array();
		foreach($data as $val){
			$ids[]=$val['user2'];
		}
		return $ids;

	}

}



?>