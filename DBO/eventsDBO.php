<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";

class EventsDBO
{
	public static function getActiveEventCategoriesByCity($city_id, $state_id, $countFilter=true){
		if($state_id==2168)
			$city_id = 0;
		global $imageurl;
		$sql = "select category_id, name as title, background_image,image_url as icon_image, event_count, city_id
					from event_category where state_id = ? ";
		if($countFilter)
			$sql .= " and event_count > 0 ";

		if($city_id != null && $city_id != 0){
			$sql .= " and (city_id = ? or city_id is null or city_id = 0) ";
			$param_array = array($state_id,$city_id);
		}else{
			$param_array = array($state_id);
		}

		$categoryList =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
		foreach($categoryList as $key => $value){
			$eventCount = $categoryList[$key]['event_count'];
			$categoryList[$key]['background_image'] = $imageurl.$value['background_image'];
			$categoryList[$key]['event_count'] = $eventCount == 1 ? "$eventCount Listing" : "$eventCount Listings";
		}
		return $categoryList;
	}

	public static function getEventsList($category_id=null,$zone_ids=null, $followStatus="recommended", $user_id=null, $isTest="false", $is_ab = false){
		$param_array = array();
		$sql = "SELECT d.list_view_image as image, d.hash_id as datespot_id, d.status, d.friendly_name, d.name, gc.name as city_name,d.is_trulymadly,d.tm_status,
 				gs.name as state_name, dl.location, d.rank, d.create_date, d.location_count, d.date_pretty_text as pretty_date,
 				geo_location,dl.zone_id, d.event_date as event_date, d.rank, d.activate_date, DATEDIFF(now(), d.activate_date) as event_age FROM datespots d
				LEFT JOIN datespot_location dl on dl.datespot_id=d.datespot_id
				JOIN geo_state gs on gs.state_id = dl.state_id
				LEFT JOIN geo_city gc on gc.city_id = dl.city_id AND gc.state_id =dl.state_id
				LEFT JOIN event_category_mapping ecm on ecm.datespot_id=d.datespot_id ";

		if($followStatus == "following"){
			$sql .= " join user_event ue on ue.datespot_hash_id = d.hash_id where ue.status='follow' and ue.user_id = $user_id ";
		}else if($followStatus == "recommended"){
			$sql .= " left join user_event ue on ue.datespot_hash_id = d.hash_id and ue.user_id=$user_id where ue.datespot_hash_id is null ";	//fetching user id from session, so sql injection prevented.
		}

		$sql .= " and type='event' and date(d.event_date) >= date(now()) ";

		if(!$is_ab){
			$sql .=  " and ecm.category_id = ? ";
			$param_array[] = $category_id;
		}else{
			$categories = explode(",", $category_id);
			$newCat = array();
			foreach($categories as $value){
				if(is_integer((int)$value))
					$newCat[] = $value;
			}
			$catString = implode(",",$newCat);
			$sql .=  " and ecm.category_id in ($catString) ";
		}

		if($isTest)
			$sql .= " and d.status in ('active', 'test') ";
		else
			$sql .= " and d.status = 'active'";

		if($zone_ids != null && $zone_ids != -1){
			$sql .= " and dl.zone_id in ($zone_ids)";
		}

		$sql .= " group by d.datespot_id order by d.rank desc, d.datespot_id  desc" ;

		return Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
	}

	public static function getDatespotById($datespot_id)
	{
		$sql = "select d.hash_id as datespot_id,friendly_name,d.name as name, images_json as images, terms_and_conditions,d.offer as details,
				list_view_image as image, d.location_count, d.sms_text, d.date_pretty_text as pretty_date, d.event_ticket_url,
				d.status as status,d.event_ticket_text as event_ticket_text, d.event_date as event_date,d.pricing as ticket_price,
				d.is_trulymadly, d.fb_page_url, d.tm_status, d.spark_display_name
				from datespots d where d.hash_id=? ";
		$datespot = Query_Wrapper::SELECT($sql, array($datespot_id), Query::$__slave, "true" ) ;
		return $datespot;
	}

	public static function getEventFollowStatus($user_id, $event_id){
		$sql = "select user_id, datespot_hash_id as datespot_id, status, update_date from user_event where user_id = ? and datespot_hash_id = ?";
		return Query_Wrapper::SELECT($sql, array($user_id,$event_id), Query::$__slave);
	}

	public static function updateUserEvent($user_id, $datespot_id, $status){
		if($status == "follow"){
			$sql = "INSERT into user_event (status, user_id, datespot_hash_id, update_date) values(?, ?, ?, now())";
			$param_array = array($status,$user_id,$datespot_id);
		}else{
			$sql = "DELETE from user_event where user_id=? and datespot_hash_id=?";
			$param_array = array($user_id,$datespot_id);
		}
		Query_Wrapper::INSERT($sql, $param_array, "user_event") ;
	}

	/*
	 * Temporary function. Will be replaced by Vineet's
	 * hook to provide the required data
	 */
	public static function getUsersFollowingEvent($event_id, $current_user,$oppositeGender){
		$sql = "select ue.user_id, u.fname, up.thumbnail from user_event ue
				join user u on u.user_id=ue.user_id
				join user_photo up on up.user_id=ue.user_id
				where ue.status='follow' and up.status ='active' and up.is_profile='yes' and ue.datespot_hash_id = ? and ue.user_id != ? and u.gender = ?";
		return Query_Wrapper::SELECT($sql, array($event_id, $current_user,$oppositeGender), Query::$__slave);
	}

        /*
         * The function is essentially an overloaded version of the function getUsersFollowingEvent 
         * but returns only the ids.
         */
        public static function getUserIdsFollowingEvent($event_id, $current_user,$oppositeGender){
        $sql = "select  u.user_id from user u
                join user_event ue on u.user_id=ue.user_id
                where ue.status='follow' and ue.datespot_hash_id = ? and ue.user_id != ? and u.gender = ?";
        return Query_Wrapper::SELECT($sql, array($event_id, $current_user,$oppositeGender), Query::$__slave);
        }

        /*
         * The function is essentially an over loaded function of the function getUsersFollowingEvent 
         * but filters based on the passed list of users
         */
        public static function getNameAndThumbNailForUsers($filterIds,$event_id, $current_user,$oppositeGender) {
            $filterIdsString = "(" . implode(',',$filterIds) . ")";
            $sql = "select u.user_id, u.fname, up.thumbnail from user_event ue
                    join user u on u.user_id=ue.user_id
                    join user_photo up on up.user_id=ue.user_id
                    where ue.status='follow' and up.status ='active' and up.is_profile='yes' and ue.user_id in $filterIdsString and ue.datespot_hash_id = ? and ue.user_id != ? and u.gender = ? ORDER BY FIELD(u.user_id,". implode(',',$filterIds).")";
            return Query_Wrapper::SELECT($sql, array($event_id, $current_user,$oppositeGender), Query::$__slave);
        }
        
	public function addNewCategory($categoryName, $stateId, $cityId, $imageUrl, $backgroundImage){
		$categoryId=null;
		$sql = "INSERT INTO event_category(name,status,image_url,background_image,event_count,state_id,city_id) VALUES (?,'active',?,?,0,?,?)";
		$categoryId = Query_Wrapper::INSERT($sql, array($categoryName, $imageUrl, $backgroundImage, $stateId, $cityId), "event_category");
		return $categoryId;
	}

	public function getEventCategory($event_id){
		$sql = "select category_id from event_category_mapping where datespot_id = ? limit 1";
		$category =  Query_Wrapper::SELECT($sql, array($event_id), Query::$__slave);
		if(sizeof($category) > 0 )
			return $category[0];
		else return -1;
	}
        
	public function getMutualMatchesForEvent($user_id, $event_id, $limit = -1){
		/*$sql = "select ue.datespot_hash_id, um.receiver_id as match_id, up.thumbnail from messages_queue um join user_event ue on ue.user_id=um.receiver_id ".
				" left join user_photo up on up.user_id = um.receiver_id and up.is_profile='yes' where ue.datespot_hash_id= ?  and um.msg_type != 'SPARK' and um.blocked_by is null and um.sender_id = ? group by ue.datespot_hash_id, match_id order by tStamp desc";*/
		// IN query above, spark wasn't handled.
		$sql = "select ue.datespot_hash_id, um.receiver_id as match_id, up.thumbnail from messages_queue um join user_event ue on ue.user_id=um.receiver_id left join user_photo up on up.user_id = um.receiver_id and up.is_profile='yes'
				left join user_spark us on ((um.receiver_id = us.user1 and um.sender_id = us.user2) or (um.receiver_id = us.user2 and um.sender_id = us.user1))
				where ue.datespot_hash_id= ?  and um.blocked_by is null and um.sender_id = ?  and (us.status is null or us.status='accepted')
				group by ue.datespot_hash_id, match_id order by um.tStamp desc";
		if($limit > 0){
			$sql .= " limit $limit";
		}
		return Query_Wrapper::SELECT($sql, array($event_id, $user_id));
	}

	/*
   * following was a redundant function due to bad coding practice. Shall remove code after safely releasing the code.

   public function getMutualMatchesForEventWithLimit($user_id, $event_id,$limit){
       $sql = "select ue.datespot_hash_id, um.receiver_id as match_id, up.thumbnail from messages_queue um join user_event ue on ue.user_id=um.receiver_id ".
               " left join user_photo up on up.user_id = um.receiver_id and up.is_profile='yes' where ue.datespot_hash_id= ? and um.blocked_by is null and um.sender_id = ? group by ue.datespot_hash_id, match_id order by tStamp desc limit $limit";
       if($limit < 0) {
           return eventsDBO::getMutualMatchesForEvent($user_id,$event_id);
       }
       return Query_Wrapper::SELECT($sql, array($event_id, $user_id));
   }*/

	public function getMutualMatchesForAllEvents($user_id){
		$sql = "select ue.datespot_hash_id, um.receiver_id as match_id, up.thumbnail from messages_queue um join user_event ue on ue.user_id=um.receiver_id  ".
			" left join user_photo up on up.user_id = um.receiver_id and up.is_profile='yes' where um.blocked_by is null and um.sender_id = ? group by ue.datespot_hash_id, match_id ";
		return Query_Wrapper::SELECT($sql, array($user_id));
	}
        
        /*
         * The funciton is an overloaded version of the function getMutualMatchesForAllEvents which
         * also honours the limit passed to it.
         */
        public function getMutualMatchesForAllEventsWithLimit($user_id,$limit){
            $sql = "select ue.datespot_hash_id, um.receiver_id as match_id, up.thumbnail from messages_queue um join user_event ue on ue.user_id=um.receiver_id  ".
                    " left join user_photo up on up.user_id = um.receiver_id and up.is_profile='yes' where um.blocked_by is null and um.sender_id = ? group by ue.datespot_hash_id, match_id limit $limit";
            if($limit < 0) {
                return eventsDBO::getMutualMatchesForAllEvents($user_id);
            }
            return Query_Wrapper::SELECT($sql, array($user_id));
        }

	public function getUserCountForEvent($event_id){
		global $redis;
		if($redis->exists("event_user_count_".$event_id)){
			$count = $redis->get("event_user_count_".$event_id);
		}else{
			$sql = "select count(*) as count from user_event where datespot_hash_id = ?";
			$res = Query_Wrapper::SELECT($sql, array($event_id), Query::$__slave, true);
			$count = $res['count'];
			$redis->set("event_user_count_".$event_id, $count, 300);
		}
		return $count;
	}
}
