<?php 
require_once dirname (__FILE__). '/../include/config.php';
require_once dirname (__FILE__). '/../abstraction/query_wrapper.php';

class UserFlagsDBO {
	private $redis;
	
	function __construct(){
		global $redis;
		$this->redis=$redis;
		
	}
	
	function read($redis_key,$key=NULL)
	{
		$val=array();
 	  	//check if key exists in redis
 	  	$ret=$this->redis->exists($redis_key);
 	  	if($ret==TRUE)
 	  	{
 	  	$type=$this->getType($redis_key);
		//var_dump($type);
		switch ($type) {
			case 1:
			//string
				$val[]=$this->read_string($redis_key);
				break;
			case 2:
			//sets	
				$val=$this->read_set($redis_key);
				break;
			case 3:
			//list	
				$val=$this->read_list($redis_key);
				break;
			case 4:
			//sorted sets
				$val=$this->read_sorted_set($redis_key);
				break;
			case 5:
			//hash
			    $val=$this->read_hash($redis_key);	
			    break;
			default:
				break;
		}
 	  	}
 	  	//key not present in redis
 	  	else
 	  	{
 	  		$query="select * from system_configuration where name=?";
 	  		$param_array=array($redis_key);
 	  		$rs=Query_Wrapper::SELECT($query, $param_array);
 	  		//var_dump($rs);
 	  		foreach ($rs as $arr) {
 	  			//var_dump($arr);
 	  			if($arr['type']=='HASH')
 	  			{
 	  				$val[$arr['key_name']]=$arr['value'];
 	  			}
 	  			else 
 	  			{
 	  				$val[]=$arr['value'];
 	  			}
 	  		switch ($arr['type']) {
			case 'STRING':
			//string
				$this->write_string($redis_key,$arr['value']);
				break;
			case 'SET':
			//sets	
				$this->write_set($redis_key,$arr['value']);
				break;
			case 'LIST':
			//list	
				$this->write_list($redis_key,$arr['value']);
				break;
			case 'SORTED SET':
			//sorted sets
				$this->write_sorted_set($redis_key,$arr['value']);
				break;
			case 'HASH':
			//hash
			    $this->write_hash($redis_key,$arr['key_name'],$arr['value']);
			    break;
			default:
				break;
		 }
 	  		}
 	  	}
 	  	
 	  	return $val;
	}
	
	//value should be an array if type!=HASH 
	function write($type,$redis_key,$val,$key_name=NULL,$os=null)
	{
		if($type=='HASH')
		{
			$this->redis->hdel($redis_key,$key_name);
		}
		else
		 $this->redis->del($redis_key);
		 
		if($key_name==NULL)
		{
		foreach ($val as $value) {
			
			
			if($type=='STRING')
			{
				$query="select count(*) as count from system_configuration where type=? and name=?";
				$param_array=array($type,$redis_key);
				$rs=Query_Wrapper::SELECT($query,$param_array,'slave',true);
				if($rs['count']==0)
				{
					$query="insert into system_configuration (type,name,key_name,value) values(?,?,?,?)";
					$param_array=array($type,$redis_key,$key_name,$value);
					$tablename='system_configuration';
					Query_Wrapper::INSERT($query,$param_array,$tablename);
				}
				else 
				{
					$query="update system_configuration set value=? where type=? and name=?";
					$param_array=array($value,$type,$redis_key);
					$tablename='system_configuration';
					Query_Wrapper::UPDATE($query,$param_array,$tablename);
				}
			}
			else{
			
			$query="select count(*) as count from system_configuration where type=? and name=? and  value=?";
			$param_array=array($type,$redis_key,$value);
			$rs=Query_Wrapper::SELECT($query,$param_array,'slave',true);
			if($rs['count']==0)
			{
				$query="insert into system_configuration (type,name,key_name,value) values(?,?,?,?)";
				$param_array=array($type,$redis_key,$key_name,$value);
				$tablename='system_configuration';
				Query_Wrapper::INSERT($query,$param_array,$tablename);
			}
		}}
		$temp_arr=$this->read($redis_key);
		
		}
		else
		{ 
			
			$query="select count(*) as count from system_configuration where type=? and name=? and key_name=? ";
			$param_array=array($type,$redis_key,$key_name);
			$rs=Query_Wrapper::SELECT($query,$param_array,'slave',true);
			if($rs['count']==0)
			{
				$query="insert into system_configuration (type,name,key_name,value) values(?,?,?,?)";
				$param_array=array($type,$redis_key,$key_name,$val);
				$tablename='system_configuration';
				Query_Wrapper::INSERT($query,$param_array,$tablename);
			}
			else 
			{
				$query="update system_configuration set value=? where type=? and name=? and key_name=?";
				$param_array=array($val,$type,$redis_key,$key_name);
				$tablename='system_configuration';
				Query_Wrapper::UPDATE($query,$param_array,$tablename);
				
				
			}
		$this->write_hash($redis_key,$key_name,$val);	
		}
		
	}
	
	
	function delete($redis_key,$index=NULL)
	{
		//var_dump($redis_key);
		//var_dump($index);
		if($index==NULL)
		{
			//echo "a";
			$this->redis->delete($redis_key);
			$query="delete from system_configuration where name=?";
			$param_array=array($redis_key);
			$tablename='system_configuration';
			Query_Wrapper::DELETE($query,$param_array,$tablename);
			
		}
		else 
		{
		    //echo "b";
			$query="select type from system_configuration where name=? ";
			$param_array=array($redis_key);
			$rs=Query_Wrapper::SELECT($query,$param_array,'slave');
			//var_dump($rs[0]);
			switch ($rs[0]['type']) {
			case 'LIST':
				//var_dump($rs[0]['type']);
			    $this->redis->lrem($redis_key,$index,0);
			    $query="delete from system_configuration where name=? and value=?";
			    $param_array=array($redis_key,$index);
			    $tablename='system_configuration';
			    Query_Wrapper::DELETE($query,$param_array,$tablename);
				break;
			case 'HASH':
				//var_dump($rs[0]['type']);
				$this->redis->hdel($redis_key,$index);
				$query="delete from system_configuration where name=? and key_name=?";
				$param_array=array($redis_key,$index);
				$tablename='system_configuration';
				Query_Wrapper::DELETE($query,$param_array,$tablename);
				break;
			case 'SET':
				//var_dump($rs[0]['type']);
			    $this->redis->srem($redis_key,$index);
				$query="delete from system_configuration where name=? and value=?";
				$param_array=array($redis_key,$index);
				$tablename='system_configuration';
				Query_Wrapper::DELETE($query,$param_array,$tablename);
				break;
			case 'SORTED SET':
				//var_dump($rs[0]['type']);
			    $this->redis->zrem($redis_key,$index);
				$query="delete from system_configuration where name=? and value=?";
				$param_array=array($redis_key,$index);
				$tablename='system_configuration';
				Query_Wrapper::DELETE($query,$param_array,$tablename);
				break;
			default:
				break;
		}
		
		}
	}
	
	private function getType($redis_key)
	{
		return $this->redis->TYPE($redis_key);
	}
	
	private function read_string($redis_key) {
		$val=$this->redis->get($redis_key);
		return $val;
	}
	
	private function write_string($name,$value,$os=NULL) {
		$this->redis->set($name,$value);
	}
	
	private function read_set($redis_key) {
	$contents=$this->redis->sMembers($redis_key);
	return $contents;
	}
	
	private function write_set($name,$value) {
		$this->redis->sAdd($name,$value);
	}
	
	private function read_list($redis_key) {
		$contents=$this->redis->lrange($redis_key,0,-1);
	    return $contents;
	}
	
	private function write_list($name,$value) {
		$this->redis->rPush($name,$value);
	}
	
	private function read_sorted_set($redis_key) {
		$contents=$this->redis->zRange($redis_key,0,-1);
	    return $contents;
	}
	
	private function write_sorted_set($name,$value) {
	   $this->redis->zAdd($name,0,$value);
	}
	
	private function read_hash($redis_key) {
		$contents=$this->redis->hgetall($redis_key);
		return $contents;
	}
	
	private function write_hash($name,$key,$value) {
		$this->redis->hSet($name,$key,$value);
	}
    
	private function del($redis_key)
	{
		$this->redis->delete($redis_key);
	}

	public function hasActiveEvent($location){

		$ncr_cities = array(16743,47965,47964,47968,47967,47966);
		if(in_array ($location['city_id'], $ncr_cities)){
			$location['state_id']=2168;
		}

		if($location['state_id'] == 2168) {
			$sql = "select * from geo_zone WHERE state_id = ? and event_active = 1 limit 1";
			$param_array = array($location['state_id']);
		}
		else {
			$sql = "select * from geo_zone WHERE city_id = ? and event_active = 1 limit 1";
			$param_array = array($location['city_id']);
		}
		$res = Query_Wrapper::SELECT($sql, $param_array, 'slave', true);
		if(count($res) > 0)
			return true;

		return false;
	}

}

/*
try {
	$obj=new UserFlagsDBO();
	var_dump($obj->write('LIST','aa',array('newer')));
	
} catch (Exception $e) {
	echo "$e->getMessage()";
}
*/
?>