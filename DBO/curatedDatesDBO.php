<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../curatedDeals/RecommendationSystem/UtilsDate.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/eventsDBO.php";
require_once dirname ( __FILE__ ) . "/user_flagsDBO.php";

class CuratedDatesDBO 
{
	public static function currentLocation($user_id) 
	{ 
		 $sql = "select city_id,state_id,zones from user_deal_icon_shown where user_id =?" ;
		 $location = Query_Wrapper::SELECT($sql, array($user_id) , Query::$__slave, "true") ;
		 return $location ;
	}
	
	public static function last_deal_location ($user_id)
	{
		$sql = "Select * as cu from user_deal_icon_shown where user_id =?" ;
		$location = Query_Wrapper::SELECT($sql, array($user_id) , Query::$__slave, "true") ;
		return $location ;
	}
	
	public static function getCuratedStatus($name = "curated")
	{
		$sql = "SELECT status FROM config WHERE name= ? " ;
		$status_arr = Query_Wrapper::SELECT($sql, array($name), Query::$__slave, "true") ;
		return $status_arr['status'] ; 
	}

	public static function clickableShown($user_id, $match_id)
	{ 
		$sql = "Select count(*) as cu from user_match_deal_icon_shown where user_id =? and match_id = ?" ;
		$location = Query_Wrapper::SELECT($sql, array($user_id, $match_id) , Query::$__slave, "true") ;
		return $location['cu'] ; 
	}
	    
	public static function setUserPairClickableShown ($user_id, $match_id)
	{
		$table = "user_match_deal_icon_shown" ;
		$sql = "INSERT IGNORE INTO $table (user_id, match_id) values (?,?)" ;
		Query_Wrapper::INSERT($sql, array($user_id, $match_id), $table) ;
	}
	
	// to check if deal exists for a particular Location 
	public static function countDealsForLocation ($city_id, $state_id)
	{
		$ncr_city_array = array(16743,47965,47964,47968,47967,47966);
		if(in_array ($city_id, $ncr_city_array)){
			$state_id=2168;
		}
		$sql = "Select count(*) as cu from deals_location where city_id in (? , ? ) and state_id = ? and status ='active' " ;
	    $count = Query_Wrapper::SELECT($sql, array($city_id, 0 , $state_id), Query::$__slave, "true") ;
	    return $count['cu'] ; 
	}
	
	public static function setCurrentLocation ($user_id, $city_id, $state_id )
	{
		$table = "user_deal_icon_shown" ;
		$sql = "insert into user_deal_icon_shown ( user_id, city_id, state_id, zones) values (? ,?, ?, null) on duplicate key update city_id = ? , state_id = ?, zones=null " ;
		Query_Wrapper::INSERT($sql, array($user_id, $city_id, $state_id, $city_id, $state_id), $table) ;
	}

		public static function allActiveDealsForLocation($city_id, $state_id,$zones=NULL,$test=NULL)
	{
		$sql = "select distinct(d.hash_id) as datespot_id, friendly_name, d.name, dl.location, amount_type, amount_value, list_view_image as image, hashtag_json as hashtags,
				pricing,deal_type, d.location_count, 'Multiple Locations' as 'multiple_locations_text',dl.zone_id,dl.geo_location,rank,
				if(datediff(now(),d.activate_date)<=".UtilsDate::$lifeNewDatespot
				.",1,0) as isNew
				from datespots d
				JOIN datespot_location dl on dl.datespot_id = d.datespot_id
			 LEFT JOIN geo_zone gz on gz.zone_id=dl.zone_id
			 WHERE (dl.city_id =? OR dl.city_id is null OR dl.city_id=0) and dl.state_id = ?  and type='datespot'" ;
		$param_array =  array($city_id, $state_id);
		if(isset($zones) && $zones != null && $zones != 0){
			$zone_array= explode(',', $zones);
			$param_count  = Query_Wrapper::getParamCount($zone_array);
			$sql .= " and dl.zone_id in ($param_count)"; 		//zone is comma separated, coming from db, so no worry about injection
			$param_array = array_merge($param_array, $zone_array);
		}
		if(isset($test))
		{
			$sql .= "  and d.status in ('test', 'active') group by d.hash_id order by rank desc, pricing desc ";
		}
		else
		{
			$sql .= "  and d.status = 'active' group by d.hash_id order by rank desc, pricing desc " ;
		}
		$deals = Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
		return $deals ;
	}

	public static function getUserLocation($user_id)
	{
		$sql = "SELECT user_id, stay_city as city_id, stay_state as state_id from user_data where user_id = ?" ;
		$location = Query::SELECT($sql,  array($user_id), Query::$__slave, true) ;
		return $location ;
	}
	
	
	public static function getDatespotById($datespot_id)
	{
		$sql = "select d.hash_id as datespot_id,friendly_name,d.name,dl.location,images_json as images,
				menu_images_json as menu_images,offer,hashtag_json as hashtag,recommendation,dl.phone_number,dl.address,
				terms_and_conditions,list_view_image as communication_image, gz.zone_id, gz.name as zone_name,
				d.location_count, d.sms_text, d.date_pretty_text as pretty_date from datespots d  JOIN datespot_location dl ON dl.datespot_id=d.datespot_id
				LEFT JOIN geo_zone gz on gz.zone_id=dl.zone_id where d.hash_id=? GROUP by d.datespot_id ";
		$datespot = Query_Wrapper::SELECT($sql, array($datespot_id), Query::$__slave, "true" ) ;
		return $datespot;
 	}
	
	public static function getDatespotByCity($city_id, $status){
		$sql = "select * from datespots where city_id=? and status=? and type='datespot'";
		$datespot = Query_Wrapper::SELECT($sql, array($city_id, $status), Query::$__slave, "false" ) ;
	}
 

	function createNewDeal($user_id, $match_id, $datespot_id, $app_deal_id){
		$table = "user_deals" ;
		$sql = "insert into $table ( datespot_hash_id, user1, user2, created_tstamp, last_updated_tstamp, status, app_deal_id) values (? ,?, ?, NOW(), NOW(), 'waiting', ?) " ;
		Query_Wrapper::INSERT($sql, array($datespot_id, $user_id, $match_id, $app_deal_id), $table) ;
	}
	
	function updateDeal($app_deal_id, $status, $coupon_code, $datespot_id){
		$table = "user_deals" ;
		$datespot_coupon_code = self::getDatespotCouponCode($datespot_id);
		if($datespot_coupon_code != null)
			$coupon_code = $datespot_coupon_code;


		$sql = "update $table set status = ?, coupon_code = ?, last_updated_tstamp=now() where app_deal_id=?" ;
		Query_Wrapper::INSERT($sql, array($status, $coupon_code, $app_deal_id), $table ) ;
	}

	function getDatespotCouponCode($datespot_id){
		$sql = "select coupon_code,multiple_coupon from datespots where hash_id = ?" ;
		$datespot_coupon_arr = Query_Wrapper::SELECT($sql, array($datespot_id), Query::$__slave, "true" ) ;
		$datespot_coupon_code = $datespot_coupon_arr['coupon_code'];
		$multiple_coupon = $datespot_coupon_arr['multiple_coupon'];

		if($datespot_coupon_code != null && isset($datespot_coupon_code) && $datespot_coupon_code != "") {
			if($multiple_coupon == 1){
				$couponCodes = explode(",","$datespot_coupon_code");
				$couponCode = $couponCodes[0];

				if(sizeof($couponCodes) > 1)
					$datespot_coupon_code = str_replace($couponCode.",", "", $datespot_coupon_code);
				else {
					$datespot_coupon_code = str_replace($couponCode, "", $datespot_coupon_code);
					self::changeDatespotStatus($datespot_id, 'inactive');
				}

				$sqlUpdate = "update datespots set coupon_code = ? where hash_id = ?";
				Query_Wrapper::UPDATE($sqlUpdate,array($datespot_coupon_code, $datespot_id), "datespots");
				return $couponCode;
			}
			return $datespot_coupon_code;
		}

		return null;
	}
	
	function getCuratedDateByAppDealId($app_deal_id){
		$sql = "select datespot_hash_id as datespot_id, user1 as user_id, user2 as match_id,created_tstamp,status,
				app_deal_id as deal_id, coupon_code from user_deals where app_deal_id=? order by created_tstamp desc limit 1";

		$deal = Query_Wrapper::SELECT($sql, array($app_deal_id), Query::$__slave, "true" ) ;
		return $deal;
	}
	
	public static function addSampleDS( $friendly_name, $name, $location, $amount_value)
	{
		$table = "datespots" ;
	   $sql = "INSERT INTO $table (friendly_name, name, location, amount_value )  VALUES (?, ?, ?, ?)" ;
	  $insert_id =  Query_Wrapper::INSERT($sql, array($friendly_name, $name, $location, $amount_value), $table) ;
	  
	  $hash_id = $insert_id . "_". rand(pow(10, 5-1), pow(10, 5)-1);
	  $sql1= "UPDATE $table SET hash_id = ? where datespot_id = ?";
	  Query_Wrapper::UPDATE($sql1, array($hash_id, $insert_id), $table) ;
	}
	
	public static function addCompleteDS($data, $type="datespot")
	{
		$table = "datespots" ;
		if(isset($data['type']) && $data['type'] != null){
			$type = $data['type'];
		}
		$sql = "INSERT INTO $table (status, friendly_name, name, deal_type, amount_type, pricing,
		       images_json, list_view_image, menu_images_json, offer, hashtag_json, recommendation, terms_and_conditions,
		       location_count, sms_text, coupon_code,multiple_coupon, type, date_pretty_text, event_ticket_url, event_date, event_ticket_text,is_trulymadly, fb_page_url, tm_status,spark_display_name) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)" ;
		$insert_id = Query_Wrapper::INSERT($sql, array('test', $data['friendly_name'], $data['name'], $data['deal_type'], $data['amount_type'], $data['pricing'],
										  $data['images_json'], $data['list_view_image'],
										  $data['menu_images_json'], $data['offer'], $data['hashtag_json'],
										$data['recommendation'], $data['terms_and_conditions'], $data['location_count'],
										$data['sms_text'],$data['coupon_code'],$data['multiple_coupon'],$type,
										$data['date_pretty_text'],$data['event_ticket_url'],$data['event_date'],$data['event_ticket_text'],
										$data['is_trulymadly'],$data['fb_page_url'],$data['tm_status'],$data['spark_display_name']), $table) ;
		$hash_id = $insert_id . "_". rand(pow(10, 5-1), pow(10, 5)-1);
		$sql1= "UPDATE $table SET hash_id = ? where datespot_id = ?";
		Query_Wrapper::UPDATE($sql1, array($hash_id, $insert_id), $table) ;
		self::addDatespotLocations($data['locations_json'], $insert_id,$hash_id);
		if(isset($data['category_id']) && $data['category_id'] != null)
			self::addDatespotCategoryMapping($insert_id,$data['category_id']);
		return $insert_id;
	}

	public static function addDatespotLocations($data, $datespot_id, $datespot_hash_id){
		$jsonData = json_decode($data, true);
		$table = "datespot_location";
		$sql = "DELETE FROM $table where datespot_id = ?";
		Query_Wrapper::DELETE($sql, array($datespot_id), $table) ;
		$sql1 = "INSERT INTO $table (datespot_id,hash_id,location,zone_id,city_id,state_id,phone_number,address,update_date,geo_location) VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, now(),?) ";
		$location_array = array();
		foreach($jsonData['locations'] as $key => $value){
			$row = $value;
			$location_array[] = array($datespot_id, $datespot_hash_id, $row['location'],$row['zone_id'],$row['city_id'],$row['state_id'],$row['phone_number'],$row['address'],$row['geo_location']);
		}
		Query_Wrapper::BulkINSERT($sql1, $location_array,$table);
	}

	public static function addDatespotCategoryMapping($datespot_id, $category_id){
		$table = "event_category_mapping";
		$sql = "insert into $table (category_id, datespot_id) values (?,?)";
		Query_Wrapper::INSERT($sql,array($category_id,$datespot_id),$table);
	}

	public static function editDS($datespot_id, $field, $value)
	{
		$table = "datespots" ;
		$sql = "UPDATE $table SET $field = ? WHERE datespot_id = ?" ;
		Query_Wrapper::UPDATE($sql, array($value, $datespot_id), $table) ;
	}

	public static function editDatespotCategoryMapping($datespot_id, $category_id){
		$table = "event_category_mapping";
		if(eventsDBO::getEventCategory($datespot_id) != -1) {
			$sql = "UPDATE $table set category_id = ? where datespot_id = ?";
			Query_Wrapper::UPDATE($sql, array($category_id, $datespot_id), $table);}else{
			$sql = "INSERT INTO $table values (null, ?, ?)";
			Query_Wrapper::INSERT($sql, array($category_id, $datespot_id), $table);
		}
	}

	public static function getDatespotList( $status = null, $lower, $upper, $type="datespot")
	{
		$sql = "SELECT d.datespot_id, d.hash_id as datespot_hash_id, d.status, d.friendly_name, d.name, gc.name as city_name, gs.name as state_name, dl.location, d.rank, d.create_date, d.location_count, d.event_date, d.date_pretty_text
				FROM datespots d JOIN datespot_location dl on dl.datespot_id=d.datespot_id JOIN geo_state gs on gs.state_id = dl.state_id
				LEFT JOIN geo_city gc on gc.city_id = dl.city_id AND gc.state_id =dl.state_id where type = ? and d.is_deleted=0" ;
		if($status != null)
			$sql .= "and status = '$status'";

		$sql .= " group by d.datespot_id order by datespot_id  desc" ;
		$list =  Query_Wrapper::SELECT($sql, array($type) ) ;
		return $list ;
	}
	
	public static function getDatespotDetails($datespot_id)
	{
		$sql = "SELECT d.*, gc.name as city, gs.name as state, gz.zone_id, gz.name as zone_name FROM datespots d
				left join datespot_location dl on dl.datespot_id=d.datespot_id
				left join geo_zone gz on gz.zone_id=dl.zone_id
				left join geo_state gs on gs.state_id = dl.state_id
				left join geo_city gc on gc.city_id = dl.city_id and gc.state_id = dl.state_id
				where d.datespot_id = ? GROUP BY d.datespot_id" ;
		$datespot = Query_Wrapper::SELECT($sql, array($datespot_id), Query::$__slave, 'false');
		return $datespot ;
	}
	
	public static function getUserNumberFromdeals($user_id) 
	{
		$sql = "SELECT user_id, phone_number FROM user_deal_icon_shown WHERE user_id = ? " ;
		$phone = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true) ;
		return  $phone ;	
	}
	
	
	public static function getUserNumberFromTB($user_id) 
	{
		$sql = "SELECT user_id, user_number as phone_number FROM user_phone_number WHERE user_id = ?" ;
		$phone = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
		return $phone ;
	}
	
	public static function saveUserNumberForDeals($user_id, $phone_number) 
	{
		$table = "user_deal_icon_shown" ;
		$sql = "UPDATE $table SET phone_number = ? WHERE user_id = ? " ;
		Query_Wrapper::UPDATE($sql, array( $phone_number,$user_id), $table) ;
	}
	
	public static function getSmsSentStatus($deal_id)
	{ 
		$sql = "SELECT deal_id, datespot_hash_id,status, user1, user2, sms_user1, sms_user2,coupon_code FROM user_deals WHERE app_deal_id = ? " ;
		$status = Query_Wrapper::SELECT($sql, array($deal_id), Query::$__slave, true) ;
		return $status ;
	}
	
	public static function setSmsSentStatus($deal_id, $user_col, $status='sent')
	{
		$table = "user_deals" ;
		$sql = "UPDATE $table SET $user_col = ? where deal_id = ? " ;
		Query_Wrapper::UPDATE($sql, array($status,$deal_id), $table) ;
	}
	
	public static function changeDatespotStatus($datespot_id, $status)
	{
		global $redis;
		$res = self::getDatespotType($datespot_id);
		$table = "datespots" ;
		$activateDate = " ";
		$active_date_added = false;
		if($res['status'] == $status){
			return "Already in given status";
		}
		if((!isset($res['activate_date']) || $res['activate_date'] == null) && $status == 'active'){
			$activateDate = ", activate_date = now() ";
			$active_date_added = true;
		}
		$sql = "UPDATE $table SET status = ?, update_date=now() ".$activateDate." WHERE hash_id= ?" ;
		Query_Wrapper::UPDATE($sql, array($status, $datespot_id), $table) ;
		if($res['type'] == 'event'){
			$redis->del('event_'.$datespot_id);
			if($active_date_added){
				$res = self::getDatespotType($datespot_id);
				$obj=new UserFlagsDBO();
				$datespot_locations = self::getDatespotLocations($datespot_id);
				$datespot_location = $datespot_locations[0];
				$ncr_city_array = array(16743,47965,47964,47968,47967,47966);
				$city_id = $datespot_location['city_id'];
				if(in_array($city_id, $ncr_city_array) || $datespot_location['state_id'] == 2168){
					$city_id = 2168;
				}
				$obj->write("HASH","last_activated_event_date",$res['activate_date'],$city_id);
			}
		}
		self::updateDealsLocation($datespot_id, $status,$res['type']);
		self::updateZoneStatus($datespot_id, $status,$res['type']);
		self::updateDatespotCategory($datespot_id, $status,$res['type']);
	}

	public static function updateDatespotCategory($datespot_id, $status, $type="event"){
		$sql = "select * from event_category_mapping ecm where ecm.datespot_id = ?";
		$ecmList = Query_Wrapper::SELECT($sql, array($datespot_id), Query::$__slave);
		foreach($ecmList as $ecm){
			$event_count = self::activeEventCountByCategory($ecm['category_id']);
			$sqlU = "update event_category set event_count = ? where category_id = ?";
			Query_Wrapper::UPDATE($sqlU, array($event_count, $ecm['category_id']), "event_category") ;
		}
	}

	public static function changeDatespotRank($datespot_id, $rank)
	{
		$table = "datespots" ;
		$sql = "UPDATE $table SET rank = ? WHERE hash_id= ?" ;
		Query_Wrapper::UPDATE($sql, array($rank, $datespot_id), $table) ;
	}

	public static function deleteDatespotAction($datespot_id, $delteStatus)
	{
		$table = "datespots" ;
		$sql = "UPDATE $table SET is_deleted = ? WHERE hash_id= ?" ;
		Query_Wrapper::UPDATE($sql, array($delteStatus, $datespot_id), $table) ;
	}
	
	public static function updateDealsLocation($datespot_id, $status, $type="datespot"){
		if($status == 'active'){
			$sql = 'select * from deals_location dl inner join datespot_location d on d.city_id=dl.city_id and d.state_id=dl.state_id'.
					' where d.hash_id = ? ';
			$deals_location =  Query_Wrapper::SELECT($sql, array($datespot_id), Query::$__slave, true);
			if(count($deals_location) == 0){
				//no row found
				$table = "deals_location" ;
				$sql = "INSERT IGNORE INTO $table (state_id, city_id, tstamp, status ) select state_id, city_id, NOW(), 'active' from datespot_location where hash_id = ? limit 1" ;
				$insert_id =  Query_Wrapper::INSERT($sql, array($datespot_id), $table) ;
			}	
		}
		else if($status == 'inactive'){
			$sql = 'select city_id, state_id from datespot_location where hash_id = ? limit 1';
			$location = Query_Wrapper::SELECT($sql, array($datespot_id), Query::$__slave, true) ;
			
			$sql = 'select * from deals_location dl inner join datespot_location d on d.city_id=dl.city_id and d.state_id=dl.state_id inner join datespots dt on dt.datespot_id=d.datespot_id'.
					' where dt.status = "active" and d.city_id = ? and d.state_id = ?';
			$deals_location =  Query_Wrapper::SELECT($sql, array($location['city_id'], $location['state_id']), Query::$__slave, true);
			if(count($deals_location) == 0){
				//no row found
				$table = "deals_location" ;
				$sql = "DELETE FROM $table where city_id = ? and state_id = ?" ;
				$insert_id =  Query_Wrapper::INSERT($sql, array($location['city_id'], $location['state_id']), $table) ;
			}
		}
	}

	public static function updateZoneStatus($datespot_id, $status, $type="datespot"){
		if($status == 'active'){
			if($type == "event")
				$sql = "update geo_zone set event_active = 1 where zone_id in (select zone_id from datespot_location where hash_id = ?)";
			else
				$sql = "update geo_zone set deal_active = 1 where zone_id in (select zone_id from datespot_location where hash_id = ?)";
			Query_Wrapper::UPDATE($sql, array($datespot_id), "geo_zone");
		}

		if($status == 'inactive'){
			if($type=="event") {
				$sql = "UPDATE geo_zone SET event_active = 0 WHERE zone_id IN (SELECT t.zone_id FROM (SELECT zone_id FROM datespot_location WHERE hash_id = ? )t
					LEFT JOIN (SELECT dl.zone_id FROM datespot_location dl JOIN datespots d ON d.datespot_id = dl.datespot_id WHERE d.hash_id != ? AND d.status='active' and d.type='event')r
					ON t.zone_id = r.zone_id WHERE r.zone_id IS NULL)";
			}else {
				$sql = "UPDATE geo_zone SET deal_active = 0 WHERE zone_id IN (SELECT t.zone_id FROM (SELECT zone_id FROM datespot_location WHERE hash_id = ? )t
					LEFT JOIN (SELECT dl.zone_id FROM datespot_location dl JOIN datespots d ON d.datespot_id = dl.datespot_id WHERE d.hash_id != ? AND d.status='active' and d.type='datespot')r
					ON t.zone_id = r.zone_id WHERE r.zone_id IS NULL)";
			}
			Query_Wrapper::UPDATE($sql, array($datespot_id, $datespot_id), "geo_zone");
		}
	}

	public static function getZonesForCity($city_id, $type="deal"){
		if($type == "deal")
			$field = "deal_active";
		else
			$field = "event_active";
		$ncr_city_array = array(16743,47965,47964,47968,47967,47966);
		if(in_array($city_id, $ncr_city_array)){
			$city_id = "2168";
			$sql = "select zone_id,display_name as name, 'false' as selected from geo_zone where state_id = ? and $field=1";
		}
		else{
			$sql = "select zone_id,display_name as name, 'false' as selected from geo_zone where city_id = ?  and $field=1";
		}
		$zones = Query_Wrapper::SELECT($sql, array($city_id) , Query::$__slave) ;
		return $zones ; 
	}
	
	public static function getCityOrStateName($city_id)
	{
		$ncr_city_array = array(16743,47965,47964,47968,47967,47966);
		if(in_array($city_id, $ncr_city_array)){
			$city_id = "2168";
			$sql = "select name from geo_state where state_id = ?" ;
		}
		else{
			$sql = "select name from geo_city where city_id = ?" ;
		}
		$location  = Query_Wrapper::SELECT($sql, array($city_id), Query_Wrapper::$__slave, true) ;
		return $location['name'] ;
	}
	
    public static function updatePreferredZone($user_id, $zones)
    {
    	$table = "user_deal_icon_shown" ;
    	// Get the previous preferred zones and add the lateset zone
    	$sql = "select zones from user_deal_icon_shown where user_id =?";
    	$zonesJSON = Query_Wrapper::SELECT($sql, array($user_id) , Query::$__slave);
    	// Check NULL
    	if(empty($zonesJSON) || $zonesJSON[0]['zones']==NULL || $zonesJSON[0]['zones']=='null'){
    		$zonesArray = array();
    	} else{
    		$zonesArray = json_decode($zonesJSON[0]['zones'],true);
    	}
    	// Flag to determine if the preferences are through zones or nearby
    	
    	array_push($zonesArray, $zones);
    	if(count($zonesArray)>10){
    		array_shift($zonesArray);
    	}
    	$zonesJSON = json_encode($zonesArray);
    	$sql = "UPDATE user_deal_icon_shown SET zones = ? WHERE user_id = ?" ;
    	Query_Wrapper::UPDATE($sql, array($zonesJSON,$user_id), $table);
    }

	public static function getDatespotLocations($datespot_id,$zone_array=null)
	{
		$sql = "select d.hash_id as datespot_id, location, gz.zone_id, gz.name as zone_name, d.address, d.phone_number, d.city_id, d.state_id,d.geo_location
 				from datespot_location d LEFT JOIN geo_zone gz on gz.zone_id=d.zone_id
				where d.hash_id = ? " ;
		$param_array =  array($datespot_id);
		
		if($zone_array != null && is_array($zone_array) && count($zone_array) > 0)
		{
			$param_prepare_count = Query_Wrapper::getParamCount($zone_array);
			$sql.="and d.zone_id in ($param_prepare_count)" ;
			$param_array = array_merge($param_array,$zone_array);
		}
		
		$deals = Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
		return $deals ;
	}

	public static function getZonesData(){
		$sql = "select zone_id, city_id, country_id, state_id, display_name as name from geo_zone";
		return Query_Wrapper::SELECT($sql, array(), Query::$__slave);
	}

	public static function addNewZone($zoneName, $stateId, $cityId){
		$zoneId=null;
		$rowCount = 1;
		while($rowCount != 0){
			$zoneId = rand(pow(10, 4-1), pow(10, 4)-1);
			$sql = "select * from geo_zone where zone_id = ?";
			$row = Query_Wrapper::SELECT($sql, array($zoneId),Query_Wrapper::$__slave, true);
			$rowCount = count($row);
		}
		$sql = "INSERT INTO geo_zone(name,display_name,state_id,city_id, zone_id,country_id) VALUES (?,?,?,?,?,'113')";
		$insertId = Query_Wrapper::INSERT($sql, array($zoneName,$zoneName,$stateId,$cityId,$zoneId), "geo_zone");
		return $zoneId;
	}

	public static function getDatespotType($hash_id){
		$sql = "select type, status, activate_date from datespots where hash_id = ?";
		$res = Query_Wrapper::SELECT($sql, array($hash_id), Query::$__master, true);
		return $res;
	}
	
	/**
	 * Flag to check if a new deal is added after the user has clicked on Datelicious tab
	 */
	public static function getNewDatesFlag($user_id,$lastDealViewedTimeStamp){
		$location = curatedDatesDBO::getUserLocation($user_id);
		$ncr_city_array = array(16743,47965,47964,47968,47967,47966);
		if(in_array ($location['city_id'], $ncr_city_array)){
			$location['state_id']=2168;
		}
		$sql = "select count(distinct(d.hash_id)) as newDatesCount from datespots d 
				JOIN datespot_location dl on dl.datespot_id = d.datespot_id 
				where (dl.city_id =? OR dl.city_id is null OR dl.city_id=0) and dl.state_id = ?
				and d.status = 'active' and 
				d.type = 'datespot' and 
				d.activate_date > ?
				and datediff(date(now()),d.activate_date)<=".UtilsDate::$lifeNewDatespot;
		$result = Query_Wrapper::SELECT($sql, array($location['city_id'],$location['state_id'],$lastDealViewedTimeStamp), Query::$__slave, true);
// 		if($result['newSpots']>0){
// 			return true;
// 		} else {return false;}
		return $result;
	}
	/**
	 * Function to return all zones and coordinates for datespot with multiple locations
	 */
	public static function getDatespotLocationData($hash_id){
		$sql = "select dl.zone_id,dl.geo_location,dl.location from datespot_location dl where dl.hash_id = ?";
		$result = Query_Wrapper::SELECT($sql, array($hash_id), Query::$__slave, true);
		return $result;
	}

	public function activeEventCountByCategory($category_id){
		$sql = "select count(*) as event_count from event_category_mapping ecm join datespots d on d.datespot_id=ecm.datespot_id".
			" where d.status='active' and ecm.category_id = ?";
		$result = Query_Wrapper::SELECT($sql, array($category_id), Query::$__master, true);
		if(isset($result) && $result != null)
			return $result['event_count'];
		return 0;
	}
}

?>
