<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";

/* Photo share Database Object , 
 * any DBO action pertaining to Photo share goes here
 * Author :: Sumit 
 * 🖖 Live Long and Prosper.
 */

class photoShareDBO 
{
	public static function saveSharedPhoto($user_id, $match_id, $image, $thumbnail)
	{
		$table = "user_shared_photo" ;
		$sql = "INSERT INTO user_shared_photo (user_id, match_id, image, thumbnail) VALUES (?, ?, ?, ?)" ;
		$param_array = array($user_id, $match_id, $image, $thumbnail) ;
		Query_Wrapper::INSERT($sql, $param_array, $table) ;
	}
}

?>