<?php 

require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";

class TopFacebookPageDBO {
	
	public static function get_random_suggestions($zone, $categories) {
		// SATISH TODO: change this query to get fb pages alone as suggestions proper way, handle food, travel merge
		$param_count = Query_Wrapper::getParamCount($categories);
		$sql = "select page_id, tm_category from facebook_top_pages where zone = ? and tm_category in ($param_count) order by rand()";
		$rows = Query_Wrapper::SELECT($sql, array_merge(array_merge(array($zone), $categories)));
		$result = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$result[] = $row['page_id'];
			}
		}
		return $result;
	}
}

?>
