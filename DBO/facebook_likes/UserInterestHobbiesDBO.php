<?php 

require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../userFacebookDBO.php";

class UserInterestHobbiesDBO {
	
	public function __construct() {
		
	}


	public function get_user_interest_hobbies($users) {
		$sql = "select * from interest_hobbies where user_id in (". implode(',', $users). ")";
		$int_hob = Query::SELECT($sql, null, Query::$__slave, false);
		$result = array();
		foreach ($users as $user) {
			$result[$user] = array();
		}
		foreach ($int_hob as $val) {
			$result[$val['user_id']]['music_favorites'] = $val['music_favorites'];
			$result[$val['user_id']]['books_favorites'] = $val['books_favorites'];
			$result[$val['user_id']]['movies_favorites'] = $val['movies_favorites'];
			$result[$val['user_id']]['sports_favorites'] = $val['sports_favorites'];
			$result[$val['user_id']]['food_favorites'] = $val['food_favorites'];
			$result[$val['user_id']]['travel_favorites'] = $val['travel_favorites'];
			$result[$val['user_id']]['other_favorites'] = $val['other_favorites'];
			$result[$val['user_id']]['preferences'] = $val['preferences'];
		}
		return $result;
	}
	
	public function update_user_interest_hobbies($user_id, $array) {
		$query = "update  interest_hobbies set
			music_favorites = ?, books_favorites = ?, food_favorites = ?,
			movies_favorites = ?, other_favorites = ?, travel_favorites = ? where user_id = ?";
		$param_array = array(json_encode($array['music_favorites']), json_encode($array['books_favorites']),
				json_encode($array['food_favorites']), json_encode($array['movies_favorites']),
				json_encode($array['other_favorites']), json_encode($array['travel_favorites']), $user_id);
		$tablename = "interest_hobbies";
		Query_Wrapper::update($query, $param_array, $tablename);
		$this->update_steps_count($user_id, $array);
	}


	public function insert_user_interest_hobbies($user_id, $array) {
		$query = "insert into interest_hobbies (user_id, music_favorites, books_favorites, food_favorites,
			movies_favorites, other_favorites, travel_favorites) 
			values(?,?,?,?,?,?,?)";
		$param_array = array($user_id, json_encode($array['music_favorites']), json_encode($array['books_favorites'])
				, json_encode($array['food_favorites']), json_encode($array['movies_favorites']),
				json_encode($array['others_favorites']), json_encode($array['travel_favorites']));
		$tablename = "interest_hobbies";
		Query_Wrapper::INSERT($query, $param_array, $tablename);
		$this->update_steps_count($user_id, $array);
	}


	private function update_steps_count($user_id, $array) {
		$likes_count = 0;
		foreach (array_values(Utils::$page_categories_db_fields_map) as $cat) {
			$likes_count += count($array[$cat]);
		}
		
		$sql = "select steps_completed from user where user_id = ?";
		$row = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
		if ($row != null && isset($row['steps_completed']) && $likes_count > 0) {
			$steps = explode ( ",", $row['steps_completed']);
			if (!in_array('hobby', $steps)) {
				$steps[] = 'hobby';
				$steps_completed = implode(',', $steps);
				
				$query = "update user set steps_completed=? where user_id=?";
				$param_array = array($steps_completed, $user_id);
				$tablename = 'user';
				Query_Wrapper::UPDATE($query, $param_array, $tablename);
			}
		}
	}

}

?>
