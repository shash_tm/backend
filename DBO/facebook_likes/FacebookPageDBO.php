<?php 

require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../../facebook_likes/FacebookPageSearch.php";


class FacebookPageDBO {

	public static function get_facebook_pages($page_ids) {
		if (count($page_ids) == 0) return array();
		$param_count = Query_Wrapper::getParamCount($page_ids);
		$sql = "select * from facebook_pages where page_id in ($param_count)";
		$rows = Query_Wrapper::SELECT($sql, $page_ids, Query::$__slave);
		return $rows;
	}
	

	// SATISH TODO: this is only as dummy search	
	public static function search_facebook_pages($category, $search_text) {
		$sql = "select * from facebook_pages where tm_category = ? and name like '%$search_text%' limit 20";
		$rows = Query_Wrapper::SELECT($sql, array($category), Query::$__slave);
		return $rows;
	}

	
	public static function get_existing_pages_map($page_ids) {
		if (count($page_ids) == 0) return array();
		$param_count = Query_Wrapper::getParamCount($page_ids);
		$sql = "select page_id from facebook_pages where page_id in ($param_count)";
		$rows = Query_Wrapper::SELECT($sql, $page_ids, Query::$__slave);
		$result = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				$result[$row['page_id']] = true;
			}
		}
		return $result;
	}
	
	
	// when new user comes add his pages to db and redis
	public static function check_and_add_pages($pages) {
		$page_ids = array();
		foreach ($pages as $cat => $cat_data) {
			$page_ids = array_merge($page_ids, array_keys($cat_data));
		}
		$existing_pages = FacebookPageDBO::get_existing_pages_map($page_ids);
		// unset existing pages
		foreach ($pages as $cat => $cat_data) {
			foreach ($cat_data as $key => $value) {
				if (isset($existing_pages[$key]))
					unset($pages[$cat][$key]);
			}
		}
		$insert_sql = "insert ignore into facebook_pages (page_id, name, tm_category) values(?,?,?)";
		$arr = array();
		foreach ($pages as $cat => $cat_data) {
			foreach ($cat_data as $key => $value) {
				$arr[] = array($key, $value, strtolower($cat));
			}
		}
		if (count($arr) > 0) {
			Query::BulkINSERT($insert_sql, $arr);
		}
		FacebookPageSearch::check_and_add_pages_to_redis($pages);
	}	
}

?>

