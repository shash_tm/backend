<?php 

require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";

class ZoneMappingDBO {
	
	public static function get_zone($state) {
		$sql = "select zone_id from geo_state where state_id = ?";
		$row = Query_Wrapper::SELECT($sql, array($state), Query::$__slave, true);
		if ($row != null && isset($row['zone_id']))
			return $row['zone_id'];
	}
	
}

?>
