<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../Redis_data_cache.php";
require_once dirname ( __FILE__ ) . "/user_flagsDBO.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";

/**
 * DBO for all the user util calls
 * @author himanshu
 *
 */
class userUtilsDBO{
 
	public function setFirstTileShownForAuthenticUser($user){
		$sql = "UPDATE user set is_first_tile_shown = 1 where user_id = ?";
		Query_Wrapper::UPDATE($sql, array($user), 'user');
	}
	
	public function setOldUserMessageDiv($user){
		$sql = "UPDATE user set old_user_div_shown  = 1 where user_id = ?";
		Query_Wrapper::UPDATE($sql, array($user),'user');
	}
	
	public function getUsersMutualLikes ($user)
	{
		$sql = "SELECT timestamp, case when user1 = ? then user2 else user1 end as match_id from user_mutual_like where (user1 =? or user2= ?) and blocked_by is null order by timestamp desc ";
		$res = Query::SELECT($sql, array($user, $user, $user),Query::$__slave, true);
		return $res;
	}
        
        public function getUsersMutualLikesWithLimit ($user, $limit)
	{
            if($limit >= 0) {
		$sql = "SELECT timestamp, case when user1 = ? then user2 else user1 end as match_id from user_mutual_like where (user1 =? or user2= ?) and blocked_by is null order by timestamp desc limit $limit";
		$res = Query::SELECT($sql, array($user, $user, $user),Query::$__slave, true);
		return $res;
            }
            else {
                return $this->getUsersMutualLikes($user);
            }
	}

	public function getSameGenderUsers($gender,$userList,$limit)
	{
            $sql = "SELECT user_id from user where gender=? and user_id in $userList";
            if($limit >= 0) {
		$sql = "SELECT user_id from user where gender=? and user_id in $userList limit $limit";
            }
		$res = Query::SELECT($sql, array($gender),Query::$__slave, true);
		return $res;
	}

	public function setMaybeFirstTile($user){
		$sql = "UPDATE user set  is_first_maybe_shown = 1 where user_id = ?";
		Query_Wrapper::UPDATE($sql, array($user),'user');
	}
	
	public function getUserBasicInfo($user_array , $fetchRowsEnabled = true){
		global $redis;
		$uf=new UserFlagsDBO();
                $user_array1=array();
                $user_array2=array();
                $rs=array();
                $rs2=array();
		$user_array=array_unique($user_array);
		$data=$uf->read("chat_configurations");
		$data_type=$uf->read("chat_configurations_ab");
		$usersTest=$uf->read("chat_configure_users");
		$ab_range=$data_type['redis_cache'];
		
		foreach ($user_array as $user)
		{
                    if($data['redis_cache']==true && ((($user)%10<$ab_range) || in_array($user, $usersTest)))
                        $user_array1[]=$user;
                    else 
                        $user_array2[]=$user;
		}
		if(!empty($user_array1))
		{	
                    $i=0;
                    $user_array1=array_unique($user_array1);
                    foreach ($user_array1 as $user)
                    {
			$redis_cache=new RedisCache($user);
			//getting 'basic' from redis/mysql
                        $temp_arr1=$redis_cache->getval('basic');
                        if(!$temp_arr1 || $temp_arr1['validation_flag']==0)
                        {
                            $temp_arr1=$redis_cache->getarr('basic');
                            $redis_cache->setval('basic', $temp_arr1);
                        }
                        $temp_arr1['user_id']=$user;
                        unset($temp_arr1['validation_flag']);

			//getting 'lastlogin' from redis/mysql 
			$temp_arr2=$redis_cache->getval('lastlogin');
			if(!$temp_arr2 || $temp_arr2['validation_flag']==0)
			{
                            $temp_arr2=$redis_cache->getarr('lastlogin');
                            $redis_cache->setval('lastlogin', $temp_arr2);
			}
			$temp_arr2['user_id']=$user;
			unset($temp_arr2['validation_flag']);
			$rs[$i]=array_merge($temp_arr1,$temp_arr2);
			$i++;
                    }
	
                    if($fetchRowsEnabled==true && $i==1) 
                    {
                            $rs=$rs[0];
                    }
		}
		if(!empty($user_array2))
		{
                    $str = implode(',',$user_array2);
                    $usersql = "SELECT user.user_id,gender,status,fname,ul.last_login,user.email_id,ud.stay_country,DATEDIFF(now(),user.registered_at) as daysSpentOnTheApp from user left join user_lastlogin ul on user.user_id=ul.user_id join user_data ud on user.user_id = ud.user_id where user.user_id In ($str)";
                    $rs2 = Query::SELECT($usersql, null , Query::$__slave, $fetchRowsEnabled);
		}
		
		$result=array_merge($rs,$rs2);
		return $result;
	}
	
	/**
	 * return all the system level flags defined for a user
	 * @param unknown_type $user_array
	 * @param unknown_type $fetchRowsEnabled
	 */
	public function getUserFlags ($user_array = array() ,  $fetchRowsEnabled = true )
	{
		$str = implode(',',$user_array);
		$usersql = "SELECT * from user_flags where user_id In ($str)";
		$rs = Query::SELECT($usersql, null , Query::$__slave, $fetchRowsEnabled);
		return $rs;
	}

	public function getUserPiInfo($user_array , $fetchRowsEnabled = true){
		$str = implode(',',$user_array);
		$usersql = "SELECT * from user_pi_activity where user_id In ($str)";
		$rs = Query::SELECT($usersql, null , Query::$__slave, $fetchRowsEnabled);
		return $rs;
	}

	public function isAnyApprovedPic($user_id){
		$sql = "SELECT count(*)  as count from user_photo where status = 'active' AND user_id = ?";
		$approvedPics = Query::SELECT($sql, array($user_id));
		$isAnyApproved = ($approvedPics['count'] > 0)?1:0;
		return $isAnyApproved;
	}


	public function getActiveAndModeratedImages($user_array , $fetchRowsEnabled = true,$webp){
		$str = implode(',',$user_array); 
		//$sql_image = "SELECT name, thumbnail, is_profile, is_moderated, user_id from user_photo where user_id In ($str) AND status ='active' and admin_approved = 'yes' ORDER BY FIELD(user_id, $str)";
		$sql_image = "SELECT name, thumbnail, system_profile ,is_profile, is_moderated, user_id,webp_flag from user_photo where user_id In ($str) AND status ='active' and admin_approved = 'yes' ORDER BY FIELD(user_id, $str)";
		$images = Query::SELECT($sql_image,  null , Query::$__slave, $fetchRowsEnabled);
		foreach($images as $key => $img)
		{
			if($img['webp_flag'] == 'yes' && $webp == true)
			{
				$images[$key]['name'] = Utils::getWebpName($img['name']) ;
				$images[$key]['thumbnail'] = Utils::getWebpName($img['thumbnail']) ;
			}

		}
		return $images;
	}
	
	public function CheckIfFemaleWithNoPics($user_id)
	{
		//Returns true , if the female user doesn't have any profile pic , that can now be derived from user_Search.
		$sql="select haspics from user_search where user_id=? and gender='F'";
		$result = Query::SELECT($sql,  array($user_id) , Query::$__slave,true);
		if(count($result)>0)
		{
			if($result['haspics']==0) {
				
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
		
		
		
		
	}
	
	public function getOriginalProfilePic($user_id_array,$myprofile)
	{
		$str = implode(',',$user_id_array);
		//This functions gets called when there are no active system profile for the user, then an active profile will be sent to the user
		if($myprofile==false) {
			$profile_pic_query="select name,thumbnail,webp_flag,user_id from user_photo where user_id in ($str) and status='active' and admin_approved='yes' and is_profile='yes'";
	    	$profile_pic=Query::SELECT($profile_pic_query, null,Query::$__slave, false);
	    //var_dump($profile_pic);
		}
		else {
			$profile_pic_query="select name,thumbnail,webp_flag,user_id from user_photo where user_id in ($str) and status='active' and is_profile='yes'";
			$profile_pic=Query::SELECT($profile_pic_query, null,Query::$__slave, false);
		}


	    return $profile_pic;
	    
	}
	
	public function getActiveImages($user_array , $fetchRowsEnabled = true, $webp = false){
		$str = implode(',',$user_array);
		$sql_image = "SELECT name, thumbnail, is_profile, is_moderated, user_id, mapped_to, photo_id,webp_flag from user_photo where user_id In ($str) AND status in ('active','temp_active')  order by photo_id desc";
		$images = Query::SELECT($sql_image,  null , Query::$__slave, $fetchRowsEnabled);
		$temp_images = array() ; 
		$return_array = array() ;
		foreach ($images as $img )
		{
			if( $img['webp_flag'] == 'yes' && $webp == true)
			{
				$img['name'] = Utils::getWebpName($img['name']);
				$img['thumbnail'] = Utils::getWebpName($img['thumbnail']);

			}
			if ( isset($img ['mapped_to']) && $img ['mapped_to'] != null)
     		{
     			$temp_images [] = $img ['mapped_to'] ;
     		}
     		if ( !in_array($img ['photo_id'], $temp_images) )
     			$return_array [] = $img ;
		}
//		print_r(json_encode($value))
		return $return_array;
	}

	public function getProfilePic($user_array, $fetchRowsEnabled = true){
		$str = implode(',',$user_array);
		$sql_image = "SELECT name, thumbnail, user_id from user_photo where user_id In ($str) AND status ='active' and is_profile = 'yes' ";
		$images = Query::SELECT($sql_image,  null , Query::$__slave, $fetchRowsEnabled);
		return $images;
	}

	/**
	 * returns the likes of a user which are not present in mutual like
	 * @param unknown_type $userId
	 */
	public function getMyLikesQueue($userId, $limit){

		$sql = "select * from user_like 
				where user1 = ? 
				and user2 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				order by timestamp desc limit $limit";
		//echo $sql;
		/*$sql = "select * from user_like where user1 = ? 
			and user2 not in ((select user1 from user_mutual_like where user2 = ?)) 
			and user2 not in (select user2 from user_mutual_like where user1 = ?) 
			order by timestamp desc";
		*/$result = Query::SELECT($sql, array($userId, $userId,$userId));
		//var_dump($result); die; 
		return $result;
	}
	
	/**
	 * returns the likes of a user which got wasted because the other user hid him/her
	 * @param unknown_type $userId
	 */
	public function getMyWastedLikesQueue($userId, $limit){

		$sql = "select user2 from user_like 
				where user1 = ? 
				and user2 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user2 in( select user1 from user_hide where user2 = ?) order by timestamp desc ";
                if($limit >= 0) {
                    $sql = "select user2 from user_like 
				where user1 = ? 
				and user2 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user2 in( select user1 from user_hide where user2 = ?) order by timestamp desc limit $limit";
                }
		$result = Query::SELECT($sql, array($userId, $userId,$userId,$userId));
		return $result;
	}
	
	/**
	 * returns the likes of a user which got wasted because the other user hid him/her
	 * @param unknown_type $userId
	 */
	public function getMyLikesNotActedUponQueue($userId, $limit){

		$sql = "select user2 from user_like 
				where user1 = ? 
				and user2 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user2 not in( select user1 from user_hide where user2 = ?) order by timestamp desc ";
                if($limit >= 0) {
                    $sql = "select user2 from user_like 
				where user1 = ? 
				and user2 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user2 not in( select user1 from user_hide where user2 = ?) order by timestamp desc limit $limit";
                }
		$result = Query::SELECT($sql, array($userId, $userId,$userId,$userId));
		return $result;
	}
	
	/**
	 * returns the likes of a user which got wasted because the other user hid him/her
	 * @param unknown_type $userId
	 */
	public function getOthersWastedLikesOnMeQueue($userId, $limit){

		$sql = "select user1 from user_like 
				where user2 = ? 
				and user1 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user1 in( select user2 from user_hide where user1 = ?) order by timestamp desc ";
                if($limit >= 0) {
                    $sql = "select user1 from user_like 
				where user2 = ? 
				and user1 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user1 in( select user2 from user_hide where user1 = ?) order by timestamp desc limit $limit";
                }
		$result = Query::SELECT($sql, array($userId, $userId,$userId,$userId));
		return $result;
	}
	
	/**
	 * returns the likes of a user which got wasted because the other user hid him/her
	 * @param unknown_type $userId
	 */
	public function getOthersLikesNotActedUponByMeQueue($userId, $limit){

		$sql = "select user1 from user_like 
				where user2 = ? 
				and user1 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user1 not in( select user2 from user_hide where user1 = ?) order by timestamp desc ";
                if($limit >= 0) {
                    $sql = "select user1 from user_like 
				where user2 = ? 
				and user1 not in 
					(select user1 as user from user_mutual_like where user2 = ? 
					union
					 select user2 as user from user_mutual_like where user1 = ?) 
				and user1 not in( select user2 from user_hide where user1 = ?) order by timestamp desc limit $limit";
                }
		$result = Query::SELECT($sql, array($userId, $userId,$userId,$userId));
		return $result;
	}
	
	public function getMayBeQueue($userId){
		$sql = "select * from user_maybe where user1 = ? and is_active = 1 order by timestamp"; 
		$result = Query::SELECT($sql, array($userId));
		return $result;
	}
	
	public function getMayBeQueueCount($userId){
		$sql = "select count(umb.user2) as count from user_maybe umb JOIN user u on umb.user2 = u.user_id where user1 = ? and is_active = 1 and u.status = 'authentic' "; 
		$result = Query::SELECT($sql, array($userId),Query::$__slave, true);
		return $result;
	}
	
	
	public function getMutualConnections($user_id, $match_array){
		$data = array();
		$lesser_array = array();
		$greater_array = array();
		foreach ($match_array as $val){
			if($val>$user_id) $greater_array[] = $val;
			else $lesser_array[] = $val;
		}

		$lesser_array_str = implode(',', $lesser_array);
		$greater_array_str = implode(',', $greater_array);
		$lesser_array_query = "(user1=? and user2 IN($lesser_array_str))";
		$greater_array_query = "(user1 IN($greater_array_str) and user2 =? )";
		$params = array($user_id);
		$sql_mConns = "SELECT user1, user2, mutual_count, mutual_connections from user_facebook_mutual_connections where ";
		if(strlen($lesser_array_str)>0 && strlen($greater_array_str)>0){
			$sql_mConns .= $lesser_array_query . " OR ". $greater_array_query;
			$params = array($user_id, $user_id);
		}elseif (strlen($lesser_array_str)>0 ){
			$sql_mConns .= $lesser_array_query ;
		}elseif (strlen($greater_array_str)>0 ){
			$sql_mConns .= $greater_array_query ;
		}
		//echo $sql_mConns;
		$mutual_connections = Query::SELECT($sql_mConns, $params , Query::$__slave, false);
		foreach ($mutual_connections as $val){
			$index = ($val['user1'] == $user_id) ? $val['user2']: $val['user1'];
			$data[$index]["mutual_connections_count"] = ((isset($val['mutual_count']))?$val['mutual_count']:null);
			$data[$index]["mutual_connections"] = (isset($val['mutual_connections']))? json_decode($val['mutual_connections'], true):null;
		}


		return $data;
	}



	public function getActiveImagesOld($user_array, $user_gender = null){
		//return null;
		global $imageurl, $dummy_male_image, $dummy_female_image ;

		$this->uuDBO = new userUtilsDBO();
		//$isAnyApproved = $this->uuDBO->isAnyApprovedPic($user_id);
		$images = $this->uuDBO->getActiveImages($user_array, false);

		$arr = array();
		foreach ($images as $val){
			$arr[$val['user_id']][] = $val;
		}

		$photos = array();
		$idsWithPhoto = array_keys($arr);
		$remainingIds = array_diff($user_array, $idsWithPhoto);

		foreach ($remainingIds as $val){
			$photos[$val]['profile_pic'] = ($user_gender == 'F')? $dummy_male_image: $dummy_female_image;
		}
		//var_dump($photos); die;

		foreach ($arr as $key=>$val){
			$photoCount = count($arr[$key]);
			for($i=0;$i<$photoCount;$i++){
				if($val[$i]['is_profile']=='yes'){
					$photos[$key]['profile_pic'] = $imageurl.$val[$i]['name'];
				}
				else{
					$photos[$key]['other_pics'][] = $imageurl.$val[$i]['name'];
					$photos[$key]['thumbnail_pics'][] = $imageurl.$val[$i]['thumbnail'];
				}
			}

			if(!isset($photos[$key]['profile_pic'])){
				$photos[$key]['profile_pic'] = ($user_gender == 'F')? $dummy_male_image: $dummy_female_image;

			}
		}
		//var_dump($photos);
		return $photos;
	}


	/**
	 * get hashtags of users
	 * @param unknown_type $user_array
	 */
	public function getHashTags($user_array = array()){
		$result = array();
		if(!empty($user_array))
		{
			$ids=implode(",",$user_array);
			$sql = "SELECT user_id, preferences FROM interest_hobbies where user_id in ($ids)";
			$result = Query::SELECT($sql, null, Query::$__slave,false);
		} 
		return $result;
	}
	
	
	public function gettiledate($user_array){
		if(count($user_array)==0)return;
		$ids=implode(",",$user_array);

		$sql="select ud.user_id,TIMESTAMPDIFF(year,ud.DateOfBirth,curdate()) as age, gs.name as state, gci.display_name as stay_city_display,gc.country_id,
		u.status, u.deletion_status, u.fname,u.gender, up.thumbnail,up.name
			from user_data ud join user u on ud.user_id=u.user_id 
			left join user_photo up on ud.user_id=up.user_id and is_profile='yes' and up.status='active'
			 LEFT JOIN geo_city gci on ud.stay_city = gci.city_id AND gci.state_id = ud.stay_state  
			LEFT JOIN geo_state gs on ud.stay_state = gs.state_id
			LEFT JOIN geo_country gc on ud.stay_country =gc.country_id
				where ud.user_id in ($ids)";
		//var_dump($sql);
		$res_data = Query::SELECT($sql, null, Query::$__slave,false);
		//var_dump($res_data); die;
		return $res_data;
	}
	
	/**
	 * function for user_data user_photo and user_trust_score 
	 */
	public function getUserDataForProfileComplete($user_id, $fetchRowsEnabled = true) {
		$sql = "select ud.user_id, FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age, ud.institute_details, ud.stay_country,
				ud.company_name, ud.income_start, ud.income_end, hb.music_favorites, hb.movies_favorites, 
				hb.books_favorites, hb.travel_favorites, hb.other_favorites, sum(if(is_profile='yes',1,0)) as is_profile, 
				sum(if(admin_approved='yes',1,0)) as total_approved_photo, ut.trust_score 
				from user_data ud left join interest_hobbies hb on ud.user_id=hb.user_id 
				left join user_photo on ud.user_id=user_photo.user_id left join user_trust_score ut on 
				ud.user_id=ut.user_id where user_photo.status='active' and ud.user_id=$user_id";
		
		$res_data = Query::SELECT($sql, null, Query::$__slave, $fetchRowsEnabled);
		return $res_data;
	}
	
	/**
	 * returns data from `user_data` table
	 */
	public function getUserData($user_array, $fetchRowsEnabled = true){
		//$count = count($user_array);
		global $redis;
		$uf=new UserFlagsDBO();
		
		//echo $count;
		$user_array1=array();
		$user_array2=array();
		$res_data=array();
		$res_data2=array();
		$user_array=array_unique($user_array);
		//for staged release
		//$data=$redis->hGetAll("chat_configurations");
		$data=$uf->read("chat_configurations");
		//$data_type=$redis->hGetAll("chat_configurations_ab");
		$data_type=$uf->read("chat_configurations_ab");
		//$usersTest = $redis->sMembers("chat_configure_users");
		$usersTest=$uf->read("chat_configure_users");
		foreach ($user_array as $user)
		{
			if($data['redis_cache']==true && ((($user)%10<$ab_range) || in_array($user, $usersTest)))
			    $user_array1[]=$user;
			else 
				$user_array2[]=$user;
			
		}
		
		if(!empty($user_array1)){
		$user_array1=array_unique($user_array1);
		$res_data=array();
		$i=0;
		foreach ($user_array1 as $user)
		{
			$redis_cache=new RedisCache($user);
			$temp_arr=$redis_cache->getval('data');
			if(!$temp_arr || $temp_arr['validation_flag']==0)
			{
				$temp_arr=$redis_cache->getarr('data');
				$redis_cache->setval('data', $temp_arr);
			}
			$temp_arr['user_id']=$user;
			unset($temp_arr['validation_flag']);
			$res_data[$i]=$temp_arr;
			$i++;
		}
		if($fetchRowsEnabled==true && $i==1)
		{
			$res_data=$res_data[0];
		}
		}
		
		if(!empty($user_array2)){
		
		$ids = implode(',', $user_array2);
		//var_dump($ids);
		$sql_data = "SELECT ud.user_id, tc.is_verified_celeb, ud.marital_status,  ud.stay_country, ud.stay_state, ud.haveChildren, ud.height, ud.stay_city,
		ud.income_start, ud.income_end, ud.designation, ud.work_status, ud.company_name, ud.institute_details, ud.highest_degree,ud.industry, 
		i.name as industry_name,
		hd.degree_name as degree, hd.rank as education_group, FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age,
		 gci.display_name as stay_city_display,gs.name as state, gc.name as country, sc.code as state_code
		FROM user_data ud
		LEFT JOIN geo_state gs on ud.stay_state = gs.state_id
   		LEFT JOIN geo_city gci on ud.stay_city = gci.city_id AND gci.state_id = ud.stay_state
   		LEFT JOIN state_codes sc on sc.state_id = gci.state_id
   		LEFT JOIN geo_country gc on ud.stay_country = gc.country_id
   		LEFT JOIN industries_new i on ud.industry = i.id 
   		LEFT JOIN highest_degree hd on ud.highest_degree = hd.id 
   		LEFT JOIN tm_celeb tc on ud.user_id=tc.user_id
		where ud.user_id IN( $ids ) ";
		//	echo $sql_data;
		/*for ($i = $count; $i>0; $i++ )
		$sql_data .= "?,";

		$sql_data = rtrim($sql_data, ",");
		$sql_data  .= ")";*/
		//echo $sql_data; die;

		//var_dump($user_array);
		$res_data2 = Query::SELECT($sql_data, null, Query::$__slave, $fetchRowsEnabled);
		}
		$result=array_merge($res_data,$res_data2);
		return $result;
	}
	
	/**
	 * suspend a user and add the same to log as well with previous status
	 * pass admin id in case action is taken from admin side
	 * @param $user_id
	 * @param $reason
	 * @param $previous_status
	 */
public function suspend($user_id, $reason, $previous_status, $admin_id = null, $statusToSet = "suspended"){
		$sql = "UPDATE user SET status = ? , last_changed_status =? where user_id=?";
		Query_Wrapper::UPDATE($sql, array($statusToSet, $previous_status, $user_id),'user',"master",$user_id);

		$sql2 = "INSERT INTO user_suspension_log(user_id,reason, previous_status, current_status, timestamp, admin_id ) values(?,?,?,?,now(),?)";
		Query_Wrapper::INSERT($sql2, array($user_id,$reason, $previous_status, $statusToSet, $admin_id),'user_suspension_log',"master",false,$user_id);
		
	
	}
	
/*
 * block the LATEST device id of a particular user_id
 * @param $user_id
 * @param $reason
 */	
	
public function block_device($user_id, $reason)
{
	$device_id=NULL;
	$sql1 = "Select device_id from user_gcm_current where user_id=? order by tstamp desc limit 1";
	$rs=Query::SELECT($sql1, array($user_id));
	
	
	if(isset($rs))
	{
		$device_id=$rs[0]['device_id'];
	}
	
	if(!isset($device_id)) {
		$sql2 = "Select device_id from user where user_id=? ";
		$rs=Query::SELECT($sql2, array($user_id));
		
		if(isset($rs))
		{
			$device_id=$rs[0]['device_id'];
		}
	}
	
	
	if(isset($device_id))
	{
		$sql2 = "INSERT INTO block_fids(user_id,reason, tstamp, device_id ) values(?,?,now(),?)";
		Query_Wrapper::INSERT($sql2, array($user_id,$reason, $device_id),'block_fids',"master",false,$user_id);
	}
}

	public function blockAllUsersFromDevice($user_id)
	{
		$sql_device = "Select device_id from user where user_id = ?";
		$result = Query_Wrapper::SELECT($sql_device, array($user_id),Query::$__slave,true);
		$device_id = $result['device_id'];

		$sql_users = "Select user_id, status, last_changed_status from user WHERE  device_id = ? and status ='authentic'";
		$users = Query_Wrapper::SELECT($sql_users,array($device_id));
		foreach($users as $user)
		{
			$this->suspend($user['user_id'],'user blocked by device_id blocking', $user['status'],$_SESSION['admin_id'],'blocked');
		}
	}

	/** 
	 * mark a user for deletion as well as add the same to deletion_log */
	public function markForDeletion ($user_id, $reason, $sub_reason, $rating )
	{
		$sql = "UPDATE user set deletion_status = 'mark_deleted' WHERE user_id = ? ";
		Query_Wrapper::UPDATE($sql, array($user_id),'user',"master",$user_id);
		
		$sql_log = "INSERT IGNORE INTO user_deletion_log (user_id, reason, sub_reason, timestamp, rating) values ( ?, ?, ?, now(), ?)" ;
		Query_Wrapper::INSERT($sql_log, array($user_id, $reason, $sub_reason, $rating ),'user_deletion_log',"master",false,$user_id); 
	}

	public function getCurrentCredits($user_array, $fetchRowsEnabled= false){
		$ids = implode(',', $user_array);
		$sql = "SELECT * FROM user_current_credits where user_id in ( $ids )";
		$res_data = Query::SELECT($sql, null, Query::$__slave, $fetchRowsEnabled);
		return  $res_data;
	}

	public function getUserPreferenceData($user_array, $fetchRowsEnabled = true){
		$ids = implode(',', $user_array);
		$sql = "SELECT * FROM user_preference_data WHERE user_id IN ($ids)";
		$res = Query::SELECT($sql, null, Query::$__slave, $fetchRowsEnabled);
		return $res;
	}
	
	public function getEducationAmplificationDegreesForFemale($degree_id) {
		$sql = "SELECT amplification_degrees FROM education_rules_amplification_female_to_male WHERE degree_id = $degree_id";
		$res = Query::SELECT($sql, null, Query::$__slave, true);
		$amp_degrees = array();
		if ($res != null && isset($res['amplification_degrees'])) {
			$amp_degrees = preg_split('/#/', $res['amplification_degrees'], NULL, PREG_SPLIT_NO_EMPTY);
		}
		return $amp_degrees;
	}
	
	public function getEducationAmplificationDegreesForMale($degree_id) {
		$sql = "SELECT degree_id FROM education_rules_amplification_female_to_male WHERE amplification_degrees like '%#$degree_id#%'";
		$rows = Query::SELECT($sql, null, Query::$__slave);
		$amp_degrees = array();
		if (count($rows) > 0) {
			foreach ($rows as $key => $value) {
				$amp_degrees[] = $rows[$key]['degree_id'];
			}
		}
		return $amp_degrees;
	}
	
	public function getEducationSuppressionDegreesForFemale($degree_id) {
		$sql = "SELECT suppression_degrees FROM education_rules_suppression_female_to_male WHERE degree_id = $degree_id";
		$res = Query::SELECT($sql, null, Query::$__slave, true);
		$sup_degrees = array();
		if ($res != null && isset($res['suppression_degrees'])) {
			$sup_degrees = preg_split('/#/', $res['suppression_degrees'], NULL, PREG_SPLIT_NO_EMPTY);
		}
		return $sup_degrees;
	}
	
	public function getEducationSuppressionDegreesForMale($degree_id) {
		$sql = "SELECT degree_id FROM education_rules_suppression_female_to_male WHERE suppression_degrees like '%#$degree_id#%'";
		$rows = Query::SELECT($sql, null, Query::$__slave);
		$sup_degrees = array();
		if (count($rows) > 0) {
			foreach ($rows as $key => $value) {
				$sup_degrees[] = $rows[$key]['degree_id'];
			}
		}
		return $sup_degrees;
	}

	public function getPsychoScores ($user_array, $fetchRowsEnabled = true){
		$ids = implode(',', $user_array);
		$sql = "SELECT * FROM user_psycho_answer  WHERE user_id IN ($ids)";
		$res = Query::SELECT($sql, null, Query::$__slave, $fetchRowsEnabled);
		return $res;
	}
	public function getTrustBuilderData($user_array , $fetchRowsEnabled = true){
		$sql_trust = "SELECT * from  user_trust_score where user_id IN (" . implode(',', $user_array) . ")";
		$trustMeter = Query::SELECT($sql_trust, null, Query::$__slave, $fetchRowsEnabled );
		return $trustMeter;
	}

	public function getInterestsAndHobbies($user_array, $fetchRowsEnabled = true){
		$sql = "select * from interest_hobbies where user_id IN (". implode(',', $user_array). ")";
		$intHob = Query::SELECT($sql, null, Query::$__slave, $fetchRowsEnabled);
		$arr = null;
		foreach ($intHob as $val){
			$arr[$val['user_id']]['music_favorites'] =$val['music_favorites'];
			$arr[$val['user_id']]['books_favorites']= $val['books_favorites'];
			$arr[$val['user_id']]['food_favorites'] =$val['food_favorites'];
			$arr[$val['user_id']]['movies_favorites'] = $val['movies_favorites'];
			$arr[$val['user_id']]['sports_favorites'] = $val['sports_favorites'];
			$arr[$val['user_id']]['travel_favorites'] = $val['travel_favorites'];
			$arr[$val['user_id']]['other_favorites'] = $val['other_favorites'];
			$arr[$val['user_id']]['preferences'] = $val['preferences'];
		}
		//var_dump($arr); die;
		return $arr;
	}

	public function getPreferenceInterests($user_array, $fetchRowsEnabled = true){
		$ids =  implode(',', $user_array);
		$sql = "SELECT user_id, preferences FROM interest_hobbies where user_id in ($ids)";
		$result = Query::SELECT($sql, null, Query::$__slave, $fetchRowsEnabled);
		return $result;
	}

	public function getPsychoResult($user_array, $fetchRowsEnabled = true){
		$ids =  implode(',', $user_array);
		$sql = "SELECT user_id,sum_value_A,sum_value_B,sum_value_C,sum_value_D, sum_value_E,sum_value_F, interpersonal_score FROM user_psycho_answer where user_id in ($ids)";
		$result = Query::SELECT($sql, null, Query::$__slave, $fetchRowsEnabled);
		return $result;
	}
	
	
/**
	 * to set all the values for the user who is endorsing a user
	 * @param array $data  - contains user_id, fid, gender, fname, pic 
	 */
	public function setEndorsementsDetailsFromFb($data = array() , $ifAffectedRows = false){
		if(!empty($data)){
			$sql = "INSERT IGNORE  INTO `user_endorsements` (user_id, fid, gender, fname, pic, is_endorsed, tstamp) values (?,?,?,?,?,?,now())";
			$insertId = Query_Wrapper::INSERT($sql, array($data['user_id'], $data['fid'], $data['gender'], $data['fname'], $data['pic'], $data['is_endorsed']),'user_endorsements', Query::$__master, $ifAffectedRows)	;
			return $insertId;
		}
	}
	
	
	/**
	 * update user trust score for endorsement only when count <3 and increment the count of endorsement in any case
	 * @param unknown_type $user_id
	 */
	public function updateTrustScoreForEndorsements($user_id, $unitTrustScore, $maxEndorsementsForScoreUpdation, $netScore){
		if(isset($user_id)){
			$sql = "INSERT INTO `user_trust_score`  (`user_id`, `endorsement_count`, `trust_score`,`endorsement_score`) values (?,?,?,?)
					on duplicate key update `endorsement_count` = `endorsement_count` + 1, `trust_score` = case when `endorsement_score` = 0
					then  (case when ifnull(`trust_score`,0) + ? <=100
					then ifnull(`trust_score`,0)+ ? else 100 end) else `trust_score` end,`endorsement_score` = ? ";
			$param_array = array($user_id, 1, $netScore,$netScore, $netScore, $netScore, $netScore,$netScore) ;
			Query_Wrapper::INSERT($sql,$param_array ,'user_trust_score');

		}
	}
	
	/**
	 * get endorsement data
	 * @param unknown_type $user_id
	 */
	public function getEndorsementData($user_array = array()){
		$result = array();
		if(isset($user_array) && !empty($user_array)){
			$ids =  implode(',', $user_array);
			$sql = "SELECT user_id, fname, pic FROM `user_endorsements` where user_id IN ($ids)  and fname is not null and is_endorsed = 'yes' order by tstamp desc";
			$result = Query::SELECT($sql, null);
		}
		
		$result_arr = array();
		foreach ($result as $val){
			$result_arr[$val['user_id']][] = $val;
		}
		
		return $result_arr;
	}
	
	/**
	 * get bucket data for the users
	 * @param unknown_type $user_array
	 */
	public function getUserBucketData($user_array = array()){
		$result = array();
		if(isset($user_array) && !empty($user_array)){
			$ids =  implode(',', $user_array);
			$sql = "SELECT last_14_days_visibility as visibility, bucket, user_id from user_pi_activity where user_id in ($ids)";
			$result = Query::SELECT($sql, null);
		}
		return  $result;
	}
	
	
	/**
	 * will return the details of user matching a certain pattern 
	 * e.g. u want to get the user with an email id then 
	 * data = array ("email_id" => "himanshu@tm.com")
	 * @param unknown_type $data
	 */
	public function getUserDataWithPattern($data){
		/*$keys = array_keys($data);
		$values = array_values($data);
		$sql = "SELECT * FROM user where " ;
		$count = count($data);
		$i = 0;
		foreach ($data as $key => $val){
			$i++;
			$sql .= "$key = '$val'";
			if($i < $count) $sql .= " OR "; 
		}*/
		$sql = "SELECT * from user where fid = ? or email_id = ?";
		$result = Query::SELECT($sql, array($data['fid'],$data['email_id']));
		//$result = Query::SELECT($sql, null);
		return $result;
	}
	
	/**
	 * set the value of a flag in `user_flags` table
	 * @param $flag
	 * @param $user_array - array of user ids with key as `user_id` and value as `flag value`
	 */
	public function setUserFlag($flag, $user_array){
		$sql = "INSERT INTO `user_flags` (user_id, $flag) values (?,?) ON DUPLICATE KEY UPDATE $flag = ?";
		$arr = array();
		foreach ($user_array as $key=>$val){
			$arr[] = array($key, $val, $val);
		}
		if(!empty($arr))
		Query::BulkINSERT($sql, $arr,'user_flags');
	}
	
	
	
	public function setEndorsementNoThanksResponse($user_id, $response){
		$sql = " INSERT INTO `endorsements_reasons_log` (user_id, no_thanks_reasons, no_thanks_tstamp) values (?,?,now())";
		Query::INSERT($sql,array($user_id, $response));
	}
	
	public function log_profile_pic($user_id,$match_id,$profile,$actual,$rand) {
		$query="insert into profile_pic_log (user_id,match_id,profile_shown,actual_profile,tstamp,rand) values(?,?,?,?,now(),?)";
		$param_array=array($user_id,$match_id,$profile,$actual,$rand);
		$tablename="profile_pic_log";
		Query_Wrapper::INSERT($query, $param_array, $tablename);
	}
	
	public function getBucket($user_id)
	{
		$query="select bucket from user_search where user_id=?";
		$param_array=array($user_id);
		$rs=Query_Wrapper::SELECT($query,$param_array,'slave',true);
		$bucket=(int)$rs['bucket'];
		return $bucket;
	}
}


?>
