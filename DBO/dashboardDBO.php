<?php 
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";

/* DBO class for reporting dashboard
 * Author :: Sumit
 * 🖖 Live Long and Prosper.
 */

class dashboardDBO
{
	public function saveNewQuery ($data)
	{
		$table = "admin_query" ;
		$sql = "INSERT INTO admin_query (query, query_name, query_description, created_by,set_as_cron,frequency,mailing_list,tStamp)
				values (?,?,?,?,?,?,?,now())" ;
		$param_array = array($data['query_text'],
							$data['query_name'], 
							$data['query_description'],
							$data['admin_id'],
							$data['set_as_cron'],
							$data['frequency'],
							$data['mailing_list']) ;
		$insert_id = Query_Wrapper::INSERT($sql, $param_array, $table);
		return $insert_id ;
	}
	
	public function fetchSavedQueries ($filter)
	{
		$sql = "select a.user_name as admin, aq.* from admin_query  aq join admin a on  a.id = aq.created_by where ";
		$param_array = array();
		if(isset($filter['admin']) && is_numeric($filter['admin']) && $filter['admin'] != 0 )
		{
			$sql .= "aq.created_by = ? and ";
			$param_array[] = $filter['admin'] ;
		}
		if(isset($filter['starred']) && $filter['starred'] != NULL)
		{
			$sql .= "aq.is_starred= ? and " ;
			$param_array[] = $filter['starred'] ;
		}
		$sql .=	"aq.status= 'active'	order by tstamp desc" ; 
		$result = Query_Wrapper::SELECT($sql, $param_array);
		return $result;
	}
	
	public function updateQuery ($data)
	{
		$table = "admin_query" ;
		$sql = "UPDATE admin_query SET query = ?, query_name = ?,query_description = ?,set_as_cron =?, frequency =?, mailing_list =? where query_id = ?" ;
		$param_array = array($data['query_text'], $data['query_name'], $data['query_description'], $data['set_as_cron'], $data['frequency'],
							$data['mailing_list'],$data['query_id']) ;
		Query_Wrapper::UPDATE($sql, $param_array, $table) ;
	}

	public function getQueryById($query_id)
	{
		$sql = 'select aq.* from admin_query aq where aq.query_id = ?' ;
		$result = Query_Wrapper::SELECT($sql, array($query_id), Query_Wrapper::$__slave, true);
		return $result;
	}

	public function showTables(){
		$sql = 'show tables' ;
		$result = Query_Wrapper::SELECT($sql, array(), Query_Wrapper::$__slave);
		return $result;
	}

	public function getTableDetails($table_name)
	{
		$sql = 'desc ' . $table_name;                    //No sql works with desc query in UNION. Tested by Tarun and Master Sumit
		$result = Query_Wrapper::SELECT($sql, array(), Query_Wrapper::$__slave);
		return $result;
	}

	public function logQueryDBO($data)
	{
		$table = "admin_query_log" ;
		$sql = "INSERT INTO admin_query_log (admin_id,query,time_taken,rows_count,error,ran_from,tstamp) VALUES (?, ?, ?, ?, ?, ?, now())" ;
		$param_array = array($data['admin_id'], $data['query'], $data['time_taken'], $data['rows_count'], $data['error'], $data['ran_from']) ;
		Query_Wrapper::INSERT($sql, $param_array, $table) ;
	}

	public function getAdminList(){
		$sql = "select id as admin_id, user_name from admin where privileges like '%reporting%' or super_admin='yes'" ;
		return Query_Wrapper::SELECT($sql, array(), Query_Wrapper::$__slave) ;
	}

	public function getAllAdmins(){
		$sql = "select * from admin" ;
		return Query_Wrapper::SELECT($sql, array(), Query_Wrapper::$__slave) ;
	}
	
	public function changeQueryStatusDBO ($query_id, $column, $value) 
	{	
		$tablename = "admin_query" ;
		$sql = "UPDATE admin_query SET $column = ? where query_id = ?" ;
		Query_Wrapper::UPDATE($sql, array($value, $query_id), $tablename) ;
	}

	public function updateAdminPermissions($admin_id, $privileges){
		$tablename = "admin" ;
		$sql = "UPDATE admin SET privileges = ? where id = ?" ;
		Query_Wrapper::UPDATE($sql, array($privileges,$admin_id), $tablename) ;
	}
}


?>