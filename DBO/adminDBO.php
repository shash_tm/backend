<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";



/**
 * Admin Database Object
 * Contains all the db queries related to admin
 * TODO: @Sumit: move all admin queries to this file for simplicity and maintenance of connections
 * @author himanshu
 */

class AdminDBO{
	/**
	 * set default value of discovery whenever all photos are rejected for a user
	 * @param unknown_type $user_id
	 * @param unknown_type $default
	 */
	public function setDiscoveryFlagOnAllPhotosRejection ($user_id, $default = true)
	{
		$sql = "INSERT INTO `user_flags` (user_id, hide_myself) values (?, ?) ON DUPLICATE KEY UPDATE hide_myself = ?" ;
		$result = Query::UPDATE($sql, array($user_id, $default, $default));
		return $result;
	}
	
	public function verifyUsernameAndPassword($user_name, $password)
	{
		$sql= "select id,user_name,super_admin,privileges from admin where user_name=? and password=md5(?)";
		$result = Query_Wrapper::SELECT($sql, array($user_name, $password), Query::$__slave, true) ;
		return $result ;
	}
	
	public function getRowFromGid($gid) 
	{
		$sql = "SELECT id,user_name,super_admin,privileges FROM admin where gid = ?" ;
		$result = Query_Wrapper::SELECT($sql, array($gid),Query::$__slave,true) ;
		return $result ;
	}
	
	public function createNewAdmin($email,$gid)
	{
		$table = "admin" ;
		$sql = "INSERT INTO admin (user_name, super_admin,privileges, gid) values (?,'no','none',?)" ;
		$param_array = array($email,$gid) ;
		$insert_id = Query_Wrapper::INSERT($sql, $param_array, $table) ;
		return $insert_id ;
	}
	
	public function setPrivileges ($admin_id, $permissions)
	{
		$table = "admin" ;
		$sql = "UPDATE admin SET privileges = ? WHERE id = ?" ;
		Query_Wrapper::UPDATE($sql, array($permissions,$admin_id), $table) ;
	}
	public function getAllPrivileges($admin_id = NULL) 
	{
		$sql = "SELECT id, user_name,privileges FROM admin ";
		$param_array = array() ;
		if($admin_id != null)
		{
			$sql .= "WHERE id = ?" ;
			$param_array[] = $admin_id ;
 		}
 		$result = Query_Wrapper::SELECT($sql, $param_array) ;
	}
	
	
}

?>