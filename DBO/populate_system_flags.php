<?php 
require_once dirname (__FILE__). '/../include/config.php';
require_once dirname (__FILE__). '/../abstraction/query_wrapper.php';

class Populate {
	private $redis;
	
	function __construct()
	{
		global $redis;
		$this->redis=$redis;
	}
	
	function populate($redis_key) {
		$type=$this->getType($redis_key);
		//var_dump($type);
		switch ($type) {
			case 1:
			//string
				$this->populate_string($redis_key);
				break;
			case 2:
			//sets	
				$this->populate_set($redis_key);
				break;
			case 3:
			//list	
				$this->populate_list($redis_key);
				break;
			case 4:
			//sorted sets
				$this->populate_sorted_set($redis_key);
				break;
			case 5:
			//hash
			    $this->populate_hash($redis_key);	
			    break;
			default:
				echo "\nType of Key Unknown\n";
				break;
		}
	}
	
	
	private function getType($redis_key)
	{
		return $this->redis->TYPE($redis_key);
	}
	
	private function populate_hash($redis_key) {
		$contents=$this->redis->hgetall($redis_key);
		//var_dump($contents);
		$name=$redis_key;
		$type='HASH';
		foreach ($contents as $key=>$val)
		{
			$query="insert into system_configuration (type,name,key_name,value) values(?,?,?,?)";
			$param_array=array($type,$name,$key,$val);
			$tablename='system_configuration';
			Query_Wrapper::INSERT($query,$param_array,$tablename);
		}
	}
	
	private function populate_list($redis_key) {
		$contents=$this->redis->lrange($redis_key,0,-1);
		//var_dump($contents);
		$name=$redis_key;
		$type='LIST';
		foreach ($contents as $val)
		{
			$query="insert into system_configuration (type,name,value) values(?,?,?)";
			$param_array=array($type,$name,$val);
			$tablename='system_configuration';
			Query_Wrapper::INSERT($query,$param_array,$tablename);
		}
	}
	
	private function populate_set($redis_key) {
	$contents=$this->redis->sMembers($redis_key);
		//var_dump($contents);
		$name=$redis_key;
		$type='SET';
		foreach ($contents as $val)
		{
			$query="insert into system_configuration (type,name,value) values(?,?,?)";
			$param_array=array($type,$name,$val);
			$tablename='system_configuration';
			Query_Wrapper::INSERT($query,$param_array,$tablename);
		}
	}
	
	private function populate_string($redis_key) {
		$val=$this->redis->get($redis_key);
		//var_dump($val);
		$type='STRING';
		$name=$redis_key;
		$query="insert into system_configuration (type,name,value) values(?,?,?)";
		$param_array=array($type,$name,$val);
		$tablename='system_configuration';
		Query_Wrapper::INSERT($query,$param_array,$tablename);
	}
	private function populate_sorted_set($redis_key) {
		$contents=$this->redis->zRange($redis_key,0,-1);
		//var_dump($contents);
		$name=$redis_key;
		$type='SORTED SET';
		foreach ($contents as $val)
		{
			$query="insert into system_configuration (type,name,value) values(?,?,?)";
			$param_array=array($type,$name,$val);
			$tablename='system_configuration';
			Query_Wrapper::INSERT($query,$param_array,$tablename);
		}
	}
	
	function del_db()
	{
		$query="delete from system_configuration";
		$param_array=NULL;
		$tablename='system_configuration';
		Query_Wrapper::DELETE($query,$param_array,$tablename);
	}
}

try {
	$obj=new Populate();
	$array=array('chat_configurations','chat_configurations_ab','native_ads_flags','chat_configurations_prefix','chat_configure_users');
	foreach ($array as $val)
	{
	   var_dump($val);
       $obj->populate($val);
	}
} catch (Exception $e) {
	echo $e->getMessage();
}

?>