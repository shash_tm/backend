<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";

/**
 * Messages Database Object
 * Contains all the db queries related to messages
 * @author himanshu
 */

class MessagesDBO{

	/**
	 * sets the last seen to tstamp = tstamp + diff for a conversation in messages queue
	 * NOTE: updating in reverse order - the person who has marked seen will be treated as receiver
	 * @param unknown_type $user1
	 * @param unknown_type $user2 
	 * @param unknown_type $diff
	 */
	public function updateLastSeenWithRespectToTstamp($user1, $user2, $diff){
		$sql = "UPDATE `messages_queue` SET `last_seen` = `tStamp` + ? where sender_id = ? and receiver_id = ?";
		$result = Query::UPDATE($sql, array($diff,  $user2, $user1));
		return $result;
	}
	
	public function getAllMessageQueues($userId, $tstamp){
		if (isset($tstamp)==false){
			$sql = "SELECT * FROM messages_queue where sender_id = ? or receiver_id = ? and msg_type != 'SPARK' order by tStamp desc";
			$result = Query::SELECT($sql, array($userId, $userId));
		}

		else {
			$sql = "SELECT * FROM messages_queue where (sender_id = ? or receiver_id = ?) and row_updated_tstamp >= ?  and msg_type != 'SPARK' order by tStamp desc";
			$result = Query::SELECT($sql, array($userId, $userId, $tstamp));
		}
		return $result;
	}

	/**
	 * fetching message list only for admin
	 * @param unknown_type $userId
	 */
	public function getAllMessageQueuesForAdmin($userId){
		$sql = "SELECT * FROM messages_queue where (sender_id = ? or receiver_id = ?) and tstamp >= date_sub(now(), interval 15 day) order by tStamp desc";
		$result = Query::SELECT($sql, array($userId, $userId));
		return $result;
	}
	
	public function getMessageQueue($user1, $user2){
		$mqSql = "SELECT *  FROM messages_queue WHERE sender_id =? AND receiver_id = ?  and msg_type != 'SPARK' ";
		$result = Query::SELECT($mqSql, array($user1, $user2), Query::$__slave, true);
		return $result;
	}
	
	public function setBlockedShown($user1, $user2){
		$sql = "UPDATE messages_queue SET is_blocked_shown = 1  where sender_id = ? AND receiver_id =? and blocked_by is not null";
		Query::UPDATE($sql, array($user1, $user2));
		 
		$sql2 = "UPDATE user_mutual_like set blocked_shown=? where user1 = ? and user2 = ? and blocked_by is not null";
		$params = array(0=>true,
						1 => ($user1<$user2)?$user1:$user2,
						2=> ($user1>$user2)?$user1:$user2);
		Query::UPDATE($sql2, $params);
	}
	
	public function getBlockingStatus($user1, $user2){
		/*$mqSql = "SELECT sender_id, receiver_id, is_blocked, tstamp  FROM messages_queue WHERE (sender_id =? AND receiver_id = ?) OR (sender_id =? AND receiver_id = ?)";
		$result = Query::SELECT($mqSql, array($user1, $user2, $user2, $user1), Query::$__slave, false);
		return $result;*/
		$mlsql = "SELECT blocked_by from user_mutual_like where user1=? AND user2=?";
		$params = ($user1 > $user2)? array($user2, $user1): array($user1, $user2);
		$result = Query::SELECT($mlsql, $params, Query::$__slave, true);
		return $result;
	}
	public function updateMessageQueueLastSeenMsgId($user1, $user2, $lastSeenMsgId,$time){
		$sql = "UPDATE messages_queue set last_seen = ?, last_seen_msg_id = ? WHERE sender_id =? AND receiver_id = ?";
		Query::UPDATE($sql, array($time,$lastSeenMsgId, $user1, $user2));
	}

	/**
	 * returns the conversation between two users
	 * if msg id is set then chat after that id is fetched
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 * @param unknown_type $msg_id
	 */
	public function getChat($user1, $user2, $msg_id = null){
		$sql = "SELECT * from current_messages_new where ((sender_id = ? AND receiver_id =?) OR (sender_id = ? AND receiver_id =?))";
		if(isset($msg_id) && $msg_id > 0)
		$sql .= " and msg_id > ?";

		$sql .= " order by msg_id";

		if(isset($msg_id) && $msg_id > 0)
		$param=  array($user1, $user2, $user2, $user1,$msg_id);
		else
		$param = array($user1, $user2, $user2, $user1);

		$result = Query::SELECT($sql, $param);
		return $result;

	}
	
	/**
	 * user1 blocks user2
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	public function block($user1, $user2){
		
		/*$msg = NULL; 
		$msgContentSql = "SELECT msg_content from messages_queue where ((sender_id = ? AND receiver_id =?) OR (sender_id = ? AND receiver_id =?)) order by tStamp desc limit 1 ";
		$msgRow = Query::SELECT($msgContentSql, array($user1, $user2, $user2, $user1));
		$msg = $msgRow[0]['msg_content'];*/
		
		$sql = "INSERT INTO messages_queue (sender_id,receiver_id, blocked_by, is_blocked_shown) VALUES (?,?,?,?)
				  ON DUPLICATE KEY 
				UPDATE blocked_by = ?, is_blocked_shown = ?";
		Query::INSERT($sql, array($user1, $user2, $user1, true,  $user1, true));
		
		
		$sql = "INSERT INTO messages_queue (sender_id,receiver_id, blocked_by, is_blocked_shown) VALUES (?,?,?,?)
				  ON DUPLICATE KEY 
				UPDATE blocked_by = ?, is_blocked_shown = ? ";
		Query::INSERT($sql, array($user2, $user1, $user1, false,  $user1, false));
		
		$sql2 = "UPDATE user_mutual_like set blocked_by =? where user1 = ? and user2 = ?";
		$params = array(0=> $user1,
						1 => ($user1<$user2)?$user1:$user2,
						2=> ($user1>$user2)?$user1:$user2);
		Query::UPDATE($sql2, $params);
	}
	
	public function reportAbuse($user1, $user2, $label_id, $reason, $description){
		$sql = "INSERT ignore INTO report_abuse (sender_id ,abuse_id ,abuse_msg ,abuse_time ,reason ,reason_description ,not_offensive)  values(?,?,?,now(),?,?,null) " ; 
		Query::INSERT($sql, array($user1, $user2, $label_id, $reason, $description));
	}
	/**
	 * 
	 * @param unknown_type $user1 - sender
	 * @param unknown_type $user2 - receiver
	 * @param unknown_type $message - message content
	 * @param unknown_type $message_type - contains the type of message
	 * $time -> time will be passed from the system so that same time can be used across the platform
	 */
	public function storeMessage($user1, $user2, $message, $message_type,$time,$unique_id,$quiz_id){
		if($message_type=='NUDGE_SINGLE'||$message_type=='NUDGE_DOUBLE'||$message_type=='QUIZ_MESSAGE'){
			$message_array=array("quiz_id"=>$quiz_id,"msg"=>$message);
			$message_json=json_encode($message_array);
		}
		else{
			$message_json=$message;
		}
		$sql = "INSERT INTO current_messages_new (sender_id,receiver_id, tStamp, msg_content, msg_type)values(?,?,?,?,?)" ;
		$insertId = Query::INSERT($sql, array($user1, $user2, $time,$message_json,$message_type));
		//added for the new data
		$sql_user_messages="INSERT INTO user_messages(user_id,tstamp,sender_id,receiver_id,msg_content,msg_type,msg_id,unique_id)
				values(?,?,?,?,?,?,?,?),(?,?,?,?,?,?,?,?)";
		$param2 =  array($user1,$time,$user1,$user2,$message_json,$message_type, $insertId,$unique_id,
				$user2,$time,$user1,$user2,$message_json,$message_type, $insertId,$unique_id);
		Query::INSERT($sql_user_messages, $param2);
		
		if($message_type == "JSON"){
			$message_array=json_decode($message);
			$message = $message_array->msg;
			$message_type = $message_array->message_type;
		}
		
		$sql2 = "INSERT INTO messages_queue (sender_id,receiver_id, msg_content, tStamp,msg_type) VALUES (?,?,?, ?,?)
				  ON DUPLICATE KEY 
				UPDATE msg_content = ?, tStamp = ?, msg_type = ?";
		 $param2 =  array($user1,$user2,$message,$time,$message_type, $message,$time,$message_type);
		 Query::INSERT($sql2, $param2);

		 return $insertId;
	}
	
	public function saveBadMessage($user1, $user2, $msg, $source=NULL){
		$sql = "INSERT into badMessages_exchanged values(?,?,?,now(),?)";
		Query::INSERT($sql,array($user1, $user2, $msg, $source));
	}
	

	/**
	 * set entry in messages queue if it's first entry through mutual like call
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	public function setEntryInMessageQueueOnMutualLike ($user1, $user2)
	{
		$table = "messages_queue" ;
		$sql2 = "INSERT IGNORE INTO messages_queue (sender_id,receiver_id, tStamp,msg_type) VALUES (?,?,now(),'MUTUAL_LIKE'), (?,?,now(),'MUTUAL_LIKE')";
		$param2 =  array($user1,$user2, $user2, $user1);
		return Query::INSERT($sql2, $param2,$table, null,true);
	}
	
	/**
	 * Make an entry of chat
	 * @param int $user1 - chat deleted by user1
	 * @param int $user2 - chat deleted of user2
	 * @param int $lastMessageId - last message id for user1
	 */
	public function clearChatMessageList($user1, $user2, $lastMessageId)
	{
		$clearChatData = $this->getLastChatClearedHistory($user1, $user2);
		if($clearChatData == NULL){
			$sql = "INSERT INTO user_clear_chat_history (user_id_1,user_id_2, time_stamp,last_message_id) VALUES (?,?,now(),?)";
			$param =  array($user1,$user2,$lastMessageId);
			Query::INSERT($sql, $param);
		}else{
			$sql = "UPDATE user_clear_chat_history SET time_stamp = NOW(),last_message_id = ? where clear_chat_id = ?";
			$param =  array($lastMessageId, $clearChatData['clear_chat_id']);
			Query::UPDATE($sql, $param);
		}
	}
	
	/**
	 * Get last message id for user1, where user1 is chatting with user2
	 * Used in clear chat function. Return -1 in case user clears chat without single message.
	 */
	public function getLastMessageIdForUser($user1, $user2){
		$sql = "SELECT MAX(msg_id) as msgId from user_messages where user_id = ? and (sender_id = ? or receiver_id = ?)";
		$result = Query::SELECT($sql, array($user1, $user2, $user2));
		return ($result != NULL && isset($result[0]['msgId'])) ? $result[0]['msgId'] : -1;
	}
	
	/**
	 * Get clear chat history for two users
	 */
	public function getLastChatClearedHistory($user1, $user2){
		$sql = "SELECT * from user_clear_chat_history where user_id_1 = ? and user_id_2 = ?";
		$result = Query::SELECT($sql, array($user1, $user2));
		return ($result != NULL && isset($result[0])) ? $result[0] : NULL;
	}
	
	/**
	 * Get all the clear chat history for a user
	 */
	public function getClearChatHistoryForUser($user){
		$sql = "SELECT * from user_clear_chat_history where user_id_1 = ?";
		$result = Query::SELECT($sql, array($user));
		return ($result != NULL && isset($result[0])) ? $result : NULL;
	}
}



?>
