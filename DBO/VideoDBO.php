<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../abstraction/userActions.php";


class VideoDBO
{
    public function GetHashParameters($user_id)
    {
        //To Validate whether the file uploaded is originally uploaded by the user
        //Fetch Gender and Registered from user table
        $sql="select md5(concat(gender,registered_at,user_id)) as myhash from user where user_id=?";
        $result=Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave,true);
        return $result;

    }


    public function GetVideosForAdmin($action_taken,$page_no)
    {
        $sql='';
        $start_page=$page_no*20;
        if($action_taken) {
            //Action not taken
            $sql = "select video_id,user_id,filename,duration,ismuted,vision_results,status,available_resolutions,is_approved,time_of_saving,tm_approved from user_video where status not in ('deleted') and sns_completed=1 and tm_approved is null order by time_of_saving asc";
        }else{
            //Action taken
            $sql="select video_id,user_id,filename,duration,ismuted,vision_results,status,available_resolutions,is_approved,time_of_saving,tm_approved from user_video where tm_approved is not null order by video_id desc limit $start_page,20";
        }
        $videos = Query::SELECT($sql,  null , Query::$__slave, false);

        return $videos;
    }

    public function ChangeVisionApproval($video_id,$vision_status)
    {
        //Called from adminPanel only
        $video_status='';
        if($vision_status=='yes')
        {
            $video_status='active';

        }else
        {
            $video_status='rejected';

        }
        $sql="update user_video set is_approved=?,status=? where video_id=?";
        $param_array=array($vision_status,$video_status,$video_id);
        Query_Wrapper::UPDATE($sql, ($param_array));

    }

    public function GetVideosPendingCli()
    {
        //This will be called from a manual script to update the videos in case for some videos the sns callbacks are failed
        $sql="select encoding_id as encode from user_video where job_status='pending' and  TIMESTAMPDIFF(MINUTE,time_of_saving,now())>5;";
        $videos = Query::SELECT($sql,  null , Query::$__slave, false);
        return $videos;
    }

    public function setTMApproved($video_id,$status)
    {
        $tm_approved=null;
        $video_status=null;
        if($status=='true')
        {
            $video_status='active';
            $tm_approved='yes';

        }else if($status='false')
        {
            $video_status='rejected';
            $tm_approved='no';
        }
        $sql="update user_video set tm_approved=?,status=? where video_id=?";
        $param_array=array($tm_approved,$video_status,$video_id);
        Query_Wrapper::UPDATE($sql, ($param_array));

    }

    public function InsertNewVideo($user_id,$jobid,$filename,$width,$height,$caption=null,$ismute=false,$duration=0)
    {
        $status='pending';
        if($jobid==-1)
        {
            //this was already at the lowest resolution
            $status='completed';
        }
        $param_array=array($user_id,$filename,$width,$height,$jobid,$status,'active',$caption,$ismute,$duration);
        $sql="insert into user_video(user_id,filename,width,height,encoding_id,job_status,status,caption,ismuted,duration) values (?,?,?,?,?,?,?,?,?,?)";
        $insert_id = Query_Wrapper::INSERT($sql, $param_array, 'user_video' ) ;
        return $insert_id;
    }

    public function UserProfileVideos($user_id,$myprofile)
    {
        $sql="select * from user_video where user_id=?";
        if(!$myprofile)
        {
            $sql=$sql." and status='active'";
        }
        $sql=$sql.' order by time_of_saving desc limit 1';
        $result=Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave,true);


        //So that we have the latest video of the user
    }


    public function getActiveVideos($user_ids,$fromphotos=false)
    {
        $status_var='';
        if($fromphotos)
        {
            $status_var="'active','rejected'";
        }
        else
        {
            $status_var="'active'";
        }
        $rejected_shown=array();
        if(count($user_ids)>0)
        {
            //To do: Remove Rejected If Required
            $str = implode(',', $user_ids);
            $sql_video = "SELECT video_id,user_id,filename,thumbnail,available_resolutions,is_approved,user_id,caption,encoding_id,status,job_status,height,width,duration,ismuted  from user_video where user_id in ($str) AND status in ($status_var) ORDER BY FIELD(user_id, $str)";
            $videos = Query::SELECT($sql_video,  null , Query::$__slave, false);
            if($fromphotos)
            {
                //Loop through $videos to check all those who have status as rejected and update them as rejected shown
                foreach($videos as $item)
                {
                    if($item['status']=='rejected')
                    {
                        array_push($rejected_shown,$item['video_id']);
                    }
                }
            }
            if(count($rejected_shown)>0)
            {
                $this->MarkRejectedShown($rejected_shown);
            }
            return $videos;
        }

        return null;

    }

    public function MarkRejectedShown($video_id_array)
    {
        $param_count = Query_Wrapper::getParamCount($video_id_array);
        $sql="update user_video set status='rejected_shown' where video_id in ($param_count) ";
        Query_Wrapper::UPDATE($sql, $video_id_array);
    }

//Try
    public function getActiveAndModeratedVideos($user_ids)
    {
        if(count($user_ids)>0) {
            $str = implode(',', $user_ids);
            $sql_video = "SELECT video_id,user_id,filename,thumbnail,available_resolutions,is_approved,user_id,caption,encoding_id,status,job_status,height,width,duration,ismuted  from user_video where user_id In ($str) AND status ='active' and is_approved = 'yes' and tm_approved='yes' and sns_completed=1 and job_status='completed' ORDER BY FIELD(user_id, $str)";
            $videos = Query::SELECT($sql_video,  null , Query::$__slave, false);
            return $videos;

        }
        return null;
    }

    public function MarkDeleted($user_id,$video_id)
    {
        $sql="update user_video set status='deleted' where video_id=? and user_id=?";
        $param_array=array($video_id,$user_id);
        Query_Wrapper::UPDATE($sql, ($param_array));
    }

    public function GetFilename($encoding_id)
    {
        $sql ="select filename from user_video where encoding_id=?";
        $param_array=array($encoding_id);
        $result=Query_Wrapper::SELECT($sql, ($encoding_id), Query::$__slave,true);


    }

    public function MarkSnsCompleted($encoding_id)
    {
        $sql="update user_video set sns_completed=1 where encoding_id=?";
        $param_array=array($encoding_id);
        Query_Wrapper::UPDATE($sql, ($param_array));
    }

    public function fetchUserId($encoding_id,$video_id=null)
    {
        if($video_id!=null)
        {
            $sql="select user_id from user_video where video_id=?";
            $param_array=array($video_id);
            $result=Query_Wrapper::SELECT($sql, $param_array, Query::$__slave,true);
            return $result;
        }
        $sql="select user_id from user_video where encoding_id=?";
        $param_array=array($encoding_id);
        $result=Query_Wrapper::SELECT($sql, $param_array, Query::$__slave,true);
        return $result;

    }

    public  function MarkApproved($encoding_id,$status,$vision_array)
    {
         //$vision_array contains the result set obtained from Vision Api
         $status_enum=null;
         $tm_status=null;
         if($status==true)
         {
             $status_enum='yes';
             $tm_status='active';
         }
         else
         {
            $status_enum='no';
             $tm_status='rejected';
         }
        $vision_json='';
        if(count($vision_array)>0)
        {

                $vision_json=json_encode($vision_array);
        }

         $sql="update user_video set sns_completed=1,is_approved='$status_enum',vision_results='$vision_json',status='$tm_status' where encoding_id=?";
         Query_Wrapper::UPDATE($sql, ($encoding_id));

    }
    public function MarkCompleted($encoding_id,$resolutions,$status,$thumbnail)
    {
        $status_enum=null;
        //$resolution is a json array containing the list of all th
        //e resolutions in which the video will be available
        $sql="update user_video set job_status=?,available_resolutions=?,thumbnail=? where encoding_id=?";
        $param_array=array($status,$resolutions,$thumbnail,$encoding_id);
        Query_Wrapper::UPDATE($sql, $param_array);
    }


    public function LogError($error_json,$encoding_id)
    {
        if(count($error_json)>0) {
            //Log an error in a table , according to the error that is associated
            $sql = "insert into video_error(error_details,encoding_id) values (?,?)";
            $sql_update="update user_video set job_status='error',status='rejected' where encoding_id=?";
            $param_array_update=array($encoding_id);
            $param_array =array(json_encode($error_json),$encoding_id);
            Query_Wrapper::INSERT($sql, $param_array, 'user_video' );
            Query_Wrapper::UPDATE($sql_update, $param_array_update);

            }
    }

}
