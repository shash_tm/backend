<?php 
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";

class QuizDBO 
{
	public static $nudgeListLimit = 21  ;
	public static $maximum_quiz_limit = 10000;
	
	public function getVersionFromconfig ($param)
	{
		$sql= "select * from config where name=?" ;
		$val = Query::SELECT($sql, array($param), 'slave', true) ;
		
		return $val['version'] ;
	}

	public static function updateVersionInConfig($param)
	{
		$sql = "UPDATE config SET version = version + 1 WHERE name = ?";
		Query_Wrapper::UPDATE($sql, array($param));
	}
	
	
	public function userPlayStatusAll ($user_id, $match_id, $tstamp = null ) 
	{
		$sql_user_quiz = "select quiz_id, user_id,timestamp from user_quiz where user_id in ( ? , ?)";
		$quiz_play_status =  Query::SELECT($sql_user_quiz, array($user_id, $match_id) );

	   return $quiz_play_status;
	}
	
	public function userPlayStatusWithTime ($user_id, $match_id, $user_tstamp, $match_tstamp )
	{

		$sql_user_quiz = "select quiz_id, user_id, timestamp from user_quiz where (user_id = ? and timestamp >= ? ) or (user_id = ? and timestamp >= ? )";
		$quiz_play_status =  Query::SELECT($sql_user_quiz, array($user_id, $user_tstamp, $match_id, $match_tstamp) );
	
		return $quiz_play_status;
	}
	public function quizPlayStatus ($user_id, $match_id, $quiz_array )
	{
		$quiz_ids = implode(",", $quiz_array) ;
		$sql_quiz = "select quiz_id, count(user_id) as cu from user_quiz where user_id in (? , ?) and quiz_id in ($quiz_ids) group by quiz_id having cu = 2";
		$quiz_play_status =  Query::SELECT($sql_quiz, array($user_id, $match_id) );
	
		return $quiz_play_status;
	}
	
	
	public function allQuizzes ($quiz_status, $state_id, $city_id)
	{
		$sql = "select quiz_id, display_name, image, banner as banner_url, description,is_sponsored,sponsored_metadata from quiz ";
		if($quiz_status == 'test')
		{
			$sql .=  "where status in ('test','active') ";
			$param_array = array();
		}
		else
		{
			$sql .= "where status = ? ";
			$param_array = array($quiz_status);
		}

		$sql .= "and (state_id is null or state_id = ?) and (city_id is null or city_id = ?) and type != 'select' order by quiz_id desc";
		array_push($param_array,$state_id, $city_id);

		$quiz_data = Query::SELECT($sql,$param_array );
		return $quiz_data ;
	}
	
	public function QuizExists ($quiz_id )
	{
		if ($quiz_id < 1 || $quiz_id > QuizDBO::$maximum_quiz_limit)
		{
		 return ;
		}
		$sql = "select * from quiz where quiz_id = ? " ;
		$quiz = Query::SELECT($sql, array($quiz_id), Query::$__slave, true );
		return $quiz;
	}
	
	public function userQuizStatus ( $quiz_id, $user_id_arr)
	{
		$user_ids = implode($user_id_arr, ',');
		$sql = "select * from user_quiz where quiz_id= ? and user_id in ( $user_ids )" ;
        $status = Query::SELECT($sql, array($quiz_id)) ;
        
        $final_arr = array();
        foreach ($status as $val)
        {
        	$final_arr[$val['user_id']] = $val;
        }
       
		return $final_arr ;
	}
	
	public function showQuizQuestions ( $quiz_id)
	{
		$sql = "select q.question_id,q.rank, q.text as question_text, q.image as question_image, o.option_id, o.text as option_text,
		o.image as option_image 
		        from quiz_question q 
		        join quiz_option o on q.question_id=o.question_id 
		        where quiz_id= ? and q.status='active' and o.status='active' order by q.rank, q.question_id, o.option_id ";
		$ques_arr = Query::SELECT($sql, array($quiz_id));
		return $ques_arr ;
	}
	
	public function saveAllAnswers ($answers_arr)
	{
		$sql = "insert ignore into quiz_answer ( user_id, quiz_id, question_id, answer ) values (?, ? , ?, ? ) " ;
		Query::BulkINSERT($sql, $answers_arr);
	}
	
	public function saveUserQuizStatus ($user_id, $quiz_id )
	{
		$sql = "insert into user_quiz values ( ? , ?, now())" ;
		Query::INSERT($sql, array($user_id, $quiz_id));
	}
	
	public function getQuestionAndAnswers($user_id, $match_id, $quiz_id) 
	{
		$sql = "select q.quiz_id, user_id, q.question_id, q.text as question_text, q.image as question_image, a.answer, o.text as answer_text, 
		o.image as answer_image from quiz_question q join quiz_answer a on  a.question_id = q.question_id 
		join quiz_option o on (q.question_id= o.question_id and a.answer= o.option_id)		
		where q.quiz_id= ? and  a.user_id in ( ?, ? ) order by q.rank,q.question_id";
		$answers= Query::SELECT($sql, array($quiz_id, $user_id, $match_id )); 
		
		return $answers;
	}

	public function getQuestionAndAnswersIds ($user_id, $match_id, $quiz_id)
	{
		$sql = "select q.quiz_id, user_id, q.question_id, a.answer from quiz_question q join quiz_answer a on  a.question_id = q.question_id
		where q.quiz_id= ? and  a.user_id in ( ?, ? ) order by q.question_id";
		$answers= Query::SELECT($sql, array($quiz_id, $user_id, $match_id ));
	   
		return $answers;
	}
	
	public function storeMatchScore ($score)
	{
		$sql = "INSERT IGNORE INTO user_quiz_score (user1, user2, quiz_id, all_count, answer_match_count, score) VALUES 
				(?, ?, ?, ?, ?, ?)" ;
		Query::BulkINSERT($sql, $score ) ;
	}
	
	
	public function getUserIdForNudgeList($user_id, $quiz_id , $match_id = null )
	{
		/*$sql= "select match_id, t.timestamp, q.user_id from 
		(select user1 as user_id, user2 as match_id,timestamp  from user_mutual_like where user1= ? and blocked_by is null
			union  
		 select user2 as user_id, user1 as match_id,timestamp  from user_mutual_like where user2= ? and blocked_by is null) t 
		left join user_quiz q on ( q.user_id=t.match_id and q.quiz_id= ? ) order by ";*/
		$sql = "select receiver_id as match_id from messages_queue mq join  user u on 
		mq.receiver_id= u.user_id  where  sender_id= ? and u.status= 'authentic' and blocked_by is null order by " ;  
		if( $match_id )
		{
			 
			$sql .= "  receiver_id = ? desc,  " ;
			$param = array($user_id, $match_id) ;
		}
		else 
		{
			$param = array( $user_id ) ;
		}
		$sql .= "  mq.tstamp desc limit " . QuizDBO::$nudgeListLimit;
		$users= Query::SELECT($sql, $param );
		$matchIds = array();
	    foreach ($users as $val) 
		{
			$matchIds [] = $val['match_id'] ;
			//echo $val['match_id'] . ", ";
		}
		
		$match_id_list = implode(",", $matchIds);
		$sql_quiz = "select user_id from user_quiz where user_id in ($match_id_list) and quiz_id = ?" ;
		$quiz_status = Query::SELECT($sql_quiz, array( $quiz_id));
		$quiz_array = array();
		foreach ($quiz_status as $val)
		{
			$quiz_array[$val['user_id']]= $val['user_id'];
		}
		
		$fin_array = array();
		foreach ($users as $value)
		{
			$temp_array = array();
			$temp_array ['match_id'] = $value['match_id'];
			$temp_array ['user_id']  = $quiz_array[$value['match_id']] ;
			$fin_array[] = $temp_array;
		}
		//var_dump($fin_array);
		return $fin_array;
	}
	
	public function getOneUserToNudge($user_id, $quiz_id, $match_id)
	{
		$sql= "select receiver_id as match_id, q.user_id from messages_queue mq left join user_quiz q on 
		( q.user_id=mq.receiver_id and q.quiz_id= ? )  where blocked_by is null and sender_id= ? and receiver_id = ?" ;
		$users= Query::SELECT($sql, array($quiz_id, $user_id, $match_id));
		//var_dump($users);
		return $users;
	}
	
	/**
	 * To Fetch the quiz score between two users for the active quizzes .
	 * 
	 */
	public function fetchQuizScore($user_id, $match_id, $quiz_array) 
	{

		if($user_id < $match_id)
		{
			$user1 = $user_id ;
			$user2 = $match_id ;
		} else
		{
			$user1 = $match_id ;
			$user2 = $user_id ;
		} 
		$quizzes = implode(',', $quiz_array);
		$sql= "SELECT * from user_quiz_score where user1 = ? and user2 = ? and quiz_id IN ($quizzes) ";
		$score = Query::SELECT($sql, array ($user1, $user2)) ;
		$score_key = array();
		foreach ($score as $value) 
		{
			$score_key[$value['quiz_id']] = $value;
		}
		return $score_key ;
	}

	public static function getQuizListDBO($status= null)
	{
		$sql = "SELECT quiz_id, title, display_name, description, status, date_created,date_edited "
			  ."from quiz WHERE status not in ('deleted')" ;
		$param_array = array();
//		if($status != null)
//		{
//			$sql .= " WHERE status = ?";
//			$param_array[] = $status;
//		}
		$quizzes = Query_Wrapper::SELECT($sql, $param_array);
		return $quizzes;
	}

	public  static function saveQuizAttributesDBO($quiz_id, $description, $title, $display_name, $image, $banner, $state_id, $city_id)
	{
		$sql = "INSERT INTO quiz (quiz_id, description, title, display_name, image, banner, state_id, city_id) VALUES (?, ?, ?, ?, ?, ?, ?, ? ) ".
			   "ON DUPLICATE KEY UPDATE description = ?, title = ?, display_name = ?, image = ?, banner = ?, state_id = ?, city_id = ?" ;
		$param_array = array($quiz_id, 
							$description, 
							$title, 
							$display_name, 
							$image, 
							$banner,
							$state_id, 
							$city_id, 
							$description, 
							$title, 
							$display_name, 
							$image,
							$banner,
							$state_id,
							$city_id ); 
		$insert_id= Query_Wrapper::INSERT($sql, $param_array, 'quiz');
		return $insert_id ;
	}

	public static function updateQuestionTextDBO($quiz_id, $question_id, $question_text, $status,$rank = null)
	{
		$sql = "INSERT INTO quiz_question (question_id, quiz_id, rank, text, status) VALUES  (?, ?, ?, ?, ?) ".
			   "ON DUPLICATE KEY UPDATE text = ?, rank = ?, status = ?" ;
		$param_array = array($question_id, $quiz_id, $rank, $question_text, $status, $question_text, $rank, $status) ;
		$insert_id = Query_Wrapper::INSERT($sql, $param_array, 'quiz_question' ) ;
		return $insert_id ;
	}

	public static function updateQuizOptionsDBO ($question_id, $options)
	{
		$sql = "INSERT INTO quiz_option  (question_id, option_id, text, image) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE text = ?, image = ?" ;
		$param_array = array();
		foreach($options as $option)
		{
			if((isset($option['option_text']) && $option['option_text'] != null) || (isset($option['option_image']) && $option['option_image'] != null))
			$param_array[] = array($question_id, $option['option_id'], $option['option_text'], $option['option_image'], $option['option_text'], $option['option_image']);
		}
		if(count($param_array) > 0)
		Query_Wrapper::BulkINSERT($sql, $param_array, 'quiz_option');
	}

	public static function updateQuizStatusDBO($quiz_id, $status)
	{
		$sql ="UPDATE quiz SET status= ? WHERE quiz_id= ?";
		Query_Wrapper::UPDATE($sql, array($status, $quiz_id));
	}
}   

?>