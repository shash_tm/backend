<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../abstraction/userActions.php";

class SelectDBO
{

	public static function updateSpark($user1, $user2, $action="seen")
	{
		if($action=="seen") {
			$sql = "UPDATE user_spark SET status = ?, expired_date=DATE_ADD(now(), INTERVAL `expiry_time` SECOND) WHERE user1 = ? AND user2 = ?";
		}else{
			$sql = "UPDATE user_spark SET status = ? WHERE user1 = ? AND user2 = ?";
		}
		$param_array = array($action, $user1, $user2);

		Query_Wrapper::UPDATE($sql,$param_array,"user_spark");

		if($action == 'accepted'){
			$userAction = new UserActions();
			$userAction->performAction($user1, $user2,'like',0,null,0,true);
			$userAction->performAction($user2, $user1,'like',0,null,0,true);

		}else if($action == 'rejected'){
			$userAction = new UserActions();
			$userAction->performAction($user1, $user2,'like',0,null,0,true);
			$userAction->performAction($user2, $user1,'hide',0,null,0,true);
		}
	}

	public static function getActivePackageBuckets($source){
		$param_array = array($source);
		$sql = "select id as bucket_id, source,status, filter from spark_bucket where status = 'active' and source = ? ";
		$bucketList =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
		return $bucketList;
	}

	public static function getSelectQuizId(){
		$sql = "select quiz_id from quiz where type='select'";
		$result =  Query_Wrapper::SELECT($sql, array(), Query::$__slave, true);
		return $result['quiz_id'];
	}

	public static function getCurrentUserSelectData($user_id){
		$param_array = array($user_id);
		$sql = "select DATEDIFF(expiry_date, now()) as days_left, status, metadata from
				user_subscription where user_id = ? order by id desc limit 1";
		$bucketList =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave, true);
		return $bucketList;
	}

	public static function getUserListSelectData($user_id_array){
		if(!isset($user_id_array) || count($user_id_array) <= 0)
			return array();
		$user_id_string = Query_Wrapper::getParamCount($user_id_array);
		$sql = "select user_id, status, metadata from
				user_subscription where user_id in ($user_id_string) and status in ('active','trial') and expiry_date > now() and metadata is not null";
		$userSelectList =  Query_Wrapper::SELECT($sql,$user_id_array, Query::$__slave);
		return $userSelectList;
	}

	public static function createSpark($user1, $user2, $message, $message_id, $expiry_time, $pendingSparks){
		$sql = "INSERT INTO user_spark(user1, user2,status,message,message_id, expiry_time) values (?, ?, 'sent', ?, ?, ?)";
		$param_array = array($user1,$user2,$message,$message_id,$expiry_time);
		$spark_id = Query_Wrapper::INSERT($sql, $param_array, "user_spark") ;
		self::updateUserSparkCounter($user1, $pendingSparks);
		return array("success"=> "Spark created","responseCode"=>"200");
	}

	public static function subscribeToSelect($user_id, $expiry_days, $status)
	{
		$selectSubscriptionId = 1;
		$sql = "INSERT INTO user_subscription (subscription_id, user_id, tstamp, expiry_date,status)
								VALUES (?, ?, now(), DATE_ADD(now(), interval ? day), ?) ON DUPLICATE KEY UPDATE ".
			"status = ?,  expiry_date=  CASE
                												WHEN now() >= expiry_date
                													THEN DATE_ADD(now(), interval ? day)
																	ELSE DATE_ADD(expiry_date, interval ? day)
																END";

		$param_array = array($selectSubscriptionId,$user_id, $expiry_days, $status, $status,$expiry_days,$expiry_days) ;
		Query_Wrapper::INSERT($sql, $param_array, "user_spark_counter");
	}

	public static function markSelectQuizPlayed($user_id, $play_status){
		$metadata = json_encode(array("quiz_played" => $play_status));
		$sql = "update user_subscription set metadata = ? where user_id = ?";
		Query_Wrapper::UPDATE($sql, array($metadata, $user_id), "user_subscription");
                self::updateUserSearchAfterSelectTxn($user_id);
	}

	public static function deleteSelectMembership($user_id){
		$sql = "delete from user_subscription where user_id = ?";
		Query_Wrapper::UPDATE($sql, array($user_id), "user_subscription");

		$sql1 = "delete from quiz_answer where user_id=? and quiz_id=25";
		Query_Wrapper::UPDATE($sql1, array($user_id), "quiz_answer");
	}

	public static function getCategoriesForSelectMatches($user_id, $match_id_array){
		$quiz_id = self::getSelectQuizId();

		$param_array = array($user_id, $quiz_id);
		$param_array = array_merge($param_array, $match_id_array);
		$param_array[] = $quiz_id;

		$match_param_string = Query_Wrapper::getParamCount($match_id_array);
		$sql = "select m.match_id, u.category, count(u.category) as count from
 				(select qa.quiz_id,qa.user_id,qq.category,qa.answer, qa.question_id from quiz_answer qa join quiz_question qq on qa.question_id = qq.question_id
 					where qa.user_id = ? and qa.quiz_id= ?) u RIGHT JOIN
				(select qa.quiz_id,qa.user_id as match_id ,qq.category,qa.answer, qa.question_id from quiz_answer qa join quiz_question qq on qa.question_id = qq.question_id
					where qa.user_id in ($match_param_string) and qa.quiz_id = ?) m
				on m.question_id = u.question_id where u.answer=m.answer  group by u.category, m.match_id order by m.match_id";
		$result = Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
		return $result;
	}

	public static function updateUserSearchAfterSelectTxn($user_id){
		$sql = "update user_search set isSelect = 1 where user_id = ?";
		Query_Wrapper::UPDATE($sql, array($user_id), "user_search") ;
	}

	public static function registerUserSeenSelectPackages($user_id){
		if(self::hasViewedPaidSelectPackages($user_id) == 0) {
			$sql = "INSERT INTO has_seen_select_paid(id,user_id,tstamp) VALUES (NULL, ?,now()) ON DUPLICATE KEY UPDATE user_id = ? ";
			$param_array = array($user_id, $user_id);
			Query_Wrapper::INSERT($sql, $param_array, "user_spark");
		}
	}

	public static function hasViewedPaidSelectPackages($user_id){
		$param_array = array($user_id);
		$sql = "select count(user_id) as user_count from has_seen_select_paid where user_id = ?";
		$result =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave, true);
		if((int)$result['user_count'] <= 0)
			return 0;
		return 1;
	}
}
