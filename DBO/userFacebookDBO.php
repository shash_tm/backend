<?php 
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";

class UserFacebookDBO {

	public function get_facebook_token($user_id) {
		$sql = "select fb_token from user_facebook where user_id = ?" ;
		$fb_token_row = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
		if ($fb_token_row != null && isset($fb_token_row['fb_token']))
			return $fb_token_row['fb_token'];
	}


	public function get_facebook_id($user_id) {
		$sql = "select fid from user_facebook where user_id = ?" ;
		$fb_token_row = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
		if ($fb_token_row != null && isset($fb_token_row['fid']))
			return $fb_token_row['fid'];
	}


	public function get_facebook_ids($user_ids) {
		$fb_ids = array();
		foreach (array_chunk($user_ids, MutualFriends::$mysql_chunk_size) as $slice) {
			$param_count = Query_Wrapper::getParamCount($slice);
			$sql = "select user_id, fid from user_facebook where user_id in ($param_count)";
			$rows = Query_Wrapper::SELECT($sql, $slice);
			if (count($rows) > 0) {
				foreach ($rows as $key => $value)
					$fb_ids[$rows[$key]['user_id']] = $rows[$key]['fid'];
			}
		}
		return $fb_ids;
	}
	
	public function update_facebook_token($user_id, $fb_token) {
		$query = "UPDATE user_facebook SET fb_token = ? where user_id = ?";
		$param_array = array($fb_token, $user_id);
		$tablename = "user_facebook";
		Query_Wrapper::UPDATE($query, $param_array, $tablename);
	}
	
	public function get_last_logout_time($user_id) {
		$sql = "select last_logout from user_facebook where user_id = ?" ;
		$fb_token_row = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
		if ($fb_token_row != null && isset($fb_token_row['last_logout']))
			return $fb_token_row['last_logout'];
	}
	
	public function update_last_logout_time($user_id, $time) {
		$query = "UPDATE user_facebook SET last_logout = ? where user_id = ?";
		$param_array = array($time, $user_id);
		$tablename = "user_facebook";
		Query_Wrapper::UPDATE($query, $param_array, $tablename);
	}

	public function update_likes_count($user_id, $likes_count) {
		$query = "update user_facebook set no_of_likes = ? where user_id = ?";
		$param_array = array($no_of_likes, $user_id);
		$tablename = "user_facebook";
		Query_Wrapper::update($query, $param_array, $tablename);
	}
}


?>
