<?php 
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";
//require_once dirname ( __FILE__ ) . "/../logging/QueryLog.class.php";

/**
 * Likes Database Object
 * Contains all the db queries required for likes functionality
 * @author himanshu
 */

class userActionsDBO{
	
	/**
	 * sets mutual like between two users
	 * stores in db as  (user1, user2 , time) where user1 > user2
	 * @param $user1
	 * @param $user2
	 */
	public function setMutualLike($user1, $user2){
		$table = "user_mutual_like" ;
		$query = "INSERT ignore INTO user_mutual_like (user1, user2 , timestamp) values (?,?, now())";
		$params = ($user1 >  $user2)? array($user2, $user1): array($user1, $user2);
		$result = Query::INSERT($query, $params,$table, Query::$__master);
		return $result;
	}
	/**
	 * returns true if user1 has taken action on user2 else false
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	public function isActionSet($user1, $user2, $action_table){
		$sql = "SELECT * FROM $action_table where user1=? and user2 =?";
		$result = Query::SELECT($sql, array($user1, $user2));
		//var_dump($result);
		return ((!empty($result))? true:false);
	}
	
	/**
	 * returns true if user1 and user2 mutually liked
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 */
	public function isMutualLikeSet($user1, $user2){
		$sql = "SELECT * FROM `user_mutual_like` where user1=? and user2 =?";
		$params = ($user1 >  $user2)? array($user2, $user1): array($user1, $user2);
		$result = Query::SELECT($sql, $params);
		return ((!empty($result))? true:false);
	}
	
	/**
	 * 
	 */
	public function insertRepeatLikes($user1, $user2,$action_taken){
		$table = "user_mutual_like" ;
		$query = "INSERT ignore INTO user_repeat_profile (user1, user2 , action,tstamp) values (?,?,?, now())";
		$params =  array($user1, $user2,$action_taken);
		$result = Query::INSERT($query, $params,$table, Query::$__master);
		return $result;
	}
	
	
	/**
	 * to add to a particular queue like/hide/reject
	 * @param unknown_type $user1
	 * @param unknown_type $user2
	 * @param unknown_type $table
	 */
	public function addToQueue($user1, $user2, $table) {
            $sql =  "insert ignore into $table(user1,user2,timestamp) values(?,?,now())" ;
            $result = Query::INSERT($sql, array($user1, $user2),$table, Query::$__master, true);
            return $result;
	}
	
	public function setInactiveForMayBeQueue($user1, $user2){
		$sql = "UPDATE user_maybe set is_active = 0 where user1 = ? and user2 = ?";
		Query::UPDATE($sql, array($user1, $user2));
	}
	
	
	
	/**
	 * to check if any action is already happened between two users
	 * @param $user1
	 * @param $user2
	 * @param $action - the action to check for
	 */
	
	public function isActionDone($user1, $user2, $table){ 

		$sql = "SELECT * from $table where user1 = ? and user2 =? ";
		if($table == "user_maybe") $sql .= " and is_active = 1 ";

		$params = array($user1, $user2);
		$result = Query::SELECT($sql, $params);

		if(!empty($result)) return  true;
		
		return false;
		
	}
		
		
	/**
	 * get all the ids from like/hide tables on which actions is been done by user_id on the user array
	 * @param $user_id
	 * @param $user_array
	 */
	public function getActionsDone($user_id , $user_array){
		$sql_like = "SELECT user2 as user_id from user_like where user1 = ? and user2 IN (". implode(',', $user_array) .") ";
		$sql_hide= "SELECT user2 as user_id from user_hide where user1 = ? and user2 IN (". implode(',', $user_array) .")";

		$sql_maybe= "SELECT user2 as user_id from user_maybe where is_active = 1 and user1 = ? and user2 IN (". implode(',', $user_array) .")";
		
		//echo $sql_like;
		$result_like = Query::SELECT($sql_like, array($user_id), Query::$__slave, false);
		$result_hide = Query::SELECT($sql_hide, array( $user_id), Query::$__slave, false);
		$result_maybe = Query::SELECT($sql_maybe, array( $user_id), Query::$__slave, false);
		
		if(isset($result_like)){
			foreach ($result_like as $val)
			$likeIds[] = $val['user_id'];
		}
		
		if(isset($result_hide)){
			foreach ($result_hide as $val)
			$hideIds[] = $val['user_id'];
		}
		
	
		if(isset($result_maybe)){
			foreach ($result_maybe as $val)
			$maybeIds[] = $val['user_id'];
		}
		
		$result = array("like_ids" => $likeIds,
						"hide_ids" => $hideIds,
						"maybe_ids" => $maybeIds);
		return $result;
	}
        
        /*
         * The function is used to update the user_current_session_table when an action (like/hide) has been taken by the user.
         * The idea is to log that the session was used by the user and for next set of recommendations an incremented counter shall be inferred.
         * @param - $user_id = The id of the user that has taken action
         */
        public function updateSessionTable($user_id) {
            $session_update_sql = "UPDATE user_current_session_table set action_taken = ? where user_id = ?";
            Query::UPDATE($session_update_sql, array(1 ,$user_id));
        }
}


?>
