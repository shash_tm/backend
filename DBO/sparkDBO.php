<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../abstraction/userActions.php";
require_once dirname ( __FILE__ ) . "/../RecommendationEngines/Utils/TwoWayMatching.php";

class SparkDBO
{
	public static function getActivePackages($user_id, $source, $bucket_id, $type='spark'){
		$param_array = array($type, $bucket_id);
		if($source == 'iOSApp'){
			$sql = "select sp.price, sp.spark_count,sp.expiry_days, sp.title, sp.description,sp.sku as apple_sku, sp.metadata, sp.type, sp.currency from ".
				" spark_packages sp join spark_bucket_package sbp on sbp.package_id = sp.package_id  where sp.source = 'iOSApp' ";

		}else if($source == 'androidApp'){
			$sql = "select sp.price, sp.spark_count,sp.expiry_days, sp.title, sp.description,sp.sku as google_sku, sp.metadata, sp.type,sp.currency from ".
				" spark_packages sp join spark_bucket_package sbp on sbp.package_id = sp.package_id  where sp.source = 'androidApp' ";
		}

		$sql .= "and sp.type =? and sp.status = 'active' and sbp.bucket_id = ? order by sp.price limit 3";
		$sparkList =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);

		return $sparkList;
	}

	/*
	 * To Validate wether given package id is valid and app_visible
	 */
	public function getPackageDetails($package_id)
	{
		$sql = "SELECT package_id, price, spark_count, expiry_days, metadata, type FROM spark_packages WHERE package_id= ? ";
		$spark_details = Query_Wrapper::SELECT($sql, array($package_id), Query::$__slave, true);
		return  $spark_details;
	}

	public function getPackageDetailsBySku($sku, $source)
	{
		$sql = "SELECT package_id, price, spark_count, expiry_days, status, metadata, type FROM spark_packages WHERE sku= ? and source = ?";
		$spark_details = Query_Wrapper::SELECT($sql, array($sku, $source), Query::$__slave, true);
		return  $spark_details;
	}

	public function initiatePayment($package_details,$user_id, $payment_id, $pg)
	{
		$sql = "INSERT INTO user_spark_transaction (user_id, package_id, price, spark_count, tstamp, payment_id, payment_gateway) VALUES ".
			   "(?, ?, ?, ?, now(), ?, ? )" ;
		$param_array = array($user_id, $package_details['package_id'],$package_details['price'],$package_details['spark_count'],$payment_id, $pg) ;
		$transaction_id = Query_Wrapper::INSERT($sql, $param_array, "user_spark_transaction") ;
		return $transaction_id ;
	}

	// get unique transaction row from payment_id
	public function getSparkPaymentByPaymentId($payment_id){
		$sql = "SELECT ups.user_id, ups.package_id, ups.price, ups.spark_count, ups.tstamp, ups.payment_id, ups.gateway_txn_id, ".
			" ups.status, ups.google_token, sp.sku ".
			" from user_spark_transaction ups join spark_packages sp on sp.package_id = ups.package_id where ups.payment_id = ?";
		$result =  Query_Wrapper::SELECT($sql, array($payment_id), Query::$__slave, true);
		return $result;
	}

	//check for duplicate txn id for apple payments
	public function getSparkPaymentByAppleTxnId($transaction_id, $status='consumed', $payment_id){
		$sql = "SELECT ups.user_id, ups.package_id, ups.price, ups.spark_count, ups.tstamp, ups.payment_id, ".
			" ups.status, ups.gateway_txn_id as apple_txn_id, sp.sku ".
			" from user_spark_transaction ups join spark_packages sp on sp.package_id = ups.package_id where ups.gateway_txn_id = ? and ups.payment_gateway = 'apple' and ups.status = ? and ups.payment_id = ?";
		$result =  Query_Wrapper::SELECT($sql, array($transaction_id, $status,$payment_id), Query::$__slave, true);
		return $result;
	}

	public function addSparkCount($user_id, $spark_count, $expiry_days, $chat_assist)
	{
        $sql = "INSERT INTO user_spark_counter (user_id, spark_count, expiry_date,chat_assist) VALUES (?, ?,  DATE_ADD(now(), interval ? day), $chat_assist) ON DUPLICATE KEY UPDATE ".
                "spark_count = spark_count+?, chat_assist= $chat_assist, expiry_date=  CASE
                												WHEN DATE_ADD(now(), interval ? day) > expiry_date
                													THEN DATE_ADD(now(), interval ? day)
																	ELSE expiry_date
																END";

        $param_array = array($user_id,$spark_count, $expiry_days, $spark_count, $expiry_days, $expiry_days) ;
        Query_Wrapper::INSERT($sql, $param_array, "user_spark_counter");
	}

	public static function createSpark($user1, $user2, $message, $message_id, $expiry_time, $pendingSparks){
		$sql = "INSERT INTO user_spark(user1, user2,status,message,message_id, expiry_time) values (?, ?, 'sent', ?, ?, ?)";
		$param_array = array($user1,$user2,$message,$message_id,$expiry_time);
		$spark_id = Query_Wrapper::INSERT($sql, $param_array, "user_spark") ;
		self::updateUserSparkCounter($user1, $pendingSparks);
		return array("success"=> "Spark created","responseCode"=>"200");
	}

	public static function updateUserSparkCounter($user_id, $pendingSparks){
		$sql = "update user_spark_counter set spark_count = ?, last_used_date=now(),
			spark_used = spark_used+1 where user_id = ?";
		Query_Wrapper::UPDATE($sql, array($pendingSparks, $user_id), "user_spark_counter") ;
	}

	public static function updateUserSparkTxnStatus($payment_id, $status, $token, $pg, $order_id=null){
		//$pg can be google, apple or paytm
		$sql = "update user_spark_transaction set status = ?, google_token = ?, gateway_txn_id = ? where payment_id = ?";
		Query_Wrapper::UPDATE($sql, array($status, $token, $order_id, $payment_id), "user_spark_transaction") ;
	}

	public static function getUserActiveSparkDetails($user_id){
		$sql = "select user_id,spark_count,expiry_date,spark_used,last_used_date from user_spark_counter where user_id = ? and date(expiry_date) >= date(NOW())";
		$userSparkCounter = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
		return $userSparkCounter;
	}
        
	public static function getUserActiveSparkCount($user_id){
		$sql = "select spark_count from user_spark_counter where user_id = ? and date(expiry_date) >= date(NOW())";
		$userSparkCounter = Query_Wrapper::SELECT($sql, array($user_id), Query::$__master, true);
		if(sizeof($userSparkCounter) <= 0) {
			return 0;
		}
		return $userSparkCounter['spark_count'];
	}

	public static function getPendingReceivedSparks($user_id){
		$sql = "select count(*) as count from user_spark where user2 = ? and status='sent'";
		$result = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave, true);
		if((int)$result['count'] <= 0)
			return 0;
		return (int)$result['count'];
	}

	public static function checkForDuplicateSparkForUsers($user1, $user2){
		$sql = "select count(*) as count from user_spark where user1 = ? and user2=?";
		$userSparkCounter = Query_Wrapper::SELECT($sql, array($user1,$user2), Query::$__slave, true);
		if((int)$userSparkCounter['count'] > 0)
			return true;

		return false;
	}

	public static function getUserSparkTxnHistory($user_id){
		$output = array();
		$sql = "select  spt.payment_gateway, group_concat(distinct sp.sku) as packages from spark_packages sp join user_spark_transaction spt on spt.package_id=sp.package_id where spt.user_id = ? group by spt.payment_gateway";
		$result = Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave);
		if(isset($result) && count($result) > 0){
			foreach($result as $row){
				if($row['payment_gateway'] == 'google')
					$output['packages'] = $row['packages'];
				if($row['payment_gateway'] == 'paytm')
					$output['paytm_packages'] = $row['packages'];

			}
			return $output;
		}
		return array();
	}

	public static function getSparksForUser($user_id,$limit = -1){
            $filters = array("male_age","female_age","male_height","female_height","male_bucket","female_bucket");
            $matchingObj = new TwoWayMatching();
            $twoWayMatchSql = $matchingObj->getTwoMatchingSQLString($user_id, $filters,"uss.");
            $score = 1;
            $limitString = ($limit > 0)?" limit ".$limit:"";
                    
		$sql =  "select t1.*, t2.*,"
                        . "case when t2.sparksSuccess is NULL then 0 else t2.sparksSuccess end as sparkRateInverseScore,"
                        . "case when t3.sender_id_2 is NULL then 0 else 1 end as likeScore "
                        . "from "
                        . "(select "
                        . "case when (".$twoWayMatchSql.") then ".$score." else 0 end as twoWayMatchscore,"
                        . "case when uss.country != '113' then ".$score." else 0 end as nriScore,"
                        . "case when uss.isSelect = 1 then ".$score." else 0 end as selectScore,"
                        . "us.user1 as sender_id,"
                        . "us.user2 as user2,"
                        . "us.tstamp as create_date,"
                        . "us.status as status,"
                        . "us.message as message,"
                        . "us.message_id as message_id,"
                        . "us.expired_date as expired_date,"
                        . "now() as current_mysql_time,"
                        . "us.expiry_time as expiry_time,"
                        . "ud.designation as designation,"
                        . "hd.degree_name as highest_degree,"
                        . "gc.display_name as city,"
                        . "u.fname as fname,"
                        . "uss.age as age,"
                        . "uss.height as height,"
                        . "uss.bucket as bucket "
                            . "from user_spark us "
                            . "join user_data ud on ud.user_id = us.user1 "
                            . "join highest_degree hd on ud.highest_degree = hd.id "
                            . "join user u on u.user_id = us.user1 "
                            . "join geo_city gc on gc.city_id = ud.stay_city "
                            . "join user_search uss on uss.user_id=us.user1 "
                            . "join user_photo up on up.user_id=us.user1 and up.is_profile='yes' and up.admin_approved = 'yes' "
                        . "where us.user2 = ? and us.status in ('seen','sent') and (us.expired_date is null or us.expired_date > now()) "
                        . "and u.status not in ('incomplete','non-authentic','blocked','suspended') and u.deletion_status is null "
                        . ") t1 "
                        . "left join "
                        . "(select user1 as sender_id_1,count(*) as sparksSuccess from user_spark where status in ('seen','accepted') group by user1) t2 "
                        . "on t1.sender_id = t2.sender_id_1 "
                        . "left join "
                        . "(select user2 as sender_id_2 from user_like where user1 = ?) t3 "
                        . "on t1.sender_id = t3.sender_id_2 "
                        . "group by t1.sender_id order by nriScore desc, selectScore desc, likeScore desc,twoWayMatchscore desc,sparkRateInverseScore,create_date ". $limitString;

		$param_array = array($user_id, $user_id);
		$result = Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
		return $result;
	}

	public static function updateSpark($user1, $user2, $action="seen")
	{
		if($action=="seen") {
			$sql = "UPDATE user_spark SET status = ?, expired_date=DATE_ADD(now(), INTERVAL `expiry_time` SECOND) WHERE user1 = ? AND user2 = ?";
		}else{
			$sql = "UPDATE user_spark SET status = ? WHERE user1 = ? AND user2 = ?";
		}
		$param_array = array($action, $user1, $user2);

		Query_Wrapper::UPDATE($sql,$param_array,"user_spark");

		if($action == 'accepted'){
			$userAction = new UserActions();
			$userAction->performAction($user1, $user2,'like',0,null,0,true);
			$userAction->performAction($user2, $user1,'like',0,null,0,true);

		}else if($action == 'rejected'){
			$userAction = new UserActions();
			$userAction->performAction($user1, $user2,'like',0,null,0,true);
			$userAction->performAction($user2, $user1,'hide',0,null,0,true);
		}
	}
        
	/*
	 * The function finds out if the user currently  has capability to send out sparks or not.
	 */
	public static function canUserSendOutSparks($user_id) {
            $userSparkCounter = SparkDBO::getUserActiveSparkCount($user_id);
            if($userSparkCounter <= 0) {
                return false;
            }
            return true;
	}


	public static function areUsersSparked($user1, $user2, $status=null){
		$sql = "SELECT * from user_spark where (user1 = ? and user2 = ?) or (user1 = ? and user2 = ?)";
		if($status != null)
			$sql.=" and status = '$status'" ;

		$result = Query_Wrapper::SELECT($sql, array($user1, $user2, $user2, $user1),  Query::$__slave);
		if(isset($result) && $result != null && count($result) > 0){
			return true;
		}
		return false;
	}


	public static function areUsersSetsSparked($user_id, $match_array,$status)
	{
		if(!isset($match_array) || count($match_array) == 0)
			return array();
		$match_array= array_unique($match_array);

		$match_array_count= Query_Wrapper::getParamCount($match_array) ;
		//$match_id_list = implode(',',$match_array);
		$sql = "SELECT if(user1 = ?,user2,user1) as match_id FROM user_spark where ((user1 = ? and user2 in ($match_array_count)) or (user2 = ?  and user1 in ($match_array_count))) " ;
		if($status != null)
			$sql .= " and status = '$status'";

		$param_array = array($user_id, $user_id) ;
		$param_array = array_merge($param_array, $match_array);
		$param_array[] = $user_id ;
		$param_array = array_merge($param_array, $match_array);
		$result = Query_Wrapper::SELECT($sql, $param_array);
		$sparked_array = array();
		foreach($result as $val)
		{
			$sparked_array[$val['match_id']] = true ;
		}
		return $sparked_array ;
	}

	public static function getMd5Hash($user, $param2,$action="spark", $payment_id=null){
		global $md5_salt;
		if($action == "spark")
			return  md5($action."|".$user."|".$param2."|".$md5_salt);	//param2=user2 in this case (spark receiver)
		else if($action == "payment"){
			 return  md5($action."|".$user."|".$param2."|".$payment_id."|".$md5_salt);	//param2 is google sku in this case
		}
	}

	public static function chatAssistMatched($user_id, $chat_assist_users){
		if(!isset($chat_assist_users) || trim($chat_assist_users) == "" || $chat_assist_users == null)
			return false;
		$sql = "SELECT *  FROM messages_queue WHERE ".
			" (sender_id = ? and receiver_id in ($chat_assist_users)) or ".
			" (receiver_id = ? and sender_id in ($chat_assist_users)) ".
			"  and blocked_by is null";
		$result = Query_Wrapper::SELECT($sql, array($user_id, $user_id),  Query::$__slave);
		if(isset($result) && $result != null && count($result) > 0){
			return true;
		}
		return false;
	}

	public static function getActivePackageBuckets($source, $package_type = 'spark'){
		$param_array = array($source,$package_type);
		$sql = "select bucket_name,id as bucket_id, source,status, filter from spark_bucket where status = 'active' and source = ? and type= ?";
		$bucketList =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave);
		return $bucketList;
	}

	public static function hasPreviousTransaction($user_id, $package_type='spark'){
		$param_array = array($user_id);
		if($package_type == 'spark')
			$sql = "select count(transaction_id) as txn_count from user_spark_transaction where user_id = ? and status = 'consumed'";
		else if($package_type == 'select')
			$sql = "select count(user_id) as txn_count from user_subscription where user_id = ?";

		$result =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave, true);
		if((int)$result['txn_count'] <= 0)
			return 0;
		return 1;
	}

	public static function getUsersSparkedByMe($user_id, $limit){
		$result = array();
		$sql = "select user2 from user_spark where user1 = ? and status in ('sent','seen') and (expired_date is null or expired_date > now())";
		if($limit != -1 && $limit > 0)
			$sql .= " limit $limit";
		$res =  Query_Wrapper::SELECT($sql, array($user_id), Query::$__slave);
		if(count($res) > 0){
			foreach($res as $value){
				$result[(int)$value['user2']] = (int)$value['user2'];
			}
		}
		return $result;
	}

	public static function checkIfPackageExistsInBucker($bucket_id, $package_id){
		$param_array = array($bucket_id,$package_id);
		$sql = "select count(*) as pkg_count from spark_bucket_package where bucket_id = ? and package_id = ?";
		$result =  Query_Wrapper::SELECT($sql, $param_array, Query::$__slave, true);
		if((int)$result['pkg_count'] <= 0)
			return false;
		return true;
	}

	//Filter should be an array. not json encoded already
	public static function updateBucketFilter($bucket_id, $filter){
		$param_array = array(json_encode($filter),$bucket_id);
		$sql = "update spark_bucket set filter = ?  where id = ?";
		Query_Wrapper::UPDATE($sql, $param_array, "spark_bucket") ;
	}

	public function getUserSubscriptionType($user_array)
	{
		$param_count = Query_Wrapper::getParamCount($user_array);
		$sql_spark = "Select user_id, spark_count  from user_spark_counter WHERE user_id in ($param_count)";
		$spark_data = Query_Wrapper::SELECT($sql_spark, $user_array);

		$sql_select = "select user_id, if(expiry_date > now(),'active','expired') as select_status, status from user_subscription ".
					   "where subscription_id =1 and user_id in ($param_count)" ;
		$select_data = Query_Wrapper::SELECT($sql_select,$user_array);

		$maxRows = count($spark_data) > count($select_data) ? count($spark_data) : count($select_data) ;
		$subs_array = array();
		for($i =0; $i <$maxRows; $i++ )
		{
			$subs_array[$spark_data[$i]['user_id']]['spark_count'] = $spark_data[$i]['spark_count'] ;
			$subs_array[$select_data[$i]['user_id']]['select_status'] = $select_data[$i]['select_status'];
		}
		return $subs_array;
	}

}
