<?php
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";



/**
 * Mailer admin DBO Object
 * Author:: Sumit
 *  🖖 Live Long and Prosper.
 */

class MailerDBO
{
    public static function getCampaignsListDBO($status, $limit = 50)
    {
        $sql = "SELECT campaign_id, campaign_name,status FROM mailer_campaigns " ;
        $param_array = array();
        if($status != null)
        {
            $sql .= "WHERE status = ?" ;
            $param_array[]= $status;
        }

        $sql .= "order by campaign_id desc limit $limit" ;
        $campaigns = Query_Wrapper::SELECT($sql, $param_array) ;
        return $campaigns;
    }

    public static function getCampaignDBO($campaign_id)
    {
        $sql = "SELECT * FROM mailer_campaigns WHERE campaign_id = ?" ;
        $campaign = Query_Wrapper::SELECT($sql, array($campaign_id), Query::$__slave, true) ;
        return $campaign ;
    }

    public static function saveNewCampaignDBO($campaign_name)
    {
        $table = "mailer_campaigns";
        $sql = "INSERT INTO mailer_campaigns (campaign_name, created_tstamp) VALUES (?,now())" ;
        $campaign_id = Query_Wrapper::INSERT($sql,array($campaign_name), $table) ;
        return $campaign_id ;
    }

    public static function updateCampaign($campaign_id,$status,$data)
    {
        $table = "mailer_campaigns" ;
        $sql = "UPDATE mailer_campaigns SET " ;
        $param_array = array();
        if(isset($data['html']) &&  $data['html'] != null)
        {
            $sql .= "html = ?, " ;
            $param_array[] = $data['html'] ;
        }
        if(isset($data['query']) &&  $data['query'] != null)
        {
            $sql .= "query = ?, " ;
            $param_array[] = $data['query'] ;
        }
        if(isset($data['row_count']) &&  $data['row_count'] != null)
        {
            $sql .= "row_count = ?, " ;
            $param_array[] = $data['row_count'] ;
        }
        if(isset($data['mail_subject']) &&  $data['mail_subject'] != null)
        {
            $sql .= "mail_subject = ?, " ;
            $param_array[] = $data['mail_subject'] ;
        }
        $sql .= "status = ? WHERE campaign_id = ?" ;
        $param_array[] = $status ;
        $param_array[] = $campaign_id ;
        Query_Wrapper::UPDATE($sql, $param_array, $table) ;
    }


    public static function updateCampaignStatus($campaign_id, $status)
    {
        $table = "mailer_campaigns" ;
        $sql = 'UPDATE mailer_campaigns SET status= ? WHERE campaign_id = ?' ;
        Query_Wrapper::UPDATE($sql, array($status, $campaign_id), $table) ;
    }

    public static function increaseSentCounter($campaign_id)
    {
        $table = "mailer_campaigns" ;
        $sql = "UPDATE mailer_campaigns SET sent_to = sent_to+1 WHERE campaign_id = ?" ;
        Query_Wrapper::UPDATE($sql,array($campaign_id),$table);
    }

}

?>
