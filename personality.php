<?php 
require_once (dirname ( __FILE__ ) . '/include/config.php');
require_once (dirname ( __FILE__ ) . '/include/function.php');
require_once (dirname ( __FILE__ ) . '/psycho_registration.php');
require_once (dirname ( __FILE__ ) . '/include/User.php');
require_once (dirname ( __FILE__ ) . "/include/header.php");
require_once (dirname ( __FILE__ ) . '/interesthobbies/Interesthobbies.php');

class Psycho{
	
	private $user_id;
	private $user;
	private $session;
	private $baseurl;
	private $psycho_registration;
	private $steps;
	public function __construct(){
		global $baseurl;
		$this->user = new User();
		$this->session = $this->user->getUserDetailsFromSession();
		$this->user_id=$this->session['user_id'];
		$this->baseurl = $baseurl;
		$this->psycho_registration = new PyschoRegistration();
	}
	
	function getPage($step,$rd=''){
		global $login_mobile;
		if($step=='hobby'){
			global $smarty;
			$this->checkPage($step);
			$interesthobbies = new Interesthobbies($this->user_id);
			$interesthobbies->initializeIH($smarty,$login_mobile,$rd,$this->steps,$_REQUEST['reg']);
			if(!$login_mobile){
				//$smarty->assign('stepCompleted',$this->steps);
				$smarty->display(dirname ( __FILE__ ) .  "/templates/registration/hobby.tpl" );
			}
		}else{
			if($this->checkPage($step)){
				header("Location:".$this->baseurl."/personality.php"); exit;
			}else
				$this->psycho_registration->getRegistrationPage($step,$login_mobile,$rd,$this->steps,$this->user_id,$_REQUEST['reg']);
		}
	}
	
	function saveData($step){
		global $login_mobile;
		$values = $_COOKIE;
		$values['user_id'] = $this->user_id;
		
		if($step=='hobby'){
			$interesthobbies = new Interesthobbies($this->user_id);
			$interesthobbies->saveInterestAndHobbies();
		}else{
			if($this->checkPage($step) && !$login_mobile){
				header("Location:".$this->baseurl."/personality.php"); exit;
			}else{
				try{
					$this->psycho_registration->saveRegistrationPage($values['user_id'],$values["pa"],$step);
					$this->psycho_registration->unRegisterCookie();
				}catch(Exception $e){
					trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
				}
			}
		}
		
		if($login_mobile){
			print_r(json_encode(array("responseCode"=>200)));die;
		}
	}
	
	function checkPage($step){
		$data = $this->user->getUserDetails($this->user_id);
		$stepsCompleted =  $data['steps_completed'];
		$steps = explode ( ",", $stepsCompleted );
		$this->steps = $steps;
		
		if(in_array($step,$steps))
		{
			return true;
		}else
			return false;
	}
	
	function getStepsCompleted(){
		global $smarty,$login_mobile;
		$data = $this->user->getUserDetails($this->user_id);
		$no_of_likes = $this->user->getFbLikesNum($this->user_id);
		$stepsCompleted =  $data['steps_completed'];
		$steps = explode ( ",", $stepsCompleted );
		$this->steps = $steps;
		
		if($login_mobile){
			$response['responseCode'] = 200;
			$response['data'] = array("steps_completed"=>$steps,"no_of_likes"=>$no_of_likes);
			print_r(json_encode($response));die();
		}else{
			/*if(in_array('psycho1',$steps) && in_array('psycho2',$steps)){
				header("Location:".$this->baseurl."/matches.php"); exit;
			}else{*/
				$header = new header($this->user_id);
				$header_values = $header->getHeaderValues();
				$smarty->assign('uid',$this->user_id);
				$smarty->assign('header',$header_values);
				$smarty->assign('stepCompleted',$steps);
				$smarty->assign('no_of_likes',$no_of_likes);
				if(isset($_REQUEST['from_trust']) && $_REQUEST['from_trust'] == true){
					$smarty->assign("isRegistration",'1');
				}
				$smarty->display (dirname ( __FILE__ ) . "/templates/psycho/interstitial.tpl" );
			//}
		}
		
	}
}

$func = new functionClass();
$login_mobile = $func->isMobileLogin();
/*if(!$login_mobile){
	functionClass::redirect ('psycho');
}*/
functionClass::redirect ('psycho',$login_mobile);

$psycho = new Psycho();
if(isset($_REQUEST['rd'])){
	$rd = htmlentities($_REQUEST['rd']);
	echo $rd;
}else
	$rd = '';

if(isset($_REQUEST['step'])){
	if($_REQUEST['step']=='psycho1'){
		$psycho->getPage('psycho1',$rd);
	}else if($_REQUEST['step']=='psycho2'){
		$psycho->getPage('psycho2',$rd);
	}else if($_REQUEST['step']=='hobby'){
		$psycho->getPage('hobby',$rd);
	}
	die();
}

if(isset($_REQUEST['save'])){
	if($_REQUEST['save']=='psycho1'){
		$psycho->saveData('psycho1');
	}else if($_REQUEST['save']=='psycho2'){
		$psycho->saveData('psycho2');
	}else if($_REQUEST['save']=='hobby'){
		$psycho->saveData('hobby');
	}
	die();
}

$psycho->getStepsCompleted();

?>