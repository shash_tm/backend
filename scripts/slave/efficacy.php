<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

$start_date = date ( 'Y-m-d' ,strtotime ( '-1 week' ) );
$start_date1= date ( 'Y-m-d' ,strtotime ( '-8 day' ) );
$active_date= date('Y-m-d',strtotime ('-2 week') );
$today= date('Y-m-d',strtotime('today'));
$today1= date ( 'Y-m-d' ,strtotime ( '-1 day' ) );
function getrev($oneweek,$twoweek,$gen){
	global $smarty, $conn_slave; 
	$sql="SELECT SUM(likes) AS likes, SUM(revmatch) as rmatch, SUM(likedback) as lback, SUM(not_logged_in) as not_log, sum(deac) as deact
FROM (SELECT DISTINCT(ul.user1)AS user_id, COUNT(DISTINCT(ul.user2)) AS  likes,  
COUNT(DISTINCT(url.user1))  AS revmatch , COUNT(DISTINCT(uli.user1)) AS likedback, 
SUM(IF(ul.timestamp>u.last_login && url.user1 IS NULL && uli.user1 IS NULL && u.status='authentic',1,0 )) AS not_logged_in,
SUM(IF(u.status IN ('suspended','blocked'),1,0)) AS deac
FROM user_like ul
LEFT JOIN user_recommendation_log url ON url.user1=ul.user2 AND url.user2=ul.user1
LEFT JOIN user_like uli ON uli.user1=url.user1 AND uli.user2=url.user2
LEFT JOIN user u ON u.user_id=ul.user2 
WHERE u.gender='$gen' AND DATE(ul.timestamp)>='$twoweek' and DATE(ul.timestamp)<'$oneweek'
GROUP BY ul.user1)t";
	var_dump($sql);
	$rs=$conn_slave->Execute($sql);
	//var_dump($rs);
	$res = $rs->fetchRow ();
	$not_pushed=$res['likes']-$res['rmatch']-$res['not_log']-$res['deact'];
	$stat =array();
	$stat['likes']=$res['likes'];
	$stat['rmatch']=round((100*($res['rmatch']/$res['likes'])),2);
	$stat['lback']=round((100*($res['lback']/$res['rmatch'])),2);
	$stat['not_log']=round((100*($res['not_log']/$res['likes'])),2);
	$stat['deact']=round((100*($res['deact']/$res['likes'])),2);
	$stat['not_pushed']=round((100*($not_pushed/$res['likes'])),2);
	return $stat;
}

$mdata1= getrev($start_date, $active_date, F);
$fdata1= getrev($start_date, $active_date, M);
$mdata= getrev($today, $start_date, F);
$fdata= getrev($today, $start_date, M);

$smarty->assign("mdata",$mdata);
$smarty->assign("fdata",$fdata);
$smarty->assign("mdata1",$mdata1);
$smarty->assign("fdata1",$fdata1);

$smarty->assign("twoweek",$active_date);
$smarty->assign("oneweek",$start_date);
$smarty->assign("today",$today);
$smarty->assign("oneweek1",$start_date1);
$smarty->assign("today1",$today1);
$file = dirname ( __FILE__ ) . "/efficacymat.tpl";
try{
	$data = $smarty->fetch ( $file );
}catch (Exception $e){
	echo $e->getMessage();
}
var_dump($data);
//$to = "team@trulymadly.com";
$to="rahul@trulymadly.com,shashwat@trulymadly.com,sumit.kumar@trulymadly.com";
$subject = "Matching Efficacy matrix for $start_date to $today ";
$message = $data;
$from = "admintm@trulymadly.com";
Utils::sendEmail($to, $from, $subject, $message,true);

?>