<?php

require_once dirname ( __FILE__ ) . "/../../GenerateNewMatches.php";
include dirname ( __FILE__ ) . "/../../include/config_admin.php";


/**
 * Cron to generate new match set
 * @Himanshu
 */

function ifCheckpointExists(){
	global $conn_slave;
	$today = date('Y-m-d');
	//2014-02-03
	$rs = $conn_slave->Execute("SELECT * FROM user_search_checkpoint where checkpoint = '$today'");
	$checkpoint = $rs->FetchRow();
	var_dump($checkpoint);
	return $checkpoint;
}
function createDumpAndMarkCheckpoint(){
	echo 'here 	';
	global $conn_slave;
	$table = Utils::getSlaveRecoTableName();
	$fbTable = Utils::getSlavefbConnTableName();

	$file1 = "/tmp/sql_$table.txt";
	$file2 = "/tmp/sql_$fbTable.txt";
	
	//unlink('/tmp/reco_result.txt');

	
	$sql = "SELECT * INTO OUTFILE '$file1'	FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n'	 FROM $table order by user1";
	echo $sql;
	$ex = $conn_slave->Execute($sql);
	
	$sql2 = "SELECT * INTO OUTFILE '$file2'	FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n'	 FROM $fbTable ";
	echo $sql2;
	$conn_slave->Execute($sql2);

	$saveto = "recommendation/sql_$table.txt";
	Utils::saveOnS3($file1, $saveto);
	
	$saveto = "recommendation/sql_$fbTable.txt";
	Utils::saveOnS3($file2, $saveto);
	
	$sql1 = $conn_slave->Execute("INSERT into slave_recommendation_checkpoint (checkpoint) values (now())");

}

function generateReccomendations(){
		echo 'there 	';
	
	global $conn_slave;

	$todayTable = Utils::getSlaveRecoTableName();

	$sql1 = "CREATE TABLE $todayTable like user_recommendation";
	
	/*$sql1 = "CREATE TABLE $todayTable (
  `user1` bigint(20) NOT NULL,
  `user2` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `matchScore` tinyint(3) DEFAULT '0',
  `trust_score` tinyint(3) DEFAULT '0',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total_score` bigint(20) DEFAULT NULL,
  UNIQUE KEY `u1u2` (`user1`,`user2`),
  KEY `user1` (`user1`)
)";*/
	$conn_slave->Execute($sql1);

	$fbTable = Utils::getSlavefbConnTableName();
	$sql3 = "CREATE TABLE $fbTable like  user_facebook_mutual_connections";
	$conn_slave->Execute($sql3);
	/*$sql = "SELECT `user_id` FROM `user` WHERE `status`='authentic' AND `plan` is not null";
	 $ex = $conn_slave->Execute($sql);
	 //$res = $ex->GetRows();

	 $t = microtime(true);

	 if ($ex) {
		while ($val = $ex->FetchRow()) {*/
	//$val['user_id'] =6000100;
	
	
	$sql2 = "SELECT user_id from user where status in ('authentic', 'non-authentic') ";
	$rs = $conn_slave->Execute($sql2);
	
	foreach ($rs as $val){
	echo $val['user_id'];
	$GLOBALS['conn'] = $conn_slave;
	$generate = new generateNewMatches($val['user_id']);
	$res = $generate->generateNewReccomendations(true, true);
	var_dump($res);
	echo PHP_EOL;
	echo $t;

	$t2= microtime(true);
	echo PHP_EOL;
	//	echo $t2;
	echo '	per user time consumption: ' . ($t2-$t);
	$t = $t2;
	}

}

try{
	$t1= microtime(true);

	$flag = ifCheckpointExists();
	echo $flag;
	if(!empty($flag)){
		generateReccomendations();
		createDumpAndMarkCheckpoint();

		$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
				$subject = "Recommendations genereated";
		$from = "admin@trulymadly.com"; 
		Utils::sendEmail($to, $from, $subject, $subject);
	}

	else{
		$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Checkpoint doesn't exist for today's table";
	$message = "User search table not created yet!";
	$from = "admin@trulymadly.com";
	//$headers = "From:" . $from;
	Utils::sendEmail($to, $from, $subject, $message);
		
	}
/*	$saveto = "recommendation/sql_test.txt";
	$file2 = "/tmp/test.txt";
 	Utils::saveOnS3($file2, $saveto);*/

}
catch(Exception $e){
		$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
		$subject = "Recommendation Cron Failure";
	$message = "Cron failed due to " . $e->getMessage();
	$from = "admin@trulymadly.com";
	//$headers = "From:" . $from;
	Utils::sendEmail($to, $from, $subject, $message);
	//mail($to,$subject,$message,$headers);
	echo "Mail Sent.";
	error_log($subject,1,$to,$from);
	trigger_error ( $e->getTraceAsString (), E_USER_WARNING );
}


?>