<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

function forDay()
{
	global  $conn_reporting, $smarty;
	$isttime= date ( 'Y-m-d H:i' ,strtotime ( '+330 minutes' ) );
	
    $end_date_gmt =date('Y-m-d 18:30:00', strtotime('-1 day', strtotime($isttime)));
    $start_date_gmt = date('Y-m-d 18:30:00', strtotime('-2 day', strtotime($isttime)));
    
	$sql="SELECT COUNT(DISTINCT(ugc.device_id)) AS installs FROM user_gcm_current ugc WHERE install_tstamp >= '$start_date_gmt' and install_tstamp < '$end_date_gmt' ";
	$rs = $conn_reporting->Execute ( $sql );
	$install = $rs->FetchRow ();
    

	$sql2="SELECT SUM(IF(u.gender='M',1,0)) AS male , SUM(IF(u.gender='F',1,0)) AS female FROM user u WHERE u.registered_at >= '$start_date_gmt'
	and u.registered_at < '$end_date_gmt'";
	$rs2=$conn_reporting->Execute ( $sql2 );
	$gender = $rs2->FetchRow ();
	
	$install_male_percent= intval(100*($gender['male'] /($gender['male'] +$gender['female'] )));
	$install_female_percent= 100- $install_male_percent;

	$sql3="SELECT COUNT(distinct(ugc.device_id)) as count_uninstall, SUM(IF(u.gender='M',1,0)) AS male, SUM(IF(u.gender='F',1,0)) AS female 
	FROM user_gcm_current ugc LEFT JOIN user u ON ugc.user_id=u.user_id WHERE ugc.status='uninstall'  AND  
	 tstamp >= '$start_date_gmt' and tstamp < '$end_date_gmt' ";
	$rs3=$conn_reporting->Execute ( $sql3 );
	$uninstall = $rs3->FetchRow ();
	 
	
	$uninstall_male_percent= intval(100*($uninstall['male']/($uninstall['male']+$uninstall['female'])));
	$uninstall_female_percent= 100 - $uninstall_male_percent ;
	 
	$sql4="SELECT u.gender, COUNT(*) AS all_installs,  SUM(IF(ugc.status IN ('login','logout','install'),1,0)) AS active
FROM user_gcm_current ugc  LEFT JOIN user u ON u.user_id=ugc.user_id GROUP BY u.gender ";
	$rs4=$conn_reporting->Execute ( $sql4 );
	$install_stats = $rs4->GetRows ();

	$all_installs =0 ;
	$active_installs = 0 ;
	foreach ($install_stats as $row=>$col){
		$all_installs += $col['all_installs'];
		$active_installs += $col['active'];
		if ($col['gender'] == 'M') $active_male = $col['active'];
		if ($col['gender'] == 'F') $active_female = $col['active'];
	}
	$active_percent = intval(100*($active_installs/$all_installs));
	$active_male_percent = intval(100*($active_male/($active_male+$active_female)));
	$active_female_percent = 100 - $active_male_percent;
    
	
	$sql5= "SELECT COUNT(*) AS count_matches FROM user_recommendation_rank_log ur WHERE  tstamp >= '$start_date_gmt' and tstamp < '$end_date_gmt' ";
	$rs5=$conn_reporting->Execute ( $sql5 );
	$matches = $rs5->FetchRow ();
	 
	$sql6= "SELECT COUNT(*) AS count_mutual FROM user_mutual_like uml WHERE  timestamp >= '$start_date_gmt' and timestamp < '$end_date_gmt' ";
	$rs6=$conn_reporting->Execute ( $sql6 );
	$mutual = $rs6->FetchRow ();
	 
	$sql7= "SELECT COUNT(DISTINCT(cmn.msg_id)) AS messages FROM current_messages_new cmn  WHERE  tstamp >= '$start_date_gmt' and 
	tstamp < '$end_date_gmt' ";
	$rs7=$conn_reporting->Execute ( $sql7 );
	$messages = $rs7->FetchRow ();
	 
	$sql8= "SELECT COUNT(*) AS count_active FROM user u  join user_lastlogin ull on ull.user_id=u.user_id WHERE  ull.last_login  >= '$start_date_gmt' and u.registered_at  < '$end_date_gmt' 
	AND (u.status = 'authentic' or u.last_changed_status='authentic') ";
	$rs8=$conn_reporting->Execute ( $sql8 );
	$active = $rs8->FetchRow ();
	 
	//   $sql9= "SELECT DISTINCT(aln.device_id) FROM action_log_new aln
	// WHERE aln.activity='install' AND aln.event_type='install_referrer' AND aln.event_info REGEXP 'Tyroo'
	//AND ADDTIME((tstamp),'05:30:00') >= '2015-02-16 00:00:00' AND ADDTIME((tstamp),'05:30:00') <= '2015-02-17 00:00:00'" ;
	//   $rs9=$conn_reporting->Execute ( $sq9 );
	//   $active = $rs9->FetchRow ();
	  

	 
	$stats= array();
	$stats['duration'] = 'Dai';
	$stats['installs'] = $install['installs'];
	$stats['install_male'] = $install_male_percent;
	$stats['install_female'] = $install_female_percent;
	$stats['uninstalls']= $uninstall['count_uninstall'];
	$stats['uninstall_male'] = $uninstall_male_percent;
	$stats['uninstall_female'] = $uninstall_female_percent ;
	$stats['active_installs'] = $active_installs;
	$stats['active_install_percent'] = $active_percent;
	$stats['active_male'] = $active_male_percent;
	$stats['active_female'] = $active_female_percent;
	$stats['recommendations'] = $matches['count_matches'];
	$stats['matches'] = $mutual['count_mutual'];
	$stats['messages'] = $messages['messages'];
	$stats['active'] = $active['count_active'];
	return $stats;
}

function forWeek() {
	global  $conn_reporting, $smarty;
	$isttime= date ( 'Y-m-d H:i' ,strtotime ( '+330 minutes' ) );
	$end_date_gmt =date('Y-m-d 18:30:00', strtotime('-1 day', strtotime($isttime)));
    $start_date_gmt = date('Y-m-d 18:30:00', strtotime('-8 day', strtotime($isttime)));
    $two_week_gmt = date('Y-m-d 18:30:00', strtotime('-15 day', strtotime($isttime)));
	
	$sql="SELECT COUNT(DISTINCT(ugc.device_id)) AS installs FROM user_gcm_current ugc WHERE install_tstamp >= '$start_date_gmt' and install_tstamp < '$end_date_gmt' ";
	$rs = $conn_reporting->Execute ( $sql );
	$install = $rs->FetchRow ();


	$sql2="SELECT SUM(IF(u.gender='M',1,0)) AS male , SUM(IF(u.gender='F',1,0)) AS female FROM user u WHERE u.registered_at >= '$start_date_gmt'
	and u.registered_at < '$end_date_gmt'";
	$rs2=$conn_reporting->Execute ( $sql2 );
	$gender = $rs2->FetchRow ();

	$install_male_percent= intval(100*($gender['male'] /($gender['male'] +$gender['female'] )));
	$install_female_percent= 100- $install_male_percent;

	$sql3="SELECT COUNT(distinct(ugc.device_id)) as count_uninstall, SUM(IF(u.gender='M',1,0)) AS male, SUM(IF(u.gender='F',1,0)) AS female 
	FROM user_gcm_current ugc LEFT JOIN user u ON ugc.user_id=u.user_id WHERE ugc.status='uninstall'  AND  
	 tstamp >= '$start_date_gmt' and tstamp < '$end_date_gmt' ";
	$rs3=$conn_reporting->Execute ( $sql3 );
	$uninstall = $rs3->FetchRow ();
	 
	
	$uninstall_male_percent= intval(100*($uninstall['male']/($uninstall['male']+$uninstall['female'])));
	$uninstall_female_percent= 100 - $uninstall_male_percent ;
	 
	$sql4="SELECT u.gender, COUNT(*) AS all_installs,  SUM(IF(ugc.status IN ('login','logout','install'),1,0)) AS active
FROM user_gcm_current ugc  LEFT JOIN user u ON u.user_id=ugc.user_id GROUP BY u.gender ";
	$rs4=$conn_reporting->Execute ( $sql4 );
	$install_stats = $rs4->GetRows ();

	$all_installs =0 ;
	$active_installs = 0 ;
	foreach ($install_stats as $row=>$col){
		$all_installs += $col['all_installs'];
		$active_installs += $col['active'];
		if ($col['gender'] == 'M') $active_male = $col['active'];
		if ($col['gender'] == 'F') $active_female = $col['active'];
	}
	$active_percent = intval(100*($active_installs/$all_installs));
	$active_male_percent = intval(100*($active_male/($active_male+$active_female)));
	$active_female_percent = 100 - $active_male_percent;
	 
	
	$sql5= "SELECT COUNT(*) AS count_matches FROM user_recommendation_rank_log ur WHERE tstamp >= '$start_date_gmt' and tstamp < '$end_date_gmt'";
	$rs5=$conn_reporting->Execute ( $sql5 );
	$matches = $rs5->FetchRow ();
	
	$sql51= "SELECT COUNT(*) AS count_matches FROM user_recommendation_rank_log ur WHERE tstamp >= '$two_week_gmt' and tstamp < '$start_date_gmt'";
	$rs51=$conn_reporting->Execute ( $sql51 );
	$matches1 = $rs51->FetchRow ();
	$matches_p = intval ( 100 * ($matches['count_matches'] / $matches1['count_matches']) ); 
	
	$sql6= "SELECT COUNT(*) AS count_mutual FROM user_mutual_like uml WHERE  timestamp >= '$start_date_gmt' and timestamp < '$end_date_gmt' ";
	$rs6=$conn_reporting->Execute ( $sql6 );
	$mutual = $rs6->FetchRow ();
	
	$sql61= "SELECT COUNT(*) AS count_mutual FROM user_mutual_like uml WHERE  timestamp >= '$two_week_gmt' and timestamp < '$start_date_gmt' ";
	$rs61=$conn_reporting->Execute ( $sql61 );
	$mutual1 = $rs61->FetchRow ();
	$mutual_p = intval ( 100 * ($mutual['count_mutual'] / $mutual1['count_mutual']) );
	 
	$sql7= "SELECT COUNT(DISTINCT(cmn.msg_id)) AS messages FROM  current_messages_new cmn WHERE  
	tstamp >= '$start_date_gmt' and tstamp < '$end_date_gmt' ";
	$rs7=$conn_reporting->Execute ( $sql7 );
	$messages = $rs7->FetchRow ();
	
	
	$sql71= "SELECT COUNT(DISTINCT(cmn.msg_id)) AS messages FROM  current_messages_new cmn WHERE  
	tstamp >= '$two_week_gmt' and tstamp < '$start_date_gmt' ";
	$rs71=$conn_reporting->Execute ( $sql71 );
	$messages1 = $rs71->FetchRow ();
	$messages_p = intval ( 100 * ($messages['messages'] / $messages1['messages'] ) );	
	$sql8= "SELECT COUNT(*) AS count_active FROM user u join user_lastlogin ull on ull.user_id=u.user_id WHERE  ull.last_login  >= '$start_date_gmt' and u.registered_at  < '$end_date_gmt'
	AND (u.status = 'authentic' or u.last_changed_status='authentic') ";
	$rs8=$conn_reporting->Execute ( $sql8 );
	$active = $rs8->FetchRow ();

	$sql11 = "select sum(if(gender='f',1,0)) as female,sum(if(gender='M',1,0)) as male  from user u where u.registered_at >= '$start_date_gmt' and "
		."u.registered_at < '$end_date_gmt' and (u.status ='authentic' or last_changed_status ='authentic')";
	$rs11 = $conn_reporting->Execute($sql11) ;
	$authentic = $rs11->FetchRow();

	$sql12 = "select sum(if(gender='f',1,0)) as female,sum(if(gender='M',1,0)) as male  from user u where u.registered_at >= '$two_week_gmt' and "
		."u.registered_at < '$start_date_gmt' and (u.status ='authentic' or last_changed_status ='authentic')";
	$rs12 = $conn_reporting->Execute($sql12) ;
	$authentic1 = $rs12->FetchRow() ;
	$authentic_p = array();
	foreach($authentic as $key =>$auth)
	{
		$authentic_p [$key] = intval( 100 *($authentic[$key] / $authentic1[$key])) ;
	}
	
	$stats= array();
	$stats['duration'] = 'Week';
	$stats['installs'] = $install['installs'];
	$stats['install_male'] = $install_male_percent;
	$stats['install_female'] = $install_female_percent;
	$stats['uninstalls']= $uninstall['count_uninstall'];
	$stats['uninstall_male'] = $uninstall_male_percent;
	$stats['uninstall_female'] = $uninstall_female_percent ;
	$stats['active_installs'] = $active_installs;
	$stats['active_install_percent'] = $active_percent;
	$stats['active_male'] = $active_male_percent;
	$stats['active_female'] = $active_female_percent;
	$stats['recommendations'] = $matches['count_matches'];
	$stats['matches'] = $mutual['count_mutual'];
	$stats['messages'] = $messages['messages'];
	$stats['recommendations_p'] = $matches_p ;
	$stats['matches_p'] = $mutual_p ;
	$stats['messages_p'] = $messages_p ;
	$stats['active'] = $active['count_active'];
	$stats['authentic'] = $authentic ;
	$stats['authentic_p'] = $authentic_p ;
	return $stats; 
}
 
function  forMonth () {
	global  $conn_reporting, $smarty;
	$isttime= date ( 'Y-m-d H:i:s' ,strtotime ( '+330 minutes' ) );
	$today = date ( 'Y-m-d H:i:s',strtotime($isttime) );
	$yesterday= date ( 'Y-m-d H:i:s', strtotime ( '-1 day',strtotime($isttime) ) );
	$thismonth = date ( 'Y-m-01 H:i:s ',strtotime($isttime) );
	$onemonth= date ( 'Y-m-01 H:i:s', strtotime ( '-1 month',strtotime($isttime) ) );
	$twomonth= date ( 'Y-m-01 H:i:s', strtotime ( '-2 month',strtotime($isttime) ) );

	$sql="SELECT COUNT(DISTINCT(ugc.device_id)) AS installs FROM user_gcm_current ugc WHERE install_tstamp >= '$onemonth' and install_tstamp < '$thismonth' ";
	$rs = $conn_reporting->Execute ( $sql );
	$install = $rs->FetchRow ();


	$sql2="SELECT SUM(IF(u.gender='M',1,0)) AS male , SUM(IF(u.gender='F',1,0)) AS female FROM user u WHERE u.registered_at >= '$onemonth'
	and u.registered_at < '$thismonth'";
	$rs2=$conn_reporting->Execute ( $sql2 );
	$gender = $rs2->FetchRow ();

	$install_male_percent= intval(100*($gender['male'] /($gender['male'] +$gender['female'] )));
	$install_female_percent= 100- $install_male_percent;

	$sql3="SELECT COUNT(distinct(ugc.device_id)) as count_uninstall, SUM(IF(u.gender='M',1,0)) AS male, SUM(IF(u.gender='F',1,0)) AS female 
	FROM user_gcm_current ugc LEFT JOIN user u ON ugc.user_id=u.user_id WHERE ugc.status='uninstall'  AND  
	 tstamp >= '$onemonth' and tstamp < '$thismonth' ";
	$rs3=$conn_reporting->Execute ( $sql3 );
	$uninstall = $rs3->FetchRow ();
	 
	
	$uninstall_male_percent= intval(100*($uninstall['male']/($uninstall['male']+$uninstall['female'])));
	$uninstall_female_percent= 100 - $uninstall_male_percent ;
	 
	$sql4="SELECT u.gender, COUNT(*) AS all_installs,  SUM(IF(ugc.status IN ('login','logout','install'),1,0)) AS active
FROM user_gcm_current ugc  LEFT JOIN user u ON u.user_id=ugc.user_id GROUP BY u.gender ";
	$rs4=$conn_reporting->Execute ( $sql4 );
	$install_stats = $rs4->GetRows ();

	$all_installs =0 ;
	$active_installs = 0 ;
	foreach ($install_stats as $row=>$col){
		$all_installs += $col['all_installs'];
		$active_installs += $col['active'];
		if ($col['gender'] == 'M') $active_male = $col['active'];
		if ($col['gender'] == 'F') $active_female = $col['active'];
	}
	$active_percent = intval(100*($active_installs/$all_installs));
	$active_male_percent = intval(100*($active_male/($active_male+$active_female)));
	$active_female_percent = 100 - $active_male_percent;
	 
	
	$sql5= "SELECT COUNT(*) AS count_matches FROM user_recommendation_rank_log ur WHERE tstamp >= '$onemonth' and tstamp < '$thismonth'";
	$rs5=$conn_reporting->Execute ( $sql5 );
	$matches = $rs5->FetchRow ();
	
	$sql51= "SELECT COUNT(*) AS count_matches FROM user_recommendation_rank_log ur WHERE tstamp >= '$twomonth' and tstamp < '$onemonth'";
	$rs51=$conn_reporting->Execute ( $sql51 );
	$matches1 = $rs51->FetchRow ();
	$matches_p = intval ( 100 * ($matches['count_matches'] / $matches1['count_matches']) ); 
	
	$sql6= "SELECT COUNT(*) AS count_mutual FROM user_mutual_like uml WHERE  timestamp >= '$onemonth' and timestamp < '$thismonth' ";
	$rs6=$conn_reporting->Execute ( $sql6 );
	$mutual = $rs6->FetchRow ();
	
	$sql61= "SELECT COUNT(*) AS count_mutual FROM user_mutual_like uml WHERE  timestamp >= '$twomonth' and timestamp < '$onemonth' ";
	$rs61=$conn_reporting->Execute ( $sql61 );
	$mutual1 = $rs61->FetchRow ();
	$mutual_p = intval ( 100 * ($mutual['count_mutual'] / $mutual1['count_mutual']) );
	 
	$sql7= "SELECT COUNT(DISTINCT(cmn.msg_id)) AS messages FROM  current_messages_new cmn WHERE  
	tstamp >= '$onemonth' and tstamp < '$thismonth' ";
	$rs7=$conn_reporting->Execute ( $sql7 );
	$messages = $rs7->FetchRow ();
	
	
	$sql71= "SELECT COUNT(DISTINCT(cmn.msg_id)) AS messages FROM  current_messages_new cmn WHERE  
	tstamp >= '$twomonth' and tstamp < '$onemonth' ";
	$rs71=$conn_reporting->Execute ( $sql71 );
	$messages1 = $rs71->FetchRow ();
	$messages_p = intval ( 100 * ($messages['messages'] / $messages1['messages'] ) );

	$sql11 = "select sum(if(gender='f',1,0)) as female,sum(if(gender='M',1,0)) as male  from user u where u.registered_at >= '$onemonth' and "
		."u.registered_at < '$thismonth' and (u.status ='authentic' or last_changed_status ='authentic')";
	$rs11 = $conn_reporting->Execute($sql11) ;
	$authentic = $rs11->FetchRow();

	$sql12 = "select sum(if(gender='f',1,0)) as female,sum(if(gender='M',1,0)) as male  from user u where u.registered_at >= '$twomonth' and "
		."u.registered_at < '$onemonth' and (u.status ='authentic' or last_changed_status ='authentic')";
	$rs12 = $conn_reporting->Execute($sql12) ;
	$authentic1 = $rs12->FetchRow() ;
	$authentic_p = array();
	foreach($authentic as $key =>$auth)
	{
		$authentic_p [$key] = intval( 100 *($authentic[$key] / $authentic1[$key])) ;
	}

	//var_dump($authentic_p); var_dump($authentic); var_dump($authentic1);

	$sql8= "SELECT COUNT(*) AS count_active FROM user u join user_lastlogin ull on ull.user_id=u.user_id WHERE 
	 ull.last_login  >= '$onemonth' and u.registered_at  < '$thismonth'
	AND (u.status = 'authentic' or u.last_changed_status='authentic') ";
	$rs8=$conn_reporting->Execute ( $sql8 );
	$active = $rs8->FetchRow (); 
	
	$sql9= "SELECT COUNT(*) AS count_all, SUM(IF(t.age<=23,1,0)) AS age_one, SUM(IF(t.age<=27 && t.age >=24 ,1,0))  AS age_two, 
	SUM(IF(t.age>=28,1,0)) AS age_three FROM (SELECT u.user_id,EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) AS age 
	FROM user u JOIN user_data ud ON u.user_id=ud.user_id WHERE u.registered_at >= '$onemonth' AND u.registered_at < '$thismonth' )t";
	$rs9=$conn_reporting->Execute ( $sql9 );
	$age = $rs9->FetchRow ();
	
	$sql10= "SELECT gc.name AS city, COUNT(u.user_id)  AS count_u FROM user u JOIN user_data ud ON u.user_id=ud.user_id 
	JOIN geo_city gc ON gc.city_id=ud.stay_city WHERE u.registered_at >= '$onemonth' and u.registered_at < '$thismonth'
	 GROUP BY gc.name ORDER BY count_u DESC "; 
	$rs10=$conn_reporting->Execute ( $sql10 );
	$city_arr = $rs10->GetRows ();
    $city= array ();
    $city[11]['count'] = 0;
    $city[12]['count'] = 0;
    $i=0; 
    $ncr = array ('Faridabad','Gurgaon','Delhi','Noida','Greater Noida','Ghaziabad');
	foreach ($city_arr as $row => $col){
	if(in_array($col['city'], $ncr)){
		$city[12]['city']=  'NCR';
		$city[12]['count'] += $col['count_u'];
	}
    else if ($i < 10){
    	           $city[$i]['city']=  $col['city'];
    	          $city[$i]['count']= $col['count_u'];
    	          $i++;
                 } 
    else {
    	     $city[11]['city']= 'Others';
    	     $city[11]['count'] += $col['count_u'];     
    }
	}



	$stats= array();
	$stats['duration'] = 'Month';
	$stats['installs'] = $install['installs'];
	$stats['install_male'] = $install_male_percent;
	$stats['install_female'] = $install_female_percent;
	$stats['uninstalls']= $uninstall['count_uninstall'];
	$stats['uninstall_male'] = $uninstall_male_percent;
	$stats['uninstall_female'] = $uninstall_female_percent ;
	$stats['active_installs'] = $active_installs;
	$stats['active_install_percent'] = $active_percent;
	$stats['active_male'] = $active_male_percent;
	$stats['active_female'] = $active_female_percent;
	$stats['recommendations'] = $matches['count_matches'];
	$stats['matches'] = $mutual['count_mutual'];
	$stats['messages'] = $messages['messages'];
	$stats['recommendations_p'] = $matches_p ;
	$stats['matches_p'] = $mutual_p ;
	$stats['messages_p'] = $messages_p ;
	$stats['active'] = $active['count_active'];
	$stats['age'] = $age;
	$stats['city'] = $city;
	$stats['authentic'] = $authentic ;
	$stats['authentic_p'] = $authentic_p ;
	return $stats;
}

$options=getopt ( "d:" );

if ($options[d]){
	$dur = $options[d];
}
//$dur= 'month';
//$isttime = '2014-11-30 00:00:00';
$day= date ( 'w' ,strtotime ('today'));
//if ( date ( 't' ,strtotime ($isttime)) == date ( 'd' ,strtotime ($isttime)) ){
if ( $dur == 'month'){
	echo "Monthly";
	$result= forMonth();
	//var_dump($result);
	
}
else if ($dur == 'week' ){
	echo "Weekly";
	$result= forWeek();
	//var_dump($result);
} 
else 
{ 
	echo "Daily";
$result= forDay();
//var_dump($result);
}

$smarty->assign("result",$result);

$file = dirname ( __FILE__ ) . "/dashboard_new.tpl";
try{
	$data = $smarty->fetch ( $file );
}catch (Exception $e){
	echo $e->getMessage();
} 

$to = "sumit@trulymadly.com, hitesh@trulymadly.com, rahul@trulymadly.com, sachin@trulymadly.com, shashwat@trulymadly.com, snehil@trulymadly.com, gaurav@trulymadly.com, "
      ."saumya@trulymadly.com, maria@trulymadly.com,shubhanshi@trulymadly.com ";
 	//$to="sumit.kumar@trulymadly.com";

// $to = "sumit@trulymadly.com";
$subject = $result['duration'].'ly Dashboard';
$message = $data;
$from = "admintm@trulymadly.com";
Utils::sendEmail($to, $from, $subject, $message,true);

//echo $data;
?>