<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

$today= date('Y-m-d 00:00:00',strtotime('today'));
$yesterday= date ( 'Y-m-d 00:00:00' ,strtotime ( '-1 day' ) ); 

$now = date('Y-m-d H:i:s'); 
$hour = date ( 'Y-m-d H:i:s' ,strtotime ( '-1 hour' ) ); 

$options=getopt ( "d:" );
if ($options[d])
{
	$dur = $options[d];
}



function phone_data ($start, $end)
{
	global $conn_reporting, $smarty;
	$sql="SELECT DISTINCT(upn.status) as status, COUNT(upn.user_id) AS  count_l FROM user_phone_number upn WHERE 
	upload_time >= '$start' and upload_time <'$end' GROUP BY upn.status";
	$rs = $conn_reporting->Execute ( $sql );
	$phone_arr = $rs->GetRows ();
	//var_dump($phone_arr);
   $success_arr = array ();
   $failure_arr = array ();
	foreach ($phone_arr as $row=>$col){
		if ( $col['status'] == 'active' ) {
			$success_arr =  $col;
		} else {
			$failure_arr[] = $col;
		} 
	$smarty->assign("success", $success_arr);
	$smarty->assign("fail", $failure_arr);
	$file = dirname ( __FILE__ ) . "/phoneNumber.tpl";
	$data = $smarty->fetch ( $file );	
	}
 return $data;
}
try {

	if ( $dur == 'hour')
	{
		$data = "For Hour since : $hour" ;
		$data .= phone_data ($hour, $now) ;	
		$data .= "For Whole day since:".$today ;
		$data .= phone_data($today, $now) ;
		$hourtime= date ( 'Y-m-d  H:i' ,strtotime ( '-1 hour' ) );
	    $subject = 'Hourly Phone number verification statistics for '.$hourtime;
	} 
	else 
	{
		$data = "Whole Day" ;
		$data .= phone_data($yesterday, $today) ;
		$yesterday= date ( 'Y-m-d' ,strtotime ( '-1 day' ) );
	    $subject = 'Phone number verification statistics for '.$yesterday;
	}
	
	$to=  'sumit@trulymadly.com, shashwat@trulymadly.com, rahul@trulymadly.com';
	//$to = 'sumit@trulymadly.com' ;
	$from = 'admintm@trulymadly.com'; 
	$message = $data;
	Utils::sendEmail($to, $from, $subject, $message,true);
} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
	Utils::sendEmail('sumit@trulymadly.com', 'sumit@trulymadly.com', 'Reporting Alert', 'Phone Number Reporting failed',true);
}



?>