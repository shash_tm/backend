<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Matching Efficacy</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h2>
Matching Efficacy for {$oneweek} to {$today1}
</h2>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>
</td>
<th> No of Likes
</th>
   <th>
       % Pushed to match
    </th>
   <th>
        % liked Back from pushed to matches
   </th>
   <th>
        % Not logged In
    </th>
   <th>
   % Profile suspended/blocked
</th>
<th>
   % Not Pushed
</th>
</tr>
 <tr>
 <th>Male users</th>
  <td>
   {$mdata.likes}
  </td>
  <td>
    {$mdata.rmatch}%
  </td>
  <td>
   {$mdata.lback}%
  </td>
  <td>
   {$mdata.not_log}%
  </td>
  <td>
  {$mdata.deact}%
  </td>
  <td>
  {$mdata.not_pushed}%
  </td>
  </tr>
  <tr>
 <th>Female users</th>
  <td>
   {$fdata.likes}
  </td>
  <td>
    {$fdata.rmatch}%
  </td>
  <td>
   {$fdata.lback}%
  </td>
  <td>
   {$fdata.not_log}%
  </td>
  <td>
  {$fdata.deact}%
  </td>
   <td>
  {$fdata.not_pushed}%
  </td>
  </tr>
  </table>
  <h2>
Matching Efficacy for {$twoweek} to {$oneweek1} 
</h2>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>
</td>
<th> No of Likes
</th>
   <th>
       % Pushed to match
    </th>
   <th>
        % liked Back from pushed to matches
   </th>
   <th>
        % Not logged In
    </th>
   <th>
   % Profile suspended/blocked
</th>
<th>
   % Not Pushed
</th>
</tr>
 <tr>
 <th>Male users</th>
  <td>
   {$mdata1.likes}
  </td>
  <td>
    {$mdata1.rmatch}%
  </td>
  <td>
   {$mdata1.lback}%
  </td>
  <td>
   {$mdata1.not_log}%
  </td>
  <td>
  {$mdata1.deact}%
  </td>
   <td>
  {$mdata1.not_pushed}%
  </td>
  </tr>
  <tr>
 <th>Female users</th>
  <td>
   {$fdata1.likes}
  </td>
  <td>
    {$fdata1.rmatch}%
  </td>
  <td>
   {$fdata1.lback}%
  </td>
  <td>
   {$fdata1.not_log}%
  </td>
  <td>
  {$fdata1.deact}%
  </td>
   <td>
  {$fdata1.not_pushed}%
  </td>
  </tr>
  </table>
  </body>
</html>
 