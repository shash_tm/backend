<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Daily Tracker</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<h3> {$source} </h3>

<h3> Installs </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600" >
<tr>
<th> Platform</th>
<th> Installs</th>
<th> Reinstalls same device </th>
<th> Reinstalls new device </th>
<th> Fresh Installs </th>
<th> Registration </th>
</tr>
{foreach  from=$installs_arr key=k item=v}
<tr>
<th> {$k} </th>
<td> {$v.install} </td>
<td> {$v.reinst} </td>
<td> {$v.new_device} </td>
<td> {$v.install - $v.reinst - $v.new_device } </td>
<td> {$v.reg} </td> 
</tr>
{/foreach} 
</table>

<br>

<h4>Facebook Errors </h4>
<br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
 
 <tr>
<th width="200" align="center">Platform </th>
<td width="200" align="center">Age limit errors</td>
<td width="200" align="center">Married errors</td>
<td width="200" align="center">Too few connections</td>
<td align="center"> Others </td>
</tr>
{foreach  from=$fb_errors key=k item=v}
<tr>
<th width="200" align="center">{$k}</th>
<td width="200" align="center">{$v.age_limit}</td>
<td width="200" align="center">{$v.married_error}</td>
<td width="200" align="center">{$v.too_few}</td>
<td align="center">{$v.Other}</td>
</tr> 
{/foreach}
</table>

<br>
<h4>Registrations </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">

<tr>
<th>Source </th>
<th>Gender </th>
<th>Registration Starts </th>
<th>Completed </th>
<th>% Completed </th>
<th>Authentic </th>
<th>% Authentic </th>
</tr>
{foreach from=$registrations item=v}
    <tr>
        <th align="center">{$v.registered_from}</th>
        <td align="center">{$v.gender}</td>
        <td align="center">{$v.reg_start}</td>
        <td align="center">{$v.complete}</td>
        <td align="center">{$v.complete_perc} %</td>
        <td align="center">{$v.authentic}</td>
        <td align="center">{$v.authentic_perc} %</td>
    </tr>
{/foreach}
</table>
<br>

{if $city_m neq null}
<h5> Cities Male </h5>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="1000">
<tr>
<th>City</th>
{foreach from=$city_m item=v} 
<td> {$v.city_grp} </td>
{/foreach}
</tr>
<tr> 
<th>Numbers </th>
{foreach from=$city_m item=v}
<td> {$v.c_u}  ( {$v.perc} % ) </td>
{/foreach}
</tr>
</table>
<h5> Cities Female </h5>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="1000">
<tr>
<th>City</th>
{foreach from=$city_f item=v}
<td> {$v.city_grp} </td>
{/foreach}
</tr>
<tr>
<th>Numbers </th>
{foreach from=$city_f item=v} 
<td> {$v.c_u}  ( {$v.perc} % ) </td> 
{/foreach}
</tr>
</table>
<h5> Age groups </h5>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="500">
<tr>
<th>Gender</th>
<th>  18-21  </th>
<th>  22-24  </th>
<th>  25-28  </th>
<th>  29-32  </th>
<th>  32 +  </th>
</tr>

{foreach from=$age item=v} 
<tr>
<th>{$v.gender}</th> 
<td>{$v.age_18_p}  % ( {$v.age_18}  ) </td>
<td>{$v.age_22_p}  % ( {$v.age_22}  ) </td>
<td>{$v.age_25_p}  % ( {$v.age_25}  ) </td>
<td>{$v.age_29_p}  % ( {$v.age_29}  ) </td>
<td>{$v.age_33_p}  % ( {$v.age_33}  ) </td>
</tr>
{/foreach}
</table>
{/if}

</body>
</html>