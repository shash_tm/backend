<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Engagement Tracker</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<h2>
Engagement Matrix for {$oneweek} to {$today1}
</h2>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>

</td>
<th>Active Users who liked or hid at least one profile
</th>
   <th>
       % Liked at least one profile
    </th>
   <th>
        %Liked 1- 20% of Profiles
   </th>
   <th>
        %Liked  20- 50% of Profiles
    </th>
   <th>
   % Liked 50-100% of profiles
</th>
 <th>
   Got Likeback
</th>
<th>
   Exchanged Messages
</th>
  
</tr>
 <tr>
<td>Male users</td>
  <td>
   {$mdata.like_or}
  </td>
  <td>
    {$mdata.one_like}%
  </td>
  <td>
   {$mdata.twe_like}%
  </td>
  <td>
   {$mdata.fif_like}%
  </td>
  <td>
  {$mdata.hun_like}%
  </td>
  
  <td>
  {$mdata.like_back} ({$mdata.like_backp} %)
  </td>
  <td>
  {$mdata.msg_back} ({$mdata.msg_backp} %)
  </td> 
  
  <tr>
<td>Female users</td>
  <td>
   {$fdata.like_or}
  </td>
  <td>
    {$fdata.one_like}%
  </td>
  <td>
   {$fdata.twe_like}%
  </td>
  <td>
   {$fdata.fif_like}%
  </td>
  <td>
  {$fdata.hun_like}%
  </td>
  
  <td>
  {$fdata.like_back} ({$fdata.like_backp} %)
  </td>
  <td>
  {$fdata.msg_back} ({$fdata.msg_backp} %)
  </td> 
</tr>
</table>


<h2>
Reverse Engagement Matrix for {$oneweek} to {$today1}
</h2>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>

</td>
   <th>
       % Liked by at least one profile
    </th>
   <th>
        %Liked by 1- 20% of Profiles
   </th>
   <th>
        %Liked by 20- 50% of Profiles
    </th>
   <th>
     % Liked by 50-100% of profiles
</th>
 </tr>
 <tr>
<td>Male users</td>
  
  <td>
    {$mrevdata.like_only_p}%
  </td>
  <td>
   {$mrevdata.twe_like}%
  </td>
  <td>
   {$mrevdata.fif_like}%
  </td>
  <td>
    {$mrevdata.hun_like}%
  </td>
  
  <tr>
<td>Female users</td>
  
  <td>
    {$frevdata.like_only_p}%
  </td>
  <td>
   {$frevdata.twe_like}%
  </td>
  <td>
   {$frevdata.fif_like}%
  </td>
  <td>
    {$frevdata.hun_like}%
  </td>
  </tr>
</table>



<h2>
Engagement Matrix for {$twoweek} to {$oneweek1} 
</h2>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>
</td>
<th>Active Users who liked or hid at least one profile
</th>
   <th>
       % Liked at least one profile
    </th>
   <th>
        %Liked 1- 20% of Profiles
   </th>
   <th>
        %Liked  20- 50% of Profiles
    </th>
   <th>
   % Liked 50-100% of profiles
</th>
 <th>
   Got Likeback
</th>
<th>
   Exchanged Messages
</th>  
</tr>
 <tr>
<td>Male users</td>
  <td>
   {$mdata1.like_or}
  </td>
  <td>
    {$mdata1.one_like}%
  </td>
  <td>
   {$mdata1.twe_like}%
  </td>
  <td>
   {$mdata1.fif_like}%
  </td>
  <td>
  {$mdata1.hun_like}%
  </td>
  
  <td>
  {$mdata1.like_back} ({$mdata1.like_backp} %)
  </td>
  <td>
  {$mdata1.msg_back} ({$mdata.msg_backp} %)
  </td> 
  
  <tr>
<td>Female users</td>
  <td>
   {$fdata1.like_or}
  </td>
  <td>
    {$fdata1.one_like}%
  </td>
  <td>
   {$fdata1.twe_like}%
  </td>
  <td>
   {$fdata1.fif_like}%
  </td>
  <td>
  {$fdata1.hun_like}%
  </td>
  <td>
  {$fdata1.like_back} ({$fdata1.like_backp} %)
  </td>
  <td>
  {$fdata1.msg_back} ({$fdata1.msg_backp} %)
  </td> 
</tr>
  
</table>

<!-- Replica -->

<h2>
Reverse Engagement Matrix for {$twoweek} to {$oneweek1} 
</h2>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<td>
</td>
   <th>
       % Liked by at least one profile
    </th>
   <th>
        %Liked by 1- 20% of Profiles
   </th>
   <th>
        %Liked by  20- 50% of Profiles
    </th>
   <th>
     % Liked by 50-100% of profiles
</th>
 </tr>
 <tr>
<td>Male users</td>
  <td>
    {$mrevdata1.like_only_p}%
  </td>
  <td>
   {$mrevdata1.twe_like}%
  </td>
  <td>
   {$mrevdata1.fif_like}%
  </td>
  <td>
    {$mrevdata1.hun_like}%
  </td>
  
  <tr>
<td>Female users</td>
  <td>
    {$frevdata1.like_only_p}%
  </td>
  <td>
   {$frevdata1.twe_like}%
  </td>
  <td>
   {$frevdata1.fif_like}%
  </td> 
  <td>
    {$frevdata1.hun_like}%
  </td>
  </tr>
  
</table>




</body>
</html>