<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../analytics/us_report.php";

$isttime= date ( 'Y-m-d H:i' ,strtotime ( '+330 minutes' ) );
$start_date = date ( 'Y-m-d' ,strtotime ( '-1 hour',strtotime($isttime)) );
$end_date=date('Y-m-d H:i', strtotime('+1 day', strtotime($isttime)));
$date_hour_start = date ( 'Y-m-d H:i', strtotime ( '-1 hour',strtotime($isttime) ) );
$date_hour_end = date ( 'Y-m-d H:i',strtotime($isttime) );
$date_mail = date ( 'Y-m-d',strtotime($isttime) );
$hour = date ( 'H',strtotime($isttime) );
    
var_dump($hour);  
//$isttime= date ( 'Y-m-d H:i:s' ,strtotime ( '+330 minutes' ) );
$end_date_gmt = date ( 'Y-m-d 18:30:00' ,strtotime ( $isttime ) );
$start_date_gmt=date('Y-m-d 18:30:00', strtotime('-1 day', strtotime($isttime)));
$two_day_gmt = date('Y-m-d 18:30:00', strtotime('-2 day', strtotime($isttime)));
$start_hour_gmt= date('Y-m-d H:i:s', strtotime( '-60 minutes'));
$end_hour_gmt= date('Y-m-d H:i:s');
$yesterday = date ( 'Y-m-d' ,strtotime ( '-1 day',strtotime($isttime)) );

class RegistrationTracker
{
	private $conn_reporting;
	private $smarty ;

	function __construct($day_start, $day_end)
	{
		global $conn_reporting, $smarty;
		$this->day_start = $day_start ;
		$this->day_end = $day_end ;
		$this->conn_reporting = $conn_reporting;
		$this->smarty = $smarty;
	}

	public function getCompleteData()
	{
		$this->getInstalls();
		$this->getRegistrations();
		$this->getFBErrors();
		$file = dirname ( __FILE__ ) . "/tracker.tpl";
		$data = $this->smarty->fetch ( $file );
		return $data;
	}

	public function getAggregatedData()
	{
		$sql_city_m ="select if(pos<10 ,city,'Others') as city_grp, sum(users) as c_u from
(select city, users, @row := @row + 1 AS pos from
(select case when gc.name = 'Delhi' || gc.name ='Faridabad' || gc.name ='Gurgaon' || gc.name ='Noida' ||
gc.name ='Greater Noida' || gc.name ='Ghaziabad' then 'NCR' else case when gc.name is not null then gc.name else
gs.name end  end  as city, count(u.user_id) as users from user u
		 join user_data ud on u.user_id=ud.user_id
		 left join geo_city gc on gc.city_id=ud.stay_city and gc.state_id= ud.stay_state
		 left join geo_state gs on gs.state_id=ud.stay_state
		 where gender='M' and registered_at >='$this->day_start' and  registered_at <'$this->day_end'
		 group by city order by users desc ) t  JOIN    (SELECT @row := 0) r  )s group by city_grp order by c_u desc ";

		$res_city_m= $this->conn_reporting->Execute($sql_city_m);
		$city_m = $res_city_m->GetRows ();
		$city_m_arr = array();
		$sum_city_male= 0;
		foreach ($city_m as $val){
			$sum_city_male += $val['c_u'];
		}
		$i=0;
		foreach ($city_m as $val) {
			$city_m_arr[$i]= $val;
			$city_m_arr[$i]['perc'] = intval ( 100 * ($val['c_u'] / $sum_city_male) );
			$i++;
		}


		$sql_city_f ="select if(pos<10 ,city,'Others') as city_grp, sum(users) as c_u from
(select city, users, @row := @row + 1 AS pos from
(select case when gc.name = 'Delhi' || gc.name ='Faridabad' || gc.name ='Gurgaon' || gc.name ='Noida' ||
gc.name ='Greater Noida' || gc.name ='Ghaziabad' then 'NCR' else case when gc.name is not null then gc.name else
gs.name end  end  as city, count(u.user_id) as users from user u
		 join user_data ud on u.user_id=ud.user_id
		 left join geo_city gc on gc.city_id=ud.stay_city and gc.state_id= ud.stay_state
		 left join geo_state gs on gs.state_id=ud.stay_state
		 left join device_referrer dr on u.device_id = dr.device_id
		 where gender='F' and registered_at >='$this->day_start' and  registered_at <'$this->day_end' and dr.device_id is null
		 group by city order by users desc ) t  JOIN    (SELECT @row := 0) r  )s group by city_grp order by c_u desc ";

		$res_city_f= $this->conn_reporting->Execute($sql_city_f);
		$city_f = $res_city_f->GetRows ();
		$city_f_arr = array();
		$sum_city_female= 0;
		foreach ($city_f as $val){
			$sum_city_female += $val['c_u'];
		}
		$i=0;
		foreach ($city_f as $val) {
			$city_f_arr[$i]= $val;
			$city_f_arr[$i]['perc'] = intval ( 100 * ($val['c_u'] / $sum_city_female) );
			$i++;
		}
		$sql_age= "select gender, COUNT(*) AS count_all, SUM(IF(t.age<=21,1,0)) AS age_18, SUM(IF(t.age<=24 && t.age >=22 ,1,0))  AS age_22,
		 SUM(IF(t.age>=25 && t.age <=28,1,0)) AS age_25, SUM(IF(t.age>=29 && t.age <=32,1,0)) AS age_29 , SUM(IF(t.age>32,1,0)) AS age_33,
		 CAST(100*( SUM(IF(t.age<=21,1,0)) /COUNT(*) ) AS unsigned) as age_18_p,
		 CAST(100*( SUM(IF(t.age<=24 && t.age >=22 ,1,0)) /COUNT(*) ) AS unsigned) as age_22_p,
		 CAST(100*( SUM(IF(t.age>=25 && t.age <=28,1,0)) /COUNT(*) ) AS unsigned) as age_25_p,
		 CAST(100*( SUM(IF(t.age>=29 && t.age <=32,1,0)) /COUNT(*) ) AS unsigned) as age_29_p,
		 CAST(100*( SUM(IF(t.age>32,1,0)) /COUNT(*) ) AS unsigned) as age_33_p FROM
                   ( SELECT u.user_id,gender, EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) AS age
                   FROM user u JOIN user_data ud ON u.user_id=ud.user_id
                   WHERE registered_at >='$this->day_start' and  registered_at <'$this->day_end' )t group by gender";
		$res_age= $this->conn_reporting->Execute($sql_age);
		$age_arr = $res_age->GetRows ();
		$this->smarty->assign ( "city_m", $city_m_arr );
		$this->smarty->assign ( "city_f", $city_f_arr );
		$this->smarty->assign ( "age", $age_arr );
	}

	private function getFBErrors()
	{
		$sql_fb_error = "select  source, case when aln.event_status regexp 'error403 - Our maximum age' || aln.event_status regexp 'error403 - Our minimum age'
		then 'age_limit' else (case when aln.event_status regexp 'Aha! Looks like' then 'married_error' else (case when aln.event_status regexp 'error403 - Oho! You don'
		then 'too_few' else 'Other' end ) end) end  as Error, count(distinct(device_id)) as counts from action_log_new aln  where tstamp >= '$this->day_start' and
    	tstamp < '$this->day_end' and  aln.activity in ('login','signup') and  aln.event_type in ('fb_server_call') and event_status != 'success'
	    and event_status is not null and admin_id is null  group by source, Error";

		$fb_error_arr = array();
		$res_fb_error = $this->conn_reporting->Execute($sql_fb_error);
		if($res_fb_error->RowCount()>0){
			while ($row = $res_fb_error->FetchRow()){
				$fb_error_arr[$row['source']][$row['Error']]= $row['counts'];
			}
		}
		$this->smarty->assign ( "fb_errors", $fb_error_arr );
	}

	private function getInstalls()
	{
		$sql_install = "select if(country_id = 254,country_id,113) as country, if(source is null,'androidApp',source) as platform, count(distinct(ugc.device_id)) as c_u from user_gcm_current  ugc
	   where install_tstamp >='$this->day_start' and install_tstamp <'$this->day_end' group by platform, country" ;

		$inst = $this->conn_reporting->Execute ( $sql_install );
		$install_arr = array();
		while ($row = $inst->FetchRow())
		{
			$row = $this->processRow($row);
			if($row['country'] == 254)
				$row['platform']  .= ' US';
			$install_arr[$row['platform']]['install'] = $row['c_u'];
		}

		$sql_reinstall = "select if(country_id = 254,country_id,113) as country, if(source is null,'androidApp',source) as platform, count(distinct(ugc.device_id)) as c_u  from user_gcm_current  ugc
					  join user u on u.device_id= ugc.device_id
					  where install_tstamp >= '$this->day_start'  and install_tstamp < '$this->day_end' and
					   u.registered_at < '$this->day_start' group by platform, country" ;
		$reinst = $this->conn_reporting->Execute ( $sql_reinstall );

		while ($row = $reinst->FetchRow() )
		{
			$row = $this->processRow($row);
			if($row['country'] == 254)
				$row['platform']  .= ' US';
			$install_arr[$row['platform']]['reinst'] = $row ['c_u'];
		}

		$sql_new_device = "select if(country_id = 254,country_id,113) as country,if(source is null,'androidApp',source) as platform, count(distinct(ugc.device_id)) as c_u from user_gcm_current ugc
						join user u on u.user_id= ugc.user_id
						where install_tstamp >'$this->day_start' and install_tstamp < '$this->day_end' and
						u.registered_at < '$this->day_start' and ugc.device_id != u.device_id  group by platform, country" ;

		$new_device = $this->conn_reporting->Execute ($sql_new_device);

		while ($row = $new_device->FetchRow() )
		{
			$row = $this->processRow($row);
			if($row['country'] == 254)
				$row['platform']  .= ' US';
			$install_arr[$row['platform']]['new_device'] = $row ['c_u'];
		}

		$sql_reg = "SELECT if(ud.stay_country = 254,254,113) as country,registered_from, count(*) as reg_start
	  				FROM user u left join user_data ud on u.user_id = ud.user_id WHERE registered_at>='$this->day_start' and  registered_at<'$this->day_end'
				 group by registered_from, country";
		$all_reg = $this->conn_reporting->Execute ($sql_reg);
		$reg_array = array();
		while($row = $all_reg->FetchRow())
		{
			if($row['country'] == 254)
				$row['registered_from']  .= ' US';
			$reg_array[$row['registered_from']] = $row['reg_start'];
		}
		foreach($install_arr as $key => $value)
		{
			$install_arr[$key]['reg'] = $reg_array[$key];
		}

		$this->smarty->assign("installs_arr",$install_arr);
	}

	private function processRow($row)
	{
		if($row['platform'] =='androidApp')
			$row['platform'] = 'android_app' ;

		if($row['platform'] =='iOSApp')
			$row['platform'] = 'ios_app' ;

		return $row;
	}

	private function getRegistrations()
	{
		$sql_reg = "SELECT if(ud.stay_country = 254,254,113) as country, gender, registered_from, count(*) as reg_start, SUM(IF(steps_completed REGEXP 'photo',1,0)) as complete,
				SUM(IF(status= 'authentic' || last_changed_status = 'authentic',1,0)) as authentic, SUM(IF(is_fb_connected ='Y',1,0)) as fb_connnected,
				CAST(100*(SUM(IF(steps_completed REGEXP 'photo',1,0)) /count(*) ) AS unsigned) as complete_perc,
				CAST(100*(SUM(IF(status= 'authentic' || last_changed_status = 'authentic',1,0)) /count(*) ) AS unsigned) as authentic_perc
				FROM user u left join user_data ud on u.user_id = ud.user_id
				 WHERE (email_id NOT REGEXP 'trulymadly.com' or email_id is null)  and (fid NOT IN ('100008254001811','100005921989349','100005780296713','100004832236718','100008305415203') or fid is null)
				  and registered_at>='$this->day_start' and  registered_at<'$this->day_end'
				 group by registered_from,country,gender ORDER BY country";

		$res_data = $this->conn_reporting->Execute($sql_reg);
		$registrations = array();
		while ($row = $res_data->FetchRow())
		{
			if($row['country'] == 254)
				$row['registered_from']  .= ' US';
			$registrations[] = $row;
		}
		$this->smarty->assign("registrations",$registrations);
	}
}




if(php_sapi_name() === 'cli')
{
	try
	{
		$from = "admintm@trulymadly.com";
		if($hour == 00)
		{
			$tracker = new RegistrationTracker($two_day_gmt, $start_date_gmt);
			$tracker->getAggregatedData();
			$data = $tracker->getCompleteData();

			$us_subject = "Daily US registration Data: $date_mail";
			$report = new USReport($two_day_gmt, $start_date_gmt ) ;
			$us_data = $report->getDailyData();
			$newYear = "2017-01-01 00:00:00";
			$report_cumulative = new USReport($newYear, $start_date_gmt ) ;
			$us_data .= "<br><br> Cumulative";
			$us_data .= $report_cumulative->getDailyData();
			Utils::sendEmail('sumit@trulymadly.com,lavi@trulymadly.com', $from, $us_subject, $us_data,true);
		}
		else {
			$tracker = new RegistrationTracker($start_date_gmt, $end_date_gmt);
			$data = $tracker->getCompleteData();
		}

		$data.="<br><br> For Last Hour";

		$tracker = new RegistrationTracker($start_hour_gmt, $end_hour_gmt);
		$data .= $tracker->getCompleteData();

		if($hour == 0)
			$to = $emailIdsForReporting['tracker_daily'];
		else
			$to = $emailIdsForReporting['tracker'];

		$subject = "Registration Tracker For $date_mail";
//		$to = 'sumit@trulymadly.com';
		$message = $data; //echo $data;
		$from = "admintm@trulymadly.com";
		Utils::sendEmail($to, $from, $subject, $message,true);
		echo "sent";
	}
	catch (Exception $e)
	{
		Utils::sendEmail('backend@trulymadly.com','admintm@trulymadly.com','Hourly registration tracked failed',$e->getMessage());
		trigger_error($e->getMessage(),E_USER_WARNING);
	}
}
?>