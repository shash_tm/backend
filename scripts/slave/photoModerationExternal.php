<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

$today= date('Y-m-d 00:00:00',strtotime('today'));
$yesterday= date ( 'Y-m-d 00:00:00' ,strtotime ( '-1 day' ) ); 

$now = date('Y-m-d H:i:s'); 
$hour = date ( 'Y-m-d H:i:s' ,strtotime ( '-1 hour' ) ); 
$hour_ist = date ( 'Y-m-d H:i:s' ,strtotime ( '+330 minutes' ) );


function photo_data ($start, $end)
{
	global $conn_reporting, $smarty;
	$external_moderator_ids = "20, 21";
//	$external_moderator_ids = "14";
	
	$sql = "select a.user_name ,pic_active , pic_rejected , c_p from  ( SELECT id, sum(if(step='Photo active',1,0)) as pic_active, 
			sum(if(step='Photo rejected',1,0)) as pic_rejected, count(*)as  c_p FROM admin_log al  WHERE al.step in ('Photo active','Photo rejected')
			and tstamp >= '$start' and tstamp < '$end' group by id  )t join admin a on t.id = a.id where a.id in ($external_moderator_ids) ";
	//echo  $sql;
	$rs = $conn_reporting->Execute ( $sql );
	$photo_arr = $rs->GetRows ();
	
	$smarty->assign("photo_data", $photo_arr);
	
	$file = dirname ( __FILE__ ).'/../../templates/reporting/photoModerationExternal.tpl';
	$data = $smarty->fetch ( $file );	
	
 return $data;
}
try {

	if ( php_sapi_name() == 'cli')
	{
		// Get the stats for the last hour as well as the whole day today 
		$data = "For Hour since : $hour_ist" ;
		$data .= photo_data ($hour, $now) ;	
		$data .= "For Whole day since:".$today ;
		$data .= photo_data($today, $now) ;
		$hourtime= date ( 'Y-m-d  H:i' ,$hour_ist );
	    $subject = 'Hourly Photo verification statistics for '.$hour_ist;
	} 
	 
	$to = $emailIdsForReporting['photo_extra']; 
	//$to = "sumit@trulymadly.com, shashwat@trulymadly.com, smita.prakash@trulymadly.com" ;
	$from = 'admintm@trulymadly.com'; 
	$message = $data;  
	Utils::sendEmail($to, $from, $subject, $message,true);
} catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_WARNING);
	Utils::sendEmail('sumit@trulymadly.com', 'sumit@trulymadly.com', 'Reporting Alert', 'External Photo moderation Reporting failed',true);
}
?>