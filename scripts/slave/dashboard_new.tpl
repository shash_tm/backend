<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Daily Tracker</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">


<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">

<tr>
<th>  </th>
<th>All  </th>
<th>Male  </th>
<th>Female  </th>
</tr>
  
 <tr>
<th width="200" align="center">All Installs </th>
<td align="center">{$result.installs}</td>
<td align="center">{$result.install_male} % </td>
<td align="center">{$result.install_female} % </td>
</tr>


<tr>
<th width="200" align="center">Uninstalls </th>
<td align="center">{$result.uninstalls}</td>
<td align="center">{$result.uninstall_male} % </td>
<td align="center">{$result.uninstall_female} % </td>
</tr>

<tr>
<th width="200" align="center">Total active installs </th>
<td align="center">{$result.active_installs}</td>
<td align="center">{$result.active_male} % </td>
<td align="center">{$result.active_female} % </td>
</tr>
<!--
<tr>
<th width="200" align="center">New Installs </th>
<td align="center">{$install.installs - $install_old.installs}</td>
</tr>
<tr>
<th width="300" align="center">Registrations from android app</th>
<td align="center">{$male.app+ $female.app}</td>
</tr>
 -->
 
 
</table>
  <h3> Active users and Installs </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<th width="400" align="center"> Active Installs as a percentage of total installs </th>
<th> {$result.active_install_percent} % </th>

</tr>
<tr>
<th width="400" align="center"> Total active users </th>
<th> {$result.active}  </th>
</tr>
</table>  

<h3> Engagement </h3>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<th width="400" align="center"> Metric </th>
<th> Numbers </th>
 {if $result.duration eq 'Month' || $result.duration eq 'Week'}
<th> % of last {$result.duration} </th>
{/if}
</tr>
<tr>
<th width="400" align="center"> Matches recommended </th>
<td> {$result.recommendations}  </td>
{if $result.duration eq 'Month' || $result.duration eq 'Week'}
<td> {$result.recommendations_p} % </td>
{/if}
</tr>
<tr>
<th width="400" align="center"> Matches made </th>
<td> {$result.matches}  </td>
{if $result.duration eq 'Month' || $result.duration eq 'Week'}
<td> {$result.matches_p} % </td>
{/if}
</tr>
<tr>
<th width="400" align="center"> Messages exchanged  </th>
<td> {$result.messages}  </td>
{if $result.duration eq 'Month' || $result.duration eq 'Week'}
<td> {$result.messages_p} % </td>
{/if}
</tr>
 {if $result.duration eq 'Month' || $result.duration eq 'Week'}
 <tr>
  <th width="400" align="center"> Male Authentic  </th>
  <td> {$result.authentic.male}  </td>
   <td> {$result.authentic_p.male} % </td>
 </tr>
  <tr>
   <th width="400" align="center"> Female Authentic  </th>
   <td> {$result.authentic.female}  </td>
   <td> {$result.authentic_p.female} % </td>
  </tr>
 {/if}
</table>

{if $result.duration eq 'Month'}
<h3> Demography </h3>
<h4> Age </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<tr>
<th> Age Group </th>
<th> No. of users </th>
</tr>
<tr>
<td> Less than 23  </td>
<td> {$result.age.age_one} </td>
</tr>
<tr>
<td> 24 to 27  </td>
<td> {$result.age.age_two} </td>
</tr>
<tr>
<td> More than or eqaul to 28  </td>
<td> {$result.age.age_three} </td>
</tr>
</table>

<h4> City </h4>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
<th> City </th>
<th> No. of users </th>
{foreach from=$result.city item=v}
<tr> 
<td> {$v.city} </td>
<td> {$v.count} </td>
</tr>
{/foreach}
</table>

{/if}
</body>
</html>