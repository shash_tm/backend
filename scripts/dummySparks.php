<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../spark/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../DBO/sparkDBO.php";
require_once dirname ( __FILE__ ) . "/../DBO/messagesDBO.php";

/* Author : Tarun
 * sends custom push notifications to different set of users based on certain conditions  */

class newCustomSparkNotifications {
	public $conn_reporting;
	public $admin_id;
	public $dummy_user_id;
	public $type;
	public $test_user_id;
	public $target_gender;
	public $message;

	function __construct() {
		global $admin_id,$imageurl,$conn_reporting;
		$this->conn_reporting = $conn_reporting;
		$this->admin_id = $admin_id;
		$this->dummy_user_id =  null; 	// for prod 1615723;
		$this->message = "Hey! The Spark you send appears like this on her profile. Pretty neat right? Get Sparking!";
	}

	function giveSparksToLadies($user_id){
		SparkDBO::addSparkCount($user_id, 5, 500,0);	//500 is expiry days
	}

	function sendDummySparkToGuy($user_id){
		$spark = new Spark($this->dummy_user_id) ;
		$unique_id = $user_id . "_" . microtime(true);
		$sparkHash = SparkDBO::getMd5Hash($this->dummy_user_id, $user_id, "spark");
		$output = $spark->sendSpark($this->dummy_user_id, $user_id, $unique_id, $this->message, $sparkHash);
	}

	function blockOldDummySparkMatches(){
		$sql = "SELECT us.user1, us.user2 from user_spark us join messages_queue mq on mq.sender_id = us.user1 ".
			"WHERE status ='accepted' and mq.blocked_by is null and us.tstamp < DATE_SUB(now(), INTERVAL 3 DAY) and us.user1 = ".$this->dummy_user_id.
			" GROUP by user2";
		$res = $this->conn_reporting->Execute($sql);
		if ($res->RowCount() > 0){
			while ($row = $res->FetchRow()) {
				$user = $row['user2'];
				$messagesDBO = new MessagesDBO();
				$messagesDBO->block($this->dummy_user_id, $user);
			}
		}
	}
}

try {

	if(php_sapi_name() === 'cli') {
		$date= date('Y-m-d',strtotime('today'));
		$sparkNotification = new newCustomSparkNotifications();
		$sparkNotification->dummy_user_id = 1615723;
		$sparkNotification->type = isset($argv[1]) && $argv[1] != null ? $argv[1] : null;
		if($sparkNotification->type != 'one_time' && $sparkNotification->type != 'daily'){
			echo "\n set params: \n 1 => type: value can be one_time or daily. \n 2 => target_gender: M or F or all. \n 3 => test_user_id  (Optional) \n";
			exit;
		}
		$sparkNotification->target_gender = isset($argv[2]) && $argv[2] != null ? $argv[2] : null;
		if($sparkNotification->target_gender != 'M' && $sparkNotification->target_gender != 'F' && $sparkNotification->target_gender != 'all' ){
			echo "\n set params: \n 1 => type: value can be 'one_time' or 'daily'. \n 2 => target_gender: M or F or all. \n 3 => test_user_id  (Optional) \n";
			exit;
		}
		$sparkNotification->test_user_id = isset($argv[3]) && $argv[3] != null ? $argv[3] : null;
		$count=0;
		$sparkAddedCount = 0;

		if($sparkNotification->type == 'one_time') {
			$userSql = "SELECT gender, user_id from user_search where gender='M'";
		}else if ($sparkNotification->type == 'daily'){
			$userSql = "SELECT gender, user_id from user where DATE(registered_at) = DATE(DATE_SUB(now(), INTERVAL 1 DAY))";
			$sparkNotification->blockOldDummySparkMatches();
		}

		$res = $sparkNotification->conn_reporting->Execute($userSql);
		if (($res->RowCount() > 0 && $sparkNotification->dummy_user_id != null) && $sparkNotification->test_user_id == null) {
			$mailmessage = "Total Dummy sparks sent: ";

			while ($row = $res->FetchRow()) {
				$user_id = $row['user_id'];
				$gender = $row['gender'];
				if($gender == 'M' && ($sparkNotification->target_gender == 'M' || $sparkNotification->target_gender == 'all')) {
					$sparkNotification->sendDummySparkToGuy($user_id);
					$count++;
				}else if($gender == 'F' && ($sparkNotification->target_gender == 'F' || $sparkNotification->target_gender == 'all')){
					$sparkNotification->giveSparksToLadies($user_id);
					$sparkAddedCount++;
				}
			}
			$to = "tarun@trulymadly.com, shashwat@trulymadly.com";
			$subject = "Events Notifications sent ".$date;
			$from = "admintm@trulymadly.com";
			Utils::sendEmail($to, $from, $subject, $mailmessage." : ".$count,true);
		}else if($sparkNotification->test_user_id != null){
			if($sparkNotification->target_gender == 'M' || $sparkNotification->target_gender == 'all') {
				echo "\nSending a dummy spark to: $sparkNotification->test_user_id from $sparkNotification->dummy_user_id \n";
				$sparkNotification->sendDummySparkToGuy($sparkNotification->test_user_id);
				$count++;
			}
			if($sparkNotification->target_gender == 'F' || $sparkNotification->target_gender == 'all'){
				echo "\n adding 5 sparks to test user id:  ".$sparkNotification->test_user_id."\n\n";
				$sparkNotification->giveSparksToLadies($sparkNotification->test_user_id);
				$sparkAddedCount++;
			}
		}
		else{
			echo "\n\nEnter dummy user ID ya Dummy ! (or maybe no new registrations :() \n\n";
		}
	}

} catch (Exception $e) {
	echo $e->getTraceAsString();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "Custom Notification script failed".$baseurl, $e->getTraceAsString());
}


?>