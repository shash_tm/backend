<?php

require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

/**
 * Daily running to delete extra rows in user_gcm_current for windows
 * updating the user table's device id
 * @author himanshu
 *
 */
class GCMUpdate
{

	private $conn_master;
	private $conn_reporting;

	function __construct()
	{
		global $conn_reporting, $conn_master;
		$this->conn_master = $conn_master;
		$this->conn_reporting = $conn_reporting;

	}
	public  function notifyTechTeam($msg){
		global $techTeamIds, $baseurl;
		$subject = $msg. " for " . $baseurl;
		$from = "himanshu@trulymadly.com";
		Utils::sendEmail($techTeamIds, $from, $subject, $msg);
	}

	public function removeRowsWithMultipleEntriesForWindows()
	{
		$sql = "Delete ug from user_gcm_current ug join ( select u.ts, u.user_id from (select user_id, max(tstamp) as ts from user_gcm_current where source = 'windows_app' and tstamp >= '2015-03-25' group by user_id) u join  (select user_id from user_gcm_current where source= 'windows_app' and tstamp >= '2015-03-25'  group by user_id having count(user_id)> 2 )x on u.user_id = x.user_id  )m on  ug.user_id = m.user_id and ug.tstamp != m.ts and ug.source = 'windows_app' where tstamp >= '2015-03-25'";
		$res = $this->conn_master->Execute($sql);
		return $this->conn_master->Affected_Rows();
		/*$arr = array();
		 while ($row = $res->FetchRow())
		 {
			$arr[] = array($row['user_id'], $row['registration_id'], $row['source'], $row['device_id']);
			}


			$uwsql = $this->conn_master->Prepare("DELETE FROM user_gcm_current where user_id = ? and registration_id =? and source =? and device_id = ? ");
			$this->conn_master->bulkBind = true;
			$this->conn_master->Execute($uwsql, $arr);*/
	}

	public function mapDeviceIdsFromGCMtoUserTable ()
	{
		$sql = "SELECT user_id, device_id , case when source = 'androidApp' then 'android_app' else (case when source = 'iOSApp'  then 'ios_app' else 'windows_app' end  )  end as source from user_gcm_current where tstamp>=curdate()-1 and tstamp<curdate() and length(device_id)> 30 and user_id is not null";
		$res = $this->conn_reporting->Execute($sql);

		$arr = array();
		$uwsql = $this->conn_master->Prepare("update ignore user set device_id = ? where user_id = ? and registered_from = ? ");
		while ($row = $res->FetchRow())
		{
			$arr[] = array($row['device_id'], $row['user_id'] ,$row['source']);
		}
		$this->conn_master->bulkBind = true;
		$this->conn_master->Execute($uwsql, $arr);
		return $this->conn_master->Affected_Rows();

	}
}
try{
	$obj = new GCMUpdate();
	$ar1 = $obj->removeRowsWithMultipleEntriesForWindows();
	$ar2 = $obj->mapDeviceIdsFromGCMtoUserTable();
	$obj->notifyTechTeam("extra rows deleted in user_gcm_current for windows: $ar1 and corresponding device_id updated in user: $ar2");

}
catch(Exception $e){
	$msg = "update device_id on user_gcm_current table script failed";
	echo $e->getMessage();
	//$obj->notifyTechTeam($msg. $e->getMessage());
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}
?>