<?php 
require_once dirname ( __FILE__ ) . '/../../include/config_admin.php';
require_once dirname ( __FILE__ ) . '/../../abstraction/query_wrapper.php';
require_once dirname ( __FILE__ ) . '/../../include/Utils.php';

class User_likes_monitor
{
	private $redisKey;
	private $redisKey_Check;
	private $redis;
    private $today;
    private $yesterday;

	function __construct()
	{
		global $redis;
		$this->redis=$redis;
		$this->redisKey=Utils::$redis_keys['female_like_day'];
		$this->redisKey_Check=Utils::$redis_keys['female_like_checkpoint'];
		$date=date_create(date("y-m-d"));
		$this->today=date_format($date,"Y-m-d");
		date_sub($date,date_interval_create_from_date_string("1 day"));
		$this->yesterday=date_format($date,"Y-m-d");
	}
	
	function process()
	{
		$redis_key=$this->redisKey.$this->today;
		//var_dump($redis_key);
		//echo "a";
		$this->process_redis($redis_key);
		$flag=$this->redis->exists($this->redisKey_Check.$this->today);
		//var_dump($flag);
		if($flag==false)
		{$redis_key=$this->redisKey.$this->yesterday;
		 $this->process_redis($redis_key); 
		 $this->redis->set($this->redisKey_Check.$this->today,"1");
		 $this->redis->del($this->redisKey_Check.$this->yesterday);
		 $this->redis->del($redis_key);
		}
		
	}
	
	private function process_redis($redis_key) {
		$val=$this->redis->hGetAll($redis_key);
		$i=0;
		//var_dump($val);
		//echo "\nc\n";
		$arr=array();
		foreach ($val as $key => $value) {
			if($value>=Utils::$dailyFemaleLikeThreshold)
			{
				//echo "\n$i\n";
				$arr[$i][]=$key;
				$arr[$i][]=$value;
				$arr[$i][]=$value;
				$i++;
			}
		}
		//	var_dump($arr);
		if(count($arr)>0)
		{
		
		$query="insert into user_under_review (user_id, max_likes, tstamp) values (?,?,now()) on duplicate key update max_likes=?,tstamp=now()";
		$param_array=$arr;
		$tablename="user_under_review"; 
		Query_Wrapper::BulkINSERT($query,$param_array,$tablename);
		
		$del_array=array();
		foreach($arr as $value)
		{
			$del_array[]=$value[0];
		}
		$param_count = Query_Wrapper::getParamCount($del_array);
		$query="delete from user_search where user_id in ($param_count)";
		$tablename="user_search";
		Query_Wrapper::DELETE($query,$del_array,$tablename);
        }
	}
}

try {
	if ( php_sapi_name() == 'cli' ) {
		$obj=new User_likes_monitor();
	    $obj->process();
	}
	
} catch (Exception $e) {
	//send mail for fatal error
	global $baseurl;
	echo $e->getMessage();
	if($baseurl=='https://dev.trulymadly.com/trulymadly' || $baseurl=='http://dev.trulymadly.com/trulymadly')
	{
		$to = "arpan@trulymadly.com";
	}
	else if($baseurl=='https://t1.trulymadly.com' || $baseurl=='http://t1.trulymadly.com')
	{
		$to = "vikash@trulymadly.com, shashwat@trulymadly.com";
	}
	else
		$to = "arpan@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Cron for user_like_monitor failed at ".$baseurl ;
	$message = "Cron for user_like_monitor failed due to " . PHP_EOL . $e->getTraceAsString();
	$from = "admintm";
	$headers = "From:" . $from;
	Utils::sendEmail($to, $from, $subject, null);
	//mail($to,$subject,$message,$headers);
	//	echo "Mail Sent.";
	error_log($subject,1,$to,$from);
	trigger_error ( "PHP WEB: ".$e->getTraceAsString (), E_USER_WARNING );
}

?>