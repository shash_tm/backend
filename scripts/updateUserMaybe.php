<?php

require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";


function notifyTechTeam($msg){
	global $techTeamIds, $baseurl;
	var_dump($techTeamIds);
	$subject = $msg. " for " . $baseurl;
	$from = "himanshu@trulymadly.com";
	Utils::sendEmail($techTeamIds, $from, $subject, $msg); 
}

function updateUserMaybeTable(){
	global $conn_master, $techTeamIds;
	$x_days = intval(Utils::$mayBeExpiryDays);
	if( $x_days<=1) $x_days = 10;

	$sql = "UPDATE user_maybe  set is_active = 0 where date(timestamp) <=  date_sub(CURDATE(), interval $x_days day)" ; 
	echo $sql;
	$res = $conn_master->Execute($sql);
	$count = $conn_master->Affected_Rows() ;

	notifyTechTeam("number of rows of user_maybe table updated: " . $count);
}



try{
	updateUserMaybeTable();
}
catch(Exception $e){
	$msg = "update user_maybe table script failed";
	notifyTechTeam($msg);
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}
?>