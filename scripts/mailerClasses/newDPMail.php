<?php

require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";

/**
 * @Chetan
 * Mailer for New, active authentic  who completed registration, Registered in last 1 week
 */

class newDpMail
{
	public function sendMailer($external_email = null)
	{
		try
		{
			global $config, $conn_reporting, $smarty, $imageurl, $dummy_female_image, $dummy_male_image, $mailSleepTimeSES, $baseurl, $admin_id, $techTeamIds; // global veriable defined for values required in the emailer
			$sysLog = new SystemLogger (); // for system log date n time
			$uu = new UserUtils (); // for user info
			$mailObject = new MailFunctions ();



			$subject = ", your profile picture is worth a thousand words!";

			// sql query for fatching data from db for target user
			$sql = "SELECT u.user_id, u.email_id,u.gender, u.fname FROM user u
					where  u.status in ('authentic') 
					and date(registered_at) >= DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 1 week) AND date(registered_at) < curdate() 
					AND email_id not like '%trulymadly%' AND email_status is null";
			$res = $conn_reporting->Execute ( $sql ); // for executing query
			$data = $res->GetRows (); // for fetching data from executed query

			$i=0;
			// var_dump($data); // for print on to console
			foreach ( $data as $val )
			{

				$name = ucfirst ( $val ['fname'] );
				$smarty->assign ( "name", $name );

				$toAddress = $val ['email_id'];
				if (isset ( $external_email ))
				$toAddress = $external_email;


				if ($val ['gender'] == 'M'){
					$campaign_subject = "DPmale";
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$campaign_subject"; // for emailer tracking
					$utm_content = "view_$campaign_subject"; // tracker
					$campaignName = $campaign_subject; // tracker
					$analyticsLinks = Utils::generateSystemMailerLinks ( $campaignName, $utm_content, $redirectionFile, $toAddress, $val ['user_id'] );
					$smarty->assign ( "analyticsLinks", $analyticsLinks );

					$mailObject->sendMail ( array (
					$toAddress
					), $smarty->fetch ( dirname ( __FILE__ ) . '/../../templates/utilities/newmailers/DPmale.tpl' ), "", $name . $subject, $val ['user_id'] );
				}
				else {
					$campaign_subject = "DPfemale";
					$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$campaign_subject"; // for emailer tracking
					$utm_content = "view_$campaign_subject"; // tracker
					$campaignName = $campaign_subject; // tracker
					$analyticsLinks = Utils::generateSystemMailerLinks ( $campaignName, $utm_content, $redirectionFile, $toAddress, $val ['user_id'] );
					$smarty->assign ( "analyticsLinks", $analyticsLinks );

					$mailObject->sendMail ( array (
					$toAddress
					), $smarty->fetch ( dirname ( __FILE__ ) . '/../../templates/utilities/newmailers/DPfemale.tpl' ), "", $name . $subject, $val ['user_id'] );
				}
					
				// $smarty->fetch ( dirname ( __FILE__ ) . '/../templates/utilities/newmailers/EmailerJaipur.tpl' ), "", $name . ", " . $subject, $val['user_id'] );
				// till here mail is sent
				// now we are logging the same into our system
				$loggerData = null;
				$loggerData ['email_id'] = $toAddress;
				$loggerData ['subject'] = $campaign_subject;
				$loggerData ['user_id'] = $val ['user_id'];
				$sysLog->logSystemMail ( $loggerData );

				$i++;
			}
			
			return  array("subject of mailer" => $subject, "count" => $i, "query" => $sql );
			
			/*$reportLog = array("subject of mailer" => $subject, "count" => $i, "query" => $sql );
			 Utils::sendEmail ( $techTeamIds, "chetan@trulymadly.com", "Mailer Report: " .$subject, json_encode($reportLog));
			 */
		}
		catch ( Exception $e )
		{

			global $baseurl, $techTeamIds;
			$subject = "DPmalefemale Mailer failed due to ". $e->getMessage();
			Utils::sendEmail ( $techTeamIds, "himanshu@trulymadly.com", $subject, $e->getTraceAsString () );
			trigger_error ( "PHP WEB: DPmalefemale Mailer failed". $e->getTraceAsString(), E_USER_WARNING );


		}
	}
}

?>