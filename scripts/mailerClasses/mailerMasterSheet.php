<?php


require_once dirname ( __FILE__ ) . "/successstory.php";
require_once dirname ( __FILE__ ) . "/lowPiIncrease.php";
require_once dirname ( __FILE__ ) . "/authenticNotLoggedInOneDayAfterReg.php";
require_once dirname ( __FILE__ ) . "/incompleteRegistrationMailer.php";
require_once dirname ( __FILE__ ) . "/newDPMail.php";
require_once dirname ( __FILE__ ) . "/weeklyUnreadMessages.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";


/**
 * master sheet to run all the mailers which are not real time
 * @author himanshu
 */

class masterSheet
{

	private $_external_email = null;

	function __construct ($email)
	{
		if($email != null )
		$this->_external_email = $email;
	}


	/**
	 * to be executed for daily mailers
	 */
	public function sendDailyMailers ()
	{
		$successStoryMailerObj = new successStory();
		$successStoryMailerObj->sendMailer($this->_external_email);

		$newUserCheckMatchesMailerObj = new authenticNotLoggedInOneDayAfterRegistration();
		$newUserCheckMatchesMailerObj->sendMailer($this->_external_email);

		$incompleteRegMailerObj = new incompleteRegistration();
		$incompleteRegMailerObj->sendMailer($this->_external_email);

	}

	/**
	 * to be executed weekly based on some specific day
	 */
	public function sendWeeklyMailers()
	{
		//run it on monday only
		if (date('D', time()) === 'Mon')
		{
			$lowPiMailerObj = new lowPiIncrease();
			$lowPiMailerObj->sendMailer($this->_external_email);

			$weeklyMsgMailerObj = new weeklyUnreadMessages();
			$weeklyMsgMailerObj->sendMailer($this->_external_email);
				
			$newDPMailerObj = new newDpMail();
			$newDPMailerObj->sendMailer($this->_external_email);
		}
	}

	/**
	 * monthly mailers need to be coded here
	 */
	public function sendMonthlyMailers()
	{

	}


	/**
	 * to send the consolidated report of this mailer sheet
	 */
	public function sendMailerReport ()
	{
		Utils::sendEmail ( $this->_managementTeam, "himanshu@trulymadly.com","Mailer Reporting for master mailer sheet", json_encode($this->_mailer_report));
	}

}




try
{
	global $baseurl, $techTeamIds;
	$options = getopt ( "e:" );

	if(php_sapi_name() === 'cli')
	{
		$masterMailer = new masterSheet ($options ["e"]);
		$masterMailer->sendDailyMailers();
		$masterMailer->sendWeeklyMailers();
		$masterMailer->sendMonthlyMailers();
		require_once dirname ( __FILE__ ) . "/../emailStats.php";
		
		//$masterMailer->sendMailerReport();
		//Utils::sendEmail ( $techTeamIds, "himanshu@trulymadly.com", "master mailer list cron ran " , "successful mailer ran for all the daily and weekly list" );
	}
}
catch ( Exception $e )
{
	global $baseurl, $techTeamIds;
	$subject = $baseurl . " Master sheet mailer failed due to ". $e->getMessage();
	Utils::sendEmail ( $techTeamIds, "himanshu@trulymadly.com", $subject, $e->getTraceAsString () );
	trigger_error ( "PHP WEB: Master sheet mailer failed " . $e->getTraceAsString (), E_USER_WARNING );
}


?>