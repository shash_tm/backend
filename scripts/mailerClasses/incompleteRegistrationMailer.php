<?php

require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";

/**
 * @Himanshu
 * Class for Mailer for the user who don't complete the registration, one time- first day after registration
 */

class incompleteRegistration
{
	public function sendMailer ($external_email = null)
	{
		global $config, $smarty,$conn_master, $conn_reporting, $imageurl, $dummy_female_image, $dummy_male_image,$baseurl;

		try
		{
			$sysLog = new SystemLogger();

			$sql = "SELECT fname,email_id, email_status, user_id from user where status = 'incomplete' and date(registered_at) = date_sub(CURDATE(), interval 1 day)  AND email_id not like '%trulymadly%' AND email_status is null";
			$res = $conn_reporting->Execute($sql);
			$rows =$res->GetRows();

			$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=incomplete_profile_nextDay";
			$redirectionFile = "/register.php";
			$utm_content="view_incomplete_profile_nextDay";
			$campaignName = "incomplete_profile_nextDay";

			$i=0;
			foreach ($rows as $key=>$val){
				
				$data = null;
				$data['email_id'] = $val['email_id'];
				$data['subject'] = "incomplete_profile_nextDay";
				$data['user_id'] = $val['user_id'];
					
				$smarty->assign("name", ucfirst($val['fname']));
				$smarty->assign("rnum", rand(100, 200));

				$toAddress = $val['email_id'];

				if(isset($external_email))
				$toAddress = $external_email;

				$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $val['user_id']);
				$smarty->assign("analyticsLinks", $analyticsLinks);

				$mailObject = new MailFunctions();
				$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../../templates/utilities/newmailers/RegistrationNotComplete.tpl'),"",ucfirst($val['fname']) . ", ". "where did you go?", $val['user_id']);
				$sysLog->logSystemMail($data);
				$i++;
			}
			
			return  array("subject of mailer" => "where did you go?", "count" => $i, "query" => $sql );
		}

		catch(Exception $e)
		{
			global $baseurl, $techTeamIds;
			$subject = "Incomplete Profile Mailer failed due to ". $e->getMessage();
			Utils::sendEmail ( $techTeamIds, "himanshu@trulymadly.com", $subject, $e->getTraceAsString () );
			trigger_error ( "PHP WEB: Incomplete Profile Mailer failed". $e->getTraceAsString(), E_USER_WARNING );
		}


	}

}


?>