<?php

require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../../logging/UnsubscriptionClass.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";



/**
 * @Himanshu
 * Mailer for weekly unread messages where receiver id is inactive
 */


class weeklyUnreadMessages
{
	public function sendMailer($external_email = null){

		try
		{
			global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl, $admin_id;
			$sysLog = new SystemLogger();
			$uu = new UserUtils();

			$sql = "select group_concat(mq.sender_id) as senders,group_concat(mq.msg_type) as msg_type,  group_concat(mq.tStamp) as time,group_concat('TMSALT' ,  mq.msg_content) as content, count(mq.sender_id) as count, mq.receiver_id from messages_queue mq join user u on u.user_id = mq.receiver_id where mq.blocked_by is null and date(u.last_login) < date_sub(curdate(), interval 7 day) and mq.last_seen < mq.tStamp  and mq.tStamp >= date_sub(curdate(), interval 7 day) and mq.tStamp <= date_sub(NOW(), interval 1 day) and msg_type != 'MUTUAL_LIKE' group by mq.receiver_id order by mq.receiver_id, mq.tStamp desc";
			$res = $conn_slave->Execute($sql);
			$data =$res->GetRows();

			$sub ="weekly_unread_messages";
			$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$sub"; 
			$redirectionFile = "/msg/messages.php";
			$utm_content="view_$sub";
			$campaignName = "$sub";

			$i = 0;
			$prev_user = null;
			$smartydata = null;
			$idsToMail = null;
			$subjects = null;

			foreach ($data as $row => $col)
			{
				$senders = null;
				$timeStamp = null;
				$idsToMail[] = $col['receiver_id'];
				$msgs = null;
				$senders  = explode(',', $col['senders']);
				$timeStamp = explode(',', $col['time']);
				$contents = explode(",TMSALT", $col['content']);
				$msg_types = explode(',', $col['msg_type']);
				$timeArr = null;
				$content_arr = null;
				for($i=0;$i<count($timeStamp); $i++)
				{
					$timeArr[$senders[$i]] = $timeStamp[$i];
				}

				var_dump($contents);
				for( $j=0; $j<count($contents); $j++)
				{
					if($j==0) {
						$content_arr[$senders[$j]] = ltrim($contents[$j], "TMSALT");
					}
					else
					$content_arr[$senders[$j]] = $contents[$j];

					if($msg_types[$j] == "STICKER"){
						$content_arr[$senders[$j]] = "You have received a sticker.";
					}
				}
				/*var_dump($senders);
				 var_dump($content_arr); die;
				 */
				$subjects[$col['receiver_id']] = ($col['count'] == 1)?"you have 1 new message!":"you have " .$col['count']." new messages!";

				$tileData = $uu->getMailerTile($senders, $col['receiver_id'], $campaign);

				foreach ($tileData as $key => $value){
					$tileData[$key]['timestamp'] = Utils::GMTTOIST($timeArr[$key]);
					$tileData[$key]['msg']= $content_arr[$key];
				}
				$smartydata[$col['receiver_id']]['data'] = $tileData;
				$smartydata[$col['receiver_id']]['msg_count'] = $col['count'];
				if(count($col['count']) == 1 && $senders[0] == $admin_id) $smartydata[$col['receiver_id']]["noWoohoo"] = 1;

			}

			if(count($idsToMail) >0 ){
				$sql2 = "SELECT fname, email_id, user_id,email_status from user where status = 'authentic' and user_id in (". implode(',', $idsToMail). ')';
				$res2 = $conn_slave->Execute($sql2);
				$basicinfo =$res2->GetRows();

				//$basicinfo = Query::SELECT($sql2, null);
				$info = null;
				foreach ($basicinfo as $val)
				{
					$info[$val['user_id']] = $val;
				}
			}

			$j=0;
			
			for ($i=0; $i<count($idsToMail) ;$i++)
			{
				if($idsToMail[$i] == $admin_id) continue;
				//if info comes from above query only then run the below
				if(isset($info[$idsToMail[$i]]) && $info[$idsToMail[$i]]['email_status'] == null){
					$name = ucfirst($info[$idsToMail[$i]]['fname']);
					$smarty->assign("data", $smartydata[$idsToMail[$i]]);
					$smarty->assign("name", $name);

					$toAddress = $info[$idsToMail[$i]]['email_id'];
					if(isset($external_email))
					$toAddress = $external_email;

					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $idsToMail[$i]);
					$smarty->assign("analyticsLinks", $analyticsLinks);
					$mailObject = new MailFunctions();

					$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ). '/../../templates/utilities/newmailers/emailMsgs.tpl'),"",$name . ", ".$subjects[$idsToMail[$i]], $idsToMail[$i]);
					$loggerData = null;
					$loggerData['email_id'] = $toAddress;
					$loggerData['subject'] = $campaignName;
					$loggerData['user_id'] = $idsToMail[$i];
					$sysLog->logSystemMail($loggerData);
					$j++;
				}
			}
			
			return  array("subject of mailer" => "weekly unread messages", "count" => $j, "query" => $sql );
		}
		catch ( Exception $e )
		{
			global $baseurl, $techTeamIds;
			$subject = "Weekly Messaging Mailer failed due to ". $e->getMessage();
			Utils::sendEmail ( $techTeamIds, "himanshu@trulymadly.com", $subject, $e->getTraceAsString () );
			trigger_error ( "Weekly Messaging Mailer failed". $e->getTraceAsString(), E_USER_WARNING );
		}


	}
}

?>