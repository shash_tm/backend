<?php


require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
require_once  dirname ( __FILE__ ) . "/../include/config.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../logging/UnsubscriptionClass.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";



/**
 * @Chetan
 * Mailer for Recommendation received(by females for male and anyone for females)
 * If (number of endorsements <2)
 if male then "Now you are only 1 female endorsement short of getting the requisite points ."
 if female then "Now you are only 1 endorsement short of getting the requisite points."
 */

class endorsementMailer{

	private $user_id;


	function __construct($user_id){
		$this->user_id = $user_id;
	}

	private function _getMailerContentForLessThanThreshold ( $endorsementCount, $myData, $endorserData )
	{
		if ( $endorsementCount < 2)
		{
			if ($myData ['gender'] == "F")
			{
				$content = "Now you are only 1 endorsement short of getting the requisite points.";
			}
			else
			{
				$content = "Now you are only 1 female endorsement short of getting the requisite points.";
			}
		}
		
		return $content;
	}

	public function sendMailer ( $endorsementCount = 0 , $endorserData = array ())
	{

		if (!empty($endorserData) && $endorsementCount > 0)
		{
			try
			{
				global  $smarty,  $baseurl;
				$sysLog = new SystemLogger();

				$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=endorsement_given";
				$redirectionFile = "";
				$utm_content="view_endorsement_given";
				$campaignName = "endorsement_given";

				$uu = new UserUtils();

				$sql2 = "SELECT fname, email_id, user_id,email_status, gender from user where user_id = ?";
				$basicinfo = Query::SELECT($sql2, array($this->user_id), Query::$__slave, true);

				$name = ucfirst($endorserData['fname']);
				if($basicinfo['email_status']  == null)
				{
					$time = time();
					$ts = Utils::getDateGMTTOISTfromUnixTimestamp($time);
					$smarty->assign("ts", $ts);
					$toAddress = $basicinfo['email_id'];

					$smarty->assign("endorser_name", $name);
					$smarty->assign("name", ucfirst($basicinfo['fname']));
					$mailerContent = $this->_getMailerContentForLessThanThreshold($endorsementCount, $basicinfo, $endorserData);

					if(isset($mailerContent))
					$smarty->assign("content", $mailerContent);

					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $this->user_id);
					$smarty->assign("analyticsLinks", $analyticsLinks);
					$mailObject = new MailFunctions();
					//	$smarty->display('../templates/utilities/emailMsgs.tpl');die;

					$mailerSubject = "Sweet! $name just recommended you!";
					$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/newmailers/EndorsementMailer.tpl'),"",$mailerSubject, $this->user_id);

					$data['email_id'] = $basicinfo['email_id'];
					$data['subject'] = "endorsement_given";
					$data['user_id'] = $this->user_id;
					$sysLog->logSystemMail($data);

				}
			}
				
			catch(Exception $e)
			{
				trigger_error("PHP WEB: $e->getMessage() " . $e->getTraceAsString(), E_USER_WARNING);
			}

		}
	}

}




?>