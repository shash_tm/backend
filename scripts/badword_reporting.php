<?php 
/*
 * @uthor: Arpan Jain
 * valar morghulis
 * script for reporting of bad words in conversation of previous day
 */

require_once dirname ( __FILE__ ) . "/../utilities/wordlist-regex.php";
require_once  dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";

function scan_msgs()
{
	
	global $badwords_hashtag,$smarty,$conn_reporting,$miss_tm_id;
	$str_to_insert_start='<font size="3" color="red">';
	$str_to_insert_end='</font>';
	$today_msgs=array();
	$bad_conv=array();

	//echo "Query Running....\n";
	$sql = "SELECT sender_id,receiver_id, msg_content, tStamp FROM current_messages_new where tStamp>=date_sub(curdate(), interval '1 5:30' day_minute) and receiver_id!=$miss_tm_id and tStamp<=date_sub(curdate(), interval '0 5:30' day_minute) and msg_type='TEXT'";
	$result= $conn_reporting->Execute($sql);

	//echo "Processing....\n";
	//echo "\n";
	$i=0;
	
	while ($msg=$result->FetchRow())
	{
		
		$data=$msg['msg_content'];
		$data = htmlspecialchars($data);
		$arr = explode(' ', $data);
		$arr = array_map('strtolower', $arr);
		$res=array_intersect($arr, $badwords_hashtag);
		
		if($res!=NULL)
		{ 
			
			
			
			$bad_conv[$i]=$msg;
			
			$oldstr=$msg["msg_content"];
			foreach ($res as $word)
			{
				$str_len=strlen($word);
				$pos=strpos($oldstr, $word);
			
				$str = substr($oldstr, 0, $pos) . $str_to_insert_start . substr($oldstr, $pos,$str_len) . $str_to_insert_end . substr($oldstr, $pos+$str_len);
				$oldstr=$str;
				
			}
      		$bad_conv[$i]['msg_content']=$oldstr;
			$i++;
			
		}
	}

	$smarty->assign("badMsgs_yesterday", $bad_conv);
}



function scan_msgs_for_phone_exchange()
{
	global $smarty,$conn_reporting;
	$phn_exchange=array();
	
	$sql1="select sender_id, receiver_id, msg_content, tStamp  from current_messages_new 
		where msg_type='TEXT' and tStamp>date_sub(curdate(), interval '1 5:30' day_minute) and msg_content regexp '[0-9]{10}'";
	$result= $conn_reporting->Execute($sql1);
	$i=0;
	while ($data=$result->FetchRow())
	{
		$sql2="SELECT count(*) as count FROM current_messages_new WHERE sender_id=? and receiver_id=? and msg_type='TEXT'";
		$result2=$conn_reporting->Execute($sql2,array($data['sender_id'],$data['receiver_id']));
		$msg_count=$result2->FetchRow();
		if($msg_count['count']<=4)
		{
			$phn_exchange[$i]['sender_id']=$data['sender_id'];
			$phn_exchange[$i]['receiver_id']=$data['receiver_id'];
			$phn_exchange[$i]['msg_content']=$data['msg_content'];
			$phn_exchange[$i]['tStamp']=$data['tStamp'];
			$i++;
		}
		
	}
	//var_dump($phn_exchange);
	$smarty->assign("phnexchanged_yesterday", $phn_exchange);
}

function sendMail(){
	global $smarty,$emailIdsForReporting;
	$subject = "Bad Words in Conversation Statistics: ". date('d-m-Y',strtotime("-1 days"));
    $to=$emailIdsForReporting['badword_reporting'];
	//$to ="arpan@trulymadly.com" ;
	$message = $smarty->Fetch(dirname ( __FILE__ ).'/../templates/reporting/badword_reporting.tpl') ;
	Utils::sendEmail( $to , "admintm@trulymadly.com", $subject, $message , TRUE);
}

try {
	//echo "Scanning....\n";
	scan_msgs();
	//scan_msgs_for_phone_exchange();
	sendMail();
	

} catch (Exception $e) {
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}

?>