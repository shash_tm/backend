<?php
require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/User.php";
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


class CouponCodesMailer
{
    private $conn_reporting ;
    private $smarty ;
    private $subject ;
    private $template ;
    private $mailBody ;
    private $mailObject ;
    private $sendCount = 0;
    private $emailIds = array();

    function __construct()
    {
        global $conn_reporting, $smarty;
        $this->conn_reporting = $conn_reporting;
        $this->smarty = $smarty ;
        $this->mailObject = new MailFunctions();
        $this->getConstants();
    }

    public function sendCouponCodeMailer($target = null)
    {
        if($target == 'all')
        {
            foreach($this->emailIds as $val)
            {

                $this->sendToEmailId($val);
            }
        }
        else if(isset($target) && $target != null)
        {

            $this->sendToEmailId($target) ;

        }
        else
        {
            echo "No target";
        }

        echo "sent to " .$this->sendCount ;
        Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com','Mailer sent to ' . $this->sendCount .' users', "Subject line :".$this->subject);
    }

    private function getConstants ()
    {
        global $cdnurl ;
        $image = $cdnurl . "/images/mailers/readings.png";
        $this->smarty->assign("image_url",$image);
        $this->emailIds = array('shirinraigupta@gmail.com',
                                'Priyanka.mukherji@gmail.com',
                                 ' Abhi_indrmz@rediffmail.com',
                                 ' priyamnayak9@yahoo.com ',
                                 ' tejaswi.subramanian@gmail.com',
                                 ' akshitha2110@gmail.com',
                                 ' megha.m254@gmail.com',
                                 ' carameljames.chakson@gmail.com',
                                 ' olivadass@gmail.com',
                                 ' Souvita@gmail.com',
                                 ' vanshika_ika@yahoo.com',
                                 ' sherry.jindal08@gmail.com',
                                 ' anusri.kumar95@gmail.com',
                                 ' archana.natarajan0@gmail.com',
                                 ' abirameraju11@gmail.com',
                                 ' namratachin@gmail.com ',
                                 ' NCLAREGEORGE@GMAIL.COM',
                                 ' cooldude@gmail.com',
                                 ' brundakamb@gmail.com',
                                 ' poojaa.ggupta@gmail.com',
                                 ' rahuldev0970@gmail.com ',
                                 ' poonkothai20@gmail.com',
                                 ' anangopal21@gmail.com',
                                 ' dsilvanatasha214@gmail.com',
                                 ' anangopal21@gmail.com',
                                 ' swathi28nair@gmail.com',
                                 ' Anusha.g.1302@gmail.com ',
                                 ' wrutuja.botre@gmail.com',
                                 ' souvita@gmail.com',
                                 ' Priyanka.tsj@gmail.com ',
                                 ' bajajnamrata30@gmail.com',
                                 ' Pallavi. Semwal @yahoo.co.in',
                                 ' bhawya28@gmail.com',
                                 ' sairariyaz14@gmail.com',
                                 ' Jenny.laffery@yahoo.com',
                                 ' vijayan.shekhar@gmail.com',
                                 ' janet.shamili17@gmail.com',
                                 ' sabhya.soni1708@gmail.com',
                                 ' bansari.patel95@gmail.com',
                                 '  ssreeja164@gmail.com',
                                 ' Krishu20@gmail.com',
                                 ' chitsdev@gmail.com',
                                 ' info@heka.co.in',
                                 ' throughthelanes@gmail.com',
                                 ' dayita.sengupta@gmail.com',
                                 ' shikhartyag@gmail.com ',
                                 ' Rejula1991@gmail.com',
                                 ' sammohan444@gmail.com',
                                 ' Aahana.banerjee@gmail.com',
                                 ' harika.svec@gmail.com',
                                 ' poonkothai20@gmail.com',
                                 ' nitishree7@gmail.com',
                                 ' jps.prashant@gmail.com',
                                 ' shrutiagrawal.21@gmail.com',
                                 ' akshaypradipjain@gmail.com',
                                 ' vishalcse.cgc@gmail.com',
                                 ' Singhsupriya878@gmail.com',
                                 ' puneet.srivastava08@gmail.com',
                                 ' swatipanda.mit@gmail.com',
                                 ' utsavagarwal.23@gmail.com',
                                 ' ninateresageorge@gmail.com',
                                 ' ranjithav21@gmail. com',
                                 ' ranjithav21@gmail. com',
                                 ' yashikasingh422@gmail.com',
                                 ' edwin.kimberly@gmail.com',
                                 ' sakshi.narula15@gmail.com',
                                 ' pujan.gc@gmail.com',
                                 ' Pritha0aash@gmail. Com',
                                 ' Shivani.singh88@gmail.com',
                                 ' Sonujn1998@gmail.com',
                                 ' santoshmishra@intellevet.com',
                                 ' sudeeptapradhan.imnu@gmail.com',
                                 ' richanegi@gmail.com',
                                 ' kritikapadode@gmail.com',
                                 ' vish.6904@gmail.com',
                                 ' shara.eco@gmail.com',
                                 ' Naren_sh@rediffmail.com',
                                 ' olivadass@gmail.com',
                                 ' sakshi_ahuja75@yahoo.com',
                                 ' amedha1412@gmail.com',
                                 ' madhavimishra00@gmail.com',
                                 ' rejula1991@gmail.com',
                                 ' MURALI.VEERAIYAN@GMAIL.COM',
                                 ' ankuagarwal@yahoo.com',
                                 ' Luvhari@gmail.com',
                                 ' abhijeethnagaraj@gmail.com',
                                 ' Sohil.poojara@gmail.com',
                                 ' dinesh0033@gmail.com',
                                 ' Sanjaykrish04@gmail.com',
                                 ' giritalks@gmail.com',
                                 ' kaushikanabayan@gmail.com',
                                 ' robinjosephabraham@gmail.com',
                                 ' Archit.bhasin@anz.com',
                                 ' Jinomathew899@gmail.com',
                                 ' Svinay1@gmail.com',
                                 ' wahidtalha.wahid@gmail.com',
                                 ' babuanjan@gmail.com',
                                 ' Vibhore1908@gmail.com',
                                 ' Sohil.poojara@gmail.com',
                                 ' Arjunsinghbais@gmail.com',
                                 ' Sanilkku@rediffmail.com',
                                 ' Mcsekargct@gmail.com',
                                 ' Gniranjank@gmail.com',
                                 ' scrdesigna2015@ gmail.com',
                                 ' Absolutelyfrank@gmail.com',
                                 ' Falgun91@gmail.com',
                                 ' Prakash.brili@gmail.com',
                                 ' dishendra.platinum@gmail.com',
                                 ' Mcsekargct@gmail.com',
                                 ' ankursethia89@gmail.com',
                                 ' abilashs@gmail.com',
                                 ' meetgowrishankar@gmail.com',
                                 ' Kartic.4u1@gmail.com',
                                 ' Xyz@gmail.com',
                                 ' praveenkn555@gmail.com',
                                 ' Knarayana405@gmail.com',
                                 ' Yguptabgh@gmail.com',
                                 ' Murali.veeraiyan@gmail.com',
                                 ' Vanshika_ika@yahoo.com',
                                 ' cindhu.rao@gmail.com',
                                 ' Amolsharma91@gmail.com',
                                 ' saarthholdings@yahoo.com',
                                 ' Guzaarish4nouf@gmail.com',
                                 ' sourish.ghosh@gmail.com',
                                 ' Shrutibaghel92@gmail.com',
                                 ' Kajalr671@gmail.com');



        $this->subject = "Reminder- UnSingleReadings!";
        $this->template = dirname ( __FILE__ ). '/../../templates/utilities/newmailers/tmScenes.tpl';
        $this->mailBody = $this->smarty->fetch($this->template);
       // echo $this->mailBody ; die ;
    }

    private function sendToEmailId($email)
    {
        try
        {
            echo PHP_EOL.'sent to '.$email;
            $mid= $this->mailObject->sendMail(array($email) ,$this->mailBody,"",$this->subject, null);
            $this->sendCount++ ;
        }
        catch (Exception $e)
        {
            echo 'could not send to ' . $email ;
        }

    }
}



try
{
    if(php_sapi_name() === 'cli') {
        $target = $argv[1];
        $couponMailer = New CouponCodesMailer();
        $couponMailer->sendCouponCodeMailer($target);
    }
}
catch (Exception $e)
{
    trigger_error($e->getMessage(), E_USER_WARNING);
    echo $e->getMessage() ;
}




?>
