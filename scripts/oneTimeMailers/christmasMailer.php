<?php 
require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/User.php";
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


function sendChristmasMailer (){
	global $smarty, $conn_reporting;
	$sql="SELECT u.user_id, u.email_id,u.gender, u.fname FROM user u where status in ('authentic','non-authentic')";
	//$sql="SELECT u.user_id, u.email_id, u.fname, u.gender FROM user u JOIN user_data ud ON u.user_id=ud.user_id WHERE u.user_id=2049 ";
	$rs=$conn_reporting->Execute($sql);
	$user_ids= $rs->GetRows();
	//var_dump($user_ids);
	
	$mailObject = new MailFunctions();
	$user=  new User(); 
	$count=0;
	foreach ($user_ids as $r => $c)
	{
		$isValid = $user->canSendMail($c['email_id']);
		$smarty->assign("name", ucfirst($c['fname']));  
		if ($isValid>0) {
			$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=christmas_mailer";
			$redirectionFile = "/matches.php";
			$utm_content="christmas_mailer";
			$campaignName = "christmas_mailer";
			$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $c['email_id'], $c['user_id']);
			
			$smarty->assign("analyticsLinks", $analyticsLinks);
		
		
				$template = dirname ( __FILE__ ). '/../../templates/utilities/newmailers/Emailer_christmas.tpl';
				$subject = "Season's greetings!" ;
		
			//echo $template;
			$mid= $mailObject->sendMail(array($c['email_id']) ,$smarty->fetch($template),"",$subject, $c['user_id']);
			echo "<br>";
			echo "sent to";
			echo  $c['user_id'];
			$count++;
			
		}
		
	}
	echo $count;
}

sendChristmasMailer();

//echo "success"; 


?>