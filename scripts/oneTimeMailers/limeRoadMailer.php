<?php
require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/User.php";
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


class LimeRoadMailer
{
    private $conn_reporting ;
    private $gender ;
    private $smarty ;
    private $subject ;
    private $template ;
    private $mailBody ;
    private $mailObject ;
    private $sendCount = 0;
    private $emailIds = array();

    function __construct($gender)
    {
        global $conn_reporting, $smarty;
        $this->conn_reporting = $conn_reporting;
        $this->smarty = $smarty ;
        $this->mailObject = new MailFunctions();
        $this->gender = $gender;
        $this->getConstants();

    }

    public function sendLimeRoadMailer($target = null)
    {
        if($target == 'all')
        {
            $sql = "select email_id from user u join user_data ud on u.user_id=ud.user_id left join geo_city gc on ud.stay_city=gc.city_id and ud.stay_state= gc.state_id
                where   u.status ='authentic' and u.gender= '$this->gender'
                and email_id is not null and email_status is null" ;
            $res = $this->conn_reporting->Execute($sql) ;
            while($row = $res->FetchRow())
            {
                $email = $row['email_id'] ;
                $this->sendToEmailId($email) ;
            }
        }
        else if(isset($target) && $target != null)
        {
            $this->sendToEmailId($target) ;
        }
        else
        {
            echo "No target";
        }

        echo "sent to " .$this->sendCount ;
        Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com','Mailer sent to ' . $this->sendCount .' users', "Subject line :".$this->subject);
    }

    private function getConstants ()
    {
        if($this->gender == "F")
        {
            $this->subject = "Style the men on TrulyMadly & win!";
            $this->template = dirname ( __FILE__ ). '/../../templates/utilities/newmailers/limeRoadFemale.tpl';

        }
        else
        {
            $this->subject = "This is what women want you to wear!";
            $this->template = dirname ( __FILE__ ). '/../../templates/utilities/newmailers/limeRoadMale.tpl';
        }
        $this->mailBody = $this->smarty->fetch($this->template);
       // echo $this->mailBody; die ;
    }

    private function sendToEmailId($email)
    {
        try
        {
            echo PHP_EOL.'sent to '.$email;
            $mid= $this->mailObject->sendMail(array($email) ,$this->mailBody,"",$this->subject, null);
           //    Utils::sendEmail($email,'admintm@trulymadly.com',$this->subject,$this->mailBody,true);
            $this->sendCount++ ;
        }
        catch (Exception $e)
        {
            echo 'could not send to ' . $email ;
        }

    }
}



try
{
    if(php_sapi_name() === 'cli') {
        $target = $argv[1];
        $gender = $argv[2];

        if( $target == null )
        {
            echo "No Target given, Give first argument an Email id or all"; die ;
        }
        if($gender == null || ($gender != 'F' && $gender !='M'))
        {
            echo "Specify a gender as second Argument, M or F Only"; die ;
        }
        $scenesMailer = New LimeRoadMailer($gender);
        $scenesMailer->sendLimeRoadMailer($target);
    }
}
catch (Exception $e)
{
    trigger_error($e->getMessage(), E_USER_WARNING);
    Utils::sendEmail('sumit@trulymadly.com','sumit@trulymadly.com','Mailer sending failed '.$baseurl,"Error :: ".$e->getMessage());
}




?>