<?php
require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/User.php";
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


function doctorMailer ($target){
    global  $conn_reporting;
    if($target == 'all')
    {
        $sql="SELECT u.user_id, u.email_id,u.gender, u.fname FROM user u join user_data ud on u.user_id = ud.user_id
where status in ('authentic','non-authentic') and email_status is null and email_id is not null
and FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) >= 24 limit 50000";
    }
    else if (isset($target) && $target != null)
    {
        $sql="SELECT u.user_id,'$target' as email_id ,u.gender, u.fname FROM user u join user_data ud on u.user_id = ud.user_id
where status in ('authentic','non-authentic') and email_status is null and email_id is not null
and FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) >= 24 limit 1";
    }
    else
    {
        die ;
    }


    $rs=$conn_reporting->Execute($sql);
    //var_dump($user_ids);

    $mailObject = new MailFunctions();
    $user=  new User();
    $count=0;
    while ($c = $rs->FetchRow())
    {
        $isValid = $user->canSendMail($c['email_id']);
        $name = ucfirst($c['fname']);
        if ($isValid>0) {
            $subject = "Looking for a doctor or health expert? " ;
            $msg = "Hi " .$name ."," . "<br>" . "<br>" .
                  "We are building a platform to simplify healthcare discovery in India and need just a few minutes of yours to take this quick survey. Your inputs will be extremely valuable for anyone looking for doctors, healthcare professionals or advise on medical symptoms."
                  ."<br>" .  "https://www.surveymonkey.com/r/H627TGG" ."<br>" 
                ."<br>" . "Regards,"
                ."<br>" . "Team Docsonline.me";


            $mid= $mailObject->sendMail(array($c['email_id']) ,$msg, "",$subject, $c['user_id'], "survey@docsonline.me");
            echo "<br>";
            echo "$mid sent to ";
            echo  $c['user_id'];
            $count++;
        }

    }
    echo $count;
}
$target = $argv[1];
doctorMailer ($target);

//echo "success";


?>