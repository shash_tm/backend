<?php
require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../include/User.php";
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


class TMScenesMailer
{
    private $conn_reporting ;
    private $smarty ;
    private $subject ;
    private $template ;
    private $mailBody ;
    private $mailObject ;
    private $sendCount = 0;
    private $emailIds = array();

    function __construct()
    {
        global $conn_reporting, $smarty;
        $this->conn_reporting = $conn_reporting;
        $this->smarty = $smarty ;
        $this->mailObject = new MailFunctions();
        $this->getConstants();
    }

    public function sendTMScenesMailer($target = null)
    {
        if($target == 'all')
        {
            $sql = "select email_id from user u join user_data ud on u.user_id=ud.user_id left join geo_city gc on ud.stay_city=gc.city_id and ud.stay_state= gc.state_id
                where   u.status ='authentic' and u.gender='f'
                and email_id is not null and email_status is null" ;
            $res = $this->conn_reporting->Execute($sql) ;
            while($row = $res->FetchRow())
            {
                $email = $row['email_id'] ;
                $this->sendToEmailId($email) ;
            }
        }
        else if(isset($target) && $target != null)
        {
            $this->sendToEmailId($target) ;
        }
        else
        {
            echo "No target";   
        }

        echo "sent to " .$this->sendCount ;
        Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com','Mailer sent to ' . $this->sendCount .' users', "Subject line :".$this->subject);
    }

    private function getConstants ()
    {
        global $cdnurl ;
        $image = $cdnurl . "/images/mailers/referral.jpg";
        $this->smarty->assign("image_url",$image);


        $this->subject = "Do this for your bestie! Won't you?";
        $this->template = dirname ( __FILE__ ). '/../../templates/utilities/newmailers/tmScenes.tpl';
        $this->mailBody = $this->smarty->fetch($this->template);
     //   echo $this->mailBody; die ;
    }

    private function sendToEmailId($email)
    {
        try
        {
            echo PHP_EOL.'sent to '.$email;
            $mid= $this->mailObject->sendMail(array($email) ,$this->mailBody,"",$this->subject, null);
         //  Utils::sendEmail($email,'admintm@trulymadly.com',$this->subject,$this->mailBody,true);
            $this->sendCount++ ;
        }
        catch (Exception $e)
        {
            echo 'could not send to ' . $email ;
        }

    }
}



try
{
    if(php_sapi_name() === 'cli') {
        $target = $argv[1];
        $scenesMailer = New TMScenesMailer();
        $scenesMailer->sendTMScenesMailer($target);
    }
}
catch (Exception $e)
{
    trigger_error($e->getMessage(), E_USER_WARNING);
    echo $e->getMessage() ;
}




?>