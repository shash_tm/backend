<?php 
require_once (dirname ( __FILE__ ) . '/../include/config_admin.php');
require_once (dirname ( __FILE__ ) . '/../include/Utils.php');

class revertToPoll {
	
	private $conn;
	private $conn_master;
	private $rowsAffected = 0;
	private $totalUsers = 0;
	private $user_id = array();
	
	function __construct() {
		global $conn_reporting,$conn_master;
		$this->conn = $conn_reporting;
		$this->conn_master = $conn_master;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	} 
	function getUserId() {
		$threeDayBefore = date('Y-m-d 00:00:00', strtotime('-3 days'));
		
		$sql = $this->conn->Execute("SELECT user_id FROM (SELECT user_id, GROUP_CONCAT(DISTINCT(event_type)) AS error FROM action_log_new
                                            WHERE activity = 'socket'
                                            AND sdk_version = '1.7.8'
                                            AND tstamp >= '$threeDayBefore'
                                            GROUP BY user_id) t
									WHERE t.error NOT REGEXP 'get_missed_messages'
									AND (t.error REGEXP 'error_connection'
											OR t.error REGEXP 'failed_reconnections')"
								);
		
		if($sql->RowCount()>0){
			while ($row = $sql->FetchRow()){
				$this->totalUsers++;
				$this->user_id[] = $row['user_id'];
				$this->updateUserFlag($row['user_id']);
			}
 			$sub = "Revert to polling. Date:".date('Y-m-d H:i:s');
 			$body = "Rows affected :".$this->rowsAffected."\nTotal users affected :".$this->totalUsers."\n Affected user ids are :".implode(',',$this->user_id);
 			Utils::sendEmail("shashwat@trulymadly.com,rajesh.singh@trulymadly.com", "admin@trulymadly.com", $sub,$body);
		}	
	}
	private function updateUserFlag($user_id) {
		$this->conn_master->Execute($this->conn_master->prepare("insert into user_flags(user_id,hide_myself,is_socket_enabled) values(?,?,?) on DUPLICATE KEY UPDATE is_socket_enabled=?"),
				array($user_id,null,'no','no'));
		$this->rowsAffected = $this->rowsAffected+$this->conn_master->Affected_Rows();
	}
}

try {
	
	if(PHP_SAPI !== 'cli') {
		die();
	}
	$revert = new revertToPoll();
	$revert->getUserId();
} catch (Exception $e) {
	echo $e->getMessage();
}

?>