<?php 
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ )  . "/../../include/config_admin.php";
require_once dirname ( __FILE__ )  . "/../../DBO/messagesDBO.php";
require_once dirname ( __FILE__ )  . "/../../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ )  . "/../../utilities/counter.php";

/* This class selects all the user ids which are to be deleted from the DB and deletes their personal data
 * @Sumit  */

class DELETEUSER 
{
	private $conn_reporting;
	private $conn_master ;
	private $param_prepare ;
	private $message_DBO ; 
	static $max_delete_count = 1000 ; 
	
	function __construct()
	{
		global $conn_master, $conn_reporting ;
		$this->conn_master = $conn_master ;
		$this->conn_reporting = $conn_reporting ;
		$this->param_prepare = null ;
		$this->message_DBO = new MessagesDBO() ;
	}
	
	
	public function deleteIdProof()
	{
		$query="select img_location from user_id_proof_log where status!='under_moderation' and tstamp<date_sub(curdate(),interval '7' day) and s3_status='not_deleted'";
		$param_array=NULL;
		$rs=Query_Wrapper::SELECT($query,$param_array);
		
		foreach ($rs as $value) {
			$data=$value['img_location'];
			//var_dump($data);
			Utils::deleteFromS3(array($data));
			Utils::deleteFromDisk(array($data));
			
			$query="update user_id_proof_log set s3_status='deleted' where img_location=?";
			$param_array=array($value['img_location']);
			$tablename='user_id_proof_log';
			Query_Wrapper::UPDATE($query,$param_array,$tablename);
			
			$query="update user_id_proof set s3_status='deleted' where img_location=?";
			$param_array=array($value['img_location']);
			$tablename='user_id_proof';
			Query_Wrapper::UPDATE($query,$param_array,$tablename);
		}

	}


	public function deleteSharedPhoto()
	{
		$sql = "select photo_id, image, thumbnail from user_shared_photo where s3_status = 'active' and DATEDIFF(NOW(),tstamp) >30";
		$sql_exec = $this->conn_reporting->Execute($sql) ;
		while($row = $sql_exec->FetchRow())
		{
			$photo_id = $row['photo_id'];
			$image= $row['image'] ;
			$thumbnail = $row['thumbnail'] ;
			$image_webp = rtrim($image, '.jpg')  . ".webp";
			$thumbnail_webp = rtrim($thumbnail,'.jpg') . ".webp" ;
			Utils::deleteFromS3(array($image,$image_webp, $thumbnail,$thumbnail_webp));
			$sql_update = "UPDATE user_shared_photo SET s3_status= 'deleted' WHERE photo_id= ?" ;
			$sql_update_prep = $this->conn_master->Prepare($sql_update);
			$this->conn_master->Execute($sql_update_prep, array($photo_id));
		}
	}


	
	public function performAction ($manual)
	{
		echo "getting user Ids";
		$user_array = $this->getUsersForDeletion();
		$cu = count($user_array) ;
		//var_dump(count($user_array)) ; die ;
		if($cu == 0)
		{
			echo "returning because of zero" ;
			return ;
		}
			
		if ( $cu < DELETEUSER::$max_delete_count || $manual == "manual_deletion" )
		{
			
		    $this->param_prepare = $this->getParamPrepareCount($user_array) ;
		    
		    //delete from user_facebook
		    echo "deleting from facebook" ;
		    $this->deleteFacebook($user_array) ;
		    
		    //delete from Linkdin
		    echo "deleting form linekdin";
		    $this->deleteLinkedin($user_array) ;
		    
		    //delete from user_phone_number
		    echo "deleting from user phone number";
		    $this->deletePhoneNumber($user_array);
		    
		    
		    //delete user_data
		    //  $this->deleteUserData($user_array);
		    
		    
		    //delete user_preference_data
		    //     $this->deletePreferenceData($user_array) ;
		    
		    //delete from user photo
		    $this->deletePhoto($user_array) ;
		    
		    // block conversations
		    $this->blockConversations($user_array) ;
		    
		    //wipe data from user_table
		    $this->wipeUserTable($user_array) ;
		    
		    //mark deleted in deletion_log
		    //  $this->markDeleted($user_array) ;
		    
		    return count($user_array) ;
		} else 
		{
			$subject = "Deletion script stopped";
			$message = "Deletion script stopped because " . $cu .  " users were to be deleted" ;
			echo $message ;
			Utils::sendEmail('shashwat@trulymadly.com, sumit@trulymadly.com', 'admintm@trulymadly.com', $subject, $message);
			
		}
		
		
	}
	
	/*  Select all those user Ids which are to be deleted*/
	private function getUsersForDeletion() 
	{
		$sql = "SELECT user_id FROM user WHERE deletion_status ='mark_deleted' ";
		$user_res = $this->conn_reporting->Execute($sql);
		
		$user_ids_array = array ();
		while ($row = $user_res->FetchRow())  
		{
			$user_ids_array[] = $row['user_id'];
		}
		
		return $user_ids_array;
	}
	  
	private function deleteFacebook ($user_array) 
	{
		$sql_select = "SELECT user_id, fid from user_facebook where user_id in ( " . $this->param_prepare . " )" ;
		$prep_select = $this->conn_reporting->Prepare ($sql_select) ;
	 	$exec_select = $this->conn_reporting->Execute ($prep_select, $user_array) ; 
		
		$data_insert = array();
		while ($row = $exec_select->FetchRow()) 
		{
			$temp_array = array( ) ;
			$temp_array ['user_id'] = $row['user_id'] ;
			$temp_array ['fid'] = $row['fid'] ;
			$data_insert[] = $temp_array ;
		}
		
		if ($data_insert != array() )
		{
			$query="INSERT IGNORE INTO user_facebook_archive (user_id, fid) values ( ?, ?) ";
			$param_array=$data_insert;
			$tablename="user_facebook_archive";
			$insert_exec=Query_Wrapper::BulkINSERT($query, $param_array, $tablename);
		 	/*$sql_insert = "INSERT IGNORE INTO user_facebook_archive (user_id, fid) values ( ?, ?) " ;
		 	$insert_prep = $this->conn_master->Prepare ($sql_insert);
			$this->conn_master->bulkBind = true;
			$insert_exec = $this->conn_master->Execute ($insert_prep, $data_insert) ;*/
		}
		
		$query="UPDATE user_facebook set fid = NULL, email = NULL WHERE user_id IN ( " . $this->param_prepare . " )";
		$param_array=$user_array;
		$tablename="user_facebook";
		$insert_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
		
		
		/*$sql ="UPDATE user_facebook set fid = NULL, email = NULL WHERE user_id IN ( " . $this->param_prepare . " )";
		$sql_prep = $this->conn_master->Prepare($sql);
		$sql_exec = $this->conn_master->Execute($sql_prep, $user_array) ;*/  
	}
	
	private function deleteLinkedin ($user_array) 
	{
		$sql_select = "SELECT user_id, member_id, name, linkedin_user_data  from user_linkedin where user_id in ( " . $this->param_prepare . " )" ;
		$prep_select = $this->conn_reporting->Prepare ($sql_select) ;
		$exec_select = $this->conn_reporting->Execute ($prep_select, $user_array) ;
		
		$data_insert = array();
		while ($row = $exec_select->FetchRow())
		{
			$temp_array = array( ) ;
			$temp_array ['user_id'] = $row ['user_id'] ;
			$temp_array ['member_id'] = $row ['member_id'] ;
			$temp_array ['name'] = $row ['name'] ;
			$temp_array ['linkedin_user_data'] = $row ['linkedin_user_data'] ;
			$data_insert[] = $temp_array ;
		} 
		if ($data_insert != array() ) 
		{
			
			$query="INSERT IGNORE INTO user_linkedin_archive (user_id, member_id, name, linkedin_user_data) values ( ?, ?, ?, ?) ";
			$param_array=$data_insert;
			$tablename="user_linkedin_archive";
			$insert_exec=Query_Wrapper::BulkINSERT($query, $param_array, $tablename);
			
			/*$sql_insert = "INSERT IGNORE INTO user_linkedin_archive (user_id, member_id, name, linkedin_user_data) values ( ?, ?, ?, ?) " ;
			$insert_prep = $this->conn_master->Prepare ($sql_insert);
			$this->conn_master->bulkBind = true;
			$insert_exec = $this->conn_master->Execute ($insert_prep, $data_insert) ;*/
		}
		
		$query="UPDATE user_linkedin SET  member_id = NULL, name = NULL, linkedin_user_data = NULL  WHERE user_id IN ( " . $this->param_prepare . " )";
		$param_array=$user_array;
		$tablename="user_linkedin";
		$insert_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
		
		/*$sql ="UPDATE user_linkedin SET  member_id = NULL, name = NULL, linkedin_user_data = NULL  WHERE user_id IN ( " . $this->param_prepare . " )";
		$sql_prep = $this->conn_master->Prepare($sql);
		$sql_exec = $this->conn_master->Execute($sql_prep, $user_array) ; */
	}
	
	private function deletePhoneNumber ($user_array)
	{
		$sql_select = "SELECT user_id, user_number  from user_phone_number where user_id in ( " . $this->param_prepare . " )" ;
		$prep_select = $this->conn_reporting->Prepare ($sql_select) ;
		$exec_select = $this->conn_reporting->Execute ($prep_select, $user_array) ;
		
		$data_insert = array();
		while ($row = $exec_select->FetchRow())
		{
			$temp_array = array( ) ;
			$temp_array ['user_id'] = $row ['user_id'] ;
			$temp_array ['user_number'] = $row ['user_number'] ;
			$data_insert[] = $temp_array ;
		}
		
		if ($data_insert != array() )
		{
			
			
			$query="INSERT IGNORE INTO user_phone_number_archive (user_id, user_number) values ( ?, ?) ";
			$param_array=$data_insert;
			$tablename="user_phone_number_archive";
			$insert_exec=Query_Wrapper::BulkINSERT($query, $param_array, $tablename);
				
		/*$sql_insert = "INSERT IGNORE INTO user_phone_number_archive (user_id, user_number) values ( ?, ?) " ;
		$insert_prep = $this->conn_master->Prepare ($sql_insert);
		$this->conn_master->bulkBind = true;
		$insert_exec = $this->conn_master->Execute ($insert_prep, $data_insert) ;*/
		}
		
		
		$query="UPDATE user_phone_number SET user_number = NULL WHERE user_id IN ( " . $this->param_prepare . " )  ";
		$param_array=$user_array;
		$tablename="user_phone_number";
		$insert_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
			
		/*$sql = "UPDATE user_phone_number SET user_number = NULL WHERE user_id IN ( " . $this->param_prepare . " ) " ;
		$sql_prep = $this->conn_master->Prepare($sql);
		$sql_exec = $this->conn_master->Execute($sql_prep, $user_array) ; */
	}
	
	private function deletePhoto ($user_array)
	{
		$sql_select = "SELECT name, thumbnail ,blur_image, blur_thumbnail from user_photo where user_id in ( " . $this->param_prepare . " )" ;
		$prep_select = $this->conn_reporting->Prepare ($sql_select) ;
		$exec_select = $this->conn_reporting->Execute ($prep_select, $user_array) ;
		
		$data = array( );
		while ($row = $exec_select->FetchRow())
		{
			$data[] = $row ['name'];
			$data[] = $row ['thumbnail'] ;
			$data[] = $row ['blur_image'] ;
			$data[] = $row ['blur_thumbnail'] ;			
		}
		if ($data != array() )
		  Utils::deleteFromS3($data) ;
		
		
		$query="UPDATE user_photo SET status ='system_deleted' WHERE user_id IN ( " . $this->param_prepare . " ) ";
		$param_array=$user_array;
		$tablename="user_photo";
		$insert_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
		
		/*$sql = "UPDATE user_photo SET status ='system_deleted' WHERE user_id IN ( " . $this->param_prepare . " ) " ;
		$sql_prep = $this->conn_master->Prepare ($sql) ;
		$sql_exec = $this->conn_master->Execute ($sql_prep, $user_array) ; */
	}
	
	private function deleteUserIdProof ($user_array)
	{
		$sql_select = "Select img_location from user_id_proof where user_id in ( " . $this->param_prepare . " )" ;
		$prep_select = $this->conn_reporting->Prepare ($sql_select) ;
		$exec_select = $this->conn_reporting->Execute ($prep_select, $user_array) ;
		
		$data = array() ;
		while ($row = $exec_select->FetchRow()) 
		{
			$data[] = $row ['img_location'] ;
		}
		if ($data != array() )
		    Utils::deleteFromS3($data);
	}
	
	private function deleteUserData ($user_array) 
	{
		
		$query="DELETE FROM user_data WHERE user_id IN ( " . $this->param_prepare . " ) ";
		$param_array=$user_array;
		$tablename="user_data";
		$insert_exec=Query_Wrapper::DELETE($query, $param_array, $tablename);
		
	 /* $sql = "DELETE FROM user_data WHERE user_id IN ( " . $this->param_prepare . " ) " ;
		$sql_prep = $this->conn_master->Prepare($sql);
		$sql_exec = $this->conn_master->Execute($sql_prep, $user_array) ; */
	}
	
	
    private function deletePreferenceData ($user_array) 
	{
		
		$query="DELETE FROM user_preference_data WHERE user_id IN ( " . $this->param_prepare . " ) ";
		$param_array=$user_array;
		$tablename="user_preference_data";
		$insert_exec=Query_Wrapper::DELETE($query, $param_array, $tablename);
		
	 /* $sql = "DELETE FROM user_preference_data WHERE user_id IN ( " . $this->param_prepare . " ) " ;
		$sql_prep = $this->conn_master->Prepare($sql);
		$sql_exec = $this->conn_master->Execute($sql_prep, $user_array) ; */
	}
	
	private function blockConversations ($param_array)
	{
		$counterObj = new Counters();
		$sql = "SELECT sender_id, receiver_id FROM messages_queue WHERE sender_id IN ( " . $this->param_prepare . " ) AND blocked_by IS NULL" ; 
		$sql_prep = $this->conn_reporting->Prepare ($sql) ;
		$sql_res = $this->conn_reporting->Execute ($sql_prep, $param_array) ;
		
		while ($row = $sql_res->FetchRow()) 
		{
			$sender_id = $row ['sender_id'];
			$receiver_id = $row ['receiver_id'];
			
			// block in conversations
			$this->message_DBO->block($sender_id, $receiver_id) ;
			
			//remove counter from redis
			$counterObj->clearUnreadCount($sender_id,$receiver_id);
			$counterObj->clearUnreadCount($receiver_id,$sender_id);
		}	
	}
	
	private function wipeUserTable ( $param_array)
	{
		
		$query="UPDATE user SET lname = null , email_id = null , password =   null , fid = null , deletion_status = 'deleted' 
				   where user_id in ( " . $this->param_prepare . " ) ";
		$tablename="user";
		$insert_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
		
		/*$sql = "UPDATE user SET lname = null , email_id = null , password =   null , fid = null , is_fb_connected = null , deletion_status = 'deleted' 
				   where user_id in ( " . $this->param_prepare . " ) " ;
		$sql_prep = $this->conn_master->Prepare ($sql) ;
		$sql_res = $this->conn_master->Execute ($sql_prep, $param_array) ;*/
	}
	
	private function markDeleted ($param_array)
	{
		$query="UPDATE user_deletion_log SET status = 'deleted' WHERE user_id IN ( " . $this->param_prepare . " ) ";
		$tablename="user_deletion_log";
		$insert_exec=Query_Wrapper::UPDATE($query, $param_array, $tablename);
		
		
		/*$sql = "UPDATE user_deletion_log SET status = 'deleted' WHERE user_id IN ( " . $this->param_prepare . " ) "; 
		$sql_prep = $this->conn_master->Prepare ($sql) ;
		$sql_exec = $this->conn_master->Execute ($sql_prep, $param_array ); */
	}
	
	private function getParamPrepareCount ($param_array)
	{
		$param_count = count($param_array) ; 
		$in_param =  trim(str_repeat('?, ', $param_count), ', ');
		return $in_param ;
	}
	
}

try 
{
	if(php_sapi_name() == 'cli'   )
	{

		$options=getopt ( "d:" );
		if ($options[d])
		{
			$manual = $options[d];
		}
		
		
		$deleteUser = new DELETEUSER();
		
		//function to delete 1 week old ID Proofs
		$deleteUser->deleteIdProof();

		$deleteUser->deleteSharedPhoto();

		
		$count= $deleteUser->performAction($manual);
		if ($count > 0)
		{
			$date =  date('Y-m-d') ;
			$subject = "Users deletion Performed on $count users on $date";
			$message = $count . " users deleted" ;
			echo $message ;
			Utils::sendEmail('shashwat@trulymadly.com,sumit@trulymadly.com', 'admintm@trulymadly.com', $subject, $message) ;
		}
	}
	
}
 catch (Exception $e) 
{
	trigger_error($e->getMessage(), E_USER_WARNING) ;
}



?>