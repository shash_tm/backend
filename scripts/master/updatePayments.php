<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../spark/Paytm_App_Checksum_Kit_PHP/lib/encdec_paytm.php";
require_once dirname ( __FILE__ ) . "/../../spark/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../../mobile_utilities/pushNotification.php";

/*
 * Cron to update the user_app_status table from user_gcm_current 
 * Updated by @Sumit
 * 🖖 Live Long and Prosper.
 */
function notifyTechTeam($msg){
	global  $baseurl;
	$subject = "Paytm reconciliation result for " . $baseurl;
	$from = "admintm@trulymadly.com";
	$to = "backend@trulymadly.com" ;
	Utils::sendEmail($to, $from, $subject, $msg); 
}

function getPaytmStatus($orderid, $sku, $user_id, $user_name, $package_type, $time_diff)
{
	global $paytmStatusUrl;
	global $paytmMID;
	$result = array();
	$reqestParamList = array("MID"=>$paytmMID,"ORDERID"=>$orderid);
	$paramList = getTxnStatus($reqestParamList, $paytmStatusUrl);
	if($paramList['STATUS'] == 'TXN_SUCCESS' || $paramList['STATUS'] == 'TXN_FAILURE'){
		$spark = new Spark($user_id);
		$output = $spark->sparkPaymentForPaytm($sku, null, $paramList['ORDERID'],$paramList['TXNID'],$paramList['STATUS'], true, true);
		if(isset($output['new_sparks_added']) && $output['new_sparks_added'] >0 ){
			if($paramList['STATUS'] == 'TXN_SUCCESS') {
				$result[$orderid] = "SPARK - $user_id, TIME INTERVAL - $time_diff MINUTES";
				sendNotification($user_id, $user_name, "spark");
			}
		}
		else if(isset($output['remove_sparks_manually'])){
			$result[$orderid] = "Sparks are required to be removed manually. I am a cron and not authorized to perform such an action";
		}
		else if(isset($output['my_select']) && isset($output['my_select']['status']) && $output['my_select']['status'] == 'active'){
			if($paramList['STATUS'] == 'TXN_SUCCESS') {
				$result[$orderid] = "SELECT - $user_id, TIME INTERVAL - $time_diff MINUTES";
				sendNotification($user_id, $user_name, "select");
			}
		}
	}
	return $result;
}

function reconcilePayments(){
	global  $conn_reporting;
	$sql = "select p.payment_id, pk.sku, p.user_id, pk.type, u.fname, TIMESTAMPDIFF(MINUTE,p.tstamp, now()) as time_diff from user_spark_transaction p ".
		" JOIN spark_packages pk on pk.package_id = p.package_id".
		" JOIN user u on p.user_id = u.user_id".
		" where p.payment_gateway = 'paytm' and p.status in ('initiated','purchased')  ".
		" and p.tstamp > DATE_SUB(now(), INTERVAL 1 HOUR) ";
	//echo $sql;
	$res = $conn_reporting->Execute($sql);
	$result = array();
	$total_count = 0;
	while ($val = $res->FetchRow())
	{
		$result[] = getPaytmStatus($val['payment_id'], $val['sku'], $val['user_id'], $val['fname'], $val['type'], $val['time_diff']);
		$total_count++;
	}
	return $result;
}

function sendNotification( $user_id, $user_name, $package_type){
	$ticker_text= 'Message from TrulyMadly';
	$title_text= 'TrulyMadly';
	$push_notification = new pushNotification();
	global $admin_id;
	$data['ticker_text'] = $ticker_text;
	$title = "Hey $user_name!";
	$content = $package_type == 'spark' ? "Your Sparks pack is now active. Get Sparking!":"Welcome to TM Select. Now meet other singles who are #ReadyForMore";
	$data = array(
		'title_text' =>$title,
		'content_text' => $content,
		'type' => 'PROMOTE',
		'push_type' => 'PROMOTE',
		'category_id' => NULL,
		'categoryTitle' => NULL,
		'event_status' => 'PENDING_PAYMENT'
	);

	$push_notification->notify($user_id,
		$data,
		$admin_id);
}

try{
	if(php_sapi_name() == 'cli')
	{
		$file_name = "/tmp/updatePayments";
		$fptr = fopen($file_name, "c");

		if (flock($fptr, LOCK_EX|LOCK_NB, $wouldblock))
//		  if (flock($fp, LOCK_EX | LOCK_NB))
		{
			$isLocked = 0 ;
			// lock obtained
			if(flock($fptr,LOCK_EX) == false){
				throw new Exception("unable to acquire lock");
			}else{
				echo "lock acquired";
				$lockStatus = true ;
//				$gmtTime = $this->getTime();
//				Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "No previous lock found ".$this->baseurl , " Lock acquired ". $gmtTime  );
			}
		}
		else
		{
			if ($wouldblock) {
				echo "already locked";
				$isLocked = 1 ;
				$lockStatus = false ;
				// something already has a lock
			}
			else
			{
				echo "could not lock";
				// couldn't lock for some other reason
			}

		}

		if($lockStatus == true)
		{
			$data = reconcilePayments();
			$notify=0;
			foreach($data as $key => $value){
				if(is_array($value) && count($value) == 0){
					unset($data[$key]);
				}else{
					$notify++;
				}
			}
			if($notify > 0) {
				notifyTechTeam(json_encode($data));
			}
		}

	}
	else 
	{
		$msg = "update payments was tried via CLI from IP " . $_SERVER['REMOTE_ADDR'] ;
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != null)
		{
			$msg .= " from forwarded from " . $_SERVER['HTTP_X_FORWARDED_FOR'] ;
		}
	//	notifyTechTeam($msg);
	}
}
catch(Exception $e){
	$msg = "Update paytm payment script failed: ";
	notifyTechTeam($msg);
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}
?>