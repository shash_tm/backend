<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";


class Shuffle_Pic
{
	const timeQuantom=6; //hours
	const tablename="photo_views_male"; 
	const viewsThreshold=20;
	const likePerc=1.5;
	const escortThreshold=100;
	
	private $conn;
	private $time;
	private $tempTable;
	private $extra_photoids;
	private $isfirstime;
	private $inittime;
	function __construct($isfirsttime,$daysback=-1) {
		global $conn_reporting,$conn_master;
		
		$this->conn=$conn_reporting;
		$this->conn_master=$conn_master;
		$this->tempTable="photo_views_male_temp";
		$this->isfirstime=$isfirsttime;
	    if($daysback!=-1)
	    {
	    	$this->inittime=$daysback;
	    }
	}
	
	
	
   private function getFileName(){
		$completePath="/tmp/profile_pic_shuffling.txt";
		chmod($completePath, 0777);
		return $completePath;
	}
	
   private function loadData(){
		global $baseurl;
		try{
			$dropTempTable = "DROP TABLE IF EXISTS $this->tempTable";
			//var_dump($dropTempTable);
			$this->conn_master->Execute($dropTempTable);
			//echo "\n Table Dropped\n";
			$sql_create="CREATE TABLE $this->tempTable (
			`photo_id` int(11) NOT NULL,
			`user_id` int(11) NOT NULL,
			`likes` int(10) DEFAULT '0',
			`hides` int(10) DEFAULT '0',
			`last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`shuffled` int(2) DEFAULT '0',
			PRIMARY KEY (`photo_id`),
			KEY `key1` (`user_id`),
			KEY `key2` (`shuffled`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1";
	
			$this->conn_master->Execute($sql_create);
			//echo "\ntable created\n";
			$completePath = $this->getFileName();
			//var_dump($completePath);
			if(filesize($completePath) > 0 ){
				$sql = "LOAD DATA LOCAL INFILE '$completePath' INTO TABLE $this->tempTable  fields terminated by '\t' ";
				//echo $sql;
				
				$ex = $this->conn_master->Execute($sql);
				echo "\nloaded data into temp table\n";
				// var_dump($conn_master->isWarning());
				$warningQuery = "SHOW WARNINGS";
				$warnings = $this->conn_master->Execute($warningQuery);
				$warningCount = $warnings->RowCount();
				//var_dump($warningCount);
				if($warningCount > 0 ){
					$msg = "Warnings in loading data (Profile pic shuffle ):" . $warningCount ."\n" ;
					for($i=0 ; $i<$warningCount;$i++){
						while ($row = $warnings->FetchRow()) {
							foreach ($row as $val){
								$msg .= "$val\t";
							}
							$msg .="\n";
						}
					}
					//var_dump($msg);
					echo $msg;
				}
			}else {
				$to = "raghav@trulymadly.com";
				$subject = "(No Warnings) Profile Pic Shuffling file on tmp directory is empty for" . $baseurl ;
				$from = "admintm@trulymadly.com";
				Utils::sendEmail($to, $from, $subject,'',false);
			}
		
			$sql_update="INSERT INTO `photo_views_male`
			(photo_id,user_id,likes,hides,last_updated,shuffled)
			select photo_id,user_id,likes,hides,last_updated,shuffled from $this->tempTable
			ON DUPLICATE KEY UPDATE
			likes =$this->tempTable.likes + photo_views_male.likes,
			hides=$this->tempTable.hides + photo_views_male.hides,
			last_updated=$this->tempTable.last_updated,
			shuffled=$this->tempTable.shuffled";
				
			$this->conn_master->Execute($sql_update);
			
			echo "Photo Views Male Table updated".PHP_EOL;
			$dropTable = "DROP TABLE IF EXISTS $this->tempTable";
			//Query::UPDATE($dropTable, null);
			$this->conn_master->Execute($dropTable);
			//echo "\ntable Dropped\n";
	
		}
		catch (Exception $e){
			trigger_error($e->getMessage());
			echo $e->getTraceAsString();
		}
	}
	
	
   public function setSystemDpForChangedUsers()
   {

	    $notempty=true;
	    $user_ids="select (user_id) as uid from user_photo where  status='active' and admin_approved='yes'  group by user_id having sum(if(system_profile='yes',1,0))=0 and sum(if(is_profile='yes',1,0))>0";
	    if($notempty)
		{
			//echo $user_ids;
			$rs=$this->conn->Execute($user_ids);
			$userids_array=array();
			$csv_uids='';
			while($row=$rs->fetchRow())
			{
				$userids_array[]=$row['uid'];
			}
			if(count($userids_array)>0)
			{
				$csv_uids=implode(',',$userids_array);
			}
		
			if($csv_uids!='')
			{
				$uids=$csv_uids;
				$query="select group_concat(photo_id) as ps from user_photo where user_id in ($uids) and is_profile='yes' and status='active' and admin_approved='yes' ";
				$rs=$this->conn->Execute($query);
				//Check for the warnings from the previous query 
				$warningQuery = "SHOW WARNINGS";
				$warnings = $this->conn->Execute($warningQuery);
				$warningCount = $warnings->RowCount();
				//var_dump($warningCount);
				if($warningCount > 0 ){
					$msg = "Warnings for Group Concat" . $warningCount ."\n" ;
					for($i=0 ; $i<$warningCount;$i++){
						while ($row = $warnings->FetchRow()) {
							foreach ($row as $val){
								$msg .= "$val\t";
							}
							$msg .="\n";
						}
					}
					//var_dump($msg);
					echo $msg.PHP_EOL;
					Utils::sendEmail('raghav@trulymadly.com', 'admintm@trulymadly.com', 'PhotoShuffling Warnings',$msg ,false);
				}
				
				$csv_photoids=$rs->fetchRow();
				if($csv_photoids['ps']!=null)
				{
					$this->extraids=$csv_photoids['ps'];
					//var_dump($this->extraids);
				   
				}
				else 
				{
					
					$this->extraids=NULL;
				}
			}
			else
			{
				$this->extraids=NULL;
			}
		//var_dump($this->extraids);
		
		//$sql="update user_photo set system_profile='yes' where photo_id  and is_profile='yes' and  admin_approved='yes' and status='active')";
	}
}
	
	public function MarkSystemInMaster()
	{	//At the end of the script , all this ids in the variable will be marked as system_profile 
		if($this->extraids!=NULL)
		{
				$photo_array=explode(',',$this->extraids);
				if(count($photo_array)>0)
				{
				
					try {	
					$param_count = Query_Wrapper::getParamCount($photo_array);
					$query="update user_photo set system_profile='yes' where photo_id in ($param_count)";
					Query_Wrapper::UPDATE($query,($photo_array),"user_photo","master",null);
					
					Utils::sendEmail('raghav@trulymadly.com', 'admintm@trulymadly.com', 'Update Query Photo Shuffling',count($photo_array).' Pics Updated' ,false);
						
					//echo 'Executed';
			    }
				catch(Exception $ex)
				{
					
					trigger_error($e->getMessage());
				}
			}
	
		}
	}
	

	public function UpdatedProfilePics()
	{
		//If the user has updated its profile pic in the last day,make it a priority to shuffle if the old pic is eliglble
		//For the shuffling to be done
		echo 'Checking for UpdateProfilePics for the last day'.PHP_EOL;
		//$user_id_sql="select user_id as uid from user_photo where time_of_saving>='$this->time' and status='active' and admin_approved='yes' group by user_id having sum(if(system_profile='yes',1,0))>0";
	    $user_id_sql="select user_photo.user_id as uid from user_photo join user on user_photo.user_id=user.user_id where time_of_saving>='$this->time' and user_photo.status='active' and admin_approved='yes' and user.gender='M' group by user_photo.user_id having sum(if(system_profile='yes',1,0))>0;";
		$rs=$this->conn->Execute($user_id_sql);
	  
	    while ($row=$rs->fetchRow())
	    {
	    	
	    	$user_id=$row['uid'];
	    	$profile_pic_sql="select photo_id from user_photo where user_id=$user_id and is_profile='yes' and status='active' and admin_approved='yes' and time_of_saving>='$this->time'";
	    	
	    	$profile_rs=$this->conn->Execute($profile_pic_sql);
	        if($profile_rs->rowCount()>0)
	        {
	    	
	        	$new_photo=$profile_rs->fetchRow();
		    	$new_photo_id=$new_photo['photo_id'];
		    	
		    	$system_pic_sql="select photo_id from user_photo where user_id=$user_id and system_profile='yes'";
		    	$system_rs=$this->conn->Execute($system_pic_sql);
		    	$old_photo=$system_rs->fetchRow();
		    	$old_photo_id=$old_photo['photo_id'];
		    	
		    	
		    
		    	//Now get the hides and likes of the old photo
		    	$views=$this->getLikeAndHide($old_photo_id);
		        if($views["total"]>=$this::viewsThreshold && $views["like_perc"]<$this::likePerc)
		    	{
		    		//The Old Pic is Eligible for Shuffling,Shuffle with the current profile pic
		    		if($new_photo_id!=$old_photo_id)
		    		{
		    			$this->changeSystem_Profile($user_id,$old_photo_id,$new_photo_id);
		    		}
		    	}
	   	
	    	}
	 
		}
	
	}
	
	public function populate_db()
	{
		$filename=$this->getFileName();
		
		if(file_exists($filename))
			unlink($filename);
			
		echo "\nGetting Checkpoint Value\n";
		$sql_time="select checkpoint from profile_pic_shuffling_checkpoint";
        $rs=$this->conn->Execute($sql_time);
        foreach ($rs as $value)
        {
        	
        	$this->time=$value['checkpoint'];
        	echo 'Calculating Since '.$this->time;
        	//var_dump($this->time);
        }
        $sql_time_update="update profile_pic_shuffling_checkpoint set checkpoint=now(), last_checkpoint=? ";
        
        Query_Wrapper::UPDATE($sql_time_update,$this->time,"profile_pic_shuffling_checkpoint");
         
        if(!$this->isfirstime)  
        {
        	echo 'Accounting for New Registrations having no system_flags as true'.PHP_EOL;
	        $this->setSystemDpForChangedUsers();
	    }
	    
	     
	     
	    
        //var_dump($this->time);
		//selecting likes and hides for last 6 hours
		
        echo "\nGetting users and there likes and hides\n";
        
        $sql_select="select us.user_id, ifnull(ul.likes,0) as likes, ifnull(uh.hides,0) as hides from user_search us left join (select user2, count(user1) as likes from user_like where timestamp>= '$this->time' and user1 not in (select user_id from user u join (select count(user2) as count,user1 from user_like where timestamp>=date_sub(now(),interval 1 day) group by user1 having count>100)l  on l.user1=u.user_id where u.gender='F') and user1 not in (1112486,1112539,1034621,1032546,1061247,1126423) and user1 not in (select user_id from advertisement_campaign) group by user2)ul on us.user_id=ul.user2 left join (select user2, count(user1) as hides from user_hide where timestamp>= '$this->time'  group by user2)uh on us.user_id=uh.user2 where gender='M'";
        
        $rs=$this->conn->Execute($sql_select);
        echo "\nGetting users and there likes and hides -Done\n";
		//var_dump($rs->GetRows());
		//$rs=Query_Wrapper::SELECT($sql_select,array());
		$timestamp = date('Y-m-d G:i:s');
			foreach($rs as $value)
			{
				
				$sql_profile_pic="select photo_id, shuffled from user_photo where (user_id=?  and status='active' and system_profile='yes' and admin_approved='yes')";
				
				$param_array=array($value['user_id']);
				$photo_data_conn=$this->conn->Execute($sql_profile_pic,$param_array);
				$photo_data=$photo_data_conn->GetRows();
				$count=$photo_data_conn->rowCount();
				//echo 'The count is '.$count.PHP_EOL;
				//var_dump($photo_data);
	            if($count>0)
	            {
					$str = '';
					$str .= (!isset($photo_data[0]['photo_id']))?"\N\t":$photo_data[0]['photo_id']."\t";
					$str .= (!isset($value['user_id']))?"\N\t":$value['user_id']."\t";
					$str .= (!isset($value['likes']))?"\N\t":$value['likes']."\t";
					$str .= (!isset($value['hides']))?"\N\t":$value['hides']."\t";
					$str .= $timestamp."\t";
					$str .= (!isset($photo_data[0]['shuffled']))?"\N\t":$photo_data[0]['shuffled']."\t";
	   				$str .="\n";
	   				   			
	   				file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
	            }
				
			}
			echo "\nPopulating photo_views_male\n";
			$this->loadData();
		    
			echo "\nIdle for 2 Minutes, So That changes are reflected in Slaves\n";
			sleep(120);
			
	}
	
	
	
	//Not to be used now,shuffled flag now signifies the number of times the pic has been shuffled
	private function update_shuffled_flag()
	{
		//updating shuffled flag on total views
		$sql_select="select * from photo_views_male where user_id in (select user_id from user_search where gender='M')";
		$rs=$this->conn->Execute($sql_select);
		foreach ($rs as $value) {
			if($value["likes"]+$value["hides"]>=20)
				$new_shuffled=2;
			else if ($value["likes"]+$value["hides"]>=10)
				$new_shuffled=1;
			else
				$new_shuffled=0;
			
			if($new_shuffled!=$value["shuffled"])
			{
				
				$sql="update user_photo set shuffled=? where photo_id=?";
				$param_array=array($new_shuffled,$value["photo_id"]);
				$tablename="user_photo";
				Query_Wrapper::INSERT($sql,$param_array,$tablename,"master",$value["user_id"]);
				
				$sql="update photo_views_male set shuffled=? where photo_id=?";
				$param_array=array($new_shuffled,$value["photo_id"]);
				$tablename="photo_views_male";
				Query_Wrapper::INSERT($sql,$param_array,$tablename,"master",$value["user_id"]);
			}
		}
	}
	
	
	public function InitializeSystem()
	{
	    echo 'Initialising System'.PHP_EOL;
		$sql="update user_photo set system_profile='no',shuffled=0";
		Query_Wrapper::UPDATE($sql,null,"user_photo","master",null);
		$sql="update user_photo set system_profile='yes' where is_profile='yes' and admin_approved='yes' and status='active'";
		Query_Wrapper::UPDATE($sql,null,"user_photo","master",null);
		$sql="delete from photo_views_male";
		Query_Wrapper::DELETE($sql,null,"photo_views_male","master",null);
		$sql="update profile_pic_shuffling_checkpoint set checkpoint=date_sub(now(),interval $this->inittime day)";
		Query_Wrapper::UPDATE($sql,null,"profile_pic_shuffling_checkpoint","master",null);
		
		$sql="delete from photo_reshuffling_user_checkpoint";
		$this->conn->Execute($sql);
		
	    
		echo 'Waiting for the Changes to be reflected in slave'.PHP_EOL;
		
		sleep(60);
		
		
	}
	
	
	private function getLikeAndHide($photo_id)
	{
		$views=array();
		$sql="select * from photo_views_male where photo_id =?";
		//$rs=Query_Wrapper::SELECT($sql,array($photo_id),"slave",true);
		$rs=$this->conn->Execute($sql,array($photo_id));
		$rs=$rs->FetchRow();
		if($rs!==false)
		{
			$views["likes"]=$rs["likes"];
			$views["hides"]=$rs["hides"];
			$views["total"]=$rs["likes"]+$rs["hides"];
			if($views["total"]==0)
				$views["like_perc"]=0;
			else
			    $views["like_perc"]=($views["likes"]/$views["total"])*100;
			//$views["shuffled"]=$rs["shuffled"];
		}
		else 
		{
			$views["likes"]=0;
			$views["hides"]=0;
			$views["total"]=0;
			$views["like_perc"]=0;
			//$views["shuffled"]=0;
		}
		return $views;
	}
	
	private function changeSystem_Profile($user_id,$old_photo_id,$new_photo_id)
	{
		//echo 'Changing From '.$old_photo_id.' to '.$new_photo_id.PHP_EOL;
	
		$sql="update user_photo set system_profile='no',shuffled=shuffled+1 where photo_id=? ";
		Query_Wrapper::UPDATE($sql,array($old_photo_id),"user_photo","master",$user_id);
		
		$sql="update photo_views_male set shuffled=shuffled+1 where photo_id=?";
		Query_Wrapper::UPDATE($sql,array($old_photo_id),"photo_views_male","master",$user_id);
		
		
		$sql="update user_photo set system_profile='yes' where photo_id=? ";
		Query_Wrapper::UPDATE($sql,array($new_photo_id),"user_photo","master",$user_id);
		
		$checkpoint_sql="insert into photo_reshuffling_user_checkpoint values($user_id,$old_photo_id,$new_photo_id,now())";
		$this->conn->Execute($checkpoint_sql);
	}
	
	public function checkGroupConcat()
	{
		$sql="show variables like 'group%'";
		$rs=$this->conn->Execute($sql);
		$rs=$rs->fetchRow();
		if(intval($rs['Value'])<99999)
		{
			return false;
		}
		return true;
	}
	
	public function shuffle() {
		
		echo "\nRunning The Shuffling Logic\n";
		$sql="select  us.user_id, up.photo_id, up.shuffled, up.is_profile from user_search us join user_photo up on us.user_id=up.user_id where gender='M' and system_profile='yes' and up.status='active' and admin_approved='yes'";
		
		$rs=$this->conn->Execute($sql);
		foreach ($rs as $value) {
			//var_dump($value["user_id"]);
			$views=$this->getLikeAndHide($value["photo_id"]);
		   
			if($views["total"]>=$this::viewsThreshold && $views["like_perc"]<$this::likePerc)
			{
			   $sql_photos="select photo_id, shuffled , is_profile from user_photo where user_id=? and system_profile='no' and status='active' and admin_approved='yes'";
				//$other_pics=Query_Wrapper::SELECT($sql_photos,$value['user_id']);
				
				$other_pics_conn=$this->conn->Execute($sql_photos,$value['user_id']);
				$other_pics=$other_pics_conn->GetRows();
				
				if($other_pics_conn->RowCount()>0)
				{
					//echo $value['user_id'].' Eligible for logic'.PHP_EOL;
					
					$new_system_dp=$value['photo_id'];
					$flag_repeat=0;
					
					$max_like=$views["like_perc"];
					//Get the best pic from the lot
					foreach($other_pics as $other_pic)
					{
						$view_other=$this->getLikeAndHide($other_pic["photo_id"]);
						if($view_other['like_perc']>$max_like)
						{
							$new_system_dp=$other_pic['photo_id'];
							$max_like=$view_other['like_perc'];
						}
					}
					
					
					if($new_system_dp!=$value['photo_id'])
					{
						//echo 'Got the one with the maximum likes'.PHP_EOL;
						//If the logic gets better percentage than the current one,it shuffles
						$this->changeSystem_Profile($value['user_id'],$value['photo_id'],$new_system_dp);
						continue;
					}			
					else 
					{
						//echo 'Now checking for Unshuffled pics'.PHP_EOL;
						//If the current one was the one that was having highest like 
						//Now check if there are some other non shuffled pics, if yes set it
						//check for non-shuffled photos
						foreach($other_pics as $other_pic)
						{
							if($other_pic['shuffled']==0)
							{
								$new_system_dp=$other_pic['photo_id'];
							    break;
							}
						}
						
						//some non-shuffled photo as systemDP
						if($new_system_dp!=$value['photo_id'])
						{
							$this->changeSystem_Profile($value['user_id'],$value['photo_id'],$new_system_dp);
							continue;
						}
						else 
						{  
							    //echo 'No unshuffled pics set'.PHP_EOL;
						        //If all the pics have been shuffled and the current one has the maximum likes then
								foreach($other_pics as $other_pic)
								{
									if($other_pic['is_profile']=='yes')
									{
										$new_system_dp=$other_pic['photo_id'];
										break;
									}
								}
								
								if($new_system_dp!=$value['photo_id'])
								{
									$this->changeSystem_Profile($value['user_id'],$value['photo_id'],$new_system_dp);
									continue;
								}
								
						
						
						}
					
					}
				}
				
			}
		}
	}
	
}


try {
	
	$firsttime=false;
	
	if(isset($argv[1])&&$argv[1]=="init")
	{
		$firsttime=true;
	}
	$days=-1;
	if(isset($argv[2]))
	{
		$days=$argv[2];
	}
	$obj=new Shuffle_Pic($firsttime,$days);
	if(!$obj->checkGroupConcat())
	{
		trigger_error( 'Group Concat Limit low',E_USER_WARNING);
		die;
	}
	if($firsttime)
	{
		$obj->InitializeSystem();
		
	}
	
	$obj->populate_db();
	
	if(!$firsttime)
	{
		$obj->UpdatedProfilePics();
		echo 'Waiting for the slaves to get updated , Idle for the moment'.PHP_EOL;
		sleep(60);
		$obj->shuffle();
		$obj->MarkSystemInMaster();
	}
	
	echo PHP_EOL."Done";
	
	
} catch (Exception $e) {
	echo $e->getMessage(); 
}

?>
