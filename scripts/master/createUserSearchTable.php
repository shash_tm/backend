<?php

require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ )  . "/../../include/config_admin.php";
//require_once dirname ( __FILE__ )  . "/../../include/config_himanshu.php";


/**
 * Cron to generate new search table
 * @Himanshu
 */



function getFileName(){
	global $filepathForUserSearch;
	if(!file_exists($filepathForUserSearch)){
		$fileDetails =  array("filename" => "user_search.txt", "path" => "/tmp");
		$filename = $fileDetails['filename'];
		$completePath = $fileDetails['path'] . "/$filename";
	}else{
		$completePath = $filepathForUserSearch;
	}
    chmod($completePath, 0777);
	return $completePath;
}

/**
 * will load data from local file in to user search new table (file should be tab separated)
 */

function loadDataFromLocalFile(){
	global $conn_master, $config, $baseurl;
	try{
		$completePath = getFileName();
		if(filesize($completePath) > 0 ){
		 $sql = "LOAD DATA LOCAL INFILE '$completePath' INTO TABLE user_search_new fields terminated by '\t' ";
		 $ex = $conn_master->Execute($sql);
		 // var_dump($conn_master->isWarning());
		 $warningQuery = "SHOW WARNINGS";
		 $warnings = $conn_master->Execute($warningQuery);
		 $warningCount = $warnings->RowCount();
		 if($warningCount > 0 ){
		 	$msg = "Warnings in loading data:" . $warningCount ."\n" ;
		 	for($i=0 ; $i<$warningCount;$i++){
		 		while ($row = $warnings->FetchRow()) {
		 			foreach ($row as $val){
		 				$msg .= "$val\t";
		 			}
		 			$msg .="\n";
		 		}
		 	}
		 	//Utils::sendEmail("arpan@trulymadly.com, shashwat@trulymadly.com", "himanshu@trulymadly.com", " User Search warnings in loading data", $msg, false);
		 }
		}else {
			$to = "arpan@trulymadly.com, shashwat@trulymadly.com";
			$subject = "User search creation file on admin tmp directory is empty for" . $baseurl ;
			$from = "himanshu@trulymadly.com";
			Utils::sendEmail($to, $from, $subject, false);
		}

	}
	catch (Exception $e){
		trigger_error($e->getMessage());
		echo $e->getTraceAsString();
	}
}



function createMemoryTable(){
	global $conn_master, $conn_reporting, $techTeamIds, $smarty,$baseurl,$miss_tm_id;
	$conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );

	if($miss_tm_id)
	{
		$miss_tm=$miss_tm_id;
	}
	else 
		$miss_tm=-1;
	$t1= microtime(true);
	$todayTable = 'user_search_new';
	$dropTodayTable = "DROP TABLE IF EXISTS $todayTable";
	$conn_master->Execute($dropTodayTable);

	$sql = "CREATE TABLE $todayTable (
  `user_id` int(11) NOT NULL,
  `gender` enum('M','F') DEFAULT NULL,
  `age` int(3) NOT NULL,
  `marital_status` varchar(256) DEFAULT NULL,
  `country` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `height` int(5) NOT NULL,
  `highest_education` int(3) DEFAULT NULL,
  `education_group` int(2) DEFAULT NULL,
  `pref_start_age` int(3) NOT NULL,
  `pref_end_age` int(3) NOT NULL,
  `pref_location` varchar(8096) DEFAULT NULL,
  `pref_marital_status` varchar(500) DEFAULT NULL,
  `pref_start_height` int(5) DEFAULT NULL,
  `pref_end_height` int(5) DEFAULT NULL,
  `pref_education` varchar(500) DEFAULT NULL,
  `pi` float DEFAULT NULL,
  `ai` float DEFAULT NULL,
  `bucket` int(2) NOT NULL,
  `visibility` int(10) DEFAULT '0',
  `hide_flag` tinyint(1) DEFAULT NULL,
  `install_status` enum('install','uninstall') DEFAULT 'install',
  `haspics` tinyint not null DEFAULT 0,
  `pending_sparks` int(10) not null DEFAULT '0',
  `isSelect` tinyint(1) not null DEFAULT 0,
  KEY `key1` (`gender`,`user_id`) USING BTREE,
  KEY `key2` (`user_id`) USING HASH
) ENGINE=MEMORY DEFAULT CHARSET=latin1 
	";

	/**
	 * CREATE TABLE `user_search_himanshu2` (
	 `user_id` int(11) NOT NULL,
	 `gender` enum('M','F') DEFAULT NULL,
	 `age` int(3) NOT NULL,
	 `religion` int(11) DEFAULT NULL,
	 `marital_status` enum('Never Married','Divorced','Widowed') DEFAULT NULL,
	 `country` int(11) NOT NULL,
	 `state` int(11) DEFAULT NULL,
	 `city` int(11) DEFAULT NULL,
	 `height` int(5) NOT NULL,
	 `smoking_status` enum('Never','Socially','Habitually') NOT NULL,
	 `drinking_status` enum('Never','Socially','Habitually') NOT NULL,
	 `food_status` enum('Vegetarian','Eggetarian','Non-Vegetarian') NOT NULL,
	 `highest_education` int(3) DEFAULT NULL,
	 `education_group` int(2) DEFAULT NULL,
	 `income_start` int(3) DEFAULT NULL,
	 `income_end` int(3) DEFAULT NULL,
	 `pref_start_age` int(3) NOT NULL,
	 `pref_end_age` int(3) NOT NULL,
	 `pref_religion` varchar(256) DEFAULT NULL,
	 `pref_location` varchar(512) DEFAULT NULL,
	 `pref_marital_status` varchar(500) DEFAULT NULL,
	 `pref_start_height` int(5) DEFAULT NULL,
	 `pref_end_height` int(5) DEFAULT NULL,
	 `pref_smoking_status` varchar(500) DEFAULT NULL,
	 `pref_drinking_status` varchar(500) DEFAULT NULL,
	 `pref_food_status` varchar(500) DEFAULT NULL,
	 `pref_education` varchar(500) DEFAULT NULL,
	 `pref_income` varchar(512) DEFAULT NULL,
	 `sumA` int(2) DEFAULT '0',
	 `sumB` int(2) DEFAULT '0',
	 `sumC` int(2) DEFAULT '0',
	 `sumD` int(2) DEFAULT '0',
	 `sumE` int(2) DEFAULT '0',
	 `sumF` int(2) DEFAULT '0',


	 KEY `test` (`gender`)
	 ) ENGINE=MEMORY
	 */


	$conn_master->Execute($sql);
	$defaultConstant = Utils::$defaultBucketConstant;

	$massSelectQuery ="SELECT distinct(u.user_id), u.gender,
	FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age,
	case when ud.marital_status = 'Never Married' then ud.marital_status else 'Married Before' end as myMaritalStatus ,
	ifnull(ud.stay_country,113) as country, ud.stay_state as state, ud.stay_city as city, ud.height,
  	ud.highest_degree, hd.rank as education_group,
    upd.start_age, upd.end_age,
  	upd.location_preference_for_matching as pref_location,
  	case when (upd.marital_status is null or upd.marital_status in ( 'Never Married','Married Before')) then upd.marital_status else ( case when upd.marital_status like 'Never Married,%' then 'Never Married,Married Before' else 'Married Before'  end )end as marital_status_pref,
  	upd.start_height, upd.end_height,
  	upd.education, 
  	case when upa.actual_pi is null then -1 else upa.actual_pi end,  
  	case when upa.ai is not null then upa.ai else 0.1 end ,
  	case when upa.bucket is not null then upa.bucket else 0 end,
  	COALESCE(upa.last_14_days_visibility, 0)  as visibility,
  	hide_myself as hide_flag,
  	case when android_app is null then 'install' else android_app end as app_status,
  	if(up.user_id is not null,1,0),
        if(t.pending_sparks is not null,t.pending_sparks,0),
        if(sub.user_id is not null,1,0)
  	FROM user u 
  	LEFT JOIN user_photo up on u.user_id = up.user_id and  up.is_profile ='yes' and up.status = 'active' and up.admin_approved = 'yes'
  	JOIN user_lastlogin ull on u.user_id = ull.user_id
  	LEFT JOIN user_data ud on u.user_id = ud.user_id 
  	LEFT JOIN user_preference_data upd on u.user_id = upd.user_id 
  	LEFT JOIN highest_degree hd on ud.highest_degree = hd.id
  	LEFT JOIN user_pi_activity upa on u.user_id = upa.user_id 
  	LEFT JOIN user_flags uf on u.user_id = uf.user_id
        LEFT JOIN (select user2,count(*) as pending_sparks from user_spark where status = 'sent' group by user2)t on u.user_id = t.user2
  	LEFT JOIN user_app_status ups on u.user_id = ups.user_id
  	LEFT JOIN user_subscription sub on u.user_id=sub.user_id and (sub.status='active' or sub.status='trial') and sub.expiry_date>now() and sub.metadata is not null
  	WHERE u.status = 'authentic' and (u.gender='F' or (gender='M' and up.user_id is not null)) and
  	case when u.gender = 'F' then ull.last_login >= date_sub(CURDATE(), interval 90 day) else ull.last_login >= date_sub(CURDATE(), interval 15 day) end and
  	case when u.gender ='M' then (ups.user_id is null or ups.android_app = 'install')  else (ups.user_id is null or ups.android_app In ('install','uninstall') ) end and
  	     ( u.email_id not like '%trulymadly.com%' or u.email_id is null) and u.fid not in (100008254001811,100005921989349,100005780296713,100004832236718,100008305415203)  
  	and u.user_id not in ($miss_tm) and u.user_id not in (select user_id from user_under_review where admin_approved is null or admin_approved='no')
			";	
	//date(u.last_login) >= date_sub(curdate(), interval 15 day) and
	//case when (ifnull(upa.total_likes,0) + ifnull(upa.totalmatches/matchesUtilites.php_hides,0)) < 10 then (case when u.gender = 'F' then 0.65 else 0.1 end) else upa.pi end ,
	//LEFT JOIN user_photo up on u.user_id = up.user_id where up.status = 'active' and up.is_profile ='yes' group by up.user_id;

	/* "SELECT u.user_id, u.gender,  u.last_login,uts.trust_score,
	 FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age, ud.religion, ud.caste, ud.marital_status, ud.stay_country, ud.stay_state, ud.stay_city,
	 ud.mother_tongue, ud.languages_known,
	 ut.height, ut.skin_tone, ut.body_type, ut.smoking_status, ut.drinking_status, ut.food_status,
	 uf.family_type, uf.family_status,
	 uw.working_area, uw.industry, uw.education, uw.income_start, uw.income_end,
	 upd.start_age as pref_start_age,
	 upd.end_age as pref_end_age,
	 upd.languages_preference as pref_mother_tounge,
	 upd.location_preference_for_matching  as pref_location,
	 upd.religionCaste as pref_religionCaste,
	 upd.marital_status as pref_marital_status,
	 upt.start_height as pref_start_height,
	 upt.end_height as pref_end_height,
	 upt.body_type_search as pref_body_type,
	 upt.smoking_status_search as pref_smoking_status,
	 upt.drinking_status_search as pref_drinking_status,
	 upt.food_status_search as pref_food_status,
	 upf.family_status as pref_family_status,
	 upw.working_area_search as pref_working_area,
	 upw.industry as pref_industry,
	 upw.education as pref_education,
	 upw.income_start as pref_income_start,
	 upw.income_end as pref_income_end
	 FROM user u LEFT JOIN user_trust_score uts ON u.user_id = uts.user_id
	 LEFT JOIN user_demography ud ON u.user_id = ud.user_id
	 LEFT JOIN user_trait ut ON u.user_id = ut.user_id
	 LEFT JOIN user_family uf ON u.user_id = uf.user_id
	 LEFT JOIN user_work uw ON u.user_id = uw.user_id
	 LEFT JOIN user_preference_demography upd on u.user_id=upd.user_id
	 LEFT JOIN user_preference_trait upt on u.user_id=upt.user_id
	 LEFT JOIN user_preference_family upf on u.user_id=upf.user_id
	 LEFT JOIN user_preference_work upw on u.user_id=upw.user_id
	 WHERE u.status = 'authentic' and u.email_id not regexp 'trulymadly.com'
	 order by last_login desc";
	 */

	$resultSelect = $conn_reporting->Execute($massSelectQuery);


	$filename = getFileName();
	
	if(file_exists($filename))
	unlink($filename);


	/**
	 * row wise iterator instead of mass fetching
	 */
	while ($row = $resultSelect->FetchRow()) {
		if($row['age'] == 0 )continue;
		$str = '';
		foreach ($row as $val){
			$str .= (!isset($val))?"\N\t":"$val\t";
			//$str .= "$val\t";
				
		}
		$str .="\n";
		//echo $str;
		file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
	}

	echo "written data into file ". $filename;

	//calling to write data into user search new
	loadDataFromLocalFile();
	echo "loaded data into user_search table ";
	/*
	 $totalRows = $resultSelect->GetRows();

	 echo count($totalRows);
	 $arr = array();
	 foreach ($totalRows as $val){
	 if($val['age'] == 0 )continue;
		$arr[] = $val;
	 }
	 echo "Rows:" . count($arr);
	 $conn_master->bulkBind = true;
	 //$massInsertQuery = " INSERT INTO $todayTable $massSelectQuery";
	 $massInsertQuery = $conn_master->Prepare(" INSERT INTO $todayTable (user_id,gender, age,  marital_status,country, state, city ,   height, highest_education, education_group,  pref_start_age , pref_end_age , pref_location , pref_marital_status , pref_start_height, pref_end_height,  pref_education  , pi,ai,bucket ,  visibility, hide_flag )
	 values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	 //$result = Query::INSERT($massInsertQuery, null);
	 $result = $conn_master->Execute($massInsertQuery, $arr);
	 */

	//$count_inserted_rows = $conn_master->Affected_Rows() ;

	$existingTable = 'user_search';
	$oldTable = 'user_search_old';
	/*	var_dump($result);
	 if($result!=false){*/
	$dropTable = "DROP TABLE IF EXISTS $oldTable";
	//Query::UPDATE($dropTable, null);
	$conn_master->Execute($dropTable);
	echo 'dropped';

	$renameQuery = "RENAME TABLE  $existingTable TO $oldTable, $todayTable TO $existingTable ";
	//Query::UPDATE($renameQuery, null);
	$conn_master->Execute($renameQuery);

	$checkpointSql = "INSERT INTO user_search_checkpoint (checkpoint) value (now())";
	//Query::INSERT($checkpointSql, null);
	$conn_master->Execute($checkpointSql);

	$t2 = microtime(true);
	/*1) How much time user_search creation table take
	 2) How many rows were created
	 3) How many male/female*/
	$timeTaken = $t2 - $t1;

	$timeTaken = round($timeTaken, 2) . "s";
	$sqlUsearch = "SELECT count(user_id) as count, gender  from user_search group by gender";
	$rowsSearch = $conn_master->Execute($sqlUsearch);
    $Count=array();
    $i=0;
	$count_inserted_rows = null;
	foreach ($rowsSearch as  $val){
		$Count[$i]=$val['count'];
		$i++;
		$count_inserted_rows += $val['count'];
	}
	//echo "$Count[0]".","."$Count[1]";
	$smarty->assign("rows", $rowsSearch);
	$smarty->assign("count", $count_inserted_rows);
	$smarty->assign("timeTaken", $timeTaken);
	//$time = time()+'+330';
	$now = date ( 'M d,Y h:i A', strtotime ( '+330 minutes',  time()  ) );
	//$now->format('Y-m-d H:i:s');
	//Utils::sendEmail($techTeamIds  , "himanshu@trulymadly.com", "user Search Stats ". $now, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/createuserSearch.tpl'), TRUE);
	
	if($baseurl=='https://dev.trulymadly.com/trulymadly' || $baseurl=='http://dev.trulymadly.com/trulymadly')
	{
		$to = "arpan@trulymadly.com";
	}
	else if($baseurl=='https://t1.trulymadly.com' || $baseurl=='http://t1.trulymadly.com')
	{
		$to = "vikash@trulymadly.com, poonam.kumawat@trulymadly.com";
	}
	else
		$to = "arpan@trulymadly.com, shashwat@trulymadly.com";
	
	
	echo "\n".$to;
	Utils::sendEmail( $to, "admintm@trulymadly.com", "user Search Stats ".$baseurl." ". $now ."   "."   Male Count: ".$Count[0]."   Female Count: ".$Count[1], $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/createuserSearch.tpl'), TRUE);

	//Utils::sendEmail($techTeamIds, "himanshu@trulymadly.com", $subject, null);


	//}

}



try{
	global $baseurl;
	//echo $baseurl;
	if ( php_sapi_name() == 'cli' )
	{
		createMemoryTable();
		
		if($baseurl=='https://dev.trulymadly.com/trulymadly' || $baseurl=='http://dev.trulymadly.com/trulymadly')
	    {
		  $to = "arpan@trulymadly.com";
	      }
	    else if($baseurl=='https://t1.trulymadly.com' || $baseurl=='http://t1.trulymadly.com')
	    {
		  $to = "vikash@trulymadly.com, poonam.kumawat@trulymadly.com";
	     }
	    else
	     { $to = "arpan@trulymadly.com, shashwat@trulymadly.com";}
		$subject = "User Search table created for ". $baseurl;
		//$from = "admin@trulymadly.com";
		//$from = gethostname();
		$from = $baseurl; 
	}

	//Utils::sendEmail($to,$from , $subject, $subject);

}
catch(Exception $e){
	//echo "codh cdo";
	global $baseurl;
	echo $e->getMessage();
	if($baseurl=='https://dev.trulymadly.com/trulymadly' || $baseurl=='http://dev.trulymadly.com/trulymadly')
	{
		$to = "arpan@trulymadly.com";
	}
	else if($baseurl=='https://t1.trulymadly.com' || $baseurl=='http://t1.trulymadly.com')
	{
		$to = "vikash@trulymadly.com, poonam.kumawat@trulymadly.com";
	}
	else
		$to = "arpan@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Table Generation Cron Failure ".$baseurl ;
	$message = "Table Generation Cron failed due to " . PHP_EOL . $e->getTraceAsString();
	$from = "admintm";
	$headers = "From:" . $from;
	Utils::sendEmail($to, $from, $subject, null);
	//mail($to,$subject,$message,$headers);
	//	echo "Mail Sent.";
	error_log($subject,1,$to,$from);
	trigger_error ( "PHP WEB: ".$e->getTraceAsString (), E_USER_WARNING );
}


?>
