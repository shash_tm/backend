<?php

include dirname ( __FILE__ ) . "/../../include/config_admin.php";
include dirname ( __FILE__ ) . "/../../include/Utils.php";
//require_once dirname ( __FILE__ ) . "/../../mobile_utilities/pushNotification.php";


/**
 * Cron to poll the file on remote slave and dump the data in to
 * our sql table
 * @Himanshu
 */

/*function getFileName(){
	$table = Utils::getSlaveRecoTableName();
	$filename = "/tmp/sql_$table.txt";
	return $filename;
}*/

function copyfilesFromS3(){
	global $config;
	$today = date("ymd");
	
	$table = Utils::getSlaveRecoTableName();
	Utils::getFromS3("recommendation/sql_$table.txt", "/tmp/reco_$today.txt");
	/*$filename = $config['amazon_s3']['cloudfront_url'] . "recommendation/sql_$table.txt";
	copy($filename, "/tmp/reco_$today.txt");
*/

	$fbtable = Utils::getSlavefbConnTableName();
	Utils::getFromS3("recommendation/sql_$fbtable.txt", "/tmp/mutual_conn_$today.txt");
	
	/*$filename = $config['amazon_s3']['cloudfront_url'] . "recommendation/sql_$fbtable.txt";
	copy($filename, "/tmp/mutual_conn_$today.txt");*/
}

function push($user_id, $cnt){
	$push = new pushNotification();
	$push_data = array("matches_notification"=>"You've got $cnt new matches");
	$push->notify($user_id, $push_data);
}
function pushNotifications(){
	global $config;
	$today = date("ymd");
	$table = Utils::getSlaveRecoTableName();
	$filename = "/tmp/reco_$today.txt";
	$handle = fopen($filename, "r");

	try{
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				// process the line read.
				$arr = explode(',', $line);

				if($arr[0]!=$prev_user1){
					$j=0;
					if($prev_user1!=null){
						addMatches($user_array, "u:m:$prev_user1");
						push($prev_user1, count($user_array));
					}
					$user_array = null;
				}
				$user_array[$j++]=(int)$arr[1];
				$prev_user1 = $arr[0];

			}
			push($prev_user1, count($user_array));
		} else {
			// error opening the file.
			$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
			$subject = "failure in Push notifications";
			$from = "admin@trulymadly.com";
			Utils::sendEmail($to, $from, $subject, null);
			trigger_error ( "failure in Push notifications", E_USER_WARNING );
		}

	}
	catch (Exception $e){
		trigger_error($e->getTraceAsString(), E_USER_WARNING);
	}
}

function addMatches($user_array, $key) {
	global $redis;

	/*echo  $key . ' ';
	 var_dump($user_array);
	 */	$args[] = $key;
	for ($i = 0; $i < count($user_array); $i++) {
		$args[] = $user_array[$i];
	}
	var_dump($args);
	call_user_func_array(array($redis,'LPUSH'),$args);
}

function dumpRedis(){
	global $config;

	$today = date("ymd");
	$table = Utils::getSlaveRecoTableName();
	//$filename = "/tmp/sql_$table.txt";
	/*var_dump($config);
	echo $config['amazon_s3']['cloudfront_url']; die;*/
	$filename = "/tmp/reco_$today.txt";
	$handle = fopen($filename, "r");
	echo $filename;
	//$handle= fopen("/var/www/html/trulymadly/scripts/slave/sql_user_recommendation_slave_140203.txt", "r");
	$prev_user1 = null;
	$user_array = array();

	$j=0;
	if ($handle) {
		while (($line = fgets($handle)) !== false) {
			// process the line read.
			$arr = explode(',', $line);

			if($arr[0]!=$prev_user1){
				$j=0;
				if($prev_user1!=null){
					addMatches($user_array, "u:m:$prev_user1");
				}
				$user_array = null;
			}
			$user_array[$j++]=(int)$arr[1];
			$prev_user1 = $arr[0];

		}
		addMatches($user_array, "u:m:$prev_user1");
	} else {
		// error opening the file.
		$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
		$subject = "failure in Recommendations dumping in Redis";
		$from = "admin@trulymadly.com";
		Utils::sendEmail($to, $from, $subject, null);
		trigger_error ( "error writing redis dump", E_USER_WARNING );
	}
}

function dumpData(){
	global $conn_master, $config;
	try{
			
		$today = date("ymd");
	//	$table = Utils::getSlaveRecoTableName();
		//$filename = "/tmp/sql_$table.txt";
		//$filename = $config['amazon_s3']['cloudfront_url'] . "recommendation/sql_$table.txt";
		//echo $filename;
		//$filename = "/var/www/html/trulymadly/scripts/slave/sql_user_recommendation_slave_140203.txt";
		$filename1 = "/tmp/reco_$today.txt";
		$file1 = file_get_contents($filename1, true);

		//copy($filename, 'test_reco.txt');
		if($file1!=null){
		 $sql = "LOAD DATA LOCAL INFILE '$filename1' INTO TABLE user_recommendation fields terminated by ','";
		 $ex = $conn_master->Execute($sql);
		}


	/*	$fbtable = Utils::getSlavefbConnTableName();
		$filename = $config['amazon_s3']['cloudfront_url'] . "recommendation/sql_$fbtable.txt";
	*/	//$filename = "/var/www/html/trulymadly/scripts/slave/sql_user_recommendation_slave_140203.txt";

		$filename2 = "/tmp/mutual_conn_$today.txt";
		$file2 = file_get_contents($filename2, true);
		//copy($filename, 'test_mutual_conn.txt');

		if($file2!=null){
		 $sql = "LOAD DATA LOCAL INFILE '$filename2' INTO TABLE user_facebook_mutual_connections fields terminated by ','";
		 $ex = $conn_master->Execute($sql);
		}

		/*
		 $redis_file = '/tmp/redis_mass_insert.txt';
		 //echo $redis_file;
		 $cmd = "echo -e ". '"$(cat /tmp/redis_mass_insert.txt)"' ."| redis-cli --pipe";
		 if($redis_file!=null){
		 echo $cmd;
		 $output = shell_exec($cmd);
		 echo $output;
		 }
		 */
	}
	catch (Exception $e){
		trigger_error($e->getMessage());
		echo $e->getMessage();
	}
}

try{

	copyfilesFromS3();
	dumpRedis();
	dumpData();
	//pushNotifications();
		$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
		$subject = "Recommendations dumped in Master db and Redis";
	$from = "admin@trulymadly.com";
	Utils::sendEmail($to, $from, $subject, null);
}
catch(Exception $e){

		$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
		$subject = "Recommendation Dump Failure";
	$message = "Dump failed due to " . $e->getMessage();
	$from = "admin@trulymadly.com";
	//$headers = "From:" . $from;
	Utils::sendEmail($to, $from, $subject, $message);
	//mail($to,$subject,$message,$headers);
	echo "Mail Sent.";
	error_log($subject,1,$to,$from);
	trigger_error ( $e->getMessage (), E_USER_WARNING );

}
