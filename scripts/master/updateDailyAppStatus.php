<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

/*
 * Cron to update the user_app_status table from user_gcm_current 
 * Updated by @Sumit
 * 🖖 Live Long and Prosper.
 */
function notifyTechTeam($msg){
	global  $baseurl;
	$subject = $msg. " for " . $baseurl;
	$from = "admintm@trulymadly.com";
	$to = "backend@trulymadly.com" ;
	Utils::sendEmail($to, $from, $subject, $msg); 
}

function updateAppStatus()
{
	global $conn_master, $conn_reporting;	
	$sql = "select r.user_id , android_app, app_status from
(SELECT user_id , case when t.statuses like '%uninstall%' AND t.statuses NOT like '%login%' AND t.statuses NOT like '%logout%'
 then 'uninstall' else 'install' end as app_status from 
(SELECT ugc.user_id, GROUP_CONCAT(distinct(ugc.status)) AS statuses FROM user_gcm_current ugc 
WHERE ugc.user_id IS NOT NULL AND 
ugc.status IS NOT NULL AND  ugc.status not in ('expired') 
group by ugc.user_id)t )r 
left join user_app_status uas on uas.user_id = r.user_id";
	//echo $sql;
	$res = $conn_reporting->Execute($sql);
	$arr = array();
	$i = 0 ;
	$j = 0 ; 
	$k = 0 ;
	$all_count = $res->RowCount();
	$chunkCount = 1000; 
	
	$updateSql = "INSERT INTO user_app_status (user_id,android_app, tstamp) values (?,?, now()) ON DUPLICATE KEY UPDATE android_app = ?";
	$updatePrep = $conn_master->Prepare($updateSql) ;
	
	while ($val = $res->FetchRow())
	{
		$k++ ;
		if ($val['android_app'] != $val['app_status'])
		{
			$arr[] = array($val['user_id'], $val['app_status'], $val['app_status']);
			$i++ ;
			
			if($i >= $chunkCount || $k == $all_count )
			{
				$conn_master->bulkBind = true;
				$conn_master->Execute($updatePrep, $arr);
				if($k != $all_count)
				{
					$i = 0 ;
					$j++ ;
					$arr = array();
				}
			}
		}
	}
	
	$total_count =$i+  $chunkCount * $j ; 	
	$return_array = array("updates" =>$total_count ,
						  "iterations" => $j	) ;
	return $return_array ;
}

function getstatusCount()
{
	global $conn_reporting ;
	$sql = "SELECT android_app, count(*) as cu FROM user_app_status group by android_app" ;
	$res = $conn_reporting->Execute($sql) ;
	$data = array() ;
	while($row = $res->FetchRow())
	{
		$data[$row['android_app']] = $row['cu'] ;
	}
	return $data ;
}



try{
	if(php_sapi_name() == 'cli')
	{
		$data = updateAppStatus();
		$msg = "user_app_status updated for " . $data['updates'] . " users with " . $data['iterations'] . " iterations" ;
		$status = getstatusCount() ;
		$msg .= PHP_EOL . "Current " . $status['install'] . " installs and " .$status['uninstall'] ." uninstalls" ;
		echo $msg ;
		notifyTechTeam($msg);
	}
	else 
	{
		$msg = "update User app status was tried to be run without CLI from IP " . $_SERVER['REMOTE_ADDR'] ;
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != null)
		{
			$msg .= " from forwarded from " . $_SERVER['HTTP_X_FORWARDED_FOR'] ;
		}
		notifyTechTeam($msg);
	}
}
catch(Exception $e){
	$msg = "update user_app_status table script failed";
	notifyTechTeam($msg);
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}
?>