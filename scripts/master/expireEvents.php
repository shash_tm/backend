<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../curatedDeals/dealManagerClass.php";

/*
 * Cron to update the user_app_status table from user_gcm_current 
 * Updated by @Sumit
 * 🖖 Live Long and Prosper.
 */
function notifyTechTeam($msg){
	global  $baseurl;
	$subject = $msg. " for " . $baseurl;
	$from = "admintm@trulymadly.com";
	$to = "backend@trulymadly.com, ecosystem@trulymadly.com" ;
	Utils::sendEmail($to, $from, $subject, $msg); 
}

function expireEvents()
{
	$manager = new DealsManager() ;
	global  $conn_reporting;
	$sql = "select name, datespot_id, hash_id from datespots where type='event' and event_date < now() and status='active' ";
	//echo $sql;
	$res = $conn_reporting->Execute($sql);
	$total_count = 0;
	$event_name_string = "";
	while ($val = $res->FetchRow())
	{
		$manager->changeStatus($val['hash_id'], 'inactive');
		$event_name_string .= $val['name'].",  ";
		$total_count++;
	}

	$return_array = array("updates" =>$total_count, "names"=>$event_name_string) ;
	return $return_array ;
}


try{
	if(php_sapi_name() == 'cli')
	{
		$data = expireEvents();
		$msg = $data['updates']. " events expired yesterday:  ".$data['names'] ;
		echo $msg ;
		notifyTechTeam($msg);
	}
	else 
	{
		$msg = "update User app status was tried to be run without CLI from IP " . $_SERVER['REMOTE_ADDR'] ;
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != null)
		{
			$msg .= " from forwarded from " . $_SERVER['HTTP_X_FORWARDED_FOR'] ;
		}
		notifyTechTeam($msg);
	}
}
catch(Exception $e){
	$msg = "Expire events script failed";
	notifyTechTeam($msg);
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}
?>