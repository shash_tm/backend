<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../PricingStrategies/DynamicPricing.php";
require_once dirname ( __FILE__ ) . "/../../DBO/sparkDBO.php";

/*
 * Cron to update the user_app_status table from user_gcm_current 
 * Updated by @Sumit
 * 🖖 Live Long and Prosper.
 */
function notifyTechTeam($msg){
	global  $baseurl;
	$subject = $msg. " for " . $baseurl;
	$from = "admintm@trulymadly.com";
	$to = "backend@trulymadly.com" ;
	Utils::sendEmail($to, $from, $subject, $msg); 
}

function updateFilters($source,$multiplier,$max_female)
{
	global  $conn_reporting;
	$source = $source == 'both' ? "'iOSApp','androidApp'": "'".$source."'";
	$sql = "select * from spark_bucket where type = 'select' and bucket_name like '%free-trial%' and source in ($source) ";
	$res = $conn_reporting->Execute($sql);
	$total_count = 0;
	$not_in_state = DynamicPricing::getStateWithSelectFemaleThreshold($max_female, $multiplier);
	while ($val = $res->FetchRow())
	{
		$filt = $val['filter'];
		$bucket_id = $val['id'];
		$filter = json_decode($filt, true);
		$filter['not_in_state'] = $not_in_state;
		if(count($not_in_state) == 0){
			unset($filter['not_in_state']);
		}
		sparkDBO::updateBucketFilter($bucket_id,$filter);
		$total_count++;
	}

	$return_array = array("updates" =>$total_count) ;
	return $return_array ;
}


try{
	if(php_sapi_name() == 'cli')
	{
		$source = $argv[1];
		if(!isset($source) || ($source != 'androidApp' && $source != 'iOSApp'  && $source != 'both')){
			echo "Pass source as param 1 (accepted values: 'androidApp', 'iOSApp', 'both'";
			die;
		}

		$multiplier = $argv[2];
		if(!isset($multiplier)){
			echo "Pass multiplier as 2nd parameter";
			die;
		}

		$max_female = $argv[3];
		if(!isset($multiplier)){
			echo "Pass max female percentage as 3rd parameter";
			die;
		}

		$data = updateFilters($source,$multiplier,$max_female);
		$msg = $data['updates']. " Bucket filters changed for select  ";
		echo $msg ;
	//	notifyTechTeam($msg);
	}
	else 
	{
		$msg = "update User app status was tried to be run without CLI from IP " . $_SERVER['REMOTE_ADDR'] ;
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != null)
		{
			$msg .= " from forwarded from " . $_SERVER['HTTP_X_FORWARDED_FOR'] ;
		}
		notifyTechTeam($msg);
	}
}
catch(Exception $e){
	$msg = "Expire events script failed";
	notifyTechTeam($msg);
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}
?>