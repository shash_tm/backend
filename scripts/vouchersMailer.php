<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../mobile_utilities/pushNotification.php";


$conn_slave->SetFetchMode ( ADODB_FETCH_ASSOC );
$conn_master->SetFetchMode ( ADODB_FETCH_ASSOC );


/**
 * @Himanshu
 * Mailer for vouchers includes push notiication as well
 */

function sendMail( $data, $voucher_data, $campaignName, $campaign, $redirectionFile, $utm_content, $external_email =null){

	global $smarty;

	$sysLog = new SystemLogger();

	$ts = Utils::getDateGMTTOISTfromUnixTimestamp($time);
	$smarty->assign("data", $data);
	$smarty->assign("name", ucfirst($data['fname']));
	$smarty->assign("ts", $ts);
	$smarty->assign("voucher", $voucher_data);
	$toAddress = $data['email_id'];

	if(isset($external_email)) $toAddress = $external_email;

	$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $data['user_id']);
	$smarty->assign("analyticsLinks", $analyticsLinks);
	$mailObject = new MailFunctions();
	$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/newmailers/emailCCDvoucher.tpl'),"","Hey ". ucfirst($data['fname']).", you’ve just won a voucher. ", $data['user_id']);

	$logdata['email_id'] = $data['email_id'];
	$logdata['subject'] = $campaignName;
	$logdata['user_id'] = $data['user_id'];
	$sysLog->logSystemMail($logdata);
}

function sendPushNotification($data, $tmPic, $voucher_data){
	global $admin_id;
	$pushnotify = new pushNotification();
	$voucher_data['friendly_text'] = "Cafe Coffee Day voucher worth Rs.50";
	$title_text = "Yay! You won a CCD voucher!";
	$ticker_text = "Yay! You won a CCD voucher!";
	$ticker_content_text = "Worth Rs.50";
	$pushnotify->notify($data['user_id'], array("content_text"=>$ticker_content_text, "voucher_data" =>json_encode($voucher_data),"pic_url"=>$tmPic,"ticker_text"=>$ticker_text,"title_text"=>$title_text,"push_type"=>"VOUCHER"),$admin_id);
}

function updateVoucher($data, $campaign){
	global $conn_master;
	$sql = $conn_master->Prepare("UPDATE vouchers SET user_id = ? , tstamp = now() WHERE user_id is null and campaign = ? LIMIT 1");
	$conn_master->Execute($sql, array($data['user_id'], $campaign));

	$sql2= $conn_master->Prepare("SELECT * FROM vouchers where campaign =? and user_id = ?");
	$res = $conn_master->Execute($sql2, array($campaign, $data['user_id']));
	$row = $res->FetchRow();
	return $row;
}
function getTMPic(){
	global $conn_slave, $admin_id, $imageurl;
	$sql = $conn_slave->Prepare("select thumbnail as pic from user_photo where user_id = ?");
	$ex = $conn_slave->Execute($sql, array($admin_id));
	$res = $ex->FetchRow();
	return  $imageurl.$res['pic'];
}



function generateQuery($voucher_tag, $referrer){
	$sql1 = null;
	if($voucher_tag == "ahmedabad_ccd_50"){
	$sql1= "select u.user_id, u.email_id, u.fname, md.referrer, md.app_version_code from user u
				join user_data ud on u.user_id = ud.user_id 
				left JOIN vouchers v on u.user_id = v.user_id 
				left join mobile_data md on u.user_id = md.user_id 
				WHERE v.user_id is null and  u.registered_at >= date_sub(NOW(), interval 1 day) 
				and (u.email_status is null or u.email_status =  'unsubscribe') 
				and u.status in ('authentic', 'non-authentic')  and u.registered_from = 'android_app' and  ( ud.stay_city = 6448 OR md.referrer regexp 'utm_source=$referrer')";
	}else{
		$sql1= "select u.user_id, u.email_id, u.fname, md.referrer, md.app_version_code from user u
				join user_data ud on u.user_id = ud.user_id 
				left JOIN vouchers v on u.user_id = v.user_id 
				join mobile_data md on u.user_id = md.user_id 
				WHERE v.user_id is null and  u.registered_at >= date_sub(NOW(), interval 1 day) 
				and (u.email_status is null or u.email_status =  'unsubscribe') 
				and u.status in ('authentic', 'non-authentic')  and  u.registered_from = 'android_app' and md.referrer regexp 'utm_source=$referrer'";
	}
	
	return  $sql1;
}
function sendVoucher($voucher_tag, $referrer,  $external_email = null){

	global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl, $admin_id;

	$tmPic = getTMPic();
	//query needs to be changed in case u want to send multiple vouchers to same user (null check to be removed)
	$sql1= generateQuery($voucher_tag, $referrer);
	
	echo $sql1;
	$res = $conn_slave->Execute($sql1);
	$data =$res->GetRows();

	var_dump($data);
	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$voucher_tag";
	$redirectionFile = "/matches.php";
	$utm_content="view_$voucher_tag";
	$campaignName = $voucher_tag;


	$campaignColumn = $voucher_tag;

	//var_dump($data); die;
	foreach ($data as $rows){
		$voucher = null;
		$voucher = updateVoucher($rows, $campaignColumn);
		sendMail($rows, $voucher, $campaignName, $campaign, $redirectionFile, $utm_content, $external_email);
		//if($rows['app_version_code'] >= 35)
		sendPushNotification($rows, $tmPic, $voucher);
	}
}

function run($email){
	global $conn_slave;
	$sql =" select * from voucher_info where status = 'active' ";
	$res = $conn_slave->Execute($sql);
	$rows = $res->GetRows();

	foreach ($rows as $col){

		if($col['voucher_name'] == 'ahmedabad_ccd_50') {
			$referrer = "josh";
			//$referrerStr = " and  ( ud.stay_city = 6448 OR md.referrer regexp 'utm_source=$referrer')";
			sendVoucher($col['voucher_name'], $referrer, $email);
		}
		
	}

}

try{
	global $baseurl;
	$options=getopt ( "e:" );
	run($options[e]);
}
catch (Exception $e){
	echo $e->getMessage();
	global $baseurl, $techTeamIds;
	$subject = $baseurl. " - Voucher Mailer Failed";
	Utils::sendEmail($techTeamIds, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Voucher Mailer failed". $e->getTraceAsString(), E_USER_WARNING );
}

?>