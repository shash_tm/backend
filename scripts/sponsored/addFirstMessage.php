<?php 
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ )  . "/../../include/config_admin.php";
require_once dirname ( __FILE__ )  . "/../../DBO/messagesDBO.php";

/**
 * @Sumit
 * To run a cron add the first Message in all the Mutual Likes for the sponsered profiles
 */

class AddFirstMessage 
{
	private $conn_reporting;
	private $conn_master ;
	private $message_DBO ;
	private $maleSponseredId ;
	private $femaleSponseredId ;
	private $message ;
	          
	function __construct () 
	{
		global $conn_reporting, $conn_master;
		$this->conn_reporting = $conn_reporting ;
		$this->conn_master = $conn_master ;
		$this->message_DBO = new MessagesDBO() ;
		$this->maleSponseredId = 1184126;
		//$this->femaleSponseredId = 492854 ;	
		$this->message = "Hey! The offer has now been extended until 11th of January! So rush to your nearest PVR now! ";
	}
	
	function checkAndAddMessages () 
	{
		
		$sql = "select if(user1= $this->maleSponseredId, user1, user2) as sender_id, if(user1= $this->maleSponseredId, user2, user1) as receiver_id from 
		user_mutual_like  where ( user1 in ( $this->maleSponseredId) or user2 in ( $this->maleSponseredId )) and blocked_by is null and timestamp <'2016-01-06 00:00:00'" ;
		$res = $this->conn_reporting->Execute($sql) ;  

		
		while ($row = $res->FetchRow()) 
		{
		   $sender_id = $row ['sender_id'] ;
		   $receiver_id = $row ['receiver_id'] ;
		   $tstamp=date('Y-m-d H:i:s'); 
		  // var_dump($row); die ;  
		   $this->message_DBO->storeMessage($sender_id, $receiver_id, $this->message, 'TEXT', $tstamp, null, null); 
		   
		}
	}
}

try {
	if(php_sapi_name() =='cli')
	{
		$addMsg = new AddFirstMessage();
		$addMsg->checkAndAddMessages();
	}
	
} catch (Exception $e) {
	echo $e->getMessage();
}



?>