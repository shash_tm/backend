<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";


function incompleteRegisterationMailerNextDay($external_email = null){
	global $config, $smarty,$conn_master, $conn_slave, $imageurl, $dummy_female_image, $dummy_male_image,$baseurl;
	$sysLog = new SystemLogger();

	$sql = "SELECT fname,email_id, email_status, user_id from user where status = 'incomplete' and date(registered_at) = date_sub(CURDATE(), interval 1 day)  AND email_id not like '%trulymadly%' AND email_status is null";
	echo $sql;
	$res = $conn_slave->Execute($sql);
	$rows =$res->GetRows();


	//$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'

	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=incomplete_profile_nextDay";
	$redirectionFile = "/register.php";
	$utm_content="view_incomplete_profile_nextDay";
	$campaignName = "incomplete_profile_nextDay";

	foreach ($rows as $key=>$val){

		$data = null;
		$data['email_id'] = $val['email_id'];
		$data['subject'] = "incomplete_profile_nextDay";
		$data['user_id'] = $val['user_id'];
			
		$smarty->assign("name", ucfirst($val['fname']));
		$smarty->assign("rnum", rand(100, 200));

		$toAddress = $val['email_id'];

		if(isset($external_email))
		$toAddress = $external_email;

		$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $val['user_id']);
		$smarty->assign("analyticsLinks", $analyticsLinks);

		$mailObject = new MailFunctions();
		$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/newmailers/emailRegIncomplete1.tpl'),"",ucfirst($val['fname']) . ", ". "true love is waiting for you to complete your profile.", $val['user_id']);
		$sysLog->logSystemMail($data);
	}
}

function incompleteRegisterationMailerFiveDay($external_email = null){
	global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$baseurl;
	$sysLog = new SystemLogger();

	$sql = "SELECT fname,email_id, email_status, user_id from user where status = 'incomplete' and date(registered_at) = date_sub(CURDATE(), interval 5 day)  AND email_id not like '%trulymadly%' AND email_status is null";
	echo $sql;
	$res = $conn_slave->Execute($sql);
	$rows =$res->GetRows();

	//$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'

	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=incomplete_profile_after5Days";
	$redirectionFile = "/register.php";
	$utm_content="view_incomplete_profile_after5Days";
	$campaignName = "incomplete_profile_after5Days";

	foreach ($rows as $key=>$val){

		$data = null;
		$data['email_id'] = $val['email_id'];
		$data['subject'] = "incomplete_profile_after5Days";
		$data['user_id'] = $val['user_id'];
			
		$smarty->assign("name", ucfirst($val['fname']));
		$smarty->assign("rnum", rand(900, 1000)+100);

		$toAddress = $val['email_id'];

		if(isset($external_email))
		$toAddress = $external_email;

		$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $val['user_id']);
		$smarty->assign("analyticsLinks", $analyticsLinks);

		$mailObject = new MailFunctions();
		$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/newmailers/emailRegIncomplete2.tpl'),"",ucfirst($val['fname']) . ", ". "no pressure, but our numbers are increasing by the day.", $val['user_id']);
		$sysLog->logSystemMail($data);
	}
}

function incompleteRegisterationMailerTenDay($external_email = null){
	global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$baseurl;
	$sysLog = new SystemLogger();

	$sql = "SELECT fname,email_id, email_status, user_id from user where status = 'incomplete' and date(registered_at) = date_sub(CURDATE(), interval 10 day)  AND email_id not like '%trulymadly%' AND email_status is null";
	echo $sql;
	$res = $conn_slave->Execute($sql);
	$rows =$res->GetRows();


	//$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'

	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=incomplete_profile_after10Days";
	$redirectionFile = "/register.php";
	$utm_content="view_incomplete_profile_after10Days";
	$campaignName = "incomplete_profile_after10Days";

	foreach ($rows as $key=>$val){

		$data = null;
		$data['email_id'] = $val['email_id'];
		$data['subject'] = "incomplete_profile_after10Days";
		$data['user_id'] = $val['user_id'];
			
		$smarty->assign("name", ucfirst($val['fname']));
		$smarty->assign("rnum", rand(900, 1000) + 500);

		$toAddress = $val['email_id'];

		if(isset($external_email))
		$toAddress = $external_email;

		$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $val['user_id']);
		$smarty->assign("analyticsLinks", $analyticsLinks);

		$mailObject = new MailFunctions();
		$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/newmailers/emailRegIncomplete3.tpl'),"",ucfirst($val['fname']) . ", ". "Long time no see. Would you like to complete your profile now?", $val['user_id']);
		$sysLog->logSystemMail($data);
	}
}

try{
	$options=getopt ( "e:" );
	incompleteRegisterationMailerNextDay($options[e]);
	incompleteRegisterationMailerFiveDay($options[e]);
	incompleteRegisterationMailerTenDay($options[e]);


}
catch(Exception $e){	global $baseurl;
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Incomplete Profile Mailer failed";
	Utils::sendEmail($to, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Incomplete Profile Mailer failed". $e->getTraceAsString(), E_USER_WARNING );}