<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";



/**
 * @Himanshu
 * Mailer for daily matches
 */

/*function ifMatchExists($user_id){

	try{
		$sql1 = "select query from user_recommendation_new where user_id = ? order by tstamp desc limit 1";
		$query = Query::SELECT($sql1, array($user_id), Query::$__slave, true);

		if($query!=null){
			$userQuery= $query['query'];

			//echo $userQuery;
			$user_ids = null;
			$result = Query::SELECT($userQuery, null);
			if($result == null) return false;
			foreach ($result as $col){
				$user_ids[] = $col['user_id'];
			}
			$userIdsCount = count($user_ids);

			$idList = implode(",", $user_ids);
			$sql2 = "SELECT (select count(user2) as count from user_like where user1 =? and user2 in ($idList)) +
			 (select count(user2) as count from user_hide where user1 = ? and user2 in ($idList)) as count "; 

			$countSet = Query::SELECT($sql2, array($user_id,$user_id), Query::$__slave, true);
			$countTocheck = $countSet['count'];

			if($userIdsCount > (Utils::$recommendation_count+$countTocheck)) return true;
		}
		return false;
	}
	catch(Exception $e){
		trigger_error ( "PHP WEB: Mailer failed". $e->getTraceAsString(), E_USER_WARNING );

	}

}*/

function sendMatchesMail($external_email = null){

	global $config, $conn_master, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl;
	try{
		$subject_arr = array("Lazy Sunday now has a plan.",
		"What Monday blues?",
		"Tuesdays are getting better." ,
		"Midweek happiness sometimes comes packed in a mail.",
		"Thursdays are happier when you have new matches to see.",
		"Freaky Fridays, NOT! Check out your new matches.",
		"Saturday Day Fever buster!"
		);

		$sysLog = new SystemLogger();


		$sql =" SELECT u.fname, u.email_id, u.user_id from user u JOIN (SELECT ur.user_id from user_recommendation_new ur JOIN (select user_id, max(tstamp) as ts from user_recommendation_new group by user_id )t on  ur.user_id = t.user_id AND ur.tstamp = t.ts AND ur.result != '[]')inrq on inrq.user_id = u.user_id where u.status in ('authentic', 'non-authentic') and u.email_id not like '%trulymadly.com%' and u.email_status is null";
		//$sql = "SELECT fname, email_id, user_id from user where status in ('authentic', 'non-authentic') and email_id not like '%trulymadly.com%' and email_status is null ";
		//$user = new UserData($user_id);

		$res = $conn_slave->Execute($sql);
		$result  = $res->GetRows();
		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=matches";
		$redirectionFile = "/matches.php";
		$utm_content="view_matches";
		$campaignName = "matches";

		foreach ($result as $col){

			//$flag = ifMatchExists($col['user_id']);
			//if($flag == false) continue;
				
			$smarty->assign('name', ucfirst($col['fname']));
			$toAddress = $col['email_id'];

			echo $col['user_id'];
			if(isset($external_email))
			$toAddress = $external_email;

			//echo $toAddress;
			$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $col['user_id']);
			$smarty->assign("analyticsLinks", $analyticsLinks);

			//var_dump($analyticsLinks);
			$data['email_id'] = $col['email_id'];
			$data['subject'] = "matches";
			$data['user_id'] = $col['user_id'];
			$mailObject = new MailFunctions();
			$dayOfWeek = date(w);
			$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/newmailers/emailMatches.tpl'),"","Hi " .ucfirst($col['fname']). ", ".$subject_arr[$dayOfWeek], $col['user_id']);
			$sysLog->logSystemMail($data);
			//exit();
		}
	}
	catch(Exception $e){
		//echo $e->getMessage();
		trigger_error("PHP WEB: ".$e->getTraceAsString(), E_USER_WARNING);
	}
}

try{
	$options=getopt ( "e:" );
	sendMatchesMail($options[e]);
}
catch (Exception $e){
	global $baseurl;
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Matches Mailer failed";
	$from = gethostname();
	Utils::sendEmail($to, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Matches Mailer failed". $e->getTraceAsString(), E_USER_WARNING );
}




?>