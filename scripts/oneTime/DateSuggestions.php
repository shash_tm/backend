<?php

require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../logging/systemLogger.php";

function mailDateSuggestions($external_email){
	global $conn, $config, $smarty;
	$sysLog = new SystemLogger();
	
	$sql = "SELECT  u.user_id, email_id, email_status from user u join user_demography ud on u.user_id = ud.user_id where status in ('authentic', 'non-authentic') and  ud.stay_city in (16743) and u.email_id not like '%trulymadly%'";
	$res = $conn->Execute($sql);
	$rows = $res->GetRows();

	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=date_suggestions";
	$redirectionFile = "";
	$utm_content="view_date_suggestions";
	$campaignName = "date_suggestions";

	$blogLink = "http://blog.trulymadly.com/dates-said". $campaign;
	$smarty->assign("blogLink", $blogLink);
	
	foreach ($rows as $r=>$val){
		if($c['email_status'] == null){
			$data = null;
			$data['email_id'] = $val['email_id'];
			$data['subject'] = "date_suggestions";
			$data['user_id'] = $val['user_id'];

			$toAddress = $val['email_id'];
			if(isset($external_email))
			$toAddress = $external_email;

			$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $val['user_id']);
			$smarty->assign("analyticsLinks", $analyticsLinks);

			//echo $smarty->fetch(dirname ( __FILE__ ).'/../../templates/utilities/emailStereotypes.tpl');
			//var_dump($analyticsLinks);
			//echo $toAddress;
			/*echo $blogLink;
			echo $val['user_id']; die;
			*/
			echo $toAddress;
			$mailObject = new MailFunctions();
			$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../../templates/utilities/others/DateSuggestions.tpl'),"","We want you to have the best date of your life!", $val['user_id'], 'TrulyMadly <smita.prakash@trulymadly.com>');
			$sysLog->logSystemMail($data);
		}
	}
}

try{
	mailDateSuggestions($argv[1]);
}
catch(Exception $e){
	//echo $e->getTraceAsString();
	trigger_error($e->getMessage(), E_USER_WARNING);
}
?>