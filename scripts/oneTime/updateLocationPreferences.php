<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";


/**
 * Update the location preferences for users who haven't updated the preference yet
 */
function updateLocationPreferences()
{
    global $conn_reporting, $conn_master;
    $sql = "select u.user_id, upd.location, upd.location_preference_for_matching from user u join user_preference_data upd on u.user_id = upd.user_id
            where upd.location is not null and upd.location not regexp '254' and upd.location regexp '113' and upd.location regexp ';' and u.status='authentic'";
    $res = $conn_reporting->Execute($sql);
    $counter = 0;
    while ($row = $res->FetchRow())
    {
        $user_id = $row['user_id'];
        echo "Updated ".$user_id .PHP_EOL;
        $counter++;
        $sql ='Update user_preference_data set location = concat(location,"254;") , location_preference_for_matching = concat(location_preference_for_matching,",254")
                where user_id = ?';
        $sql_prep =$conn_master->Prepare($sql);
        $sql_exec = $conn_master->Execute($sql_prep, array($user_id));
    }

    Utils::sendEmail('sumit@trulymadly.com','sumit@trulymadly.com','Location preference updated','Location preference updated for '.$counter. ' users');

}
if([php_sapi_name()=== 'cli'])
{
    updateLocationPreferences();
}


?>