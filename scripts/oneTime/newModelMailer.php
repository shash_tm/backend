<?php

require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../../include/config_admin.php";
include dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../logging/systemLogger.php";



/**
 * @Himanshu
 * Mailer for new model launch
 */

function sendMail($external_email = null){

	global $config, $conn_master, $conn_slave, $smarty, $baseurl;
	
		$sysLog = new SystemLogger();
		$sql =" SELECT u.fname, u.email_id, u.user_id from user u  where u.email_id not like '%trulymadly.com%' and u.email_status is null";

		$res = $conn_slave->Execute($sql);
		$result  = $res->GetRows();
		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=new_model_launch";
		$redirectionFile = "/index.php";
		$utm_content="view_new_model";
		$campaignName = "new_model_launch";

		foreach ($result as $col){

			$name = ucfirst($col['fname']);
			$smarty->assign('name', $name);
			$toAddress = $col['email_id'];

			echo $col['user_id'];
			if(isset($external_email))
			$toAddress = $external_email;

			$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $col['user_id']);
			$smarty->assign("analyticsLinks", $analyticsLinks);

			$data['email_id'] = $col['email_id'];
			$data['subject'] = "new_model_launch";
			$data['user_id'] = $col['user_id'];
			$mailObject = new MailFunctions();
			$subject = $name.", let’s better your chances of finding love.";
			$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../../templates/utilities/newmailers/whatsnewmailer.tpl'),"",$subject, $col['user_id']);
			$sysLog->logSystemMail($data);
			//exit();
		}
	}

try{
	$options=getopt ( "e:" );
	sendMail($options[e]);
}
catch (Exception $e){
	
	global $techTeamIds, $baseurl;
	$to = $techTeamIds;
	$subject = "Mailer for new model launch " . $baseurl;
	Utils::sendEmail($to, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Mailer for new model launch". $e->getTraceAsString(), E_USER_WARNING );
	
}




?>