<?php


//require_once dirname ( __FILE__ ) . "/../../include/config_himanshu.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";

require_once dirname ( __FILE__ ) . "/../../include/Utils.php"; 

function fillActivityColumns(){
	global $conn_slave, $conn_master;

	$conn_slave->SetFetchMode ( ADODB_FETCH_ASSOC );
	$k = 14;
	for($i=1; $i<=14; $i++){
		$filename = '/tmp/activity.txt';
		if(file_exists($filename))
		unlink($filename);

		$tempTable  = "user_pi_temp";
		$createTemp = "CREATE TEMPORARY TABLE $tempTable (user_id int(10), visibility int(5) default 0)";
		$conn_master->Execute($createTemp);
		echo "$i) temp table creation";


		$sql = "select u.user_id, COALESCE(t1.like_count,0)  + COALESCE(t2.hide_count, 0) as total_action from user u       left join (select count(user1) as like_count, user2 from user_like where date(timestamp)=date_sub(curdate(), interval $k day) group by user2 )t1        on t1.user2 = u.user_id       left join (select count(user1) as hide_count, user2 from user_hide where date(timestamp)=date_sub(curdate(), interval $k day)  group by user2)t2        on u.user_id = t2.user2          where status ='authentic' and COALESCE(t1.like_count,0)  + COALESCE(t2.hide_count, 0) > 0 ";
		$res = $conn_slave->Execute($sql);
		$arr = array();
		//	$rows = $res->GetRows();

		while ($row = $res->FetchRow()){
			$str = "";
			foreach ($row as $val){
				$str .= (!isset($val))?"\N\t":"$val\t";
			}
			$str .="\n";
			//echo $str;
			file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
			//			$arr[] = array($val['user_id'], $val['total_action'], $val['total_action']);
		}

		echo "written to file";
		$sql = "LOAD DATA LOCAL INFILE '$filename' INTO TABLE $tempTable fields terminated by '\t' ";
		$ex = $conn_master->Execute($sql);

		echo "loading date into temp table";

		//$j = $i -1;

		$activityColumnToUpdate = "visibility_day". (date('z')+$i) % 14;
		echo "\n". $activityColumnToUpdate."\n";
		$k--;

		$updateSql  = "INSERT INTO `user_pi_activity` (user_id, $activityColumnToUpdate)
		 				SELECT * FROM $tempTable  on duplicate key update $activityColumnToUpdate = values ($activityColumnToUpdate) ";
		$exUpdate  = $conn_master->Execute($updateSql);
			
		echo "writing to actual table" ;
			
		$dropSql = "DROP table $tempTable ";
		$exDrop = $conn_master->Execute($dropSql);


			
			
		/*	while ($row = $resultSelect->FetchRow()) {
		 if($row['age'] == 0 )continue;
		 $str = '';
		 foreach ($row as $val){
			$str .= (!isset($val))?"\N\t":"$val\t";
			//$str .= "$val\t";

			}
			$str .="\n";
			//echo $str;
			file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
			}
			*/


		/*foreach ($rows as $val){
			$arr[] = array($val['user_id'], $val['total_action'], $val['total_action']);
			}*/

		/*	echo "inserting rows ... ". count($arr);
		 $j = $i -1;
		 $k--;

		 $conn_master->bulkBind = true;

		 if(!empty($arr)){
			$sql2 = $conn_master->Prepare("INSERT INTO user_pi_activity (user_id, visibility_day$j) values (?,?) on duplicate key update visibility_day$j = ?");
			$conn_master->Execute($sql2, $arr);
			}*/
	}

}


function fillLagInBucketTable(){
	global $conn_slave, $conn_master;
	$conn_slave->SetFetchMode ( ADODB_FETCH_ASSOC );
		$filename = '/tmp/likeHides.txt';
		if(file_exists($filename))
		unlink($filename);

		$tempTable  = "user_likeHide_temp";
		$createTemp = "CREATE TEMPORARY TABLE $tempTable (user_id int(10), likes int(5) default 0, hides int(5) default 0)";
		$conn_master->Execute($createTemp);
		echo "\n temp table creation for filling lag";


		$sql = "select u.user_id, COALESCE(t1.like_count,0) as likes, COALESCE(t2.hide_count, 0) as hides from user u       left join (select count(user1) as like_count, user2 from user_like where date(timestamp)>='2015-01-19' and date(timestamp)<='2015-02-03' group by user2 )t1        on t1.user2 = u.user_id       left join (select count(user1) as hide_count, user2 from user_hide where date(timestamp)>='2015-01-19' and date(timestamp)<='2015-02-03'  group by user2)t2        on u.user_id = t2.user2          where status ='authentic' and COALESCE(t1.like_count,0) + COALESCE(t2.hide_count,0) > 0 ";
		$res = $conn_slave->Execute($sql);
		$arr = array();
		//	$rows = $res->GetRows();

		while ($row = $res->FetchRow()){
			$str = "";
			foreach ($row as $val){
				$str .= (!isset($val))?"\0\t":"$val\t";
			}
			$str .="\n";
			//echo $str;
			file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
			//			$arr[] = array($val['user_id'], $val['total_action'], $val['total_action']);
		}

		echo "written to file";
		$sql = "LOAD DATA LOCAL INFILE '$filename' INTO TABLE $tempTable fields terminated by '\t' ";
		$ex = $conn_master->Execute($sql);

		echo "loading date into temp table";


		$updateSql  = "INSERT INTO `user_pi_activity` (user_id, total_likes, total_hides)
		 				SELECT * FROM $tempTable  on duplicate key update total_likes = total_likes + likes , total_hides = total_hides + hides ";
		$exUpdate  = $conn_master->Execute($updateSql);
			
		echo "writing to actual table" ;
			
		$dropSql = "DROP table $tempTable ";
		$exDrop = $conn_master->Execute($dropSql);

	
}

try{
	fillActivityColumns();
	fillLagInBucketTable();
}
catch (Exception $e) {
	$msg = "One time activity generation script failed";
	echo $e->getMessage();
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}




?>
