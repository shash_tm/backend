<?php
require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
include_once dirname ( __FILE__ ) . '/../../photomodule/photoUtils.php';

/*
 * To convert and store users photos in the webp format in a one time script
 * Author :: Sumit
 */

class createAndStoreWebp
{
    private $conn_master ;
    private $conn_slave ;
    private $imageurl;
    private $userListObj;
    private $image_basedir;
    private $user_count;
    private $photos_count;
    function __construct()
    {
        global $conn_slave, $conn_master,$imageurl,$image_basedir;  ;
        $this->conn_master = $conn_master ;
        $this->conn_slave = $conn_slave ;
        $this->conn_slave->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->imageurl = $imageurl ;
        $this->image_basedir = $image_basedir ;
        $this->userListObj;
        $this->user_count;
        $this->photos_count;
    }

    public function performTasks()
    {
        $this->getUsersList();
        while($row = $this->userListObj->FetchRow())
        {
            $this->getPhotosAndConvert($row['user_id']);
        }

        $message = "TOtal $this->photos_count photos converted from $this->user_count users";
        echo $message;

    }

    private function getUsersList()
    {
        $sql = "SELECT user_id FROM user_search";
        $sql_prep = $this->conn_slave->Prepare($sql);
        $this->userListObj = $this->conn_slave->Execute($sql_prep);
    }

    private function getPhotosAndConvert($user_id)
    {
        $sql_photo = "SELECT user_id, photo_id, name, thumbnail from user_photo WHERE user_id = ? and status = 'active' and webp_flag='no'";
        $sql_photo_prep = $this->conn_slave->Prepare($sql_photo);
        $sql_photo_exec = $this->conn_slave->Execute($sql_photo_prep, array($user_id));

        $this->user_count++ ;

        $update = "UPDATE user_photo set webp_flag ='yes' WHERE photo_id = ?";
        $sql_update_prep = $this->conn_master->Prepare($update);

        while ($row = $sql_photo_exec->FetchRow())
        {
            try
            {
                $image = $row['name'] ;
                $thumbnail_image = $row['thumbnail'] ;

                $local_image = $this->image_basedir.$image ;
                $local_thumbnail_image = $this->image_basedir . $thumbnail_image;

                PhotoUtils::downloadFileFromURL($this->imageurl.$image , $local_image) ;
                PhotoUtils::downloadFileFromURL($this->imageurl.$thumbnail_image , $local_thumbnail_image) ;

                $webp_image = Utils::getWebpName($image);
                $webp_thumbnail_image = Utils::getWebpName($thumbnail_image);

                $webp_local_image = Utils::getWebpName($local_image);
                $webp_local_thumbnail_image = Utils::getWebpName($local_thumbnail_image);

                PhotoUtils::convertTowebP($local_image, $webp_local_image);
                PhotoUtils::convertTowebP($local_thumbnail_image,$webp_local_thumbnail_image);

                Utils::saveOnS3($webp_local_image, "files/images/profiles/".$webp_image);
                Utils::saveOnS3($webp_local_thumbnail_image, "files/images/profiles/".$webp_thumbnail_image);

                unlink($local_image);
                unlink($local_thumbnail_image);
                unlink($webp_local_image);
                unlink($webp_local_thumbnail_image);

                $this->conn_master->Execute($sql_update_prep, array($row['photo_id']));
                $this->photos_count++ ;
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
            }

        }

    }



}




try{
    if(php_sapi_name() == 'cli')
    {
        $imageObj = new createAndStoreWebp();
        $imageObj->performTasks();
    }
}
catch(Exception $e){
    echo $e->getMessage();
}


?>
