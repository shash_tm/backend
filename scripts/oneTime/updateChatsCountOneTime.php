<?php

//require_once dirname ( __FILE__ ) . "/../../include/matchConstants.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
//require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";

require_once dirname ( __FILE__ ) . "/../../include/Utils.php";


/**
 * get active chats count per user of a week (which are not blocked by either party)
 */
function updateActiveChats ()
{
	global $conn_reporting, $conn_master;
	$sql = "select mq1.sender_id , count(mq1.receiver_id) as count from messages_queue mq1 join messages_queue mq2 on (mq1.sender_id = mq2.receiver_id and mq1.receiver_id = mq2.sender_id) where mq1.tStamp >=date_sub(curdate(), interval 1 week) and mq2.tStamp >=date_sub(curdate(), interval 1 week) and mq1.blocked_by is null and mq2.blocked_by is null group by mq1.sender_id";
	$res = $conn_reporting->Execute($sql);
	$arr = array();
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{
			$arr[] = array($row['count'], $row['sender_id']);
		}
	}
	
	//mark all chats to 0 first everytime so that we can update the correct value
	$sql_up = "UPDATE user_pi_activity set active_chats = 0";
	$conn_master->Execute($sql_up);
	
	//update the week stats for chats
	$conn_master->bulkBind = true;
	$sql2 = "UPDATE user_pi_activity set active_chats = ? where user_id = ?";
	$conn_master->Execute($sql2, $arr);
	
	
}

updateActiveChats();

?>