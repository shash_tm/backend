<?php

require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once  dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../../logging/systemLogger.php";

function mailStereos($external_email){
	global $conn, $config, $smarty;
	$sysLog = new SystemLogger();
	$sql = "SELECT fname, user_id, email_id, email_status from user where status in ('authentic', 'non-authentic')";
	$res = $conn->Execute($sql);
	$rows = $res->GetRows();

	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=stereotypes";
	$redirectionFile = "";
	$utm_content="view_stereotypes";
	$campaignName = "stereotypes";

	foreach ($rows as $r=>$val){
		if($c['email_status'] == null){
			$data = null;
			$data['email_id'] = $val['email_id'];
			$data['subject'] = "stereotypes";
			$data['user_id'] = $val['user_id'];

			$smarty->assign("name", ucfirst($val['fname']));
			$toAddress = $val['email_id'];
			if(isset($external_email))
			$toAddress = $external_email;

			$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $val['user_id']);
			$smarty->assign("analyticsLinks", $analyticsLinks);

			//echo $smarty->fetch(dirname ( __FILE__ ).'/../../templates/utilities/emailStereotypes.tpl');
			//var_dump($analyticsLinks);
			echo $val['user_id'];
			$mailObject = new MailFunctions();
			$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../../templates/utilities/emailStereotypes.tpl'),"","Be a part of the Breaking Stereotypes campaign", $val['user_id'], 'TrulyMadly <smita.prakash@trulymadly.com>');
			$sysLog->logSystemMail($data);
		}
	}
}

try{
	mailStereos($argv[1]);
}
catch(Exception $e){
	//echo $e->getTraceAsString();
	trigger_error($e->getMessage(), E_USER_WARNING);
}
?>