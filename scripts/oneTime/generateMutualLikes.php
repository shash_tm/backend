<?php

include dirname ( __FILE__ ) . "/../../include/config.php";
include dirname ( __FILE__ ) . "/../../include/Utils.php";


function MutualLikes(){

	global $conn;

	$sql = "select ul.user1, ul.user2, ul.timestamp from user_likeMe ul
			JOIN user_likeMe ulm on ulm.user1 = ul.user2 and ulm.user2 =  ul.user1 
			order by ul.user1, ul.timestamp desc";
	$res = $conn->Execute($sql);
	$rows = $res->GetRows();

	echo $sql;
	$conn->bulkBind = true;
	$arr = array();
	$insertSql = $conn->Prepare("INSERT ignore INTO user_mutual_like values(?,?,now())");


	foreach ($rows as $val){
		if($val['user1'] > $val['user2'])
		$arr[] = array($val['user1'], $val['user2']);
		else
		$arr[] = array($val['user2'], $val['user1']);
	}

	if(!empty($arr)){
		$conn->Execute($insertSql, $arr );
	}

}


function msgToMutual(){
	
	global $conn;
	$sql = "select sender_id, receiver_id from  messages_queue";
	$ex = $conn->Execute($sql);
	$res = $ex->GetRows();
	
	echo $sql;
	$conn->bulkBind = true;
	$arr = array();
	$insertSql = $conn->Prepare("INSERT ignore INTO user_mutual_like values(?,?,now())");
	
	foreach ($res as $row => $val){
		if($val['sender_id'] > $val['receiver_id'])
		$arr[] = array($val['sender_id'], $val['receiver_id']);
		else
		$arr[] = array($val['receiver_id'], $val['sender_id']);
	}
	
	$conn->Execute($insertSql, $arr );
	
}




try{
	MutualLikes();
	msgToMutual();
}

catch(Exception $e){
	echo $e->getMessage();
}