<?php 

if(php_sapi_name() !== 'cli')
{
	echo 'Not running from CLI';
	die;
}
if(!isset($argv[1]))
{
	echo 'No field name mentioned as Command Line Argument,please mention institute or company';
	die;
}
$field=null;
$similar_text_threshold=-1;
if($argv[1]=="institute")
{
	$field="institute_details";
	$similar_text_threshold=60;
}
else if($argv[1]=="company")
{
	$field="company_name";
	$similar_text_threshold=60;
}
else
{
   echo "Not a valid option, Please mention 'institute' or 'company'".PHP_EOL;
   die;
}


require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
if($field!=null)
{
	//Continue Execution
	$sql="select $field,user_data.user_id as id from trulymadly_prod.user_data join trulymadly_prod.user_search on user_data.user_id=user_search.user_id;";
	$rs=$conn_reporting->Execute($sql);
  
    while($row=$rs->fetchRow())
    {
    	$user_id=$row['id'];
    	
    	$data_arr=json_decode($row[$field],true);
    	
    	$institute=array();
    	if(count($data_arr)==0)
    	{
    		continue;
    	}
    	//Insert into log, to keep a backup
    	$sql_log="insert into duplicates_log values(?,?,?)";
    	Query_Wrapper::INSERT($sql_log,array($user_id,$row[$field],$field),"duplicates_log","master",null);
    	
    	foreach ($data_arr as $item)
    	{
    		$unset_index=-1;
    		$insert=true;
    		if(count($institute)>=1)
    		{
    			$index=0;
    			
    			foreach ($institute as $inst)
    			{
    				$similarity_percent=0;
    				$str=strtolower($item);
    				$str1=strtolower($inst);
    				similar_text($str,$str1,$similarity_percent);
    				//echo $str.PHP_EOL.$str1.PHP_EOL.$similarity_percent.PHP_EOL.PHP_EOL;
    				 
    				if($similarity_percent>$similar_text_threshold)
    				{
    					if(strlen($str)>strlen($str1))
    					{
    						//Insert this into array and unset the $index position in the array
    						$unset_index=$index;
    						
    							
    					}else
    					{
    						//
    						$insert=false;
    					}
    				}
    				$index++;
    			}
    		}
    		if($unset_index!=-1)
    		{
    			
    			//The index has beeen changed, proceed to unset
    			$institute=Utils::custom_unset($institute,$unset_index);
    		}
    		if($insert)
    			$institute[] = $item;
    		
    		
    		
    	}
    	//$val=json_encode($institute,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    	$val=json_encode($institute);
    	$update_sql="update user_data set $field=? where user_id=?";
    	//echo $val.PHP_EOL;
    	Query_Wrapper::UPDATE($update_sql,array($val,$user_id),"user_data","master",$user_id);
    	//print_r($institute);
    	
    	
    }



}

	
	





?>