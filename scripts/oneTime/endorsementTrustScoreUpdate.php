<?php

require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../TrustBuilder.class.php";


/**
 * Get all users who have at least one endorsement of any gender and give them endorsement trust score
 * and Authenticate (if Eligible)
 */

Class EndorsementOneTimeTrustScore
{
    private $conn_reporting;
    private $conn_master;
    private $allUsers;

    function __construct()
    {
        global $conn_reporting, $conn_master ;
        $this->conn_master = $conn_master ;
        $this->conn_reporting = $conn_reporting ;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->allUsers ;
    }

    public function performAction()
    {

        $this->getUsersToUpdate() ;
        $sql_update = "update user_trust_score set endorsement_score= 15 , trust_score= trust_score + 15 WHERE
                      user_id = ? and endorsement_score = 0" ;
        $sql_update_prep = $this->conn_master->Prepare($sql_update);

        while($row = $this->allUsers->FetchRow())
        {
            $user_id = $row['user_id'] ;

            $this->conn_master->Execute($sql_update_prep, array($user_id)) ;

            $trustBuilder = new TrustBuilder($user_id);
            $trustBuilder->authenticateUser();
            echo $user_id . PHP_EOL;



        }
    }

    private function getUsersToUpdate ()
    {
        $sql = "select t.user_id from (select distinct(user_id)  from user_endorsements )t
                join user_trust_score uts on t.user_id = uts.user_id where endorsement_score =0" ;
        $sql_prep = $this->conn_reporting->Prepare($sql);
        $this->allUsers = $this->conn_reporting->Execute($sql_prep);
    }


}


try
{
    $endorseObj = new EndorsementOneTimeTrustScore();
    $endorseObj->performAction();
}
catch (Exception $e)
{
    echo $e->getMessage();
}

?>