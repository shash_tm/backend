<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserActions.php";
require_once dirname ( __FILE__ ) . "/../include/User.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../logging/UnsubscriptionClass.php";

/*
 * Emailers to be sent weekly viz. users logging in or not logging in weekly
 * @Himanshu
 */

function notLoggedInReceivedMatches($external_mail = null){
	global $conn_slave, $baseurl, $smarty;
	$uu = new UserUtils();
	$sysLog= new SystemLogger();

	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=notLogged_Received_Matches";
	$redirectionFile = "/register.php";
	$utm_content="view_notLogged_matches";
	$campaignName = "notLogged_Received_Matches";

	$sql = "select distinct(ur.user1), u.fname as name, u.email_id, u.email_status from user_recommendation ur
			 join user u 
			on ur.user1 = u.user_id 
			where date( u.last_login) < date_sub(curdate(), interval 7 day) and date(ur.timestamp) >= date_sub(curdate(), interval 7 day)";
	$res = $conn_slave->Execute($sql);
	$rows = $res->GetRows();

	foreach ($rows as $rows=>$col){

		if($col['email_status'] == null){
			$isApproved = $uu->isAnyApprovedPic($col['user1']);
			$smarty->assign("name", ucfirst($col['name']));
			$smarty->assign("isApproved",$isApproved);

			$sql1 = $conn_slave->Prepare("SELECT * FROM user_recommendation where user1 = ? and date(timestamp) >= date_sub(curdate(), interval 7 day) order by timestamp desc limit 4");
			$res1 = $conn_slave->Execute($sql1, array($col['user1']));
			$row1 = $res1->GetRows();
			$id_arrays = array();

			foreach ($row1 as $r){
				$id_arrays[] = $r['user2'];
			}

			$details = $uu->getAttributes("matches", $id_arrays, $col['user1']);
			$arr = array();

			foreach ($row1 as $c){
				/*
				 $str = $col['user1'] .'|' . $c['user2'];
				 $mid = Utils::calculateCheckSum($str);*/
				$profile_link = $uu->generateProfileLink($col['user1'], $c['user2'], $campaign);

				$arr[$col['user1']][] = array(
						   		  "user_id" => $c['user2'],
								  "name" => $details[$c['user2']]['fname'],// . ' ' . $col['lname'],
								  "age" =>$details[$c['user2']]['age'],
								  "religion" => ucfirst($details[$c['user2']]['religion']),
								  "city" => $details[$c['user2']]['city'],
								  "height" => $details[$c['user2']]['height'],
								  "pic" => $details[$c['user2']]['image_url'],
								  "trust_score" => (isset($details[$c['user2']]['trust_score'])?$details[$c['user2']]['trust_score']:0),
								  "profile_link" =>	$profile_link	  
				//"profile_link" => $baseurl."/profile.php".$campaign."&utm_content=profile&uid=".$col['user1']."&pid=$mid".'_'.$c['user2']
				);

			}
			if(isset($external_mail)) $toAddress = $external_mail;
			else $toAddress= $col['email_id'];

			$smarty->assign("data", array_values($arr));

			$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $col['user1']);
			$smarty->assign("analyticsLinks", $analyticsLinks);

			$data['email_id'] = $toAddress;
			$data['subject'] = "notLogged_Received_Matches";
			$data['user_id'] = $col['user1'];
			echo $toAddress;
			$mailObject = new MailFunctions();
			$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/emailMatchesNoLogdIn.tpl'),"",ucfirst($col['name']).", Your matches are missing you!", $col['user1']);
			$sysLog->logSystemMail($data);
		}
	}

	Utils::sendEmail(Utils::$techTeam, 'admin', "notLoggedInReceivedMatches", "Mail sent to users who have not logged in past 7 days but received matches");
}

function loggedInNotReceivedMatches($external_mail = null){

	global $conn, $baseurl, $smarty;
	$uu = new UserUtils();
	$sysLog= new SystemLogger();

	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=Logged_NotReceived_Matches";
	$redirectionFile = "/register.php";
	$utm_content="view_Logged_noMatches";
	$campaignName = "Logged_NotReceived_Matches";

	$sql = "select distinct( ur.user1 ),u1.fname as name, u1.email_id, u1.email_status from user_recommendation ur 
			join user u1 on ur.user1 = u1.user_id 
			where ur.user1 in  
			(select ( u.user_id) from user u left join ( select distinct(ur2.user1) from user_recommendation ur2 where date(ur2.timestamp)>=date_sub(curdate(), interval 7 day) )t 
			on u.user_id=t.user1 
			where u.last_login>date_sub(curdate(), interval 7 day) and t.user1 is null)"; 
	/*$sql = "select distinct(ur.user1), u.fname as name, u.email_id, u.email_status from user_recommendation ur
			 join user u on ur.user1 = u.user_id 
			where date( u.last_login) >= date_sub(curdate(), interval 7 day) and date(ur.timestamp) < date_sub(curdate(), interval 7 day)";
	*/$res = $conn_slave->Execute($sql);
	$rows = $res->GetRows();

	foreach ($rows as $rows=>$col){

		if($col['email_status'] == null){
			$smarty->assign("name", ucfirst($col['name']));

			if(isset($external_mail)) $toAddress = $external_mail;
			else $toAddress= $col['email_id'];


			$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $col['user1']);
			$smarty->assign("analyticsLinks", $analyticsLinks);

			$data['email_id'] = $toAddress;
			$data['subject'] = "Logged_NotReceived_Matches";
			$data['user_id'] = $col['user1'];
			echo $toAddress;
			$mailObject = new MailFunctions();
			$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/emailNoMatchesLogdIn.tpl'),"",ucfirst($col['name']).", Your matches are around the corner", $col['user1']);
			$sysLog->logSystemMail($data);
		}
	}

	Utils::sendEmail(Utils::$techTeam, 'admin', "loggedInNotReceivedMatches", "Mail sent to users who have logged in past 7 days but not received matches");
}


try{
	notLoggedInReceivedMatches($argv[1]);
	loggedInNotReceivedMatches($argv[1]);
}

catch(Exception $e){
	$subject = "Weekly Mailer failed";
	Utils::sendEmail(Utils::$techTeam, 'admin', $subject, $e->getMessage());
	trigger_error ( $e->getTraceAsString() , E_USER_WARNING );
}