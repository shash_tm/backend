<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../mobile_utilities/pushNotification.php";


$conn_slave->SetFetchMode ( ADODB_FETCH_ASSOC );
$conn_master->SetFetchMode ( ADODB_FETCH_ASSOC );


/**
 * @Himanshu
 * Mailer for Fashion n You vouchers
 * Usage: pass userIds in command line arg e.g. manualMailerFnU.php -e "<user_ids>" 
 */

function sendMail( $data, $voucher_data, $campaignName, $campaign, $redirectionFile, $utm_content){

	global $smarty;

	$sysLog = new SystemLogger();

	$ts = Utils::getDateGMTTOISTfromUnixTimestamp($time);
	$smarty->assign("data", $data);
	$smarty->assign("name", ucfirst($data['fname']));
	$smarty->assign("ts", $ts);
	$smarty->assign("voucher", $voucher_data);
	$toAddress = $data['email_id'];

	if(isset($external_email)) $toAddress = $external_email;

	$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $data['user_id']);
	$smarty->assign("analyticsLinks", $analyticsLinks);
	$mailObject = new MailFunctions();
	$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/newmailers/FNUvoucher.tpl'),"","Hey ". ucfirst($data['fname']).", you've won a FashionAndYou voucher!", $data['user_id']);

	$logdata['email_id'] = $data['email_id'];
	$logdata['subject'] = $campaignName;
	$logdata['user_id'] = $data['user_id'];
	$sysLog->logSystemMail($logdata);
}


function updateVoucher($id, $campaign){
	global $conn_master;
	$sql = $conn_master->Prepare("UPDATE vouchers SET user_id = ? , tstamp = now() WHERE user_id is null and campaign = ? LIMIT 1");
	$conn_master->Execute($sql, array($id, $campaign));

	$sql2= $conn_master->Prepare("SELECT * FROM vouchers where campaign =? and user_id = ?");
	$res = $conn_master->Execute($sql2, array($campaign, $id));
	$row = $res->FetchRow();
	return $row;
}





function sendVoucher($ids){

	global $config, $conn_slave, $smarty, $baseurl;

	//query needs to be changed in case u want to send multiple vouchers to same user (null check to be removed)
	
	

	var_dump($ids);
	$ids_arr = explode(',', $ids);
	$voucher_tag = 'fashion_n_you_selected';
	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$voucher_tag";
	$redirectionFile = "/matches.php";
	$utm_content="view_$voucher_tag";
	$campaignName = $voucher_tag;


	$campaignColumn = $voucher_tag;

	$sql = "select * from user where user_id in ($ids)";
	$res = $conn_slave->Execute($sql);
	$rows = $res->GetRows();
	$details = array();
	foreach ($rows as $col){
		$details[$col['user_id']] = $col;
	}
	//var_dump($data); die;
	foreach ($ids_arr as $id){
		$voucher = null;
		$voucher = updateVoucher($id, $campaignColumn);
		$data = $details[$id];
		sendMail($data, $voucher, $campaignName, $campaign, $redirectionFile, $utm_content);
	}
}


try{
	global $baseurl;
	$options=getopt ( "e:" );
	sendVoucher($options[e]);
}
catch (Exception $e){
	echo $e->getMessage();
	global $baseurl, $techTeamIds;
	$subject = $baseurl. " - Voucher Mailer Failed";
	Utils::sendEmail($techTeamIds, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Voucher Mailer failed". $e->getTraceAsString(), E_USER_WARNING );
}

?>