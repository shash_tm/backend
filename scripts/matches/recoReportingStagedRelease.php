<?php

require_once dirname ( __FILE__ ) . "/../../include/matchConstants.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../Advertising/IgnoredProfileUsersForReporting.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php"; 
require_once dirname ( __FILE__ ) . "/recoNoMatchesReport.php"; 

/**
 * reporting for AB running on PI logic
 * @Himanshu
 */

$duration = 1;
$date = "curdate()";
if ($argc > 1) {
    $duration = $argv[1];
    if ($argc > 2) {
        $date = $argv[2];
    }
}
function getRecoData($date, $duration){
	global $conn_reporting, $smarty, $miss_tm_id;
	$echo_count=0;
	$ignoredAdCampaignUserIds = IgnoredProfileUsersForReporting::getUserIdsString();
	$defaultConstant = Utils::$defaultBucketConstant;
	$slots_filled_limit = Utils::$availability_slots_limit;
	$slots_filled_limit_18_20= Utils::$availability_slots_limit_18_20;
	$slots_filled_limit_21_22= Utils::$availability_slots_limit_21_22;
	$slots_filled_limit_23_26= Utils::$availability_slots_limit_23_26;
	$ab_variable = Utils::$recoStagedReleaseConstant;
	
// 	$slots_filled_limit=40;
	echo "\n".$echo_count++;
	//$no_match_obj = new noMatchesRecoReport();
	echo "\n".$echo_count++;
	//$report_no_match = $no_match_obj->getData($abFlag = true);

	echo "\n".$echo_count++;
	//$smarty->assign("noMatchReport", $report_no_match);
    $t1=time();
	$sql1 = "select count(up.user_id) as count,gender,  bucket from  user_search up where user_id%$ab_variable=0  and user_id not in $ignoredAdCampaignUserIds group by up.gender, bucket";
 $result1 = $conn_reporting->Execute($sql1);
	$rows1 = $result1->GetRows();
	//var_dump($rows1);
	$smarty->assign("bucketStats",  $rows1);
    echo "\n".$echo_count++;
   //$sql2="select count(k.user_id) as count , case when u.gender='M'  then  (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=70 then '31-70' else '70+' end)end)end) end ) end as matches,        sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0)  = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) )/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where tstamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and tstamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE)  group by user1)t2   on t.user_id = t2.user1        where coalesce(t2.hide_count,0) +coalesce(t1.like_count,0) > 0  )k     join user u on k.user_id = u.user_id   where u.status = 'authentic'          group by u.gender, case when u.gender='M' then   (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=70 then '31-70' else '70+' end)end)end) end ) end";
	
	$sql2="select count(k.user_id) as count , case when u.gender='M'  then
  (case when k.cntFetch =0 then '0' else 
  (case  when k.cntFetch <=5 then '01-05' 
  else 
  (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)   
            else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=30 then '01-30'

            else (case when k.cntFetch <=99 then '31-99' else (case when k.cntFetch =100 then '100' else '100+' end)end)end) end ) end as matches,        sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0)  = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) )/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where tstamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and tstamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE)  and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1)t        left join (select count(user2) as like_count, user1  from user_like where timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds


              group by user1)t2   on t.user_id = t2.user1        where coalesce(t2.hide_count,0) +coalesce(t1.like_count,0) > 0  )k     join user u on k.user_id = u.user_id   where u.status = 'authentic'          group by u.gender, case when u.gender='M' then   (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=30 then '01-30' else (case when k.cntFetch <=99 then '31-99' else (case when k.cntFetch =100 then '100' else '100+' end)end)end) end ) end";
	
	
	$result2 = $conn_reporting->Execute($sql2);
 
	$rows2 = $result2->GetRows();
	echo "\n".$echo_count++;
 
	$sql_zero_action="select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=30 then '01-30' else (case when k.cntFetch <=99 then '31-99' else (case when k.cntFetch =100 then '100' else '100+' end)end)end) end ) end as matches, u.gender      from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0)  = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) )/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where tstamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and tstamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE)  group by user1)t2   on t.user_id = t2.user1        where coalesce(t2.hide_count,0) +coalesce(t1.like_count,0) = 0  )k            join user u on k.user_id = u.user_id   where u.status = 'authentic'  group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=30 then '01-30' else (case when k.cntFetch <=99 then '31-99' else (case when k.cntFetch =100 then '100' else '100+' end)end)end) end ) end";
	
	$res_no_action = $conn_reporting->Execute($sql_zero_action);
 
	$rows_no_action = $res_no_action->GetRows();
 
	$sql_zero_match = "select  count(t.user_id) as count,  gender   from  (select user_id,   sum(    case    when isLikeQuery =1 then ifnull(countFetched,0)    else    ( case       when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0)       else countToFetch       end )     end  )   as cntFetch   from user_recommendation_query_log   where countFetched is not null and   tstamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and tstamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user_id not in $ignoredAdCampaignUserIds   group by user_id)t    join   user u   on u.user_id = t.user_id   where t.cntFetch = 0 group by u.gender";
	
	$result_zero = $conn_reporting->Execute($sql_zero_match);
 
	$rows_zero = $result_zero->GetRows();
	echo "\n".$echo_count++;
	//	$smarty->assign("zeromatchesStats",  $rows_zero);
    
	$arr = array();
	$i = 0;
	
 foreach ($rows_zero as $val){
		$val['matches'] = 0;
		$arr[$val['gender']][] = $val ;
	}
	
	$j = 0;
	$no_action = array();
	foreach ($rows_no_action as $val){
		$no_action[$val['gender']][$val['matches']] = $val['count'];
	}
	
	foreach ($rows2 as $val){
		$val['no_action']= $no_action[$val['gender']][$val['matches']] ; 
		$arr[$val['gender']][] = $val ;
	}
	echo "\n".$echo_count++;
	ksort($arr);
	echo "\n".$echo_count++;
	$smarty->assign("matchesStats_male",  $arr['M']);
	$smarty->assign("matchesStats_female",  $arr['F']);
	$maleUsageSum = 0;
	foreach($arr['M'] as $item) {
		$maleUsageSum += $item['count'];
	}
	$femaleUsageSum = 0;
	foreach($arr['F'] as $item) {
		$femaleUsageSum += $item['count'];
	}

	/**
	 * no matches by bucket and by age 
	 */
	
 $sql_zero_match_bucket_age = "select count(t.user_id) as count, case when bucket is null then 0 else bucket end as bucket , 
								  case when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >=18 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=20 then '18-20' when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >20 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=22 then '20-22' else (case when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >22 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=26 then '22-26' else (case when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >26 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=30 then '26-30' else '30+' end) end ) end as age_group 
								  from (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch 
								  	from user_recommendation_query_log where countFetched is not null and tstamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE)  and tstamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user_id not in $ignoredAdCampaignUserIds group by user_id)t 
								  join user u on u.user_id = t.user_id 
								  join user_data ud on ud.user_id = t.user_id 
								  left join user_pi_activity up on up.user_id = t.user_id 
								  where  t.cntFetch = 0 and gender = 'M' 
								  group by case when bucket is null then 0 else bucket end,   
								  	case when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >=18 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=20 then '18-20' when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >20 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=22 then '20-22' else (case when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >22 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=26 then '22-26' else (case when TIMESTAMPDIFF(year,ud.DateOfBirth,$date) >26 and TIMESTAMPDIFF(year,ud.DateOfBirth,$date) <=30 then '26-30' else '30+' end) end ) end";
	
	
 
$result_noMatch_bucket_age = $conn_reporting->Execute ($sql_zero_match_bucket_age);

$rows_bucket_age_noMatch = $result_noMatch_bucket_age->GetRows();
	
	$smarty->assign ("noMatch_age_bucket", $rows_bucket_age_noMatch);
	echo "\n".$echo_count++;
	$sql_like_percent = "select gender, up.bucket,  round(sum(t.like_perc)*100/count(t.user1),2) as percent  from (select t1.user1, t1.liked/(t1.liked+ ifnull(t2.hide,0)) as like_perc from  (select user1, count(user2) as liked from user_like where  timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1)t1 left join  (select user1, count(user2) as hide from user_hide where  timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1)t2 on t1.user1 = t2.user1)t  join user u on u.user_id = t.user1  join user_pi_activity up on t.user1 = up.user_id  group by u.gender, up.bucket";
	
	$res_like_percent = $conn_reporting->Execute($sql_like_percent);
 
	$rows_like_percent = $res_like_percent->GetRows();
	
	$likes_rows = array();
 
	foreach ($rows_like_percent as $val)
	{
		$likes_rows[$val['gender']][$val['bucket']] = $val['percent'];
}

	$smarty->assign("likesPercent", $likes_rows);
	
	echo "\n".$echo_count++;
	$sql_mutual_like = "select u.gender, count(x.fcount) as num, case when x.fcount <=5 then x.fcount else '5plus' end as ml from  (select user, sum(count) as fcount from  (select user1 as user, count(user2) as count from user_mutual_like ul1 where  timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1  union  select user2 as user, count(user1) as count from user_mutual_like ul2 where  timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE)and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user2  ) as m group by m.user  )x join user u on u.user_id = x.user     group by u.gender, case when x.fcount <=5 then x.fcount else '5plus' end";
	$res_ml= $conn_reporting->Execute($sql_mutual_like);
	$rows_ml = $res_ml->GetRows();
	
	$total_mls_sql = "  select  count(user2) as count from user_mutual_like ul1 where  timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds";
	$res_mls_total = $conn_reporting->Execute($total_mls_sql);
	$rows_mls_total = $res_mls_total->FetchRow();
	
	$mls = array();
	$userWise_mls = array();
	foreach ($rows_ml as $val)
	{
		$mls[$val['gender']][$val['ml']] = $val['num'];
		$userWise_mls[$val['gender']] += $val['num'];
	}
	
	
	$smarty->assign("mls", $mls);
	$smarty->assign("mls_userWise", $userWise_mls);
	$smarty->assign("total_mls", $rows_mls_total['count']);
	
	echo "\n".$echo_count++;
	/**
	 * bucket wise mutual likes %
	 */
	
	$sql_likes_sent  = "select gender, up.bucket,  sum(t.liked) as likes  from (select user1, count(user2) as liked from user_like where  timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1)t  join user_pi_activity up on t.user1 = up.user_id  join user u on u.user_id = t.user1  group by u.gender, up.bucket";
	$res_ls= $conn_reporting->Execute($sql_likes_sent);
	$rows_ls = $res_ls->GetRows();
	$likes_arr = array();
	foreach ($rows_ls as $val)
	{
		$likes_arr[$val['gender']][$val['bucket']] = $val['likes'];
	}
	
	$sql_mutual_like_bucketwise = "select u.gender, up.bucket, sum(x.fcount) as num from 
						(select user, sum(count) as fcount from 
								(select user1 as user, count(user2) as count from user_mutual_like ul1 where 
                                timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1 
								union 
								select user2 as user, count(user1) as count from user_mutual_like ul2 where 
                                timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user2 
								) as m group by m.user
						 )x join user u on u.user_id = x.user join user_pi_activity up on x.user= up.user_id
						 group by u.gender, up.bucket";
	$res_mlb= $conn_reporting->Execute($sql_mutual_like_bucketwise);
	$rows_mlb = $res_mlb->GetRows();
	$ml_perc = array();
	foreach ($rows_mlb as $val)
	{
		$ml_perc[$val['gender']][$val['bucket']] = round(($val['num']/$likes_arr[$val['gender']][$val['bucket']])*100,2);
	}
	
	$smarty->assign("mls_perc", $ml_perc);
	
	echo "\n".$echo_count++;
	
	/**
	 * mutual like count / like count of user
	 */
	
	
		
	$sql_likes_sent_count  = "select gender, up.bucket, 
						count(t.liked) as likesUni 
						from
						(select user1, count(user2) as liked from user_like where 
                        timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1)t 
						join user_pi_activity up on t.user1 = up.user_id 
						join user u on u.user_id = t.user1
						group by u.gender, up.bucket";
	$res_lsc= $conn_reporting->Execute($sql_likes_sent_count);
	$rows_lsc = $res_lsc->GetRows();
	$likes_arrc = array();
	foreach ($rows_lsc as $val)
	{
		$likes_arrc[$val['gender']][$val['bucket']] = $val['likesUni'];
	}
	

	$sql_mutual_like_bucketwise_count = "select u.gender, up.bucket, count(x.fcount) as cnt from 
						(select user, sum(count) as fcount from 
								(select user1 as user, count(user2) as count from user_mutual_like ul1 where 
                                 timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user1 
								union 
								select user2 as user, count(user1) as count from user_mutual_like ul2 where 
                                timestamp >= DATE_SUB($date, INTERVAL '$duration 5:30' DAY_MINUTE) and timestamp <= DATE_SUB($date, INTERVAL '0 5:30' DAY_MINUTE) and user1 not in $ignoredAdCampaignUserIds and user2 not in $ignoredAdCampaignUserIds group by user2 
								) as m group by m.user
						 )x join user u on u.user_id = x.user join user_pi_activity up on x.user= up.user_id 
						 group by u.gender, up.bucket";
	$res_mlbc= $conn_reporting->Execute($sql_mutual_like_bucketwise_count);
	$rows_mlbc = $res_mlbc->GetRows();
	$ml_percc = array();
	foreach ($rows_mlbc as $val)
	{
		$ml_percc[$val['gender']][$val['bucket']] = round(($val['cnt']/$likes_arrc[$val['gender']][$val['bucket']])*100,2);
	}
	
	$smarty->assign("mls_perc_count", $ml_percc);
	
	echo "\n".$echo_count++;

	
	/**
	 * availability query by bucket
	 */
	$sql_available_bucket = "select count(u.user_id) as  count, bucket , 
case 
when u.age>=18 and u.age<=20 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2 *$slots_filled_limit_18_20 and slots_filled is not null then 'slots_full' when slots_filled>=$slots_filled_limit_18_20 and slots_filled is not null then 'slots_full' else 'free' end)
when u.age>20 and u.age<=22 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2 *$slots_filled_limit_21_22 and slots_filled is not null then 'slots_full' when slots_filled>=$slots_filled_limit_21_22 and slots_filled is not null then 'slots_full' else 'free' end)
when u.age>22 and u.age<=26 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2 *$slots_filled_limit_23_26 and slots_filled is not null then 'slots_full' when slots_filled>=$slots_filled_limit_23_26 and slots_filled is not null then 'slots_full' else 'free' end)
else (case when slots_filled>=$slots_filled_limit and slots_filled is not null then 'slots_full' else 'free' end)
end as slots from user_search u left join user_lastlogin ul on u.user_id = ul.user_id left join user_availability ua on ua.user_id = u.user_id  where u.gender = 'F' and u.user_id%$ab_variable = 0 and u.user_id not in $ignoredAdCampaignUserIds group by bucket, 
case 
when u.age>=18 and u.age<=20 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2 *$slots_filled_limit_18_20 and slots_filled is not null then 'slots_full' when slots_filled>=$slots_filled_limit_18_20 and slots_filled is not null then 'slots_full' else 'free' end)
when u.age>20 and u.age<=22 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2 *$slots_filled_limit_21_22 and slots_filled is not null then 'slots_full' when slots_filled>=$slots_filled_limit_21_22 and slots_filled is not null then 'slots_full' else 'free' end)
when u.age>22 and u.age<=26 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2 *$slots_filled_limit_23_26 and slots_filled is not null then 'slots_full' when slots_filled>=$slots_filled_limit_23_26 and slots_filled is not null then 'slots_full' else 'free' end)
else (case when slots_filled>=$slots_filled_limit and slots_filled is not null then 'slots_full' else 'free' end)
end";
	$result5 = $conn_reporting->Execute($sql_available_bucket);
	$rows5 = $result5->GetRows();

	$arr = null;
	foreach ($rows5 as $val){
		$arr[$val['bucket']][$val['slots']]= $val['count'];
	}


	echo "\n".$echo_count++;
	$sql_available_bucket_fresh = "select count(u.user_id) as count, bucket,  
case 
when u.age>=18 and u.age<=20 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and (fresh_slots_filled<".(2* AvailabilityFilters::age_18_20)." or fresh_slots_filled is null) then 'free_fresh' when (fresh_slots_filled<".AvailabilityFilters::age_18_20." or fresh_slots_filled is null) then 'free_fresh' else 'full_fresh' end)
when u.age>20 and u.age<=22 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and (fresh_slots_filled<".(2* AvailabilityFilters::age_21_22)." or fresh_slots_filled is null) then 'free_fresh' when (fresh_slots_filled<".AvailabilityFilters::age_21_22." or fresh_slots_filled is null) then 'free_fresh' else 'full_fresh' end)
when u.age>22 and u.age<=26 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and (fresh_slots_filled<".(2* AvailabilityFilters::age_23_26)." or fresh_slots_filled is null) then 'free_fresh' when (fresh_slots_filled<".AvailabilityFilters::age_23_26." or fresh_slots_filled is null) then 'free_fresh' else 'full_fresh' end)
when bucket=0 then (case when fresh_slots_filled<".AvailabilityFilters::bucketZero." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=1 then (case when fresh_slots_filled<".AvailabilityFilters::bucketOne." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=2 then (case when fresh_slots_filled<".AvailabilityFilters::bucketTwo." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=3 then (case when fresh_slots_filled<".AvailabilityFilters::bucketThree." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=4 then (case when fresh_slots_filled<".AvailabilityFilters::bucketFour." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
else (case when fresh_slots_filled<".AvailabilityFilters::bucketFive." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
end as 'fresh_slots'  
from user_search u left join user_lastlogin ul on u.user_id = ul.user_id left join user_availability ua on u.user_id = ua.user_id where gender = 'F' and u.user_id not in $ignoredAdCampaignUserIds 
group by bucket ,
case  
when u.age>=18 and u.age<=20 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and (fresh_slots_filled<".(2* AvailabilityFilters::age_18_20)." or fresh_slots_filled is null) then 'free_fresh' when (fresh_slots_filled<".AvailabilityFilters::age_18_20." or fresh_slots_filled is null) then 'free_fresh' else 'full_fresh' end)
when u.age>20 and u.age<=22 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and (fresh_slots_filled<".(2* AvailabilityFilters::age_21_22)." or fresh_slots_filled is null) then 'free_fresh' when (fresh_slots_filled<".AvailabilityFilters::age_21_22." or fresh_slots_filled is null) then 'free_fresh' else 'full_fresh' end)
when u.age>22 and u.age<=26 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and (fresh_slots_filled<".(2* AvailabilityFilters::age_23_26)." or fresh_slots_filled is null) then 'free_fresh' when (fresh_slots_filled<".AvailabilityFilters::age_23_26." or fresh_slots_filled is null) then 'free_fresh' else 'full_fresh' end)
when bucket=0 then (case when fresh_slots_filled<".AvailabilityFilters::bucketZero." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=1 then (case when fresh_slots_filled<".AvailabilityFilters::bucketOne." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=2 then (case when fresh_slots_filled<".AvailabilityFilters::bucketTwo." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=3 then (case when fresh_slots_filled<".AvailabilityFilters::bucketThree." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
when bucket=4 then (case when fresh_slots_filled<".AvailabilityFilters::bucketFour." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
else (case when fresh_slots_filled<".AvailabilityFilters::bucketFive." or fresh_slots_filled is null then 'free_fresh' else 'full_fresh' end)
end";
	
	
	$result_freshSlots = $conn_reporting->Execute($sql_available_bucket_fresh);
	$rows_freshSlots = $result_freshSlots->GetRows();

	//$arr = null;
	foreach ($rows_freshSlots as $val){
		$arr[$val['bucket']][$val['fresh_slots']]= $val['count'];
	}


	$smarty->assign("SlotsFilledStats_bucket",  $arr);
	echo "\n".$echo_count++;
	
	/**
	 * ~~~ availability query  by fresh  - section ends
	 */
	//$smarty->assign("SlotsFilledStats_bucket_fresh",  $arr);
 
// availabilty by age-group
	$sql_available_age = "select count(ua.user_id) as count,
case 
when age >=18 and age <=20 then '18-20' 
when age >20 and age <=22 then '20-22'
when age >22 and age <=26 then '22-26'
when age >26 and age <=30 then '26-30'
else '30+'
end as age_group,
case 
when age>=18 and age<=20 then  (case 
when (ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2*$slots_filled_limit_18_20 and slots_filled is not null) then 'slots_full' when (slots_filled>=$slots_filled_limit_18_20 and slots_filled is not null) then 'slots_full' else 'free' 
end) 
when age>20 and age<=22 then  (case 
when (ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2*$slots_filled_limit_21_22 and slots_filled is not null) then 'slots_full' when (slots_filled>=$slots_filled_limit_21_22 and slots_filled is not null) then 'slots_full' else 'free' 
end) 
when age>22 and age<=26 then  (case 
when (ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2*$slots_filled_limit_23_26 and slots_filled is not null) then 'slots_full' when (slots_filled>=$slots_filled_limit_23_26 and slots_filled is not null) then 'slots_full' else 'free' 
end) 
else (case 
when (slots_filled>=$slots_filled_limit and slots_filled is not null) then 'slots_full' else 'free' 
end) 
end as slots 
from user_search ud left join user_lastlogin ul on ud.user_id = ul.user_id left join user_availability ua  on ua.user_id = ud.user_id where ud.gender = 'F' and ud.user_id not in $ignoredAdCampaignUserIds 
group by 
case 
when age >=18 and age <=20 then '18-20' 
when age >20 and age <=22 then '20-22'
when age >22 and age <=26 then '22-26'
when age >26 and age <=30 then '26-30'
else '30+'
end, 
case 
when age>=18 and age<=20 then  (case 
when (ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2*$slots_filled_limit_18_20 and slots_filled is not null) then 'slots_full' when (slots_filled>=$slots_filled_limit_18_20 and slots_filled is not null) then 'slots_full' else 'free' 
end) 
when age>20 and age<=22 then  (case 
when (ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2*$slots_filled_limit_21_22 and slots_filled is not null) then 'slots_full' when (slots_filled>=$slots_filled_limit_21_22 and slots_filled is not null) then 'slots_full' else 'free' 
end) 
when age>22 and age<=26 then  (case 
when (ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and slots_filled>=2*$slots_filled_limit_23_26 and slots_filled is not null) then 'slots_full' when (slots_filled>=$slots_filled_limit_23_26 and slots_filled is not null) then 'slots_full' else 'free' 
end) 
else (case 
when (slots_filled>=$slots_filled_limit and slots_filled is not null) then 'slots_full' else 'free' 
end) 
end ";
	
	$result6 = $conn_reporting->Execute($sql_available_age);
	$rows6 = $result6->GetRows();

	$arr = null;
	foreach ($rows6 as $val){
		$arr[$val['age_group']][$val['slots']]= $val['count'];
	}


$sql_available_age2="select count(u.user_id) as count, 
(case
  when u.age>=18 and u.age <=20 then '18-20'
  when u.age >20 and u.age <=22 then '20-22'
  when u.age >22 and u.age <=26 then '22-26'
  when u.age >26 and u.age <=30 then '26-30'
  else '30+'
end) as age_group,  


case 
when u.age>=18 and u.age<=20 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.fresh_slots_filled  <".(2*AvailabilityFilters::age_18_20)."  or ua.fresh_slots_filled is null then 'free_fresh' when ua.fresh_slots_filled  <".AvailabilityFilters::age_18_20."  or ua.fresh_slots_filled is null then 'free_fresh'
              else 'full_fresh' 
          end)
when u.age>20 and u.age<=22 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.fresh_slots_filled  <".(2*AvailabilityFilters::age_21_22)."  or ua.fresh_slots_filled is null then 'free_fresh' when ua.fresh_slots_filled  <".AvailabilityFilters::age_21_22."  or ua.fresh_slots_filled is null then 'free_fresh'
              else 'full_fresh' 
          end)
when u.age>22 and u.age<=26 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.fresh_slots_filled  <".(2*AvailabilityFilters::age_23_26)."  or ua.fresh_slots_filled is null then 'free_fresh' when ua.fresh_slots_filled  <".AvailabilityFilters::age_23_26."  or ua.fresh_slots_filled is null then 'free_fresh'
              else 'full_fresh' 
          end)
when u.bucket > 0 then 
  (case when u.bucket = 1 then 
         (case when ua.fresh_slots_filled  <".AvailabilityFilters::bucketOne."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end) 

        when u.bucket = 2 then 
         (case when ua.fresh_slots_filled  <".AvailabilityFilters::bucketTwo." or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)    

        when u.bucket = 3 then 
         (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketThree."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)   

        when u.bucket = 4 then 
         (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketFour."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)   
        
        else 
          (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketFive."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)     
   end)   
        else 
          (case when ua.fresh_slots_filled < ".AvailabilityFilters::bucketZero." or ua.fresh_slots_filled is null  then 'free_fresh' 
                else 'full_fresh' 
           end)
end as fresh_slots 


from user_search u left join user_lastlogin ul on u.user_id = ul.user_id left join user_availability ua on u.user_id = ua.user_id 

where gender = 'F' and u.user_id not in $ignoredAdCampaignUserIds 

group by   
case when u.age >=18 and u.age <=20 then '18-20'
     when u.age >20 and u.age <=22 then '20-22'
     when u.age >22 and u.age <=26 then '22-26'
     when u.age >26 and u.age <=30 then '26-30'
     else '30+'
end, 
 
case 
when u.age>=18 and u.age<=20 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.fresh_slots_filled  <".(2*AvailabilityFilters::age_18_20)."  or ua.fresh_slots_filled is null then 'free_fresh' when ua.fresh_slots_filled  <".AvailabilityFilters::age_18_20."  or ua.fresh_slots_filled is null then 'free_fresh'
              else 'full_fresh' 
          end)
when u.age>20 and u.age<=22 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.fresh_slots_filled  <".(2*AvailabilityFilters::age_21_22)."  or ua.fresh_slots_filled is null then 'free_fresh' when ua.fresh_slots_filled  <".AvailabilityFilters::age_21_22."  or ua.fresh_slots_filled is null then 'free_fresh'
              else 'full_fresh' 
          end)
when u.age>22 and u.age<=26 then (case when ul.last_login is not null and TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.fresh_slots_filled  <".(2*AvailabilityFilters::age_23_26)."  or ua.fresh_slots_filled is null then 'free_fresh' when ua.fresh_slots_filled  <".AvailabilityFilters::age_23_26."  or ua.fresh_slots_filled is null then 'free_fresh'
              else 'full_fresh' 
          end)
when u.bucket > 0 then 
  (case when u.bucket = 1 then 
         (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketOne."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end) 

        when u.bucket = 2 then 
         (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketTwo."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)    

        when u.bucket = 3 then 
         (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketThree."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)   

        when u.bucket = 4 then 
         (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketFour."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)   
        
        else 
          (case when ua.fresh_slots_filled  < ".AvailabilityFilters::bucketFive."  or ua.fresh_slots_filled is null then 'free_fresh' 
              else 'full_fresh' 
          end)     
   end)   
        else 
          (case when ua.fresh_slots_filled < ".AvailabilityFilters::bucketZero."  or ua.fresh_slots_filled is null  then 'free_fresh' 
                else 'full_fresh' 
           end)
end     ";

$result7 = $conn_reporting->Execute($sql_available_age2);
	$rows7 = $result7->GetRows();

	foreach ($rows7 as $val){
		$arr[$val['age_group']][$val['fresh_slots']]= $val['count'];
	}

	$smarty->assign("SlotsFilledStats_age",  $arr);
 
	echo "\n".$echo_count++;
//availabilty by age-group ends
	$sql_part_slots_filled = "case when slots_filled>$slots_filled_limit then 'slots_full' else 'free' end";
	$sql_part_slots_filled_18_20 = "case when TIMESTAMPDIFF(day,u.last_login, $date)<=7 and slots_filled>2*$slots_filled_limit_18_20 then 'slots_full' when slots_filled>$slots_filled_limit_18_20 then 'slots_full' else 'free' end";
	$sql_part_slots_filled_21_22 = "case when TIMESTAMPDIFF(day,u.last_login, $date)<=7 and slots_filled>2*$slots_filled_limit_21_22 then 'slots_full' when slots_filled>$slots_filled_limit_21_22 then 'slots_full' else 'free' end";
	$sql_part_slots_filled_23_26 = "case when TIMESTAMPDIFF(day,u.last_login, $date)<=7 and slots_filled>2*$slots_filled_limit_23_26 then 'slots_full' when slots_filled>$slots_filled_limit_23_26 then 'slots_full' else 'free' end";

	$sql_available_lastLogin = "select count(u.user_id) as count,  case when datediff(now(),u.last_login) > 15 then '15+' else  datediff(now(),u.last_login) end as ll, case when age >=18 and age <=20 then ".$sql_part_slots_filled_18_20. " when age >20 and age <=22 then ".$sql_part_slots_filled_21_22. " when age >22 and age <=26 then ".$sql_part_slots_filled_23_26." else ".$sql_part_slots_filled." end as slots from  user_search us left join user_availability ua on us.user_id = ua.user_id join user u on u.user_id = us.user_id  where us.user_id%$ab_variable=0 and u.user_id not in $ignoredAdCampaignUserIds and us.gender = 'F' group by  case when datediff(now(),u.last_login) > 15 then '15+' else  datediff(now(),u.last_login) end , case when age >=18 and age <=20 then ".$sql_part_slots_filled_18_20. " when age >20 and age <=22 then ".$sql_part_slots_filled_21_22. " when age >22 and age <=26 then ".$sql_part_slots_filled_23_26." else ".$sql_part_slots_filled." end order by u.last_login desc ";
	$result_ll = $conn_reporting->Execute($sql_available_lastLogin);
	$rows_ll = $result_ll->GetRows();

	$arr = null;
	foreach ($rows_ll as $val){
		$arr[$val['ll']][$val['slots']]= $val['count'];
	}
	$smarty->assign("SlotsFilledStats_ll",  $arr);

	$sql_part_slots_filled = "case when ua.slots_filled>$slots_filled_limit then 1 else 0 end";
	$sql_part_slots_filled_18_20 = "case when TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.slots_filled>2*$slots_filled_limit_18_20 then 1 when ua.slots_filled>$slots_filled_limit_18_20 then 1 else 0 end";
	$sql_part_slots_filled_21_22 = "case when TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.slots_filled>2*$slots_filled_limit_21_22 then 1 when ua.slots_filled>$slots_filled_limit_21_22 then 1 else 0 end";
	$sql_part_slots_filled_23_26 = "case when TIMESTAMPDIFF(day,ul.last_login, $date)<=7 and ua.slots_filled>2*$slots_filled_limit_23_26 then 1 when ua.slots_filled>$slots_filled_limit_23_26 then 1 else 0 end";

	$sql_available_city = "select x.bucket, x.total_count, x.filled, x.name, t1.male_count from 
(
select u.bucket, count(u.user_id) as total_count, sum(case when u.age >=18 and u.age <= 20 then ".$sql_part_slots_filled_18_20." when u.age>20 and u.age <=22 then ".$sql_part_slots_filled_21_22." when u.age>22 and u.age <=26 then ".$sql_part_slots_filled_23_26." else ".$sql_part_slots_filled." end) as filled,  t.name, t.city
from 
(select city, gc.name from user_search us join geo_city gc on gc.city_id = us.city  where gender = 'F' and us.user_id%$ab_variable=0 and us.user_id not in $ignoredAdCampaignUserIds group by city order by count(*) desc limit 20)t join user_search u on u.city = t.city 
 left join user_lastlogin ul on u.user_id = ul.user_id left join user_availability ua on u.user_id = ua.user_id where gender = 'F' and u.user_id%$ab_variable=0 and u.user_id not in $ignoredAdCampaignUserIds group by t.city, u.bucket)x
 join (select count(*) as male_count, city from user_search where gender ='M' and user_id%$ab_variable=0 group by city
)t1 on x.city=t1.city order by x.total_count desc ";
	$result_city = $conn_reporting->Execute($sql_available_city);
	$rows_city = $result_city->GetRows();
	$city_arr = array();
	foreach ($rows_city as $val)
	{
		$city_arr[$val['name']][$val['bucket']] = $val;
	}
	//var_dump($city_arr);
	$smarty->assign("SlotsFilledStats_city",  $city_arr);
	
	$sql_checkpoint = "select max(checkpoint) as checkpoint from user_search_checkpoint";
	$result_checkpoint = $conn_reporting->Execute($sql_checkpoint);
	$checkpointRow = $result_checkpoint->FetchRow();
	$smarty->assign("checkpoint", $checkpointRow['checkpoint']);

	$sqlUsearch = "SELECT count(user_id) as count, gender , install_status from user_search   where user_id%$ab_variable=0 and user_id not in $ignoredAdCampaignUserIds  group by gender, install_status	";
	$rowsSearch = $conn_reporting->Execute($sqlUsearch);

	$count_inserted_rows = null;
	$genderWiseUsers = null;
	$genderWiseUsersInstalled = null;
	foreach ($rowsSearch as  $val){
		$count_inserted_rows += $val['count'];
		$genderWiseUsers[$val['gender']] += $val['count'] ;
		if($val['install_status'] == 'install')
		{
			$genderWiseUsersInstalled[$val['gender']] = $val['count'];
		}
	}
	$smarty->assign("rows_us", $rowsSearch);
	$smarty->assign("count_us", $count_inserted_rows);
	echo "\n".$echo_count++;
	$userWise_mls_percent = array();
	$userWise_mls_percent_userActioned = array();
	$usageSum = array();
	$usageSum['M'] = $maleUsageSum;
	$usageSum['F'] = $femaleUsageSum;
	foreach ($userWise_mls as $key => $val){
		$userWise_mls_percent[$key] = round(($val*100)/$genderWiseUsersInstalled[$key],2);
		$userWise_mls_percent_userActioned[$key] = round(($val*100)/$usageSum[$key],2);
	}
	
	$smarty->assign("userWiseMls", $userWise_mls_percent);
	$smarty->assign("userWiseMlsUsage", $userWise_mls_percent_userActioned);
	
	$sql_bucket = "select * from bucket_log order by tstamp desc , bucket asc limit 4 ";
	$res_bucket = $conn_reporting->Execute($sql_bucket);
	$rowsBucket = $res_bucket->GetRows();
	
	$smarty->assign("last_bucket_generated", $rowsBucket[0]['tstamp']);
	$smarty->assign("bucket_log", $rowsBucket);
	
	// Generating Report for daily bucket changes gender wise
	$sql_bucket_change_male="select last_bucket as 'from', u.bucket as 'to', count(up.user_id) as count from user_pi_activity up join user_search u on up.user_id=u.user_id where u.gender='M' and u.user_id not in $ignoredAdCampaignUserIds and u.bucket!=last_bucket group by last_bucket,u.bucket";
	$sql_bucket_change_female="select last_bucket as 'from', u.bucket as 'to', count(up.user_id) as count from user_pi_activity up join user_search u on up.user_id=u.user_id where u.gender='F' and u.user_id not in $ignoredAdCampaignUserIds and u.bucket!=last_bucket group by last_bucket,u.bucket";
	$bucket_change_m=$conn_reporting->Execute($sql_bucket_change_male);
	$bucket_change_f=$conn_reporting->Execute($sql_bucket_change_female);
	$smarty->assign("bucket_change_male", $bucket_change_m);
	$smarty->assign("bucket_change_female", $bucket_change_f);
	echo "\n".$echo_count++;
}

function notifyTechTeams($msg){
	global $techTeamIds, $baseurl;
	var_dump($techTeamIds);
	$subject = $msg. " for " . $baseurl;
	$from = "himanshu@trulymadly.com";
	Utils::sendEmail($techTeamIds, $from, $subject, $msg);
}

function sendEmail(){
	global $smarty, $emailIdsForReporting;
	
	$subject = "Recommendation Statistics Report for Algo B (AB profiles) :". $date;;
        if ($date == "curdate()") {
		$subject = "Recommendation Statistics Report for Algo B (AB profiles) :". date('d-m-Y',strtotime("-1 days"));;
	}
	//$to=$emailIdsForReporting['recommendation_test'];
	$to = (date('D', time()) === 'Mon') ? ( $emailIdsForReporting['recommendation'] . ", hitesh@trulymadly.com" ) :  ( $emailIdsForReporting['recommendation'] ) ;
	//Utils::sendEmail($to , "arpan@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/recoReport_test.tpl'), TRUE);
	Utils::sendEmail($to , "admintm@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/recoReport.tpl'), TRUE);
	//Utils::sendEmail($to , "vineet@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/recoReport.tpl'), TRUE);
	}

try{
	if(php_sapi_name() === 'cli'){
		getRecoData($date,$duration);
		sendEmail(); 
	}
}
catch (Exception $e){
	$msg = "Recommendation Reporting Failed";
	notifyTechTeams($msg);
	echo $e->getMessage();
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );

}

?>
