<?php

require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";

require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../Advertising/IgnoredProfileUsersForReporting.php";

$tempTable = "user_pi_temp";


/**
 *  calculating PI on daily basis
 * Dyanamic bucket resizing
 * NOTE: DO NOT EDIT unless you are very sure
 * @Himanshu
 */

function notifyTechTeam($msg){
	global $techTeamIds, $baseurl;
	echo $msg;
	//$subject = "generate bucket script failed for " . $baseurl;
	$from = "himanshu@trulymadly.com";
	Utils::sendEmail($techTeamIds, $from, $msg . " for " . $baseurl, $msg);
}



function getFileName(){
	global $filepathForUserBucketGeneration;
	if(!file_exists($filepathForUserBucketGeneration)){
		$fileDetails =  array("filename" => "user_pi_activity.txt", "path" => "/tmp");
		$filename = $fileDetails['filename'];
		$completePath = $fileDetails['path'] . "/$filename";
	}else{
		$completePath = $filepathForUserBucketGeneration;
	}

	return $completePath;
}


function resizeBucket(){
	global $conn_master, $conn_reporting;
	$sql1= "set @y=0, @x=0";
	$conn_reporting->Execute($sql1);
	$sql2= "select t1.actual_pi, t1.x, t1.gender from (select t.actual_pi , case when gender = 'F' then (@x:=@x+1) else (@y:=@y+1) end as x , gender from (select actual_pi, gender from user_pi_activity up join user_search u on u.user_id = up.user_id where up.actual_pi >=0 order by up.actual_pi)t)t1
			join
 			(select gender, round(count(*)/5) as b1,round(count(*)/5)*2 as b2, round(count(*)/5)*3 as b3,round(count(*)/5)*4  as b4 from  user_pi_activity up join user_search u on u.user_id = up.user_id where up.actual_pi >=0 group by gender)t2 
			on (t1.x = t2.b1 or t1.x = t2.b2 or t1.x = t2.b3 or t1.x = t2.b4) and t1.gender = t2.gender";
	$res = $conn_reporting->Execute($sql2);
	$rows = $res->GetRows();
    
	$conn_master->bulkBind = true;
	$arr = array();
	$insertArr = array();
	$male_bucket = array();
	$female_bucket = array();
	$i=0;
	$j=0;
	foreach ($rows as $col){
		if($col['gender'] == "M"){
			$arr[$i]['M'] = $col['actual_pi'];
			$male_bucket[]= $col['actual_pi'];
			$i++;
		}
		else {
			$arr[$j]['F'] = $col['actual_pi'];
			$female_bucket[] = $col['actual_pi'];
			$j++;
		}
	}

	foreach ($arr as $key => $val){
		$insertArr[] = array($key+1, $val['M'], $val['F']);
	}

	var_dump($insertArr);//die;

	$bucketSql = "INSERT INTO bucket_log (bucket, pi_male, pi_female,tstamp) values (?,?,?,now())";
	$conn_master->Execute($bucketSql, $insertArr);
	echo "\naaaaaaa\n";
	return array("male_bucket" => $male_bucket, "female_bucket" => $female_bucket);
}

/**
 * delete all the activity keys of users updated
 * @param unknown_type $user_array
 */
/*function clearRedisActivityKeys(){
 global $redis_db_userActivity, $redis;
 $redis->SELECT ($redis_db_userActivity);
 $redis->FLUSHDB();

 echo "updated redis database after writing to table\n";
 */
//in case u have to do key by key use below code
/*$activityKeys = array();
 foreach ($user_array as $val){
 $activityKeys[] = Utils::$redis_keys['activity'].$val;
 }

 if(!empty($activityKeys))
 call_user_func_array(array($this->redis,'DEL'),$activityKeys);
 */
//}


/**
 * will load data from local file in to user search new table (file should be tab separated)
 */

function loadDataFromLocalFile(){
	global $conn_master, $config, $baseurl, $tempTable;
	try{
		$completePath = getFileName();
		if(filesize($completePath) > 0 ){
		 $sql = "LOAD DATA LOCAL INFILE '$completePath' INTO TABLE $tempTable fields terminated by '\t' ";
		 $ex = $conn_master->Execute($sql);
		 echo "loaded data into temp table";
		 // var_dump($conn_master->isWarning());
		 $warningQuery = "SHOW WARNINGS";
		 $warnings = $conn_master->Execute($warningQuery);
		 $warningCount = $warnings->RowCount();
		 if($warningCount > 0 ){
		 	$msg = "Warnings in loading data:" . $warningCount ."\n" ;
		 	for($i=0 ; $i<$warningCount;$i++){
		 		while ($row = $warnings->FetchRow()) {
		 			foreach ($row as $val){
		 				$msg .= "$val\t";
		 			}
		 			$msg .="\n";
		 		}
		 	}
		 	Utils::sendEmail("arpan@trulymadly.com", "himanshu@trulymadly.com", " User PI activity generation warnings in loading data".$baseurl, $msg, false);
		 }
		}else {
			$subject = "User PI creation file on admin tmp directory is empty for" . $baseurl ;
			notifyTechTeam($subject);
		}

	}
	catch (Exception $e){
		trigger_error($e->getMessage());
		echo $e->getTraceAsString();
	}
}

function createTempTable(){
	global $conn_master, $conn_reporting, $tempTable;

	$sql_tempTable = "CREATE TEMPORARY TABLE $tempTable (
  						`user_id` bigint(20) NOT NULL,
  						`pi` float DEFAULT '0',
  						`total_likes` int(10) DEFAULT '0',
  						`total_hides` int(10) DEFAULT '0',
						`today_activity` int(3) default 0, 
  						`ai` float DEFAULT '0',
  						`last_day_visibility` int(10) DEFAULT '0',
						`lastLogin` timestamp  ,
						`default_count` int(3) default 0 )";

	$conn_master->Execute($sql_tempTable);
	echo "created temp table\n";
}


function dropTempTable(){
	global $conn_master, $conn_reporting, $tempTable;

	$sql = "DROP table $tempTable";
	$conn_master->Execute($sql);
}


/**
 * get active chats count per user of a week (which are not blocked by either party)
 */
function updateActiveChats ()
{
	global $conn_reporting, $conn_master, $admin_id;
	$sql = "select mq1.sender_id , count(mq1.receiver_id) as count from messages_queue mq1 join messages_queue mq2 on (mq1.sender_id = mq2.receiver_id and mq1.receiver_id = mq2.sender_id) where mq1.tStamp >=date_sub(curdate(), interval 1 week) and mq2.tStamp >=date_sub(curdate(), interval 1 week) and mq1.blocked_by is null and mq2.blocked_by is null and mq1.sender_id != $admin_id and mq1.receiver_id != $admin_id and mq2.sender_id != $admin_id and mq2.receiver_id != $admin_id  and mq1.msg_type != 'MUTUAL_LIKE' and mq2.msg_type != 'MUTUAL_LIKE' group by mq1.sender_id";
	
	$res = $conn_reporting->Execute($sql);
	$arr = array();
	if($res->RowCount()>0)
	{
		while ($row = $res->FetchRow())
		{
			$arr[] = array($row['count'], $row['sender_id']);
		}
	}
	
	//mark all chats to 0 first everytime so that we can update the correct value
	$sql_up = "UPDATE user_pi_activity set active_chats = 0";
	$conn_master->Execute($sql_up);
	
	//update the week stats for chats
	$conn_master->bulkBind = true;
	$sql2 = "UPDATE user_pi_activity set active_chats = ? where user_id = ?";
	$conn_master->Execute($sql2, $arr);
	
	
}

/**
 * fetch the data required for pi generation and activity generation
 * save it in a text file
 * load the data into temp table
 * copy the data into actual table from it
 * clear redis db for the same
 * and then resize the buckets
 * place actual value of bucket for each user
 */
function generatePI(){
	global $conn_master, $conn_reporting, $tempTable;

	//get the column to update on the basis of day number of the year with modulo to 14
	$activityColumnToUpdate = "visibility_day". date('z') % 14;

	echo "starting select query\n";
	$defaultConstant = Utils::$defaultBucketConstant;
	
	/**
	 * get data for one day for bucketization
	 */
        $ignoredIds = IgnoredProfileUsersForReporting::getUserIdsString();
	$selectsql = "select u.user_id,  $defaultConstant as default_pi, case when gender = 'M' then 20 else 10 end as default_count, 
	             date(ull.last_login) as lastLogin, COALESCE(t1.like_count,0)/(COALESCE(t1.like_count,0) + COALESCE(t2.hide_count,0)) as pi,
	             COALESCE(t1.like_count,0) + COALESCE(t2.hide_count, 0) as last_day_visibility , 
	             COALESCE(t1.like_count,0) as like_count , 
	             COALESCE(t2.hide_count, 0) as hide_count, 
	             case when date(ull.last_login)>=date_sub(curdate(), interval 1 day) then 1 else 0 end as today_activity       
	             from user u join user_lastlogin ull on u.user_id=ull.user_id       
	             left join 
	             	(select count(user1) as like_count, user2 from user_like where timestamp>=date_sub(curdate(), interval 1 day) and timestamp <curdate() and user1 not in $ignoredIds group by user2 )t1  
	             	 on t1.user2 = u.user_id       
	             left join (select count(user1) as hide_count, user2 from user_hide where timestamp>=date_sub(curdate(), interval 1 day) and timestamp <curdate() and user1 not in $ignoredIds group by user2)t2
	                 on u.user_id = t2.user2          
	             where status ='authentic' and (COALESCE(t1.like_count,0) + COALESCE(t2.hide_count,0)>0)";
	$res = $conn_reporting->Execute($selectsql);
	$filename = getFileName();
	if(file_exists($filename)) unlink($filename);
	$str = "";

	echo "executed select and preparing for write in $filename\n";
	echo $res->RowCount();
	if($res->RowCount()>0){
		while ($row = $res->FetchRow()){

			$ai = 0;
			$ai = ((15 - intval($row['today_activity']))*intval($row['today_activity'])/(15*15));
			$pi = ($row['like_count']+ $row['hide_count'])< $row['default_count'] ? $row['default_pi'] : ($row['like_count'])/ ($row['hide_count']+$row['like_count']);


			$str = $row['user_id']."\t";
			$str .= $pi."\t";
			$str .= isset($row['like_count'])?$row['like_count']."\t":"0\t";
			$str .= isset($row['hide_count'])?$row['hide_count']."\t":"0\t";
			$str .= isset($row['today_activity'])?$row['today_activity']."\t":"0\t";
			$str .= $ai."\t";
			$str .= isset($row['last_day_visibility'])?$row['last_day_visibility']."\t":"0\t";
			$str .= $row['lastLogin']."\t";
			$str .= $row['default_count'];
			$str .="\n";

			$result = file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
			if($result == false){
				notifyTechTeam("Error in writing... empty row");
			}
		}


		//loading data into temp table
		loadDataFromLocalFile();
        
		echo "writing data to actual pi table";


		try{
			if(strlen($str)>0){
				$sql1 = "INSERT INTO `user_pi_activity`
		(user_id, pi, total_likes, total_hides,activity_count, ai, activity_bits_number,actual_pi,$activityColumnToUpdate, tstamp) 
		select user_id, pi, total_likes, total_hides, today_activity, ai, today_activity, pi, last_day_visibility, now() from $tempTable
		ON DUPLICATE KEY UPDATE
			 pi =(($tempTable.total_likes+user_pi_activity.total_likes)/($tempTable.total_likes+user_pi_activity.total_likes + $tempTable.total_hides + user_pi_activity.total_hides)),
			 actual_pi = case when ($tempTable.total_likes+user_pi_activity.total_likes + $tempTable.total_hides + user_pi_activity.total_hides) <default_count  then $defaultConstant else (    case when  ($tempTable.total_likes+user_pi_activity.total_likes) =0 then 0.0001 else  ($tempTable.total_likes+user_pi_activity.total_likes)/($tempTable.total_likes+user_pi_activity.total_likes + $tempTable.total_hides + user_pi_activity.total_hides) end) end,
			  total_likes = ($tempTable.total_likes+user_pi_activity.total_likes), 
			  total_hides = ($tempTable.total_hides + user_pi_activity.total_hides), 
			  activity_count= BIT_COUNT(((activity_bits_number<<1)|$tempTable.today_activity)&32767),
			   ai =( 15 - datediff(curdate(),date($tempTable.lastLogin)))*BIT_COUNT((((activity_bits_number<<1)|$tempTable.today_activity)&32767))/(15*15) , 
			   activity_bits_number=((activity_bits_number<<1)|$tempTable.today_activity)&32767, $activityColumnToUpdate=$tempTable.last_day_visibility,
			   tstamp = now()
			   "; 

				$conn_master->Execute($sql1);
			}
			
			
			
	/**
	 * update the chat count for users 
	 */
	updateActiveChats();
				

			/**
			 * for updating the bucket
			 * written in separate query as we will move to dynamic bucket resizing
			 */


			$buckets = resizeBucket();
			$male_bucket = $buckets['male_bucket'];
			$female_bucket = $buckets['female_bucket'];
			var_dump($male_bucket);
			var_dump($female_bucket);
		
			$sql2 = "update user_pi_activity up join user u on u.user_id = up.user_id
			set
			last_bucket_change_tstamp = case when last_bucket != bucket then now() else last_bucket_change_tstamp end,
			last_bucket = case when bucket is null then 0 else bucket end ,
			bucket = case when gender = 'F' then (case when actual_pi = -1 then 0 when actual_pi<= $female_bucket[0] then 1 when actual_pi<= $female_bucket[1] then 2 when actual_pi<= $female_bucket[2] then 3 when actual_pi<= $female_bucket[3] then 4 else 5 end ) else (case when (total_likes+total_hides)>=10 and (total_likes+total_hides)<20 then ( case when total_likes = 0 then ROUND(2+RAND()) when total_likes = 1 then 4 else 5 end)else (case when  actual_pi = -1 then 0 when actual_pi<= $male_bucket[0] then 1 when actual_pi<= $male_bucket[1] then 2 when actual_pi<= $male_bucket[2] then 3 when actual_pi<= $male_bucket[3] then 4 else 5 end ) end) end,
			last_14_days_visibility = COALESCE(visibility_day0,0)+
			COALESCE(visibility_day1,0)+
			COALESCE(visibility_day2,0)+
			COALESCE(visibility_day3,0)+
			COALESCE(visibility_day4,0)+
			COALESCE(visibility_day5,0)+
			COALESCE(visibility_day6,0)+
			COALESCE(visibility_day7,0)+
			COALESCE(visibility_day8,0)+
			COALESCE(visibility_day9,0)+
			COALESCE(visibility_day10,0)+
			COALESCE(visibility_day11,0)+
			COALESCE(visibility_day12,0)+
			COALESCE(visibility_day13,0)";
			$res2 = $conn_master->Execute($sql2);
			
			
			$sql3="update user_pi_activity set bucket=5 where user_id=606821"; //karan kundra id
			$res3 = $conn_master->Execute($sql3);

		}
		catch (Exception $e){
			echo $e->getMessage();
			notifyTechTeam($e->getMessage());

		}
		echo "rows updated";

	}
	else{
		notifyTechTeam("SELECT query failed in fetching data");

	}

}


/**
 * for changing actual_pi as default pi has changed for both male and female
 * update user_pi_activity up join user u on up.user_id = u.user_id set actual_pi = case when u.gender = 'F' then (case when (ifnull( up.total_likes,0) + ifnull( up.total_hides,0)) < 20 then  0.65 else  up.actual_pi end) else (case when (ifnull( up.total_likes,0) + ifnull( up.total_hides,0)) < 10 then  0.1 else  up.actual_pi end) end;
 */

/*
 * one time query
 *
 * Insert into user_pi_activity (user_id, pi, total_likes, total_hides,activity_count, ai)
 select u.user_id, t1.like_count/(t1.like_count + t2.hide_count), t1.like_count , t2.hide_count, t3.activity_count,( 15 - datediff(curdate(),date(t3.maxt)))*t3.activity_count/(15*15)  as ai  from user u
 left join (select count(user2) as like_count, user1 from user_like where group by user1 )t1 on t1.user1 = u.user_id
 left join (select count(user2) as hide_count, user1 from user_hide where group by user1)t2 on u.user_id = t2.user1
 left join (select t.user_id, count(t.days) as activity_count, t.maxt  from (select user_id, count(distinct(date(tstamp))) as days , date(tstamp), max(tstamp) as maxt from action_log_new
 where admin_id is null and user_id is not null and date(tstamp) >= date_sub(curdate(), interval 15 day) group by date(tstamp) , user_id order by tstamp desc )t group by t.user_id)t3 on u.user_id = t3.user_id
 where status ='authentic'
 on duplicate key
 update total_likes = t1.like_count + total_likes, total_hides = t2.hide_count + total_hides, pi =(t1.like_count+total_likes)/(t1.like_count + total_likes + t2.hide_count + total_hides), activity_count= t3.activity_count, ai =( 15 - datediff(curdate(),date(t3.maxt)))*t3.activity_count/(15*15)
 */




/**
 * Insert into user_pi_activity (user_id, pi, total_likes, total_hides,activity_count, ai,activity_bits_number)     select u.user_id, t1.like_count/(t1.like_count + t2.hide_count), ifnull(t1.like_count,0) , ifnull(t2.hide_count,0), 7,( 15 - datediff(curdate(),date(u.last_login)))*7/(15*15)  as ai ,127 from user u     left join (select count(user1) as like_count, user2 from user_like  group by user2 )t1 on t1.user2 = u.user_id     left join (select count(user1) as hide_count, user2 from user_hide  group by user2)t2 on u.user_id = t2.user2       where status ='authentic'  and date(u.last_login)>=date_sub(curdate(), interval 15 day);

 */


try{
	createTempTable();
	generatePI();
	dropTempTable();
}
catch(Exception $e){
	$msg = "PI generation script failed";
	notifyTechTeam($msg . $e->getTraceAsString());
	echo $e->getMessage();
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}


?>
