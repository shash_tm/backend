<?php

require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

/**
 * Purge likes from bucket 1 and 2
 * for bucket 1 male who have liked buckets [0,3,4,5]
 * for bucket 2 male who have liked buckets [0,4,5]
 * @Himanshu
 * NOTE: DON'T EDIT UNLESS YOU ARE VERY SURE
 *
 */

function purgeLikes ()
{
	global $conn_reporting, $conn_master, $techTeamIds, $baseurl;
	/*$sql = 'SELECT x.user2 , count(x.user1) as count from
   (select t.user_id as user1, t.bucket, t1.user2 from 
       (select u.user_id, up.bucket from user u join user_pi_activity up on u.user_id = up.user_id where gender = "M" and status = "authentic" and up.bucket in (1,2,3))t  
       join 
       (select user1, user2  from user_like where timestamp>=date_sub(curdate(), interval 1 day) and timestamp <curdate() )t1 
       on t.user_id=t1.user1  
    )x join user_pi_activity upa on upa.user_id = x.user2 where case when x.bucket = 1 then upa.bucket in (0,2,3,4,5) else (case when x.bucket = 2 then upa.bucket in (0,3,4,5) else upa.bucket in (0,4,5) end ) end  group by x.user2';
*/
	/*$sql = "SELECT x.user2 , sum( case when x.user_type = 'ab' then 1 else 0 end) as 'ab', sum( case when x.user_type = 'normal' then 1 else 0 end) as 'normal'from
   (select t.user_id as user1, t.bucket, t1.user2,t.user_type from 
       (select u.user_id, case when u.user_id%" . Utils::$recoStagedReleaseConstant . "=0 then 'ab' else 'normal' end as user_type, up.bucket from user u join user_pi_activity up on u.user_id = up.user_id where gender = 'M' and status = 'authentic' and up.bucket in (1,2,3))t  
       join 
       (select user1, user2  from user_like where timestamp>=date_sub(curdate(), interval 1 day) and timestamp <curdate() )t1 
       on t.user_id=t1.user1  
    )x join user_pi_activity upa on upa.user_id = x.user2 where case when x.user_type = 'ab' then case when x.bucket = 1 then upa.bucket in (0,2,3,4,5) else (case when x.bucket = 2 then upa.bucket in (0,3,4,5) else upa.bucket in (0,4,5) end ) end else case when x.bucket = 1 then upa.bucket in (0,3,4,5) else upa.bucket in (0,4,5) end end  group by x.user2";
	*/
	
	
	//sql for uninstall/suspended/bucketized profiles
	/*
	 * $sql = "SELECT x.user2 , sum( case when x.status = 'bucketed' then 1 else 0 end) as 'ab', sum( case when x.status in ('suspended', 'uninstalled') then 1 else 0 end) as 'normal' from
(select m.user_id, m.status, n.user2 , m.bucket from 
(select t.user_id, t.status , t.bucket from (select user_id , 'suspended' as status,-1 as bucket  from (select user_id,previous_status, current_status  from user_suspension_log where date(timestamp) = date_sub(curdate(), interval 1 day) group by user_id order by timestamp desc )t where t.current_status = "suspended" and previous_status = "authentic"
union 
select user_id, 'uninstalled' as status , -1  as bucket from user_app_status where date(tstamp) = date_sub(curdate(), interval 1 day) and android_app = 'install'
union
select user_id , 'bucketed' as status, bucket  from user_pi_activity where bucket in (1,2,3) and last_bucket = 0 )t
join user u on t.user_id = u.user_id left join user_pi_activity up on up.user_id = t.user_id where u.gender = 'M' and case when t.status in ('bucketed', 'uninstalled') then u.status = 'authentic' else u.status in ('authentic', 'non-authentic', 'blocked') end)m
  join 
(select user1, user2  from user_like where timestamp>=date_sub(curdate(), interval 1 day) and timestamp <curdate() )n
       on m.user_id=n.user1      
       )x 
join user_pi_activity upa on upa.user_id = x.user2 where case when x.status = 'bucketed' then case when x.bucket = 1 then upa.bucket in (0,2,3,4,5) else (case when x.bucket = 2 then upa.bucket in (0,3,4,5) else upa.bucket in (0,4,5) end ) end else upa.bucket in (0,1,2,3,4,5) or upa.bucket is null end group by x.user2";
	 */
	
	$sql = "SELECT x.user2 , sum( case when x.status = 'bucketed' or upa.bucket is null or upa.bucket = 0 then 1 else 0 end) as 'ab', sum( case when x.status in ('suspended', 'uninstalled') then 1 else 0 end) as 'normal' from
(select m.user_id, m.status, n.user2 , m.bucket from 
(select t.user_id, t.status , case when up.bucket  is null then 0 else up.bucket end as bucket from (select p.user_id , 'suspended' as status  from (select max(timestamp) as timestamp, user_id from user_suspension_log where date(timestamp) = date_sub(curdate(), interval 1 day) group by user_id)p join user_suspension_log usl on usl.user_id = p.user_id and usl.timestamp = p.timestamp where usl.current_status = 'suspended' and previous_status in ('authentic', 'blocked')
union 
select user_id, 'uninstalled' as status  from user_app_status where date(tstamp) = date_sub(curdate(), interval 1 day) and android_app = 'uninstall'
union
select user_id , 'bucketed' as status from user_pi_activity where bucket in (1,2,3) and last_bucket = 0 )t
join user u on t.user_id = u.user_id left join user_pi_activity up on up.user_id = t.user_id where u.gender = 'M' )m
  join 
(select user1, user2  from user_like where timestamp>=date_sub(curdate(), interval 1 day) and timestamp <curdate() )n
       on m.user_id=n.user1      
       )x 
left join user_pi_activity upa on upa.user_id = x.user2 where case when x.status = 'bucketed' or upa.bucket is null or upa.bucket = 0  then case when x.bucket = 1 then upa.bucket in (0,2,3,4,5) else (case when x.bucket = 2 then upa.bucket in (0,3,4,5) else upa.bucket in (0,4,5) end ) end else upa.bucket in (0,1,2,3,4,5) or upa.bucket is null end group by x.user2";
	/*$sql = "SELECT x.user2 , sum( case when x.user_type = 'ab' then 1 else 0 end) as 'ab', sum( case when x.user_type = 'normal' then 1 else 0 end) as 'normal'from
   (select t.user_id as user1, t.bucket, t1.user2,t.user_type from 
       (select u.user_id, case when u.user_id%" . Utils::$recoStagedReleaseConstant . " =0 then 'ab' else 'normal' end as user_type, up.bucket from user u join user_pi_activity up on u.user_id = up.user_id where gender = 'M' and status = 'authentic' and up.bucket in (1,2,3) and up.last_bucket = 0 )t  
       join 
       (select user1, user2  from user_like where timestamp>=date_sub(curdate(), interval 1 day) and timestamp <curdate() )t1 
       on t.user_id=t1.user1  
    )x join user_pi_activity upa on upa.user_id = x.user2 where case when x.user_type = 'ab' then case when x.bucket = 1 then upa.bucket in (0,2,3,4,5) else (case when x.bucket = 2 then upa.bucket in (0,3,4,5) else upa.bucket in (0,4,5) end ) end else case when x.bucket = 1 then upa.bucket in (0,3,4,5) else upa.bucket in (0,4,5) end end  group by x.user2";
*/	$resultSelect = $conn_reporting->Execute($sql);
	$arr = array();
	while ($row = $resultSelect->FetchRow()) {
		$arr[] = array($row['user2'], $row['normal']*-1, $row['ab']*-1, $row['ab'], $row['ab'], $row['normal'], $row['normal']);
	}

	$conn_master->bulkBind = true;
	
	
	//TODO: check if old version ids then purge 1 and 2 buckets only and that too from old availability stack
	if(!empty($arr))
	{
	//	$updateSql = $conn_master->Prepare("INSERT into user_availability (user_id, slots_filled) values (?,?) ON duplicate key UPDATE  slots_filled = slots_filled - ?"); // add tstamp
	//	$updateSql = $conn_master->Prepare("INSERT into user_availability (user_id, fresh_slots_filled, last_updated_fresh_slots_tstamp) values (?,?, now()) ON duplicate key UPDATE  fresh_slots_filled = ifnull(fresh_slots_filled,0) - ?, last_updated_fresh_slots_tstamp = now()");
		
		$updateSql = "INSERT into user_availability (user_id, slots_filled, last_updated, fresh_slots_filled, last_updated_fresh_slots_tstamp) 
							values (?,?,now(),?, now()) 
							ON duplicate key UPDATE  
											fresh_slots_filled = ifnull(fresh_slots_filled,0) - ?, 
											last_updated_fresh_slots_tstamp = case when ? >0 then now() else last_updated_fresh_slots_tstamp end , 
											slots_filled =  ifnull(slots_filled,0) - ?, 
											last_updated =  case when ? >0 then now() else last_updated end ";
		$res = $conn_master->Execute($updateSql, $arr);
	}
	//Utils::sendEmail($techTeamIds, "himanshu@trulymadly.com", "likes purging done for $baseurl", "Rows affected : ". $conn_master->Affected_Rows() . PHP_EOL . "ids updated :" . json_encode($arr));
}


try
{
	purgeLikes();
}
catch (Exception $e)
{
	global $baseurl, $techTeamIds;
	$msg = "Purging Likes from bucket 1, 2 and 3 script failed for $baseurl";
	Utils::sendEmail($techTeamIds, "himanshu@trulymadly.com", $msg, $e->getMessage() . "  --- " . $e->getTraceAsString());
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );

}

?>