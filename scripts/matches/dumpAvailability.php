<?php

//require_once dirname ( __FILE__ ) . "/../../include/matchConstants.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";


/**
 * update fresh and bucketized stack of availability count from redis into user_availability
 * flush the redis after clean up
 * @Himanshu
 */


class dumpAvailability{
	private $redis;
	private $conn;
	private $redisEvenDB;
	private $redisOddDB;
	private $file;
	private $fptr;

	function __construct() {
		global $redis, $conn_master,$redis_db_odd, $redis_db_even;
		$this->redis = $redis;
		$this->conn = $conn_master;
		$this->redisEvenDB = $redis_db_even;
		$this->redisOddDB = $redis_db_odd;
		$this->file = "/tmp/lock.txt";
		$this->fptr = fopen($this->file, 'x');
	}


	private function unlock()
	{
		flock($this->fptr, LOCK_UN);
		unlink($this->file);
	}

	private function lock()
	{
		if (flock($this->fptr, LOCK_EX)) {  // acquire an exclusive lock
			fwrite($this->fptr, getmypid());
			//fclose($lock);
			return true;
		}
		else
		{
			return false;
		}
	}

	public function notifyTechTeam($msg){
		global $techTeamIds, $baseurl;
		$subject = $msg. " for " . $baseurl;
		$from = "himanshu@trulymadly.com";
		Utils::sendEmail($techTeamIds, $from, $subject, $msg);
	}

	/**
	 * prepare an array for bulk insert
	 * @param unknown_type $availabilityKeys
	 * @param unknown_type $values
	 * @param unknown_type $namespace
	 */
	private function _formInsertionRowsArray ($availabilityKeys, $values, $namespace)
	{
		$arr = array();

		for ($i=0; $i<count($availabilityKeys); $i++){
			$id = ltrim( $availabilityKeys[$i], $namespace);
			$val = $values[$i];
			$user_ids_availability[$id] = $val;
			$arr[] = array($id, 1, $val, $val, $val);
			//$this->redis->MOVE($availabilityKeys[$i], 1);
			//$args[] = $availabilityKeys[$i] ;
		}
		return $arr;
	}

	/**
	 * updating the availability of bucket profiles, i.e., non-fresh
	 * @param unknown_type $availabilityKeys
	 * @param unknown_type $values
	 * @param unknown_type $namespace
	 */
	private function _updateBucketizedAvailability ($availabilityKeys, $values, $namespace)
	{

		$user_ids_availability = null;
		$arr = $this->_formInsertionRowsArray($availabilityKeys, $values, $namespace);
		$this->conn->bulkBind = true;
		$sql = $this->conn->Prepare("INSERT INTO user_availability (user_id, fresh_slots_filled, slots_filled, last_updated) values (?,?,?, now()) on duplicate key update last_updated = now(), slots_filled = case when (ifnull(slots_filled,1) + ?)< 1 then 1 else ifnull(slots_filled,1) + ? end  ");
		$this->conn->Execute($sql, $arr);
		var_dump($user_ids_availability);
	}

		
	/**
	 * updating the availability of fresh profiles
	 * @param unknown_type $availabilityKeys
	 * @param unknown_type $values
	 * @param unknown_type $namespace
	 */
	private function _updateFreshBucketAvailability ($availabilityKeys, $values, $namespace)
	{
		var_dump($availabilityKeys);
		$user_ids_availability = null;
		$arr = $this->_formInsertionRowsArray($availabilityKeys, $values, $namespace);
		$this->conn->bulkBind = true;
		var_dump($arr);
		$sql = $this->conn->Prepare("INSERT INTO user_availability (user_id, slots_filled, fresh_slots_filled, last_updated_fresh_slots_tstamp) values (?,?,?, now()) on duplicate key update last_updated_fresh_slots_tstamp= now(), fresh_slots_filled = case when (ifnull(fresh_slots_filled,1) + ?)< 1 then 1 else ifnull(fresh_slots_filled,1) + ? end  ");
		$this->conn->Execute($sql, $arr);
		var_dump($user_ids_availability);
	}


	/**
	 * acquires and releases the lock after the work is done
	 * driving function - calls different availability dumps and flush the redis
	 */
	public function dumpAvailability()
	{
		$lockFlag = $this->lock();
		if($lockFlag == true)
		{
			$minuteOfHour = Utils::getCurrentRedisIntervalInSpanOfFiveMinutes($this->redis);
			/*
			 $time = $this->redis->TIME;
			 var_dump($time);
			 $unixTimeStampInMin = ($time[1])/60;
			 */
			//$minuteOfHour = floor($unixTimeStampInMin/5)%2;
			/*	echo $unixTimeStampInMin;
			echo $minuteOfHour; die;*/
			//opposite db to be selected; other than like action db
			if($minuteOfHour == 0)$db =$this->redisEvenDB; else $db =$this->redisOddDB;
			$this->redis->SELECT($db);

			/**
			 * to get the bucketized slots filling
			 */
			$key = Utils::$redis_keys['like_count'] . "*";
			$availabilityKeys = $this->redis->KEYS($key);
			if(count($availabilityKeys)>0)
			{
				$values = $this->redis->MGET($availabilityKeys);
				$this->_updateBucketizedAvailability($availabilityKeys, $values, Utils::$redis_keys['like_count']);
			}


			$key = Utils::$redis_keys['fresh_like_count'] . "*";
			$freshAvailabilityKeys = $this->redis->KEYS($key);
			if(count($freshAvailabilityKeys)>0)
			{
				$values = $this->redis->MGET($freshAvailabilityKeys);
				$this->_updateFreshBucketAvailability($freshAvailabilityKeys, $values, Utils::$redis_keys['fresh_like_count']);
			}

			if (count($availabilityKeys)>0 || count($freshAvailabilityKeys) >0)
			$this->redis->FLUSHDB();


			$this->unlock();
		}
		else
		{
			$this->notifyTechTeam("Availability cron is still running. Cannot acquire a lock at the moment.");
		}

		//call_user_func_array(array($this->redis,'MOVE'),$args);
		/*	if($first_time==true){
		$this->redis->RPUSH($this->key,"-1");
		}
		*/
	}
}

try
{
	$dumpObj = new dumpAvailability();
	$dumpObj->dumpAvailability();
}
catch(Exception $e)
{
	$msg = "Availability generation script failed";
	$dumpObj->notifyTechTeam($msg);
	echo $e->getMessage();
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
}


?>
