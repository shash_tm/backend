<?php

require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
//require_once dirname ( __FILE__ ) . "/../../include/config_sumit.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
//require_once dirname ( __FILE__ ) . "/../../GenerateRecommendations.php";


/**
 * class to debug the case of no matches
 * @author himanshu
 *
 */
class noMatchesRecoReport
{
	public function getData($abFlag = false){

		$abVar = Utils::$recoStagedReleaseConstant;
		global $conn_reporting;
		if($abFlag == false)
		{	
			require_once dirname ( __FILE__ ) . "/../../GenerateRecommendations.php";
			$sql = "select t.user_id from (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch from user_recommendation_query_log where countFetched is not null and tstamp >= DATE_SUB(curdate(), INTERVAL '1 5:30' DAY_MINUTE)  and tstamp <= DATE_SUB(curdate(), INTERVAL '0 5:30' DAY_MINUTE) group by user_id)t join user u on u.user_id = t.user_id where u.user_id%$abVar !=0 and t.cntFetch = 0 and u.gender = 'M'";

		}
		else 
		{
			echo "\nxyz\n";
			require_once dirname ( __FILE__ ) . "/../../GenerateRecommendations_piLogic.php";
			$sql = "select t.user_id from (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch from user_recommendation_query_log where countFetched is not null and tstamp >= DATE_SUB(curdate(), INTERVAL '1 5:30' DAY_MINUTE)  and tstamp <= DATE_SUB(curdate(), INTERVAL '0 5:30' DAY_MINUTE) group by user_id)t join user u on u.user_id = t.user_id where u.user_id%$abVar=0 and t.cntFetch = 0 and u.gender = 'M'";
			echo "\nxyz\n";
		}

		echo "\nxyz\n";
		//$sql = "select t.user_id from (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch from user_recommendation_query_log where countFetched is not null   group by user_id)t join user u on u.user_id = t.user_id where t.cntFetch = 0 and u.gender = 'M'";
		$res = $conn_reporting->Execute($sql);
		$report_arr = array('actioned'=>0, 'availability' => 0 , 'pref' => 0);
		$user_ids = array();

		$i = 0;
		//$j = 10;
		if($res->RowCount()>0)
		{
			while ($row = $res->FetchRow())
			{
				$i++;
				//if($i == $j) break;
				$result = array();
				$recoObj = new generateNewMatches($row['user_id']);
				$result = $recoObj->generateMatchesUsingMemoryTable($row['user_id'], 0 , true);
				if(!empty($result[$row['user_id']]))
				{
					if($result[$row['user_id']]['pref_filter'] > 0)
					{
						if($result[$row['user_id']]['already_actioned'] > 0 && $result[$row['user_id']]['actioned_filter'] == 0 )
						{
							if ($result[$row['user_id']]['actioned_filter'] == 0)
							{
								$report_arr['actioned'] =	$report_arr['actioned'] +1;
								$user_ids['actioned'][] = $row['user_id'];
							}
						}
						else
						{
							if ($result[$row['user_id']]['availability_filter'] == 0 )
							{
								$report_arr['availability'] = $report_arr['availability'] + 1;
								$user_ids['availability'][] = $row['user_id'];
							}
						}
					}
					else
					{
						$report_arr['pref'] = $report_arr['pref'] + 1;
						$user_ids['preference'][] = $row['user_id'];
					}
				}
			}
		}

		$report_arr['will_get'] = $i - ($report_arr['pref'] + $report_arr['availability'] + $report_arr['actioned']);

		echo "\nxyz\n";
		//write a separate mailing on user ids here
		Utils::sendEmail("shashwat@trulymadly.com, himanshu@trulymadly.com, poonam.kumawat@trulymadly.com" , "himanshu@trulymadly.com", "today's male user ids with no matches ". date("d-m-y"), json_encode($user_ids));
		echo "\nxyz\n";
		return $report_arr;
	}

}


?>