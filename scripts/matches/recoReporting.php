<?php

//require_once dirname ( __FILE__ ) . "/../../include/matchConstants.php";
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
//require_once dirname ( __FILE__ ) . "/../../include/config_sumit.php";

require_once dirname ( __FILE__ ) . "/../../include/Utils.php"; 
require_once dirname ( __FILE__ ) . "/recoNoMatchesReport.php"; 



function getData(){
	global $conn_reporting, $smarty;

	$defaultConstant = Utils::$defaultBucketConstant;
	$slots_filled_limit = Utils::$availability_slots_limit;
// 	$slots_filled_limit=40;

	$ab_variable = Utils::$recoStagedReleaseConstant;
	$no_match_obj = new noMatchesRecoReport();
	$report_no_match = $no_match_obj->getData();
	$smarty->assign("noMatchReport", $report_no_match);

	$sql1 = "select count(up.user_id) as count,gender,  bucket from  user_search up where user_id%$ab_variable!=0 group by up.gender, bucket";
	/*$sql1 = "select sum(t.user_id) as count, t.gender , t.bucket  from (select count(up.user_id) as user_id,gender,     pi,
			case when gender = 'F' then case when     pi = $defaultConstant then 0 else (case when     pi< 0.3 then 1 else (case when     pi<0.45 then 2 else (case when     pi<0.6 then 3 else(case when     pi<0.75 then 4 else 5 end)end)end) end)end
			 else 
			(case when      pi = $defaultConstant then 0 else (case when     pi< 0.04 then 1 else (case when     pi<0.08 then 2 else (case when     pi<0.14 then 3 else(case when     pi<0.25 then 4 else 5 end)end)end) end) end)end as bucket from  user_search up group by up.gender,     pi)t group by t.gender,t.bucket order by t.gender desc ";
	*//*	$sql1 ="select sum(t.user_id) as count, t.gender , t.bucket  from (select count(up.user_id) as user_id,gender, actual_pi,
	 case when gender = 'F' then case when actual_pi = $defaultConstant then 0 else (case when actual_pi< 0.3 then 1 else (case when actual_pi<0.45 then 2 else (case when actual_pi<0.6 then 3 else(case when actual_pi<0.75 then 4 else 5 end)end)end) end)end
	 else
	 (case when  actual_pi = $defaultConstant then 0 else (case when actual_pi< 0.04 then 1 else (case when actual_pi<0.08 then 2 else (case when actual_pi<0.14 then 3 else(case when actual_pi<0.25 then 4 else 5 end)end)end) end) end)end as bucket from user_pi_activity up join user_search u on up.user_id = u.user_id  group by u.gender, actual_pi)t group by t.gender,t.bucket order by t.gender desc ";
	 */	$result1 = $conn_reporting->Execute($sql1);
	$rows1 = $result1->GetRows();
	//var_dump($rows1);
	$smarty->assign("bucketStats",  $rows1);

	//$sql2 = "select count(t.user_id) as count , t.cntFetch as matches, u.gender  from(select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch ,sum( countFetched) from user_recommendation_query_log where countFetched is not null and DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))='2014-11-12' group by user_id)t join user u on u.user_id = t.user_id group by u.gender,t.cntFetch";
	/*$sql2 = "select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end) 			else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <10 then '01-10' else (case when k.cntFetch <30 then '11-30' else (case when k.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end as matches, sum(k.flag) as action_count, sum(k.flag)*100/count(k.user_id) as perc, u.gender  from

	(select t.user_id,  t.cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) = t.cntFetch then 1 else 0 end as flag from (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch from user_recommendation_query_log where countFetched is not null and DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))='2014-11-12' group by user_id)t left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))='2014-11-12' group by user1 )t1 on t.user_id=t1.user1
	left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))='2014-11-12'  group by user1)t2   on t.user_id = t2.user1
	left join (select count(user2) as may_count, user1 from user_maybe where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))='2014-11-12' and is_active=1  group by user1)t3   on t.user_id = t3.user1
	)k
	join user u on k.user_id = u.user_id

	group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end) 			else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <10 then '01-10' else (case when k.cntFetch <30 then '11-30' else (case when k.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end";
	*/	//$sql2="select count(t.user_id) as count , case when u.gender='M' then (case when t.cntFetch =0 then '0' else (case  when t.cntFetch <=5 then '1-5' else (case  when t.cntFetch <=10 then '6-10' else (case  when t.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end) 			else    (case when t.cntFetch =0 then '0' else (case when t.cntFetch <10 then '1-10' else (case when t.cntFetch <30 then '11-30' else (case when t.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end as matches, u.gender  from(select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch ,sum( countFetched) from user_recommendation_query_log where countFetched is not null and DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))='2014-11-12' group by user_id)t join user u on u.user_id = t.user_id group by u.gender, case when u.gender='M' then (case when t.cntFetch =0 then '0' else (case  when t.cntFetch <=5 then '1-5' else (case  when t.cntFetch <=10 then '6-10' else (case  when t.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end) 			else    (case when t.cntFetch =0 then '0' else (case when t.cntFetch <10 then '1-10' else (case when t.cntFetch <30 then '11-30' else (case when t.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end";


	//$sql2 = "select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <10 then '01-10' else (case when k.cntFetch <30 then '11-30' else (case when k.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end as matches, sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0))/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))='2014-11-12' group by user1)t        left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))='2014-11-12' group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))='2014-11-12'  group by user1)t2   on t.user_id = t2.user1     left join (select count(user2) as may_count, user1 from user_maybe where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))='2014-11-12' and is_active=1  group by user1)t3   on t.user_id = t3.user1     )k     join user u on k.user_id = u.user_id         group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <10 then '01-10' else (case when k.cntFetch <30 then '11-30' else (case when k.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end";
	//$sql2 = "select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <10 then '01-10' else (case when k.cntFetch <30 then '11-30' else (case when k.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end as matches, sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0))/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day)  group by user1)t2   on t.user_id = t2.user1     left join (select count(user2) as may_count, user1 from user_maybe where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) and is_active=1  group by user1)t3   on t.user_id = t3.user1     )k     join user u on k.user_id = u.user_id         group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <10 then '01-10' else (case when k.cntFetch <30 then '11-30' else (case when k.cntFetch <50 then '31-50' else '50+' end)end)end) end ) end";
	//$sql2 ="select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=50 then '31-50' else '50+' end)end)end) end ) end as matches, sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0) = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0))/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) + ifnull(t3.may_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day)  group by user1)t2   on t.user_id = t2.user1     left join (select count(user2) as may_count, user1 from user_maybe where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) and is_active=1  group by user1)t3   on t.user_id = t3.user1     )k     join user u on k.user_id = u.user_id         group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=50 then '31-50' else '50+' end)end)end) end ) end";
	//$sql2 = "select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=60 then '31-60' else '60+' end)end)end) end ) end as matches, sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0)  = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) )/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day)  group by user1)t2   on t.user_id = t2.user1        where coalesce(t2.hide_count,0) +coalesce(t1.like_count,0) > 0  )k     join user u on k.user_id = u.user_id   where u.status = 'authentic'      group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=60 then '31-60' else '60+' end)end)end) end ) end";
	$sql2 = "select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=60 then '31-60' else '60+' end)end)end) end ) end as matches, sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0)  = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) )/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day)  group by user1)t2   on t.user_id = t2.user1        where coalesce(t2.hide_count,0) +coalesce(t1.like_count,0) > 0  )k     join user u on k.user_id = u.user_id   where u.status = 'authentic' and u.user_id%$ab_variable !=0      group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=60 then '31-60' else '60+' end)end)end) end ) end";
	//$sql2 = " select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=40 then '11-40' else (case when k.cntFetch <=80 then '41-80' else '80+' end)end)end) end ) end as matches, sum(k.flag) as action_count, sum(k.flag) as full_action, sum(k.flag_70) as 70_action, sum(k.flag)*100/count(k.user_id) as full_perc, sum(k.flag_70)*100/count(k.user_id) as 70_perc, sum(k.flag_50)*100/count(k.user_id) as 50_perc, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0)  = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) )/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day)  group by user1)t2   on t.user_id = t2.user1        where coalesce(t2.hide_count,0) +coalesce(t1.like_count,0) > 0  )k     join user u on k.user_id = u.user_id   where u.status = 'authentic'      group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=40 then '11-40' else (case when k.cntFetch <=80 then '41-80' else '80+' end)end)end) end ) end";
	$result2 = $conn_reporting->Execute($sql2);
	$rows2 = $result2->GetRows();
	//var_dump($rows2);
	
	$sql_zero_action = "select count(k.user_id) as count , case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=60 then '31-60' else '60+' end)end)end) end ) end as matches, u.gender  from        (select t.user_id,  ifnull(t.cntFetch,0) as cntFetch,  ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) as action, case when ifnull(t1.like_count,0) + ifnull(t2.hide_count,0)  = ifnull(t.cntFetch,0) then 1 else 0 end as flag, case when  (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0) )/(ifnull(t.cntFetch,0)) >=0.70   then 1 else 0 end as flag_70, case when (ifnull(t1.like_count,0) + ifnull(t2.hide_count,0))/(ifnull(t.cntFetch,0)) >=0.50   then 1 else 0 end as flag_50 from         ( select count(distinct(user2)) as cntFetch, user1 as user_id from user_recommendation_rank_log where DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t        left join (select count(user2) as like_count, user1  from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 )t1 on t.user_id=t1.user1    left join (select count(user2) as hide_count, user1 from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day)  group by user1)t2   on t.user_id = t2.user1        where coalesce(t2.hide_count,0) +coalesce(t1.like_count,0) = 0  )k     join user u on k.user_id = u.user_id   where u.status = 'authentic'  and u.user_id%$ab_variable!=0     group by u.gender, case when u.gender='M' then (case when k.cntFetch =0 then '0' else (case  when k.cntFetch <=5 then '01-05' else (case  when k.cntFetch <=10 then '06-10' else (case  when k.cntFetch <=20 then '11-20'  else '20+' end)end)end ) end)             else    (case when k.cntFetch =0 then '0' else (case when k.cntFetch <=10 then '01-10' else (case when k.cntFetch <=30 then '11-30' else (case when k.cntFetch <=60 then '31-60' else '60+' end)end)end) end ) end";
	$res_no_action = $conn_reporting->Execute($sql_zero_action);
	$rows_no_action = $res_no_action->GetRows();
	
	//$sql_zero_match = "select count(t.user_id) as count, gender from (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch from user_recommendation_query_log where countFetched is not null and DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))='2014-11-12' group by user_id)t join user u on u.user_id = t.user_id where t.cntFetch = 0 group by u.gender";
	$sql_zero_match = "select count(t.user_id) as count, gender from (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else( case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch from user_recommendation_query_log where countFetched is not null and DATE(CONVERT_TZ(tstamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user_id)t join user u on u.user_id = t.user_id where u.user_id%$ab_variable !=0 and t.cntFetch = 0 group by u.gender";
	$result_zero = $conn_reporting->Execute($sql_zero_match);
	$rows_zero = $result_zero->GetRows();
	//	$smarty->assign("zeromatchesStats",  $rows_zero);

	$arr = array();
	$i = 0;
	foreach ($rows_zero as $val){
		$val['matches'] = 0;
		$arr[$val['gender']][] = $val ;
	}
	
	$j = 0;
	$no_action = array();
	foreach ($rows_no_action as $val){
		$no_action[$val['gender']][$val['matches']] = $val['count'];
	}
	
	foreach ($rows2 as $val){
		$val['no_action']= $no_action[$val['gender']][$val['matches']] ; 
		//$val["no_action"] = $rows_no_action[$j];
		$arr[$val['gender']][] = $val ;
	}

	ksort($arr);

	$smarty->assign("matchesStats_male",  $arr['M']);
	$smarty->assign("matchesStats_female",  $arr['F']);


	//	var_dump($arr); die;
	//$smarty->assign("matchesStats",  array_values($arr));
	/*$sql3 = " select count(*) as count from user_availability where slots_filled >=40";
	$result3 = $conn_reporting->Execute($sql3);
	$rows3 = $result3->FetchRow();
	//var_dump($rows3);
	$smarty->assign("availabilityStats",  $rows3);
	*//*
	$sql4="select count(user_id) as count, gender from user_search group by gender";
	$result4 = $conn_reporting->Execute($sql4);
	$rows4 = $result4->GetRows();
	//var_dump($rows4);
	$smarty->assign("userSearch",  $rows4);*/
	/*
	 $sql5 = "select count(ua.user_id) as count , date(u.last_login) as ll from user_availability ua join user u on u.user_id = ua.user_id  where slots_filled >=40 group by date( u.last_login)";
	 $result5 = $conn_reporting->Execute($sql5);
	 $rows5 = $result5->GetRows();
	 //var_dump($rows5);
	 $smarty->assign("SlotsFilledStats",  $rows5);*/


	
	$sql_like_percent = "select gender, up.bucket, 
						round(sum(t.like_perc)*100/count(t.user1),2) as percent 
						from
						(select t1.user1, t1.liked/(t1.liked+ ifnull(t2.hide,0)) as like_perc from 
							(select user1, count(user2) as liked from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t1 left join 
							(select user1, count(user2) as hide from user_hide where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t2 on t1.user1 = t2.user1)t 
						join user u on u.user_id = t.user1 
						join user_pi_activity up on t.user1 = up.user_id 
						where u.user_id%$ab_variable !=0 
						group by u.gender, up.bucket";
	
	$res_like_percent = $conn_reporting->Execute($sql_like_percent);
	$rows_like_percent = $res_like_percent->GetRows();
	
	$likes_rows = array();
	foreach ($rows_like_percent as $val)
	{
		$likes_rows[$val['gender']][$val['bucket']] = $val['percent'];
	}
	$smarty->assign("likesPercent", $likes_rows);
	
	
	$sql_mutual_like = "select u.gender, count(x.fcount) as num, case when x.fcount <=5 then x.fcount else '5plus' end as ml from 
						(select user, sum(count) as fcount from 
								(select user1 as user, count(user2) as count from user_mutual_like ul1 where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 
								union 
								select user2 as user, count(user1) as count from user_mutual_like ul2 where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user2 
								) as m group by m.user
						 )x join user u on u.user_id = x.user 
						 where u.user_id%$ab_variable!=0 
						 group by u.gender, case when x.fcount <=5 then x.fcount else '5plus' end";
	$res_ml= $conn_reporting->Execute($sql_mutual_like);
	$rows_ml = $res_ml->GetRows();
	
	$total_mls_sql = " select  count(user2) as count from user_mutual_like ul1 where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) and (user1% $ab_variable !=0 OR user2 %$ab_variable !=0)";
	$res_mls_total = $conn_reporting->Execute($total_mls_sql);
	$rows_mls_total = $res_mls_total->FetchRow();
	
	$mls = array();
	$userWise_mls = array();
	foreach ($rows_ml as $val)
	{
		$mls[$val['gender']][$val['ml']] = $val['num'];
		$userWise_mls[$val['gender']] += $val['num'];
	}
	
	
	$smarty->assign("mls", $mls);
	$smarty->assign("mls_userWise", $userWise_mls);
	$smarty->assign("total_mls", $rows_mls_total['count']);
	
	
	/**
	 * bucket wise mutual likes %
	 */
	
	$sql_likes_sent  = "select gender, up.bucket, 
						sum(t.liked) as likes 
						from
						(select user1, count(user2) as liked from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t 
						join user_pi_activity up on t.user1 = up.user_id 
						join user u on u.user_id = t.user1
						where u.user_id%$ab_variable!=0 
						group by u.gender, up.bucket";
	$res_ls= $conn_reporting->Execute($sql_likes_sent);
	$rows_ls = $res_ls->GetRows();
	$likes_arr = array();
	foreach ($rows_ls as $val)
	{
		$likes_arr[$val['gender']][$val['bucket']] = $val['likes'];
	}
	
	$sql_mutual_like_bucketwise = "select u.gender, up.bucket, sum(x.fcount) as num from 
						(select user, sum(count) as fcount from 
								(select user1 as user, count(user2) as count from user_mutual_like ul1 where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 
								union 
								select user2 as user, count(user1) as count from user_mutual_like ul2 where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user2 
								) as m group by m.user
						 )x join user u on u.user_id = x.user join user_pi_activity up on x.user= up.user_id
						 where u.user_id%$ab_variable!=0 
						 group by u.gender, up.bucket";
	$res_mlb= $conn_reporting->Execute($sql_mutual_like_bucketwise);
	$rows_mlb = $res_mlb->GetRows();
	$ml_perc = array();
	foreach ($rows_mlb as $val)
	{
		$ml_perc[$val['gender']][$val['bucket']] = round(($val['num']/$likes_arr[$val['gender']][$val['bucket']])*100,2);
	}
	
	$smarty->assign("mls_perc", $ml_perc);
	
	
	
	
	
		/**
	 * mutual like count / like count of user
	 */
	
	
		
	$sql_likes_sent_count  = "select gender, up.bucket, 
						count(t.liked) as likesUni 
						from
						(select user1, count(user2) as liked from user_like where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1)t 
						join user_pi_activity up on t.user1 = up.user_id 
						join user u on u.user_id = t.user1
						where u.user_id%$ab_variable !=0 
						group by u.gender, up.bucket";
	$res_lsc= $conn_reporting->Execute($sql_likes_sent_count);
	$rows_lsc = $res_lsc->GetRows();
	$likes_arrc = array();
	foreach ($rows_lsc as $val)
	{
		$likes_arrc[$val['gender']][$val['bucket']] = $val['likesUni'];
	}
	

	$sql_mutual_like_bucketwise_count = "select u.gender, up.bucket, count(x.fcount) as cnt from 
						(select user, sum(count) as fcount from 
								(select user1 as user, count(user2) as count from user_mutual_like ul1 where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user1 
								union 
								select user2 as user, count(user1) as count from user_mutual_like ul2 where DATE(CONVERT_TZ(timestamp,'+00:00','+05:30'))=date_sub(curdate(), interval 1 day) group by user2 
								) as m group by m.user
						 )x join user u on u.user_id = x.user join user_pi_activity up on x.user= up.user_id
						 where u.user_id%$ab_variable !=0 
						 group by u.gender, up.bucket";
	$res_mlbc= $conn_reporting->Execute($sql_mutual_like_bucketwise_count);
	$rows_mlbc = $res_mlbc->GetRows();
	$ml_percc = array();
	foreach ($rows_mlbc as $val)
	{
		$ml_percc[$val['gender']][$val['bucket']] = round(($val['cnt']/$likes_arrc[$val['gender']][$val['bucket']])*100,2);
	}
	
	$smarty->assign("mls_perc_count", $ml_percc);
	
	/**
	 * 
	 */
	
	
	
	//$sql_available_bucket = "select count(u.user_id) as  count, case when     pi = $defaultConstant then 0 else (case when     pi< 0.3 then 1 else (case when     pi<0.45 then 2 else (case when     pi<0.6 then 3 else(case when     pi<0.75 then 4 else 5 end)end)end) end)end as bucket , case when slots_filled>$slots_filled_limit and slots_filled is not null then 'slots_full' else 'free' end as slots from user_search u left join user_availability ua on ua.user_id = u.user_id  where u.gender = 'F' group by case when     pi = $defaultConstant then 0 else (case when     pi< 0.3 then 1 else (case when     pi<0.45 then 2 else (case when     pi<0.6 then 3 else(case when     pi<0.75 then 4 else 5 end)end)end) end)end, case when slots_filled>$slots_filled_limit  and slots_filled is not null then 'slots_full' else 'free' end";
	$sql_available_bucket = "select count(u.user_id) as  count, bucket , case when slots_filled>$slots_filled_limit and slots_filled is not null then 'slots_full' else 'free' end as slots from user_search u left join user_availability ua on ua.user_id = u.user_id  where u.gender = 'F' and u.user_id%$ab_variable != 0 group by bucket, case when slots_filled>$slots_filled_limit  and slots_filled is not null then 'slots_full' else 'free' end";
	$result5 = $conn_reporting->Execute($sql_available_bucket);
	$rows5 = $result5->GetRows();

	$arr = null;
	foreach ($rows5 as $val){
		$arr[$val['bucket']][$val['slots']]= $val['count'];
	}

	$smarty->assign("SlotsFilledStats_bucket",  $arr);



	$sql_available_age = "select count(ua.user_id) as count,  case when age >=18 and age <=22 then '18-22' else (case when age >22 and age <=26 then '22-26' else (case when age >26 and age <=30 then '26-30' else ( case when age >30 and age <=35 then '30-35' else  '35+' end) end) end ) end as age_group , case when (slots_filled>$slots_filled_limit  and slots_filled is not null) then 'slots_full' else 'free' end as slots from user_search ud left join user_availability ua  on ua.user_id = ud.user_id where ud.user_id%$ab_variable!=0 and ud.gender = 'F' group by  case when age >=18 and age <=22 then '18-22' else (case when age >22 and age <=26 then '22-26' else (case when age >26 and age <=30 then '26-30' else ( case when age >30 and age <=35 then '30-35' else  '35+' end) end) end ) end, case when (slots_filled>$slots_filled_limit and slots_filled is not null) then 'slots_full' else 'free' end ";
	$result6 = $conn_reporting->Execute($sql_available_age);
	$rows6 = $result6->GetRows();

	$arr = null;
	foreach ($rows6 as $val){
		$arr[$val['age_group']][$val['slots']]= $val['count'];
	}
	$smarty->assign("SlotsFilledStats_age",  $arr);


	$sql_available_lastLogin = "select count(u.user_id) as count,  case when datediff(now(),u.last_login) > 15 then '15+' else  datediff(now(),u.last_login) end as ll, case when slots_filled>$slots_filled_limit then 'slots_full' else 'free' end as slots from  user_search us left join user_availability ua on us.user_id = ua.user_id join user u on u.user_id = us.user_id  where us.user_id%$ab_variable!=0 and us.gender = 'F' group by  case when datediff(now(),u.last_login) > 15 then '15+' else  datediff(now(),u.last_login) end , case when slots_filled>$slots_filled_limit then 'slots_full' else 'free' end order by u.last_login desc ";
	$result_ll = $conn_reporting->Execute($sql_available_lastLogin);
	$rows_ll = $result_ll->GetRows();

	$arr = null;
	foreach ($rows_ll as $val){
		$arr[$val['ll']][$val['slots']]= $val['count'];
	}
	$smarty->assign("SlotsFilledStats_ll",  $arr);



	//$sql_available_city = " select t.total_count, t.filled, t.name, t1.male_count from (select count(us.user_id) as total_count, sum(case when slots_filled>$slots_filled_limit then 1 else 0 end) as filled,  gc.name, city, state from user_search us left join user_availability ua on us.user_id = ua.user_id join geo_city gc on gc.city_id = us.city and gc.state_id=us.state  where gender = 'F' group by city order by count(*) desc limit 20)t join (select count(*) as male_count, city,state from user_search where gender ='M' group by city)t1 on t.city=t1.city and t.state= t1.state order by t.total_count desc ";
	//$sql_available_city = "select t.total_count, t.filled, t.name, t1.male_count from (select count(us.user_id) as total_count, sum(case when slots_filled>$slots_filled_limit then 1 else 0 end) as filled,  gc.name, city from user_search us left join user_availability ua on us.user_id = ua.user_id join geo_city gc on gc.city_id = us.city  where gender = 'F' group by city order by count(*) desc limit 20)t join (select count(*) as male_count, city from user_search where gender ='M' group by city)t1 on t.city=t1.city order by t.total_count desc ";
	$sql_available_city = "select x.bucket, x.total_count, x.filled, x.name, t1.male_count from 
(
select u.bucket, count(u.user_id) as total_count, sum(case when ua.slots_filled>$slots_filled_limit then 1 else 0 end) as filled,  t.name, t.city
from 
(select city, gc.name from user_search us join geo_city gc on gc.city_id = us.city  where gender = 'F' group by city order by count(*) desc limit 20)t join user_search u on u.city = t.city 
 left join user_availability ua on u.user_id = ua.user_id where gender = 'F' and u.user_id%$ab_variable !=0   group by t.city, u.bucket)x
 join (select count(*) as male_count, city from user_search where gender ='M' and user_id%$ab_variable !=0  group by city
)t1 on x.city=t1.city order by x.total_count desc ";
	$result_city = $conn_reporting->Execute($sql_available_city);
	$rows_city = $result_city->GetRows();
	$city_arr = array();
	foreach ($rows_city as $val)
	{
		$city_arr[$val['name']][$val['bucket']] = $val;
	}
	//var_dump($city_arr);
/*	$arr = null;
	foreach ($rows_city as $val){
		$arr[$val['city']][$val['slots']]= $val['count'];
	}*/
	//$smarty->assign("SlotsFilledStats_ll",  $arr);
	$smarty->assign("SlotsFilledStats_city",  $city_arr);

	/*$sql6 ="select count(ua.user_id) as count ,  case when slots_filled>40 then 'slots_full' else 'free' end as slots, gc.name as city from user_availability ua join user_data ud on ud.user_id = ua.user_id join geo_city gc on gc.city_id = ud.stay_city   group by case when slots_filled>40 then 'slots_full' else 'free' end, ud.stay_city   order by  case when slots_filled>40 then 'slots_full' else 'free' end, gc.name";
	 $result6 = $conn_reporting->Execute($sql6);
	 $rows6 = $result6->GetRows();
	 $arr = null;
	 foreach ($rows6 as $val){
		$arr[$val['city']][$val['slots']]= $val['count'];
		}
		//var_dump($arr);die;
		$smarty->assign("cityStats",  $arr);*/



	/*$sql7 = " select sum(t.user_id) as bucket_count , t.bucket  from (select count(up.user_id) as user_id, actual_pi,  case when actual_pi< 0.3 then 1 else (case when actual_pi<0.45 then 2 else (case when actual_pi<0.6 then 3 else(case when actual_pi<0.75 then 4 else 5 end)end)end) end  as bucket from user_pi_activity up join user_availability u on up.user_id = u.user_id and u.slots_filled<40 group by  actual_pi)t group by t.bucket";
	 $result7 = $conn_reporting->Execute($sql7);
	 $rows7 = $result7->GetRows();
	 //var_dump($rows7);
	 $percents = null;
	 $bucketSum = null;
	 foreach ($rows7 as $val){
		$bucketSum += $val['bucket_count'];
		}
		$i=0;
		foreach ($rows7 as $val){
		$percents[$i]['percent'] = round(($val['bucket_count']/$bucketSum)*100, 2);
		$percents[$i]['bucket'] = $val['bucket'];
		$percents[$i]['bucket_count'] = $val['bucket_count'];
		$i++;
		}

		$final_arr = null;
		$i=0;
		foreach ($rows1 as $val){
		$final_arr[$i] =$val;
		if($val['gender'] == 'F'){
		$final_arr[$i]['percent'] = $percents[$i]['percent'];
		$final_arr[$i]['bucket_count'] = $percents[$i]['bucket_count'];
		}
		$i++;
		}
		//var_dump($final_arr); die;

		$smarty->assign("bucketBreak",  $final_arr);
		*/
	//	$smarty->assign("percentStats",  $percents);
	
	
	
	$sql_checkpoint = "select max(checkpoint) as checkpoint from user_search_checkpoint";
	$result_checkpoint = $conn_reporting->Execute($sql_checkpoint);
	$checkpointRow = $result_checkpoint->FetchRow();
	$smarty->assign("checkpoint", $checkpointRow['checkpoint']);
	
		
	$sqlUsearch = "SELECT count(user_id) as count, gender  from user_search  where user_id%$ab_variable !=0 group by gender";
	$rowsSearch = $conn_reporting->Execute($sqlUsearch);

	$count_inserted_rows = null;
	$genderWiseUsers = null;
	foreach ($rowsSearch as  $val){
		$count_inserted_rows += $val['count'];
		$genderWiseUsers[$val['gender']] = $val['count'];
	}
	$smarty->assign("rows_us", $rowsSearch);
	$smarty->assign("count_us", $count_inserted_rows);
	
	$userWise_mls_percent = array();
	foreach ($userWise_mls as $key => $val){
		$userWise_mls_percent[$key] = round(($val*100)/$genderWiseUsers[$key],2);
	}
	$smarty->assign("userWiseMls", $userWise_mls_percent);
	
	$sql_bucket = "select * from bucket_log order by tstamp desc , bucket asc limit 4 ";
	$res_bucket = $conn_reporting->Execute($sql_bucket);
	$rowsBucket = $res_bucket->GetRows();
	
	$smarty->assign("last_bucket_generated", $rowsBucket[0]['tstamp']);
	$smarty->assign("bucket_log", $rowsBucket);
	
}

function notifyTechTeam($msg){
	global $techTeamIds, $baseurl;
	var_dump($techTeamIds);
	$subject = $msg. " for " . $baseurl;
	$from = "himanshu@trulymadly.com";
	Utils::sendEmail($techTeamIds, $from, $subject, $msg);
}

function sendMail(){
	global $smarty;
	$subject = "Recommendation Statistics Report for Algo A: ". date('d-m-Y',strtotime("-1 days"));;
	$to = (date('D', time()) === 'Mon') ? ( Utils::$managementTeam . ", hitesh@trulymadly.com" ) :  Utils::$managementTeam ;
	Utils::sendEmail($to , "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/recoReport.tpl'), TRUE);
	//Utils::sendEmail("himanshu@trulymadly.com", "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/recoReport.tpl'), TRUE);
}

try{
	if(php_sapi_name() === 'cli'){
		getData();
		sendMail(); 
		//require_once  dirname ( __FILE__ ). '/recoReportingStagedRelease.php';
	}
}
catch (Exception $e){
	$msg = "Recommendation Reporting Failed";
	notifyTechTeam($msg . " " . $e->getMessage());
	echo $e->getMessage();
	trigger_error ( "PHP WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );

}

?>