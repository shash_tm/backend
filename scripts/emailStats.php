<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";




function mailStats($external_date = null)
{
	
	global $conn_reporting,$smarty, $emailReportingIds, $emailIdsForReporting;

	// Number of Emails opened 
	$campaigns = array(
					"messages", "incomplete_profile_nextDay", "FoundSomeone", "endorsement_given", "check_new_match_nextDay",  "mutual_like_with_common_hashTags", "mutual_like_without_common_hashTags",
					 "admin_photoId_approved", "admin_photoId_notClear","admin_photoId_name_age_mismatch", "admin_photoId_name_mismatch", "admin_photoId_age_mismatch", "admin_photoId_password_required", "admin_all_photos_rejected", "admin_profile_photo_approved_others_rejected",
					"weekly_unread_messages", "BoostPIScore",  "DPmale", "DPfemale"
				);
	$smarty->assign("campaigns", $campaigns);

	$sql = "select count(distinct(user_id)) as count , event_type  as campaign from action_log_new where activity = 'emailers' and  tstamp >= date_sub(CURDATE(), interval 1 day) and tstamp < curdate()  group by event_type";
	//$sql = " select count(distinct(user_id)) as count , campaign from action_log where content_part_clicked ='open_rate' and  date(tstamp) = date_sub(CURDATE(), interval 1 day)  group by campaign order by campaign";
	//$result = Query::SELECT($sql, null);
	$rows = $conn_reporting->Execute($sql);
	$result = $rows->GetRows();
	
	$openArr = array();
	
	foreach ($result as $val)
	{
		$openArr[$val['campaign']] = $val['count'];
	}
	$smarty->assign("openRate", $openArr);
	
	$sentMails = array();
	
	//Number of Emails sent yesterday
	$sql2 = "select count(subject) as count, subject from system_mailer_log where   tstamp >= date_sub(CURDATE(), interval 1 day) and tstamp < curdate()  group by subject order by subject";
	//$result2 = Query::SELECT($sql2, null);
	
	$rows2 = $conn_reporting->Execute($sql2);
	$result2 = $rows2->GetRows();
	
	foreach ($result2 as $r => $c)
	{
		$sentMails[$c['subject']] = $c['count'];
	}
	$smarty->assign("sentMails", $sentMails);
	
	// Number of Members who came to the site from Email

	$siteTraction = array();
	
	$sql3 = "select count(distinct(user_id)) as count , event_type  as campaign from action_log_new where activity = 'emailers_click' and  tstamp >= date_sub(CURDATE(), interval 1 day) and tstamp < curdate()  group by event_type order by event_type";
	//$sql3="select count(distinct(user_id)) as count , campaign from action_log where source = 'system_emailer' and content_part_clicked !='open_rate' and date(tstamp) = date_sub(CURDATE(), interval 1 day)  group by campaign order by campaign";
	//$result3 = Query::SELECT($sql3, null);
	$rows3 = $conn_reporting->Execute($sql3);
	$result3 = $rows3->GetRows();
	
	foreach ($result3 as $e => $v)
	{
		$siteTraction[$v['campaign']] = $v['count'];
	}
	$smarty->assign("siteVisits", $siteTraction);
	
//Number of Emails sent today

	$todayMails = array();
	
	$sql4 = "select count(subject) as count, subject from system_mailer_log where date(tstamp) = curdate() group by subject order by subject";
	//$result4 = Query::SELECT($sql4, null);
	
	$rows4 = $conn_reporting->Execute($sql4);
	$result4 = $rows4->GetRows();
	
	foreach ($result4 as $r => $c)
	{
		$todayMails[$c['subject']] = $c['count'];
	}
	$smarty->assign("todayMails", $todayMails);
	
	
	// =  $emailReportingIds . ", hitesh@trulymadly.com, sachin@trulymadly.com, masrat@trulymadly.com, saumya.agrawal@trulymadly.com, gaurav@trulymadly.com";
	$to = $emailIdsForReporting['mailer_reporting'];
	$subject = "Email Statistics: ". date("d-m-y");
	$from = "himanshu@trulymadly.com";
	Utils::sendEmail($to, $from, $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../templates/utilities/emailStats.tpl'), TRUE);
	//Utils::sendEmail($from, $from, $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../templates/utilities/emailStats.tpl'), TRUE);
	
}

try
{
	if(php_sapi_name() === 'cli')
	{
		$options=getopt ( "d:e:" );
		mailStats();
	}
}
catch (Exception $e)
{
	global $techTeamIds, $baseurl;
	$to = $techTeamIds;
	//$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Email Reporting Mailer failed for " . $baseurl;
	Utils::sendEmail($to, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Email Reporting Mailer ". $e->getTraceAsString(), E_USER_WARNING );
}

?>