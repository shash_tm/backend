<?php
require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../spark/Spark.class.php";
require_once dirname ( __FILE__ ) . "/../DBO/sparkDBO.php";
require_once dirname ( __FILE__ ) . "/../msg/MessageFullConversation.class.php";

/* Author : Tarun
 * sends custom push notifications to different set of users based on certain conditions  */

class newSparkObject {
	public $conn_reporting;
	public $conn_master;
	public $admin_id;
	public $data;
	public $ticker_text;
	public $push_notification;
	public $user_id;
	private $messageFullObj ;
	private $message ;
	private $messageId ;
	private $uu ;

	function __construct() {
		global $admin_id,$conn_reporting,$miss_tm_id, $conn_master;
		$this->conn_reporting = $conn_reporting;
		$this->conn_master = $conn_master;
		$this->admin_id = $admin_id;
		$this->ticker_text= 'Message from TrulyMadly';
		$this->data = array(
			'title_text' => "1 free spark !!",
			'content_text' => "Use it wisely my friend !",
			'type' => 'PROMOTE',
			'push_type' => 'PROMOTE',
			'category_id' => NULL,
			'categoryTitle' => NULL,
			'event_status' => 'FREE_SPARKS'
		);
		$this->push_notification = new pushNotification();
		$this->misstm= $miss_tm_id;
		$this->uu = new UserUtils();
	}

	function sendMissTmMessage($user_id)
	{
		$this->user_id = $user_id;
		$this->createMessageConvoObj();
		$this->getMessage();
		$this->sendMessage();
		$this->sendPushNotification();
		$insertSql = "UPDATE free_sparks_history SET sms_sent=1 where user_id = $user_id";
		$res = $this->conn_master->Execute($insertSql);
	}

	private function getImageMessage()
	{
		global $cdnurl  ;

		$this->imageMessage = array( "msg"=>"One free Spark! Use it today",
			"metadata" => array("link_landing_url" => "trulymadly://launch",
				"message_type" => "IMAGE_LINK",
				"link_image_url" =>$cdnurl."/images/notifications/sparky.jpg"),
			"message_type" => "IMAGE_LINK");

		$this->imageMessage= json_encode($this->imageMessage);

	}

	private function getMessage()
	{
		$this->message="Guess who won 5 free Sparks today? That's right, YOU did! Use 'em wisely before they expire and get set, get Sparking!";
	}

	private function sendPushNotification()
	{
		$message_url = $this->uu->generateMessageLink($this->user_id,$this->misstm);
		$push_arr =  array("is_admin_set"=> 0,
			"content_text"=>"Guess who won 5 free Sparks today? That's right, YOU did! Use 'em wisely before they expire and get set, get Sparking!",
			"ticker_text"=>"Message From Miss TM",
			"title_text"=>"TrulyMadly",
			"message_url"=>$message_url,
			"match_id"=>$this->user_id,
			"msg_type" => "TEXT",
			"msg_id"=> $this->messageId,
			"push_type"=>"MESSAGE",
			"event_status"=> "BUY_sparks_trial_campaign");

		$this->push_notification->notify($this->user_id, $push_arr, $this->misstm);
	}

	function giveSparksToUser($spark_given,  $event_status, $test_user_id = null){
		$userSql = "select u.user_id
						from user u
						join user_data ud on u.user_id = ud.user_id
						join geo_city gc on gc.city_id = ud.stay_city and gc.state_id = ud.stay_state
						Where u.user_id not in (select distinct(user_id) from user_spark_counter)
						and u.registered_from in ('android_app','ios_app')
						and u.gender='M'
						and gc.name in ('Delhi','Mumbai','Bengaluru','Pune','Kolkata','Hyderabad','Chennai',
						'Gurgaon','Ahmedabad','Noida')
						and EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) > 24
						and u.user_id in
						(SELECT distinct user_id from action_log_new
						where activity='sparks' and event_type='buy' and event_status='$event_status' and tstamp >  DATE(DATE_SUB(now(), INTERVAL 2 DAY))
						and tstamp <=  DATE(DATE_SUB(now(), INTERVAL 1 DAY))) ";

		if ($event_status == 'view')
			$userSql .= " limit 200";

		$res = $this->conn_reporting->Execute($userSql);

		if ($res->RowCount() > 0 && (!isset($test_user_id) || $test_user_id == null)) {
			while ($row = $res->FetchRow()) {
				$user_id = $row['user_id'];
				SparkDBO::addSparkCount($user_id, $spark_given, 7, 0);
				$insertSql = "INSERT INTO free_sparks_history VALUES(null, $user_id, $spark_given, 0, now())";
				$res1 = $this->conn_master->Execute($insertSql);
				$this->user_id = $user_id;
			}
		}else if($test_user_id != null) {
			SparkDBO::addSparkCount($test_user_id, $spark_given, 7, 0);
			$insertSql = "INSERT INTO free_sparks_history VALUES(null, $test_user_id, $spark_given, 0, now())";
			$res1 = $this->conn_master->Execute($insertSql);
			$this->user_id = $test_user_id;
		}
	}

	private function sendImageMessage()
	{
		$unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
		$this->messageId =  $this->messageFullObj->storeMostRecentChat ( $this->imageMessage, 'JSON', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
		//  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

	}
	private function createMessageConvoObj()
	{
		$this->messageFullObj = new MessageFullConversation ( $this->misstm, $this->user_id );
	}

	private function sendMessage()
	{
		$unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
		$this->messageId=  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
		//  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

	}

}

try {

	if(php_sapi_name() === 'cli') {
		$date= date('Y-m-d',strtotime('today'));
		$sparkObj = new newSparkObject();

		$action = isset($argv[1]) && $argv[1] != null ? $argv[1] : null;
		$event_status = isset($argv[2]) && $argv[2] != null ? $argv[2] : 'buy';
		$test_user_id = isset($argv[3]) && $argv[3] != null ? $argv[3] : null;

		if($action == null || !isset($action)){
			echo "pass action as first param. Accepted values: 'send_sms', 'free_sparks'";
			die;
		}

		if($action == 'free_sparks') {
			if($event_status == 'buy' || $event_status == 'both')
				$sparkObj->giveSparksToUser(5, 'buy', $test_user_id);
			if($event_status == 'view' || $event_status == 'both')
				$sparkObj->giveSparksToUser(5, 'view', $test_user_id);
		}else if($action == 'send_sms'){
			$userSql = "SELECT user_id from free_sparks_history where sms_sent = 0 and tstamp <  DATE_SUB(now(), INTERVAL 3 HOUR)";
			$res = $sparkObj->conn_reporting->Execute($userSql);
			if ($res->RowCount() > 0 && (!isset($test_user_id) || $test_user_id == null)) {
				while ($row = $res->FetchRow()) {
					$user_id = $row['user_id'];
					if($action == 'send_sms'){
						$sparkObj->sendMissTmMessage($user_id);
						echo PHP_EOL;
					}
				}
				$to = "tarun@trulymadly.com, shashwat@trulymadly.com";
				$subject = "5 free spark given to users: ".$date;
				$from = "admintm@trulymadly.com";
				$mailmessage = "5 free spark given to Target males.";
				Utils::sendEmail($to, $from, $subject, $mailmessage." : ".$count,true);
			}else if($test_user_id != null){
				$sparkObj->sendMissTmMessage($test_user_id);
				echo PHP_EOL;
			}
		}else{
			echo "pass action as first param. Accepted values: 'send_sms', 'free_sparks'";
			die;
		}
	}

} catch (Exception $e) {
	echo $e->getMessage();
	trigger_error($e->getTraceAsString(), E_USER_WARNING);
	Utils::sendEmail("backend@trulymadly.com" , "tarun@trulymadly.com", "Free sparks script failed".$baseurl, $e->getTraceAsString());
}


?>