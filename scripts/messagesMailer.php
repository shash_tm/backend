<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../logging/UnsubscriptionClass.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";



/**
 * @Himanshu
 * Mailer for hourly unread messages
 */



function sendMailer($external_email = null){

	try{
		global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl, $admin_id;
		$sysLog = new SystemLogger();
		$uu = new UserUtils();

		$sql = "select group_concat(mq.sender_id) as senders,group_concat(mq.msg_type) as msg_type,  group_concat(mq.tStamp) as time,group_concat('TMSALT' ,  mq.msg_content) as content, count(mq.sender_id) as count, mq.receiver_id from messages_queue mq where mq.blocked_by is null and mq.last_seen < mq.tStamp  and mq.tStamp >= date_sub(NOW(), interval 2 HOUR) and mq.tStamp <= date_sub(NOW(), interval 1 HOUR)  and msg_type != 'MUTUAL_LIKE'  group by mq.receiver_id order by mq.receiver_id, mq.tStamp desc";

		//echo $sql;
		$res = $conn_slave->Execute($sql);
		$data =$res->GetRows();
		//		$data = Query::SELECT($sql, null);

		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=messages";
		$redirectionFile = "/msg/messages.php";
		$utm_content="view_messages";
		$campaignName = "messages";

		$i = 0;
		$prev_user = null;
		$smartydata = null;
		$idsToMail = null;
		$subjects = null;
		foreach ($data as $row => $col){
			$senders = null;
			$timeStamp = null;
			$idsToMail[] = $col['receiver_id'];
			$msgs = null;
			$senders  = explode(',', $col['senders']);
			$timeStamp = explode(',', $col['time']);
			$contents = explode(",TMSALT", $col['content']);
			$msg_types = explode(',', $col['msg_type']);
			$timeArr = null;
			$content_arr = null;
			for($i=0;$i<count($timeStamp); $i++){
				$timeArr[$senders[$i]] = $timeStamp[$i];
			}
				
			var_dump($contents);
			for( $j=0; $j<count($contents); $j++){
				if($j==0) {
					$content_arr[$senders[$j]] = ltrim($contents[$j], "TMSALT");
				}
				else
				$content_arr[$senders[$j]] = $contents[$j];

				if($msg_types[$j] == "STICKER"){
					$content_arr[$senders[$j]] = "You have received a sticker.";
				}
			}
			/*var_dump($senders);
			 var_dump($content_arr); die;
			 */
			$subjects[$col['receiver_id']] = ($col['count'] == 1)?"you have 1 new message!":"you have " .$col['count']." new messages!";
				
			$tileData = $uu->getMailerTile($senders, $col['receiver_id'], $campaign);
				
			foreach ($tileData as $key => $value){
				$tileData[$key]['timestamp'] = Utils::GMTTOIST($timeArr[$key]);
				$tileData[$key]['msg']= $content_arr[$key];
			}
			$smartydata[$col['receiver_id']]['data'] = $tileData;
			$smartydata[$col['receiver_id']]['msg_count'] = $col['count'];
			if(count($col['count']) == 1 && $senders[0] == $admin_id) $smartydata[$col['receiver_id']]["noWoohoo"] = 1;
				
		}

		//	var_dump($smartydata); die;
		//var_dump($idsToMail);
		if(count($idsToMail) >0 ){
			$sql2 = "SELECT fname, email_id, user_id,email_status from user where status = 'authentic' and user_id in (". implode(',', $idsToMail). ')';
			$res2 = $conn_slave->Execute($sql2);
			$basicinfo =$res2->GetRows();

			//$basicinfo = Query::SELECT($sql2, null);
			$info = null;
			foreach ($basicinfo as $val){
				$info[$val['user_id']] = $val;
			}
		}

		for($i=0; $i<count($idsToMail) ;$i++){
			if($idsToMail[$i] == $admin_id) continue;

			//if info comes from above query only then run the below
			if(isset($info[$idsToMail[$i]]) && $info[$idsToMail[$i]]['email_status'] == null){
				$name = ucfirst($info[$idsToMail[$i]]['fname']);
				$smarty->assign("data", $smartydata[$idsToMail[$i]]);
				$smarty->assign("name", $name);

				$toAddress = $info[$idsToMail[$i]]['email_id'];
				if(isset($external_email))
				$toAddress = $external_email;

					
				$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $idsToMail[$i]);
				$smarty->assign("analyticsLinks", $analyticsLinks);
				$mailObject = new MailFunctions();
				//	$smarty->display('../templates/utilities/emailMsgs.tpl');die;

				$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/newmailers/emailMsgs.tpl'),"",$name . ", ".$subjects[$idsToMail[$i]], $idsToMail[$i]);
				$loggerData = null;
				$loggerData['email_id'] = $toAddress;
				$loggerData['subject'] = "messages";
				$loggerData['user_id'] = $idsToMail[$i];
				$sysLog->logSystemMail($loggerData);
					
			}
		}
	}
	catch(Exception $e){
		trigger_error($e->getMessage());
		echo $e->getMessage();
	}


}

try{
	global $baseurl;
	$options=getopt ( "e:" );
	sendMailer($options[e]);
	//Utils::sendEmail(Utils::$techTeam, $baseurl, $baseurl. " - mailed Messages for ". Utils::GMTTOISTfromUnixTimestamp(time()), "this hour mail sent for messages" );

}
catch (Exception $e){
	global $baseurl;
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = $baseurl. " - Messages Mailer failed";
	$from = gethostname();

	Utils::sendEmail($to, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Messages Mailer failed". $e->getTraceAsString(), E_USER_WARNING );

}

?>