<?php 
/*
 * @uthor: Arpan Jain
 * valar morghulis
 * script for reporting of bad words in conversation of previous day
 */

require_once  dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";



function scan_msgs_for_phone_exchange()
{
	global $smarty,$conn_reporting;
	$phn_exchange=array();
	
	$sql1="select sender_id, receiver_id, msg_content, tStamp  from current_messages_new 
		where msg_type='TEXT' and tStamp>date_sub(now(), interval 1 hour) and msg_content regexp '[0-9]{10}'";
	$result= $conn_reporting->Execute($sql1);
	$i=0;
	while ($data=$result->FetchRow())
	{
		$sql2="SELECT count(*) as count FROM current_messages_new WHERE sender_id=? and receiver_id=? and msg_type='TEXT'";
		$result2=$conn_reporting->Execute($sql2,array($data['sender_id'],$data['receiver_id']));
		$msg_count=$result2->FetchRow();
		if($msg_count['count']<=4)
		{
			$phn_exchange[$i]['sender_id']=$data['sender_id'];
			$phn_exchange[$i]['receiver_id']=$data['receiver_id'];
			$phn_exchange[$i]['msg_content']=$data['msg_content'];
			$phn_exchange[$i]['tStamp']=$data['tStamp'];
			$i++;
		}
		
	}
	//var_dump($phn_exchange);
	$smarty->assign("phnexchanged_lasthour", $phn_exchange);
}

function sendMail(){
	global $smarty,$emailIdsForReporting;

    $dateTime=date_create("now",timezone_open("Asia/Calcutta"));  
   

	$subject = "Data for phone number exchanged (for the last hour): ". $dateTime->format('Y-m-d H:i:s');
	 
	$to=$emailIdsForReporting['phone_number_exchanged'];
	//$to ="arpan@trulymadly.com" ;
	$message = $smarty->Fetch(dirname ( __FILE__ ).'/../templates/reporting/phone_exchanged.tpl') ;
	Utils::sendEmail( $to , "admintm@trulymadly.com", $subject, $message , TRUE);
}

try {
	scan_msgs_for_phone_exchange();
	sendMail();
	

} catch (Exception $e) {
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}

?>