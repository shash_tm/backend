<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
include_once dirname ( __FILE__ ) . '/../../photomodule/photoUtils.php';


class QuizPhotos
{
    private $conn_master ;
    private $conn_slave ;
    private $imageurl;
    private $image_basedir;
    private $cdnurl;
    function __construct()
    {
        global $conn_slave, $conn_master,$imageurl,$image_basedir, $cdnurl ;
        $this->conn_master = $conn_master ;
        $this->conn_slave = $conn_slave ;
        $this->conn_slave->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->imageurl = $imageurl ;
        $this->image_basedir = $image_basedir ;
        $this->cdnurl = $cdnurl ;
    }

    function performAction()
    {
       // image and banner photos
       $banner_images = $this->getBannerPhotos();
        foreach($banner_images as $val)
        {
            $this->cdnToS3( dirname ( __FILE__ ) . "/../../images/quiz/" . $val['image'] , "quiz/" . $val['image']);
            $this->cdnToS3( dirname ( __FILE__ ) . "/../../images/quiz/" . $val['banner'] , "quiz/" . $val['banner']);
        }

        $option_images = $this->getOptionImages();
        foreach($option_images as $val)
        {
            try
            {
                $this->cdnToS3( dirname ( __FILE__ ) . "/../../images/quiz/options/" . $val, "quiz/" . $val);
            }
            catch(exception $e)
            {
                echo $e->getMessage();
            }

        }
    }

    private function getBannerPhotos()
    {
        $sql = "SELECT quiz_id, banner, image from quiz";
        $banner_rs = $this->conn_slave->Execute($sql);
        $images = $banner_rs->GetRows();
        return $images;
    }

    private function cdnToS3($imageLocation, $s3Url)
    {
        $imagepath = "files/images/profiles/";
        if(file_exists($imageLocation))
        {
            Utils::saveOnS3($imageLocation , $imagepath. $s3Url);
        }
    }

    private function getOptionImages()
    {
         $target_dir = dirname ( __FILE__ ) . "/../../images/quiz/options/";
        $allFiles = scandir($target_dir) ;
        $files = array_diff($allFiles, array('.', '..'));
         return $files;

    }

}

try
{
    if(php_sapi_name() == 'cli')
    {
        $quizPhotos = new QuizPhotos();
        $quizPhotos->performAction();
        echo "Photos Ported";
    }

}
catch (exception $e)
{
    echo $e->getMessage();
}




?>