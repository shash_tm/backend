<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../analytics/sourceOfInstall.php";


function getDevices($start,$end){
	global $conn_reporting,$conn_master;
	$sql = "select device_id, event_info, tstamp from action_log_new where activity='install' and event_type='install_referrer' and 
	tstamp>'$start' and tstamp<='$end'"; 
	echo "running query";
	$res = $conn_reporting->Execute($sql);
	echo 'Query Ran';
	if($res->RowCount()>0){
		while ($row = $res->FetchRow()){
		 $utm_source= sourceOfInstall::parseReferrerUrl($row['event_info'],"utm_source"); 
		 $arr[] = array ($row['device_id'], $utm_source, $row['event_info'],$row['tstamp']);
		}
		}
		$conn_master->bulkBind = true;
		$sql_prepare = $conn_master->Prepare("insert ignore into device_referrer   (device_id, utm_source, referrer, timestamp) values (?,?,?,?)");
		$sql_insert = $conn_master->Execute ( $sql_prepare, $arr);
}

function runforeachday(){
	$isttime= date ( 'Y-m-d H:i:s');
	for($i =0 ; $i <5; $i++){
		$j= $i+1;
     $end_date = date ( 'Y-m-d 00:00:00', strtotime('-'.$i.' day', strtotime($isttime)));	
     $start_date=date('Y-m-d 00:00:00', strtotime('-'.$j.' day', strtotime($isttime)));	
     
     echo "$i Running for $end_date";
     getDevices($start_date, $end_date);
      echo PHP_EOL;
	}
	
}

runforeachday();

?>