<?php 

require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";

$unreadCount = array();


function getStart() {
	global $conn;
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	// 1966,81144,1026,16919,2136,707,17266
	
	$counter1 = 0;
	$rs = $conn->Execute($conn->prepare("select user_id from user where status in ('authentic', 'suspended') and user_id != 19632"));
	if ($rs){
		while ($arr = $rs->FetchRow()) {
			$user_id = $arr["user_id"];
			echo $counter1." ".$user_id;
			$counter1++;
			getUnreadCount($user_id);
		}
	}
}


function getUnreadCount($user_id) {
	
	global $conn,$unreadCount;
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$rs = $conn->Execute($conn->prepare("select sender_id,receiver_id,last_seen,tStamp from 
			user left join messages_queue on user_id=sender_id where last_seen<tStamp and blocked_by is NULL 
			and receiver_id=$user_id and sender_id!=19632 and user.status not in('blocked') group by sender_id,receiver_id"));
	
	$data = $rs->GetRows();
	
	$receiver_arr = $data;
	
	$sql = "select receiver_id,msg_type,tStamp from messages_queue where
		 blocked_by is NULL and receiver_id!=19632 and sender_id=$user_id";
	
	$rs = $conn->Execute($conn->prepare($sql));
	
	$sender_arr = $rs->GetRows();
	
	$result = array();
	
	for($i=0; $i<count($receiver_arr); $i++) {
		$hasFound = false;
		for($j=0; $j<count($sender_arr); $j++) {
			if($sender_arr[$j]['receiver_id'] == $receiver_arr[$i]['sender_id']) {
				$hasFound = true;
				if($sender_arr[$j]['tStamp'] < $receiver_arr[$i]['tStamp']) {
					$result[] = $sender_arr[$j]['receiver_id'];
					break;
				}
				else if($receiver_arr[$i]['last_seen'] == '0000-00-00 00:00:00' && $sender_arr[$j]['msg_type'] == 'MUTUAL_LIKE') {
					$result[] = $sender_arr[$j]['receiver_id'];
					break;
				}
			}	
		}
		if($hasFound == false) {
			$result[] = $receiver_arr[$i]['sender_id'];
		}
	}
	$result = array_values(array_unique($result));
	
	var_dump($result);
	
	setToRedis($user_id,$result);
}

function setToRedis($user_id,$data) {
	global $redis;
	$key = Utils::$redis_keys['conversation_list_union'].$user_id;
	echo "redis changes \n";
	$redis->DEL($key);
	
	for($i=0; $i<count($data); $i++) {
		$redis->SADD($key , $data[$i]);
	}
}

try {
	if(php_sapi_name() == 'cli') {
		getStart();
	}
} catch (Exception $e) {
	trigger_error("PHP WEB: Error: " . $e->getMessage() . "  trace: " .$e->getTraceAsString(), E_USER_WARNING);
}
			
?>