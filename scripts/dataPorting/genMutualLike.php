<?php

require_once dirname ( __FILE__ ) . "/../../include/config.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";



function MutualLikes(){

	$sql = "select ul.user1, ul.user2, ul.timestamp from user_likeMe ul
			JOIN user_likeMe ulm on ulm.user1 = ul.user2 and ulm.user2 =  ul.user1 
			order by ul.user1, ul.timestamp desc";
//	$res = $conn->Execute($sql);
	$rows = Query::SELECT($sql, null, Query::$__master, false);

	echo $sql;
	$arr = array();
	$insertSql = "INSERT ignore INTO user_mutual_like (user1, user2, timestamp) values(?,?,?)";


	foreach ($rows as $val){
		if($val['user1'] < $val['user2'])
		$arr[] = array($val['user1'], $val['user2'], $val['timestamp']);
		else
		$arr[] = array($val['user2'], $val['user1'], $val['timestamp']);
	}

	//var_dump(count($arr));die;
	if(!empty($arr)){
		Query::BulkINSERT($insertSql, $arr);
	}

}


function msgToMutual(){
	$sql = "select sender_id, receiver_id, tStamp from  messages_queue";
	$res = Query::SELECT($sql, null, Query::$__master, false);
	
	echo $sql;
	$arr = array();
	$insertSql = "INSERT ignore INTO user_mutual_like (user1, user2, timestamp) values(?,?,?)";
	
	foreach ($res as $row => $val){
		if($val['sender_id'] < $val['receiver_id'])
		$arr[] = array($val['sender_id'], $val['receiver_id'], $val['tStamp']);
		else
		$arr[] = array($val['receiver_id'], $val['sender_id'], $val['tStamp']);
	}
	Query::BulkINSERT($insertSql, $arr);
	
	$arr2 = array();
	$i=0;
	$insertLikeSql = "INSERT ignore INTO user_like (user1, user2, timestamp) values(?,?,?)";
	foreach ($res as $row=>$val){
		$arr2[$i]= array($val['sender_id'], $val['receiver_id'], $val['tStamp']);
		$i++;
		$arr2[$i] = array($val['receiver_id'], $val['sender_id'], $val['tStamp']);
		$i++;
	}

	Query::BulkINSERT($insertLikeSql, $arr2);
}



function updateMessageBlockedUser(){
	
	$msgSql = "select sender_id, receiver_id from  messages_queue mq  join user_reject ur on mq.sender_id = ur.user1 and mq.receiver_id = ur.user2";
	$msgRows = Query::SELECT($msgSql, null, Query::$__master, false);
	var_dump($msgRows);
	$updateSql1 = "UPDATE messages_queue set is_blocked_shown =? , blocked_by =? where ((sender_id =? AND receiver_id=?) OR (sender_id = ? AND receiver_id=?))";
	foreach ($msgRows as $row => $val){
		//if($val['sender_id'] < $val['receiver_id'])
		$arr[] = array(1,$val['sender_id'],$val['sender_id'], $val['receiver_id'],  $val['receiver_id'],$val['sender_id']);
	}
	
	Query::BulkINSERT($updateSql1, $arr);
	
	
	$updateSql2 = "UPDATE user_mutual_like set blocked_by =?, blocked_shown =? where user1 =? AND user2 =?";
	foreach ($msgRows as $row => $val){
		if($val['sender_id'] < $val['receiver_id'])
		$arr[] = array($val['sender_id'], 1, $val['sender_id'], $val['receiver_id']);
		else
		$arr[] = array($val['receiver_id'],1,$val['receiver_id'], $val['sender_id']);
	}
	Query::BulkINSERT($updateSql2, $arr);
}

function updateMutualLikeBlockedUser(){
	$likeSql = "select ul.user1, ul.user2, ul.timestamp from user_likeMe ul JOIN user_likeMe ulm on ulm.user1 = ul.user2 and ulm.user2 =  ul.user1 JOIN user_reject ur on ul.user1 = ur.user1 and ul.user2 = ur.user2 order by ul.user1, ul.timestamp desc";
	$likeRows = Query::SELECT($likeSql, null, Query::$__master, false);
	var_dump($likeRows);
	$updateSql1 = "UPDATE messages_queue set is_blocked_shown =? , blocked_by =? where ((sender_id =? AND receiver_id=?) OR (sender_id = ? AND receiver_id=?))";
	foreach ($likeRows as $row => $val){
		// if($val['user1'] < $val['user2'])
		$arr[] = array(1,$val['user1'],$val['user1'], $val['user2'],  $val['user2'],$val['user1']);
	}
	
	Query::BulkINSERT($updateSql1, $arr);
	
	
	$updateSql2 = "UPDATE user_mutual_like set blocked_by =?, blocked_shown =? where user1 =? AND user2 =?";
	foreach ($likeRows as $row => $val){
		if($val['user1'] < $val['user2'])
		$arr[] = array($val['user1'], 1, $val['user1'], $val['user2']);
		else
		$arr[] = array($val['user1'],1,$val['user2'], $val['user1']);
	}
	Query::BulkINSERT($updateSql2, $arr);
}



try{
	MutualLikes();
	msgToMutual();
	updateMessageBlockedUser();
	updateMutualLikeBlockedUser();
}

catch(Exception $e){
	echo $e->getMessage();
}