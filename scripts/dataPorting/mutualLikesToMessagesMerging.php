<?php

require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../abstraction/query.php";



/**
 * updating the message queue table from mutual like table ignoring the existing entries
 * @author Himanshu
 *
 */

function updateMessageQFromML()
{

	global $conn_master, $conn_slave;
	$mSql = "SELECT user1, user2, timestamp from user_mutual_like where blocked_by is null";
	$resultSelect = $conn_slave->Execute($mSql);

	$arr = array();

	while ($val = $resultSelect->FetchRow())
	{
		$arr [] = array ($val['user1'],$val['user2'],NULL, $val['timestamp'],$val['user2'], $val['user1'], NULL, $val['timestamp']);
	}

	/*	foreach ($res as $val)
	 {
		$arr [] = array ($val['user1'],$val['user2'],$val['timestamp'], $val['user2'], $val['user1'], $val['timestamp']);
		}
		*/

	$conn_master->bulkBind = true;
	$inSql = $conn_master->Prepare("INSERT IGNORE INTO messages_queue (sender_id,receiver_id,last_seen, tStamp) VALUES (?,?,?,?), (?,?,?,?)");
	$conn_master->Execute ($inSql, $arr);

}

function updateRedisKey()
{

	global $redis;
	$keys=$redis->keys('mCnt:*');
	foreach($keys as $row){
		$values=explode(":",$row);
		if(count($values)==2){
			var_dump($values); 
			$id="conversation_list_union:".$values[1];
			
			$redis->sUnionStore($id,$row);
			//exit;
		}
	}
	//var_dump($keys);
	

}

function removeDuplicateValue() {
	global $redis;
	$keys = $redis->keys('conversation_list_union:*');
	foreach ($keys as $row) {
		$values=explode(":",$row);
		if(count($values)==2) {
			$key = "conversation_list_union:".$values[1];
			if($redis->sismember($key,$values[1]) == 1) {
				echo $key."\n";
				$redis->SREM($key,$values[1]);
			}
		}
	}
}


try{
	if ( php_sapi_name() == 'cli' )
	{
		//updateRedisKey();
		echo "running the sql update";
		updateMessageQFromML();
		//removeDuplicateValue();
	}
}
catch(Exception $e)
{
	echo $e->getMessage();
}
?>
