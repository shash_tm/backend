<?php


require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
require_once  dirname ( __FILE__ ) . "/../include/config.php";
require_once  dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../logging/UnsubscriptionClass.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query.php";



/**
 * @Himanshu
 * Mailer for mutual Like, Likeback received- common hashtags 
 */

class mutualLikeMailer{

	private $user_id;
	private $match_id;


	function __construct($user_id, $match_id){
		$this->user_id = $user_id;
		$this->match_id = $match_id;
	}

	public function sendMailer(){

		try{
			global  $smarty,  $baseurl;
			$sysLog = new SystemLogger();

			$ifHashTagMatches = false;
			
			

			$smartydata = null;
			$uu = new UserUtils();

			$sql2 = "SELECT fname, email_id, user_id,email_status from user where user_id = ?";
			$basicinfo = Query::SELECT($sql2, array($this->match_id));

			$hashTags = $uu->getHashTagsIntersection($this->user_id, array($this->match_id));
			if(isset($hashTags[$this->match_id]))
			$ifHashTagMatches = count($hashTags[$this->match_id])>0 ? true : false;
			
			if($ifHashTagMatches)
			{
				$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=mutual_like_with_common_hashTags";
				$redirectionFile = "/msg/messages.php";
				$utm_content="view_mutual_like_with_common_hashTags";
				$campaignName = "mutual_like_with_common_hashTags";
			}
			else {
				$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=mutual_like_without_common_hashTags";
				$redirectionFile = "/msg/messages.php";
				$utm_content="view_mutual_like_without_common_hashTags";
				$campaignName = "mutual_like_without_common_hashTags";
			}
			

			$smartydata = $uu->getMailerTile(array($this->user_id), $this->match_id, $campaign);
			
			
			if($basicinfo[0]['email_status']  == null){
				$time = time();
				$ts = Utils::getDateGMTTOISTfromUnixTimestamp($time);
 				$smarty->assign("data", $smartydata);
				$smarty->assign("name", ucfirst($basicinfo[0]['fname']));
				$smarty->assign("matchName", $smartydata[$this->user_id]['fname']);
				$smarty->assign("ifHashTagMatches", $ifHashTagMatches);
				$smarty->assign("hashTags",$hashTags[$this->match_id]);
				$smarty->assign("ts", $ts);
				$toAddress = $basicinfo[0]['email_id'];
					
				$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $this->match_id);
				$smarty->assign("analyticsLinks", $analyticsLinks);
				$mailObject = new MailFunctions();
				//	$smarty->display('../templates/utilities/emailMsgs.tpl');die;

				$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/newmailers/emailMutMatch.tpl'),"","Hurray, ". ucfirst($smartydata[$this->user_id]['fname'])." likes you too!", $idsToMail[$i]);
					
				$data['email_id'] = $basicinfo[0]['email_id'];
				$data['subject'] = $campaignName;
				$data['user_id'] = $this->match_id;
				$sysLog->logSystemMail($data);
					
			}
		}
		catch(Exception $e){
				
			//echo $e->getMessage();
			trigger_error("PHP WEB:" . $e->getTraceAsString(), E_USER_WARNING);
		}


	}

}

?>