<?php
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ )  . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../msg/MessageFullConversation.class.php";
require_once dirname ( __FILE__ ) . "/../../mobile_utilities/pushNotification.php";


class SparkTutorialVideo
{
    private $conn_reporting;
    private $user_id ;
    private $messageFullObj ;
    private $message ;
    private $misstm ;
    private $gender ;
    private $notify;
    private $uu ;
    private $messageId ;
    function __construct()
    {
        global $conn_reporting, $miss_tm_id ;
        $this->conn_reporting = $conn_reporting ;
        $this->user_id ;
        $this->messageFullObj ;
        $this->message ;
        $this->imageMessage;
        $this->misstm= $miss_tm_id;
        $this->gender ;
        $this->notify = new pushNotification();
        $this->uu = new UserUtils();
        $this->messageId;
    }

    private function sendMessage()
    {
        $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
        $this->messageId=  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
        //  $msg_id =  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );

    }

    private function getMessage($target)
    {
       if($target == 'all')
       {
           if($this->gender == 'F' )
           {
               $this->message = "Check out SPARK, our newest feature that's got everyone talking! ".
                                "Take a look and try it today! https://www.youtube.com/watch?v=-ko_Jktjr8Q";
           }
           else
           {
               $this->message = "Now the wait for a Like back is over! Check out SPARK, our newest feature that's got everyone talking! ".
                                "Take a look and try it today! https://www.youtube.com/watch?v=FJdFH4MFvPs";
            }
       }
        else
        {
            if($this->gender == 'F' )
            {
                $this->message = "You couldn't have joined at a better time. Check out SPARK, our newest feature that's got everyone talking! ".
                                "Take a look and try it today! https://www.youtube.com/watch?v=-ko_Jktjr8Q";
            }
            else
            {
                $this->message = "You couldn't have joined at a better time. Check out SPARK, our newest feature that's got everyone talking! ".
                                 "Take a look and try it today! https://www.youtube.com/watch?v=FJdFH4MFvPs";
            }
        }
    }



    private function createMessageConvoObj()
    {
        $this->messageFullObj = new MessageFullConversation ( $this->misstm, $this->user_id );
    }
    private function sendPushNotification()
    {
        $message_url = $this->uu->generateMessageLink($this->user_id,$this->misstm);
        $push_arr =  array("is_admin_set"=> 0,
            "content_text"=>"Now wait for the likeback is over",
            "ticker_text"=>"Message From Miss TM",
            "title_text"=>"TrulyMaldy",
            "message_url"=>$message_url,
            "match_id"=>$this->user_id,
            "msg_type" => "TEXT",
            "event_status" => "Spark_tutorial_video",
            "msg_id"=> $this->messageId,
            "push_type"=>"MESSAGE");
        $this->notify->notify($this->user_id, $push_arr, $this->misstm);
    }

    public function getUsersAndSend($target)
    {
        $isttime= date ( 'Y-m-d H:i:s');
        $oneday = date ( 'Y-m-d 00:00:00' ,strtotime ('-0 day',strtotime($isttime) ) );
        $twoday = date ( 'Y-m-d 00:00:00' ,strtotime ('-1 day',strtotime($isttime) ) );
        if($target == 'all')
        {
            $sql="select distinct(u.user_id), u.gender from user_data ud join user u  on u.user_id = ud.user_id  join messages_queue mq on mq.sender_id = u.user_id
                       join user_app_status uas on uas.user_id = u.user_id
                        where  mq.receiver_id = $this->misstm and mq.blocked_by is null and uas.android_app ='install' and u.status='authentic' and u.registered_at<'$twoday'";
        } else
        {
            $sql="select u.user_id, u.gender from user_data ud join user u  on u.user_id = ud.user_id  join messages_queue mq on mq.sender_id = u.user_id
                  where  mq.receiver_id = $this->misstm and mq.blocked_by is null and u.status='authentic' and  u.registered_at >= '$twoday' and  u.registered_at < '$oneday' ";
        }


        $stmt=$this->conn_reporting->Prepare($sql);
        $exe=$this->conn_reporting->Execute($stmt,array());
        $i=0;
        while($row=$exe->fetchRow())
        {
            $this->gender = $row['gender'] ;
            $this->user_id = $row['user_id'] ;
            //    echo "user_id :  " .$this->user_id .PHP_EOL;
            $this->createMessageConvoObj() ;
            $this->getMessage($target) ;
            $this->sendMessage() ;
            $this->sendPushNotification();
            $i++;
        }
        echo "Sent to $i users";
        return $i ;
    }

}


try
{
    if (php_sapi_name() == 'cli')
    {
        $target = $argv[1];
        $message_obj = New SparkTutorialVideo() ;
        $count = $message_obj->getUsersAndSend($target);
        Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com','Spark Tutorial Video Link Sent from Miss TM',"Message sent to $count users");
    }

}
catch (Exception $e)
{
   trigger_error("Error in Sending Spark Tutorial Video : ". $e->getMessage(),E_USER_WARNING);
    Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com','Error in Sending Spark Tutorial Video',$e->getMessage());
}

?>
