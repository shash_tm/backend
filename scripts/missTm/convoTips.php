<?php
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ )  . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../msg/MessageFullConversation.class.php";
require_once dirname ( __FILE__ ) . "/../../mobile_utilities/pushNotification.php";


class MissTMConvoTips
{
    private $conn_reporting;
    private $user_id ;
    private $messageFullObj ;
    private $message ;
    private $misstm ;
    private $gender ;
    private $notify;
    private $uu ;
    private $messageId ;
    private $isttime;
    private $oneday;
    private $twoday;
    private $oneMonth;
    private $maleTipsCount;
    private $femaleTipsCount;
    function __construct()
    {
        global $conn_reporting, $miss_tm_id ;
        $this->conn_reporting = $conn_reporting ;
        $this->user_id ;
        $this->messageFullObj ;
        $this->message ;
        $this->misstm= $miss_tm_id;
        $this->gender ;
        $this->notify = new pushNotification();
        $this->uu = new UserUtils();
        $this->messageId;
        $this->isttime= date ( 'Y-m-d H:i:s');
        $this->oneday = date ( 'Y-m-d 00:00:00' ,strtotime ('-0 day',strtotime($this->isttime) ) );
        $this->twoday = date ( 'Y-m-d 00:00:00' ,strtotime ('-1 day',strtotime($this->isttime) ) );
        $this->oneMonth = date ( 'Y-m-d 00:00:00' ,strtotime ('-30 day',strtotime($this->isttime) ) );
        $this->maleTipsCount = count(MessageConstants::$miss_tm_convo_tips_male) ;
        $this->femaleTipsCount = count(MessageConstants::$miss_tm_convo_tips_male) ;
    }

    private function sendMessage()
    {
        $unique_id =1000*microtime(true)."_".str_pad ( rand ( 0, pow ( 10, 5 ) - 1 ), 5, '0', STR_PAD_LEFT ) ;
        $this->messageId=  $this->messageFullObj->storeMostRecentChat ( $this->message, 'TEXT', date ( 'Y-m-d H:i:s', time () + 1 ), $unique_id, null );
    }

    private function getMessage($tips_count)
    {
    	if($this->gender == 'M')
    	{
    		$this->message = MessageConstants::$miss_tm_convo_tips_male[$tips_count+1];
    		$total_tips_count= count(MessageConstants::$miss_tm_convo_tips_male);
    	}
    	else
    	{
    		$this->message = MessageConstants::$miss_tm_convo_tips_female[$tips_count+1];
    		$total_tips_count= count(MessageConstants::$miss_tm_convo_tips_female);
    	} 
    		
    	if($tips_count % 2 == 1 && $tips_count+1 != $total_tips_count )
    		$this->message .= "\n To opt out of tips by Miss TM text back STOPTIP"; 
    }



    private function createMessageConvoObj()
    {
        $this->messageFullObj = new MessageFullConversation ( $this->misstm, $this->user_id );
    }
    private function sendPushNotification()
    {
        $message_url = $this->uu->generateMessageLink($this->user_id,$this->misstm);
        $push_arr =  array("is_admin_set"=> 0,
            "content_text"=>$this->message,
            "ticker_text"=>"Message From Miss TM",
            "title_text"=>"TrulyMadly",
            "message_url"=>$message_url,
            "match_id"=>$this->user_id,
            "msg_type" => "TEXT",
            "event_status" => "CONVO_TIPS",
            "msg_id"=> $this->messageId,
            "push_type"=>"MESSAGE");
        $this->notify->notify($this->user_id, $push_arr, $this->misstm);
    }
    
    
    public function sendConvoTips()
    {
    	$sql = "select uct.user_id, tips_count,u.gender from user_convo_tips uct join user u on u.user_id = uct.user_id
				join messages_queue mq on u.user_id =mq.receiver_id 
				WHERE mq.sender_id = $this->misstm and mq.blocked_by is null and uct.status='active' and u.status='authentic'
    			and ((u.gender= 'M' and tips_count < $this->maleTipsCount) or (u.gender= 'F' and tips_count < $this->femaleTipsCount))
    			and last_sent_tstamp < '$this->oneday'";
    	$sql_prep = $this->conn_reporting->Prepare($sql);
    	$sql_exec = $this->conn_reporting->Execute($sql_prep);
    	$i = 0 ;
        $chunkCounter = 0 ;
    	while($row = $sql_exec->FetchRow())
    	{
    		$this->gender = $row['gender'] ;
    		$this->user_id = $row['user_id'] ;
    		$tips_count = $row['tips_count'] ;
    		   echo "user_id : $this->user_id Tip No - $tips_count ".PHP_EOL;
    		$this->createMessageConvoObj() ;
    		$this->getMessage($tips_count) ;
    		$this->sendMessage() ;
    		$this->sendPushNotification();
    		$this->updateTipsSentStatus();
    		$i++;
            $chunkCounter++;
            if($chunkCounter >= 1000)
            {
                sleep(100);
                echo "sleeping for 100 seconds";
                $chunkCounter =0;
            }
    	}
    	echo "convo tip sent to $i users ".PHP_EOL ;
    	return $i ;
    }
    
    private function updateTipsSentStatus()
    {
    	$sql = "UPDATE user_convo_tips SET tips_count = tips_count+1, last_sent_tstamp = now() WHERE user_id = ?";
    	Query_Wrapper::INSERT($sql, array($this->user_id),'user_convo_tips');
    }

    public function addUsersToTable()
    {
  
            $sql="select distinct(u.user_id), u.gender from user_data ud join user u  on u.user_id = ud.user_id 
					join messages_queue mq on mq.sender_id = u.user_id
					join user_lastlogin ull on u.user_id = ull.user_id 
					join (select distinct(sender_id) from messages_queue  where receiver_id  not in ($this->misstm,1615723) 
					and receiver_id not in (select user_id from advertisement_campaign)
					and blocked_by is null)ms on u.user_id = ms.sender_id
					left join user_convo_tips uct on uct.user_id = u.user_id 
					where  mq.receiver_id = $this->misstm and mq.blocked_by is null  and ull.last_login >'$this->oneMonth'
					and uct.user_id is null and u.status='authentic'";
		 
        $stmt=$this->conn_reporting->Prepare($sql);
        $exe=$this->conn_reporting->Execute($stmt,array());
        $sql_insert = "INSERT IGNORE INTO user_convo_tips (user_id, status) values (? , 'active')" ;
        $i=0;
        while($row=$exe->fetchRow())
        {
        	$user_id = $row['user_id'];
           Query_Wrapper::INSERT($sql_insert, array($user_id),'user_convo_tips');
           $i++ ; 
        }
        echo "$i users added to table".PHP_EOL;
        return $i ;
    }

}


try
{
    if (php_sapi_name() == 'cli')
    {
        $message_obj = New MissTMConvoTips();
        $message_obj->addUsersToTable();
        $message_obj->sendConvoTips();
        Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com','Spark Tutorial Video Link Sent from Miss TM',"Message sent to $count users");
    }

}
catch (Exception $e)
{
   trigger_error("Error in Miss TM convo Tips ". $e->getMessage(),E_USER_WARNING);
  // Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com','Error in Miss TM convo Tips',$e->getMessage());
}

?>
