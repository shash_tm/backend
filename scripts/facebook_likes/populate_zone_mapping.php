<?php

require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";

$file = fopen("state_zone_mapping_data.csv", "r");
while (!feof($file)) {
	$line = fgets($file);
	$words = explode(',', trim($line));
	$sql = 'update geo_state set zone_id = ? where state_id = ?';
	Query_Wrapper::update($sql, array($words[3], $words[0]), "geo_state");
}
fclose($file);


