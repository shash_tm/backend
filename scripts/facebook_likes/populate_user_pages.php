<?php

if ($argv[1] == 1)
	$conn = new mysqli("172.31.28.235", "slave_access", "slavenewacessTMLukin123#", "trulymadly_prod");
else
	$conn = new mysqli("localhost", "root", "Lukin123", "trulymadly_prod");
if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);

$dev_conn = new mysqli("localhost", "root", "Lukin123", "fb_likes_prod");
if ($dev_conn->connect_error) die("Connection failed on dev: " . $dev_conn->connect_error);

$min_user_id = $argv[2];
$max_user_id = $argv[3];
$result = $conn->query("select user_id, gender, state from user_search where user_id >= " .  $min_user_id . " and user_id <= " . $max_user_id . " order by user_id");
$user_ids = array();
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$user_ids[] = $row["user_id"];
	}
}

$zones_map = array();
$result = $dev_conn->query("select state_id, zone from trulymadly_prod.geo_state where country_id = 113;");
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$zones_map[$row['state_id']] = $row['zone'];
	}
}

$output_file = fopen("/code_git/satish/backend/scripts/facebook_likes/logs/" . $argv[4] . ".txt", "a");
foreach (array_chunk($user_ids, 500) as $slice) {
	$ids = join(', ', $slice);

	$user_zones_map = array();
	$user_genders_map = array();
	$result = $conn->query("select user_id, gender, state from user_search where user_id in ($ids)");
	if ($result->num_rows > 0) {
        	while ($row = $result->fetch_assoc()) {
                	$user_zones_map[$row['user_id']] = isset($zones_map[$row['state']]) ? $zones_map[$row['state']] : 1;
			$user_genders_map[$row['user_id']] = $row['gender'];
        	}
	}
	$result = $conn->query("select user_id, music_favorites, books_favorites, food_favorites, travel_favorites, movies_favorites, other_favorites from interest_hobbies where user_id in ($ids)");
	$page_category_map = array();
	$page_title_map = array();	
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$user_id = $row['user_id'];
			fwrite($output_file, $user_id . "\n");
			$music = (array)json_decode($row['music_favorites'], true);
			$books = (array)json_decode($row['books_favorites'], true);
			$food = (array)json_decode($row['food_favorites'], true);
			$travel = (array)json_decode($row['travel_favorites'], true);
			$movies = (array)json_decode($row['movies_favorites'], true);
			$others = (array)json_decode($row['other_favorites'], true);
			$data = array("music" => $music, "books" => $books, "food" => $food, "travel" => $travel, "movies" => $movies, "others" => $others);
			$likes_ids = array();
			foreach ($data as $c => $cdata) {
				foreach ($cdata as $key => $val) {
					$likes_ids[] = $key;
					$page_category_map[$key] = $c;
					$page_title_map[$key] = $val;
				}
			}
			$sql = array();
			foreach ($likes_ids as $fid) {
				$page_id = $dev_conn->real_escape_string($fid);
				$tm_category = $city = $dev_conn->real_escape_string($page_category_map[$fid]);
				$name = $city = $dev_conn->real_escape_string($page_title_map[$fid]);
				$zone_id = isset($user_zones_map[$user_id]) ? $user_zones_map[$user_id] : 1;
				$gender = isset($user_genders_map[$user_id]) ? $user_genders_map[$user_id] : 'M';
				$sql[] = '(' . $user_id . ',"' . $page_id . '", "' . $tm_category . '", "' . $name . '",' . $zone_id . ',"' . $gender . '")';
			}
			if ($dev_conn->query('INSERT INTO user_facebook_likes (user_id, page_id, category, page, zone, gender) VALUES ' . implode(',', $sql)) === TRUE) {
				echo "Record updated successfully";
			} else {
				echo "Error updating record: " . $dev_conn->error . "\n";
				print "QUERY: " . 'INSERT INTO pages (page_id, tm_category, name, gender) VALUES ' . implode(',', $sql) . "\n\n";
			}
			unset($sql);
		}
	}
	unset($page_category_map);
	unset($page_title_map);
}

$conn->close();
$dev_conn->close();
fclose($output_file);
