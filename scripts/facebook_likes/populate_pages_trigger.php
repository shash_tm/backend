<?php
$min_user_id = $argv[1];
$max_user_id = $argv[2];
$start_time = $min_user_id . "_" . $max_user_id . "_" . time();
$chunk_size = 500;
$chunks = intval(($max_user_id - $min_user_id) / $chunk_size);
for ($i = 0; $i <= $chunks; $i ++) {
	$start = $min_user_id + $i * $chunk_size;
	$end = $start + $chunk_size - 1;
	shell_exec("php populate_pages.php "  . $start . " " . $end . " $start_time");
}

