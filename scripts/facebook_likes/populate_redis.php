<?php

require_once dirname (__FILE__). '/../../include/config.php';
require_once dirname (__FILE__). '/../../include/Utils.php';

global $config;
$redis = new Redis();
$redis->connect($config['search_redis']['ip'], $config['search_redis']['port']);

$conn = new mysqli("localhost", "root", "Lukin123", "fb_likes_prod");
if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);

$page_end_pattern = $argv[1];
// SATISH TODO: remove all absolute paths in all scripts
$output_file = fopen("/code_git/satish/backend/scripts/facebook_likes/logs/" . $argv[2] . ".txt", "a");
$merge_categories_map = array('travel' => 'food');

$result = $conn->query("select page_id, name, tm_category, likes_count from pages where page_id like 'fid:%$page_end_pattern'");
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$page_id = $row['page_id'];
		$category = $row['tm_category'];
		$likes_count = intval($row['likes_count']);
		$likes_count = $likes_count > 0 ? $likes_count - 1 : $likes_count;
		// merge travel and food and store as food
		if (isset($merge_categories_map[$category]))
			$category = $merge_categories_map[$category];
		fwrite($output_file, $page_id . "\n");
		$title = strtolower($row['name']);
		$title = trim(preg_replace('!\s+!', ' ', str_replace(array ("\r", "\t", "\n"), " ", $title)));
		$words = explode(" ", $title);
		for ($i = 0; $i < count($words); $i ++) {
			$key = "";
			for ($j = $i; $j < count($words); $j ++) {
				if (strlen($key) > 0) $key .= " ";
				$key .= $words[$j];
			}
			$key .= "##" . $row['name'] . "##" . $page_id . "##" . $likes_count;
			$redis->zAdd($category . "_pages", 1, $key);
		}
		$last_page = $page_id;
	}
}

$conn->close();
fclose($output_file);
