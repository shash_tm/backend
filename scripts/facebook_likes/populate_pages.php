<?php
require_once dirname ( __FILE__ ) . "/../../abstraction/query_wrapper.php";

function populate_pages($min_user_id, $max_user_id, $output_file) {
	$sql = "select user_id, music_favorites, books_favorites, food_favorites, travel_favorites, movies_favorites, other_favorites
				from interest_hobbies where user_id >= ? and user_id <= ?";
	$rows = Query_Wrapper::SELECT($sql, array($min_user_id, $max_user_id));
	$user_ids_str = "";
	$arr = array();
	$sql = "INSERT INTO facebook_pages (page_id, tm_category, name) values (?, ?, ?) ON DUPLICATE KEY UPDATE name = ?";
	if (count($rows) > 0) {
		foreach ($rows as $row) {
			$user_ids_str .= $row['user_id'] . "\n";
			$music = (array)json_decode($row['music_favorites'], true);
			$books = (array)json_decode($row['books_favorites'], true);
			$food = (array)json_decode($row['food_favorites'], true);
			$travel = (array)json_decode($row['travel_favorites'], true);
			$movies = (array)json_decode($row['movies_favorites'], true);
			$others = (array)json_decode($row['other_favorites'], true);
			$data = array("music" => $music, "books" => $books, "food" => $food, "travel" => $travel, "movies" => $movies, "others" => $others);
			foreach ($data as $c => $cdata) {
				foreach ($cdata as $key => $val) {
					$arr[] = array($key, $c, $val, $val);
				}
			}
		}
		if (!empty($arr))
			Query_Wrapper::BulkINSERT($sql, $arr, 'facebook_pages');
	}
	fwrite($output_file, $user_ids_str);
}

$min_user_id = $argv[1];
$max_user_id = $argv[2];
//$output_file = fopen("script_logs/populate_pages_" . $min_user_id . "_" . $max_user_id . "_" . $argv[3] . ".txt", "a");
$output_file = fopen("script_logs/populate_pages_" . $argv[3] . ".txt", "a");
$chunk_size = 500;
$chunks = intval(($max_user_id - $min_user_id) / $chunk_size);
for ($i = 0; $i <= $chunks; $i ++) {
	$start = $min_user_id + $i * $chunk_size;
	$end = $start + $chunk_size - 1;
	populate_pages($start, $end, $output_file);
}
fclose($output_file);


