<?php

$dev_conn = new mysqli("localhost", "root", "Lukin123", "fb_likes_prod");
if ($dev_conn->connect_error) die("Connection failed on dev: " . $dev_conn->connect_error);

$top_pages_per_zone_size = 200;
$min_likes = 5;
$min_likes_male = 25;
$zone = $argv[1];
$category = $argv[2];

$result = $dev_conn->query("select page_id, count(*) as likes_count from user_facebook_likes where zone = $zone and category = '$category' and gender = 'F' and page_id like 'fid:%' group by page_id order by count(*) desc limit 2000");
$female_top_page_ids = array();
if ($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {
		$female_top_page_ids[$row["page_id"]] = $row["likes_count"];
	}
}

$result = $dev_conn->query("select page_id, count(*) as likes_count from user_facebook_likes where zone = $zone and category = '$category' and gender = 'M' and page_id like 'fid:%' group by page_id order by count(*) desc limit 2000");
$male_top_page_ids = array();
if ($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {
		$male_top_page_ids[$row["page_id"]] = $row["likes_count"];
	}
}

$common_pages = array();
foreach ($female_top_page_ids as $page_id => $val) {
	if (count($common_pages) < $top_pages_per_zone_size && isset($male_top_page_ids[$page_id]) && $val >= $min_likes)
		$common_pages[$page_id] = $val + $male_top_page_ids[$page_id];
}

foreach ($female_top_page_ids as $page_id => $val) {
	if (count($common_pages) < $top_pages_per_zone_size && !isset($common_pages[$page_id]) && $val >= $min_likes)
		$common_pages[$page_id] = $val + (isset($male_top_page_ids[$page_id]) ? $male_top_page_ids[$page_id] : 0);
}

foreach ($male_top_page_ids as $page_id => $val) {
        if (count($common_pages) < $top_pages_per_zone_size && !isset($common_pages[$page_id]) && $val >= $min_likes_male)
                $common_pages[$page_id] = $val;// + (isset($male_top_page_ids[$page_id]) ? $male_top_page_ids[$page_id] : 0);
}


$sql = array();
foreach ($common_pages as $page_id => $val) {
	$pid = $dev_conn->real_escape_string($page_id);
	$sql[] = '(' . $zone . ', "' . $category . '", "' . $pid . '", ' . $val . ')';
}

$dev_conn_insert = new mysqli("localhost", "root", "Lukin123", "fb_likes_prod");
if ($dev_conn_insert->connect_error) die("Connection failed on dev: " . $dev_conn_insert->connect_error);

if ($dev_conn_insert->query('INSERT IGNORE INTO facebook_top_pages (zone, tm_category, page_id, likes_count) VALUES ' . implode(',', $sql)) === TRUE) {
	echo "Record updated successfully";
} else {
	echo "Error updating record: " . $dev_conn_insert->error . "\n";
	print "QUERY: " . 'INSERT IGNORE INTO facebook_top_pages (zone, category, page_id, likes_count) VALUES ' . implode(',', $sql) . "\n\n";
}

$dev_conn->close();
$dev_conn_insert->close();

