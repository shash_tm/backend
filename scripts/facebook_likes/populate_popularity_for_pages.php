<?php

require_once dirname (__FILE__). '/../../include/config.php';
require_once dirname (__FILE__). '/../../include/Utils.php';

global $redis;

$conn = new mysqli("localhost", "root", "Lukin123", "fb_likes_prod");
if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);

$page_end_pattern = $argv[1];
// SATISH TODO: remove all absolute paths in all scripts
$output_file = fopen("/code_git/satish/backend/scripts/facebook_likes/logs/" . $argv[2] . ".txt", "a");

$result = $conn->query("select page_id from pages where page_id like 'fid:%$page_end_pattern'");
$page_ids = array();
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$page_ids[] = $row['page_id'];
	}
}


$insert_conn = new mysqli("localhost", "root", "Lukin123", "fb_likes_prod");
if ($insert_conn->connect_error) die("Connection failed: " . $insert_conn->connect_error);
foreach (array_chunk($page_ids, 500) as $slice) {
	populate_page_like_counts($slice, $conn, $insert_conn);
}


function populate_page_like_counts($page_ids, $conn, $insert_conn) {
	$ids = "";
	foreach ($page_ids as $page_id) {
		$ids .= '"' . $page_id . '",';
	}
	$ids = trim($ids, ",");
	$result = $conn->query("select page_id, count(*) as likes_count from user_facebook_likes where page_id in ($ids) group by page_id");
	$page_like_counts = array();
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$page_like_counts[$row['page_id']] = $row['likes_count'];
		}
	}
	

	foreach ($page_like_counts as $page_id => $count) {
		if ($insert_conn->query("update facebook_pages set likes_count = $count where page_id = '$page_id'")) {
			//echo "Record updated successfully";
		} else {
			//echo "Error updating record: " . $dev_conn->error . "\n";
			//print "QUERY: " . "update pages set likes_count = $count where page_id = $page_id\n";
		}
	}
}


$conn->close();
fclose($output_file);
