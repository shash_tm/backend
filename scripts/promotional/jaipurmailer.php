<?php
require_once dirname ( __FILE__ ) . "/../../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../../include/config_admin.php";
include dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../../logging/systemLogger.php";

/**
 * @Chetan
 * Mailer for Blog Article, Target user blongs to jaipur
 */

// function to run emailer
function sendMailer($external_email = null) {
	try {
		global $config, $conn_reporting, $smarty, $imageurl, $dummy_female_image, $dummy_male_image, $mailSleepTimeSES, $baseurl, $admin_id; // global veriable defined for values required in the emailer
		$sysLog = new SystemLogger (); // for system log date n time
		$uu = new UserUtils (); // for user info
		$mailObject = new MailFunctions ();

		$campaign_subject = "jaipur";
		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=$campaign_subject"; // for emailer tracking
		// $redirectionFile = "/msg/messages.php"; // emailer landing page link, redirecting url
		$utm_content = "view_$campaign_subject"; // tracker
		$campaignName = $campaign_subject; // tracker

		$subject = "Restaurants that" . "'" . "ll make you forget you" . "'" . "re in Jaipur";

		// sql query for fatching data from db for target user
		$sql = "select u.user_id, u.fname, u.email_id from user u join user_data ud on u.user_id = ud.user_id where ud.stay_city = 4060 and u.email_status is null and u.status in ('authentic', 'non-authentic') ";
		$res = $conn_reporting->Execute ( $sql ); // for executing query
		$data = $res->GetRows (); // for fetching data from executed query

		$i=0;
		// var_dump($data); // for print on to console
		foreach ( $data as $val ) {

			$name = ucfirst ( $val ['fname'] );
			$smarty->assign ( "name", $name );

			$toAddress = $val ['email_id'];
			if (isset ( $external_email ))
			$toAddress = $external_email;

			$analyticsLinks = Utils::generateSystemMailerLinks ( $campaignName, $utm_content, $redirectionFile, $toAddress, $val ['email_id'] );
			$smarty->assign ( "analyticsLinks", $analyticsLinks );

			// $smarty->display('../templates/utilities/emailMsgs.tpl');die;

			$mailObject->sendMail ( array (
			$toAddress
			), $smarty->fetch ( dirname ( __FILE__ ) . '/../../templates/utilities/newmailers/EmailerJaipur.tpl' ), "", $subject, $val ['user_id'] );
			// $smarty->fetch ( dirname ( __FILE__ ) . '/../templates/utilities/newmailers/EmailerJaipur.tpl' ), "", $name . ", " . $subject, $val['user_id'] );
			// till here mail is sent
			// now we are logging the same into our system
			$loggerData = null;
			$loggerData ['email_id'] = $toAddress;
			$loggerData ['subject'] = $campaign_subject;
			$loggerData ['user_id'] = $val ['user_id'];
			$sysLog->logSystemMail ( $loggerData );

			$i++;


		}

		$reportLog = array("subject of mailer" => $subject, "count" => $i, "query" => $sql );
		Utils::sendEmail ( "himanshu@trulymadly.com, shashwat@trulymadly.com, chetan@trulymadly.com", "chetan@trulymadly.com", "Mailer Report: " .$subject, json_encode($reportLog));


	} catch ( Exception $e ) {
		trigger_error ( $e->getTraceAsString () );
		echo $e->getMessage ();
	}
}

try {
	global $baseurl;
	$options = getopt ( "e:" );

	//to allow to run from command line only
	if(php_sapi_name() === 'cli'){
		sendMailer ( $options [e] );
	}
	// Utils::sendEmail(Utils::$techTeam, $baseurl, $baseurl. " - mailed Messages for ". Utils::GMTTOISTfromUnixTimestamp(time()), "this hour mail sent for messages" );
} catch ( Exception $e ) {
	global $baseurl;
	$to = "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = $baseurl . " - Jaipur Restaurants Mailer failed";
	$from = gethostname ();

	Utils::sendEmail ( $to, "chetan@trulymadly.com", $subject, $e->getTraceAsString () );
	trigger_error ( "PHP WEB: Jaipur Restaurants Mailer failed" . $e->getTraceAsString (), E_USER_WARNING );
}

?>