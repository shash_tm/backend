<?php
$fptr = fopen("/tmp/incr.txt", 'c');

try{
    if(php_sapi_name() === 'cli'){
	$isLocked  = isScriptRunning($fptr);
	if($isLocked == false){
	    ini_set('max_execution_time', 0);
	    require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
	    require_once  dirname ( __FILE__ ) . "/../../include/aws/S3Upload.php";
	    BackUp();
	}//else {
	//releaseLock($fptr);
	//}
    }
}
catch(Exception $e) {
   global $fptr;
   releaseLock($fptr);
   echo $e->getMessage();
   trigger_error("PHP WEB:". $e->getTraceAsString(), E_USER_WARNING);
}

function releaseLock($fptr) {
    flock($fptr, LOCK_UN);
}

function isScriptRunning() {
    global $fptr,$baseurl;
    if (!flock($fptr, LOCK_EX|LOCK_NB, $wouldblock)) {
	if ($wouldblock) {
	   echo "already locked";
	   $isLocked = 1;
	  // Utils::sendEmail("rajesh.singh@trulymadly.com", "rajesh.singh@trulymadly.com", "Already Locked: incremental backup file is running.", "" );
	}
	else {
	   echo "could not lock";
	}
    }
    else {
	echo "obtain lock";
	$isLocked = 0;
	if(flock($fptr,LOCK_EX) == false){
	     throw new Exception("unable to acquire lock");
	}else{
	     echo "lock acquired";
	}
    }
    return $isLocked;
}

function BackUp() {
    $date = date("H");
	
    if($date=="00") {
	fullBackUp();
    }
    else {
	IncrementalBackUp();
    }
}

function fullBackUp() {
   echo "inside full backup";

   $flush_logs = "mysqladmin -u root -pLukin123 flush-logs";
   $newestLog = "ls -d /var/lib/mysql/bin_logs.?????? | sed 's/^.*\.//' | sort -g | tail -n 1";
   
   $stopmysql = "/etc/init.d/mysqld stop";
   $startmysql = "/etc/init.d/mysqld start";
 
   $file = "/mysql/mysql_tmp/mysql_slave.tar.gz";
   $dump = "tar -zcvf $file /mysql/data_mysql/mysql"; 

   try{
   	$out = array ();
        $output = null;

        echo $flush_logs;
        exec ( $flush_logs, $out, $output );
        if ($output != 0) {
           $out=implode("..",$out );
           throw new Exception ( $out );
        }

	echo $newestLog;
        $newLog = array();
        exec ( $newestLog, $newLog, $output );
        if ($output != 0) {
           $out=implode("..",$newLog );
           throw new Exception ( $newLog );
        }
        echo "new log file";
        echo $newLog[0];

	//stop the mysql
	echo $stopmysql;
        exec ( $stopmysql, $out, $output );
        if ($output != 0) {
           $out=implode("..",$out );
           throw new Exception ( $out );
        }

	//copy the data and upload t S3
	echo $dump;
        exec ( $dump, $out, $output );
        if ($output != 0) {
           $out=implode("..",$out );
           throw new Exception ( $out );
        }
	
	//start the mysql
        echo $startmysql;
        exec ( $startmysql, $out, $output );
        if ($output != 0) {
           $out=implode("..",$out );
           throw new Exception ( $out );
        }

        //purge all binary logs till latest file
        purgeLogs("bin_logs.".$newLog[0]);

        global $fptr;
        releaseLock($fptr);



	$year = date('Y');
        $month = date('m');
        $day = date('d');
	$date = date("d-m-Y-h");

        $dumpSize = formatBytes ( filesize ( $file ) );
	echo "file size is ".$dumpSize." ";
	list($out,$output,$path) = S3Upload::upload($file, "trulymadlybackups/sqlSlaveDumps/$year/$month/$day","mysql_slave_$date.gz");

        if ($output != 0) {
             $out=implode("..",$out )."S3 Path to check: $path";
             throw new Exception ( $out);
        }else {
             exec ("rm $file",$out,$output);
	     Utils::sendEmail("rajesh.singh@trulymadly.com,shashwat@trulymadly.com", "admintm@trulymadly.com", "FullBack up Done. Name is mysql_slave_$date.gz And Size is $dumpSize", "" );
        }
	
   }
   catch(Exception $e){
	//start the mysql
	echo $startmysql;
        exec ( $startmysql, $out, $output );
        if ($output != 0) {
           $out=implode("..",$out );
           throw new Exception ( $out );
        }
	
	global $fptr;
        releaseLock($fptr);
Utils::sendEmail("rajesh.singh@trulymadly.com,shashwat@trulymadly.com", "admintm@trulymadly.com", "Fail - FullBack up Fail", "" );
   }

}

function IncrementalBackUp(){
	
    echo "inside incremental back up";
	
    //$dump = "tar -zcvf $file /tmp/slave_dump.sql";
	
    $newestLog = "ls -d /var/lib/mysql/bin_logs.?????? | sed 's/^.*\.//' | sort -g | tail -n 1";
	
    $listAllFiles = "ls /var/lib/mysql/bin_logs.??????";
	
    try {
	$out = array ();
	$output = null;
		
	echo $newestLog;
	$newLog = array();
	exec ( $newestLog, $newLog, $output );
	if ($output != 0) {
	   $out=implode("..",$newLog );
           throw new Exception ( $newLog );
	}
	echo "new log file";
	echo $newLog[0];
	
	$result = array();
	echo $listAllFiles;
	exec ( $listAllFiles, $result, $output );
	var_dump($result);
		
	if ($output != 0) {
	   $out=implode("..",$out );
	   throw new Exception ( $out );
	}else {
	   $fileArray = array();
	   foreach ($result as $file) {
	      if("/var/lib/mysql/bin_logs.".$newLog[0] !== $file) {
	   	 echo $file;
		 $file1 = explode("/",$file);
		 $dumpSize = formatBytes ( filesize ( $file ) );
		 $fileArray[] = $file1[4].'/'.$dumpSize;
		 uploadToS3($file,$file1[4]);
	      }
	   }
	   purgeLogs("bin_logs.".$newLog[0]);
	}
			
	} 
	catch (Exception $e) {
		
	}	
}

function uploadToS3($file,$tofile) {
	$year = date('Y');
	$month = date('m');
	$day = date('d');
	
	$file1 = "/tmp/$tofile".".tar.gz";
	
	exec ( "tar -zcvf $file1 $file", $out, $output );
	
	list($out,$output,$path) = Utils::saveOnS3($file1, "sqlSlaveDumps/$year/$month/$day/$tofile.tar.gz");
	
	if ($output != 0) {
		$out=implode("..",$out )."S3 Path to check: $path";
		throw new Exception ( $out);
	}else {
	    exec ("rm $file1",$out,$output);
	}
}

function purgeLogs($latestFile) {
     $str1 = "'$latestFile'";
     $sql = '"PURGE BINARY LOGS TO '.$str1.'"';
     $purgeLogs = "mysql -u root -pLukin123 -e "."$sql";
     echo $purgeLogs;
     exec($purgeLogs,$out,$output);
}

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');
    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
    $bytes /= pow(1024, $pow);
    return round($bytes, $precision) . ' ' . $units[$pow];
}
?>
