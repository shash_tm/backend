<?php
if (! empty ( $argc ) && strstr ( $argv [0], basename ( __FILE__ ) )) {
	require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


	function run() {
		//global $conn;
		//var_dump($conn->user);die;
		/*$user = $argv[1];
		$pass = $argv[2];*/
		$options=getopt ( "u:p:h:" );
	//	var_dump($options) ; die;
	
		//$time =time();
		$date = date("d-m-Y-h");
		$date .=" hrs";
		//echo $date;die;
		//$date = date ( "Y-m-d" ). '_'. time();
		//$date = Utils::GMTTOISTfromUnixTimestamp(time());
		//echo $date; die;

		$file = "/tmp/mysql_slave.tar.gz";
		//`rm $file`;
		$raw_dump = "mysqldump -u $options[u] -p$options[p] --all-databases > /tmp/slave_dump.sql";
		//$raw_dump = "mysqldump -u $options[u] -p$options[p] -h $options[h] --all-databases > /tmp/slave_dump.sql";
		$stopmysql = "/etc/init.d/mysqld stop";
		$startmysql = "/etc/init.d/mysqld start";
		$dump = "tar -zcvf $file /tmp/slave_dump.sql";
		$deleteDump = "rm $file";

		try {
			//take dump
			$out = array ();
			$output = null;

			echo $raw_dump ;
			exec ( $raw_dump, $out, $output );
			if ($output != 0) {
				$out=implode("..",$out );
				throw new Exception ( $out );
			}

			//stop the mysql server
			/*		echo $stopmysql ;
			exec ( $stopmysql, $out, $output );
			if ($output != 0) {
			$out=implode("..",$out );
			throw new Exception ( $out );
			}*/

			echo $dump;
			exec ( $dump, $out, $output );
			if ($output != 0) {
				$out=implode("..",$out );
				throw new Exception ($out );
			}
			/*	echo $startmysql;
			 exec ( $startmysql, $out, $output );
			 if ($output != 0) {
				$out=implode("..",$out );
				throw new Exception ($out );
				}*/
			// upload to S3 for each date name
			$dumpSize = formatBytes ( filesize ( $file ) );
			//$date = date ( "Y-m-d", strtotime ( "-1 day" ) );
			//list($out,$output,$path) = Utils::uploadToS3 ( $file, $date . ".gz" );
			list($out,$output,$path) = Utils::saveOnS3($file, "sqlSlaveDumps/mysql_slave_$date.gz");
			if ($output != 0) {
				$out=implode("..",$out )."S3 Path to check: $path";
				throw new Exception ( $out);
			}
			/*if ($uploadRes == false) {
			 $sub = "Report Machine backup to S3 failed.Daily_Stats MaxDate:$maxdateindailystats Date:$date\n Aggregation size:$dumpSize";
			 } else {
			 $sub = "Report Machine backup to S3 success.Daily_Stats MaxDate:$maxdateindailystats Date:$date\n Aggregation size:$dumpSize";
			 }
			 */
				

			//deleting the tar file from /tmp
			exec ( $deleteDump, $out, $output );
			if ($output != 0) {
				$out=implode("..",$out );
				throw new Exception ($out );
			}

			$sub = "Report Machine backup to S3 success.Daily_Stats:  Date:$date\n Aggregation size:$dumpSize";
			echo $sub;
			Utils::sendEmail(Utils::$techTeam, "himanshu@trulymadly.com", "Successful replication of slave db", $sub);
				
		} catch ( Exception $e ) {
			exec ( $startmysql, $out, $output );
			$sub = "Report Machine backup failed.Daily_Stats:  Date:$date\n  Aggregation size:$dumpSize";
//			Utils::sendEmail(Utils::$techTeam, "himanshu@trulymadly.com", "slave dump failed", $sub);
			Utils::sendEmail(Utils::$techTeam, "himanshu@trulymadly.com", "slave dump failed", $sub ."\n Exception: ". $e->getTraceAsString());
			trigger_error("PHP WEB:". $e->getTraceAsString(), E_USER_WARNING);

		}
	}
	function formatBytes($bytes, $precision = 2) {
		$units = array('B', 'KB', 'MB', 'GB', 'TB');
		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);
		$bytes /= pow(1024, $pow);
		return round($bytes, $precision) . ' ' . $units[$pow];
	}
	run();
}

?>
