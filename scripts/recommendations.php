<?php

require_once dirname ( __FILE__ ) . "/../GenerateNewMatches.php";
include dirname ( __FILE__ ) . "/../include/config.php";

/**
 * Cron to generate new match set
 * To be scheduled nightly
 * NOTE: shouldn't be run more than once in a day
 * TODO: check when the unlimited plan expires and stop generating recommendation for that user
 * @Himanshu
 */

function createMemoryTable(){
	global $conn;
	/*$today = date("ymd");
	 $todayTable = 'user_memory_'.$today;*/
	$todayTable = 'user_search_new';
	$dropTodayTable = "DROP TABLE IF EXISTS $todayTable";
	$conn->Execute($dropTodayTable);

	/*$sql = "CREATE TABLE $todayTable
	 ( `user_id` int(11) NOT NULL, `gender` enum('M','F') DEFAULT NULL, `status` enum('incomplete','non-authentic','authentic','blocked') NOT NULL DEFAULT 'incomplete',
	 `last_login` timestamp NOT NULL,
	 `trust_score` int(3) NOT NULL DEFAULT '0',
	 `age` int(3) NOT NULL, `religion` int(11) DEFAULT NULL, `caste` int(11) DEFAULT NULL, `marital_status` varchar(100) DEFAULT NULL,
	 `country` int(11) NOT NULL, `state` int(11) DEFAULT NULL, `city` int(11) DEFAULT NULL,
	 `mother_tongue` varchar(32) NOT NULL, `languagues_known` varchar(128) DEFAULT NULL,
	 `height` int(5) NOT NULL, `skin_tone` varchar(100) NOT NULL,
	 `body_type` varchar(100) NOT NULL, `smoking_status` varchar(100) NOT NULL, `drinking_status` varchar(100) NOT NULL, `food_status` varchar(100) NOT NULL,
	 `family_type` varchar(50) NOT NULL, `family_status` varchar(50) NOT NULL,
	 `working_area` varchar(100) DEFAULT NULL, `industry` varchar(100) DEFAULT NULL, `highest_education` int(3) DEFAULT NULL,
	 `income_start` varchar(50) DEFAULT NULL, `income_end` varchar(50) DEFAULT NULL,
	 PRIMARY KEY (`user_id`), KEY `userId` (`user_id`)) ENGINE=MEMORY ";
	 */

	$sql = "CREATE TABLE $todayTable
			( `user_id` int(11) NOT NULL, `gender` enum('M','F') DEFAULT NULL,
			 `last_login` timestamp NOT NULL, 
			 `trust_score` int(3) NOT NULL DEFAULT '0', 
			 `age` int(3) NOT NULL, `religion` int(11) DEFAULT NULL, `caste` int(11) DEFAULT NULL, `marital_status` varchar(100) DEFAULT NULL,
			 `country` int(11) NOT NULL, `state` int(11) DEFAULT NULL, `city` int(11) DEFAULT NULL,  
			 `mother_tongue` varchar(32) NOT NULL, `languagues_known` varchar(128) DEFAULT NULL, 
			 `height` int(5) NOT NULL, `skin_tone` varchar(100) NOT NULL, 
			 `body_type` varchar(100) NOT NULL, `smoking_status` varchar(100) NOT NULL, `drinking_status` varchar(100) NOT NULL, `food_status` varchar(100) NOT NULL, 
			 `family_type` varchar(50) NOT NULL, `family_status` varchar(50) NOT NULL, 
			 `working_area` varchar(100) DEFAULT NULL, `industry` varchar(100) DEFAULT NULL, `highest_education` int(3) DEFAULT NULL, 
			 `income_start` varchar(50) DEFAULT NULL, `income_end` varchar(50) DEFAULT NULL, KEY `test_index` (gender,country,religion,body_type,age,user_id) 
			 ) ENGINE=MEMORY";

	//echo $sql;
	$conn->Execute($sql);


	/*$massSelectQuery = "SELECT u.user_id, u.gender, u.last_login,
	 uts.trust_score,
	 ud.age, ud.religion, ud.caste, ud.stay_country, ud.stay_state, ud.stay_city, ud.mother_tongue, ud.languages_known,
	 ut.height, ut.skin_tone, ut.body_type, ut.smoking_status, ut.drinking_status, ut.food_status,
	 uf.family_type, uf.family_status,
	 uw.working_area, uw.industry, uw.highest_education, uw.income_start, uw.income_end
	 FROM user u
	 LEFT JOIN  user_trust_score uts ON u.user_id = uts.user_id
	 LEFT JOIN  user_demography ud ON u.user_id = ud.user_id
	 LEFT JOIN  user_trait ut ON u.user_id = ut.user_id
	 LEFT JOIN  user_family uf ON u.user_id = uf.user_id
	 LEFT JOIN  user_work uw ON u.user_id = uw.user_id order by last_login desc";*/

	$massSelectQuery = "SELECT u.user_id, u.gender,  u.last_login,uts.trust_score,
	FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age, ud.religion, ud.caste, ud.marital_status, ud.stay_country, ud.stay_state, ud.stay_city, 
	ud.mother_tongue, ud.languages_known,
	ut.height, ut.skin_tone, ut.body_type, ut.smoking_status, ut.drinking_status, ut.food_status, 
	uf.family_type, uf.family_status, 
	uw.working_area, uw.industry, uw.education, uw.income_start, uw.income_end
	 FROM user u LEFT JOIN user_trust_score uts ON u.user_id = uts.user_id 
	 LEFT JOIN user_demography ud ON u.user_id = ud.user_id 
	 LEFT JOIN user_trait ut ON u.user_id = ut.user_id 
	 LEFT JOIN user_family uf ON u.user_id = uf.user_id 
	 LEFT JOIN user_work uw ON u.user_id = uw.user_id
	 WHERE u.status = 'authentic'  
	 order by last_login desc";

	// echo $massSelectQuery;

	$massInsertQuery = " INSERT INTO $todayTable $massSelectQuery";
	//echo $massInsertQuery;
	$result = $conn->Execute($massInsertQuery);

	$existingTable = 'user_search';
	$oldTable = 'user_search_old';
	var_dump($result);
	if($result!=false){
		$dropTable = "DROP TABLE IF EXISTS $oldTable";
		$conn->Execute($dropTable);
		echo 'dropped';
		$renameQuery = "RENAME TABLE  $existingTable TO $oldTable, $todayTable TO $existingTable ";
		$conn->Execute($renameQuery);
	}

	/*$dayBeforeYesterday = date('ymd' - 2);
	 $$dayBeforeyesterdayTable = 'user_memory_'.$dayBeforeYesterday;
	 $dropdayBeforeyesterdayTable = "DROP TABLE $dayBeforeyesterdayTable";
	 $conn->Execute($dropdayBeforeyesterdayTable);
	 */
	/*$massInsertQuery = " INSERT INTO user_search_himanshu values(
	 $i,'$gender', '$status', '$last_login', $trust, $age, $mother_tongue,
	 $religion, $caste, 113 , null, null , '$languagues_known', $height, '$skin_tone',
	 '$body_type', '$smoke', '$drink', '$food', '$family_type', '$family_status','$workArea', $industry, $ed)";*/

	/*$sql6 = " INSERT into user_trust_score (user_id, trust_score) values ($i,$trust ) ";

	$sql1 = " INSERT into user (user_id, fname, gender, status, last_login) values ($i,'$fname', '$gender', '$status', '$last_login') ";
	$sql2 = " INSERT into user_demography (user_id,age,religion,caste,mother_tongue, languages_known ) values ($i,$age, $religion, $caste, $mother_tongue, $languagues_known) ";
	$sql3 = " INSERT into user_trait (user_id, height,skin_tone,  body_type, smoking_status, drinking_status, food_status) values ($i, $height, '$skin_tone', '$body_type', '$smoke', '$drink', '$food') ";
	$sql4 = " INSERT into user_family (user_id, family_type, family_status) values ($i,'$family_type', '$family_status') ";
	$sql5 = " INSERT into user_work (user_id,working_area, industry, highest_education, income_start, income_end) values ($i, '$workArea', $industry, $ed, $stIncome, $endIncome) ";
	*/
}


function generateReccomendations(){
	global $conn;
	$sql = "SELECT `user_id` FROM `user` WHERE `status`='authentic' AND `plan` is not null";
	$ex = $conn->Execute($sql);
	//$res = $ex->GetRows();

	$t = microtime(true);

	if ($ex) {
		while ($val = $ex->FetchRow()) {
			 
			echo $val['user_id'];
			$generate = new generateNewMatches($val['user_id']);
			$generate->generateNewReccomendations(true);
			echo PHP_EOL;
			echo $t;

			$t2= microtime(true);
			echo PHP_EOL;
			//	echo $t2;
			echo '	per user time consumption: ' . ($t2-$t);
			$t = $t2;

			# process $arr
		} }
		//var_dump($res); die;
		/*foreach ($res as $key=>$val){
		//echo $val['user_id'];
		$generate = new generateNewMatches($val['user_id']);
		$generate->generateNewReccomendations();
		echo PHP_EOL;
		echo $t;

		$t2= microtime(true);
		echo PHP_EOL;
		echo $t2;
		echo PHP_EOL;
		echo '	per user time consumption: ' . ($t2-$t);
		$t = $t2;
		}*/
}

try{
	$t1= microtime(true);
	echo 'start: ' . $t1;

	createMemoryTable();

//generateReccomendations();


	$t2= microtime(true);
	echo PHP_EOL;
	echo '	end: ' . $t2;
	echo PHP_EOL;
	echo '	total:' . ($t2-$t1);
}
catch(Exception $e){
	$to = "shashwat@trulymadly.com, himanshu@trulymadly.com";
	$subject = "Recommendation Cron Failure";
	$message = "Cron failed due to " . $e->getMessage();
	$from = "admin@trulymadly.com";
	$headers = "From:" . $from;
	mail($to,$subject,$message,$headers);
	echo "Mail Sent.";
	error_log($subject,1,$to,$from);
	trigger_error ( $e->getMessage (), E_USER_WARNING );
}


?>
