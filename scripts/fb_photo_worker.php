<?php 
require_once (dirname (__FILE__ ) . '/../photomodule/Photo.class.php');
require_once (dirname (__FILE__ ) . '/../include/Utils.php');
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";


/*@uthor:Arpan
 *Valhar Morghulis
 */
$currtime=date("F j, Y, g:i a"); 
$msg = "FB_PHOTO_WORKER STARTED AT   ".$currtime;
trigger_error ( "PHP Fatal error WEB: ($msg) " ,E_USER_WARNING);
$worker = new GearmanWorker();
$res=$worker->addServer('127.0.0.1',4730);

$worker->addFunction("download_photo", "download_photo_server");
try {

	while (1) {
		print "Waiting for job...\n";
		$ret = $worker->work(); // work() will block execution until a job is delivered
		if ($worker->returnCode() != GEARMAN_SUCCESS) {
			{
				//$msg = "GEARMAN UNABLE TO PROCESS   ".$currtime;
				//trigger_error ( "PHP Fatal error WEB: ($msg) " ,E_USER_WARNING);
				break;
			}
		}
	}
	
} catch (Exception $e) {
	$msg = "fb_photo_worker script failed";
	trigger_error ( "PHP Fatal error WEB: Script failed ($msg) ". $e->getTraceAsString(), E_USER_WARNING );
	
}

function download_photo_server(GearmanJob $job) {
	global $redis;
	$workload2 = $job->workload();
	echo "Received job: " . $job->handle() . "\n";
	$workload=json_decode($workload2);
	//echo "Workload: $workload, workload: $workload2\n";
	//sleep(10);
	ini_set("error_log", "/var/log/httpd/error_log");
	$photomodule = new Photo($workload[0]);
	if($workload[2]==1)
		$insert_id = $photomodule->addFromComputer(false,$workload[1],true,'yes');
	else 
		$insert_id = $photomodule->addFromComputer(false,$workload[1],true,'no');
	if($insert_id==NULL)
	{
		$msg=" Photo not uploaded ";
		trigger_error ( "PHP Fatal error: ($msg) ", E_USER_WARNING );
	}
	else if(isset($workload[3]) )
	{
		if(is_numeric($workload[3]))
			$likes = $workload[3];
		else
			$likes = 0 ;
		$sql = "insert into user_facebook_photo_like (photo_id, user_id, photo_likes) values (?,?,?)" ;
		Query_Wrapper::INSERT($sql,array($insert_id, $workload[0],$likes));
	}
	$str=$workload[0].':'.$workload[1].':'.$workload[2].':'.$workload[3];
	$redis->sRem(Utils::$redis_keys['gearman_key'],$str);
	//echo "Result: $result\n";
	return $insert_id;
}



?>
