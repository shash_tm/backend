<?php 

require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/mandrill_template_mailer.php";
require_once dirname ( __FILE__ ) . "/../../logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";


function returnDummyArray ()
{
	$message=array(
				//'text'=>"Hey! " .$this->match_name . "<br><br>Wish your match a Happy Birthday",
				'subject'=>"Wish your match a Happy Birthday!!!!!",
				'from_name'=>"Trulymadly",
				'from_email'=>"noreply@trulymadly.com",
				'to'=>array ( ),
				'merge_language' => 'mailchimp',
				'merge_vars' => array( ) 
		);
	return $message;
}

function birthday_reminder( $gen ) 
{
	global $config, $conn_master, $conn_reporting;
	$mandrill_template =new mandrill_template(); 
    $uninTrk = new EventTrackingClass();
	
	$date = getdate();
	$mon=$date["mon"];
	$mday=$date["mday"];	
	
	$sql="select distinct(uu.email_id),  uu.fname,  u.fname  as bday_name from user u join user_data ud on u.user_id =ud.user_id
join  
( select user1 as user_id, user2 as match_id from user_mutual_like u1 where u1.blocked_by is null 
union 
 select user2 as user_id , user1 as match_id from user_mutual_like u2 where u2.blocked_by is null ) 
 um on u.user_id=um.user_id
 join user uu on uu.user_id= um.match_id
where  day(DateOfBirth)= $mday and month(DateOfBirth)= $mon and uu.email_id is not null and uu.email_status is null and u.gender = '$gen' 
and u.status ='authentic' and uu.status ='authentic'  ";

	$res=$conn_reporting->Execute($sql); 
	$i = 0 ;
	$all_count = $res->RowCount();
	$current_count = 0 ; 
	$to = array() ;
	$merge_vars = array() ;
    
	if($gen == "M" )
	$template_name = "match-s-birthday-m-1" ;
	else 
	$template_name = "match-s-birthday-f" ;  
	 
	
	while ($row = $res->FetchRow()) 
		{
			$current_count++ ;
			$i++ ;
			$to [] = array( 'email'  => $row['email_id'],
						    'name'   => $row['fname'] ) ;
			$merge_vars [] = array(
								     'rcpt' => $row['email_id'],
								     'vars' => array(   
													array(
														'name' => "FNAME",
														'content' =>$row['fname'] . "!!!"
										                 ),
										            array(
												        'name' => "MatchName",
												        'content' =>$row['bday_name'] 
										                 )
				 				                      )
						           ) ;
						           
			if ($i == 100 || $all_count == $current_count)
			{
				$message_arr = returnDummyArray() ;
				$message_arr ['to'] = $to ;
				$message_arr ['merge_vars'] = $merge_vars ;
				$result = $mandrill_template->mail_user($template_name, $template_content, $message_arr); 
				
		        $data[] = array("activity" => "mandrill_mailer" , "event_type" => "Birthday","event_status" => "success", 
		                       "event_info" => json_encode($result));
		        
		        $uninTrk->logActions($data);
				$to = array();
				$merge_vars = array() ;
			}
		}  
	
	
	
		
   return $all_count ;
}
try {
	
     $date = date ( 'Y-m-d');

	$female = birthday_reminder('F');	 
	$male =  birthday_reminder('M');
	$message = "Male Birthday Mailer sent to " . $male . PHP_EOL . "Female birthday Mailer sent to " .$female ;
	Utils::sendEmail("sumit.kumar@trulymadly.com, masrat@trulymadly.com","admintm@trulymadly.com", "Mandrill birthday Mailer  for " . $date , $message);
} 
catch (Exception $e) {
	echo "birthday scripts failed ".$e->getMessage();
	Utils::sendEmail("sumit.kumar@trulymadly.com, arpan@trulymadly.com","arpan@trulymadly.com", "Mandrill birthday Mailer failed ".$baseurl, $e->getMessage());
}



?>