<?php
require_once dirname ( __FILE__ ) .'/../../include/MailChimp.php';
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";


$options=getopt ( "d:" );
if ($options[d])
{
	$dur = $options[d];
}

if($dur == 'daily')
{
	$time_dur = $dur ;
}
else
{
	$time_dur = 'hourly' ;
}


class MailChimpUpdates
{

	private $mailchimpObj;
	private $conn_reporting;
	private $conn_master ;
	private $mailchimpId;
	private $mailchimp_api_key;
	private $file;
	private $fptr;

	function __construct()
	{ 
		global $conn_reporting, $mailchimp_api_key , $mailchimp_temp_file, $conn_master;
		$this->file = $mailchimp_temp_file ;
		if(!file_exists($mailchimp_temp_file))
		 $this->file = "/tmp/mail_chimp.txt";
		$this->fptr = fopen($this->file, 'c'); 
		
		$this->mailchimp_api_key = $mailchimp_api_key ;
		$this->mailchimpObj = new \Drewm\MailChimp($this->mailchimp_api_key);
		$this->mailchimpId = "51c9fe48e5";
		$this->conn_reporting = $conn_reporting;
		$this->conn_master = $conn_master ;
	}

    public function setLockOnTempFile(){
		if (!flock($this->fptr, LOCK_EX|LOCK_NB, $wouldblock)) {
			if ($wouldblock) {
				//echo "already locked";
				$isLocked = 1;
				// something already has a lock
			}
			else {
				echo "could not lock";
				// couldn't lock for some other reason
			}
		}
		else {
			//echo "obtain lock";
			$isLocked = 0;
			// lock obtained
			if(flock($this->fptr,LOCK_EX) == false){
				throw new Exception("unable to acquire lock");
			}else{
				echo "lock acquired";
				//Utils::sendEmail($techTeamIds .",sumit.kumar@trulymadly.com", "sumit.kumar@trulymadly.com", "Mail chimp script ran again ".$this->baseurl, " Lock acquired" );
			}  
		}
 
		return $isLocked;
	}
	
	public function oneTimeUpdate (  $is_day)
	{
		$current_time =  date ( 'Y-m-d H:i:s');
		$sql_time ="select max(tstamp) as last_time from mail_chimp_log where update_type='$is_day' and result= 'success' " ;
		$res_t = $this->conn_reporting->Execute ($sql_time);

		$res_time = $res_t->FetchRow() ;
		$last_update= $res_time ['last_time'];

//		if ( !isset($last_update))
//		$last_update = "2015-03-19 00:00:00" ;
 
		$sql = "select u.user_id, email_id, u.last_login,  ud.DateOfBirth, last_14_days_visibility, u.registered_from as OS ,
		date(u.last_login) as last_active, u.status , u.registered_at , i.name as industry_name, fname, lname, gender,
		TIMESTAMPDIFF(year,ud.DateOfBirth,curdate()) as age, gci.name as city, gs.name as state,  
		case when bucket is null then 0 else bucket end as pi, uas.android_app as app_status
		from user u 
		join user_data ud on u.user_id = ud.user_id  
		join user_app_status uas on u.user_id=uas.user_id
		LEFT JOIN geo_city gci on ud.stay_city = gci.city_id AND gci.state_id = ud.stay_state 
		join geo_state gs on gs.state_id = ud.stay_state  
		LEFT JOIN industries_new i on ud.industry = i.id 
		left join user_pi_activity up on u.user_id = up.user_id  
		where email_id is not null and email_status is null " ;

		if ($is_day == 'daily')
		{
			//$sql .= "and u.last_login > '$last_update'  ";
		}
		else
		{
			$sql .= "and (u.last_row_updated > '$last_update' or  ud.tstamp > '$last_update' or up.tstamp > '$last_update' or uas.tstamp > '$last_update') " ; 
		}
        
		echo "running query";
		$res = $this->conn_reporting->Execute ($sql);

		$arr = array();
		$i = 0 ;
		$response['add_count'] = 0;
		$response['update_count'] = 0;
		$response['error_count'] = 0;
		$response['errors'] = array() ;
        $all_count = $res->RowCount();
        $current_count = 0 ;

		while ($row = $res->FetchRow())
		{
	        $current_count++ ;
			$arr[] = array( "email" => array("email" => $row['email_id']),
					 "email_type" => "html", // optional, for the email type option (html or text)
                 	 "merge_vars" => array( // optional, data for the various list specific and special merge vars documented in lists/subscribe
                                        "fname" => $row ['fname'],
                  		                "lname"  => $row ['lname'],
			                             "OS" =>  $row ['OS'],
			                            "status" => $row ['status'],
										"last_login" => $row ['last_login'],
							 			"user_id" => $row ['user_id'],
										"age" => $row ['age'],
										"industry" => $row ['industry_name'],
										"gender" => $row ['gender'],
										"bucket" => $row ['pi'],
										"city" => $row ['city'],
										"state" => $row ['state'],
										"reg_date" => $row ['registered_at'],
										"views" => $row ['last_14_days_visibility'],
			                            "bday" =>  $row ['DateOfBirth'],
			                             "app_status" =>  $row ['app_status']
				 
			)
			);
				
			$i++ ;
				
			if ($i == 100 || $all_count == $current_count)
			{

				$results = $this->mailchimpObj->call("lists/batch-subscribe", array(
 							 		"id" => $this->mailchimpId, // required, the list id to pull abuse reports for (can be gathered using lists/list())
								    "batch"=> $arr,
 									"double_optin" =>     false, // optional, flag to control whether to send an opt-in confirmation email - defaults to true
  									"update_existing" =>  true, // optional, flag to control whether to update members that are already subscribed to the list or to return an error, defaults to false (return error)
 									"replace_interests" => true // optional, flag to determine whether we replace the interest groups with the updated groups provided, or we add the provided groups to the member's interest groups
				));
				//var_dump($results);
				echo "add : ".$results['add_count'];
				echo "update : "  .$results['update_count'];
				echo "Errors : "  .$results['error_count'].PHP_EOL;
				if ($results['status'] == 'error')
				{
		             $response['Error_results'] = $results;
				}
				else
				{
					$response['add_count'] += $results['add_count'];
					$response['update_count'] += $results['update_count'];
					$response['error_count'] += $results['error_count'];
					if ($results['error_count'] > 0 )
					{
			           $response['errors'][] = $results['errors'] ;
					}
				}
				$arr = array();
				$i = 0;
			}
				
				

		}

		$log = json_encode($response);
		
		if($response['error_results'] )
		{
			$response_result = 'error';
		}
		else 
		{
			$response_result = 'success' ;
		}

		$sql = "insert into mail_chimp_log (update_type, tstamp, log, result ) values (?, ?, ?, ?)" ;
		$this->conn_master->Execute ($this->conn_master->Prepare ($sql) , array($is_day, $current_time, $log, $response_result ) );

		return $response;
	}
}
try
{
	if(php_sapi_name() == 'cli')
	{
		$updateObj = new MailChimpUpdates();
		$checkLock = $updateObj->setLockOnTempFile();
		if ( $checkLock == false )
		{
		$result = $updateObj->oneTimeUpdate ( $time_dur );
		$message = "Added : " . $result['add_count'] . PHP_EOL . "Updated : " . $result['update_count'] . PHP_EOL . "Errors: " . $result ['error_count'] ;
		//Utils::sendEmail("sumit.kumar@trulymadly.com", "admintm@trulymadly.com",  $time_dur. " Mail chimp Update Report", $message,true);
		echo $message;
		echo "sent";	
		}
		
	}
}

catch (Exception $e)
{
	trigger_error($e->getMessage(), E_USER_WARNING);
	Utils::sendEmail($techTeamIds .",sumit.kumar@trulymadly.com", "sumit.kumar@trulymadly.com", "Mail chimp update script failed 	".$baseurl, $e->getMessage() );
}