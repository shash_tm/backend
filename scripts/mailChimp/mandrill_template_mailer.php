<?php
require_once dirname ( __FILE__ ) .'/../../mandrill_lib/src/Mandrill.php';

class mandrill_template
{
	
	private $mandrill_api_key;
	private $mandrill;
	
	function __construct()
	{
		global $mandrill_api_key;
		$this->mandrill_api_key= $mandrill_api_key;
		$this->mandrill= new Mandrill($this->mandrill_api_key);
	}
	
	
	public function mail_user($template_name, $template_content,$message)
	{
     	$result=$this->mandrill->messages->sendTemplate($template_name, $template_content,$message);
		return $result ;	
	}

}



?>