<?php
require_once dirname ( __FILE__ ) .'/../../include/MailChimp.php';
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";




class MailChimpUpdates
{

	private $mailchimpObj;
	private $conn_reporting;
	private $conn_master ;
	private $mailchimpId;
	private $mailchimp_api_key;
	private $file;
	private $fptr;

	function __construct()
	{ 
		global $conn_reporting, $mailchimp_api_key , $mailchimp_temp_file, $conn_master;
		$this->file = $mailchimp_temp_file ;
		if(!file_exists($mailchimp_temp_file))
		 $this->file = "/tmp/mail_chimp.txt";
		$this->fptr = fopen($this->file, 'c'); 
		
		$this->mailchimp_api_key = $mailchimp_api_key ;
		$this->mailchimpObj = new \Drewm\MailChimp($this->mailchimp_api_key);
		$this->mailchimpId = "51c9fe48e5";
		$this->conn_reporting = $conn_reporting;
		$this->conn_master = $conn_master ;
	}

    public function setLockOnTempFile(){
		if (!flock($this->fptr, LOCK_EX|LOCK_NB, $wouldblock)) {
			if ($wouldblock) {
				//echo "already locked";
				$isLocked = 1; 
				// something already has a lock
			}
			else {
				echo "could not lock";
				// couldn't lock for some other reason
			}
		}
		else {
			//echo "obtain lock";
			$isLocked = 0;
			// lock obtained
			if(flock($this->fptr,LOCK_EX) == false){
				throw new Exception("unable to acquire lock");
			}else{
				echo "lock acquired";
				//Utils::sendEmail($techTeamIds .",sumit.kumar@trulymadly.com", "sumit.kumar@trulymadly.com", "Mail chimp script ran again ".$this->baseurl, " Lock acquired" );
			}  
		}
 
		return $isLocked;
	}
	
	public function oneTimeUpdate (  )
	{
		$current_time =  date ( 'Y-m-d H:i:s');

//		if ( !isset($last_update))
//		$last_update = "2015-03-19 00:00:00" ;
 
		$sql = "select u.user_id, email_id, u.registered_from as OS , uas.android_app as app_status
		from user u 
		join user_data ud on u.user_id = ud.user_id  
		join user_app_status uas on u.user_id=uas.user_id
		where email_id is not null and email_status is null" ;

		
		echo "running query";
		$res = $this->conn_reporting->Execute ($sql);

		$arr = array();
		$i = 0 ;
		$response['add_count'] = 0;
		$response['update_count'] = 0;
		$response['error_count'] = 0;
		$response['errors'] = array() ;
        $all_count = $res->RowCount();
        $current_count = 0 ;

		while ($row = $res->FetchRow())
		{
			echo $row['user_id'];
			echo " ";
	        $current_count++ ;
			$arr[] = array( "email" => array("email" => $row['email_id']),
					 "email_type" => "html", // optional, for the email type option (html or text)
                 	 "merge_vars" => array( // optional, data for the various list specific and special merge vars documented in lists/subscribe
			                             "OS" =>  $row ['OS'],
							 			"user_id" => $row ['user_id'],
			                             "app_status" =>  $row ['app_status']
				 
			)
			);
				
			$i++ ;
				
			if ($i == 1000 || $all_count == $current_count)
			{

				$results = $this->mailchimpObj->call("lists/batch-subscribe", array(
 							 		"id" => $this->mailchimpId, // required, the list id to pull abuse reports for (can be gathered using lists/list())
								    "batch"=> $arr,
 									"double_optin" =>     false, // optional, flag to control whether to send an opt-in confirmation email - defaults to true
  									"update_existing" =>  true, // optional, flag to control whether to update members that are already subscribed to the list or to return an error, defaults to false (return error)
 									"replace_interests" => true // optional, flag to determine whether we replace the interest groups with the updated groups provided, or we add the provided groups to the member's interest groups
				));
				//var_dump($results);
				echo "add : ".$results['add_count'];
				echo "update : "  .$results['update_count'];
				echo "Errors : "  .$results['error_count'].PHP_EOL;
				if ($results['status'] == 'error')
				{
		             $response['Error_results'] = $results;
				}
				else
				{
					$response['add_count'] += $results['add_count'];
					$response['update_count'] += $results['update_count'];
					$response['error_count'] += $results['error_count'];
					if ($results['error_count'] > 0 )
					{
			           $response['errors'][] = $results['errors'] ;
					}
				}
				$arr = array();
				$i = 0;
			}
				
				

		}

		$log = json_encode($response);
		
		if($response['error_results'] )
		{
			$response_result = 'error';
		}
		else 
		{
			$response_result = 'success' ;
		}
		return $response;
	}
}
try
{
	if(php_sapi_name() == 'cli')
	{
		$updateObj = new MailChimpUpdates();
		$checkLock = $updateObj->setLockOnTempFile();
		if ( $checkLock == false )
		{
		$result = $updateObj->oneTimeUpdate ( );
		$message = "Added : " . $result['add_count'] . PHP_EOL . "Updated : " . $result['update_count'] . PHP_EOL . "Errors: " . $result ['error_count'] ;
		//Utils::sendEmail("sumit.kumar@trulymadly.com", "admintm@trulymadly.com",  $time_dur. " Mail chimp Update Report", $message,true);
		echo $message;
		echo "sent";	
		}
		
	}
}

catch (Exception $e)
{
	trigger_error($e->getMessage(), E_USER_WARNING);
	Utils::sendEmail($techTeamIds .",sumit.kumar@trulymadly.com", "sumit.kumar@trulymadly.com", "Mail chimp update script failed 	".$baseurl, $e->getMessage() );
}