<?php

require_once  dirname ( __FILE__ ) . "/../../include/config.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";

function notifyTechTeam($subject){
	//$to = "himanshu@trulymadly.com";
	$to =  Utils::$techTeam;
	$from = gethostname();
	Utils::sendEmail($to, $from, $subject, null);
}

function paymentReporting(){
	global $conn, $smarty;
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
	$sql1 = "select count(distinct(user_id)) as payClickers from action_log where step = 'pay-now' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql2 = "select count(distinct(user_id)) as paymentVisitors from action_log where step = 'payments' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql3 = " SELECT CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(  GROUP_CONCAT(time_taken ORDER BY time_taken desc SEPARATOR ','),   ',', 95/100 * COUNT(*) + 1), ',', -1) AS DECIMAL) AS `time` FROM action_log where step = 'payments-iframe' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	//$sql3 = " select avg(time_taken) as time from action_log where step = 'payments-iframe' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql4 = "select distinct(user_id) as count from action_log where step = 'payments-iframe' and info like '%Error%' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql5 = "select count(upc.user_id) as txns,group_concat(upc.user_id) as Ids, pp.cost, upc.status  from user_payment_callback upc left join payment_plan pp on pp.plan_id = upc.plan_id where date(upc.time) = date_sub(CURDATE(), interval 1 day) group by upc.plan_id, upc.status";
	
	$res1 = $conn->Execute($sql1);
	$res2 = $conn->Execute($sql2);
	$res3 = $conn->Execute($sql3);
	$res4 = $conn->Execute($sql4);
	$res5 = $conn->Execute($sql5);
	
	$row1 = $res1->FetchRow();
	$row2 = $res2->FetchRow();
	$row3 = $res3->FetchRow();
	$row4 = $res4->FetchRow();
	$row5 = $res5->GetRows();
	
	$sql6 = "SELECT user_id , response, status from user_payment_callback where date(time) = date_sub(CURDATE(), interval 1 day)";
	$res6 = $conn->Execute($sql6);
	$row6 = $res6->GetRows();
	//var_dump($row6); die;
	
	//var_dump($row5); die; 
	$smarty->assign("payClickers", $row1['payClickers']);
	$smarty->assign("paymentVisitors", $row2['paymentVisitors']);
	$smarty->assign("iFTime", round($row3['time'],2));
	$smarty->assign("ErrorCount", $row4['count']);
	$smarty->assign("paymentReport", $row5);
	$smarty->assign("response", $row6);
	
	 
	$subject = "Payment Statistics: ". date('d-m-Y',strtotime("-1 days"));;
	Utils::sendEmail(Utils::$managementTeam . ",hitesh@trulymadly.com", "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/paymentReport.tpl'), TRUE);
	//Utils::sendEmail("himanshu@trulymadly.com", "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/paymentReport.tpl'), TRUE);

}

try{
	paymentReporting();
}
catch(Exception $e){
	notifyTechTeam("Payment Reporting failed");
	trigger_error($e->getTraceAsString());
}
?>