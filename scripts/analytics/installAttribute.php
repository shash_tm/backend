<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php"; 
require_once  dirname ( __FILE__ ) . "/sourceOfInstall.php";


$isttime= date ( 'Y-m-d H:i:s' ,strtotime ( '+330 minutes' ) );
$end_date = date ( 'Y-m-d 18:30:00' ,strtotime ( $isttime ) );
$start_date=date('Y-m-d 18:30:00', strtotime('-1 day', strtotime($isttime)));
$start_hour= date('Y-m-d H:i:s', strtotime( '-60 minutes'));
$end_hour= date('Y-m-d H:i:s');
$yesterday = date ( 'Y-m-d' ,strtotime ( '-1 day',strtotime($isttime)) );

function getInstalls($start_date, $end_date) {
	global $conn_reporting;
	//echo "query running".PHP_EOL;
	$sql = "SELECT device_id, event_info as all_referrer FROM action_log_new aln WHERE aln.activity='install' AND aln.event_type='install_referrer'
 AND  tstamp>='$start_date'  AND  tstamp <'$end_date' ";

	$res = $conn_reporting->Execute ( $sql );
	$install_arr = $res->GetRows();
	//echo "query run".PHP_EOL;
	$source_arr= array();
	//echo "parsing data".PHP_EOL;
	foreach ($install_arr as $row => $col){
		$referrer= $col["all_referrer"];
		$source = sourceOfInstall::parseReferrerUrl($referrer,"utm_source");
		$source_arr[$source][]= $col["device_id"];

	}
	//echo "data parsed".PHP_EOL;
	$data= getAttributes($source_arr, $start_date);
	return $data;

}


function getOrganicInstalls($start_date, $end_date) {
	global $conn_reporting;
	//echo "query running".PHP_EOL;
	$sql = "SELECT device_id  FROM action_log_new aln WHERE aln.activity='install' AND  event_type='install' AND event_status IS NULL
	and  tstamp >='$start_date'  AND  ADDTIME((tstamp),'05:30:00')<'$end_date' "; 

	$res = $conn_reporting->Execute ( $sql );
	$install_arr = $res->GetRows();
	//echo "query run".PHP_EOL;
	$source_arr= array();

	foreach ($install_arr as $row => $col){

		$source_arr["All"][]= $col["device_id"];

	}
	//echo "data parsed".PHP_EOL;
	$data= getAttributes($source_arr, $start_date);
	return $data;

}


function getAttributes($source_arr,$start_date) {
	global $conn_reporting;
	$data_attribute = array();
	foreach ($source_arr as $key => $val) {
		//echo "for ".$key.PHP_EOL;
		$device_list = "";
		//var_dump($val);
		foreach ($val as $r => $c) {
			$device_list .= "'".	$c."',";
		}
		$installs = substr_count($device_list, ',');
		$device_list = rtrim($device_list, ",");
		//echo $device_list.PHP_EOL;
		$sql=  "select count(*) as registrations, sum(if(u.gender='M',1,0)) as male,sum(if(u.gender='F',1,0)) as female,
		sum(if(u.steps_completed regexp 'photo' && u.gender='M',1,0)) as completed_male,sum(if(u.steps_completed regexp 'photo' && u.gender='F',1,0)) as completed_female,
		sum(if(u.password = '',1,0)) as via_fb, sum(if(u.password != '',1,0)) as via_email, sum(if(u.status = 'authentic' && u.gender='M',1,0)) as auth_male, 
		sum(if(u.status = 'authentic' && u.gender='F',1,0)) as auth_female  from user u where device_id in ($device_list) and u.registered_at >='$start_date' ";
		//var_dump($sql);
		$res = $conn_reporting->Execute ( $sql );
		$attrib_arr = $res->FetchRow ();
		//echo $key.PHP_EOL;
		//echo $device_list.PHP_EOL;
		//var_dump($attrib_arr);
		$male_complete_percent = intval ( 100 * ($attrib_arr ['completed_male'] / $attrib_arr['male']) );
		$female_complete_percent = intval ( 100 * ($attrib_arr ['completed_female'] / $attrib_arr['female']) );

		$via_fb_percent= intval ( 100 * ($attrib_arr ['via_fb'] / ($attrib_arr ['via_fb'] + $attrib_arr ['via_email'])) );
		$via_email_percent = intval ( 100 * ($attrib_arr ['via_email'] / ($attrib_arr ['via_fb'] + $attrib_arr ['via_email'])) );

		$male_auth_percent = intval ( 100 * ($attrib_arr ['auth_male'] / $attrib_arr ['completed_male']) );
		$female_auth_percent = 	intval ( 100 * ($attrib_arr ['auth_female'] / $attrib_arr ['completed_female']) );

		$data_attribute[] = array(
		                   "source" => $key,  
		                   "installs" => $installs,
		            	   "registrations" => $attrib_arr['registrations'],
		 				   "male_complete" => $attrib_arr['completed_male'],
		            	   "female_complete" => $attrib_arr['completed_female'],
		            	   "male_complete_percent" => $male_complete_percent,
		            	   "female_complete_percent" => $female_complete_percent,
		             	   "female_auth" =>   $attrib_arr ['auth_female'],
		                   "male_auth" => $attrib_arr['auth_male'],
		                    "male_auth_percent"=> $male_auth_percent,
		                    "female_auth_percent" => $female_auth_percent,
		                    "via_fb" => $attrib_arr['via_fb'],
		                    "via_email" => $attrib_arr['via_email'],
		                    "via_fb_percent" => $via_fb_percent,
		                    "via_email_percent" => $via_email_percent
		);

		/*$sql_age ="SELECT gender, COUNT(*) AS count_all, SUM(IF(t.age<=23,1,0)) AS age_one, SUM(IF(t.age<=27 && t.age >=24 ,1,0))  AS age_two,
		 SUM(IF(t.age>=28,1,0)) AS age_three FROM (SELECT u.user_id, u.gender,
		 EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) AS age FROM user u JOIN user_data ud ON u.user_id=ud.user_id
		 WHERE device_id in ($device_list) and ADDTIME((u.registered_at),'05:30:00')>='$start_date' )t group by gender";
		 $resAge = $conn_reporting->Execute ( $sql_age );
		 $attrib_age = $resAge->FetchRow ();
		 $age_array = array();
		 foreach ($attrib_age as $val) {
			$age_array[$val['gender']] = $val;
			}
			$age[] = $age_array;*/
		 

		/*$sql_city ="select if(gc.name is not null,gc.name, gs.name) as city, u.gender, count(u.user_id) as users from user u
		 join user_data ud on u.user_id=ud.user_id
		 left join geo_city gc on gc.city_id=ud.stay_city and gc.state_id= ud.stay_state
		 left join geo_state gs on gs.state_id=ud.stay_state
		 where ADDTIME((u.registered_at),'05:30:00')>='$start_date' and u.steps_completed regexp 'photo' group by city,u.gender order by gender,users desc";

		 $res_city= $conn_reporting->Execute($sql_city);
		 $city_arr = $res_city->GetRows ();
		 $city= array ();
		 $city_count= 5;
		 $city[$city_count+2]['M']['count'] = 0;
		 $city[$city_count+2]['F']['count'] = 0;
		 $city[$city_count+1]['M']['count'] = 0;
		 $city[$city_count+1]['M']['count'] = 0;
		 $imale=0;
		 $ifemale=0;
		 $ncr = array ('Faridabad','Gurgaon','Delhi','Noida','Greater Noida','Ghaziabad');
		 foreach ($city_arr as $row => $col){
		 if(in_array($col['city'], $ncr)){
		 $city[$city_count+2][$col['gender']]['city']=  'Delhi/NCR';
		 $city[$city_count+2][$col['gender']]['count'] += $col['users'];
		 }
		 else if ($imale < $city_count && $col['gender'] == 'M'){
		 $city[$imale][$col['gender']]['city']=  $col['city'];
		 $city[$imale][$col['gender']]['count']= $col['users'];
		 $imale++;
		 }
		 else if ($ifemale < $city_count && $col['gender'] == 'F'){
		 $city[$ifemale][$col['gender']]['city']=  $col['city'];
		 $city[$ifemale][$col['gender']]['count']= $col['users'];
		 $ifemale++;
		 }
		 else {
		 $city[$city_count+1][$col['gender']]['city']= 'Others';
		 $city[$city_count+1][$col['gender']]['count'] += $col['users'];
		 }
		 }
		 $city_a[$key]= $city;
		 */


	} 
	return array("attributes" => $data_attribute, "city"=> $city_a, "age" => $age ) ;

}    


try {
	 
	if(php_sapi_name() === 'cli'){
		echo "in cli";

		$data["referrer"] = getInstalls($start_date,$end_date);
		//$data["organic"]  = getOrganicInstalls($start_date,$end_date);
		//var_dump($data);
       var_dump($data);
		$smarty->assign("data",$data);
		$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/installAttribute.tpl');

		$data_new["referrer"] = getInstalls($start_hour,$end_hour);
		//$data_new["organic"]  = getOrganicInstalls($start_hour,$end_hour);

		$smarty->assign("data",$data_new);
		$message.="<br><br> For Last Hour";
		$message.=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/installAttribute.tpl');



		$smarty->assign("yesterday",$yesterday);


		$to=  Utils::$contentTeam . ", snehil@trulymadly.com,hitesh@trulymadly.com";
		//$to='sumit.kumar@trulymadly.com';
		$from = 'admintm@trulymadly.com';
		$subject = 'Sourcewise Attributes'.$isttime;
		Utils::sendEmail($to, $from, $subject, $message,true); 
		//echo $message; 
		echo "mail_sent";
	} else {
   echo "in web"; 
		$data = $_POST;
		if ($data ['signup']) {

			$username = $data ['user'];
			$password = $data ['pass'];
			$sql = $conn_slave->Prepare ( "select id,user_name,super_admin from admin where user_name=? and password=md5(?)" );
			$rs = $conn_slave->Execute ( $sql, array (
			$username,
			$password
			) );
			if($rs&&$rs->rowCount()>0){
				$arr = $rs->FetchRow ();
				session_start();
				//var_dump($arr);exit;
				setcookie("admin_id",$arr['id'],time()+3600*21*30,'/');
				$_SESSION ['admin_id']=$arr['id'];
				$_SESSION ['super_admin']=$arr['super_admin'];
			}
			else{
				$error_login=true;
			}

		}

		$adminId= $_SESSION ['admin_id'];

		if (isset ( $adminId ) == false) {
			if ($error_login == true) {
				$smarty->assign ( "error_login", $error_login );
			}
			$smarty->display ( "../../templates/admin/adminlogin.tpl" );
			die ();
		}else{
			if(!isset($_COOKIE['admin_id']))
			setcookie("admin_id",$adminId,time()+3600*21*30,'/');
		}



		$data["referrer"] = getInstalls($start_date,$end_date);
		//$data["organic"]  = getOrganicInstalls($start_date,$end_date);
		//var_dump($data);

		$smarty->assign("data",$data);
		$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/installAttribute.tpl');

		$data_new["referrer"] = getInstalls($start_hour,$end_hour);
		//$data_new["organic"]  = getOrganicInstalls($start_hour,$end_hour);

		$smarty->assign("data",$data_new);
		$message.="<br><br> For Last Hour";
		$message.=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/installAttribute.tpl');



		$smarty->assign("yesterday",$yesterday);

		echo $message;
		$to=  'snehil@trulymadly.com';
		//$to='sumit.kumar@trulymadly.com';
		$from = 'admintm@trulymadly.com';
		$subject = 'Sourcewise Attributes '.$isttime;
		Utils::sendEmail($to, $from, $subject, $message,true);

    echo "sent to Snehil";



	}

} catch (Exception $e) {
	trigger_error($e->getMessage(),E_USER_WARNING);
}
?>


