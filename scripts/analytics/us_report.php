<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


class USReport
{
    private $conn_reporting;
    private $start;
    private $end;
    private $smarty ;
    private $us_data = array();

    function __construct($start, $end)
    {
        global $conn_reporting , $smarty ;
        $this->start = $start ;
        $this->end = $end;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->smarty = $smarty;
    }

    public function getDailyData()
    {
        $this->getinstalls();
        $this->getRegistrations();
        $this->getCumulativeUsers();
        $this->getSparkData();
        $this->smarty->assign('us_data',$this->us_data);
        $file = dirname ( __FILE__ ) . "/../../templates/reporting/usDaily.tpl";
        $data = $this->smarty->fetch ( $file );
        return $data;
    }

    private function getinstalls()
    {
        $sql ="SELECT count(DISTINCT(device_id)) as installs FROM user_gcm_current ugc WHERE install_tstamp>= '$this->start' and install_tstamp <'$this->end'
              and source='iosapp' and country_id= 254";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = $res->FetchRow();
        $this->us_data['installs'] = $data['installs'];
    }

    private function getRegistrations()
    {
        $sql = "select gender, count(*) as registrations, sum(if(steps_completed regexp 'photo' ,1,0)) as complete, sum(if(u.status ='authentic',1,0)) as authentic,
                sum(if(u.deletion_status is not null,1,0)) as deleted, sum(if(steps_completed  not regexp 'photo' || steps_completed is null,1,0)) as incomplete
                from user u join user_data ud on u.user_id = ud.user_id where ud.stay_country = 254 and registered_from ='ios_app'
                and registered_at >='$this->start' and registered_at <'$this->end'
                group by gender";
        $res = $this->conn_reporting->Execute($sql) ;

        while($row = $res->FetchRow())
        {
            $gender = $row['gender'];
            foreach($row as $k => $v)
            {
                if($k != 'gender')
                {
                    $this->us_data[$k][$gender] = $v ;
                }
            }
        }
    }

    private function getCumulativeUsers()
    {
        $sql = "select gender, count(*) as reg from user_search us where us.country= 254 group by gender";
        $res = $this->conn_reporting->Execute($sql) ;

        while($row = $res->FetchRow())
        {
            $this->us_data['cumulative authentic users'][$row['gender']] = $row['reg'];
        }
    }

    private function getSparkData()
    {
        $sql = "select gender, count(*) as cu from user_spark_transaction ust  join spark_packages sp on ust.package_id = sp.package_id
                join user u on u.user_id = ust.user_id where sp.currency='dollar' and sp.type='spark' and ust.status='consumed'
                and ust.tstamp >='$this->start' and  ust.tstamp <'$this->end' group by gender";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $this->us_data['spark transactions'][$row['gender']] = $row['cu'];
        }
    }
}
?>