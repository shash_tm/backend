<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../email/MailFunctions.php";

class VideoDaily
{
    private $conn_reporting;
    private $smarty ;
    private $start_time;
    private $end_time ;
    private $testUsers = "605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919,281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063,1615723";
    private $data_array = array() ;
    private $exclude_dummy;
    private $sucess=0;
    function __construct ()
    {
        global $conn_reporting, $smarty;
        $this->conn_reporting = $conn_reporting ;
        $this->smarty = $smarty ;
//        $this->exclude_dummy = "trim(BOTH '". '"' . "' FROM JSON_EXTRACT(event_info, " . '"$.match_id"))';
        $this->exclude_dummy = '"match_id":"1615723"';

    }

    public function getAllData($start, $end)
    {
        $this->start_time = $start ;
        $this->end_time = $end ;
        $final_data=array();
        $data=$this->getVideoDataSuccessful();
        $count=$this->getCountPending();
        $reasons=null;
        $reasons=$this->getVideoAttempts();
        $final_data['data']=$data;
        $final_data['pending']=$count;
        $final_data['fail_reasons']=$reasons;
        $final_data['attempt_fail']=count($reasons);//
        $final_data['attempt_success']=$this->sucess;
        $final_data['cummulative']=$this->getCummulativeCount();
        $this->smarty->assign("all_data",$final_data);
    }


    public function getVideoDataSuccessful()
    {
        $sql="select count(*) as mycount,u.gender,if(uv.status='active' and tm_approved is null,'pending for approval',uv.status) as new_status from user_video uv join
              user_data ud on uv.user_id=ud.user_id join user u on u.user_id=uv.user_id
              where time_of_saving>='$this->start_time' and u.user_id not in ($this->testUsers) and time_of_saving<'$this->end_time'
              group by u.gender,new_status;";
        $data=array();
        $res = $this->conn_reporting->Execute($sql);
        //var_dump($res);die;
        while($row = $res->FetchRow())
        {
            $mydata=array();
            $mydata['totalCount']=$row['mycount'];
            $this->sucess+=$row['mycount'];
            $mydata['gender']=$row['gender'];
            $mydata['status']=$row['new_status'];
            array_push($data,$mydata);
        }
        return $data;

    }

    public function getCummulativeCount()
    {
        $sql="select count(*) as cu,gender from user_video uv join user u on u.user_id=uv.user_id where uv.status='active' group by u.gender";
        $data=array();
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $mydata=array();
            $mydata['count']=$row['cu'];
            $mydata['gender']=$row['gender'];
            array_push($data,$mydata);
        }

        return $data;
    }

    public function getCountPending()
    {
        $sql="select count(*) as mycount from user_video
              where user_id not in ($this->testUsers) and status!='deleted' and  tm_approved is null";
        $data=array();
        $res = $this->conn_reporting->Execute($sql);
        $res=$res->FetchRow();
        return $res['mycount'];

    }

    public function getVideoAttempts()
    {
        $sql="select event_info from action_log_new where  tstamp>='$this->start_time' and tstamp<'$this->end_time' and user_id not in ($this->testUsers)  and event_type='video_upload' and event_status='fail' ";
        $data=array();
        $count=0;
        $errors=array();
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow()) {
            $msg_info = json_decode($row['event_info'], true);
            if ($msg_info['error_message'] != null) {
                $msg_info['error_message'] = $this->IncorporateErrorCode($msg_info);
                array_push($errors, $msg_info);
            }
        }
        return $errors;

    }

    public function IncorporateErrorCode($error)
    {

        $myerror=trim($error['error_message']);
        if($myerror=="5")
        {
            return "Out of duration bounds, original length ".$error['video_length_ms'].' ms';
        }
        else if($myerror=="4")
        {
            return "Not able to calculate duration , server Error";
        }
        else if($myerror=="3")
        {
            return "Unauthenticated filename";
        }
        else if($myerror=="2")
        {
            return "Filename not mentioned";
        }
        else if($myerror=="1")
        {
            return "Transcoder server error";
        }
        else return $myerror;
    }


}

try
{
    if(php_sapi_name() == 'cli')
    {
        if($argv[1] == 'week')
            $duration = 'week' ;
        else if($argv[1] == 'month')
            $duration = 'month' ;
        else
            $duration = 'day';

        $isttime = date ( 'Y-m-d H:i' ,strtotime ( '+330 minutes' ) );
        $indianTime = Utils::getDateGMTTOISTfromUnixTimestamp(microtime(true));
        if($duration == 'week')
        {
            $start_date=date('Y-m-d 18:30:00', strtotime('-8 day', strtotime($isttime)));
            $subject = "Video Weekly Report ". $indianTime;
        }
        else if($duration == 'month')
        {
            $start_date=date('Y-m-d 18:30:00', strtotime('-31 day', strtotime($isttime)));
            $subject = "Video Daily Report ". $indianTime;
        }
        else
        {
            $start_date=date('Y-m-d 18:30:00', strtotime('-2 day', strtotime($isttime)));
            $subject = "Video Daily Report ". $indianTime;
        }
        $end_date = date ( 'Y-m-d 18:30:00',strtotime('-1 day', strtotime($isttime)));
        $report = new VideoDaily();
        $report->getAllData($start_date,$end_date);


        $file = dirname ( __FILE__ ) . "/../../templates/reporting/videoReport.tpl";
        $data = $smarty->fetch ( $file );
        $to = $emailIdsForReporting['video_report'];
        //$to = "raghav@trulymadly.com";

        $email_array = explode(',',$to);
        //Utils::sendEmail("raghav@trulymadly.com","admintm@trulymadly.com",$subject,$data,true);
        $mailObject = new MailFunctions();
        $mid= $mailObject->sendMail($email_array ,$data,$data,$subject,null,null, array(),true);
        // Utils::sendEmail('sumit@trulymadly.com', 'admintm@trulymadly.com',$subject,$data, true);
        echo "mail sent to ".$to;
    }
}
catch (Exception $e)
{
    echo $e->getMessage();
    Utils::sendEmail("raghav@trulymadly.com","raghav@trulymadly.com","Video Reporting Error","Error ". $e->getMessage());
}


?>