<?php 
require_once dirname (__FILE__) . '/../../include/config_admin.php';
require_once dirname (__FILE__) . '/../../include/Utils.php';

function getAdminMessages()
{
	global $admin_id,$conn_reporting,$smarty;
	$query="select count(*) as total, sum(flag_seen) as seen_count, sum(flag_replied) as replied_count from
(select 
case
when t1.last_seen>t1.tStamp then 1
else 0
end as flag_seen,
case
when t2.tStamp>t1.tStamp then 1
else 0
end as flag_replied,
t1.receiver_id as admin_id
from 
(select * from messages_queue where receiver_id=? and tStamp>=date_sub(curdate(), interval 1 day) and tStamp<curdate())t1
left join 
(select * from messages_queue where sender_id=? and tStamp>=date_sub(curdate(), interval 1 day) and tStamp<curdate())t2
on t1.sender_id=t2.receiver_id
)t group by admin_id";
	$param_array=array($admin_id,$admin_id);
	$result=$conn_reporting->Execute($conn_reporting->prepare($query),$param_array);
    $result=$result->FetchRow();
    $admin_monitor=array();
    $admin_monitor['total_count']=$result['total'];
    $admin_monitor['seen_count']=$result['seen_count'];
    $admin_monitor['replied_count']=$result['replied_count'];
	//var_dump($admin_monitor);
    $query="select count(receiver_id) as total_replied from
(select receiver_id from messages_queue where sender_id=? and  
tStamp>=date_sub(curdate(), interval 1 day) and tStamp<curdate() 
group by receiver_id)t";
    $param_array=array($admin_id);
    $result=$conn_reporting->Execute($conn_reporting->prepare($query),$param_array);
    $result=$result->FetchRow();
    $admin_monitor['total_replied']=$result['total_replied'];
    
    
    $query="select count(*) as count from current_messages_new where sender_id=? and tStamp>=date_sub(curdate(), interval 1 day) and tStamp<curdate() ";
    $param_array=array($admin_id);
    $result=$conn_reporting->Execute($conn_reporting->prepare($query),$param_array);
    $result=$result->FetchRow();
    $admin_monitor['count']=$result['count'];
    
	$smarty->assign("admin_monitor",$admin_monitor);
}

function sendMail(){
	global $smarty,$emailIdsForReporting;
	$subject = "Admin Panel Messages Report : ". date('d-m-Y',strtotime("-1 days"));
	$to=$emailIdsForReporting['admin_panel_messages'];
	
	//$to ="arpan@trulymadly.com" ;
	$message = $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/admin_messages.tpl') ;
	Utils::sendEmail( $to , "admintm@trulymadly.com", $subject, $message , TRUE);
}

try {
	getAdminMessages();
	sendMail();
} catch (Exception $e) {
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}
?>