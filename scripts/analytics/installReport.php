<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once  dirname ( __FILE__ ) . "/sourceOfInstall.php";


function getInstalls($days){ 
	$end_day = $days - 1;
	global $smarty, $conn_reporting;
	//var_dump($DBHOST_slave);
	$sql="SELECT device_id, event_info as all_referrer FROM action_log_new aln WHERE aln.activity='install' AND aln.event_type='install_referrer'
 AND  ADDTIME((tstamp),'05:30:00')>=DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL $days DAY)  AND  ADDTIME((tstamp),'05:30:00')<DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL $end_day DAY) ";
	//$sql = "select fname from user where user_id=2049";
	//	echo $sql;
	//	echo "running on action_log".PHP_EOL;
	$res = $conn_reporting->Execute ( $sql );
	$install_arr = $res->GetRows();
	//var_dump($install_arr);
	//die;
	//echo "parsing the data";
	$source_arr= array();
	foreach ($install_arr as $row => $col){
		$referrer= $col["all_referrer"];
		//	echo $referrer.PHP_EOL;
		//$referrer =  '{"referrer":"mat_click_id=037b146adf8baf06ae0184b9328f61ea-20150318-160850&utm_campaign=trulymadly+%28Default%29&utm_content=Internal&utm_medium=Internal&utm_source=DCI&utm_term=Internal"}';
		$source = sourceOfInstall::parseReferrerUrl($referrer,"utm_source");
		//echo $source.PHP_EOL;
		$source_arr[$source][]= $col["device_id"];

	}
	//echo "data parsed".PHP_EOL;
	//var_dump($source_arr);
	$data = getAcquisition($source_arr, $end_day);
	ksort($data);
	//var_dump($data);

	return $data;
}

function getAcquisition($source_arr, $day){
	global $conn_reporting;
	$data = array();
	foreach ($source_arr as $key => $val) {
		//echo "for ".$key.PHP_EOL;
		$device_list = "";
		//var_dump($val);
		foreach ($val as $r => $c) {
			$device_list .= "'".	$c."',";
		}
		$device_list = rtrim($device_list, ",");
		//echo $device_list.PHP_EOL;
		$sql=  "select sum(if(gender='M',1,0)) as all_acquired_male, sum(if(gender='F',1,0)) as all_acquired_female , 
		sum(if(gender='M' && u.last_login >DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 1 DAY),1,0)) as active_male,
		sum(if(gender='F' && u.last_login >DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 1 DAY),1,0)) as active_female
		 from user u where device_id in ($device_list) and  ADDTIME((registered_at),'05:30:00')<DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL $day DAY)";
		//echo $sql;
		$res = $conn_reporting->Execute ( $sql );
		$ret_arr = $res->FetchRow ();
		$ret_percent_male=round((100*($ret_arr['active_male']/$ret_arr['all_acquired_male'])),2);
		$ret_percent_female=round((100*($ret_arr['active_female']/$ret_arr['all_acquired_female'])),2);
		
		$data[] = array(
		                   "source" => $key, 
		            "registrations_male" => $ret_arr['all_acquired_male'],
					"registrations_female" => $ret_arr['all_acquired_female'], 
		             "active_today_male" => $ret_arr['active_male'],
					"active_today_female" => $ret_arr['active_female'], 
		              "ret_percent_male" => $ret_percent_male,
		            "ret_percent_female" => $ret_percent_female
		);
	}
	return $data;
}

	
try {
	if(php_sapi_name() === 'cli'){
	$data = array();
	$data[3] = getInstalls(4);
	$data[7] = getInstalls(8);
	$data[1] = getInstalls(2);
	$smarty->assign("data",$data);

	$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/installReport.tpl');
	$to= Utils::$contentTeam . ",sumit.kumar@trulymadly.com,hitesh@trulymadly.com,rahul@trulymadly.com,snehil@trulymadly.com";
	//$to='sumit.kumar@trulymadly.com';
	$from = 'admintm@trulymadly.com';
	$subject = 'Sourcewise Retention';
	Utils::sendEmail($to, $from, $subject, $message,true);
	}

} catch (Exception $e) {
	trigger_error($e->getMessage(),E_USER_WARNING);
}



?>