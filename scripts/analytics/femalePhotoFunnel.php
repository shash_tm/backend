<?php 
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once  dirname ( __FILE__ ) . "/USFunnel.php";

class FemalePhotoFunnel {
	private $conn_reporting;
	private $start;
	private $end;
	private $photo_data;
	private $approved_photo ;
	function __construct() 
	{
		global $conn_reporting;
		$this->conn_reporting = $conn_reporting ;
		$this->start ;
		$this->end ;
		$this->photo_data ;
		$this->approved_photo;

	}
	function getPhotoFunnel($start, $end) 
	{
		$this->start = $start;
		$this->end = $end;
		$this->getInstalls() ;
		$this->getAllReg();
		$this->getFemaleReg();  
		$this->getUploadedPhotos() ;
		$this->photo_data['Approved PP'] = array();
		$this->getApprovedPhotos() ;
		$this->getActionTakenCount() ;
		$this->getLikedCount() ;
		$this->getMutualMatchesCount();
		$this->getConvoCount();
		$this->photo_data['No PP'] = array() ;	
		$this->calculateNotApprovedPhoto() ;
		$this->getNoPPActionCount() ;
		$this->getNoPPLikeCount() ;
		return $this->photo_data ;
		
	}
	private function getAllReg() 
	{
		$sql = "select if(registered_from= 'android_app','android_fb',registered_from) as  src, count(user_id) as cu from user u  "
				."where registered_at >='$this->start' and  registered_at <'$this->end' and registered_from in ('android_app','ios_app') "
				."group by src";
		$res = $this->conn_reporting->Execute ( $sql );
		$data = array ();
		while ( $row = $res->FetchRow () ) 
		{
			$data[$row['src']]= $row['cu'];
			$data[$row['src']."_p"]= round(100 * $row['cu']/$this->photo_data['All new installs'][$row['src']]);
		}  
		$this->photo_data['All new registrations start'] = $data ;
	}
	
	private function getFemaleReg()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else ( case when uf.user_id is not null   then 'android_fb' "
			  ."else 'android_email' end) end  as src , count(distinct(u.user_id)) as cu, sum(if(steps_completed regexp 'photo',1,0))  as cc, "
			  ."sum(if(u.status ='authentic' || last_changed_status= 'authentic',1,0)) as ca from user u left join user_facebook uf on u.user_id = uf.user_id "
			  ."where registered_at >='$this->start'	and  registered_at <'$this->end' and u.gender='f' and registered_from in ('android_app','ios_app') "
			  ."group by src";
		$res = $this->conn_reporting->Execute($sql) ;
		$data = array() ;
		while ($row = $res->FetchRow()) 
		{
			$data['cu'][$row['src']] = $row['cu'];
			$data['cc'][$row['src']] = $row['cc'];
			$data['ca'][$row['src']] = $row['ca'];
			$data['cc'][$row['src']."_p"] = round(100 * $row['cc']/$row['cu']);
			$data['ca'][$row['src']."_p"] = round(100 * $row['ca']/$row['cc']);
		}
		$this->photo_data['Female registration start'] = $data['cu'] ;
		$this->photo_data['Female registration complete'] = $data['cc'] ;
		$this->photo_data['Female authentic'] = $data['ca'] ;
	}

	private function getUploadedPhotos()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else ( case when uf.user_id is not null   then 'android_fb' "
			  ."else 'android_email' end) end  as src , count(distinct(u.user_id)) as cu from user u left join user_facebook uf on u.user_id = uf.user_id "
			  ."join user_photo up on u.user_id = up.user_id where registered_at >='$this->start'	and  registered_at <'$this->end' and u.gender='f' and "
			  ."steps_completed regexp 'photo' and (u.status ='authentic' or last_changed_status= 'authentic') and registered_from in ('android_app','ios_app') ". "group by src"; 
		$res = $this->conn_reporting->Execute ( $sql ) ;
		$data = array();
		while ($row = $res->FetchRow()) 
		{
			$data[$row['src']] = $row['cu'] ;
			$data[$row['src']."_p"] =  round(100 * $row['cu']/$this->photo_data['Female authentic'][$row['src']]); ;
		}
		$this->photo_data['Female uploaded photos'] = $data;
	}
	
	private function getApprovedPhotos ()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else ( case when uf.user_id is not null   then 'android_fb' "
			  ."else 'android_email' end) end  as src,if(ufl.hide_myself is null,'None',ufl.hide_myself) as visibility, "
              ."count(distinct(u.user_id)) as cu from user u left join user_facebook uf on u.user_id = uf.user_id join user_photo up on u.user_id = up.user_id "
			  ."left join user_flags ufl on u.user_id = ufl.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and u.gender='f' "
			  ."and steps_completed regexp 'photo'	and (u.status ='authentic' or last_changed_status= 'authentic')  and registered_from in ('android_app','ios_app') " 
 			  ."and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted' ) group by src, visibility" ;
		$res = $this->conn_reporting->Execute ($sql) ;
		$data = array() ;
		$new_data = array();
		while ($row = $res->FetchRow())
		{ 
			$data[$row['visibility']][$row['src']] = $row['cu'] ;
		}
		foreach ($data['None'] as $key=> $val) 
		{
			$sum = $data['None'][$key] + $data[0][$key] + $data[1][$key] ; 
			$visible_p = round(100 * $data[1][$key]/ $sum) ;
			$not_visible_p = round(100 * $data[0][$key]/ $sum) ;
			$this->approved_photo[$key] = $sum ;
			// 1380(8%, 2%)
			$new_data[$key] = $this->approved_photo[$key] . " (" . $visible_p ."%, " .$not_visible_p ."%)";
			$new_data[$key."_p"] = round(100 * $sum/$this->photo_data['Female uploaded photos'][$key]) ;
		}
		$this->photo_data['Female with profile photo approved'] = $new_data ;
	}
	
	private function getActionTakenCount ()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else (case when uf.user_id is not null then 'android_fb' else 'android_email' end) end "
			  ."as src, count(distinct(u.user_id)) as cu from user u  left join user_facebook uf on u.user_id = uf.user_id join user_photo up on "
			  ."u.user_id = up.user_id join (select distinct(user1) as user_id from "
			  ."(select user1 from user_like where timestamp >='$this->start'  union  select user1 from user_hide where timestamp >='$this->start' )t )r "
			  ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and u.gender='f' and steps_completed regexp 'photo' "
			  ."and registered_from in ('android_app','ios_app') and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
			  ."and (u.status ='authentic' or last_changed_status= 'authentic') group by  src";
		$res = $this->conn_reporting->Execute($sql) ;
		$data = array();
		while ($row = $res->FetchRow())
		{
			$data[$row['src']] = $row['cu'] ;
			$data[$row['src']."_p"] = round(100 * $row['cu']/ $this->approved_photo[$row['src']]);
		}
		$this->photo_data['Female took action'] = $data ;
	}
	
	private function getLikedCount()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else (case when uf.user_id is not null then 'android_fb' else 'android_email' end) end "
			  ."as src, count(distinct(u.user_id)) as cu from user u  left join user_facebook uf on u.user_id = uf.user_id join user_photo up on "
			  ."u.user_id = up.user_id join (select distinct(user1) as user_id  from user_like where timestamp >='$this->start' )r "
			  ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and u.gender='f' and steps_completed regexp 'photo' "
			  ."and registered_from in ('android_app','ios_app') and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
			  ."and (u.status ='authentic' or last_changed_status= 'authentic') group by  src";
		$res = $this->conn_reporting->Execute($sql) ;
		$data = array() ;
		while ($row = $res->FetchRow())
		{
			$data[$row['src']] = $row['cu'] ;
			$data[$row['src']."_p"] = round(100 * $row['cu']/ $this->photo_data['Female took action'][$row['src']]);
		}
		$this->photo_data['Female liked a profile'] = $data ;
	}
	
	private function getMutualMatchesCount()
	{
		global $miss_tm_id ;
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else (case when uf.user_id is not null then 'android_fb' else 'android_email' end) end "
			  ."as src, count(distinct(u.user_id)) as cu from user u  left join user_facebook uf on u.user_id = uf.user_id join user_photo up on "
			  ."u.user_id = up.user_id join messages_queue mq  on u.user_id = mq.sender_id where registered_at >='$this->start'	 and  "
			  ."registered_at <'$this->end' and u.gender='f' and  up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') and "
			  ."steps_completed regexp 'photo' and registered_from in ('android_app','ios_app') and (u.status ='authentic' or last_changed_status= 'authentic' ) "
			  ."and mq.tstamp >= '$this->start' and mq.receiver_id not in ($miss_tm_id) group by  src";
		 $res = $this->conn_reporting->Execute($sql) ;
		 $data = array() ;
		 while ($row = $res->FetchRow())
		 {
		 	$data[$row['src']] = $row['cu'] ;
			$data[$row['src']."_p"] = round(100 * $row['cu']/ $this->photo_data['Female liked a profile'][$row['src']]);
		 }
		 $this->photo_data['Female got mutual match'] = $data ;
	}
	
	private function getConvoCount()
	{
		global $miss_tm_id ;
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else (case when uf.user_id is not null then 'android_fb' else 'android_email' end) end "
				."as src, count(distinct(u.user_id)) as cu from user u  left join user_facebook uf on u.user_id = uf.user_id join user_photo up on "
				."u.user_id = up.user_id join messages_queue mq  on u.user_id = mq.sender_id where registered_at >='$this->start'	 and  "
				."registered_at <'$this->end' and u.gender='f' and  up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') and "
				."steps_completed regexp 'photo' and registered_from in ('android_app','ios_app') and (u.status ='authentic' or last_changed_status= 'authentic' ) "
				  ."and mq.tstamp >= '$this->start' and mq.msg_type != 'mutual_like' and mq.receiver_id not in ($miss_tm_id) group by  src";
		$res = $this->conn_reporting->Execute($sql) ;
	    $data = array() ;
		while ($row = $res->FetchRow())
		{
			$data[$row['src']] = $row['cu'] ;
			$data[$row['src']."_p"] = round(100 * $row['cu']/ $this->photo_data['Female got mutual match'][$row['src']]);
		}
		$this->photo_data['Female sent a message'] = $data ;
	}
	
	private function calculateNotApprovedPhoto ()
	{
		$data = array();
		foreach ($this->approved_photo as $key => $val)
		{
			$data[$key] = $this->photo_data['Female uploaded photos'][$key] - $this->approved_photo[$key] ;
			$data[$key."_p"] = round(100 * $data[$key]/$this->photo_data['Female uploaded photos'][$key]) ;
		}
		$this->photo_data['Female with profile photo not approved'] = $data ;
	}
	
	private function getInstalls() 
	{	
		$sql_reinstall = "select if(source is null,'androidApp',source) as src, count(distinct(ugc.device_id)) as c_u  from user_gcm_current ugc " 
					 		 ."join user u on u.device_id= ugc.device_id  or u.user_id = ugc.user_id where install_tstamp >= '$this->start' and install_tstamp < '$this->end' "
					 		 ."and u.registered_at < '$this->start' group by src" ;

		$reinstall = $this->conn_reporting->Execute($sql_reinstall) ;
		$reinstall_res = array();
		while ($row = $reinstall->FetchRow()) 
		{
			$reinstall_res[$row['src']] = $row['c_u'] ;
		}
		$sql_install = "select if(source is null,'androidApp',source) as src,count(distinct(device_id)) as c_u from user_gcm_current ugc "
					   ."where install_tstamp >='$this->start'  and install_tstamp < '$this->end' group by src";
		 
		$install = $this->conn_reporting->Execute($sql_install);
	    $install_res = array();
		while ($row = $install->FetchRow())
		{
			$install_res[$row['src']] = $row['c_u'] - $reinstall_res[$row['src']] ;
		}
		$install_row = array();
		$install_row['android_fb'] = $install_res['androidApp'];
		$install_row['ios_app'] = $install_res['iOSApp'];
		$this->photo_data['All new installs'] = $install_row ;
	}
	
	private function getNoPPActionCount()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else (case when uf.user_id is not null then 'android_fb' else 'android_email' end) end "
			  ."as src, count(distinct(u.user_id)) as cu from user u  left join user_facebook uf on u.user_id = uf.user_id "
			  ."join (select distinct(user_id) from user_photo where time_of_saving> '2015-12-21 00:00:00')p on p.user_id = u.user_id "
			  ."left join user_photo up on u.user_id = up.user_id and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
			  ."join (select distinct(user1) as user_id from "
			  ."(select user1 from user_like where timestamp >='$this->start'  union  select user1 from user_hide where timestamp >='$this->start' )t )r "
			  ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and u.gender='f' and steps_completed regexp 'photo' "
			  ."and registered_from in ('android_app','ios_app') and up.user_id is null "
			  ."and (u.status ='authentic' or last_changed_status= 'authentic') group by  src";
		$res = $this->conn_reporting->Execute($sql) ;
		$data = array();
		while ($row = $res->FetchRow())
		{
			$data[$row['src']] = $row['cu'] ;
			$data[$row['src']."_p"] = round(100 * $row['cu']/ $this->photo_data['Female with profile photo not approved'][$row['src']]);
		}
		
		$this->photo_data['Female took action(No PP)'] = $data ;
	}

	private function getNoPPLikeCount()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else (case when uf.user_id is not null then 'android_fb' else 'android_email' end) end "
			  ."as src, count(distinct(u.user_id)) as cu from user u  left join user_facebook uf on u.user_id = uf.user_id "
			  ."join (select distinct(user_id) from user_photo where time_of_saving> '2015-12-21 00:00:00')p on p.user_id = u.user_id "
			  ."left join user_photo up on u.user_id = up.user_id and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
			  ."join (select distinct(user1) as user_id from "
			  ."(select user1 from user_like where timestamp >='$this->start'  )t )r "
			  ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and u.gender='f' and steps_completed regexp 'photo' "
			  ."and registered_from in ('android_app','ios_app') and up.user_id is null "
			  ."and (u.status ='authentic' or last_changed_status= 'authentic') group by  src";
		
		$res = $this->conn_reporting->Execute($sql) ;
	  	$data = array();
  		while ($row = $res->FetchRow())
  		{
  			$data[$row['src']] = $row['cu'] ;
  			$data[$row['src']."_p"] = round(100 * $row['cu']/ $this->photo_data['Female took action(No PP)'][$row['src']]);
  		}

  		$this->photo_data['Female liked a profile(No PP)'] = $data ;
	}

}


try 
{
	$isttime= date ( 'Y-m-d H:i:s');
	$today = date('Y-m-d') ;
	$end_date = date('Y-m-d 00:00:00', strtotime('-2 day', strtotime($isttime)));
	$start_date=date('Y-m-d 00:00:00', strtotime('-9 day', strtotime($isttime)));
	$upto = date('Y-m-d', strtotime('-2 day', strtotime($isttime)));
	$from_s=date('Y-m-d', strtotime('-9 day', strtotime($isttime)));
	
	if (php_sapi_name() =='cli' )
	{
		$photoObj = new FemalePhotoFunnel() ;
		$photo = $photoObj->getPhotoFunnel($start_date, $end_date) ;
	//	var_dump($photo);
		$smarty->assign("data",$photo);
		$smarty->assign("from",$from_s);
		$smarty->assign("to",$upto);
		$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/userPhotoData.tpl'); #
	 	$subject = 'Weekly Photo report '.$today;
	 	$to = 'sumit@trulymadly.com,purav@trulymadly.com,rahul@trulymadly.com,shashwat@trulymadly.com,lavi@trulymadly.com' ;
	 	$from = 'admintm@trulymadly.com' ;
	 	Utils::sendEmail($to, $from, $subject, $message,true);


		$photoObjUS = new USPhotoFunnel() ;
		$photo_us = $photoObjUS->getPhotoFunnel($start_date, $end_date) ;
		$smarty->assign("data_US",$photo_us);
		$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/USFunnel.tpl');
		$subject = 'Weekly Photo report For US '.$today;
		Utils::sendEmail($to, $from, $subject, $message,true);
		}	
} catch (Exception $e) {
	echo "some error";
	echo $e->getMessage();
	Utils::sendEmail("sumit@trulymadly.com, shashwat@trulymadly.com", "sumit@trulymadly.com", "Female Photo funnel reporting failed ".$baseurl, $e->getMessage());
}



?>