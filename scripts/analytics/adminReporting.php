<?php

require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";

function notifyTechTeam($subject){
	//$to = "himanshu@trulymadly.com";
	$to =  Utils::$techTeam;
	$from = gethostname();  
	Utils::sendEmail($to, $from, $subject, null);
}

function photos(){
	global $conn_reporting, $smarty;
	$sql = "select count(photo_id) as count, up.status, gender, admin_approved from user_photo up left join user u on up.user_id = u.user_id where date(approval_time) = CURDATE() and up.status in ('active', 'rejected') and admin_approved = 'yes' and u.email_id not like '%trulymadly%' group by status, gender";
	$res = $conn_reporting->Execute($sql);
	$row = $res->GetRows();
	$smarty->assign("photos", $row);
	
	$sqlh = "SELECT COUNT(photo_id) AS count, up.status,gender, admin_approved FROM user_photo up LEFT JOIN user u ON up.user_id = u.user_id WHERE approval_time >= DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 1 HOUR) AND up.status IN ('active', 'rejected') AND admin_approved = 'yes' AND u.email_id NOT LIKE '%trulymadly%' GROUP BY status, gender";
	$resh = $conn_reporting->Execute($sqlh);
	$rowh = $resh->GetRows();
	//echo  $rowh;
	$smarty->assign("photosh", $rowh);
 
	$sql2 = "select count(photo_id) as count, up.status,gender, admin_approved from user_photo up left join user u on up.user_id = u.user_id where  up.status in ('active', 'rejected') and admin_approved = 'no' and u.email_id not like '%trulymadly%' and  u.status = 'authentic' group by status,gender, admin_approved";
	$res2 = $conn_reporting->Execute($sql2);
	$row2 = $res2->GetRows();
	/*if($res2->RowCount() == 0){
		$smarty->assign("totalModPhoto", 0);
	}
	else*/
	$smarty->assign("totalModPhoto", $row2);
}

	function profilePic() 
	{
		global $conn_reporting, $smarty;
		$sql = "select case when profile_pic > 0 then 'Profile Pic Approved' else ( case when active_pics > 0 then 'Profile Pic Rejected Others Aprroved' 
else (case when reject > 0 then 'No Photo approved' else ( case when not_moderated >0 then 'Photos still Under Moderation' else 'No Photos found' end) end) end) end as pic_status, 
count(*) as c_u from (select u.user_id, sum(if(is_profile='yes' && admin_approved='yes' && up.status ='active',1,0)) as profile_pic, 
sum(if(admin_approved ='yes' and up.status ='active',1,0)) as active_pics , sum(if(admin_approved ='yes' and up.status ='rejected',1,0)) as reject  ,
 sum(if(admin_approved ='no' and up.status ='active',1,0)) as not_moderated from user u join user_photo up on u.user_id=up.user_id where
date(registered_at) = date_sub(curdate(), interval 1 day)  and up.time_of_saving >=date_sub(curdate(), interval 1 day) and u.gender='f' and u.status ='authentic' group by user_id) t group by pic_status order by c_u desc ";
		
		$pic_res = $conn_reporting->Execute($sql);
		$pic_data = $pic_res->GetRows();
		
		$smarty->assign("female_pics", $pic_data) ; 
		
	}

function noProfilePicUnderModPhotos(){
	global $conn_reporting, $smarty; 
	$sql ="select group_concat(a.user_id) as ids , count(a.user_id) as count  from (select distinct user_id from user_photo where status ='active' and can_profile is null)a left join (select distinct user_id from user_photo where is_profile='yes')b on a.user_id=b.user_id left join user u on u.user_id = a.user_id  where  u.email_id not like '%trulymadly%' and u.status != 'suspended' and  b.user_id is null";
	//$sql = "select count(up.status) as count , up.status, up.user_id, group_concat(is_profile) as profiles from user_photo up left join user u on up.user_id = u.user_id where up.status = 'active'  and u.email_id not like '%trulymadly%' and u.status != 'suspended' and date(up.time_of_saving) = date_sub(CURDATE(), interval 1 day) group by up.status, up.user_id order by user_id";
	//$sql = "select count(status) as count , status, user_id, group_concat(is_profile) as profiles from user_photo where status = 'active'  and date(time_of_saving) = date_sub(CURDATE(), interval 1 day) group by status, user_id order by user_id";
	$res = $conn_reporting->Execute($sql);
	$row = $res->FetchRow();
	$count = 0;
	/*
	foreach ($row as $val){
		if(strpos($val['profiles'], 'yes')===false){
			$count++;
			echo $count; 
		}
	}*/
	$smarty->assign("noProfilePic", $row['count']);
	$smarty->assign("ids", $row['ids']);
}

function yesterdayRegWithoutPhoto(){
	global $conn_reporting, $smarty;

	$sql = "SELECT (select count(user_id) from user where date(registered_at) = date_sub(CURDATE(), interval 1 day) and email_id not like '%trulymadly%') - (select count(distinct(up.user_id)) as c2 from user_photo  up where up.user_id in (select user_id from user where date(registered_at) = date_sub(CURDATE(), interval 1 day) and email_id not like '%trulymadly%') and status = 'active') as count";
	//$sql1 = " select count(u1.user_id) as c1 from user u1 where date(u1.registered_at) = date_sub(CURDATE(), interval 1 day) and u1.email_id not like '%trulymadly%'";
	$res1  = $conn_reporting->Execute($sql);
	$row1 = $res1->FetchRow();
	/*
	 $sql2 = "select count(distinct(up.user_id)) as c2 from user_photo  up where up.user_id in (select user_id from user where date(registered_at) = date_sub(CURDATE(), interval 1 day) and email_id not like '%trulymadly%') ";
	 $res2 = $conn_reporting->Execute($sql2);
	 $row2 = $res2->FetchRow();

	 $count = $row1['c1'] - $row2['c2'];
	 */	$smarty->assign("woPohoto", $row1['count']);
}
 
function idProof($externalDate = null){
	global $conn_reporting,$smarty;
	if(isset($externalDate))
	$sql = "select count(up.user_id) as count, up.status,gender from user_id_proof  up left join user u on up.user_id = u.user_id where date(upload_time) >= '$externalDate' and u.email_id not like '%trulymadly%' group by status, gender";
	else
	$sql = "select count(up.user_id) as count, up.status,gender from user_id_proof  up left join user u on up.user_id = u.user_id where  date(upload_time) = CURDATE() and u.email_id not like '%trulymadly%' and up.status != 'under_moderation' group by up.status,gender";
	//$sql = "select count(up.user_id) as count, up.status from user_id_proof  up left join user u on up.user_id = u.user_id where  date(upload_time) = date_sub(CURDATE(), interval 1 day) and u.email_id not like '%trulymadly%' group by status";
	$res = $conn_reporting->Execute($sql);
	$row = $res->GetRows();
	$smarty->assign("idProof", $row);

	$sql2 = "select count(up.user_id) as count, up.status,gender from user_id_proof  up left join user u on up.user_id = u.user_id where  u.status != 'suspended'and  u.email_id not like '%trulymadly%' and up.status = 'under_moderation' group by gender";
	$res2 = $conn_reporting->Execute($sql2);
	$row2 = $res2->GetRows();
	$smarty->assign("IdsMod", $row2);
	//u.status != 'suspended'
}

function addProof(){
	global $conn_reporting,$smarty;
	//$sql = " select count(up.user_id) as count, up.status from user_address_proof  up left join user u on up.user_id = u.user_id where   date(upload_time) = date_sub(CURDATE(), interval 1 day) and u.email_id not like '%trulymadly%'  group by status";
	$sql = "select count(up.user_id) as count, up.status from user_address_proof  up left join user u on up.user_id = u.user_id where  date(upload_time) = date_sub(CURDATE(), interval 1 day) and u.email_id not like '%trulymadly%' and up.status != 'under_moderation' group by up.status";
	$res = $conn_reporting->Execute($sql);
	$row = $res->GetRows();
	$smarty->assign("addProof", $row);

	$sql2 = "select count(up.user_id) as count, up.status, up.sent_for_verification as sent from user_address_proof  up left join user u on up.user_id = u.user_id where  u.status != 'suspended'and  u.email_id not like '%trulymadly%' and up.status = 'under_moderation' group by up.sent_for_verification";
	//$sql2 = "select count(up.user_id) as count, up.status from user_address_proof  up left join user u on up.user_id = u.user_id where  u.status != 'suspended'and  u.email_id not like '%trulymadly%' and up.status = 'under_moderation'";
	$res2 = $conn_reporting->Execute($sql2);
	//$row2 = $res2->FetchRow();
	$row2 = $res2->GetRows();
	$smarty->assign("AddsMod", $row2); 
}

function empProof(){

	global $conn_reporting,$smarty;
	//$sql = "select count(up.user_id) as count, up.status from user_employment_proof  up left join user u on up.user_id = u.user_id where  date(upload_time) = date_sub(CURDATE(), interval 1 day) and  u.email_id not like '%trulymadly%' group by status";
	$sql = "select count(up.user_id) as count, up.status from user_employment_proof  up left join user u on up.user_id = u.user_id where  date(upload_time) = CURDATE() and u.email_id not like '%trulymadly%' and up.status != 'under_moderation' group by up.status";
	echo $sql;
	$res = $conn_reporting->Execute($sql);
	$row = $res->GetRows();
	$smarty->assign("empProof", $row);

	//$sql2 = "select count(up.user_id) as count, up.status from user_employment_proof  up left join user u on up.user_id = u.user_id where  u.status != 'suspended'and  u.email_id not like '%trulymadly%' and up.status = 'under_moderation'";
	$sql2 = " select count(up.user_id) as count, up.status, up.sent_for_verification as sent from user_employment_proof  up left join user u on up.user_id = u.user_id where  u.status != 'suspended'and  u.email_id not like '%trulymadly%' and up.status = 'under_moderation' group by up.sent_for_verification";
	$res2 = $conn_reporting->Execute($sql2);
	$row2 = $res2->GetRows();
	$smarty->assign("EmpsMod", $row2);
}

function getUploadedPhotos(){

	global $conn_reporting,$smarty;
	$sql = "select gender, count(*) as cu from user_photo up join user u on u.user_id=up.user_id where  date(time_of_saving)= CURDATE()
			and (admin_approved = 'yes'  or up.status='active') and u.status ='authentic' group by gender";

	$res = $conn_reporting->Execute($sql);
	$row = $res->GetRows();
	$smarty->assign("uploadedPics", $row);
}

function sendMail(){
	global $smarty, $emailIdsForReporting;
	$final=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/adminMailing.tpl');
	//echo $final;
	$to = $emailIdsForReporting['admin_reporting'] ;
	$subject = "Admin Statistics: ". date('d-m-Y H:i');
	Utils::sendEmail($to , "admintm@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/adminMailing.tpl'), TRUE);
	//Utils::sendEmail("sumit.kumar@trulymadly.com", "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/adminMailing.tpl'), TRUE);

}

try{
	if(php_sapi_name() === 'cli'){
	$options=getopt ( "d:" );
	photos();
	profilePic ();
	getUploadedPhotos();
	//noProfilePicUnderModPhotos();
	//yesterdayRegWithoutPhoto();
	idProof();
	//addProof();
	//empProof();
	sendMail();
	}
}
catch (Exception $e){
	trigger_error($e->getMessage(), E_USER_WARNING);
}
?>