<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


$options=getopt ( "d:" );
if ($options[d])
{
    $dur = $options[d];
}

$isttime= date ( 'Y-m-d H:i:s');
$end_date = date ( 'Y-m-d 00:00:00');
$start_date=date('Y-m-d 00:00:00', strtotime('-7 day', strtotime($isttime)));
$end_date = date ( 'Y-m-d 00:00:00');




class sharedPhotoReport
{
    protected $conn_reporting;
    private $start ;
    private $end ;
    private $smarty ;

    function __construct()
    {
        global $conn_reporting, $smarty;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->start;
        $this->end ;
        $this->smarty = $smarty;
    }
    /*Time taken for different events*/
    function getStats($start_time, $end_time)
    {
        $this->start = $start_time ;
        $this->end = $end_time ;
        $this->getsharedStats();
        $this->geticonClicked();
        $this->getphotoUploaded();
        $this->getphotoSource();
        $this->getdeliveredPhoto();
        $this->getdownloadedPhoto();
    }

    /*Error counts for facebook linkdin and phone verification*/

    private  function getsharedStats()
    {
        $sql ="select gender, count(distinct(u.user_id)) as users,count(*)  as all_photos , "
             ."count(distinct(if(usp.user_id>match_id, concat(usp.user_id,'_',match_id), concat(match_id,'_',usp.user_id))))  as pairs "
             ."from user_shared_photo usp  join  user u on u.user_id = usp.user_id where tstamp >'$this->start'  and tstamp <'$this->end' "
             ."group by gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[] = $row ;
        }
        $this->smarty->assign("stats",$data);
    }
    private  function geticonClicked()
    {
        $sql ="select gender, log.source as app, count(distinct(u.user_id)) as users, count(u.user_id) as actions ".
              "from action_log_new log ".
              "join user u on u.user_id = log.user_id ".
              "where activity = \"photo_share\" and event_type = \"icon_clicked\" ". 
              "and u.user_id not in (605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919, 281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063) ". 
              "and log.tstamp >'$this->start'  and log.tstamp <'$this->end' ".
              "group by gender, app" ;
            
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[] = $row ;
        }
        $this->smarty->assign("iconclick",$data);
    }
    private  function getphotoUploaded()
    {
        $sql = "select event_status as status, gender, log.source as app, count(distinct(u.user_id)) as users, count(u.user_id) as actions ".
              "from action_log_new log ".
              "join user u on u.user_id = log.user_id ".
              "where activity = \"photo_share\" and event_type = \"upload\" ". 
              "and u.user_id not in (605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919, 281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063) ". 
              "and log.tstamp >'$this->start'  and log.tstamp <'$this->end' ".
              "group by gender, app, event_status" ;
            
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[] = $row ;
        }
        $this->smarty->assign("upload",$data);
    }
    private  function getphotoSource()
    {
        $sql = "select gender, if(log.source = \"android_app\", LCASE(JSON_EXTRACT(log.event_info,\"$.source\")), LCASE(JSON_EXTRACT(log.event_info,\"$.Source\"))) as app , count(distinct(u.user_id)) as users, count(u.user_id) as actions ".
              "from action_log_new log ".
              "join user u on u.user_id = log.user_id ".
              "where activity = \"photo_share\" and event_type = \"upload\" and event_status = \"success\" ". 
              "and u.user_id not in (605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919, 281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063) ". 
              "and log.tstamp >'$this->start'  and log.tstamp <'$this->end' ".
              "group by gender, app" ;
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[] = $row ;
        }
        $this->smarty->assign("source",$data);
    }
    private  function getdeliveredPhoto()
    {
        $sql = "select event_status as status, gender, log.source as app, count(distinct(u.user_id)) as users, count(u.user_id) as actions ".
              "from action_log_new log ".
              "join user u on u.user_id = log.user_id ".
              "where activity = \"photo_share\" and event_type = \"download_thumbnail\" ". 
              "and u.user_id not in (605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919, 281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063) ". 
              "and log.tstamp >'$this->start'  and log.tstamp <'$this->end' ".
              "group by gender, app, event_status" ;
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[] = $row ;
        }
        $this->smarty->assign("deliver",$data);
    }
    private  function getdownloadedPhoto()
    {
        $sql = "select event_status as status, gender, log.source as app, count(distinct(u.user_id)) as users, count(u.user_id) as actions ".
              "from action_log_new log ".
              "join user u on u.user_id = log.user_id ".
              "where activity = \"photo_share\" and event_type = \"download_image\" ". 
              "and u.user_id not in (605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919, 281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063) ". 
              "and log.tstamp >'$this->start'  and log.tstamp <'$this->end' ".
              "group by gender, app, event_status" ;
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[] = $row ;
        }
        $this->smarty->assign("download",$data);
    }
}



?>
