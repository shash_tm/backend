<?php

require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php"; 

function notifyTechTeam($subject){
	//$to = "himanshu@trulymadly.com";
	$to =  Utils::$techTeam . "sumit@trulymadly.com";
	$from = gethostname();
	Utils::sendEmail($to, $from, $subject, null);
} 
  
function paymentReporting(){
	global $conn_reporting, $smarty;
	$conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );

	$sql1 = "select count(distinct(user_id)) as payClickers, group_concat(distinct(user_id)) as ids from action_log where step = 'pay-now' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	//$sql1 = "select count(distinct(user_id)) as payClickers from action_log where step = 'pay-now' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql2 = "select count(distinct(user_id)) as paymentVisitors from action_log where step = 'payments' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql3 = " SELECT CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(  GROUP_CONCAT(time_taken ORDER BY time_taken desc SEPARATOR ','),   ',', 95/100 * COUNT(*) + 1), ',', -1) AS DECIMAL) AS `time` FROM action_log where step = 'payments-iframe' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	//$sql3 = " select avg(time_taken) as time from action_log where step = 'payments-iframe' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql4 = "select distinct(user_id) as count from action_log where step = 'payments-iframe' and info like '%Error%' and date(tstamp) = date_sub(CURDATE(), interval 1 day)";
	$sql5 = "select count(upc.user_id) as txns,group_concat(upc.user_id) as Ids, pp.cost, upc.status  from user_payment_callback upc left join payment_plan pp on pp.plan_id = upc.plan_id where date(upc.time) = date_sub(CURDATE(), interval 1 day) group by upc.plan_id, upc.status";

	$res1 = $conn_reporting->Execute($sql1);
	$res2 = $conn_reporting->Execute($sql2);
	$res3 = $conn_reporting->Execute($sql3);
	$res4 = $conn_reporting->Execute($sql4);
	$res5 = $conn_reporting->Execute($sql5);

	$row1 = $res1->FetchRow();
	$row2 = $res2->FetchRow();
	$row3 = $res3->FetchRow();
	$row4 = $res4->FetchRow();
	$row5 = $res5->GetRows();

	$sql6 = "SELECT user_id , response, status from user_payment_callback where date(time) = date_sub(CURDATE(), interval 1 day)";
	$res6 = $conn_reporting->Execute($sql6);
	$row6 = $res6->GetRows();
	//var_dump($row6); die;

	//var_dump($row5); die;
	$smarty->assign("payClickers", $row1['payClickers']);
	$smarty->assign("ids", $row1['ids']);
	$smarty->assign("paymentVisitors", $row2['paymentVisitors']);
	$smarty->assign("iFTime", round($row3['time'],2));
	$smarty->assign("ErrorCount", $row4['count']);
	$smarty->assign("paymentReport", $row5);
	$smarty->assign("response", $row6);


	/*$subject = "Payment Statistics: ". date('d-m-Y',strtotime("-1 days"));;
	 //Utils::sendEmail(Utils::$managementTeam . ",hitesh@trulymadly.com", "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/paymentReport.tpl'), TRUE);
	 Utils::sendEmail("himanshu@trulymadly.com", "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/paymentReport.tpl'), TRUE);
	 */
}

//TODO: remove reactivation Ids
function suspensionReporting(){
	global $conn_reporting, $smarty, $admin_id ;
	$conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
	//$sql = " select usl.user_id, usl.reason, u.fname from user_suspension_log usl left join user u on usl.user_id = u.user_id  where admin_id is null and date(timestamp) = date_sub(CURDATE(), interval 1 day) ";
	//$sql = "select distinct(usl.user_id) as user_id, usl.reason, u.fname, u.is_fb_connected, u.email_id, upn.user_number, date(u.registered_at) as reg_date,u.phone_number  from user_suspension_log usl left join user u on usl.user_id = u.user_id left join user_phone_number upn on usl.user_id = upn.user_id where admin_id is null and date(timestamp) = date_sub(CURDATE(), interval 1 day) and u.email_id not like '%@trulymadly.com%' order by u.registered_at";
	$sql = "SELECT * FROM (SELECT DISTINCT(usl.user_id) AS user_id, usl.reason, u.fname, u.gender,usl.previous_status AS last_status, u.is_fb_connected, u.email_id, upn.user_number, 
DATE(u.registered_at) AS reg_date,u.phone_number, CAST(DATEDIFF(CURDATE(),ud.dateofbirth ) / 365.25 AS UNSIGNED) AS age,gc.name AS city ,
gs.name AS state,
COUNT(DISTINCT(ull.user1)) AS likeback, COUNT(DISTINCT(mqq.sender_id)) AS msgs
 FROM user_suspension_log usl 
LEFT JOIN user u ON usl.user_id = u.user_id 
LEFT JOIN user_phone_number upn ON usl.user_id = upn.user_id 
LEFT JOIN user_data ud ON usl.user_id=ud.user_id
LEFT JOIN geo_city gc ON ud.stay_city=gc.city_id
LEFT JOIN geo_state gs ON ud.stay_state=gs.state_id
LEFT JOIN user_like ul ON usl.user_id=ul.user1
LEFT JOIN user_like ull ON (ull.user1=ul.user2 AND ull.user2=ul.user1)
LEFT JOIN messages_queue mq ON mq.sender_id=usl.user_id
LEFT JOIN messages_queue mqq ON (mqq.sender_id=mq.receiver_id AND mq.receiver_id=mqq.sender_id and mqq.sender_id !='$admin_id')
WHERE admin_id IS NULL AND DATE(usl.timestamp) = DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND usl.current_status = 'suspended' 
 and usl.previous_status !='suspended'
AND u.email_id NOT LIKE '%@trulymadly.com%' GROUP BY usl.user_id )t ORDER BY  gender DESC , last_status DESC  , likeback DESC";
	//echo $sql;
	$res = $conn_reporting->Execute($sql);
	$rows = $res->GetRows();
	var_dump($rows);
	$smarty->assign("details", $rows);

	$sql2 = "select count(distinct(usl.user_id)) as count, usl.reason, u.gender from user_suspension_log usl join 
	user u on usl.user_id = u.user_id  where admin_id is null and date(timestamp) = date_sub(CURDATE(), interval 1 day) 
	and usl.current_status = 'suspended' and u.email_id not like '%@trulymadly.com%' 
	group by  usl.reason, u.gender order by u.gender,count desc";
	$res2 = $conn_reporting->Execute($sql2);
	$row2 = $res2->GetRows();
	$smarty->assign("summary", $row2);


}

function uninstallReporting(){
	global $conn_reporting, $smarty;
	$sql="SELECT u.user_id, CONCAT(u.fname,' ',u.lname) AS nameofuser, DATE(u.registered_at) AS registration_date, DATE(t.time_uni) AS date_uninstall, upn.user_number,
	 EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,DateOfBirth)))) AS age, gc.name AS city FROM user u JOIN
( SELECT DISTINCT(ugc.user_id),MAX(ugc.tstamp) AS time_uni, GROUP_CONCAT(ugc.status) AS statuses FROM user_gcm_current ugc WHERE ugc.user_id IS NOT NULL AND 
ugc.status IS NOT NULL AND ugc.tstamp >DATE_SUB(CURDATE(), INTERVAL 1 DAY)
GROUP BY ugc.user_id)t ON u.user_id=t.user_id
JOIN user_phone_number upn ON u.user_id=upn.user_id
LEFT JOIN user_data ud ON u.user_id=ud.user_id
LEFT JOIN geo_city gc ON gc.city_id=ud.stay_city AND gc.state_id=ud.stay_state 
WHERE t.statuses REGEXP 'uninstall' AND t.statuses NOT REGEXP 'login' AND t.statuses NOT REGEXP 'logout' AND u.gender='f'
AND u.status NOT IN ('suspended','blocked') ";
	$res = $conn_reporting->Execute($sql);
	$rows = $res->GetRows();
	//var_dump($rows);
	$user_ids = array();
	foreach ($rows as $value)
	{
		$user_ids[] = $value['user_id'];
		$all_arr[$value['user_id']] = $value ;
	}
	if (count($user_ids) == 0 )
	   return  ;
	   
    $user_id_list= implode(',', $user_ids);
   
	$sql_message= "select sender_id, count(*) as all_count , sum(if(msg_type='MUTUAL_LIKE',1,0)) AS likes_not_chatted from messages_queue 
	where sender_id in ($user_id_list) group by sender_id";
	$res_msg= $conn_reporting->Execute($sql_message);
	while ($row = $res_msg->FetchRow())
		{
			$all_arr[$row['sender_id']]['all_m'] = $row['all_count']; 
			$all_arr[$row['sender_id']]['likes_not_chatted'] = $row['likes_not_chatted']; 
		}
	
	$smarty->assign("uninstalls", $all_arr);
}
function abuseReported(){
	global $conn_reporting, $smarty;
	$arr = array("Has sent abusive email/message","Misleading profile information","Inappropriate content","Already married/engaged","Harassment","Asking for Money","Other");
	$conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
	$sql = "SELECT sender_id, abuse_id, reason, abuse_msg from report_abuse where date(abuse_time) = date_sub(CURDATE(), interval 1 day) AND reason !='Not interested anymore'";
	$res = $conn_reporting->Execute($sql);
	$rows = $res->GetRows();
	foreach ($rows as $val){
		$val['abuse_msg'] = $arr[$val['abuse_msg']];
	}
	$smarty->assign("abuses", $rows);
}

function badMessages(){
	global $conn_reporting, $smarty;
	$conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
	$sql = "SELECT sender_id, receiver_id, bad_msg from badMessages_exchanged where date(tStamp) = date_sub(CURDATE(), interval 1 day) ";
	$res = $conn_reporting->Execute($sql);
	$rows = $res->GetRows();
	$smarty->assign("badMsgs", $rows);
}

function suspendedByAdmin(){

}

function getBlockProfiles(){
	global $conn_reporting, $smarty;
	$sql= "select t.abuse_id,gender, concat(fname,' ',lname) as name, c_u, c_u_fifteen, ADDTIME((last_reported),'05:30:00') as last_rep
   	from (SELECT abuse_id, count(distinct(sender_id)) as c_u, sum(if(abuse_time>date_sub(CURDATE(), interval 1 day),1,0)) as recent, sum(if(abuse_time>date_sub(CURDATE(), interval 15 day),1,0)) as c_u_fifteen,max(abuse_time) 
   	as last_reported from report_abuse where 
 reason  in ('Inappropriate or offensive content','Inappropriate content','Offensive content','Others') and not_offensive is null
   	group by abuse_id having c_u > 3 and recent >0 )t join user u on t.abuse_id=u.user_id where u.status !='blocked' order by u.gender, last_rep desc ";
	// having c_u > 3
	$res = $conn_reporting->Execute($sql);
	$block_arr = array();
	if($res->RowCount()>0){
		while ($row = $res->FetchRow()){
			$block_arr[]= $row ;
		}
	}
	//var_dump($block_arr);
	//die;
	$smarty->assign("blocks", $block_arr);

}

function sendMail(){
	global $smarty, $emailIdsForReporting ;
	$subject = "Suspension, uninstall and block Statistics: ". date('d-m-Y',strtotime("-1 days"));
	$to = $emailIdsForReporting['suspension'] ;
	//$to='arpan@trulymadly.com';
	$message = $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/paymentsNsuspension.tpl') ;
	Utils::sendEmail( $to , "admintm@trulymadly.com", $subject, $message , TRUE);
//	 Utils::sendEmail("sumit.kumar@trulymadly.com", "admintm@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/paymentsNsuspension.tpl'), TRUE);

}

try{
    //paymentReporting();
 	suspensionReporting();
	abuseReported();
	badMessages();
 	suspendedByAdmin();
	uninstallReporting();
	getBlockProfiles();
	sendMail();
}
catch(Exception $e){
	notifyTechTeam("Suspension and uninstall reporting failed");
	trigger_error($e->getMessage());
}
?>