<?php 
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";

class datesReport
{
	private $conn_reporting;
	private $start;
	private $end;
	private $excluded_ids;
	private $smarty;
	private $ncr ;
	
	function __construct() 
	{
		global $conn_reporting, $smarty ;
		$this->conn_reporting = $conn_reporting ;
		$this->start ;
		$this->end ;
		$this->excluded_ids =  "605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919," 
							  ."281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063";
		$this->ncr = "16743,47965,47964,47968,47967,47966" ; 
		$this->smarty = $smarty ;
	}
	
	public function getDatesReport($start, $end) 
	{
		$this->start = $start ;
		$this->end = $end ; 
 		$this->getUserDatesIcon() ;
  		$this->getSentSuggestions() ;
 		$this->getAcceptedSuggestions();
 		$this->getPairsSuggestWithoutAccept();
 		$this->getPairsSuggestAndAccept();
 		$this->getPairAcceptMultiple();
 		$this->venues();
 		$this->getViewed();
	}
	
	private function getUserDatesIcon()
	{
		$sql =  "select  case when ud.city_id in ($this->ncr) then 'Delhi / NCR'  when gc.city_id is null then gs.name else gc.name end as location, "
		       	."u.gender, count(distinct(u.user_id)) as cu from user_deal_icon_shown ud join user u on u.user_id = ud.user_id "
		       	."join (select distinct(user_id) from user_match_deal_icon_shown)t on t.user_id = u.user_id "
			   	."left join geo_city gc on gc.city_id = ud.city_id and gc.state_id = ud.state_id join geo_state gs on gs.state_id = ud.state_id "
				." where u.user_id not in ($this->excluded_ids) group by location, u.gender" ;
		$res = $this->conn_reporting->Execute($sql) ;
		$icon = array() ;
		$sum = array() ;
		while( $row = $res->FetchRow())
		{
			$icon[$row['location']][$row['gender']] = $row['cu'] ;
			$sum[$row['gender']] += $row['cu'];
		}
		$icon['Total'] = $sum ;
		$this->smarty->assign('icon',$icon) ;
	}
	
   private function getSentSuggestions() 
   {
   	   $sql = "select  case when ud.city_id in ($this->ncr) then 'Delhi / NCR' when gc.city_id is null then gs.name else gc.name end as location, "
   	   		."u.gender, count(distinct(u.user_id)) as cu, count(*) as ca  from user_deal_icon_shown ud join user u on u.user_id = ud.user_id "
	        ."join user_deals udd on u.user_id = udd.user1 "
	        ."left join geo_city gc on gc.city_id = ud.city_id and gc.state_id = ud.state_id join geo_state gs on gs.state_id = ud.state_id "
			." where u.user_id not in ($this->excluded_ids)  and created_tstamp >= '$this->start' and created_tstamp < '$this->end'   group by location,gender";
   	   
	   $res = $this->conn_reporting->Execute($sql) ;
	   $suggest = array();
	   $suggest_all = array();
	   $sum = array() ;
	   $sum_all = array() ;
	   while ($row = $res->FetchRow()) 
	   {
	   		$suggest[$row['location']][$row['gender']] = $row['cu'] ;
	   		$suggest_all[$row['location']][$row['gender']] = $row['ca'] ;
	   		$sum[$row['gender']] += $row['cu'];
	   		$sum_all[$row['gender']] += $row['ca'];
	   }
	   $suggest['Total'] = $sum ;
	   $suggest_all['Total'] = $sum_all ;
	  
	 //  var_dump($suggest) ; var_dump($suggest_all);
	   $this->smarty->assign('suggest',$suggest) ;
	   $this->smarty->assign('suggest_all',$suggest_all) ;
   }
   
   private function getAcceptedSuggestions() 
   {
   	   $sql = "select  case when ud.city_id in ($this->ncr) then 'Delhi / NCR' when gc.city_id is null then gs.name else gc.name end as location, "
   	   		."u.gender, count(distinct(u.user_id)) as cu, count(*) as ca  from user_deal_icon_shown ud join user u on u.user_id = ud.user_id "
	        ."join user_deals udd on u.user_id = udd.user1 "
	        ."left join geo_city gc on gc.city_id = ud.city_id and gc.state_id = ud.state_id join geo_state gs on gs.state_id = ud.state_id "
			."where udd.status ='accepted' and created_tstamp >= '$this->start' and created_tstamp < '$this->end' and "
			."u.user_id not in ($this->excluded_ids) group by location,gender";
   	   
	   $res = $this->conn_reporting->Execute($sql) ;
	   $accept_suggest = array();
	   $accept_suggest_all = array();
	   $sum = array() ;
	   $sum_all = array() ;
	   
	   while ($row = $res->FetchRow()) 
	   {
	   		$accept_suggest[$row['location']][$row['gender']] = $row['cu'] ;
	   		$accept_suggest_all[$row['location']][$row['gender']] = $row['ca'] ;
	   		$sum[$row['gender']] += $row['cu'];
	   		$sum_all[$row['gender']] += $row['ca'];
	   }
	   
	   $accept_suggest['Total'] = $sum;
	   $accept_suggest_all['Total'] = $sum_all;
	  // var_dump($accept_suggest) ; var_dump($accept_suggest_all) ;
	   $this->smarty->assign('accept_suggest',$accept_suggest) ;
	   $this->smarty->assign('accept_suggest_all',$accept_suggest_all) ;
   }
   
   private function getPairsSuggestWithoutAccept()
   {
   		$sql =  "select  case when ud.city_id in ($this->ncr) then 'Delhi / NCR'  when gc.city_id is null then gs.name else gc.name end as location, "
		       	."u.gender, count(distinct(pairs)) as cu,count(distinct(user1)) as ca from user_deal_icon_shown ud join user u on u.user_id = ud.user_id "
		       	."join (select user1,concat(user1,user2) as pairs,sum(if(status='accepted',1,0)) as acc, count(*) as cu from user_deals "
		       	."where created_tstamp >= '$this->start' and created_tstamp < '$this->end'"
		       	."group by pairs having cu>1 and acc =0)t on t.user1 = u.user_id "
			   	."left join geo_city gc on gc.city_id = ud.city_id and gc.state_id = ud.state_id join geo_state gs on gs.state_id = ud.state_id "
				." where user1 not in ($this->excluded_ids) group by location, u.gender" ;
   		$res = $this->conn_reporting->Execute($sql);
   		$pair_not_accept =array();
   		$pair_not_accept_users = array();
   		$sum = array() ;
   		$sum_all = array() ;
   		
   		while ($row = $res->FetchRow())
   		{
   			$pair_not_accept[$row['location']][$row['gender']] = $row['cu'] ;
   			$pair_not_accept_users[$row['location']][$row['gender']] = $row['ca'] ;
   			$sum[$row['gender']] += $row['cu'];
   			$sum_all[$row['gender']] += $row['ca'];
   		}
   		
   		$pair_not_accept['Total'] = $sum ;
   		$pair_not_accept_users['Total'] = $sum_all ;
   		
   		$this->smarty->assign('pair_not_accept',$pair_not_accept) ;
   		$this->smarty->assign('pair_not_accept_users',$pair_not_accept_users) ;
   }
   
   
   private function getPairsSuggestAndAccept()
   {
   		$sql =  "select count(*) as cu from (select user1,concat(user1,user2) as pairs, sum(if(status='accepted',1,0)) as acc, count(*) as cu from"
   				." user_deals where user1 not in ($this->excluded_ids) and created_tstamp >= '$this->start' and created_tstamp < '$this->end' group by pairs having cu>1 and acc >0)t" ;
   		$res = $this->conn_reporting->Execute($sql);
   		$pair_accept = $res->FetchRow();
   						   							 
   		$this->smarty->assign('pair_accept',$pair_accept['cu']) ;
   }
   
   private function getPairAcceptMultiple()
   {
   		$sql =  "select count(*) as cu from (select user1,concat(user1,user2) as pairs,sum(if(status='accepted',1,0)) as acc, count(*) as cu from "
   				."user_deals where user1 not in ($this->excluded_ids) and created_tstamp >= '$this->start' and created_tstamp < '$this->end' group by pairs having cu>1 and acc >1)t" ;
   		$res = $this->conn_reporting->Execute($sql);
   		$pair_accept_multi = $res->FetchRow();
   						   							 
   		$this->smarty->assign('pair_accept_multi',$pair_accept_multi['cu']) ;
   }
   
   private function getViewed()
   {
   		$sql = "select  case when ud.city_id in ($this->ncr) then 'Delhi / NCR' when gc.city_id is null then gs.name else gc.name end as location, "
   			  ."u.gender, sum(sumi) as cu from user_deal_icon_shown ud join user u on u.user_id = ud.user_id "
              ."join  (select user_id, count(distinct(concat(user_id,event_status))) as sumi from action_log_new where activity='datespots' "
              ."and event_type='lets_go_viewed' and tstamp>='$this->start'  and tstamp <'$this->end' group by user_id)d on u.user_id = d.user_id "
 	          ."left join geo_city gc on gc.city_id = ud.city_id and gc.state_id = ud.state_id join geo_state gs on gs.state_id = ud.state_id "
			  ."group by location,gender" ;
   		$res = $this->conn_reporting->Execute($sql) ;
   		$viewed = array();
   		$sum = array();
   		
   		while ($row = $res->FetchRow()) 
   		{
   			$viewed[$row['location']][$row['gender']] = $row['cu'] ;
   			$sum[$row['gender']] += $row['cu'];
   		}
   		$viewed['Total'] = $sum ;
   		$this->smarty->assign('viewed',$viewed) ;
   }
   
   private function venues() 
   {
   		$sql = "select d.name,case when d.city_id in ($this->ncr) then 'Delhi / NCR'
            	when gc.city_id is null then gs.name else gc.name end as location_deal, location_count,
            	sum(if(gender='F',1,0))  as fca, sum(if(gender='F' && ud.status='accepted',1,0))  as fcu,
            	sum(if(gender='M',1,0))  as mca, sum(if(gender='M' && ud.status='accepted',1,0))  as mcu
   			    from user_deals ud 
				join datespots d on ud.datespot_hash_id = d.hash_id
				join user u on ud.user1= u.user_id
				left join geo_city gc on gc.city_id = d.city_id and gc.state_id = d.state_id
				join geo_state gs on gs.state_id = d.state_id
				where u.user_id not in ($this->excluded_ids) and created_tstamp >= '$this->start' and created_tstamp < '$this->end'
				group by d.hash_id order by location_deal ";
   		
   		$res = $this->conn_reporting->Execute($sql);
   		$venue_suggested= array();
   		while ($row = $res->FetchRow())
   		{
   			$venue_suggested[] = $row ;
   		}
   		$this->smarty->assign('venues',$venue_suggested) ;
   } 
}
 
try 
{
    	if(php_sapi_name() == 'cli')
    	{
			$isttime= date ( 'Y-m-d H:i:s');
			$start_date=date('Y-m-d 00:00:00', strtotime('-1 day', strtotime($isttime)));
			$end_date = date ( 'Y-m-d 00:00:00');
			$date = date ( 'Y-m-d' ,strtotime ('-1 day',strtotime($isttime) ) );
			$dates = new datesReport();
			$dates->getDatesReport($start_date, $end_date) ;
			$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/dateReport.tpl');
			//echo $message ;
			$from = "admintm@trulymadly.com" ;
			$to = $emailIdsForReporting['date_report'] ;
			//$to ="sumit@trulymadly.com" ;
			$subject = 'Datelicious Report '.$date;
			Utils::sendEmail($to, $from, $subject, $message,true);
		}
	
} catch (Exception $e) 
{
	trigger_error($e->getMessage(), E_USER_WARNING);
	echo $e->getMessage();
	Utils::sendEmail("sumit@trulymadly.com, shashwat@trulymadly.com", "sumit@trulymadly.com", "Datelicious reporting failed ".$baseurl, $e->getMessage()) ;
}


?>