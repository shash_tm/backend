<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once  dirname ( __FILE__ ) . "/../../google_analytics/HelloAnalytics.php"; 	
require_once  dirname ( __FILE__ ) . "/scenesReport.php";
require_once  dirname ( __FILE__ ) . "/sharedPhotoReport.php";

$options=getopt ( "d:f:" );
if ($options[d])
{
	$dur = $options[d];
}

  
$isttime= date ( 'Y-m-d H:i:s');
$hour = date ( 'm-d H:i' ,strtotime($isttime) );
$date = date ( 'Y-m-d' ,strtotime ('-1 day',strtotime($isttime) ) );
$one_week_date=date('Y-m-d 00:00:00', strtotime('-7 day', strtotime($isttime)));
$end_date = date ( 'Y-m-d 00:00:00');

if($dur == 'hour')
{    
	$start_date=date('Y-m-d H:i:s', strtotime('-1 hour', strtotime($isttime)));	
	$end_date = $isttime ;
}
 else 
{
	$start_date=date('Y-m-d 00:00:00', strtotime('-1 day', strtotime($isttime)));
	$end_date = date ( 'Y-m-d 00:00:00');
}


class performanceReport {
	protected $conn_reporting;
	public static $android_2G = "Android2G" ;
	public static $android_3G = "Android3G" ;
	public static $android_Wifi = "AndroidWifi" ;
	public static $ios_2G = "IOS2G" ;
	public static $ios_3G = "IOS3G" ;
	public static $ios_Wifi = "IOSWifi" ;
	public static $android = "Android" ;
	public static $ios = "IOS" ;
    public static $ga_report_array = array('ack_received_1','ack_timed_out_1','error_1');
    public static $ga_main_column = 'sent_1' ;
//	private $analytics ;
//	private $ana_profile ;
	public $info_array = array("Facebook" => array("activity" => "'login','signup'", "event_info" => "'fb_server_call'" ),
	                            "Linkedin" => array("activity" => "'trustbuilder','trustbuilder_register'","event_info"=> "'linkedin_server_call'"),
								"Phone Verification" => array("activity" => "'trustbuilder','trustbuilder_register'","event_info"=> "'phone_server_call'")
	);




	function __construct() {
		global $conn_reporting;
		$this->conn_reporting = $conn_reporting;
		$this->info_array = $info_array;
//		$this->anaObject = new HelloAnalytics() ;
//		$this->analytics = $this->anaObject->getService() ;
//		$this->ana_profile = $this->anaObject->getFirstprofileId($this->analytics) ;
	}
	/*Time taken for different events*/
	function getPerformance($start, $end) {
		// echo "running query";
		$n_3g = '"NetworkClass":"3G"' ;
		$n_4g = '"NetworkClass":"4G"' ;
		$n_2g = '"NetworkClass":"2G"' ; 
		$n_wifi = '"NetworkClass":"WIFI"' ;
		$n_cellular = '"NetworkClass":"CELLULAR"';
		   
		/* $sql="select ( case when event_info regexp '$n_3g' || event_info regexp '$n_4g'   then '3G4G'  else 
		(case when event_info regexp '$n_2g' then '2G' else 
(case when event_info regexp '$n_wifi' then 'WIFI' else ( case when event_info regexp '$n_cellular' then 'CELLULAR' else
 'other' end ) end) end ) end  ) as network, case when aln.event_type ='page_load'  then concat('Loading ',aln.activity)
 else (case when aln.activity='matches' && aln.event_type in ('fetching images','fetching_images') then 'Fetching Images' else 
 (case when aln.activity in ('login','signup','sign_up') && aln.event_type='fb_server_call' then 'FB Signup/Login' else 
 (case when aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  = 'linkedin_server_call' then 'Linkedin Connection' else 
 (case when aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  = 'phone_server_call' then 'Phone Verification' else 
  (case when aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  = 'id_server_call' then 'Uploading PhotoId' else 
  (case when  aln.activity='photo' && aln.event_type in ('take_from_camera_upload','add_from_computer_upload') then 'Uploading Images'else 
  (case when aln.activity ='register_basics' && aln.event_type ='register_server_save_call' then 'Register save' else 'other' 
  end) end ) end ) end ) end ) end) end) end as actions, source,count(*) as all_calls,gender,
sum(if((aln.event_status='success' or aln.event_status is null) && aln.time_taken =0 ,1,0)) as zero,
sum(if((aln.event_status='success' or aln.event_status is null) && aln.time_taken >0  && aln.time_taken <5 ,1,0)) as five,
sum(if((aln.event_status='success' or aln.event_status is null) && aln.time_taken >=5 && aln.time_taken < 10 ,1,0)) as ten,
sum(if((aln.event_status='success' or aln.event_status is null) && aln.time_taken >=10 && aln.time_taken < 20 ,1,0)) as twenty,
sum(if((aln.event_status='success' or aln.event_status is null) && aln.time_taken >=20 && aln.time_taken < 30 ,1,0)) as thirty,
sum(if((aln.event_status='success' or aln.event_status is null) && aln.time_taken >=30,1,0)) as above_thirty , 
sum(if(aln.event_status='error',1,0)) as error, sum(if(aln.event_status='error_timeout',1,0)) as error_timeout,
sum(if(aln.event_status not in  ('error','error_timeout','success') && aln.event_status is not null,1,0)) as error_code
from action_log_new aln left join user u on aln.user_id=u.user_id where tstamp >= '$start' and tstamp <'$end' and admin_id is null and 
sdk_version not in ('1.0.1063','1.0.1062') and 
( (aln.activity='matches' and  aln.event_type in ('fetching images','fetching_images')  )
 or (aln.activity in ('login','signup','sign_up') and aln.event_type='fb_server_call' )
 or (aln.activity in ('register_basics') and aln.event_type = 'register_server_save_call' )
 or (aln.activity in ('messages','mutual_like','matches','edit_preference','trustbuilder','trustbuilder_register','conversation_list') and aln.event_type='page_load')
 or ( aln.activity='photo' and aln.event_type in ('taken_from_camera_upload','add_from_computer_upload'))
 or (aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  in ('linkedin_server_call','phone_server_call','id_server_call'))
 ) group by source, actions, gender, network "; */
		
		  
		$sql="select ( case when event_info regexp '$n_3g' || event_info regexp '$n_4g'   then '3G4G'  else 
		(case when event_info regexp '$n_2g' then '2G' else 
(case when event_info regexp '$n_wifi' then 'WIFI' else ( case when event_info regexp '$n_cellular' then 'CELLULAR' else
 'other' end ) end) end ) end  ) as network, case when aln.event_type ='page_load'  then concat('Loading ',aln.activity)
 else (case when aln.activity='matches' && aln.event_type in ('fetching images','fetching_images') then 'Fetching Images' else 
 (case when aln.activity in ('login','signup','sign_up') && aln.event_type='fb_server_call' then 'FB Signup/Login' else 
 (case when aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  = 'linkedin_server_call' then 'Linkedin Connection' else 
 (case when aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  = 'phone_server_call' then 'Phone Verification' else 
  (case when aln.activity in ('scenes')   then concat('Scenes ', aln.event_type) else
  (case when aln.activity in ('photo_share')   then concat('Photo Share ', aln.event_type) else
  (case when aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  = 'id_server_call' then 'Uploading PhotoId' else
  (case when  aln.activity='photo' && aln.event_type in ('take_from_camera_upload','add_from_computer_upload') then 'Uploading Images'else
  (case when aln.activity ='register_basics' && aln.event_type ='register_server_save_call' then 'Register save' else 'other' 
  end) end ) end )end ) end ) end ) end) end) end)end as actions, source,count(*) as all_calls,gender,
sum(if((aln.event_status='success' or aln.event_status is null  or aln.activity='scenes') && aln.time_taken <5 ,1,0)) as five,
sum(if((aln.event_status='success' or aln.event_status is null  or aln.activity='scenes') && aln.time_taken >=5 && aln.time_taken < 10 ,1,0)) as ten,
sum(if((aln.event_status='success' or aln.event_status is null  or aln.activity='scenes') && aln.time_taken >=10 ,1,0)) as twenty,
sum(if(aln.event_status not in  ('success') && aln.event_status is not null && aln.activity not in ('scenes'),1,0)) as error_code
from action_log_new aln left join user u on aln.user_id=u.user_id where tstamp >= '$start' and tstamp <'$end' and admin_id is null and 
sdk_version not in ('1.0.1063','1.0.1062') and 
( (aln.activity='matches' and  aln.event_type in ('fetching images','fetching_images')  )
 or (aln.activity in ('login','signup','sign_up') and aln.event_type='fb_server_call' )
 or (aln.activity in ('register_basics') and aln.event_type = 'register_server_save_call' )
 or (aln.activity in ('messages','mutual_like','matches','edit_preference','trustbuilder','trustbuilder_register','conversation_list') and aln.event_type='page_load')
 or ( aln.activity='photo' and aln.event_type in ('taken_from_camera_upload','add_from_computer_upload'))
 or (aln.activity='scenes' and aln.event_type in ('list_following','list_event','details_event','follow_event') )
 or (aln.activity='photo_share' and aln.event_type in ('upload','download_thumbnail','download_image')  and aln.event_status !='cancel'  )
 or (aln.activity in ('trustbuilder','trustbuilder_register')  and aln.event_type  in ('linkedin_server_call','phone_server_call','id_server_call'))
 ) group by source, actions, gender, network ";
     
		
	//	echo $sql; die ;
		$res= $this->conn_reporting->Execute($sql);
		$perf_array= $res->GetRows ();
		echo "query ran";
		$performance= array();
		$i=0;
		foreach ($perf_array as $key=>$val) {
			$performance[$i] = $val;
			/*
			$all= $val['zero']+$val['five'] + $val['ten'] + $val['twenty'] + $val['thirty'] + $val['above_thirty'] + $val['error'] + $val['error_timeout'] + $val['error_code'];
			$performance[$i]['zero_p']=  round ( 100 * ($val['zero'] / $all) );
			$performance[$i]['five_p']=  round ( 100 * ($val['five'] / $all) );
			$performance[$i]['ten_p']=  round ( 100 * ($val['ten'] / $all) );
			$performance[$i]['twenty_p']=  round ( 100 * ($val['twenty'] / $all) );
			$performance[$i]['thirty_p']=  round ( 100 * ($val['thirty'] / $all) );
			$performance[$i]['above_thirty_p']=  round ( 100 * ($val['above_thirty'] / $all) );
			$performance[$i]['error_p']=  round ( 100 * ($val['error'] / $all) );
			$performance[$i]['error_code_p']=  round ( 100 * ($val['error_code'] / $all) );
			$performance[$i]['error_timeout_p']=  round ( 100 * ($val['error_timeout'] / $all) ); */
			
			$all= $val['five'] + $val['ten'] + $val['twenty']  + $val['error_code'];
			$performance[$i]['five_p']=  round ( 100 * ($val['five'] / $all) );
			$performance[$i]['ten_p']=  round ( 100 * ($val['ten'] / $all) );
			$performance[$i]['twenty_p']=  round ( 100 * ($val['twenty'] / $all) );
			$performance[$i]['error_code_p']=  round ( 100 * ($val['error_code'] / $all) );
			$i++;
		}
		
		$net_array = array();
		foreach ($performance as $tup)
		{
		  $net_array[$tup['source'] . $tup['actions'].$tup['gender']] [$tup['network']] = $tup ;
		  if ($tup ['source'] ==  'android_app' && $tup ['network'] == '3G4G' && $tup['actions'] == 'Loading matches')
		  {
		  	 $subjectInfo['all'] = $tup ['all_calls'] ;
		  	 $subjectInfo['perc'] = $tup ['five_p'] ; 
		  }
		}
       
		$penultimate_arr = array() ;
		foreach ($net_array as $key => $value) 
		{  
		   $sum = 0 ;
		   $temp_array = array();
		   foreach ($value as $par)
		   {
		   	  $sum += $par['all_calls'] ;
		   }
		  $temp_array ['source'] = $par ['source'];
		  $temp_array ['actions'] = $par ['actions'];
		  $temp_array ['gender'] = $par ['gender'];
		  $temp_array ['all_calls']  = $sum;
		  
		   foreach ($value as $v)
		   {
		   	$temp_array[$v['network']] = $v;
		   	$temp_array[$v['network']] ['portion'] = round(100 * ($v['all_calls'] / $sum)) ;
		   }
		   
		   $penultimate_arr[] = $temp_array ;
		  // var_dump($temp_array);
 		}
		return array ('all_data' => $penultimate_arr ,
		              'subject_data' => $subjectInfo )	;
		 
	}

	/*Error counts for facebook linkdin and phone verification*/
	public function getErrors($start, $end) {
		$errorData = array();
		$new_arr =	array("Facebook" => array("activity" => "'login','signup'", "event_type" => "'fb_server_call'" ),
	                  "Linkedin" => array("activity" => "'trustbuilder','trustbuilder_register'","event_type"=> "'linkedin_server_call'"),
					  "Phone Verification" => array("activity" => "'trustbuilder','trustbuilder_register'","event_type"=> "'phone_server_call'")
		);
		/*echo $this->info_array;
		 var_dump($this->info_array);*/
		foreach ($new_arr as $key => $val){
			/*echo $key.PHP_EOL;
			 var_dump($val);*/
			$errorData[$key] = $this->getErrorCount($start, $end, $val['activity'] , $val['event_type']);
		}
		return $errorData;
	}

	/*Message stats */
	public function messagesStats ($start, $end){

		$sql_all="select sum(if(event_status ='sent',1,0)) as sent, sum(if(event_status in ('ack_received','retry_ack_received'),1,0)) as received,
				CAST(100*( sum(if(event_status in ('ack_received','retry_ack_received'),1,0)) /sum(if(event_status ='sent',1,0)) ) AS unsigned) as perc 
				from action_log_new where  tstamp >= '$start' and tstamp <'$end' and  sdk_version not in ('1.0.1063','1.0.1062') and activity='socket' and  event_type='chat_sent' 
				and event_status in ('sent','ack_received','retry_ack_received')";

		$res_all= $this->conn_reporting->Execute($sql_all);
		$msg_all = array();
		if($res_all->RowCount()>0){
			while ($row = $res_all->FetchRow()){
		  $msg_all= $row;
		  	
		 }
		}


		/*		$sql_version= "select sdk_version, sum(if(event_status ='sent',1,0)) as sent,
		 sum(if(event_status in ('ack_received','retry_ack_received'),1,0)) as received,
		 CAST(100*( sum(if(event_status in ('ack_received','retry_ack_received'),1,0)) /sum(if(event_status ='sent',1,0)) ) AS unsigned) as perc
		 from action_log_new where tstamp >= '$start' and tstamp <'$end' and
		 activity='socket' and   event_type='chat_sent' and event_status in ('sent','ack_received','retry_ack_received')
		 group by sdk_version ";

		 $res_version = $this->conn_reporting->Execute($sql_version);
		 $msg_version = array();

		 if($res_version->RowCount()>0){
		 while ($row = $res_version->FetchRow()){
		 $msg_version[] = $row ;
		 }
		 }

		 $sql_error = "select sdk_version,count(*) as c_u from action_log_new where tstamp >= '$start' and tstamp <'$end' and activity='socket'
		 and event_type='error_connection' group by sdk_version";

		 $res_error = $this->conn_reporting->Execute($sql_error);
		 $error_connections = array();

		 if($res_error->RowCount()>0){
		 while ($row = $res_error->FetchRow()){
		 $error_connections[$row['sdk_version']] =  $row['c_u'];
		 }
		 }

		 $sql_socket = "select count(distinct(user_id)) as c_u from action_log_new where activity='socket' AND tstamp>='$start' AND tstamp<'$end' " ;
		 $res_socket = $this->conn_reporting->Execute($sql_socket);
		 $socket_count = $res_socket->FetchRow() ;


		 $sql_poll = "select count(sender_id)  as c_u from (select distinct(sender_id) from messages_queue where tstamp > '$start')  m
		 left join  (select distinct(user_id) from action_log_new where activity='socket' AND tstamp>='$start'
		 AND tstamp<'$end' ) s on m.sender_id=s.user_id where s.user_id is null" ;
		 $res_poll = $this->conn_reporting->Execute($sql_poll);
		 $poll_count = $res_poll->FetchRow () ;
		  

		 $sql_retry = $this->conn_reporting->Prepare (' SELECT count(unique_id)  as c_u FROM (SELECT DISTINCT(SUBSTR(event_info,t.start,t.end-t.start))AS unique_id,
		 GROUP_CONCAT(CONCAT(event_status)) AS groups FROM ( SELECT LOCATE("unique_id\":\"",event_info)+12 AS START,
		 LOCATE("\"",event_info,LOCATE("unique_id\":\"",event_info)+12) AS END, event_info,event_status FROM
		 action_log_new WHERE tstamp >= ? AND tstamp < ? AND activity = "socket" AND
		 event_type  ="chat_sent" AND event_status IN ("sent","ack_received","retry_ack_received","manual_retry--sent") )t
		 GROUP BY unique_id )s WHERE groups REGEXP "manual_retry--sent" ' );

		 $res_retry = $this->conn_reporting->Execute($sql_retry, array($start,$end)) ;
		 $retry_count = $res_retry->FetchRow();
		 $failed = array();
		 $sql_failed = "select event_type, count(*) as c_u from action_log_new where tstamp>='$start' and tstamp < '$end'
		 and event_status='ack_timed_out' group by event_type";
		 $res_failed = $this->conn_reporting->Execute ($sql_failed) ;
			if($res_failed->RowCount()>0){
			while ($row = $res_failed->FetchRow()){
			$failed [] = $row ;
			}
			}
				
				
			$sql_one_fail = 'SELECT count(*)  as c_u FROM (SELECT DISTINCT(SUBSTR(event_info,t.start,t.end-t.start))AS unique_id,
			GROUP_CONCAT(event_status) AS groups FROM ( SELECT LOCATE("unique_id\":\"",event_info)+12 AS START,
			LOCATE("\"",event_info,LOCATE("unique_id\":\"",event_info)+12) AS END, event_info,event_status FROM action_log_new
			WHERE tstamp >=  ? AND tstamp <  ? AND activity = "socket" AND event_type  ="chat_sent" AND event_status IN
			("sent","ack_received","retry_ack_received","manual_retry_sent") )t
			GROUP BY unique_id )s WHERE groups NOT regexp "ack_received" ' ;
			$res_one_fail = $this->conn_reporting->Execute($sql_one_fail, array($start,$end)) ;
			$one_fail_count = $res_one_fail->FetchRow();

          */




			$sql_time = " SELECT SUM(IF(time_taken <=1,1,0)) AS one, SUM(IF(time_taken >1 &&time_taken <=5 ,1,0)) AS five,
			SUM(IF(time_taken >5 &&time_taken <=10 ,1,0)) AS ten, SUM(IF(time_taken >10,1,0 )) AS greater10 FROM action_log_new WHERE
			tstamp >= '$start' AND tstamp < '$end' AND 
			sdk_version not in ('1.0.1063','1.0.1062') and activity = 'socket' AND event_type = 'chat_sent' AND event_status ='ack_received'" ;
			$res_time = $this->conn_reporting->Execute ($sql_time) ;
			$time_taken = $res_time->FetchRow();


		return array (     "all" => $msg_all,
					   "version" => $msg_version, 
					    "errors" => $error_connections,
		                "socket" => $socket_count['c_u'],
		                  "poll" => $poll_count['c_u'],
						 "retry" => $retry_count['c_u'],
		                "one_fail" => $one_fail_count['c_u'],
		//"one_fail" => $one_fail_count['c_u'],
		            "time_taken" => $time_taken,
		           "api_fail"  => $failed
		);
	}

	public function getGAChatData ()
	{
	 global $smarty ;
	 
	 $segment_android_2G = "sessions::condition::ga:operatingSystem==Android;ga:dimension5==2G" ;
	 $segment_android_3G = 'sessions::condition::ga:operatingSystem==Android;ga:dimension5==3G,ga:dimension5==4G' ;
	 $segment_android_Wifi = 'sessions::condition::ga:operatingSystem==Android;ga:dimension5==WIFI' ;
	 $segment_ios_2G = 'sessions::condition::ga:operatingSystem==iOS;ga:dimension5==2G' ;
	 $segment_ios_3G = 'sessions::condition::ga:operatingSystem==iOS;ga:dimension5==3G,ga:dimension5==4G' ;
	 $segment_ios_Wifi = 'sessions::condition::ga:operatingSystem==iOS;ga:dimension5==Wifi' ;
	 
	 // sessions::condition::ga:appVersion==2.0.2,ga:appVersion==2.0.3;ga:dimension5==WIFI 
	 
	 $android_key = "Android_2G" ;
	 
	// $segment_array = array(  "Andro = $segment_android_2G  );
	 
	 $segment_array =  array( performanceReport::$android_2G => $segment_android_2G,
	                    	  performanceReport::$android_3G => $segment_android_3G,
	                  		performanceReport::$android_Wifi => $segment_android_Wifi,
						          performanceReport::$ios_2G => $segment_ios_2G,
	    				 		  performanceReport::$ios_3G => $segment_ios_3G,
	                            performanceReport::$ios_Wifi => $segment_ios_Wifi ) ;
	 

	 $anaObj = new HelloAnalytics();

	 $analytics = $anaObj->getService();
	 $profile = $anaObj->getFirstProfileId($analytics);
	 
	 
	 // for connection
	 foreach ($segment_array as $row => $val ) 
	 {
	 	  $extra_conn = array(  'dimensions' => 'ga:eventLabel',
         						   'filters' => 'ga:eventAction==connection',
	  								  'sort' => '-ga:totalEvents' ,
    	                           'segment' => $val   ) ;
	 	   
	 	$request_connection = array ('start_date' => 'yesterday',
                        			   'end_date' => 'yesterday',
                                        'metrics' => 'totalEvents',
                                          'extra' => $extra_conn) ; 
	 	
	 $connection [ $row ] = $anaObj->getResults($analytics, $profile, $request_connection) ;
	 } 
	 
	 $connection = $this->segregateOS($connection) ;
	 $connection = $this->joinNetworks($connection);
	 $connection = $this->calculatePercentage($connection);
	
	 
	 foreach ($segment_array as $row => $val) 
	 {
	 	$extra_missed =  array(  'dimensions' => 'ga:eventLabel',
         						    'filters' => 'ga:eventAction==get_missed_messages',
	  								   'sort' => '-ga:totalEvents' ,
    	                            'segment' => $val   ) ;
	 	$request_missed = array ('start_date' => 'yesterday',
                        			   'end_date' => 'yesterday',
                                        'metrics' => 'totalEvents',
                                          'extra' => $extra_missed) ;
	 	$missed [ $row ] = $anaObj->getResults($analytics, $profile, $request_missed) ; 
	 }
	 
	   $missed = $this->segregateOS($missed) ;
	   $missed = $this->joinNetworks($missed) ;  
	   $missed = $this->calculatePercentage($missed);

	   
	 
	 foreach ($segment_array as $row => $val) 
	 {
	 	$extra_chat = array(  'dimensions' => 'ga:eventLabel',
         						 'filters' => 'ga:eventAction==chat_sent',
	  						        'sort' => '-ga:totalEvents' ,
    	                         'segment' => $val   ) ;
	 	$request_chat = array ('start_date' => 'yesterday',
                        	       'end_date' => 'yesterday',
                                    'metrics' => 'totalEvents',
                                      'extra' => $extra_chat) ;
	 	$chat [ $row ] = $anaObj->getResults($analytics, $profile, $request_chat) ;
	 }
	 
	 $chat = $this->segregateOS($chat) ;
	 $chat = $this->joinNetworks($chat) ;
	 $chat = $this->calculatePercentage($chat);
	 
	   
	 foreach ($segment_array as $row => $val) 
	 {
	 	$extra_disconnect = array(  'dimensions' => 'ga:eventLabel',
         						 'filters' => 'ga:eventAction==disconnect',
	  						        'sort' => '-ga:totalEvents' ,
    	                         'segment' => $val   ) ;
	 	$request_disconnect = array ('start_date' => 'yesterday',
                        	       'end_date' => 'yesterday',
                                    'metrics' => 'totalEvents',
                                      'extra' => $extra_disconnect) ;
	 	$disconnect [ $row ] = $anaObj->getResults($analytics, $profile, $request_disconnect) ; 
	 }
	 
	 $disconnect = $this->segregateOS($disconnect) ;
	 $disconnect = $this->joinNetworks($disconnect, true) ;
	 
	 
	// print_r(json_encode($disconnect)) ; die ; 
	
      $smarty->assign ('connection' , $connection ) ;  
      $smarty->assign ('missed' , $missed ) ;
      $smarty->assign ('chat' , $chat ) ;
      $smarty->assign ('disconnect', $disconnect  );
	}
	
	private function segregateOS ($data)
	{
		$seg_array = array () ;
		$seg_array [ performanceReport::$android ] [ performanceReport::$android_Wifi ] = $data [ performanceReport::$android_Wifi ] ;
		$seg_array [ performanceReport::$android ] [ performanceReport::$android_3G ] = $data [ performanceReport::$android_3G ] ;
		$seg_array [ performanceReport::$android ] [ performanceReport::$android_2G ] = $data [ performanceReport::$android_2G ] ;
		$seg_array [ performanceReport::$ios ] [ performanceReport::$ios_Wifi ] = $data [ performanceReport::$ios_Wifi ] ;
		$seg_array [ performanceReport::$ios ] [ performanceReport::$ios_3G ] = $data [ performanceReport::$ios_3G ] ;
		$seg_array [ performanceReport::$ios ] [ performanceReport::$ios_2G ] = $data [ performanceReport::$ios_2G ] ;
		
		return $seg_array ;
	}
	
	private function calculatePercentage($data)
	{
		$cal_array = array();
		foreach ($data as $key => $value) 
		{
			$temp_value = array();
			$temp_value_p = array();
			foreach ($value as $k =>$val)
			{ 
				$temp_val = array();
				$temp_val_p = array();
				foreach ($val as $category => $count)
				{
					if($k == performanceReport::$ga_main_column )
					{
						$temp_val[$category] = $count; 
					}
					else 
					{
						$temp_val[$category] = $count; 
						$temp_val_p[$category] =  round(100 * ($count/ $value[performanceReport::$ga_main_column][$category] )) . " %";
					}
					
				}
			
				$temp_value[$k] = $temp_val;
				if($k != performanceReport::$ga_main_column )
				$temp_value[$k . " %"] = $temp_val_p ;
			}
			
			$cal_array[$key] = $temp_value ;
		}
		return $cal_array ;

	}
	
	
	// for each OS all , segregate all three netwrok into one array 
	private function joinNetworks ($data, $by_force= false)
	{
	  $os_array = array();
	  foreach ($data as $os => $network_data )
	  {
	  
	  	  foreach ($network_data as $network_name => $info )
	  	  {
	  	  //	var_dump($network_name) ; die ;
	  	  	foreach ($info as $net => $val) 
	  	  	{
	  	  		/*if ( strpos($val [0] ,'xhr poll error') !== false )
	  	  		{
	  	  		//	echo "if " . $val [] .PHP_EOL;
	  	  		
	  	  		    if (strpos($val [0] ,'failed to connect to chat1.trulymadly.com') !== false) 
	  	  		    {
	  	  		    	if ($os_array [ $os ] ['xhr poll error :: failed to connect to chat1.trulymadly.com'] [ $network_name ] > 0 )
	  	  		    		$os_array [ $os ] ['xhr poll error :: failed to connect to chat1.trulymadly.com'] [ $network_name ]  += $val [1] ;
	  	  		    		else
	  	  		    		$os_array [ $os ] ['xhr poll error :: failed to connect to chat1.trulymadly.com'] [ $network_name ] = $val [1]  ;
	  	  		    } 
	  	  		    else  if (strpos($val [0] ,'Unable to resolve host') !== false) 
	  	  		    {
	  	  		    	if ($os_array [ $os ] ['xhr poll error :: Unable to resolve host'] [ $network_name ] > 0 )
	  	  		    		$os_array [ $os ] ['xhr poll error :: Unable to resolve host'] [ $network_name ]  += $val [1] ;
	  	  		    		else
	  	  		    		$os_array [ $os ] ['xhr poll error :: Unable to resolve host'] [ $network_name ] = $val [1]  ;
	  	  		    } 
	  	  		    else 
	  	  		    {
	  	  		    	if ($os_array [ $os ] ['xhr poll error :: Other'] [ $network_name ] > 0 )
	  	  		    		$os_array [ $os ] ['xhr poll error :: Other'] [ $network_name ]  += $val [1] ;
	  	  		    		else
	  	  		    		$os_array [ $os ] ['xhr poll error :: Other'] [ $network_name ] = $val [1]  ;	
	  	  		    }
	  	  		    
	  	  		}
	  	  		else 
	  	  		{
	  	  			//	echo "else " . $val [1] .PHP_EOL; 
	  	  		    $os_array [ $os ] [$val [0] ] [ $network_name ]  = $val [1]  ;	
	  	  		} */
	  	  		
	  	  		 if($val[0] == performanceReport::$ga_main_column || in_array($val[0], performanceReport::$ga_report_array)  || $by_force == true)
	  	  		 {
	  	  		 	$os_array [ $os ] [$val [0] ] [ $network_name ]  = $val [1]  ; 
	  	  		 }
	  	  	} 
	  	  }
	  }
	  
	  return $os_array ;
	}

	public function getOTPData ( $start, $end )
	{
		global  $smarty ;
		$sql = "select gender, sum(if(uo.number_of_trials > 0 ,1,0 )) as requested, sum(if(otp_tries > 0 ,1,0 )) as otp_tried,
                sum(if(uo.is_verified = 'yes' ,1,0 )) as Verified_by_otp, sum(if(uo.number_of_trials >0 && otp_tries =0 ,1,0 )) as otp_not_tried,
				sum(if( upn.is_verified > 0  ,1,0 )) as by_call
                from user_otp uo join user_phone_number upn on upn.user_id = uo.user_id join user u on u.user_id = uo.user_id 
                where uo.timestamp >= '$start' and upn.upload_time >= '$start' and uo.timestamp < '$end' and upn.upload_time < '$end' 
                group by gender" ;
		$res = $this->conn_reporting->Execute ($sql) ;
		$phone = $res->GetRows();
		$new_phone = array() ;
		foreach ($phone as $row)
		{
			$new_phone [ $row ['gender'] ] = $row ;
		}
		
		/* $sql_call = "select gender, count(*) as c_u from user_phone_number upn join user u on u.user_id = upn.user_id where upload_time >= '$start' and 
		            upload_time < '$end' and is_verified = 1 and upn.status ='active' group by gender";
		$res_call = $this->conn_reporting->Execute ($sql_call) ;
		$call = $res_call->GetRows () ;
		
		foreach ($call as $col)
		{
			$new_phone [ $col['gender'] ] ['by_call'] = $col['c_u'] ; 
		} */  
		//var_dump($new_phone) ;
		$smarty->assign ('phone' , $new_phone) ; 
	}
	
	private  function getErrorCount($start, $end,$activity, $event_type) 
	{
		$sql= "select  source, aln.event_status, count(*) as counts from action_log_new aln where tstamp >= '$start' and
   	    tstamp <'$end' and  sdk_version not in ('1.0.1063','1.0.1062') and aln.activity in ($activity) and aln.event_type in ($event_type) and admin_id is null  group by source,aln.event_status " ;
		$res= $this->conn_reporting->Execute($sql);
		$error_array= $res->GetRows ();
		return $error_array;
	}

	public function getPhotoData($start, $end)
	{
		$sql = "select  u.gender, registered_from, count(distinct(u.user_id)) as allUsers,
			count(distinct(if(profile_url is null,uf.user_id,0))) -1 as permission_issue,
 count(distinct(u.user_id)) - count(distinct(up.user_id))  as without_Photos
from user u
join user_facebook uf on u.user_id = uf.user_id
left join user_photo up on u.user_id = up.user_id  and from_fb='yes'
 where registered_at >='$start' and  registered_at <'$end'
 and (u.status ='authentic' or last_changed_status= 'authentic'
 or (deletion_status is not null and last_changed_status ='incomplete') )
 and registered_from in ('android_app','ios_app')
 group by u.gender, registered_from";
		$res = $this->conn_reporting->Execute($sql);
		$photo_data = $res->GetRows();
		return $photo_data;
	}
}

try {
	if(php_sapi_name() === 'cli')
	{
		$performance = new performanceReport();
		$data = $performance->getPerformance($start_date, $end_date); echo " Got performance ";
 		if($dur != 'hour')
 		{
            echo "getting GA ";
 			$ga_chat = $performance->getGAChatData(); echo "got GA";
 			$errors_data = $performance->getErrors($start_date, $end_date, $activity, $event_type); echo " Got errors";
 			$phone = $performance->getOTPData($start_date, $end_date) ; echo " Got OTP";
 			$photo_data = $performance->getPhotoData($start_date, $end_date) ;
 		}

          $msg_data = $performance->messagesStats($start_date, $end_date);  echo " msg stats ";
		

		$smarty->assign("data",$data ['all_data']);
		$smarty->assign("Errors_data",$errors_data);
		$smarty->assign("msg_data",$msg_data);
		$smarty->assign("ga",$ga_chat);
		$smarty->assign("photo_data",$photo_data);

	 	$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/performanceReport.tpl');
	 	//echo $message ; die;

		//$to=  'sumit.kumar@trulymadly.com,rahul@trulymadly.com,shashwat@trulymadly.com';
		if($dur == 'hour')
		{
			$to=  'shashwat@trulymadly.com, sumit@trulymadly.com ';
			$subjectinfo = $data ['subject_data'];
			$subject = 'HP '.$hour . ' Ch: ' . $msg_data['all']['sent'] . '(' . $msg_data['all']['perc'] . ' %) ' ;
			$subject .= ' MT:' . $subjectinfo['all'] . '( ' . $subjectinfo ['perc'] . ' %)';
			
		}
		else 
		{ 
			$to=  'backend@trulymadly.com, rahul@trulymadly.com, purav@trulymadly.com, tabish@trulymadly.com';
			$subject = 'Daily Performance report '.$date;		
		} 
		
		//$to = 'sumit@trulymadly.com' ; 
		$from = 'admintm@trulymadly.com'; 
		Utils::sendEmail($to, $from, $subject, $message,true);
		echo "Sent to ". $to.PHP_EOL ;

		if($dur != 'hour') {

			// check if Monday and send photo report
	if (date("w") == 1 || $options[f] == 'photo' ) {
				echo "getting photo report" . PHP_EOL;
				$photoReport = new sharedPhotoReport();
				$data = $photoReport->getStats($one_week_date, $end_date);
				$message = $smarty->Fetch(dirname(__FILE__) . '/../../templates/reporting/sharedPhotoReport.tpl');
				$to = $emailIdsForReporting['photo_sharing'];
				$subject = 'Photo Share Weekly report ' . $date;
				//$to = 'sumit@trulymadly.com' ;
				$from = 'admintm@trulymadly.com';
				Utils::sendEmail($to, $from, $subject, $message, true);
				echo "sent to " . $to;
			}


			$scene = new scenesReport();
			echo "Getting scenes report" . PHP_EOL;

			$data = $scene->getStats($start_date, $end_date);


			$message = $smarty->Fetch(dirname(__FILE__) . '/../../templates/reporting/scenesReport.tpl');
			$to = $emailIdsForReporting['date_report'];
			//$to=  'sumit@trulymadly.com';
			$subject = 'Scenes Daily Report ' . $date;
			$from = 'admintm@trulymadly.com';
			Utils::sendEmail($to, $from, $subject, $message, true);
			echo "sent to " . $to;

		}
	}
 

} catch (Exception $e) {
	trigger_error($e->getMessage(),E_USER_WARNING);
	Utils::sendEmail("backend@trulymadly.com", "sumit@trulymadly.com", "Performance Reporting failed ".$baseurl, $e->getMessage());
}

?>
