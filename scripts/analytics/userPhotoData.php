<?php 
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";

class PhotoData {
	private $conn_reporting;
	private $start;
	private $end;
	private $photo_data;
	function __construct() 
	{
		global $conn_reporting;
		$this->conn_reporting = $conn_reporting ;
		$this->start ;
		$this->end ;
		$this->photo_data ;

	}
	function getPhotosData($start, $end) 
	{
		$this->start = $start;
		$this->end = $end;
		
		// get registrations 
		$this->getReg() ;
		$this->getUploadedPhotos() ;
		$this->getApprovedPhotos() ;
		$this->getNotApprovedPhoto() ;
		$this->calculateNotApprovedPhoto() ;
		return $this->photo_data ;
		
	}
	private function getReg() 
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else ( case when is_fb_connected ='Y'  then 'android Fb Connected' 
			else 'android fb Not connected' end) end  as source , count(distinct(u.user_id)) as cu from user u  where registered_at >='$this->start' 
			and  registered_at <'$this->end' and gender='f' and steps_completed regexp 'photo' and (u.status ='authentic' or last_changed_status= 'authentic' )
			and registered_from in ('android_app','ios_app')
			group by source";
		$res = $this->conn_reporting->Execute ( $sql );
		$data = array ();
		while ( $row = $res->FetchRow () ) 
		{
			$data [$row ['source']]['reg'] = $row['cu'];
		}
		$this->photo_data = $data ;
	}
	
	private function getUploadedPhotos()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else ( case when is_fb_connected ='Y'  then 'android Fb Connected' 
				else 'android fb Not connected' end) end  as source , count(distinct(u.user_id)) as cp from user u 
				join user_photo up on u.user_id = up.user_id 
 				where registered_at >='$this->start' and  registered_at <'$this->end' and gender='f'  and steps_completed regexp 'photo'  
				and (u.status ='authentic' or last_changed_status= 'authentic' )  and registered_from in ('android_app','ios_app') group by source"; 
		$res = $this->conn_reporting->Execute ( $sql ) ;
		
		while ($row = $res->FetchRow()) 
		{
			$this->photo_data [$row ['source']]['uploaded'] = $row['cp'] ; 
		}
	}
	
	private function getApprovedPhotos ()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else ( case when is_fb_connected ='Y'  then 'android Fb Connected' 
				else 'android fb Not connected' end) end  as source ,if(uf.hide_myself = 0 || uf.hide_myself is null,'visible','not_visible') as visibility, 
                count(distinct(u.user_id)) as ca from user u 
				join user_photo up on u.user_id = up.user_id 
				left join user_flags uf on u.user_id = uf.user_id 
 				where registered_at >='$this->start' and  registered_at <'$this->end' and gender='f' and steps_completed regexp 'photo' 
 				and (u.status ='authentic' or last_changed_status= 'authentic')  and registered_from in ('android_app','ios_app')  
 				and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted' )
				group by source, visibility" ;
		$res = $this->conn_reporting->Execute ($sql) ;
		while ($row = $res->FetchRow())
		{
			if ( $row ['visibility'] == 'visible' )
				$this->photo_data [$row ['source']]['approved_visible'] = $row['ca'] ;
			else 
				$this->photo_data [$row ['source']]['approved_not_visible'] = $row['ca'] ;
		}
	}
	
	private function getNotApprovedPhoto ()
	{
		$sql = "select  case when registered_from= 'ios_app'then 'ios_app' else ( case when is_fb_connected ='Y'  then 'android Fb Connected' else 
				'android fb Not connected' end) end  as source ,if(uf.hide_myself = 0 || uf.hide_myself is null,'visible','not_visible') as visibility, 
            	count(distinct(u.user_id)) as cn from user u 
				left join user_photo up on u.user_id = up.user_id and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted' )
				join (select distinct(user1) from user_like where timestamp>='$this->start')t on t.user1= u.user_id
				left join user_flags uf on u.user_id = uf.user_id 
 				where registered_at >='$this->start' and  registered_at <'$this->end' and gender='f'  and steps_completed regexp 'photo' 
				and (u.status ='authentic' or last_changed_status= 'authentic')  and registered_from in ('android_app','ios_app') and up.user_id is null 
				group by source, visibility" ;
		$res = $this->conn_reporting->Execute ($sql ) ;
		while ($row = $res->FetchRow())
		{
			if ( $row ['visibility'] == 'visible' )
				$this->photo_data [$row ['source']]['liked_visible'] = $row['cn'] ;
			else 
				$this->photo_data [$row['source']]['liked_not_visible'] = $row['cn'] ;
		}
		
	}
	
	private function calculateNotApprovedPhoto ()
	{
		foreach ($this->photo_data as $key => $val) 
		{
			$this->photo_data[$key] ['liked_all'] = $val['liked_visible'] + $val ['liked_not_visible'] ;
			$this->photo_data[$key] ['not_approved'] = $val['uploaded'] - $val['approved_visible'] - $val['approved_not_visible'] ;
		}
	}
}

$isttime= date ( 'Y-m-d H:i:s');
$today = date('Y-m-d') ; 
$end_date = date ( 'Y-m-d 00:00:00');
$start_date=date('Y-m-d 00:00:00', strtotime('-7 day', strtotime($isttime)));

 if (php_sapi_name() =='cli' )
 {
	$photoObj = new PhotoData() ;
	$photo = $photoObj->getPhotosData($start_date, $end_date) ;
	$smarty->assign("data",$photo);
	$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/userPhotoData.tpl');
	$subject = 'Weekly Photo report '.$today;
	$to = 'sumit@trulymadly.com, rahul@trulymadly.com, shashwat@trulymadly.com' ;
	$from = 'admin@trulymadly.com' ;
	Utils::sendEmail($to, $from, $subject, $message,true);
  }


?>