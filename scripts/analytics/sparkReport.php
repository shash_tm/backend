<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../email/MailFunctions.php";


class SparkReport
{
    private $conn_reporting;
    private $smarty ;
    private $testUsers = "605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919,281684,160486,707,
    1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063,1615723,573722,433066,1285095";
    public $total_revenue;
    public $dollar_rate ;
    function __construct ()
    {
        global $conn_reporting, $smarty;
        $this->conn_reporting = $conn_reporting ;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->smarty = $smarty ;
        $this->total_revenue = 0;
        $this->setDollarRate();
    }

    public function getAllData($start_time, $end_time)
    {
        $this->getPaymentFunnel($start_time,$end_time);
        $this->getErrors($start_time,$end_time);
        $file = dirname ( __FILE__ ) . "/../../templates/reporting/sparkReport.tpl";
        $data = $this->smarty->fetch ( $file );
        return $data;
    }

    public function getSelectData($start_time, $end_time)
    {
        $this->getSelectPaymentFunnel($start_time,$end_time);
        $revenue = round($this->total_revenue,2);
        $this->smarty->assign('revenue',$revenue);
        $file = dirname ( __FILE__ ) . "/../../templates/reporting/selectReport.tpl";
        $data = $this->smarty->fetch ( $file );
        return $data;
    }

    private function getSelectPaymentFunnel($start_time, $end_time)
    {
        $sql = "select gender,ust.package_id, sp.source, ust.spark_count, ust.price,ust.payment_gateway, count(*) as all_transactions , sum(if(ust.status in ('consumed','apple_restore'),1,0)) as completed
                from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where ust.tstamp >'$start_time' and ust.tstamp <'$end_time' and ust.user_id not in ($this->testUsers) and sp.type ='select'
                group by ust.price, gender,ust.payment_gateway order by  source desc ,price,gender";
        $res = $this->conn_reporting->Execute($sql);
        $payment_data = array();
        $sum_array = array() ;
        while($row = $res->FetchRow())
        {
            $row['total_price'] = $row['price'] * $row['completed'] ;
            $payment_data[] = $row;
            $sum_array['all_transactions'] += $row['all_transactions'];
            $sum_array['completed'] += $row['completed'];
            $sum_array['total_price'] += $row['total_price'];
        }

        $this->total_revenue += $sum_array['total_price'];
        $this->smarty->assign ( "select_data", $payment_data );
        $this->smarty->assign ( "sum_payment_select", $sum_array );
        $this->smarty->assign ( "date_now", $end_time );
    }

    private function setDollarRate()
    {
        try
        {
            $url = "http://api.fixer.io/latest?symbols=INR&base=USD";
            $data = Utils::curlCall($url,null,null,"GET");
            $exchange_rate = json_decode($data,true);
            $this->dollar_rate = $exchange_rate['rates']['INR'];
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage(),E_USER_WARNING);
        }
    }


    private function getErrors($start_time, $end_time)
    {
    	$sql = "SELECT activity, case when source ='ios_app' then 'apple'
				else trim(both '\"' from JSON_EXTRACT(event_info, \"$.pg\")) end as gate, 
				trim(both '\"' from JSON_EXTRACT(event_info, \"$.error\")) as error_type, count(*) as err_count
				 FROM action_log_new Where activity in ('sparks','select') and event_type='buy'
				 and (event_status='buy_error' or (source='ios_app' and event_status='tm_error'))
				 and user_id not in ($this->testUsers) and tstamp>='$start_time'  and tstamp<'$end_time' group by activity,gate,error_type";
    	$res = $this->conn_reporting->Execute($sql);
    	$data = array();
    	$sum_data = array();
    	while ($row = $res->FetchRow())
    	{
    		$data[] = $row;
    		$sum_data[$row['activity']][$row['gate']] += $row['err_count'];
    	}
    	foreach ($data as $key=>$val)
    	{
    		$data[$key]['percent'] = round(100*($val['err_count']/$sum_data[$val['activity']][$val['gate']]));
    	}
    	//print_r(json_encode($sum_data)); 
    	//print_r(json_encode($data));die ;
    	$this->smarty->assign ( "error_data", $data);
    }

    private function getPaymentFunnel ($start_time, $end_time)
    {
        $sql = "select if(sp.currency is null,'rupee',sp.currency) as currenc,gender,ust.package_id, sp.source, ust.spark_count, ust.price,ust.payment_gateway, count(*) as all_transactions , sum(if(ust.status='consumed',1,0)) as completed
                from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where ust.tstamp >'$start_time' and ust.tstamp <'$end_time' and ust.user_id not in ($this->testUsers) and sp.type ='spark'
                group by  currenc, ust.price,ust.spark_count, gender,ust.payment_gateway order by  currenc,source desc ,price,gender";
        $res = $this->conn_reporting->Execute($sql);
        $payment_data = array();
        $sum_array = array() ;
        $prev_curr = "";
        while($row = $res->FetchRow())
        {
            if($row['currenc'] != $prev_curr && count($sum_array)>0 )
            {
                $payment_data[]= $sum_array;
                if($prev_curr == 'dollar')
                {
                    $currency_multiplier = $this->dollar_rate;
                }
                else
                {
                    $currency_multiplier =1 ;
                }
                $this->total_revenue += $currency_multiplier * $sum_array['total_price'];
                $sum_array = array();
            }
            $row['total_price'] = $row['price'] * $row['completed'] ;
            $payment_data[] = $row;
            $sum_array['all_transactions'] += $row['all_transactions'];
            $sum_array['completed'] += $row['completed'];
            $sum_array['total_price'] += $row['total_price'];
            $sum_array['currenc'] = 'total';
            $prev_curr = $row['currenc'];
        }
        $payment_data[] = $sum_array;
        if($prev_curr == 'dollar')
        {
            $currency_multiplier = $this->dollar_rate;
        }
        else
        {
            $currency_multiplier =1 ;
        }
        $this->total_revenue += $currency_multiplier * $sum_array['total_price'];

        $this->smarty->assign ( "payment_data", $payment_data );
    }
}

try
{
   if(php_sapi_name() == 'cli')
   {
       $isttime = date ( 'Y-m-d H:i:s' ,strtotime ( '+330 minutes' ) );
       $indianTime = Utils::getDateGMTTOISTfromUnixTimestamp(microtime(true));
       $start_date=date('Y-m-d 18:30:00', strtotime('-1 day', strtotime($isttime)));
       $end_date =  $isttime ;
       $spark_report = new SparkReport();
       $spark =  $spark_report->getAllData($start_date, $end_date);
       $select = $spark_report->getSelectData($start_date,$end_date);
       $data = $select .$spark;
       $conversion_rate = $spark_report->dollar_rate;
        $total_revenue = $spark_report->total_revenue;
       $subject = "Spark and Select Payment Report ". $indianTime;
       $to = $emailIdsForReporting['spark_payment'];
     //$to = "sumit@trulymadly.com";
      // Utils::sendEmail($to, 'admintm@trulymadly.com',$subject,$data, true);

       $email_array = explode(',',$to);
       $mailObject = new MailFunctions();
       $mid= $mailObject->sendMail($email_array ,$data,$data,$subject,null,null, array(),true);
   }

}
catch (Exception $e)
{
    echo $e->getMessage();
    Utils::sendEmail("sumit@trulymadly.com","sumit@trulymadly.com","Spark Payment reporting failed","Error ". $e->getMessage());
}


?>