<?php
require_once  dirname ( __FILE__ ) . "/../../include/config.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";

function notifyTechTeam($subject){
	//$to = "himanshu@trulymadly.com";
	$to =  Utils::$techTeam;
	$from = gethostname();
	Utils::sendEmail($to, $from, $subject, null);
}

function suspensionReporting(){
	global $conn, $smarty;
	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	$sql = " select usl.user_id, usl.reason, u.fname from user_suspension_log usl left join user u on usl.user_id = u.user_id  where admin_id is null and date(timestamp) = date_sub(CURDATE(), interval 1 day) ";
	//echo $sql;
	$res = $conn->Execute($sql);
	$rows = $res->GetRows();
	$smarty->assign("details", $rows);

	$subject = "Suspension Statistics: ". date('d-m-Y',strtotime("-1 days"));;
	Utils::sendEmail(Utils::$managementTeam , "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/suspensionReport.tpl'), TRUE);
	//Utils::sendEmail("himanshu@trulymadly.com", "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/suspensionReport.tpl'), TRUE);
	
}

try{
	suspensionReporting();
}
catch(Exception $e){
	notifyTechTeam("Payment Reporting failed");
	trigger_error($e->getTraceAsString());
}


?>