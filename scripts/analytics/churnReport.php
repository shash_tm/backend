<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php"; 


   $isttime= date ( 'Y-m-d H:i:s');
   $date = date ( 'Y-m-d' ,strtotime ('-1 day',strtotime($isttime) ) );
   $end_date = date ( 'Y-m-d 00:00:00');
   $start_date=date('Y-m-d 00:00:00', strtotime('-7 day', strtotime($isttime)));

function getChurnReport($start, $end) {
	global $conn_reporting; 
	
		$sql_pre="SET SESSION group_concat_max_len = 1000000" ;
		$res_pre= $conn_reporting->Execute($sql_pre);
		
	 $sql_purge="SELECT case when gc.name in ('Delhi','Faridabad','Greater Faridabad','Gurgaon','Noida','Greater Noida','Ghaziabad') then 'NCR' else
(case when  gc.name in ('Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur') then gc.name else 'Other' end ) end as city_grp, 
case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=21 then 'age_18' else 
(case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=22 && 
EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=24 then 'age_22' else
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=25 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=28 then 'age_25' else 
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=29 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=32 then 'age_29' else 'age_33' end )end ) end )end as age_grp,
case when dr.device_id is null then 'Organic' else 'Incent' end as acquired, u.gender, upa.bucket,
count(*) as uninstalls  FROM user u 
join user_data ud on u.user_id=ud.user_id
join user_pi_activity upa on u.user_id= upa.user_id
join user_gcm_current ugc on ugc.user_id=u.user_id
left join device_referrer dr on dr.device_id= u.device_id
left join geo_city gc on ud.stay_city=gc.city_id and ud.stay_state = gc.state_id 
where u.status ='authentic' and ugc.status='uninstall' && ugc.tstamp >= '$start' && ugc.tstamp <'$end'
group by u.gender,acquired, age_grp, upa.bucket, city_grp order by u.gender, upa.bucket,
field(city_grp,'NCR','Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur','Other')" ;
// echo $sql; 
// die; 
   $res_purge = $conn_reporting->Execute($sql_purge);
   $purge_array = getEmptyArray(0);
   	if($res_purge->RowCount()>0){
		while ($row = $res_purge->FetchRow()){
			$purge_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][$row['bucket']]['perc']=$row['uninstalls'];
		}
   	}
 
   	$misc_array= getMiscData($start, $end);
   	 
   	$sql_all = "SELECT case when gc.name in ('Delhi','Faridabad','Greater Faridabad','Gurgaon','Noida','Greater Noida','Ghaziabad') 
   	then 'NCR' else (case when  gc.name in ('Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur') 
then gc.name else 'Other' end ) end as city_grp,
case when age <=21 then 'age_18' else (case when age >=22 && age <=24 then 'age_22' else
(case when  age >=25 && age <=28 then 'age_25' else  (case when  age >=29 && age <=32 then 'age_29' 
else 'age_33' end )end ) end )end as age_grp, case when dr.device_id is null then 'Organic' else 'Incent' end as acquired,
 u.gender, bucket, count(*) as count_u from user_search us 
 left join user u on u.user_id=us.user_id
 left join device_referrer dr on dr.device_id=u.device_id
left join geo_city gc on us.city=gc.city_id and us.state = gc.state_id 
group by u.gender, acquired,age_grp, bucket, city_grp order by u.gender, bucket, 
field(city_grp,'NCR','Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur','Other')";
   
	$res = $conn_reporting->Execute($sql_all);
	$churn_array= getEmptyArray(0);
	if($res->RowCount()>0){
		while ($row = $res->FetchRow()){
			$perc=  100 * ($purge_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][$row['bucket']]['perc']/$row['count_u']);
			$churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][$row['bucket']]['perc']= intval($perc);  
			if($row['count_u']){
			if ($churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][6]) {
				$churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][6] += $row['count_u'];
			} else {
				$churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][6] = $row['count_u'];
		
			}
			} 
			  
			   $temp_array = $misc_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']];
			    $all_p= intval(100 *($temp_array['all']/$churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][6])) ;
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][7] = $temp_array['all'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][8] = $all_p; 
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][9] = $temp_array['likehide'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][10] = $temp_array['like'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][11] = $temp_array['mutual']; 
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][12] = $temp_array['uninstall']['oneday'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][13] = $temp_array['uninstall']['threeday'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][14] = $temp_array['uninstall']['7_day'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][15] = $temp_array['uninstall']['14_day'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][16] = $temp_array['uninstall']['month'];
			   $churn_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']][17] = $temp_array['uninstall']['monthplus'];
		}
	}
	
	return $churn_array;
}

function getMiscData ($start, $end){
   global $conn_reporting;
	  	$sql_misc= " SELECT case when gc.name in ('Delhi','Faridabad','Greater Faridabad','Gurgaon','Noida','Greater Noida','Ghaziabad') then 'NCR' else
(case when  gc.name in ('Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur') then gc.name else 'Other' end ) end as city_grp, 
case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=21 then 'age_18' else 
(case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=22 && 
EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=24 then 'age_22' else
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=25 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=28 then 'age_25' else 
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=29 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=32 then 'age_29' else 'age_33' end )end ) end )end as age_grp,
u.gender, case when dr.device_id is null then 'Organic' else 'Incent' end as acquired, 
count(distinct(u.user_id))  as count_u , group_concat(distinct(u.user_id))  as users FROM user u 
join user_data ud on u.user_id=ud.user_id
join user_gcm_current ugc on ugc.user_id=u.user_id
left join device_referrer dr on dr.device_id= u.device_id
left join geo_city gc on ud.stay_city=gc.city_id and ud.stay_state = gc.state_id 
where u.status ='authentic' and ugc.status='uninstall' && ugc.tstamp >= '$start' 
&& ugc.tstamp <'$end' 
group by u.gender,acquired, age_grp, city_grp order by u.gender,
field(city_grp,'NCR','Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur','Other')";
	  	
	  	
	 $res = $conn_reporting->Execute($sql_misc);
	 $misc_array= array();
	if($res->RowCount()>0){
		while ($row = $res->FetchRow()){ 
			$user_ids= $row['users'];
			 
			$sql_likeHide="select count(distinct(u.user_id))  as count_u from user u left join user_like ul on u.user_id=ul.user1 
			 left join user_hide uh on u.user_id=uh.user1 where u.user_id in ( $user_ids ) and (ul.user1 is not null or uh.user1 is not null)";
              $res_likehide = $conn_reporting->Execute($sql_likeHide);
              $likehide= $res_likehide->FetchRow();
              
              $sql_like="select count(distinct(user1)) as count_u from  user_like ul where user1 in ($user_ids)";
              $res_like = $conn_reporting->Execute($sql_like);
              $like= $res_like->FetchRow();
			 
                $sql_mutual="select count(distinct(user)) as count_u from (
                         select user1 as user from user_mutual_like ul1 where ul1.user1 in ($user_ids) group by user1 
							union 
						select user2 as user from user_mutual_like ul2 where ul2.user2 in ($user_ids) group by user2 
						 )t";
              $res_mutual = $conn_reporting->Execute($sql_mutual);
              $mutual = $res_mutual->FetchRow();
              
              $sql_retention="select count(*) as count_un,sum(if(days<=1,1,0)) as oneday, sum(if(days>1 && days <= 3,1,0)) as threeday, 
              sum(if(days>3 && days <= 7,1,0)) as 7_day, sum(if(days>7 && days <= 14,1,0)) as 14_day, sum(if(days>14 && days <= 30,1,0)) 
              as month,  sum(if(days>30,1,0)) as monthplus from ( select DATEDIFF(max(ugc.tstamp),u.registered_at) as days from user u 
               join user_gcm_current ugc on u.user_id=ugc.user_id where u.user_id in ($user_ids ) and ugc.status='uninstall' group by u.user_id) t";
              $res_retention = $conn_reporting->Execute($sql_retention);
              $retention= $res_retention->FetchRow();
              
              
              $likehide_p= intval(100* ($likehide['count_u']/$row['count_u']));
              $like_p= intval(100* ($like['count_u']/$row['count_u']));
              $mutual_p= intval(100* ($mutual['count_u']/$row['count_u'] ));
              $uninstall= array();
              $allUninstall = $retention['count_un'];
              foreach ($retention as $key=>$value) {
              	$uninstall[$key]= intval(100* ($value/$allUninstall));
              } 
              $mis = array( "all" =>$row['count_u'], 
              				"likehide" => $likehide_p,
              				"like" => $like_p,
              				"mutual" => $mutual_p,
                            "uninstall" => $uninstall );
              $misc_array[$row['gender']][$row['acquired']][$row['age_grp']][$row['city_grp']]= $mis;
			
		  }
		}
		//var_dump($misc_array);
	  	return $misc_array 	;
	  	
}

 function getUserSearchData (){
 	global $conn_reporting;
 	$sql= "SELECT case when gc.name in ('Delhi','Faridabad','Greater Faridabad','Gurgaon','Noida','Greater Noida','Ghaziabad') then 'NCR' else
(case when  gc.name in ('Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur') 
then gc.name else 'Other' end ) end as city_group, 
case when age <=21 then 'age_18' else 
(case when age >=22 && age <=24 then 'age_22' else
(case when  age >=25 && age <=28 then 'age_25' else 
(case when  age >=29 && age <=32 then 'age_29' else 'age_33' end )end ) end )end as age_group,
 gender, bucket, count(*) as count_u from user_search us 
left join geo_city gc on us.city=gc.city_id and us.state = gc.state_id 
group by gender, age_group, bucket, city_group order by gender, bucket, 
field(city,'NCR','Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Pune','Lucknow','Jaipur','Other')";
 	
 	$res = $conn_reporting->Execute($sql);
 	$user_search_array = getEmptyArray(1);
 	if($res->RowCount()>0){
		while ($row = $res->FetchRow()){
		  $user_search_array[$row['gender']][$row['age_group']][$row['city_group']][$row['bucket']]= $row['count_u']; 
		}
		}
 	return $user_search_array;
 }

function getEmptyArray($type) {
	 if($type == 0){
	 	$value= array('perc' => 0);
	 } else {
	 	$value = 0;
	 }
	 $bucket_array = array(0 => $value,
	 					   1 => $value,
	 					   2 => $value,
	 					   3 => $value,
	 					   4 => $value,
	 					   5 => $value );
      /* if($type == 0){
	 	$bucket_array[]= array('sum' => 0); 
	    }*/
	  
	 $city_array = array('NCR' => $bucket_array ,
	                     'Mumbai' => $bucket_array,
	                     'Bengaluru' => $bucket_array,
	                     'Hyderabad' => $bucket_array,
	                     'Chennai' => $bucket_array,
	                     'Kolkata' => $bucket_array,
	                     'Pune' => $bucket_array,
	                     'Lucknow' => $bucket_array,
	                     'Jaipur' => $bucket_array,
	                     'Other' => $bucket_array ); 
	 $age_array = array('age_18' => $city_array,
	                    'age_22' => $city_array,
	 					'age_25' => $city_array,
	                    'age_29' => $city_array,
	 					'age_33' => $city_array );
	 if($type == 0){
	 	$acquire_array = array('Organic' => $age_array,
	 	                       'Incent'  => $age_array);
	 } else {
	 	$acquire_array = $age_array ;
	 }
	  
	 $gender_array = array('M' => $acquire_array,
	 					   'F' => $acquire_array );
	 return $gender_array;
}

   $data = getChurnReport($start_date, $end_date);
   $user_search= getUserSearchData();
    
   
    $smarty->assign("data",$data); 
    $smarty->assign("user_search",$user_search);
    
	$message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/churnReport.tpl');
//	$to = "sumit.kumar@trulymadly.com";
$to = Utils::$contentTeam . ",sumit.kumar@trulymadly.com,rahul@trulymadly.com,shashwat@trulymadly.com"; 
echo $message; 
$subject = "Churn report for last 7 days ".$date;  
$from = "admintm@trulymadly.com";
Utils::sendEmail($to, $from, $subject, $message,true);  

echo "sent";
	
?>
	