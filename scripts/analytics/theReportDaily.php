<?php 
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php"; 


$options=getopt ( "d:" );
if ($options[d])
{
	$dur = $options[d];
}


$isttime= date ( 'Y-m-d H:i:s');
$date = date ( 'Y-m-d' ,strtotime ('-1 day',strtotime($isttime) ) );
$end_date = date ( 'Y-m-d 00:00:00');
$start_date=date('Y-m-d 00:00:00', strtotime('-1 day', strtotime($isttime)));
$week_start_date = date('Y-m-d 00:00:00', strtotime('-7 day', strtotime($isttime)));

$top_city_list = " 'Delhi', 'Faridabad', 'Greater Faridabad', 'Gurgaon', 'Noida', 'Greater Noida', 'Ghaziabad', 'Mumbai', 'Bengaluru', 'Hyderabad',
'Chennai', 'Kolkata', 'Pune', 'Lucknow', 'Jaipur'" ;

function getInstall ($start_date, $end_date)
{
	global $conn_reporting;
	$sql="SELECT COUNT(DISTINCT(device_id)) AS installs FROM user_gcm_current ugc WHERE  install_tstamp >='$start_date' AND  install_tstamp <'$end_date' ";
	$inst = $conn_reporting->Execute ( $sql );
  	$install= $inst->FetchRow (); 
  	
  	
  	$sql_uni = "select count(*) as uni from user_gcm_current where user_id is null and status= 'install' and tstamp >= '$start_date' and tstamp < '$end_date' ";
  	$uni = $conn_reporting->Execute ($sql_uni) ;
  	$uni_res = $uni->FetchRow() ;
  
  return  array( "install" => $install['installs'] ,
                 "uninstall" => $uni_res['uni']);
}

function get_Data ($start_date, $end_date)
{ 
	global $conn_reporting, $top_city_list ;
	$sql_reg = "select gender, if(gc.name in ($top_city_list),'top_ten','other') as city , count(*) as users from user u 
	join user_data ud on u.user_id=ud.user_id left join geo_city gc on ud.stay_city= gc.city_id
    where u.registered_at >= '$start_date' and u.registered_at < '$end_date'  group by city, gender" ;
	
    $reg_res = $conn_reporting->Execute($sql_reg); 
    $registration = array ();
    
    while ($row = $reg_res->FetchRow())
    {
    	$registration[$row['city']][$row['gender']] =  $row['users'];
    }
    
    
    $sql_uninstall =  "select gender, if(gc.name in ($top_city_list),'top_ten','other') as city, count(*) as uninstalls from user u 
    join user_gcm_current ugc on u.user_id = ugc.user_id join user_data ud on ud.user_id = u.user_id
	left join geo_city gc on gc.city_id = ud.stay_city	where ugc.status='uninstall' and
	 ugc.tstamp>='$start_date' and ugc.tstamp <'$end_date'
	group by gender, city " ;
    
    
    $uni_res = $conn_reporting->Execute($sql_uninstall) ;
    $uninstall = array() ;
    
    while ($row = $uni_res->FetchRow())
    {
    	$uninstall[$row['city']][$row['gender']] =  $row['uninstalls'];
    }
    
    $sql_one_day_uninstall = "select gender, if(gc.name in ($top_city_list),'top_ten','other') as city, count(*)  as uninstalls from user u 
    join user_gcm_current ugc on u.user_id = ugc.user_id join user_data ud on ud.user_id = u.user_id
	left join geo_city gc on gc.city_id = ud.stay_city where ugc.status='uninstall' and 
	ugc.tstamp>='$start_date' and u.registered_at>='$start_date' and u.registered_at <'$end_date' 
	group by gender, city ";
    
    $one_res  = $conn_reporting->Execute($sql_one_day_uninstall);
    $one_day_uninstall = array();

    while ($row = $one_res->FetchRow())
     {
     	$one_day_uninstall[$row['city']][$row['gender']] =$row['uninstalls'] ;
     }
     
     $all_array = array ();
     foreach ($registration as $key => $val) 
     {
     	foreach ($val as $k => $v) 
     	{
     		$all_array[$key][$k]['registration'] = $v;
     		$all_array[$key][$k]['uninstalls'] = $uninstall[$key][$k];
     		$all_array[$key][$k]['one_day_uni'] = $one_day_uninstall[$key][$k];
     	}
     }
        
     return $all_array ;
}

function get_weekly_reg ($start_date, $end_date)
{
	global $conn_reporting, $top_city_list;
	$sql_dia = "SET SESSION group_concat_max_len = 1000000 " ;
	$dia_res = $conn_reporting->Execute($sql_dia) ;
	
	$sql = "select gender, if(gc.name in ($top_city_list),'top_ten','other') as city, 
	case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=21 then 'age_18' else 
(case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=22 && 
EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=24 then 'age_22' else
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=25 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=28 then 'age_25' else 
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=29 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=32 then 'age_29' else 'age_33' end )end ) end )end as age_grp,
	count(distinct(u.user_id)) as registrations, count(distinct(ugc.user_id)) as swuni
	 from user u 
     join user_data ud on ud.user_id = u.user_id
     left join user_gcm_current ugc on ugc.user_id = u.user_id and ugc.status ='uninstall' and ugc.tstamp >'$start_date'
	left join geo_city gc on gc.city_id = ud.stay_city	where 
	 u.registered_at>='$start_date' and u.registered_at <'$end_date'
	group by gender, city, age_grp ";
	
	$reg_data = $conn_reporting->Execute($sql);
	$reg_array = array();
	while ($row = $reg_data->FetchRow())
	{
	   	$reg_array[$row['gender'] . $row['city'] .$row['age_grp']]['reg'] = $row['registrations'] ;
 		$reg_array[$row['gender'] . $row['city'] .$row['age_grp']]['swuni'] = $row['swuni'] ;
	}
	
	return $reg_array ;
}

function get_uninstalls ($start_date, $end_date)
{
	global $conn_reporting, $top_city_list, $miss_tm_id;
	//$miss_tm_id = 1126423;
	$reg_array = get_weekly_reg($start_date, $end_date);
	
	
	// to increase the character limit for group concat
	$sql_dia = "SET SESSION group_concat_max_len = 10000000 " ;
	$dia_res = $conn_reporting->Execute($sql_dia) ;
	
	$sql = "select gender, if(gc.name in ($top_city_list),'top_ten','other') as city, 
	case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=21 then 'age_18' else 
(case when EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=22 && 
EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=24 then 'age_22' else
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=25 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=28 then 'age_25' else 
(case when  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) >=29 
&& EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) <=32 then 'age_29' else 'age_33' end )end ) end )end as age_grp,
	count(distinct(u.user_id)) as uninstalls, 
	group_concat(distinct(u.user_id)) as user_ids from user u 
    join user_gcm_current ugc on u.user_id = ugc.user_id join user_data ud on ud.user_id = u.user_id
	left join geo_city gc on gc.city_id = ud.stay_city	where ugc.status='uninstall' and
	 ugc.tstamp>='$start_date' and ugc.tstamp <'$end_date'
	group by gender, city, age_grp " ;
	
	$uni = $conn_reporting->Execute($sql) ;
    $uninstall = array() ;
    
	while ($row = $uni->FetchRow())
    {
    	$temp_array = array() ;
    	$temp_array ['gender'] = $row ['gender'] ;
    	$temp_array ['city'] = $row ['city'] ;
    	$temp_array ['age'] = $row ['age_grp'] ;
    	$temp_array ['reg'] = $reg_array[$row['gender'] . $row['city'] .$row['age_grp']]['reg'] ;
    	$temp_array ['swuni'] = $reg_array[$row['gender'] . $row['city'] .$row['age_grp']]['swuni'] ;
    	$temp_array ['sameweek_perc'] = round(100 * ($temp_array['swuni'] / $temp_array ['reg'])) ;
    	$temp_array ['uninstalls'] = $row ['uninstalls'] ;
     	
    	$user_ids = $row['user_ids'] ;
    	
    	$auth_sql = "select count(*) as ct, group_concat(user_id) as users  from user u 	where u.user_id in ($user_ids) and
    	( status='authentic' or last_changed_status='authentic' )"  ;
    	$auth_res = $conn_reporting->Execute($auth_sql) ;
    	$auth_row = $auth_res->FetchRow();
    	
    	$user_id_auth = $auth_row ['users'];
    	
    	$sql_photo = "select count(*) as c_u from user_photo where user_id in ($user_id_auth) and is_profile ='yes' and status = 'active'";
    	$photo_res = $conn_reporting->Execute($sql_photo);
    	$photo_row = $photo_res->FetchRow();
    	$no_photo_count = $auth_row ['ct'] - $photo_row['c_u'];
    	
    
    	
    	$temp_array['auth'] = round( 100 * ($row['uninstalls'] - $auth_row['ct'] )/ $row ['uninstalls'] ) ;
    	
    	
    	
    	$seen_matches_sql = "select count(distinct(t.user1)) as recs, sum(if(up.user_id is null,1,0)) as recs_photo from 
(select distinct(user1) from user_recommendation_rank_log where user1 in ($user_id_auth) )t
left join user_photo up on t.user1= up.user_id and up.is_profile='yes' and up.status ='active'  ";
    	//echo $seen_matches_sql;
    	$seen_res = $conn_reporting->Execute($seen_matches_sql);
    	$seen_count = $seen_res->FetchRow() ;
    	$temp_array['matches'] = round( 100 * ($auth_row['ct'] - $seen_count['recs'] )/ $auth_row['ct'] ) ;
    	$temp_array['matches_photo'] = round( 100 * ($no_photo_count - $seen_count['recs_photo'] )/ ($row['uninstalls'] - $seen_count['recs'] )) ;
    	
    	$likes_or_hid_sql = "select count(distinct(user1)) as like_or_hide, sum(if(up.user_id is null,1,0)) as like_or_hide_photo from 
    	( select user1 from user_like where user1 in ($user_id_auth) and user2 not in (select user_id from advertisement_campaign )
    	union select user1 from user_hide where user1 in ($user_id_auth)  and user2 not in (select user_id from advertisement_campaign )  )t
		left join user_photo up on t.user1= up.user_id and up.is_profile='yes' and up.status ='active' ";
    	$like_or_hid_res = $conn_reporting->Execute($likes_or_hid_sql) ;
    	$like_or_hid_count = $like_or_hid_res->FetchRow() ;
		//echo $likes_or_hid_sql ;
    	$temp_array['like_or_hide'] = round( 100 * ( $seen_count['recs'] - $like_or_hid_count['like_or_hide'] )/ $row ['uninstalls'] ) ;
    	$temp_array['like_or_hide_photo'] = round( 100 * ( $seen_count['recs_photo'] - $like_or_hid_count['like_or_hide_photo'] )/ ($seen_count['recs'] - $like_or_hid_count['like_or_hide']) ) ;
       
    	$like_sql = "select count(distinct(t.user1)) as likes, sum(if(up.user_id is null,1,0)) as likes_photo from 
(select distinct(user1)  from user_like where user1 in ($user_id_auth) )t
left join user_photo up on t.user1= up.user_id and up.is_profile='yes' and up.status ='active'  " ;
    	$like_res = $conn_reporting->Execute($like_sql);
    	$like_count = $like_res->FetchRow();
    	$temp_array['like'] = round( 100 * (  $like_or_hid_count['like_or_hide'] - $like_count['likes']  )/ $row ['uninstalls'] ) ;
    	$temp_array['like_photo'] = round( 100 * (  $like_or_hid_count['like_or_hide_photo'] - $like_count['likes_photo']  )/ (  $like_or_hid_count['like_or_hide'] - $like_count['likes']  ) ) ;
    	
    	$sql_mutual = "select count(distinct(sender_id)) as all_mutual , sum(if(not_chatted = c_all,1,0)) as no_chat, 
    	sum(if(up.user_id is null,1,0)) as all_mutual_photo from 
    	(select sender_id, sum(if(msg_type = 'MUTUAL_LIKE',1,0)) as not_chatted , count(*) as c_all from messages_queue mq 
    	where mq.sender_id in ($user_id_auth) and mq.receiver_id not in ($miss_tm_id)	group by sender_id ) t
		left join user_photo up on t.sender_id= up.user_id and up.is_profile='yes' and up.status ='active' " ; 
    	$mutual_res = $conn_reporting->Execute($sql_mutual) ;
    	$mutual_count = $mutual_res->FetchRow() ;
    	
    	$temp_array['mutual'] = round( 100 * (  $like_count['likes'] - $mutual_count['all_mutual']  )/ $row ['uninstalls'] ) ;
    	$temp_array['mutual_photo'] = round( 100 * (  $like_count['likes_photo'] - $mutual_count['all_mutual_photo']  )/ $row ['uninstalls'] ) ;
    	$temp_array['chat'] = round( 100 * (  $mutual_count['no_chat'] )/ $row ['uninstalls'] ) ;
    	$temp_array['chat_yes'] = round( 100 * (  $mutual_count['all_mutual'] - $mutual_count['no_chat'] )/ $row ['uninstalls'] ) ;
    	$uninstall [$row['gender'] . $row['city'] .$row['age_grp'] ] = $temp_array;
    	//var_dump($temp_array);
    }
	return $uninstall;
	
}
try 
{
	if(php_sapi_name() == "cli")
	{
	  if($dur == 'week')
	  {
	  	 $install = getInstall ( $week_start_date, $end_date) ;
		 $smarty->assign("install", $install) ;
	  	 
		 $data = get_Data($week_start_date, $end_date) ;
	  	 $smarty->assign("data",$data);
		 $message= $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/theReportDaily.tpl');
		
	  	   
	  	 $data_week = get_uninstalls($week_start_date, $end_date);
	  	 $smarty->assign("uni",$data_week);
	  	 $message .= $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/weeklyChurn.tpl');
	  	 $subject = "Weekly report ".$date ;
	  } 
	  else if ($dur == 'month') 
	  {    
	  	 
	  }
	  else  
	  {
	  	 $data = get_Data($start_date, $end_date) ;
	  	 $smarty->assign("data",$data);
	  	 
	  	 $install = getInstall ( $start_date, $end_date) ;
		 $smarty->assign("install", $install) ; 
	  	 
	  	 $message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/theReportDaily.tpl');
	  	 $subject = "Daily report ".$date ;
	  }

	  
	  $to = $emailIdsForReporting['churn'] ;  
	 //  $to = "sumit@trulymadly.com";
	  
	  $from = "admintm@trulymadly.com" ;
 	  Utils::sendEmail($to, $from, $subject, $message,true); 
 	  echo "mail_sent to ".$to; 
	}
} 
catch (Exception $e) 
{
	trigger_error($e->getMessage(),E_USER_WARNING);
	Utils::sendEmail("sumit@trulymadly.com", "sumit@trulymadly.com", "The Daily report failed ".$baseurl, $e->getMessage());
}

?>