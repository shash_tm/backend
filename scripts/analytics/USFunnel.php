<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";

class USPhotoFunnel {
    private $conn_reporting;
    private $start;
    private $end;
    private $photo_data;
    private $approved_photo ;
    function __construct()
    {
        global $conn_reporting;
        $this->conn_reporting = $conn_reporting ;
        $this->start ;
        $this->end ;
        $this->photo_data ;
        $this->approved_photo;

    }
    function getPhotoFunnel($start, $end)
    {
        $this->start = $start;
        $this->end = $end;

        $this->getInstalls() ;
        $this->getReg();
        $this->getUploadedPhotos() ;
        $this->photo_data['Approved PP'] = null;
        $this->getApprovedPhotos() ;
        $this->getActionTakenCount() ;
        $this->getLikedCount() ;
        $this->getMutualMatchesCount();
        $this->getConvoCount();
        $this->photo_data['No PP'] = null ;
        $this->calculateNotApprovedPhoto() ;
        $this->getNoPPActionCount() ;
        $this->getNoPPLikeCount() ;
        return $this->photo_data ;

    }

    private function getReg()
    {
        $sql = "select  u.gender , count(distinct(u.user_id)) as cu, sum(if(steps_completed regexp 'photo',1,0))  as cc, "
            ."sum(if(u.status ='authentic' || last_changed_status= 'authentic',1,0)) as ca from user u join user_data ud on u.user_id = ud.user_id "
            ."where registered_at >='$this->start'	and  registered_at <'$this->end' and registered_from = 'ios_app' and ud.stay_country= 254 "
            ."group by u.gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array() ;
        while ($row = $res->FetchRow())
        {
            $data['cu'][$row['gender']] = $row['cu'];
            $data['cc'][$row['gender']] = $row['cc'];
            $data['ca'][$row['gender']] = $row['ca'];
            $data['cc'][$row['gender']."_p"] = round(100 * $row['cc']/$row['cu']);
            $data['ca'][$row['gender']."_p"] = round(100 * $row['ca']/$row['cc']);
        }
        $this->photo_data['Registration start'] = $data['cu'] ;
        $this->photo_data['Registration complete'] = $data['cc'] ;
        $this->photo_data['Authentic'] = $data['ca'] ;
    }

    private function getUploadedPhotos()
    {
        $sql = "select  u.gender , count(distinct(u.user_id)) as cu from user u join user_data ud on u.user_id = ud.user_id join user_photo up on u.user_id = up.user_id"
            ." where registered_at >='$this->start'	and  registered_at <'$this->end' and "
            ."steps_completed regexp 'photo' and (u.status ='authentic' or last_changed_status= 'authentic') and registered_from ='ios_app' and ud.stay_country= 254 "
            . "group by u.gender";
        $res = $this->conn_reporting->Execute ( $sql ) ;
        $data = array();
        while ($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
            $data[$row['gender']."_p"] =  round(100 * $row['cu']/$this->photo_data['Authentic'][$row['gender']]); ;
        }
        $this->photo_data['Uploaded photos'] = $data;
    }

    private function getApprovedPhotos ()
    {
        $sql = "select u.gender,if(ufl.hide_myself is null,'None',ufl.hide_myself) as visibility, "
            ."count(distinct(u.user_id)) as cu from user u join user_data ud on u.user_id = ud.user_id join user_photo up on u.user_id = up.user_id "
            ."left join user_flags ufl on u.user_id = ufl.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and ud.stay_country= 254 "
            ."and steps_completed regexp 'photo'	and (u.status ='authentic' or last_changed_status= 'authentic')  and registered_from ='ios_app' "
            ."and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted' ) group by u.gender, visibility" ;
        $res = $this->conn_reporting->Execute ($sql) ;
        $data = array() ;
        $new_data = array();
        while ($row = $res->FetchRow())
        {
            $data[$row['visibility']][$row['gender']] = $row['cu'] ;
        }
        foreach ($data['None'] as $key=> $val)
        {
            $sum = $data['None'][$key] + $data[0][$key] + $data[1][$key] ;
            $visible_p = round(100 * $data[1][$key]/ $sum) ;
            $not_visible_p = round(100 * $data[0][$key]/ $sum) ;
            $this->approved_photo[$key] = $sum ;
            // 1380(8%, 2%)
            $new_data[$key] = $this->approved_photo[$key] . " (" . $visible_p ."%, " .$not_visible_p ."%)";
            $new_data[$key."_p"] = round(100 * $sum/$this->photo_data['Uploaded photos'][$key]) ;
        }
        $this->photo_data['Profile photo approved'] = $new_data ;
    }

    private function getActionTakenCount ()
    {
        $sql = "select  u.gender, count(distinct(u.user_id)) as cu from user u  join user_data ud on u.user_id = ud.user_id join user_photo up on "
            ."u.user_id = up.user_id join (select distinct(user1) as user_id from "
            ."(select user1 from user_like where timestamp >='$this->start'  union  select user1 from user_hide where timestamp >='$this->start' )t )r "
            ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and steps_completed regexp 'photo' "
            ."and registered_from ='ios_app' and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
            ."and (u.status ='authentic' or last_changed_status= 'authentic') and ud.stay_country= 254 group by  u.gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while ($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
            $data[$row['gender']."_p"] = round(100 * $row['cu']/ $this->approved_photo[$row['gender']]);
        }
        $this->photo_data['Took action'] = $data ;
    }

    private function getLikedCount()
    {
        $sql = "select  u.gender, count(distinct(u.user_id)) as cu from user u  join user_data ud on u.user_id = ud.user_id join user_photo up on "
            ."u.user_id = up.user_id join (select distinct(user1) as user_id  from user_like where timestamp >='$this->start')r "
            ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and steps_completed regexp 'photo' "
            ."and registered_from = 'ios_app' and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
            ."and (u.status ='authentic' or last_changed_status= 'authentic') and ud.stay_country= 254 group by  u.gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array() ;
        while ($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
            $data[$row['gender']."_p"] = round(100 * $row['cu']/ $this->photo_data['Took action'][$row['gender']]);
        }
        $this->photo_data['Liked a profile'] = $data ;
    }

    private function getMutualMatchesCount()
    {
        global $miss_tm_id ;
        $sql = "select  u.gender, count(distinct(u.user_id)) as cu from user u  join user_data ud on u.user_id = ud.user_id join user_photo up on "
            ."u.user_id = up.user_id join messages_queue mq  on u.user_id = mq.sender_id where registered_at >='$this->start'	 and  "
            ."registered_at <'$this->end' and  up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') and "
            ."steps_completed regexp 'photo' and registered_from = 'ios_app' and (u.status ='authentic' or last_changed_status= 'authentic' ) "
            ."and mq.tstamp >= '$this->start' and mq.receiver_id not in ($miss_tm_id) and ud.stay_country = 254 group by  u.gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array() ;
        while ($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
            $data[$row['gender']."_p"] = round(100 * $row['cu']/ $this->photo_data['Liked a profile'][$row['gender']]);
        }
        $this->photo_data['Got mutual match'] = $data ;
    }

    private function getConvoCount()
    {
        global $miss_tm_id ;
        $sql = "select  u.gender, count(distinct(u.user_id)) as cu from user u  join user_data ud on u.user_id = ud.user_id join user_photo up on "
            ."u.user_id = up.user_id join messages_queue mq  on u.user_id = mq.sender_id where registered_at >='$this->start' and  "
            ."registered_at <'$this->end' and  up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') and ud.stay_country = 254 and "
            ."steps_completed regexp 'photo' and registered_from in ('ios_app') and (u.status ='authentic' or last_changed_status= 'authentic' ) "
            ."and mq.tstamp >= '$this->start' and mq.msg_type not in( 'mutual_like','spark') and mq.receiver_id not in ($miss_tm_id) group by  gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array() ;
        while ($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
            $data[$row['gender']."_p"] = round(100 * $row['cu']/ $this->photo_data['Got mutual match'][$row['gender']]);
        }
        $this->photo_data['Sent a message'] = $data ;
    }

    private function calculateNotApprovedPhoto ()
    {
        $data = array();
        foreach ($this->approved_photo as $key => $val)
        {
            $data[$key] = $this->photo_data['Uploaded photos'][$key] - $this->approved_photo[$key] ;
            $data[$key."_p"] = round(100 * $data[$key]/$this->photo_data['Uploaded photos'][$key]) ;
        }
        $this->photo_data['Profile photo not approved'] = $data ;
    }

    private function getInstalls()
    {
        $sql_reinstall = "select count(distinct(ugc.device_id)) as c_u  from user_gcm_current ugc "
            ."join user u on u.device_id= ugc.device_id  or u.user_id = ugc.user_id where install_tstamp >= '$this->start' and install_tstamp < '$this->end' "
            ."and u.registered_at < '$this->start' and ugc.source='iOSApp' and ugc.country_id= 254";

        $reinstall = $this->conn_reporting->Execute($sql_reinstall) ;
        $reinstall_res = array();
        while ($row = $reinstall->FetchRow())
        {
            $reinstall_res = $row['c_u'] ;
        }
        $sql_install = "Select count(distinct(device_id)) as c_u from user_gcm_current ugc "
            ."where install_tstamp >='$this->start'  and install_tstamp < '$this->end' and ugc.source='iOSApp' and ugc.country_id= 254";
        $install = $this->conn_reporting->Execute($sql_install);
        while ($row = $install->FetchRow())
        {
            $install_res = $row['c_u'] ;#- $reinstall_res ;
        }
        $this->photo_data['All Installs'] = $install_res ;
    }

    private function getNoPPActionCount()
    {
        $sql = "select  u.gender, count(distinct(u.user_id)) as cu from user u  join user_data ud on u.user_id = ud.user_id "
            ."join (select distinct(user_id) from user_photo where time_of_saving> '2016-12-21 00:00:00')p on p.user_id = u.user_id "
            ."left join user_photo up on u.user_id = up.user_id and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
            ."join (select distinct(user1) as user_id from "
            ."(select user1 from user_like where timestamp >='$this->start'  union  select user1 from user_hide where timestamp >='$this->start' )t )r "
            ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end'  and steps_completed regexp 'photo' "
            ."and registered_from in ('ios_app') and up.user_id is null and ud.stay_country= 254 "
            ."and (u.status ='authentic' or last_changed_status= 'authentic') group by  u.gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while ($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
            $data[$row['gender']."_p"] = round(100 * $row['cu']/ $this->photo_data['Profile photo not approved'][$row['gender']]);
        }

        $this->photo_data['Took action(No PP)'] = $data ;
    }

    private function getNoPPLikeCount()
    {
        $sql = "select  u.gender, count(distinct(u.user_id)) as cu from user u  join user_data ud on u.user_id = ud.user_id "
            ."join (select distinct(user_id) from user_photo where time_of_saving> '2016-12-21 00:00:00')p on p.user_id = u.user_id "
            ."left join user_photo up on u.user_id = up.user_id and up.admin_approved='yes' and is_profile='yes' and up.status in ('active','system_deleted') "
            ."join (select distinct(user1) as user_id from "
            ."(select user1 from user_like where timestamp >='$this->start'  )t )r "
            ."on u.user_id = r.user_id where registered_at >='$this->start' and  registered_at <'$this->end' and steps_completed regexp 'photo' "
            ."and registered_from in ('ios_app') and up.user_id is null and ud.stay_country = 254 "
            ."and (u.status ='authentic' or last_changed_status= 'authentic') group by  u.gender";

        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while ($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
            $data[$row['gender']."_p"] = round(100 * $row['cu']/ $this->photo_data['Took action(No PP)'][$row['gender']]);
        }

        $this->photo_data['Liked a profile(No PP)'] = $data ;
    }

}


/*try
{
    $isttime= date ( 'Y-m-d H:i:s');
    $today = date('Y-m-d') ;
    $end_date = date('Y-m-d 00:00:00', strtotime('-2 day', strtotime($isttime)));
    $start_date=date('Y-m-d 00:00:00', strtotime('-12 day', strtotime($isttime)));
    $upto = date('Y-m-d', strtotime('-2 day', strtotime($isttime)));
    $from_s=date('Y-m-d', strtotime('-12 day', strtotime($isttime)));

//    if (php_sapi_name() =='cli' )
//    {
    $miss_tm_id = 1126423;
        $photoObj = new USPhotoFunnel() ;
        $photo_us = $photoObj->getPhotoFunnel($start_date, $end_date) ;
        $smarty->assign("data_US",$photo_us);
        $smarty->assign("from",$from_s);
        $smarty->assign("to",$upto);
        $message=$smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/USFunnel.tpl'); #echo $message;
        $subject = 'Weekly Photo report '.$today; echo $message;
       // $to = 'sumit@trulymadly.com,rahul@trulymadly.com,shashwat@trulymadly.com' ;
        $from = 'admintm@trulymadly.com' ;
       // Utils::sendEmail($to, $from, $subject, $message,true);
   // }
} catch (Exception $e) {
    echo "some error";
    echo $e->getMessage();
  //  Utils::sendEmail("sumit@trulymadly.comm", "sumit@trulymadly.com", "US Photo funnel reporting failed ".$baseurl, $e->getMessage());
}*/



?>