<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../email/MailFunctions.php";

class SelectRecoReport
{
    private $conn_reporting;
    private $smarty ;
    private $start_time;
    private $end_time ;
    private $testUsers = "605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919,281684,160486,707,
    1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063,1615723,573722,433066,1285095";
    private $data_array = array();
    private $user_id_array = array();
    private $age_array = array();
    private $city_array = array();
    private $user_id_list;

    function __construct($start_time, $end_time)
    {
        global $conn_reporting, $smarty ;
        $this->conn_reporting = $conn_reporting ;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->smarty = $smarty ;
        $this->start_time = $start_time ;
        $this->end_time = $end_time ;
    }

    public function getUserWiseData()
    {
        $this->getSelectUserList();
        $this->getUserMatches();
        $this->getUserLikes();
        $this->getUserLikeBacks();
        $this->getUserHides();
        $this->getUserSparks();
        $this->getUserSelectPackage();
        $this->checkEmptyPackage();
        $this->smarty->assign('data_array',$this->data_array);
        $select_file = dirname ( __FILE__ ) . "/../../templates/reporting/selectUserData.tpl";
        $select = $this->smarty->fetch ( $select_file );
        return $select;
    }

    public function getAgeData()
    {
        $this->aggregateAge();
        $this->smarty->assign('age_data',$this->age_array);
        $select_age_file = dirname ( __FILE__ ) . "/../../templates/reporting/selectAgeData.tpl";
        $age_data = $this->smarty->fetch($select_age_file);
        return $age_data ;
    }

    public function getCityData()
    {
        $this->aggregateCityData();
        $this->smarty->assign('city_data',$this->city_array);
        $select_city_file = dirname ( __FILE__ ) . "/../../templates/reporting/SelectCityData.tpl";
        $city_data = $this->smarty->fetch($select_city_file);
        return $city_data ;
    }

    private function aggregateCityData()
    {
        $city_data_array = array();
        foreach($this->data_array as $key => $val)
        {
            $city = $val['city'];
            $city_gender_key = $val['gender']."__" . $city  ;
            $city_data_array[$city_gender_key]['count'] += 1 ;
            $city_data_array[$city_gender_key]['gender'] = $val['gender'] ;
            $city_data_array[$city_gender_key]['city'] = $city ;
            $city_data_array[$city_gender_key]['matches'] += $val['matches'] ;
            $city_data_array[$city_gender_key]['matches_select'] += $val['matches_select'] ;
            $city_data_array[$city_gender_key]['matches_non_select'] += $val['matches_non_select'] ;
            $city_data_array[$city_gender_key]['likes'] += $val['likes'] ;
            $city_data_array[$city_gender_key]['hides'] += $val['hides'] ;
            $city_data_array[$city_gender_key]['sparks'] += $val['sparks'] ;
            if($val['price'] > 0)
            {
                $city_data_array[$city_gender_key]['price'] += $val['price'];
                $city_data_array[$city_gender_key]['count_real'] += 1;
            }
        }
        foreach($city_data_array as $k => $v)
        {
            $this->city_array[$k]['users'] = $v['count'] ;
            $this->city_array[$k]['gender'] = $v['gender'] ;
            $this->city_array[$k]['city'] = $v['city'] ;
            $this->city_array[$k]['matches'] = round($v['matches']/$v['count']) ;
            $this->city_array[$k]['matches_select'] = round($v['matches_select']/$v['count']) ;
            $this->city_array[$k]['matches_non_select'] = round($v['matches_non_select']/$v['count']) ;
            $this->city_array[$k]['likes'] = round($v['likes']/$v['count']) ;
            $this->city_array[$k]['hides'] = round($v['hides']/$v['count']) ;
            $this->city_array[$k]['sparks'] = round($v['sparks']/$v['count']) ;
            $this->city_array[$k]['price'] = round($v['price']/$v['count_real']) ;
        }
    }

    private function aggregateAge()
    {
        $age_data_array = array() ;
        foreach($this->data_array as $key => $val)
        {
            $age_group = $this->getAgeGroup($val['age']) ;
            $age_gender_key =  $val['gender'] ."__".$age_group ;
            $age_data_array[$age_gender_key]['count'] += 1 ;
            $age_data_array[$age_gender_key]['gender'] = $val['gender'] ;
            $age_data_array[$age_gender_key]['age'] = $age_group ;
            $age_data_array[$age_gender_key]['matches'] += $val['matches'] ;
            $age_data_array[$age_gender_key]['matches_select'] += $val['matches_select'] ;
            $age_data_array[$age_gender_key]['matches_non_select'] += $val['matches_non_select'] ;
            $age_data_array[$age_gender_key]['likes'] += $val['likes'] ;
            $age_data_array[$age_gender_key]['hides'] += $val['hides'] ;
            $age_data_array[$age_gender_key]['sparks'] += $val['sparks'] ;
            if($val['price'] > 0)
            {
                $age_data_array[$age_gender_key]['price'] += $val['price'] ;
                $age_data_array[$age_gender_key]['count_real'] += 1 ;
            }
        }
        foreach($age_data_array as $k => $v)
        {
            $this->age_array[$k]['users'] = $v['count'] ;
            $this->age_array[$k]['gender'] = $v['gender'] ;
            $this->age_array[$k]['age'] = $v['age'] ;
            $this->age_array[$k]['matches'] = round($v['matches']/$v['count']) ;
            $this->age_array[$k]['matches_select'] = round($v['matches_select']/$v['count']) ;
            $this->age_array[$k]['matches_non_select'] = round($v['matches_non_select']/$v['count']) ;
            $this->age_array[$k]['likes'] = round($v['likes']/$v['count']) ;
            $this->age_array[$k]['hides'] = round($v['hides']/$v['count']) ;
            $this->age_array[$k]['sparks'] = round($v['sparks']/$v['count']) ;
            $this->age_array[$k]['price'] = round($v['price']/$v['count_real']) ;
        }
        ksort($this->age_array);
    }
    private function getAgeGroup($age)
    {
       if($age <= 24)
           return '18-24';
        else if ($age <= 28)
            return '25-28' ;
        else if ($age <= 32)
            return '29-32' ;
        else if($age <= 36)
            return '33-36';
        else if($age <= 39)
            return '37-39';
        else if($age <= 44)
            return '40-44';
        else if($age <= 59)
            return '45-59';
        else
            return '60+' ;

    }

    private function checkEmptyPackage()
    {
        foreach($this->data_array as $key => $val)
        {
            if($val['status'] == 'trial')
            {
                $this->data_array[$key]['price'] = $val['status'];
            }
        }
    }

    private function getUserSelectPackage()
    {
        $sql = "select ust.user_id , sp.price from user_spark_transaction ust join spark_packages sp on ust.package_id = sp.package_id
               where sp.type='select'  and user_id in ($this->user_id_list) and ust.status ='consumed'
               order by ust.tstamp desc" ;
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            if(isset($this->data_array[$row['user_id']]['price']))
                $this->data_array[$row['user_id']]['price'] .= ',' . $row['price'] ;
            else
                $this->data_array[$row['user_id']]['price'] = $row['price'] ;
        }
    }

    private function getUserSparks()
    {
        $sql = "select ul.user1 as user_id, count(distinct(ul.user2)) as sparks, count(distinct(us.user_id)) as sparks_select,
                count(distinct(ul.user2)) - count(distinct(us.user_id)) as sparks_non_select from user_spark ul
                left join user_subscription us on ul.user2 = us.user_id and us.status in ('active','trial') and us.expiry_date > ul.tstamp
                where ul.tstamp >='$this->start_time' and ul.tstamp < '$this->end_time'
                and ul.user1 in ($this->user_id_list) and ul.user2 not in (select user_id from advertisement_campaign)
                 and ul.user2 not in ($this->testUsers)
                group by ul.user1";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $this->data_array[$row['user_id']]['sparks'] = $row['sparks'];
            $this->data_array[$row['user_id']]['sparks_select'] = $row['sparks_select'];
            $this->data_array[$row['user_id']]['sparks_non_select'] = $row['sparks_non_select'];
        }
    }

    private function getUserHides()
    {
        $sql = "select ul.user1 as user_id, count(distinct(ul.user2)) as hides, count(distinct(us.user_id)) as hides_select,
                count(distinct(ul.user2)) - count(distinct(us.user_id)) as hides_non_select from user_hide ul
                left join user_subscription us on ul.user2 = us.user_id and us.status in ('active','trial') and us.expiry_date > ul.timestamp
                where ul.timestamp >='$this->start_time' and ul.timestamp < '$this->end_time'
                and ul.user1 in ($this->user_id_list) and ul.user2 not in (select user_id from advertisement_campaign)
                and ul.user2 not in ($this->testUsers)
                group by ul.user1";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $this->data_array[$row['user_id']]['hides'] = $row['hides'];
            $this->data_array[$row['user_id']]['hides_select'] = $row['hides_select'];
            $this->data_array[$row['user_id']]['hides_non_select'] = $row['hides_non_select'];
        }
    }

    private function getUserLikeBacks()
    {
        $sql = "select ul.user_id as user_id, count(distinct(ul.match_id)) as likebacks, count(distinct(us.user_id)) as likebacks_select,
                 count(distinct(ul.match_id)) - count(distinct(us.user_id)) as likebacks_non_select
                from (select user1 as user_id, user2 as match_id, timestamp from user_mutual_like uml
                where uml.timestamp >= '$this->start_time'  and uml.timestamp <'$this->end_time'
                union
                select user2 as user_id, user1 as match_id, timestamp from user_mutual_like uml
                where uml.timestamp > '$this->start_time'  and uml.timestamp <'$this->end_time')ul
                left join user_subscription us on ul.match_id = us.user_id and us.status in ('active','trial') and us.expiry_date > ul.timestamp
                where ul.user_id in ($this->user_id_list) and ul.match_id not in (select user_id from advertisement_campaign)
                and ul.match_id not in ($this->testUsers)
                group by ul.user_id";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $this->data_array[$row['user_id']]['likebacks'] = $row['likebacks'];
            $this->data_array[$row['user_id']]['likebacks_select'] = $row['likebacks_select'];
            $this->data_array[$row['user_id']]['likebacks_non_select'] = $row['likebacks_non_select'];
        }
    }

    private function getUserLikes()
    {
        $sql = "select ul.user1 as user_id, count(distinct(ul.user2)) as likes, count(distinct(us.user_id)) as likes_select,
                count(distinct(ul.user2)) - count(distinct(us.user_id)) as likes_non_select from user_like ul
                left join user_subscription us on ul.user2 = us.user_id and us.status in ('active','trial') and us.expiry_date > ul.timestamp
                where ul.timestamp >='$this->start_time' and ul.timestamp < '$this->end_time'
                and ul.user1 in ($this->user_id_list) and ul.user2 not in (select user_id from advertisement_campaign)
                and ul.user2 not in ($this->testUsers)
                group by ul.user1";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $this->data_array[$row['user_id']]['likes'] = $row['likes'];
            $this->data_array[$row['user_id']]['likes_select'] = $row['likes_select'];
            $this->data_array[$row['user_id']]['likes_non_select'] = $row['likes_non_select'];
        }
    }

    private function getUserMatches()
    {
        $sql = "select ul.user1 as user_id, count(distinct(ul.user2)) as matches, count(distinct(us.user_id)) as matches_select, count(distinct(ul.user2)) - count(distinct(us.user_id)) as matches_non_select
                from user_recommendation_rank_log ul
                left join user_subscription us on ul.user2 = us.user_id and us.status in ('active','trial') and us.expiry_date > ul.tstamp
                where ul.tstamp >='$this->start_time' and ul.tstamp < '$this->end_time' and ul.user1 in ($this->user_id_list)
                and ul.user2 not in (select user_id from advertisement_campaign)
                and ul.user2 not in ($this->testUsers)
                group by ul.user1";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $this->data_array[$row['user_id']]['matches'] = $row['matches'];
            $this->data_array[$row['user_id']]['matches_select'] = $row['matches_select'];
            $this->data_array[$row['user_id']]['matches_non_select'] = $row['matches_non_select'];
        }
    }

    private function getSelectUserList()
    {
        $sql = "select us.user_id, u.gender,  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) as age,
                if(gc.name in ('Delhi','Faridabad','Greater Faridabad','Gurgaon','Noida','Greater Noida','Ghaziabad'),'Delhi/NCR',gc.name) as city,
                if(gc.name in ('Delhi','Faridabad','Greater Faridabad','Gurgaon','Noida','Greater Noida', 'Ghaziabad','Mumbai','Bengaluru','Pune','Kolkata','Hyderabad',
                    'Chennai'),'Metro','Non-Metro') as city_type, us.status from user_subscription us
                JOIN user u  on u.user_id = us.user_id
                JOIN user_data ud ON u.user_id=ud.user_id
                LEFT JOIN geo_state gs ON gs.state_id=ud.stay_state
                LEFT JOIN geo_city gc ON ud.stay_city=gc.city_id AND gc.state_id=ud.stay_state
                where us.expiry_date > now() and us.status in ('active','trial')  and us.user_id not in ($this->testUsers)
                order by us.status desc, gc.name,ud.DateOfBirth desc";
        $res = $this->conn_reporting->Execute($sql);
        while($row = $res->FetchRow())
        {
            $this->data_array[$row['user_id']] =  $row ;
            $this->user_id_array[] = $row['user_id'] ;
        }
        $this->user_id_list = implode(',',$this->user_id_array);
    }
}

try
{
    if(php_sapi_name() == 'cli')
    {
        $isttime = date ( 'Y-m-d H:i' ,strtotime ( '+330 minutes' ) );
        $indianTime = Utils::getDateGMTTOISTfromUnixTimestamp(microtime(true));
        $start_date=date('Y-m-d 18:30:00', strtotime('-8 day', strtotime($isttime)));
        $end_date = date ( 'Y-m-d 18:30:00',strtotime('-1 day', strtotime($isttime)));
        $subject = "Select Weekly Analysis ". $indianTime;

        $selectReco = new SelectRecoReport($start_date,$end_date);
        $selectData = $selectReco->getUserWiseData();
        $ageData = $selectReco->getAgeData();
        $cityData = $selectReco->getCityData();
        $mailData = $ageData .$cityData.$selectData;
        $to = $emailIdsForReporting['select_reco'];
        $email_array = explode(',',$to);
        $mailObject = new MailFunctions();
        $mid= $mailObject->sendMail($email_array ,$mailData,$mailData,$subject,null,null, array(),true);
      //  Utils::sendEmail('sumit@trulymadly.com, dhruv@trulymadly.com, purav@trulymadly.com', 'admintm@trulymadly.com',$subject,$mailData, true);

    }

}
catch(Exception $e)
{
    echo $e->getMessage();
    Utils::sendEmail("sumit@trulymadly.com","sumit@trulymadly.com","Select Reporting failed","Error ". $e->getMessage());
}









?>