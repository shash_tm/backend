<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../email/MailFunctions.php";

class SparkFullReport
{
    private $conn_reporting;
    private $smarty ;
    private $start_time;
    private $end_time ;
    private $testUsers = "605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919,281684,160486,707,
    1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063,1615723,573722,433066,1285095";
    private $data_array = array() ;
    private $select_array = array() ;
    private $exclude_dummy;
    function __construct ($start_date,$end_date)
    {
        global $conn_reporting, $smarty;
        $this->conn_reporting = $conn_reporting ;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->smarty = $smarty ;
        $this->start_time = $start_date ;
        $this->end_time = $end_date ;
        $this->exclude_dummy = '"match_id":"1615723"';

    }

    public function getAllData()
    {
        $this->data_array[] = "Payment Funnel" ;
        $this->getSeenForSparkBuy();
        $this->getUserSparksInitiated();
        $this->getUserSparksPurchased();
        $this->getUserSparksViaGateway();
        $this->data_array[] = "Engagement Funnel" ;
        $this->getSenderStats();
        $this->getSentSparks();
        $this->getSentAcceptedSparks();
        $this->getReceiverStats();
        $this->getSparkReceivedUnique();
        $this->getCountSparksSeen();
        $this->getConvoListstats();
        $this->getChatStats();
        $this->getExpired();
        $this->smarty->assign ( "all_data", $this->data_array );
    }

    private function getSeenForSparkBuy()
    {
        $sql = "select if(ud.stay_country= 254,stay_country,113) as country_id, gender,source,event_status, count(distinct(aln.user_id)) as cu
                from action_log_new aln join user u on aln.user_id=u.user_id
                join user_data ud on u.user_id = ud.user_id
                where aln.tstamp>='$this->start_time' and aln.tstamp<'$this->end_time'
                and activity='sparks' and event_type='buy' and event_status in ('view','buy') and u.user_id not in ($this->testUsers)
                group by country_id, gender, source,event_status";
        $res = $this->conn_reporting->Execute($sql);
        $data = array();
        while($row = $res->FetchRow())
        {
            if($row['country_id'] != 254)
                $data[$row['event_status']][$row['source']][$row['gender']] = $row['cu'] ;
            else
                $data[$row['event_status']][$row['source'].'_us'][$row['gender']] = $row['cu'] ;
        }
        $temp_data['metrics'] = "Number of unique users who saw the Sparks buy Page";
        $temp_data['data'] = $data['view'] ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of unique users who clicked on the buy button";
        $temp_data['data'] = $data['buy'] ;
        $this->data_array[] = $temp_data ;
    }

    private function getUserSparksInitiated()
    {
        $sql= "select if (sp.currency is null,'rupee',sp.currency) as curren,gender,sp.source, count(DISTINCT(ust.user_id)) as cu
                from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where ust.tstamp >='$this->start_time' and ust.tstamp <'$this->end_time' and ust.user_id not in ($this->testUsers)
                and ust.spark_count > 0
                group by curren, gender, source";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data[$row['source']][$row['gender']] = $row['cu'] ;
        }
        $temp_data['metrics'] = "Number of unique users who initiated the payments";
        $temp_data['data'] = $data ;
        $this->data_array[] = $temp_data ;
        //$this->smarty->assign ( "seenPackage", $seenPackage );
    }

    private function getUserSparksPurchased()
    {
        $sql= "select if (sp.currency is null,'rupee',sp.currency) as curren, gender,sp.source, count(DISTINCT(ust.user_id)) as cu, count(*) as ca,sum(ust.price) as rev
                from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where ust.tstamp >='$this->start_time' and ust.tstamp <'$this->end_time' and ust.status='consumed'
                and ust.user_id not in ($this->testUsers) and ust.spark_count > 0
                group by curren, gender, source";

        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        $data_all = array();
        $revenue = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data[$row['source']][$row['gender']] = $row['cu'] ;
            $data_all[$row['source']][$row['gender']] = $row['ca'] ;
            $revenue[$row['source']][$row['gender']] =  round($row['rev'],2) ;
        }
        $temp_data['metrics'] = "Number of unique users who bought a Spark package";
        $temp_data['data'] = $data ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of Spark packages purchased";
        $temp_data['data'] = $data_all ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Revenue Generated";
        $temp_data['data'] = $revenue ;
        $this->data_array[] = $temp_data ;
    }

    private function getUserSparksViaGateway()
    {
        $sql= "select if (sp.currency is null,'rupee',sp.currency) as curren,gender,sp.source, ust.payment_gateway, count(*) as ca
                from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where ust.tstamp >'$this->start_time' and ust.tstamp <'$this->end_time' and ust.status='consumed'
                and ust.user_id not in ($this->testUsers) and ust.spark_count > 0
                group by curren, gender, source,ust.payment_gateway";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        $data_all = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data_all[$row['payment_gateway']][$row['source']][$row['gender']] = $row['ca'] ;
        }
        foreach($data_all as $gateway_name=>$gateway_arr)
        {
            $temp_data['metrics'] = "Payments made Via ".$gateway_name;
            $temp_data['data'] = $gateway_arr;
            $this->data_array[] = $temp_data; //print_r(json_encode($this->data_array)); die ;
        }
    }

    private function getSentSparks()
    {
        $sql = "select if(ud.stay_country= 254,stay_country,113) as country_id, registered_from as source,u.gender, count(distinct(user1)) as unique_senders,count(*) as all_sent, sum(if(us.message_id is null,1,0)) as no_message
              from user_spark us  join user u on u.user_id = us.user1
              JOIN user_data ud on u.user_id = ud.user_id
              WHERE us.tstamp>='$this->start_time' and us.tstamp<'$this->end_time' and us.user1 not in ($this->testUsers)
              and us.user2 not in ($this->testUsers) group by country_id, registered_from, u.gender" ;
        $res = $this->conn_reporting->Execute($sql) ;
        $unique_senders = array();
        $all_sent = array() ;
        $no_message = array() ;
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $unique_senders[$row['source']][$row['gender']] = $row['unique_senders'] ;
            $all_sent[$row['source']][$row['gender']] = $row['all_sent'] ;
            $no_message[$row['source']][$row['gender']] = $row['no_message'] ;
        }
        $temp_data['metrics'] = "Number of unique users who sent a Spark";
        $temp_data['data'] = $unique_senders ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of Sparks sent";
        $temp_data['data'] = $all_sent ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of Sparks sent without a message";
        $temp_data['data'] = $no_message ;
        $this->data_array[] = $temp_data ;
    }

    private function getSentAcceptedSparks()
    {
        $sql = "select if(ud.stay_country= 254,stay_country,113) as country_id, registered_from as source,u.gender, count(distinct(user1)) as unique_senders, count(*) as ca
              from user_spark us  join user u on u.user_id = us.user1
              JOIN user_data ud on u.user_id = ud.user_id
              WHERE us.tstamp>='$this->start_time' and us.tstamp<'$this->end_time' and us.user1 not in ($this->testUsers)
               and us.user2 not in ($this->testUsers) and us.status = 'accepted'
              group by country_id, registered_from, u.gender" ;
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        $data_all = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data[$row['source']][$row['gender']] = $row['unique_senders'] ;
            $data_all[$row['source']][$row['gender']] = $row['ca'] ;
        }
        $temp_data['metrics'] = "Number of unique users with at least 1 Spark accepted";
        $temp_data['data'] = $data ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of sparks Accepted ";
        $temp_data['data'] = $data_all ;
        $this->data_array[] = $temp_data ;
    }

    private function getSparkReceivedUnique()
    {
        $sql = "select if(ud.stay_country= 254,stay_country,113) as country_id,registered_from as source,u.gender, count(distinct(user2)) as unique_senders
              from user_spark us  join user u on u.user_id = us.user2
              JOIN user_data ud on u.user_id = ud.user_id
              WHERE us.tstamp>'$this->start_time' and us.tstamp<'$this->end_time' and us.user1 not in ($this->testUsers)
              and us.user2 not in ($this->testUsers) group by country_id,registered_from, u.gender" ;
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data[$row['source']][$row['gender']] = $row['unique_senders'] ;
        }
        $temp_data['metrics'] = "Number of unique users who received a Spark";
        $temp_data['data'] = $data ;
        $this->data_array[] = $temp_data ;
    }

    private function getCountSparksSeen()
    {
        $sql = "select if(ud.stay_country= 254,stay_country,113) as country_id,gender,source, count(distinct(aln.user_id)) as cu, count(*) as ca from action_log_new aln
                join user u on aln.user_id=u.user_id
                JOIN user_data ud on u.user_id = ud.user_id
                where aln.tstamp>='$this->start_time' and aln.tstamp<'$this->end_time'
                and activity='sparks' and event_type='conv_list' and event_status in ('seen')  and u.user_id not in ($this->testUsers)
                and event_info not regexp '$this->exclude_dummy'
                group by country_id,gender, source";
        
        $res = $this->conn_reporting->Execute($sql);
        $data = array();
        $data_all = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data[$row['source']][$row['gender']] = $row['cu'] ;
            $data_all[$row['source']][$row['gender']] = $row['ca'] ;
        }
        $temp_data['metrics'] = "Number of unique users who viewed their Sparks";
        $temp_data['data'] = $data ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of Sparks viewed";
        $temp_data['data'] = $data_all ;
        $this->data_array[] = $temp_data ;
    }

    private function getConvoListstats()
    {
        $sql = "select if(ud.stay_country= 254,stay_country,113) as country_id, gender,source,event_status, count(distinct(aln.user_id)) as cu from action_log_new aln
                join user u on aln.user_id=u.user_id
                JOIN user_data ud on u.user_id = ud.user_id
                where aln.tstamp>='$this->start_time' and aln.tstamp<'$this->end_time'
                and activity='sparks' and event_type='conv_list' and event_status in ('clicked','say_hi','delete_confirmed')  
                and u.user_id not in ($this->testUsers) and event_info not regexp '$this->exclude_dummy'
                group by country_id, gender, source,event_status";
        $res = $this->conn_reporting->Execute($sql);
        $delete = array();
        $accepted = array();
        $clicked = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            if($row['event_status'] == 'clicked')
                $clicked[$row['source']][$row['gender']] = $row['cu'] ;
            else if ($row['event_status'] == 'say_hi')
                $accepted[$row['source']][$row['gender']] = $row['cu'] ;
            else if ($row['event_status'] == 'delete_confirmed')
                $delete[$row['source']][$row['gender']] = $row['cu'] ;

        }
        $temp_data['metrics'] = "Number of Sparks rejected from conversation list";
        $temp_data['data'] = $delete ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of Sparks accepted from conversation list";
        $temp_data['data'] = $accepted ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of Sparks clicked";
        $temp_data['data'] = $clicked ;
        $this->data_array[] = $temp_data ;
    }

    private function getChatStats()
    {
        $sql = "select if(ud.stay_country= 254,stay_country,113) as country_id,gender,source,event_status, count(distinct(aln.user_id)) as cu from action_log_new aln
                join user u on aln.user_id=u.user_id
                JOIN user_data ud on u.user_id = ud.user_id
                where aln.tstamp>='$this->start_time' and aln.tstamp<'$this->end_time'
                and activity='sparks' and event_type='chat' and event_status in ('message_sent','delete_confirmed')  
                and u.user_id not in ($this->testUsers) and event_info not regexp '$this->exclude_dummy'
                group by country_id, gender, source,event_status";
        $res = $this->conn_reporting->Execute($sql);
        $delete = array();
        $accepted = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            if ($row['event_status'] == 'message_sent')
                $accepted[$row['source']][$row['gender']] = $row['cu'] ;
            else if ($row['event_status'] == 'delete_confirmed')
                $delete[$row['source']][$row['gender']] = $row['cu'] ;

        }
        $temp_data['metrics'] = "Number of Sparks rejected from chat";
        $temp_data['data'] = $delete ;
        $this->data_array[] = $temp_data ;
        $temp_data['metrics'] = "Number of Sparks accepted from chat";
        $temp_data['data'] = $accepted ;
        $this->data_array[] = $temp_data ;
    }

    private function getExpired()
    {
        $sql= "select if(ud.stay_country= 254,stay_country,113) as country_id,registered_from as source,u.gender, count(*) as cu
              from user_spark us  join user u on u.user_id = us.user2
              JOIN user_data ud on u.user_id = ud.user_id
              WHERE us.expired_date>='$this->start_time' and us.expired_date<'$this->end_time' and us.user1 not in ($this->testUsers) 
              and us.status in ('seen','expired') and us.user2 not in ($this->testUsers)
              group by country_id, registered_from, u.gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[$row['source']][$row['gender']] = $row['cu'] ;
        }
        $temp_data['metrics'] = "Number of Sparks expired";
        $temp_data['data'] = $data ;
        $this->data_array[] = $temp_data ;
    }

    private function getSenderStats()
    {
        $data = array();
        $data['metrics'] = "Sender Statistics";
        $data['power'] = "one";
        $this->data_array[] = $data ;
    }

    private function getReceiverStats()
    {
        $data = array();
        $data['metrics'] = "Receiver Statistics";
        $data['power'] = "one";
        $this->data_array[] = $data ;
    }



    private function processTheRow($row)
    {
        if($row['source'] == 'iOSApp')
            $row['source'] = 'ios_app' ;
        else if( $row['source'] == 'androidApp')
            $row['source'] = 'android_app' ;
        if($row['country_id'] == 254)
            $row['curren'] = 'dollar' ;

        if($row['curren'] == 'dollar')
            $row['source'] .= "_us";
        return $row ;
    }

    public function getSelectData()
    {
        $this->getSelectViewAndBuy();
        $this->getSelectRevenue();
        $this->getSelectPaymentsByGateway();
        $this->getNoMatches();
        $this->getSelectMembers();
        $this->smarty->assign ( "select_data", $this->select_array );
    }

    private function getSelectMembers()
    {
        $sql ="select gender, count(*) as cu from user_subscription us join user u on us.user_id = u.user_id
              where us.expiry_date > now()  and us.status in ('trial','active') and us.user_id not in ($this->testUsers) group by gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
        }
        $this->select_array['Total Select Members']['power'] = 2 ;
        $this->select_array['Total Select Members']['android_app'] = $data ;
    }

    private function getNoMatches()
    {
        $sql ="select u.gender,count(*) as cu from  (select user_id, sum( case when isLikeQuery =1 then ifnull(countFetched,0) else
              (case when ifnull(countToFetch,0) > ifnull(countFetched,0)  then ifnull(countFetched,0) else countToFetch end ) end  ) as cntFetch
                from user_recommendation_query_log where countFetched is not null and tstamp >= '$this->start_time'  and tstamp < '$this->end_time' group by user_id)t
                join user u on u.user_id = t.user_id where t.cntFetch = 0  group by u.gender";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[$row['gender']] = $row['cu'] ;
        }
        $this->select_array['Total zero matches']['power'] = 2 ;
        $this->select_array['Total zero matches']['android_app'] = $data ;
    }

    private function getSelectPaymentsByGateway()
    {
        $sql = "select gender,sp.source, ust.payment_gateway, count(*) as ca from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where ust.tstamp >='$this->start_time' and ust.tstamp <'$this->end_time' and ust.status='consumed'
                and  ust.spark_count = 0 and ust.user_id not in ($this->testUsers) group by gender, source,ust.payment_gateway";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        $data_all = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data_all[$row['payment_gateway']][$row['source']][$row['gender']] = $row['ca'] ;
        }
        foreach($data_all as $gateway_name=>$gateway_arr)
        {
            $this->select_array["Payments made Via ".$gateway_name] = $gateway_arr ;
        }
    }

    private function getSelectRevenue()
    {
        $sql = "select gender,sp.source, sum(ust.price) as rev from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where ust.tstamp >='$this->start_time'  and ust.tstamp <'$this->end_time'  and ust.status='consumed' and ust.spark_count = 0
                and ust.user_id not in ($this->testUsers)
                group by gender, source";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $row = $this->processTheRow($row);
            $data[$row['source']][$row['gender']] = $row['rev'];
        }
        $this->select_array['Revenue']  = $data ;
    }

    private function getSelectViewAndBuy()
    {
        $sql= "select gender, source, case  when event_type ='buy_trial' then 'free' WHEN event_type ='buy' and event_status ='view' then 'Buy_views'
              else  p.price end as package,event_status, count(*)  as cu
              from action_log_new aln  join user u  on u.user_id = aln.user_id
              left join (select price, sku from spark_packages where type ='select' and source='androidapp' and sku is not null)p
              on trim(both '\"' from  JSON_EXTRACT(event_info, \"$.pkg_id\")) = p.sku
              where activity ='select' and  event_type in ('buy','buy_trial') and event_status in ('buy','view','success')
               and  aln.tstamp >='$this->start_time' and aln.tstamp <'$this->end_time' and aln.user_id not in ($this->testUsers)
              group by source, gender, package, event_status ORDER  by package desc, event_status";
        //echo $sql ;die ;
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $row_name = "" ;
             if($row['package'] == 'free')
             {
                 if($row['event_status'] == 'view')
                     $row_name = "Clicks on Free Trial" ;
                 else
                     $row_name = "New Registration Free" ;
             }
             else if($row['package'] == 'Buy_views')
             {
                 $row_name = "Select Buy Page views" ;
             }
            else
            {
                if($row['event_status'] == 'buy')
                    $row_name = "Clicks on the " .$row['package'] . " pack" ;
                else if($row['event_status'] == 'success')
                    $row_name = "New Registrations ". $row['package'] ;
            }
            $data[$row_name][$row['source']][$row['gender']] = $row['cu'];
        }
        $this->select_array = $data;
    }


}

try
{
   if(php_sapi_name() == 'cli')
    {
        if($argv[1] == 'week')
            $duration = 'week' ;
        else if($argv[1] == 'month')
        	$duration = 'month' ;
      	else
      	    $duration = 'day';
      	
        $isttime = date ( 'Y-m-d H:i' ,strtotime ( '+330 minutes' ) );
        $indianTime = Utils::getDateGMTTOISTfromUnixTimestamp(microtime(true));
        if($duration == 'week')
        {
            $start_date=date('Y-m-d 18:30:00', strtotime('-8 day', strtotime($isttime)));
            $subject = "Spark and Select Weekly Report ". $indianTime;
        }
        else if($duration == 'month')
        {
            $start_date=date('Y-m-d 18:30:00', strtotime('-31 day', strtotime($isttime)));
            $subject = "Spark and Select Monthly Report ". $indianTime;
        }
        else
        {
        	$start_date=date('Y-m-d 18:30:00', strtotime('-2 day', strtotime($isttime)));
        	$subject = "Spark and Select Daily Report ". $indianTime;
        }
        $end_date = date ( 'Y-m-d 18:30:00',strtotime('-1 day', strtotime($isttime)));
        $report = new SparkFullReport($start_date,$end_date);
        $report->getAllData();
        $spark_file = dirname ( __FILE__ ) . "/../../templates/reporting/sparkFullReport.tpl";
        $spark_data = $smarty->fetch ( $spark_file );
       // echo $spark_data ; die ;
        $report->getSelectData();
        $select_file = dirname ( __FILE__ ) . "/../../templates/reporting/selectFullReport.tpl";
        $select_data = $smarty->fetch ( $select_file );

        $mail_data =  $select_data . $spark_data ;
      //  echo $mail_data ;
        $to = $emailIdsForReporting['spark_report'];
       // $to = "sumit@trulymadly.com";
        $email_array = explode(',',$to);
        $mailObject = new MailFunctions();
       $mid= $mailObject->sendMail($email_array ,$mail_data,$mail_data,$subject,null,null, array(),true);
//        Utils::sendEmail('sumit@trulymadly.com', 'admintm@trulymadly.com',$subject,$mail_data, true);
        echo "mail sent to ".$to;
   }
}
catch (Exception $e)
{
    echo $e->getMessage();
    Utils::sendEmail("sumit@trulymadly.com","sumit@trulymadly.com","Spark reporting failed","Error ". $e->getMessage());
}


?>