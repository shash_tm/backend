<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


/**
 * @Himanshu
 * Mailer for hourly messages exchanged with admin
 */



function sendMailer(){
	global $config, $conn_reporting, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl, $admin_id;
	//$sql = " select *, ADDTIME(tStamp, '05:30:00') as ts from current_messages_new where (sender_id = $admin_id or receiver_id = $admin_id) and  tStamp >= date_sub(NOW(), interval 1 HOUR) order by tStamp,sender_id, receiver_id";
	$sql = "select TIMESTAMPDIFF(year,ud.DateOfBirth,curdate()) as age,  u.fname, u.gender, ADDTIME(u.registered_at, '05:30:00') as registered_at,  msg.sender_id, msg.receiver_id, msg.msg_content, ADDTIME(msg.tStamp, '05:30:00') as ts  from current_messages_new msg join user u on u.user_id= case when sender_id =$admin_id then receiver_id else sender_id end  join user_data ud on ud.user_id= case when sender_id =$admin_id then receiver_id else sender_id end  where (sender_id = $admin_id or receiver_id = $admin_id) and  msg.tStamp >= date_sub(NOW(), interval 1 HOUR) order by msg.tStamp,sender_id, receiver_id";
	$res = $conn_reporting->Execute($sql);
	$data =$res->GetRows();
	$smarty->assign("data", $data);
	$smarty->assign("admin_id", $admin_id);
	$subject = "Messaging Admin Statistics: count-". count($data);
	//Utils::sendEmail( "himanshu@trulymadly.com" , "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/adminMessaging.tpl'), TRUE);
	 Utils::sendEmail(Utils::$managementTeam , "himanshu@trulymadly.com", $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../../templates/reporting/adminMessaging.tpl'), TRUE);
}

try{
	sendMailer();
}
catch (Exception $e){
	global $baseurl;
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = $baseurl. " - Admin Messages Mailer failed";

	Utils::sendEmail($to, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Messages Admin Reporting Mailer failed". $e->getTraceAsString(), E_USER_WARNING );

}

?>