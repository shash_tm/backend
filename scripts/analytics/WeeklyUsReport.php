<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


class USWeekly
{
    private $conn_reporting;
    private $start;
    private $end;
    private $smarty ;

    function __construct($start, $end)
    {
        global $conn_reporting , $smarty ;
        $this->start = $start ;
        $this->end = $end;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->smarty = $smarty;
    }

    public function getWeeklyData()
    {
        $engage = $this->getCrossEngagement();
        $this->smarty->assign('engage',$engage);
        $male_location = $this->getLocations('M','registered');
        $female_location = $this->getLocations('F','registered');
        $male_location_active = $this->getLocations('M','active');
        $female_location_active = $this->getLocations('F','active');

        $this->smarty->assign('m_loc',$male_location);
        $this->smarty->assign('f_loc',$female_location);
        $this->smarty->assign('m_loc_active',$male_location_active);
        $this->smarty->assign('f_loc_active',$female_location_active);

        $age = $this->getAgeData('registered');
        $age_active = $this->getAgeData('active');
        $this->smarty->assign('age',$age);
        $this->smarty->assign('age_active',$age_active);

        $spark = $this->getSparkData();
        $this->smarty->assign('spark',$spark);

        $file = dirname ( __FILE__ ) . "/../../templates/reporting/usWeekly.tpl";
        $data = $this->smarty->fetch ( $file );
        return $data;
    }


    private function getCrossEngagement()
    {
        $sql_likes ="select u.gender, if(ud1.stay_country = 254,'US','India') as liker_country,  if(ud2.stay_country = 254,'US','India') as likee_country,
                    count(distinct(user1)) as unique_liker,  count(distinct(user2)) as unique_likee, count(*) as all_actions
                    from user_like ul join user u on ul.user1 = u.user_id join user_data ud1 on ul.user1 = ud1.user_id
                    join user_data ud2 on ul.user2 = ud2.user_id where ul.timestamp >= '$this->start' and ul.timestamp< '$this->end'
                    group by u.gender, liker_country, likee_country";
        $likes_obj = $this->conn_reporting->Execute($sql_likes);
        $likes_data = array();
        while($row = $likes_obj->FetchRow())
        {
            $likes_data[$row['liker_country'] ."_".$row['likee_country']."_".$row['gender']] = $row;
        }

        $sql_hides = "select u.gender, if(ud1.stay_country = 254,'US','India') as liker_country,  if(ud2.stay_country = 254,'US','India') as likee_country,
                      count(distinct(user1)) as unique_liker,  count(distinct(user2)) as unique_likee, count(*) as all_actions from user_hide ul
                      join user u on ul.user1 = u.user_id join user_data ud1 on ul.user1 = ud1.user_id join user_data ud2 on ul.user2 = ud2.user_id
                    where ul.timestamp>= '$this->start' and ul.timestamp<  '$this->end'
                  group by u.gender, liker_country, likee_country";
        $hides_obj = $this->conn_reporting->Execute($sql_hides) ;
        $hides_data = array();
        while($row = $hides_obj->FetchRow() )
        {
            $hides_data[$row['liker_country'] ."_".$row['likee_country']."_".$row['gender']] = $row;
        }

        $sql_spark = "select u.gender, if(ud1.stay_country = 254,'US','India') as liker_country,  if(ud2.stay_country = 254,'US','India') as likee_country,
                    count(distinct(user1)) as unique_liker,  count(distinct(user2)) as unique_likee,
                    count(*) as all_actions from user_spark ul join user u on ul.user1 = u.user_id
                    join user_data ud1 on ul.user1 = ud1.user_id
                    join user_data ud2 on ul.user2 = ud2.user_id
                    where ul.tstamp>= '$this->start' and ul.tstamp <'$this->end'
                    group by u.gender, liker_country, likee_country";
        $spark_obj = $this->conn_reporting->Execute($sql_spark);
        $spark_data = array();
        while($row = $spark_obj->FetchRow())
        {
            $spark_data[$row['liker_country'] ."_".$row['likee_country']."_".$row['gender']] = $row;
        }
        $sql_mutual_like = "select u.gender, if(ud1.stay_country = 254,'US','India') as liker_country, if(ud2.stay_country = 254,'US','India') as likee_country,
                            count(distinct(ul.user_id)) as unique_liker,  count(distinct(ul.match_id)) as unique_likee,
                            count(*) as all_actions from (
                            select user1 as user_id, user2 as match_id from user_mutual_like where timestamp >='$this->start' and timestamp <'$this->end'
                                  union
                            select user2 as user_id, user1 as match_id from user_mutual_like where timestamp >='$this->start' and timestamp <'$this->end'
                            )ul join user u on ul.user_id = u.user_id
                            join user_data ud1 on ul.user_id = ud1.user_id
                            join user_data ud2 on ul.match_id = ud2.user_id
                            where ul.user_id not in (select user_id from advertisement_campaign)
                            and ul.user_id not in (1648758,1648760,1126423)
                            and ul.match_id not in (select user_id from advertisement_campaign)
                            and ul.match_id not in (1648758,1648760,1126423)
                            group by u.gender, liker_country, likee_country";

        $mutual_obj = $this->conn_reporting->Execute($sql_mutual_like);
        $mutual_data = array();
        while($row = $mutual_obj->FetchRow())
        {
            $mutual_data[$row['liker_country'] ."_".$row['likee_country']."_".$row['gender']] = $row;
        }

        $engage = array();
        foreach($likes_data as $key => $val)
        {
            $engage[$key]['likes'] = $likes_data[$key];
            $engage[$key]['hides'] = $hides_data[$key];
            $engage[$key]['sparks'] = $spark_data[$key];
            $engage[$key]['mutual_likes'] = $mutual_data[$key];
            $engage[$key]['gender'] = $likes_data[$key]['gender'];
            $engage[$key]['liker_country'] = $likes_data[$key]['liker_country'];
            $engage[$key]['likee_country'] = $likes_data[$key]['likee_country'];
            $inverse_pi = $likes_data[$key]['all_actions'] /($likes_data[$key]['all_actions'] + $hides_data[$key]['all_actions'] + $spark_data[$key]['all_actions']) ;
            $engage[$key]['likes']['inverse_pi'] = round(100 * $inverse_pi,2) . " %";
            //$engage[$key]['hides']['inverse_pi'] = $hides_data[$key]['all_actions'] /($likes_data[$key]['all_actions'] + $hides_data[$key]['all_actions'] + $spark_data[$key]['all_actions']) ;
            //$engage[$key]['sparks']['inverse_pi'] = $likes_data[$key]['all_actions'] /($likes_data[$key]['all_actions'] + $hides_data[$key]['all_actions'] + $spark_data[$key]['all_actions']) ;
        }
        return $engage ;
       // print_r(json_encode($engage)) ; die ;
    }

    private function getLocations($gender, $case)
    {
        $sql ="select if(pos<9 ,city,'Others') as city_grp, sum(users) as c_u from (select city, users, @row := @row + 1 AS pos from
              (select case when gc.name is not null then gc.name else gs.name end as city, count(u.user_id) as users from user u
             join user_data ud on u.user_id=ud.user_id
             left join user_search us on u.user_id = us.user_id
             left join geo_city gc on gc.city_id=ud.stay_city and gc.state_id= ud.stay_state
             left join geo_state gs on gs.state_id=ud.stay_state
             where u.gender='$gender' and ud.stay_country= 254 and (u.status ='authentic' or u.last_changed_status ='authentic') and" ;
        if($case == 'active')
            $sql .= " us.user_id is not null" ;
        else
            $sql .= " registered_at >='$this->start' and  registered_at <'$this->end'" ;

        $sql .=   " group by city order by users desc ) t  JOIN    (SELECT @row := 0) r  )s group by city_grp order by case when city_grp='Others'  then 1 else 0 end, c_u desc";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = $res->GetRows();
        $city_arr = array();
        $sum_city= 0;
        foreach ($data as $val){
            $sum_city += $val['c_u'];
        }
        $i=0;
        foreach ($data as $val) {
            $city_arr[$i]= $val;
            $city_arr[$i]['perc'] = intval ( 100 * ($val['c_u'] / $sum_city) );
            $i++;
        }
        $city_arr[]=array('city_grp' => "Total",
                           'c_u' =>$sum_city,
                           'perc' => 100) ;
        return $city_arr;
    }

    private function getAgeData($case)
    {
        $sql_age= "select gender, COUNT(*) AS count_all, SUM(IF(t.age<=21,1,0)) AS age_18, SUM(IF(t.age<=24 && t.age >=22 ,1,0))  AS age_22,
		 SUM(IF(t.age>=25 && t.age <=28,1,0)) AS age_25, SUM(IF(t.age>=29 && t.age <=32,1,0)) AS age_29 , SUM(IF(t.age>32,1,0)) AS age_33,
		 CAST(100*( SUM(IF(t.age<=21,1,0)) /COUNT(*) ) AS unsigned) as age_18_p,
		 CAST(100*( SUM(IF(t.age<=24 && t.age >=22 ,1,0)) /COUNT(*) ) AS unsigned) as age_22_p,
		 CAST(100*( SUM(IF(t.age>=25 && t.age <=28,1,0)) /COUNT(*) ) AS unsigned) as age_25_p,
		 CAST(100*( SUM(IF(t.age>=29 && t.age <=32,1,0)) /COUNT(*) ) AS unsigned) as age_29_p,
		 CAST(100*( SUM(IF(t.age>32,1,0)) /COUNT(*) ) AS unsigned) as age_33_p FROM
                   ( SELECT u.user_id,u.gender, EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(CURRENT_TIMESTAMP,ud.DateOfBirth)))) AS age
                   FROM user u JOIN user_data ud ON u.user_id=ud.user_id
                   left join user_search us on u.user_id = us.user_id
                   WHERE ";
        if($case=='active')
            $sql_age .= " us.user_id is not null" ;
        else
            $sql_age .= " registered_at >='$this->start' and  registered_at <'$this->end'" ;

        $sql_age .=  " and ud.stay_country= 254 and (u.status='authentic' or u.last_changed_status='authentic')  )t group by gender";
        $res_age= $this->conn_reporting->Execute($sql_age);
        $age_arr = $res_age->GetRows();
        return $age_arr;
    }

    private function getSparkData()
    {
        $sql = "select  gender, ust.price, count(*) as ca from user_spark_transaction ust join user u  on u.user_id = ust.user_id
                join user_data ud on u.user_id = ud.user_id
                join spark_packages sp on sp.package_id = ust.package_id
                where  ust.tstamp >='$this->start' and ust.tstamp <'$this->end' and ust.status='consumed' and
                ust.spark_count > 0 and sp.currency ='dollar'
                group by  gender, ust.price";
        $res = $this->conn_reporting->Execute($sql) ;
        $data = array();
        while($row = $res->FetchRow())
        {
            $gender =$row['gender'];
            $data[$row['price']][$gender] = $row['ca'];
        }
        return $data;
    }

}

try
{

    $isttime = date ( 'Y-m-d H:i' ,strtotime ( '+330 minutes' ) );
    $indianTime = Utils::getDateGMTTOISTfromUnixTimestamp(microtime(true));
    $start_date=date('Y-m-d 18:30:00', strtotime('-8 day', strtotime($isttime)));
    $end_date = date ( 'Y-m-d 18:30:00',strtotime('-1 day', strtotime($isttime)));
    $subject = "US Weekly Analysis ". $indianTime;

    $us_report = new USWeekly($start_date,$end_date);
    $data = $us_report->getWeeklyData();
    Utils::sendEmail('sumit@trulymadly.com,lavi@trulymadly.com,purav@trulymadly.com,rahul@trulymadly.com','admintm@trulymadly.com',$subject,$data,true);

}
catch(Exception $e)
{
    echo $e->getMessage();
    Utils::sendEmail('backend@trulymadly.com','sumit@trulymadly.com','US engagement reporting failed',$e->getMessage(),E_USER_WARNING);
}


?>