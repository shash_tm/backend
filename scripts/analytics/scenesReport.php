<?php
require_once  dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once  dirname ( __FILE__ ) . "/../../include/Utils.php";


$options=getopt ( "d:" );
if ($options[d])
{
    $dur = $options[d];
}

class scenesReport
{
    protected $conn_reporting;
    private $start ;
    private $end ;
    private $smarty ;
    private $redis ;
    private $cityData;
    private $redis_key = "scenes_cumulative_data";
    private $ncr  = "16743,47965,47964,47968,47967,47966" ;
    private $testUsers = "605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919,281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063";
    function __construct()
    {
        global $conn_reporting, $smarty, $redis;
        $this->conn_reporting = $conn_reporting;
        $this->conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->start;
        $this->end ;
        $this->smarty = $smarty;
        $this->redis = $redis;
    }
    /*Time taken for different events*/
    function getStats($start_time, $end_time)
    {
        $this->start = $start_time ;
        $this->end = $end_time ;

        $this->getCityData();
        echo " getting one day scenes data ".PHP_EOL;
        $today_scenes_data = $this->getClickScenes();
        $this->smarty->assign("scenes",$today_scenes_data);
        $this->getPreviousData($today_scenes_data);


    }

    private function getPreviousData($today_scenes_data)
    {
        if(date("w") == 0)
        {
            // delete the previous data every sunday morning
            $this->redis->del($this->redis_key);
        }
        $previous_data = $this->redis->get($this->redis_key) ;
        $cumulative_data = array();

        if( is_array(json_decode($previous_data, true)) == true)
        {
            // previous data is present add that to cumulative wali sheet
            $previous_data_arr_with_date = json_decode($previous_data, true) ;
            if($previous_data_arr_with_date['time'] == $this->end)
            {
                echo "same time , not adding further";
                $cumulative_data = $previous_data_arr_with_date['data'];
            }
            else
            {
                $previous_data_arr = $previous_data_arr_with_date['data'];
                foreach($today_scenes_data as $key => $val)
                {
                    $cumulative_data[$key] = $val ;
                    foreach($val as $k => $v)
                    {
                        if(is_array($v) == true)
                        {
                            $cumulative_data[$key][$k]['M'] += $previous_data_arr[$key][$k]['M'];
                            $cumulative_data[$key][$k]['F'] += $previous_data_arr[$key][$k]['F'];
                            $cumulative_data[$key][$k]['MF'] = intval(100 * ($cumulative_data[$key][$k]['F']/ $cumulative_data[$key][$k]['M'])) ;
                        }
                    }

                }
            }

        }
        else
        {
            $isttime= date ( 'Y-m-d H:i:s');
            $this->start = date('Y-m-d 00:00:00', strtotime('-40 day', strtotime($isttime)));
            echo "Getting cumulative data, this may take some time ".PHP_EOL;
            $cumulative_data = $this->getClickScenes();
        }
        $cumulative_data_with_date = array('time' => $this->end, 'data'=>$cumulative_data);
        $this->redis->set($this->redis_key,json_encode($cumulative_data_with_date));
        $this->smarty->assign("cumulative_scenes",$cumulative_data);
        // if previous data is not present then get last 40 days data and add it to to redis and return the value

    }

    private  function getCityData()
    {
        $sql ="select if(geo.city_id in ($this->ncr),'Delhi/NCR',geo.name) as city, case when activity='matches' then 'Tutorial'
              when event_type='list_category' then 'IconCliked' when event_type='list_event' then 'CategoryClicked' else 'EventDetails' end as user_step,
              count(distinct(log.user_id)) as users from action_log_new log
              join user_data udata on udata.user_id = log.user_id
            join geo_city geo on geo.city_id = udata.stay_city
            where  ((activity = 'matches' and event_type = 'scenes_tutorial' and event_status = 'started' )
            or (activity ='scenes' and event_type in ('details_event','details_event_followed','details_following_event','list_category','list_event')))
            and log.user_id not in ($this->testUsers)  and log.tstamp >'$this->start'  and log.tstamp <'$this->end'
             and ( udata.stay_city in (select distinct(city_id) from datespot_location ) or udata.stay_city in ($this->ncr))
             group by city, user_step
            order by users desc";
        //echo $sql ; die ;
        $res = $this->conn_reporting->Execute($sql);
        $data = array();
        while($row = $res->FetchRow())
        {
            $data[$row['city']][$row['user_step']]= $row['users'];
        }
        $this->smarty->assign("cities",$data);
    }





    private  function getClickScenes()
    {
        $sql ="select d.name,ol.location, date(d.create_date) as cd, t.*, ec.name as category from
(select if(event_type='send',trim(both '\"' from JSON_EXTRACT(event_info, \"$.event_id\")), event_status) as event_status_new,
case when event_type in ('details_event','details_event_followed','details_following_event') then  'details'
when event_type='send' then 'spark_sent' else event_type  end as user_step,
gender, count(distinct(u.user_id)) as users, count(u.user_id) as actions
from action_log_new log join user u on u.user_id = log.user_id where ((activity = 'scenes'
and event_type in ('details_event','details_event_followed','details_following_event',
 'share_event','share_outside','follow_event') )
 or (activity='sparks' and event_type='send' and event_status='send') and trim(both '\"' from JSON_EXTRACT(event_info, \"$.from_scenes\")) = 'true')
 and u.user_id not in (605134,160486,113406,584,19206,143804,81144,17266,1251921,198657,17266,587441,19063,1026,1170129,271069,16919,281684,160486,707,1966,173933,1643,2136,587441,198657,2173,20188,204236,482991,286172,17266,16093,418169,19063)
and log.tstamp >'$this->start' and log.tstamp <'$this->end' group by gender, event_status_new, user_step)t
join (select hash_id, if(gc.name is null, gs.name, gc.name) as location from
(select hash_id, city_id, state_id from datespot_location dl group by datespot_id)l
join geo_state gs on gs.state_id = l.state_id
left join geo_city gc on gc.city_id = l.city_id and gc.state_id = l.state_id)ol on ol.hash_id = t.event_status_new
join event_category_mapping ecm on ecm.datespot_id = t.event_status_new
join event_category ec on ec.category_id = ecm.category_id
join datespots d on d.hash_id = t.event_status_new order by location,category, d.name";
        echo "runnning query";
        $res = $this->conn_reporting->Execute($sql) ;
        echo "query ran, Parsing result";
        $data = array();
        $sum_array = array();
        while($row = $res->FetchRow())
        {
            $data[$row['event_status_new']]['name'] = $row['name'] ;
            $data[$row['event_status_new']]['location'] = $row['location'] ;
            $data[$row['event_status_new']]['cd'] = $row['cd'] ;
            $data[$row['event_status_new']]['category'] = $row['category'] ;
            $data[$row['event_status_new']][$row['user_step']][$row['gender']] = $row['users'] ;
            $sum_array[$row['user_step']][$row['gender']] += $row['users'];
            if(isset($data[$row['event_status_new']][$row['user_step']]['M']) && $data[$row['event_status_new']][$row['user_step']]['F'])
            {
                $data[$row['event_status_new']][$row['user_step']]['MF'] = intval(100 * ($data[$row['event_status_new']][$row['user_step']]['F']/$data[$row['event_status_new']][$row['user_step']]['M'])) ;
            }
        }
        foreach($sum_array as $key=>$val)
        {
            $val['MF'] = intval(100 * ($val['F']/$val['M']));
            $sum_array[$key]=$val;
        }

        $sum_array["name"] = "Total";
        $data["Sum"] = $sum_array;
        return $data ;
    }
}


/*$isttime= date ( 'Y-m-d H:i:s');
$start_date=date('Y-m-d 00:00:00', strtotime('-1 day', strtotime($isttime)));
$end_date = date ( 'Y-m-d 00:00:00');
$scene = new scenesReport();
echo "Getting scenes report" . PHP_EOL;

$data = $scene->getStats($start_date, $end_date);

$message = $smarty->Fetch(dirname(__FILE__) . '/../../templates/reporting/scenesReport.tpl');
$to = "sumit@trulymadly.com";
//$to=  'sumit@trulymadly.com';
$subject = 'Scenes Daily Report ' . $date;
$from = 'admintm@trulymadly.com';
Utils::sendEmail($to, $from, $subject, $message, true);*/

?>
