<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../mobile_utilities/pushNotification.php";


$conn_reporting->SetFetchMode ( ADODB_FETCH_ASSOC );
$conn_master->SetFetchMode ( ADODB_FETCH_ASSOC );


/**
 * @Himanshu
 * Mailer for vouchers includes push notiication as well
 */


function sendPushNotification($data, $tmPic, $voucher_codes){
	global $admin_id;
	$pushnotify = new pushNotification();
	
	$title_text = "Rs. 3,000 Vero Moda Gift Hamper awaits.";
	$ticker_text = "Message from Trulymadly";
	$ticker_content_text = "Love Surprises? We thought you would.";
	
	$voucher_data['voucher_code'] =  $voucher_codes [ $data ['user_id'] ] ;
	$voucher_data['user_id'] =  $data ['user_id'] ;
	$voucher_data['expiry'] =  '2015-11-01 00:00:00' ;
	$voucher_data['campaign'] = 'Vero_Moda';
	$voucher_data['friendly_text'] = "We love that you’re enjoying TrulyMadly! Here's some love from us" . PHP_EOL . "Rs. 3,000 Vero Moda Gift Hamper for you.)";
	//:'17423','expiry':'2016-01-01 00:00:00','campaign':'JABONG_500','friendly_text':'this is friendsly text'}"
	//var_dump(json_encode($voucher_data));
	
	$pushnotify->notify($data['user_id'], array("content_text"=>$ticker_content_text, "voucher_data" =>json_encode($voucher_data),"pic_url"=>$tmPic,"ticker_text"=>$ticker_text,"title_text"=>$title_text,"push_type"=>"VOUCHER"),$admin_id);
}


function getTMPic(){
	global $conn_reporting, $admin_id, $imageurl;
	$sql = $conn_reporting->Prepare("select thumbnail as pic from user_photo where user_id = ?");
	$ex = $conn_reporting->Execute($sql, array($admin_id));
	$res = $ex->FetchRow();
	return  $imageurl.$res['pic'];
}



function generateQuery($user_ids){
			//	join user_app_status uap on uap.user_id= v.user_id
	$sql1= "select u.user_id, u.fname from user u
				WHERE  u.user_id in ($user_ids)
				and u.status in ('authentic', 'non-authentic')  ";
	return  $sql1;
}
function sendVoucher( )
{
	global $config, $conn_reporting, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl, $admin_id;

	$tmPic = getTMPic();
	$user_ids = "478838, 506622, 545085, 20167 ";
	$voucher_array = array( 478838	=> 'VM32952325' ,
							506622	=> 'VM37467296' ,
							545085	=> 'VM47366475',
							20167	=> 'VM25465315',
							18726 =>  'VM1234224'
	);
	
	//query needs to be changed in case u want to send multiple vouchers to same user (null check to be removed)
	$sql1= generateQuery($user_ids);
	

	$res = $conn_reporting->Execute($sql1);
	$data =$res->GetRows();

	//var_dump($data); die;
	foreach ($data as $rows){
		$voucher = null;

		sendPushNotification($rows, $tmPic, $voucher_array);
	}
}


try{
	global $baseurl;
	$options=getopt ( "e:" );
	sendVoucher();
}
catch (Exception $e){
	echo $e->getMessage();
	global $baseurl, $techTeamIds;
	$subject = $baseurl. " - Voucher Notification failed";
	//Utils::sendEmail($techTeamIds, $baseurl, $subject, $e->getTraceAsString());
	trigger_error ( "PHP WEB: Voucher Notification failed". $e->getMessage(), E_USER_WARNING );
}

?>
