<?php 

require_once dirname ( __FILE__ ) . "/../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../abstraction/query_wrapper.php";
require_once dirname ( __FILE__ ) . "/../include/Utils.php";


function getFileName(){
	/*if(!file_exists($this->filepath)){
		$fileDetails =  array("filename" => "profile_pic_shuffling.txt", "path" => "/tmp");
		$filename = $fileDetails['filename'];
		$completePath = $fileDetails['path'] . "/$filename";
	}else{
		$completePath = $this->filepath;
	}*/
	     $completePath="/tmp/profile_pic_shuffling.txt";
	     chmod($completePath, 0777);
	     return $completePath;
	/*
	$completePath="/tmp/profile_pic_shuffling.txt";
	chmod($completePath, 0777);
	return $completePath;*/
}

function loadData(){
	global $conn_master ,$conn_reporting, $baseurl;
	$tempTable="photo_views_male_temp";
	try{

		$dropTempTable = "DROP TABLE IF EXISTS $tempTable";
		//var_dump($dropTempTable);
		$conn_master->Execute($dropTempTable);
		//echo "\n Table Dropped\n";
		$sql_create="CREATE TABLE $tempTable (
		`photo_id` int(11) NOT NULL,
		`user_id` int(11) NOT NULL,
		`likes` int(10) DEFAULT '0',
		`hides` int(10) DEFAULT '0',
		`last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		`shuffled` int(2) DEFAULT '0',
		PRIMARY KEY (`photo_id`),
		KEY `key1` (`user_id`),
		KEY `key2` (`shuffled`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1";
		
		$conn_master->Execute($sql_create);
		echo "\ntable created\n";
		$completePath = getFileName();
		//var_dump($completePath);
		if(filesize($completePath) > 0 ){
			$sql = "LOAD DATA LOCAL INFILE '$completePath' INTO TABLE $tempTable fields terminated by '\t' ";
			echo $sql;
			$ex = $conn_master->Execute($sql);
			echo "\nloaded data into temp table\n";
			// var_dump($conn_master->isWarning());
			$warningQuery = "SHOW WARNINGS";
			$warnings = $conn_master->Execute($warningQuery);
			$warningCount = $warnings->RowCount();
			//var_dump($warningCount);
			if($warningCount > 0 ){
				$msg = "Warnings in loading data (Profile pic shuffle ):" . $warningCount ."\n" ;
				for($i=0 ; $i<$warningCount;$i++){
					while ($row = $warnings->FetchRow()) {
						foreach ($row as $val){
							$msg .= "$val\t";
						}
						$msg .="\n";
					}
				}
				var_dump($msg);
				//Utils::sendEmail("arpan@trulymadly.com, shashwat@trulymadly.com", "himanshu@trulymadly.com", " User Search warnings in loading data", $msg, false);
			}
		}else {
			$to = "arpan@trulymadly.com";
			$subject = "Profile Pic Shuffling file on tmp directory is empty for" . $baseurl ;
			$from = "arpan@trulymadly.com";
			Utils::sendEmail($to, $from, $subject, false);
		}
		/*$sql_select="select * from $tempTable";
		$rs=$conn_reporting->Execute($sql_select);
		foreach($rs as $value)
		{
			var_dump($value);
		}*/
		$sql_update="INSERT INTO `photo_views_male`
		(photo_id,user_id,likes,hides,last_updated,shuffled)
		select photo_id,user_id,likes,hides,last_updated,shuffled from $tempTable
		ON DUPLICATE KEY UPDATE
		likes =$tempTable.likes + photo_views_male.likes,
		hides=$tempTable.hides + photo_views_male.hides,
		last_updated=$tempTable.last_updated,
		shuffled=$tempTable.shuffled";
		
		$conn_master->Execute($sql_update);
			
		echo "Table updated";
		
		/*
		//updating
		$sql_update="UPDATE photo_views_male t1 JOIN $tempTable t2 on t1.photo_id=t2.photo_id SET t1.likes = t2.likes+ t1.likes, t1.hides=t2.hides + t1.hides, t1.shuffled=t2.shuffled, t1.last_updated=now()";

		$conn_master->Execute($sql_update);
        echo "\nphoto_views_male table updated\n";
		//insertion
		$sql_update="UPDATE photo_views_male t1 RIGHT JOIN $tempTable t2 on t1.photo_id=t2.photo_id SET t1.photo_id=t2.photo_id, t1.user_id=t2.user_id, t1.likes = t2.likes, t1.hides=t2.hides, t1.shuffled=t2.shuffled, t1.last_updated=now() where t1.photo_id IS NULL";
			
		$conn_master->Execute($sql_update);
		echo "\nphoto_views_male updated\n";
        */ 
		
		$dropTable = "DROP TABLE IF EXISTS $tempTable";
		//Query::UPDATE($dropTable, null);
		$conn_master->Execute($dropTable);
		echo "\ntable Dropped\n";

	}
	catch (Exception $e){
		trigger_error($e->getMessage());
		echo $e->getTraceAsString();
	}
}


function populate_db()
{
	global $conn_reporting;
	$filename=getFileName();
	//var_dump($filename);
	
    if(file_exists($filename))
    		unlink($filename);
	//selecting likes and hides for last 6 hours
	echo "\nGetting users and there likes and hides\n";
	$sql_select="select us.user_id, ifnull(ul.likes,0) as likes, ifnull(uh.hides,0) as hides from user_search us left join
(select user2, count(user1) as likes from user_like where timestamp>=date_sub(now(),interval 7 day) group by user2)ul on us.user_id=ul.user2 left join
(select user2, count(user1) as hides from user_hide where timestamp>=date_sub(now(),interval 7 day) group by user2)uh on us.user_id=uh.user2 where gender='M' ";

	//echo $sql_select;
	$rs=$conn_reporting->Execute($sql_select);
	echo "\nGetting users and there likes and hides -DONE\n";
	//$rs=Query_Wrapper::SELECT($sql_select,array());
	$timestamp = date('Y-m-d G:i:s');
	foreach($rs as $value)
	{
		//var_dump($value['user_id']);
			
			
		$sql_profile_pic="select photo_id, shuffled from user_photo where user_id=? and status='active' and system_profile='yes' and admin_approved='yes'";
		$param_array=array($value['user_id']);
		$photo_data_conn=$conn_reporting->Execute($sql_profile_pic,$param_array);
		$photo_data=$photo_data_conn->GetRows();
		$count=$photo_data_conn->rowCount();
		//echo 'The count is '.$count.PHP_EOL;
		//var_dump($photo_data);
		if($count>0)
		{
			$str = '';
			/*var_dump($photo_data[0]['photo_id']);
			 var_dump($value['user_id']);
			 var_dump($value['likes']);
			 var_dump($value['hides']);
			 var_dump($photo_data[0]['shuffled']);*/
			$str .= (!isset($photo_data[0]['photo_id']))?"\N\t":$photo_data[0]['photo_id']."\t";
			$str .= (!isset($value['user_id']))?"\N\t":$value['user_id']."\t";
			$str .= (!isset($value['likes']))?"\N\t":$value['likes']."\t";
			$str .= (!isset($value['hides']))?"\N\t":$value['hides']."\t";
			$str .= $timestamp."\t";
			$str .= (!isset($photo_data[0]['shuffled']))?"\N\t":$photo_data[0]['shuffled']."\t";
		
			$str .="\n";
			//var_dump($str);
		
		
			file_put_contents($filename, $str, FILE_APPEND | LOCK_EX);
		}
		/*$sql_insert="insert into photo_views_male(photo_id,user_id,likes,hides,shuffled) values(?,?,?,?,?) on duplicate key update likes=?, hides=?, shuffled=?";
			$param_array=array($photo_data[0]['photo_id'],$value['user_id'],$value['likes'],$value['hides'],$photo_data[0]['shuffled'],$value['likes'],$value['hides'],$photo_data[0]['shuffled']);
			$tablename="photo_views_male";
			Query_Wrapper::INSERT($sql_insert,$param_array,$tablename,"master",false,$value["user_id"]);*/
	}
	echo "\nPopulating photo_views_male\n";
	loadData();
}


/*
function populate_db()
{
	global $conn_reporting;
	$sql_select="select user_id from user_search where gender='M'";

	$rs=$conn_reporting->Execute($sql_select);
	//$rs=Query_Wrapper::SELECT($sql_select,array());
	foreach($rs as $value)
	{
		var_dump($value['user_id']);
		$sql_profile_pic="select photo_id, shuffled from user_photo where user_id=? and status='active' and system_profile='yes' and admin_approved='yes'";
		$param_array=array($value['user_id']);
		$photo_data=Query_Wrapper::SELECT($sql_profile_pic,$param_array);
		var_dump($photo_data);
		
		$sql_likes="select count(user1) as likes from user_like where user2=? and timestamp>=date_sub(now(),interval 1 month)";
		$like=Query_Wrapper::SELECT($sql_likes,array($value['user_id']),"slave",true);
		$like=$like["likes"];
		
		$sql_hides="select count(user1) as hides from user_hide where user2=? and timestamp>=date_sub(now(),interval 1 month)";
		$hide=Query_Wrapper::SELECT($sql_hides,array($value['user_id']),"slave",true);
		$hide=$hide["hides"];
		

		$sql_insert="insert into photo_views_male(photo_id,user_id,likes,hides,last_updated, shuffled) values(?,?,?,?,now(),?) on duplicate key update likes=?, hides=?, shuffled=?, last_updated=now()";
		$param_array=array($photo_data[0]['photo_id'],$value['user_id'],$like,$hide,$photo_data[0]['shuffled'],$like,$hide,$photo_data[0]['shuffled']);
		$tablename="photo_views_male";
		Query_Wrapper::INSERT($sql_insert,$param_array,$tablename,"master",false,$value["user_id"]);
	}
}*/

/*
function populate_db()
{
	global $conn_reporting;
	$sql_select="select t1.user_id,t1.likes, t2.hides from
				 (select us.user_id, count(user1) as likes from user_search us left join user_like ul on us.user_id=ul.user2 where gender='M' and ul.timestamp>=date_sub(now(),interval 1 month) group by us.user_id) t1 
			     join
			     (select us.user_id, count(user1) as hides from user_search us left join user_hide uh on us.user_id=uh.user2 where gender='M' and uh.timestamp>=date_sub(now(),interval 1 month) group by us.user_id) t2 
			     on t1.user_id=t2.user_id";

	$rs=$conn_reporting->Execute($sql_select);
	//$rs=Query_Wrapper::SELECT($sql_select,array());
	foreach($rs as $value)
	{
		var_dump($value['user_id']);
		$sql_profile_pic="select photo_id, shuffled from user_photo where user_id=? and status='active' and system_profile='yes' and admin_approved='yes'";
		$param_array=array($value['user_id']);
		$photo_data=Query_Wrapper::SELECT($sql_profile_pic,$param_array);
		var_dump($photo_data);

		$sql_insert="insert into photo_views_male(photo_id,user_id,likes,hides,shuffled) values(?,?,?,?,?) on duplicate key update likes=?, hides=?, shuffled=?";
		$param_array=array($photo_data[0]['photo_id'],$value['user_id'],$value['likes'],$value['hides'],$photo_data[0]['shuffled'],$value['likes'],$value['hides'],$photo_data[0]['shuffled']);
		$tablename="photo_views_male";
		Query_Wrapper::INSERT($sql_insert,$param_array,$tablename,"master",false,$value["user_id"]);
	}
}*/

function update_shuffled_flag()
{
	global $conn_reporting;
	//updating shuffled flag on total views
	echo "\nUpdating Shuffled Flag\n";
	$sql_select="select * from photo_views_male where user_id in (select user_id from user_search where gender='M')";
	$rs=$conn_reporting->Execute($sql_select);
	foreach ($rs as $value) {
		if($value["likes"]+$value["hides"]>=20)
			$new_shuffled=2;
			else if ($value["likes"]+$value["hides"]>=10)
				$new_shuffled=1;
				else
					$new_shuffled=0;
						
					if($new_shuffled!=$value["shuffled"])
					{
						$sql="update user_photo set shuffled=? where photo_id=?";
						$param_array=array($new_shuffled,$value["photo_id"]);
						$tablename="user_photo";
						Query_Wrapper::INSERT($sql,$param_array,$tablename,"master",$value["user_id"]);

						$sql="update photo_views_male set shuffled=? where photo_id=?";
						$param_array=array($new_shuffled,$value["photo_id"]);
						$tablename="photo_views_male";
						Query_Wrapper::INSERT($sql,$param_array,$tablename,"master",$value["user_id"]);
					}
	}
}

try {
	populate_db();
	update_shuffled_flag();
	echo "\nDone\n";
} catch (Exception $e) {
	echo $e->getMessage();
	
}


?>
