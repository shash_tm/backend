<?php 
/*
 * @uthor:Arpan
 * Valar Morghulis
 * file to populate user data in redis key
 */
require_once dirname(__FILE__).'/../include/config_admin.php';
require_once dirname(__FILE__).'/../include/config.php';
require_once dirname(__FILE__).'/../include/Utils.php';

class Populate {
	private $userId_array;
	private $user_lastlogin;
	private $user_basic;
	private $user_data;
	private $user_trustData;
	private $user_interests;
	private $user_photo;
	private $conn;
	private $redis;
	function __construct() {
		global $conn_slave,$redis;
		$this->userId_array=array();
		$this->user_basic=array();
		$this->user_data=array();
		$this->user_trustData=array();
		$this->user_interests=array();
		$this->user_photo=array();
		$this->conn=$conn_slave;
		$this->redis = $redis;
	}
	
	function storeUserSearch() {
	    $rs = $this->conn->Execute($this->conn->prepare("SELECT user_id FROM user_search"));
		$i=0;
		while ($val=$rs->FetchRow()) {
			$this->userId_array[$i]=$val['user_id'];
			$i++;	
		}
		foreach ($this->userId_array as $userId)
		{   $key=Utils::$redis_keys['user_data'].$userId;
		    //var_dump($key);
		    echo "\n";
			$this->getJSON($userId);
			$this->redis->delete($key);
			$this->redis->hMset($key, array('lastlogin'=>$this->user_lastlogin,'basic' => $this->user_basic, 'data' => $this->user_data, 'trust_score' => $this->user_trustData, 'interest_hobbies' => $this->user_interests, 'photos' => $this->user_photo));
			$this->redis->setTimeout($key,604800);
			echo "hash set \n";
			$this->clearVar();
		}
		//var_dump($this->userId_array);
	}
	 private function clearVar()
	 { 
	 	$this->user_lastlogin=array();
	 	$this->user_basic=array();
		$this->user_data=array();
		$this->user_trustData=array();
		$this->user_interests=array();
		$this->user_photo=array();
	 }
	
	private function getJSON($userId){
		$this->getLastlogin($userId);
		$this->getBasicData($userId);
		$this->getData($userId);
		$this->getInterests($userId);
		$this->getTrustScoreData($userId);
		$this->getPhoto($userId);
	}
	
	private function getLastlogin($userId) {
		$rs = $this->conn->Execute($this->conn->prepare("SELECT last_login from user_lastlogin where user_id=?"),array($userId));
		$val=$rs->FetchRow();
		$this->user_lastlogin['last_login']=$val['last_login'];
		$this->user_lastlogin['validation_flag']=1;
		//$this->user_basic=json_encode($this->user_basic);
		//var_dump($this->user_basic);
		$this->user_lastlogin=json_encode($this->user_lastlogin);
	
	}
	
	private function getBasicData($userId) {
		$rs = $this->conn->Execute($this->conn->prepare("SELECT user_id,gender,status,fname from user where user_id=?"),array($userId));
		$val=$rs->FetchRow();
		$this->user_basic['gender']=$val['gender'];
		$this->user_basic['status']=$val['status'];
		$this->user_basic['fname']=$val['fname'];
		$this->user_basic['validation_flag']=1;
		$this->user_basic=json_encode($this->user_basic);
		//var_dump($this->user_basic);
	}
	
	private function getData($userId) {
		$sql="SELECT ud.user_id, tc.is_verified_celeb, ud.marital_status,  ud.stay_country, ud.stay_state, ud.haveChildren, ud.height, ud.stay_city,
		ud.income_start, ud.income_end, ud.designation, ud.work_status, ud.company_name, ud.institute_details, ud.highest_degree,ud.industry, 
		i.name as industry_name,
		hd.degree_name as degree, hd.rank as education_group, FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age,
		 gci.display_name as stay_city_display,gs.name as state
		FROM user_data ud
		LEFT JOIN geo_state gs on ud.stay_state = gs.state_id
   		LEFT JOIN geo_city gci on ud.stay_city = gci.city_id AND gci.state_id = ud.stay_state 
   		LEFT JOIN industries_new i on ud.industry = i.id 
   		LEFT JOIN highest_degree hd on ud.highest_degree = hd.id 
   		LEFT JOIN tm_celeb tc on ud.user_id=tc.user_id
		where ud.user_id=? ";
		$rs = $this->conn->Execute($this->conn->prepare($sql),array($userId));
		$val=$rs->FetchRow();
		$this->user_data['is_verified_celeb']=$val['is_verified_celeb'];
		$this->user_data['marital_status']=$val['marital_status'];
		$this->user_data['stay_country']=$val['stay_country'];
		$this->user_data['stay_state']=$val['stay_state'];
		$this->user_data['haveChildren']=$val['haveChildren'];
		$this->user_data['height']=$val['height'];
		$this->user_data['stay_city']=$val['stay_city'];
		$this->user_data['income_start']=$val['income_start'];
		$this->user_data['income_end']=$val['income_end'];
		$this->user_data['designation']=$val['designation'];
		$this->user_data['work_status']=$val['work_status'];
		$this->user_data['company_name']=$val['company_name'];
		$this->user_data['institute_details']=$val['institute_details'];
		$this->user_data['highest_degree']=$val['highest_degree'];
		$this->user_data['industry']=$val['industry'];
		$this->user_data['industry_name']=$val['industry_name'];
		$this->user_data['degree']=$val['degree'];
		$this->user_data['education_group']=$val['education_group'];
		$this->user_data['age']=$val['age'];
		$this->user_data['stay_city_display']=$val['stay_city_display'];
		$this->user_data['state']=$val['state'];
		$this->user_data['validation_flag']=1;
		$this->user_data=json_encode($this->user_data);
		//var_dump($this->user_data);
	}
	
	private function getTrustScoreData($userId) {
		$sql="SELECT * FROM  user_trust_score WHERE user_id=?";
		$rs = $this->conn->Execute($this->conn->prepare($sql),array($userId));
		$val=$rs->FetchRow();
		$this->user_trustData['fb_connections']=$val['fb_connections'];
		$this->user_trustData['linkedin_connections']=$val['linkedin_connections'];
		$this->user_trustData['mobile_number']=$val['mobile_number'];
		$this->user_trustData['address']=$val['address'];
		$this->user_trustData['id_proof']=$val['id_proof'];
		$this->user_trustData['employment']=$val['employment'];
		$this->user_trustData['trust_score']=$val['trust_score'];
		$this->user_trustData['fb_credits']=$val['fb_credits'];
		$this->user_trustData['linked_credits']=$val['linked_credits'];
		$this->user_trustData['address_credits']=$val['address_credits'];
		$this->user_trustData['id_credits']=$val['id_credits'];
		$this->user_trustData['employment_credits']=$val['employment_credits'];
		$this->user_trustData['mobile_credits']=$val['mobile_credits'];
		$this->user_trustData['id_type']=$val['id_type'];
		$this->user_trustData['address_verified_on']=$val['address_verified_on'];
		$this->user_trustData['employment_verified_on']=$val['employment_verified_on'];
		$this->user_trustData['linkedin_designation']=$val['linkedin_designation'];
		$this->user_trustData['company_name']=$val['company_name'];
		$this->user_trustData['endorsement_count']=$val['endorsement_count'];
		$this->user_trustData['endorsement_score']=$val['endorsement_score'];
		$this->user_trustData['validation_flag']=1;
		$this->user_trustData=json_encode($this->user_trustData);
		//var_dump($this->user_trustData);
	}
	
	private function getInterests($userId) {
		$sql="select * from interest_hobbies where user_id=?";
		$rs = $this->conn->Execute($this->conn->prepare($sql),array($userId));
		$val=$rs->FetchRow();
		$this->user_interests['music_preferences']=$val['music_preferences'];
		$this->user_interests['music_favorites']=$val['music_favorites'];
		$this->user_interests['books_preferences']=$val['books_preferences'];
		$this->user_interests['books_favorites']=$val['books_favorites'];
		$this->user_interests['food_preferences']=$val['food_preferences'];
		$this->user_interests['food_favorites']=$val['food_favorites'];
		$this->user_interests['movies_preferences']=$val['movies_preferences'];
		$this->user_interests['movies_favorites']=$val['movies_favorites'];
		$this->user_interests['sports_preferences']=$val['sports_preferences'];
		$this->user_interests['other_favorites']=$val['other_favorites'];
		$this->user_interests['travel_preferences']=$val['travel_preferences'];
		$this->user_interests['travel_favorites']=$val['travel_favorites'];
		$this->user_interests['other_preferences']=$val['other_preferences'];
		$this->user_interests['preferences']=$val['preferences'];
		$this->user_interests['tstamp']=$val['tstamp'];
		$this->user_interests['validation_flag']=1;
		$this->user_interests=json_encode($this->user_interests);

	}
	
	private function getPhoto($userId) {
		$sql="select * from user_photo where user_id=? and status='active'";
		
		$rs = $this->conn->Execute($this->conn->prepare($sql),array($userId));
		$i=0;
		while($val=$rs->FetchRow())
		{
			$this->user_photo['photo_data'][$i]['photo_id']=$val['photo_id'];
			$this->user_photo['photo_data'][$i]['name']=$val['name'];
			$this->user_photo['photo_data'][$i]['time_of_saving']=$val['time_of_saving'];
			$this->user_photo['photo_data'][$i]['is_profile']=$val['is_profile'];
			$this->user_photo['photo_data'][$i]['width']=$val['width'];
			$this->user_photo['photo_data'][$i]['height']=$val['height'];
			$this->user_photo['photo_data'][$i]['is_moderated']=$val['is_moderated'];
			$this->user_photo['photo_data'][$i]['from_fb']=$val['from_fb'];
			$this->user_photo['photo_data'][$i]['thumbnail']=$val['thumbnail'];
			$this->user_photo['photo_data'][$i]['status']=$val['status'];
			$this->user_photo['photo_data'][$i]['blur_image']=$val['blur_image'];
			$this->user_photo['photo_data'][$i]['blur_thumbnail']=$val['blur_thumbnail'];
			$this->user_photo['photo_data'][$i]['can_profile']=$val['can_profile'];
			$this->user_photo['photo_data'][$i]['admin_approved']=$val['admin_approved'];
			$this->user_photo['photo_data'][$i]['approval_time']=$val['approval_time'];
			$this->user_photo['photo_data'][$i]['original_name']=$val['original_name'];
			$this->user_photo['photo_data'][$i]['original_height']=$val['original_height'];
			$this->user_photo['photo_data'][$i]['original_width']=$val['original_width'];
			$i++;
		}
		$this->user_photo['validation_flag']=1;
		$this->user_photo=json_encode($this->user_photo);
	}
}
try {
	$pop=new Populate();
	$pop->storeUserSearch();
} catch (Exception $e) {
	echo "\NError Message:".$e->getMessage();
}

?>