<?php

require_once  dirname ( __FILE__ ) . "/../email/MailFunctions.php";
include dirname ( __FILE__ ) . "/../include/config_admin.php";
include dirname ( __FILE__ ) . "/../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../UserActions.php";
require_once dirname ( __FILE__ ) . "/../include/User.php";
require_once dirname ( __FILE__ ) . "/../UserUtils.php";
require_once dirname ( __FILE__ ) . "/../logging/systemLogger.php";
require_once dirname ( __FILE__ ) . "/../logging/UnsubscriptionClass.php";



/**
 * @Himanshu
 * Mailer for daily digest viz. like, messages, matches
 */
function notifyTechTeam($mailer_type){
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
		$subject = "mailed todays " . $mailer_type;
		$from = gethostname();//"admin@trulymadly.com";
		Utils::sendEmail($to, $from, $subject, null);
}

function passNameFromRuleEngine($user_id, $profile_id, $payment_mode, &$fname, &$lname){
	$UserActions=new UserActions();
	$actionsDone = $UserActions->getUserActionsDone($user_id, array($profile_id));
	$actions = $actionsDone[$profile_id];
	if(!(in_array(UserActionConstantValues::credit_used, $actions) || $payment_mode == 'unlimited')){
		$first_name_count=strlen($fname);
		$fname  = ucfirst(substr($fname , 0 ,1)) .  str_repeat("x",$first_name_count-1 );
		$last_name_count=strlen($lname);
		$lname  = ucfirst(substr($lname , 0 ,1)) . str_repeat("x",$last_name_count-1 );
	}
}
function msgsMailer($external_email = null, $external_date = null){
	try{
		global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl;
		$sysLog = new SystemLogger();
		
		//$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'
		//$sql = " select mq.sender_id, mq.receiver_id, mq.msg_content ,u.fname, u.lname, u.gender, up.name as pic,  up.status from messages_queue mq left join user u on u.user_id = mq.receiver_id join user_photo up on mq.receiver_id= up.user_id where tStamp >= CURDATE()-5 and  up.is_profile = 'yes' order by receiver_id, tStamp desc";
		if(isset($external_date))
		$sql = "select mq.sender_id, mq.receiver_id from messages_queue mq where mq.last_seen < mq.tStamp  and DATE(mq.tStamp) = '$external_date' order by mq.receiver_id, mq.tStamp desc";
		
		else
		$sql = "select mq.sender_id, mq.receiver_id from messages_queue mq where mq.last_seen < mq.tStamp  and DATE(mq.tStamp) = date_sub(CURDATE(), interval 1 day) order by mq.receiver_id, mq.tStamp desc";
		/*$sql = "select ul.user1, ul.user2, ul.timestamp, u.fname, u.lname, u.gender, up.name as pic,  up.status  from user_likeMe ul
		 left join user u on u.user_id = ul.user2
		 join user_photo up on ul.user2= up.user_id
		 where ul.timestamp = CURDATE() and  up.is_profile = 'yes' order by user2, timestamp desc";*/
		echo $sql;
		$rs = $conn_slave->Execute($sql);
		$data = $rs->GetRows();
		$uu = new UserUtils();
		$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=messages";
		$redirectionFile = "/msg/messages.php";
		$utm_content="view_messages";
		$campaignName = "messages";
		
		$i = 0;
		$prev_user = null;
		foreach ($data as $row => $col){

			if($prev_user == $col['receiver_id'] || $prev_user == null) $i++;
			else {
				$i=0;
				//TODO: email for prev id
			}

			if($i>4) continue;

			//passNameFromRuleEngine($col['receiver_id'], $col['sender_id'], $col['payment_mode'], $col['fname'], $col['lname']);

			$str = $col['receiver_id'] .'|' . $col['sender_id'];
			$mid = Utils::calculateCheckSum($str);

			$details = $uu->getAttributes("matches", array($col['sender_id']), $col['receiver_id'], null);
			
			$arr[$col['receiver_id']][] = array(
			
			
			 "user_id" => $col['sender_id'],
								  "name" => $details[$col['sender_id']]['fname'],// . ' ' . $col['lname'],
								  "age" =>$details[$col['sender_id']]['age'],
								  "religion" => ucfirst($details[$col['sender_id']]['religion']),
								  "city" => $details[$col['sender_id']]['city'],
								  "height" => $details[$col['sender_id']]['height'],
								  "pic" => $details[$col['sender_id']]['image_url'],
								  "trust_score" => (isset($details[$col['sender_id']]['trust_score'])?$details[$col['sender_id']]['trust_score']:0),
								  "profile_link" => $baseurl."/profile.php".$campaign."&utm_content=profile&uid=".$col['receiver_id']."&pid=$mid".'_'.$col['sender_id']
			
			);
			
			/*if(isset($details[$col['receiver_id']]['blurSet']))
			$arr[$col['receiver_id']]['blurSet'] = $details[$col['receiver_id']]['blurSet'];
*/
			$prev_user = $col['receiver_id'];


		}
		//var_dump(array_keys($arr)); die;
		$t = array_filter(array_keys($arr));// die;
		var_dump($t);
		if(count($t)>0){
			$sql2 = "SELECT fname, lname,email_id, user_id,email_status from user where user_id in (". implode(',', array_keys($arr)). ')';
			$ex = $conn_slave->Execute($sql2);
			$res = $ex->GetRows();

			$emailIds = array();
			$names = array();
			$email_status = array();
			foreach ($res as $k=>$v){
				$emailIds[$v['user_id']] = $v['email_id'];
				$email_status[$v['user_id']] = $v['email_status'];
				$names[$v['user_id']] = $v['fname'];
			}
			//var_dump($emailIds); die;
			foreach ($arr as $r => $c){
				$data = null;
				//	echo $email_status[$r];
				if($email_status[$r] == null){
					/*$picFlag = isAnyApprovedPic($r);
					$smarty->assign("picFlag", $picFlag);*/
					 
					$names[$r] = ucfirst($names[$r]);
					$smarty->assign("baseurl", $baseurl);
					$smarty->assign("data", $c);
					$flag = isAnyApprovedPic($r);
					echo "is approved:". $flag;
					$smarty->assign("isApproved",$flag);
					$smarty->assign('name', $names[$r]);
					$smarty->assign("count", count($c));
					
					/*$currentTime = time();
					$smarty->assign("analytics_login_link", $baseurl."/msg/messages.php".$campaign."&utm_content=view_messages&uid=$r");
					$smarty->assign("analytics_header_link", $baseurl."/index.php".$campaign."&utm_content=header&uid=$r");
					$smarty->assign("analytics_footer_link", $baseurl."/index.php".$campaign."&utm_content=footer&uid=$r");
					$smarty->assign("analytics_open_rate_link", "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$r&t=event&ec=email&ea=open&el=$r&cs=system_mailers&cm=email&cn=messages");
					$smarty->assign("analytics_open_rate_system_link", $baseurl."/trk.php" .$campaign."&utm_content=open_rate&uid=$r&step=openRate&t=$currentTime");
					echo $baseurl."/trk.php" .$campaign."&utm_content=open_rate&uid=$r&step=openRate&t=$currentTime";
					*/
					
					
					//$smarty->assign("analytics_match_link", $baseurl.$campaign."&utm_content=profile");
					
					// $smarty->display ("email.tpl" ) ;
					//echo $emailIds[$r];
					$data['email_id'] = $emailIds[$r];
					$data['subject'] = "messages";
					$data['user_id'] = $r;
					
					/*$toAddress = array($emailIds[$r]);
					if(isset($external_email)){
					$toAddress = array($external_email);
					$unsubscribe = new Unsubscription($external_email);
					}
					else{
						$unsubscribe = new Unsubscription($emailIds[$r]);
					}
					var_dump($c);
					
					$unsubLink = $unsubscribe->generateLink();
					echo $unsubLink;
					$smarty->assign("analytics_unsubscribe_link", $unsubLink."&".substr($campaign, 1,strlen($campaign))."&utm_content=unsubscribe&uid=$r");
					*/
					
				//	var_dump($toAddress);die;
									
					//$toAddress = array("him26.89@gmail.com", "chetan2182@gmail.com");
					
					//$toAddress = array("himanshu@trulymadly.com");
					
					$toAddress = $emailIds[$r];
					if(isset($external_email))
					$toAddress = $external_email;

					
					$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $r);
					$smarty->assign("analyticsLinks", $analyticsLinks);
					$mailObject = new MailFunctions();
				//	$smarty->display('../templates/utilities/emailMsgs.tpl');die;

					$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ). '/../templates/utilities/emailMsgs.tpl'),"","$names[$r], You have a new message", $r);
					
					$sysLog->logSystemMail($data);
				}
				//usleep($mailSleepTimeSES);
			}
			
			notifyTechTeam("messages");
		}
	}
	catch(Exception $e){
		trigger_error($e->getMessage());
		echo $e->getMessage();
	}
}

function isAnyApprovedPic($user_id){

	$myprofile = new User();
	$myData = $myprofile->getUserDetails($user_id);
	$approvedPics = $myprofile->getApprovedPics($user_id);
	$isAnyApproved = (count($approvedPics) > 0)?1:0;
	return $isAnyApproved;
}

function likesMailer($external_email = null, $external_date = null){
	global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$baseurl;
try{
			$sysLog = new SystemLogger();
	//$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'
	if(isset($external_date))
		$sql = "select ul.user1, ul.user2, ul.timestamp from user_likeMe ul	 where DATE(ul.timestamp) = '$external_date'  order by ul.user1, ul.timestamp desc";
	else
		$sql = "select ul.user1, ul.user2, ul.timestamp from user_likeMe ul	 where DATE(ul.timestamp) = date_sub(CURDATE(), interval 1 day)  order by ul.user1, ul.timestamp desc";
	echo $sql;
	$rs = $conn_slave->Execute($sql);
	$data = $rs->GetRows();
	
		
	//$UserActions=new UserActions();
	//$actionsDone = $UserActions->getUserActionsDone($user_id, $ids);
	
	$uu = new UserUtils();
	
		
	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=likes";
	$redirectionFile = "/likeme.php";
	$utm_content="view_likes";
	$campaignName = "likes";
	
	$arr = array();

	$i = 0;
	$prev_user = null;
	foreach ($data as $row => $col){

		if($prev_user == $col['user1'] || $prev_user == null) $i++;
		else {
			$i=0;
			//TODO: email for prev id
		}

		if($i>4) continue;

		$str = $col['user1'] .'|' . $col['user2'];
		$mid = Utils::calculateCheckSum($str);
		$details = $uu->getAttributes("matches", array($col['user2']), $col['user1'], null);

		//passNameFromRuleEngine($col['user1'], $col['user2'], $col['payment_mode'], $col['fname'], $col['lname']);

		$arr[$col['user1']][] = array(
						   		  "user_id" => $col['user2'],
								  "name" => $details[$col['user2']]['fname'],// . ' ' . $col['lname'],
								  "age" =>$details[$col['user2']]['age'],
								  "religion" => ucfirst($details[$col['user2']]['religion']),
								  "city" => $details[$col['user2']]['city'],
								  "height" => $details[$col['user2']]['height'],
								  "pic" => $details[$col['user2']]['image_url'],
								  "trust_score" => (isset($details[$col['user2']]['trust_score'])?$details[$col['user2']]['trust_score']:0),

		//"pic" => ($col['status'] == 'active')?($imageurl.$col['pic']):(($col['gender'] == 'F')?$dummy_female_image:$dummy_male_image),
		//"blur_pic" => ($col['status'] == 'active')?($imageurl.$col['blur_pic']):(($col['gender'] == 'F')?$dummy_female_image:$dummy_male_image),
								  "profile_link" => $baseurl."/profile.php".$campaign."&utm_content=profile&uid=".$col['user1']."&pid=$mid".'_'.$col['user2']
		);

		/*if(isset($details[$col['user1']]['blurSet']))
			$arr[$col['user1']]['blurSet'] = $details[$col['user1']]['blurSet'];
		*/	
		$prev_user = $col['user1'];


	}
	//var_dump(array_keys($arr)); die;
	$t = array_filter(array_keys($arr));// die;
	var_dump($t);
	if(count($t)>0){
		$sql2 = "SELECT fname, lname,email_id, user_id, email_status from user where user_id in (". implode(',', array_keys($arr)). ')';
		$ex = $conn_slave->Execute($sql2);
		$res = $ex->GetRows();

		$emailIds = array();
		$names = array();
		$email_status = array();
		foreach ($res as $k=>$v){
			$email_status[$v['user_id']] = $v['email_status'];
			$emailIds[$v['user_id']] = $v['email_id'];
			$names[$v['user_id']] = $v['fname'];
		}
		//var_dump($emailIds); die;
		foreach ($arr as $r => $c){
			$data = null;
			if($email_status[$r] == null){
					
				/*$picFlag = isAnyApprovedPic($r);
				echo $picFlag;
				$smarty->assign("picFlag", $picFlag);*/

			//	$smarty->assign("isBlurSet",$c['blurSet']);
				$flag = isAnyApprovedPic($r);
				$names[$r] = ucfirst($names[$r]);
				
				$smarty->assign("isApproved",$flag);
				$smarty->assign("data", $c);
				$smarty->assign('name', $names[$r]);
				//$smarty->assign("count", count($c));

				/*$currentTime = time();
				$smarty->assign("analytics_login_link", $baseurl."/likeme.php".$campaign."&utm_content=view_likes&uid=$r");
				$smarty->assign("analytics_header_link", $baseurl."/index.php".$campaign."&utm_content=header&uid=$r");
				$smarty->assign("analytics_footer_link", $baseurl."/index.php".$campaign."&utm_content=footer&uid=$r");
				$smarty->assign("analytics_open_rate_link", "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$r&t=event&ec=email&ea=open&el=$r&cs=system_mailers&cm=email&cn=likes");
				$smarty->assign("analytics_open_rate_system_link", $baseurl."/trk.php" .$campaign."&utm_content=open_rate&uid=$r&step=openRate&t=$currentTime");
				*/// $smarty->display ("email.tpl" ) ;
				//echo $emailIds[$r];
				$data['email_id'] = $emailIds[$r];
				$data['subject'] = "likes";
				$data['user_id'] = $r;

				$toAddress = $emailIds[$r];
					
				if(isset($external_email))
				$toAddress = $external_email;

				$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $r);
				$smarty->assign("analyticsLinks", $analyticsLinks);
					
				/*$toAddress = array($emailIds[$r]);
				//var_dump($c); die;
			if(isset($external_email)){
				$toAddress = array($external_email);
				$unsubscribe = new Unsubscription($external_email);
			}
			else{
				$unsubscribe = new Unsubscription($emailIds[$r]);
			}
					var_dump($c);
					
					$unsubLink = $unsubscribe->generateLink();
					echo $unsubLink;
					$smarty->assign("analytics_unsubscribe_link", $unsubLink."&".substr($campaign, 1,strlen($campaign))."&utm_content=unsubscribe&uid=$r");
			*/		
				//$toAddress = array("him26.89@gmail.com", "chetan2182@gmail.com");
				$mailObject = new MailFunctions();
				//$smarty->display('../templates/utilities/emailLikes.tpl');//die;

				 $mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/emailLikes.tpl'),"","$names[$r], Someone has expressed interest in you!", $r);
				//usleep($mailSleepTimeSES);
				$sysLog->logSystemMail($data);
			}
		}
		notifyTechTeam("likes");
	}
}
	catch(Exception $e){
		trigger_error($e->getMessage(), E_USER_WARNING);
	}
	
}

function sendMatchesMail($user_id, $user_array, $external_email = null){
/*var_dump($user_array);
echo $user_id; die;*/
	global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$mailSleepTimeSES, $baseurl;
	try{
		$sysLog = new SystemLogger();
	$conn_slave->SetFetchMode ( ADODB_FETCH_ASSOC );

	$UserActions=new UserActions();
	$actionsDone = $UserActions->getUserActionsDone($user_id, $ids);
	
	$uu = new UserUtils();
	$details = $uu->getAttributes("matches", $user_array, $user_id, $actionsDone);
	
	$user = new User();
	$myDetails = $user->getUserDetails($user_id);
	/*var_dump($details);
	echo PHP_EOL;
	var_dump($myDetails); die;*/
	/*$sql = " select u.fname, u.lname, u.gender, u.email_id, up.thumbnail as pic, FLOOR(DATEDIFF(now(),ud.DateOfBirth)/365.25) as age, up.blur_thumbnail as blur_pic, up.status, u.user_id ,u.email_status, u.plan from  user u
		join user_photo up on u.user_id= up.user_id
		join user_demography ud on ud.user_id = u.user_id
		 where  up.is_profile = 'yes' and u.user_id in ($user_id, ". implode(',', $user_array). ")";

	echo $sql;
	$ex = $conn_slave->Execute($sql);
	$res = $ex->GetRows();*/
	//var_dump($res);
	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=matches";
	$redirectionFile = "/matches.php";
	$utm_content="view_matches";
	$campaignName = "matches";
		//	$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'
	
	$arr = array();
	$emailId = null;
	$myName = ucfirst($myDetails['fname']);
	$payment_mode = null;
	$myEmailStatus = 'yes';
	$count =0;
	foreach ($details as $k=>$v){
		$count++;
		if($count>2) break;
		//var_dump($v); die;
	/*	if($user_id == $v['user_id']){
			$emailId = $v['email_id'];
			$myName = $v['fname'];
			$myEmailStatus = $v['email_status'];
			$payment_mode = $v['plan'];
			continue;
		}*/
			
		//passNameFromRuleEngine($user_id, $v['user_id'], $payment_mode, $v['fname'], $v['lname']);
		$str = $user_id .'|' . $v['id'];
		$mid = Utils::calculateCheckSum($str);
		$arr[$v['id']][] = array(
								  "name" => $v['fname'] ,//. ' ' . $v['lname'],
								  "age" => $v['age'],
								  //"pic" => ($v['status'] == 'active')?($imageurl.$v['pic']):(($v['gender'] == 'F')?$dummy_female_image:$dummy_male_image),
								  "pic" => $v['image_url'],
								  "religion" => $v['religion'],
								  "city" => $v['city'],
								  "height" => $v['height'],
								"trust_score" => (isset($v['trust_score'])?$v['trust_score']:0),
										  "profile_link" => $baseurl."/profile.php".$campaign."&utm_content=profile&uid=".$user_id."&pid=$mid".'_'.$v['id']
		);

	}
	/*	echo $emailId;
	 echo $myName;
	 var_dump(array_values($arr));
	 */
	//var_dump(array_keys($arr));
	/*echo count(array_keys($arr));
	var_dump(array_values($arr));die;*/
	
	$myEmailStatus = $myDetails['email_status'];
	if($myEmailStatus == null){
	/*	$picFlag = isAnyApprovedPic($user_id);
		$smarty->assign("picFlag", $picFlag);
	*/	$smarty->assign("data", array_values($arr));
		$smarty->assign("count", count(array_keys($arr)));
		$smarty->assign('name', ucfirst($myDetails['fname']));
		$smarty->assign("status", $myDetails['status']);
		//$smarty->assign("isBlurSet",$details[$user_id]['blurSet']);
		
		$flag = isAnyApprovedPic($user_id);
		$smarty->assign("isApproved",$flag);
		//$smarty->assign("analytics_login_link", $baseurl."/matches.php".$campaign."&utm_content=login_through_content&uid=$user_id");
		/*$smarty->assign("analytics_header_link", $baseurl."/index.php".$campaign."&utm_content=header&uid=$user_id");
		$smarty->assign("analytics_footer_link", $baseurl."/index.php".$campaign."&utm_content=footer&uid=$user_id");		
		//$smarty->assign("analytics_open_rate_link", $baseurl."/email/images/spacer.gif".$campaign."&utm_content=open_mail");		
		$currentTime = time();
		$smarty->assign("analytics_open_rate_link", "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$user_id&t=event&ec=email&ea=open&el=$user_id&cs=system_mailers&cm=email&cn=matches");
		$smarty->assign("analytics_open_rate_system_link", $baseurl."/trk.php" .$campaign."&utm_content=open_rate&uid=$user_id&step=openRate&t=$currentTime");
		*///http://www.google-analytics.com/collect?v=1&tid=UA-45604694-4&t=event&ec=email&ea=open&cs=system_mailers&cm=email&cn=matches//
		//http://www.google-analytics.com/collect?v=1&tid=UA-45604694-4&cid=CLIENT_ID_NUMBER&t=event&ec=email&ea=open&el=recipient_id&cs=system_mailers&cm=email&cn=Campaign_Name
		// $smarty->display ("email.tpl" ) ;
		//echo $emailIds[$r];
		$toAddress = $myDetails['email_id'];
		//$toAddress = array($emailId);
//echo $toAddress;
	/*if(isset($external_email)){
				$toAddress = array($external_email);
				$unsubscribe = new Unsubscription($external_email);
			}
			else{
				$unsubscribe = new Unsubscription($myDetails['email_id']);
			}
					
					$unsubLink = $unsubscribe->generateLink();
					echo $unsubLink;
					$smarty->assign("analytics_unsubscribe_link", $unsubLink."&".substr($campaign, 1,strlen($campaign))."&utm_content=unsubscribe&uid=$user_id");
			*/		//echo $external_email;
			//echo PHP_EOL; 
			//var_dump($toAddress);die;
			//	$toAddress = array("him26.89@gmail.com", "chetan2182@gmail.com");

					
				if(isset($external_email))
				$toAddress = $external_email;

				$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $user_id);
				$smarty->assign("analyticsLinks", $analyticsLinks);
				
			$data['email_id'] = $myDetails['email_id'];
			$data['subject'] = "matches";
			$data['user_id'] = $user_id;
		//	var_dump($arr); die;
		//	var_dump($c);
		//$toAddress = array("himanshu@trulymadly.com");
		$mailObject = new MailFunctions();
		//$smarty->display('../templates/utilities/emailMatches.tpl');die;
		$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/emailMatches.tpl'),"","$myName, meet your matches!", $user_id);
		$sysLog->logSystemMail($data);
	}
//	die;
	//usleep($mailSleepTimeSES);
	}
	catch(Exception $e){
		trigger_error($e->getMessage(), E_USER_WARNING);
	}

}

function copyfileFromS3(){
	global $config;
	$today = date("ymd");

	$table = Utils::getSlaveRecoTableName();
	Utils::getFromS3("recommendation/sql_$table.txt", "/tmp/reco_$today.txt");

}

function matchesMailer($external_email = null, $external_date =null){


	global $config;

	copyfileFromS3();
	if(isset($external_date)){
		$today =  date("ymd", strtotime($external_date));
	}
	else{
	$today = date("ymd");
	}
	$table = Utils::getSlaveRecoTableName();
	//$filename = "/tmp/sql_$table.txt";
	/*var_dump($config);
	echo $config['amazon_s3']['cloudfront_url']; die;*/
	$filename = "/tmp/reco_$today.txt";
	$handle = fopen($filename, "r");
	echo $filename;
	//$handle= fopen("/var/www/html/trulymadly/scripts/slave/sql_user_recommendation_slave_140203.txt", "r");
	$prev_user1 = null;
	$user_array = array();

	$j=0;
	if ($handle) {
		while (($line = fgets($handle)) !== false) {
			// process the line read.
			$arr = explode(',', $line);

			if($arr[0]!=$prev_user1){
				$j=0;
				if($prev_user1!=null){
					// emailer
					sendMatchesMail($prev_user1, $user_array, $external_email);
				}
				$user_array = null;
			}
			$user_array[$j++]=(int)$arr[1];
			$prev_user1 = $arr[0];

		}
		//emailer
		sendMatchesMail($prev_user1, $user_array, $external_email);
		notifyTechTeam("matches");
		

	} else {
		// error opening the file.
		$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
		$subject = "unable to open recommendation file on SES";
		$from = "admin@trulymadly.com";
		Utils::sendEmail($to, $from, $subject, null);
		trigger_error ( "unable to open recommendation file on S3", E_USER_WARNING );
	}

}

function incompleteRegisterationMailer($external_email = null, $external_date = null){
	global $config, $conn_slave, $smarty, $imageurl, $dummy_female_image, $dummy_male_image,$baseurl;
	$sysLog = new SystemLogger();
	if(isset($external_date))
		$sql = "SELECT fname,email_id, email_status, user_id from user where status = 'incomplete' and date(registered_at) = '$external_date' AND steps_completed regexp 'demographic,trait'  AND email_status is null";
	else
		$sql = "SELECT fname,email_id, email_status, user_id from user where status = 'incomplete' and date(registered_at) = date_sub(CURDATE(), interval 1 day) AND steps_completed regexp 'demographic,trait'  AND email_status is null";
	
		echo $sql;
	$res = $conn_slave->Execute($sql);
	$rows = $res->GetRows();


	//$tid = $config['google_analytics']['google_analytics_code'];//UA-45604694-3'
	
	$campaign = "?utm_source=system_emailer&utm_medium=email&utm_campaign=incomplete_profile";
	$redirectionFile = "/register.php";
	$utm_content="view_incomplete_profile";
	$campaignName = "incomplete_profile";
	
	foreach ($rows as $key=>$val){

		$data = null;
		$data['email_id'] = $val['email_id'];
			$data['subject'] = "incomplete_profile";
			$data['user_id'] = $val['user_id'];
			
			$smarty->assign("name", ucfirst($val['fname']));
			$smarty->assign("rnum", rand(100, 200));
/*			$smarty->assign("analytics_login_link", $baseurl."/register.php".$campaign."&utm_content=view_incomplete_profile&uid=".$val['user_id']);
			$smarty->assign("analytics_header_link", $baseurl."/index.php".$campaign."&utm_content=header&uid=".$val['user_id']);
			$smarty->assign("analytics_footer_link", $baseurl."/index.php".$campaign."&utm_content=footer&uid=".$val['user_id']);
		$smarty->assign("analytics_open_rate_link", "http://www.google-analytics.com/collect?v=1&tid=$tid&cid=$user_id&t=event&ec=email&ea=open&el=$user_id&cs=system_mailers&cm=email&cn=incomplete_profile");

		$currentTime = time();
		$smarty->assign("analytics_open_rate_system_link", $baseurl."/trk.php" .$campaign."&utm_content=open_rate&uid=$user_id&step=openRate&t=$currentTime");
*/		
			$toAddress = $val['email_id'];
/*			if(isset($external_email)){
				$toAddress = array($external_email);
			$unsubscribe = new Unsubscription($external_email);
			}
			else{
				$unsubscribe = new Unsubscription($myDetails['email_id']);
			}
			
			$unsubLink = $unsubscribe->generateLink();
					echo $unsubLink;
					$smarty->assign("analytics_unsubscribe_link", $unsubLink."&".substr($campaign, 1,strlen($campaign))."&utm_content=unsubscribe&uid=$user_id");
*/					
			//$toAddress = array("him26.89@gmail.com", "chetan2182@gmail.com");
			//var_dump($toAddress);
			/*$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Complete your profile to find that special someone!";
	$message = $smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/emailCompleteProfile.tpl');
	$from = "admin@trulymadly.com";
	Utils::sendEmail($to, $from, $subject, $message );*/
	
			if(isset($external_email))
				$toAddress = $external_email;

				$analyticsLinks = Utils::generateSystemMailerLinks($campaignName, $utm_content, $redirectionFile, $toAddress, $val['user_id']);
				$smarty->assign("analyticsLinks", $analyticsLinks);
				
			$mailObject = new MailFunctions();
			$mailObject->sendMail(array($toAddress),$smarty->fetch(dirname ( __FILE__ ).'/../templates/utilities/emailCompleteProfile.tpl'),"","Complete your profile to find that special someone!", $val['user_id']);
			$sysLog->logSystemMail($data);
	}
}

/**
 * - Number of Emails sent
- Number of Emails opened 
- Number of Members who came to the site from Email
 */
function mailStats($external_date = null){
	
	global $conn_slave,$smarty;

	// Number of Emails opened 
	$campaigns = array("incomplete_profile" ,"likes",  "matches","messages",  "Logged_NotReceived_Matches", "notLogged_Received_Matches");
	$smarty->assign("campaigns", $campaigns);
	/*if(isset($external_date))
		$sql = " select count(distinct(user_id)) as count , campaign from action_log where content_part_clicked ='open_rate' and  date(tstamp) = '$external_date'  group by campaign order by campaign";
	else
	*/	$sql = " select count(distinct(user_id)) as count , campaign from action_log where content_part_clicked ='open_rate' and  date(tstamp) = date_sub(CURDATE(), interval 1 day)  group by campaign order by campaign";
	
	$res = $conn_slave->Execute($sql);
	$result = $res->GetRows();
	$openArr = array();
	
	foreach ($result as $val){
		$openArr[$val['campaign']] = $val['count'];
	}
	$smarty->assign("openRate", $openArr);
	
	$sentMails = array();
	//Number of Emails sent yesterday
	/*if(isset($external_date))
		$sql2 = "select count(subject) as count, subject from system_mailer_log where date(tstamp) = '$external_date' group by subject order by subject";
	else*/
		$sql2 = "select count(subject) as count, subject from system_mailer_log where date(tstamp) = date_sub(CURDATE(), interval 1 day) group by subject order by subject";
	
	$res2 = $conn_slave->Execute($sql2);
	$result2 = $res2->GetRows();
	
	foreach ($result2 as $r => $c){
		$sentMails[$c['subject']] = $c['count'];
	}
	$smarty->assign("sentMails", $sentMails);
	
	// Number of Members who came to the site from Email

	$siteTraction = array();
	/*if(isset($external_date))
		$sql3="select count(distinct(user_id)) as count , campaign from action_log where source = 'system_emailer' and content_part_clicked !='open_rate' and date(tstamp) = '$external_date' group by campaign order by campaign";
	else*/
		$sql3="select count(distinct(user_id)) as count , campaign from action_log where source = 'system_emailer' and content_part_clicked !='open_rate' and date(tstamp) = date_sub(CURDATE(), interval 1 day)  group by campaign order by campaign";
	$res3 = $conn_slave->Execute($sql3);
	$result3 = $res3->GetRows();
	
	foreach ($result3 as $e => $v){
		$siteTraction[$v['campaign']] = $v['count'];
	}
	$smarty->assign("siteVisits", $siteTraction);
	
//Number of Emails sent today

	$todayMails = array();
	
	$sql4 = "select count(subject) as count, subject from system_mailer_log where date(tstamp) = curdate() group by subject order by subject";
	$res4 = $conn_slave->Execute($sql4);
	$result4 = $res4->GetRows();
	foreach ($result4 as $r => $c){
		$todayMails[$c['subject']] = $c['count'];
	}
	$smarty->assign("todayMails", $todayMails);
	
	
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com, rahul@trulymadly.com";
	$subject = "Email Statistics: ". date("d-m-y");
	$from = "himanshu@trulymadly.com";
	Utils::sendEmail($to, $from, $subject, $smarty->Fetch(dirname ( __FILE__ ).'/../templates/utilities/emailStats.tpl'), TRUE);
	
}

function openRateCountMail(){
	global $conn_slave;

	// Number of Emails opened 
	$sql = " select count(distinct(user_id)) as count , campaign from action_log where content_part_clicked ='open_rate' and  date(tstamp) = date_sub(CURDATE(), interval 1 day)  group by campaign";
	$res = $conn_slave->Execute($sql);
	$result = $res->GetRows();
	$message = "Number of users who opened emailes yesterday:" . PHP_EOL;
	
	foreach ($result as $k=>$v){
		$message .= "	".$v['campaign'] .":". $v['count'] ;
	}
	
	$message .= PHP_EOL . "Number of Emails sent yesterday" .PHP_EOL;
		
	$detailedMessage .= PHP_EOL . PHP_EOL . PHP_EOL . "Details of Emails sent yesterday to users:" .PHP_EOL;
	
	
	//Number of Emails sent yesterday
	$sql2 = "select count(subject) as count, subject, group_concat(user_id) as ids from system_mailer_log where date(tstamp) = date_sub(CURDATE(), interval 1 day) group by subject";
	$res2 = $conn_slave->Execute($sql2);
	$result2 = $res2->GetRows();
	foreach ($result2 as $r => $c){
		$message .= "	".$c['subject'] .":". $c['count'] ;
		$detailedMessage .= "	".$c['subject'] .":" ."(". $c['ids'] .")" .PHP_EOL;
	}
	
	
	// Number of Members who came to the site from Email
	$message .= PHP_EOL . "Number of Members who came to the site from Email:". PHP_EOL;
	
	$sql3="select count(distinct(user_id)) as count , campaign from action_log where source = 'system_emailer' and content_part_clicked !='open_rate' and date(tstamp) = date_sub(CURDATE(), interval 1 day)  group by campaign";
	//$sql3="select count(distinct(user_id)) as count , campaign from action_log where source = 'system_emailer' and  date(tstamp) = curdate() -1  group by campaign";
	//$sql3 = "select count(campaign) as count , campaign from action_log where source = 'system_emailer' and  date(tstamp) = curdate() -1  group by campaign";
	$res3 = $conn_slave->Execute($sql3);
	$result3 = $res3->GetRows();
	
	foreach ($result3 as $e => $v){
		$message .= "	".$v['campaign']. ":" . $v['count']   .PHP_EOL;
	}
	
	
//Number of Emails sent today
	$message .= PHP_EOL . "Number of Emails sent today" .PHP_EOL;
	$detailedMessage .= PHP_EOL . "Details of Emails sent today to users:" .PHP_EOL;
	
	$sql4 = "select count(subject) as count, subject, group_concat(user_id) as ids from system_mailer_log where date(tstamp) = curdate() group by subject";
	$res4 = $conn_slave->Execute($sql4);
	$result4 = $res4->GetRows();
	foreach ($result4 as $r => $c){ 
		$message .= "	".$c['subject'] .":". $c['count'];
		$detailedMessage .= "	".$c['subject'] .":"."(" . $c['ids'] .")" .PHP_EOL;// ':'. $c['ids'];
	}
	
	
	
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com, rahul@trulymadly.com";
	$subject = "Email Statistics: ". date("d-m-y");
	$from = gethostname();//"himanshu@trulymadly.com";
	Utils::sendEmail($to, $from, $subject, $message .$detailedMessage);
}


//var_dump($argv[1]);die;  
try{
			
	$options=getopt ( "d:e:" );
	//var_dump($options); die;
	msgsMailer($options[e],$options[d]); 
	likesMailer($options[e], $options[d]) ;
	matchesMailer($options[e], $options[d]);
	incompleteRegisterationMailer($options[e], $options[d]);
	//if(!isset($options[d]))
	mailStats(); 
	//openRateCountMail();
}
catch (Exception $e){
	$to =  "himanshu@trulymadly.com, shashwat@trulymadly.com";
	$subject = "Daily Mailer failed";
	$from = gethostname();
	Utils::sendEmail($to, $from, $subject, $e->getTraceAsString());
	trigger_error ( "Mailer failed", E_USER_WARNING );
}

?>
