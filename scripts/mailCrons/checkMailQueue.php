  <?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../DBO/mailerDBO.php";
require_once dirname ( __FILE__ ) . "/../../mailers/queueMailSender.php";

/*
 * To obtain exclusive lock on a file to control the no. of instance running
 * Author: Sumit
 *   🖖 Live Long and Prosper
 */

class MailQueue
{
    private $conn_reporting;
    private $conn_master ;
    private $redis;
    private $no_of_scripts = 10 ;
    private $tempFileName = "/tmp/mail_queue";
    private $lockStatus = false ;
    private $fptr ;
    private $waitForBlockingReadInRedis =0;
    private $mailSender ;

    function __construct()
    {
        global $conn_reporting,$conn_master,$redis ;
        $this->conn_reporting = $conn_reporting ;
        $this->conn_master = $conn_master ;
        $this->redis = $redis ;
        $this->redis->setOption(Redis::OPT_READ_TIMEOUT, -1);
        $this->mailSender = new MailSender();
    }


    public function checkLockOnFiles()
    {
        for ($i = 1 ; $i <= $this->no_of_scripts ; $i++)
        {
            $file_name = $this->tempFileName . $i . ".txt" ;
            $lock_status = $this->setLockOnFile($file_name );
//            echo "Lock Status " . $lock_status ;
//            echo PHP_EOL ;
            if ($lock_status == 0 )
            {
                $gmtTime = $this->getTime();
                $subject = "Mailer queue No previous lock found on file "  .$this->baseurl ;
//				echo $subject ;
//				echo PHP_EOL . $file_name ;
                Utils::sendEmail("sumit@trulymadly.com", "sumit@trulymadly.com", $subject , " Lock acquired on file ". $i . " " . $gmtTime  );

                $this->startMailers();
            }

        }

        if($i >= $this->no_of_scripts )
        {
           /* $count = $this->checkNotificationCount() ;
            if ($count >= $this->count_allowed_pending_msg )
            {
                $subject = "Warning :: no. of  pending messages in the queue " . $count  . " on  " . $this->baseurl;
                $body = "All " . $this->no_of_scripts . " scripts already running " . $this->getTime() ;
                Utils::sendEmail("sumit@trulymadly.com", "sumit@trulymadly.com", $subject , $body  );
//				echo $subject .PHP_EOL ;
//				echo $body ;
            }*/
            echo "All Scripts already running" . PHP_EOL;
        }
    }

    private function setLockOnFile($file_name)
    {
        $this->fptr = fopen($file_name, "c");

        if (flock($this->fptr, LOCK_EX|LOCK_NB, $wouldblock))
        {
            $isLocked = 0 ;
            // lock obtained
            if(flock($this->fptr,LOCK_EX) == false){
                throw new Exception("unable to acquire lock");
            }else{
                echo "lock acquired";
                $this->lockStatus = true ;
//				$gmtTime = $this->getTime();
//				Utils::sendEmail($techTeamIds .",sumit@trulymadly.com", "sumit@trulymadly.com", "No previous lock found ".$this->baseurl , " Lock acquired ". $gmtTime  );
            }
        }
        else
        {
            if ($wouldblock) {
                echo "already locked";
                $isLocked = 1 ;
                $this->lockStatus = false ;
                // something already has a lock
            }
            else
            {
                echo "could not lock";
                // couldn't lock for some other reason
            }

        }

        return $isLocked;
    }

    private function getTime ()
    {
        $gmtTime = date ( 'Y-m-d H:i:s') ;
        return $gmtTime;
    }

    private function startMailers()
    {
        while(true)
        {
            $data = $this->redis->BRPOP(Utils::$redis_keys['mailing_queue'], $this->waitForBlockingReadInRedis);
            if($data[0] == Utils::$redis_keys['mailing_queue'])
            {
                $mail_data = json_decode($data[1],true);
                $this->mailSender->sendMailFromQueue($mail_data);
               // die ;

            }
        }
    }

    public function releaseLock(){
        if(flock($this->fptr,LOCK_UN) == false){
            throw new Exception("unable to release lock");
        }
    }
}


try
{
    if(php_sapi_name() == 'cli')
    {
        $mailQueue = new MailQueue();
        $mailQueue->checkLockOnFiles();
    }
}

catch (Exception $e)
{
    $mailQueue->releaseLock();
    trigger_error($e->getMessage(), E_USER_WARNING);
//    Utils::sendEmail($techTeamIds .",sumit.kumar@trulymadly.com", "sumit.kumar@trulymadly.com", "Mail queue script failed 	".$baseurl, $e->getMessage() );
}