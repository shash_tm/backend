<?php
require_once dirname ( __FILE__ ) . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ ) . "/../../DBO/mailerDBO.php";

/*
 * Check for any queued campaigns and add them to the mailer queue
 * Author : Sumit
 *🖖 Live Long and Prosper
 */


class MailCampaignsScheduler
{
    private $conn_reporting;
    private $conn_master ;
    private $campaign_data = array();
    private $redis ;

    function __construct()
    {
        global $conn_reporting,$conn_master,$redis ;
        $this->conn_reporting = $conn_reporting ;
        $this->conn_master = $conn_master ;
        $this->redis = $redis ;
    }

    public function checkQueuedCampaigns()
    {
        $sql = "SELECT campaign_id FROM mailer_campaigns WHERE status = 'queued'" ;
        $res = $this->conn_reporting->Execute($sql) ;

        while($row = $res->FetchRow())
        {
            $campaign_id = $row['campaign_id'];
            $campaign_sql = "SELECT * FROM mailer_campaigns WHERE campaign_id = ?" ;
            $campaign_prep = $this->conn_reporting->Prepare($campaign_sql) ;
            $campagn_exec = $this->conn_reporting->Execute($campaign_prep, array($campaign_id)) ;
            $this->campaign_data = $campagn_exec->FetchRow();
            if ($this->campaign_data['status'] == 'queued')
            {
                $this->startTheCampaign();
            }

        }
    }


    private function startTheCampaign()
    {
        // first set the status as sending
        MailerDBO::updateCampaignStatus($this->campaign_data['campaign_id'], 'sending');

        // Now add the mails to mail queue
        $query = "SELECT u.user_id, u.fname, u.email_id ".$this->campaign_data['query'] ;
        $query_exec = $this->conn_reporting->Execute($query) ;

        while($row = $query_exec->FetchRow())
        {
            $email_arr = array( "user_id" =>  $row['user_id'],
                                "name" => $row['fname'],
                                "email_id" => $row['email_id'],
                                "campaign_id" => $this->campaign_data['campaign_id']);
            $data = json_encode($email_arr);
            // Use rpop at the receiving end since we are using lpush here
            $this->redis->lpush(Utils::$redis_keys['mailing_queue'], $data);
        }
        MailerDBO::updateCampaignStatus($this->campaign_data['campaign_id'], 'sent');
    }
}


try
{
    if(php_sapi_name() == 'cli')
    {
       // TODO:: 1. Check if a mailer is scheduled for now , send it
        //       2. Check if some mailer has just been pushed into the queue
        $CampaignScheduler = new MailCampaignsScheduler();
        $CampaignScheduler->checkQueuedCampaigns();
    }
}

catch (Exception $e)
{
    trigger_error($e->getMessage(), E_USER_WARNING);
//    Utils::sendEmail($techTeamIds .",sumit.kumar@trulymadly.com", "sumit.kumar@trulymadly.com", "Mail chimp update script failed 	".$baseurl, $e->getMessage() );
}