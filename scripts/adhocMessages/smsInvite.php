<?php
require_once dirname ( __FILE__ ) . "/../../include/Utils.php";
require_once dirname ( __FILE__ )  . "/../../include/config_admin.php";
require_once dirname ( __FILE__ ) . "/../../utilities/send_sms.php";


class NewTextMessage
{
    private $conn_reporting;
    private $user_id ;
    private $message;
    private $sendSmsObj ;

    function __construct()
    {
        global $conn_reporting ;
        $this->conn_reporting = $conn_reporting ;
        $this->user_id ;
        $this->getMessage();
    }

    private function getMessage()
    {
        $this->message = "Hello Ladies! Join us for an epic Gossip With The Girls afternoon at L'amandier, RA Puram and take home shopping vouchers worth Rs 1000! ".
                         "Sign up: http://bit.ly/28UU2vR";
    }

    public function getUsersAndSend($target, $after_given)
    {
        if($target == 'all')
        {
            if(isset($after_given) && $after_given != null)
                $after = $after_given ;
            else
                $after = 0;
            $sql="select u.user_id, upn.user_number from user u ".
                "join user_data ud on u.user_id=ud.user_id join user_phone_number upn on upn.user_id = u.user_id ".
                "join geo_city gc on ud.stay_city=gc.city_id and ud.stay_state= gc.state_id where  u.gender='f' and gc.city_id in (13147) and u.status= 'authentic' " .
                "and u.user_id >= $after";
        } else
        {
            $sql="select u.user_id,upn.user_number from user_data ud join user u  on u.user_id = ud.user_id  join user_phone_number upn on upn.user_id = u.user_id where u.user_id = $target ";
        }
        echo $sql ;

        $stmt=$this->conn_reporting->Prepare($sql);
        $exe=$this->conn_reporting->Execute($stmt,array());
        $i=0;
        $success = 0 ;
        $failure = 0 ;
        while($row=$exe->fetchRow())
        {
            $this->gender = $row['gender'] ;
            $this->user_id = $row['user_id'] ;

            $this->sendSmsObj = new SendMessage() ;
            $response = $this->sendSmsObj->sendSMS($row['user_number'], $this->message) ;
            echo PHP_EOL. $this->user_id ." ".$response['status'];
            if($response['status'] == 'success')
                $success++ ;
            else
                $failure++ ;
           // print_r(json_encode($response));
            sleep(5);
            $i++;
        }
        Utils::sendEmail('sumit@trulymadly.com','admintm@trulymadly.com',"SMS sent to Delhi users",'SMS sent to '.$success.' and failed '.$failure);


    }

}


try
{
    if (php_sapi_name() == 'cli')
    {
        $target = $argv[1];
        $after_given = $argv[2];
        $message_obj = New NewTextMessage() ;
        if(isset($target) && $target != null)
            $message_obj->getUsersAndSend($target, $after_given);
        else
            echo " NO target";
    }

}
catch (Exception $e)
{
    echo $e->getMessage();
}

?>
