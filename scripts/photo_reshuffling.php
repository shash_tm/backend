<?php 
try {
	// Weekly Report for Photo Reshuffling,author Raghav 
	if(php_sapi_name() !== 'cli')
	{
		echo 'Not running from CLI';
		die;
	}
	
	if(!isset($argv[1]))
	{
		echo 'Please pass number of days as an option to the script';
		die;
	}
	
	require_once  dirname ( __FILE__ ) . "/../include/config_admin.php";


require_once dirname ( __FILE__ ) . "/../include/Utils.php";
$sql_query="select user_id,group_concat(concat(photo_id,':',likes,':',hides)) as data from trulymadly_prod.photo_views_male where user_id in (select user_id from photo_reshuffling_user_checkpoint where updated>=DATE_SUB(now(),INTERVAL $argv[1] DAY)) group by user_id";
echo $sql_query;

$conn_reporting->SetFetchMode (ADODB_FETCH_ASSOC);

//$my_result=$conn_reporting->query($sql_query);
$fetch=$conn_reporting->Execute($sql_query);

$i=0;
//var_dump($mystatus);
$total_count=0;
$threshold_count=20000;
$total_count=$fetch->rowCount();
$arr_result=array();
$limit_number=false;
if($total_count>$threshold_count)
{
	$limit_number=true;
}
while($item=$fetch->fetchRow())
{
    echo $i.PHP_EOL;
	$user_id=$item['user_id'];
	$concat_string=$item['data'];
	$picmeta=explode(',',$concat_string);
	// var_dump($picmeta);


	$profile_pic="select photo_id from trulymadly_prod.user_photo where user_id=? and is_profile='yes' and status='active'";
	$prepared=$conn_reporting->Prepare($profile_pic);
	$result1=$conn_reporting->execute($prepared,array($user_id));
	$result=$result1->fetchRow();
	$uphoto_id=$result['photo_id'];
    
	$myarray=array();
	foreach($picmeta as $pics)
	{

		$pic=explode(':',$pics);
		$photo_id=$pic[0];
		$likes=intval($pic[1]);
		$hides=intval($pic[2]);
		
		$sum=$likes+$hides;
		$perc=0;
		if($sum>0)
		{
			$perc=round($likes/$sum,10);
			$perc=round($perc*100,3);
				
		}
		
		if($photo_id==$uphoto_id)
		{
			$is_user_profile=true;
			array_unshift($myarray,array("photo"=>$photo_id,"likes"=>$likes,"hides"=>$hides,"perc"=>$perc));
		}else
		{

			array_push($myarray,array("photo"=>$photo_id,"likes"=>$likes,"hides"=>$hides,"perc"=>$perc));
		}


	}
	
    if($is_user_profile)
    {
		$arr_result[]=array("user_id"=>$user_id,"pics"=>$myarray);
    }
	$i++;
	
	if($limit_number&&$i>$threshold_count)
	{
		break;
	}
}


if($arr_result!=null)
{
	
	$smarty->assign('myreport',$arr_result);
	$message=$smarty->Fetch(dirname ( __FILE__ ).'/../templates/reporting/photo_reshuffling_report.tpl');
	//Utils::sendEmail( 'raghav@trulymadly.com,rahul@trulymadly.com,shashwat@trulymadly.com', "admintm@trulymadly.com", 'Profile Pic Shuffling Weekly Report', $message , TRUE);
	Utils::sendEmail( 'raghav@trulymadly.com', "admintm@trulymadly.com", 'Profile Pic Shuffling Weekly Report', $message , TRUE);
	

}



}catch (Exception $ex)
{
	echo $ex->getMessage();
	Utils::sendEmail( 'raghav@trulymadly.com', "admintm@trulymadly.com", 'Profile Pic Shuffling Report FAIL', $ex->getMessage() , TRUE);
	
	
}







?>