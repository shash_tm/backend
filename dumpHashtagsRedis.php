<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";

try {
	if(PHP_SAPI !== 'cli') {
		die();
	}
// 	$conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	
// 	$sql = $conn->Execute("select name from user_hashtags");
// 	$data = $sql->GetRows ();
	
	global $redis;
	
	$items = $redis->zRange("hashtags", 0, -1);
	
	$hashArr = array();
	foreach ($items as $hashtag) {
		$tmp = explode(':',$hashtag);
		$hashTag = ucfirst(strtolower(ltrim($tmp[1],"#")));
		$hashArr[] = $hashTag;
	} 
	//var_dump($hashArr);exit;
	
	foreach($hashArr as $value){
		$redis->zAdd("user_hashtags", 1, $value);
	}
	
} catch (Exception $e) {
	echo $e-getMessage();
}

?>