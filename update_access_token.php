<?php

require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/DBO/userFacebookDBO.php";
require_once dirname ( __FILE__ ) . "/fb.php";
require_once dirname ( __FILE__ ) . "/include/Utils.php";

try {
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$token = (isset($_REQUEST ['token'])) ? $_REQUEST ['token'] : null;
	$response = array();
	$user_fb = new UserFacebookDBO();
	$response["responseCode"] = 200;
	// update the token sent by client
	if (isset($user_id) && $token != null && strlen($token) > 0) {
		$user_fb->update_facebook_token($user_id, $token);
	}
	// check if token is valid and send 401
	if (isset($user_id)) {
		$token = $user_fb->get_facebook_token($user_id);
		if ($token != null) {
			$last_logout_time = $user_fb->get_last_logout_time($user_id);
			$cur_time = time();
			if ($last_logout_time != null && (($cur_time - $last_logout_time) > Utils::$token_expired_logout_window)) {
				$fb = new fb($token);
				if (!$fb->is_token_valid($user_id)) {
					$user_fb->update_last_logout_time($user_id, $cur_time);
					$response["responseCode"] = 401;
				}
			}
		}
	}
	
	echo json_encode($response);
}
catch(Exception $e){
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
}
?>
