<?php 
require_once dirname ( __FILE__ ) . "/include/config.php";
require_once dirname ( __FILE__ ) . "/fb.php";
require_once dirname ( __FILE__ ) . "/include/function.php";
require_once dirname ( __FILE__ ) . "/UserUtils.php";
require_once dirname ( __FILE__ ) . "/TrustBuilder.class.php";
require_once dirname ( __FILE__ ) . "/include/tbconfig.php";
require_once dirname ( __FILE__ ) . "/include/tbmessages.php";
require_once dirname ( __FILE__ ) . "/include/header.php";
require_once dirname ( __FILE__ ) . "/logging/EventTrackingClass.php";
require_once dirname ( __FILE__ ) . "/UserData.php";
require_once dirname ( __FILE__ ) . "/abstraction/query_wrapper.php";


if(isset($_REQUEST['utm_campaign'])){
		//if($_REQUEST['utm_content'] == "view_matches")
		$event_type = $_REQUEST['utm_campaign'] ;
		$data[] = array("user_id" => $_REQUEST['uid'], "activity" => "emailers_click" , "event_type" => $event_type);
		$eventTrk = new EventTrackingClass();
		$eventTrk->logActions($data);	
	}
	
$data = $_POST;
$func=new functionClass();
$login_mobile = $func->isMobileLogin();
$user = functionClass::getUserDetailsFromSession ();
if(isset($user['user_id'])){
	$status= functionClass::getUserStatus($user['user_id']);
}

if(isset($_REQUEST['caller']) && $_REQUEST['caller']=='admin' && isset($_SESSION['admin_id'])){
	$user['user_id'] = $_REQUEST['user_id'];
	$_SESSION['user_id_admin'] = $_REQUEST['user_id'];
	//$_SESSION['id'] = "admin";
}
if(!isset($user['user_id']) && isset($_SESSION['user_id_admin'])){
	$user['user_id'] = $_SESSION['user_id_admin'];
}

$trustBuilder = new TrustBuilder($user['user_id']);

if(isset($data['checkFacebook'])&&$data['checkFacebook']=='check'&&isset($data['token'])){
	$output = "";
	try{
		//var_dump($data);exit;
		$facebook = new fb ( $data['token'] );
		$fb_data = $facebook->getBasicData();
		$facebook->logFacebook($user['user_id']);
		$presence = $facebook->checkPrescence($user['user_id']);
		$fb_likes = "";
		if(($presence['status']=='NEW')||isset($data['reimport'])){
			if(!isset($data['connected_from']))
			$data['connected_from'] = 'trustbuilder';

			if(isset($data['reimport']) && $data['reimport'] == 'reimport'){
				$output = $trustBuilder->imposeReimportedData($data['token']);
			}else{
				$output = $trustBuilder->verifyFacebook($data['token'],true,$data['connected_from']);
			}
			/*	if(($user[status]=='non-authentic')&& !isset($output['error'])){
					if($trustBuilder->authenticateUser()){
						functionClass::setSessionStatus('authentic');
					}
				}*/
			
			if(!isset($output['error']) && $status=='non-authentic'){
				$trustBuilder->authenticateUser();
			}
			/*	if($output['status'] == 'SUCCESS' && isset($data['mcFlag']) && $data['mcFlag'] == 'true'){
					$userUtils = new UserUtils();
					$userUtils->setMutualsInFBConnect($user['user_id']);
				}*/
		}elseif($presence['status']=='SUCCESS'){
			$output['status']='success';
		}elseif($presence['status']=='ANOTHER'){
			$facebook->logFacebook($user['user_id'],"Another user exits");
			$output['status']='fail';
			$output['error']='Please use the same facebook account used to create the profile';
		}
	/*	elseif(isset($data['reimport'])&& $data['reimport'] == 'reimport'){
			$TMdiff = $trustBuilder->checkFbTMDiff();
			$trustBuilder->logOverrideAction($TMdiff, 'reimport from facebook');
			if(isset($TMdiff)){
				$output['status'] = 'FAIL';
				$output['error'] = 'Facebook still not updated!';
			}
		}*/
		else{
			$facebook->logFacebook($user['user_id'],"Another user exits");
			$output['status']='fail';
			$output['error']='This facebook account is in use by another trulymadly profile';
		}

		if(isset($data['action'])&&$data['action']=='interest'&&($presence['status']=='NEW'||$presence=='SUCCESS')){
			$output['fb_likes']=$func->getFbLikes();
		}
	}
	catch (Exception $e){
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		$output['status']='refresh';
	}
	print_r(json_encode($output));
	die;
}elseif(isset($data['fileSubmit'])&&isset($data['type'])&&isset($_FILES['file'])){
	$name=1000*microtime(true)."_".$func->random(18);
	$trustBuilder->documentUpload($data,$name);
}elseif(isset($data['photoUpload']) && $data['photoUpload']='yes' && isset($data['src']) && isset($data['type'])){
	try{
		$error = 0;
		$name=1000*microtime(true)."_".$func->random(18);
		$datas=explode("/",$data['src']);
		if(!($datas[0]=='http:' || $datas[0]=='https:')) {
			list($type, $data['src']) = explode(';', $data['src']);
			list(, $data['src']) = explode(',', $data['src']);
			$type_array=explode(":",$type);
			$fileType=$type_array[1];
			$fileTypeArray=explode("/",$fileType);
			$image_type=$fileTypeArray[1];
			$data['src'] = base64_decode($data['src']);

			if(!file_exists($baseurl."/files/images/profiles/".$name.".".$image_type))  {
				file_put_contents($baseurl."/files/images/profiles/".$name.".".$image_type,$data['src']);
				
				if($data['type']==1){
					$rs = $conn->Execute($conn->prepare("Select * from user_id_proof where user_id=?"),array($user['user_id']));
					if ($rs->RowCount () > 0) {
						$row = $rs->FetchRow ();
						
						$query="update user_id_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? ";
						$param_array=array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']);
						$tablename="user_id_proof";
						Query_Wrapper::UPDATE($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("update user_id_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
								array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']));*/
					}else{
						$query="insert into user_id_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?) ";
						$param_array=array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation');
						$tablename="user_id_proof";
						Query_Wrapper::INSERT($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("insert into user_id_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status) values(Now(),?,?,?,?,?)"),
								array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation'));*/
					}
				}elseif ($data['type']==2){
					$rs = $conn->Execute($conn->prepare("Select * from user_address_proof where user_id=?"),array($user['user_id']));
					if ($rs->RowCount () > 0) {
						$row = $rs->FetchRow ();
						
						$query="update user_address_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=?";
						$param_array=array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']);
						$tablename="user_address_proof";
						Query_Wrapper::UPDATE($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("update user_address_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
								array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']));*/
					}else{
						$query="insert into user_address_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,landmark) values(Now(),?,?,?,?,?,'','','','','')";
						$param_array=array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation');
						$tablename="user_address_proof";
						Query_Wrapper::INSERT($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("insert into user_address_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,landmark) values(Now(),?,?,?,?,?,'','','','','')"),
								array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation'));*/
					}
				}elseif($data['type']==3){
					$rs = $conn->Execute($conn->prepare("Select * from user_employment_proof where user_id=?"),array($user['user_id']));
					if ($rs->RowCount () > 0) {
						$row = $rs->FetchRow ();
						
						$query="update user_employment_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? ";
						$param_array=array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']);
						$tablename="user_employment_proof";
						Query_Wrapper::UPDATE($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("update user_employment_proof SET upload_time=NOW(),proof_type=?,img_location=?,status='under_moderation',number_of_trials=? where user_id=? "),
								array($data['proofType'],$name.".".$image_type,$row['number_of_trials']+1,$user['user_id']));*/
					}else{
						
						$query="insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,name,type,tenure) values(Now(),?,?,?,?,?,'','','','','','','')";
						$param_array=array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation');
						$tablename="user_employment_proof";
						Query_Wrapper::INSERT($query, $param_array, $tablename);
						
						/*$conn->Execute($conn->prepare("insert into user_employment_proof (upload_time,user_id,proof_type,img_location,number_of_trials,status,address,city,state,pincode,name,type,tenure) values(Now(),?,?,?,?,?,'','','','','','','')"),
								array($user['user_id'],$data['proofType'],$name.".".$image_type,'1','under_moderation'));*/
					}
				}
				
			}else{
				$error = 1;
			}
		}else{
			$error = 1;
		}
	}catch(Exception $e) {
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		$error = 1;
	}
	$return = '';
	if($error==0){
		$return['status']='success';
	}else{
		$return['status']='fail';
		$return['error']='Some Error Occured. Please try again.';
	}
	print_r(json_encode($return));
	die;
}elseif(isset($data['unlimited'])){
	
	$query="update user SET plan=? where user_id=?";
	$param_array=array('unlimited',$user['user_id']);
	$tablename="user";
	Query_Wrapper::UPDATE($query, $param_array, $tablename);
	
	/*$conn->Execute($conn->prepare("update user SET plan=? where user_id=?"),
			array('unlimited',$user['user_id']));*/
	$return['status']='success';
	print_r(json_encode($return));
	die;
}elseif(isset($data['action'])&&$data["action"]=='addressForm'){
	if($trustBuilder->saveAddressForm($data)){
		$return['status']='success';
	}else{
		$return['status']='fail';
	}
	print_r(json_encode($return));
	die;
}elseif (isset($data['action'])&&$data["action"]=='employmentForm'){
if($trustBuilder->saveEmploymentForm($data)){
		$return['status']='success';
	}else{
		$return['status']='fail';
	}
	print_r(json_encode($return));
	die;
}elseif(isset($data['action'])&&$data['action']=='numberVerify'){
	$status = $trustBuilder->phoneSubmit($data['number']);
	if($status)
		print_r(json_encode($status));
	else print_r(json_encode(array("status"=>"FAIL","error"=>"Some error occured.Please retry.")));
	die;
}elseif(isset($data['action'])&&$data['action']=='checkNumber'){
	$status = $trustBuilder->checkPhoneStatus();
	if($status)
		print_r(json_encode($status));
	else print_r(json_encode(array("status"=>"FAIL")));
	die;
}elseif(isset($data['action'])&&$data['action']=='sendpassword'){
	$return = $trustBuilder->sendPassword($data);
	print_r(json_encode($return));
	die;
}elseif(isset($data['action'])&&$data['action']=='syncNamewithID'){
	if($data['type']==1){
		
		$return = $trustBuilder->syncNamewithID($data);
	/*	$rs = $conn->Execute($conn->prepare("Select * from user_id_proof where user_id=?"),array($user['user_id']));
		if ($rs->RowCount () > 0) {
			$row = $rs->FetchRow ();
			if($data['update']=='age_mismatch'){
				$conn->Execute($conn->prepare("update user_data SET DateOfBirth=? where user_id=? "),
						array($row['dateofbirth'],$user['user_id']));
			}else if($data['update']=='name_mismatch'){
				$conn->Execute($conn->prepare("update user SET fname=?,lname=? where user_id=? "),
						array($row['fname'],$row['lname'],$user['user_id']));
			}else if($data['update']=='name_age_mismatch'){
				$conn->Execute($conn->prepare("update user_data SET DateOfBirth=? where user_id=? "),
						array($row['dateofbirth'],$user['user_id']));
				$conn->Execute($conn->prepare("update user SET fname=?,lname=? where user_id=? "),
						array($row['fname'],$row['lname'],$user['user_id']));
			}
			
			if($data['update']=='age_mismatch' || $data['update']=='name_mismatch' || $data['update']=='name_age_mismatch'){
				$conn->Execute($conn->prepare("update user_id_proof SET status='active',reject_reason='' where user_id=? "),
						array($user['user_id']));
				$conn->Execute($conn->prepare("insert into user_id_proof_log (tstamp,user_id,proof_type,img_location,number_of_trials,status,reject_reason,password,name_age_mismatch_allow) values(Now(),?,?,?,?,?,?,?,?)"),
						array($user['user_id'],$row['proof_type'],$row['img_location'],$row['number_of_trials'],'active',$row['reject_reason'],'','user done the changes'));
				
				$trustBuilder->updateIdTrustScore($row['proof_type']);
				$trustBuilder->authenticateUser();
			}
			
			
			$return['status']='success';
		}else 
			$return['status'] = 'fail';
		*/
		print_r(json_encode($return));
		die;
	}
}
/*elseif (isset($_REQUEST['resetAction']) && $_REQUEST['resetAction'] == 'overrideTmFb'){
	$trustBuilder->overrideTMFromFb();
	$newdiff = $trustBuilder->checkFbTMDiff();
	
	if(empty($newdiff)) {
		$uu = new UserUtils();
		$connections = $uu->getFbConnectionCount($user ['user_id']);
		$trustBuilder->updateFacebookTrustScore($connections);
		$trustBuilder->creditPoint($tbNumbers['credits']['facebook'],'facebook',$tbNumbers['validity']['facebook']);
		echo 'corrected';
		die;
	}
}*/

// if(!(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')){
// 	header("Location:".$baseurl_https."/trustbuilder.php");
// }

//$device_type = ($_REQUEST ['login_mobile'] == true) ? 'mobile' : 'html';
if(!(isset($_SESSION['user_id_admin']))){
	/*if ($device_type == 'mobile') {
		$authentication = functionClass::redirect ( 'trustbuilder', true );
		if ($authentication ['error'] != null)
			echo json_encode ( $authentication );
	} else {
		functionClass::redirect ( 'trustbuilder' );
	}*/
	functionClass::redirect ( 'trustbuilder',$login_mobile );
}

try {
	
	if(isset($_REQUEST['from_registration']) && $_REQUEST['from_registration'] == true){
		$smarty->assign("isRegistration",'1');
	}
	
	$user_id = $user ['user_id'];
	$header = new header($user_id);
	$header_values = $header->getHeaderValues();
	$gender = $header_values['gender'];
	//$header= $trustBuilder->getHeaderValues($user_id);
	$idStatus = $trustBuilder->checkIdProofStatus();
	$phoneStatus = $trustBuilder->checkPhoneStatus();
	$addressStatus = $trustBuilder->checkAddressProofStatus();
	$employmentStatus = $trustBuilder->checkEmploymentProofStatus();
	$facebookStatus = $trustBuilder->checkFacebookStatus();
	$isFbVerified = $trustBuilder->isFbVerified();
	$isAnyprofilePic = $trustBuilder->isAnyprofilePic();
	$authenticityConstant = $trustBuilder->getThreshold();      // for A/B testing
	
	//var_dump($facebookStatus);die;
	if(	$facebookStatus['status'] == 'no'){
		$diff = $trustBuilder->checkFbTMDiff();
	}
	$linkedStatus = $trustBuilder->checkLinkedInStatus();
	$trustPoints = $trustBuilder->getTrustScore();
	
	//get endorsementData if there are any endorsements
	
	/*
	 * endorsement block
	 */
	$endorsementData = array();
	$endorsementData['data'] =array();
	if($trustPoints['endorsement_count']>0){
		$endorses = $trustBuilder->getEndorsementData();
		$endorsementData['data'] = $endorses[$user_id];
//		if(count($endorses[$user_id])==1){
//			$endorsementData["endorsement_message"] = $tbMessages['endorsement_ifsingle_message'];
//		}
	}
	$userUtils = new UserUtils();
	$endorsementData['link']=$userUtils->generateEndorsementLink($user_id, time(), $gender, $webCall = true );
	$endorsementData["endorsement_success_message"] = $tbMessages['endorsement_success'];
	$endorsementData["endorsement_threshold_count"] = EndorsementConstants::MinimumEndorsementCount;
	
	if(!isset($endorsementData["endorsement_message"]))
	$endorsementData["endorsement_message"] = ($gender == "M")?$tbMessages['endorsement_male']:$tbMessages['endorsement_female'];
	

	// 	$trustBuilder = new TrustBuilder ( $user_id );
	// 		$page_id = 1;
	// 		if (isset ( $_GET ['page_id'] ))
	// 			$page_id = $_GET ['page_id'];
	// 		$ids = $fmatch_user->fetchResults ( $page_id );
	// 		$UserUtils=new UserUtils();

} catch ( Exception $e ) {
	trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
// 	echo $e->getMessage();
}
//var_dump($facebookStatus);

try{
		$smarty->assign("user_id",$user['user_id']);
		$smarty->assign("isFbVerified", $isFbVerified['isVerify']);
		//$smarty->assign("fbConnected",$facebookStatus['status']);
		$smarty->assign("fbConnection",$isFbVerified['connection']);
		$smarty->assign("diff", $diff);
		$smarty->assign("endorsementData", $endorsementData); 
		$smarty->assign("fbTmDiff", $facebookStatus['mismatch']);
		$smarty->assign("isAnyprofilePic",$isAnyprofilePic);
		
		if($phoneStatus){
			$smarty->assign("phoneVerified",$phoneStatus['status']);
			//$smarty->assign("phoneNumber",$phoneStatus['user_number']);
			$smarty->assign("phoneNumber",substr_replace($phoneStatus['user_number'],"xxxxxxx",3,9));
			$smarty->assign("phoneTrials",$phoneStatus['number_of_trials']);
		}else{
			$smarty->assign("phoneVerified",'N');
		}
		
		if($idStatus){
			$smarty->assign("idVerified",$idStatus['status']);
			$smarty->assign("idproofType",$idStatus['proof_type']);
			$smarty->assign("idReject",$idStatus['reject_reason']);
			$smarty->assign("id_diff",$idStatus);
		}else{
			$smarty->assign("idVerified",'N');
		}

		if($addressStatus){
			$smarty->assign("addressVerified",$addressStatus['status']);
			$smarty->assign("addressproofType",$addressStatus['proof_type']);
		}else{
			$smarty->assign("addressVerified",'N');
		}
		
		if($employmentStatus){
			if($employmentStatus['show_name']=='no'){
				$employmentStatus['company_name'] = substr($employmentStatus['company_name'], 0,1). str_repeat('x',strlen($employmentStatus['company_name']) - 2) . substr($employmentStatus['company_name'], strlen($employmentStatus['company_name']) - 1 ,1);
				$smarty->assign("companyName",$employmentStatus['company_name']);
			}else 
				$smarty->assign("companyName",$employmentStatus['company_name']);
			$smarty->assign("empVerified",$employmentStatus['status']);
			$smarty->assign("empproofType",$employmentStatus['proof_type']);
			$smarty->assign("empReject",$employmentStatus['reject_reason']);
			$smarty->assign("showName",$employmentStatus['show_name']);
		}else{
			$smarty->assign("empVerified",'N');
		}
		
//not needed; can be retrieved from header
	/*	$ud = new UserData($user_id);
		$gender = $ud->fetchGender();*/
		$smarty->assign("linkedinVerified",$linkedStatus['status']);
		$smarty->assign("linkedinConnection",$linkedStatus['connection']);
		$smarty->assign("linkedinDesignation",$linkedStatus['position']);
		$smarty->assign("trustscore",isset($trustPoints['trust_score'])?$trustPoints['trust_score']:0);
		$smarty->assign("trustpoints",$trustPoints);
		$smarty->assign("trustpie",$trustPoints['trust_score']>100 ? 100 : $trustPoints['trust_score']);
		//$smarty->assign("workarea",$trustBuilder->getUserWork());
		$smarty->assign('header',$header_values);
		$smarty->assign('trustbuilder', 'trustbuilder');
		$smarty->assign('tbNumbers', $tbNumbers);
		$smarty->assign('tbMessages', $tbMessages);
		//$smarty->assign('photoIdmsg',systemMsgs::$photoId);
		$smarty->assign('trustPage', 'trustPage');
		$smarty->assign('status', $status);
		$smarty->assign('gender', $gender);
		//$authenticityConstant = ($gender == "M")?TrustAuthenticConstants::TrustAuthenticConstantMale:TrustAuthenticConstants::TrustAuthenticConstantFemale;
		$smarty->assign('thresold', $authenticityConstant);
		$smarty->assign("endorsementData", $endorsementData);
	if(!$login_mobile){
		$smarty->display ("templates/trustbuilder.tpl" ) ;
	}else{
			$smarty->assign('from_mobile','1');
			$output = $smarty->fetch("templates/trustbuilder.tpl");
			$response['data'] = $output;
			print_r($output);die;
	}
	}catch(Exception $e){
		trigger_error("PHP Web:".$e->getTraceAsString(), E_USER_WARNING);
		//echo $e->getMessage();
	}
?>
