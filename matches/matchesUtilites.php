<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";

/**
 * ad-hoc requirements for matches
 * API deals with all the functions to call for ad-hoc feature requests
 * @author himanshu
 *
 */
class matchesUtils{

	private $user_id;

	function __construct($user_id)
	{
		$this->user_id = $user_id;
	}

	/**
	 * fill survey in case of females
	 */
	public function setSurveyFilledFlag($ans){
		$sql = "Insert into survey (user_id, non_authentic_female_survey_ans,non_authentic_female_survey_tstamp) values (?,?,now())";
		Query::UPDATE($sql, array($this->user_id,$ans));
	}

	/**
	 * set privacy option for females
	 * so that they can be seen by only those whom she has liked
	 */
	// add loggin for how many switch happened between hide and unhide - should be driven by app (into tracking calls)
	//also error tracking from app side in case some error occurs, then tracker should write into log
	public function setHideFlag($flag)
	{
		$sql = "INSERT INTO `user_flags` (user_id, hide_myself, hide_flag_update_tstamp) values (?,?, now())
				ON DUPLICATE KEY update hide_myself = ?, hide_flag_update_tstamp = now()";
		Query::INSERT($sql, array($this->user_id, $flag, $flag));

		$sql2 = "UPDATE `user_search` set hide_flag = ? where user_id = ?";
		Query::UPDATE($sql2, array($flag, $this->user_id));
	}
}

try{
	$user = functionClass::getUserDetailsFromSession ();
	$user_id = $user ['user_id'];
	$mUtils = new matchesUtils($user_id);

	if( isset ($user_id))
	{
		if($_REQUEST['set_survey_for_non_authentic'] == 1)
		{
			$mUtils->setSurveyFilledFlag($_REQUEST['survey_answer'] );
			echo json_encode(array("responseCode"=>200));
		}
		elseif (isset($_REQUEST['is_discovery_on']))
		{
			$hide_flag = $_REQUEST['is_discovery_on'];

			if($hide_flag == 1 || $hide_flag == 0)
			$mUtils->setHideFlag($hide_flag);

			echo json_encode(array("responseCode"=>200));
		}
	}

}
catch (Exception $e)
{
	echo json_encode(array("responseCode"=>403));
	trigger_error("PHP WEB: ".$e->getTraceAsString(), E_USER_WARNING);
}

?>