<?php
require_once dirname ( __FILE__ ) . "/../include/config.php";
require_once dirname ( __FILE__ ) . "/../include/function.php";

/**
 * It calculates match score between two users
 * @assumption: user1 is female and user2 is male
 *
 * @author himanshu
 *        
 */
class matchScore {
	private $usrQ;
	private $conn;
	private $redis;
	private $currentQunSet;
	private $defaultQunValues;
	private $currentQunCount;
	private $fScore;
	private $mScore;
	
	function __construct() {
		global $redis, $conn;
		// $red = new Redis ();
		// $red->connect ( $redis_instance ['IP'], $redis_instance ['PORT'] );
		
		$this->redis = $redis;
		$this->conn = $conn;
		$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		
		/*
		 * $query = "SELECT questions_set as qSet, default_values as defaults FROM current_psycho_questions order by date desc limit 1"; $ex = $this->conn->Execute ( $query ); $qSet = $ex->FetchRow ();
		 */

		/*
		 * $this->currentQunCount = $ex->RowCount(); var_dump($this->currentQunCount ); var_dump($qSet); die;
		 */
/*		$this->currentQunSet = $qSet ['qSet'];
		$quns = explode ( ',', $this->currentQunSet );
		// var_dump($quns);
		$this->currentQunCount = count ( $quns );

		if (! empty ( $qSet ['defaults'] ))
		$this->defaultQunValues = json_decode ( $qSet ['defaults'], true );*/
	}
	
	/**
	 * Gets the score of each question passing the question id
	 * and returns the cumulative score of that particular type of question set
	 * 
	 * @param
	 *        	$type
	 * @param
	 *        	$questions
	 * @param
	 *        	$debug
	 */
	private function calculate_count($type, $questions, $debug) {
		// $question_ids = array (1,2,3,4);
		$female_score = 0;
		$male_score = 0;
		
		foreach ( $questions as $que ) {
			$female_score += $this->fScore [$que] ['score'];
			$male_score += $this->mScore [$que] ['score'];
			$quetion_id = $que;
			if ($debug == true) {			
				echo "Male: Question:$quetion_id:" . $this->mScore [$que] ['question'] . "  :Answer: " . $this->mScore [$que] [answer] . " :Score: " . $this->mScore [$que] [score] . "\n<br>";
				echo "Female: Question:$quetion_id:" . $this->fScore [$que] ['question'] . "  :Answer: " . $this->fScore [$que] [answer] . " :Score: " . $this->fScore [$que] [score] . "\n<br>";
			}
		}
		if ($debug == true) {
			echo "Male Score:$male_score" . "\n<br>";
			echo "Female Score:$female_score" . "\n<br>";
		}
		return array (
				$male_score,
				$female_score 
		);
	}
	
	private function calculate_single_count($data_array,$question_array,$debug){
		$score = 0;
		foreach ( $question_array as $que ) {
			$score += $data_array [$que] ['score'];
			$quetion_id = $que;
			if ($debug == true) {
				echo "Question:$quetion_id:" . $data_array[$que] ['question'] . "  :Answer: " . $data_array [$que] [answer] . " :Score: " . $data_array[$que] [score] . "\n<br>";
			}
			}
			if ($debug == true) {
			echo "Score:$score" . "\n<br>";
		}
		return  $score;		
	}
	
	/**
	 * first is female id and second is male id
	 */
	private function fetchDataFromCache($fid, $mid) {
		$hash = ($fid << 32 | $mid);
		$key = $hash / 1000;
		$val = $this->redis->hGet ( "ps:$key", $hash );
		return $val;
	}
	
	public function setCountInCache($fid, $mid, $value) {
		$hash = ($fid << 32 | $mid);
		$key = $hash / 1000;
		$this->redis->hSet ( "ps:$key", $hash, $value );
	}
	
	
	public function calculateBasicProfileScore($data,$gender,$debug){
		$score=0;
		
		if ($debug == true) {
			echo "Calculating for $gender\n";
		}

	 $current_score=$this->calculate_single_count($data,array (1,2,3,4),$debug);
	 	if ($current_score <10)
			$score += -1;
		else if ($current_score <=11)
			$score += 0;
		else 	$score += 1;
		
	if ($debug == true) {
			echo "Unassured Submissive match score:$current_score" . "\n<br><br>";
			echo "final  score:$score" . "\n<br><br>";
		}
		$current_score=$this->calculate_single_count($data,array (9,10,11,12),$debug);
		if ($current_score <11)
			$score += -1;
		else if ($current_score <=12)
			$score += 0;
		else 	$score += 1;
		
	if ($debug == true) {
			echo "Warm agreeable  match score:$current_score" . "\n<br><br>";
			echo "final  score:$score" . "\n<br><br>";
		}
		
		$current_score=$this->calculate_single_count($data,array (13,14,15,16),$debug);
		if($gender=='male'){
			if ($current_score <=13)
				$score += 1;
			else if ($current_score <=15)
				$score += 0;
			else 	$score += -1;			
		}else{
			if ($current_score <=14)
				$score += 1;
			else if ($current_score ==15)
				$score += 0;
			else 	$score += -1;
		}
		
		if ($debug == true) {
			echo "Dominant  match score:$current_score" . "\n<br><br>";
			echo "final  score:$score" . "\n<br><br>";
		}
		
		$current_score=$this->calculate_single_count($data,array (17,18,19,20),$debug);
		if($gender=='male'){
			if ($current_score <=13)
				$score += 1;
			else if ($current_score <=15)
				$score += 0;
			else 	$score += -1;
				
		}else{
			if ($current_score <=14)
				$score += 1;
			else if ($current_score ==15)
				$score += 0;
			else 	$score += -1;
		}
		
		if ($debug == true) {
			echo "Arrogant  match score:$current_score" . "\n<br><br>";
			echo "final  score:$score" . "\n<br><br>";
		}
		
		$current_score=$this->calculate_single_count($data,array (21,22,23,24),$debug);	
			if ($current_score <11)
				$score += -1;
			else if ($current_score ==12)
				$score += 0;
			else 	$score += 1;
		
			if ($debug == true) {
				echo "Problem Solving  match score:$current_score" . "\n<br><br>";
				echo "final  score:$score" . "\n<br><br>";
			}
			
		$current_score=$this->calculate_single_count($data,array (25,26,27,28,29,30,31,32,33,34),$debug);
			if($gender=='male'){
				if ($current_score <15)
					$score += -1;
				else if ($current_score ==15)
					$score += 0;
				else 	$score += 1;
			
			}else{
				if ($current_score <14)
					$score += -1;
				else if ($current_score <=15)
					$score += 0;
				else 	$score += 1;
			}
		
			if ($debug == true) {
				echo "Self-esteem match score:$current_score" . "\n<br><br>";
				echo "final  score:$score" . "\n<br><br>";
			}
			return $score;
	}
	
	/**
	 * calculates the score based on ques types and sums up their results
	 * 
	 * @param unknown_type $fid        	
	 * @param unknown_type $mid        	
	 * @param unknown_type $debug        	
	 */
	public function calculateMatchScore($fid, $mid, $gender,$debug = false) {
		/*if ($debug == false) {
			// check redis if data is present.
			$val=$this->fetchDataFromCache($fid, $mid);
			if($val!==false)
				return $val;
		}
		*/
		$matchScore = 0;
		$this->fScore = $this->getuserTest ( $fid, $debug );
		$this->mScore = $this->getuserTest ( $mid, $debug );
		
		$final_match_score = 0;
		/*$male_score_individual=$this->calculateBasicProfileScore($this->mScore,"male",$debug);
		$female_score_individual=$this->calculateBasicProfileScore($this->fScore,"female",$debug);
		
		if($debug==true){
			echo "Individual Male Score:$male_score_individual\n<br><br>";
			echo "Individual Female Score:$female_score_individual\n<br><br>";
		}
		*/
		list ( $male_score, $female_score ) = $this->calculate_count ( "Talking in loving manner ", array (35), $debug );
		$abs_diff = abs ( $male_score - $female_score );
		
		$match_score = 0;
		if ($abs_diff <= 1)
			$match_score = 2;
		else if ($abs_diff <= 2)
			$match_score = 1;
		else
			$match_score = 0;
		
		$final_match_score += $match_score;
		
		if ($debug == true) {
			echo "Talking in loving manner:$match_score" . "\n<br><br>";
			echo "Final Score:$final_match_score" . "\n<br><br>";
		}
		
		list ( $male_score, $female_score ) = $this->calculate_count ( "Physical expression", array (36), $debug );
		$abs_diff = abs ( $male_score - $female_score );
		$match_score = 0;
		if ($abs_diff <= 1)
			$match_score = 2;
		else if ($abs_diff <= 3)
			$match_score = 1;
		else
			$match_score = 0;
		$final_match_score += $match_score;
		
		if ($debug == true) {
			echo "Physical expression:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		list ( $male_score, $female_score ) = $this->calculate_count ( "spending time", array (
				37 
		), $debug );
		$abs_diff = abs ( $male_score - $female_score );
		$match_score = 0;
		if ($abs_diff <= 1)
			$match_score = 2;
		else if ($abs_diff <= 3)
			$match_score = 1;
		else
			$match_score = 0;
		
		$final_match_score += $match_score;
		
		if ($debug == true) {
			echo "spending time match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		list ( $male_score, $female_score ) = $this->calculate_count ( "Doing things", array (
				38
		), $debug );
		$abs_diff = abs ( $male_score - $female_score );
		
		$match_score = 0;
		if ($abs_diff <= 1)
			$match_score = 2;
		else if ($abs_diff <= 3)
			$match_score = 1;
		else
			$match_score = 0;
		
		$final_match_score += $match_score;
		
		if ($debug == true) {
			echo "Doing things  match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
	/*	list ( $male_score, $female_score ) = $this->calculate_count ( "Household chores", array (
				43
		), $debug );
		
		if ($male_score == 1){
			if($female_score<=2)
				$match_score=0;
			else $match_score=1;
		}else if($male_score==3&&$female_score==3){
			$match_score=2;
		}else if($female_score==1&&$male_score>=2){
			$match_score=2;
		}else if($female_score==2&&$male_score==2){
			$match_score=0;
		}
		else{
			$match_score=1;
		}
		
		
		$final_match_score += $match_score;
		
		if ($debug == true) {
			echo "Household chores match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		
		list ( $male_score, $female_score ) = $this->calculate_count ( "Financial Decisions", array (
				44
		), $debug );
		
		if (($male_score == 2&&$female_score<=2)||($male_score == 3&&$female_score==1)||($male_score == 1&&$female_score==1)){
			$match_score=0;
		}else if($female_score==3){
			$match_score=2;
		}else{
			$match_score=1;
		}
		
		$final_match_score += $match_score;		
		if ($debug == true) {
			echo "Financial Decisions match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		list ( $male_score, $female_score ) = $this->calculate_count ( "Investments", array (
				45
		), $debug );
		
		if (($male_score == 1&&$female_score==1)||($male_score == 2&&$female_score<=2)){
			$match_score=0;
		}else if(($male_score == 2&&$female_score==3)||($male_score == 3&&$female_score==1)){
			$match_score=1;
		}else{
			$match_score=2;
		}
		
		$final_match_score += $match_score;
		if ($debug == true) {
			echo "Investments match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		list ( $male_score, $female_score ) = $this->calculate_count ( "Taking care of parents  ", array (
				46	
		), $debug );
		
		if (($male_score == 1&&$female_score<=2)||($male_score == 3&&$female_score==1)||($male_score == 2&&$female_score==2)){
			$match_score=0;
		}else if(($male_score == 2&&$female_score==3)||($male_score == 3&&$female_score==3)){
			$match_score=1;
		}else{
			$match_score=2;
		}
		$final_match_score += $match_score;
		if ($debug == true) {
			echo "Taking care of parents match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		
		list ( $male_score, $female_score ) = $this->calculate_count ( "Earning money ", array (
				47
		), $debug );
		
		if (($male_score == 1&&($female_score==1||$female_score==3))||($male_score == 2&&$female_score<=2)){
			$match_score=0;
			
		}else if(($male_score == 2&&$female_score==3)||($male_score == 3&&$female_score==1)){
			$match_score=2;
		}else{
			$match_score=1;
		}
		
		$final_match_score += $match_score;
		if ($debug == true) {
			echo "Earning money match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		*/
		//Rituals and tradition
		$male_que_id=40;
		$female_que_id=42;
		$male_score=$this->mScore [$male_que_id] ['score'];
		$female_score=$this->fScore [$female_que_id] ['score'];
		
		if ($male_score <= 2&&$female_score==4){
			$match_score=-100;				
		}else if(($male_score == 1&&($female_score==2||$female_score==3))||($male_score == 2&&$female_score==3)||($male_score == 3&&$female_score==4)){
			$match_score=0;
		}else{
			$match_score=1;
		}
		
		$final_match_score += $match_score;
		if ($debug == true) {
			echo "Male: Question:$quetion_id:" . $this->mScore [$male_que_id] ['question'] . "  :Answer: " . $this->mScore [$male_que_id] [answer] . " :Score: " . $this->mScore [$male_que_id] [score] . "\n<br>";
			echo "Female: Question:$quetion_id:" . $this->fScore [$female_que_id] ['question'] . "  :Answer: " . $this->fScore [$female_que_id] [answer] . " :Score: " . $this->fScore [$female_que_id] [score] . "\n<br>";
			echo "Male Score:$male_score" . "\n<br>";
			echo "Female Score:$female_score" . "\n<br>";
			echo "Rituals and tradition  match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		
		//Rituals and tradition
		$male_que_id=39;
		$female_que_id=41;
		$male_score=$this->mScore [$male_que_id] ['score'];
		$female_score=$this->fScore [$female_que_id] ['score'];
		
		if (($male_score == 1&&$female_score==2)||($male_score == 2&&$female_score==1)){
			$match_score=-100;
		}else{
			$match_score=1;
		}
		
		$final_match_score += $match_score;
		if ($debug == true) {
			
			echo "Male: Question:$quetion_id:" . $this->mScore [$male_que_id] ['question'] . "  :Answer: " . $this->mScore [$male_que_id] [answer] . " :Score: " . $this->mScore [$male_que_id] [score] . "\n<br>";
			echo "Female: Question:$quetion_id:" . $this->fScore [$female_que_id] ['question'] . "  :Answer: " . $this->fScore [$female_que_id] [answer] . " :Score: " . $this->fScore [$female_que_id] [score] . "\n<br>";
			echo "Male Score:$male_score" . "\n<br>";
			echo "Female Score:$female_score" . "\n<br>";
			echo "Career  match score:$match_score" . "\n<br><br>";
			echo "final  score:$final_match_score" . "\n<br><br>";
		}
		// Following Rituals and traditions
		
		// var_dump ( $this->fScore );
		// var_dump ( $this->mScore );
		
		// Interest and Hobbies Scoring
		$score_from_IH=$this->interestNHobbiesScore ( $fid, $mid , $debug);
		$final_match_score += $this->interestNHobbiesScore ( $fid, $mid );
		
		
		if ($debug == true) {
			echo "I&H  Match score:$score_from_IH" . "\n<br><br>";
			echo "Grand Male score:".($final_match_score+$male_score_individual)."\n<br><br>";
			echo "Grand Female score:".($final_match_score+$female_score_individual)."\n<br><br>";
		}
		
		//$final_match_score+=$female_score_individual;
		//$this->setCountInCache($fid, $mid, $final_match_score);
		//return array($final_match_score+$female_score_individual,$final_match_score+$male_score_individual);
		if($gender=="female")return $final_match_score+$female_score_individual;

		return $final_match_score+$male_score_individual;
	//	return $final_match_score+$female_score_individual;
	}
	
	private function interestNHobbiesScore($fid, $mid, $debug) {
		$sql = $this->conn->Prepare ( "SELECT * FROM interest_hobbies where user_id IN(?,?)" );
		$exec = $this->conn->Execute ( $sql, array (
				$fid,
				$mid 
		) );
		$results = $exec->GetRows ();
		
		$commonInterests = array ();
		$profileKey = ($results [0] ['user_id'] == $fid) ? 0 : 1;
		$userKey = ($profileKey == 0) ? 1 : 0;
		
		$profile_music_preferences = json_decode ( $results [$profileKey] ['music_preferences'], true );
		$profile_music_favorites = json_decode ( $results [$profileKey] ['music_favorites'], true );
		$profile_books_preferences = json_decode ( $results [$profileKey] ['books_preferences'], true );
		$profile_books_favorites = json_decode ( $results [$profileKey] ['books_favorites'], true );
		$profile_food_preferences = json_decode ( $results [$profileKey] ['food_preferences'], true );
		$profile_food_favorites = json_decode ( $results [$profileKey] ['food_favorites'], true );
		$profile_movies_preferences = json_decode ( $results [$profileKey] ['movies_preferences'], true );
		$profile_movies_favorites = json_decode ( $results [$profileKey] ['movies_favorites'], true );
		$profile_sports_preferences = json_decode ( $results [$profileKey] ['sports_preferences'], true );
		$profile_sports_favorites = json_decode ( $results [$profileKey] ['sports_favorites'], true );
		$profile_travel_preferences = json_decode ( $results [$profileKey] ['travel_preferences'], true );
		$profile_travel_favorites = json_decode ( $results [$profileKey] ['travel_favorites'], true );
		
		$user_music_preferences = json_decode ( $results [$userKey] ['music_preferences'], true );
		$user_music_favorites = json_decode ( $results [$userKey] ['music_favorites'], true );
		$user_books_preferences = json_decode ( $results [$userKey] ['books_preferences'], true );
		$user_books_favorites = json_decode ( $results [$userKey] ['books_favorites'], true );
		$user_food_preferences = json_decode ( $results [$userKey] ['food_preferences'], true );
		$user_food_favorites = json_decode ( $results [$userKey] ['food_favorites'], true );
		$user_movies_preferences = json_decode ( $results [$userKey] ['movies_preferences'], true );
		$user_movies_favorites = json_decode ( $results [$userKey] ['movies_favorites'], true );
		$user_sports_preferences = json_decode ( $results [$userKey] ['sports_preferences'], true );
		$user_sports_favorites = json_decode ( $results [$userKey] ['sports_favorites'], true );
		$user_travel_preferences = json_decode ( $results [$userKey] ['travel_preferences'], true );
		$user_travel_favorites = json_decode ( $results [$userKey] ['travel_favorites'], true );
		
		/*
		 * echo '<pre>'; var_dump($profileInterestNHobbies);die;
		 */
		$score = 0;
		// echo $key;
		
		// var_dump($interests_arr[$key]));
		/*
		 * var_dump($user_music_preferences)); var_dump($profile_music_preferences));
		 */
		$commonInterests ['common_music_preference'] = implode ( ', ', array_intersect ( $user_music_preferences, $profile_music_preferences ) );
		$commonInterests ['common_music_favorites'] = implode ( ', ', array_intersect ( $user_music_favorites, $profile_music_favorites ) );
		//var_dump($commonInterests ['common_music_favorites']);
		if (strlen ( $commonInterests ['common_music_preference'] ) > 0 || strlen ( $commonInterests ['common_music_favorites'] ) > 0)
			$score ++;
		if($debug == true){
			echo "music matched:" . $score; 
		}
			
		$commonInterests ['common_books_preference'] = implode ( ', ', array_intersect ( $user_books_preferences, $profile_books_preferences ) );
		$commonInterests ['common_books_favorites'] = implode ( ', ', array_intersect ( $user_books_favorites, $profile_books_favorites ) );
		if (strlen ( $commonInterests ['common_books_preference'] ) > 0 || strlen ( $commonInterests ['common_books_favorites'] ) > 0)
			$score ++;
	if($debug == true){
			echo "books matched:" . $score; 
		}
			
		$commonInterests ['common_food_preference'] = implode ( ', ', array_intersect ( $user_food_preferences, $profile_food_preferences ) );
		$commonInterests ['common_food_favorites'] = implode ( ', ', array_intersect ( $user_food_favorites, $profile_food_favorites ) );
		if (strlen ( $commonInterests ['common_food_preference'] ) > 0 || strlen ( $commonInterests ['common_food_favorites'] ) > 0)
			$score ++;
	if($debug == true){
			echo "food matched:" . $score; 
		}
			
		$commonInterests ['common_movies_preference'] = implode ( ', ', array_intersect ( $user_movies_preferences, $profile_movies_preferences ) );
		$commonInterests ['common_movies_favorites'] = implode ( ', ', array_intersect ( $user_movies_favorites, $profile_movies_favorites ) );
		if (strlen ( $commonInterests ['common_movies_preference'] ) > 0 || strlen ( $commonInterests ['common_movies_favorites'] ) > 0)
			$score ++;
	if($debug == true){
			echo "movies matched:" . $score; 
		}
			
		$commonInterests ['common_sports_preference'] = implode ( ', ', array_intersect ( $user_sports_preferences, $profile_sports_preferences ) );
		$commonInterests ['common_sports_favorites'] = implode ( ', ', array_intersect ( $user_sports_favorites, $profile_sports_favorites ) );
		if (strlen ( $commonInterests ['common_sports_preference'] ) > 0 || strlen ( $commonInterests ['common_sports_favorites'] ) > 0)
			$score ++;
	if($debug == true){
			echo "sports matched:" . $score; 
		}
			
		$commonInterests ['common_travel_preference'] = implode ( ', ', array_intersect ( $user_travel_preferences, $profile_travel_preferences ) );
		$commonInterests ['common_travel_favorites'] = implode ( ', ', array_intersect ( $user_travel_favorites, $profile_travel_favorites ) );
		if (strlen ( $commonInterests ['common_travel_preference'] ) > 0 || strlen ( $commonInterests ['common_travel_favorites'] ) > 0)
			$score ++;
	if($debug == true){
			echo "travel matched:" . $score; 
		}
			
		return $score;
	}
	private function getKeyforMatchScore($fid, $mid) {
		return ($fid > $mid) ? "$fid:$mid" : "$mid:$fid";
	}
	/*
	 * public function getUserScoreGroupWise($user_id){ $sql = "select sum(score) as score, group_id from user_psycho_test where user_id = $user_id and q_id in ($this->currentQunSet) group by group_id"; //$sql = "SELECT q_id FROM user_psycho_test WHERE user_id = $user_id and "; $exec = $this->conn->Execute($sql); $res = $exec->GetRows(); return $res; }
	 */
	private function getuserTest($user_id, $debug) {
		//$this->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		if ($debug == false)
			$sql = $this->conn->Prepare ( "select score, q_id,answer from user_psycho_test where  user_id = ?" );
		else
			$sql = $this->conn->Prepare ( "select p.question,u.score,u.q_id,u.answer from user_psycho_test u join psycho_questions_new p
on u.q_id=p.id where user_id = ?;" );
			
			// echo "select score, q_id,answer from user_psycho_test where user_id = $user_id";
		$exec = $this->conn->Execute ( $sql, array ($user_id));
		
		$res = $exec->GetRows ();
		$data = array ();
		foreach ( $res as $val ) {
			$data [$val ['q_id']] = $val;
		}
		// var_dump($res);
		return $data;
	}
}


if (strstr ( $_SERVER['REQUEST_URI'], basename ( __FILE__ ) )) {

	try{
	$matchScore=new matchScore();
	$fid=$_GET['fid'];
	$mid=$_GET['mid'];
	$output=$matchScore->calculateMatchScore($fid, $mid,"female",true);
	
	var_dump($output);
	}catch (Exception $e){
		trigger_error($e->getMessage());
		echo $e->getMessage();
	}
	
}

/*
 * $ms = new matchScore (); // var_dump($group_ids);942845 $male_id = $_REQUEST ['male_id']; $female_id = $_REQUEST ['female_id']; $debug = true; // $mid = 942845; $mScore = $ms->calculateMatchScore ( $female_id, $male_id, $debug );
 */

?>